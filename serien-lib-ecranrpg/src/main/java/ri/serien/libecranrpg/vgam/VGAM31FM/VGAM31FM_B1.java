
package ri.serien.libecranrpg.vgam.VGAM31FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM31FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM31FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    OP01.setValeursSelection("+", " ");
    OP02.setValeursSelection("+", " ");
    OP03.setValeursSelection("+", " ");
    OP04.setValeursSelection("+", " ");
    OP05.setValeursSelection("+", " ");
    OP06.setValeursSelection("+", " ");
    OP07.setValeursSelection("+", " ");
    OP08.setValeursSelection("+", " ");
    OP09.setValeursSelection("+", " ");
    OP10.setValeursSelection("+", " ");
    OP11.setValeursSelection("+", " ");
    OP12.setValeursSelection("+", " ");
    OP13.setValeursSelection("+", " ");
    OP14.setValeursSelection("+", " ");
    OP15.setValeursSelection("+", " ");
    OP16.setValeursSelection("+", " ");
    OP17.setValeursSelection("+", " ");
    OP18.setValeursSelection("+", " ");
    OP19.setValeursSelection("+", " ");
    OP20.setValeursSelection("+", " ");
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    FRCOL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRCOL@")).trim());
    FRFRS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRFRS@")).trim());
    FRNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOM@")).trim());
    CTNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTNUM@")).trim());
    CTLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTLIB@")).trim());
    ZON01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON01@")).trim());
    ZLI01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI01@")).trim());
    ZTY01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY01@")).trim());
    ZLN01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN01@")).trim());
    ZDE01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE01@")).trim());
    ZON02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON02@")).trim());
    ZLI02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI02@")).trim());
    ZTY2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY02@")).trim());
    ZLN2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN02@")).trim());
    ZDE2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE02@")).trim());
    ZON03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON03@")).trim());
    ZLI03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI03@")).trim());
    ZTY3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY03@")).trim());
    ZLN3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN03@")).trim());
    ZDE3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE03@")).trim());
    ZON04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON04@")).trim());
    ZLI04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI04@")).trim());
    ZTY4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY04@")).trim());
    ZLN4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN04@")).trim());
    ZDE4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE04@")).trim());
    ZON05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON05@")).trim());
    ZLI05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI05@")).trim());
    ZTY5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY05@")).trim());
    ZLN5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN05@")).trim());
    ZDE5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE05@")).trim());
    ZON06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON06@")).trim());
    ZLI06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI06@")).trim());
    ZTY6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY06@")).trim());
    ZLN6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN06@")).trim());
    ZDE6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE06@")).trim());
    ZON07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON07@")).trim());
    ZLI07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI07@")).trim());
    ZTY7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY07@")).trim());
    ZLN7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN07@")).trim());
    ZDE7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE07@")).trim());
    ZON08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON08@")).trim());
    ZLI08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI08@")).trim());
    ZTY8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY08@")).trim());
    ZLN8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN08@")).trim());
    ZDE8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE08@")).trim());
    ZON09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON09@")).trim());
    ZLI09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI09@")).trim());
    ZTY9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY09@")).trim());
    ZLN9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN09@")).trim());
    ZDE9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE09@")).trim());
    ZON10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON10@")).trim());
    ZLI10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI10@")).trim());
    ZTY10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY10@")).trim());
    ZLN10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN10@")).trim());
    ZDE10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE10@")).trim());
    ZON11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON11@")).trim());
    ZLI11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI11@")).trim());
    ZTY11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY11@")).trim());
    ZLN11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN11@")).trim());
    ZDE11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE11@")).trim());
    ZON12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON12@")).trim());
    ZLI12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI12@")).trim());
    ZTY12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY12@")).trim());
    ZLN12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN12@")).trim());
    ZDE12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE12@")).trim());
    ZON13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON13@")).trim());
    ZLI13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI13@")).trim());
    ZTY13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY13@")).trim());
    ZLN13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN13@")).trim());
    ZDE13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE13@")).trim());
    ZON14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON14@")).trim());
    ZLI14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI14@")).trim());
    ZTY14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY14@")).trim());
    ZLN14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN14@")).trim());
    ZDE14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE14@")).trim());
    ZON15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON15@")).trim());
    ZLI15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI15@")).trim());
    ZTY15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY15@")).trim());
    ZLN15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN15@")).trim());
    ZDE15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE15@")).trim());
    ZON16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON16@")).trim());
    ZLI16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI16@")).trim());
    ZTY16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY16@")).trim());
    ZLN16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN16@")).trim());
    ZDE16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE16@")).trim());
    ZON17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON17@")).trim());
    ZLI17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI17@")).trim());
    ZTY17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY17@")).trim());
    ZLN17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN17@")).trim());
    ZDE17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE17@")).trim());
    ZON18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON18@")).trim());
    ZLI18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI18@")).trim());
    ZTY18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY18@")).trim());
    ZLN18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN18@")).trim());
    ZDE18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE18@")).trim());
    ZON19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON19@")).trim());
    ZLI19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI19@")).trim());
    ZTY19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY19@")).trim());
    ZLN19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN19@")).trim());
    ZDE19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE19@")).trim());
    ZON20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZON20@")).trim());
    ZLI20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLI20@")).trim());
    ZTY20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZTY20@")).trim());
    ZLN20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZLN20@")).trim());
    ZDE20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZDE20@")).trim());
    button1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR01@")).trim());
    button2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR02@")).trim());
    button3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR03@")).trim());
    button4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR04@")).trim());
    button5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR05@")).trim());
    button6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR06@")).trim());
    button7.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR07@")).trim());
    button8.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR08@")).trim());
    button9.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR09@")).trim());
    button10.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR10@")).trim());
    button11.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR11@")).trim());
    button12.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR12@")).trim());
    button13.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR13@")).trim());
    button14.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR14@")).trim());
    button15.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR15@")).trim());
    button16.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR16@")).trim());
    button17.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR17@")).trim());
    button18.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR18@")).trim());
    button19.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR19@")).trim());
    button20.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR20@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    button1.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button2.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button3.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button4.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button5.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button6.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button7.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button8.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button9.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button10.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button11.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button12.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button13.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button14.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button15.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button16.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button17.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button18.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button19.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    button20.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    
    button1.setVisible(!lexique.HostFieldGetData("ERR01").trim().equalsIgnoreCase(""));
    button2.setVisible(!lexique.HostFieldGetData("ERR02").trim().equalsIgnoreCase(""));
    button3.setVisible(!lexique.HostFieldGetData("ERR03").trim().equalsIgnoreCase(""));
    button4.setVisible(!lexique.HostFieldGetData("ERR04").trim().equalsIgnoreCase(""));
    button5.setVisible(!lexique.HostFieldGetData("ERR05").trim().equalsIgnoreCase(""));
    button6.setVisible(!lexique.HostFieldGetData("ERR06").trim().equalsIgnoreCase(""));
    button7.setVisible(!lexique.HostFieldGetData("ERR07").trim().equalsIgnoreCase(""));
    button8.setVisible(!lexique.HostFieldGetData("ERR08").trim().equalsIgnoreCase(""));
    button9.setVisible(!lexique.HostFieldGetData("ERR09").trim().equalsIgnoreCase(""));
    button10.setVisible(!lexique.HostFieldGetData("ERR10").trim().equalsIgnoreCase(""));
    button11.setVisible(!lexique.HostFieldGetData("ERR11").trim().equalsIgnoreCase(""));
    button12.setVisible(!lexique.HostFieldGetData("ERR12").trim().equalsIgnoreCase(""));
    button13.setVisible(!lexique.HostFieldGetData("ERR13").trim().equalsIgnoreCase(""));
    button14.setVisible(!lexique.HostFieldGetData("ERR14").trim().equalsIgnoreCase(""));
    button15.setVisible(!lexique.HostFieldGetData("ERR15").trim().equalsIgnoreCase(""));
    button16.setVisible(!lexique.HostFieldGetData("ERR16").trim().equalsIgnoreCase(""));
    button17.setVisible(!lexique.HostFieldGetData("ERR17").trim().equalsIgnoreCase(""));
    button18.setVisible(!lexique.HostFieldGetData("ERR18").trim().equalsIgnoreCase(""));
    button19.setVisible(!lexique.HostFieldGetData("ERR19").trim().equalsIgnoreCase(""));
    button20.setVisible(!lexique.HostFieldGetData("ERR20").trim().equalsIgnoreCase(""));
    
    visibilite();
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void visibilite() {
    XRiCheckBox[] cases =
        { OP01, OP02, OP03, OP04, OP05, OP06, OP07, OP08, OP09, OP10, OP11, OP12, OP13, OP14, OP15, OP16, OP17, OP18, OP19, OP20 };
    XRiTextField[] tabN =
        { N201, N202, N203, N204, N205, N206, N207, N208, N209, N210, N211, N212, N213, N214, N215, N216, N217, N218, N219, N220 };
    XRiTextField[] tabD =
        { D201, D202, D203, D204, D205, D206, D207, D208, D209, D210, D211, D212, D213, D214, D215, D216, D217, D218, D219, D220 };
    XRiTextField[] tabF =
        { F201, F202, F203, F204, F205, F206, F207, F208, F209, F210, F211, F212, F213, F214, F215, F216, F217, F218, F219, F220 };
    
    for (int i = 0; i < cases.length; i++) {
      tabN[i].setVisible(cases[i].isSelected());
      tabD[i].setVisible(cases[i].isSelected());
      tabF[i].setVisible(cases[i].isSelected());
      if (!tabN[i].isVisible()) {
        tabN[i].setText("");
      }
      if (!tabD[i].isVisible()) {
        tabD[i].setText("");
      }
      if (!tabF[i].isVisible()) {
        tabF[i].setText("");
      }
    }
  }
  
  private void OP01MouseClicked(MouseEvent e) {
    visibilite();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_66 = new JLabel();
    FRETB = new XRiTextField();
    OBJ_68 = new JLabel();
    FRCOL = new RiZoneSortie();
    FRFRS = new RiZoneSortie();
    FRNOM = new RiZoneSortie();
    label1 = new JLabel();
    CTNUM = new RiZoneSortie();
    CTLIB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    panel2 = new JPanel();
    ZON01 = new RiZoneSortie();
    ZLI01 = new RiZoneSortie();
    ZTY01 = new RiZoneSortie();
    ZLN01 = new RiZoneSortie();
    ZDE01 = new RiZoneSortie();
    ZON02 = new RiZoneSortie();
    ZLI02 = new RiZoneSortie();
    ZTY2 = new RiZoneSortie();
    ZLN2 = new RiZoneSortie();
    ZDE2 = new RiZoneSortie();
    ZON03 = new RiZoneSortie();
    ZLI03 = new RiZoneSortie();
    ZTY3 = new RiZoneSortie();
    ZLN3 = new RiZoneSortie();
    ZDE3 = new RiZoneSortie();
    ZON04 = new RiZoneSortie();
    ZLI04 = new RiZoneSortie();
    ZTY4 = new RiZoneSortie();
    ZLN4 = new RiZoneSortie();
    ZDE4 = new RiZoneSortie();
    ZON05 = new RiZoneSortie();
    ZLI05 = new RiZoneSortie();
    ZTY5 = new RiZoneSortie();
    ZLN5 = new RiZoneSortie();
    ZDE5 = new RiZoneSortie();
    ZON06 = new RiZoneSortie();
    ZLI06 = new RiZoneSortie();
    ZTY6 = new RiZoneSortie();
    ZLN6 = new RiZoneSortie();
    ZDE6 = new RiZoneSortie();
    ZON07 = new RiZoneSortie();
    ZLI07 = new RiZoneSortie();
    ZTY7 = new RiZoneSortie();
    ZLN7 = new RiZoneSortie();
    ZDE7 = new RiZoneSortie();
    ZON08 = new RiZoneSortie();
    ZLI08 = new RiZoneSortie();
    ZTY8 = new RiZoneSortie();
    ZLN8 = new RiZoneSortie();
    ZDE8 = new RiZoneSortie();
    ZON09 = new RiZoneSortie();
    ZLI09 = new RiZoneSortie();
    ZTY9 = new RiZoneSortie();
    ZLN9 = new RiZoneSortie();
    ZDE9 = new RiZoneSortie();
    ZON10 = new RiZoneSortie();
    ZLI10 = new RiZoneSortie();
    ZTY10 = new RiZoneSortie();
    ZLN10 = new RiZoneSortie();
    ZDE10 = new RiZoneSortie();
    ZON11 = new RiZoneSortie();
    ZLI11 = new RiZoneSortie();
    ZTY11 = new RiZoneSortie();
    ZLN11 = new RiZoneSortie();
    ZDE11 = new RiZoneSortie();
    ZON12 = new RiZoneSortie();
    ZLI12 = new RiZoneSortie();
    ZTY12 = new RiZoneSortie();
    ZLN12 = new RiZoneSortie();
    ZDE12 = new RiZoneSortie();
    ZON13 = new RiZoneSortie();
    ZLI13 = new RiZoneSortie();
    ZTY13 = new RiZoneSortie();
    ZLN13 = new RiZoneSortie();
    ZDE13 = new RiZoneSortie();
    ZON14 = new RiZoneSortie();
    ZLI14 = new RiZoneSortie();
    ZTY14 = new RiZoneSortie();
    ZLN14 = new RiZoneSortie();
    ZDE14 = new RiZoneSortie();
    ZON15 = new RiZoneSortie();
    ZLI15 = new RiZoneSortie();
    ZTY15 = new RiZoneSortie();
    ZLN15 = new RiZoneSortie();
    ZDE15 = new RiZoneSortie();
    ZON16 = new RiZoneSortie();
    ZLI16 = new RiZoneSortie();
    ZTY16 = new RiZoneSortie();
    ZLN16 = new RiZoneSortie();
    ZDE16 = new RiZoneSortie();
    ZON17 = new RiZoneSortie();
    ZLI17 = new RiZoneSortie();
    ZTY17 = new RiZoneSortie();
    ZLN17 = new RiZoneSortie();
    ZDE17 = new RiZoneSortie();
    ZON18 = new RiZoneSortie();
    ZLI18 = new RiZoneSortie();
    ZTY18 = new RiZoneSortie();
    ZLN18 = new RiZoneSortie();
    ZDE18 = new RiZoneSortie();
    ZON19 = new RiZoneSortie();
    ZLI19 = new RiZoneSortie();
    ZTY19 = new RiZoneSortie();
    ZLN19 = new RiZoneSortie();
    ZDE19 = new RiZoneSortie();
    ZON20 = new RiZoneSortie();
    ZLI20 = new RiZoneSortie();
    ZTY20 = new RiZoneSortie();
    ZLN20 = new RiZoneSortie();
    ZDE20 = new RiZoneSortie();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    panel3 = new JPanel();
    N101 = new XRiTextField();
    D101 = new XRiTextField();
    F101 = new XRiTextField();
    N201 = new XRiTextField();
    D201 = new XRiTextField();
    F201 = new XRiTextField();
    OP01 = new XRiCheckBox();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    N102 = new XRiTextField();
    D102 = new XRiTextField();
    F102 = new XRiTextField();
    N202 = new XRiTextField();
    D202 = new XRiTextField();
    F202 = new XRiTextField();
    OP02 = new XRiCheckBox();
    N103 = new XRiTextField();
    D103 = new XRiTextField();
    F103 = new XRiTextField();
    N203 = new XRiTextField();
    D203 = new XRiTextField();
    F203 = new XRiTextField();
    OP03 = new XRiCheckBox();
    N104 = new XRiTextField();
    D104 = new XRiTextField();
    F104 = new XRiTextField();
    N204 = new XRiTextField();
    D204 = new XRiTextField();
    F204 = new XRiTextField();
    OP04 = new XRiCheckBox();
    N105 = new XRiTextField();
    D105 = new XRiTextField();
    F105 = new XRiTextField();
    N205 = new XRiTextField();
    D205 = new XRiTextField();
    F205 = new XRiTextField();
    OP05 = new XRiCheckBox();
    N106 = new XRiTextField();
    D106 = new XRiTextField();
    F106 = new XRiTextField();
    N206 = new XRiTextField();
    D206 = new XRiTextField();
    F206 = new XRiTextField();
    OP06 = new XRiCheckBox();
    N107 = new XRiTextField();
    D107 = new XRiTextField();
    F107 = new XRiTextField();
    N207 = new XRiTextField();
    D207 = new XRiTextField();
    F207 = new XRiTextField();
    OP07 = new XRiCheckBox();
    N108 = new XRiTextField();
    D108 = new XRiTextField();
    F108 = new XRiTextField();
    N208 = new XRiTextField();
    D208 = new XRiTextField();
    F208 = new XRiTextField();
    OP08 = new XRiCheckBox();
    N109 = new XRiTextField();
    D109 = new XRiTextField();
    F109 = new XRiTextField();
    N209 = new XRiTextField();
    D209 = new XRiTextField();
    F209 = new XRiTextField();
    OP09 = new XRiCheckBox();
    N110 = new XRiTextField();
    D110 = new XRiTextField();
    F110 = new XRiTextField();
    N210 = new XRiTextField();
    D210 = new XRiTextField();
    F210 = new XRiTextField();
    OP10 = new XRiCheckBox();
    N111 = new XRiTextField();
    D111 = new XRiTextField();
    F111 = new XRiTextField();
    N211 = new XRiTextField();
    D211 = new XRiTextField();
    F211 = new XRiTextField();
    OP11 = new XRiCheckBox();
    N112 = new XRiTextField();
    D112 = new XRiTextField();
    F112 = new XRiTextField();
    N212 = new XRiTextField();
    D212 = new XRiTextField();
    F212 = new XRiTextField();
    OP12 = new XRiCheckBox();
    N113 = new XRiTextField();
    D113 = new XRiTextField();
    F113 = new XRiTextField();
    N213 = new XRiTextField();
    D213 = new XRiTextField();
    F213 = new XRiTextField();
    OP13 = new XRiCheckBox();
    N114 = new XRiTextField();
    D114 = new XRiTextField();
    F114 = new XRiTextField();
    N214 = new XRiTextField();
    D214 = new XRiTextField();
    F214 = new XRiTextField();
    OP14 = new XRiCheckBox();
    N115 = new XRiTextField();
    D115 = new XRiTextField();
    F115 = new XRiTextField();
    N215 = new XRiTextField();
    D215 = new XRiTextField();
    F215 = new XRiTextField();
    OP15 = new XRiCheckBox();
    N116 = new XRiTextField();
    D116 = new XRiTextField();
    F116 = new XRiTextField();
    N216 = new XRiTextField();
    D216 = new XRiTextField();
    F216 = new XRiTextField();
    OP16 = new XRiCheckBox();
    N117 = new XRiTextField();
    D117 = new XRiTextField();
    F117 = new XRiTextField();
    N217 = new XRiTextField();
    D217 = new XRiTextField();
    F217 = new XRiTextField();
    OP17 = new XRiCheckBox();
    N118 = new XRiTextField();
    D118 = new XRiTextField();
    F118 = new XRiTextField();
    N218 = new XRiTextField();
    D218 = new XRiTextField();
    F218 = new XRiTextField();
    OP18 = new XRiCheckBox();
    N119 = new XRiTextField();
    D119 = new XRiTextField();
    F119 = new XRiTextField();
    N219 = new XRiTextField();
    D219 = new XRiTextField();
    F219 = new XRiTextField();
    OP19 = new XRiCheckBox();
    N120 = new XRiTextField();
    D120 = new XRiTextField();
    F120 = new XRiTextField();
    N220 = new XRiTextField();
    D220 = new XRiTextField();
    F220 = new XRiTextField();
    OP20 = new XRiCheckBox();
    button1 = new JButton();
    button2 = new JButton();
    button3 = new JButton();
    button4 = new JButton();
    button5 = new JButton();
    button6 = new JButton();
    button7 = new JButton();
    button8 = new JButton();
    button9 = new JButton();
    button10 = new JButton();
    button11 = new JButton();
    button12 = new JButton();
    button13 = new JButton();
    button14 = new JButton();
    button15 = new JButton();
    button16 = new JButton();
    button17 = new JButton();
    button18 = new JButton();
    button19 = new JButton();
    button20 = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Param\u00e9trage du catalogue fournisseur");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(950, 0));
          p_tete_gauche.setMinimumSize(new Dimension(850, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_66 ----
          OBJ_66.setText("Etablissement");
          OBJ_66.setName("OBJ_66");
          p_tete_gauche.add(OBJ_66);
          OBJ_66.setBounds(5, 5, 95, 20);

          //---- FRETB ----
          FRETB.setComponentPopupMenu(null);
          FRETB.setName("FRETB");
          p_tete_gauche.add(FRETB);
          FRETB.setBounds(105, 1, 40, FRETB.getPreferredSize().height);

          //---- OBJ_68 ----
          OBJ_68.setText("Fournisseur");
          OBJ_68.setName("OBJ_68");
          p_tete_gauche.add(OBJ_68);
          OBJ_68.setBounds(165, 5, 78, 20);

          //---- FRCOL ----
          FRCOL.setComponentPopupMenu(null);
          FRCOL.setText("@FRCOL@");
          FRCOL.setName("FRCOL");
          p_tete_gauche.add(FRCOL);
          FRCOL.setBounds(245, 3, 20, FRCOL.getPreferredSize().height);

          //---- FRFRS ----
          FRFRS.setComponentPopupMenu(null);
          FRFRS.setText("@FRFRS@");
          FRFRS.setName("FRFRS");
          p_tete_gauche.add(FRFRS);
          FRFRS.setBounds(270, 3, 58, FRFRS.getPreferredSize().height);

          //---- FRNOM ----
          FRNOM.setText("@FRNOM@");
          FRNOM.setName("FRNOM");
          p_tete_gauche.add(FRNOM);
          FRNOM.setBounds(335, 3, 260, FRNOM.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Catalogue");
          label1.setName("label1");
          p_tete_gauche.add(label1);
          label1.setBounds(605, 3, 85, 25);

          //---- CTNUM ----
          CTNUM.setText("@CTNUM@");
          CTNUM.setName("CTNUM");
          p_tete_gauche.add(CTNUM);
          CTNUM.setBounds(695, 3, 40, CTNUM.getPreferredSize().height);

          //---- CTLIB ----
          CTLIB.setText("@CTLIB@");
          CTLIB.setName("CTLIB");
          p_tete_gauche.add(CTLIB);
          CTLIB.setBounds(740, 3, 245, CTLIB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(980, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(971, 600));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder(new LineBorder(new Color(204, 102, 0), 3), "Zones de S\u00e9rie N", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION,
                new Font("sansserif", Font.BOLD, 14), new Color(193, 77, 0)));
              panel2.setOpaque(false);
              panel2.setForeground(Color.black);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- ZON01 ----
              ZON01.setText("@ZON01@");
              ZON01.setName("ZON01");
              panel2.add(ZON01);
              ZON01.setBounds(10, 55, 114, ZON01.getPreferredSize().height);

              //---- ZLI01 ----
              ZLI01.setText("@ZLI01@");
              ZLI01.setName("ZLI01");
              panel2.add(ZLI01);
              ZLI01.setBounds(130, 55, 334, ZLI01.getPreferredSize().height);

              //---- ZTY01 ----
              ZTY01.setText("@ZTY01@");
              ZTY01.setName("ZTY01");
              panel2.add(ZTY01);
              ZTY01.setBounds(470, 55, 20, ZTY01.getPreferredSize().height);

              //---- ZLN01 ----
              ZLN01.setText("@ZLN01@");
              ZLN01.setName("ZLN01");
              panel2.add(ZLN01);
              ZLN01.setBounds(495, 55, 40, ZLN01.getPreferredSize().height);

              //---- ZDE01 ----
              ZDE01.setText("@ZDE01@");
              ZDE01.setName("ZDE01");
              panel2.add(ZDE01);
              ZDE01.setBounds(540, 55, 28, 24);

              //---- ZON02 ----
              ZON02.setText("@ZON02@");
              ZON02.setName("ZON02");
              panel2.add(ZON02);
              ZON02.setBounds(10, 80, 114, ZON02.getPreferredSize().height);

              //---- ZLI02 ----
              ZLI02.setText("@ZLI02@");
              ZLI02.setName("ZLI02");
              panel2.add(ZLI02);
              ZLI02.setBounds(130, 80, 334, ZLI02.getPreferredSize().height);

              //---- ZTY2 ----
              ZTY2.setText("@ZTY02@");
              ZTY2.setName("ZTY2");
              panel2.add(ZTY2);
              ZTY2.setBounds(470, 80, 20, ZTY2.getPreferredSize().height);

              //---- ZLN2 ----
              ZLN2.setText("@ZLN02@");
              ZLN2.setName("ZLN2");
              panel2.add(ZLN2);
              ZLN2.setBounds(495, 80, 40, ZLN2.getPreferredSize().height);

              //---- ZDE2 ----
              ZDE2.setText("@ZDE02@");
              ZDE2.setName("ZDE2");
              panel2.add(ZDE2);
              ZDE2.setBounds(540, 80, 28, ZDE2.getPreferredSize().height);

              //---- ZON03 ----
              ZON03.setText("@ZON03@");
              ZON03.setName("ZON03");
              panel2.add(ZON03);
              ZON03.setBounds(10, 105, 114, ZON03.getPreferredSize().height);

              //---- ZLI03 ----
              ZLI03.setText("@ZLI03@");
              ZLI03.setName("ZLI03");
              panel2.add(ZLI03);
              ZLI03.setBounds(130, 105, 334, ZLI03.getPreferredSize().height);

              //---- ZTY3 ----
              ZTY3.setText("@ZTY03@");
              ZTY3.setName("ZTY3");
              panel2.add(ZTY3);
              ZTY3.setBounds(470, 105, 20, ZTY3.getPreferredSize().height);

              //---- ZLN3 ----
              ZLN3.setText("@ZLN03@");
              ZLN3.setName("ZLN3");
              panel2.add(ZLN3);
              ZLN3.setBounds(495, 105, 40, ZLN3.getPreferredSize().height);

              //---- ZDE3 ----
              ZDE3.setText("@ZDE03@");
              ZDE3.setName("ZDE3");
              panel2.add(ZDE3);
              ZDE3.setBounds(540, 105, 28, ZDE3.getPreferredSize().height);

              //---- ZON04 ----
              ZON04.setText("@ZON04@");
              ZON04.setName("ZON04");
              panel2.add(ZON04);
              ZON04.setBounds(10, 130, 114, ZON04.getPreferredSize().height);

              //---- ZLI04 ----
              ZLI04.setText("@ZLI04@");
              ZLI04.setName("ZLI04");
              panel2.add(ZLI04);
              ZLI04.setBounds(130, 130, 334, ZLI04.getPreferredSize().height);

              //---- ZTY4 ----
              ZTY4.setText("@ZTY04@");
              ZTY4.setName("ZTY4");
              panel2.add(ZTY4);
              ZTY4.setBounds(470, 130, 20, ZTY4.getPreferredSize().height);

              //---- ZLN4 ----
              ZLN4.setText("@ZLN04@");
              ZLN4.setName("ZLN4");
              panel2.add(ZLN4);
              ZLN4.setBounds(495, 130, 40, ZLN4.getPreferredSize().height);

              //---- ZDE4 ----
              ZDE4.setText("@ZDE04@");
              ZDE4.setName("ZDE4");
              panel2.add(ZDE4);
              ZDE4.setBounds(540, 130, 28, ZDE4.getPreferredSize().height);

              //---- ZON05 ----
              ZON05.setText("@ZON05@");
              ZON05.setName("ZON05");
              panel2.add(ZON05);
              ZON05.setBounds(10, 155, 114, ZON05.getPreferredSize().height);

              //---- ZLI05 ----
              ZLI05.setText("@ZLI05@");
              ZLI05.setName("ZLI05");
              panel2.add(ZLI05);
              ZLI05.setBounds(130, 155, 334, ZLI05.getPreferredSize().height);

              //---- ZTY5 ----
              ZTY5.setText("@ZTY05@");
              ZTY5.setName("ZTY5");
              panel2.add(ZTY5);
              ZTY5.setBounds(470, 155, 20, ZTY5.getPreferredSize().height);

              //---- ZLN5 ----
              ZLN5.setText("@ZLN05@");
              ZLN5.setName("ZLN5");
              panel2.add(ZLN5);
              ZLN5.setBounds(495, 155, 40, ZLN5.getPreferredSize().height);

              //---- ZDE5 ----
              ZDE5.setText("@ZDE05@");
              ZDE5.setName("ZDE5");
              panel2.add(ZDE5);
              ZDE5.setBounds(540, 155, 28, ZDE5.getPreferredSize().height);

              //---- ZON06 ----
              ZON06.setText("@ZON06@");
              ZON06.setName("ZON06");
              panel2.add(ZON06);
              ZON06.setBounds(10, 180, 114, ZON06.getPreferredSize().height);

              //---- ZLI06 ----
              ZLI06.setText("@ZLI06@");
              ZLI06.setName("ZLI06");
              panel2.add(ZLI06);
              ZLI06.setBounds(130, 180, 334, ZLI06.getPreferredSize().height);

              //---- ZTY6 ----
              ZTY6.setText("@ZTY06@");
              ZTY6.setName("ZTY6");
              panel2.add(ZTY6);
              ZTY6.setBounds(470, 180, 20, ZTY6.getPreferredSize().height);

              //---- ZLN6 ----
              ZLN6.setText("@ZLN06@");
              ZLN6.setName("ZLN6");
              panel2.add(ZLN6);
              ZLN6.setBounds(495, 180, 40, ZLN6.getPreferredSize().height);

              //---- ZDE6 ----
              ZDE6.setText("@ZDE06@");
              ZDE6.setName("ZDE6");
              panel2.add(ZDE6);
              ZDE6.setBounds(540, 180, 28, ZDE6.getPreferredSize().height);

              //---- ZON07 ----
              ZON07.setText("@ZON07@");
              ZON07.setName("ZON07");
              panel2.add(ZON07);
              ZON07.setBounds(10, 205, 114, ZON07.getPreferredSize().height);

              //---- ZLI07 ----
              ZLI07.setText("@ZLI07@");
              ZLI07.setName("ZLI07");
              panel2.add(ZLI07);
              ZLI07.setBounds(130, 205, 334, ZLI07.getPreferredSize().height);

              //---- ZTY7 ----
              ZTY7.setText("@ZTY07@");
              ZTY7.setName("ZTY7");
              panel2.add(ZTY7);
              ZTY7.setBounds(470, 205, 20, ZTY7.getPreferredSize().height);

              //---- ZLN7 ----
              ZLN7.setText("@ZLN07@");
              ZLN7.setName("ZLN7");
              panel2.add(ZLN7);
              ZLN7.setBounds(495, 205, 40, ZLN7.getPreferredSize().height);

              //---- ZDE7 ----
              ZDE7.setText("@ZDE07@");
              ZDE7.setName("ZDE7");
              panel2.add(ZDE7);
              ZDE7.setBounds(540, 205, 28, ZDE7.getPreferredSize().height);

              //---- ZON08 ----
              ZON08.setText("@ZON08@");
              ZON08.setName("ZON08");
              panel2.add(ZON08);
              ZON08.setBounds(10, 230, 114, ZON08.getPreferredSize().height);

              //---- ZLI08 ----
              ZLI08.setText("@ZLI08@");
              ZLI08.setName("ZLI08");
              panel2.add(ZLI08);
              ZLI08.setBounds(130, 230, 334, ZLI08.getPreferredSize().height);

              //---- ZTY8 ----
              ZTY8.setText("@ZTY08@");
              ZTY8.setName("ZTY8");
              panel2.add(ZTY8);
              ZTY8.setBounds(470, 230, 20, ZTY8.getPreferredSize().height);

              //---- ZLN8 ----
              ZLN8.setText("@ZLN08@");
              ZLN8.setName("ZLN8");
              panel2.add(ZLN8);
              ZLN8.setBounds(495, 230, 40, ZLN8.getPreferredSize().height);

              //---- ZDE8 ----
              ZDE8.setText("@ZDE08@");
              ZDE8.setName("ZDE8");
              panel2.add(ZDE8);
              ZDE8.setBounds(540, 230, 28, ZDE8.getPreferredSize().height);

              //---- ZON09 ----
              ZON09.setText("@ZON09@");
              ZON09.setName("ZON09");
              panel2.add(ZON09);
              ZON09.setBounds(10, 255, 114, ZON09.getPreferredSize().height);

              //---- ZLI09 ----
              ZLI09.setText("@ZLI09@");
              ZLI09.setName("ZLI09");
              panel2.add(ZLI09);
              ZLI09.setBounds(130, 255, 334, ZLI09.getPreferredSize().height);

              //---- ZTY9 ----
              ZTY9.setText("@ZTY09@");
              ZTY9.setName("ZTY9");
              panel2.add(ZTY9);
              ZTY9.setBounds(470, 255, 20, ZTY9.getPreferredSize().height);

              //---- ZLN9 ----
              ZLN9.setText("@ZLN09@");
              ZLN9.setName("ZLN9");
              panel2.add(ZLN9);
              ZLN9.setBounds(495, 255, 40, ZLN9.getPreferredSize().height);

              //---- ZDE9 ----
              ZDE9.setText("@ZDE09@");
              ZDE9.setName("ZDE9");
              panel2.add(ZDE9);
              ZDE9.setBounds(540, 255, 28, ZDE9.getPreferredSize().height);

              //---- ZON10 ----
              ZON10.setText("@ZON10@");
              ZON10.setName("ZON10");
              panel2.add(ZON10);
              ZON10.setBounds(10, 280, 114, ZON10.getPreferredSize().height);

              //---- ZLI10 ----
              ZLI10.setText("@ZLI10@");
              ZLI10.setName("ZLI10");
              panel2.add(ZLI10);
              ZLI10.setBounds(130, 280, 334, ZLI10.getPreferredSize().height);

              //---- ZTY10 ----
              ZTY10.setText("@ZTY10@");
              ZTY10.setName("ZTY10");
              panel2.add(ZTY10);
              ZTY10.setBounds(470, 280, 20, ZTY10.getPreferredSize().height);

              //---- ZLN10 ----
              ZLN10.setText("@ZLN10@");
              ZLN10.setName("ZLN10");
              panel2.add(ZLN10);
              ZLN10.setBounds(495, 280, 40, ZLN10.getPreferredSize().height);

              //---- ZDE10 ----
              ZDE10.setText("@ZDE10@");
              ZDE10.setName("ZDE10");
              panel2.add(ZDE10);
              ZDE10.setBounds(540, 280, 28, ZDE10.getPreferredSize().height);

              //---- ZON11 ----
              ZON11.setText("@ZON11@");
              ZON11.setName("ZON11");
              panel2.add(ZON11);
              ZON11.setBounds(10, 305, 114, ZON11.getPreferredSize().height);

              //---- ZLI11 ----
              ZLI11.setText("@ZLI11@");
              ZLI11.setName("ZLI11");
              panel2.add(ZLI11);
              ZLI11.setBounds(130, 305, 334, ZLI11.getPreferredSize().height);

              //---- ZTY11 ----
              ZTY11.setText("@ZTY11@");
              ZTY11.setName("ZTY11");
              panel2.add(ZTY11);
              ZTY11.setBounds(470, 305, 20, ZTY11.getPreferredSize().height);

              //---- ZLN11 ----
              ZLN11.setText("@ZLN11@");
              ZLN11.setName("ZLN11");
              panel2.add(ZLN11);
              ZLN11.setBounds(495, 305, 40, ZLN11.getPreferredSize().height);

              //---- ZDE11 ----
              ZDE11.setText("@ZDE11@");
              ZDE11.setName("ZDE11");
              panel2.add(ZDE11);
              ZDE11.setBounds(540, 305, 28, ZDE11.getPreferredSize().height);

              //---- ZON12 ----
              ZON12.setText("@ZON12@");
              ZON12.setName("ZON12");
              panel2.add(ZON12);
              ZON12.setBounds(10, 330, 114, ZON12.getPreferredSize().height);

              //---- ZLI12 ----
              ZLI12.setText("@ZLI12@");
              ZLI12.setName("ZLI12");
              panel2.add(ZLI12);
              ZLI12.setBounds(130, 330, 334, ZLI12.getPreferredSize().height);

              //---- ZTY12 ----
              ZTY12.setText("@ZTY12@");
              ZTY12.setName("ZTY12");
              panel2.add(ZTY12);
              ZTY12.setBounds(470, 330, 20, ZTY12.getPreferredSize().height);

              //---- ZLN12 ----
              ZLN12.setText("@ZLN12@");
              ZLN12.setName("ZLN12");
              panel2.add(ZLN12);
              ZLN12.setBounds(495, 330, 40, ZLN12.getPreferredSize().height);

              //---- ZDE12 ----
              ZDE12.setText("@ZDE12@");
              ZDE12.setName("ZDE12");
              panel2.add(ZDE12);
              ZDE12.setBounds(540, 330, 28, ZDE12.getPreferredSize().height);

              //---- ZON13 ----
              ZON13.setText("@ZON13@");
              ZON13.setName("ZON13");
              panel2.add(ZON13);
              ZON13.setBounds(10, 355, 114, ZON13.getPreferredSize().height);

              //---- ZLI13 ----
              ZLI13.setText("@ZLI13@");
              ZLI13.setName("ZLI13");
              panel2.add(ZLI13);
              ZLI13.setBounds(130, 355, 334, ZLI13.getPreferredSize().height);

              //---- ZTY13 ----
              ZTY13.setText("@ZTY13@");
              ZTY13.setName("ZTY13");
              panel2.add(ZTY13);
              ZTY13.setBounds(470, 355, 20, ZTY13.getPreferredSize().height);

              //---- ZLN13 ----
              ZLN13.setText("@ZLN13@");
              ZLN13.setName("ZLN13");
              panel2.add(ZLN13);
              ZLN13.setBounds(495, 355, 40, ZLN13.getPreferredSize().height);

              //---- ZDE13 ----
              ZDE13.setText("@ZDE13@");
              ZDE13.setName("ZDE13");
              panel2.add(ZDE13);
              ZDE13.setBounds(540, 355, 28, ZDE13.getPreferredSize().height);

              //---- ZON14 ----
              ZON14.setText("@ZON14@");
              ZON14.setName("ZON14");
              panel2.add(ZON14);
              ZON14.setBounds(10, 380, 114, ZON14.getPreferredSize().height);

              //---- ZLI14 ----
              ZLI14.setText("@ZLI14@");
              ZLI14.setName("ZLI14");
              panel2.add(ZLI14);
              ZLI14.setBounds(130, 380, 334, ZLI14.getPreferredSize().height);

              //---- ZTY14 ----
              ZTY14.setText("@ZTY14@");
              ZTY14.setName("ZTY14");
              panel2.add(ZTY14);
              ZTY14.setBounds(470, 380, 20, ZTY14.getPreferredSize().height);

              //---- ZLN14 ----
              ZLN14.setText("@ZLN14@");
              ZLN14.setName("ZLN14");
              panel2.add(ZLN14);
              ZLN14.setBounds(495, 380, 40, ZLN14.getPreferredSize().height);

              //---- ZDE14 ----
              ZDE14.setText("@ZDE14@");
              ZDE14.setName("ZDE14");
              panel2.add(ZDE14);
              ZDE14.setBounds(540, 380, 28, ZDE14.getPreferredSize().height);

              //---- ZON15 ----
              ZON15.setText("@ZON15@");
              ZON15.setName("ZON15");
              panel2.add(ZON15);
              ZON15.setBounds(10, 405, 114, ZON15.getPreferredSize().height);

              //---- ZLI15 ----
              ZLI15.setText("@ZLI15@");
              ZLI15.setName("ZLI15");
              panel2.add(ZLI15);
              ZLI15.setBounds(130, 405, 334, ZLI15.getPreferredSize().height);

              //---- ZTY15 ----
              ZTY15.setText("@ZTY15@");
              ZTY15.setName("ZTY15");
              panel2.add(ZTY15);
              ZTY15.setBounds(470, 405, 20, ZTY15.getPreferredSize().height);

              //---- ZLN15 ----
              ZLN15.setText("@ZLN15@");
              ZLN15.setName("ZLN15");
              panel2.add(ZLN15);
              ZLN15.setBounds(495, 405, 40, ZLN15.getPreferredSize().height);

              //---- ZDE15 ----
              ZDE15.setText("@ZDE15@");
              ZDE15.setName("ZDE15");
              panel2.add(ZDE15);
              ZDE15.setBounds(540, 405, 28, ZDE15.getPreferredSize().height);

              //---- ZON16 ----
              ZON16.setText("@ZON16@");
              ZON16.setName("ZON16");
              panel2.add(ZON16);
              ZON16.setBounds(10, 430, 114, ZON16.getPreferredSize().height);

              //---- ZLI16 ----
              ZLI16.setText("@ZLI16@");
              ZLI16.setName("ZLI16");
              panel2.add(ZLI16);
              ZLI16.setBounds(130, 430, 334, ZLI16.getPreferredSize().height);

              //---- ZTY16 ----
              ZTY16.setText("@ZTY16@");
              ZTY16.setName("ZTY16");
              panel2.add(ZTY16);
              ZTY16.setBounds(470, 430, 20, ZTY16.getPreferredSize().height);

              //---- ZLN16 ----
              ZLN16.setText("@ZLN16@");
              ZLN16.setName("ZLN16");
              panel2.add(ZLN16);
              ZLN16.setBounds(495, 430, 40, ZLN16.getPreferredSize().height);

              //---- ZDE16 ----
              ZDE16.setText("@ZDE16@");
              ZDE16.setName("ZDE16");
              panel2.add(ZDE16);
              ZDE16.setBounds(540, 430, 28, ZDE16.getPreferredSize().height);

              //---- ZON17 ----
              ZON17.setText("@ZON17@");
              ZON17.setName("ZON17");
              panel2.add(ZON17);
              ZON17.setBounds(10, 455, 114, ZON17.getPreferredSize().height);

              //---- ZLI17 ----
              ZLI17.setText("@ZLI17@");
              ZLI17.setName("ZLI17");
              panel2.add(ZLI17);
              ZLI17.setBounds(130, 455, 334, ZLI17.getPreferredSize().height);

              //---- ZTY17 ----
              ZTY17.setText("@ZTY17@");
              ZTY17.setName("ZTY17");
              panel2.add(ZTY17);
              ZTY17.setBounds(470, 455, 20, ZTY17.getPreferredSize().height);

              //---- ZLN17 ----
              ZLN17.setText("@ZLN17@");
              ZLN17.setName("ZLN17");
              panel2.add(ZLN17);
              ZLN17.setBounds(495, 455, 40, ZLN17.getPreferredSize().height);

              //---- ZDE17 ----
              ZDE17.setText("@ZDE17@");
              ZDE17.setName("ZDE17");
              panel2.add(ZDE17);
              ZDE17.setBounds(540, 455, 28, ZDE17.getPreferredSize().height);

              //---- ZON18 ----
              ZON18.setText("@ZON18@");
              ZON18.setName("ZON18");
              panel2.add(ZON18);
              ZON18.setBounds(10, 480, 114, ZON18.getPreferredSize().height);

              //---- ZLI18 ----
              ZLI18.setText("@ZLI18@");
              ZLI18.setName("ZLI18");
              panel2.add(ZLI18);
              ZLI18.setBounds(130, 480, 334, ZLI18.getPreferredSize().height);

              //---- ZTY18 ----
              ZTY18.setText("@ZTY18@");
              ZTY18.setName("ZTY18");
              panel2.add(ZTY18);
              ZTY18.setBounds(470, 480, 20, ZTY18.getPreferredSize().height);

              //---- ZLN18 ----
              ZLN18.setText("@ZLN18@");
              ZLN18.setName("ZLN18");
              panel2.add(ZLN18);
              ZLN18.setBounds(495, 480, 40, ZLN18.getPreferredSize().height);

              //---- ZDE18 ----
              ZDE18.setText("@ZDE18@");
              ZDE18.setName("ZDE18");
              panel2.add(ZDE18);
              ZDE18.setBounds(540, 480, 28, ZDE18.getPreferredSize().height);

              //---- ZON19 ----
              ZON19.setText("@ZON19@");
              ZON19.setName("ZON19");
              panel2.add(ZON19);
              ZON19.setBounds(10, 505, 114, ZON19.getPreferredSize().height);

              //---- ZLI19 ----
              ZLI19.setText("@ZLI19@");
              ZLI19.setName("ZLI19");
              panel2.add(ZLI19);
              ZLI19.setBounds(130, 505, 334, ZLI19.getPreferredSize().height);

              //---- ZTY19 ----
              ZTY19.setText("@ZTY19@");
              ZTY19.setName("ZTY19");
              panel2.add(ZTY19);
              ZTY19.setBounds(470, 505, 20, ZTY19.getPreferredSize().height);

              //---- ZLN19 ----
              ZLN19.setText("@ZLN19@");
              ZLN19.setName("ZLN19");
              panel2.add(ZLN19);
              ZLN19.setBounds(495, 505, 40, ZLN19.getPreferredSize().height);

              //---- ZDE19 ----
              ZDE19.setText("@ZDE19@");
              ZDE19.setName("ZDE19");
              panel2.add(ZDE19);
              ZDE19.setBounds(540, 505, 28, ZDE19.getPreferredSize().height);

              //---- ZON20 ----
              ZON20.setText("@ZON20@");
              ZON20.setName("ZON20");
              panel2.add(ZON20);
              ZON20.setBounds(10, 530, 114, ZON20.getPreferredSize().height);

              //---- ZLI20 ----
              ZLI20.setText("@ZLI20@");
              ZLI20.setName("ZLI20");
              panel2.add(ZLI20);
              ZLI20.setBounds(130, 530, 334, ZLI20.getPreferredSize().height);

              //---- ZTY20 ----
              ZTY20.setText("@ZTY20@");
              ZTY20.setName("ZTY20");
              panel2.add(ZTY20);
              ZTY20.setBounds(470, 530, 20, ZTY20.getPreferredSize().height);

              //---- ZLN20 ----
              ZLN20.setText("@ZLN20@");
              ZLN20.setName("ZLN20");
              panel2.add(ZLN20);
              ZLN20.setBounds(495, 530, 40, ZLN20.getPreferredSize().height);

              //---- ZDE20 ----
              ZDE20.setText("@ZDE20@");
              ZDE20.setName("ZDE20");
              panel2.add(ZDE20);
              ZDE20.setBounds(540, 530, 28, ZDE20.getPreferredSize().height);

              //---- label2 ----
              label2.setText("Code");
              label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
              label2.setName("label2");
              panel2.add(label2);
              label2.setBounds(10, 30, 50, 25);

              //---- label3 ----
              label3.setText("Description");
              label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
              label3.setName("label3");
              panel2.add(label3);
              label3.setBounds(130, 30, 160, 25);

              //---- label4 ----
              label4.setText("Type");
              label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
              label4.setName("label4");
              panel2.add(label4);
              label4.setBounds(465, 30, 50, 25);

              //---- label5 ----
              label5.setText("Lng");
              label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
              label5.setHorizontalAlignment(SwingConstants.CENTER);
              label5.setName("label5");
              panel2.add(label5);
              label5.setBounds(495, 30, 35, 25);

              //---- label6 ----
              label6.setText("Dec");
              label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
              label6.setName("label6");
              panel2.add(label6);
              label6.setBounds(540, 30, 35, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(10, 5, 580, 565);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder(new LineBorder(new Color(223, 176, 42), 3), "Zones du fichier d'import", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION,
                new Font("sansserif", Font.BOLD, 14), new Color(223, 145, 0)));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- N101 ----
              N101.setName("N101");
              panel3.add(N101);
              N101.setBounds(15, 55, 34, N101.getPreferredSize().height);

              //---- D101 ----
              D101.setName("D101");
              panel3.add(D101);
              D101.setBounds(55, 55, 34, D101.getPreferredSize().height);

              //---- F101 ----
              F101.setName("F101");
              panel3.add(F101);
              F101.setBounds(90, 55, 34, F101.getPreferredSize().height);

              //---- N201 ----
              N201.setName("N201");
              panel3.add(N201);
              N201.setBounds(165, 55, 34, N201.getPreferredSize().height);

              //---- D201 ----
              D201.setName("D201");
              panel3.add(D201);
              D201.setBounds(205, 55, 34, D201.getPreferredSize().height);

              //---- F201 ----
              F201.setName("F201");
              panel3.add(F201);
              F201.setBounds(240, 55, 34, F201.getPreferredSize().height);

              //---- OP01 ----
              OP01.setName("OP01");
              OP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP01);
              OP01.setBounds(new Rectangle(new Point(135, 60), OP01.getPreferredSize()));

              //---- label7 ----
              label7.setText("Colonne");
              label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
              label7.setName("label7");
              panel3.add(label7);
              label7.setBounds(7, 30, 50, 25);

              //---- label8 ----
              label8.setText("Colonne");
              label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
              label8.setName("label8");
              panel3.add(label8);
              label8.setBounds(157, 30, 50, 25);

              //---- label9 ----
              label9.setText("de");
              label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
              label9.setHorizontalAlignment(SwingConstants.CENTER);
              label9.setName("label9");
              panel3.add(label9);
              label9.setBounds(55, 30, 34, 25);

              //---- label10 ----
              label10.setText("\u00e0");
              label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
              label10.setHorizontalAlignment(SwingConstants.CENTER);
              label10.setName("label10");
              panel3.add(label10);
              label10.setBounds(90, 30, 34, 25);

              //---- label11 ----
              label11.setText("de");
              label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
              label11.setHorizontalAlignment(SwingConstants.CENTER);
              label11.setName("label11");
              panel3.add(label11);
              label11.setBounds(205, 30, 34, 25);

              //---- label12 ----
              label12.setText("\u00e0");
              label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
              label12.setHorizontalAlignment(SwingConstants.CENTER);
              label12.setName("label12");
              panel3.add(label12);
              label12.setBounds(240, 30, 34, 25);

              //---- label13 ----
              label13.setText("+");
              label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
              label13.setHorizontalAlignment(SwingConstants.CENTER);
              label13.setName("label13");
              panel3.add(label13);
              label13.setBounds(130, 30, 25, 25);

              //---- N102 ----
              N102.setName("N102");
              panel3.add(N102);
              N102.setBounds(15, 80, 34, N102.getPreferredSize().height);

              //---- D102 ----
              D102.setName("D102");
              panel3.add(D102);
              D102.setBounds(55, 80, 34, D102.getPreferredSize().height);

              //---- F102 ----
              F102.setName("F102");
              panel3.add(F102);
              F102.setBounds(90, 80, 34, F102.getPreferredSize().height);

              //---- N202 ----
              N202.setName("N202");
              panel3.add(N202);
              N202.setBounds(165, 80, 34, N202.getPreferredSize().height);

              //---- D202 ----
              D202.setName("D202");
              panel3.add(D202);
              D202.setBounds(205, 80, 34, D202.getPreferredSize().height);

              //---- F202 ----
              F202.setName("F202");
              panel3.add(F202);
              F202.setBounds(240, 80, 34, F202.getPreferredSize().height);

              //---- OP02 ----
              OP02.setName("OP02");
              OP02.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP02);
              OP02.setBounds(new Rectangle(new Point(135, 85), OP02.getPreferredSize()));

              //---- N103 ----
              N103.setName("N103");
              panel3.add(N103);
              N103.setBounds(15, 105, 34, N103.getPreferredSize().height);

              //---- D103 ----
              D103.setName("D103");
              panel3.add(D103);
              D103.setBounds(55, 105, 34, D103.getPreferredSize().height);

              //---- F103 ----
              F103.setName("F103");
              panel3.add(F103);
              F103.setBounds(90, 105, 34, F103.getPreferredSize().height);

              //---- N203 ----
              N203.setName("N203");
              panel3.add(N203);
              N203.setBounds(165, 105, 34, N203.getPreferredSize().height);

              //---- D203 ----
              D203.setName("D203");
              panel3.add(D203);
              D203.setBounds(205, 105, 34, D203.getPreferredSize().height);

              //---- F203 ----
              F203.setName("F203");
              panel3.add(F203);
              F203.setBounds(240, 105, 34, F203.getPreferredSize().height);

              //---- OP03 ----
              OP03.setName("OP03");
              OP03.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP03);
              OP03.setBounds(new Rectangle(new Point(135, 110), OP03.getPreferredSize()));

              //---- N104 ----
              N104.setName("N104");
              panel3.add(N104);
              N104.setBounds(15, 130, 34, N104.getPreferredSize().height);

              //---- D104 ----
              D104.setName("D104");
              panel3.add(D104);
              D104.setBounds(55, 130, 34, D104.getPreferredSize().height);

              //---- F104 ----
              F104.setName("F104");
              panel3.add(F104);
              F104.setBounds(90, 130, 34, F104.getPreferredSize().height);

              //---- N204 ----
              N204.setName("N204");
              panel3.add(N204);
              N204.setBounds(165, 130, 34, N204.getPreferredSize().height);

              //---- D204 ----
              D204.setName("D204");
              panel3.add(D204);
              D204.setBounds(205, 130, 34, D204.getPreferredSize().height);

              //---- F204 ----
              F204.setName("F204");
              panel3.add(F204);
              F204.setBounds(240, 130, 34, F204.getPreferredSize().height);

              //---- OP04 ----
              OP04.setName("OP04");
              OP04.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP04);
              OP04.setBounds(new Rectangle(new Point(135, 135), OP04.getPreferredSize()));

              //---- N105 ----
              N105.setName("N105");
              panel3.add(N105);
              N105.setBounds(15, 155, 34, N105.getPreferredSize().height);

              //---- D105 ----
              D105.setName("D105");
              panel3.add(D105);
              D105.setBounds(55, 155, 34, D105.getPreferredSize().height);

              //---- F105 ----
              F105.setName("F105");
              panel3.add(F105);
              F105.setBounds(90, 155, 34, F105.getPreferredSize().height);

              //---- N205 ----
              N205.setName("N205");
              panel3.add(N205);
              N205.setBounds(165, 155, 34, N205.getPreferredSize().height);

              //---- D205 ----
              D205.setName("D205");
              panel3.add(D205);
              D205.setBounds(205, 155, 34, D205.getPreferredSize().height);

              //---- F205 ----
              F205.setName("F205");
              panel3.add(F205);
              F205.setBounds(240, 155, 34, F205.getPreferredSize().height);

              //---- OP05 ----
              OP05.setName("OP05");
              OP05.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP05);
              OP05.setBounds(new Rectangle(new Point(135, 160), OP05.getPreferredSize()));

              //---- N106 ----
              N106.setName("N106");
              panel3.add(N106);
              N106.setBounds(15, 180, 34, N106.getPreferredSize().height);

              //---- D106 ----
              D106.setName("D106");
              panel3.add(D106);
              D106.setBounds(55, 180, 34, D106.getPreferredSize().height);

              //---- F106 ----
              F106.setName("F106");
              panel3.add(F106);
              F106.setBounds(90, 180, 34, F106.getPreferredSize().height);

              //---- N206 ----
              N206.setName("N206");
              panel3.add(N206);
              N206.setBounds(165, 180, 34, N206.getPreferredSize().height);

              //---- D206 ----
              D206.setName("D206");
              panel3.add(D206);
              D206.setBounds(205, 180, 34, D206.getPreferredSize().height);

              //---- F206 ----
              F206.setName("F206");
              panel3.add(F206);
              F206.setBounds(240, 180, 34, F206.getPreferredSize().height);

              //---- OP06 ----
              OP06.setName("OP06");
              OP06.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP06);
              OP06.setBounds(new Rectangle(new Point(135, 185), OP06.getPreferredSize()));

              //---- N107 ----
              N107.setName("N107");
              panel3.add(N107);
              N107.setBounds(15, 205, 34, N107.getPreferredSize().height);

              //---- D107 ----
              D107.setName("D107");
              panel3.add(D107);
              D107.setBounds(55, 205, 34, D107.getPreferredSize().height);

              //---- F107 ----
              F107.setName("F107");
              panel3.add(F107);
              F107.setBounds(90, 205, 34, F107.getPreferredSize().height);

              //---- N207 ----
              N207.setName("N207");
              panel3.add(N207);
              N207.setBounds(165, 205, 34, N207.getPreferredSize().height);

              //---- D207 ----
              D207.setName("D207");
              panel3.add(D207);
              D207.setBounds(205, 205, 34, D207.getPreferredSize().height);

              //---- F207 ----
              F207.setName("F207");
              panel3.add(F207);
              F207.setBounds(240, 205, 34, F207.getPreferredSize().height);

              //---- OP07 ----
              OP07.setName("OP07");
              OP07.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP07);
              OP07.setBounds(new Rectangle(new Point(135, 210), OP07.getPreferredSize()));

              //---- N108 ----
              N108.setName("N108");
              panel3.add(N108);
              N108.setBounds(15, 230, 34, N108.getPreferredSize().height);

              //---- D108 ----
              D108.setName("D108");
              panel3.add(D108);
              D108.setBounds(55, 230, 34, D108.getPreferredSize().height);

              //---- F108 ----
              F108.setName("F108");
              panel3.add(F108);
              F108.setBounds(90, 230, 34, F108.getPreferredSize().height);

              //---- N208 ----
              N208.setName("N208");
              panel3.add(N208);
              N208.setBounds(165, 230, 34, N208.getPreferredSize().height);

              //---- D208 ----
              D208.setName("D208");
              panel3.add(D208);
              D208.setBounds(205, 230, 34, D208.getPreferredSize().height);

              //---- F208 ----
              F208.setName("F208");
              panel3.add(F208);
              F208.setBounds(240, 230, 34, F208.getPreferredSize().height);

              //---- OP08 ----
              OP08.setName("OP08");
              OP08.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP08);
              OP08.setBounds(new Rectangle(new Point(135, 235), OP08.getPreferredSize()));

              //---- N109 ----
              N109.setName("N109");
              panel3.add(N109);
              N109.setBounds(15, 255, 34, N109.getPreferredSize().height);

              //---- D109 ----
              D109.setName("D109");
              panel3.add(D109);
              D109.setBounds(55, 255, 34, D109.getPreferredSize().height);

              //---- F109 ----
              F109.setName("F109");
              panel3.add(F109);
              F109.setBounds(90, 255, 34, F109.getPreferredSize().height);

              //---- N209 ----
              N209.setName("N209");
              panel3.add(N209);
              N209.setBounds(165, 255, 34, N209.getPreferredSize().height);

              //---- D209 ----
              D209.setName("D209");
              panel3.add(D209);
              D209.setBounds(205, 255, 34, D209.getPreferredSize().height);

              //---- F209 ----
              F209.setName("F209");
              panel3.add(F209);
              F209.setBounds(240, 255, 34, F209.getPreferredSize().height);

              //---- OP09 ----
              OP09.setName("OP09");
              OP09.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP09);
              OP09.setBounds(new Rectangle(new Point(135, 260), OP09.getPreferredSize()));

              //---- N110 ----
              N110.setName("N110");
              panel3.add(N110);
              N110.setBounds(15, 280, 34, N110.getPreferredSize().height);

              //---- D110 ----
              D110.setName("D110");
              panel3.add(D110);
              D110.setBounds(55, 280, 34, D110.getPreferredSize().height);

              //---- F110 ----
              F110.setName("F110");
              panel3.add(F110);
              F110.setBounds(90, 280, 34, F110.getPreferredSize().height);

              //---- N210 ----
              N210.setName("N210");
              panel3.add(N210);
              N210.setBounds(165, 280, 34, N210.getPreferredSize().height);

              //---- D210 ----
              D210.setName("D210");
              panel3.add(D210);
              D210.setBounds(205, 280, 34, D210.getPreferredSize().height);

              //---- F210 ----
              F210.setName("F210");
              panel3.add(F210);
              F210.setBounds(240, 280, 34, F210.getPreferredSize().height);

              //---- OP10 ----
              OP10.setName("OP10");
              OP10.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP10);
              OP10.setBounds(new Rectangle(new Point(135, 285), OP10.getPreferredSize()));

              //---- N111 ----
              N111.setName("N111");
              panel3.add(N111);
              N111.setBounds(15, 305, 34, N111.getPreferredSize().height);

              //---- D111 ----
              D111.setName("D111");
              panel3.add(D111);
              D111.setBounds(55, 305, 34, D111.getPreferredSize().height);

              //---- F111 ----
              F111.setName("F111");
              panel3.add(F111);
              F111.setBounds(90, 305, 34, F111.getPreferredSize().height);

              //---- N211 ----
              N211.setName("N211");
              panel3.add(N211);
              N211.setBounds(165, 305, 34, N211.getPreferredSize().height);

              //---- D211 ----
              D211.setName("D211");
              panel3.add(D211);
              D211.setBounds(205, 305, 34, D211.getPreferredSize().height);

              //---- F211 ----
              F211.setName("F211");
              panel3.add(F211);
              F211.setBounds(240, 305, 34, F211.getPreferredSize().height);

              //---- OP11 ----
              OP11.setName("OP11");
              OP11.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP11);
              OP11.setBounds(new Rectangle(new Point(135, 310), OP11.getPreferredSize()));

              //---- N112 ----
              N112.setName("N112");
              panel3.add(N112);
              N112.setBounds(15, 330, 34, N112.getPreferredSize().height);

              //---- D112 ----
              D112.setName("D112");
              panel3.add(D112);
              D112.setBounds(55, 330, 34, D112.getPreferredSize().height);

              //---- F112 ----
              F112.setName("F112");
              panel3.add(F112);
              F112.setBounds(90, 330, 34, F112.getPreferredSize().height);

              //---- N212 ----
              N212.setName("N212");
              panel3.add(N212);
              N212.setBounds(165, 330, 34, N212.getPreferredSize().height);

              //---- D212 ----
              D212.setName("D212");
              panel3.add(D212);
              D212.setBounds(205, 330, 34, D212.getPreferredSize().height);

              //---- F212 ----
              F212.setName("F212");
              panel3.add(F212);
              F212.setBounds(240, 330, 34, F212.getPreferredSize().height);

              //---- OP12 ----
              OP12.setName("OP12");
              OP12.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP12);
              OP12.setBounds(new Rectangle(new Point(135, 335), OP12.getPreferredSize()));

              //---- N113 ----
              N113.setName("N113");
              panel3.add(N113);
              N113.setBounds(15, 355, 34, N113.getPreferredSize().height);

              //---- D113 ----
              D113.setName("D113");
              panel3.add(D113);
              D113.setBounds(55, 355, 34, D113.getPreferredSize().height);

              //---- F113 ----
              F113.setName("F113");
              panel3.add(F113);
              F113.setBounds(90, 355, 34, F113.getPreferredSize().height);

              //---- N213 ----
              N213.setName("N213");
              panel3.add(N213);
              N213.setBounds(165, 355, 34, N213.getPreferredSize().height);

              //---- D213 ----
              D213.setName("D213");
              panel3.add(D213);
              D213.setBounds(205, 355, 34, D213.getPreferredSize().height);

              //---- F213 ----
              F213.setName("F213");
              panel3.add(F213);
              F213.setBounds(240, 355, 34, F213.getPreferredSize().height);

              //---- OP13 ----
              OP13.setName("OP13");
              OP13.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP13);
              OP13.setBounds(new Rectangle(new Point(135, 360), OP13.getPreferredSize()));

              //---- N114 ----
              N114.setName("N114");
              panel3.add(N114);
              N114.setBounds(15, 380, 34, N114.getPreferredSize().height);

              //---- D114 ----
              D114.setName("D114");
              panel3.add(D114);
              D114.setBounds(55, 380, 34, D114.getPreferredSize().height);

              //---- F114 ----
              F114.setName("F114");
              panel3.add(F114);
              F114.setBounds(90, 380, 34, F114.getPreferredSize().height);

              //---- N214 ----
              N214.setName("N214");
              panel3.add(N214);
              N214.setBounds(165, 380, 34, N214.getPreferredSize().height);

              //---- D214 ----
              D214.setName("D214");
              panel3.add(D214);
              D214.setBounds(205, 380, 34, D214.getPreferredSize().height);

              //---- F214 ----
              F214.setName("F214");
              panel3.add(F214);
              F214.setBounds(240, 380, 34, F214.getPreferredSize().height);

              //---- OP14 ----
              OP14.setName("OP14");
              OP14.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP14);
              OP14.setBounds(new Rectangle(new Point(135, 385), OP14.getPreferredSize()));

              //---- N115 ----
              N115.setName("N115");
              panel3.add(N115);
              N115.setBounds(15, 405, 34, N115.getPreferredSize().height);

              //---- D115 ----
              D115.setName("D115");
              panel3.add(D115);
              D115.setBounds(55, 405, 34, D115.getPreferredSize().height);

              //---- F115 ----
              F115.setName("F115");
              panel3.add(F115);
              F115.setBounds(90, 405, 34, F115.getPreferredSize().height);

              //---- N215 ----
              N215.setName("N215");
              panel3.add(N215);
              N215.setBounds(165, 405, 34, N215.getPreferredSize().height);

              //---- D215 ----
              D215.setName("D215");
              panel3.add(D215);
              D215.setBounds(205, 405, 34, D215.getPreferredSize().height);

              //---- F215 ----
              F215.setName("F215");
              panel3.add(F215);
              F215.setBounds(240, 405, 34, F215.getPreferredSize().height);

              //---- OP15 ----
              OP15.setName("OP15");
              OP15.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP15);
              OP15.setBounds(new Rectangle(new Point(135, 410), OP15.getPreferredSize()));

              //---- N116 ----
              N116.setName("N116");
              panel3.add(N116);
              N116.setBounds(15, 430, 34, N116.getPreferredSize().height);

              //---- D116 ----
              D116.setName("D116");
              panel3.add(D116);
              D116.setBounds(55, 430, 34, D116.getPreferredSize().height);

              //---- F116 ----
              F116.setName("F116");
              panel3.add(F116);
              F116.setBounds(90, 430, 34, F116.getPreferredSize().height);

              //---- N216 ----
              N216.setName("N216");
              panel3.add(N216);
              N216.setBounds(165, 430, 34, N216.getPreferredSize().height);

              //---- D216 ----
              D216.setName("D216");
              panel3.add(D216);
              D216.setBounds(205, 430, 34, D216.getPreferredSize().height);

              //---- F216 ----
              F216.setName("F216");
              panel3.add(F216);
              F216.setBounds(240, 430, 34, F216.getPreferredSize().height);

              //---- OP16 ----
              OP16.setName("OP16");
              OP16.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP16);
              OP16.setBounds(new Rectangle(new Point(135, 435), OP16.getPreferredSize()));

              //---- N117 ----
              N117.setName("N117");
              panel3.add(N117);
              N117.setBounds(15, 455, 34, N117.getPreferredSize().height);

              //---- D117 ----
              D117.setName("D117");
              panel3.add(D117);
              D117.setBounds(55, 455, 34, D117.getPreferredSize().height);

              //---- F117 ----
              F117.setName("F117");
              panel3.add(F117);
              F117.setBounds(90, 455, 34, F117.getPreferredSize().height);

              //---- N217 ----
              N217.setName("N217");
              panel3.add(N217);
              N217.setBounds(165, 455, 34, N217.getPreferredSize().height);

              //---- D217 ----
              D217.setName("D217");
              panel3.add(D217);
              D217.setBounds(205, 455, 34, D217.getPreferredSize().height);

              //---- F217 ----
              F217.setName("F217");
              panel3.add(F217);
              F217.setBounds(240, 455, 34, F217.getPreferredSize().height);

              //---- OP17 ----
              OP17.setName("OP17");
              OP17.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP17);
              OP17.setBounds(new Rectangle(new Point(135, 460), OP17.getPreferredSize()));

              //---- N118 ----
              N118.setName("N118");
              panel3.add(N118);
              N118.setBounds(15, 480, 34, N118.getPreferredSize().height);

              //---- D118 ----
              D118.setName("D118");
              panel3.add(D118);
              D118.setBounds(55, 480, 34, D118.getPreferredSize().height);

              //---- F118 ----
              F118.setName("F118");
              panel3.add(F118);
              F118.setBounds(90, 480, 34, F118.getPreferredSize().height);

              //---- N218 ----
              N218.setName("N218");
              panel3.add(N218);
              N218.setBounds(165, 480, 34, N218.getPreferredSize().height);

              //---- D218 ----
              D218.setName("D218");
              panel3.add(D218);
              D218.setBounds(205, 480, 34, D218.getPreferredSize().height);

              //---- F218 ----
              F218.setName("F218");
              panel3.add(F218);
              F218.setBounds(240, 480, 34, F218.getPreferredSize().height);

              //---- OP18 ----
              OP18.setName("OP18");
              OP18.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP18);
              OP18.setBounds(new Rectangle(new Point(135, 485), OP18.getPreferredSize()));

              //---- N119 ----
              N119.setName("N119");
              panel3.add(N119);
              N119.setBounds(15, 505, 34, N119.getPreferredSize().height);

              //---- D119 ----
              D119.setName("D119");
              panel3.add(D119);
              D119.setBounds(55, 505, 34, D119.getPreferredSize().height);

              //---- F119 ----
              F119.setName("F119");
              panel3.add(F119);
              F119.setBounds(90, 505, 34, F119.getPreferredSize().height);

              //---- N219 ----
              N219.setName("N219");
              panel3.add(N219);
              N219.setBounds(165, 505, 34, N219.getPreferredSize().height);

              //---- D219 ----
              D219.setName("D219");
              panel3.add(D219);
              D219.setBounds(205, 505, 34, D219.getPreferredSize().height);

              //---- F219 ----
              F219.setName("F219");
              panel3.add(F219);
              F219.setBounds(240, 505, 34, F219.getPreferredSize().height);

              //---- OP19 ----
              OP19.setName("OP19");
              OP19.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP19);
              OP19.setBounds(new Rectangle(new Point(135, 510), OP19.getPreferredSize()));

              //---- N120 ----
              N120.setName("N120");
              panel3.add(N120);
              N120.setBounds(15, 530, 34, N120.getPreferredSize().height);

              //---- D120 ----
              D120.setName("D120");
              panel3.add(D120);
              D120.setBounds(55, 530, 34, D120.getPreferredSize().height);

              //---- F120 ----
              F120.setName("F120");
              panel3.add(F120);
              F120.setBounds(90, 530, 34, F120.getPreferredSize().height);

              //---- N220 ----
              N220.setName("N220");
              panel3.add(N220);
              N220.setBounds(165, 530, 34, N220.getPreferredSize().height);

              //---- D220 ----
              D220.setName("D220");
              panel3.add(D220);
              D220.setBounds(205, 530, 34, D220.getPreferredSize().height);

              //---- F220 ----
              F220.setName("F220");
              panel3.add(F220);
              F220.setBounds(240, 530, 34, F220.getPreferredSize().height);

              //---- OP20 ----
              OP20.setName("OP20");
              OP20.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  OP01MouseClicked(e);
                }
              });
              panel3.add(OP20);
              OP20.setBounds(new Rectangle(new Point(135, 535), OP20.getPreferredSize()));

              //---- button1 ----
              button1.setText("                                                                                                                                                                                                                                                                                       ");
              button1.setToolTipText("@ERR01@");
              button1.setPreferredSize(new Dimension(28, 28));
              button1.setName("button1");
              panel3.add(button1);
              button1.setBounds(280, 55, 30, 28);

              //---- button2 ----
              button2.setText("                                                                                                                                                                                                                                                                                        ");
              button2.setToolTipText("@ERR02@");
              button2.setPreferredSize(new Dimension(28, 28));
              button2.setName("button2");
              panel3.add(button2);
              button2.setBounds(280, 80, 30, button2.getPreferredSize().height);

              //---- button3 ----
              button3.setText("                                                                                                                                                                                                                                                                                       ");
              button3.setToolTipText("@ERR03@");
              button3.setPreferredSize(new Dimension(28, 28));
              button3.setName("button3");
              panel3.add(button3);
              button3.setBounds(280, 105, 30, button3.getPreferredSize().height);

              //---- button4 ----
              button4.setText("                                                                                                                                                                                                                                                                                    ");
              button4.setToolTipText("@ERR04@");
              button4.setPreferredSize(new Dimension(28, 28));
              button4.setName("button4");
              panel3.add(button4);
              button4.setBounds(280, 130, 30, button4.getPreferredSize().height);

              //---- button5 ----
              button5.setText("                                                                                                                            ");
              button5.setToolTipText("@ERR05@");
              button5.setPreferredSize(new Dimension(28, 28));
              button5.setName("button5");
              panel3.add(button5);
              button5.setBounds(280, 155, 30, button5.getPreferredSize().height);

              //---- button6 ----
              button6.setText("                                                                                                                                                                                                                                                                                       ");
              button6.setToolTipText("@ERR06@");
              button6.setPreferredSize(new Dimension(28, 28));
              button6.setName("button6");
              panel3.add(button6);
              button6.setBounds(280, 180, 30, button6.getPreferredSize().height);

              //---- button7 ----
              button7.setText("                                                                                                                                                                                                                                                                                        ");
              button7.setToolTipText("@ERR07@");
              button7.setPreferredSize(new Dimension(28, 28));
              button7.setName("button7");
              panel3.add(button7);
              button7.setBounds(280, 205, 30, button7.getPreferredSize().height);

              //---- button8 ----
              button8.setText("                                                                                                                                                                                                                                                                                       ");
              button8.setToolTipText("@ERR08@");
              button8.setPreferredSize(new Dimension(28, 28));
              button8.setName("button8");
              panel3.add(button8);
              button8.setBounds(280, 230, 30, button8.getPreferredSize().height);

              //---- button9 ----
              button9.setText("                                                                                                                                                                                                                                                                                    ");
              button9.setToolTipText("@ERR09@");
              button9.setPreferredSize(new Dimension(28, 28));
              button9.setName("button9");
              panel3.add(button9);
              button9.setBounds(280, 255, 30, button9.getPreferredSize().height);

              //---- button10 ----
              button10.setText("                                                                                                                            ");
              button10.setToolTipText("@ERR10@");
              button10.setPreferredSize(new Dimension(28, 28));
              button10.setName("button10");
              panel3.add(button10);
              button10.setBounds(280, 280, 30, button10.getPreferredSize().height);

              //---- button11 ----
              button11.setText("                                                                                                                            ");
              button11.setToolTipText("@ERR11@");
              button11.setPreferredSize(new Dimension(28, 28));
              button11.setName("button11");
              panel3.add(button11);
              button11.setBounds(280, 305, 30, button11.getPreferredSize().height);

              //---- button12 ----
              button12.setText("                                                                                                                            ");
              button12.setToolTipText("@ERR12@");
              button12.setPreferredSize(new Dimension(28, 28));
              button12.setName("button12");
              panel3.add(button12);
              button12.setBounds(280, 330, 30, button12.getPreferredSize().height);

              //---- button13 ----
              button13.setText("                                                                                                                            ");
              button13.setToolTipText("@ERR13@");
              button13.setPreferredSize(new Dimension(28, 28));
              button13.setName("button13");
              panel3.add(button13);
              button13.setBounds(280, 355, 30, button13.getPreferredSize().height);

              //---- button14 ----
              button14.setText("                                                                                                                            ");
              button14.setToolTipText("@ERR14@");
              button14.setPreferredSize(new Dimension(28, 28));
              button14.setName("button14");
              panel3.add(button14);
              button14.setBounds(280, 380, 30, button14.getPreferredSize().height);

              //---- button15 ----
              button15.setText("                                                                                                                            ");
              button15.setToolTipText("@ERR15@");
              button15.setPreferredSize(new Dimension(28, 28));
              button15.setName("button15");
              panel3.add(button15);
              button15.setBounds(280, 405, 30, button15.getPreferredSize().height);

              //---- button16 ----
              button16.setText("                                                                                                                            ");
              button16.setToolTipText("@ERR16@");
              button16.setPreferredSize(new Dimension(28, 28));
              button16.setName("button16");
              panel3.add(button16);
              button16.setBounds(280, 430, 30, button16.getPreferredSize().height);

              //---- button17 ----
              button17.setText("                                                                                                                            ");
              button17.setToolTipText("@ERR17@");
              button17.setPreferredSize(new Dimension(28, 28));
              button17.setName("button17");
              panel3.add(button17);
              button17.setBounds(280, 455, 30, button17.getPreferredSize().height);

              //---- button18 ----
              button18.setText("                                                                                                                            ");
              button18.setToolTipText("@ERR18@");
              button18.setPreferredSize(new Dimension(28, 28));
              button18.setName("button18");
              panel3.add(button18);
              button18.setBounds(280, 480, 30, button18.getPreferredSize().height);

              //---- button19 ----
              button19.setText("                                                                                                                            ");
              button19.setToolTipText("@ERR19@");
              button19.setPreferredSize(new Dimension(28, 28));
              button19.setName("button19");
              panel3.add(button19);
              button19.setBounds(280, 505, 30, button19.getPreferredSize().height);

              //---- button20 ----
              button20.setText("                                                                                                                            ");
              button20.setToolTipText("@ERR20@");
              button20.setPreferredSize(new Dimension(28, 28));
              button20.setName("button20");
              panel3.add(button20);
              button20.setBounds(280, 530, 30, button20.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel3);
            panel3.setBounds(600, 5, 320, 565);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(925, 60, 25, 233);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(925, 330, 25, 233);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 954, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 577, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(8, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_66;
  private XRiTextField FRETB;
  private JLabel OBJ_68;
  private RiZoneSortie FRCOL;
  private RiZoneSortie FRFRS;
  private RiZoneSortie FRNOM;
  private JLabel label1;
  private RiZoneSortie CTNUM;
  private RiZoneSortie CTLIB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JPanel panel2;
  private RiZoneSortie ZON01;
  private RiZoneSortie ZLI01;
  private RiZoneSortie ZTY01;
  private RiZoneSortie ZLN01;
  private RiZoneSortie ZDE01;
  private RiZoneSortie ZON02;
  private RiZoneSortie ZLI02;
  private RiZoneSortie ZTY2;
  private RiZoneSortie ZLN2;
  private RiZoneSortie ZDE2;
  private RiZoneSortie ZON03;
  private RiZoneSortie ZLI03;
  private RiZoneSortie ZTY3;
  private RiZoneSortie ZLN3;
  private RiZoneSortie ZDE3;
  private RiZoneSortie ZON04;
  private RiZoneSortie ZLI04;
  private RiZoneSortie ZTY4;
  private RiZoneSortie ZLN4;
  private RiZoneSortie ZDE4;
  private RiZoneSortie ZON05;
  private RiZoneSortie ZLI05;
  private RiZoneSortie ZTY5;
  private RiZoneSortie ZLN5;
  private RiZoneSortie ZDE5;
  private RiZoneSortie ZON06;
  private RiZoneSortie ZLI06;
  private RiZoneSortie ZTY6;
  private RiZoneSortie ZLN6;
  private RiZoneSortie ZDE6;
  private RiZoneSortie ZON07;
  private RiZoneSortie ZLI07;
  private RiZoneSortie ZTY7;
  private RiZoneSortie ZLN7;
  private RiZoneSortie ZDE7;
  private RiZoneSortie ZON08;
  private RiZoneSortie ZLI08;
  private RiZoneSortie ZTY8;
  private RiZoneSortie ZLN8;
  private RiZoneSortie ZDE8;
  private RiZoneSortie ZON09;
  private RiZoneSortie ZLI09;
  private RiZoneSortie ZTY9;
  private RiZoneSortie ZLN9;
  private RiZoneSortie ZDE9;
  private RiZoneSortie ZON10;
  private RiZoneSortie ZLI10;
  private RiZoneSortie ZTY10;
  private RiZoneSortie ZLN10;
  private RiZoneSortie ZDE10;
  private RiZoneSortie ZON11;
  private RiZoneSortie ZLI11;
  private RiZoneSortie ZTY11;
  private RiZoneSortie ZLN11;
  private RiZoneSortie ZDE11;
  private RiZoneSortie ZON12;
  private RiZoneSortie ZLI12;
  private RiZoneSortie ZTY12;
  private RiZoneSortie ZLN12;
  private RiZoneSortie ZDE12;
  private RiZoneSortie ZON13;
  private RiZoneSortie ZLI13;
  private RiZoneSortie ZTY13;
  private RiZoneSortie ZLN13;
  private RiZoneSortie ZDE13;
  private RiZoneSortie ZON14;
  private RiZoneSortie ZLI14;
  private RiZoneSortie ZTY14;
  private RiZoneSortie ZLN14;
  private RiZoneSortie ZDE14;
  private RiZoneSortie ZON15;
  private RiZoneSortie ZLI15;
  private RiZoneSortie ZTY15;
  private RiZoneSortie ZLN15;
  private RiZoneSortie ZDE15;
  private RiZoneSortie ZON16;
  private RiZoneSortie ZLI16;
  private RiZoneSortie ZTY16;
  private RiZoneSortie ZLN16;
  private RiZoneSortie ZDE16;
  private RiZoneSortie ZON17;
  private RiZoneSortie ZLI17;
  private RiZoneSortie ZTY17;
  private RiZoneSortie ZLN17;
  private RiZoneSortie ZDE17;
  private RiZoneSortie ZON18;
  private RiZoneSortie ZLI18;
  private RiZoneSortie ZTY18;
  private RiZoneSortie ZLN18;
  private RiZoneSortie ZDE18;
  private RiZoneSortie ZON19;
  private RiZoneSortie ZLI19;
  private RiZoneSortie ZTY19;
  private RiZoneSortie ZLN19;
  private RiZoneSortie ZDE19;
  private RiZoneSortie ZON20;
  private RiZoneSortie ZLI20;
  private RiZoneSortie ZTY20;
  private RiZoneSortie ZLN20;
  private RiZoneSortie ZDE20;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JPanel panel3;
  private XRiTextField N101;
  private XRiTextField D101;
  private XRiTextField F101;
  private XRiTextField N201;
  private XRiTextField D201;
  private XRiTextField F201;
  private XRiCheckBox OP01;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private XRiTextField N102;
  private XRiTextField D102;
  private XRiTextField F102;
  private XRiTextField N202;
  private XRiTextField D202;
  private XRiTextField F202;
  private XRiCheckBox OP02;
  private XRiTextField N103;
  private XRiTextField D103;
  private XRiTextField F103;
  private XRiTextField N203;
  private XRiTextField D203;
  private XRiTextField F203;
  private XRiCheckBox OP03;
  private XRiTextField N104;
  private XRiTextField D104;
  private XRiTextField F104;
  private XRiTextField N204;
  private XRiTextField D204;
  private XRiTextField F204;
  private XRiCheckBox OP04;
  private XRiTextField N105;
  private XRiTextField D105;
  private XRiTextField F105;
  private XRiTextField N205;
  private XRiTextField D205;
  private XRiTextField F205;
  private XRiCheckBox OP05;
  private XRiTextField N106;
  private XRiTextField D106;
  private XRiTextField F106;
  private XRiTextField N206;
  private XRiTextField D206;
  private XRiTextField F206;
  private XRiCheckBox OP06;
  private XRiTextField N107;
  private XRiTextField D107;
  private XRiTextField F107;
  private XRiTextField N207;
  private XRiTextField D207;
  private XRiTextField F207;
  private XRiCheckBox OP07;
  private XRiTextField N108;
  private XRiTextField D108;
  private XRiTextField F108;
  private XRiTextField N208;
  private XRiTextField D208;
  private XRiTextField F208;
  private XRiCheckBox OP08;
  private XRiTextField N109;
  private XRiTextField D109;
  private XRiTextField F109;
  private XRiTextField N209;
  private XRiTextField D209;
  private XRiTextField F209;
  private XRiCheckBox OP09;
  private XRiTextField N110;
  private XRiTextField D110;
  private XRiTextField F110;
  private XRiTextField N210;
  private XRiTextField D210;
  private XRiTextField F210;
  private XRiCheckBox OP10;
  private XRiTextField N111;
  private XRiTextField D111;
  private XRiTextField F111;
  private XRiTextField N211;
  private XRiTextField D211;
  private XRiTextField F211;
  private XRiCheckBox OP11;
  private XRiTextField N112;
  private XRiTextField D112;
  private XRiTextField F112;
  private XRiTextField N212;
  private XRiTextField D212;
  private XRiTextField F212;
  private XRiCheckBox OP12;
  private XRiTextField N113;
  private XRiTextField D113;
  private XRiTextField F113;
  private XRiTextField N213;
  private XRiTextField D213;
  private XRiTextField F213;
  private XRiCheckBox OP13;
  private XRiTextField N114;
  private XRiTextField D114;
  private XRiTextField F114;
  private XRiTextField N214;
  private XRiTextField D214;
  private XRiTextField F214;
  private XRiCheckBox OP14;
  private XRiTextField N115;
  private XRiTextField D115;
  private XRiTextField F115;
  private XRiTextField N215;
  private XRiTextField D215;
  private XRiTextField F215;
  private XRiCheckBox OP15;
  private XRiTextField N116;
  private XRiTextField D116;
  private XRiTextField F116;
  private XRiTextField N216;
  private XRiTextField D216;
  private XRiTextField F216;
  private XRiCheckBox OP16;
  private XRiTextField N117;
  private XRiTextField D117;
  private XRiTextField F117;
  private XRiTextField N217;
  private XRiTextField D217;
  private XRiTextField F217;
  private XRiCheckBox OP17;
  private XRiTextField N118;
  private XRiTextField D118;
  private XRiTextField F118;
  private XRiTextField N218;
  private XRiTextField D218;
  private XRiTextField F218;
  private XRiCheckBox OP18;
  private XRiTextField N119;
  private XRiTextField D119;
  private XRiTextField F119;
  private XRiTextField N219;
  private XRiTextField D219;
  private XRiTextField F219;
  private XRiCheckBox OP19;
  private XRiTextField N120;
  private XRiTextField D120;
  private XRiTextField F120;
  private XRiTextField N220;
  private XRiTextField D220;
  private XRiTextField F220;
  private XRiCheckBox OP20;
  private JButton button1;
  private JButton button2;
  private JButton button3;
  private JButton button4;
  private JButton button5;
  private JButton button6;
  private JButton button7;
  private JButton button8;
  private JButton button9;
  private JButton button10;
  private JButton button11;
  private JButton button12;
  private JButton button13;
  private JButton button14;
  private JButton button15;
  private JButton button16;
  private JButton button17;
  private JButton button18;
  private JButton button19;
  private JButton button20;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  // JFormDesigner - End of variables declaration  //GEN-END:variables


}
