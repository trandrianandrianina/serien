
package ri.serien.libecranrpg.vgam.VGAM49FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * @author Stéphane Vénéri
 */
public class VGAM49FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public VGAM49FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // Etablissement client
    IdEtablissement idEtablissementFournisseur = IdEtablissement.getInstance(lexique.HostFieldGetData("FRETB"));
    
    // fournisseur et magasin
    
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(idEtablissementFournisseur);
    snFournisseur.charger(false);
    snFournisseur.setSelectionParChampRPG(lexique, "FRCOL", "FRFRS");
    snFournisseur.setEnabled(false);
    
    // Etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setEnabled(true);
    snEtablissement.setSelectionParChampRPG(lexique, "WETBV");
    
    snClient.setEnabled(false);
    snMagasin.setEnabled(false);
    if (snEtablissement.getSelection() != null) {
      snMagasin.setSession(getSession());
      snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
      snMagasin.charger(false);
      snMagasin.setSelectionParChampRPG(lexique, "WMAGV");
      snMagasin.setEnabled(true);
      
      // Client
      snClient.setSession(getSession());
      snClient.setIdEtablissement(snEtablissement.getIdSelection());
      snClient.charger(true);
      snClient.setSelectionParChampRPG(lexique, "WNUCLP", "WNUCLS");
      snClient.setEnabled(true);
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Lien fournisseur/client"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETBV");
    snClient.renseignerChampRPG(lexique, "WNUCLP", "WNUCLS");
    snMagasin.renseignerChampRPG(lexique, "WMAGV");
  }
  
  protected void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      // Client et magasin
      snClient.setSelection(null);
      snMagasin.setSelection(null);
      snClient.setEnabled(false);
      snMagasin.setEnabled(false);
      
      if (snEtablissement.getSelection() != null) {
        snClient.setSession(getSession());
        snClient.setIdEtablissement(snEtablissement.getIdSelection());
        snClient.charger(true);
        snClient.setEnabled(true);
        
        snMagasin.setSession(getSession());
        snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
        snMagasin.charger(true);
        snMagasin.setEnabled(true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbClient = new SNLabelChamp();
    snClient = new SNClient();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    
    // ======== this ========
    setMinimumSize(new Dimension(800, 260));
    setPreferredSize(new Dimension(800, 260));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ---- lbFournisseur ----
      lbFournisseur.setText("Fournisseur");
      lbFournisseur.setName("lbFournisseur");
      pnlContenu.add(lbFournisseur,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ---- snFournisseur ----
      snFournisseur.setName("snFournisseur");
      pnlContenu.add(snFournisseur,
          new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbEtablissement ----
      lbEtablissement.setText("Etablissement du client");
      lbEtablissement.setMaximumSize(new Dimension(200, 30));
      lbEtablissement.setMinimumSize(new Dimension(200, 30));
      lbEtablissement.setPreferredSize(new Dimension(200, 30));
      lbEtablissement.setName("lbEtablissement");
      pnlContenu.add(lbEtablissement,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ---- snEtablissement ----
      snEtablissement.setName("snEtablissement");
      snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
        @Override
        public void valueChanged(SNComposantEvent e) {
          snEtablissementValueChanged(e);
        }
      });
      pnlContenu.add(snEtablissement,
          new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbClient ----
      lbClient.setText("Client");
      lbClient.setName("lbClient");
      pnlContenu.add(lbClient,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ---- snClient ----
      snClient.setName("snClient");
      pnlContenu.add(snClient,
          new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbMagasin ----
      lbMagasin.setText("Magasin");
      lbMagasin.setName("lbMagasin");
      pnlContenu.add(lbMagasin,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- snMagasin ----
      snMagasin.setName("snMagasin");
      pnlContenu.add(snMagasin,
          new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbClient;
  private SNClient snClient;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
