
package ri.serien.libecranrpg.vgam.VGAM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM14FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM14FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    RB.setValeurs("1", RB_GRP);
    RB_2.setValeurs("2");
    RB_3.setValeurs("3");
    RB_4.setValeurs("4");
    RB_5.setValeurs("5");
    RB_6.setValeurs("6");
    RB_7.setValeurs("7");
    RB_8.setValeurs("8");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Sélection"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    RB_3 = new XRiRadioButton();
    RB_4 = new XRiRadioButton();
    RB_6 = new XRiRadioButton();
    RB_7 = new XRiRadioButton();
    ARG3 = new XRiTextField();
    ARG4 = new XRiTextField();
    ARG1 = new XRiTextField();
    RB_5 = new XRiRadioButton();
    RB_2 = new XRiRadioButton();
    RB = new XRiRadioButton();
    ARG5 = new XRiTextField();
    ARG2 = new XRiTextField();
    ARGABC = new XRiTextField();
    ARG6 = new XRiCalendrier();
    RB_8 = new XRiRadioButton();
    ARG7 = new XRiTextField();
    ARGGP = new XRiTextField();
    OBJ_44 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    RB_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(620, 300));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- RB_3 ----
          RB_3.setText("Recherche alphab\u00e9tique");
          RB_3.setToolTipText("tri\u00e9 par");
          RB_3.setComponentPopupMenu(BTD);
          RB_3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RB_3.setName("RB_3");
          panel2.add(RB_3);
          RB_3.setBounds(25, 35, 215, 20);

          //---- RB_4 ----
          RB_4.setText("Mot de classement 2");
          RB_4.setToolTipText("tri\u00e9 par");
          RB_4.setComponentPopupMenu(BTD);
          RB_4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RB_4.setName("RB_4");
          panel2.add(RB_4);
          RB_4.setBounds(25, 93, 215, 20);

          //---- RB_6 ----
          RB_6.setText("Date de 1\u00e8re rupture");
          RB_6.setComponentPopupMenu(BTD);
          RB_6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RB_6.setName("RB_6");
          panel2.add(RB_6);
          RB_6.setBounds(25, 180, 215, 20);

          //---- RB_7 ----
          RB_7.setText("Code ABC (*=Tous)");
          RB_7.setToolTipText("tri\u00e9 par");
          RB_7.setComponentPopupMenu(BTD);
          RB_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RB_7.setName("RB_7");
          panel2.add(RB_7);
          RB_7.setBounds(25, 209, 215, 20);

          //---- ARG3 ----
          ARG3.setComponentPopupMenu(null);
          ARG3.setName("ARG3");
          panel2.add(ARG3);
          ARG3.setBounds(245, 31, 160, ARG3.getPreferredSize().height);

          //---- ARG4 ----
          ARG4.setComponentPopupMenu(null);
          ARG4.setName("ARG4");
          panel2.add(ARG4);
          ARG4.setBounds(245, 89, 160, ARG4.getPreferredSize().height);

          //---- ARG1 ----
          ARG1.setComponentPopupMenu(null);
          ARG1.setName("ARG1");
          panel2.add(ARG1);
          ARG1.setBounds(245, 118, 160, ARG1.getPreferredSize().height);

          //---- RB_5 ----
          RB_5.setText("Sous-famille");
          RB_5.setToolTipText("tri\u00e9 par");
          RB_5.setComponentPopupMenu(BTD);
          RB_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RB_5.setName("RB_5");
          panel2.add(RB_5);
          RB_5.setBounds(25, 151, 215, 20);

          //---- RB_2 ----
          RB_2.setText("Famille");
          RB_2.setToolTipText("tri\u00e9 par");
          RB_2.setComponentPopupMenu(BTD);
          RB_2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RB_2.setName("RB_2");
          panel2.add(RB_2);
          RB_2.setBounds(25, 64, 215, 20);

          //---- RB ----
          RB.setText("Article");
          RB.setToolTipText("tri\u00e9 par");
          RB.setComponentPopupMenu(BTD);
          RB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RB.setName("RB");
          panel2.add(RB);
          RB.setBounds(25, 122, 215, 20);

          //---- ARG5 ----
          ARG5.setComponentPopupMenu(BTD);
          ARG5.setName("ARG5");
          panel2.add(ARG5);
          ARG5.setBounds(245, 147, 60, ARG5.getPreferredSize().height);

          //---- ARG2 ----
          ARG2.setComponentPopupMenu(BTD);
          ARG2.setName("ARG2");
          panel2.add(ARG2);
          ARG2.setBounds(245, 60, 40, ARG2.getPreferredSize().height);

          //---- ARGABC ----
          ARGABC.setComponentPopupMenu(BTD);
          ARGABC.setName("ARGABC");
          panel2.add(ARGABC);
          ARGABC.setBounds(245, 205, 20, ARGABC.getPreferredSize().height);

          //---- ARG6 ----
          ARG6.setComponentPopupMenu(null);
          ARG6.setName("ARG6");
          panel2.add(ARG6);
          ARG6.setBounds(245, 176, 105, ARG6.getPreferredSize().height);

          //---- RB_8 ----
          RB_8.setText("Regroupement achat");
          RB_8.setToolTipText("tri\u00e9 par");
          RB_8.setComponentPopupMenu(BTD);
          RB_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RB_8.setName("RB_8");
          panel2.add(RB_8);
          RB_8.setBounds(25, 238, 215, 20);

          //---- ARG7 ----
          ARG7.setComponentPopupMenu(null);
          ARG7.setName("ARG7");
          panel2.add(ARG7);
          ARG7.setBounds(245, 234, 114, ARG7.getPreferredSize().height);

          //---- ARGGP ----
          ARGGP.setComponentPopupMenu(BTD);
          ARGGP.setName("ARGGP");
          panel2.add(ARGGP);
          ARGGP.setBounds(339, 205, 20, ARGGP.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("GP");
          OBJ_44.setName("OBJ_44");
          panel2.add(OBJ_44);
          OBJ_44.setBounds(310, 205, 30, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 430, 280);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }

    //---- RB_GRP ----
    RB_GRP.add(RB_3);
    RB_GRP.add(RB_4);
    RB_GRP.add(RB_6);
    RB_GRP.add(RB_7);
    RB_GRP.add(RB_5);
    RB_GRP.add(RB_2);
    RB_GRP.add(RB);
    RB_GRP.add(RB_8);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiRadioButton RB_3;
  private XRiRadioButton RB_4;
  private XRiRadioButton RB_6;
  private XRiRadioButton RB_7;
  private XRiTextField ARG3;
  private XRiTextField ARG4;
  private XRiTextField ARG1;
  private XRiRadioButton RB_5;
  private XRiRadioButton RB_2;
  private XRiRadioButton RB;
  private XRiTextField ARG5;
  private XRiTextField ARG2;
  private XRiTextField ARGABC;
  private XRiCalendrier ARG6;
  private XRiRadioButton RB_8;
  private XRiTextField ARG7;
  private XRiTextField ARGGP;
  private JLabel OBJ_44;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_16;
  private ButtonGroup RB_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
