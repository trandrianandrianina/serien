//$$david$$ ££03/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vgam.VGAM04FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM04FM_C3 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] FRIN1_Value = { "", "1", "2", };
  private String[] FRIN11_Value = { "", "2", };
  private String[] FRIN7_Value = { "", "J" };
  private String[] FRIN13_Value = { "1", "2", "3" };
  private boolean isConsult = false;
  private boolean top[] = { false, false, false, false };
  
  public VGAM04FM_C3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    FRIN1.setValeurs(FRIN1_Value, null);
    FRIN4.setValeursSelection("O", "N");
    FRIN7.setValeurs(FRIN7_Value, null);
    FRIN8.setValeursSelection("", "1");
    FRIN11.setValeurs(FRIN11_Value, null);
    FRIN13.setValeurs(FRIN13_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    DVLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIBR@")).trim());
    TFLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TFLIBR@")).trim());
    NOMFRR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOMFRR@")).trim());
    OBJ_84.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WKAP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    isConsult = lexique.isTrue("53");
    
    setCloseKey("ENTER", "F4", "F12", "F22");
    
    
    OBJ_32.setVisible(lexique.isPresent("FRDEV"));
    TFLIB.setVisible(lexique.isPresent("TFLIBR"));
    
    // FRIN4.setVisible( lexique.isPresent("FRIN4"));
    // FRIN4.setSelected(lexique.HostFieldGetData("FRIN4").equalsIgnoreCase("O"));
    DVLIBR.setVisible(lexique.isPresent("DVLIBR"));
    
    // constitue 4 tops SGM avec 1 zone SDA
    String chaine = lexique.HostFieldGetData("FRNLA");
    for (int i = 0; i < 4; i++) {
      if (chaine.charAt(i) == '*') {
        top[i] = true;
      }
      else {
        top[i] = false;
      }
    }
    FRNLA1.setSelected(top[0]);
    FRNLA2.setSelected(top[1]);
    FRNLA3.setSelected(top[2]);
    FRNLA4.setSelected(top[3]);
    
    // en consult
    FRIN1.setEnabled(!isConsult);
    FRNLA1.setEnabled(!isConsult);
    FRNLA2.setEnabled(!isConsult);
    FRNLA3.setEnabled(!isConsult);
    FRNLA4.setEnabled(!isConsult);
    
    // Titre
    setTitle("Commande/facturation");
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    StringBuffer topReconstitue = new StringBuffer("    ");
    if (FRNLA1.isSelected()) {
      topReconstitue.setCharAt(0, '*');
    }
    if (FRNLA2.isSelected()) {
      topReconstitue.setCharAt(1, '*');
    }
    if (FRNLA3.isSelected()) {
      topReconstitue.setCharAt(2, '*');
    }
    if (FRNLA4.isSelected()) {
      topReconstitue.setCharAt(3, '*');
    }
    lexique.HostFieldPutData("FRNLA", 0, topReconstitue.toString());
    
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgam04"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 37);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel5 = new JPanel();
    DVLIBR = new RiZoneSortie();
    OBJ_48 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_38 = new JLabel();
    FRSRN = new XRiTextField();
    TFLIB = new RiZoneSortie();
    FRFRR = new XRiTextField();
    FRSRT = new XRiTextField();
    OBJ_32 = new JLabel();
    FRNCG = new XRiTextField();
    FRDEV = new XRiTextField();
    OBJ_29 = new JLabel();
    OBJ_104 = new JLabel();
    FRNIP = new XRiTextField();
    FRNIK = new XRiTextField();
    OBJ_35 = new JLabel();
    OBJ_36 = new JLabel();
    FRCOR = new XRiTextField();
    FRTFA = new XRiTextField();
    NOMFRR = new RiZoneSortie();
    FRIN8 = new XRiCheckBox();
    FRIN11 = new XRiComboBox();
    lbWIN11 = new JLabel();
    panel6 = new JPanel();
    FRIN1 = new XRiComboBox();
    OBJ_83 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_65 = new JLabel();
    FRIN4 = new XRiCheckBox();
    OBJ_51 = new JLabel();
    FRMCO = new XRiTextField();
    FRCTR = new XRiTextField();
    FRKAP = new XRiTextField();
    OBJ_84 = new RiZoneSortie();
    FRREM1 = new XRiTextField();
    FRREM2 = new XRiTextField();
    FRREM3 = new XRiTextField();
    FRMIN = new XRiTextField();
    FRFCO = new XRiTextField();
    FRCNR = new XRiTextField();
    FRESC = new XRiTextField();
    FRMEX = new XRiTextField();
    FRDEL1 = new XRiTextField();
    FRDEL2 = new XRiTextField();
    FRPER = new XRiTextField();
    FRIN2 = new XRiTextField();
    OBJ_77 = new JLabel();
    label1 = new JLabel();
    FRIN7 = new XRiComboBox();
    panel7 = new JPanel();
    OBJ_90 = new JLabel();
    OBJ_97 = new JLabel();
    OBJ_95 = new JLabel();
    FRLAN = new XRiTextField();
    FRNLA1 = new JCheckBox();
    FRNLA2 = new JCheckBox();
    FRNLA4 = new JCheckBox();
    FRNLA3 = new JCheckBox();
    FRIN3 = new XRiTextField();
    lb_FRIN13 = new JLabel();
    FRIN13 = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    FRNCA = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(910, 660));
    setPreferredSize(new Dimension(910, 660));
    setMaximumSize(new Dimension(910, 660));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Bonification");
              riSousMenu_bt6.setToolTipText("Code bonification");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes");
              riSousMenu_bt14.setToolTipText("Bloc-notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel5 ========
        {
          panel5.setBorder(new TitledBorder("Type"));
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);

          //---- DVLIBR ----
          DVLIBR.setText("@DVLIBR@");
          DVLIBR.setName("DVLIBR");
          panel5.add(DVLIBR);
          DVLIBR.setBounds(300, 39, 292, DVLIBR.getPreferredSize().height);

          //---- OBJ_48 ----
          OBJ_48.setText("Fournisseur de regroupement");
          OBJ_48.setName("OBJ_48");
          panel5.add(OBJ_48);
          OBJ_48.setBounds(20, 101, 179, 20);

          //---- OBJ_55 ----
          OBJ_55.setText("Num\u00e9ro compte collectif");
          OBJ_55.setName("OBJ_55");
          panel5.add(OBJ_55);
          OBJ_55.setBounds(20, 129, 145, 20);

          //---- OBJ_38 ----
          OBJ_38.setText("Num\u00e9ro d'identification UE");
          OBJ_38.setName("OBJ_38");
          panel5.add(OBJ_38);
          OBJ_38.setBounds(20, 71, 160, 20);

          //---- FRSRN ----
          FRSRN.setToolTipText("N\u00b0 Siren");
          FRSRN.setComponentPopupMenu(null);
          FRSRN.setName("FRSRN");
          panel5.add(FRSRN);
          FRSRN.setBounds(340, 67, 100, FRSRN.getPreferredSize().height);

          //---- TFLIB ----
          TFLIB.setText("@TFLIBR@");
          TFLIB.setName("TFLIB");
          panel5.add(TFLIB);
          TFLIB.setBounds(87, 39, 88, TFLIB.getPreferredSize().height);

          //---- FRFRR ----
          FRFRR.setComponentPopupMenu(null);
          FRFRR.setName("FRFRR");
          panel5.add(FRFRR);
          FRFRR.setBounds(230, 97, 65, FRFRR.getPreferredSize().height);

          //---- FRSRT ----
          FRSRT.setToolTipText("Dernier n\u00b0 du code Siret");
          FRSRT.setComponentPopupMenu(null);
          FRSRT.setName("FRSRT");
          panel5.add(FRSRT);
          FRSRT.setBounds(448, 67, 60, FRSRT.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("Devise");
          OBJ_32.setName("OBJ_32");
          panel5.add(OBJ_32);
          OBJ_32.setBounds(200, 42, 46, 18);

          //---- FRNCG ----
          FRNCG.setComponentPopupMenu(null);
          FRNCG.setName("FRNCG");
          panel5.add(FRNCG);
          FRNCG.setBounds(200, 125, 60, FRNCG.getPreferredSize().height);

          //---- FRDEV ----
          FRDEV.setComponentPopupMenu(BTD);
          FRDEV.setName("FRDEV");
          panel5.add(FRDEV);
          FRDEV.setBounds(255, 37, 40, FRDEV.getPreferredSize().height);

          //---- OBJ_29 ----
          OBJ_29.setText("Type");
          OBJ_29.setName("OBJ_29");
          panel5.add(OBJ_29);
          OBJ_29.setBounds(20, 41, 36, 20);

          //---- OBJ_104 ----
          OBJ_104.setText("Siret");
          OBJ_104.setName("OBJ_104");
          panel5.add(OBJ_104);
          OBJ_104.setBounds(305, 71, 35, 20);

          //---- FRNIP ----
          FRNIP.setToolTipText("Code pays");
          FRNIP.setComponentPopupMenu(null);
          FRNIP.setName("FRNIP");
          panel5.add(FRNIP);
          FRNIP.setBounds(200, 67, 34, FRNIP.getPreferredSize().height);

          //---- FRNIK ----
          FRNIK.setToolTipText("Cl\u00e9");
          FRNIK.setComponentPopupMenu(null);
          FRNIK.setName("FRNIK");
          panel5.add(FRNIK);
          FRNIK.setBounds(261, 67, 34, FRNIK.getPreferredSize().height);

          //---- OBJ_35 ----
          OBJ_35.setText("CP");
          OBJ_35.setName("OBJ_35");
          panel5.add(OBJ_35);
          OBJ_35.setBounds(180, 71, 24, 20);

          //---- OBJ_36 ----
          OBJ_36.setText("Cl\u00e9");
          OBJ_36.setName("OBJ_36");
          panel5.add(OBJ_36);
          OBJ_36.setBounds(240, 71, 24, 20);

          //---- FRCOR ----
          FRCOR.setComponentPopupMenu(null);
          FRCOR.setName("FRCOR");
          panel5.add(FRCOR);
          FRCOR.setBounds(200, 97, 22, FRCOR.getPreferredSize().height);

          //---- FRTFA ----
          FRTFA.setComponentPopupMenu(BTD);
          FRTFA.setName("FRTFA");
          panel5.add(FRTFA);
          FRTFA.setBounds(60, 37, 24, FRTFA.getPreferredSize().height);

          //---- NOMFRR ----
          NOMFRR.setText("@NOMFRR@");
          NOMFRR.setName("NOMFRR");
          panel5.add(NOMFRR);
          NOMFRR.setBounds(300, 99, 292, NOMFRR.getPreferredSize().height);

          //---- FRIN8 ----
          FRIN8.setText("gestion des \u00e9co-taxes");
          FRIN8.setName("FRIN8");
          panel5.add(FRIN8);
          FRIN8.setBounds(300, 127, 190, 25);

          //---- FRIN11 ----
          FRIN11.setModel(new DefaultComboBoxModel(new String[] {
            "Frais de port non inclus",
            "Frais de port inclus"
          }));
          FRIN11.setComponentPopupMenu(null);
          FRIN11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FRIN11.setName("FRIN11");
          panel5.add(FRIN11);
          FRIN11.setBounds(200, 155, 280, FRIN11.getPreferredSize().height);

          //---- lbWIN11 ----
          lbWIN11.setText("Facturation frais de port");
          lbWIN11.setName("lbWIN11");
          panel5.add(lbWIN11);
          lbWIN11.setBounds(20, 158, 200, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel5.getComponentCount(); i++) {
              Rectangle bounds = panel5.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel5.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel5.setMinimumSize(preferredSize);
            panel5.setPreferredSize(preferredSize);
          }
        }

        //======== panel6 ========
        {
          panel6.setBorder(new TitledBorder("Conditions"));
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);

          //---- FRIN1 ----
          FRIN1.setModel(new DefaultComboBoxModel(new String[] {
            "Ni cumul ni r\u00e9percussion",
            "Cumul remises en-t\u00eate plus ligne",
            "R\u00e9percussion remises sur CNA"
          }));
          FRIN1.setComponentPopupMenu(null);
          FRIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FRIN1.setName("FRIN1");
          panel6.add(FRIN1);
          FRIN1.setBounds(398, 47, 242, FRIN1.getPreferredSize().height);

          //---- OBJ_83 ----
          OBJ_83.setText("Coefficient moyen d'approche");
          OBJ_83.setName("OBJ_83");
          panel6.add(OBJ_83);
          OBJ_83.setBounds(398, 231, 181, 20);

          //---- OBJ_79 ----
          OBJ_79.setText("S\u00e9curit\u00e9");
          OBJ_79.setName("OBJ_79");
          panel6.add(OBJ_79);
          OBJ_79.setBounds(398, 201, 190, 20);

          //---- OBJ_85 ----
          OBJ_85.setText("Coefficient d'approche fixe");
          OBJ_85.setName("OBJ_85");
          panel6.add(OBJ_85);
          OBJ_85.setBounds(20, 261, 160, 20);

          //---- OBJ_59 ----
          OBJ_59.setText("Pourcentage d'escompte");
          OBJ_59.setName("OBJ_59");
          panel6.add(OBJ_59);
          OBJ_59.setBounds(398, 81, 154, 20);

          //---- OBJ_61 ----
          OBJ_61.setText("Maximum de commande");
          OBJ_61.setName("OBJ_61");
          panel6.add(OBJ_61);
          OBJ_61.setBounds(20, 111, 152, 20);

          //---- OBJ_57 ----
          OBJ_57.setText("Minimum de commande");
          OBJ_57.setName("OBJ_57");
          panel6.add(OBJ_57);
          OBJ_57.setBounds(20, 81, 148, 20);

          //---- OBJ_81 ----
          OBJ_81.setText("P\u00e9riodicit\u00e9 commande");
          OBJ_81.setName("OBJ_81");
          panel6.add(OBJ_81);
          OBJ_81.setBounds(20, 231, 138, 20);

          //---- OBJ_63 ----
          OBJ_63.setText("Mode d'exp\u00e9dition");
          OBJ_63.setName("OBJ_63");
          panel6.add(OBJ_63);
          OBJ_63.setBounds(398, 111, 133, 20);

          //---- OBJ_75 ----
          OBJ_75.setText("D\u00e9lais de livraison");
          OBJ_75.setName("OBJ_75");
          panel6.add(OBJ_75);
          OBJ_75.setBounds(20, 201, 132, 20);

          //---- OBJ_67 ----
          OBJ_67.setText("Transporteur habituel");
          OBJ_67.setName("OBJ_67");
          panel6.add(OBJ_67);
          OBJ_67.setBounds(398, 141, 130, 20);

          //---- OBJ_65 ----
          OBJ_65.setText("Minimum pour franco");
          OBJ_65.setName("OBJ_65");
          panel6.add(OBJ_65);
          OBJ_65.setBounds(20, 141, 126, 20);

          //---- FRIN4 ----
          FRIN4.setText("Reliquat interdit");
          FRIN4.setComponentPopupMenu(null);
          FRIN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FRIN4.setName("FRIN4");
          panel6.add(FRIN4);
          FRIN4.setBounds(398, 171, 117, 20);

          //---- OBJ_51 ----
          OBJ_51.setText("Remises");
          OBJ_51.setName("OBJ_51");
          panel6.add(OBJ_51);
          OBJ_51.setBounds(20, 51, 57, 20);

          //---- FRMCO ----
          FRMCO.setComponentPopupMenu(null);
          FRMCO.setName("FRMCO");
          panel6.add(FRMCO);
          FRMCO.setBounds(200, 107, 66, FRMCO.getPreferredSize().height);

          //---- FRCTR ----
          FRCTR.setComponentPopupMenu(BTD);
          FRCTR.setName("FRCTR");
          panel6.add(FRCTR);
          FRCTR.setBounds(606, 137, 34, FRCTR.getPreferredSize().height);

          //---- FRKAP ----
          FRKAP.setComponentPopupMenu(null);
          FRKAP.setName("FRKAP");
          panel6.add(FRKAP);
          FRKAP.setBounds(200, 257, 58, FRKAP.getPreferredSize().height);

          //---- OBJ_84 ----
          OBJ_84.setText("@WKAP@");
          OBJ_84.setToolTipText("Rapport moyen, sur les derniers achats, entre prix d'achat et prix de revient");
          OBJ_84.setName("OBJ_84");
          panel6.add(OBJ_84);
          OBJ_84.setBounds(591, 229, 49, OBJ_84.getPreferredSize().height);

          //---- FRREM1 ----
          FRREM1.setComponentPopupMenu(null);
          FRREM1.setName("FRREM1");
          panel6.add(FRREM1);
          FRREM1.setBounds(200, 47, 50, FRREM1.getPreferredSize().height);

          //---- FRREM2 ----
          FRREM2.setComponentPopupMenu(null);
          FRREM2.setName("FRREM2");
          panel6.add(FRREM2);
          FRREM2.setBounds(255, 47, 50, FRREM2.getPreferredSize().height);

          //---- FRREM3 ----
          FRREM3.setComponentPopupMenu(null);
          FRREM3.setName("FRREM3");
          panel6.add(FRREM3);
          FRREM3.setBounds(310, 47, 50, FRREM3.getPreferredSize().height);

          //---- FRMIN ----
          FRMIN.setComponentPopupMenu(null);
          FRMIN.setName("FRMIN");
          panel6.add(FRMIN);
          FRMIN.setBounds(200, 77, 50, FRMIN.getPreferredSize().height);

          //---- FRFCO ----
          FRFCO.setComponentPopupMenu(null);
          FRFCO.setName("FRFCO");
          panel6.add(FRFCO);
          FRFCO.setBounds(200, 137, 50, FRFCO.getPreferredSize().height);

          //---- FRCNR ----
          FRCNR.setComponentPopupMenu(BTD);
          FRCNR.setName("FRCNR");
          panel6.add(FRCNR);
          FRCNR.setBounds(200, 167, 60, FRCNR.getPreferredSize().height);

          //---- FRESC ----
          FRESC.setComponentPopupMenu(null);
          FRESC.setName("FRESC");
          panel6.add(FRESC);
          FRESC.setBounds(598, 77, 42, FRESC.getPreferredSize().height);

          //---- FRMEX ----
          FRMEX.setComponentPopupMenu(BTD);
          FRMEX.setName("FRMEX");
          panel6.add(FRMEX);
          FRMEX.setBounds(606, 107, 34, FRMEX.getPreferredSize().height);

          //---- FRDEL1 ----
          FRDEL1.setComponentPopupMenu(null);
          FRDEL1.setName("FRDEL1");
          panel6.add(FRDEL1);
          FRDEL1.setBounds(200, 197, 30, FRDEL1.getPreferredSize().height);

          //---- FRDEL2 ----
          FRDEL2.setComponentPopupMenu(null);
          FRDEL2.setName("FRDEL2");
          panel6.add(FRDEL2);
          FRDEL2.setBounds(244, 197, 30, FRDEL2.getPreferredSize().height);

          //---- FRPER ----
          FRPER.setComponentPopupMenu(null);
          FRPER.setName("FRPER");
          panel6.add(FRPER);
          FRPER.setBounds(200, 227, 30, FRPER.getPreferredSize().height);

          //---- FRIN2 ----
          FRIN2.setComponentPopupMenu(BTD);
          FRIN2.setName("FRIN2");
          panel6.add(FRIN2);
          FRIN2.setBounds(620, 197, 20, FRIN2.getPreferredSize().height);

          //---- OBJ_77 ----
          OBJ_77.setText("/");
          OBJ_77.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_77.setName("OBJ_77");
          panel6.add(OBJ_77);
          OBJ_77.setBounds(231, 201, 12, 20);

          //---- label1 ----
          label1.setText("Code de bonification");
          label1.setName("label1");
          panel6.add(label1);
          label1.setBounds(20, 171, 150, 20);

          //---- FRIN7 ----
          FRIN7.setModel(new DefaultComboBoxModel(new String[] {
            "semaines",
            "jours"
          }));
          FRIN7.setComponentPopupMenu(null);
          FRIN7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FRIN7.setName("FRIN7");
          panel6.add(FRIN7);
          FRIN7.setBounds(280, 198, 102, FRIN7.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel6.getComponentCount(); i++) {
              Rectangle bounds = panel6.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel6.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel6.setMinimumSize(preferredSize);
            panel6.setPreferredSize(preferredSize);
          }
        }

        //======== panel7 ========
        {
          panel7.setBorder(new TitledBorder("Edition de bon"));
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);

          //---- OBJ_90 ----
          OBJ_90.setText("N\u00b0 du libell\u00e9 article \u00e9dit\u00e9");
          OBJ_90.setName("OBJ_90");
          panel7.add(OBJ_90);
          OBJ_90.setBounds(20, 37, 160, 20);

          //---- OBJ_97 ----
          OBJ_97.setText("Nombre d'exemplaires bon ");
          OBJ_97.setName("OBJ_97");
          panel7.add(OBJ_97);
          OBJ_97.setBounds(395, 40, 180, 20);

          //---- OBJ_95 ----
          OBJ_95.setText("Code langue");
          OBJ_95.setName("OBJ_95");
          panel7.add(OBJ_95);
          OBJ_95.setBounds(20, 66, 81, 20);

          //---- FRLAN ----
          FRLAN.setComponentPopupMenu(null);
          FRLAN.setName("FRLAN");
          panel7.add(FRLAN);
          FRLAN.setBounds(200, 62, 70, FRLAN.getPreferredSize().height);

          //---- FRNLA1 ----
          FRNLA1.setText("1");
          FRNLA1.setComponentPopupMenu(null);
          FRNLA1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FRNLA1.setName("FRNLA1");
          panel7.add(FRNLA1);
          FRNLA1.setBounds(200, 37, 35, 20);

          //---- FRNLA2 ----
          FRNLA2.setText("2");
          FRNLA2.setComponentPopupMenu(null);
          FRNLA2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FRNLA2.setName("FRNLA2");
          panel7.add(FRNLA2);
          FRNLA2.setBounds(240, 37, 35, 20);

          //---- FRNLA4 ----
          FRNLA4.setText("4");
          FRNLA4.setComponentPopupMenu(null);
          FRNLA4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FRNLA4.setName("FRNLA4");
          panel7.add(FRNLA4);
          FRNLA4.setBounds(320, 37, 35, 20);

          //---- FRNLA3 ----
          FRNLA3.setText("3");
          FRNLA3.setComponentPopupMenu(null);
          FRNLA3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FRNLA3.setName("FRNLA3");
          panel7.add(FRNLA3);
          FRNLA3.setBounds(280, 37, 35, 20);

          //---- FRIN3 ----
          FRIN3.setComponentPopupMenu(null);
          FRIN3.setName("FRIN3");
          panel7.add(FRIN3);
          FRIN3.setBounds(635, 40, 20, FRIN3.getPreferredSize().height);

          //---- lb_FRIN13 ----
          lb_FRIN13.setText("Edition chiffrage ");
          lb_FRIN13.setName("lb_FRIN13");
          panel7.add(lb_FRIN13);
          lb_FRIN13.setBounds(395, 73, 100, 20);

          //---- FRIN13 ----
          FRIN13.setModel(new DefaultComboBoxModel(new String[] {
            "Chiffr\u00e9 avec total",
            "Chiffr\u00e9",
            "Non chiffr\u00e9\t"
          }));
          FRIN13.setComponentPopupMenu(null);
          FRIN13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          FRIN13.setName("FRIN13");
          panel7.add(FRIN13);
          FRIN13.setBounds(500, 70, 155, FRIN13.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel7.getComponentCount(); i++) {
              Rectangle bounds = panel7.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel7.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel7.setMinimumSize(preferredSize);
            panel7.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 197, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(panel7, GroupLayout.DEFAULT_SIZE, 111, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_21 ----
      OBJ_21.setText("Choix possibles");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }

    //---- FRNCA ----
    FRNCA.setComponentPopupMenu(BTD);
    FRNCA.setName("FRNCA");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private JPanel p_contenu;
  private JPanel panel5;
  private RiZoneSortie DVLIBR;
  private JLabel OBJ_48;
  private JLabel OBJ_55;
  private JLabel OBJ_38;
  private XRiTextField FRSRN;
  private RiZoneSortie TFLIB;
  private XRiTextField FRFRR;
  private XRiTextField FRSRT;
  private JLabel OBJ_32;
  private XRiTextField FRNCG;
  private XRiTextField FRDEV;
  private JLabel OBJ_29;
  private JLabel OBJ_104;
  private XRiTextField FRNIP;
  private XRiTextField FRNIK;
  private JLabel OBJ_35;
  private JLabel OBJ_36;
  private XRiTextField FRCOR;
  private XRiTextField FRTFA;
  private RiZoneSortie NOMFRR;
  private XRiCheckBox FRIN8;
  private XRiComboBox FRIN11;
  private JLabel lbWIN11;
  private JPanel panel6;
  private XRiComboBox FRIN1;
  private JLabel OBJ_83;
  private JLabel OBJ_79;
  private JLabel OBJ_85;
  private JLabel OBJ_59;
  private JLabel OBJ_61;
  private JLabel OBJ_57;
  private JLabel OBJ_81;
  private JLabel OBJ_63;
  private JLabel OBJ_75;
  private JLabel OBJ_67;
  private JLabel OBJ_65;
  private XRiCheckBox FRIN4;
  private JLabel OBJ_51;
  private XRiTextField FRMCO;
  private XRiTextField FRCTR;
  private XRiTextField FRKAP;
  private RiZoneSortie OBJ_84;
  private XRiTextField FRREM1;
  private XRiTextField FRREM2;
  private XRiTextField FRREM3;
  private XRiTextField FRMIN;
  private XRiTextField FRFCO;
  private XRiTextField FRCNR;
  private XRiTextField FRESC;
  private XRiTextField FRMEX;
  private XRiTextField FRDEL1;
  private XRiTextField FRDEL2;
  private XRiTextField FRPER;
  private XRiTextField FRIN2;
  private JLabel OBJ_77;
  private JLabel label1;
  private XRiComboBox FRIN7;
  private JPanel panel7;
  private JLabel OBJ_90;
  private JLabel OBJ_97;
  private JLabel OBJ_95;
  private XRiTextField FRLAN;
  private JCheckBox FRNLA1;
  private JCheckBox FRNLA2;
  private JCheckBox FRNLA4;
  private JCheckBox FRNLA3;
  private XRiTextField FRIN3;
  private JLabel lb_FRIN13;
  private XRiComboBox FRIN13;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_20;
  private XRiTextField FRNCA;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
