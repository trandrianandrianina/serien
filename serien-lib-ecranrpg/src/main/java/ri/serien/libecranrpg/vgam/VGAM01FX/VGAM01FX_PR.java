
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_PR extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM01FX_PR(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    PRLIB.setEnabled(lexique.isPresent("PRLIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LIBGRP/+1/@ - Personnalisation de @LIBGRP/-1/@"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_48 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel3 = new JXTitledPanel();
    panel3 = new JPanel();
    PRLIB = new XRiTextField();
    OBJ_51 = new JLabel();
    panel4 = new JPanel();
    PRF01 = new XRiTextField();
    PRF02 = new XRiTextField();
    PRF03 = new XRiTextField();
    PRF04 = new XRiTextField();
    PRF05 = new XRiTextField();
    PRF06 = new XRiTextField();
    PRF07 = new XRiTextField();
    PRF08 = new XRiTextField();
    PRF09 = new XRiTextField();
    PRF10 = new XRiTextField();
    PRL01 = new XRiTextField();
    PRL02 = new XRiTextField();
    PRL03 = new XRiTextField();
    PRL04 = new XRiTextField();
    PRL05 = new XRiTextField();
    PRL06 = new XRiTextField();
    PRL07 = new XRiTextField();
    PRL08 = new XRiTextField();
    PRL09 = new XRiTextField();
    PRL10 = new XRiTextField();
    PRB01 = new XRiTextField();
    PRB02 = new XRiTextField();
    PRF4 = new XRiTextField();
    PRB04 = new XRiTextField();
    PRB05 = new XRiTextField();
    PRB06 = new XRiTextField();
    PRB07 = new XRiTextField();
    PRB08 = new XRiTextField();
    PRB09 = new XRiTextField();
    PRB10 = new XRiTextField();
    PRC01 = new XRiTextField();
    PRC02 = new XRiTextField();
    PRC03 = new XRiTextField();
    PRC04 = new XRiTextField();
    PRC05 = new XRiTextField();
    PRC06 = new XRiTextField();
    PRC07 = new XRiTextField();
    PRC08 = new XRiTextField();
    PRC09 = new XRiTextField();
    PRC10 = new XRiTextField();
    PRM01 = new XRiTextField();
    PRM02 = new XRiTextField();
    PRM03 = new XRiTextField();
    PRM04 = new XRiTextField();
    PRM05 = new XRiTextField();
    PRM06 = new XRiTextField();
    PRM07 = new XRiTextField();
    PRM08 = new XRiTextField();
    PRM09 = new XRiTextField();
    PRM10 = new XRiTextField();
    PRT01 = new XRiTextField();
    PRT02 = new XRiTextField();
    PRT03 = new XRiTextField();
    PRT04 = new XRiTextField();
    PRT05 = new XRiTextField();
    PRT06 = new XRiTextField();
    PRT07 = new XRiTextField();
    PRT08 = new XRiTextField();
    PRT09 = new XRiTextField();
    PRT10 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_48 ----
          OBJ_48.setText("Ordre");
          OBJ_48.setName("OBJ_48");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(750, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Calcul de prix de revient");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder(""));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- PRLIB ----
              PRLIB.setComponentPopupMenu(BTD);
              PRLIB.setName("PRLIB");
              panel3.add(PRLIB);
              PRLIB.setBounds(130, 10, 160, PRLIB.getPreferredSize().height);

              //---- OBJ_51 ----
              OBJ_51.setText("Type");
              OBJ_51.setName("OBJ_51");
              panel3.add(OBJ_51);
              OBJ_51.setBounds(25, 14, 36, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel3ContentContainer.add(panel3);
            panel3.setBounds(20, 10, 660, 50);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("El\u00e9ments de frais"));
              panel4.setOpaque(false);
              panel4.setName("panel4");

              //---- PRF01 ----
              PRF01.setName("PRF01");

              //---- PRF02 ----
              PRF02.setName("PRF02");

              //---- PRF03 ----
              PRF03.setName("PRF03");

              //---- PRF04 ----
              PRF04.setName("PRF04");

              //---- PRF05 ----
              PRF05.setName("PRF05");

              //---- PRF06 ----
              PRF06.setName("PRF06");

              //---- PRF07 ----
              PRF07.setName("PRF07");

              //---- PRF08 ----
              PRF08.setName("PRF08");

              //---- PRF09 ----
              PRF09.setName("PRF09");

              //---- PRF10 ----
              PRF10.setName("PRF10");

              //---- PRL01 ----
              PRL01.setName("PRL01");

              //---- PRL02 ----
              PRL02.setName("PRL02");

              //---- PRL03 ----
              PRL03.setName("PRL03");

              //---- PRL04 ----
              PRL04.setName("PRL04");

              //---- PRL05 ----
              PRL05.setName("PRL05");

              //---- PRL06 ----
              PRL06.setName("PRL06");

              //---- PRL07 ----
              PRL07.setName("PRL07");

              //---- PRL08 ----
              PRL08.setName("PRL08");

              //---- PRL09 ----
              PRL09.setName("PRL09");

              //---- PRL10 ----
              PRL10.setName("PRL10");

              //---- PRB01 ----
              PRB01.setName("PRB01");

              //---- PRB02 ----
              PRB02.setName("PRB02");

              //---- PRF4 ----
              PRF4.setName("PRF4");

              //---- PRB04 ----
              PRB04.setName("PRB04");

              //---- PRB05 ----
              PRB05.setName("PRB05");

              //---- PRB06 ----
              PRB06.setName("PRB06");

              //---- PRB07 ----
              PRB07.setName("PRB07");

              //---- PRB08 ----
              PRB08.setName("PRB08");

              //---- PRB09 ----
              PRB09.setName("PRB09");

              //---- PRB10 ----
              PRB10.setName("PRB10");

              //---- PRC01 ----
              PRC01.setName("PRC01");

              //---- PRC02 ----
              PRC02.setName("PRC02");

              //---- PRC03 ----
              PRC03.setName("PRC03");

              //---- PRC04 ----
              PRC04.setName("PRC04");

              //---- PRC05 ----
              PRC05.setName("PRC05");

              //---- PRC06 ----
              PRC06.setName("PRC06");

              //---- PRC07 ----
              PRC07.setName("PRC07");

              //---- PRC08 ----
              PRC08.setName("PRC08");

              //---- PRC09 ----
              PRC09.setName("PRC09");

              //---- PRC10 ----
              PRC10.setName("PRC10");

              //---- PRM01 ----
              PRM01.setName("PRM01");

              //---- PRM02 ----
              PRM02.setName("PRM02");

              //---- PRM03 ----
              PRM03.setName("PRM03");

              //---- PRM04 ----
              PRM04.setName("PRM04");

              //---- PRM05 ----
              PRM05.setName("PRM05");

              //---- PRM06 ----
              PRM06.setName("PRM06");

              //---- PRM07 ----
              PRM07.setName("PRM07");

              //---- PRM08 ----
              PRM08.setName("PRM08");

              //---- PRM09 ----
              PRM09.setName("PRM09");

              //---- PRM10 ----
              PRM10.setName("PRM10");

              //---- PRT01 ----
              PRT01.setName("PRT01");

              //---- PRT02 ----
              PRT02.setName("PRT02");

              //---- PRT03 ----
              PRT03.setName("PRT03");

              //---- PRT04 ----
              PRT04.setName("PRT04");

              //---- PRT05 ----
              PRT05.setName("PRT05");

              //---- PRT06 ----
              PRT06.setName("PRT06");

              //---- PRT07 ----
              PRT07.setName("PRT07");

              //---- PRT08 ----
              PRT08.setName("PRT08");

              //---- PRT09 ----
              PRT09.setName("PRT09");

              //---- PRT10 ----
              PRT10.setName("PRT10");

              //---- label1 ----
              label1.setText("Code");
              label1.setName("label1");

              //---- label2 ----
              label2.setText("Libell\u00e9");
              label2.setHorizontalAlignment(SwingConstants.CENTER);
              label2.setName("label2");

              //---- label3 ----
              label3.setText("Base de frais");
              label3.setHorizontalAlignment(SwingConstants.CENTER);
              label3.setName("label3");

              //---- label4 ----
              label4.setText("Coefficient");
              label4.setHorizontalAlignment(SwingConstants.CENTER);
              label4.setName("label4");

              //---- label5 ----
              label5.setText("ou");
              label5.setHorizontalAlignment(SwingConstants.CENTER);
              label5.setName("label5");

              //---- label6 ----
              label6.setText("Valeur");
              label6.setHorizontalAlignment(SwingConstants.CENTER);
              label6.setName("label6");

              //---- label7 ----
              label7.setText("Unit\u00e9");
              label7.setHorizontalAlignment(SwingConstants.CENTER);
              label7.setName("label7");

              GroupLayout panel4Layout = new GroupLayout(panel4);
              panel4.setLayout(panel4Layout);
              panel4Layout.setHorizontalGroup(
                panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(31, 31, 31)
                    .addComponent(label1)
                    .addGap(35, 35, 35)
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                    .addGap(4, 4, 4)
                    .addComponent(label3)
                    .addGap(18, 18, 18)
                    .addComponent(label4, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label5)
                    .addGap(11, 11, 11)
                    .addComponent(label6, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                    .addGap(40, 40, 40)
                    .addComponent(label7))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(36, 36, 36)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(PRF02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRF01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRF04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRF06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRF09, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRF07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRF10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRF08, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRF05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRF03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGap(40, 40, 40)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(PRL01, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRL07, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRL08, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRL06, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRL03, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRL04, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRL09, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRL05, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRL10, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRL02, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                    .addGap(30, 30, 30)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(PRB01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRB07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRB06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRB04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRB08, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRF4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRB10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRB09, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRB05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRB02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGap(45, 45, 45)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(PRC05, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRC04, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRC01, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRC02, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRC03, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRC09, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRC08, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRC10, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRC07, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRC06, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
                    .addGap(35, 35, 35)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(PRM06, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRM07, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRM05, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRM02, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRM04, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRM03, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRM01, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRM09, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRM10, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRM08, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                    .addGap(45, 45, 45)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(PRT02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRT03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRT05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRT06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRT01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRT04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRT10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRT08, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRT09, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(PRT07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
              );
              panel4Layout.setVerticalGroup(
                panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(label1)
                      .addComponent(label2)
                      .addComponent(label3)
                      .addComponent(label4)
                      .addComponent(label5)
                      .addComponent(label6)
                      .addComponent(label7))
                    .addGap(4, 4, 4)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGroup(panel4Layout.createParallelGroup()
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(PRF02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(PRF01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(22, 22, 22)
                        .addGroup(panel4Layout.createParallelGroup()
                          .addComponent(PRF04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(50, 50, 50)
                            .addComponent(PRF06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(125, 125, 125)
                            .addComponent(PRF09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(75, 75, 75)
                            .addComponent(PRF07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(150, 150, 150)
                            .addComponent(PRF10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(PRF08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(PRF05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(PRF03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addComponent(PRL01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22)
                        .addGroup(panel4Layout.createParallelGroup()
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(PRL07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(125, 125, 125)
                            .addComponent(PRL08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(75, 75, 75)
                            .addComponent(PRL06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(PRL03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(PRL04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(150, 150, 150)
                            .addComponent(PRL09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(50, 50, 50)
                            .addComponent(PRL05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(175, 175, 175)
                            .addComponent(PRL10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(PRL02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addComponent(PRB01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22)
                        .addGroup(panel4Layout.createParallelGroup()
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(PRB07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(75, 75, 75)
                            .addComponent(PRB06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(PRB04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(125, 125, 125)
                            .addComponent(PRB08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(PRF4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(175, 175, 175)
                            .addComponent(PRB10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(150, 150, 150)
                            .addComponent(PRB09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(50, 50, 50)
                            .addComponent(PRB05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(PRB02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGroup(panel4Layout.createParallelGroup()
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(PRC05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(75, 75, 75)
                            .addComponent(PRC04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(PRC01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(PRC02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(50, 50, 50)
                            .addComponent(PRC03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addGap(22, 22, 22)
                        .addGroup(panel4Layout.createParallelGroup()
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(50, 50, 50)
                            .addComponent(PRC09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(PRC08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(75, 75, 75)
                            .addComponent(PRC10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(PRC07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addComponent(PRC06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGroup(panel4Layout.createParallelGroup()
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(125, 125, 125)
                            .addComponent(PRM06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(150, 150, 150)
                            .addComponent(PRM07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(PRM05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(PRM02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(75, 75, 75)
                            .addComponent(PRM04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(50, 50, 50)
                            .addComponent(PRM03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(PRM01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGap(22, 22, 22)
                        .addGroup(panel4Layout.createParallelGroup()
                          .addComponent(PRM09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(PRM10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addComponent(PRM08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGroup(panel4Layout.createParallelGroup()
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(PRT02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(50, 50, 50)
                            .addComponent(PRT03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(PRT05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(125, 125, 125)
                            .addComponent(PRT06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(PRT01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(75, 75, 75)
                            .addComponent(PRT04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                        .addGap(22, 22, 22)
                        .addGroup(panel4Layout.createParallelGroup()
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(50, 50, 50)
                            .addComponent(PRT10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addComponent(PRT08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(PRT09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addComponent(PRT07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
              );
            }
            xTitledPanel3ContentContainer.add(panel4);
            panel4.setBounds(20, 60, 660, 350);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 703, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 449, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_48;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel3;
  private JPanel panel3;
  private XRiTextField PRLIB;
  private JLabel OBJ_51;
  private JPanel panel4;
  private XRiTextField PRF01;
  private XRiTextField PRF02;
  private XRiTextField PRF03;
  private XRiTextField PRF04;
  private XRiTextField PRF05;
  private XRiTextField PRF06;
  private XRiTextField PRF07;
  private XRiTextField PRF08;
  private XRiTextField PRF09;
  private XRiTextField PRF10;
  private XRiTextField PRL01;
  private XRiTextField PRL02;
  private XRiTextField PRL03;
  private XRiTextField PRL04;
  private XRiTextField PRL05;
  private XRiTextField PRL06;
  private XRiTextField PRL07;
  private XRiTextField PRL08;
  private XRiTextField PRL09;
  private XRiTextField PRL10;
  private XRiTextField PRB01;
  private XRiTextField PRB02;
  private XRiTextField PRF4;
  private XRiTextField PRB04;
  private XRiTextField PRB05;
  private XRiTextField PRB06;
  private XRiTextField PRB07;
  private XRiTextField PRB08;
  private XRiTextField PRB09;
  private XRiTextField PRB10;
  private XRiTextField PRC01;
  private XRiTextField PRC02;
  private XRiTextField PRC03;
  private XRiTextField PRC04;
  private XRiTextField PRC05;
  private XRiTextField PRC06;
  private XRiTextField PRC07;
  private XRiTextField PRC08;
  private XRiTextField PRC09;
  private XRiTextField PRC10;
  private XRiTextField PRM01;
  private XRiTextField PRM02;
  private XRiTextField PRM03;
  private XRiTextField PRM04;
  private XRiTextField PRM05;
  private XRiTextField PRM06;
  private XRiTextField PRM07;
  private XRiTextField PRM08;
  private XRiTextField PRM09;
  private XRiTextField PRM10;
  private XRiTextField PRT01;
  private XRiTextField PRT02;
  private XRiTextField PRT03;
  private XRiTextField PRT04;
  private XRiTextField PRT05;
  private XRiTextField PRT06;
  private XRiTextField PRT07;
  private XRiTextField PRT08;
  private XRiTextField PRT09;
  private XRiTextField PRT10;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
