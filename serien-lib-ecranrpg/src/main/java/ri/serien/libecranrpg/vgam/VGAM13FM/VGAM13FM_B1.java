
package ri.serien.libecranrpg.vgam.VGAM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGAM13FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM13FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    FRS1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRS1@")).trim());
    FRS2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRS2@")).trim());
    FRS3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRS3@")).trim());
    FRS4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRS4@")).trim());
    FRS5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRS5@")).trim());
    FRN1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRN1@")).trim());
    FRN2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRN2@")).trim());
    FRN3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRN3@")).trim());
    FRN4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRN4@")).trim());
    FRN5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRN51@")).trim());
    TOT01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT01@")).trim());
    TOT1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT1@")).trim());
    TOT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT2@")).trim());
    TOT3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT3@")).trim());
    TOT4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT4@")).trim());
    TOT5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT5@")).trim());
    TOT6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT6@")).trim());
    TOT02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT02@")).trim());
    TOT03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT03@")).trim());
    TOT04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT04@")).trim());
    TOT05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT05@")).trim());
    TOT06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT06@")).trim());
    TOT07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT07@")).trim());
    TOT08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT08@")).trim());
    TOT09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT09@")).trim());
    TOT10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT10@")).trim());
    TOT11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT11@")).trim());
    TOT12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT12@")).trim());
    TOT13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT13@")).trim());
    TOT14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT14@")).trim());
    TOT15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_26 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_28 = new JLabel();
    INDMAG = new XRiTextField();
    OBJ_64 = new JLabel();
    INDAR0 = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_40 = new JLabel();
    A1FAM = new XRiTextField();
    OBJ_41 = new JLabel();
    A1CL1 = new XRiTextField();
    OBJ_42 = new JLabel();
    A1CL2 = new XRiTextField();
    OBJ_55 = new JLabel();
    A1UNS = new JTextField();
    DAT01 = new XRiCalendrier();
    DAT02 = new XRiCalendrier();
    DAT03 = new XRiCalendrier();
    DAT04 = new XRiCalendrier();
    DAT05 = new XRiCalendrier();
    DAT06 = new XRiCalendrier();
    DAT07 = new XRiCalendrier();
    DAT08 = new XRiCalendrier();
    DAT09 = new XRiCalendrier();
    DAT10 = new XRiCalendrier();
    DAT11 = new XRiCalendrier();
    DAT12 = new XRiCalendrier();
    DAT13 = new XRiCalendrier();
    DAT14 = new XRiCalendrier();
    DAT15 = new XRiCalendrier();
    OBJ_43 = new JLabel();
    FRS1 = new RiZoneSortie();
    FRS2 = new RiZoneSortie();
    FRS3 = new RiZoneSortie();
    FRS4 = new RiZoneSortie();
    FRS5 = new RiZoneSortie();
    FRN1 = new RiZoneSortie();
    FRN2 = new RiZoneSortie();
    FRN3 = new RiZoneSortie();
    FRN4 = new RiZoneSortie();
    FRN5 = new RiZoneSortie();
    TOT01 = new RiZoneSortie();
    QT101 = new XRiTextField();
    QT102 = new XRiTextField();
    QT103 = new XRiTextField();
    QT104 = new XRiTextField();
    QT105 = new XRiTextField();
    QT106 = new XRiTextField();
    QT107 = new XRiTextField();
    QT108 = new XRiTextField();
    QT109 = new XRiTextField();
    QT110 = new XRiTextField();
    QT111 = new XRiTextField();
    QT112 = new XRiTextField();
    QT113 = new XRiTextField();
    QT114 = new XRiTextField();
    QT115 = new XRiTextField();
    QT201 = new XRiTextField();
    QT301 = new XRiTextField();
    QT401 = new XRiTextField();
    QT501 = new XRiTextField();
    QT202 = new XRiTextField();
    QT203 = new XRiTextField();
    QT204 = new XRiTextField();
    QT205 = new XRiTextField();
    QT206 = new XRiTextField();
    QT207 = new XRiTextField();
    QT208 = new XRiTextField();
    QT209 = new XRiTextField();
    QT210 = new XRiTextField();
    QT211 = new XRiTextField();
    QT212 = new XRiTextField();
    QT213 = new XRiTextField();
    QT214 = new XRiTextField();
    QT215 = new XRiTextField();
    QT302 = new XRiTextField();
    QT303 = new XRiTextField();
    QT304 = new XRiTextField();
    QT305 = new XRiTextField();
    QT306 = new XRiTextField();
    QT307 = new XRiTextField();
    QT308 = new XRiTextField();
    QT309 = new XRiTextField();
    QT310 = new XRiTextField();
    QT311 = new XRiTextField();
    QT312 = new XRiTextField();
    QT313 = new XRiTextField();
    QT314 = new XRiTextField();
    QT315 = new XRiTextField();
    QT402 = new XRiTextField();
    QT403 = new XRiTextField();
    QT404 = new XRiTextField();
    QT405 = new XRiTextField();
    QT406 = new XRiTextField();
    QT407 = new XRiTextField();
    QT408 = new XRiTextField();
    QT409 = new XRiTextField();
    QT410 = new XRiTextField();
    QT411 = new XRiTextField();
    QT412 = new XRiTextField();
    QT413 = new XRiTextField();
    QT414 = new XRiTextField();
    QT415 = new XRiTextField();
    QT502 = new XRiTextField();
    QT503 = new XRiTextField();
    QT504 = new XRiTextField();
    QT505 = new XRiTextField();
    QT506 = new XRiTextField();
    QT507 = new XRiTextField();
    QT508 = new XRiTextField();
    QT509 = new XRiTextField();
    QT510 = new XRiTextField();
    QT511 = new XRiTextField();
    QT512 = new XRiTextField();
    QT513 = new XRiTextField();
    QT514 = new XRiTextField();
    QT515 = new XRiTextField();
    OBJ_38 = new JLabel();
    TOT1 = new RiZoneSortie();
    TOT2 = new RiZoneSortie();
    TOT3 = new RiZoneSortie();
    TOT4 = new RiZoneSortie();
    TOT5 = new RiZoneSortie();
    TOT6 = new RiZoneSortie();
    TOT02 = new RiZoneSortie();
    TOT03 = new RiZoneSortie();
    TOT04 = new RiZoneSortie();
    TOT05 = new RiZoneSortie();
    TOT06 = new RiZoneSortie();
    TOT07 = new RiZoneSortie();
    TOT08 = new RiZoneSortie();
    TOT09 = new RiZoneSortie();
    TOT10 = new RiZoneSortie();
    TOT11 = new RiZoneSortie();
    TOT12 = new RiZoneSortie();
    TOT13 = new RiZoneSortie();
    TOT14 = new RiZoneSortie();
    TOT15 = new RiZoneSortie();
    OBJ_44 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("R\u00e9approvisionnement multi-fournisseurs");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_26 ----
          OBJ_26.setText("Etablissement");
          OBJ_26.setName("OBJ_26");
          p_tete_gauche.add(OBJ_26);
          OBJ_26.setBounds(5, 2, 95, 25);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(110, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setText("Magasin");
          OBJ_28.setName("OBJ_28");
          p_tete_gauche.add(OBJ_28);
          OBJ_28.setBounds(175, 2, 60, 25);

          //---- INDMAG ----
          INDMAG.setComponentPopupMenu(BTD);
          INDMAG.setName("INDMAG");
          p_tete_gauche.add(INDMAG);
          INDMAG.setBounds(240, 0, 30, INDMAG.getPreferredSize().height);

          //---- OBJ_64 ----
          OBJ_64.setText("Article");
          OBJ_64.setName("OBJ_64");
          p_tete_gauche.add(OBJ_64);
          OBJ_64.setBounds(305, 2, 60, 25);

          //---- INDAR0 ----
          INDAR0.setComponentPopupMenu(BTD);
          INDAR0.setName("INDAR0");
          p_tete_gauche.add(INDAR0);
          INDAR0.setBounds(365, 0, 210, INDAR0.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(970, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Fournisseurs r\u00e9f\u00e9renc\u00e9s"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_40 ----
            OBJ_40.setText("Groupe famille");
            OBJ_40.setName("OBJ_40");
            panel1.add(OBJ_40);
            OBJ_40.setBounds(15, 25, 90, 25);

            //---- A1FAM ----
            A1FAM.setComponentPopupMenu(BTD);
            A1FAM.setName("A1FAM");
            panel1.add(A1FAM);
            A1FAM.setBounds(120, 25, 40, A1FAM.getPreferredSize().height);

            //---- OBJ_41 ----
            OBJ_41.setText("Classement 1");
            OBJ_41.setName("OBJ_41");
            panel1.add(OBJ_41);
            OBJ_41.setBounds(200, 25, 105, 25);

            //---- A1CL1 ----
            A1CL1.setComponentPopupMenu(BTD);
            A1CL1.setName("A1CL1");
            panel1.add(A1CL1);
            A1CL1.setBounds(305, 25, 160, A1CL1.getPreferredSize().height);

            //---- OBJ_42 ----
            OBJ_42.setText("Classement 2");
            OBJ_42.setName("OBJ_42");
            panel1.add(OBJ_42);
            OBJ_42.setBounds(495, 25, 105, 25);

            //---- A1CL2 ----
            A1CL2.setComponentPopupMenu(BTD);
            A1CL2.setName("A1CL2");
            panel1.add(A1CL2);
            A1CL2.setBounds(600, 25, 160, A1CL2.getPreferredSize().height);

            //---- OBJ_55 ----
            OBJ_55.setText("Un.Stk");
            OBJ_55.setName("OBJ_55");
            panel1.add(OBJ_55);
            OBJ_55.setBounds(780, 30, 42, 16);

            //---- A1UNS ----
            A1UNS.setComponentPopupMenu(BTD);
            A1UNS.setName("A1UNS");
            panel1.add(A1UNS);
            A1UNS.setBounds(835, 25, 30, A1UNS.getPreferredSize().height);

            //---- DAT01 ----
            DAT01.setName("DAT01");
            panel1.add(DAT01);
            DAT01.setBounds(60, 115, 105, DAT01.getPreferredSize().height);

            //---- DAT02 ----
            DAT02.setName("DAT02");
            panel1.add(DAT02);
            DAT02.setBounds(60, 140, 105, DAT02.getPreferredSize().height);

            //---- DAT03 ----
            DAT03.setName("DAT03");
            panel1.add(DAT03);
            DAT03.setBounds(60, 165, 105, DAT03.getPreferredSize().height);

            //---- DAT04 ----
            DAT04.setName("DAT04");
            panel1.add(DAT04);
            DAT04.setBounds(60, 190, 105, DAT04.getPreferredSize().height);

            //---- DAT05 ----
            DAT05.setName("DAT05");
            panel1.add(DAT05);
            DAT05.setBounds(60, 215, 105, DAT05.getPreferredSize().height);

            //---- DAT06 ----
            DAT06.setName("DAT06");
            panel1.add(DAT06);
            DAT06.setBounds(60, 240, 105, DAT06.getPreferredSize().height);

            //---- DAT07 ----
            DAT07.setName("DAT07");
            panel1.add(DAT07);
            DAT07.setBounds(60, 265, 105, DAT07.getPreferredSize().height);

            //---- DAT08 ----
            DAT08.setName("DAT08");
            panel1.add(DAT08);
            DAT08.setBounds(60, 290, 105, DAT08.getPreferredSize().height);

            //---- DAT09 ----
            DAT09.setName("DAT09");
            panel1.add(DAT09);
            DAT09.setBounds(60, 315, 105, DAT09.getPreferredSize().height);

            //---- DAT10 ----
            DAT10.setName("DAT10");
            panel1.add(DAT10);
            DAT10.setBounds(60, 340, 105, DAT10.getPreferredSize().height);

            //---- DAT11 ----
            DAT11.setName("DAT11");
            panel1.add(DAT11);
            DAT11.setBounds(60, 365, 105, DAT11.getPreferredSize().height);

            //---- DAT12 ----
            DAT12.setName("DAT12");
            panel1.add(DAT12);
            DAT12.setBounds(60, 390, 105, DAT12.getPreferredSize().height);

            //---- DAT13 ----
            DAT13.setName("DAT13");
            panel1.add(DAT13);
            DAT13.setBounds(60, 415, 105, DAT13.getPreferredSize().height);

            //---- DAT14 ----
            DAT14.setName("DAT14");
            panel1.add(DAT14);
            DAT14.setBounds(60, 440, 105, DAT14.getPreferredSize().height);

            //---- DAT15 ----
            DAT15.setName("DAT15");
            panel1.add(DAT15);
            DAT15.setBounds(60, 465, 105, DAT15.getPreferredSize().height);

            //---- OBJ_43 ----
            OBJ_43.setText("Dates de r\u00e9appro");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(60, 90, 105, 25);

            //---- FRS1 ----
            FRS1.setText("@FRS1@");
            FRS1.setName("FRS1");
            panel1.add(FRS1);
            FRS1.setBounds(200, 55, 84, FRS1.getPreferredSize().height);

            //---- FRS2 ----
            FRS2.setText("@FRS2@");
            FRS2.setName("FRS2");
            panel1.add(FRS2);
            FRS2.setBounds(320, 55, 84, FRS2.getPreferredSize().height);

            //---- FRS3 ----
            FRS3.setText("@FRS3@");
            FRS3.setName("FRS3");
            panel1.add(FRS3);
            FRS3.setBounds(440, 55, 84, FRS3.getPreferredSize().height);

            //---- FRS4 ----
            FRS4.setText("@FRS4@");
            FRS4.setName("FRS4");
            panel1.add(FRS4);
            FRS4.setBounds(560, 55, 84, FRS4.getPreferredSize().height);

            //---- FRS5 ----
            FRS5.setText("@FRS5@");
            FRS5.setName("FRS5");
            panel1.add(FRS5);
            FRS5.setBounds(680, 55, 84, FRS5.getPreferredSize().height);

            //---- FRN1 ----
            FRN1.setText("@FRN1@");
            FRN1.setName("FRN1");
            panel1.add(FRN1);
            FRN1.setBounds(170, 85, 114, FRN1.getPreferredSize().height);

            //---- FRN2 ----
            FRN2.setText("@FRN2@");
            FRN2.setName("FRN2");
            panel1.add(FRN2);
            FRN2.setBounds(290, 85, 114, FRN2.getPreferredSize().height);

            //---- FRN3 ----
            FRN3.setText("@FRN3@");
            FRN3.setName("FRN3");
            panel1.add(FRN3);
            FRN3.setBounds(410, 85, 114, FRN3.getPreferredSize().height);

            //---- FRN4 ----
            FRN4.setText("@FRN4@");
            FRN4.setName("FRN4");
            panel1.add(FRN4);
            FRN4.setBounds(530, 85, 114, FRN4.getPreferredSize().height);

            //---- FRN5 ----
            FRN5.setText("@FRN51@");
            FRN5.setName("FRN5");
            panel1.add(FRN5);
            FRN5.setBounds(650, 85, 114, FRN5.getPreferredSize().height);

            //---- TOT01 ----
            TOT01.setText("@TOT01@");
            TOT01.setName("TOT01");
            panel1.add(TOT01);
            TOT01.setBounds(775, 120, 114, TOT01.getPreferredSize().height);

            //---- QT101 ----
            QT101.setName("QT101");
            panel1.add(QT101);
            QT101.setBounds(170, 115, 114, QT101.getPreferredSize().height);

            //---- QT102 ----
            QT102.setName("QT102");
            panel1.add(QT102);
            QT102.setBounds(170, 140, 114, QT102.getPreferredSize().height);

            //---- QT103 ----
            QT103.setName("QT103");
            panel1.add(QT103);
            QT103.setBounds(170, 165, 114, QT103.getPreferredSize().height);

            //---- QT104 ----
            QT104.setName("QT104");
            panel1.add(QT104);
            QT104.setBounds(170, 190, 114, QT104.getPreferredSize().height);

            //---- QT105 ----
            QT105.setName("QT105");
            panel1.add(QT105);
            QT105.setBounds(170, 215, 114, QT105.getPreferredSize().height);

            //---- QT106 ----
            QT106.setName("QT106");
            panel1.add(QT106);
            QT106.setBounds(170, 240, 114, QT106.getPreferredSize().height);

            //---- QT107 ----
            QT107.setName("QT107");
            panel1.add(QT107);
            QT107.setBounds(170, 265, 114, QT107.getPreferredSize().height);

            //---- QT108 ----
            QT108.setName("QT108");
            panel1.add(QT108);
            QT108.setBounds(170, 290, 114, QT108.getPreferredSize().height);

            //---- QT109 ----
            QT109.setName("QT109");
            panel1.add(QT109);
            QT109.setBounds(170, 315, 114, QT109.getPreferredSize().height);

            //---- QT110 ----
            QT110.setName("QT110");
            panel1.add(QT110);
            QT110.setBounds(170, 340, 114, QT110.getPreferredSize().height);

            //---- QT111 ----
            QT111.setName("QT111");
            panel1.add(QT111);
            QT111.setBounds(170, 365, 114, QT111.getPreferredSize().height);

            //---- QT112 ----
            QT112.setName("QT112");
            panel1.add(QT112);
            QT112.setBounds(170, 390, 114, QT112.getPreferredSize().height);

            //---- QT113 ----
            QT113.setName("QT113");
            panel1.add(QT113);
            QT113.setBounds(170, 415, 114, QT113.getPreferredSize().height);

            //---- QT114 ----
            QT114.setName("QT114");
            panel1.add(QT114);
            QT114.setBounds(170, 440, 114, QT114.getPreferredSize().height);

            //---- QT115 ----
            QT115.setName("QT115");
            panel1.add(QT115);
            QT115.setBounds(170, 465, 114, QT115.getPreferredSize().height);

            //---- QT201 ----
            QT201.setName("QT201");
            panel1.add(QT201);
            QT201.setBounds(290, 115, 114, QT201.getPreferredSize().height);

            //---- QT301 ----
            QT301.setName("QT301");
            panel1.add(QT301);
            QT301.setBounds(410, 115, 114, QT301.getPreferredSize().height);

            //---- QT401 ----
            QT401.setName("QT401");
            panel1.add(QT401);
            QT401.setBounds(530, 115, 114, QT401.getPreferredSize().height);

            //---- QT501 ----
            QT501.setName("QT501");
            panel1.add(QT501);
            QT501.setBounds(650, 115, 114, QT501.getPreferredSize().height);

            //---- QT202 ----
            QT202.setName("QT202");
            panel1.add(QT202);
            QT202.setBounds(290, 140, 114, QT202.getPreferredSize().height);

            //---- QT203 ----
            QT203.setName("QT203");
            panel1.add(QT203);
            QT203.setBounds(290, 165, 114, QT203.getPreferredSize().height);

            //---- QT204 ----
            QT204.setName("QT204");
            panel1.add(QT204);
            QT204.setBounds(290, 190, 114, QT204.getPreferredSize().height);

            //---- QT205 ----
            QT205.setName("QT205");
            panel1.add(QT205);
            QT205.setBounds(290, 215, 114, QT205.getPreferredSize().height);

            //---- QT206 ----
            QT206.setName("QT206");
            panel1.add(QT206);
            QT206.setBounds(290, 240, 114, QT206.getPreferredSize().height);

            //---- QT207 ----
            QT207.setName("QT207");
            panel1.add(QT207);
            QT207.setBounds(290, 265, 114, QT207.getPreferredSize().height);

            //---- QT208 ----
            QT208.setName("QT208");
            panel1.add(QT208);
            QT208.setBounds(290, 290, 114, QT208.getPreferredSize().height);

            //---- QT209 ----
            QT209.setName("QT209");
            panel1.add(QT209);
            QT209.setBounds(290, 315, 114, QT209.getPreferredSize().height);

            //---- QT210 ----
            QT210.setName("QT210");
            panel1.add(QT210);
            QT210.setBounds(290, 340, 114, QT210.getPreferredSize().height);

            //---- QT211 ----
            QT211.setName("QT211");
            panel1.add(QT211);
            QT211.setBounds(290, 365, 114, QT211.getPreferredSize().height);

            //---- QT212 ----
            QT212.setName("QT212");
            panel1.add(QT212);
            QT212.setBounds(290, 390, 114, QT212.getPreferredSize().height);

            //---- QT213 ----
            QT213.setName("QT213");
            panel1.add(QT213);
            QT213.setBounds(290, 415, 114, QT213.getPreferredSize().height);

            //---- QT214 ----
            QT214.setName("QT214");
            panel1.add(QT214);
            QT214.setBounds(290, 440, 114, QT214.getPreferredSize().height);

            //---- QT215 ----
            QT215.setName("QT215");
            panel1.add(QT215);
            QT215.setBounds(290, 465, 114, QT215.getPreferredSize().height);

            //---- QT302 ----
            QT302.setName("QT302");
            panel1.add(QT302);
            QT302.setBounds(410, 140, 114, QT302.getPreferredSize().height);

            //---- QT303 ----
            QT303.setName("QT303");
            panel1.add(QT303);
            QT303.setBounds(410, 165, 114, QT303.getPreferredSize().height);

            //---- QT304 ----
            QT304.setName("QT304");
            panel1.add(QT304);
            QT304.setBounds(410, 190, 114, QT304.getPreferredSize().height);

            //---- QT305 ----
            QT305.setName("QT305");
            panel1.add(QT305);
            QT305.setBounds(410, 215, 114, QT305.getPreferredSize().height);

            //---- QT306 ----
            QT306.setName("QT306");
            panel1.add(QT306);
            QT306.setBounds(410, 240, 114, QT306.getPreferredSize().height);

            //---- QT307 ----
            QT307.setName("QT307");
            panel1.add(QT307);
            QT307.setBounds(410, 265, 114, QT307.getPreferredSize().height);

            //---- QT308 ----
            QT308.setName("QT308");
            panel1.add(QT308);
            QT308.setBounds(410, 290, 114, QT308.getPreferredSize().height);

            //---- QT309 ----
            QT309.setName("QT309");
            panel1.add(QT309);
            QT309.setBounds(410, 315, 114, QT309.getPreferredSize().height);

            //---- QT310 ----
            QT310.setName("QT310");
            panel1.add(QT310);
            QT310.setBounds(410, 340, 114, QT310.getPreferredSize().height);

            //---- QT311 ----
            QT311.setName("QT311");
            panel1.add(QT311);
            QT311.setBounds(410, 365, 114, QT311.getPreferredSize().height);

            //---- QT312 ----
            QT312.setName("QT312");
            panel1.add(QT312);
            QT312.setBounds(410, 390, 114, QT312.getPreferredSize().height);

            //---- QT313 ----
            QT313.setName("QT313");
            panel1.add(QT313);
            QT313.setBounds(410, 415, 114, QT313.getPreferredSize().height);

            //---- QT314 ----
            QT314.setName("QT314");
            panel1.add(QT314);
            QT314.setBounds(410, 440, 114, QT314.getPreferredSize().height);

            //---- QT315 ----
            QT315.setName("QT315");
            panel1.add(QT315);
            QT315.setBounds(410, 465, 114, QT315.getPreferredSize().height);

            //---- QT402 ----
            QT402.setName("QT402");
            panel1.add(QT402);
            QT402.setBounds(530, 140, 114, QT402.getPreferredSize().height);

            //---- QT403 ----
            QT403.setName("QT403");
            panel1.add(QT403);
            QT403.setBounds(530, 165, 114, QT403.getPreferredSize().height);

            //---- QT404 ----
            QT404.setName("QT404");
            panel1.add(QT404);
            QT404.setBounds(530, 190, 114, QT404.getPreferredSize().height);

            //---- QT405 ----
            QT405.setName("QT405");
            panel1.add(QT405);
            QT405.setBounds(530, 215, 114, QT405.getPreferredSize().height);

            //---- QT406 ----
            QT406.setName("QT406");
            panel1.add(QT406);
            QT406.setBounds(530, 240, 114, QT406.getPreferredSize().height);

            //---- QT407 ----
            QT407.setName("QT407");
            panel1.add(QT407);
            QT407.setBounds(530, 265, 114, QT407.getPreferredSize().height);

            //---- QT408 ----
            QT408.setName("QT408");
            panel1.add(QT408);
            QT408.setBounds(530, 290, 114, QT408.getPreferredSize().height);

            //---- QT409 ----
            QT409.setName("QT409");
            panel1.add(QT409);
            QT409.setBounds(530, 315, 114, QT409.getPreferredSize().height);

            //---- QT410 ----
            QT410.setName("QT410");
            panel1.add(QT410);
            QT410.setBounds(530, 340, 114, QT410.getPreferredSize().height);

            //---- QT411 ----
            QT411.setName("QT411");
            panel1.add(QT411);
            QT411.setBounds(530, 365, 114, QT411.getPreferredSize().height);

            //---- QT412 ----
            QT412.setName("QT412");
            panel1.add(QT412);
            QT412.setBounds(530, 390, 114, QT412.getPreferredSize().height);

            //---- QT413 ----
            QT413.setName("QT413");
            panel1.add(QT413);
            QT413.setBounds(530, 415, 114, QT413.getPreferredSize().height);

            //---- QT414 ----
            QT414.setName("QT414");
            panel1.add(QT414);
            QT414.setBounds(530, 440, 114, QT414.getPreferredSize().height);

            //---- QT415 ----
            QT415.setName("QT415");
            panel1.add(QT415);
            QT415.setBounds(530, 465, 114, QT415.getPreferredSize().height);

            //---- QT502 ----
            QT502.setName("QT502");
            panel1.add(QT502);
            QT502.setBounds(650, 140, 114, QT502.getPreferredSize().height);

            //---- QT503 ----
            QT503.setName("QT503");
            panel1.add(QT503);
            QT503.setBounds(650, 165, 114, QT503.getPreferredSize().height);

            //---- QT504 ----
            QT504.setName("QT504");
            panel1.add(QT504);
            QT504.setBounds(650, 190, 114, QT504.getPreferredSize().height);

            //---- QT505 ----
            QT505.setName("QT505");
            panel1.add(QT505);
            QT505.setBounds(650, 215, 114, QT505.getPreferredSize().height);

            //---- QT506 ----
            QT506.setName("QT506");
            panel1.add(QT506);
            QT506.setBounds(650, 240, 114, QT506.getPreferredSize().height);

            //---- QT507 ----
            QT507.setName("QT507");
            panel1.add(QT507);
            QT507.setBounds(650, 265, 114, QT507.getPreferredSize().height);

            //---- QT508 ----
            QT508.setName("QT508");
            panel1.add(QT508);
            QT508.setBounds(650, 290, 114, QT508.getPreferredSize().height);

            //---- QT509 ----
            QT509.setName("QT509");
            panel1.add(QT509);
            QT509.setBounds(650, 315, 114, QT509.getPreferredSize().height);

            //---- QT510 ----
            QT510.setName("QT510");
            panel1.add(QT510);
            QT510.setBounds(650, 340, 114, QT510.getPreferredSize().height);

            //---- QT511 ----
            QT511.setName("QT511");
            panel1.add(QT511);
            QT511.setBounds(650, 365, 114, QT511.getPreferredSize().height);

            //---- QT512 ----
            QT512.setName("QT512");
            panel1.add(QT512);
            QT512.setBounds(650, 390, 114, QT512.getPreferredSize().height);

            //---- QT513 ----
            QT513.setName("QT513");
            panel1.add(QT513);
            QT513.setBounds(650, 415, 114, QT513.getPreferredSize().height);

            //---- QT514 ----
            QT514.setName("QT514");
            panel1.add(QT514);
            QT514.setBounds(650, 440, 114, QT514.getPreferredSize().height);

            //---- QT515 ----
            QT515.setName("QT515");
            panel1.add(QT515);
            QT515.setBounds(650, 465, 114, QT515.getPreferredSize().height);

            //---- OBJ_38 ----
            OBJ_38.setText("Totaux");
            OBJ_38.setFont(OBJ_38.getFont().deriveFont(OBJ_38.getFont().getStyle() | Font.BOLD, OBJ_38.getFont().getSize() + 3f));
            OBJ_38.setName("OBJ_38");
            panel1.add(OBJ_38);
            OBJ_38.setBounds(60, 500, 110, 25);

            //---- TOT1 ----
            TOT1.setComponentPopupMenu(BTD);
            TOT1.setText("@TOT1@");
            TOT1.setName("TOT1");
            panel1.add(TOT1);
            TOT1.setBounds(170, 500, 114, TOT1.getPreferredSize().height);

            //---- TOT2 ----
            TOT2.setComponentPopupMenu(BTD);
            TOT2.setText("@TOT2@");
            TOT2.setName("TOT2");
            panel1.add(TOT2);
            TOT2.setBounds(290, 500, 114, TOT2.getPreferredSize().height);

            //---- TOT3 ----
            TOT3.setComponentPopupMenu(BTD);
            TOT3.setText("@TOT3@");
            TOT3.setName("TOT3");
            panel1.add(TOT3);
            TOT3.setBounds(410, 500, 114, TOT3.getPreferredSize().height);

            //---- TOT4 ----
            TOT4.setComponentPopupMenu(BTD);
            TOT4.setText("@TOT4@");
            TOT4.setName("TOT4");
            panel1.add(TOT4);
            TOT4.setBounds(530, 500, 114, TOT4.getPreferredSize().height);

            //---- TOT5 ----
            TOT5.setComponentPopupMenu(BTD);
            TOT5.setText("@TOT5@");
            TOT5.setName("TOT5");
            panel1.add(TOT5);
            TOT5.setBounds(650, 500, 114, TOT5.getPreferredSize().height);

            //---- TOT6 ----
            TOT6.setComponentPopupMenu(BTD);
            TOT6.setText("@TOT6@");
            TOT6.setFont(TOT6.getFont().deriveFont(TOT6.getFont().getStyle() | Font.BOLD));
            TOT6.setName("TOT6");
            panel1.add(TOT6);
            TOT6.setBounds(775, 500, 114, TOT6.getPreferredSize().height);

            //---- TOT02 ----
            TOT02.setText("@TOT02@");
            TOT02.setName("TOT02");
            panel1.add(TOT02);
            TOT02.setBounds(775, 145, 114, TOT02.getPreferredSize().height);

            //---- TOT03 ----
            TOT03.setText("@TOT03@");
            TOT03.setName("TOT03");
            panel1.add(TOT03);
            TOT03.setBounds(775, 170, 114, TOT03.getPreferredSize().height);

            //---- TOT04 ----
            TOT04.setText("@TOT04@");
            TOT04.setName("TOT04");
            panel1.add(TOT04);
            TOT04.setBounds(775, 195, 114, TOT04.getPreferredSize().height);

            //---- TOT05 ----
            TOT05.setText("@TOT05@");
            TOT05.setName("TOT05");
            panel1.add(TOT05);
            TOT05.setBounds(775, 220, 114, TOT05.getPreferredSize().height);

            //---- TOT06 ----
            TOT06.setText("@TOT06@");
            TOT06.setName("TOT06");
            panel1.add(TOT06);
            TOT06.setBounds(775, 245, 114, TOT06.getPreferredSize().height);

            //---- TOT07 ----
            TOT07.setText("@TOT07@");
            TOT07.setName("TOT07");
            panel1.add(TOT07);
            TOT07.setBounds(775, 270, 114, TOT07.getPreferredSize().height);

            //---- TOT08 ----
            TOT08.setText("@TOT08@");
            TOT08.setName("TOT08");
            panel1.add(TOT08);
            TOT08.setBounds(775, 295, 114, TOT08.getPreferredSize().height);

            //---- TOT09 ----
            TOT09.setText("@TOT09@");
            TOT09.setName("TOT09");
            panel1.add(TOT09);
            TOT09.setBounds(775, 320, 114, TOT09.getPreferredSize().height);

            //---- TOT10 ----
            TOT10.setText("@TOT10@");
            TOT10.setName("TOT10");
            panel1.add(TOT10);
            TOT10.setBounds(775, 345, 114, TOT10.getPreferredSize().height);

            //---- TOT11 ----
            TOT11.setText("@TOT11@");
            TOT11.setName("TOT11");
            panel1.add(TOT11);
            TOT11.setBounds(775, 370, 114, TOT11.getPreferredSize().height);

            //---- TOT12 ----
            TOT12.setText("@TOT12@");
            TOT12.setName("TOT12");
            panel1.add(TOT12);
            TOT12.setBounds(775, 395, 114, TOT12.getPreferredSize().height);

            //---- TOT13 ----
            TOT13.setText("@TOT13@");
            TOT13.setName("TOT13");
            panel1.add(TOT13);
            TOT13.setBounds(775, 420, 114, TOT13.getPreferredSize().height);

            //---- TOT14 ----
            TOT14.setText("@TOT14@");
            TOT14.setName("TOT14");
            panel1.add(TOT14);
            TOT14.setBounds(775, 445, 114, TOT14.getPreferredSize().height);

            //---- TOT15 ----
            TOT15.setText("@TOT15@");
            TOT15.setName("TOT15");
            panel1.add(TOT15);
            TOT15.setBounds(775, 470, 114, TOT15.getPreferredSize().height);

            //---- OBJ_44 ----
            OBJ_44.setText("Quantit\u00e9s totales");
            OBJ_44.setName("OBJ_44");
            panel1.add(OBJ_44);
            OBJ_44.setBounds(775, 90, 105, 25);

            //---- OBJ_45 ----
            OBJ_45.setText("N\u00b0");
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(170, 55, 25, 25);

            //---- OBJ_46 ----
            OBJ_46.setText("N\u00b0");
            OBJ_46.setName("OBJ_46");
            panel1.add(OBJ_46);
            OBJ_46.setBounds(290, 55, 25, 25);

            //---- OBJ_47 ----
            OBJ_47.setText("N\u00b0");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(410, 55, 25, 25);

            //---- OBJ_48 ----
            OBJ_48.setText("N\u00b0");
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(530, 55, 25, 25);

            //---- OBJ_49 ----
            OBJ_49.setText("N\u00b0");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(650, 55, 25, 25);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(900, 120, 25, 175);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(900, 318, 25, 176);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 944, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_26;
  private XRiTextField INDETB;
  private JLabel OBJ_28;
  private XRiTextField INDMAG;
  private JLabel OBJ_64;
  private XRiTextField INDAR0;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_40;
  private XRiTextField A1FAM;
  private JLabel OBJ_41;
  private XRiTextField A1CL1;
  private JLabel OBJ_42;
  private XRiTextField A1CL2;
  private JLabel OBJ_55;
  private JTextField A1UNS;
  private XRiCalendrier DAT01;
  private XRiCalendrier DAT02;
  private XRiCalendrier DAT03;
  private XRiCalendrier DAT04;
  private XRiCalendrier DAT05;
  private XRiCalendrier DAT06;
  private XRiCalendrier DAT07;
  private XRiCalendrier DAT08;
  private XRiCalendrier DAT09;
  private XRiCalendrier DAT10;
  private XRiCalendrier DAT11;
  private XRiCalendrier DAT12;
  private XRiCalendrier DAT13;
  private XRiCalendrier DAT14;
  private XRiCalendrier DAT15;
  private JLabel OBJ_43;
  private RiZoneSortie FRS1;
  private RiZoneSortie FRS2;
  private RiZoneSortie FRS3;
  private RiZoneSortie FRS4;
  private RiZoneSortie FRS5;
  private RiZoneSortie FRN1;
  private RiZoneSortie FRN2;
  private RiZoneSortie FRN3;
  private RiZoneSortie FRN4;
  private RiZoneSortie FRN5;
  private RiZoneSortie TOT01;
  private XRiTextField QT101;
  private XRiTextField QT102;
  private XRiTextField QT103;
  private XRiTextField QT104;
  private XRiTextField QT105;
  private XRiTextField QT106;
  private XRiTextField QT107;
  private XRiTextField QT108;
  private XRiTextField QT109;
  private XRiTextField QT110;
  private XRiTextField QT111;
  private XRiTextField QT112;
  private XRiTextField QT113;
  private XRiTextField QT114;
  private XRiTextField QT115;
  private XRiTextField QT201;
  private XRiTextField QT301;
  private XRiTextField QT401;
  private XRiTextField QT501;
  private XRiTextField QT202;
  private XRiTextField QT203;
  private XRiTextField QT204;
  private XRiTextField QT205;
  private XRiTextField QT206;
  private XRiTextField QT207;
  private XRiTextField QT208;
  private XRiTextField QT209;
  private XRiTextField QT210;
  private XRiTextField QT211;
  private XRiTextField QT212;
  private XRiTextField QT213;
  private XRiTextField QT214;
  private XRiTextField QT215;
  private XRiTextField QT302;
  private XRiTextField QT303;
  private XRiTextField QT304;
  private XRiTextField QT305;
  private XRiTextField QT306;
  private XRiTextField QT307;
  private XRiTextField QT308;
  private XRiTextField QT309;
  private XRiTextField QT310;
  private XRiTextField QT311;
  private XRiTextField QT312;
  private XRiTextField QT313;
  private XRiTextField QT314;
  private XRiTextField QT315;
  private XRiTextField QT402;
  private XRiTextField QT403;
  private XRiTextField QT404;
  private XRiTextField QT405;
  private XRiTextField QT406;
  private XRiTextField QT407;
  private XRiTextField QT408;
  private XRiTextField QT409;
  private XRiTextField QT410;
  private XRiTextField QT411;
  private XRiTextField QT412;
  private XRiTextField QT413;
  private XRiTextField QT414;
  private XRiTextField QT415;
  private XRiTextField QT502;
  private XRiTextField QT503;
  private XRiTextField QT504;
  private XRiTextField QT505;
  private XRiTextField QT506;
  private XRiTextField QT507;
  private XRiTextField QT508;
  private XRiTextField QT509;
  private XRiTextField QT510;
  private XRiTextField QT511;
  private XRiTextField QT512;
  private XRiTextField QT513;
  private XRiTextField QT514;
  private XRiTextField QT515;
  private JLabel OBJ_38;
  private RiZoneSortie TOT1;
  private RiZoneSortie TOT2;
  private RiZoneSortie TOT3;
  private RiZoneSortie TOT4;
  private RiZoneSortie TOT5;
  private RiZoneSortie TOT6;
  private RiZoneSortie TOT02;
  private RiZoneSortie TOT03;
  private RiZoneSortie TOT04;
  private RiZoneSortie TOT05;
  private RiZoneSortie TOT06;
  private RiZoneSortie TOT07;
  private RiZoneSortie TOT08;
  private RiZoneSortie TOT09;
  private RiZoneSortie TOT10;
  private RiZoneSortie TOT11;
  private RiZoneSortie TOT12;
  private RiZoneSortie TOT13;
  private RiZoneSortie TOT14;
  private RiZoneSortie TOT15;
  private JLabel OBJ_44;
  private JLabel OBJ_45;
  private JLabel OBJ_46;
  private JLabel OBJ_47;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
