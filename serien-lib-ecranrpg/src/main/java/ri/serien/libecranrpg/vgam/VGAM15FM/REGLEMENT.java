
package ri.serien.libecranrpg.vgam.VGAM15FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.PopupPerso;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class REGLEMENT extends SNPanelEcranRPG implements ioFrame {
   
  private PopupPerso popupPerso = new PopupPerso(this);  
  
  public REGLEMENT(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    setCloseKey("ENTER", "F12");
    // Initialisation particulière dédiée à la popup perso
    setPopupPerso(popupPerso);
    popupPerso.ajouterProprieteTouche("ENTER", true, true, false);
    popupPerso.ajouterProprieteTouche("F4", true, true, true);
    popupPerso.ajouterProprieteTouche("F12", false, true, false);

    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LRG1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRG1@")).trim());
    LRG2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRG2@")).trim());
    LRG3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRG3@")).trim());
    LRG4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRG4@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // icones
    bouton_valider.setIcon(lexique.chargerImage("images/ok_p.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    popupPerso.traiterTouche("ENTER");
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    popupPerso.traiterTouche("F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    P_Centre = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    EARG1 = new XRiTextField();
    label5 = new JLabel();
    label6 = new JLabel();
    EARG2 = new XRiTextField();
    EARG3 = new XRiTextField();
    EARG4 = new XRiTextField();
    EAEC1 = new XRiTextField();
    label3 = new JLabel();
    label4 = new JLabel();
    EAEC2 = new XRiTextField();
    EAEC3 = new XRiTextField();
    EAEC4 = new XRiTextField();
    EADTE1 = new XRiCalendrier();
    EADTE2 = new XRiCalendrier();
    EADTE3 = new XRiCalendrier();
    EADTE4 = new XRiCalendrier();
    label7 = new JLabel();
    EAPC1 = new XRiTextField();
    EAPC2 = new XRiTextField();
    EAPC3 = new XRiTextField();
    EAPC4 = new XRiTextField();
    label8 = new JLabel();
    MRG1 = new XRiTextField();
    MRG2 = new XRiTextField();
    MRG3 = new XRiTextField();
    MRG4 = new XRiTextField();
    LRG1 = new RiZoneSortie();
    LRG2 = new RiZoneSortie();
    LRG3 = new RiZoneSortie();
    LRG4 = new RiZoneSortie();
    label9 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(950, 240));
    setPreferredSize(new Dimension(950, 240));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_menus ========
    {
      p_menus.setPreferredSize(new Dimension(170, 0));
      p_menus.setMinimumSize(new Dimension(170, 0));
      p_menus.setBackground(new Color(238, 239, 241));
      p_menus.setBorder(LineBorder.createGrayLineBorder());
      p_menus.setName("p_menus");
      p_menus.setLayout(new BorderLayout());

      //======== menus_bas ========
      {
        menus_bas.setOpaque(false);
        menus_bas.setBackground(new Color(238, 239, 241));
        menus_bas.setName("menus_bas");
        menus_bas.setLayout(new VerticalLayout());

        //======== navig_valid ========
        {
          navig_valid.setName("navig_valid");

          //---- bouton_valider ----
          bouton_valider.setText("Valider");
          bouton_valider.setToolTipText("Valider");
          bouton_valider.setName("bouton_valider");
          bouton_valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_validerActionPerformed(e);
            }
          });
          navig_valid.add(bouton_valider);
        }
        menus_bas.add(navig_valid);

        //======== navig_retour ========
        {
          navig_retour.setName("navig_retour");

          //---- bouton_retour ----
          bouton_retour.setText("Retour");
          bouton_retour.setToolTipText("Retour");
          bouton_retour.setName("bouton_retour");
          bouton_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_retourActionPerformed(e);
            }
          });
          navig_retour.add(bouton_retour);
        }
        menus_bas.add(navig_retour);
      }
      p_menus.add(menus_bas, BorderLayout.SOUTH);
    }
    add(p_menus, BorderLayout.EAST);

    //======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("R\u00e9glements"));
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- label1 ----
        label1.setText("1");
        label1.setHorizontalAlignment(SwingConstants.CENTER);
        label1.setName("label1");
        panel1.add(label1);
        label1.setBounds(15, 49, 15, 20);

        //---- label2 ----
        label2.setText("2");
        label2.setHorizontalAlignment(SwingConstants.CENTER);
        label2.setName("label2");
        panel1.add(label2);
        label2.setBounds(15, 79, 15, 20);

        //---- EARG1 ----
        EARG1.setHorizontalAlignment(SwingConstants.RIGHT);
        EARG1.setComponentPopupMenu(BTD);
        EARG1.setName("EARG1");
        panel1.add(EARG1);
        EARG1.setBounds(40, 45, 34, EARG1.getPreferredSize().height);

        //---- label5 ----
        label5.setText("3");
        label5.setHorizontalAlignment(SwingConstants.CENTER);
        label5.setName("label5");
        panel1.add(label5);
        label5.setBounds(15, 109, 15, 20);

        //---- label6 ----
        label6.setText("4");
        label6.setHorizontalAlignment(SwingConstants.CENTER);
        label6.setName("label6");
        panel1.add(label6);
        label6.setBounds(15, 139, 15, 20);

        //---- EARG2 ----
        EARG2.setHorizontalAlignment(SwingConstants.RIGHT);
        EARG2.setComponentPopupMenu(BTD);
        EARG2.setName("EARG2");
        panel1.add(EARG2);
        EARG2.setBounds(40, 75, 34, 28);

        //---- EARG3 ----
        EARG3.setHorizontalAlignment(SwingConstants.RIGHT);
        EARG3.setComponentPopupMenu(BTD);
        EARG3.setName("EARG3");
        panel1.add(EARG3);
        EARG3.setBounds(40, 105, 34, 28);

        //---- EARG4 ----
        EARG4.setHorizontalAlignment(SwingConstants.RIGHT);
        EARG4.setComponentPopupMenu(BTD);
        EARG4.setName("EARG4");
        panel1.add(EARG4);
        EARG4.setBounds(40, 135, 34, 28);

        //---- EAEC1 ----
        EAEC1.setHorizontalAlignment(SwingConstants.RIGHT);
        EAEC1.setComponentPopupMenu(BTD);
        EAEC1.setName("EAEC1");
        panel1.add(EAEC1);
        EAEC1.setBounds(74, 45, 34, 28);

        //---- label3 ----
        label3.setText("Mod");
        label3.setName("label3");
        panel1.add(label3);
        label3.setBounds(40, 30, 35, 16);

        //---- label4 ----
        label4.setText("Ech");
        label4.setName("label4");
        panel1.add(label4);
        label4.setBounds(77, 30, 23, 16);

        //---- EAEC2 ----
        EAEC2.setHorizontalAlignment(SwingConstants.RIGHT);
        EAEC2.setComponentPopupMenu(BTD);
        EAEC2.setName("EAEC2");
        panel1.add(EAEC2);
        EAEC2.setBounds(74, 75, 34, 28);

        //---- EAEC3 ----
        EAEC3.setHorizontalAlignment(SwingConstants.RIGHT);
        EAEC3.setComponentPopupMenu(BTD);
        EAEC3.setName("EAEC3");
        panel1.add(EAEC3);
        EAEC3.setBounds(74, 105, 34, 28);

        //---- EAEC4 ----
        EAEC4.setHorizontalAlignment(SwingConstants.RIGHT);
        EAEC4.setComponentPopupMenu(BTD);
        EAEC4.setName("EAEC4");
        panel1.add(EAEC4);
        EAEC4.setBounds(74, 135, 34, 28);

        //---- EADTE1 ----
        EADTE1.setName("EADTE1");
        panel1.add(EADTE1);
        EADTE1.setBounds(108, 45, 105, EADTE1.getPreferredSize().height);

        //---- EADTE2 ----
        EADTE2.setName("EADTE2");
        panel1.add(EADTE2);
        EADTE2.setBounds(109, 75, 105, 28);

        //---- EADTE3 ----
        EADTE3.setName("EADTE3");
        panel1.add(EADTE3);
        EADTE3.setBounds(109, 105, 105, 28);

        //---- EADTE4 ----
        EADTE4.setName("EADTE4");
        panel1.add(EADTE4);
        EADTE4.setBounds(109, 135, 105, 28);

        //---- label7 ----
        label7.setText("Date \u00e9ch\u00e9ance");
        label7.setName("label7");
        panel1.add(label7);
        label7.setBounds(109, 30, 85, 16);

        //---- EAPC1 ----
        EAPC1.setName("EAPC1");
        panel1.add(EAPC1);
        EAPC1.setBounds(215, 45, 34, EAPC1.getPreferredSize().height);

        //---- EAPC2 ----
        EAPC2.setName("EAPC2");
        panel1.add(EAPC2);
        EAPC2.setBounds(215, 75, 34, 28);

        //---- EAPC3 ----
        EAPC3.setName("EAPC3");
        panel1.add(EAPC3);
        EAPC3.setBounds(215, 105, 34, 28);

        //---- EAPC4 ----
        EAPC4.setName("EAPC4");
        panel1.add(EAPC4);
        EAPC4.setBounds(215, 135, 34, 28);

        //---- label8 ----
        label8.setText("%");
        label8.setName("label8");
        panel1.add(label8);
        label8.setBounds(225, 30, 20, 16);

        //---- MRG1 ----
        MRG1.setHorizontalAlignment(SwingConstants.RIGHT);
        MRG1.setName("MRG1");
        panel1.add(MRG1);
        MRG1.setBounds(250, 45, 140, MRG1.getPreferredSize().height);

        //---- MRG2 ----
        MRG2.setHorizontalAlignment(SwingConstants.RIGHT);
        MRG2.setName("MRG2");
        panel1.add(MRG2);
        MRG2.setBounds(250, 75, 140, 28);

        //---- MRG3 ----
        MRG3.setHorizontalAlignment(SwingConstants.RIGHT);
        MRG3.setName("MRG3");
        panel1.add(MRG3);
        MRG3.setBounds(250, 105, 140, 28);

        //---- MRG4 ----
        MRG4.setHorizontalAlignment(SwingConstants.RIGHT);
        MRG4.setName("MRG4");
        panel1.add(MRG4);
        MRG4.setBounds(250, 135, 140, 28);

        //---- LRG1 ----
        LRG1.setText("@LRG1@");
        LRG1.setName("LRG1");
        panel1.add(LRG1);
        LRG1.setBounds(395, 47, 320, LRG1.getPreferredSize().height);

        //---- LRG2 ----
        LRG2.setText("@LRG2@");
        LRG2.setName("LRG2");
        panel1.add(LRG2);
        LRG2.setBounds(395, 77, 320, LRG2.getPreferredSize().height);

        //---- LRG3 ----
        LRG3.setText("@LRG3@");
        LRG3.setName("LRG3");
        panel1.add(LRG3);
        LRG3.setBounds(395, 107, 320, LRG3.getPreferredSize().height);

        //---- LRG4 ----
        LRG4.setText("@LRG4@");
        LRG4.setName("LRG4");
        panel1.add(LRG4);
        LRG4.setBounds(395, 137, 320, LRG4.getPreferredSize().height);

        //---- label9 ----
        label9.setText("Montant");
        label9.setName("label9");
        panel1.add(label9);
        label9.setBounds(255, 30, 60, 16);
      }
      P_Centre.add(panel1);
      panel1.setBounds(15, 10, 735, 190);
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel P_Centre;
  private JPanel panel1;
  private JLabel label1;
  private JLabel label2;
  private XRiTextField EARG1;
  private JLabel label5;
  private JLabel label6;
  private XRiTextField EARG2;
  private XRiTextField EARG3;
  private XRiTextField EARG4;
  private XRiTextField EAEC1;
  private JLabel label3;
  private JLabel label4;
  private XRiTextField EAEC2;
  private XRiTextField EAEC3;
  private XRiTextField EAEC4;
  private XRiCalendrier EADTE1;
  private XRiCalendrier EADTE2;
  private XRiCalendrier EADTE3;
  private XRiCalendrier EADTE4;
  private JLabel label7;
  private XRiTextField EAPC1;
  private XRiTextField EAPC2;
  private XRiTextField EAPC3;
  private XRiTextField EAPC4;
  private JLabel label8;
  private XRiTextField MRG1;
  private XRiTextField MRG2;
  private XRiTextField MRG3;
  private XRiTextField MRG4;
  private RiZoneSortie LRG1;
  private RiZoneSortie LRG2;
  private RiZoneSortie LRG3;
  private RiZoneSortie LRG4;
  private JLabel label9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
