//$$david$$ ££15/12/10££ -> nouveau de A à Z

package ri.serien.libecranrpg.vgam.VGAM35FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class OPTION_PANEL extends SNPanelEcranRPG implements ioFrame {
   
  
  public OPTION_PANEL(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bt_valider);
    
    bt_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    bt_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    setTitle("Calcul prix de vente");
    
    initDiverses();
    R0TYP.setValeurs("V", R0TYP_GRP);
    R0TYP_01.setValeurs("C");
    R0TYP_02.setValeurs("W");
    R0TYP_03.setValeurs("N");
    R0TYP_04.setValeurs("M");
    R0TYP_05.setValeurs("A");
    R0TYP_06.setValeurs("1");
    R0TYP_07.setValeurs("2");
    R0TYP_08.setValeurs("3");
    R0TYP_09.setValeurs("4");
    R0TYP_10.setValeurs("5");
    R0TYP_11.setValeurs("6");
    R0TYP_12.setValeurs("7");
    R0TYP_13.setValeurs("8");
    R0TYP_14.setValeurs("9");
    R0TYP_15.setValeurs("P");
    R0TYP_16.setValeurs("Q");
    R0TYP_17.setValeurs("R");
    R0TYP_18.setValeurs("D");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // On cache le bouton valider en Consult
    riMenu1.setVisible(!(lexique.getMode() == Lexical.MODE_CONSULTATION));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bt_validerActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
  }
  
  private void bt_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(false);
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    pnl_menus = new JPanel();
    panel1 = new JPanel();
    riMenu1 = new RiMenu();
    bt_valider = new RiMenu_bt();
    riMenu2 = new RiMenu();
    bt_retour = new RiMenu_bt();
    pnl_contenu = new JPanel();
    panel2 = new JPanel();
    R0TYP = new XRiRadioButton();
    R0TYP_01 = new XRiRadioButton();
    R0TYP_02 = new XRiRadioButton();
    R0TYP_03 = new XRiRadioButton();
    R0TYP_04 = new XRiRadioButton();
    R0TYP_05 = new XRiRadioButton();
    R0TYP_06 = new XRiRadioButton();
    R0TYP_07 = new XRiRadioButton();
    R0TYP_08 = new XRiRadioButton();
    R0TYP_09 = new XRiRadioButton();
    R0TYP_10 = new XRiRadioButton();
    R0TYP_11 = new XRiRadioButton();
    R0TYP_12 = new XRiRadioButton();
    R0TYP_13 = new XRiRadioButton();
    R0TYP_14 = new XRiRadioButton();
    panel3 = new JPanel();
    R0TYP_15 = new XRiRadioButton();
    R0TYP_16 = new XRiRadioButton();
    R0TYP_17 = new XRiRadioButton();
    R0TYP_18 = new XRiRadioButton();
    R0TYP_GRP = new ButtonGroup();

    //======== this ========
    setName("this");
    setLayout(new BorderLayout());

    //======== pnl_menus ========
    {
      pnl_menus.setPreferredSize(new Dimension(170, 0));
      pnl_menus.setMinimumSize(new Dimension(170, 0));
      pnl_menus.setBorder(new LineBorder(Color.lightGray));
      pnl_menus.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
      pnl_menus.setBackground(new Color(238, 239, 241));
      pnl_menus.setName("pnl_menus");
      pnl_menus.setLayout(new BorderLayout());

      //======== panel1 ========
      {
        panel1.setName("panel1");
        panel1.setLayout(new VerticalLayout());

        //======== riMenu1 ========
        {
          riMenu1.setName("riMenu1");

          //---- bt_valider ----
          bt_valider.setText("Valider");
          bt_valider.setName("bt_valider");
          bt_valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_validerActionPerformed(e);
            }
          });
          riMenu1.add(bt_valider);
        }
        panel1.add(riMenu1);

        //======== riMenu2 ========
        {
          riMenu2.setName("riMenu2");

          //---- bt_retour ----
          bt_retour.setText("Retour");
          bt_retour.setName("bt_retour");
          bt_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_retourActionPerformed(e);
            }
          });
          riMenu2.add(bt_retour);
        }
        panel1.add(riMenu2);
      }
      pnl_menus.add(panel1, BorderLayout.SOUTH);
    }
    add(pnl_menus, BorderLayout.EAST);

    //======== pnl_contenu ========
    {
      pnl_contenu.setBackground(new Color(238, 238, 210));
      pnl_contenu.setName("pnl_contenu");

      //======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setBorder(new TitledBorder("Mise \u00e0 jour depuis condition d'achat"));
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- R0TYP ----
        R0TYP.setText("Prix catalogue x coeff.");
        R0TYP.setName("R0TYP");
        panel2.add(R0TYP);
        R0TYP.setBounds(25, 35, 265, R0TYP.getPreferredSize().height);

        //---- R0TYP_01 ----
        R0TYP_01.setText("Prix catalogue seul");
        R0TYP_01.setName("R0TYP_01");
        panel2.add(R0TYP_01);
        R0TYP_01.setBounds(25, 58, 265, 18);

        //---- R0TYP_02 ----
        R0TYP_02.setText("Prix catalogue x coeff. d'approche x coeff.");
        R0TYP_02.setName("R0TYP_02");
        panel2.add(R0TYP_02);
        R0TYP_02.setBounds(25, 81, 265, 18);

        //---- R0TYP_03 ----
        R0TYP_03.setText("Prix net d'achat x coeff.");
        R0TYP_03.setName("R0TYP_03");
        panel2.add(R0TYP_03);
        R0TYP_03.setBounds(25, 104, 265, 18);

        //---- R0TYP_04 ----
        R0TYP_04.setText("Prix net x coeff. d'approche moyen x coeff.");
        R0TYP_04.setName("R0TYP_04");
        panel2.add(R0TYP_04);
        R0TYP_04.setBounds(25, 127, 265, 18);

        //---- R0TYP_05 ----
        R0TYP_05.setText("Prix net x Coeff. d'approche saisi x coeff.");
        R0TYP_05.setName("R0TYP_05");
        panel2.add(R0TYP_05);
        R0TYP_05.setBounds(25, 150, 265, 18);

        //---- R0TYP_06 ----
        R0TYP_06.setText("1er prix quantitatif");
        R0TYP_06.setName("R0TYP_06");
        panel2.add(R0TYP_06);
        R0TYP_06.setBounds(25, 173, 265, 18);

        //---- R0TYP_07 ----
        R0TYP_07.setText("2\u00e8me prix quantitatif");
        R0TYP_07.setName("R0TYP_07");
        panel2.add(R0TYP_07);
        R0TYP_07.setBounds(25, 196, 265, 18);

        //---- R0TYP_08 ----
        R0TYP_08.setText("3\u00e8me prix quantitatif");
        R0TYP_08.setName("R0TYP_08");
        panel2.add(R0TYP_08);
        R0TYP_08.setBounds(25, 219, 265, 18);

        //---- R0TYP_09 ----
        R0TYP_09.setText("4\u00e8me prix quantitatif");
        R0TYP_09.setName("R0TYP_09");
        panel2.add(R0TYP_09);
        R0TYP_09.setBounds(25, 242, 265, 18);

        //---- R0TYP_10 ----
        R0TYP_10.setText("5\u00e8me prix quantitatif");
        R0TYP_10.setName("R0TYP_10");
        panel2.add(R0TYP_10);
        R0TYP_10.setBounds(25, 265, 265, 18);

        //---- R0TYP_11 ----
        R0TYP_11.setText("6\u00e8me prix quantitatif");
        R0TYP_11.setName("R0TYP_11");
        panel2.add(R0TYP_11);
        R0TYP_11.setBounds(25, 288, 265, 18);

        //---- R0TYP_12 ----
        R0TYP_12.setText("7\u00e8me prix quantitatif");
        R0TYP_12.setName("R0TYP_12");
        panel2.add(R0TYP_12);
        R0TYP_12.setBounds(25, 311, 265, 18);

        //---- R0TYP_13 ----
        R0TYP_13.setText("8\u00e8me prix quantitatif");
        R0TYP_13.setName("R0TYP_13");
        panel2.add(R0TYP_13);
        R0TYP_13.setBounds(25, 334, 265, 18);

        //---- R0TYP_14 ----
        R0TYP_14.setText("9\u00e8me prix quantitatif");
        R0TYP_14.setName("R0TYP_14");
        panel2.add(R0TYP_14);
        R0TYP_14.setBounds(25, 357, 265, 18);
      }

      //======== panel3 ========
      {
        panel3.setBorder(new TitledBorder("Mise \u00e0 jour li\u00e9e aux achats"));
        panel3.setOpaque(false);
        panel3.setName("panel3");
        panel3.setLayout(null);

        //---- R0TYP_15 ----
        R0TYP_15.setText("PUMP d\u00e8s la r\u00e9ception d'achat x coeff.");
        R0TYP_15.setName("R0TYP_15");
        panel3.add(R0TYP_15);
        R0TYP_15.setBounds(25, 35, 265, 18);

        //---- R0TYP_16 ----
        R0TYP_16.setText("PUMP \u00e0 la demande (GAM 561) x coeff.");
        R0TYP_16.setName("R0TYP_16");
        panel3.add(R0TYP_16);
        R0TYP_16.setBounds(25, 58, 265, 18);

        //---- R0TYP_17 ----
        R0TYP_17.setText("Prix de revient x coeff.");
        R0TYP_17.setName("R0TYP_17");
        panel3.add(R0TYP_17);
        R0TYP_17.setBounds(25, 81, 265, 18);

        //---- R0TYP_18 ----
        R0TYP_18.setText("Prix de revient r\u00e9el du dernier achat x coeff.");
        R0TYP_18.setName("R0TYP_18");
        panel3.add(R0TYP_18);
        R0TYP_18.setBounds(25, 104, 265, 18);
      }

      GroupLayout pnl_contenuLayout = new GroupLayout(pnl_contenu);
      pnl_contenu.setLayout(pnl_contenuLayout);
      pnl_contenuLayout.setHorizontalGroup(
        pnl_contenuLayout.createParallelGroup()
          .addGroup(pnl_contenuLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnl_contenuLayout.createParallelGroup()
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
              .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
      );
      pnl_contenuLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {panel2, panel3});
      pnl_contenuLayout.setVerticalGroup(
        pnl_contenuLayout.createParallelGroup()
          .addGroup(pnl_contenuLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 402, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
            .addGap(19, 19, 19))
      );
    }
    add(pnl_contenu, BorderLayout.CENTER);

    //---- R0TYP_GRP ----
    R0TYP_GRP.add(R0TYP);
    R0TYP_GRP.add(R0TYP_01);
    R0TYP_GRP.add(R0TYP_02);
    R0TYP_GRP.add(R0TYP_03);
    R0TYP_GRP.add(R0TYP_04);
    R0TYP_GRP.add(R0TYP_05);
    R0TYP_GRP.add(R0TYP_06);
    R0TYP_GRP.add(R0TYP_07);
    R0TYP_GRP.add(R0TYP_08);
    R0TYP_GRP.add(R0TYP_09);
    R0TYP_GRP.add(R0TYP_10);
    R0TYP_GRP.add(R0TYP_11);
    R0TYP_GRP.add(R0TYP_12);
    R0TYP_GRP.add(R0TYP_13);
    R0TYP_GRP.add(R0TYP_14);
    R0TYP_GRP.add(R0TYP_15);
    R0TYP_GRP.add(R0TYP_16);
    R0TYP_GRP.add(R0TYP_17);
    R0TYP_GRP.add(R0TYP_18);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel pnl_menus;
  private JPanel panel1;
  private RiMenu riMenu1;
  private RiMenu_bt bt_valider;
  private RiMenu riMenu2;
  private RiMenu_bt bt_retour;
  private JPanel pnl_contenu;
  private JPanel panel2;
  private XRiRadioButton R0TYP;
  private XRiRadioButton R0TYP_01;
  private XRiRadioButton R0TYP_02;
  private XRiRadioButton R0TYP_03;
  private XRiRadioButton R0TYP_04;
  private XRiRadioButton R0TYP_05;
  private XRiRadioButton R0TYP_06;
  private XRiRadioButton R0TYP_07;
  private XRiRadioButton R0TYP_08;
  private XRiRadioButton R0TYP_09;
  private XRiRadioButton R0TYP_10;
  private XRiRadioButton R0TYP_11;
  private XRiRadioButton R0TYP_12;
  private XRiRadioButton R0TYP_13;
  private XRiRadioButton R0TYP_14;
  private JPanel panel3;
  private XRiRadioButton R0TYP_15;
  private XRiRadioButton R0TYP_16;
  private XRiRadioButton R0TYP_17;
  private XRiRadioButton R0TYP_18;
  private ButtonGroup R0TYP_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
