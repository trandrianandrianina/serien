
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_D2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] SN_Value = { " ", "+", "-" };
  
  public VGAM01FX_D2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    SNR13.setValeurs(SN_Value, null);
    SN13.setValeurs(SN_Value, null);
    SNR12.setValeurs(SN_Value, null);
    SN12.setValeurs(SN_Value, null);
    SNR11.setValeurs(SN_Value, null);
    SN11.setValeurs(SN_Value, null);
    SNR10.setValeurs(SN_Value, null);
    SN10.setValeurs(SN_Value, null);
    SNR09.setValeurs(SN_Value, null);
    SN09.setValeurs(SN_Value, null);
    SNR08.setValeurs(SN_Value, null);
    SN08.setValeurs(SN_Value, null);
    SNR07.setValeurs(SN_Value, null);
    SN07.setValeurs(SN_Value, null);
    SNR06.setValeurs(SN_Value, null);
    SN06.setValeurs(SN_Value, null);
    SNR05.setValeurs(SN_Value, null);
    SN05.setValeurs(SN_Value, null);
    SNR04.setValeurs(SN_Value, null);
    SN04.setValeurs(SN_Value, null);
    SNR03.setValeurs(SN_Value, null);
    SN03.setValeurs(SN_Value, null);
    SNR02.setValeurs(SN_Value, null);
    SN02.setValeurs(SN_Value, null);
    SNR01.setValeurs(SN_Value, null);
    SN01.setValeurs(SN_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    XD01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD01@")).trim());
    XD02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD02@")).trim());
    XD03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD03@")).trim());
    XD04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD04@")).trim());
    XD05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD05@")).trim());
    XD06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD06@")).trim());
    XD07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD07@")).trim());
    XD08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD08@")).trim());
    XD09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD09@")).trim());
    XD10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD10@")).trim());
    XD11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD11@")).trim());
    XD12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD12@")).trim());
    XD13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XD13@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // SN01.setSelectedIndex(getIndice("SN01", SN_Value));
    // SN02.setSelectedIndex(getIndice("SN02", SN_Value));
    // SN03.setSelectedIndex(getIndice("SN03", SN_Value));
    // SN04.setSelectedIndex(getIndice("SN04", SN_Value));
    // SN05.setSelectedIndex(getIndice("SN05", SN_Value));
    // SN06.setSelectedIndex(getIndice("SN06", SN_Value));
    // SN07.setSelectedIndex(getIndice("SN07", SN_Value));
    // SN08.setSelectedIndex(getIndice("SN08", SN_Value));
    // SN09.setSelectedIndex(getIndice("SN09", SN_Value));
    // SN10.setSelectedIndex(getIndice("SN10", SN_Value));
    // SN11.setSelectedIndex(getIndice("SN11", SN_Value));
    // SN12.setSelectedIndex(getIndice("SN12", SN_Value));
    // SN13.setSelectedIndex(getIndice("SN13", SN_Value));
    // SNR01.setSelectedIndex(getIndice("SNR01", SN_Value));
    // SNR02.setSelectedIndex(getIndice("SNR02", SN_Value));
    // SNR03.setSelectedIndex(getIndice("SNR03", SN_Value));
    // SNR04.setSelectedIndex(getIndice("SNR04", SN_Value));
    // SNR05.setSelectedIndex(getIndice("SNR05", SN_Value));
    // SNR06.setSelectedIndex(getIndice("SNR06", SN_Value));
    // SNR07.setSelectedIndex(getIndice("SNR07", SN_Value));
    // SNR08.setSelectedIndex(getIndice("SNR08", SN_Value));
    // SNR09.setSelectedIndex(getIndice("SNR09", SN_Value));
    // SNR10.setSelectedIndex(getIndice("SNR10", SN_Value));
    // SNR11.setSelectedIndex(getIndice("SNR11", SN_Value));
    // SNR12.setSelectedIndex(getIndice("SNR12", SN_Value));
    // SNR13.setSelectedIndex(getIndice("SNR13", SN_Value));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("SN01", 0, SN_Value[SN01.getSelectedIndex()]);
    // lexique.HostFieldPutData("SN02", 0, SN_Value[SN02.getSelectedIndex()]);
    // lexique.HostFieldPutData("SN03", 0, SN_Value[SN03.getSelectedIndex()]);
    // lexique.HostFieldPutData("SN04", 0, SN_Value[SN04.getSelectedIndex()]);
    // lexique.HostFieldPutData("SN05", 0, SN_Value[SN05.getSelectedIndex()]);
    // lexique.HostFieldPutData("SN06", 0, SN_Value[SN06.getSelectedIndex()]);
    // lexique.HostFieldPutData("SN07", 0, SN_Value[SN07.getSelectedIndex()]);
    // lexique.HostFieldPutData("SN08", 0, SN_Value[SN08.getSelectedIndex()]);
    // lexique.HostFieldPutData("SN09", 0, SN_Value[SN09.getSelectedIndex()]);
    // lexique.HostFieldPutData("SN10", 0, SN_Value[SN10.getSelectedIndex()]);
    // lexique.HostFieldPutData("SN11", 0, SN_Value[SN11.getSelectedIndex()]);
    // lexique.HostFieldPutData("SN12", 0, SN_Value[SN12.getSelectedIndex()]);
    // lexique.HostFieldPutData("SN13", 0, SN_Value[SN13.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR01", 0, SN_Value[SNR01.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR02", 0, SN_Value[SNR02.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR03", 0, SN_Value[SNR03.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR04", 0, SN_Value[SNR04.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR05", 0, SN_Value[SNR05.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR06", 0, SN_Value[SNR06.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR07", 0, SN_Value[SNR07.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR08", 0, SN_Value[SNR08.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR09", 0, SN_Value[SNR09.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR10", 0, SN_Value[SNR10.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR11", 0, SN_Value[SNR11.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR12", 0, SN_Value[SNR12.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNR13", 0, SN_Value[SNR13.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_48 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel1 = new JPanel();
    OBJ_52 = new JLabel();
    D2LIB = new XRiTextField();
    panel2 = new JPanel();
    TD01 = new XRiTextField();
    TD02 = new XRiTextField();
    TD03 = new XRiTextField();
    TD04 = new XRiTextField();
    TD05 = new XRiTextField();
    TD06 = new XRiTextField();
    TD07 = new XRiTextField();
    TD08 = new XRiTextField();
    TD09 = new XRiTextField();
    TD10 = new XRiTextField();
    TD11 = new XRiTextField();
    TD12 = new XRiTextField();
    TD13 = new XRiTextField();
    XD01 = new RiZoneSortie();
    XD02 = new RiZoneSortie();
    XD03 = new RiZoneSortie();
    XD04 = new RiZoneSortie();
    XD05 = new RiZoneSortie();
    XD06 = new RiZoneSortie();
    XD07 = new RiZoneSortie();
    XD08 = new RiZoneSortie();
    XD09 = new RiZoneSortie();
    XD10 = new RiZoneSortie();
    XD11 = new RiZoneSortie();
    XD12 = new RiZoneSortie();
    XD13 = new RiZoneSortie();
    RG01 = new XRiTextField();
    SN01 = new XRiComboBox();
    NJ01 = new XRiTextField();
    TDR01 = new XRiTextField();
    TDR02 = new XRiTextField();
    TDR03 = new XRiTextField();
    TDR04 = new XRiTextField();
    TDR05 = new XRiTextField();
    TDR06 = new XRiTextField();
    TDR07 = new XRiTextField();
    TDR08 = new XRiTextField();
    TDR09 = new XRiTextField();
    TDR10 = new XRiTextField();
    TDR11 = new XRiTextField();
    TDR12 = new XRiTextField();
    TDR13 = new XRiTextField();
    SNR01 = new XRiComboBox();
    NJR01 = new XRiTextField();
    NJRS01 = new XRiTextField();
    RG02 = new XRiTextField();
    SN02 = new XRiComboBox();
    NJ02 = new XRiTextField();
    SNR02 = new XRiComboBox();
    NJR02 = new XRiTextField();
    NJRS02 = new XRiTextField();
    RG03 = new XRiTextField();
    SN03 = new XRiComboBox();
    NJ03 = new XRiTextField();
    SNR03 = new XRiComboBox();
    NJR03 = new XRiTextField();
    NJRS03 = new XRiTextField();
    RG04 = new XRiTextField();
    SN04 = new XRiComboBox();
    NJ04 = new XRiTextField();
    SNR04 = new XRiComboBox();
    NJR04 = new XRiTextField();
    NJRS04 = new XRiTextField();
    RG05 = new XRiTextField();
    SN05 = new XRiComboBox();
    NJ05 = new XRiTextField();
    SNR05 = new XRiComboBox();
    NJR05 = new XRiTextField();
    NJRS05 = new XRiTextField();
    RG06 = new XRiTextField();
    SN06 = new XRiComboBox();
    NJ06 = new XRiTextField();
    SNR06 = new XRiComboBox();
    NJR06 = new XRiTextField();
    NJRS06 = new XRiTextField();
    RG07 = new XRiTextField();
    SN07 = new XRiComboBox();
    NJ07 = new XRiTextField();
    SNR07 = new XRiComboBox();
    NJR07 = new XRiTextField();
    NJRS07 = new XRiTextField();
    RG08 = new XRiTextField();
    SN08 = new XRiComboBox();
    NJ08 = new XRiTextField();
    SNR08 = new XRiComboBox();
    NJR08 = new XRiTextField();
    NJRS08 = new XRiTextField();
    RG09 = new XRiTextField();
    SN09 = new XRiComboBox();
    NJ09 = new XRiTextField();
    SNR09 = new XRiComboBox();
    NJR09 = new XRiTextField();
    NJRS09 = new XRiTextField();
    RG10 = new XRiTextField();
    SN10 = new XRiComboBox();
    NJ10 = new XRiTextField();
    SNR10 = new XRiComboBox();
    NJR10 = new XRiTextField();
    NJRS10 = new XRiTextField();
    RG11 = new XRiTextField();
    SN11 = new XRiComboBox();
    NJ11 = new XRiTextField();
    SNR11 = new XRiComboBox();
    NJR11 = new XRiTextField();
    NJRS11 = new XRiTextField();
    RG12 = new XRiTextField();
    SN12 = new XRiComboBox();
    NJ12 = new XRiTextField();
    SNR12 = new XRiComboBox();
    NJR12 = new XRiTextField();
    NJRS12 = new XRiTextField();
    RG13 = new XRiTextField();
    SN13 = new XRiComboBox();
    NJ13 = new XRiTextField();
    SNR13 = new XRiComboBox();
    NJR13 = new XRiTextField();
    NJRS13 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledSeparator2 = new JXTitledSeparator();
    xTitledSeparator3 = new JXTitledSeparator();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    label18 = new JLabel();
    label19 = new JLabel();
    label20 = new JLabel();
    label21 = new JLabel();
    xTitledSeparator4 = new JXTitledSeparator();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_48 ----
          OBJ_48.setText("Ordre");
          OBJ_48.setName("OBJ_48");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(840, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Sc\u00e9nario de dates");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder(""));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_52 ----
              OBJ_52.setText("Libell\u00e9");
              OBJ_52.setName("OBJ_52");
              panel1.add(OBJ_52);
              OBJ_52.setBounds(25, 10, 43, 20);

              //---- D2LIB ----
              D2LIB.setComponentPopupMenu(BTD);
              D2LIB.setName("D2LIB");
              panel1.add(D2LIB);
              D2LIB.setBounds(85, 10, 310, D2LIB.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(20, 10, 635, 55);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder(""));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- TD01 ----
              TD01.setComponentPopupMenu(BTD);
              TD01.setName("TD01");
              panel2.add(TD01);
              TD01.setBounds(15, 65, 50, TD01.getPreferredSize().height);

              //---- TD02 ----
              TD02.setComponentPopupMenu(BTD);
              TD02.setName("TD02");
              panel2.add(TD02);
              TD02.setBounds(15, 93, 50, TD02.getPreferredSize().height);

              //---- TD03 ----
              TD03.setComponentPopupMenu(BTD);
              TD03.setName("TD03");
              panel2.add(TD03);
              TD03.setBounds(15, 121, 50, TD03.getPreferredSize().height);

              //---- TD04 ----
              TD04.setComponentPopupMenu(BTD);
              TD04.setName("TD04");
              panel2.add(TD04);
              TD04.setBounds(15, 149, 50, TD04.getPreferredSize().height);

              //---- TD05 ----
              TD05.setComponentPopupMenu(BTD);
              TD05.setName("TD05");
              panel2.add(TD05);
              TD05.setBounds(15, 177, 50, TD05.getPreferredSize().height);

              //---- TD06 ----
              TD06.setComponentPopupMenu(BTD);
              TD06.setName("TD06");
              panel2.add(TD06);
              TD06.setBounds(15, 205, 50, TD06.getPreferredSize().height);

              //---- TD07 ----
              TD07.setComponentPopupMenu(BTD);
              TD07.setName("TD07");
              panel2.add(TD07);
              TD07.setBounds(15, 233, 50, TD07.getPreferredSize().height);

              //---- TD08 ----
              TD08.setComponentPopupMenu(BTD);
              TD08.setName("TD08");
              panel2.add(TD08);
              TD08.setBounds(15, 261, 50, TD08.getPreferredSize().height);

              //---- TD09 ----
              TD09.setComponentPopupMenu(BTD);
              TD09.setName("TD09");
              panel2.add(TD09);
              TD09.setBounds(15, 289, 50, TD09.getPreferredSize().height);

              //---- TD10 ----
              TD10.setComponentPopupMenu(BTD);
              TD10.setName("TD10");
              panel2.add(TD10);
              TD10.setBounds(15, 317, 50, TD10.getPreferredSize().height);

              //---- TD11 ----
              TD11.setComponentPopupMenu(BTD);
              TD11.setName("TD11");
              panel2.add(TD11);
              TD11.setBounds(15, 345, 50, TD11.getPreferredSize().height);

              //---- TD12 ----
              TD12.setComponentPopupMenu(BTD);
              TD12.setName("TD12");
              panel2.add(TD12);
              TD12.setBounds(15, 373, 50, TD12.getPreferredSize().height);

              //---- TD13 ----
              TD13.setComponentPopupMenu(BTD);
              TD13.setName("TD13");
              panel2.add(TD13);
              TD13.setBounds(15, 401, 50, TD13.getPreferredSize().height);

              //---- XD01 ----
              XD01.setText("@XD01@");
              XD01.setName("XD01");
              panel2.add(XD01);
              XD01.setBounds(70, 67, 310, XD01.getPreferredSize().height);

              //---- XD02 ----
              XD02.setText("@XD02@");
              XD02.setName("XD02");
              panel2.add(XD02);
              XD02.setBounds(70, 95, 310, XD02.getPreferredSize().height);

              //---- XD03 ----
              XD03.setText("@XD03@");
              XD03.setName("XD03");
              panel2.add(XD03);
              XD03.setBounds(70, 123, 310, XD03.getPreferredSize().height);

              //---- XD04 ----
              XD04.setText("@XD04@");
              XD04.setName("XD04");
              panel2.add(XD04);
              XD04.setBounds(70, 151, 310, XD04.getPreferredSize().height);

              //---- XD05 ----
              XD05.setText("@XD05@");
              XD05.setName("XD05");
              panel2.add(XD05);
              XD05.setBounds(70, 179, 310, XD05.getPreferredSize().height);

              //---- XD06 ----
              XD06.setText("@XD06@");
              XD06.setName("XD06");
              panel2.add(XD06);
              XD06.setBounds(70, 207, 310, XD06.getPreferredSize().height);

              //---- XD07 ----
              XD07.setText("@XD07@");
              XD07.setName("XD07");
              panel2.add(XD07);
              XD07.setBounds(70, 235, 310, XD07.getPreferredSize().height);

              //---- XD08 ----
              XD08.setText("@XD08@");
              XD08.setName("XD08");
              panel2.add(XD08);
              XD08.setBounds(70, 263, 310, XD08.getPreferredSize().height);

              //---- XD09 ----
              XD09.setText("@XD09@");
              XD09.setName("XD09");
              panel2.add(XD09);
              XD09.setBounds(70, 291, 310, XD09.getPreferredSize().height);

              //---- XD10 ----
              XD10.setText("@XD10@");
              XD10.setName("XD10");
              panel2.add(XD10);
              XD10.setBounds(70, 319, 310, XD10.getPreferredSize().height);

              //---- XD11 ----
              XD11.setText("@XD11@");
              XD11.setName("XD11");
              panel2.add(XD11);
              XD11.setBounds(70, 347, 310, XD11.getPreferredSize().height);

              //---- XD12 ----
              XD12.setText("@XD12@");
              XD12.setName("XD12");
              panel2.add(XD12);
              XD12.setBounds(70, 375, 310, XD12.getPreferredSize().height);

              //---- XD13 ----
              XD13.setText("@XD13@");
              XD13.setName("XD13");
              panel2.add(XD13);
              XD13.setBounds(70, 403, 310, XD13.getPreferredSize().height);

              //---- RG01 ----
              RG01.setName("RG01");
              panel2.add(RG01);
              RG01.setBounds(405, 65, 30, RG01.getPreferredSize().height);

              //---- SN01 ----
              SN01.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN01.setName("SN01");
              panel2.add(SN01);
              SN01.setBounds(440, 66, 40, SN01.getPreferredSize().height);

              //---- NJ01 ----
              NJ01.setName("NJ01");
              panel2.add(NJ01);
              NJ01.setBounds(485, 65, 30, 28);

              //---- TDR01 ----
              TDR01.setComponentPopupMenu(BTD);
              TDR01.setName("TDR01");
              panel2.add(TDR01);
              TDR01.setBounds(540, 65, 50, TDR01.getPreferredSize().height);

              //---- TDR02 ----
              TDR02.setComponentPopupMenu(BTD);
              TDR02.setName("TDR02");
              panel2.add(TDR02);
              TDR02.setBounds(540, 93, 50, TDR02.getPreferredSize().height);

              //---- TDR03 ----
              TDR03.setComponentPopupMenu(BTD);
              TDR03.setName("TDR03");
              panel2.add(TDR03);
              TDR03.setBounds(540, 121, 50, TDR03.getPreferredSize().height);

              //---- TDR04 ----
              TDR04.setComponentPopupMenu(BTD);
              TDR04.setName("TDR04");
              panel2.add(TDR04);
              TDR04.setBounds(540, 149, 50, TDR04.getPreferredSize().height);

              //---- TDR05 ----
              TDR05.setComponentPopupMenu(BTD);
              TDR05.setName("TDR05");
              panel2.add(TDR05);
              TDR05.setBounds(540, 177, 50, TDR05.getPreferredSize().height);

              //---- TDR06 ----
              TDR06.setComponentPopupMenu(BTD);
              TDR06.setName("TDR06");
              panel2.add(TDR06);
              TDR06.setBounds(540, 205, 50, TDR06.getPreferredSize().height);

              //---- TDR07 ----
              TDR07.setComponentPopupMenu(BTD);
              TDR07.setName("TDR07");
              panel2.add(TDR07);
              TDR07.setBounds(540, 233, 50, TDR07.getPreferredSize().height);

              //---- TDR08 ----
              TDR08.setComponentPopupMenu(BTD);
              TDR08.setName("TDR08");
              panel2.add(TDR08);
              TDR08.setBounds(540, 261, 50, TDR08.getPreferredSize().height);

              //---- TDR09 ----
              TDR09.setComponentPopupMenu(BTD);
              TDR09.setName("TDR09");
              panel2.add(TDR09);
              TDR09.setBounds(540, 289, 50, TDR09.getPreferredSize().height);

              //---- TDR10 ----
              TDR10.setComponentPopupMenu(BTD);
              TDR10.setName("TDR10");
              panel2.add(TDR10);
              TDR10.setBounds(540, 317, 50, TDR10.getPreferredSize().height);

              //---- TDR11 ----
              TDR11.setComponentPopupMenu(BTD);
              TDR11.setName("TDR11");
              panel2.add(TDR11);
              TDR11.setBounds(540, 345, 50, TDR11.getPreferredSize().height);

              //---- TDR12 ----
              TDR12.setComponentPopupMenu(BTD);
              TDR12.setName("TDR12");
              panel2.add(TDR12);
              TDR12.setBounds(540, 373, 50, TDR12.getPreferredSize().height);

              //---- TDR13 ----
              TDR13.setComponentPopupMenu(BTD);
              TDR13.setName("TDR13");
              panel2.add(TDR13);
              TDR13.setBounds(540, 401, 50, TDR13.getPreferredSize().height);

              //---- SNR01 ----
              SNR01.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR01.setName("SNR01");
              panel2.add(SNR01);
              SNR01.setBounds(595, 65, 40, SNR01.getPreferredSize().height);

              //---- NJR01 ----
              NJR01.setName("NJR01");
              panel2.add(NJR01);
              NJR01.setBounds(645, 65, 30, NJR01.getPreferredSize().height);

              //---- NJRS01 ----
              NJRS01.setName("NJRS01");
              panel2.add(NJRS01);
              NJRS01.setBounds(710, 65, 30, NJRS01.getPreferredSize().height);

              //---- RG02 ----
              RG02.setName("RG02");
              panel2.add(RG02);
              RG02.setBounds(405, 93, 30, RG02.getPreferredSize().height);

              //---- SN02 ----
              SN02.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN02.setName("SN02");
              panel2.add(SN02);
              SN02.setBounds(440, 94, 40, SN02.getPreferredSize().height);

              //---- NJ02 ----
              NJ02.setName("NJ02");
              panel2.add(NJ02);
              NJ02.setBounds(485, 93, 30, NJ02.getPreferredSize().height);

              //---- SNR02 ----
              SNR02.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR02.setName("SNR02");
              panel2.add(SNR02);
              SNR02.setBounds(595, 93, 40, SNR02.getPreferredSize().height);

              //---- NJR02 ----
              NJR02.setName("NJR02");
              panel2.add(NJR02);
              NJR02.setBounds(645, 93, 30, NJR02.getPreferredSize().height);

              //---- NJRS02 ----
              NJRS02.setName("NJRS02");
              panel2.add(NJRS02);
              NJRS02.setBounds(710, 93, 30, NJRS02.getPreferredSize().height);

              //---- RG03 ----
              RG03.setName("RG03");
              panel2.add(RG03);
              RG03.setBounds(405, 121, 30, RG03.getPreferredSize().height);

              //---- SN03 ----
              SN03.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN03.setName("SN03");
              panel2.add(SN03);
              SN03.setBounds(440, 122, 40, SN03.getPreferredSize().height);

              //---- NJ03 ----
              NJ03.setName("NJ03");
              panel2.add(NJ03);
              NJ03.setBounds(485, 121, 30, NJ03.getPreferredSize().height);

              //---- SNR03 ----
              SNR03.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR03.setName("SNR03");
              panel2.add(SNR03);
              SNR03.setBounds(595, 121, 40, SNR03.getPreferredSize().height);

              //---- NJR03 ----
              NJR03.setName("NJR03");
              panel2.add(NJR03);
              NJR03.setBounds(645, 121, 30, NJR03.getPreferredSize().height);

              //---- NJRS03 ----
              NJRS03.setName("NJRS03");
              panel2.add(NJRS03);
              NJRS03.setBounds(710, 121, 30, NJRS03.getPreferredSize().height);

              //---- RG04 ----
              RG04.setName("RG04");
              panel2.add(RG04);
              RG04.setBounds(405, 149, 30, RG04.getPreferredSize().height);

              //---- SN04 ----
              SN04.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN04.setName("SN04");
              panel2.add(SN04);
              SN04.setBounds(440, 150, 40, SN04.getPreferredSize().height);

              //---- NJ04 ----
              NJ04.setName("NJ04");
              panel2.add(NJ04);
              NJ04.setBounds(485, 149, 30, NJ04.getPreferredSize().height);

              //---- SNR04 ----
              SNR04.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR04.setName("SNR04");
              panel2.add(SNR04);
              SNR04.setBounds(595, 149, 40, SNR04.getPreferredSize().height);

              //---- NJR04 ----
              NJR04.setName("NJR04");
              panel2.add(NJR04);
              NJR04.setBounds(645, 149, 30, NJR04.getPreferredSize().height);

              //---- NJRS04 ----
              NJRS04.setName("NJRS04");
              panel2.add(NJRS04);
              NJRS04.setBounds(710, 149, 30, NJRS04.getPreferredSize().height);

              //---- RG05 ----
              RG05.setName("RG05");
              panel2.add(RG05);
              RG05.setBounds(405, 177, 30, RG05.getPreferredSize().height);

              //---- SN05 ----
              SN05.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN05.setName("SN05");
              panel2.add(SN05);
              SN05.setBounds(440, 178, 40, SN05.getPreferredSize().height);

              //---- NJ05 ----
              NJ05.setName("NJ05");
              panel2.add(NJ05);
              NJ05.setBounds(485, 177, 30, NJ05.getPreferredSize().height);

              //---- SNR05 ----
              SNR05.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR05.setName("SNR05");
              panel2.add(SNR05);
              SNR05.setBounds(595, 177, 40, SNR05.getPreferredSize().height);

              //---- NJR05 ----
              NJR05.setName("NJR05");
              panel2.add(NJR05);
              NJR05.setBounds(645, 177, 30, NJR05.getPreferredSize().height);

              //---- NJRS05 ----
              NJRS05.setName("NJRS05");
              panel2.add(NJRS05);
              NJRS05.setBounds(710, 177, 30, NJRS05.getPreferredSize().height);

              //---- RG06 ----
              RG06.setName("RG06");
              panel2.add(RG06);
              RG06.setBounds(405, 205, 30, RG06.getPreferredSize().height);

              //---- SN06 ----
              SN06.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN06.setName("SN06");
              panel2.add(SN06);
              SN06.setBounds(440, 206, 40, SN06.getPreferredSize().height);

              //---- NJ06 ----
              NJ06.setName("NJ06");
              panel2.add(NJ06);
              NJ06.setBounds(485, 205, 30, NJ06.getPreferredSize().height);

              //---- SNR06 ----
              SNR06.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR06.setName("SNR06");
              panel2.add(SNR06);
              SNR06.setBounds(595, 205, 40, SNR06.getPreferredSize().height);

              //---- NJR06 ----
              NJR06.setName("NJR06");
              panel2.add(NJR06);
              NJR06.setBounds(645, 205, 30, NJR06.getPreferredSize().height);

              //---- NJRS06 ----
              NJRS06.setName("NJRS06");
              panel2.add(NJRS06);
              NJRS06.setBounds(710, 205, 30, NJRS06.getPreferredSize().height);

              //---- RG07 ----
              RG07.setName("RG07");
              panel2.add(RG07);
              RG07.setBounds(405, 233, 30, RG07.getPreferredSize().height);

              //---- SN07 ----
              SN07.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN07.setName("SN07");
              panel2.add(SN07);
              SN07.setBounds(440, 234, 40, SN07.getPreferredSize().height);

              //---- NJ07 ----
              NJ07.setName("NJ07");
              panel2.add(NJ07);
              NJ07.setBounds(485, 233, 30, NJ07.getPreferredSize().height);

              //---- SNR07 ----
              SNR07.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR07.setName("SNR07");
              panel2.add(SNR07);
              SNR07.setBounds(595, 233, 40, SNR07.getPreferredSize().height);

              //---- NJR07 ----
              NJR07.setName("NJR07");
              panel2.add(NJR07);
              NJR07.setBounds(645, 233, 30, NJR07.getPreferredSize().height);

              //---- NJRS07 ----
              NJRS07.setName("NJRS07");
              panel2.add(NJRS07);
              NJRS07.setBounds(710, 233, 30, NJRS07.getPreferredSize().height);

              //---- RG08 ----
              RG08.setName("RG08");
              panel2.add(RG08);
              RG08.setBounds(405, 261, 30, RG08.getPreferredSize().height);

              //---- SN08 ----
              SN08.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN08.setName("SN08");
              panel2.add(SN08);
              SN08.setBounds(440, 262, 40, SN08.getPreferredSize().height);

              //---- NJ08 ----
              NJ08.setName("NJ08");
              panel2.add(NJ08);
              NJ08.setBounds(485, 261, 30, NJ08.getPreferredSize().height);

              //---- SNR08 ----
              SNR08.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR08.setName("SNR08");
              panel2.add(SNR08);
              SNR08.setBounds(595, 261, 40, SNR08.getPreferredSize().height);

              //---- NJR08 ----
              NJR08.setName("NJR08");
              panel2.add(NJR08);
              NJR08.setBounds(645, 261, 30, NJR08.getPreferredSize().height);

              //---- NJRS08 ----
              NJRS08.setName("NJRS08");
              panel2.add(NJRS08);
              NJRS08.setBounds(710, 261, 30, NJRS08.getPreferredSize().height);

              //---- RG09 ----
              RG09.setName("RG09");
              panel2.add(RG09);
              RG09.setBounds(405, 289, 30, RG09.getPreferredSize().height);

              //---- SN09 ----
              SN09.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN09.setName("SN09");
              panel2.add(SN09);
              SN09.setBounds(440, 290, 40, SN09.getPreferredSize().height);

              //---- NJ09 ----
              NJ09.setName("NJ09");
              panel2.add(NJ09);
              NJ09.setBounds(485, 289, 30, NJ09.getPreferredSize().height);

              //---- SNR09 ----
              SNR09.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR09.setName("SNR09");
              panel2.add(SNR09);
              SNR09.setBounds(595, 289, 40, SNR09.getPreferredSize().height);

              //---- NJR09 ----
              NJR09.setName("NJR09");
              panel2.add(NJR09);
              NJR09.setBounds(645, 289, 30, NJR09.getPreferredSize().height);

              //---- NJRS09 ----
              NJRS09.setName("NJRS09");
              panel2.add(NJRS09);
              NJRS09.setBounds(710, 289, 30, NJRS09.getPreferredSize().height);

              //---- RG10 ----
              RG10.setName("RG10");
              panel2.add(RG10);
              RG10.setBounds(405, 317, 30, RG10.getPreferredSize().height);

              //---- SN10 ----
              SN10.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN10.setName("SN10");
              panel2.add(SN10);
              SN10.setBounds(440, 318, 40, SN10.getPreferredSize().height);

              //---- NJ10 ----
              NJ10.setName("NJ10");
              panel2.add(NJ10);
              NJ10.setBounds(485, 317, 30, NJ10.getPreferredSize().height);

              //---- SNR10 ----
              SNR10.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR10.setName("SNR10");
              panel2.add(SNR10);
              SNR10.setBounds(595, 317, 40, SNR10.getPreferredSize().height);

              //---- NJR10 ----
              NJR10.setName("NJR10");
              panel2.add(NJR10);
              NJR10.setBounds(645, 317, 30, NJR10.getPreferredSize().height);

              //---- NJRS10 ----
              NJRS10.setName("NJRS10");
              panel2.add(NJRS10);
              NJRS10.setBounds(710, 317, 30, NJRS10.getPreferredSize().height);

              //---- RG11 ----
              RG11.setName("RG11");
              panel2.add(RG11);
              RG11.setBounds(405, 345, 30, RG11.getPreferredSize().height);

              //---- SN11 ----
              SN11.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN11.setName("SN11");
              panel2.add(SN11);
              SN11.setBounds(440, 346, 40, SN11.getPreferredSize().height);

              //---- NJ11 ----
              NJ11.setName("NJ11");
              panel2.add(NJ11);
              NJ11.setBounds(485, 345, 30, NJ11.getPreferredSize().height);

              //---- SNR11 ----
              SNR11.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR11.setName("SNR11");
              panel2.add(SNR11);
              SNR11.setBounds(595, 345, 40, SNR11.getPreferredSize().height);

              //---- NJR11 ----
              NJR11.setName("NJR11");
              panel2.add(NJR11);
              NJR11.setBounds(645, 345, 30, NJR11.getPreferredSize().height);

              //---- NJRS11 ----
              NJRS11.setName("NJRS11");
              panel2.add(NJRS11);
              NJRS11.setBounds(710, 345, 30, NJRS11.getPreferredSize().height);

              //---- RG12 ----
              RG12.setName("RG12");
              panel2.add(RG12);
              RG12.setBounds(405, 373, 30, RG12.getPreferredSize().height);

              //---- SN12 ----
              SN12.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN12.setName("SN12");
              panel2.add(SN12);
              SN12.setBounds(440, 374, 40, SN12.getPreferredSize().height);

              //---- NJ12 ----
              NJ12.setName("NJ12");
              panel2.add(NJ12);
              NJ12.setBounds(485, 373, 30, NJ12.getPreferredSize().height);

              //---- SNR12 ----
              SNR12.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR12.setName("SNR12");
              panel2.add(SNR12);
              SNR12.setBounds(595, 373, 40, SNR12.getPreferredSize().height);

              //---- NJR12 ----
              NJR12.setName("NJR12");
              panel2.add(NJR12);
              NJR12.setBounds(645, 373, 30, NJR12.getPreferredSize().height);

              //---- NJRS12 ----
              NJRS12.setName("NJRS12");
              panel2.add(NJRS12);
              NJRS12.setBounds(710, 373, 30, NJRS12.getPreferredSize().height);

              //---- RG13 ----
              RG13.setName("RG13");
              panel2.add(RG13);
              RG13.setBounds(405, 401, 30, RG13.getPreferredSize().height);

              //---- SN13 ----
              SN13.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SN13.setName("SN13");
              panel2.add(SN13);
              SN13.setBounds(440, 402, 40, SN13.getPreferredSize().height);

              //---- NJ13 ----
              NJ13.setName("NJ13");
              panel2.add(NJ13);
              NJ13.setBounds(485, 401, 30, NJ13.getPreferredSize().height);

              //---- SNR13 ----
              SNR13.setModel(new DefaultComboBoxModel(new String[] {
                "  ",
                "+",
                "-"
              }));
              SNR13.setName("SNR13");
              panel2.add(SNR13);
              SNR13.setBounds(595, 401, 40, SNR13.getPreferredSize().height);

              //---- NJR13 ----
              NJR13.setName("NJR13");
              panel2.add(NJR13);
              NJR13.setBounds(645, 401, 30, NJR13.getPreferredSize().height);

              //---- NJRS13 ----
              NJRS13.setName("NJRS13");
              panel2.add(NJRS13);
              NJRS13.setBounds(710, 401, 30, NJRS13.getPreferredSize().height);

              //---- label1 ----
              label1.setText("Date");
              label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
              label1.setName("label1");
              panel2.add(label1);
              label1.setBounds(15, 35, 90, label1.getPreferredSize().height);

              //---- label2 ----
              label2.setText("Rg");
              label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
              label2.setName("label2");
              panel2.add(label2);
              label2.setBounds(new Rectangle(new Point(413, 45), label2.getPreferredSize()));

              //---- label3 ----
              label3.setText("+/-");
              label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
              label3.setName("label3");
              panel2.add(label3);
              label3.setBounds(new Rectangle(new Point(453, 45), label3.getPreferredSize()));

              //---- label4 ----
              label4.setText("NbJ");
              label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
              label4.setName("label4");
              panel2.add(label4);
              label4.setBounds(new Rectangle(new Point(489, 45), label4.getPreferredSize()));

              //---- label5 ----
              label5.setText("Date");
              label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
              label5.setName("label5");
              panel2.add(label5);
              label5.setBounds(new Rectangle(new Point(552, 45), label5.getPreferredSize()));

              //---- label6 ----
              label6.setText("+/-");
              label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
              label6.setName("label6");
              panel2.add(label6);
              label6.setBounds(new Rectangle(new Point(608, 45), label6.getPreferredSize()));

              //---- label7 ----
              label7.setText("NbJ");
              label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
              label7.setName("label7");
              panel2.add(label7);
              label7.setBounds(new Rectangle(new Point(649, 45), label7.getPreferredSize()));

              //---- label8 ----
              label8.setText("NbJ");
              label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
              label8.setName("label8");
              panel2.add(label8);
              label8.setBounds(new Rectangle(new Point(714, 45), label8.getPreferredSize()));

              //---- xTitledSeparator1 ----
              xTitledSeparator1.setTitle("Calcul");
              xTitledSeparator1.setName("xTitledSeparator1");
              panel2.add(xTitledSeparator1);
              xTitledSeparator1.setBounds(395, 30, 125, xTitledSeparator1.getPreferredSize().height);

              //---- xTitledSeparator2 ----
              xTitledSeparator2.setTitle("Premi\u00e8re");
              xTitledSeparator2.setName("xTitledSeparator2");
              panel2.add(xTitledSeparator2);
              xTitledSeparator2.setBounds(535, 30, 145, xTitledSeparator2.getPreferredSize().height);

              //---- xTitledSeparator3 ----
              xTitledSeparator3.setTitle("Suivantes");
              xTitledSeparator3.setName("xTitledSeparator3");
              panel2.add(xTitledSeparator3);
              xTitledSeparator3.setBounds(690, 30, 70, xTitledSeparator3.getPreferredSize().height);

              //---- label9 ----
              label9.setText("=");
              label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
              label9.setHorizontalAlignment(SwingConstants.CENTER);
              label9.setName("label9");
              panel2.add(label9);
              label9.setBounds(385, 71, 14, label9.getPreferredSize().height);

              //---- label10 ----
              label10.setText("=");
              label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
              label10.setHorizontalAlignment(SwingConstants.CENTER);
              label10.setName("label10");
              panel2.add(label10);
              label10.setBounds(385, 99, 14, label10.getPreferredSize().height);

              //---- label11 ----
              label11.setText("=");
              label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
              label11.setHorizontalAlignment(SwingConstants.CENTER);
              label11.setName("label11");
              panel2.add(label11);
              label11.setBounds(385, 127, 14, label11.getPreferredSize().height);

              //---- label12 ----
              label12.setText("=");
              label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
              label12.setHorizontalAlignment(SwingConstants.CENTER);
              label12.setName("label12");
              panel2.add(label12);
              label12.setBounds(385, 155, 14, label12.getPreferredSize().height);

              //---- label13 ----
              label13.setText("=");
              label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
              label13.setHorizontalAlignment(SwingConstants.CENTER);
              label13.setName("label13");
              panel2.add(label13);
              label13.setBounds(385, 183, 14, label13.getPreferredSize().height);

              //---- label14 ----
              label14.setText("=");
              label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
              label14.setHorizontalAlignment(SwingConstants.CENTER);
              label14.setName("label14");
              panel2.add(label14);
              label14.setBounds(385, 211, 14, label14.getPreferredSize().height);

              //---- label15 ----
              label15.setText("=");
              label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
              label15.setHorizontalAlignment(SwingConstants.CENTER);
              label15.setName("label15");
              panel2.add(label15);
              label15.setBounds(385, 239, 14, label15.getPreferredSize().height);

              //---- label16 ----
              label16.setText("=");
              label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
              label16.setHorizontalAlignment(SwingConstants.CENTER);
              label16.setName("label16");
              panel2.add(label16);
              label16.setBounds(385, 267, 14, label16.getPreferredSize().height);

              //---- label17 ----
              label17.setText("=");
              label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
              label17.setHorizontalAlignment(SwingConstants.CENTER);
              label17.setName("label17");
              panel2.add(label17);
              label17.setBounds(385, 295, 14, label17.getPreferredSize().height);

              //---- label18 ----
              label18.setText("=");
              label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
              label18.setHorizontalAlignment(SwingConstants.CENTER);
              label18.setName("label18");
              panel2.add(label18);
              label18.setBounds(385, 323, 14, label18.getPreferredSize().height);

              //---- label19 ----
              label19.setText("=");
              label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
              label19.setHorizontalAlignment(SwingConstants.CENTER);
              label19.setName("label19");
              panel2.add(label19);
              label19.setBounds(385, 351, 14, label19.getPreferredSize().height);

              //---- label20 ----
              label20.setText("=");
              label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
              label20.setHorizontalAlignment(SwingConstants.CENTER);
              label20.setName("label20");
              panel2.add(label20);
              label20.setBounds(385, 379, 14, label20.getPreferredSize().height);

              //---- label21 ----
              label21.setText("=");
              label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD));
              label21.setHorizontalAlignment(SwingConstants.CENTER);
              label21.setName("label21");
              panel2.add(label21);
              label21.setBounds(385, 407, 14, label21.getPreferredSize().height);

              //---- xTitledSeparator4 ----
              xTitledSeparator4.setTitle("Relances");
              xTitledSeparator4.setName("xTitledSeparator4");
              panel2.add(xTitledSeparator4);
              xTitledSeparator4.setBounds(535, 10, 225, xTitledSeparator4.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(20, 75, 775, 445);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 816, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 561, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_48;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel1;
  private JLabel OBJ_52;
  private XRiTextField D2LIB;
  private JPanel panel2;
  private XRiTextField TD01;
  private XRiTextField TD02;
  private XRiTextField TD03;
  private XRiTextField TD04;
  private XRiTextField TD05;
  private XRiTextField TD06;
  private XRiTextField TD07;
  private XRiTextField TD08;
  private XRiTextField TD09;
  private XRiTextField TD10;
  private XRiTextField TD11;
  private XRiTextField TD12;
  private XRiTextField TD13;
  private RiZoneSortie XD01;
  private RiZoneSortie XD02;
  private RiZoneSortie XD03;
  private RiZoneSortie XD04;
  private RiZoneSortie XD05;
  private RiZoneSortie XD06;
  private RiZoneSortie XD07;
  private RiZoneSortie XD08;
  private RiZoneSortie XD09;
  private RiZoneSortie XD10;
  private RiZoneSortie XD11;
  private RiZoneSortie XD12;
  private RiZoneSortie XD13;
  private XRiTextField RG01;
  private XRiComboBox SN01;
  private XRiTextField NJ01;
  private XRiTextField TDR01;
  private XRiTextField TDR02;
  private XRiTextField TDR03;
  private XRiTextField TDR04;
  private XRiTextField TDR05;
  private XRiTextField TDR06;
  private XRiTextField TDR07;
  private XRiTextField TDR08;
  private XRiTextField TDR09;
  private XRiTextField TDR10;
  private XRiTextField TDR11;
  private XRiTextField TDR12;
  private XRiTextField TDR13;
  private XRiComboBox SNR01;
  private XRiTextField NJR01;
  private XRiTextField NJRS01;
  private XRiTextField RG02;
  private XRiComboBox SN02;
  private XRiTextField NJ02;
  private XRiComboBox SNR02;
  private XRiTextField NJR02;
  private XRiTextField NJRS02;
  private XRiTextField RG03;
  private XRiComboBox SN03;
  private XRiTextField NJ03;
  private XRiComboBox SNR03;
  private XRiTextField NJR03;
  private XRiTextField NJRS03;
  private XRiTextField RG04;
  private XRiComboBox SN04;
  private XRiTextField NJ04;
  private XRiComboBox SNR04;
  private XRiTextField NJR04;
  private XRiTextField NJRS04;
  private XRiTextField RG05;
  private XRiComboBox SN05;
  private XRiTextField NJ05;
  private XRiComboBox SNR05;
  private XRiTextField NJR05;
  private XRiTextField NJRS05;
  private XRiTextField RG06;
  private XRiComboBox SN06;
  private XRiTextField NJ06;
  private XRiComboBox SNR06;
  private XRiTextField NJR06;
  private XRiTextField NJRS06;
  private XRiTextField RG07;
  private XRiComboBox SN07;
  private XRiTextField NJ07;
  private XRiComboBox SNR07;
  private XRiTextField NJR07;
  private XRiTextField NJRS07;
  private XRiTextField RG08;
  private XRiComboBox SN08;
  private XRiTextField NJ08;
  private XRiComboBox SNR08;
  private XRiTextField NJR08;
  private XRiTextField NJRS08;
  private XRiTextField RG09;
  private XRiComboBox SN09;
  private XRiTextField NJ09;
  private XRiComboBox SNR09;
  private XRiTextField NJR09;
  private XRiTextField NJRS09;
  private XRiTextField RG10;
  private XRiComboBox SN10;
  private XRiTextField NJ10;
  private XRiComboBox SNR10;
  private XRiTextField NJR10;
  private XRiTextField NJRS10;
  private XRiTextField RG11;
  private XRiComboBox SN11;
  private XRiTextField NJ11;
  private XRiComboBox SNR11;
  private XRiTextField NJR11;
  private XRiTextField NJRS11;
  private XRiTextField RG12;
  private XRiComboBox SN12;
  private XRiTextField NJ12;
  private XRiComboBox SNR12;
  private XRiTextField NJR12;
  private XRiTextField NJRS12;
  private XRiTextField RG13;
  private XRiComboBox SN13;
  private XRiTextField NJ13;
  private XRiComboBox SNR13;
  private XRiTextField NJR13;
  private XRiTextField NJRS13;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator xTitledSeparator2;
  private JXTitledSeparator xTitledSeparator3;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label17;
  private JLabel label18;
  private JLabel label19;
  private JLabel label20;
  private JLabel label21;
  private JXTitledSeparator xTitledSeparator4;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
