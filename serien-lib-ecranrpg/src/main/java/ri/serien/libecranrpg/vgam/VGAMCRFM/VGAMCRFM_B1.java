
package ri.serien.libecranrpg.vgam.VGAMCRFM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAMCRFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public VGAMCRFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLibelleRegroupement.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@R0LIB@")).trim());
    tfRegroupement.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRGA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    rafraichirEtablissement();
    rafraichirFournisseur();
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  /**
   * Initialise le composant établissement.
   */
  private void rafraichirEtablissement() {
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    snEtablissement.setEnabled(false);
  }
  
  /**
   * Initialise le composant fournisseur suivant l'établissement.
   */
  private void rafraichirFournisseur() {
    boolean venteComptoir = lexique.isTrue("92");
    
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(false);
    snFournisseur.setSelectionParChampRPG(lexique, "WCOLX", "WFRSX");
    snFournisseur.setEnabled(false);
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlReappro = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    lbRegroupement = new SNLabelChamp();
    pnlRegroupement = new SNPanel();
    tfRegroupement = new SNTexte();
    lbLibelleRegroupement = new SNLabelUnite();
    pnlCriteres = new SNPanel();
    lbClasses = new SNLabelTitre();
    lbN = new SNLabelTitre();
    lbA = new SNLabelTitre();
    lbB = new SNLabelTitre();
    lbC = new SNLabelTitre();
    lbD = new SNLabelTitre();
    lbSemainesAnalyse = new SNLabelChamp();
    CRNSA = new XRiTextField();
    CRASA = new XRiTextField();
    CRBSA = new XRiTextField();
    CRCSA = new XRiTextField();
    CRDSA = new XRiTextField();
    lbSemainesStockMini = new SNLabelChamp();
    CRNSCM = new XRiTextField();
    CRASCM = new XRiTextField();
    CRBSCM = new XRiTextField();
    CRCSCM = new XRiTextField();
    CRDSCM = new XRiTextField();
    lbSemainesAnalyse3 = new SNLabelChamp();
    CRNSCL = new XRiTextField();
    CRASCL = new XRiTextField();
    CRBSCL = new XRiTextField();
    CRCSCL = new XRiTextField();
    CRDSCL = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(870, 595));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlReappro ========
      {
        pnlReappro.setName("pnlReappro");
        pnlReappro.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlReappro.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlReappro.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlReappro.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlReappro.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlReappro.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setName("snEtablissement");
        pnlReappro.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbFournisseur ----
        lbFournisseur.setText("Fournisseur");
        lbFournisseur.setName("lbFournisseur");
        pnlReappro.add(lbFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snFournisseur ----
        snFournisseur.setName("snFournisseur");
        pnlReappro.add(snFournisseur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbRegroupement ----
        lbRegroupement.setText("Regroupement achat");
        lbRegroupement.setName("lbRegroupement");
        pnlReappro.add(lbRegroupement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlRegroupement ========
        {
          pnlRegroupement.setName("pnlRegroupement");
          pnlRegroupement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRegroupement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlRegroupement.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRegroupement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlRegroupement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- tfRegroupement ----
          tfRegroupement.setText("@WRGA@");
          tfRegroupement.setEnabled(false);
          tfRegroupement.setName("tfRegroupement");
          pnlRegroupement.add(tfRegroupement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbLibelleRegroupement ----
          lbLibelleRegroupement.setText("@R0LIB@");
          lbLibelleRegroupement.setName("lbLibelleRegroupement");
          pnlRegroupement.add(lbLibelleRegroupement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlReappro.add(pnlRegroupement, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlReappro,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlCriteres ========
      {
        pnlCriteres.setName("pnlCriteres");
        pnlCriteres.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCriteres.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCriteres.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCriteres.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlCriteres.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbClasses ----
        lbClasses.setText("Classes");
        lbClasses.setName("lbClasses");
        pnlCriteres.add(lbClasses, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbN ----
        lbN.setText("N");
        lbN.setMaximumSize(new Dimension(50, 30));
        lbN.setMinimumSize(new Dimension(50, 30));
        lbN.setPreferredSize(new Dimension(50, 30));
        lbN.setHorizontalAlignment(SwingConstants.CENTER);
        lbN.setName("lbN");
        pnlCriteres.add(lbN, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbA ----
        lbA.setText("A");
        lbA.setMaximumSize(new Dimension(50, 30));
        lbA.setMinimumSize(new Dimension(50, 30));
        lbA.setPreferredSize(new Dimension(50, 30));
        lbA.setHorizontalAlignment(SwingConstants.CENTER);
        lbA.setName("lbA");
        pnlCriteres.add(lbA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbB ----
        lbB.setText("B");
        lbB.setMaximumSize(new Dimension(50, 30));
        lbB.setMinimumSize(new Dimension(50, 30));
        lbB.setPreferredSize(new Dimension(50, 30));
        lbB.setHorizontalAlignment(SwingConstants.CENTER);
        lbB.setName("lbB");
        pnlCriteres.add(lbB, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbC ----
        lbC.setText("C");
        lbC.setMaximumSize(new Dimension(50, 30));
        lbC.setMinimumSize(new Dimension(50, 30));
        lbC.setPreferredSize(new Dimension(50, 30));
        lbC.setHorizontalAlignment(SwingConstants.CENTER);
        lbC.setName("lbC");
        pnlCriteres.add(lbC, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbD ----
        lbD.setText("D");
        lbD.setMaximumSize(new Dimension(50, 30));
        lbD.setMinimumSize(new Dimension(50, 30));
        lbD.setPreferredSize(new Dimension(50, 30));
        lbD.setHorizontalAlignment(SwingConstants.CENTER);
        lbD.setName("lbD");
        pnlCriteres.add(lbD, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbSemainesAnalyse ----
        lbSemainesAnalyse.setText("Nombre de semaines d'analyse");
        lbSemainesAnalyse.setMaximumSize(new Dimension(250, 30));
        lbSemainesAnalyse.setMinimumSize(new Dimension(250, 30));
        lbSemainesAnalyse.setPreferredSize(new Dimension(250, 30));
        lbSemainesAnalyse.setName("lbSemainesAnalyse");
        pnlCriteres.add(lbSemainesAnalyse, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- CRNSA ----
        CRNSA.setName("CRNSA");
        pnlCriteres.add(CRNSA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- CRASA ----
        CRASA.setName("CRASA");
        pnlCriteres.add(CRASA, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- CRBSA ----
        CRBSA.setName("CRBSA");
        pnlCriteres.add(CRBSA, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- CRCSA ----
        CRCSA.setName("CRCSA");
        pnlCriteres.add(CRCSA, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- CRDSA ----
        CRDSA.setName("CRDSA");
        pnlCriteres.add(CRDSA, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbSemainesStockMini ----
        lbSemainesStockMini.setText("Nombre de semaines de consommation pour calcul du stock minimum");
        lbSemainesStockMini.setMaximumSize(new Dimension(250, 30));
        lbSemainesStockMini.setMinimumSize(new Dimension(250, 30));
        lbSemainesStockMini.setPreferredSize(new Dimension(250, 30));
        lbSemainesStockMini.setName("lbSemainesStockMini");
        pnlCriteres.add(lbSemainesStockMini, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- CRNSCM ----
        CRNSCM.setName("CRNSCM");
        pnlCriteres.add(CRNSCM, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- CRASCM ----
        CRASCM.setName("CRASCM");
        pnlCriteres.add(CRASCM, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- CRBSCM ----
        CRBSCM.setName("CRBSCM");
        pnlCriteres.add(CRBSCM, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- CRCSCM ----
        CRCSCM.setName("CRCSCM");
        pnlCriteres.add(CRCSCM, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- CRDSCM ----
        CRDSCM.setName("CRDSCM");
        pnlCriteres.add(CRDSCM, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbSemainesAnalyse3 ----
        lbSemainesAnalyse3.setText("Nombre de semaines de consommation pour calcul du lot de commandes");
        lbSemainesAnalyse3.setMaximumSize(new Dimension(500, 30));
        lbSemainesAnalyse3.setMinimumSize(new Dimension(500, 30));
        lbSemainesAnalyse3.setPreferredSize(new Dimension(500, 30));
        lbSemainesAnalyse3.setName("lbSemainesAnalyse3");
        pnlCriteres.add(lbSemainesAnalyse3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- CRNSCL ----
        CRNSCL.setName("CRNSCL");
        pnlCriteres.add(CRNSCL, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- CRASCL ----
        CRASCL.setName("CRASCL");
        pnlCriteres.add(CRASCL, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- CRBSCL ----
        CRBSCL.setName("CRBSCL");
        pnlCriteres.add(CRBSCL, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- CRCSCL ----
        CRCSCL.setName("CRCSCL");
        pnlCriteres.add(CRCSCL, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- CRDSCL ----
        CRDSCL.setName("CRDSCL");
        pnlCriteres.add(CRDSCL, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCriteres,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlReappro;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNLabelChamp lbRegroupement;
  private SNPanel pnlRegroupement;
  private SNTexte tfRegroupement;
  private SNLabelUnite lbLibelleRegroupement;
  private SNPanel pnlCriteres;
  private SNLabelTitre lbClasses;
  private SNLabelTitre lbN;
  private SNLabelTitre lbA;
  private SNLabelTitre lbB;
  private SNLabelTitre lbC;
  private SNLabelTitre lbD;
  private SNLabelChamp lbSemainesAnalyse;
  private XRiTextField CRNSA;
  private XRiTextField CRASA;
  private XRiTextField CRBSA;
  private XRiTextField CRCSA;
  private XRiTextField CRDSA;
  private SNLabelChamp lbSemainesStockMini;
  private XRiTextField CRNSCM;
  private XRiTextField CRASCM;
  private XRiTextField CRBSCM;
  private XRiTextField CRCSCM;
  private XRiTextField CRDSCM;
  private SNLabelChamp lbSemainesAnalyse3;
  private XRiTextField CRNSCL;
  private XRiTextField CRASCL;
  private XRiTextField CRBSCL;
  private XRiTextField CRCSCL;
  private XRiTextField CRDSCL;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
