/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.vgam.VGAM3CFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JScrollPane;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * Détail d'un article importé dans l'import des catalogues fournisseurs.
 */
public class VGAM3CFM_B1 extends SNPanelEcranRPG implements ioFrame {
  private static final String[] WTP01_TOP = null;
  private static final String[] WTP01_TITLE = { "Zone      Valeur                                            ", };
  private static final String[][] WTP01_DATA = { { "R01", }, { "R02", }, { "R03", }, { "R04", }, { "R05", }, { "R06", }, { "R07", },
      { "R08", }, { "R09", }, { "R10", }, { "R11", }, { "R12", }, };
  private static final int[] WTP01_WIDTH = { 200, };
  
  public VGAM3CFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    initDiverses();
    
    // Renseigner le titre de la boîte de dialogue
    setTitle("Détail d'un article importé");
    
    // Configurer les valeurs des champs
    R01.setAspectTable(WTP01_TOP, WTP01_TITLE, WTP01_DATA, WTP01_WIDTH, false, null, null, null, null);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Afficher les erreurs dans une boîte de dialogue
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  // Méthode évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelContenu();
    pnlListe = new SNPanel();
    SCROLLPANE_LIST = new JScrollPane();
    R01 = new XRiTable();
    pnlBouton = new SNPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(550, 315));
    setPreferredSize(new Dimension(550, 315));
    setBackground(new Color(239, 239, 222));
    setMaximumSize(new Dimension(550, 315));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      //======== pnlListe ========
      {
        pnlListe.setName("pnlListe");
        pnlListe.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlListe.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlListe.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlListe.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlListe.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setMinimumSize(new Dimension(300, 222));
          SCROLLPANE_LIST.setPreferredSize(new Dimension(800, 222));
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- R01 ----
          R01.setName("R01");
          SCROLLPANE_LIST.setViewportView(R01);
        }
        pnlListe.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlBouton ========
        {
          pnlBouton.setBorder(null);
          pnlBouton.setName("pnlBouton");
          pnlBouton.setLayout(new GridLayout(2, 1, 5, 5));

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          pnlBouton.add(BT_PGUP);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          pnlBouton.add(BT_PGDOWN);
        }
        pnlListe.add(pnlBouton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlListe, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private SNPanelContenu pnlPrincipal;
  private SNPanel pnlListe;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable R01;
  private SNPanel pnlBouton;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
