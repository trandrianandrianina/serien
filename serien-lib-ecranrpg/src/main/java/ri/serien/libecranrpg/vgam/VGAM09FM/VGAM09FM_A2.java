
package ri.serien.libecranrpg.vgam.VGAM09FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * [GAM13] Gestion des achats -> Fiches permanentes -> Adresses fournisseurs
 * Indicateur : 001000000
 * Titre :Bloc adresse (Boites dialogue)
 */
public class VGAM09FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, };
  private int[] _WTP01_Width = { 543, };
  private String BOUTON_ACCES_CONTACT = "Accéder aux contacts";
  private String BOUTON_CREATION = "Créer";
  private String BOUTON_MODIFIER = "Modifier";
  
  public VGAM09FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(BOUTON_ACCES_CONTACT, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_CREATION, 't', true);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER, 'c', false);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle("Liste des adresses fournisseurs");
    
    // Vérification afin d'activer le bouton modifier
    if (WTP01.getSelectedRowCount() != 0) {
      snBarreBouton.activerBouton(BOUTON_MODIFIER, true);
    }
    else {
      snBarreBouton.activerBouton(BOUTON_MODIFIER, false);
    }
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    // Traite les clique des bouton spéciaux
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_ACCES_CONTACT)) {
        lexique.HostScreenSendKey(this, "F20");
      }
      else if (pSNBouton.isBouton(BOUTON_CREATION)) {
        lexique.HostScreenSendKey(this, "F13");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER)) {
        lexique.HostScreenSendKey(this, "F14");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.getSelectedRowCount() != 0) {
      if (WTP01.getValueAt(WTP01.getSelectedRow(), WTP01.getSelectedColumn()).toString().trim().isEmpty()) {
        snBarreBouton.activerBouton(BOUTON_MODIFIER, false);
      }
      else {
        snBarreBouton.activerBouton(BOUTON_MODIFIER, true);
      }
    }
    else {
      snBarreBouton.activerBouton(BOUTON_MODIFIER, false);
    }
    
    if (WTP01.doubleClicSelection(e)) {
      if (WTP01.getValueAt(WTP01.getSelectedRow(), WTP01.getSelectedColumn()).toString().trim().isEmpty()) {
        // ne rien faire si la selection est vide
      }
      else {
        lexique.HostScreenSendKey(this, "ENTER");
      }
    }
  }
  
  private void miChoisirActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void miModifierActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void miAnnulerActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void miInterrogerActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void miAideEnLigneActionPerformed(ActionEvent e) {
    lexique.WatchHelp(pmBTD.getInvoker().getName());
  }
  
  private void miInviteActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(pmBTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlBlocAdresseFournisseur = new SNPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    pmBTD = new JPopupMenu();
    miChoisir = new JMenuItem();
    miModifier = new JMenuItem();
    miAnnuler = new JMenuItem();
    miInterroger = new JMenuItem();
    miAideEnLigne = new JMenuItem();
    miInivte = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(1000, 230));
    setPreferredSize(new Dimension(1000, 230));
    setMaximumSize(new Dimension(1000, 230));
    setName("this");
    setLayout(new BorderLayout());

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlBlocAdresseFournisseur ========
      {
        pnlBlocAdresseFournisseur.setOpaque(false);
        pnlBlocAdresseFournisseur.setName("pnlBlocAdresseFournisseur");
        pnlBlocAdresseFournisseur.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlBlocAdresseFournisseur.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlBlocAdresseFournisseur.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlBlocAdresseFournisseur.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlBlocAdresseFournisseur.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setComponentPopupMenu(pmBTD);
          SCROLLPANE_LIST.setPreferredSize(new Dimension(452, 138));
          SCROLLPANE_LIST.setMinimumSize(new Dimension(21, 138));
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- WTP01 ----
          WTP01.setComponentPopupMenu(pmBTD);
          WTP01.setPreferredSize(new Dimension(543, 110));
          WTP01.setName("WTP01");
          WTP01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTP01MouseClicked(e);
            }
          });
          SCROLLPANE_LIST.setViewportView(WTP01);
        }
        pnlBlocAdresseFournisseur.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setPreferredSize(new Dimension(28, 65));
        BT_PGUP.setMinimumSize(new Dimension(28, 65));
        BT_PGUP.setMaximumSize(new Dimension(28, 65));
        BT_PGUP.setName("BT_PGUP");
        pnlBlocAdresseFournisseur.add(BT_PGUP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setToolTipText("Page suivante");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setPreferredSize(new Dimension(28, 65));
        BT_PGDOWN.setMinimumSize(new Dimension(28, 65));
        BT_PGDOWN.setMaximumSize(new Dimension(28, 65));
        BT_PGDOWN.setName("BT_PGDOWN");
        pnlBlocAdresseFournisseur.add(BT_PGDOWN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlBlocAdresseFournisseur);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //======== pmBTD ========
    {
      pmBTD.setName("pmBTD");

      //---- miChoisir ----
      miChoisir.setText("Choisir");
      miChoisir.setName("miChoisir");
      miChoisir.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoisirActionPerformed(e);
        }
      });
      pmBTD.add(miChoisir);

      //---- miModifier ----
      miModifier.setText("Modifier");
      miModifier.setName("miModifier");
      miModifier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miModifierActionPerformed(e);
        }
      });
      pmBTD.add(miModifier);

      //---- miAnnuler ----
      miAnnuler.setText("Annuler");
      miAnnuler.setName("miAnnuler");
      miAnnuler.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAnnulerActionPerformed(e);
        }
      });
      pmBTD.add(miAnnuler);

      //---- miInterroger ----
      miInterroger.setText("Interroger");
      miInterroger.setName("miInterroger");
      miInterroger.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miInterrogerActionPerformed(e);
        }
      });
      pmBTD.add(miInterroger);
      pmBTD.addSeparator();

      //---- miAideEnLigne ----
      miAideEnLigne.setText("Aide en ligne");
      miAideEnLigne.setName("miAideEnLigne");
      miAideEnLigne.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAideEnLigneActionPerformed(e);
        }
      });
      pmBTD.add(miAideEnLigne);

      //---- miInivte ----
      miInivte.setText("Invite");
      miInivte.setName("miInivte");
      miInivte.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miInviteActionPerformed(e);
        }
      });
      pmBTD.add(miInivte);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlBlocAdresseFournisseur;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu pmBTD;
  private JMenuItem miChoisir;
  private JMenuItem miModifier;
  private JMenuItem miAnnuler;
  private JMenuItem miInterroger;
  private JMenuItem miAideEnLigne;
  private JMenuItem miInivte;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
