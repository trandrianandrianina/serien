
package ri.serien.libecranrpg.vgam.VGAM20FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.table.JTableHeader;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GAM284] Gestion des achats -> Documents d'achats -> Editions factures fournisseurs -> Analyse des frais
 * Indicateur : 00000000
 * Titre :Analyse des imputations de frais
 * 
 */
public class VGAM20FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private Message V03F = null;
  
  private String[] _EFL01_Title = { "Type  de  frais", "<html>Frais sur<br>facture saisis</html>",
      "<html>Frais sur<br>facture répartis</html>", "<html>Frais sur<br>facture calculés</html>", "<html>Frais<br>internes</html>", };
  private String[][] _EFL01_Data = { { "EFLB1", "WF101", "WF201", "WF301", "WF401", }, { "EFLB2", "WF102", "WF202", "WF302", "WF402", },
      { "EFLB3", "WF103", "WF203", "WF303", "WF403", }, { "EFLB4", "WF104", "WF204", "WF304", "WF404", },
      { "EFLB5", "WF105", "WF205", "WF305", "WF405", }, { "EFLB6", "WF106", "WF206", "WF306", "WF406", },
      { "EFLB7", "WF107", "WF207", "WF307", "WF407", }, { "EFLB8", "WF108", "WF208", "WF308", "WF408", },
      { "EFLB9", "WF109", "WF209", "WF309", "WF409", }, { "EFLB10", "WF110", "WF210", "WF310", "WF410", },
      { "EFLB11", "WF111", "WF211", "WF311", "WF411", }, { "EFLB12", "WF112", "WF212", "WF312", "WF412", },
      { "EFLB13", "WF113", "WF213", "WF313", "WF413", }, { "EFLB14", "WF114", "WF214", "WF314", "WF414", },
      { "EFLB15", "WF115", "WF215", "WF315", "WF415", }, { "EFLB16", "WF116", "WF216", "WF316", "WF416", },
      { "EFLB17", "WF117", "WF217", "WF317", "WF417", }, { "EFLB18", "WF118", "WF218", "WF318", "WF418", },
      { "EFLB19", "WF119", "WF219", "WF319", "WF419", }, { "EFLB20", "WF120", "WF220", "WF320", "WF420", },
      { "Total des frais", "WF121", "WF221", "WF321", "WF421", }, };
  private int[] _EFL01_Width = { 160, 80, 81, 85, 75, };
  
  public VGAM20FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WF101.setAspectTable(null, _EFL01_Title, _EFL01_Data, _EFL01_Width, false, null, null, null, null);
    
    JTableHeader header = WF101.getTableHeader();
    header.setPreferredSize(new Dimension(100, 35));
    WF101.add(header);
    
    xriBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    xriBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    xriBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
        xriBarreBouton.isTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    lbVO3F.setVisible(!lexique.HostFieldGetData("V03F").trim().isEmpty());
    V03F = V03F.getMessageImportant(lexique.HostFieldGetData("V03F").trim());
    lbVO3F.setMessage(V03F);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité des composant
    WHTCAF.setVisible(!lexique.HostFieldGetData("WHTCAF").trim().isEmpty());
    lbBaseCAF.setVisible(WHTCAF.isVisible());
    WCOEF.setVisible(!lexique.HostFieldGetData("WCOEF").trim().isEmpty());
    lbEgale.setVisible(WCOEF.isVisible());
    WMHT04.setVisible(!lexique.HostFieldGetData("WMHT04").trim().isEmpty());
    lbSlash.setVisible(WMHT04.isVisible());
    WMHT03.setVisible(!lexique.HostFieldGetData("WMHT03").trim().isEmpty());
    lbCoefTheorique.setVisible(WMHT03.isVisible());
    WMHT02.setVisible(!lexique.HostFieldGetData("WMHT02").trim().isEmpty());
    lbEcart.setVisible(WMHT02.isVisible());
    WMHT01.setVisible(!lexique.HostFieldGetData("WMHT01").trim().isEmpty());
    lbMontantHtMarchandise.setVisible(WMHT01.isVisible());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlObjetAnalyse = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbDossier = new SNLabelChamp();
    WDOS = new XRiTextField();
    lbContainer = new SNLabelChamp();
    WCNT = new XRiTextField();
    lbVO3F = new SNMessage();
    pnlRecapitualitf = new SNPanelTitre();
    lbMontantHtMarchandise = new SNLabelChamp();
    WMHT01 = new XRiTextField();
    lbBaseCAF = new SNLabelChamp();
    WHTCAF = new XRiTextField();
    lbEcart = new SNLabelChamp();
    WMHT02 = new XRiTextField();
    lbCoefTheorique = new SNLabelChamp();
    WMHT03 = new XRiTextField();
    lbSlash = new SNLabelChamp();
    WMHT04 = new XRiTextField();
    lbEgale = new SNLabelChamp();
    WCOEF = new XRiTextField();
    pnlDroite = new SNPanel();
    lbImputationFrais = new SNLabelTitre();
    pnlImputationFrais = new SNPanel();
    scpScrollTableau = new JScrollPane();
    WF101 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    xriBarreBouton = new XRiBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Analyse des imputations de frais");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout(1, 2, 5, 5));
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlObjetAnalyse ========
        {
          pnlObjetAnalyse.setTitre("Objet de l'analyse");
          pnlObjetAnalyse.setName("pnlObjetAnalyse");
          pnlObjetAnalyse.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlObjetAnalyse.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlObjetAnalyse.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlObjetAnalyse.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlObjetAnalyse.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlObjetAnalyse.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setEnabled(false);
          snEtablissement.setName("snEtablissement");
          pnlObjetAnalyse.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbDossier ----
          lbDossier.setText("Dossier");
          lbDossier.setName("lbDossier");
          pnlObjetAnalyse.add(lbDossier, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WDOS ----
          WDOS.setPreferredSize(new Dimension(92, 30));
          WDOS.setMinimumSize(new Dimension(92, 30));
          WDOS.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDOS.setName("WDOS");
          pnlObjetAnalyse.add(WDOS, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbContainer ----
          lbContainer.setText("Container");
          lbContainer.setName("lbContainer");
          pnlObjetAnalyse.add(lbContainer, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WCNT ----
          WCNT.setFont(new Font("sansserif", Font.PLAIN, 14));
          WCNT.setPreferredSize(new Dimension(164, 30));
          WCNT.setMinimumSize(new Dimension(164, 30));
          WCNT.setMaximumSize(new Dimension(164, 30));
          WCNT.setName("WCNT");
          pnlObjetAnalyse.add(WCNT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbVO3F ----
          lbVO3F.setText("V03F (voir texte dans le code)");
          lbVO3F.setName("lbVO3F");
          pnlObjetAnalyse.add(lbVO3F, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlObjetAnalyse, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlRecapitualitf ========
        {
          pnlRecapitualitf.setTitre("R\u00e9capitulatif");
          pnlRecapitualitf.setName("pnlRecapitualitf");
          pnlRecapitualitf.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRecapitualitf.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlRecapitualitf.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlRecapitualitf.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRecapitualitf.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbMontantHtMarchandise ----
          lbMontantHtMarchandise.setText("Montant d'achats net HT");
          lbMontantHtMarchandise.setMinimumSize(new Dimension(190, 30));
          lbMontantHtMarchandise.setMaximumSize(new Dimension(190, 30));
          lbMontantHtMarchandise.setPreferredSize(new Dimension(190, 30));
          lbMontantHtMarchandise.setName("lbMontantHtMarchandise");
          pnlRecapitualitf.add(lbMontantHtMarchandise, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WMHT01 ----
          WMHT01.setFont(new Font("sansserif", Font.PLAIN, 14));
          WMHT01.setPreferredSize(new Dimension(100, 30));
          WMHT01.setMinimumSize(new Dimension(100, 30));
          WMHT01.setMaximumSize(new Dimension(100, 30));
          WMHT01.setName("WMHT01");
          pnlRecapitualitf.add(WMHT01, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbBaseCAF ----
          lbBaseCAF.setText("Base co\u00fbt assurance frais");
          lbBaseCAF.setName("lbBaseCAF");
          pnlRecapitualitf.add(lbBaseCAF, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WHTCAF ----
          WHTCAF.setFont(new Font("sansserif", Font.PLAIN, 14));
          WHTCAF.setPreferredSize(new Dimension(100, 30));
          WHTCAF.setMinimumSize(new Dimension(100, 30));
          WHTCAF.setMaximumSize(new Dimension(100, 30));
          WHTCAF.setName("WHTCAF");
          pnlRecapitualitf.add(WHTCAF, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbEcart ----
          lbEcart.setText("Ecart");
          lbEcart.setName("lbEcart");
          pnlRecapitualitf.add(lbEcart, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WMHT02 ----
          WMHT02.setFont(new Font("sansserif", Font.PLAIN, 14));
          WMHT02.setMaximumSize(new Dimension(100, 30));
          WMHT02.setMinimumSize(new Dimension(100, 30));
          WMHT02.setPreferredSize(new Dimension(100, 30));
          WMHT02.setName("WMHT02");
          pnlRecapitualitf.add(WMHT02, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbCoefTheorique ----
          lbCoefTheorique.setText("Coef th\u00e9orique prix de revient");
          lbCoefTheorique.setPreferredSize(new Dimension(188, 30));
          lbCoefTheorique.setMinimumSize(new Dimension(188, 30));
          lbCoefTheorique.setMaximumSize(new Dimension(188, 30));
          lbCoefTheorique.setName("lbCoefTheorique");
          pnlRecapitualitf.add(lbCoefTheorique, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WMHT03 ----
          WMHT03.setFont(new Font("sansserif", Font.PLAIN, 14));
          WMHT03.setName("WMHT03");
          pnlRecapitualitf.add(WMHT03, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbSlash ----
          lbSlash.setText("/");
          lbSlash.setPreferredSize(new Dimension(4, 30));
          lbSlash.setMinimumSize(new Dimension(4, 30));
          lbSlash.setMaximumSize(new Dimension(4, 30));
          lbSlash.setName("lbSlash");
          pnlRecapitualitf.add(lbSlash, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WMHT04 ----
          WMHT04.setPreferredSize(new Dimension(100, 30));
          WMHT04.setMinimumSize(new Dimension(100, 30));
          WMHT04.setMaximumSize(new Dimension(100, 30));
          WMHT04.setFont(new Font("sansserif", Font.PLAIN, 14));
          WMHT04.setName("WMHT04");
          pnlRecapitualitf.add(WMHT04, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbEgale ----
          lbEgale.setText("=");
          lbEgale.setPreferredSize(new Dimension(8, 30));
          lbEgale.setMinimumSize(new Dimension(8, 30));
          lbEgale.setMaximumSize(new Dimension(8, 30));
          lbEgale.setName("lbEgale");
          pnlRecapitualitf.add(lbEgale, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WCOEF ----
          WCOEF.setMinimumSize(new Dimension(80, 30));
          WCOEF.setPreferredSize(new Dimension(80, 30));
          WCOEF.setMaximumSize(new Dimension(80, 30));
          WCOEF.setFont(new Font("sansserif", Font.PLAIN, 14));
          WCOEF.setName("WCOEF");
          pnlRecapitualitf.add(WCOEF, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlRecapitualitf, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbImputationFrais ----
        lbImputationFrais.setText("Analyse des imputations de frais");
        lbImputationFrais.setName("lbImputationFrais");
        pnlDroite.add(lbImputationFrais, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlImputationFrais ========
        {
          pnlImputationFrais.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlImputationFrais.setName("pnlImputationFrais");
          pnlImputationFrais.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlImputationFrais.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlImputationFrais.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlImputationFrais.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlImputationFrais.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== scpScrollTableau ========
          {
            scpScrollTableau.setPreferredSize(new Dimension(487, 379));
            scpScrollTableau.setMinimumSize(new Dimension(21, 379));
            scpScrollTableau.setName("scpScrollTableau");
            
            // ---- WF101 ----
            WF101.setPreferredSize(new Dimension(487, 338));
            WF101.setMinimumSize(new Dimension(75, 338));
            WF101.setName("WF101");
            scpScrollTableau.setViewportView(WF101);
          }
          pnlImputationFrais.add(scpScrollTableau, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setPreferredSize(new Dimension(28, 185));
          BT_PGUP.setMinimumSize(new Dimension(28, 185));
          BT_PGUP.setMaximumSize(new Dimension(28, 185));
          BT_PGUP.setName("BT_PGUP");
          pnlImputationFrais.add(BT_PGUP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setPreferredSize(new Dimension(28, 185));
          BT_PGDOWN.setMinimumSize(new Dimension(28, 185));
          BT_PGDOWN.setMaximumSize(new Dimension(28, 185));
          BT_PGDOWN.setName("BT_PGDOWN");
          pnlImputationFrais.add(BT_PGDOWN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlImputationFrais, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- xriBarreBouton ----
    xriBarreBouton.setName("xriBarreBouton");
    add(xriBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlObjetAnalyse;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbDossier;
  private XRiTextField WDOS;
  private SNLabelChamp lbContainer;
  private XRiTextField WCNT;
  private SNMessage lbVO3F;
  private SNPanelTitre pnlRecapitualitf;
  private SNLabelChamp lbMontantHtMarchandise;
  private XRiTextField WMHT01;
  private SNLabelChamp lbBaseCAF;
  private XRiTextField WHTCAF;
  private SNLabelChamp lbEcart;
  private XRiTextField WMHT02;
  private SNLabelChamp lbCoefTheorique;
  private XRiTextField WMHT03;
  private SNLabelChamp lbSlash;
  private XRiTextField WMHT04;
  private SNLabelChamp lbEgale;
  private XRiTextField WCOEF;
  private SNPanel pnlDroite;
  private SNLabelTitre lbImputationFrais;
  private SNPanel pnlImputationFrais;
  private JScrollPane scpScrollTableau;
  private XRiTable WF101;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiBarreBouton xriBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
