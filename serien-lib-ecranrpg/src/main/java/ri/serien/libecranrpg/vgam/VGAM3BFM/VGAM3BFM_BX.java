/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.vgam.VGAM3BFM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * Boîte de dialogue pour saisir les paramètres de modifications d'articles suite à l'import d'un catalogue fournisseur.
 */
public class VGAM3BFM_BX extends SNPanelEcranRPG implements ioFrame {
  
  public VGAM3BFM_BX(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    setDialog(true);
    
    // Renseigner le titre de la boîte de dialogue
    setTitle("Importer des articles en modification");
    
    // Configurer les valeurs des champs
    REPLIB.setValeursSelection("OUI", "NON");
    REPAUT.setValeursSelection("OUI", "NON");
    REPTAR.setValeursSelection("OUI", "NON");
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Ne pas afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Afficher les erreurs dans une boîte de dialogue
    gererLesErreurs("19");
    
    // Message d'erreur
    MESERR.setVisible(lexique.isTrue("19"));
    if (!lexique.HostFieldGetData("MESERR").trim().isEmpty()) {
      Message messageErreur = Message.getMessageImportant(lexique.HostFieldGetData("MESERR"));
    }
    
    // Griser la saisie de la date des tarifs de ventes si les tarifs de ventes de sont pas mis à jour
    if (REPTAR.isSelected()) {
      DATCRV.setEnabled(true);
    }
    else {
      DATCRV.setEnabled(false);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  // Méthode évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void creationDateVenteActionPerformed(ActionEvent e) {
    if (REPTAR.isSelected()) {
      DATCRV.setEnabled(true);
    }
    else {
      DATCRV.setEnabled(false);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlIntegration = new SNPanelTitre();
    lbDateTarifAchat = new SNLabelChamp();
    DATCRE = new XRiCalendrier();
    REPLIB = new XRiCheckBox();
    REPAUT = new XRiCheckBox();
    REPTAR = new XRiCheckBox();
    lbDateTarifVente = new SNLabelChamp();
    DATCRV = new XRiCalendrier();
    MESERR = new SNMessage();
    
    // ======== this ========
    setMinimumSize(new Dimension(450, 330));
    setPreferredSize(new Dimension(450, 330));
    setMaximumSize(new Dimension(450, 330));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
      
      // ======== pnlIntegration ========
      {
        pnlIntegration.setTitre("Int\u00e9gration catalogue");
        pnlIntegration.setName("pnlIntegration");
        pnlIntegration.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlIntegration.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlIntegration.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlIntegration.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlIntegration.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbDateTarifAchat ----
        lbDateTarifAchat.setText("Date de cr\u00e9ation du tarif d'achat");
        lbDateTarifAchat.setMaximumSize(new Dimension(250, 30));
        lbDateTarifAchat.setMinimumSize(new Dimension(250, 30));
        lbDateTarifAchat.setPreferredSize(new Dimension(250, 30));
        lbDateTarifAchat.setName("lbDateTarifAchat");
        pnlIntegration.add(lbDateTarifAchat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- DATCRE ----
        DATCRE.setMaximumSize(new Dimension(110, 30));
        DATCRE.setMinimumSize(new Dimension(110, 30));
        DATCRE.setPreferredSize(new Dimension(110, 30));
        DATCRE.setName("DATCRE");
        pnlIntegration.add(DATCRE, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- REPLIB ----
        REPLIB.setText("Modification des libell\u00e9s");
        REPLIB.setFont(new Font("sansserif", Font.PLAIN, 14));
        REPLIB.setMaximumSize(new Dimension(200, 30));
        REPLIB.setMinimumSize(new Dimension(200, 30));
        REPLIB.setPreferredSize(new Dimension(200, 30));
        REPLIB.setName("REPLIB");
        pnlIntegration.add(REPLIB, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- REPAUT ----
        REPAUT.setText("Modification des autres informations");
        REPAUT.setFont(new Font("sansserif", Font.PLAIN, 14));
        REPAUT.setPreferredSize(new Dimension(200, 30));
        REPAUT.setMinimumSize(new Dimension(200, 30));
        REPAUT.setMaximumSize(new Dimension(200, 30));
        REPAUT.setName("REPAUT");
        pnlIntegration.add(REPAUT, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- REPTAR ----
        REPTAR.setText("Cr\u00e9ation du tarif de vente");
        REPTAR.setFont(new Font("sansserif", Font.PLAIN, 14));
        REPTAR.setPreferredSize(new Dimension(200, 30));
        REPTAR.setMinimumSize(new Dimension(200, 30));
        REPTAR.setMaximumSize(new Dimension(200, 30));
        REPTAR.setName("REPTAR");
        REPTAR.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            creationDateVenteActionPerformed(e);
          }
        });
        pnlIntegration.add(REPTAR, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDateTarifVente ----
        lbDateTarifVente.setText("Date de cr\u00e9ation du tarif de vente");
        lbDateTarifVente.setMaximumSize(new Dimension(50, 30));
        lbDateTarifVente.setMinimumSize(new Dimension(50, 30));
        lbDateTarifVente.setPreferredSize(new Dimension(50, 30));
        lbDateTarifVente.setName("lbDateTarifVente");
        pnlIntegration.add(lbDateTarifVente, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- DATCRV ----
        DATCRV.setMaximumSize(new Dimension(110, 30));
        DATCRV.setMinimumSize(new Dimension(110, 30));
        DATCRV.setPreferredSize(new Dimension(110, 30));
        DATCRV.setName("DATCRV");
        pnlIntegration.add(DATCRV, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- MESERR ----
        MESERR.setText("MESERR");
        MESERR.setName("MESERR");
        pnlIntegration.add(MESERR, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlIntegration,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlIntegration;
  private SNLabelChamp lbDateTarifAchat;
  private XRiCalendrier DATCRE;
  private XRiCheckBox REPLIB;
  private XRiCheckBox REPAUT;
  private XRiCheckBox REPTAR;
  private SNLabelChamp lbDateTarifVente;
  private XRiCalendrier DATCRV;
  private SNMessage MESERR;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
