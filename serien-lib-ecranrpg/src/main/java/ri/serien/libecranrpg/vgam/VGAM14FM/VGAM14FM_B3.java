
package ri.serien.libecranrpg.vgam.VGAM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM14FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _LIGSOR01_Title = { "TIT1", "Total/année", };
  private String[][] _LIGSOR01_Data = { { "LIGSOR01", "SORAN", }, { "LIGENT01", "ENTAN", }, };
  private int[] _LIGSOR01_Width = { 519, 75, };
  // private String[] _OBJ_91_Top=null;
  
  public VGAM14FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    LIGSOR01.setAspectTable(null, _LIGSOR01_Title, _LIGSOR01_Data, _LIGSOR01_Width, true, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDMAG@")).trim());
    INDAR0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDAR0@")).trim());
    A1FAM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1FAM@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    CANUA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CANUA@")).trim());
    CAQMI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAQMI@")).trim());
    OBJ_136.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADELS@")).trim());
    OBJ_128.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADEL@")).trim());
    OBJ_138.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WECAM1@")).trim());
    QTESUI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTESUI@")).trim());
    OBJ_132.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNA@")).trim());
    OBJ_122.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNC@")).trim());
    xTitledPanel3.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    OBJ_104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKX@")).trim());
    OBJ_110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    OBJ_106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTX@")).trim());
    OBJ_108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRESX@")).trim());
    OBJ_112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRFRSG@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBFRS@")).trim());
    OBJ_69.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRPNUM@")).trim());
    OBJ_70.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRPNOM@")).trim());
    OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAREF@")).trim());
    OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CALIBR@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPRAX@")).trim());
    CADAPX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADAPX@")).trim());
    CADAFX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADAFX@")).trim());
    DAPSUI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DAPSUI@")).trim());
    DAFSUI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DAFSUI@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( OBJ_91, OBJ_91.get_LIST_Title_Data_Brut(), _OBJ_91_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    boolean isConsult = lexique.isTrue("53");
    riSousMenu6.setEnabled(!isConsult);
    riSousMenu11.setEnabled(!isConsult);
    riSousMenu14.setEnabled(!isConsult);
    
    
    OBJ_132.setVisible(lexique.isPresent("CAUNA"));
    OBJ_122.setVisible(lexique.isPresent("CAUNC"));
    OBJ_112.setVisible(lexique.isPresent("A1UNS"));
    INDMAG.setEnabled(lexique.isPresent("INDMAG"));
    OBJ_136.setVisible(lexique.isPresent("CADELS"));
    OBJ_128.setVisible(lexique.isPresent("CADEL"));
    OBJ_138.setVisible(lexique.isPresent("WECAM1"));
    INDETB.setEnabled(lexique.isPresent("INDETB"));
    OBJ_69.setVisible(lexique.isPresent("FRPNUM"));
    OBJ_56.setVisible(lexique.isPresent("FRFRSG"));
    // LRDTRX.setEnabled( lexique.isPresent("LRDTRX"));
    LRQTAX.setEnabled(lexique.isPresent("LRQTAX"));
    LRQTCX.setEnabled(lexique.isPresent("LRQTCX"));
    CAQMI.setVisible(lexique.isPresent("CAQMI"));
    CANUA.setVisible(lexique.isPresent("CANUA"));
    OBJ_108.setVisible(lexique.isPresent("WRESX"));
    OBJ_106.setVisible(lexique.isPresent("WATTX"));
    OBJ_54.setVisible(lexique.HostFieldGetData("CAIN1").equalsIgnoreCase("P"));
    OBJ_110.setVisible(lexique.isPresent("WDISX"));
    LRPASX.setEnabled(lexique.isPresent("LRPASX"));
    OBJ_62.setVisible(lexique.isPresent("WPRAX"));
    OBJ_104.setVisible(lexique.isPresent("WSTKX"));
    OBJ_81.setVisible(!lexique.HostFieldGetData("V01F").trim().equalsIgnoreCase("INTERRO.") & lexique.isPresent("FRPNUM"));
    OBJ_65.setVisible(lexique.isPresent("ATTE"));
    INDAR0.setEnabled(lexique.isPresent("INDAR0"));
    OBJ_57.setVisible(lexique.isPresent("ULBFRS"));
    OBJ_117.setVisible(QTESUI.isVisible());
    XLLIB4.setEnabled(lexique.isPresent("XLLIB4"));
    XLLIB3.setEnabled(lexique.isPresent("XLLIB3"));
    XLLIB2.setEnabled(lexique.isPresent("XLLIB2"));
    XLLIB1.setEnabled(lexique.isPresent("XLLIB1"));
    OBJ_70.setVisible(lexique.isPresent("FRPNOM"));
    OBJ_78.setVisible(lexique.isPresent("CALIBR"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  REAPPROVISIONNEMENT"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(1, 1);
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(14, 1);
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_81ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("FRPTOP", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_41 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_38 = new JLabel();
    INDMAG = new RiZoneSortie();
    OBJ_39 = new JLabel();
    INDAR0 = new RiZoneSortie();
    OBJ_46 = new JLabel();
    A1FAM = new RiZoneSortie();
    OBJ_52 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    XLLIB1 = new XRiTextField();
    XLLIB3 = new XRiTextField();
    XLLIB2 = new XRiTextField();
    XLLIB4 = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_120 = new JLabel();
    OBJ_130 = new JLabel();
    LRQTCX = new XRiTextField();
    LRQTAX = new XRiTextField();
    OBJ_123 = new JLabel();
    OBJ_133 = new JLabel();
    LRDTRX = new XRiCalendrier();
    CANUA = new RiZoneSortie();
    OBJ_125 = new JLabel();
    CAQMI = new RiZoneSortie();
    OBJ_127 = new JLabel();
    OBJ_135 = new JLabel();
    OBJ_136 = new RiZoneSortie();
    OBJ_128 = new RiZoneSortie();
    OBJ_119 = new JLabel();
    OBJ_138 = new RiZoneSortie();
    OBJ_137 = new JLabel();
    QTESUI = new RiZoneSortie();
    OBJ_117 = new JLabel();
    OBJ_132 = new RiZoneSortie();
    OBJ_122 = new RiZoneSortie();
    xTitledPanel3 = new JXTitledPanel();
    SCROLLPANE_OBJ_91 = new JScrollPane();
    LIGSOR01 = new XRiTable();
    OBJ_88 = new JLabel();
    OBJ_107 = new JLabel();
    OBJ_104 = new RiZoneSortie();
    OBJ_110 = new RiZoneSortie();
    OBJ_109 = new JLabel();
    OBJ_106 = new RiZoneSortie();
    OBJ_108 = new RiZoneSortie();
    OBJ_105 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_112 = new RiZoneSortie();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_56 = new RiZoneSortie();
    OBJ_57 = new RiZoneSortie();
    OBJ_65 = new JLabel();
    OBJ_69 = new RiZoneSortie();
    OBJ_70 = new RiZoneSortie();
    OBJ_73 = new RiZoneSortie();
    OBJ_72 = new JLabel();
    OBJ_78 = new RiZoneSortie();
    OBJ_81 = new SNBoutonDetail();
    xTitledPanel5 = new JXTitledPanel();
    OBJ_75 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_62 = new RiZoneSortie();
    LRPASX = new XRiTextField();
    OBJ_54 = new JLabel();
    CADAPX = new RiZoneSortie();
    CADAFX = new RiZoneSortie();
    OBJ_66 = new JLabel();
    DAPSUI = new RiZoneSortie();
    DAFSUI = new RiZoneSortie();
    OBJ_61 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_68 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("R\u00e9approvisionnement");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(1000, 40));
          p_tete_gauche2.setMinimumSize(new Dimension(1000, 40));
          p_tete_gauche2.setName("p_tete_gauche2");

          //---- OBJ_41 ----
          OBJ_41.setText("Article");
          OBJ_41.setName("OBJ_41");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_38 ----
          OBJ_38.setText("Etablissement");
          OBJ_38.setName("OBJ_38");

          //---- INDMAG ----
          INDMAG.setComponentPopupMenu(BTD);
          INDMAG.setOpaque(false);
          INDMAG.setText("@INDMAG@");
          INDMAG.setName("INDMAG");

          //---- OBJ_39 ----
          OBJ_39.setText("Magasin");
          OBJ_39.setName("OBJ_39");

          //---- INDAR0 ----
          INDAR0.setComponentPopupMenu(BTD);
          INDAR0.setOpaque(false);
          INDAR0.setText("@INDAR0@");
          INDAR0.setName("INDAR0");

          //---- OBJ_46 ----
          OBJ_46.setText("Famille");
          OBJ_46.setName("OBJ_46");

          //---- A1FAM ----
          A1FAM.setComponentPopupMenu(BTD);
          A1FAM.setOpaque(false);
          A1FAM.setText("@A1FAM@");
          A1FAM.setName("A1FAM");

          //---- OBJ_52 ----
          OBJ_52.setText("@A1LIB@");
          OBJ_52.setOpaque(false);
          OBJ_52.setName("OBJ_52");

          GroupLayout p_tete_gauche2Layout = new GroupLayout(p_tete_gauche2);
          p_tete_gauche2.setLayout(p_tete_gauche2Layout);
          p_tete_gauche2Layout.setHorizontalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gauche2Layout.createParallelGroup()
                  .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                    .addGap(95, 95, 95)
                    .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                .addGap(30, 30, 30)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDMAG, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(A1FAM, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDAR0, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gauche2Layout.setVerticalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDMAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(A1FAM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDAR0, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gauche2Layout.createParallelGroup()
                  .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche2);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Modification stock mini");
              riSousMenu_bt6.setToolTipText("Modification stock minimum");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Liste des CNA");
              riSousMenu_bt9.setToolTipText("Liste des conditions d'achat");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Appel fiche fournisseur");
              riSousMenu_bt10.setToolTipText("Appel fiche fournisseur");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Historique sur 12 mois");
              riSousMenu_bt15.setToolTipText("Historique sur 12 mois");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Attendu / command\u00e9");
              riSousMenu_bt14.setToolTipText("D\u00e9tail attendu / command\u00e9");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");

              //---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Historique de conso.");
              riSousMenu_bt16.setToolTipText("Historique de consommation");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Condition d'achat");
              riSousMenu_bt11.setToolTipText("Condition d'achat");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");

              //---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Autres vues");
              riSousMenu_bt18.setToolTipText("Autres vues");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Bloc-notes CNA");
              riSousMenu_bt8.setToolTipText("Bloc-notes condition d'achat");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(865, 620));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(865, 620));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Commentaires \u00e0 g\u00e9n\u00e9rer sur la commande");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- XLLIB1 ----
            XLLIB1.setComponentPopupMenu(BTD);
            XLLIB1.setName("XLLIB1");
            xTitledPanel1ContentContainer.add(XLLIB1);
            XLLIB1.setBounds(20, 15, 310, XLLIB1.getPreferredSize().height);

            //---- XLLIB3 ----
            XLLIB3.setComponentPopupMenu(BTD);
            XLLIB3.setName("XLLIB3");
            xTitledPanel1ContentContainer.add(XLLIB3);
            XLLIB3.setBounds(20, 40, 310, XLLIB3.getPreferredSize().height);

            //---- XLLIB2 ----
            XLLIB2.setComponentPopupMenu(BTD);
            XLLIB2.setName("XLLIB2");
            xTitledPanel1ContentContainer.add(XLLIB2);
            XLLIB2.setBounds(335, 15, 310, XLLIB2.getPreferredSize().height);

            //---- XLLIB4 ----
            XLLIB4.setComponentPopupMenu(BTD);
            XLLIB4.setName("XLLIB4");
            xTitledPanel1ContentContainer.add(XLLIB4);
            XLLIB4.setBounds(335, 40, 310, XLLIB4.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Quantit\u00e9 \u00e0 commander");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- OBJ_120 ----
            OBJ_120.setText("UC");
            OBJ_120.setName("OBJ_120");
            xTitledPanel2ContentContainer.add(OBJ_120);
            OBJ_120.setBounds(20, 37, 22, 20);

            //---- OBJ_130 ----
            OBJ_130.setText("UA");
            OBJ_130.setName("OBJ_130");
            xTitledPanel2ContentContainer.add(OBJ_130);
            OBJ_130.setBounds(20, 62, 22, 20);

            //---- LRQTCX ----
            LRQTCX.setComponentPopupMenu(BTD);
            LRQTCX.setHorizontalAlignment(SwingConstants.RIGHT);
            LRQTCX.setName("LRQTCX");
            xTitledPanel2ContentContainer.add(LRQTCX);
            LRQTCX.setBounds(45, 33, 100, LRQTCX.getPreferredSize().height);

            //---- LRQTAX ----
            LRQTAX.setComponentPopupMenu(BTD);
            LRQTAX.setHorizontalAlignment(SwingConstants.RIGHT);
            LRQTAX.setName("LRQTAX");
            xTitledPanel2ContentContainer.add(LRQTAX);
            LRQTAX.setBounds(45, 58, 100, LRQTAX.getPreferredSize().height);

            //---- OBJ_123 ----
            OBJ_123.setText("Conditionnement");
            OBJ_123.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_123.setName("OBJ_123");
            xTitledPanel2ContentContainer.add(OBJ_123);
            OBJ_123.setBounds(180, 37, 105, 20);

            //---- OBJ_133 ----
            OBJ_133.setText("Livraison pr\u00e9vue");
            OBJ_133.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_133.setName("OBJ_133");
            xTitledPanel2ContentContainer.add(OBJ_133);
            OBJ_133.setBounds(188, 62, 97, 20);

            //---- LRDTRX ----
            LRDTRX.setComponentPopupMenu(BTD);
            LRDTRX.setName("LRDTRX");
            xTitledPanel2ContentContainer.add(LRDTRX);
            LRDTRX.setBounds(290, 58, 105, LRDTRX.getPreferredSize().height);

            //---- CANUA ----
            CANUA.setText("@CANUA@");
            CANUA.setHorizontalAlignment(SwingConstants.RIGHT);
            CANUA.setName("CANUA");
            xTitledPanel2ContentContainer.add(CANUA);
            CANUA.setBounds(290, 35, 76, CANUA.getPreferredSize().height);

            //---- OBJ_125 ----
            OBJ_125.setText("Minimum");
            OBJ_125.setName("OBJ_125");
            xTitledPanel2ContentContainer.add(OBJ_125);
            OBJ_125.setBounds(380, 37, 60, 20);

            //---- CAQMI ----
            CAQMI.setText("@CAQMI@");
            CAQMI.setHorizontalAlignment(SwingConstants.RIGHT);
            CAQMI.setName("CAQMI");
            xTitledPanel2ContentContainer.add(CAQMI);
            CAQMI.setBounds(440, 35, 92, CAQMI.getPreferredSize().height);

            //---- OBJ_127 ----
            OBJ_127.setText("Delai");
            OBJ_127.setName("OBJ_127");
            xTitledPanel2ContentContainer.add(OBJ_127);
            OBJ_127.setBounds(545, 37, 40, 20);

            //---- OBJ_135 ----
            OBJ_135.setText("Delai2");
            OBJ_135.setName("OBJ_135");
            xTitledPanel2ContentContainer.add(OBJ_135);
            OBJ_135.setBounds(545, 62, 40, 20);

            //---- OBJ_136 ----
            OBJ_136.setText("@CADELS@");
            OBJ_136.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_136.setName("OBJ_136");
            xTitledPanel2ContentContainer.add(OBJ_136);
            OBJ_136.setBounds(590, 60, 28, OBJ_136.getPreferredSize().height);

            //---- OBJ_128 ----
            OBJ_128.setText("@CADEL@");
            OBJ_128.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_128.setName("OBJ_128");
            xTitledPanel2ContentContainer.add(OBJ_128);
            OBJ_128.setBounds(590, 35, 28, OBJ_128.getPreferredSize().height);

            //---- OBJ_119 ----
            OBJ_119.setText("D\u00e9lai constat\u00e9");
            OBJ_119.setName("OBJ_119");
            xTitledPanel2ContentContainer.add(OBJ_119);
            OBJ_119.setBounds(630, 35, 100, 27);

            //---- OBJ_138 ----
            OBJ_138.setText("@WECAM1@");
            OBJ_138.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_138.setName("OBJ_138");
            xTitledPanel2ContentContainer.add(OBJ_138);
            OBJ_138.setBounds(625, 60, 34, OBJ_138.getPreferredSize().height);

            //---- OBJ_137 ----
            OBJ_137.setText("jours");
            OBJ_137.setName("OBJ_137");
            xTitledPanel2ContentContainer.add(OBJ_137);
            OBJ_137.setBounds(665, 62, 40, 20);

            //---- QTESUI ----
            QTESUI.setText("@QTESUI@");
            QTESUI.setHorizontalAlignment(SwingConstants.RIGHT);
            QTESUI.setName("QTESUI");
            xTitledPanel2ContentContainer.add(QTESUI);
            QTESUI.setBounds(590, 10, 69, QTESUI.getPreferredSize().height);

            //---- OBJ_117 ----
            OBJ_117.setText("Quantit\u00e9 pour meilleure condition");
            OBJ_117.setName("OBJ_117");
            xTitledPanel2ContentContainer.add(OBJ_117);
            OBJ_117.setBounds(390, 12, 200, 20);

            //---- OBJ_132 ----
            OBJ_132.setText("@CAUNA@");
            OBJ_132.setName("OBJ_132");
            xTitledPanel2ContentContainer.add(OBJ_132);
            OBJ_132.setBounds(150, 60, 24, 24);

            //---- OBJ_122 ----
            OBJ_122.setText("@CAUNC@");
            OBJ_122.setName("OBJ_122");
            xTitledPanel2ContentContainer.add(OBJ_122);
            OBJ_122.setBounds(150, 35, 24, 24);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("@MALIB@");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //======== SCROLLPANE_OBJ_91 ========
            {
              SCROLLPANE_OBJ_91.setComponentPopupMenu(BTD);
              SCROLLPANE_OBJ_91.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
              SCROLLPANE_OBJ_91.setName("SCROLLPANE_OBJ_91");

              //---- LIGSOR01 ----
              LIGSOR01.setRowHeight(20);
              LIGSOR01.setName("LIGSOR01");
              SCROLLPANE_OBJ_91.setViewportView(LIGSOR01);
            }
            xTitledPanel3ContentContainer.add(SCROLLPANE_OBJ_91);
            SCROLLPANE_OBJ_91.setBounds(65, 25, 625, 70);

            //---- OBJ_88 ----
            OBJ_88.setText("Num\u00e9ros de semaine");
            OBJ_88.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_88.setName("OBJ_88");
            xTitledPanel3ContentContainer.add(OBJ_88);
            OBJ_88.setBounds(65, 5, 535, 20);

            //---- OBJ_107 ----
            OBJ_107.setText("Command\u00e9");
            OBJ_107.setName("OBJ_107");
            xTitledPanel3ContentContainer.add(OBJ_107);
            OBJ_107.setBounds(335, 102, 80, 20);

            //---- OBJ_104 ----
            OBJ_104.setText("@WSTKX@");
            OBJ_104.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_104.setName("OBJ_104");
            xTitledPanel3ContentContainer.add(OBJ_104);
            OBJ_104.setBounds(65, 100, 90, OBJ_104.getPreferredSize().height);

            //---- OBJ_110 ----
            OBJ_110.setText("@WDISX@");
            OBJ_110.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_110.setName("OBJ_110");
            xTitledPanel3ContentContainer.add(OBJ_110);
            OBJ_110.setBounds(590, 100, 100, OBJ_110.getPreferredSize().height);

            //---- OBJ_109 ----
            OBJ_109.setText("Disponible");
            OBJ_109.setName("OBJ_109");
            xTitledPanel3ContentContainer.add(OBJ_109);
            OBJ_109.setBounds(520, 102, 71, 20);

            //---- OBJ_106 ----
            OBJ_106.setText("@WATTX@");
            OBJ_106.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_106.setName("OBJ_106");
            xTitledPanel3ContentContainer.add(OBJ_106);
            OBJ_106.setBounds(new Rectangle(new Point(225, 100), OBJ_106.getPreferredSize()));

            //---- OBJ_108 ----
            OBJ_108.setText("@WRESX@");
            OBJ_108.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_108.setName("OBJ_108");
            xTitledPanel3ContentContainer.add(OBJ_108);
            OBJ_108.setBounds(410, 100, 100, OBJ_108.getPreferredSize().height);

            //---- OBJ_105 ----
            OBJ_105.setText("Attendu");
            OBJ_105.setName("OBJ_105");
            xTitledPanel3ContentContainer.add(OBJ_105);
            OBJ_105.setBounds(170, 102, 55, 20);

            //---- OBJ_103 ----
            OBJ_103.setText("Stock");
            OBJ_103.setName("OBJ_103");
            xTitledPanel3ContentContainer.add(OBJ_103);
            OBJ_103.setBounds(20, 102, 45, 20);

            //---- OBJ_93 ----
            OBJ_93.setText("Conso.");
            OBJ_93.setName("OBJ_93");
            xTitledPanel3ContentContainer.add(OBJ_93);
            OBJ_93.setBounds(20, 50, 45, 20);

            //---- OBJ_100 ----
            OBJ_100.setText("Entr\u00e9es");
            OBJ_100.setName("OBJ_100");
            xTitledPanel3ContentContainer.add(OBJ_100);
            OBJ_100.setBounds(20, 70, 45, 20);

            //---- OBJ_111 ----
            OBJ_111.setText("US");
            OBJ_111.setName("OBJ_111");
            xTitledPanel3ContentContainer.add(OBJ_111);
            OBJ_111.setBounds(700, 102, 29, 21);

            //---- OBJ_112 ----
            OBJ_112.setText("@A1UNS@");
            OBJ_112.setName("OBJ_112");
            xTitledPanel3ContentContainer.add(OBJ_112);
            OBJ_112.setBounds(730, 100, 65, OBJ_112.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitle("Fournisseur");
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //---- OBJ_56 ----
            OBJ_56.setText("@FRFRSG@");
            OBJ_56.setName("OBJ_56");
            xTitledPanel4ContentContainer.add(OBJ_56);
            OBJ_56.setBounds(20, 10, 68, OBJ_56.getPreferredSize().height);

            //---- OBJ_57 ----
            OBJ_57.setText("@ULBFRS@");
            OBJ_57.setName("OBJ_57");
            xTitledPanel4ContentContainer.add(OBJ_57);
            OBJ_57.setBounds(95, 10, 300, OBJ_57.getPreferredSize().height);

            //---- OBJ_65 ----
            OBJ_65.setText("Attention promotion fournisseur");
            OBJ_65.setFont(OBJ_65.getFont().deriveFont(OBJ_65.getFont().getStyle() | Font.BOLD));
            OBJ_65.setForeground(new Color(255, 0, 51));
            OBJ_65.setName("OBJ_65");
            xTitledPanel4ContentContainer.add(OBJ_65);
            OBJ_65.setBounds(20, 37, 350, 20);

            //---- OBJ_69 ----
            OBJ_69.setText("@FRPNUM@");
            OBJ_69.setName("OBJ_69");
            xTitledPanel4ContentContainer.add(OBJ_69);
            OBJ_69.setBounds(20, 60, 68, OBJ_69.getPreferredSize().height);

            //---- OBJ_70 ----
            OBJ_70.setText("@FRPNOM@");
            OBJ_70.setName("OBJ_70");
            xTitledPanel4ContentContainer.add(OBJ_70);
            OBJ_70.setBounds(95, 60, 300, OBJ_70.getPreferredSize().height);

            //---- OBJ_73 ----
            OBJ_73.setText("@CAREF@");
            OBJ_73.setName("OBJ_73");
            xTitledPanel4ContentContainer.add(OBJ_73);
            OBJ_73.setBounds(95, 85, 210, OBJ_73.getPreferredSize().height);

            //---- OBJ_72 ----
            OBJ_72.setText("R\u00e9f\u00e9rence");
            OBJ_72.setName("OBJ_72");
            xTitledPanel4ContentContainer.add(OBJ_72);
            OBJ_72.setBounds(20, 87, 67, 21);

            //---- OBJ_78 ----
            OBJ_78.setText("@CALIBR@");
            OBJ_78.setName("OBJ_78");
            xTitledPanel4ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(20, 110, 350, OBJ_78.getPreferredSize().height);

            //---- OBJ_81 ----
            OBJ_81.setText("");
            OBJ_81.setToolTipText("Changement fournisseur");
            OBJ_81.setComponentPopupMenu(BTD);
            OBJ_81.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_81.setName("OBJ_81");
            OBJ_81.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_81ActionPerformed(e);
              }
            });
            xTitledPanel4ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(373, 110, 25, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel5 ========
          {
            xTitledPanel5.setBorder(new DropShadowBorder());
            xTitledPanel5.setTitle("Prix");
            xTitledPanel5.setName("xTitledPanel5");
            Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
            xTitledPanel5ContentContainer.setLayout(null);

            //---- OBJ_75 ----
            OBJ_75.setText("Condition en cours");
            OBJ_75.setName("OBJ_75");
            xTitledPanel5ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(20, 87, 113, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("Prochaine condition");
            OBJ_82.setName("OBJ_82");
            xTitledPanel5ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(20, 113, 121, 18);

            //---- OBJ_55 ----
            OBJ_55.setText("Prix d'achat");
            OBJ_55.setName("OBJ_55");
            xTitledPanel5ContentContainer.add(OBJ_55);
            OBJ_55.setBounds(20, 10, 82, 20);

            //---- OBJ_62 ----
            OBJ_62.setText("@WPRAX@");
            OBJ_62.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_62.setName("OBJ_62");
            xTitledPanel5ContentContainer.add(OBJ_62);
            OBJ_62.setBounds(155, 35, 110, OBJ_62.getPreferredSize().height);

            //---- LRPASX ----
            LRPASX.setComponentPopupMenu(BTD);
            LRPASX.setHorizontalAlignment(SwingConstants.RIGHT);
            LRPASX.setName("LRPASX");
            xTitledPanel5ContentContainer.add(LRPASX);
            LRPASX.setBounds(153, 59, 112, LRPASX.getPreferredSize().height);

            //---- OBJ_54 ----
            OBJ_54.setText("Promotion");
            OBJ_54.setName("OBJ_54");
            xTitledPanel5ContentContainer.add(OBJ_54);
            OBJ_54.setBounds(155, 10, 64, 20);

            //---- CADAPX ----
            CADAPX.setText("@CADAPX@");
            CADAPX.setHorizontalAlignment(SwingConstants.CENTER);
            CADAPX.setName("CADAPX");
            xTitledPanel5ContentContainer.add(CADAPX);
            CADAPX.setBounds(155, 85, 68, CADAPX.getPreferredSize().height);

            //---- CADAFX ----
            CADAFX.setText("@CADAFX@");
            CADAFX.setHorizontalAlignment(SwingConstants.CENTER);
            CADAFX.setName("CADAFX");
            xTitledPanel5ContentContainer.add(CADAFX);
            CADAFX.setBounds(235, 85, 68, CADAFX.getPreferredSize().height);

            //---- OBJ_66 ----
            OBJ_66.setText("N\u00e9goci\u00e9");
            OBJ_66.setName("OBJ_66");
            xTitledPanel5ContentContainer.add(OBJ_66);
            OBJ_66.setBounds(20, 60, 70, 20);

            //---- DAPSUI ----
            DAPSUI.setText("@DAPSUI@");
            DAPSUI.setHorizontalAlignment(SwingConstants.CENTER);
            DAPSUI.setName("DAPSUI");
            xTitledPanel5ContentContainer.add(DAPSUI);
            DAPSUI.setBounds(155, 110, 68, DAPSUI.getPreferredSize().height);

            //---- DAFSUI ----
            DAFSUI.setText("@DAFSUI@");
            DAFSUI.setHorizontalAlignment(SwingConstants.CENTER);
            DAFSUI.setName("DAFSUI");
            xTitledPanel5ContentContainer.add(DAFSUI);
            DAFSUI.setBounds(235, 110, 68, DAFSUI.getPreferredSize().height);

            //---- OBJ_61 ----
            OBJ_61.setText("Calcul\u00e9");
            OBJ_61.setName("OBJ_61");
            xTitledPanel5ContentContainer.add(OBJ_61);
            OBJ_61.setBounds(20, 37, 48, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("(UA)");
            OBJ_63.setName("OBJ_63");
            xTitledPanel5ContentContainer.add(OBJ_63);
            OBJ_63.setBounds(275, 37, 30, 20);

            //---- OBJ_68 ----
            OBJ_68.setText("(UA)");
            OBJ_68.setName("OBJ_68");
            xTitledPanel5ContentContainer.add(OBJ_68);
            OBJ_68.setBounds(276, 63, 29, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel5ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel5ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel5ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel5ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel5ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 420, GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE))
                  .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 825, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 825, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 825, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE))
                  .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_41;
  private RiZoneSortie INDETB;
  private JLabel OBJ_38;
  private RiZoneSortie INDMAG;
  private JLabel OBJ_39;
  private RiZoneSortie INDAR0;
  private JLabel OBJ_46;
  private RiZoneSortie A1FAM;
  private RiZoneSortie OBJ_52;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField XLLIB1;
  private XRiTextField XLLIB3;
  private XRiTextField XLLIB2;
  private XRiTextField XLLIB4;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_120;
  private JLabel OBJ_130;
  private XRiTextField LRQTCX;
  private XRiTextField LRQTAX;
  private JLabel OBJ_123;
  private JLabel OBJ_133;
  private XRiCalendrier LRDTRX;
  private RiZoneSortie CANUA;
  private JLabel OBJ_125;
  private RiZoneSortie CAQMI;
  private JLabel OBJ_127;
  private JLabel OBJ_135;
  private RiZoneSortie OBJ_136;
  private RiZoneSortie OBJ_128;
  private JLabel OBJ_119;
  private RiZoneSortie OBJ_138;
  private JLabel OBJ_137;
  private RiZoneSortie QTESUI;
  private JLabel OBJ_117;
  private RiZoneSortie OBJ_132;
  private RiZoneSortie OBJ_122;
  private JXTitledPanel xTitledPanel3;
  private JScrollPane SCROLLPANE_OBJ_91;
  private XRiTable LIGSOR01;
  private JLabel OBJ_88;
  private JLabel OBJ_107;
  private RiZoneSortie OBJ_104;
  private RiZoneSortie OBJ_110;
  private JLabel OBJ_109;
  private RiZoneSortie OBJ_106;
  private RiZoneSortie OBJ_108;
  private JLabel OBJ_105;
  private JLabel OBJ_103;
  private JLabel OBJ_93;
  private JLabel OBJ_100;
  private JLabel OBJ_111;
  private RiZoneSortie OBJ_112;
  private JXTitledPanel xTitledPanel4;
  private RiZoneSortie OBJ_56;
  private RiZoneSortie OBJ_57;
  private JLabel OBJ_65;
  private RiZoneSortie OBJ_69;
  private RiZoneSortie OBJ_70;
  private RiZoneSortie OBJ_73;
  private JLabel OBJ_72;
  private RiZoneSortie OBJ_78;
  private SNBoutonDetail OBJ_81;
  private JXTitledPanel xTitledPanel5;
  private JLabel OBJ_75;
  private JLabel OBJ_82;
  private JLabel OBJ_55;
  private RiZoneSortie OBJ_62;
  private XRiTextField LRPASX;
  private JLabel OBJ_54;
  private RiZoneSortie CADAPX;
  private RiZoneSortie CADAFX;
  private JLabel OBJ_66;
  private RiZoneSortie DAPSUI;
  private RiZoneSortie DAFSUI;
  private JLabel OBJ_61;
  private JLabel OBJ_63;
  private JLabel OBJ_68;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
