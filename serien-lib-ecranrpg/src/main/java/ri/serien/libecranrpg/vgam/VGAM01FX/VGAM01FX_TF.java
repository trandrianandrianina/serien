
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_TF extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] TFCP6_Value = { "", "1", "3", "5", "4", "6", "8", "9", };
  private String[] TFCP5_Value = { "", "1", "3", "5", "4", "6", "8", "9", };
  private String[] TFCP4_Value = { "", "1", "3", "5", "4", "6", "8", "9", };
  private String[] TFCP3_Value = { "", "1", "3", "5", "4", "6", "8", "9", };
  private String[] TFCP2_Value = { "", "1", "3", "5", "4", "6", "8", "9", };
  private String[] TFCP1_Value = { "", "1", "3", "5", "4", "6", "8", "9", };
  
  public VGAM01FX_TF(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TFCP6.setValeurs(TFCP6_Value, null);
    TFCP5.setValeurs(TFCP5_Value, null);
    TFCP4.setValeurs(TFCP4_Value, null);
    TFCP3.setValeurs(TFCP3_Value, null);
    TFCP2.setValeurs(TFCP2_Value, null);
    TFCP1.setValeurs(TFCP1_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // TFCP1.setSelectedIndex(getIndice("TFCP1", TFCP1_Value));
    // TFCP2.setSelectedIndex(getIndice("TFCP2", TFCP2_Value));
    // TFCP3.setSelectedIndex(getIndice("TFCP3", TFCP3_Value));
    // TFCP4.setSelectedIndex(getIndice("TFCP4", TFCP4_Value));
    // TFCP5.setSelectedIndex(getIndice("TFCP5", TFCP5_Value));
    // TFCP6.setSelectedIndex(getIndice("TFCP6", TFCP6_Value));
    
    TFTE6.setEnabled(lexique.isPresent("TFTE6"));
    TFTP6.setEnabled(lexique.isPresent("TFTP6"));
    TFTF6.setEnabled(lexique.isPresent("TFTF6"));
    TFTE5.setEnabled(lexique.isPresent("TFTE5"));
    TFTP5.setEnabled(lexique.isPresent("TFTP5"));
    TFTF5.setEnabled(lexique.isPresent("TFTF5"));
    TFTE4.setEnabled(lexique.isPresent("TFTE4"));
    TFTP4.setEnabled(lexique.isPresent("TFTP4"));
    TFTF4.setEnabled(lexique.isPresent("TFTF4"));
    TFTE3.setEnabled(lexique.isPresent("TFTE3"));
    TFTP3.setEnabled(lexique.isPresent("TFTP3"));
    TFTF3.setEnabled(lexique.isPresent("TFTF3"));
    TFTE2.setEnabled(lexique.isPresent("TFTE2"));
    TFTP2.setEnabled(lexique.isPresent("TFTP2"));
    TFTF2.setEnabled(lexique.isPresent("TFTF2"));
    TFTE1.setEnabled(lexique.isPresent("TFTE1"));
    TFTP1.setEnabled(lexique.isPresent("TFTP1"));
    TFTF1.setEnabled(lexique.isPresent("TFTF1"));
    TFRBP.setEnabled(lexique.isPresent("TFRBP"));
    TFTV6.setEnabled(lexique.isPresent("TFTV6"));
    TFTV5.setEnabled(lexique.isPresent("TFTV5"));
    TFTV4.setEnabled(lexique.isPresent("TFTV4"));
    TFTV3.setEnabled(lexique.isPresent("TFTV3"));
    TFTV2.setEnabled(lexique.isPresent("TFTV2"));
    TFTV1.setEnabled(lexique.isPresent("TFTV1"));
    TFTXPX.setEnabled(lexique.isPresent("TFTXPX"));
    TFCX6.setEnabled(lexique.isPresent("TFCX6"));
    TFCX5.setEnabled(lexique.isPresent("TFCX5"));
    TFCX4.setEnabled(lexique.isPresent("TFCX4"));
    TFCX3.setEnabled(lexique.isPresent("TFCX3"));
    TFCX2.setEnabled(lexique.isPresent("TFCX2"));
    TFCX1.setEnabled(lexique.isPresent("TFCX1"));
    OBJ_57.setVisible(lexique.isPresent("TFCX6"));
    OBJ_56.setVisible(lexique.isPresent("TFCX5"));
    OBJ_55.setVisible(lexique.isPresent("TFCX4"));
    OBJ_54.setVisible(lexique.isPresent("TFCX3"));
    OBJ_53.setVisible(lexique.isPresent("TFCX2"));
    OBJ_52.setVisible(lexique.isPresent("TFCX1"));
    TFLI6.setEnabled(lexique.isPresent("TFLI6"));
    TFLI5.setEnabled(lexique.isPresent("TFLI5"));
    TFLI4.setEnabled(lexique.isPresent("TFLI4"));
    TFLI3.setEnabled(lexique.isPresent("TFLI3"));
    TFLI2.setEnabled(lexique.isPresent("TFLI2"));
    TFLI1.setEnabled(lexique.isPresent("TFLI1"));
    /// *TFCP6.setEnabled( lexique.isPresent("TFCP6"));
    // TFCP5.setEnabled( lexique.isPresent("TFCP5"));
    // TFCP4.setEnabled( lexique.isPresent("TFCP4"));
    // TFCP3.setEnabled( lexique.isPresent("TFCP3"));
    // TFCP2.setEnabled( lexique.isPresent("TFCP2"));
    // TFCP1.setEnabled( lexique.isPresent("TFCP1"));*/
    
    panel2.setVisible(lexique.isTrue("27"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Personnalisation de @LOCGRP/-1/@"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("TFCP6", 0, TFCP6_Value[TFCP6.getSelectedIndex()]);
    // lexique.HostFieldPutData("TFCP5", 0, TFCP5_Value[TFCP5.getSelectedIndex()]);
    // lexique.HostFieldPutData("TFCP4", 0, TFCP4_Value[TFCP4.getSelectedIndex()]);
    // lexique.HostFieldPutData("TFCP3", 0, TFCP3_Value[TFCP3.getSelectedIndex()]);
    // lexique.HostFieldPutData("TFCP2", 0, TFCP2_Value[TFCP2.getSelectedIndex()]);
    // lexique.HostFieldPutData("TFCP1", 0, TFCP1_Value[TFCP1.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_48 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    panel2 = new JPanel();
    OBJ_65 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_57 = new JLabel();
    TFCX1 = new XRiTextField();
    TFCX2 = new XRiTextField();
    TFCX3 = new XRiTextField();
    TFCX4 = new XRiTextField();
    TFCX5 = new XRiTextField();
    TFCX6 = new XRiTextField();
    TFTXPX = new XRiTextField();
    TFRBP = new XRiTextField();
    panel3 = new JPanel();
    TFCP1 = new XRiComboBox();
    TFCP2 = new XRiComboBox();
    TFCP3 = new XRiComboBox();
    TFCP4 = new XRiComboBox();
    TFCP5 = new XRiComboBox();
    TFCP6 = new XRiComboBox();
    OBJ_70 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_113 = new JLabel();
    TFLI1 = new XRiTextField();
    TFLI2 = new XRiTextField();
    TFLI3 = new XRiTextField();
    TFLI4 = new XRiTextField();
    TFLI5 = new XRiTextField();
    TFLI6 = new XRiTextField();
    OBJ_71 = new JLabel();
    TFTV1 = new XRiTextField();
    TFTV2 = new XRiTextField();
    TFTV3 = new XRiTextField();
    TFTV4 = new XRiTextField();
    TFTV5 = new XRiTextField();
    TFTV6 = new XRiTextField();
    OBJ_72 = new JLabel();
    OBJ_112 = new JPanel();
    OBJ_110 = new JLabel();
    OBJ_111 = new JLabel();
    TFTF1 = new XRiTextField();
    TFTP1 = new XRiTextField();
    TFTE1 = new XRiTextField();
    TFTF2 = new XRiTextField();
    TFTP2 = new XRiTextField();
    TFTE2 = new XRiTextField();
    TFTF3 = new XRiTextField();
    TFTP3 = new XRiTextField();
    TFTE3 = new XRiTextField();
    TFTF4 = new XRiTextField();
    TFTP4 = new XRiTextField();
    TFTE4 = new XRiTextField();
    TFTF5 = new XRiTextField();
    TFTP5 = new XRiTextField();
    TFTE5 = new XRiTextField();
    TFTF6 = new XRiTextField();
    TFTP6 = new XRiTextField();
    TFTE6 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_48 ----
          OBJ_48.setText("Ordre");
          OBJ_48.setName("OBJ_48");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Table des types de facturation");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder(""));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_65 ----
              OBJ_65.setText("Taxe parafiscale");
              OBJ_65.setName("OBJ_65");
              panel2.add(OBJ_65);
              OBJ_65.setBounds(40, 80, 145, 20);

              //---- OBJ_58 ----
              OBJ_58.setText("Taux de TVA");
              OBJ_58.setName("OBJ_58");
              panel2.add(OBJ_58);
              OBJ_58.setBounds(40, 44, 145, 20);

              //---- OBJ_66 ----
              OBJ_66.setText("Taux");
              OBJ_66.setName("OBJ_66");
              panel2.add(OBJ_66);
              OBJ_66.setBounds(190, 80, 52, 20);

              //---- OBJ_68 ----
              OBJ_68.setText("Base %");
              OBJ_68.setName("OBJ_68");
              panel2.add(OBJ_68);
              OBJ_68.setBounds(344, 80, 50, 20);

              //---- OBJ_52 ----
              OBJ_52.setText("Code 1");
              OBJ_52.setName("OBJ_52");
              panel2.add(OBJ_52);
              OBJ_52.setBounds(190, 15, 58, 20);

              //---- OBJ_53 ----
              OBJ_53.setText("Code 2");
              OBJ_53.setName("OBJ_53");
              panel2.add(OBJ_53);
              OBJ_53.setBounds(267, 15, 58, 20);

              //---- OBJ_54 ----
              OBJ_54.setText("Code 3");
              OBJ_54.setName("OBJ_54");
              panel2.add(OBJ_54);
              OBJ_54.setBounds(344, 15, 58, 20);

              //---- OBJ_55 ----
              OBJ_55.setText("Code 4");
              OBJ_55.setName("OBJ_55");
              panel2.add(OBJ_55);
              OBJ_55.setBounds(421, 15, 58, 20);

              //---- OBJ_56 ----
              OBJ_56.setText("Code 5");
              OBJ_56.setName("OBJ_56");
              panel2.add(OBJ_56);
              OBJ_56.setBounds(498, 15, 58, 20);

              //---- OBJ_57 ----
              OBJ_57.setText("Code 6");
              OBJ_57.setName("OBJ_57");
              panel2.add(OBJ_57);
              OBJ_57.setBounds(575, 15, 58, 20);

              //---- TFCX1 ----
              TFCX1.setComponentPopupMenu(BTD);
              TFCX1.setName("TFCX1");
              panel2.add(TFCX1);
              TFCX1.setBounds(190, 40, 58, TFCX1.getPreferredSize().height);

              //---- TFCX2 ----
              TFCX2.setComponentPopupMenu(BTD);
              TFCX2.setName("TFCX2");
              panel2.add(TFCX2);
              TFCX2.setBounds(267, 40, 58, TFCX2.getPreferredSize().height);

              //---- TFCX3 ----
              TFCX3.setComponentPopupMenu(BTD);
              TFCX3.setName("TFCX3");
              panel2.add(TFCX3);
              TFCX3.setBounds(344, 40, 58, TFCX3.getPreferredSize().height);

              //---- TFCX4 ----
              TFCX4.setComponentPopupMenu(BTD);
              TFCX4.setName("TFCX4");
              panel2.add(TFCX4);
              TFCX4.setBounds(421, 40, 58, TFCX4.getPreferredSize().height);

              //---- TFCX5 ----
              TFCX5.setComponentPopupMenu(BTD);
              TFCX5.setName("TFCX5");
              panel2.add(TFCX5);
              TFCX5.setBounds(498, 40, 58, TFCX5.getPreferredSize().height);

              //---- TFCX6 ----
              TFCX6.setComponentPopupMenu(BTD);
              TFCX6.setName("TFCX6");
              panel2.add(TFCX6);
              TFCX6.setBounds(575, 40, 58, TFCX6.getPreferredSize().height);

              //---- TFTXPX ----
              TFTXPX.setComponentPopupMenu(BTD);
              TFTXPX.setName("TFTXPX");
              panel2.add(TFTXPX);
              TFTXPX.setBounds(267, 76, 58, TFTXPX.getPreferredSize().height);

              //---- TFRBP ----
              TFRBP.setComponentPopupMenu(BTD);
              TFRBP.setName("TFRBP");
              panel2.add(TFRBP);
              TFRBP.setBounds(421, 76, 26, TFRBP.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder(""));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- TFCP1 ----
              TFCP1.setModel(new DefaultComboBoxModel(new String[] {
                "Pas d'\u00e9dition",
                "sur ARC",
                "sur EXP",
                "sur FAC",
                "sur ARC + EXP",
                "sur ARC + FAC",
                "sur EXP + FAC",
                "sur ARC + EXP + FAC"
              }));
              TFCP1.setComponentPopupMenu(BTD);
              TFCP1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TFCP1.setName("TFCP1");
              panel3.add(TFCP1);
              TFCP1.setBounds(500, 66, 129, TFCP1.getPreferredSize().height);

              //---- TFCP2 ----
              TFCP2.setModel(new DefaultComboBoxModel(new String[] {
                "Pas d'\u00e9dition",
                "sur ARC",
                "sur EXP",
                "sur FAC",
                "sur ARC + EXP",
                "sur ARC + FAC",
                "sur EXP + FAC",
                "sur ARC + EXP + FAC"
              }));
              TFCP2.setComponentPopupMenu(BTD);
              TFCP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TFCP2.setName("TFCP2");
              panel3.add(TFCP2);
              TFCP2.setBounds(500, 102, 129, TFCP2.getPreferredSize().height);

              //---- TFCP3 ----
              TFCP3.setModel(new DefaultComboBoxModel(new String[] {
                "Pas d'\u00e9dition",
                "sur ARC",
                "sur EXP",
                "sur FAC",
                "sur ARC + EXP",
                "sur ARC + FAC",
                "sur EXP + FAC",
                "sur ARC + EXP + FAC"
              }));
              TFCP3.setComponentPopupMenu(BTD);
              TFCP3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TFCP3.setName("TFCP3");
              panel3.add(TFCP3);
              TFCP3.setBounds(500, 138, 129, TFCP3.getPreferredSize().height);

              //---- TFCP4 ----
              TFCP4.setModel(new DefaultComboBoxModel(new String[] {
                "Pas d'\u00e9dition",
                "sur ARC",
                "sur EXP",
                "sur FAC",
                "sur ARC + EXP",
                "sur ARC + FAC",
                "sur EXP + FAC",
                "sur ARC + EXP + FAC"
              }));
              TFCP4.setComponentPopupMenu(BTD);
              TFCP4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TFCP4.setName("TFCP4");
              panel3.add(TFCP4);
              TFCP4.setBounds(500, 174, 129, TFCP4.getPreferredSize().height);

              //---- TFCP5 ----
              TFCP5.setModel(new DefaultComboBoxModel(new String[] {
                "Pas d'\u00e9dition",
                "sur ARC",
                "sur EXP",
                "sur FAC",
                "sur ARC + EXP",
                "sur ARC + FAC",
                "sur EXP + FAC",
                "sur ARC + EXP + FAC"
              }));
              TFCP5.setComponentPopupMenu(BTD);
              TFCP5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TFCP5.setName("TFCP5");
              panel3.add(TFCP5);
              TFCP5.setBounds(500, 210, 129, TFCP5.getPreferredSize().height);

              //---- TFCP6 ----
              TFCP6.setModel(new DefaultComboBoxModel(new String[] {
                "Pas d'\u00e9dition",
                "sur ARC",
                "sur EXP",
                "sur FAC",
                "sur ARC + EXP",
                "sur ARC + FAC",
                "sur EXP + FAC",
                "sur ARC + EXP + FAC"
              }));
              TFCP6.setComponentPopupMenu(BTD);
              TFCP6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TFCP6.setName("TFCP6");
              panel3.add(TFCP6);
              TFCP6.setBounds(500, 246, 129, TFCP6.getPreferredSize().height);

              //---- OBJ_70 ----
              OBJ_70.setText("Type de facturation");
              OBJ_70.setName("OBJ_70");
              panel3.add(OBJ_70);
              OBJ_70.setBounds(230, 37, 118, 20);

              //---- OBJ_73 ----
              OBJ_73.setText("Codes facturation");
              OBJ_73.setName("OBJ_73");
              panel3.add(OBJ_73);
              OBJ_73.setBounds(45, 70, 135, 20);

              //---- OBJ_113 ----
              OBJ_113.setText("Edition code pays");
              OBJ_113.setName("OBJ_113");
              panel3.add(OBJ_113);
              OBJ_113.setBounds(500, 38, 111, 19);

              //---- TFLI1 ----
              TFLI1.setComponentPopupMenu(BTD);
              TFLI1.setName("TFLI1");
              panel3.add(TFLI1);
              TFLI1.setBounds(232, 65, 110, TFLI1.getPreferredSize().height);

              //---- TFLI2 ----
              TFLI2.setToolTipText("libell\u00e9");
              TFLI2.setComponentPopupMenu(BTD);
              TFLI2.setName("TFLI2");
              panel3.add(TFLI2);
              TFLI2.setBounds(232, 101, 110, TFLI2.getPreferredSize().height);

              //---- TFLI3 ----
              TFLI3.setToolTipText("libell\u00e9");
              TFLI3.setComponentPopupMenu(BTD);
              TFLI3.setName("TFLI3");
              panel3.add(TFLI3);
              TFLI3.setBounds(229, 137, 110, TFLI3.getPreferredSize().height);

              //---- TFLI4 ----
              TFLI4.setToolTipText("libell\u00e9");
              TFLI4.setComponentPopupMenu(BTD);
              TFLI4.setName("TFLI4");
              panel3.add(TFLI4);
              TFLI4.setBounds(229, 173, 110, TFLI4.getPreferredSize().height);

              //---- TFLI5 ----
              TFLI5.setToolTipText("libell\u00e9");
              TFLI5.setComponentPopupMenu(BTD);
              TFLI5.setName("TFLI5");
              panel3.add(TFLI5);
              TFLI5.setBounds(229, 209, 110, TFLI5.getPreferredSize().height);

              //---- TFLI6 ----
              TFLI6.setToolTipText("libell\u00e9");
              TFLI6.setComponentPopupMenu(BTD);
              TFLI6.setName("TFLI6");
              panel3.add(TFLI6);
              TFLI6.setBounds(229, 245, 110, TFLI6.getPreferredSize().height);

              //---- OBJ_71 ----
              OBJ_71.setText("Codes");
              OBJ_71.setName("OBJ_71");
              panel3.add(OBJ_71);
              OBJ_71.setBounds(400, 15, 43, 20);

              //---- TFTV1 ----
              TFTV1.setComponentPopupMenu(BTD);
              TFTV1.setName("TFTV1");
              panel3.add(TFTV1);
              TFTV1.setBounds(360, 65, 34, TFTV1.getPreferredSize().height);

              //---- TFTV2 ----
              TFTV2.setToolTipText("sous la forme \"123\"");
              TFTV2.setComponentPopupMenu(BTD);
              TFTV2.setName("TFTV2");
              panel3.add(TFTV2);
              TFTV2.setBounds(360, 101, 34, TFTV2.getPreferredSize().height);

              //---- TFTV3 ----
              TFTV3.setToolTipText("sous la forme \"123\"");
              TFTV3.setComponentPopupMenu(BTD);
              TFTV3.setName("TFTV3");
              panel3.add(TFTV3);
              TFTV3.setBounds(360, 137, 34, TFTV3.getPreferredSize().height);

              //---- TFTV4 ----
              TFTV4.setToolTipText("sous la forme \"123\"");
              TFTV4.setComponentPopupMenu(BTD);
              TFTV4.setName("TFTV4");
              panel3.add(TFTV4);
              TFTV4.setBounds(360, 173, 34, TFTV4.getPreferredSize().height);

              //---- TFTV5 ----
              TFTV5.setToolTipText("sous la forme \"123\"");
              TFTV5.setComponentPopupMenu(BTD);
              TFTV5.setName("TFTV5");
              panel3.add(TFTV5);
              TFTV5.setBounds(360, 209, 34, TFTV5.getPreferredSize().height);

              //---- TFTV6 ----
              TFTV6.setToolTipText("sous la forme \"123\"");
              TFTV6.setComponentPopupMenu(BTD);
              TFTV6.setName("TFTV6");
              panel3.add(TFTV6);
              TFTV6.setBounds(360, 245, 34, TFTV6.getPreferredSize().height);

              //---- OBJ_72 ----
              OBJ_72.setText("TVA");
              OBJ_72.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_72.setName("OBJ_72");
              panel3.add(OBJ_72);
              OBJ_72.setBounds(360, 40, 35, 14);

              //======== OBJ_112 ========
              {
                OBJ_112.setName("OBJ_112");
                OBJ_112.setLayout(null);
              }
              panel3.add(OBJ_112);
              OBJ_112.setBounds(370, 35, 100, 2);

              //---- OBJ_110 ----
              OBJ_110.setText("TPF");
              OBJ_110.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_110.setName("OBJ_110");
              panel3.add(OBJ_110);
              OBJ_110.setBounds(402, 40, 35, 14);

              //---- OBJ_111 ----
              OBJ_111.setText("TEC");
              OBJ_111.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_111.setName("OBJ_111");
              panel3.add(OBJ_111);
              OBJ_111.setBounds(444, 40, 35, 14);

              //---- TFTF1 ----
              TFTF1.setComponentPopupMenu(BTD);
              TFTF1.setName("TFTF1");
              panel3.add(TFTF1);
              TFTF1.setBounds(190, 65, 20, TFTF1.getPreferredSize().height);

              //---- TFTP1 ----
              TFTP1.setToolTipText("taxe parafiscale");
              TFTP1.setComponentPopupMenu(BTD);
              TFTP1.setName("TFTP1");
              panel3.add(TFTP1);
              TFTP1.setBounds(410, 65, 18, TFTP1.getPreferredSize().height);

              //---- TFTE1 ----
              TFTE1.setComponentPopupMenu(BTD);
              TFTE1.setName("TFTE1");
              panel3.add(TFTE1);
              TFTE1.setBounds(452, 65, 18, TFTE1.getPreferredSize().height);

              //---- TFTF2 ----
              TFTF2.setComponentPopupMenu(BTD);
              TFTF2.setName("TFTF2");
              panel3.add(TFTF2);
              TFTF2.setBounds(190, 101, 20, TFTF2.getPreferredSize().height);

              //---- TFTP2 ----
              TFTP2.setToolTipText("taxe parafiscale");
              TFTP2.setComponentPopupMenu(BTD);
              TFTP2.setName("TFTP2");
              panel3.add(TFTP2);
              TFTP2.setBounds(410, 101, 18, TFTP2.getPreferredSize().height);

              //---- TFTE2 ----
              TFTE2.setComponentPopupMenu(BTD);
              TFTE2.setName("TFTE2");
              panel3.add(TFTE2);
              TFTE2.setBounds(452, 101, 18, TFTE2.getPreferredSize().height);

              //---- TFTF3 ----
              TFTF3.setComponentPopupMenu(BTD);
              TFTF3.setName("TFTF3");
              panel3.add(TFTF3);
              TFTF3.setBounds(190, 137, 20, TFTF3.getPreferredSize().height);

              //---- TFTP3 ----
              TFTP3.setToolTipText("taxe parafiscale");
              TFTP3.setComponentPopupMenu(BTD);
              TFTP3.setName("TFTP3");
              panel3.add(TFTP3);
              TFTP3.setBounds(410, 137, 18, TFTP3.getPreferredSize().height);

              //---- TFTE3 ----
              TFTE3.setComponentPopupMenu(BTD);
              TFTE3.setName("TFTE3");
              panel3.add(TFTE3);
              TFTE3.setBounds(452, 137, 18, TFTE3.getPreferredSize().height);

              //---- TFTF4 ----
              TFTF4.setComponentPopupMenu(BTD);
              TFTF4.setName("TFTF4");
              panel3.add(TFTF4);
              TFTF4.setBounds(190, 173, 20, TFTF4.getPreferredSize().height);

              //---- TFTP4 ----
              TFTP4.setToolTipText("taxe parafiscale");
              TFTP4.setComponentPopupMenu(BTD);
              TFTP4.setName("TFTP4");
              panel3.add(TFTP4);
              TFTP4.setBounds(410, 173, 18, TFTP4.getPreferredSize().height);

              //---- TFTE4 ----
              TFTE4.setComponentPopupMenu(BTD);
              TFTE4.setName("TFTE4");
              panel3.add(TFTE4);
              TFTE4.setBounds(452, 173, 18, TFTE4.getPreferredSize().height);

              //---- TFTF5 ----
              TFTF5.setComponentPopupMenu(BTD);
              TFTF5.setName("TFTF5");
              panel3.add(TFTF5);
              TFTF5.setBounds(190, 209, 20, TFTF5.getPreferredSize().height);

              //---- TFTP5 ----
              TFTP5.setToolTipText("taxe parafiscale");
              TFTP5.setComponentPopupMenu(BTD);
              TFTP5.setName("TFTP5");
              panel3.add(TFTP5);
              TFTP5.setBounds(410, 209, 18, TFTP5.getPreferredSize().height);

              //---- TFTE5 ----
              TFTE5.setComponentPopupMenu(BTD);
              TFTE5.setName("TFTE5");
              panel3.add(TFTE5);
              TFTE5.setBounds(452, 209, 18, TFTE5.getPreferredSize().height);

              //---- TFTF6 ----
              TFTF6.setComponentPopupMenu(BTD);
              TFTF6.setName("TFTF6");
              panel3.add(TFTF6);
              TFTF6.setBounds(190, 245, 20, TFTF6.getPreferredSize().height);

              //---- TFTP6 ----
              TFTP6.setToolTipText("taxe parafiscale");
              TFTP6.setComponentPopupMenu(BTD);
              TFTP6.setName("TFTP6");
              panel3.add(TFTP6);
              TFTP6.setBounds(410, 245, 18, TFTP6.getPreferredSize().height);

              //---- TFTE6 ----
              TFTE6.setComponentPopupMenu(BTD);
              TFTE6.setName("TFTE6");
              panel3.add(TFTE6);
              TFTE6.setBounds(452, 245, 18, TFTE6.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }

            GroupLayout xTitledPanel2ContentContainerLayout = new GroupLayout(xTitledPanel2ContentContainer);
            xTitledPanel2ContentContainer.setLayout(xTitledPanel2ContentContainerLayout);
            xTitledPanel2ContentContainerLayout.setHorizontalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addGroup(xTitledPanel2ContentContainerLayout.createParallelGroup()
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 805, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 805, GroupLayout.PREFERRED_SIZE))
                  .addGap(20, 20, 20))
            );
            xTitledPanel2ContentContainerLayout.setVerticalGroup(
              xTitledPanel2ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel2ContentContainerLayout.createSequentialGroup()
                  .addGap(17, 17, 17)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                  .addGap(42, 42, 42))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 521, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_48;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private JPanel panel2;
  private JLabel OBJ_65;
  private JLabel OBJ_58;
  private JLabel OBJ_66;
  private JLabel OBJ_68;
  private JLabel OBJ_52;
  private JLabel OBJ_53;
  private JLabel OBJ_54;
  private JLabel OBJ_55;
  private JLabel OBJ_56;
  private JLabel OBJ_57;
  private XRiTextField TFCX1;
  private XRiTextField TFCX2;
  private XRiTextField TFCX3;
  private XRiTextField TFCX4;
  private XRiTextField TFCX5;
  private XRiTextField TFCX6;
  private XRiTextField TFTXPX;
  private XRiTextField TFRBP;
  private JPanel panel3;
  private XRiComboBox TFCP1;
  private XRiComboBox TFCP2;
  private XRiComboBox TFCP3;
  private XRiComboBox TFCP4;
  private XRiComboBox TFCP5;
  private XRiComboBox TFCP6;
  private JLabel OBJ_70;
  private JLabel OBJ_73;
  private JLabel OBJ_113;
  private XRiTextField TFLI1;
  private XRiTextField TFLI2;
  private XRiTextField TFLI3;
  private XRiTextField TFLI4;
  private XRiTextField TFLI5;
  private XRiTextField TFLI6;
  private JLabel OBJ_71;
  private XRiTextField TFTV1;
  private XRiTextField TFTV2;
  private XRiTextField TFTV3;
  private XRiTextField TFTV4;
  private XRiTextField TFTV5;
  private XRiTextField TFTV6;
  private JLabel OBJ_72;
  private JPanel OBJ_112;
  private JLabel OBJ_110;
  private JLabel OBJ_111;
  private XRiTextField TFTF1;
  private XRiTextField TFTP1;
  private XRiTextField TFTE1;
  private XRiTextField TFTF2;
  private XRiTextField TFTP2;
  private XRiTextField TFTE2;
  private XRiTextField TFTF3;
  private XRiTextField TFTP3;
  private XRiTextField TFTE3;
  private XRiTextField TFTF4;
  private XRiTextField TFTP4;
  private XRiTextField TFTE4;
  private XRiTextField TFTF5;
  private XRiTextField TFTP5;
  private XRiTextField TFTE5;
  private XRiTextField TFTF6;
  private XRiTextField TFTP6;
  private XRiTextField TFTE6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
