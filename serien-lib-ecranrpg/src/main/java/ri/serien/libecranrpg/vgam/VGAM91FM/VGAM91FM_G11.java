
package ri.serien.libecranrpg.vgam.VGAM91FM;

// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class VGAM91FM_G11 extends JDialog {
  // Variables
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  
  // chiffre d'affaire première période pour 17 clients ou articles
  private String[] ca1 = new String[17];
  // chiffre d'affaire deuxième période pour 17 clients ou articles
  private String[] ca2 = new String[17];
  // quantités première période pour 17 clients ou articles
  private String[] qt1 = new String[17];
  // quantités deuxième période pour 17 clients ou articles
  private String[] qt2 = new String[17];
  // ratios pour 17 clients ou articles
  private String[] ra1 = new String[17];
  
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe1 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe2 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe3 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe4 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe5 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe6 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe7 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe8 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe9 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe10 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe11 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe12 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe13 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe14 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe15 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe16 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe17 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe18 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe19 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe20 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe21 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe22 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe23 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe24 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe25 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe26 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe27 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe28 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe29 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe30 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe31 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe32 = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe33 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  
  private String[] ligneDonnee = new String[17];
  private String[] libelle = new String[2];
  String[] lignelibelle = new String[17];
  
  public VGAM91FM_G11(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    // Valeur
    // label2.setText(lexique.HostFieldGetData("MG01"));
    
    // GRAPHE
    
    // Chargement des libellés
    // on charge les 2 périodes à partir de la première ligne de titres
    
    libelle[0] = lexique.HostFieldGetNumericString(lexique.HostFieldGetData("HLDA21").substring(26, 46).trim(), 0);
    
    libelle[1] = lexique.HostFieldGetNumericString(lexique.HostFieldGetData("HLDA21").substring(48, 68).trim(), 0);
    
    // Chargement des données
    
    // on charge toutes les lignes de la LD
    for (int i = 0; i < ligneDonnee.length; i++) {
      ligneDonnee[i] = lexique.HostFieldGetData("L2" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)));
    }
    
    // On crée les tables de données qui nous intéresse depuis l'EBCDIC
    
    // noms des onglets = numéro client ou article
    for (int i = 0; i < tabbedPane1.getTabCount(); i++) {
      tabbedPane1.setTitleAt(i, (lexique.HostFieldGetNumericString(ligneDonnee[i].substring(0, 7).trim(), 0)));
    }
    
    for (int i = 0; i < lignelibelle.length; i++) {
      lignelibelle[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(7, 26).trim(), 0);
    }
    
    for (int i = 0; i < ca1.length; i++) {
      ca1[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(27, 37).trim(), 0);
    }
    for (int i = 0; i < qt1.length; i++) {
      qt1[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(38, 48).trim(), 0);
    }
    for (int i = 0; i < ca2.length; i++) {
      ca2[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(49, 59).trim(), 2);
    }
    for (int i = 0; i < qt2.length; i++) {
      qt2[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(60, 70).trim(), 2);
    }
    for (int i = 0; i < ra1.length; i++) {
      ra1[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(71, 76).trim(), 2);
    }
    
    // CONSTITUTION DES ONGLETS
    // On sélectionne les données utiles pour chaque graphe (1 table = 1 graphe)
    
    // Graphes 1ère page
    String[] donneeG101 = new String[2];
    String[] donneeG201 = new String[2];
    
    chargeTable(donneeG101, 1, 1);
    chargeTable(donneeG201, 2, 1);
    
    // Graphes 2ème page
    String[] donneeG102 = new String[2];
    String[] donneeG202 = new String[2];
    
    chargeTable(donneeG102, 1, 2);
    chargeTable(donneeG202, 2, 2);
    
    // Graphes 3ème page
    String[] donneeG103 = new String[2];
    String[] donneeG203 = new String[2];
    
    chargeTable(donneeG103, 1, 3);
    chargeTable(donneeG203, 2, 3);
    
    // Graphes 4ème page
    String[] donneeG104 = new String[2];
    String[] donneeG204 = new String[2];
    
    chargeTable(donneeG104, 1, 4);
    chargeTable(donneeG204, 2, 4);
    
    // Graphes 5ème page
    String[] donneeG105 = new String[2];
    String[] donneeG205 = new String[2];
    
    chargeTable(donneeG105, 1, 5);
    chargeTable(donneeG205, 2, 5);
    
    // Graphes 6ème page
    String[] donneeG106 = new String[2];
    String[] donneeG206 = new String[2];
    
    chargeTable(donneeG106, 1, 6);
    chargeTable(donneeG206, 2, 6);
    
    // Graphes 7ème page
    String[] donneeG107 = new String[2];
    String[] donneeG207 = new String[2];
    
    chargeTable(donneeG107, 1, 7);
    chargeTable(donneeG207, 2, 7);
    
    // Graphes 8ème page
    String[] donneeG108 = new String[2];
    String[] donneeG208 = new String[2];
    
    chargeTable(donneeG108, 1, 8);
    chargeTable(donneeG208, 2, 8);
    
    // Graphes 9ème page
    String[] donneeG109 = new String[2];
    String[] donneeG209 = new String[2];
    
    chargeTable(donneeG109, 1, 9);
    chargeTable(donneeG209, 2, 9);
    
    // Graphes 10ème page
    String[] donneeG110 = new String[2];
    String[] donneeG210 = new String[2];
    
    chargeTable(donneeG110, 1, 10);
    chargeTable(donneeG210, 2, 10);
    
    // Graphes 11ème page
    String[] donneeG111 = new String[2];
    String[] donneeG211 = new String[2];
    
    chargeTable(donneeG111, 1, 11);
    chargeTable(donneeG211, 2, 11);
    
    // Graphes 12ème page
    String[] donneeG112 = new String[2];
    String[] donneeG212 = new String[2];
    
    chargeTable(donneeG112, 1, 12);
    chargeTable(donneeG212, 2, 12);
    
    // Graphes 13ème page
    String[] donneeG113 = new String[2];
    String[] donneeG213 = new String[2];
    
    chargeTable(donneeG113, 1, 13);
    chargeTable(donneeG213, 2, 13);
    
    // Graphes 14ème page
    String[] donneeG114 = new String[2];
    String[] donneeG214 = new String[2];
    
    chargeTable(donneeG114, 1, 14);
    chargeTable(donneeG214, 2, 14);
    
    // Graphes 15ème page
    String[] donneeG115 = new String[2];
    String[] donneeG215 = new String[2];
    
    chargeTable(donneeG115, 1, 15);
    chargeTable(donneeG215, 2, 15);
    
    // Graphes 16ème page
    String[] donneeG116 = new String[2];
    String[] donneeG216 = new String[2];
    
    chargeTable(donneeG116, 1, 16);
    chargeTable(donneeG216, 2, 16);
    
    // Graphes 17ème page
    String[] donneeG117 = new String[2];
    String[] donneeG217 = new String[2];
    
    chargeTable(donneeG117, 1, 17);
    chargeTable(donneeG217, 2, 17);
    
    // Préparation des données
    
    // Onglet 1
    preparationOnglet(graphe, l_graphe, donneeG101, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe1, l_graphe1, donneeG201, libelle, "Quantités");
    
    // Onglet 2
    preparationOnglet(graphe2, l_graphe2, donneeG102, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe3, l_graphe3, donneeG202, libelle, "Quantités");
    
    // Onglet 3
    preparationOnglet(graphe4, l_graphe4, donneeG103, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe5, l_graphe5, donneeG203, libelle, "Quantités");
    
    // Onglet 4
    preparationOnglet(graphe6, l_graphe6, donneeG104, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe7, l_graphe7, donneeG204, libelle, "Quantités");
    
    // Onglet 5
    preparationOnglet(graphe8, l_graphe8, donneeG105, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe9, l_graphe9, donneeG205, libelle, "Quantités");
    
    // Onglet 6
    preparationOnglet(graphe10, l_graphe10, donneeG106, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe11, l_graphe11, donneeG206, libelle, "Quantités");
    
    // Onglet 7
    preparationOnglet(graphe12, l_graphe12, donneeG107, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe13, l_graphe13, donneeG207, libelle, "Quantités");
    
    // Onglet 8
    preparationOnglet(graphe14, l_graphe14, donneeG108, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe15, l_graphe15, donneeG208, libelle, "Quantités");
    
    // Onglet 9
    preparationOnglet(graphe16, l_graphe16, donneeG109, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe17, l_graphe17, donneeG209, libelle, "Quantités");
    
    // Onglet 10
    preparationOnglet(graphe18, l_graphe18, donneeG110, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe19, l_graphe19, donneeG210, libelle, "Quantités");
    
    // Onglet 11
    preparationOnglet(graphe20, l_graphe20, donneeG111, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe21, l_graphe21, donneeG211, libelle, "Quantités");
    
    // Onglet 12
    preparationOnglet(graphe22, l_graphe22, donneeG112, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe23, l_graphe23, donneeG212, libelle, "Quantités");
    
    // Onglet 13
    preparationOnglet(graphe24, l_graphe24, donneeG113, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe25, l_graphe25, donneeG213, libelle, "Quantités");
    
    // Onglet 14
    preparationOnglet(graphe26, l_graphe26, donneeG114, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe27, l_graphe27, donneeG214, libelle, "Quantités");
    
    // Onglet 15
    preparationOnglet(graphe28, l_graphe28, donneeG115, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe29, l_graphe29, donneeG215, libelle, "Quantités");
    
    // Onglet 16
    preparationOnglet(graphe30, l_graphe30, donneeG116, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe31, l_graphe31, donneeG216, libelle, "Quantités");
    
    // Onglet 17
    preparationOnglet(graphe32, l_graphe32, donneeG117, libelle, "Chiffre d'affaires");
    preparationOnglet(graphe33, l_graphe33, donneeG217, libelle, "Quantités");
    
    
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques " + lexique.HostFieldGetData("L1I1")));
  }
  
  public Object[][] getDonnees(String[] tableDonnee, String[] tableLibelle) {
    
    Object[][] data = new Object[tableLibelle.length][tableDonnee.length];
    
    for (int i = 0; i < tableLibelle.length; i++) {
      data[i][0] = tableLibelle[i];
      tableDonnee[i] = tableDonnee[i].replaceAll("\\s", "0");
      data[i][1] = Double.parseDouble(tableDonnee[i]);
    }
    
    return data;
  }
  
  public void chargeTable(String[] tableEntree, int type, int onglet) {
    
    if (type == 1) {
      
      tableEntree[0] = ca1[onglet - 1];
      tableEntree[1] = ca2[onglet - 1];
      
    }
    
    if (type == 2) {
      tableEntree[0] = qt1[onglet - 1];
      tableEntree[1] = qt2[onglet - 1];
      
    }
  }
  
  public void preparationOnglet(RiGraphe graphe, JLabel icone, String[] donnees, String[] libelle, String titre) {
    graphe.setDonnee(getDonnees(donnees, libelle), "", false);
    graphe.getGraphe(titre, false);
    icone.setIcon(graphe.getPicture(icone.getWidth(), icone.getHeight()));
  }
  
  public void getData() {
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    panel2 = new JPanel();
    tabbedPane1 = new JTabbedPane();
    panel3 = new JPanel();
    l_graphe = new JLabel();
    l_graphe1 = new JLabel();
    panel4 = new JPanel();
    l_graphe2 = new JLabel();
    l_graphe3 = new JLabel();
    panel5 = new JPanel();
    l_graphe4 = new JLabel();
    l_graphe5 = new JLabel();
    panel6 = new JPanel();
    l_graphe6 = new JLabel();
    l_graphe7 = new JLabel();
    panel7 = new JPanel();
    l_graphe8 = new JLabel();
    l_graphe9 = new JLabel();
    panel8 = new JPanel();
    l_graphe10 = new JLabel();
    l_graphe11 = new JLabel();
    panel9 = new JPanel();
    l_graphe12 = new JLabel();
    l_graphe13 = new JLabel();
    panel10 = new JPanel();
    l_graphe14 = new JLabel();
    l_graphe15 = new JLabel();
    panel11 = new JPanel();
    l_graphe16 = new JLabel();
    l_graphe17 = new JLabel();
    panel12 = new JPanel();
    l_graphe18 = new JLabel();
    l_graphe19 = new JLabel();
    panel13 = new JPanel();
    l_graphe20 = new JLabel();
    l_graphe21 = new JLabel();
    panel14 = new JPanel();
    l_graphe22 = new JLabel();
    l_graphe23 = new JLabel();
    panel15 = new JPanel();
    l_graphe24 = new JLabel();
    l_graphe25 = new JLabel();
    panel16 = new JPanel();
    l_graphe26 = new JLabel();
    l_graphe27 = new JLabel();
    panel17 = new JPanel();
    l_graphe28 = new JLabel();
    l_graphe29 = new JLabel();
    panel18 = new JPanel();
    l_graphe30 = new JLabel();
    l_graphe31 = new JLabel();
    panel19 = new JPanel();
    l_graphe32 = new JLabel();
    l_graphe33 = new JLabel();
    panel1 = new JPanel();
    OBJ_10 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(335, 220));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== panel2 ========
    {
      panel2.setBackground(new Color(238, 238, 210));
      panel2.setName("panel2");
      panel2.setLayout(new BorderLayout());
      
      // ======== tabbedPane1 ========
      {
        tabbedPane1.setName("tabbedPane1");
        
        // ======== panel3 ========
        {
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);
          
          // ---- l_graphe ----
          l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe.setComponentPopupMenu(null);
          l_graphe.setBackground(new Color(214, 217, 223));
          l_graphe.setPreferredSize(new Dimension(800, 650));
          l_graphe.setName("l_graphe");
          panel3.add(l_graphe);
          l_graphe.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe1 ----
          l_graphe1.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe1.setComponentPopupMenu(null);
          l_graphe1.setBackground(new Color(214, 217, 223));
          l_graphe1.setPreferredSize(new Dimension(800, 650));
          l_graphe1.setName("l_graphe1");
          panel3.add(l_graphe1);
          l_graphe1.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel3);
        
        // ======== panel4 ========
        {
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);
          
          // ---- l_graphe2 ----
          l_graphe2.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe2.setComponentPopupMenu(null);
          l_graphe2.setBackground(new Color(214, 217, 223));
          l_graphe2.setPreferredSize(new Dimension(800, 650));
          l_graphe2.setName("l_graphe2");
          panel4.add(l_graphe2);
          l_graphe2.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe3 ----
          l_graphe3.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe3.setComponentPopupMenu(null);
          l_graphe3.setBackground(new Color(214, 217, 223));
          l_graphe3.setPreferredSize(new Dimension(800, 650));
          l_graphe3.setName("l_graphe3");
          panel4.add(l_graphe3);
          l_graphe3.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel4);
        
        // ======== panel5 ========
        {
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);
          
          // ---- l_graphe4 ----
          l_graphe4.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe4.setComponentPopupMenu(null);
          l_graphe4.setBackground(new Color(214, 217, 223));
          l_graphe4.setPreferredSize(new Dimension(800, 650));
          l_graphe4.setName("l_graphe4");
          panel5.add(l_graphe4);
          l_graphe4.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe5 ----
          l_graphe5.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe5.setComponentPopupMenu(null);
          l_graphe5.setBackground(new Color(214, 217, 223));
          l_graphe5.setPreferredSize(new Dimension(800, 650));
          l_graphe5.setName("l_graphe5");
          panel5.add(l_graphe5);
          l_graphe5.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel5.getComponentCount(); i++) {
              Rectangle bounds = panel5.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel5.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel5.setMinimumSize(preferredSize);
            panel5.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel5);
        
        // ======== panel6 ========
        {
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);
          
          // ---- l_graphe6 ----
          l_graphe6.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe6.setComponentPopupMenu(null);
          l_graphe6.setBackground(new Color(214, 217, 223));
          l_graphe6.setPreferredSize(new Dimension(800, 650));
          l_graphe6.setName("l_graphe6");
          panel6.add(l_graphe6);
          l_graphe6.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe7 ----
          l_graphe7.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe7.setComponentPopupMenu(null);
          l_graphe7.setBackground(new Color(214, 217, 223));
          l_graphe7.setPreferredSize(new Dimension(800, 650));
          l_graphe7.setName("l_graphe7");
          panel6.add(l_graphe7);
          l_graphe7.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel6.getComponentCount(); i++) {
              Rectangle bounds = panel6.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel6.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel6.setMinimumSize(preferredSize);
            panel6.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel6);
        
        // ======== panel7 ========
        {
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);
          
          // ---- l_graphe8 ----
          l_graphe8.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe8.setComponentPopupMenu(null);
          l_graphe8.setBackground(new Color(214, 217, 223));
          l_graphe8.setPreferredSize(new Dimension(800, 650));
          l_graphe8.setName("l_graphe8");
          panel7.add(l_graphe8);
          l_graphe8.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe9 ----
          l_graphe9.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe9.setComponentPopupMenu(null);
          l_graphe9.setBackground(new Color(214, 217, 223));
          l_graphe9.setPreferredSize(new Dimension(800, 650));
          l_graphe9.setName("l_graphe9");
          panel7.add(l_graphe9);
          l_graphe9.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel7.getComponentCount(); i++) {
              Rectangle bounds = panel7.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel7.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel7.setMinimumSize(preferredSize);
            panel7.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel7);
        
        // ======== panel8 ========
        {
          panel8.setOpaque(false);
          panel8.setName("panel8");
          panel8.setLayout(null);
          
          // ---- l_graphe10 ----
          l_graphe10.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe10.setComponentPopupMenu(null);
          l_graphe10.setBackground(new Color(214, 217, 223));
          l_graphe10.setPreferredSize(new Dimension(800, 650));
          l_graphe10.setName("l_graphe10");
          panel8.add(l_graphe10);
          l_graphe10.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe11 ----
          l_graphe11.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe11.setComponentPopupMenu(null);
          l_graphe11.setBackground(new Color(214, 217, 223));
          l_graphe11.setPreferredSize(new Dimension(800, 650));
          l_graphe11.setName("l_graphe11");
          panel8.add(l_graphe11);
          l_graphe11.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel8.getComponentCount(); i++) {
              Rectangle bounds = panel8.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel8.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel8.setMinimumSize(preferredSize);
            panel8.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel8);
        
        // ======== panel9 ========
        {
          panel9.setOpaque(false);
          panel9.setName("panel9");
          panel9.setLayout(null);
          
          // ---- l_graphe12 ----
          l_graphe12.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe12.setComponentPopupMenu(null);
          l_graphe12.setBackground(new Color(214, 217, 223));
          l_graphe12.setPreferredSize(new Dimension(800, 650));
          l_graphe12.setName("l_graphe12");
          panel9.add(l_graphe12);
          l_graphe12.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe13 ----
          l_graphe13.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe13.setComponentPopupMenu(null);
          l_graphe13.setBackground(new Color(214, 217, 223));
          l_graphe13.setPreferredSize(new Dimension(800, 650));
          l_graphe13.setName("l_graphe13");
          panel9.add(l_graphe13);
          l_graphe13.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel9.getComponentCount(); i++) {
              Rectangle bounds = panel9.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel9.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel9.setMinimumSize(preferredSize);
            panel9.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel9);
        
        // ======== panel10 ========
        {
          panel10.setOpaque(false);
          panel10.setName("panel10");
          panel10.setLayout(null);
          
          // ---- l_graphe14 ----
          l_graphe14.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe14.setComponentPopupMenu(null);
          l_graphe14.setBackground(new Color(214, 217, 223));
          l_graphe14.setPreferredSize(new Dimension(800, 650));
          l_graphe14.setName("l_graphe14");
          panel10.add(l_graphe14);
          l_graphe14.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe15 ----
          l_graphe15.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe15.setComponentPopupMenu(null);
          l_graphe15.setBackground(new Color(214, 217, 223));
          l_graphe15.setPreferredSize(new Dimension(800, 650));
          l_graphe15.setName("l_graphe15");
          panel10.add(l_graphe15);
          l_graphe15.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel10.getComponentCount(); i++) {
              Rectangle bounds = panel10.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel10.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel10.setMinimumSize(preferredSize);
            panel10.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel10);
        
        // ======== panel11 ========
        {
          panel11.setOpaque(false);
          panel11.setName("panel11");
          panel11.setLayout(null);
          
          // ---- l_graphe16 ----
          l_graphe16.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe16.setComponentPopupMenu(null);
          l_graphe16.setBackground(new Color(214, 217, 223));
          l_graphe16.setPreferredSize(new Dimension(800, 650));
          l_graphe16.setName("l_graphe16");
          panel11.add(l_graphe16);
          l_graphe16.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe17 ----
          l_graphe17.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe17.setComponentPopupMenu(null);
          l_graphe17.setBackground(new Color(214, 217, 223));
          l_graphe17.setPreferredSize(new Dimension(800, 650));
          l_graphe17.setName("l_graphe17");
          panel11.add(l_graphe17);
          l_graphe17.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel11.getComponentCount(); i++) {
              Rectangle bounds = panel11.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel11.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel11.setMinimumSize(preferredSize);
            panel11.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel11);
        
        // ======== panel12 ========
        {
          panel12.setOpaque(false);
          panel12.setName("panel12");
          panel12.setLayout(null);
          
          // ---- l_graphe18 ----
          l_graphe18.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe18.setComponentPopupMenu(null);
          l_graphe18.setBackground(new Color(214, 217, 223));
          l_graphe18.setPreferredSize(new Dimension(800, 650));
          l_graphe18.setName("l_graphe18");
          panel12.add(l_graphe18);
          l_graphe18.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe19 ----
          l_graphe19.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe19.setComponentPopupMenu(null);
          l_graphe19.setBackground(new Color(214, 217, 223));
          l_graphe19.setPreferredSize(new Dimension(800, 650));
          l_graphe19.setName("l_graphe19");
          panel12.add(l_graphe19);
          l_graphe19.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel12.getComponentCount(); i++) {
              Rectangle bounds = panel12.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel12.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel12.setMinimumSize(preferredSize);
            panel12.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel12);
        
        // ======== panel13 ========
        {
          panel13.setOpaque(false);
          panel13.setName("panel13");
          panel13.setLayout(null);
          
          // ---- l_graphe20 ----
          l_graphe20.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe20.setComponentPopupMenu(null);
          l_graphe20.setBackground(new Color(214, 217, 223));
          l_graphe20.setPreferredSize(new Dimension(800, 650));
          l_graphe20.setName("l_graphe20");
          panel13.add(l_graphe20);
          l_graphe20.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe21 ----
          l_graphe21.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe21.setComponentPopupMenu(null);
          l_graphe21.setBackground(new Color(214, 217, 223));
          l_graphe21.setPreferredSize(new Dimension(800, 650));
          l_graphe21.setName("l_graphe21");
          panel13.add(l_graphe21);
          l_graphe21.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel13.getComponentCount(); i++) {
              Rectangle bounds = panel13.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel13.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel13.setMinimumSize(preferredSize);
            panel13.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel13);
        
        // ======== panel14 ========
        {
          panel14.setOpaque(false);
          panel14.setName("panel14");
          panel14.setLayout(null);
          
          // ---- l_graphe22 ----
          l_graphe22.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe22.setComponentPopupMenu(null);
          l_graphe22.setBackground(new Color(214, 217, 223));
          l_graphe22.setPreferredSize(new Dimension(800, 650));
          l_graphe22.setName("l_graphe22");
          panel14.add(l_graphe22);
          l_graphe22.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe23 ----
          l_graphe23.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe23.setComponentPopupMenu(null);
          l_graphe23.setBackground(new Color(214, 217, 223));
          l_graphe23.setPreferredSize(new Dimension(800, 650));
          l_graphe23.setName("l_graphe23");
          panel14.add(l_graphe23);
          l_graphe23.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel14.getComponentCount(); i++) {
              Rectangle bounds = panel14.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel14.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel14.setMinimumSize(preferredSize);
            panel14.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel14);
        
        // ======== panel15 ========
        {
          panel15.setOpaque(false);
          panel15.setName("panel15");
          panel15.setLayout(null);
          
          // ---- l_graphe24 ----
          l_graphe24.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe24.setComponentPopupMenu(null);
          l_graphe24.setBackground(new Color(214, 217, 223));
          l_graphe24.setPreferredSize(new Dimension(800, 650));
          l_graphe24.setName("l_graphe24");
          panel15.add(l_graphe24);
          l_graphe24.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe25 ----
          l_graphe25.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe25.setComponentPopupMenu(null);
          l_graphe25.setBackground(new Color(214, 217, 223));
          l_graphe25.setPreferredSize(new Dimension(800, 650));
          l_graphe25.setName("l_graphe25");
          panel15.add(l_graphe25);
          l_graphe25.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel15.getComponentCount(); i++) {
              Rectangle bounds = panel15.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel15.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel15.setMinimumSize(preferredSize);
            panel15.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel15);
        
        // ======== panel16 ========
        {
          panel16.setOpaque(false);
          panel16.setName("panel16");
          panel16.setLayout(null);
          
          // ---- l_graphe26 ----
          l_graphe26.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe26.setComponentPopupMenu(null);
          l_graphe26.setBackground(new Color(214, 217, 223));
          l_graphe26.setPreferredSize(new Dimension(800, 650));
          l_graphe26.setName("l_graphe26");
          panel16.add(l_graphe26);
          l_graphe26.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe27 ----
          l_graphe27.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe27.setComponentPopupMenu(null);
          l_graphe27.setBackground(new Color(214, 217, 223));
          l_graphe27.setPreferredSize(new Dimension(800, 650));
          l_graphe27.setName("l_graphe27");
          panel16.add(l_graphe27);
          l_graphe27.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel16.getComponentCount(); i++) {
              Rectangle bounds = panel16.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel16.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel16.setMinimumSize(preferredSize);
            panel16.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel16);
        
        // ======== panel17 ========
        {
          panel17.setOpaque(false);
          panel17.setName("panel17");
          panel17.setLayout(null);
          
          // ---- l_graphe28 ----
          l_graphe28.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe28.setComponentPopupMenu(null);
          l_graphe28.setBackground(new Color(214, 217, 223));
          l_graphe28.setPreferredSize(new Dimension(800, 650));
          l_graphe28.setName("l_graphe28");
          panel17.add(l_graphe28);
          l_graphe28.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe29 ----
          l_graphe29.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe29.setComponentPopupMenu(null);
          l_graphe29.setBackground(new Color(214, 217, 223));
          l_graphe29.setPreferredSize(new Dimension(800, 650));
          l_graphe29.setName("l_graphe29");
          panel17.add(l_graphe29);
          l_graphe29.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel17.getComponentCount(); i++) {
              Rectangle bounds = panel17.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel17.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel17.setMinimumSize(preferredSize);
            panel17.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel17);
        
        // ======== panel18 ========
        {
          panel18.setOpaque(false);
          panel18.setName("panel18");
          panel18.setLayout(null);
          
          // ---- l_graphe30 ----
          l_graphe30.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe30.setComponentPopupMenu(null);
          l_graphe30.setBackground(new Color(214, 217, 223));
          l_graphe30.setPreferredSize(new Dimension(800, 650));
          l_graphe30.setName("l_graphe30");
          panel18.add(l_graphe30);
          l_graphe30.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe31 ----
          l_graphe31.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe31.setComponentPopupMenu(null);
          l_graphe31.setBackground(new Color(214, 217, 223));
          l_graphe31.setPreferredSize(new Dimension(800, 650));
          l_graphe31.setName("l_graphe31");
          panel18.add(l_graphe31);
          l_graphe31.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel18.getComponentCount(); i++) {
              Rectangle bounds = panel18.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel18.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel18.setMinimumSize(preferredSize);
            panel18.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel18);
        
        // ======== panel19 ========
        {
          panel19.setBackground(new Color(238, 238, 210));
          panel19.setName("panel19");
          panel19.setLayout(null);
          
          // ---- l_graphe32 ----
          l_graphe32.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe32.setComponentPopupMenu(null);
          l_graphe32.setBackground(new Color(214, 217, 223));
          l_graphe32.setPreferredSize(new Dimension(800, 650));
          l_graphe32.setName("l_graphe32");
          panel19.add(l_graphe32);
          l_graphe32.setBounds(5, 20, 800, 455);
          
          // ---- l_graphe33 ----
          l_graphe33.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe33.setComponentPopupMenu(null);
          l_graphe33.setBackground(new Color(214, 217, 223));
          l_graphe33.setPreferredSize(new Dimension(800, 650));
          l_graphe33.setName("l_graphe33");
          panel19.add(l_graphe33);
          l_graphe33.setBounds(820, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel19.getComponentCount(); i++) {
              Rectangle bounds = panel19.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel19.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel19.setMinimumSize(preferredSize);
            panel19.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("B", panel19);
      }
      panel2.add(tabbedPane1, BorderLayout.CENTER);
      
      // ======== panel1 ========
      {
        panel1.setBackground(new Color(214, 217, 223));
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(null);
        
        // ---- OBJ_10 ----
        OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_10.setToolTipText("Retour");
        OBJ_10.setName("OBJ_10");
        OBJ_10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_10ActionPerformed(e);
          }
        });
        panel1.add(OBJ_10);
        OBJ_10.setBounds(1030, 5, 54, 40);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      panel2.add(panel1, BorderLayout.SOUTH);
    }
    contentPane.add(panel2, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel panel2;
  private JTabbedPane tabbedPane1;
  private JPanel panel3;
  private JLabel l_graphe;
  private JLabel l_graphe1;
  private JPanel panel4;
  private JLabel l_graphe2;
  private JLabel l_graphe3;
  private JPanel panel5;
  private JLabel l_graphe4;
  private JLabel l_graphe5;
  private JPanel panel6;
  private JLabel l_graphe6;
  private JLabel l_graphe7;
  private JPanel panel7;
  private JLabel l_graphe8;
  private JLabel l_graphe9;
  private JPanel panel8;
  private JLabel l_graphe10;
  private JLabel l_graphe11;
  private JPanel panel9;
  private JLabel l_graphe12;
  private JLabel l_graphe13;
  private JPanel panel10;
  private JLabel l_graphe14;
  private JLabel l_graphe15;
  private JPanel panel11;
  private JLabel l_graphe16;
  private JLabel l_graphe17;
  private JPanel panel12;
  private JLabel l_graphe18;
  private JLabel l_graphe19;
  private JPanel panel13;
  private JLabel l_graphe20;
  private JLabel l_graphe21;
  private JPanel panel14;
  private JLabel l_graphe22;
  private JLabel l_graphe23;
  private JPanel panel15;
  private JLabel l_graphe24;
  private JLabel l_graphe25;
  private JPanel panel16;
  private JLabel l_graphe26;
  private JLabel l_graphe27;
  private JPanel panel17;
  private JLabel l_graphe28;
  private JLabel l_graphe29;
  private JPanel panel18;
  private JLabel l_graphe30;
  private JLabel l_graphe31;
  private JPanel panel19;
  private JLabel l_graphe32;
  private JLabel l_graphe33;
  private JPanel panel1;
  private JButton OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
