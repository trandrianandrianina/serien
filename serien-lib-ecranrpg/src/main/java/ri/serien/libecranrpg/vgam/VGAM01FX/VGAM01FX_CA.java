
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_CA extends SNPanelEcranRPG implements ioFrame {
  
  private String[] CAP10_Value = { "", "J", "S", "D", "Q", "M", };
  
  private String[] CAP09_Value = { "", "J", "S", "D", "Q", "M", };
  private String[] CAP08_Value = { "", "J", "S", "D", "Q", "M", };
  private String[] CAP07_Value = { "", "J", "S", "D", "Q", "M", };
  private String[] CAP06_Value = { "", "J", "S", "D", "Q", "M", };
  private String[] CAP05_Value = { "", "J", "S", "D", "Q", "M", };
  private String[] CAP04_Value = { "", "J", "S", "D", "Q", "M", };
  private String[] CAP03_Value = { "", "J", "S", "D", "Q", "M", };
  private String[] CAP02_Value = { "", "J", "S", "D", "Q", "M", };
  private String[] CAP01_Value = { "", "J", "S", "D", "Q", "M", };
  // private String[] _LISTCV_Top=null;
  
  public VGAM01FX_CA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    CAP10.setValeurs(CAP10_Value, null);
    CAP09.setValeurs(CAP09_Value, null);
    CAP08.setValeurs(CAP08_Value, null);
    CAP07.setValeurs(CAP07_Value, null);
    CAP06.setValeurs(CAP06_Value, null);
    CAP05.setValeurs(CAP05_Value, null);
    CAP04.setValeurs(CAP04_Value, null);
    CAP03.setValeurs(CAP03_Value, null);
    CAP02.setValeurs(CAP02_Value, null);
    CAP01.setValeurs(CAP01_Value, null);
    CASEC.setValeursSelection("X", "");
    CAATT.setValeursSelection("1", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LISTCV, LISTCV.get_LIST_Title_Data_Brut(), _LISTCV_Top,
    // _LIST_Justification);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    OBJ_59.setVisible(lexique.isTrue("N53"));
    OBJ_72.setVisible(lexique.isTrue("N53"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@ - @TITRE2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_59ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(22, 78);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_72ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(22, 67);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_48 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_centrage = new JPanel();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel3 = new JPanel();
    LCG01 = new XRiTextField();
    label1 = new JLabel();
    CAC01 = new XRiTextField();
    CAC02 = new XRiTextField();
    LCG02 = new XRiTextField();
    CAC03 = new XRiTextField();
    LCG03 = new XRiTextField();
    CAC04 = new XRiTextField();
    LCG04 = new XRiTextField();
    LCG05 = new XRiTextField();
    CAC05 = new XRiTextField();
    LCG06 = new XRiTextField();
    CAC06 = new XRiTextField();
    LCG07 = new XRiTextField();
    CAC07 = new XRiTextField();
    LCG08 = new XRiTextField();
    CAC08 = new XRiTextField();
    LCG09 = new XRiTextField();
    CAC09 = new XRiTextField();
    LCG10 = new XRiTextField();
    CAC10 = new XRiTextField();
    LCG11 = new XRiTextField();
    CAC11 = new XRiTextField();
    LCG12 = new XRiTextField();
    CAC12 = new XRiTextField();
    LCG13 = new XRiTextField();
    CAC13 = new XRiTextField();
    LCG14 = new XRiTextField();
    CAC14 = new XRiTextField();
    LCG15 = new XRiTextField();
    CAC15 = new XRiTextField();
    CAC17 = new XRiTextField();
    LCG17 = new XRiTextField();
    CAC18 = new XRiTextField();
    LCG18 = new XRiTextField();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    label18 = new JLabel();
    label19 = new JLabel();
    label20 = new JLabel();
    label21 = new JLabel();
    label22 = new JLabel();
    panel4 = new JPanel();
    OBJ_63 = new JLabel();
    CAETC1 = new XRiTextField();
    OBJ_65 = new JLabel();
    CAJOA = new XRiTextField();
    OBJ_67 = new JLabel();
    CASEC = new XRiCheckBox();
    OBJ_70 = new JLabel();
    CAATT = new XRiCheckBox();
    OBJ_72 = new JButton();
    OBJ_59 = new JButton();
    panel6 = new JPanel();
    OBJ_97 = new JLabel();
    OBJ_110 = new JLabel();
    CAP01 = new XRiComboBox();
    CAP02 = new XRiComboBox();
    CAP03 = new XRiComboBox();
    CAP04 = new XRiComboBox();
    CAP05 = new XRiComboBox();
    CAP06 = new XRiComboBox();
    CAP07 = new XRiComboBox();
    CAP08 = new XRiComboBox();
    CAP09 = new XRiComboBox();
    CAP10 = new XRiComboBox();
    CAJ01 = new XRiTextField();
    CAJ02 = new XRiTextField();
    CAJ03 = new XRiTextField();
    CAJ04 = new XRiTextField();
    CAJ05 = new XRiTextField();
    CAJ06 = new XRiTextField();
    CAJ07 = new XRiTextField();
    CAJ08 = new XRiTextField();
    CAJ09 = new XRiTextField();
    CAJ10 = new XRiTextField();
    OBJ_77 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_96 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_102 = new JLabel();
    OBJ_105 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_48 ----
          OBJ_48.setText("Ordre");
          OBJ_48.setName("OBJ_48");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Axes analytiques");
              riSousMenu_bt7.setToolTipText("Axes analytiques");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Comptabilisation ");
              riSousMenu_bt8.setToolTipText("Comptabilisation par anticipation");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Historique modifications");
              riSousMenu_bt9.setToolTipText("Historique des modifications");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1000, 600));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Plan comptable des achats");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setPreferredSize(new Dimension(920, 520));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder(""));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- LCG01 ----
              LCG01.setName("LCG01");
              panel3.add(LCG01);
              LCG01.setBounds(215, 40, 480, 25);

              //---- label1 ----
              label1.setText("N\u00b0CO achats HT 1");
              label1.setHorizontalAlignment(SwingConstants.RIGHT);
              label1.setName("label1");
              panel3.add(label1);
              label1.setBounds(10, 40, 150, 25);

              //---- CAC01 ----
              CAC01.setName("CAC01");
              panel3.add(CAC01);
              CAC01.setBounds(170, 40, 40, 25);

              //---- CAC02 ----
              CAC02.setName("CAC02");
              panel3.add(CAC02);
              CAC02.setBounds(170, 60, 40, 25);

              //---- LCG02 ----
              LCG02.setName("LCG02");
              panel3.add(LCG02);
              LCG02.setBounds(215, 60, 480, 25);

              //---- CAC03 ----
              CAC03.setName("CAC03");
              panel3.add(CAC03);
              CAC03.setBounds(170, 80, 40, 25);

              //---- LCG03 ----
              LCG03.setName("LCG03");
              panel3.add(LCG03);
              LCG03.setBounds(215, 80, 480, 25);

              //---- CAC04 ----
              CAC04.setName("CAC04");
              panel3.add(CAC04);
              CAC04.setBounds(170, 100, 40, 25);

              //---- LCG04 ----
              LCG04.setName("LCG04");
              panel3.add(LCG04);
              LCG04.setBounds(215, 100, 480, 25);

              //---- LCG05 ----
              LCG05.setName("LCG05");
              panel3.add(LCG05);
              LCG05.setBounds(215, 120, 480, 25);

              //---- CAC05 ----
              CAC05.setName("CAC05");
              panel3.add(CAC05);
              CAC05.setBounds(170, 120, 40, 25);

              //---- LCG06 ----
              LCG06.setName("LCG06");
              panel3.add(LCG06);
              LCG06.setBounds(215, 140, 480, 25);

              //---- CAC06 ----
              CAC06.setName("CAC06");
              panel3.add(CAC06);
              CAC06.setBounds(170, 140, 40, 25);

              //---- LCG07 ----
              LCG07.setName("LCG07");
              panel3.add(LCG07);
              LCG07.setBounds(215, 160, 480, 25);

              //---- CAC07 ----
              CAC07.setName("CAC07");
              panel3.add(CAC07);
              CAC07.setBounds(170, 160, 40, 25);

              //---- LCG08 ----
              LCG08.setName("LCG08");
              panel3.add(LCG08);
              LCG08.setBounds(215, 180, 480, 25);

              //---- CAC08 ----
              CAC08.setName("CAC08");
              panel3.add(CAC08);
              CAC08.setBounds(170, 180, 40, 25);

              //---- LCG09 ----
              LCG09.setName("LCG09");
              panel3.add(LCG09);
              LCG09.setBounds(215, 200, 480, 25);

              //---- CAC09 ----
              CAC09.setName("CAC09");
              panel3.add(CAC09);
              CAC09.setBounds(170, 200, 40, 25);

              //---- LCG10 ----
              LCG10.setName("LCG10");
              panel3.add(LCG10);
              LCG10.setBounds(215, 220, 480, 25);

              //---- CAC10 ----
              CAC10.setName("CAC10");
              panel3.add(CAC10);
              CAC10.setBounds(170, 220, 40, 25);

              //---- LCG11 ----
              LCG11.setName("LCG11");
              panel3.add(LCG11);
              LCG11.setBounds(215, 240, 480, 25);

              //---- CAC11 ----
              CAC11.setName("CAC11");
              panel3.add(CAC11);
              CAC11.setBounds(170, 240, 40, 25);

              //---- LCG12 ----
              LCG12.setName("LCG12");
              panel3.add(LCG12);
              LCG12.setBounds(215, 260, 480, 25);

              //---- CAC12 ----
              CAC12.setName("CAC12");
              panel3.add(CAC12);
              CAC12.setBounds(170, 260, 40, 25);

              //---- LCG13 ----
              LCG13.setName("LCG13");
              panel3.add(LCG13);
              LCG13.setBounds(215, 280, 480, 25);

              //---- CAC13 ----
              CAC13.setName("CAC13");
              panel3.add(CAC13);
              CAC13.setBounds(170, 280, 40, 25);

              //---- LCG14 ----
              LCG14.setName("LCG14");
              panel3.add(LCG14);
              LCG14.setBounds(215, 300, 480, 25);

              //---- CAC14 ----
              CAC14.setName("CAC14");
              panel3.add(CAC14);
              CAC14.setBounds(170, 300, 40, 25);

              //---- LCG15 ----
              LCG15.setName("LCG15");
              panel3.add(LCG15);
              LCG15.setBounds(215, 320, 480, 25);

              //---- CAC15 ----
              CAC15.setName("CAC15");
              panel3.add(CAC15);
              CAC15.setBounds(170, 320, 40, 25);

              //---- CAC17 ----
              CAC17.setName("CAC17");
              panel3.add(CAC17);
              CAC17.setBounds(170, 340, 40, 25);

              //---- LCG17 ----
              LCG17.setName("LCG17");
              panel3.add(LCG17);
              LCG17.setBounds(215, 340, 480, 25);

              //---- CAC18 ----
              CAC18.setName("CAC18");
              panel3.add(CAC18);
              CAC18.setBounds(170, 360, 40, 25);

              //---- LCG18 ----
              LCG18.setName("LCG18");
              panel3.add(LCG18);
              LCG18.setBounds(215, 360, 480, 25);

              //---- label2 ----
              label2.setText("N\u00b0CO achats HT 2");
              label2.setHorizontalAlignment(SwingConstants.RIGHT);
              label2.setName("label2");
              panel3.add(label2);
              label2.setBounds(10, 60, 150, 25);

              //---- label3 ----
              label3.setText("N\u00b0CO achats HT 3");
              label3.setHorizontalAlignment(SwingConstants.RIGHT);
              label3.setName("label3");
              panel3.add(label3);
              label3.setBounds(10, 80, 150, 25);

              //---- label4 ----
              label4.setText("N\u00b0CO achats HT 4");
              label4.setHorizontalAlignment(SwingConstants.RIGHT);
              label4.setName("label4");
              panel3.add(label4);
              label4.setBounds(10, 100, 150, 25);

              //---- label5 ----
              label5.setText("N\u00b0CO achats HT 5");
              label5.setHorizontalAlignment(SwingConstants.RIGHT);
              label5.setName("label5");
              panel3.add(label5);
              label5.setBounds(10, 120, 150, 25);

              //---- label6 ----
              label6.setText("N\u00b0CO achats HT 6");
              label6.setHorizontalAlignment(SwingConstants.RIGHT);
              label6.setName("label6");
              panel3.add(label6);
              label6.setBounds(10, 140, 150, 25);

              //---- label7 ----
              label7.setText("N\u00b0CO achats HT 7");
              label7.setHorizontalAlignment(SwingConstants.RIGHT);
              label7.setName("label7");
              panel3.add(label7);
              label7.setBounds(10, 160, 150, 25);

              //---- label8 ----
              label8.setText("N\u00b0CO achats HT 8");
              label8.setHorizontalAlignment(SwingConstants.RIGHT);
              label8.setName("label8");
              panel3.add(label8);
              label8.setBounds(10, 180, 150, 25);

              //---- label9 ----
              label9.setText("N\u00b0CO achats HT 9");
              label9.setHorizontalAlignment(SwingConstants.RIGHT);
              label9.setName("label9");
              panel3.add(label9);
              label9.setBounds(10, 200, 150, 25);

              //---- label10 ----
              label10.setText("N\u00b0CO TVA \u00e0 r\u00e9cup\u00e9rer 1");
              label10.setHorizontalAlignment(SwingConstants.RIGHT);
              label10.setName("label10");
              panel3.add(label10);
              label10.setBounds(10, 220, 150, 25);

              //---- label11 ----
              label11.setText("N\u00b0CO TVA \u00e0 r\u00e9cup\u00e9rer 2");
              label11.setHorizontalAlignment(SwingConstants.RIGHT);
              label11.setName("label11");
              panel3.add(label11);
              label11.setBounds(10, 240, 150, 25);

              //---- label12 ----
              label12.setText("N\u00b0CO TVA \u00e0 r\u00e9cup\u00e9rer 3");
              label12.setHorizontalAlignment(SwingConstants.RIGHT);
              label12.setName("label12");
              panel3.add(label12);
              label12.setBounds(10, 260, 150, 25);

              //---- label13 ----
              label13.setText("N\u00b0CO TVA \u00e0 r\u00e9cup\u00e9rer 4");
              label13.setHorizontalAlignment(SwingConstants.RIGHT);
              label13.setName("label13");
              panel3.add(label13);
              label13.setBounds(10, 280, 150, 25);

              //---- label14 ----
              label14.setText("N\u00b0CO TVA \u00e0 r\u00e9cup\u00e9rer 5");
              label14.setHorizontalAlignment(SwingConstants.RIGHT);
              label14.setName("label14");
              panel3.add(label14);
              label14.setBounds(10, 300, 150, 25);

              //---- label15 ----
              label15.setText("N\u00b0CO TVA \u00e0 r\u00e9cup\u00e9rer 6");
              label15.setHorizontalAlignment(SwingConstants.RIGHT);
              label15.setName("label15");
              panel3.add(label15);
              label15.setBounds(10, 320, 150, 25);

              //---- label16 ----
              label16.setText("N\u00b0CO Escomptes obtenus");
              label16.setHorizontalAlignment(SwingConstants.RIGHT);
              label16.setName("label16");
              panel3.add(label16);
              label16.setBounds(10, 340, 150, 25);

              //---- label17 ----
              label17.setText("N\u00b0CO Stock flottant");
              label17.setHorizontalAlignment(SwingConstants.RIGHT);
              label17.setName("label17");
              panel3.add(label17);
              label17.setBounds(10, 360, 150, 25);

              //---- label18 ----
              label18.setText("March.");
              label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
              label18.setName("label18");
              panel3.add(label18);
              label18.setBounds(220, 15, 50, 25);

              //---- label19 ----
              label19.setText("Fr.G\u00e9n");
              label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
              label19.setName("label19");
              panel3.add(label19);
              label19.setBounds(265, 15, 45, 25);

              //---- label20 ----
              label20.setText("Immob.");
              label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
              label20.setName("label20");
              panel3.add(label20);
              label20.setBounds(310, 15, 50, 25);

              //---- label21 ----
              label21.setText("Libell\u00e9");
              label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD));
              label21.setName("label21");
              panel3.add(label21);
              label21.setBounds(360, 15, 335, 25);

              //---- label22 ----
              label22.setText("CO");
              label22.setHorizontalAlignment(SwingConstants.CENTER);
              label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD));
              label22.setName("label22");
              panel3.add(label22);
              label22.setBounds(170, 15, 40, 25);
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(0, 5, 705, 400);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder(""));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- OBJ_63 ----
              OBJ_63.setText("Etablissement comptable");
              OBJ_63.setName("OBJ_63");
              panel4.add(OBJ_63);
              OBJ_63.setBounds(15, 20, 156, 20);

              //---- CAETC1 ----
              CAETC1.setComponentPopupMenu(BTD);
              CAETC1.setName("CAETC1");
              panel4.add(CAETC1);
              CAETC1.setBounds(200, 15, 40, CAETC1.getPreferredSize().height);

              //---- OBJ_65 ----
              OBJ_65.setText("Code journal analytique");
              OBJ_65.setName("OBJ_65");
              panel4.add(OBJ_65);
              OBJ_65.setBounds(15, 65, 144, 20);

              //---- CAJOA ----
              CAJOA.setComponentPopupMenu(BTD);
              CAJOA.setName("CAJOA");
              panel4.add(CAJOA);
              CAJOA.setBounds(200, 60, 40, CAJOA.getPreferredSize().height);

              //---- OBJ_67 ----
              OBJ_67.setText("Suspension \u00e9dition compte-rendu");
              OBJ_67.setName("OBJ_67");
              panel4.add(OBJ_67);
              OBJ_67.setBounds(15, 115, 190, 20);

              //---- CASEC ----
              CASEC.setComponentPopupMenu(BTD);
              CASEC.setName("CASEC");
              panel4.add(CASEC);
              CASEC.setBounds(205, 115, 20, CASEC.getPreferredSize().height);

              //---- OBJ_70 ----
              OBJ_70.setText("Folio en attente");
              OBJ_70.setName("OBJ_70");
              panel4.add(OBJ_70);
              OBJ_70.setBounds(15, 160, 94, 20);

              //---- CAATT ----
              CAATT.setComponentPopupMenu(BTD);
              CAATT.setName("CAATT");
              panel4.add(CAATT);
              CAATT.setBounds(205, 160, 20, CAATT.getPreferredSize().height);

              //---- OBJ_72 ----
              OBJ_72.setText("Axes analytiques");
              OBJ_72.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_72.setName("OBJ_72");
              OBJ_72.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_72ActionPerformed(e);
                }
              });
              panel4.add(OBJ_72);
              OBJ_72.setBounds(15, 205, 210, 28);

              //---- OBJ_59 ----
              OBJ_59.setText("Comptabilisation par anticipation");
              OBJ_59.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_59.setName("OBJ_59");
              OBJ_59.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_59ActionPerformed(e);
                }
              });
              panel4.add(OBJ_59);
              OBJ_59.setBounds(15, 255, 210, 28);
            }
            xTitledPanel1ContentContainer.add(panel4);
            panel4.setBounds(710, 5, 255, 400);

            //======== panel6 ========
            {
              panel6.setBorder(new TitledBorder("Codes journaux d'achat sur p\u00e9riodicit\u00e9"));
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);

              //---- OBJ_97 ----
              OBJ_97.setName("OBJ_97");
              panel6.add(OBJ_97);
              OBJ_97.setBounds(15, 35, 130, OBJ_97.getPreferredSize().height);

              //---- OBJ_110 ----
              OBJ_110.setName("OBJ_110");
              panel6.add(OBJ_110);
              OBJ_110.setBounds(new Rectangle(new Point(15, 56), OBJ_110.getPreferredSize()));

              //---- CAP01 ----
              CAP01.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Journ\u00e9e",
                "Semaine",
                "D\u00e9cade",
                "Quinzaine",
                "Mois"
              }));
              CAP01.setComponentPopupMenu(BTD);
              CAP01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAP01.setName("CAP01");
              panel6.add(CAP01);
              CAP01.setBounds(79, 35, 91, CAP01.getPreferredSize().height);

              //---- CAP02 ----
              CAP02.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Journ\u00e9e",
                "Semaine",
                "D\u00e9cade",
                "Quinzaine",
                "Mois"
              }));
              CAP02.setComponentPopupMenu(BTD);
              CAP02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAP02.setName("CAP02");
              panel6.add(CAP02);
              CAP02.setBounds(260, 35, 91, CAP02.getPreferredSize().height);

              //---- CAP03 ----
              CAP03.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Journ\u00e9e",
                "Semaine",
                "D\u00e9cade",
                "Quinzaine",
                "Mois"
              }));
              CAP03.setComponentPopupMenu(BTD);
              CAP03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAP03.setName("CAP03");
              panel6.add(CAP03);
              CAP03.setBounds(440, 35, 91, CAP03.getPreferredSize().height);

              //---- CAP04 ----
              CAP04.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Journ\u00e9e",
                "Semaine",
                "D\u00e9cade",
                "Quinzaine",
                "Mois"
              }));
              CAP04.setComponentPopupMenu(BTD);
              CAP04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAP04.setName("CAP04");
              panel6.add(CAP04);
              CAP04.setBounds(620, 35, 91, CAP04.getPreferredSize().height);

              //---- CAP05 ----
              CAP05.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Journ\u00e9e",
                "Semaine",
                "D\u00e9cade",
                "Quinzaine",
                "Mois"
              }));
              CAP05.setComponentPopupMenu(BTD);
              CAP05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAP05.setName("CAP05");
              panel6.add(CAP05);
              CAP05.setBounds(800, 35, 91, CAP05.getPreferredSize().height);

              //---- CAP06 ----
              CAP06.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Journ\u00e9e",
                "Semaine",
                "D\u00e9cade",
                "Quinzaine",
                "Mois"
              }));
              CAP06.setComponentPopupMenu(BTD);
              CAP06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAP06.setName("CAP06");
              panel6.add(CAP06);
              CAP06.setBounds(79, 65, 91, CAP06.getPreferredSize().height);

              //---- CAP07 ----
              CAP07.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Journ\u00e9e",
                "Semaine",
                "D\u00e9cade",
                "Quinzaine",
                "Mois"
              }));
              CAP07.setComponentPopupMenu(BTD);
              CAP07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAP07.setName("CAP07");
              panel6.add(CAP07);
              CAP07.setBounds(260, 65, 91, CAP07.getPreferredSize().height);

              //---- CAP08 ----
              CAP08.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Journ\u00e9e",
                "Semaine",
                "D\u00e9cade",
                "Quinzaine",
                "Mois"
              }));
              CAP08.setComponentPopupMenu(BTD);
              CAP08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAP08.setName("CAP08");
              panel6.add(CAP08);
              CAP08.setBounds(440, 65, 91, CAP08.getPreferredSize().height);

              //---- CAP09 ----
              CAP09.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Journ\u00e9e",
                "Semaine",
                "D\u00e9cade",
                "Quinzaine",
                "Mois"
              }));
              CAP09.setComponentPopupMenu(BTD);
              CAP09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAP09.setName("CAP09");
              panel6.add(CAP09);
              CAP09.setBounds(620, 65, 91, CAP09.getPreferredSize().height);

              //---- CAP10 ----
              CAP10.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Journ\u00e9e",
                "Semaine",
                "D\u00e9cade",
                "Quinzaine",
                "Mois"
              }));
              CAP10.setComponentPopupMenu(BTD);
              CAP10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CAP10.setName("CAP10");
              panel6.add(CAP10);
              CAP10.setBounds(800, 65, 91, CAP10.getPreferredSize().height);

              //---- CAJ01 ----
              CAJ01.setComponentPopupMenu(BTD);
              CAJ01.setName("CAJ01");
              panel6.add(CAJ01);
              CAJ01.setBounds(25, 34, 30, CAJ01.getPreferredSize().height);

              //---- CAJ02 ----
              CAJ02.setComponentPopupMenu(BTD);
              CAJ02.setName("CAJ02");
              panel6.add(CAJ02);
              CAJ02.setBounds(215, 34, 30, CAJ02.getPreferredSize().height);

              //---- CAJ03 ----
              CAJ03.setComponentPopupMenu(BTD);
              CAJ03.setName("CAJ03");
              panel6.add(CAJ03);
              CAJ03.setBounds(395, 34, 30, CAJ03.getPreferredSize().height);

              //---- CAJ04 ----
              CAJ04.setComponentPopupMenu(BTD);
              CAJ04.setName("CAJ04");
              panel6.add(CAJ04);
              CAJ04.setBounds(570, 34, 30, CAJ04.getPreferredSize().height);

              //---- CAJ05 ----
              CAJ05.setComponentPopupMenu(BTD);
              CAJ05.setName("CAJ05");
              panel6.add(CAJ05);
              CAJ05.setBounds(750, 34, 30, CAJ05.getPreferredSize().height);

              //---- CAJ06 ----
              CAJ06.setComponentPopupMenu(BTD);
              CAJ06.setName("CAJ06");
              panel6.add(CAJ06);
              CAJ06.setBounds(25, 64, 30, CAJ06.getPreferredSize().height);

              //---- CAJ07 ----
              CAJ07.setComponentPopupMenu(BTD);
              CAJ07.setName("CAJ07");
              panel6.add(CAJ07);
              CAJ07.setBounds(215, 64, 30, CAJ07.getPreferredSize().height);

              //---- CAJ08 ----
              CAJ08.setComponentPopupMenu(BTD);
              CAJ08.setName("CAJ08");
              panel6.add(CAJ08);
              CAJ08.setBounds(395, 64, 30, CAJ08.getPreferredSize().height);

              //---- CAJ09 ----
              CAJ09.setComponentPopupMenu(BTD);
              CAJ09.setName("CAJ09");
              panel6.add(CAJ09);
              CAJ09.setBounds(570, 64, 30, CAJ09.getPreferredSize().height);

              //---- CAJ10 ----
              CAJ10.setComponentPopupMenu(BTD);
              CAJ10.setName("CAJ10");
              panel6.add(CAJ10);
              CAJ10.setBounds(750, 64, 30, CAJ10.getPreferredSize().height);

              //---- OBJ_77 ----
              OBJ_77.setText("/");
              OBJ_77.setName("OBJ_77");
              panel6.add(OBJ_77);
              OBJ_77.setBounds(64, 38, 6, 20);

              //---- OBJ_80 ----
              OBJ_80.setText("/");
              OBJ_80.setName("OBJ_80");
              panel6.add(OBJ_80);
              OBJ_80.setBounds(250, 38, 6, 20);

              //---- OBJ_83 ----
              OBJ_83.setText("/");
              OBJ_83.setName("OBJ_83");
              panel6.add(OBJ_83);
              OBJ_83.setBounds(430, 38, 6, 20);

              //---- OBJ_86 ----
              OBJ_86.setText("/");
              OBJ_86.setName("OBJ_86");
              panel6.add(OBJ_86);
              OBJ_86.setBounds(607, 38, 6, 20);

              //---- OBJ_89 ----
              OBJ_89.setText("/");
              OBJ_89.setName("OBJ_89");
              panel6.add(OBJ_89);
              OBJ_89.setBounds(787, 38, 6, 20);

              //---- OBJ_93 ----
              OBJ_93.setText("/");
              OBJ_93.setName("OBJ_93");
              panel6.add(OBJ_93);
              OBJ_93.setBounds(64, 68, 6, 20);

              //---- OBJ_96 ----
              OBJ_96.setText("/");
              OBJ_96.setName("OBJ_96");
              panel6.add(OBJ_96);
              OBJ_96.setBounds(250, 68, 6, 20);

              //---- OBJ_99 ----
              OBJ_99.setText("/");
              OBJ_99.setName("OBJ_99");
              panel6.add(OBJ_99);
              OBJ_99.setBounds(430, 68, 6, 20);

              //---- OBJ_102 ----
              OBJ_102.setText("/");
              OBJ_102.setName("OBJ_102");
              panel6.add(OBJ_102);
              OBJ_102.setBounds(607, 68, 6, 20);

              //---- OBJ_105 ----
              OBJ_105.setText("/");
              OBJ_105.setName("OBJ_105");
              panel6.add(OBJ_105);
              OBJ_105.setBounds(787, 68, 6, 20);
            }
            xTitledPanel1ContentContainer.add(panel6);
            panel6.setBounds(0, 410, 965, 105);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 974, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 553, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 3, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_48;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private JPanel p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel3;
  private XRiTextField LCG01;
  private JLabel label1;
  private XRiTextField CAC01;
  private XRiTextField CAC02;
  private XRiTextField LCG02;
  private XRiTextField CAC03;
  private XRiTextField LCG03;
  private XRiTextField CAC04;
  private XRiTextField LCG04;
  private XRiTextField LCG05;
  private XRiTextField CAC05;
  private XRiTextField LCG06;
  private XRiTextField CAC06;
  private XRiTextField LCG07;
  private XRiTextField CAC07;
  private XRiTextField LCG08;
  private XRiTextField CAC08;
  private XRiTextField LCG09;
  private XRiTextField CAC09;
  private XRiTextField LCG10;
  private XRiTextField CAC10;
  private XRiTextField LCG11;
  private XRiTextField CAC11;
  private XRiTextField LCG12;
  private XRiTextField CAC12;
  private XRiTextField LCG13;
  private XRiTextField CAC13;
  private XRiTextField LCG14;
  private XRiTextField CAC14;
  private XRiTextField LCG15;
  private XRiTextField CAC15;
  private XRiTextField CAC17;
  private XRiTextField LCG17;
  private XRiTextField CAC18;
  private XRiTextField LCG18;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label17;
  private JLabel label18;
  private JLabel label19;
  private JLabel label20;
  private JLabel label21;
  private JLabel label22;
  private JPanel panel4;
  private JLabel OBJ_63;
  private XRiTextField CAETC1;
  private JLabel OBJ_65;
  private XRiTextField CAJOA;
  private JLabel OBJ_67;
  private XRiCheckBox CASEC;
  private JLabel OBJ_70;
  private XRiCheckBox CAATT;
  private JButton OBJ_72;
  private JButton OBJ_59;
  private JPanel panel6;
  private JLabel OBJ_97;
  private JLabel OBJ_110;
  private XRiComboBox CAP01;
  private XRiComboBox CAP02;
  private XRiComboBox CAP03;
  private XRiComboBox CAP04;
  private XRiComboBox CAP05;
  private XRiComboBox CAP06;
  private XRiComboBox CAP07;
  private XRiComboBox CAP08;
  private XRiComboBox CAP09;
  private XRiComboBox CAP10;
  private XRiTextField CAJ01;
  private XRiTextField CAJ02;
  private XRiTextField CAJ03;
  private XRiTextField CAJ04;
  private XRiTextField CAJ05;
  private XRiTextField CAJ06;
  private XRiTextField CAJ07;
  private XRiTextField CAJ08;
  private XRiTextField CAJ09;
  private XRiTextField CAJ10;
  private JLabel OBJ_77;
  private JLabel OBJ_80;
  private JLabel OBJ_83;
  private JLabel OBJ_86;
  private JLabel OBJ_89;
  private JLabel OBJ_93;
  private JLabel OBJ_96;
  private JLabel OBJ_99;
  private JLabel OBJ_102;
  private JLabel OBJ_105;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
