
package ri.serien.libecranrpg.vgam.VGAM50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM50FM_BS extends SNPanelEcranRPG implements ioFrame {
  
  private ODialog dialog_PANEL = null;
  private ODialog dialog_PANEL2 = null;
  
  public VGAM50FM_BS(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    UPNETX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UPNETX@")).trim());
    CAPRVX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAPRVX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    navig_valid.setVisible(!lexique.isTrue("53"));
    
    riZoneSortie1.setVisible(lexique.isTrue("N60"));
    if (lexique.HostFieldGetData("WFRFRR").trim().equals("")) {
      riZoneSortie1.setText(lexique.HostFieldGetData("MERFAB"));
    }
    else {
      riZoneSortie1.setText(lexique.HostFieldGetData("PAR"));
    }
    
    lib_CACPV.setText(gererCACPV(lexique.HostFieldGetData("CACPV")));
    lib_CACPV.setVisible(lexique.isTrue("N59"));
    
    lbCodeBarre.setVisible(CAGCD.isVisible());
    OBJ_63.setVisible(CADEL.isVisible());
    OBJ_66.setVisible(CADELS.isVisible());
    OBJ_125.setVisible(CAQMI.isVisible());
    label6.setVisible(CAIN7.isVisible());
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Conditions normales d'achat d'article consigné : "
        + lexique.HostFieldGetData("A1ART").trim() + " - " + lexique.HostFieldGetData("A1LIB")));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
    bouton_art.setIcon(lexique.chargerImage("images/fin_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  protected String gererCACPV(String valeur) {
    if (valeur != null) {
      valeur = valeur.trim();
      if (valeur.equals("")) {
        return "";
      }
      else if (valeur.equals("V")) {
        return "Prix catalogue x Coeff.";
      }
      else if (valeur.equals("C")) {
        return "Prix catalogue seul";
      }
      else if (valeur.equals("W")) {
        return "Prix catalogue x coeff. d'approche x coeff.";
      }
      else if (valeur.equals("N")) {
        return "Prix net d'achat x Coeff.";
      }
      else if (valeur.equals("M")) {
        return "Prix net x Coeff. d'approche moyen x coeff.";
      }
      else if (valeur.equals("A")) {
        return "Prix net x Coeff. d'approche saisi x coeff.";
      }
      else if (valeur.equals("1")) {
        return "1er prix quantitatif";
      }
      else if (valeur.equals("2")) {
        return "2eme prix quantitatif";
      }
      else if (valeur.equals("3")) {
        return "3eme prix quantitatif";
      }
      else if (valeur.equals("4")) {
        return "4eme prix quantitatif";
      }
      else if (valeur.equals("P")) {
        return "PUMP dès la réception d'achat x coeff.";
      }
      else if (valeur.equals("Q")) {
        return "PUMP à la demande (GAM 561) x coeff.";
      }
      else if (valeur.equals("R")) {
        return "Prix de revient x coeff.";
      }
      else if (valeur.equals("D")) {
        return "Prix de revient réel du dernier achat x coeff.";
      }
      else if (valeur.equals("S")) {
        return "Variation du prix de revient standard sur prix de vente";
      }
      else {
        return "";
      }
    }
    else {
      return null;
    }
  }
  
  private void riSousMenu_bt6ActionPerformed() {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt7ActionPerformed() {
    lexique.HostCursorPut(12, 50);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt8ActionPerformed() {
    lexique.HostCursorPut("CAFRS");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt9ActionPerformed() {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt10ActionPerformed() {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt11ActionPerformed() {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt12ActionPerformed() {
    lexique.HostCursorPut(15, 20);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt14ActionPerformed() {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt18ActionPerformed() {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt19ActionPerformed() {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void CAUNAFocusLost(FocusEvent e) {
    // TODO add your code here
  }
  
  private void CAUNA_FocusLost(FocusEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void bouton_artActionPerformed() {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void riBoutonDetail2ActionPerformed(ActionEvent e) {
    if (dialog_PANEL == null) {
      dialog_PANEL = new ODialog((Window) getTopLevelAncestor(), new OPTION_PANEL(this));
    }
    dialog_PANEL.affichePopupPerso();
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    if (dialog_PANEL2 == null) {
      dialog_PANEL2 = new ODialog((Window) getTopLevelAncestor(), new OPTION_PANEL2(this));
    }
    dialog_PANEL2.affichePopupPerso();
  }
  
  private void bouton_erreursActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1");
  }
  
  private void CAKPRFocusLost(FocusEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    navig_art = new RiMenu();
    bouton_art = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    A1LIB = new XRiTextField();
    CADAPX = new XRiCalendrier();
    OBJ_29 = new JLabel();
    OBJ_26 = new JLabel();
    lbCodeBarre = new JLabel();
    CAGCD = new XRiTextField();
    CACOL = new XRiTextField();
    CAFRS = new XRiTextField();
    label3 = new JLabel();
    ULBFRS = new XRiTextField();
    OBJ_47 = new JLabel();
    CAREF = new XRiTextField();
    riZoneSortie1 = new JLabel();
    CARFC = new XRiTextField();
    CAPRAX = new XRiTextField();
    OBJ_121 = new JLabel();
    OBJ_131 = new JLabel();
    UPNETX = new RiZoneSortie();
    CAPR2X = new XRiTextField();
    OBJ_133 = new JLabel();
    CAPRVX = new RiZoneSortie();
    OBJ_132 = new JLabel();
    OBJ_161 = new JLabel();
    WPRSA = new XRiTextField();
    OBJ_125 = new JLabel();
    CAQMI = new XRiTextField();
    OBJ_63 = new JLabel();
    CADEL = new XRiTextField();
    OBJ_66 = new JLabel();
    CADELS = new XRiTextField();
    label6 = new JLabel();
    CAIN7 = new XRiComboBox();
    panel16 = new JPanel();
    OBJ_143 = new JLabel();
    WAFK1P = new XRiTextField();
    CAFV1 = new XRiTextField();
    OBJ_147 = new JLabel();
    OBJ_68 = new JLabel();
    WAFK1 = new XRiTextField();
    panel17 = new JPanel();
    OBJ_111 = new JLabel();
    OBJ_126 = new JLabel();
    CAREX1 = new XRiTextField();
    CAKPR = new XRiTextField();
    OBJ_124 = new JLabel();
    CAFK3 = new XRiTextField();
    panel18 = new JPanel();
    OBJ_146 = new JLabel();
    CAFK2 = new XRiTextField();
    panel19 = new JPanel();
    OBJ_141 = new JLabel();
    CAKPV = new XRiTextField();
    riBoutonDetail2 = new SNBoutonDetail();
    OBJ_139 = new JLabel();
    lib_CACPV = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1120, 585));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            bouton_erreurs.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_erreursActionPerformed(e);
              }
            });
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Historique CNA");
            bouton_retour.setToolTipText("Historique conditions");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
          
          // ======== navig_art ========
          {
            navig_art.setName("navig_art");
            
            // ---- bouton_art ----
            bouton_art.setText("Fiche article");
            bouton_art.setToolTipText("Fiche article");
            bouton_art.setName("bouton_art");
            bouton_art.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_artActionPerformed();
              }
            });
            navig_art.add(bouton_art);
          }
          menus_bas.add(navig_art);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt1 ----
              riMenu_bt1.setText("@V01F@");
              riMenu_bt1.setPreferredSize(new Dimension(167, 50));
              riMenu_bt1.setMinimumSize(new Dimension(167, 50));
              riMenu_bt1.setMaximumSize(new Dimension(170, 50));
              riMenu_bt1.setFont(riMenu_bt1.getFont().deriveFont(riMenu_bt1.getFont().getSize() + 2f));
              riMenu_bt1.setName("riMenu_bt1");
              riMenu_V01F.add(riMenu_bt1);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Historique d'achats");
              riSousMenu_bt6.setToolTipText("Historique d'achats");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed();
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Code bonification");
              riSousMenu_bt7.setToolTipText("Code bonification");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed();
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Acc\u00e9s fournisseur");
              riSousMenu_bt8.setToolTipText("Acc\u00e9s fournisseur");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed();
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Tarifs en colonne");
              riSousMenu_bt10.setToolTipText("Tarifs en colonne");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed();
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Prix n\u00e9goci\u00e9");
              riSousMenu_bt11.setToolTipText("Prix n\u00e9goci\u00e9");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed();
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");
              
              // ---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Histo. coeff. approche");
              riSousMenu_bt12.setToolTipText("Historique du coefficient d'approche");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed();
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);
            
            // ======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");
              
              // ---- riMenu_bt4 ----
              riMenu_bt4.setText("Fonctions");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);
            
            // ======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");
              
              // ---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Date d'applic. pr\u00e9c\u00e9dente");
              riSousMenu_bt18.setToolTipText("Date d'application pr\u00e9c\u00e9dente");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed();
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);
            
            // ======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");
              
              // ---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Date d'applic. suivante");
              riSousMenu_bt19.setToolTipText("Date d'application suivante");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed();
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        
        // ======== panel2 ========
        {
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);
          
          // ---- A1LIB ----
          A1LIB.setName("A1LIB");
          panel2.add(A1LIB);
          A1LIB.setBounds(120, 5, 310, A1LIB.getPreferredSize().height);
          
          // ---- CADAPX ----
          CADAPX.setName("CADAPX");
          panel2.add(CADAPX);
          CADAPX.setBounds(820, 5, 105, CADAPX.getPreferredSize().height);
          
          // ---- OBJ_29 ----
          OBJ_29.setText("Date d'application");
          OBJ_29.setName("OBJ_29");
          panel2.add(OBJ_29);
          OBJ_29.setBounds(705, 10, 110, 20);
          
          // ---- OBJ_26 ----
          OBJ_26.setText("Libell\u00e9");
          OBJ_26.setName("OBJ_26");
          panel2.add(OBJ_26);
          OBJ_26.setBounds(15, 15, 60, 16);
          
          // ---- lbCodeBarre ----
          lbCodeBarre.setText("Code barre");
          lbCodeBarre.setHorizontalAlignment(SwingConstants.RIGHT);
          lbCodeBarre.setName("lbCodeBarre");
          panel2.add(lbCodeBarre);
          lbCodeBarre.setBounds(435, 10, 100, 20);
          
          // ---- CAGCD ----
          CAGCD.setToolTipText("Num\u00e9ro de code barres");
          CAGCD.setComponentPopupMenu(null);
          CAGCD.setName("CAGCD");
          panel2.add(CAGCD);
          CAGCD.setBounds(545, 5, 120, CAGCD.getPreferredSize().height);
          
          // ---- CACOL ----
          CACOL.setToolTipText("Collectif fournisseur");
          CACOL.setComponentPopupMenu(BTD);
          CACOL.setName("CACOL");
          panel2.add(CACOL);
          CACOL.setBounds(120, 45, 20, CACOL.getPreferredSize().height);
          
          // ---- CAFRS ----
          CAFRS.setToolTipText("Num\u00e9ro fournisseur");
          CAFRS.setComponentPopupMenu(BTD);
          CAFRS.setName("CAFRS");
          panel2.add(CAFRS);
          CAFRS.setBounds(140, 45, 60, CAFRS.getPreferredSize().height);
          
          // ---- label3 ----
          label3.setText("Fournisseur");
          label3.setName("label3");
          panel2.add(label3);
          label3.setBounds(15, 49, 80, 20);
          
          // ---- ULBFRS ----
          ULBFRS.setToolTipText("Nom du fournisseur");
          ULBFRS.setName("ULBFRS");
          panel2.add(ULBFRS);
          ULBFRS.setBounds(205, 45, 330, ULBFRS.getPreferredSize().height);
          
          // ---- OBJ_47 ----
          OBJ_47.setText("R\u00e9f\u00e9rence fournisseur");
          OBJ_47.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_47.setName("OBJ_47");
          panel2.add(OBJ_47);
          OBJ_47.setBounds(545, 49, 150, 20);
          
          // ---- CAREF ----
          CAREF.setComponentPopupMenu(null);
          CAREF.setName("CAREF");
          panel2.add(CAREF);
          CAREF.setBounds(705, 45, 210, CAREF.getPreferredSize().height);
          
          // ---- riZoneSortie1 ----
          riZoneSortie1.setText("lib de CARFC");
          riZoneSortie1.setName("riZoneSortie1");
          panel2.add(riZoneSortie1);
          riZoneSortie1.setBounds(15, 79, 190, 20);
          
          // ---- CARFC ----
          CARFC.setComponentPopupMenu(null);
          CARFC.setName("CARFC");
          panel2.add(CARFC);
          CARFC.setBounds(205, 75, 210, CARFC.getPreferredSize().height);
          
          // ---- CAPRAX ----
          CAPRAX.setComponentPopupMenu(null);
          CAPRAX.setHorizontalAlignment(SwingConstants.RIGHT);
          CAPRAX.setFont(CAPRAX.getFont().deriveFont(CAPRAX.getFont().getStyle() | Font.BOLD));
          CAPRAX.setName("CAPRAX");
          panel2.add(CAPRAX);
          CAPRAX.setBounds(205, 150, 106, CAPRAX.getPreferredSize().height);
          
          // ---- OBJ_121 ----
          OBJ_121.setText("Prix catalogue");
          OBJ_121.setFont(OBJ_121.getFont().deriveFont(OBJ_121.getFont().getStyle() | Font.BOLD, OBJ_121.getFont().getSize() + 1f));
          OBJ_121.setName("OBJ_121");
          panel2.add(OBJ_121);
          OBJ_121.setBounds(15, 150, 190, 28);
          
          // ---- OBJ_131 ----
          OBJ_131.setText("Prix de consignation");
          OBJ_131.setFont(OBJ_131.getFont().deriveFont(OBJ_131.getFont().getStyle() | Font.BOLD, OBJ_131.getFont().getSize() + 1f));
          OBJ_131.setName("OBJ_131");
          panel2.add(OBJ_131);
          OBJ_131.setBounds(15, 207, 190, 24);
          
          // ---- UPNETX ----
          UPNETX.setHorizontalAlignment(SwingConstants.RIGHT);
          UPNETX.setFont(UPNETX.getFont().deriveFont(UPNETX.getFont().getStyle() | Font.BOLD));
          UPNETX.setComponentPopupMenu(null);
          UPNETX.setText("@UPNETX@");
          UPNETX.setName("UPNETX");
          panel2.add(UPNETX);
          UPNETX.setBounds(205, 207, 105, UPNETX.getPreferredSize().height);
          
          // ---- CAPR2X ----
          CAPR2X.setHorizontalAlignment(SwingConstants.RIGHT);
          CAPR2X.setFont(CAPR2X.getFont().deriveFont(CAPR2X.getFont().getStyle() | Font.BOLD));
          CAPR2X.setComponentPopupMenu(null);
          CAPR2X.setName("CAPR2X");
          panel2.add(CAPR2X);
          CAPR2X.setBounds(555, 207, 105, CAPR2X.getPreferredSize().height);
          
          // ---- OBJ_133 ----
          OBJ_133.setText("Prix de d\u00e9consignation");
          OBJ_133.setFont(OBJ_133.getFont().deriveFont(OBJ_133.getFont().getStyle() | Font.BOLD, OBJ_133.getFont().getSize() + 1f));
          OBJ_133.setName("OBJ_133");
          panel2.add(OBJ_133);
          OBJ_133.setBounds(390, 207, 155, 24);
          
          // ---- CAPRVX ----
          CAPRVX.setHorizontalAlignment(SwingConstants.RIGHT);
          CAPRVX.setFont(CAPRVX.getFont().deriveFont(CAPRVX.getFont().getStyle() | Font.BOLD));
          CAPRVX.setComponentPopupMenu(null);
          CAPRVX.setText("@CAPRVX@");
          CAPRVX.setName("CAPRVX");
          panel2.add(CAPRVX);
          CAPRVX.setBounds(205, 339, 105, CAPRVX.getPreferredSize().height);
          
          // ---- OBJ_132 ----
          OBJ_132.setText("Prix de revient fournisseur HT");
          OBJ_132.setFont(OBJ_132.getFont().deriveFont(OBJ_132.getFont().getStyle() | Font.BOLD, OBJ_132.getFont().getSize() + 1f));
          OBJ_132.setName("OBJ_132");
          panel2.add(OBJ_132);
          OBJ_132.setBounds(15, 339, 190, 24);
          
          // ---- OBJ_161 ----
          OBJ_161.setText("Prix de revient standard HT");
          OBJ_161.setFont(OBJ_161.getFont().deriveFont(OBJ_161.getFont().getStyle() | Font.BOLD, OBJ_161.getFont().getSize() + 1f));
          OBJ_161.setName("OBJ_161");
          panel2.add(OBJ_161);
          OBJ_161.setBounds(15, 430, 190, 28);
          
          // ---- WPRSA ----
          WPRSA.setComponentPopupMenu(null);
          WPRSA.setHorizontalAlignment(SwingConstants.RIGHT);
          WPRSA.setFont(WPRSA.getFont().deriveFont(WPRSA.getFont().getStyle() | Font.BOLD));
          WPRSA.setName("WPRSA");
          panel2.add(WPRSA);
          WPRSA.setBounds(205, 430, 108, WPRSA.getPreferredSize().height);
          
          // ---- OBJ_125 ----
          OBJ_125.setText("Quantit\u00e9 minimale");
          OBJ_125.setName("OBJ_125");
          panel2.add(OBJ_125);
          OBJ_125.setBounds(25, 520, 155, 20);
          
          // ---- CAQMI ----
          CAQMI.setComponentPopupMenu(null);
          CAQMI.setHorizontalAlignment(SwingConstants.RIGHT);
          CAQMI.setName("CAQMI");
          panel2.add(CAQMI);
          CAQMI.setBounds(205, 515, 92, CAQMI.getPreferredSize().height);
          
          // ---- OBJ_63 ----
          OBJ_63.setText("Premier d\u00e9lai");
          OBJ_63.setName("OBJ_63");
          panel2.add(OBJ_63);
          OBJ_63.setBounds(350, 515, 84, 20);
          
          // ---- CADEL ----
          CADEL.setComponentPopupMenu(null);
          CADEL.setName("CADEL");
          panel2.add(CADEL);
          CADEL.setBounds(440, 510, 26, CADEL.getPreferredSize().height);
          
          // ---- OBJ_66 ----
          OBJ_66.setText("Deuxi\u00e8me d\u00e9lai");
          OBJ_66.setName("OBJ_66");
          panel2.add(OBJ_66);
          OBJ_66.setBounds(485, 515, 97, 20);
          
          // ---- CADELS ----
          CADELS.setComponentPopupMenu(null);
          CADELS.setName("CADELS");
          panel2.add(CADELS);
          CADELS.setBounds(595, 510, 26, CADELS.getPreferredSize().height);
          
          // ---- label6 ----
          label6.setText("d\u00e9lais en");
          label6.setName("label6");
          panel2.add(label6);
          label6.setBounds(655, 515, 65, 24);
          
          // ---- CAIN7 ----
          CAIN7.setModel(new DefaultComboBoxModel(new String[] { "semaines", "jours" }));
          CAIN7.setComponentPopupMenu(null);
          CAIN7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CAIN7.setName("CAIN7");
          panel2.add(CAIN7);
          CAIN7.setBounds(725, 515, 102, CAIN7.getPreferredSize().height);
          
          // ======== panel16 ========
          {
            panel16.setOpaque(false);
            panel16.setBorder(new TitledBorder("Port"));
            panel16.setName("panel16");
            panel16.setLayout(null);
            
            // ---- OBJ_143 ----
            OBJ_143.setText("Frais de port pour");
            OBJ_143.setName("OBJ_143");
            panel16.add(OBJ_143);
            OBJ_143.setBounds(15, 29, 120, 20);
            
            // ---- WAFK1P ----
            WAFK1P.setComponentPopupMenu(null);
            WAFK1P.setName("WAFK1P");
            panel16.add(WAFK1P);
            WAFK1P.setBounds(135, 25, 60, WAFK1P.getPreferredSize().height);
            
            // ---- CAFV1 ----
            CAFV1.setComponentPopupMenu(null);
            CAFV1.setName("CAFV1");
            panel16.add(CAFV1);
            CAFV1.setBounds(215, 25, 92, CAFV1.getPreferredSize().height);
            
            // ---- OBJ_147 ----
            OBJ_147.setText("OU");
            OBJ_147.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_147.setFont(OBJ_147.getFont().deriveFont(OBJ_147.getFont().getStyle() | Font.BOLD));
            OBJ_147.setName("OBJ_147");
            panel16.add(OBJ_147);
            OBJ_147.setBounds(335, 29, 25, 20);
            
            // ---- OBJ_68 ----
            OBJ_68.setText(" coefficient");
            OBJ_68.setName("OBJ_68");
            panel16.add(OBJ_68);
            OBJ_68.setBounds(400, 29, 75, 20);
            
            // ---- WAFK1 ----
            WAFK1.setComponentPopupMenu(null);
            WAFK1.setName("WAFK1");
            panel16.add(WAFK1);
            WAFK1.setBounds(475, 25, 60, WAFK1.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel16.getComponentCount(); i++) {
                Rectangle bounds = panel16.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel16.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel16.setMinimumSize(preferredSize);
              panel16.setPreferredSize(preferredSize);
            }
          }
          panel2.add(panel16);
          panel16.setBounds(340, 240, 585, 68);
          
          // ======== panel17 ========
          {
            panel17.setOpaque(false);
            panel17.setBorder(new TitledBorder("N\u00e9gociation"));
            panel17.setName("panel17");
            panel17.setLayout(null);
            
            // ---- OBJ_111 ----
            OBJ_111.setText("Remise");
            OBJ_111.setName("OBJ_111");
            panel17.add(OBJ_111);
            OBJ_111.setBounds(15, 29, 100, 20);
            
            // ---- OBJ_126 ----
            OBJ_126.setText("Coefficient");
            OBJ_126.setName("OBJ_126");
            panel17.add(OBJ_126);
            OBJ_126.setBounds(185, 29, 69, 20);
            
            // ---- CAREX1 ----
            CAREX1.setComponentPopupMenu(null);
            CAREX1.setHorizontalAlignment(SwingConstants.RIGHT);
            CAREX1.setName("CAREX1");
            panel17.add(CAREX1);
            CAREX1.setBounds(115, 25, 50, CAREX1.getPreferredSize().height);
            
            // ---- CAKPR ----
            CAKPR.setComponentPopupMenu(null);
            CAKPR.setName("CAKPR");
            CAKPR.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                CAKPRFocusLost(e);
              }
            });
            panel17.add(CAKPR);
            CAKPR.setBounds(260, 25, 60, CAKPR.getPreferredSize().height);
            
            // ---- OBJ_124 ----
            OBJ_124.setText("Taxe ou majoration (%)");
            OBJ_124.setName("OBJ_124");
            panel17.add(OBJ_124);
            OBJ_124.setBounds(345, 29, 130, 20);
            
            // ---- CAFK3 ----
            CAFK3.setComponentPopupMenu(null);
            CAFK3.setHorizontalAlignment(SwingConstants.RIGHT);
            CAFK3.setName("CAFK3");
            panel17.add(CAFK3);
            CAFK3.setBounds(475, 25, 80, CAFK3.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel17.getComponentCount(); i++) {
                Rectangle bounds = panel17.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel17.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel17.setMinimumSize(preferredSize);
              panel17.setPreferredSize(preferredSize);
            }
          }
          panel2.add(panel17);
          panel17.setBounds(340, 130, 585, 68);
          
          // ======== panel18 ========
          {
            panel18.setOpaque(false);
            panel18.setBorder(new TitledBorder("Frais suppl\u00e9mentaires"));
            panel18.setName("panel18");
            panel18.setLayout(null);
            
            // ---- OBJ_146 ----
            OBJ_146.setText("Coefficient de majoration");
            OBJ_146.setName("OBJ_146");
            panel18.add(OBJ_146);
            OBJ_146.setBounds(15, 29, 205, 20);
            
            // ---- CAFK2 ----
            CAFK2.setComponentPopupMenu(null);
            CAFK2.setName("CAFK2");
            panel18.add(CAFK2);
            CAFK2.setBounds(220, 25, 60, CAFK2.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel18.getComponentCount(); i++) {
                Rectangle bounds = panel18.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel18.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel18.setMinimumSize(preferredSize);
              panel18.setPreferredSize(preferredSize);
            }
          }
          panel2.add(panel18);
          panel18.setBounds(340, 317, 585, 68);
          
          // ======== panel19 ========
          {
            panel19.setOpaque(false);
            panel19.setBorder(new TitledBorder("Mise \u00e0 jour du prix de vente"));
            panel19.setName("panel19");
            panel19.setLayout(null);
            
            // ---- OBJ_141 ----
            OBJ_141.setText("Coefficient calcul prix de vente");
            OBJ_141.setName("OBJ_141");
            panel19.add(OBJ_141);
            OBJ_141.setBounds(15, 29, 180, 20);
            
            // ---- CAKPV ----
            CAKPV.setComponentPopupMenu(null);
            CAKPV.setName("CAKPV");
            panel19.add(CAKPV);
            CAKPV.setBounds(220, 25, 60, CAKPV.getPreferredSize().height);
            
            // ---- riBoutonDetail2 ----
            riBoutonDetail2.setName("riBoutonDetail2");
            riBoutonDetail2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetail2ActionPerformed(e);
              }
            });
            panel19.add(riBoutonDetail2);
            riBoutonDetail2.setBounds(285, 28, 25, 22);
            
            // ---- OBJ_139 ----
            OBJ_139.setText("A partir de");
            OBJ_139.setName("OBJ_139");
            panel19.add(OBJ_139);
            OBJ_139.setBounds(15, 62, 70, 20);
            
            // ---- lib_CACPV ----
            lib_CACPV.setComponentPopupMenu(null);
            lib_CACPV.setFont(lib_CACPV.getFont().deriveFont(lib_CACPV.getFont().getSize() - 1f));
            lib_CACPV.setName("lib_CACPV");
            panel19.add(lib_CACPV);
            lib_CACPV.setBounds(220, 60, 280, lib_CACPV.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel19.getComponentCount(); i++) {
                Rectangle bounds = panel19.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel19.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel19.setMinimumSize(preferredSize);
              panel19.setPreferredSize(preferredSize);
            }
          }
          panel2.add(panel19);
          panel19.setBounds(340, 394, 585, 101);
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addContainerGap().addComponent(panel2, GroupLayout.DEFAULT_SIZE, 928, Short.MAX_VALUE).addGap(10, 10, 10)));
        p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap().addComponent(panel2, GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE).addContainerGap()));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private RiMenu navig_art;
  private RiMenu_bt bouton_art;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField A1LIB;
  private XRiCalendrier CADAPX;
  private JLabel OBJ_29;
  private JLabel OBJ_26;
  private JLabel lbCodeBarre;
  private XRiTextField CAGCD;
  private XRiTextField CACOL;
  private XRiTextField CAFRS;
  private JLabel label3;
  private XRiTextField ULBFRS;
  private JLabel OBJ_47;
  private XRiTextField CAREF;
  private JLabel riZoneSortie1;
  private XRiTextField CARFC;
  private XRiTextField CAPRAX;
  private JLabel OBJ_121;
  private JLabel OBJ_131;
  private RiZoneSortie UPNETX;
  private XRiTextField CAPR2X;
  private JLabel OBJ_133;
  private RiZoneSortie CAPRVX;
  private JLabel OBJ_132;
  private JLabel OBJ_161;
  private XRiTextField WPRSA;
  private JLabel OBJ_125;
  private XRiTextField CAQMI;
  private JLabel OBJ_63;
  private XRiTextField CADEL;
  private JLabel OBJ_66;
  private XRiTextField CADELS;
  private JLabel label6;
  private XRiComboBox CAIN7;
  private JPanel panel16;
  private JLabel OBJ_143;
  private XRiTextField WAFK1P;
  private XRiTextField CAFV1;
  private JLabel OBJ_147;
  private JLabel OBJ_68;
  private XRiTextField WAFK1;
  private JPanel panel17;
  private JLabel OBJ_111;
  private JLabel OBJ_126;
  private XRiTextField CAREX1;
  private XRiTextField CAKPR;
  private JLabel OBJ_124;
  private XRiTextField CAFK3;
  private JPanel panel18;
  private JLabel OBJ_146;
  private XRiTextField CAFK2;
  private JPanel panel19;
  private JLabel OBJ_141;
  private XRiTextField CAKPV;
  private SNBoutonDetail riBoutonDetail2;
  private JLabel OBJ_139;
  private RiZoneSortie lib_CACPV;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
