/*
 * Created by JFormDesigner on Wed Dec 04 17:22:38 CET 2019
 */

package ri.serien.libecranrpg.vgam.VGAM20FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.achat.documentachat.snacheteur.SNAcheteur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GAM284] Gestion des achats -> Documents d'achats -> Editions factures fournisseurs -> Analyse des frais
 * Indicateur : 00000000
 * Titre :Analyse des imputations de frais
 * 
 */
public class VGAM20FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WTB01_Top = { "WTB01", "WTB02", "WTB03", "WTB04", "WTB05", "WTB06", "WTB07", "WTB08", "WTB09", "WTB10", "WTB11",
      "WTB12", "WTB13", "WTB14", "WTB15", "WTB16", "WTB17", "WTB18", "WTB19", "WTB20", };
  private String[] _WTB01_Title = { "Article               Quantité    Montant HT Répartis   Cal.+Int.   Mtt  net   Coef PRV " };
  private String[][] _WTB01_Data =
      { { "LB01" }, { "LB02" }, { "LB03" }, { "LB04" }, { "LB05" }, { "LB06" }, { "LB07" }, { "LB08" }, { "LB09" }, { "LB10" },
          { "LB11" }, { "LB12" }, { "LB13" }, { "LB14" }, { "LB15" }, { "LB16" }, { "LB17" }, { "LB18" }, { "LB19" }, { "LB20" }, };
  private static String BOUTON_AFFICHE_DETAIL = "Afficher détail";
  
  private int[] _WTB01_Width = { 160, 80, 81, 81, 83, };
  
  public VGAM20FM_B1(ArrayList param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    WTB01.setAspectTable(_WTB01_Top, _WTB01_Title, _WTB01_Data, _WTB01_Width, false, null, null, null, null);
    
    // Ajout
    initDiverses();
    
    xriBarreBouton.ajouterBouton(BOUTON_AFFICHE_DETAIL, 'd', false);
    xriBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    xriBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    xriBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
        xriBarreBouton.isTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Activation du bouton détail si une ligne est sélectionnée
    if (WTB01.getSelectedRowCount() != 0) {
      xriBarreBouton.activerBouton(BOUTON_AFFICHE_DETAIL, true);
    }
    else {
      xriBarreBouton.activerBouton(BOUTON_AFFICHE_DETAIL, false);
    }
    
    // Charge de l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charge le fournisseur
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(false);
    snFournisseur.setSelectionParChampRPG(lexique, "EAFRSG");
    
    // Charge le client
    snAcheteur.setSession(getSession());
    snAcheteur.setIdEtablissement(snEtablissement.getIdSelection());
    snAcheteur.charger(false);
    snAcheteur.setSelectionParChampRPG(lexique, "EAACH");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snFournisseur.renseignerChampRPG(lexique, "EAFRSG");
    snAcheteur.renseignerChampRPG(lexique, "EAACH");
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_AFFICHE_DETAIL)) {
        WTB01.setValeurTop("1");
        lexique.HostScreenSendKey(this, "Enter");
      }
      if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTB01MouseClicked(MouseEvent e) {
    if (WTB01.getSelectedRowCount() != 0) {
      xriBarreBouton.activerBouton(BOUTON_AFFICHE_DETAIL, true);
    }
    else {
      xriBarreBouton.activerBouton(BOUTON_AFFICHE_DETAIL, false);
    }
    if (WTB01.doubleClicSelection(e)) {
      WTB01.setValeurTop("1");
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMiseEnForme = new SNPanel();
    pnlObjetAnalyse = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbDossier = new SNLabelChamp();
    WDOS = new XRiTextField();
    lbContainer = new SNLabelChamp();
    WCNT = new XRiTextField();
    PnlDetail = new SNPanelTitre();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    lbFacture = new SNLabelChamp();
    EANUM = new XRiTextField();
    lbDateFacturation = new SNLabelChamp();
    EAFACX = new XRiCalendrier();
    lbAcheteur = new SNLabelChamp();
    snAcheteur = new SNAcheteur();
    lbAnalyseImputation = new SNLabelTitre();
    pnlAnalyseImputation = new SNPanel();
    scpScrollTableau = new JScrollPane();
    WTB01 = new XRiTable();
    xriBarreBouton = new XRiBarreBouton();

    //======== this ========
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("Analyse des imputations de frais");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

      //======== pnlMiseEnForme ========
      {
        pnlMiseEnForme.setName("pnlMiseEnForme");
        pnlMiseEnForme.setLayout(new GridLayout(1, 2));

        //======== pnlObjetAnalyse ========
        {
          pnlObjetAnalyse.setTitre("Objet de l'analyse");
          pnlObjetAnalyse.setName("pnlObjetAnalyse");
          pnlObjetAnalyse.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlObjetAnalyse.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlObjetAnalyse.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlObjetAnalyse.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlObjetAnalyse.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlObjetAnalyse.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setEnabled(false);
          snEtablissement.setName("snEtablissement");
          pnlObjetAnalyse.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbDossier ----
          lbDossier.setText("Dossier");
          lbDossier.setName("lbDossier");
          pnlObjetAnalyse.add(lbDossier, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- WDOS ----
          WDOS.setPreferredSize(new Dimension(92, 30));
          WDOS.setMinimumSize(new Dimension(92, 30));
          WDOS.setMaximumSize(new Dimension(92, 30));
          WDOS.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDOS.setName("WDOS");
          pnlObjetAnalyse.add(WDOS, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbContainer ----
          lbContainer.setText("Container");
          lbContainer.setName("lbContainer");
          pnlObjetAnalyse.add(lbContainer, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WCNT ----
          WCNT.setMaximumSize(new Dimension(164, 30));
          WCNT.setMinimumSize(new Dimension(164, 30));
          WCNT.setPreferredSize(new Dimension(164, 30));
          WCNT.setFont(new Font("sansserif", Font.PLAIN, 14));
          WCNT.setName("WCNT");
          pnlObjetAnalyse.add(WCNT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlMiseEnForme.add(pnlObjetAnalyse);

        //======== PnlDetail ========
        {
          PnlDetail.setTitre("D\u00e9tail de la facture");
          PnlDetail.setName("PnlDetail");
          PnlDetail.setLayout(new GridBagLayout());
          ((GridBagLayout)PnlDetail.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)PnlDetail.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)PnlDetail.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)PnlDetail.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbFournisseur ----
          lbFournisseur.setText("Fournisseur");
          lbFournisseur.setName("lbFournisseur");
          PnlDetail.add(lbFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snFournisseur ----
          snFournisseur.setFont(new Font("sansserif", Font.PLAIN, 14));
          snFournisseur.setEnabled(false);
          snFournisseur.setName("snFournisseur");
          PnlDetail.add(snFournisseur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbFacture ----
          lbFacture.setText("Facture");
          lbFacture.setName("lbFacture");
          PnlDetail.add(lbFacture, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- EANUM ----
          EANUM.setPreferredSize(new Dimension(80, 30));
          EANUM.setMinimumSize(new Dimension(80, 30));
          EANUM.setMaximumSize(new Dimension(80, 30));
          EANUM.setFont(new Font("sansserif", Font.PLAIN, 14));
          EANUM.setName("EANUM");
          PnlDetail.add(EANUM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbDateFacturation ----
          lbDateFacturation.setText("Date de facturation");
          lbDateFacturation.setName("lbDateFacturation");
          PnlDetail.add(lbDateFacturation, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- EAFACX ----
          EAFACX.setPreferredSize(new Dimension(110, 30));
          EAFACX.setMinimumSize(new Dimension(110, 30));
          EAFACX.setMaximumSize(new Dimension(110, 30));
          EAFACX.setFont(new Font("sansserif", Font.PLAIN, 14));
          EAFACX.setName("EAFACX");
          PnlDetail.add(EAFACX, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbAcheteur ----
          lbAcheteur.setText("Acheteur");
          lbAcheteur.setName("lbAcheteur");
          PnlDetail.add(lbAcheteur, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snAcheteur ----
          snAcheteur.setEnabled(false);
          snAcheteur.setFont(new Font("sansserif", Font.PLAIN, 14));
          snAcheteur.setName("snAcheteur");
          PnlDetail.add(snAcheteur, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlMiseEnForme.add(PnlDetail);
      }
      pnlContenu.add(pnlMiseEnForme, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));

      //---- lbAnalyseImputation ----
      lbAnalyseImputation.setText("Analyse des imputations des frais");
      lbAnalyseImputation.setName("lbAnalyseImputation");
      pnlContenu.add(lbAnalyseImputation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));

      //======== pnlAnalyseImputation ========
      {
        pnlAnalyseImputation.setName("pnlAnalyseImputation");
        pnlAnalyseImputation.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlAnalyseImputation.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlAnalyseImputation.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlAnalyseImputation.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlAnalyseImputation.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== scpScrollTableau ========
        {
          scpScrollTableau.setPreferredSize(new Dimension(456, 350));
          scpScrollTableau.setMinimumSize(new Dimension(25, 350));
          scpScrollTableau.setName("scpScrollTableau");

          //---- WTB01 ----
          WTB01.setPreferredSize(new Dimension(75, 320));
          WTB01.setMinimumSize(new Dimension(15, 320));
          WTB01.setModel(new DefaultTableModel(
            new Object[][] {
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
              {null},
            },
            new String[] {
              null
            }
          ));
          WTB01.setName("WTB01");
          WTB01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTB01MouseClicked(e);
            }
          });
          scpScrollTableau.setViewportView(WTB01);
        }
        pnlAnalyseImputation.add(scpScrollTableau, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlAnalyseImputation, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- xriBarreBouton ----
    xriBarreBouton.setName("xriBarreBouton");
    add(xriBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMiseEnForme;
  private SNPanelTitre pnlObjetAnalyse;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbDossier;
  private XRiTextField WDOS;
  private SNLabelChamp lbContainer;
  private XRiTextField WCNT;
  private SNPanelTitre PnlDetail;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNLabelChamp lbFacture;
  private XRiTextField EANUM;
  private SNLabelChamp lbDateFacturation;
  private XRiCalendrier EAFACX;
  private SNLabelChamp lbAcheteur;
  private SNAcheteur snAcheteur;
  private SNLabelTitre lbAnalyseImputation;
  private SNPanel pnlAnalyseImputation;
  private JScrollPane scpScrollTableau;
  private XRiTable WTB01;
  private XRiBarreBouton xriBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
