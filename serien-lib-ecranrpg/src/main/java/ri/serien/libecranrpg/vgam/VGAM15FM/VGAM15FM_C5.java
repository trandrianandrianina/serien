
package ri.serien.libecranrpg.vgam.VGAM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM15FM_C5 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGAM15FM_C5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Titre
    setTitle("Détail d'une ligne");
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAUNA@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAUNC@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    WCOD.setVisible(lexique.isPresent("WCOD"));
    L20NAT.setVisible(lexique.isPresent("L20NAT"));
    LATVA.setEnabled(lexique.isPresent("LATVA"));
    OBJ_57.setVisible(lexique.isPresent("LAUNC"));
    OBJ_28.setVisible(lexique.isPresent("A1UNS"));
    OBJ_26.setVisible(lexique.isPresent("LAUNA"));
    WNLI.setVisible(lexique.isPresent("WNLI"));
    OBJ_62.setVisible(lexique.isPresent("LAACT"));
    L20NAT.setEnabled(lexique.isPresent("L20NAT"));
    LAACT.setEnabled(lexique.isPresent("LAACT"));
    LASAN.setEnabled(lexique.isPresent("LASAN"));
    OBJ_22.setVisible(lexique.isPresent("LASAN"));
    LAQTSX.setEnabled(lexique.isPresent("LAQTSX"));
    LAQTAX.setVisible(lexique.isPresent("LAQTAX"));
    LAQTCX.setEnabled(lexique.isPresent("LAQTCX"));
    LAMTTX.setEnabled(lexique.isPresent("LAMTTX"));
    LAPANX.setEnabled(lexique.isPresent("LAPANX"));
    LAKSCX.setEnabled(lexique.isPresent("LAKSCX"));
    LAKACX.setEnabled(lexique.isPresent("LAKACX"));
    LAART.setVisible(lexique.isPresent("LAART"));
    LALIB4.setEnabled(lexique.isPresent("LALIB4"));
    LALIB3.setEnabled(lexique.isPresent("LALIB3"));
    LALIB2.setEnabled(lexique.isPresent("LALIB2"));
    // LIB.setEnabled( lexique.isPresent("LIB"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("QTCX");
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("PANX");
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    LAART = new XRiTextField();
    OBJ_52 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_22 = new JLabel();
    LASAN = new XRiTextField();
    LAACT = new XRiTextField();
    L20NAT = new XRiTextField();
    OBJ_62 = new JLabel();
    WNLI = new XRiTextField();
    LANAT = new XRiTextField();
    WCOD = new XRiTextField();
    OBJ_21 = new JLabel();
    panel5 = new JPanel();
    LALIB1 = new XRiTextField();
    LALIB2 = new XRiTextField();
    LALIB3 = new XRiTextField();
    LALIB4 = new XRiTextField();
    LAPANX = new XRiTextField();
    LAMTTX = new XRiTextField();
    OBJ_64 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_20 = new JLabel();
    LATVA = new XRiTextField();
    OBJ_66 = new JLabel();
    panel6 = new JPanel();
    LAKACX = new XRiTextField();
    LAKSCX = new XRiTextField();
    OBJ_23 = new JLabel();
    LAQTCX = new XRiTextField();
    LAQTAX = new XRiTextField();
    LAQTSX = new XRiTextField();
    OBJ_58 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_57 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 345));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Options articles");
              riSousMenu_bt6.setToolTipText("Options articles");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("CNA");
              riSousMenu_bt7.setToolTipText("Conditions normales d'achat");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Cout derni\u00e8re entr\u00e9e");
              riSousMenu_bt8.setToolTipText("Cout derni\u00e8re entr\u00e9e");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Modification libell\u00e9");
              riSousMenu_bt9.setToolTipText("Modification libell\u00e9");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- LAART ----
        LAART.setComponentPopupMenu(BTD);
        LAART.setName("LAART");
        p_contenu.add(LAART);
        LAART.setBounds(105, 55, 210, LAART.getPreferredSize().height);

        //---- OBJ_52 ----
        OBJ_52.setText("C     N\u00b0Li");
        OBJ_52.setName("OBJ_52");
        p_contenu.add(OBJ_52);
        OBJ_52.setBounds(40, 35, 63, 20);

        //---- OBJ_61 ----
        OBJ_61.setText("Nature");
        OBJ_61.setName("OBJ_61");
        p_contenu.add(OBJ_61);
        OBJ_61.setBounds(255, 95, 43, 20);

        //---- OBJ_22 ----
        OBJ_22.setText("Sect");
        OBJ_22.setName("OBJ_22");
        p_contenu.add(OBJ_22);
        OBJ_22.setBounds(105, 95, 39, 20);

        //---- LASAN ----
        LASAN.setComponentPopupMenu(BTD);
        LASAN.setName("LASAN");
        p_contenu.add(LASAN);
        LASAN.setBounds(105, 115, 50, LASAN.getPreferredSize().height);

        //---- LAACT ----
        LAACT.setComponentPopupMenu(BTD);
        LAACT.setName("LAACT");
        p_contenu.add(LAACT);
        LAACT.setBounds(180, 115, 50, LAACT.getPreferredSize().height);

        //---- L20NAT ----
        L20NAT.setComponentPopupMenu(BTD);
        L20NAT.setName("L20NAT");
        p_contenu.add(L20NAT);
        L20NAT.setBounds(255, 115, 60, L20NAT.getPreferredSize().height);

        //---- OBJ_62 ----
        OBJ_62.setText("/  Aff");
        OBJ_62.setName("OBJ_62");
        p_contenu.add(OBJ_62);
        OBJ_62.setBounds(180, 95, 39, 20);

        //---- WNLI ----
        WNLI.setComponentPopupMenu(BTD);
        WNLI.setName("WNLI");
        p_contenu.add(WNLI);
        WNLI.setBounds(55, 55, 50, WNLI.getPreferredSize().height);

        //---- LANAT ----
        LANAT.setComponentPopupMenu(BTD);
        LANAT.setName("LANAT");
        p_contenu.add(LANAT);
        LANAT.setBounds(35, 115, 20, LANAT.getPreferredSize().height);

        //---- WCOD ----
        WCOD.setComponentPopupMenu(BTD);
        WCOD.setName("WCOD");
        p_contenu.add(WCOD);
        WCOD.setBounds(35, 55, 20, WCOD.getPreferredSize().height);

        //---- OBJ_21 ----
        OBJ_21.setText("N");
        OBJ_21.setName("OBJ_21");
        p_contenu.add(OBJ_21);
        OBJ_21.setBounds(40, 95, 13, 20);

        //======== panel5 ========
        {
          panel5.setBorder(new TitledBorder("Montant de la remise"));
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);

          //---- LALIB1 ----
          LALIB1.setComponentPopupMenu(BTD);
          LALIB1.setName("LALIB1");
          panel5.add(LALIB1);
          LALIB1.setBounds(20, 35, 310, LALIB1.getPreferredSize().height);

          //---- LALIB2 ----
          LALIB2.setComponentPopupMenu(BTD);
          LALIB2.setName("LALIB2");
          panel5.add(LALIB2);
          LALIB2.setBounds(20, 65, 310, LALIB2.getPreferredSize().height);

          //---- LALIB3 ----
          LALIB3.setComponentPopupMenu(BTD);
          LALIB3.setName("LALIB3");
          panel5.add(LALIB3);
          LALIB3.setBounds(20, 95, 310, LALIB3.getPreferredSize().height);

          //---- LALIB4 ----
          LALIB4.setComponentPopupMenu(BTD);
          LALIB4.setName("LALIB4");
          panel5.add(LALIB4);
          LALIB4.setBounds(20, 125, 310, LALIB4.getPreferredSize().height);

          //---- LAPANX ----
          LAPANX.setComponentPopupMenu(BTD);
          LAPANX.setName("LAPANX");
          panel5.add(LAPANX);
          LAPANX.setBounds(410, 65, 110, LAPANX.getPreferredSize().height);

          //---- LAMTTX ----
          LAMTTX.setComponentPopupMenu(BTD);
          LAMTTX.setName("LAMTTX");
          panel5.add(LAMTTX);
          LAMTTX.setBounds(570, 65, 110, LAMTTX.getPreferredSize().height);

          //---- OBJ_64 ----
          OBJ_64.setText("Pour 1 UA");
          OBJ_64.setName("OBJ_64");
          panel5.add(OBJ_64);
          OBJ_64.setBounds(415, 40, 73, 18);

          //---- OBJ_65 ----
          OBJ_65.setText("Mtt global");
          OBJ_65.setName("OBJ_65");
          panel5.add(OBJ_65);
          OBJ_65.setBounds(575, 40, 73, 18);

          //---- OBJ_20 ----
          OBJ_20.setText("TVA");
          OBJ_20.setName("OBJ_20");
          panel5.add(OBJ_20);
          OBJ_20.setBounds(375, 99, 30, 20);

          //---- LATVA ----
          LATVA.setComponentPopupMenu(BTD);
          LATVA.setName("LATVA");
          panel5.add(LATVA);
          LATVA.setBounds(410, 95, 20, LATVA.getPreferredSize().height);

          //---- OBJ_66 ----
          OBJ_66.setText("ou");
          OBJ_66.setName("OBJ_66");
          panel5.add(OBJ_66);
          OBJ_66.setBounds(535, 69, 18, 20);
        }
        p_contenu.add(panel5);
        panel5.setBounds(10, 160, 705, 175);

        //======== panel6 ========
        {
          panel6.setBorder(new TitledBorder("Quantit\u00e9"));
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);

          //---- LAKACX ----
          LAKACX.setComponentPopupMenu(BTD);
          LAKACX.setName("LAKACX");
          panel6.add(LAKACX);
          LAKACX.setBounds(255, 65, 90, LAKACX.getPreferredSize().height);

          //---- LAKSCX ----
          LAKSCX.setComponentPopupMenu(BTD);
          LAKSCX.setName("LAKSCX");
          panel6.add(LAKSCX);
          LAKSCX.setBounds(255, 95, 90, LAKSCX.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("Commande");
          OBJ_23.setName("OBJ_23");
          panel6.add(OBJ_23);
          OBJ_23.setBounds(20, 39, 73, 20);

          //---- LAQTCX ----
          LAQTCX.setComponentPopupMenu(BTD);
          LAQTCX.setName("LAQTCX");
          panel6.add(LAQTCX);
          LAQTCX.setBounds(95, 35, 110, LAQTCX.getPreferredSize().height);

          //---- LAQTAX ----
          LAQTAX.setComponentPopupMenu(BTD);
          LAQTAX.setName("LAQTAX");
          panel6.add(LAQTAX);
          LAQTAX.setBounds(95, 65, 110, LAQTAX.getPreferredSize().height);

          //---- LAQTSX ----
          LAQTSX.setComponentPopupMenu(BTD);
          LAQTSX.setName("LAQTSX");
          panel6.add(LAQTSX);
          LAQTSX.setBounds(95, 95, 110, LAQTSX.getPreferredSize().height);

          //---- OBJ_58 ----
          OBJ_58.setText("@NB@");
          OBJ_58.setName("OBJ_58");
          panel6.add(OBJ_58);
          OBJ_58.setBounds(255, 39, 66, 20);

          //---- OBJ_25 ----
          OBJ_25.setText("Achat");
          OBJ_25.setName("OBJ_25");
          panel6.add(OBJ_25);
          OBJ_25.setBounds(20, 69, 37, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("Stock");
          OBJ_27.setName("OBJ_27");
          panel6.add(OBJ_27);
          OBJ_27.setBounds(20, 99, 37, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("@LAUNA@");
          OBJ_26.setName("OBJ_26");
          panel6.add(OBJ_26);
          OBJ_26.setBounds(220, 69, 22, 20);

          //---- OBJ_28 ----
          OBJ_28.setText("@A1UNS@");
          OBJ_28.setName("OBJ_28");
          panel6.add(OBJ_28);
          OBJ_28.setBounds(220, 99, 22, 20);

          //---- OBJ_57 ----
          OBJ_57.setText("@LAUNC@");
          OBJ_57.setName("OBJ_57");
          panel6.add(OBJ_57);
          OBJ_57.setBounds(220, 39, 21, 20);
        }
        p_contenu.add(panel6);
        panel6.setBounds(325, 15, 390, 145);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private JPanel p_contenu;
  private XRiTextField LAART;
  private JLabel OBJ_52;
  private JLabel OBJ_61;
  private JLabel OBJ_22;
  private XRiTextField LASAN;
  private XRiTextField LAACT;
  private XRiTextField L20NAT;
  private JLabel OBJ_62;
  private XRiTextField WNLI;
  private XRiTextField LANAT;
  private XRiTextField WCOD;
  private JLabel OBJ_21;
  private JPanel panel5;
  private XRiTextField LALIB1;
  private XRiTextField LALIB2;
  private XRiTextField LALIB3;
  private XRiTextField LALIB4;
  private XRiTextField LAPANX;
  private XRiTextField LAMTTX;
  private JLabel OBJ_64;
  private JLabel OBJ_65;
  private JLabel OBJ_20;
  private XRiTextField LATVA;
  private JLabel OBJ_66;
  private JPanel panel6;
  private XRiTextField LAKACX;
  private XRiTextField LAKSCX;
  private JLabel OBJ_23;
  private XRiTextField LAQTCX;
  private XRiTextField LAQTAX;
  private XRiTextField LAQTSX;
  private JLabel OBJ_58;
  private JLabel OBJ_25;
  private JLabel OBJ_27;
  private JLabel OBJ_26;
  private JLabel OBJ_28;
  private JLabel OBJ_57;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
