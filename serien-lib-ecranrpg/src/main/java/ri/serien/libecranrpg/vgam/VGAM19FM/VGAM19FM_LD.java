/*
 * Created by JFormDesigner on Tue Feb 25 15:49:47 CET 2020
 */

package ri.serien.libecranrpg.vgam.VGAM19FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.achat.documentachat.snacheteur.SNAcheteur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GAM2192] Gestion des achats -> Documents d'achats -> Commandes et réapprovisionnements -> Gestion des demandes d'achats ->
 * Centralisation de demandes d'achats
 * Indicateur : 00000001
 * Titre : Centralisation de demandes d'achats
 */
public class VGAM19FM_LD extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TETLD", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 600, };
  
  public VGAM19FM_LD(ArrayList param) {
    super(param);
    initComponents();
    
    initDiverses();
    
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
    
    xriBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    xriBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    xriBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "P19ETB");
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("P19ETB"));
    
    // Initialise les composants
    chargerMagasin();
    chargerAcheteur();
    chargerFournisseur();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "P19ETB");
    snMagasin.renseignerChampRPG(lexique, "P19MAG");
    snAcheteur.renseignerChampRPG(lexique, "P19ACH");
    snFournisseur.renseignerChampRPG(lexique, "WCOL", "WFRS");
  }
  
  /**
   * Initialise le composant magasin
   */
  private void chargerMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "P19MAG");
  }
  
  /**
   * Initialise le composant acheteur
   */
  private void chargerAcheteur() {
    snAcheteur.setSession(getSession());
    snAcheteur.setIdEtablissement(snEtablissement.getIdSelection());
    snAcheteur.setTousAutorise(true);
    snAcheteur.charger(false);
    snAcheteur.setSelectionParChampRPG(lexique, "P19ACH");
  }
  
  /**
   * Initialise le composant fournisseur
   */
  private void chargerFournisseur() {
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(false);
    snFournisseur.setSelectionParChampRPG(lexique, "WCOL", "WFRS");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    xriBarreBouton.isTraiterClickBouton(pSNBouton);
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F13");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void miAffichageActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void miModificationActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void miDemandePrixActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void miAttenteCommandeActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbDate = new SNLabelChamp();
    WDATX = new XRiCalendrier();
    pnlAcheteurMagasin = new SNPanelTitre();
    lbAcheteur = new SNLabelChamp();
    snAcheteur = new SNAcheteur();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlInformation = new SNPanelTitre();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    scrollPane1 = new JScrollPane();
    WTP01 = new XRiTable();
    xriBarreBouton = new XRiBarreBouton();
    pmBTD = new JPopupMenu();
    miAffichage = new JMenuItem();
    miModification = new JMenuItem();
    miDemandePrix = new JMenuItem();
    miAttenteCommande = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("Centralisation de demandes d'achats");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

      //======== pnlEtablissement ========
      {
        pnlEtablissement.setName("pnlEtablissement");
        pnlEtablissement.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
        ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setMaximumSize(new Dimension(200, 30));
        lbEtablissement.setMinimumSize(new Dimension(200, 30));
        lbEtablissement.setPreferredSize(new Dimension(200, 30));
        lbEtablissement.setName("lbEtablissement");
        pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- snEtablissement ----
        snEtablissement.setEnabled(false);
        snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
        snEtablissement.setName("snEtablissement");
        pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- lbDate ----
        lbDate.setText("Date");
        lbDate.setName("lbDate");
        pnlEtablissement.add(lbDate, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WDATX ----
        WDATX.setPreferredSize(new Dimension(110, 30));
        WDATX.setMinimumSize(new Dimension(110, 30));
        WDATX.setMaximumSize(new Dimension(110, 30));
        WDATX.setEnabled(false);
        WDATX.setFont(new Font("sansserif", Font.PLAIN, 14));
        WDATX.setName("WDATX");
        pnlEtablissement.add(WDATX, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlAcheteurMagasin ========
      {
        pnlAcheteurMagasin.setName("pnlAcheteurMagasin");
        pnlAcheteurMagasin.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlAcheteurMagasin.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlAcheteurMagasin.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlAcheteurMagasin.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlAcheteurMagasin.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbAcheteur ----
        lbAcheteur.setText("Acheteur pour la  centralisation");
        lbAcheteur.setPreferredSize(new Dimension(200, 30));
        lbAcheteur.setMinimumSize(new Dimension(200, 30));
        lbAcheteur.setMaximumSize(new Dimension(200, 30));
        lbAcheteur.setName("lbAcheteur");
        pnlAcheteurMagasin.add(lbAcheteur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snAcheteur ----
        snAcheteur.setEnabled(false);
        snAcheteur.setFont(new Font("sansserif", Font.PLAIN, 14));
        snAcheteur.setName("snAcheteur");
        pnlAcheteurMagasin.add(snAcheteur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbMagasin ----
        lbMagasin.setText("Magasin d'achat");
        lbMagasin.setPreferredSize(new Dimension(200, 30));
        lbMagasin.setMinimumSize(new Dimension(200, 30));
        lbMagasin.setMaximumSize(new Dimension(200, 30));
        lbMagasin.setName("lbMagasin");
        pnlAcheteurMagasin.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- snMagasin ----
        snMagasin.setEnabled(false);
        snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
        snMagasin.setName("snMagasin");
        pnlAcheteurMagasin.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlAcheteurMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlInformation ========
      {
        pnlInformation.setName("pnlInformation");
        pnlInformation.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlInformation.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlInformation.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlInformation.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlInformation.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbFournisseur ----
        lbFournisseur.setText("Fournisseur principal");
        lbFournisseur.setPreferredSize(new Dimension(200, 30));
        lbFournisseur.setMinimumSize(new Dimension(200, 30));
        lbFournisseur.setMaximumSize(new Dimension(200, 30));
        lbFournisseur.setName("lbFournisseur");
        pnlInformation.add(lbFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snFournisseur ----
        snFournisseur.setFont(new Font("sansserif", Font.PLAIN, 14));
        snFournisseur.setName("snFournisseur");
        pnlInformation.add(snFournisseur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== scrollPane1 ========
        {
          scrollPane1.setPreferredSize(new Dimension(500, 270));
          scrollPane1.setMinimumSize(new Dimension(500, 270));
          scrollPane1.setMaximumSize(new Dimension(500, 270));
          scrollPane1.setName("scrollPane1");

          //---- WTP01 ----
          WTP01.setFont(new Font("Courier New", Font.PLAIN, 12));
          WTP01.setModel(new DefaultTableModel(
            new Object[][] {
              {"@LD01@"},
              {"@LD02@"},
              {"@LD03@"},
              {"@LD04@"},
              {"@LD05@"},
              {"@LD06@"},
              {"@LD07@"},
              {"@LD08@"},
              {"@LD09@"},
              {"@LD10@"},
              {"@LD11@"},
              {"@LD12@"},
              {"@LD13@"},
              {"@LD14@"},
              {"@LD15@"},
            },
            new String[] {
              "@TETLD@"
            }
          ));
          {
            TableColumnModel cm = WTP01.getColumnModel();
            cm.getColumn(0).setPreferredWidth(600);
          }
          WTP01.setPreferredSize(new Dimension(600, 240));
          WTP01.setMinimumSize(new Dimension(600, 240));
          WTP01.setMaximumSize(new Dimension(600, 240));
          WTP01.setComponentPopupMenu(pmBTD);
          WTP01.setName("WTP01");
          scrollPane1.setViewportView(WTP01);
        }
        pnlInformation.add(scrollPane1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlInformation, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- xriBarreBouton ----
    xriBarreBouton.setName("xriBarreBouton");
    add(xriBarreBouton, BorderLayout.SOUTH);

    //======== pmBTD ========
    {
      pmBTD.setName("pmBTD");

      //---- miAffichage ----
      miAffichage.setText("Affichage");
      miAffichage.setName("miAffichage");
      miAffichage.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAffichageActionPerformed(e);
        }
      });
      pmBTD.add(miAffichage);

      //---- miModification ----
      miModification.setText("Modification");
      miModification.setName("miModification");
      miModification.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miModificationActionPerformed(e);
        }
      });
      pmBTD.add(miModification);

      //---- miDemandePrix ----
      miDemandePrix.setText("Demande de prix");
      miDemandePrix.setName("miDemandePrix");
      miDemandePrix.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miDemandePrixActionPerformed(e);
        }
      });
      pmBTD.add(miDemandePrix);

      //---- miAttenteCommande ----
      miAttenteCommande.setText("Attente avant commande");
      miAttenteCommande.setName("miAttenteCommande");
      miAttenteCommande.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAttenteCommandeActionPerformed(e);
        }
      });
      pmBTD.add(miAttenteCommande);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbDate;
  private XRiCalendrier WDATX;
  private SNPanelTitre pnlAcheteurMagasin;
  private SNLabelChamp lbAcheteur;
  private SNAcheteur snAcheteur;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNPanelTitre pnlInformation;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private JScrollPane scrollPane1;
  private XRiTable WTP01;
  private XRiBarreBouton xriBarreBouton;
  private JPopupMenu pmBTD;
  private JMenuItem miAffichage;
  private JMenuItem miModification;
  private JMenuItem miDemandePrix;
  private JMenuItem miAttenteCommande;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
