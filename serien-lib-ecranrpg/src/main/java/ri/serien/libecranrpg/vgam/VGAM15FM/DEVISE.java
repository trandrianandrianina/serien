
package ri.serien.libecranrpg.vgam.VGAM15FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.PopupPerso;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class DEVISE extends SNPanelEcranRPG implements ioFrame {
  
  private PopupPerso popupPerso = new PopupPerso(this);
  private String tauxChange = "";
  private String base = "";
  
  public DEVISE(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    setModal(true);
    setCloseKey("ENTER", "F12");
    setPopupPerso(popupPerso);
    popupPerso.ajouterProprieteTouche("ENTER", false, false, false);
    popupPerso.ajouterProprieteTouche("F12", false, false, false);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    setTitle("Devise");
    
    // Sauvegarde valeurs en entrée
    tauxChange = lexique.HostFieldGetData("EACHGX");
    base = lexique.HostFieldGetData("EABAS");
    
    // TODO Icones
    bouton_valider.setIcon(lexique.chargerImage("images/ok_p.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  public void affiche() {
    setVisible(true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("EACHGX", 0, EACHGX.getText());
    lexique.HostFieldPutData("EABAS", 0, EABAS.getText());
    popupPerso.traiterTouche("ENTER");
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("EACHGX", 0, tauxChange);
    lexique.HostFieldPutData("EABAS", 0, base);
    popupPerso.traiterTouche("F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    P_Centre = new JPanel();
    panel1 = new JPanel();
    OBJ_38 = new JLabel();
    EACHGX = new XRiTextField();
    OBJ_39 = new JLabel();
    EABAS = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(840, 150));
    setPreferredSize(new Dimension(510, 120));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_menus ========
    {
      p_menus.setPreferredSize(new Dimension(170, 0));
      p_menus.setMinimumSize(new Dimension(170, 0));
      p_menus.setBackground(new Color(238, 239, 241));
      p_menus.setBorder(LineBorder.createGrayLineBorder());
      p_menus.setName("p_menus");
      p_menus.setLayout(new BorderLayout());

      //======== menus_bas ========
      {
        menus_bas.setOpaque(false);
        menus_bas.setBackground(new Color(238, 239, 241));
        menus_bas.setName("menus_bas");
        menus_bas.setLayout(new VerticalLayout());

        //======== navig_valid ========
        {
          navig_valid.setName("navig_valid");

          //---- bouton_valider ----
          bouton_valider.setText("Valider");
          bouton_valider.setToolTipText("Valider");
          bouton_valider.setName("bouton_valider");
          bouton_valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_validerActionPerformed(e);
            }
          });
          navig_valid.add(bouton_valider);
        }
        menus_bas.add(navig_valid);

        //======== navig_retour ========
        {
          navig_retour.setName("navig_retour");

          //---- bouton_retour ----
          bouton_retour.setText("Retour");
          bouton_retour.setToolTipText("Retour");
          bouton_retour.setName("bouton_retour");
          bouton_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_retourActionPerformed(e);
            }
          });
          navig_retour.add(bouton_retour);
        }
        menus_bas.add(navig_retour);
      }
      p_menus.add(menus_bas, BorderLayout.SOUTH);
    }
    add(p_menus, BorderLayout.EAST);

    //======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder(""));
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- OBJ_38 ----
        OBJ_38.setText("Taux de change");
        OBJ_38.setName("OBJ_38");
        panel1.add(OBJ_38);
        OBJ_38.setBounds(25, 25, 111, 20);

        //---- EACHGX ----
        EACHGX.setComponentPopupMenu(null);
        EACHGX.setName("EACHGX");
        panel1.add(EACHGX);
        EACHGX.setBounds(175, 20, 85, EACHGX.getPreferredSize().height);

        //---- OBJ_39 ----
        OBJ_39.setText("Base");
        OBJ_39.setName("OBJ_39");
        panel1.add(OBJ_39);
        OBJ_39.setBounds(25, 64, 111, 20);

        //---- EABAS ----
        EABAS.setComponentPopupMenu(null);
        EABAS.setName("EABAS");
        panel1.add(EABAS);
        EABAS.setBounds(175, 60, 52, EABAS.getPreferredSize().height);
      }
      P_Centre.add(panel1);
      panel1.setBounds(5, 5, 325, 110);
    }
    add(P_Centre, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel P_Centre;
  private JPanel panel1;
  private JLabel OBJ_38;
  private XRiTextField EACHGX;
  private JLabel OBJ_39;
  private XRiTextField EABAS;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
