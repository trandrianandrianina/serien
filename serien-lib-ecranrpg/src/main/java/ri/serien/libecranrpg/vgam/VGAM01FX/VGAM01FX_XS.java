
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_XS extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private JTextField[] etb = new JTextField[25];
  private JCheckBox[] top = new JCheckBox[75];
  private XRiComboBox[] spe = new XRiComboBox[25];
  private String[] spe_Value = { "", "1", "2" };
  
  public VGAM01FX_XS(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    etb[0] = ETB01;
    etb[1] = ETB02;
    etb[2] = ETB03;
    etb[3] = ETB04;
    etb[4] = ETB05;
    etb[5] = ETB06;
    etb[6] = ETB07;
    etb[7] = ETB08;
    etb[8] = ETB09;
    etb[9] = ETB10;
    etb[10] = ETB11;
    etb[11] = ETB12;
    etb[12] = ETB13;
    etb[13] = ETB14;
    etb[14] = ETB15;
    etb[15] = ETB16;
    etb[16] = ETB17;
    etb[17] = ETB18;
    etb[18] = ETB19;
    etb[19] = ETB20;
    etb[20] = ETB21;
    etb[21] = ETB22;
    etb[22] = ETB23;
    etb[23] = ETB24;
    etb[24] = ETB25;
    
    top[0] = T101;
    top[1] = T201;
    top[2] = T301;
    top[3] = T102;
    top[4] = T202;
    top[5] = T302;
    top[6] = T103;
    top[7] = T203;
    top[8] = T303;
    top[9] = T104;
    top[10] = T204;
    top[11] = T304;
    top[12] = T105;
    top[13] = T205;
    top[14] = T305;
    top[15] = T106;
    top[16] = T206;
    top[17] = T306;
    top[18] = T107;
    top[19] = T207;
    top[20] = T307;
    top[21] = T108;
    top[22] = T208;
    top[23] = T308;
    top[24] = T109;
    top[25] = T209;
    top[26] = T309;
    top[27] = T110;
    top[28] = T210;
    top[29] = T310;
    top[30] = T111;
    top[31] = T211;
    top[32] = T311;
    top[33] = T112;
    top[34] = T212;
    top[35] = T312;
    top[36] = T113;
    top[37] = T213;
    top[38] = T313;
    top[39] = T114;
    top[40] = T214;
    top[41] = T314;
    top[42] = T115;
    top[43] = T215;
    top[44] = T315;
    top[45] = T116;
    top[46] = T216;
    top[47] = T316;
    top[48] = T117;
    top[49] = T217;
    top[50] = T317;
    top[51] = T118;
    top[52] = T218;
    top[53] = T318;
    top[54] = T119;
    top[55] = T219;
    top[56] = T319;
    top[57] = T120;
    top[58] = T220;
    top[59] = T320;
    top[60] = T121;
    top[61] = T221;
    top[62] = T321;
    top[63] = T122;
    top[64] = T222;
    top[65] = T322;
    top[66] = T123;
    top[67] = T223;
    top[68] = T323;
    top[69] = T124;
    top[70] = T224;
    top[71] = T324;
    top[72] = T125;
    top[73] = T225;
    top[74] = T325;
    
    // intit des combos
    T401.setValeurs(spe_Value, null);
    T402.setValeurs(spe_Value, null);
    T403.setValeurs(spe_Value, null);
    T404.setValeurs(spe_Value, null);
    T405.setValeurs(spe_Value, null);
    T406.setValeurs(spe_Value, null);
    T407.setValeurs(spe_Value, null);
    T408.setValeurs(spe_Value, null);
    T409.setValeurs(spe_Value, null);
    T410.setValeurs(spe_Value, null);
    T411.setValeurs(spe_Value, null);
    T412.setValeurs(spe_Value, null);
    T413.setValeurs(spe_Value, null);
    T414.setValeurs(spe_Value, null);
    T415.setValeurs(spe_Value, null);
    T416.setValeurs(spe_Value, null);
    T417.setValeurs(spe_Value, null);
    T418.setValeurs(spe_Value, null);
    T419.setValeurs(spe_Value, null);
    T420.setValeurs(spe_Value, null);
    T421.setValeurs(spe_Value, null);
    T422.setValeurs(spe_Value, null);
    T423.setValeurs(spe_Value, null);
    T424.setValeurs(spe_Value, null);
    T425.setValeurs(spe_Value, null);
    
    spe[0] = T401;
    spe[1] = T402;
    spe[2] = T403;
    spe[3] = T404;
    spe[4] = T405;
    spe[5] = T406;
    spe[6] = T407;
    spe[7] = T408;
    spe[8] = T409;
    spe[9] = T410;
    spe[10] = T411;
    spe[11] = T412;
    spe[12] = T413;
    spe[13] = T414;
    spe[14] = T415;
    spe[15] = T416;
    spe[16] = T417;
    spe[17] = T418;
    spe[18] = T419;
    spe[19] = T420;
    spe[20] = T421;
    spe[21] = T422;
    spe[22] = T423;
    spe[23] = T424;
    spe[24] = T425;
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    for (int i = 0; i < etb.length; i++) {
      etb[i].setText(lexique.HostFieldGetData("MS1").substring(i * 3, (i * 3 + 3)));
      etb[i].setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
      
    }
    for (int i = 0; i < top.length; i++) {
      top[i].setSelected(!lexique.HostFieldGetData("MS2").substring(i, (i + 1)).equals(" "));
      top[i].setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    }
    for (int i = 0; i < spe.length; i++) {
      if (!lexique.HostFieldGetData("MS3").substring(i * 3, (i * 3 + 1)).trim().equals("")) {
        spe[i].setSelectedIndex(Integer.parseInt(lexique.HostFieldGetData("MS3").substring(i * 3, (i * 3 + 1))));
      }
      else {
        spe[i].setSelectedIndex(0);
      }
      spe[i].setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Personnalisation de @LOCGRP/-1/@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
    String etablissements = "";
    String tops = "";
    String spes = "";
    
    for (int i = 0; i < etb.length; i++) {
      if (!etb[i].getText().trim().equals("")) {
        etablissements += etb[i].getText().trim();
      }
      else {
        etablissements += "   ";
      }
    }
    for (int i = 0; i < top.length; i++) {
      if (top[i].isSelected()) {
        tops += 1;
      }
      else {
        tops += " ";
      }
    }
    for (int i = 0; i < spe.length; i++) {
      if (spe[i].getSelectedIndex() == 1) {
        spes += "1  ";
      }
      else if (spe[i].getSelectedIndex() == 2) {
        spes += "2  ";
      }
      else {
        spes += "   ";
      }
    }
    lexique.HostFieldPutData("MS1", 0, etablissements);
    lexique.HostFieldPutData("MS2", 0, tops);
    lexique.HostFieldPutData("MS3", 0, spes);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_47 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_49 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel5 = new JXTitledPanel();
    panel1 = new JPanel();
    ETB01 = new JTextField();
    ETB02 = new JTextField();
    ETB03 = new JTextField();
    ETB04 = new JTextField();
    ETB05 = new JTextField();
    ETB06 = new JTextField();
    ETB07 = new JTextField();
    ETB08 = new JTextField();
    ETB09 = new JTextField();
    ETB10 = new JTextField();
    ETB11 = new JTextField();
    ETB12 = new JTextField();
    ETB13 = new JTextField();
    T101 = new JCheckBox();
    T201 = new JCheckBox();
    T301 = new JCheckBox();
    T102 = new JCheckBox();
    T202 = new JCheckBox();
    T302 = new JCheckBox();
    T103 = new JCheckBox();
    T203 = new JCheckBox();
    T303 = new JCheckBox();
    T104 = new JCheckBox();
    T204 = new JCheckBox();
    T304 = new JCheckBox();
    T105 = new JCheckBox();
    T205 = new JCheckBox();
    T305 = new JCheckBox();
    T106 = new JCheckBox();
    T206 = new JCheckBox();
    T306 = new JCheckBox();
    T107 = new JCheckBox();
    T207 = new JCheckBox();
    T307 = new JCheckBox();
    T108 = new JCheckBox();
    T208 = new JCheckBox();
    T308 = new JCheckBox();
    T109 = new JCheckBox();
    T209 = new JCheckBox();
    T309 = new JCheckBox();
    T110 = new JCheckBox();
    T210 = new JCheckBox();
    T310 = new JCheckBox();
    T111 = new JCheckBox();
    T211 = new JCheckBox();
    T311 = new JCheckBox();
    T112 = new JCheckBox();
    T212 = new JCheckBox();
    T312 = new JCheckBox();
    T113 = new JCheckBox();
    T213 = new JCheckBox();
    T313 = new JCheckBox();
    T401 = new XRiComboBox();
    T402 = new XRiComboBox();
    T403 = new XRiComboBox();
    T404 = new XRiComboBox();
    T405 = new XRiComboBox();
    T406 = new XRiComboBox();
    T407 = new XRiComboBox();
    T408 = new XRiComboBox();
    T409 = new XRiComboBox();
    T410 = new XRiComboBox();
    T411 = new XRiComboBox();
    T412 = new XRiComboBox();
    T413 = new XRiComboBox();
    panel2 = new JPanel();
    ETB14 = new JTextField();
    ETB15 = new JTextField();
    ETB16 = new JTextField();
    ETB17 = new JTextField();
    ETB18 = new JTextField();
    ETB19 = new JTextField();
    ETB20 = new JTextField();
    ETB21 = new JTextField();
    ETB22 = new JTextField();
    ETB23 = new JTextField();
    ETB24 = new JTextField();
    ETB25 = new JTextField();
    T114 = new XRiCheckBox();
    T214 = new XRiCheckBox();
    T314 = new XRiCheckBox();
    T115 = new XRiCheckBox();
    T215 = new XRiCheckBox();
    T315 = new XRiCheckBox();
    T116 = new XRiCheckBox();
    T216 = new XRiCheckBox();
    T316 = new XRiCheckBox();
    T117 = new XRiCheckBox();
    T217 = new XRiCheckBox();
    T317 = new XRiCheckBox();
    T118 = new XRiCheckBox();
    T218 = new XRiCheckBox();
    T318 = new XRiCheckBox();
    T119 = new XRiCheckBox();
    T219 = new XRiCheckBox();
    T319 = new XRiCheckBox();
    T120 = new XRiCheckBox();
    T220 = new XRiCheckBox();
    T320 = new XRiCheckBox();
    T121 = new XRiCheckBox();
    T221 = new XRiCheckBox();
    T321 = new XRiCheckBox();
    T122 = new XRiCheckBox();
    T222 = new XRiCheckBox();
    T322 = new XRiCheckBox();
    T123 = new XRiCheckBox();
    T223 = new XRiCheckBox();
    T323 = new XRiCheckBox();
    T124 = new XRiCheckBox();
    T224 = new XRiCheckBox();
    T324 = new XRiCheckBox();
    T125 = new XRiCheckBox();
    T225 = new XRiCheckBox();
    T325 = new XRiCheckBox();
    T414 = new XRiComboBox();
    T415 = new XRiComboBox();
    T416 = new XRiComboBox();
    T417 = new XRiComboBox();
    T418 = new XRiComboBox();
    T419 = new XRiComboBox();
    T420 = new XRiComboBox();
    T421 = new XRiComboBox();
    T422 = new XRiComboBox();
    T423 = new XRiComboBox();
    T424 = new XRiComboBox();
    T425 = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_47 ----
          OBJ_47.setText("Code");
          OBJ_47.setName("OBJ_47");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_49 ----
          OBJ_49.setText("Ordre");
          OBJ_49.setName("OBJ_49");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1000, 580));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel5 ========
          {
            xTitledPanel5.setTitle("Modes dans lesquels les informations seront dupliqu\u00e9es de l'\u00e9tablissement en cours vers les \u00e9tablissements ci dessous");
            xTitledPanel5.setBorder(new DropShadowBorder());
            xTitledPanel5.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel5.setMinimumSize(new Dimension(1000, 29));
            xTitledPanel5.setPreferredSize(new Dimension(1000, 29));
            xTitledPanel5.setName("xTitledPanel5");
            Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
            xTitledPanel5ContentContainer.setLayout(null);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setBorder(new LineBorder(Color.lightGray));
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- ETB01 ----
              ETB01.setComponentPopupMenu(BTD);
              ETB01.setName("ETB01");
              panel1.add(ETB01);
              ETB01.setBounds(15, 15, 40, ETB01.getPreferredSize().height);

              //---- ETB02 ----
              ETB02.setComponentPopupMenu(BTD);
              ETB02.setName("ETB02");
              panel1.add(ETB02);
              ETB02.setBounds(15, 52, 40, ETB02.getPreferredSize().height);

              //---- ETB03 ----
              ETB03.setComponentPopupMenu(BTD);
              ETB03.setName("ETB03");
              panel1.add(ETB03);
              ETB03.setBounds(15, 89, 40, ETB03.getPreferredSize().height);

              //---- ETB04 ----
              ETB04.setComponentPopupMenu(BTD);
              ETB04.setName("ETB04");
              panel1.add(ETB04);
              ETB04.setBounds(15, 126, 40, ETB04.getPreferredSize().height);

              //---- ETB05 ----
              ETB05.setComponentPopupMenu(BTD);
              ETB05.setName("ETB05");
              panel1.add(ETB05);
              ETB05.setBounds(15, 163, 40, ETB05.getPreferredSize().height);

              //---- ETB06 ----
              ETB06.setComponentPopupMenu(BTD);
              ETB06.setName("ETB06");
              panel1.add(ETB06);
              ETB06.setBounds(15, 200, 40, ETB06.getPreferredSize().height);

              //---- ETB07 ----
              ETB07.setComponentPopupMenu(BTD);
              ETB07.setName("ETB07");
              panel1.add(ETB07);
              ETB07.setBounds(15, 237, 40, ETB07.getPreferredSize().height);

              //---- ETB08 ----
              ETB08.setComponentPopupMenu(BTD);
              ETB08.setName("ETB08");
              panel1.add(ETB08);
              ETB08.setBounds(15, 274, 40, ETB08.getPreferredSize().height);

              //---- ETB09 ----
              ETB09.setComponentPopupMenu(BTD);
              ETB09.setName("ETB09");
              panel1.add(ETB09);
              ETB09.setBounds(15, 311, 40, ETB09.getPreferredSize().height);

              //---- ETB10 ----
              ETB10.setComponentPopupMenu(BTD);
              ETB10.setName("ETB10");
              panel1.add(ETB10);
              ETB10.setBounds(15, 348, 40, ETB10.getPreferredSize().height);

              //---- ETB11 ----
              ETB11.setComponentPopupMenu(BTD);
              ETB11.setName("ETB11");
              panel1.add(ETB11);
              ETB11.setBounds(15, 385, 40, ETB11.getPreferredSize().height);

              //---- ETB12 ----
              ETB12.setComponentPopupMenu(BTD);
              ETB12.setName("ETB12");
              panel1.add(ETB12);
              ETB12.setBounds(15, 422, 40, ETB12.getPreferredSize().height);

              //---- ETB13 ----
              ETB13.setComponentPopupMenu(BTD);
              ETB13.setName("ETB13");
              panel1.add(ETB13);
              ETB13.setBounds(15, 459, 40, ETB13.getPreferredSize().height);

              //---- T101 ----
              T101.setText("cr\u00e9ation");
              T101.setName("T101");
              panel1.add(T101);
              T101.setBounds(61, 20, 80, T101.getPreferredSize().height);

              //---- T201 ----
              T201.setText("modification");
              T201.setName("T201");
              panel1.add(T201);
              T201.setBounds(135, 20, 100, T201.getPreferredSize().height);

              //---- T301 ----
              T301.setText("annulation");
              T301.setName("T301");
              panel1.add(T301);
              T301.setBounds(230, 20, 95, T301.getPreferredSize().height);

              //---- T102 ----
              T102.setText("cr\u00e9ation");
              T102.setName("T102");
              panel1.add(T102);
              T102.setBounds(60, 57, 80, T102.getPreferredSize().height);

              //---- T202 ----
              T202.setText("modification");
              T202.setName("T202");
              panel1.add(T202);
              T202.setBounds(135, 57, 100, T202.getPreferredSize().height);

              //---- T302 ----
              T302.setText("annulation");
              T302.setName("T302");
              panel1.add(T302);
              T302.setBounds(230, 57, 95, T302.getPreferredSize().height);

              //---- T103 ----
              T103.setText("cr\u00e9ation");
              T103.setName("T103");
              panel1.add(T103);
              T103.setBounds(60, 94, 80, T103.getPreferredSize().height);

              //---- T203 ----
              T203.setText("modification");
              T203.setName("T203");
              panel1.add(T203);
              T203.setBounds(135, 94, 100, T203.getPreferredSize().height);

              //---- T303 ----
              T303.setText("annulation");
              T303.setName("T303");
              panel1.add(T303);
              T303.setBounds(230, 94, 95, T303.getPreferredSize().height);

              //---- T104 ----
              T104.setText("cr\u00e9ation");
              T104.setName("T104");
              panel1.add(T104);
              T104.setBounds(60, 131, 80, T104.getPreferredSize().height);

              //---- T204 ----
              T204.setText("modification");
              T204.setName("T204");
              panel1.add(T204);
              T204.setBounds(135, 131, 100, T204.getPreferredSize().height);

              //---- T304 ----
              T304.setText("annulation");
              T304.setName("T304");
              panel1.add(T304);
              T304.setBounds(230, 131, 95, T304.getPreferredSize().height);

              //---- T105 ----
              T105.setText("cr\u00e9ation");
              T105.setName("T105");
              panel1.add(T105);
              T105.setBounds(60, 168, 80, T105.getPreferredSize().height);

              //---- T205 ----
              T205.setText("modification");
              T205.setName("T205");
              panel1.add(T205);
              T205.setBounds(135, 168, 100, T205.getPreferredSize().height);

              //---- T305 ----
              T305.setText("annulation");
              T305.setName("T305");
              panel1.add(T305);
              T305.setBounds(230, 168, 95, T305.getPreferredSize().height);

              //---- T106 ----
              T106.setText("cr\u00e9ation");
              T106.setName("T106");
              panel1.add(T106);
              T106.setBounds(60, 205, 80, T106.getPreferredSize().height);

              //---- T206 ----
              T206.setText("modification");
              T206.setName("T206");
              panel1.add(T206);
              T206.setBounds(135, 205, 100, T206.getPreferredSize().height);

              //---- T306 ----
              T306.setText("annulation");
              T306.setName("T306");
              panel1.add(T306);
              T306.setBounds(230, 205, 95, T306.getPreferredSize().height);

              //---- T107 ----
              T107.setText("cr\u00e9ation");
              T107.setName("T107");
              panel1.add(T107);
              T107.setBounds(60, 242, 80, T107.getPreferredSize().height);

              //---- T207 ----
              T207.setText("modification");
              T207.setName("T207");
              panel1.add(T207);
              T207.setBounds(135, 242, 100, T207.getPreferredSize().height);

              //---- T307 ----
              T307.setText("annulation");
              T307.setName("T307");
              panel1.add(T307);
              T307.setBounds(230, 242, 95, T307.getPreferredSize().height);

              //---- T108 ----
              T108.setText("cr\u00e9ation");
              T108.setName("T108");
              panel1.add(T108);
              T108.setBounds(60, 279, 80, T108.getPreferredSize().height);

              //---- T208 ----
              T208.setText("modification");
              T208.setName("T208");
              panel1.add(T208);
              T208.setBounds(135, 279, 100, T208.getPreferredSize().height);

              //---- T308 ----
              T308.setText("annulation");
              T308.setName("T308");
              panel1.add(T308);
              T308.setBounds(230, 279, 95, T308.getPreferredSize().height);

              //---- T109 ----
              T109.setText("cr\u00e9ation");
              T109.setName("T109");
              panel1.add(T109);
              T109.setBounds(60, 316, 80, T109.getPreferredSize().height);

              //---- T209 ----
              T209.setText("modification");
              T209.setName("T209");
              panel1.add(T209);
              T209.setBounds(135, 316, 100, T209.getPreferredSize().height);

              //---- T309 ----
              T309.setText("annulation");
              T309.setName("T309");
              panel1.add(T309);
              T309.setBounds(230, 316, 95, T309.getPreferredSize().height);

              //---- T110 ----
              T110.setText("cr\u00e9ation");
              T110.setName("T110");
              panel1.add(T110);
              T110.setBounds(60, 353, 80, T110.getPreferredSize().height);

              //---- T210 ----
              T210.setText("modification");
              T210.setName("T210");
              panel1.add(T210);
              T210.setBounds(135, 353, 100, T210.getPreferredSize().height);

              //---- T310 ----
              T310.setText("annulation");
              T310.setName("T310");
              panel1.add(T310);
              T310.setBounds(230, 353, 95, T310.getPreferredSize().height);

              //---- T111 ----
              T111.setText("cr\u00e9ation");
              T111.setName("T111");
              panel1.add(T111);
              T111.setBounds(60, 390, 80, T111.getPreferredSize().height);

              //---- T211 ----
              T211.setText("modification");
              T211.setName("T211");
              panel1.add(T211);
              T211.setBounds(135, 390, 100, T211.getPreferredSize().height);

              //---- T311 ----
              T311.setText("annulation");
              T311.setName("T311");
              panel1.add(T311);
              T311.setBounds(230, 390, 95, T311.getPreferredSize().height);

              //---- T112 ----
              T112.setText("cr\u00e9ation");
              T112.setName("T112");
              panel1.add(T112);
              T112.setBounds(60, 427, 80, T112.getPreferredSize().height);

              //---- T212 ----
              T212.setText("modification");
              T212.setName("T212");
              panel1.add(T212);
              T212.setBounds(135, 427, 100, T212.getPreferredSize().height);

              //---- T312 ----
              T312.setText("annulation");
              T312.setName("T312");
              panel1.add(T312);
              T312.setBounds(230, 427, 95, T312.getPreferredSize().height);

              //---- T113 ----
              T113.setText("cr\u00e9ation");
              T113.setName("T113");
              panel1.add(T113);
              T113.setBounds(60, 464, 80, T113.getPreferredSize().height);

              //---- T213 ----
              T213.setText("modification");
              T213.setName("T213");
              panel1.add(T213);
              T213.setBounds(135, 464, 100, T213.getPreferredSize().height);

              //---- T313 ----
              T313.setText("annulation");
              T313.setName("T313");
              panel1.add(T313);
              T313.setBounds(230, 464, 95, T313.getPreferredSize().height);

              //---- T401 ----
              T401.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T401.setName("T401");
              panel1.add(T401);
              T401.setBounds(315, 16, 165, T401.getPreferredSize().height);

              //---- T402 ----
              T402.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T402.setName("T402");
              panel1.add(T402);
              T402.setBounds(315, 53, 165, T402.getPreferredSize().height);

              //---- T403 ----
              T403.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T403.setName("T403");
              panel1.add(T403);
              T403.setBounds(315, 90, 165, T403.getPreferredSize().height);

              //---- T404 ----
              T404.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T404.setName("T404");
              panel1.add(T404);
              T404.setBounds(315, 127, 165, T404.getPreferredSize().height);

              //---- T405 ----
              T405.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T405.setName("T405");
              panel1.add(T405);
              T405.setBounds(315, 164, 165, T405.getPreferredSize().height);

              //---- T406 ----
              T406.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T406.setName("T406");
              panel1.add(T406);
              T406.setBounds(315, 201, 165, T406.getPreferredSize().height);

              //---- T407 ----
              T407.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T407.setName("T407");
              panel1.add(T407);
              T407.setBounds(315, 238, 165, T407.getPreferredSize().height);

              //---- T408 ----
              T408.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T408.setName("T408");
              panel1.add(T408);
              T408.setBounds(315, 275, 165, T408.getPreferredSize().height);

              //---- T409 ----
              T409.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T409.setName("T409");
              panel1.add(T409);
              T409.setBounds(315, 312, 165, T409.getPreferredSize().height);

              //---- T410 ----
              T410.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T410.setName("T410");
              panel1.add(T410);
              T410.setBounds(315, 349, 165, T410.getPreferredSize().height);

              //---- T411 ----
              T411.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T411.setName("T411");
              panel1.add(T411);
              T411.setBounds(315, 386, 165, T411.getPreferredSize().height);

              //---- T412 ----
              T412.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T412.setName("T412");
              panel1.add(T412);
              T412.setBounds(315, 423, 165, T412.getPreferredSize().height);

              //---- T413 ----
              T413.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T413.setName("T413");
              panel1.add(T413);
              T413.setBounds(315, 460, 165, T413.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel5ContentContainer.add(panel1);
            panel1.setBounds(5, 5, 485, 500);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setBorder(new LineBorder(Color.lightGray));
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- ETB14 ----
              ETB14.setComponentPopupMenu(BTD);
              ETB14.setName("ETB14");
              panel2.add(ETB14);
              ETB14.setBounds(15, 15, 40, ETB14.getPreferredSize().height);

              //---- ETB15 ----
              ETB15.setComponentPopupMenu(BTD);
              ETB15.setName("ETB15");
              panel2.add(ETB15);
              ETB15.setBounds(15, 52, 40, ETB15.getPreferredSize().height);

              //---- ETB16 ----
              ETB16.setComponentPopupMenu(BTD);
              ETB16.setName("ETB16");
              panel2.add(ETB16);
              ETB16.setBounds(15, 89, 40, ETB16.getPreferredSize().height);

              //---- ETB17 ----
              ETB17.setComponentPopupMenu(BTD);
              ETB17.setName("ETB17");
              panel2.add(ETB17);
              ETB17.setBounds(15, 126, 40, ETB17.getPreferredSize().height);

              //---- ETB18 ----
              ETB18.setComponentPopupMenu(BTD);
              ETB18.setName("ETB18");
              panel2.add(ETB18);
              ETB18.setBounds(15, 163, 40, ETB18.getPreferredSize().height);

              //---- ETB19 ----
              ETB19.setComponentPopupMenu(BTD);
              ETB19.setName("ETB19");
              panel2.add(ETB19);
              ETB19.setBounds(15, 200, 40, ETB19.getPreferredSize().height);

              //---- ETB20 ----
              ETB20.setComponentPopupMenu(BTD);
              ETB20.setName("ETB20");
              panel2.add(ETB20);
              ETB20.setBounds(15, 237, 40, ETB20.getPreferredSize().height);

              //---- ETB21 ----
              ETB21.setComponentPopupMenu(BTD);
              ETB21.setName("ETB21");
              panel2.add(ETB21);
              ETB21.setBounds(15, 274, 40, ETB21.getPreferredSize().height);

              //---- ETB22 ----
              ETB22.setComponentPopupMenu(BTD);
              ETB22.setName("ETB22");
              panel2.add(ETB22);
              ETB22.setBounds(15, 311, 40, ETB22.getPreferredSize().height);

              //---- ETB23 ----
              ETB23.setComponentPopupMenu(BTD);
              ETB23.setName("ETB23");
              panel2.add(ETB23);
              ETB23.setBounds(15, 348, 40, ETB23.getPreferredSize().height);

              //---- ETB24 ----
              ETB24.setComponentPopupMenu(BTD);
              ETB24.setName("ETB24");
              panel2.add(ETB24);
              ETB24.setBounds(15, 385, 40, ETB24.getPreferredSize().height);

              //---- ETB25 ----
              ETB25.setComponentPopupMenu(BTD);
              ETB25.setName("ETB25");
              panel2.add(ETB25);
              ETB25.setBounds(15, 422, 40, ETB25.getPreferredSize().height);

              //---- T114 ----
              T114.setText("cr\u00e9ation");
              T114.setName("T114");
              panel2.add(T114);
              T114.setBounds(60, 20, 80, T114.getPreferredSize().height);

              //---- T214 ----
              T214.setText("modification");
              T214.setName("T214");
              panel2.add(T214);
              T214.setBounds(130, 20, 100, T214.getPreferredSize().height);

              //---- T314 ----
              T314.setText("annulation");
              T314.setName("T314");
              panel2.add(T314);
              T314.setBounds(220, 20, 95, T314.getPreferredSize().height);

              //---- T115 ----
              T115.setText("cr\u00e9ation");
              T115.setName("T115");
              panel2.add(T115);
              T115.setBounds(60, 57, 80, T115.getPreferredSize().height);

              //---- T215 ----
              T215.setText("modification");
              T215.setName("T215");
              panel2.add(T215);
              T215.setBounds(130, 57, 100, T215.getPreferredSize().height);

              //---- T315 ----
              T315.setText("annulation");
              T315.setName("T315");
              panel2.add(T315);
              T315.setBounds(220, 57, 95, T315.getPreferredSize().height);

              //---- T116 ----
              T116.setText("cr\u00e9ation");
              T116.setName("T116");
              panel2.add(T116);
              T116.setBounds(60, 94, 80, T116.getPreferredSize().height);

              //---- T216 ----
              T216.setText("modification");
              T216.setName("T216");
              panel2.add(T216);
              T216.setBounds(130, 94, 100, T216.getPreferredSize().height);

              //---- T316 ----
              T316.setText("annulation");
              T316.setName("T316");
              panel2.add(T316);
              T316.setBounds(220, 94, 95, T316.getPreferredSize().height);

              //---- T117 ----
              T117.setText("cr\u00e9ation");
              T117.setName("T117");
              panel2.add(T117);
              T117.setBounds(60, 131, 80, T117.getPreferredSize().height);

              //---- T217 ----
              T217.setText("modification");
              T217.setName("T217");
              panel2.add(T217);
              T217.setBounds(130, 131, 100, T217.getPreferredSize().height);

              //---- T317 ----
              T317.setText("annulation");
              T317.setName("T317");
              panel2.add(T317);
              T317.setBounds(220, 131, 95, T317.getPreferredSize().height);

              //---- T118 ----
              T118.setText("cr\u00e9ation");
              T118.setName("T118");
              panel2.add(T118);
              T118.setBounds(60, 168, 80, T118.getPreferredSize().height);

              //---- T218 ----
              T218.setText("modification");
              T218.setName("T218");
              panel2.add(T218);
              T218.setBounds(130, 168, 100, T218.getPreferredSize().height);

              //---- T318 ----
              T318.setText("annulation");
              T318.setName("T318");
              panel2.add(T318);
              T318.setBounds(220, 168, 95, T318.getPreferredSize().height);

              //---- T119 ----
              T119.setText("cr\u00e9ation");
              T119.setName("T119");
              panel2.add(T119);
              T119.setBounds(60, 205, 80, T119.getPreferredSize().height);

              //---- T219 ----
              T219.setText("modification");
              T219.setName("T219");
              panel2.add(T219);
              T219.setBounds(130, 205, 100, T219.getPreferredSize().height);

              //---- T319 ----
              T319.setText("annulation");
              T319.setName("T319");
              panel2.add(T319);
              T319.setBounds(220, 205, 95, T319.getPreferredSize().height);

              //---- T120 ----
              T120.setText("cr\u00e9ation");
              T120.setName("T120");
              panel2.add(T120);
              T120.setBounds(60, 242, 80, T120.getPreferredSize().height);

              //---- T220 ----
              T220.setText("modification");
              T220.setName("T220");
              panel2.add(T220);
              T220.setBounds(130, 242, 100, T220.getPreferredSize().height);

              //---- T320 ----
              T320.setText("annulation");
              T320.setName("T320");
              panel2.add(T320);
              T320.setBounds(220, 242, 95, T320.getPreferredSize().height);

              //---- T121 ----
              T121.setText("cr\u00e9ation");
              T121.setName("T121");
              panel2.add(T121);
              T121.setBounds(60, 279, 80, T121.getPreferredSize().height);

              //---- T221 ----
              T221.setText("modification");
              T221.setName("T221");
              panel2.add(T221);
              T221.setBounds(130, 279, 100, T221.getPreferredSize().height);

              //---- T321 ----
              T321.setText("annulation");
              T321.setName("T321");
              panel2.add(T321);
              T321.setBounds(220, 279, 95, T321.getPreferredSize().height);

              //---- T122 ----
              T122.setText("cr\u00e9ation");
              T122.setName("T122");
              panel2.add(T122);
              T122.setBounds(60, 316, 80, T122.getPreferredSize().height);

              //---- T222 ----
              T222.setText("modification");
              T222.setName("T222");
              panel2.add(T222);
              T222.setBounds(130, 316, 100, T222.getPreferredSize().height);

              //---- T322 ----
              T322.setText("annulation");
              T322.setName("T322");
              panel2.add(T322);
              T322.setBounds(220, 316, 95, T322.getPreferredSize().height);

              //---- T123 ----
              T123.setText("cr\u00e9ation");
              T123.setName("T123");
              panel2.add(T123);
              T123.setBounds(60, 353, 80, T123.getPreferredSize().height);

              //---- T223 ----
              T223.setText("modification");
              T223.setName("T223");
              panel2.add(T223);
              T223.setBounds(130, 353, 100, T223.getPreferredSize().height);

              //---- T323 ----
              T323.setText("annulation");
              T323.setName("T323");
              panel2.add(T323);
              T323.setBounds(220, 353, 95, T323.getPreferredSize().height);

              //---- T124 ----
              T124.setText("cr\u00e9ation");
              T124.setName("T124");
              panel2.add(T124);
              T124.setBounds(60, 390, 80, T124.getPreferredSize().height);

              //---- T224 ----
              T224.setText("modification");
              T224.setName("T224");
              panel2.add(T224);
              T224.setBounds(130, 390, 100, T224.getPreferredSize().height);

              //---- T324 ----
              T324.setText("annulation");
              T324.setName("T324");
              panel2.add(T324);
              T324.setBounds(220, 390, 95, T324.getPreferredSize().height);

              //---- T125 ----
              T125.setText("cr\u00e9ation");
              T125.setName("T125");
              panel2.add(T125);
              T125.setBounds(60, 427, 80, T125.getPreferredSize().height);

              //---- T225 ----
              T225.setText("modification");
              T225.setName("T225");
              panel2.add(T225);
              T225.setBounds(130, 427, 100, T225.getPreferredSize().height);

              //---- T325 ----
              T325.setText("annulation");
              T325.setName("T325");
              panel2.add(T325);
              T325.setBounds(220, 427, 95, T325.getPreferredSize().height);

              //---- T414 ----
              T414.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T414.setName("T414");
              panel2.add(T414);
              T414.setBounds(305, 16, 165, T414.getPreferredSize().height);

              //---- T415 ----
              T415.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T415.setName("T415");
              panel2.add(T415);
              T415.setBounds(305, 53, 165, T415.getPreferredSize().height);

              //---- T416 ----
              T416.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T416.setName("T416");
              panel2.add(T416);
              T416.setBounds(305, 90, 165, T416.getPreferredSize().height);

              //---- T417 ----
              T417.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T417.setName("T417");
              panel2.add(T417);
              T417.setBounds(305, 127, 165, T417.getPreferredSize().height);

              //---- T418 ----
              T418.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T418.setName("T418");
              panel2.add(T418);
              T418.setBounds(305, 164, 165, T418.getPreferredSize().height);

              //---- T419 ----
              T419.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T419.setName("T419");
              panel2.add(T419);
              T419.setBounds(305, 201, 165, T419.getPreferredSize().height);

              //---- T420 ----
              T420.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T420.setName("T420");
              panel2.add(T420);
              T420.setBounds(305, 238, 165, T420.getPreferredSize().height);

              //---- T421 ----
              T421.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T421.setName("T421");
              panel2.add(T421);
              T421.setBounds(305, 275, 165, T421.getPreferredSize().height);

              //---- T422 ----
              T422.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T422.setName("T422");
              panel2.add(T422);
              T422.setBounds(305, 312, 165, T422.getPreferredSize().height);

              //---- T423 ----
              T423.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T423.setName("T423");
              panel2.add(T423);
              T423.setBounds(305, 349, 165, T423.getPreferredSize().height);

              //---- T424 ----
              T424.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T424.setName("T424");
              panel2.add(T424);
              T424.setBounds(305, 386, 165, T424.getPreferredSize().height);

              //---- T425 ----
              T425.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de sp\u00e9cificit\u00e9",
                "Article actif non stock\u00e9",
                "Article d\u00e9sactiv\u00e9"
              }));
              T425.setName("T425");
              panel2.add(T425);
              T425.setBounds(305, 423, 165, T425.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel5ContentContainer.add(panel2);
            panel2.setBounds(495, 5, 475, 500);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel5ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel5ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel5ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel5ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel5ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel5, GroupLayout.DEFAULT_SIZE, 974, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 552, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_47;
  private XRiTextField INDTYP;
  private JLabel OBJ_49;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel5;
  private JPanel panel1;
  private JTextField ETB01;
  private JTextField ETB02;
  private JTextField ETB03;
  private JTextField ETB04;
  private JTextField ETB05;
  private JTextField ETB06;
  private JTextField ETB07;
  private JTextField ETB08;
  private JTextField ETB09;
  private JTextField ETB10;
  private JTextField ETB11;
  private JTextField ETB12;
  private JTextField ETB13;
  private JCheckBox T101;
  private JCheckBox T201;
  private JCheckBox T301;
  private JCheckBox T102;
  private JCheckBox T202;
  private JCheckBox T302;
  private JCheckBox T103;
  private JCheckBox T203;
  private JCheckBox T303;
  private JCheckBox T104;
  private JCheckBox T204;
  private JCheckBox T304;
  private JCheckBox T105;
  private JCheckBox T205;
  private JCheckBox T305;
  private JCheckBox T106;
  private JCheckBox T206;
  private JCheckBox T306;
  private JCheckBox T107;
  private JCheckBox T207;
  private JCheckBox T307;
  private JCheckBox T108;
  private JCheckBox T208;
  private JCheckBox T308;
  private JCheckBox T109;
  private JCheckBox T209;
  private JCheckBox T309;
  private JCheckBox T110;
  private JCheckBox T210;
  private JCheckBox T310;
  private JCheckBox T111;
  private JCheckBox T211;
  private JCheckBox T311;
  private JCheckBox T112;
  private JCheckBox T212;
  private JCheckBox T312;
  private JCheckBox T113;
  private JCheckBox T213;
  private JCheckBox T313;
  private XRiComboBox T401;
  private XRiComboBox T402;
  private XRiComboBox T403;
  private XRiComboBox T404;
  private XRiComboBox T405;
  private XRiComboBox T406;
  private XRiComboBox T407;
  private XRiComboBox T408;
  private XRiComboBox T409;
  private XRiComboBox T410;
  private XRiComboBox T411;
  private XRiComboBox T412;
  private XRiComboBox T413;
  private JPanel panel2;
  private JTextField ETB14;
  private JTextField ETB15;
  private JTextField ETB16;
  private JTextField ETB17;
  private JTextField ETB18;
  private JTextField ETB19;
  private JTextField ETB20;
  private JTextField ETB21;
  private JTextField ETB22;
  private JTextField ETB23;
  private JTextField ETB24;
  private JTextField ETB25;
  private XRiCheckBox T114;
  private XRiCheckBox T214;
  private XRiCheckBox T314;
  private XRiCheckBox T115;
  private XRiCheckBox T215;
  private XRiCheckBox T315;
  private XRiCheckBox T116;
  private XRiCheckBox T216;
  private XRiCheckBox T316;
  private XRiCheckBox T117;
  private XRiCheckBox T217;
  private XRiCheckBox T317;
  private XRiCheckBox T118;
  private XRiCheckBox T218;
  private XRiCheckBox T318;
  private XRiCheckBox T119;
  private XRiCheckBox T219;
  private XRiCheckBox T319;
  private XRiCheckBox T120;
  private XRiCheckBox T220;
  private XRiCheckBox T320;
  private XRiCheckBox T121;
  private XRiCheckBox T221;
  private XRiCheckBox T321;
  private XRiCheckBox T122;
  private XRiCheckBox T222;
  private XRiCheckBox T322;
  private XRiCheckBox T123;
  private XRiCheckBox T223;
  private XRiCheckBox T323;
  private XRiCheckBox T124;
  private XRiCheckBox T224;
  private XRiCheckBox T324;
  private XRiCheckBox T125;
  private XRiCheckBox T225;
  private XRiCheckBox T325;
  private XRiComboBox T414;
  private XRiComboBox T415;
  private XRiComboBox T416;
  private XRiComboBox T417;
  private XRiComboBox T418;
  private XRiComboBox T419;
  private XRiComboBox T420;
  private XRiComboBox T421;
  private XRiComboBox T422;
  private XRiComboBox T423;
  private XRiComboBox T424;
  private XRiComboBox T425;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
