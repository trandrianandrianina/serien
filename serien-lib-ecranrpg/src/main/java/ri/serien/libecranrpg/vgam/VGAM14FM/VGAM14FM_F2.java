
package ri.serien.libecranrpg.vgam.VGAM14FM;
// Nom Fichier: pop_VGAM14FM_FMTF2_FMTF1_20.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VGAM14FM_F2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM14FM_F2(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    // setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GNRA@")).trim());
    OBJ_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ORDR@")).trim());
    OBJ_9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GNRA@")).trim());
    OBJ_10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BONS@")).trim());
    OBJ_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ORDR2@")).trim());
    OBJ_11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BON@")).trim());
    OBJ_14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W57BD@")).trim());
    OBJ_18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SIMU@")).trim());
    OBJ_15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XPARBD@")).trim());
    OBJ_17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XPARBF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_13.setVisible(!lexique.HostFieldGetData("W57BD").trim().equalsIgnoreCase("000000") & lexique.isPresent("W57BD"));
    OBJ_17.setVisible(lexique.isPresent("XPARBF"));
    OBJ_15.setVisible(lexique.isPresent("XPARBD"));
    OBJ_14.setVisible(!lexique.HostFieldGetData("W57BD").trim().equalsIgnoreCase("000000") & lexique.isPresent("W57BD"));
    OBJ_23.setVisible(lexique.HostFieldGetData("BON").equalsIgnoreCase("Ordre de Fabrication généré"));
    OBJ_19.setVisible(!lexique.HostFieldGetData("BON").trim().equalsIgnoreCase("Ordre de Fabrication généré"));
    OBJ_12.setVisible(lexique.HostFieldGetData("W57BD").equalsIgnoreCase("000000"));
    OBJ_11.setVisible(!lexique.HostFieldGetData("BON").trim().equalsIgnoreCase(""));
    OBJ_8.setVisible(!lexique.HostFieldGetData("W57BD").trim().equalsIgnoreCase("000000"));
    OBJ_10.setVisible(!lexique.HostFieldGetData("BONS").trim().equalsIgnoreCase(""));
    OBJ_9.setVisible(!lexique.HostFieldGetData("GNRA").trim().equalsIgnoreCase(""));
    OBJ_7.setVisible(!lexique.HostFieldGetData("ORDR").trim().equalsIgnoreCase(""));
    
    // TODO Icones
    OBJ_23.setIcon(lexique.chargerImage("images/vue21.gif", true));
    OBJ_19.setIcon(lexique.chargerImage("images/vue31.gif", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Réapprovisionnement"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_Touche")
    // lexique.HostCursorPut(20.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_Touche")
    // lexique.HostCursorPut(20.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    OBJ_6 = new JLabel();
    OBJ_7 = new JLabel();
    OBJ_9 = new JLabel();
    OBJ_10 = new JLabel();
    OBJ_8 = new JLabel();
    OBJ_11 = new JLabel();
    OBJ_12 = new JLabel();
    OBJ_13 = new JLabel();
    OBJ_14 = new JLabel();
    OBJ_18 = new JLabel();
    OBJ_15 = new JLabel();
    OBJ_17 = new JLabel();
    OBJ_16 = new JLabel();
    panel2 = new JPanel();
    OBJ_23 = new JButton();
    OBJ_19 = new JButton();

    //======== this ========
    setName("this");
    setLayout(new BorderLayout());

    //======== panel1 ========
    {
      panel1.setBorder(new TitledBorder(""));
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- OBJ_6 ----
      OBJ_6.setText("@GNRA@");
      OBJ_6.setName("OBJ_6");
      panel1.add(OBJ_6);
      OBJ_6.setBounds(25, 25, 463, 24);

      //---- OBJ_7 ----
      OBJ_7.setText("@ORDR@");
      OBJ_7.setName("OBJ_7");
      panel1.add(OBJ_7);
      OBJ_7.setBounds(25, 25, 463, 24);

      //---- OBJ_9 ----
      OBJ_9.setText("@GNRA@");
      OBJ_9.setName("OBJ_9");
      panel1.add(OBJ_9);
      OBJ_9.setBounds(25, 25, 463, 24);

      //---- OBJ_10 ----
      OBJ_10.setText("@BONS@");
      OBJ_10.setName("OBJ_10");
      panel1.add(OBJ_10);
      OBJ_10.setBounds(25, 25, 442, 24);

      //---- OBJ_8 ----
      OBJ_8.setText("@ORDR2@");
      OBJ_8.setName("OBJ_8");
      panel1.add(OBJ_8);
      OBJ_8.setBounds(25, 25, 348, 24);

      //---- OBJ_11 ----
      OBJ_11.setText("@BON@");
      OBJ_11.setName("OBJ_11");
      panel1.add(OBJ_11);
      OBJ_11.setBounds(25, 25, 277, 24);

      //---- OBJ_12 ----
      OBJ_12.setText("Aucune g\u00e9n\u00e9ration");
      OBJ_12.setName("OBJ_12");
      panel1.add(OBJ_12);
      OBJ_12.setBounds(290, 25, 192, 25);

      //---- OBJ_13 ----
      OBJ_13.setText("N\u00b0");
      OBJ_13.setName("OBJ_13");
      panel1.add(OBJ_13);
      OBJ_13.setBounds(525, 25, 21, 24);

      //---- OBJ_14 ----
      OBJ_14.setText("@W57BD@");
      OBJ_14.setName("OBJ_14");
      panel1.add(OBJ_14);
      OBJ_14.setBounds(550, 25, 57, 24);

      //---- OBJ_18 ----
      OBJ_18.setText("@SIMU@");
      OBJ_18.setName("OBJ_18");
      panel1.add(OBJ_18);
      OBJ_18.setBounds(440, 60, 91, 24);

      //---- OBJ_15 ----
      OBJ_15.setText("@XPARBD@");
      OBJ_15.setName("OBJ_15");
      panel1.add(OBJ_15);
      OBJ_15.setBounds(25, 60, 57, 24);

      //---- OBJ_17 ----
      OBJ_17.setText("@XPARBF@");
      OBJ_17.setName("OBJ_17");
      panel1.add(OBJ_17);
      OBJ_17.setBounds(115, 60, 57, 24);

      //---- OBJ_16 ----
      OBJ_16.setText("\u00e0");
      OBJ_16.setName("OBJ_16");
      panel1.add(OBJ_16);
      OBJ_16.setBounds(100, 60, 13, 24);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel1.getComponentCount(); i++) {
          Rectangle bounds = panel1.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel1.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel1.setMinimumSize(preferredSize);
        panel1.setPreferredSize(preferredSize);
      }
    }
    add(panel1, BorderLayout.CENTER);

    //======== panel2 ========
    {
      panel2.setName("panel2");
      panel2.setLayout(new VerticalLayout());

      //---- OBJ_23 ----
      OBJ_23.setText("");
      OBJ_23.setToolTipText("Affichage bon d'achat g\u00e9n\u00e9r\u00e9");
      OBJ_23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_23.setPreferredSize(new Dimension(40, 40));
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      panel2.add(OBJ_23);
    }
    add(panel2, BorderLayout.EAST);

    //---- OBJ_19 ----
    OBJ_19.setText("");
    OBJ_19.setToolTipText("Affichage ordre de fabricatiob g\u00e9n\u00e9r\u00e9");
    OBJ_19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_19.setPreferredSize(new Dimension(40, 40));
    OBJ_19.setName("OBJ_19");
    OBJ_19.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_19ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel1;
  private JLabel OBJ_6;
  private JLabel OBJ_7;
  private JLabel OBJ_9;
  private JLabel OBJ_10;
  private JLabel OBJ_8;
  private JLabel OBJ_11;
  private JLabel OBJ_12;
  private JLabel OBJ_13;
  private JLabel OBJ_14;
  private JLabel OBJ_18;
  private JLabel OBJ_15;
  private JLabel OBJ_17;
  private JLabel OBJ_16;
  private JPanel panel2;
  private JButton OBJ_23;
  private JButton OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
