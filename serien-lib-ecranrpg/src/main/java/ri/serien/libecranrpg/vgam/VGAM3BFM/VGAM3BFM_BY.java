/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.vgam.VGAM3BFM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * Boîte de dialogue pour saisir les paramètres de créations d'articles suite à l'import d'un catalogue fournisseur.
 */
public class VGAM3BFM_BY extends SNPanelEcranRPG implements ioFrame {
  
  private static final String[] NUMAUT_VALUE = { "", "1", "4", "2", "3" };
  
  public VGAM3BFM_BY(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    setDialog(true);
    
    // Renseigner le titre de la boîte de dialogue
    setTitle("Importer des articles en création");
    
    // Configurer les valeurs des champs
    NUMAUT.setValeurs(NUMAUT_VALUE, null);
    REPTAR.setValeursSelection("OUI", "NON");
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Afficher les erreurs dans une boîte de dialogue
    gererLesErreurs("19");
    
    // Message d'erreur
    if (!lexique.HostFieldGetData("MESERR").trim().isEmpty()) {
      MESERR.setMessage(Message.getMessageImportant(lexique.HostFieldGetData("MESERR")));
    }
    MESERR.setVisible(lexique.isTrue("19") || !lexique.HostFieldGetData("MESERR").trim().isEmpty());
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  // Méthode évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void riBoutonDetailListe1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("ARTREF");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void REPTARMouseReleased(MouseEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlIntegration = new SNPanelTitre();
    lbPrefixe = new SNLabelChamp();
    PREFIX = new XRiTextField();
    lbArticle = new SNLabelChamp();
    ARTREF = new XRiTextField();
    lbNumerotation = new SNLabelChamp();
    NUMAUT = new XRiComboBox();
    REPTAR = new XRiCheckBox();
    MESERR = new SNMessage();
    
    // ======== this ========
    setMinimumSize(new Dimension(660, 330));
    setPreferredSize(new Dimension(660, 330));
    setMaximumSize(new Dimension(660, 330));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
      
      // ======== pnlIntegration ========
      {
        pnlIntegration.setTitre("Importation catalogue");
        pnlIntegration.setName("pnlIntegration");
        pnlIntegration.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlIntegration.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlIntegration.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlIntegration.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlIntegration.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbPrefixe ----
        lbPrefixe.setText("Pr\u00e9fixe");
        lbPrefixe.setName("lbPrefixe");
        pnlIntegration.add(lbPrefixe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- PREFIX ----
        PREFIX.setPreferredSize(new Dimension(60, 30));
        PREFIX.setMinimumSize(new Dimension(60, 30));
        PREFIX.setMaximumSize(new Dimension(60, 30));
        PREFIX.setFont(new Font("sansserif", Font.PLAIN, 14));
        PREFIX.setName("PREFIX");
        pnlIntegration.add(PREFIX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbArticle ----
        lbArticle.setText("Article de r\u00e9f\u00e9rence");
        lbArticle.setName("lbArticle");
        pnlIntegration.add(lbArticle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- ARTREF ----
        ARTREF.setPreferredSize(new Dimension(300, 30));
        ARTREF.setMinimumSize(new Dimension(300, 30));
        ARTREF.setMaximumSize(new Dimension(300, 30));
        ARTREF.setFont(new Font("sansserif", Font.PLAIN, 14));
        ARTREF.setName("ARTREF");
        pnlIntegration.add(ARTREF, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNumerotation ----
        lbNumerotation.setText("Num\u00e9rotation automatique");
        lbNumerotation.setMinimumSize(new Dimension(200, 30));
        lbNumerotation.setMaximumSize(new Dimension(200, 30));
        lbNumerotation.setPreferredSize(new Dimension(200, 30));
        lbNumerotation.setName("lbNumerotation");
        pnlIntegration.add(lbNumerotation, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- NUMAUT ----
        NUMAUT.setMinimumSize(new Dimension(250, 30));
        NUMAUT.setMaximumSize(new Dimension(250, 30));
        NUMAUT.setPreferredSize(new Dimension(250, 30));
        NUMAUT.setFont(new Font("sansserif", Font.PLAIN, 14));
        NUMAUT.setModel(new DefaultComboBoxModel(
            new String[] { " ", "Num\u00e9ro fournisseur", "Code groupe", "Code famille", "Code sous-famille" }));
        NUMAUT.setName("NUMAUT");
        pnlIntegration.add(NUMAUT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- REPTAR ----
        REPTAR.setText("Mise \u00e0 jour des tarifs de vente");
        REPTAR.setFont(new Font("sansserif", Font.PLAIN, 14));
        REPTAR.setPreferredSize(new Dimension(200, 30));
        REPTAR.setMinimumSize(new Dimension(200, 30));
        REPTAR.setMaximumSize(new Dimension(200, 30));
        REPTAR.setName("REPTAR");
        pnlIntegration.add(REPTAR, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- MESERR ----
        MESERR.setText("MESERR");
        MESERR.setName("MESERR");
        pnlIntegration.add(MESERR, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlIntegration,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlIntegration;
  private SNLabelChamp lbPrefixe;
  private XRiTextField PREFIX;
  private SNLabelChamp lbArticle;
  private XRiTextField ARTREF;
  private SNLabelChamp lbNumerotation;
  private XRiComboBox NUMAUT;
  private XRiCheckBox REPTAR;
  private SNMessage MESERR;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
