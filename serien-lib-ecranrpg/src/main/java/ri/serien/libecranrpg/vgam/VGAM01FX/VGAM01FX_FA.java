
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_FA extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LISTCV_Top=null;
  private String[] _FAA1_Title = { "Plan comptable", "Co", "March. Fr.Gén Immob. Libellé", };
  private String[][] _FAA1_Data = { { "N° CO achats H.T. 1", "FAA1", "LCG01", }, { "N° CO achats H.T. 2", "FAA2", "LCG02", },
      { "N° CO achats H.T. 3", "FAA3", "LCG03", }, { "N° CO achats H.T. 4", "FAA4", "LCG04", }, { "N° CO achats H.T. 5", "FAA5", "LCG05", },
      { "N° CO achats H.T. 6", "FAA6", "LCG06", }, { "N° CO achats H.T. 7", "FAA7", "LCG07", }, { "N° CO achats H.T. 8", "FAA8", "LCG08", },
      { "N° CO achats H.T. 9", "FAA9", "LCG09", }, };
  private int[] _FAA1_Width = { 175, 15, 250, };
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.LEFT };
  private String[] FAGSP_Value = { "", "1", "2", "3", "4", };
  
  public VGAM01FX_FA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    FAGSP.setValeurs(FAGSP_Value, null);
    FANATM.setValeursSelection("M", " ");
    FANATI.setValeursSelection("I", " ");
    FANATF.setValeursSelection("F", " ");
    FASPE.setValeursSelection("1", " ");
    FADEB.setValeursSelection("1", " ");
    FAQEC.setValeursSelection("1", " ");
    FAA1.setAspectTable(null, _FAA1_Title, _FAA1_Data, _FAA1_Width, true, _LIST_Justification, null, null, null);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LISTCV, LISTCV.get_LIST_Title_Data_Brut(), _LISTCV_Top, _LIST_Justification);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    FAFRC.setEnabled(lexique.isPresent("FAFRC"));
    FATVA.setEnabled(lexique.isPresent("FATVA"));
    FAMEX.setEnabled(lexique.isPresent("FAMEX"));
    FAMAG.setEnabled(lexique.isPresent("FAMAG"));
    FAAGEM.setEnabled(lexique.isPresent("FAAGEM"));
    FAFPR.setEnabled(lexique.isPresent("FAFPR"));
    FAOPA.setEnabled(lexique.isPresent("FAOPA"));
    FASETO.setEnabled(lexique.isPresent("FASETO"));
    FASAN.setEnabled(lexique.isPresent("FASAN"));
    FAFRS.setEnabled(lexique.isPresent("FAFRS"));
    // FASPE.setSelected(lexique.HostFieldGetData("FASPE").equalsIgnoreCase("1"));
    FANDP.setEnabled(lexique.isPresent("FANDP"));
    // FANATM.setSelected(lexique.HostFieldGetData("FANATM").equalsIgnoreCase("M"));
    // FANATI.setSelected(lexique.HostFieldGetData("FANATI").equalsIgnoreCase("I"));
    // FANATF.setSelected(lexique.HostFieldGetData("FANATF").equalsIgnoreCase("F"));
    OBJ_64.setVisible(lexique.isPresent("FASETO"));
    // FADEB.setSelected(lexique.HostFieldGetData("FADEB").equalsIgnoreCase("1"));
    // FAGSP.setVisible( lexique.isPresent("FAGSP"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (FASPE.isSelected())
    // lexique.HostFieldPutData("FASPE", 0, "1");
    // else
    // lexique.HostFieldPutData("FASPE", 0, " ");
    // if (FANATM.isSelected())
    // lexique.HostFieldPutData("FANATM", 0, "M");
    // else
    // lexique.HostFieldPutData("FANATM", 0, " ");
    // if (FANATI.isSelected())
    // lexique.HostFieldPutData("FANATI", 0, "I");
    // else
    // lexique.HostFieldPutData("FANATI", 0, " ");
    // if (FANATF.isSelected())
    // lexique.HostFieldPutData("FANATF", 0, "F");
    // else
    // lexique.HostFieldPutData("FANATF", 0, " ");
    // if (FADEB.isSelected())
    // lexique.HostFieldPutData("FADEB", 0, "1");
    // else
    // lexique.HostFieldPutData("FADEB", 0, " ");
    // lexique.HostFieldPutData("FAGSP", 0, FAGSP_Value[FAGSP.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_49 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel1 = new JPanel();
    FALIB = new XRiTextField();
    FADEB = new XRiCheckBox();
    OBJ_50 = new JLabel();
    OBJ_52 = new JLabel();
    FASPE = new XRiCheckBox();
    OBJ_54 = new JLabel();
    FAMAG = new XRiTextField();
    FATVA = new XRiTextField();
    FAGSP = new XRiComboBox();
    OBJ_67 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_66 = new JLabel();
    FANDP = new XRiTextField();
    OBJ_60 = new JLabel();
    FAFRS = new XRiTextField();
    FACPR = new XRiTextField();
    FASETO = new XRiTextField();
    FAFRC = new XRiTextField();
    OBJ_84 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_82 = new JLabel();
    FANATF = new XRiCheckBox();
    FANATI = new XRiCheckBox();
    FANATM = new XRiCheckBox();
    OBJ_73 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_86 = new JLabel();
    FAOPA = new XRiTextField();
    FAFPR = new XRiTextField();
    FAAGEM = new XRiTextField();
    FAUNS = new XRiTextField();
    OBJ_98 = new JLabel();
    FASAN = new XRiTextField();
    OBJ_100 = new JLabel();
    FAMEX = new XRiTextField();
    FAQEC = new XRiCheckBox();
    panel2 = new JPanel();
    SCROLLPANE_LISTCV = new JScrollPane();
    FAA1 = new XRiTable();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_49 ----
          OBJ_49.setText("Ordre");
          OBJ_49.setName("OBJ_49");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 610));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Famille d'articles");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder(""));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- FALIB ----
              FALIB.setComponentPopupMenu(BTD);
              FALIB.setName("FALIB");
              panel1.add(FALIB);
              FALIB.setBounds(210, 16, 310, FALIB.getPreferredSize().height);

              //---- FADEB ----
              FADEB.setText("Pas d'unit\u00e9 suppl\u00e9mentaire sur DEB");
              FADEB.setToolTipText("Pas d'unit\u00e9 suppl\u00e9mentaire sur DEB");
              FADEB.setComponentPopupMenu(BTD);
              FADEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FADEB.setName("FADEB");
              panel1.add(FADEB);
              FADEB.setBounds(577, 51, 240, 20);

              //---- OBJ_50 ----
              OBJ_50.setText("Libell\u00e9 famille");
              OBJ_50.setName("OBJ_50");
              panel1.add(OBJ_50);
              OBJ_50.setBounds(40, 20, 99, 20);

              //---- OBJ_52 ----
              OBJ_52.setText("Code magasin");
              OBJ_52.setName("OBJ_52");
              panel1.add(OBJ_52);
              OBJ_52.setBounds(40, 51, 91, 20);

              //---- FASPE ----
              FASPE.setText("Sp\u00e9cial");
              FASPE.setComponentPopupMenu(BTD);
              FASPE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FASPE.setName("FASPE");
              panel1.add(FASPE);
              FASPE.setBounds(577, 20, 72, 20);

              //---- OBJ_54 ----
              OBJ_54.setText("Code TVA");
              OBJ_54.setName("OBJ_54");
              panel1.add(OBJ_54);
              OBJ_54.setBounds(315, 51, 140, 20);

              //---- FAMAG ----
              FAMAG.setComponentPopupMenu(BTD);
              FAMAG.setName("FAMAG");
              panel1.add(FAMAG);
              FAMAG.setBounds(210, 47, 30, FAMAG.getPreferredSize().height);

              //---- FATVA ----
              FATVA.setComponentPopupMenu(BTD);
              FATVA.setName("FATVA");
              panel1.add(FATVA);
              FATVA.setBounds(456, 47, 20, FATVA.getPreferredSize().height);

              //---- FAGSP ----
              FAGSP.setModel(new DefaultComboBoxModel(new String[] {
                " ",
                "sur la fiche article",
                "sur la fiche article + les lignes de ventes",
                "sur la fiche article + les lignes d'achats",
                "sur les lignes de ventes + les lignes d'achats"
              }));
              FAGSP.setComponentPopupMenu(BTD);
              FAGSP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FAGSP.setName("FAGSP");
              panel1.add(FAGSP);
              FAGSP.setBounds(456, 81, 359, FAGSP.getPreferredSize().height);

              //---- OBJ_67 ----
              OBJ_67.setText("Coefficient de prix de revient");
              OBJ_67.setName("OBJ_67");
              panel1.add(OBJ_67);
              OBJ_67.setBounds(40, 116, 171, 20);

              //---- OBJ_69 ----
              OBJ_69.setText("Nomenclature douani\u00e8re");
              OBJ_69.setName("OBJ_69");
              panel1.add(OBJ_69);
              OBJ_69.setBounds(315, 116, 140, 20);

              //---- OBJ_64 ----
              OBJ_64.setText("Seuil tol\u00e9rance (Kg)");
              OBJ_64.setName("OBJ_64");
              panel1.add(OBJ_64);
              OBJ_64.setBounds(577, 244, 118, 20);

              //---- OBJ_66 ----
              OBJ_66.setText("Traitement sp\u00e9cifique");
              OBJ_66.setName("OBJ_66");
              panel1.add(OBJ_66);
              OBJ_66.setBounds(315, 84, 140, 20);

              //---- FANDP ----
              FANDP.setComponentPopupMenu(BTD);
              FANDP.setName("FANDP");
              panel1.add(FANDP);
              FANDP.setBounds(456, 112, 140, FANDP.getPreferredSize().height);

              //---- OBJ_60 ----
              OBJ_60.setText("Fournisseur");
              OBJ_60.setName("OBJ_60");
              panel1.add(OBJ_60);
              OBJ_60.setBounds(40, 84, 73, 20);

              //---- FAFRS ----
              FAFRS.setComponentPopupMenu(BTD);
              FAFRS.setName("FAFRS");
              panel1.add(FAFRS);
              FAFRS.setBounds(235, 80, 60, FAFRS.getPreferredSize().height);

              //---- FACPR ----
              FACPR.setComponentPopupMenu(BTD);
              FACPR.setName("FACPR");
              panel1.add(FACPR);
              FACPR.setBounds(210, 112, 58, FACPR.getPreferredSize().height);

              //---- FASETO ----
              FASETO.setComponentPopupMenu(BTD);
              FASETO.setName("FASETO");
              panel1.add(FASETO);
              FASETO.setBounds(705, 240, 34, FASETO.getPreferredSize().height);

              //---- FAFRC ----
              FAFRC.setComponentPopupMenu(BTD);
              FAFRC.setName("FAFRC");
              panel1.add(FAFRC);
              FAFRC.setBounds(210, 80, 20, FAFRC.getPreferredSize().height);

              //---- OBJ_84 ----
              OBJ_84.setText("D\u00e9termination sur-stock, age maxi");
              OBJ_84.setName("OBJ_84");
              panel1.add(OBJ_84);
              OBJ_84.setBounds(456, 212, 225, 20);

              //---- OBJ_71 ----
              OBJ_71.setText("Formule calcul PRV");
              OBJ_71.setName("OBJ_71");
              panel1.add(OBJ_71);
              OBJ_71.setBounds(40, 148, 139, 20);

              //---- OBJ_82 ----
              OBJ_82.setText("Code unit\u00e9 de stock");
              OBJ_82.setName("OBJ_82");
              panel1.add(OBJ_82);
              OBJ_82.setBounds(40, 212, 121, 20);

              //---- FANATF ----
              FANATF.setText("Frais g\u00e9n\u00e9raux");
              FANATF.setComponentPopupMenu(BTD);
              FANATF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FANATF.setName("FANATF");
              panel1.add(FANATF);
              FANATF.setBounds(456, 180, 114, 20);

              //---- FANATI ----
              FANATI.setText("Immobilisations");
              FANATI.setComponentPopupMenu(BTD);
              FANATI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FANATI.setName("FANATI");
              panel1.add(FANATI);
              FANATI.setBounds(705, 180, 112, 20);

              //---- FANATM ----
              FANATM.setText("Marchandises");
              FANATM.setComponentPopupMenu(BTD);
              FANATM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FANATM.setName("FANATM");
              panel1.add(FANATM);
              FANATM.setBounds(210, 180, 111, 20);

              //---- OBJ_73 ----
              OBJ_73.setText("Pays d'origine");
              OBJ_73.setName("OBJ_73");
              panel1.add(OBJ_73);
              OBJ_73.setBounds(315, 148, 140, 20);

              //---- OBJ_78 ----
              OBJ_78.setText("Nature d'achat");
              OBJ_78.setName("OBJ_78");
              panel1.add(OBJ_78);
              OBJ_78.setBounds(40, 180, 96, 20);

              //---- OBJ_86 ----
              OBJ_86.setText("Jours");
              OBJ_86.setName("OBJ_86");
              panel1.add(OBJ_86);
              OBJ_86.setBounds(745, 212, 57, 20);

              //---- FAOPA ----
              FAOPA.setComponentPopupMenu(BTD);
              FAOPA.setName("FAOPA");
              panel1.add(FAOPA);
              FAOPA.setBounds(456, 144, 30, FAOPA.getPreferredSize().height);

              //---- FAFPR ----
              FAFPR.setComponentPopupMenu(BTD);
              FAFPR.setName("FAFPR");
              panel1.add(FAFPR);
              FAFPR.setBounds(210, 144, 40, FAFPR.getPreferredSize().height);

              //---- FAAGEM ----
              FAAGEM.setComponentPopupMenu(BTD);
              FAAGEM.setName("FAAGEM");
              panel1.add(FAAGEM);
              FAAGEM.setBounds(705, 208, 34, FAAGEM.getPreferredSize().height);

              //---- FAUNS ----
              FAUNS.setComponentPopupMenu(BTD);
              FAUNS.setName("FAUNS");
              panel1.add(FAUNS);
              FAUNS.setBounds(210, 208, 30, FAUNS.getPreferredSize().height);

              //---- OBJ_98 ----
              OBJ_98.setText("Code analytique");
              OBJ_98.setName("OBJ_98");
              panel1.add(OBJ_98);
              OBJ_98.setBounds(40, 244, 102, 20);

              //---- FASAN ----
              FASAN.setComponentPopupMenu(BTD);
              FASAN.setName("FASAN");
              panel1.add(FASAN);
              FASAN.setBounds(210, 240, 50, FASAN.getPreferredSize().height);

              //---- OBJ_100 ----
              OBJ_100.setText("Mode exp\u00e9dition normal");
              OBJ_100.setName("OBJ_100");
              panel1.add(OBJ_100);
              OBJ_100.setBounds(315, 244, 147, 20);

              //---- FAMEX ----
              FAMEX.setComponentPopupMenu(BTD);
              FAMEX.setName("FAMEX");
              panel1.add(FAMEX);
              FAMEX.setBounds(456, 240, 30, FAMEX.getPreferredSize().height);

              //---- FAQEC ----
              FAQEC.setText("Quantit\u00e9 \u00e9conomique non utilis\u00e9e en r\u00e9appro");
              FAQEC.setName("FAQEC");
              panel1.add(FAQEC);
              FAQEC.setBounds(40, 275, 295, FAQEC.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(15, 5, 840, 310);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Comptabilisation des achats"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //======== SCROLLPANE_LISTCV ========
              {
                SCROLLPANE_LISTCV.setComponentPopupMenu(BTD);
                SCROLLPANE_LISTCV.setName("SCROLLPANE_LISTCV");

                //---- FAA1 ----
                FAA1.setComponentPopupMenu(BTD);
                FAA1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                FAA1.setName("FAA1");
                SCROLLPANE_LISTCV.setViewportView(FAA1);
              }
              panel2.add(SCROLLPANE_LISTCV);
              SCROLLPANE_LISTCV.setBounds(150, 30, 530, 175);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(20, 325, 840, 220);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 582, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_49;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel1;
  private XRiTextField FALIB;
  private XRiCheckBox FADEB;
  private JLabel OBJ_50;
  private JLabel OBJ_52;
  private XRiCheckBox FASPE;
  private JLabel OBJ_54;
  private XRiTextField FAMAG;
  private XRiTextField FATVA;
  private XRiComboBox FAGSP;
  private JLabel OBJ_67;
  private JLabel OBJ_69;
  private JLabel OBJ_64;
  private JLabel OBJ_66;
  private XRiTextField FANDP;
  private JLabel OBJ_60;
  private XRiTextField FAFRS;
  private XRiTextField FACPR;
  private XRiTextField FASETO;
  private XRiTextField FAFRC;
  private JLabel OBJ_84;
  private JLabel OBJ_71;
  private JLabel OBJ_82;
  private XRiCheckBox FANATF;
  private XRiCheckBox FANATI;
  private XRiCheckBox FANATM;
  private JLabel OBJ_73;
  private JLabel OBJ_78;
  private JLabel OBJ_86;
  private XRiTextField FAOPA;
  private XRiTextField FAFPR;
  private XRiTextField FAAGEM;
  private XRiTextField FAUNS;
  private JLabel OBJ_98;
  private XRiTextField FASAN;
  private JLabel OBJ_100;
  private XRiTextField FAMEX;
  private XRiCheckBox FAQEC;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LISTCV;
  private XRiTable FAA1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
