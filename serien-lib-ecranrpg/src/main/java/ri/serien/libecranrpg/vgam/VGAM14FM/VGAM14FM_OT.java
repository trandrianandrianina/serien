
package ri.serien.libecranrpg.vgam.VGAM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class VGAM14FM_OT extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM14FM_OT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UPDST@")).trim());
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UVOLT@")).trim());
    OBJ_14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UMHTT@")).trim());
    UMHTT0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UMHTT0@")).trim());
    OBJ_16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRMIN@")).trim());
    OBJ_17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRFCO@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNBAT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_23.setVisible(lexique.isPresent("UNBAT"));
    OBJ_17.setVisible(lexique.isPresent("FRFCO"));
    OBJ_16.setVisible(lexique.isPresent("FRMIN"));
    UMHTT0.setVisible(lexique.isPresent("UMHTT0"));
    OBJ_14.setVisible(lexique.isPresent("UMHTT"));
    OBJ_21.setVisible(lexique.isPresent("UVOLT"));
    OBJ_19.setVisible(lexique.isPresent("UPDST"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_15 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_19 = new RiZoneSortie();
    OBJ_21 = new RiZoneSortie();
    OBJ_18 = new JLabel();
    OBJ_14 = new RiZoneSortie();
    UMHTT0 = new RiZoneSortie();
    OBJ_13 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_16 = new RiZoneSortie();
    OBJ_17 = new RiZoneSortie();
    OBJ_20 = new JLabel();
    OBJ_23 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();
    OBJ_11 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(460, 270));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Total articles s\u00e9lectionn\u00e9s"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_15 ----
          OBJ_15.setText("Minimum commande");
          OBJ_15.setName("OBJ_15");
          panel1.add(OBJ_15);
          OBJ_15.setBounds(25, 67, 135, 20);

          //---- OBJ_24 ----
          OBJ_24.setText("Nombre d'articles");
          OBJ_24.setName("OBJ_24");
          panel1.add(OBJ_24);
          OBJ_24.setBounds(25, 187, 125, 20);

          //---- OBJ_19 ----
          OBJ_19.setText("@UPDST@");
          OBJ_19.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_19.setName("OBJ_19");
          panel1.add(OBJ_19);
          OBJ_19.setBounds(126, 125, 98, OBJ_19.getPreferredSize().height);

          //---- OBJ_21 ----
          OBJ_21.setText("@UVOLT@");
          OBJ_21.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(126, 155, 98, OBJ_21.getPreferredSize().height);

          //---- OBJ_18 ----
          OBJ_18.setText("Minimum franco");
          OBJ_18.setName("OBJ_18");
          panel1.add(OBJ_18);
          OBJ_18.setBounds(25, 97, 125, 20);

          //---- OBJ_14 ----
          OBJ_14.setText("@UMHTT@");
          OBJ_14.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_14.setName("OBJ_14");
          panel1.add(OBJ_14);
          OBJ_14.setBounds(140, 35, 84, OBJ_14.getPreferredSize().height);

          //---- UMHTT0 ----
          UMHTT0.setText("@UMHTT0@");
          UMHTT0.setName("UMHTT0");
          panel1.add(UMHTT0);
          UMHTT0.setBounds(140, 35, 84, UMHTT0.getPreferredSize().height);

          //---- OBJ_13 ----
          OBJ_13.setText("Montant H.T");
          OBJ_13.setName("OBJ_13");
          panel1.add(OBJ_13);
          OBJ_13.setBounds(25, 37, 95, 20);

          //---- OBJ_22 ----
          OBJ_22.setText("Volume");
          OBJ_22.setName("OBJ_22");
          panel1.add(OBJ_22);
          OBJ_22.setBounds(25, 157, 80, 20);

          //---- OBJ_16 ----
          OBJ_16.setText("@FRMIN@");
          OBJ_16.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_16.setName("OBJ_16");
          panel1.add(OBJ_16);
          OBJ_16.setBounds(181, 65, 43, OBJ_16.getPreferredSize().height);

          //---- OBJ_17 ----
          OBJ_17.setText("@FRFCO@");
          OBJ_17.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_17.setName("OBJ_17");
          panel1.add(OBJ_17);
          OBJ_17.setBounds(181, 95, 43, OBJ_17.getPreferredSize().height);

          //---- OBJ_20 ----
          OBJ_20.setText("Poids");
          OBJ_20.setName("OBJ_20");
          panel1.add(OBJ_20);
          OBJ_20.setBounds(25, 127, 80, 20);

          //---- OBJ_23 ----
          OBJ_23.setText("@UNBAT@");
          OBJ_23.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(189, 185, 35, OBJ_23.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD.add(OBJ_6);

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }

    //---- OBJ_11 ----
    OBJ_11.setText("Total articles s\u00e9l\u00e9ctionn\u00e9s");
    OBJ_11.setName("OBJ_11");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_15;
  private JLabel OBJ_24;
  private RiZoneSortie OBJ_19;
  private RiZoneSortie OBJ_21;
  private JLabel OBJ_18;
  private RiZoneSortie OBJ_14;
  private RiZoneSortie UMHTT0;
  private JLabel OBJ_13;
  private JLabel OBJ_22;
  private RiZoneSortie OBJ_16;
  private RiZoneSortie OBJ_17;
  private JLabel OBJ_20;
  private RiZoneSortie OBJ_23;
  private JPopupMenu BTD;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  private JLabel OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
