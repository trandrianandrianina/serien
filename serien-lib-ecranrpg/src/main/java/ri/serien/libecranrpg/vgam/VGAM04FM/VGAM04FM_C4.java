//$$david$$ ££04/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vgam.VGAM04FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Note :
 * Le découpage de l'IBAN a été supprimé car il n'était pas adapté aux contraintes rencontrées (par exemple 27 caractères pour un IBAN
 * français mais cela peut aller de 14 à 34 caractères suivant les pays). A l'occasion, il faudra faire un composant dédié gérant les
 * différents foramtages internationaux. Pour l'instant, une belle zone de saisie est suffisante (bien qu'un peu courte) ...
 * @author Stéphane Vénéri
 */
public class VGAM04FM_C4 extends SNPanelEcranRPG implements ioFrame {
  
  /**
   * Constructeur.
   */
  public VGAM04FM_C4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    FRIN6.setValeursSelection("1", "");
    
    // Titre
    setTitle("Règlements");
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RGLIB@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBECH@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBRGL@")).trim());
    UCLEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
    UCLEY.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEY@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    boolean isModif = (lexique.getMode() != Lexical.MODE_CONSULTATION);
    
    UCLEX.setVisible(!lexique.HostFieldGetData("UCLEX").trim().equalsIgnoreCase(""));
    UCLEY.setVisible(!lexique.HostFieldGetData("UCLEY").trim().equalsIgnoreCase(""));
    
    coffre.setEnabled(isModif);
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void coffreActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_42 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_29 = new JLabel();
    FRRGL = new XRiTextField();
    FRECH = new XRiTextField();
    riZoneSortie1 = new RiZoneSortie();
    riZoneSortie2 = new RiZoneSortie();
    riZoneSortie3 = new RiZoneSortie();
    OBJ_43 = new JPanel();
    panel2 = new JPanel();
    WDO1 = new XRiTextField();
    OBJ_22_OBJ_22 = new JLabel();
    WBQE = new XRiTextField();
    WGUI = new XRiTextField();
    WCPT = new XRiTextField();
    WRIB = new XRiTextField();
    UCLEX = new RiZoneSortie();
    OBJ_22_OBJ_24 = new JLabel();
    OBJ_22_OBJ_25 = new JLabel();
    OBJ_22_OBJ_26 = new JLabel();
    OBJ_22_OBJ_27 = new JLabel();
    OBJ_22_OBJ_28 = new JLabel();
    coffre = new SNBoutonDetail();
    panel3 = new JPanel();
    WDO2 = new XRiTextField();
    OBJ_40_OBJ_40 = new JLabel();
    WRIE = new XRiTextField();
    UCLEY = new RiZoneSortie();
    OBJ_22_OBJ_23 = new JLabel();
    FRIN6 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1100, 330));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 300));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Autres domiciliations");
              riSousMenu_bt6.setToolTipText("Autres domiciliations");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Recherche multi-crit\u00e8res");
              riSousMenu_bt7.setToolTipText("Recherche multi-crit\u00e8res");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes");
              riSousMenu_bt14.setToolTipText("Bloc-notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- OBJ_42 ----
          OBJ_42.setText("Libell\u00e9 \u00e9dit\u00e9");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(30, 105, 94, 20);
          
          // ---- OBJ_40 ----
          OBJ_40.setText("Ech\u00e9ance");
          OBJ_40.setName("OBJ_40");
          panel1.add(OBJ_40);
          OBJ_40.setBounds(30, 49, 82, 20);
          
          // ---- OBJ_29 ----
          OBJ_29.setText("Mode");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(30, 19, 85, 20);
          
          // ---- FRRGL ----
          FRRGL.setComponentPopupMenu(BTD);
          FRRGL.setName("FRRGL");
          panel1.add(FRRGL);
          FRRGL.setBounds(115, 15, 34, FRRGL.getPreferredSize().height);
          
          // ---- FRECH ----
          FRECH.setComponentPopupMenu(BTD);
          FRECH.setName("FRECH");
          panel1.add(FRECH);
          FRECH.setBounds(115, 45, 34, FRECH.getPreferredSize().height);
          
          // ---- riZoneSortie1 ----
          riZoneSortie1.setText("@RGLIB@");
          riZoneSortie1.setName("riZoneSortie1");
          panel1.add(riZoneSortie1);
          riZoneSortie1.setBounds(165, 17, 427, riZoneSortie1.getPreferredSize().height);
          
          // ---- riZoneSortie2 ----
          riZoneSortie2.setText("@ULBECH@");
          riZoneSortie2.setName("riZoneSortie2");
          panel1.add(riZoneSortie2);
          riZoneSortie2.setBounds(165, 47, 427, riZoneSortie2.getPreferredSize().height);
          
          // ---- riZoneSortie3 ----
          riZoneSortie3.setText("@ULBRGL@");
          riZoneSortie3.setName("riZoneSortie3");
          panel1.add(riZoneSortie3);
          riZoneSortie3.setBounds(165, 105, 427, riZoneSortie3.getPreferredSize().height);
          
          // ======== OBJ_43 ========
          {
            OBJ_43.setBorder(new TitledBorder("Domiciliation bancaire principale"));
            OBJ_43.setOpaque(false);
            OBJ_43.setName("OBJ_43");
            OBJ_43.setLayout(null);
            
            // ======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);
              
              // ---- WDO1 ----
              WDO1.setComponentPopupMenu(BTD);
              WDO1.setName("WDO1");
              panel2.add(WDO1);
              WDO1.setBounds(114, 25, 220, WDO1.getPreferredSize().height);
              
              // ---- OBJ_22_OBJ_22 ----
              OBJ_22_OBJ_22.setText("RIB");
              OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");
              panel2.add(OBJ_22_OBJ_22);
              OBJ_22_OBJ_22.setBounds(355, 29, 55, 20);
              
              // ---- WBQE ----
              WBQE.setComponentPopupMenu(BTD);
              WBQE.setToolTipText("num\u00e9ro de banque");
              WBQE.setName("WBQE");
              panel2.add(WBQE);
              WBQE.setBounds(410, 25, 50, WBQE.getPreferredSize().height);
              
              // ---- WGUI ----
              WGUI.setComponentPopupMenu(BTD);
              WGUI.setToolTipText("num\u00e9ro de guichet");
              WGUI.setName("WGUI");
              panel2.add(WGUI);
              WGUI.setBounds(462, 25, 50, WGUI.getPreferredSize().height);
              
              // ---- WCPT ----
              WCPT.setComponentPopupMenu(BTD);
              WCPT.setToolTipText("num\u00e9ro de compte");
              WCPT.setName("WCPT");
              panel2.add(WCPT);
              WCPT.setBounds(514, 25, 121, WCPT.getPreferredSize().height);
              
              // ---- WRIB ----
              WRIB.setComponentPopupMenu(BTD);
              WRIB.setToolTipText("Cl\u00e9");
              WRIB.setName("WRIB");
              panel2.add(WRIB);
              WRIB.setBounds(670, 25, 28, WRIB.getPreferredSize().height);
              
              // ---- UCLEX ----
              UCLEX.setText("@UCLEX@");
              UCLEX.setForeground(Color.red);
              UCLEX.setToolTipText("Cl\u00e9 r\u00e9elle");
              UCLEX.setName("UCLEX");
              panel2.add(UCLEX);
              UCLEX.setBounds(805, 27, 25, UCLEX.getPreferredSize().height);
              
              // ---- OBJ_22_OBJ_24 ----
              OBJ_22_OBJ_24.setText("Banque");
              OBJ_22_OBJ_24.setName("OBJ_22_OBJ_24");
              panel2.add(OBJ_22_OBJ_24);
              OBJ_22_OBJ_24.setBounds(5, 29, 110, 20);
              
              // ---- OBJ_22_OBJ_25 ----
              OBJ_22_OBJ_25.setText("Banque");
              OBJ_22_OBJ_25.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_22_OBJ_25.setFont(OBJ_22_OBJ_25.getFont().deriveFont(OBJ_22_OBJ_25.getFont().getStyle() | Font.BOLD));
              OBJ_22_OBJ_25.setName("OBJ_22_OBJ_25");
              panel2.add(OBJ_22_OBJ_25);
              OBJ_22_OBJ_25.setBounds(410, 5, 50, 20);
              
              // ---- OBJ_22_OBJ_26 ----
              OBJ_22_OBJ_26.setText("Guichet");
              OBJ_22_OBJ_26.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_22_OBJ_26.setFont(OBJ_22_OBJ_26.getFont().deriveFont(OBJ_22_OBJ_26.getFont().getStyle() | Font.BOLD));
              OBJ_22_OBJ_26.setName("OBJ_22_OBJ_26");
              panel2.add(OBJ_22_OBJ_26);
              OBJ_22_OBJ_26.setBounds(462, 5, 50, 20);
              
              // ---- OBJ_22_OBJ_27 ----
              OBJ_22_OBJ_27.setText("Num\u00e9ro de compte");
              OBJ_22_OBJ_27.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_22_OBJ_27.setFont(OBJ_22_OBJ_27.getFont().deriveFont(OBJ_22_OBJ_27.getFont().getStyle() | Font.BOLD));
              OBJ_22_OBJ_27.setName("OBJ_22_OBJ_27");
              panel2.add(OBJ_22_OBJ_27);
              OBJ_22_OBJ_27.setBounds(514, 5, 121, 20);
              
              // ---- OBJ_22_OBJ_28 ----
              OBJ_22_OBJ_28.setText("Cl\u00e9");
              OBJ_22_OBJ_28.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_22_OBJ_28.setFont(OBJ_22_OBJ_28.getFont().deriveFont(OBJ_22_OBJ_28.getFont().getStyle() | Font.BOLD));
              OBJ_22_OBJ_28.setName("OBJ_22_OBJ_28");
              panel2.add(OBJ_22_OBJ_28);
              OBJ_22_OBJ_28.setBounds(670, 5, 28, 20);
              
              // ---- coffre ----
              coffre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              coffre.setName("coffre");
              coffre.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  coffreActionPerformed(e);
                }
              });
              panel2.add(coffre);
              coffre.setBounds(835, 23, 32, 32);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            OBJ_43.add(panel2);
            panel2.setBounds(10, 25, 875, 60);
            
            // ======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);
              
              // ---- WDO2 ----
              WDO2.setComponentPopupMenu(BTD);
              WDO2.setName("WDO2");
              panel3.add(WDO2);
              WDO2.setBounds(114, 15, 220, WDO2.getPreferredSize().height);
              
              // ---- OBJ_40_OBJ_40 ----
              OBJ_40_OBJ_40.setText("IBAN");
              OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
              panel3.add(OBJ_40_OBJ_40);
              OBJ_40_OBJ_40.setBounds(355, 19, 56, 20);
              
              // ---- WRIE ----
              WRIE.setComponentPopupMenu(BTD);
              WRIE.setName("WRIE");
              panel3.add(WRIE);
              WRIE.setBounds(410, 15, 285, WRIE.getPreferredSize().height);
              
              // ---- UCLEY ----
              UCLEY.setText("@UCLEY@");
              UCLEY.setForeground(Color.red);
              UCLEY.setName("UCLEY");
              panel3.add(UCLEY);
              UCLEY.setBounds(805, 17, 25, UCLEY.getPreferredSize().height);
              
              // ---- OBJ_22_OBJ_23 ----
              OBJ_22_OBJ_23.setText("Guichet ou SWIFT");
              OBJ_22_OBJ_23.setName("OBJ_22_OBJ_23");
              panel3.add(OBJ_22_OBJ_23);
              OBJ_22_OBJ_23.setBounds(5, 19, 110, 20);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            OBJ_43.add(panel3);
            panel3.setBounds(10, 90, 874, 55);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < OBJ_43.getComponentCount(); i++) {
                Rectangle bounds = OBJ_43.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = OBJ_43.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              OBJ_43.setMinimumSize(preferredSize);
              OBJ_43.setPreferredSize(preferredSize);
            }
          }
          panel1.add(OBJ_43);
          OBJ_43.setBounds(5, 145, 895, 155);
          
          // ---- FRIN6 ----
          FRIN6.setText("calcul de la date d'\u00e9ch\u00e9ance \u00e0 partir de la date de r\u00e9ception dans le cas du flottant");
          FRIN6.setName("FRIN6");
          panel1.add(FRIN6);
          FRIN6.setBounds(115, 80, 675, FRIN6.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 15, 910, 305);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Choix possibles");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_42;
  private JLabel OBJ_40;
  private JLabel OBJ_29;
  private XRiTextField FRRGL;
  private XRiTextField FRECH;
  private RiZoneSortie riZoneSortie1;
  private RiZoneSortie riZoneSortie2;
  private RiZoneSortie riZoneSortie3;
  private JPanel OBJ_43;
  private JPanel panel2;
  private XRiTextField WDO1;
  private JLabel OBJ_22_OBJ_22;
  private XRiTextField WBQE;
  private XRiTextField WGUI;
  private XRiTextField WCPT;
  private XRiTextField WRIB;
  private RiZoneSortie UCLEX;
  private JLabel OBJ_22_OBJ_24;
  private JLabel OBJ_22_OBJ_25;
  private JLabel OBJ_22_OBJ_26;
  private JLabel OBJ_22_OBJ_27;
  private JLabel OBJ_22_OBJ_28;
  private SNBoutonDetail coffre;
  private JPanel panel3;
  private XRiTextField WDO2;
  private JLabel OBJ_40_OBJ_40;
  private XRiTextField WRIE;
  private RiZoneSortie UCLEY;
  private JLabel OBJ_22_OBJ_23;
  private XRiCheckBox FRIN6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_20;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
