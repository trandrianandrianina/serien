
package ri.serien.libecranrpg.vgam.VGAM80FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM80FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LDG01", }, { "LDG02", }, { "LDG03", }, { "LDG04", }, { "LDG05", }, { "LDG06", }, { "LDG07", },
      { "LDG08", }, { "LDG09", }, { "LDG10", }, { "LDG11", }, { "LDG12", }, { "LDG13", }, { "LDG14", }, { "LDG15", }, };
  private int[] _WTP01_Width = { 1050, };
  
  public VGAM80FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    INDDD1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WID@")).trim());
    INDFAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WFAC@")).trim());
    LNLI01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI01@")).trim());
    LNLI02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI02@")).trim());
    LNLI03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI03@")).trim());
    LNLI04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI04@")).trim());
    LNLI05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI05@")).trim());
    LNLI06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI06@")).trim());
    LNLI07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI07@")).trim());
    LNLI08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI08@")).trim());
    LNLI09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI09@")).trim());
    LNLI10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI10@")).trim());
    LNLI11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI11@")).trim());
    LNLI12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI12@")).trim());
    LNLI13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI13@")).trim());
    LNLI14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI14@")).trim());
    LNLI15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNLI15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void riSousMenu_btEditionActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_32_OBJ_32 = new JLabel();
    WETB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_btEdition = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    sNLabelTitre1 = new SNLabelTitre();
    panel2 = new JPanel();
    lbDates = new JLabel();
    INDDD1 = new RiZoneSortie();
    lbNumeroDocument = new JLabel();
    INDFAC = new RiZoneSortie();
    pnlTexte = new JPanel();
    LDG01 = new XRiTextField();
    LNLI01 = new RiZoneSortie();
    BT_PGUP = new JButton();
    LDG02 = new XRiTextField();
    LNLI02 = new RiZoneSortie();
    LDG03 = new XRiTextField();
    LNLI03 = new RiZoneSortie();
    LDG04 = new XRiTextField();
    LNLI04 = new RiZoneSortie();
    LDG05 = new XRiTextField();
    LNLI05 = new RiZoneSortie();
    LDG06 = new XRiTextField();
    LNLI06 = new RiZoneSortie();
    LDG07 = new XRiTextField();
    LNLI07 = new RiZoneSortie();
    LDG08 = new XRiTextField();
    LNLI08 = new RiZoneSortie();
    LDG09 = new XRiTextField();
    LNLI09 = new RiZoneSortie();
    LDG10 = new XRiTextField();
    LNLI10 = new RiZoneSortie();
    BT_PGDOWN = new JButton();
    LDG11 = new XRiTextField();
    LNLI11 = new RiZoneSortie();
    LDG12 = new XRiTextField();
    LNLI12 = new RiZoneSortie();
    LDG13 = new XRiTextField();
    LNLI13 = new RiZoneSortie();
    LDG14 = new XRiTextField();
    LNLI14 = new RiZoneSortie();
    LDG15 = new XRiTextField();
    LNLI15 = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des demandes d'avoir");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 22));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Etablissement");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");

          //---- WETB ----
          WETB.setComponentPopupMenu(null);
          WETB.setName("WETB");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(557, 557, 557))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setToolTipText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");

            //---- riMenu_bt3 ----
            riMenu_bt3.setText("Outils");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);

          //======== riSousMenu15 ========
          {
            riSousMenu15.setName("riSousMenu15");

            //---- riSousMenu_btEdition ----
            riSousMenu_btEdition.setText("Edition");
            riSousMenu_btEdition.setName("riSousMenu_btEdition");
            riSousMenu_btEdition.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_btEditionActionPerformed(e);
              }
            });
            riSousMenu15.add(riSousMenu_btEdition);
          }
          menus_haut.add(riSousMenu15);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== pnlContenu ========
    {
      pnlContenu.setBackground(new Color(239, 239, 222));
      pnlContenu.setOpaque(true);
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};

      //---- sNLabelTitre1 ----
      sNLabelTitre1.setText("Saisie de la demande d'avoir");
      sNLabelTitre1.setName("sNLabelTitre1");
      pnlContenu.add(sNLabelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== panel2 ========
      {
        panel2.setBorder(new TitledBorder(""));
        panel2.setOpaque(false);
        panel2.setPreferredSize(new Dimension(1000, 50));
        panel2.setName("panel2");
        panel2.setLayout(new GridBagLayout());
        ((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {0, 220, 0, 0, 0, 0};
        ((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- lbDates ----
        lbDates.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDates.setText("Id de la demande");
        lbDates.setName("lbDates");
        panel2.add(lbDates, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- INDDD1 ----
        INDDD1.setText("@WID@");
        INDDD1.setName("INDDD1");
        panel2.add(INDDD1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- lbNumeroDocument ----
        lbNumeroDocument.setText("R\u00e9f\u00e9rence facture");
        lbNumeroDocument.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNumeroDocument.setName("lbNumeroDocument");
        panel2.add(lbNumeroDocument, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- INDFAC ----
        INDFAC.setComponentPopupMenu(null);
        INDFAC.setText("@WFAC@");
        INDFAC.setName("INDFAC");
        panel2.add(INDFAC, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(panel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlTexte ========
      {
        pnlTexte.setBackground(new Color(239, 239, 222));
        pnlTexte.setName("pnlTexte");
        pnlTexte.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlTexte.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlTexte.getLayout()).rowHeights = new int[] {28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlTexte.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlTexte.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- LDG01 ----
        LDG01.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG01.setName("LDG01");
        pnlTexte.add(LDG01, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI01 ----
        LNLI01.setComponentPopupMenu(null);
        LNLI01.setText("@LNLI01@");
        LNLI01.setPreferredSize(new Dimension(50, 24));
        LNLI01.setName("LNLI01");
        pnlTexte.add(LNLI01, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");
        pnlTexte.add(BT_PGUP, new GridBagConstraints(2, 0, 1, 6, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //---- LDG02 ----
        LDG02.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG02.setName("LDG02");
        pnlTexte.add(LDG02, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI02 ----
        LNLI02.setComponentPopupMenu(null);
        LNLI02.setText("@LNLI02@");
        LNLI02.setPreferredSize(new Dimension(50, 24));
        LNLI02.setName("LNLI02");
        pnlTexte.add(LNLI02, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LDG03 ----
        LDG03.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG03.setName("LDG03");
        pnlTexte.add(LDG03, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI03 ----
        LNLI03.setComponentPopupMenu(null);
        LNLI03.setText("@LNLI03@");
        LNLI03.setPreferredSize(new Dimension(50, 24));
        LNLI03.setName("LNLI03");
        pnlTexte.add(LNLI03, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LDG04 ----
        LDG04.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG04.setName("LDG04");
        pnlTexte.add(LDG04, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI04 ----
        LNLI04.setComponentPopupMenu(null);
        LNLI04.setText("@LNLI04@");
        LNLI04.setPreferredSize(new Dimension(50, 24));
        LNLI04.setName("LNLI04");
        pnlTexte.add(LNLI04, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LDG05 ----
        LDG05.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG05.setName("LDG05");
        pnlTexte.add(LDG05, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI05 ----
        LNLI05.setComponentPopupMenu(null);
        LNLI05.setText("@LNLI05@");
        LNLI05.setPreferredSize(new Dimension(50, 24));
        LNLI05.setName("LNLI05");
        pnlTexte.add(LNLI05, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LDG06 ----
        LDG06.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG06.setName("LDG06");
        pnlTexte.add(LDG06, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI06 ----
        LNLI06.setComponentPopupMenu(null);
        LNLI06.setText("@LNLI06@");
        LNLI06.setPreferredSize(new Dimension(50, 24));
        LNLI06.setName("LNLI06");
        pnlTexte.add(LNLI06, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LDG07 ----
        LDG07.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG07.setName("LDG07");
        pnlTexte.add(LDG07, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI07 ----
        LNLI07.setComponentPopupMenu(null);
        LNLI07.setText("@LNLI07@");
        LNLI07.setPreferredSize(new Dimension(50, 24));
        LNLI07.setName("LNLI07");
        pnlTexte.add(LNLI07, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LDG08 ----
        LDG08.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG08.setName("LDG08");
        pnlTexte.add(LDG08, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI08 ----
        LNLI08.setComponentPopupMenu(null);
        LNLI08.setText("@LNLI08@");
        LNLI08.setPreferredSize(new Dimension(50, 24));
        LNLI08.setName("LNLI08");
        pnlTexte.add(LNLI08, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LDG09 ----
        LDG09.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG09.setName("LDG09");
        pnlTexte.add(LDG09, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI09 ----
        LNLI09.setComponentPopupMenu(null);
        LNLI09.setText("@LNLI09@");
        LNLI09.setPreferredSize(new Dimension(50, 24));
        LNLI09.setName("LNLI09");
        pnlTexte.add(LNLI09, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LDG10 ----
        LDG10.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG10.setName("LDG10");
        pnlTexte.add(LDG10, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI10 ----
        LNLI10.setComponentPopupMenu(null);
        LNLI10.setText("@LNLI10@");
        LNLI10.setPreferredSize(new Dimension(50, 24));
        LNLI10.setName("LNLI10");
        pnlTexte.add(LNLI10, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");
        pnlTexte.add(BT_PGDOWN, new GridBagConstraints(2, 9, 1, 6, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //---- LDG11 ----
        LDG11.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG11.setName("LDG11");
        pnlTexte.add(LDG11, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI11 ----
        LNLI11.setComponentPopupMenu(null);
        LNLI11.setText("@LNLI11@");
        LNLI11.setPreferredSize(new Dimension(50, 24));
        LNLI11.setName("LNLI11");
        pnlTexte.add(LNLI11, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LDG12 ----
        LDG12.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG12.setName("LDG12");
        pnlTexte.add(LDG12, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI12 ----
        LNLI12.setComponentPopupMenu(null);
        LNLI12.setText("@LNLI12@");
        LNLI12.setPreferredSize(new Dimension(50, 24));
        LNLI12.setName("LNLI12");
        pnlTexte.add(LNLI12, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LDG13 ----
        LDG13.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG13.setName("LDG13");
        pnlTexte.add(LDG13, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI13 ----
        LNLI13.setComponentPopupMenu(null);
        LNLI13.setText("@LNLI13@");
        LNLI13.setPreferredSize(new Dimension(50, 24));
        LNLI13.setName("LNLI13");
        pnlTexte.add(LNLI13, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LDG14 ----
        LDG14.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG14.setName("LDG14");
        pnlTexte.add(LDG14, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI14 ----
        LNLI14.setComponentPopupMenu(null);
        LNLI14.setText("@LNLI14@");
        LNLI14.setPreferredSize(new Dimension(50, 24));
        LNLI14.setName("LNLI14");
        pnlTexte.add(LNLI14, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LDG15 ----
        LDG15.setFont(new Font("Courier New", Font.PLAIN, 12));
        LDG15.setName("LDG15");
        pnlTexte.add(LDG15, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LNLI15 ----
        LNLI15.setComponentPopupMenu(null);
        LNLI15.setText("@LNLI15@");
        LNLI15.setPreferredSize(new Dimension(50, 24));
        LNLI15.setName("LNLI15");
        pnlTexte.add(LNLI15, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlContenu.add(pnlTexte, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.WEST);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_32_OBJ_32;
  private XRiTextField WETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_btEdition;
  private SNPanelContenu pnlContenu;
  private SNLabelTitre sNLabelTitre1;
  private JPanel panel2;
  private JLabel lbDates;
  private RiZoneSortie INDDD1;
  private JLabel lbNumeroDocument;
  private RiZoneSortie INDFAC;
  private JPanel pnlTexte;
  private XRiTextField LDG01;
  private RiZoneSortie LNLI01;
  private JButton BT_PGUP;
  private XRiTextField LDG02;
  private RiZoneSortie LNLI02;
  private XRiTextField LDG03;
  private RiZoneSortie LNLI03;
  private XRiTextField LDG04;
  private RiZoneSortie LNLI04;
  private XRiTextField LDG05;
  private RiZoneSortie LNLI05;
  private XRiTextField LDG06;
  private RiZoneSortie LNLI06;
  private XRiTextField LDG07;
  private RiZoneSortie LNLI07;
  private XRiTextField LDG08;
  private RiZoneSortie LNLI08;
  private XRiTextField LDG09;
  private RiZoneSortie LNLI09;
  private XRiTextField LDG10;
  private RiZoneSortie LNLI10;
  private JButton BT_PGDOWN;
  private XRiTextField LDG11;
  private RiZoneSortie LNLI11;
  private XRiTextField LDG12;
  private RiZoneSortie LNLI12;
  private XRiTextField LDG13;
  private RiZoneSortie LNLI13;
  private XRiTextField LDG14;
  private RiZoneSortie LNLI14;
  private XRiTextField LDG15;
  private RiZoneSortie LNLI15;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
