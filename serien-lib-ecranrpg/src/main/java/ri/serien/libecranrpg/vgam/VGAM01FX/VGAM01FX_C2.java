
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_C2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM01FX_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    CODGR.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // CODGR.setSelected(lexique.HostFieldGetData("CODGR").equalsIgnoreCase("1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (CODGR.isSelected())
    // lexique.HostFieldPutData("CODGR", 0, "1");
    // else
    // lexique.HostFieldPutData("CODGR", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*LIB");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*LIB");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*LIB");
    }
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*LIF");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*LIF");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*LIF");
    }
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*REF");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*REF");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*REF");
    }
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*DEV");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*DEV");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*DEV");
    }
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*FAC");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*FAC");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*FAC");
    }
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*DFA");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*DFA");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*DFA");
    }
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*dfa");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*dfa");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*dfa");
    }
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*FAR");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*FAR");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*FAR");
    }
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*far");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*far");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*far");
    }
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*DLP");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*DLP");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*DLP");
    }
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*dlp");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*dlp");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*dlp");
    }
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*DLR");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*DLR");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*DLR");
    }
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*dlr");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*dlr");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*dlr");
    }
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*rec");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*rec");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*rec");
    }
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, lexique.HostFieldGetData(BTD.getInvoker().getName()) + "*RBC");
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*RBC");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*RBC");
    }
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_48 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel1 = new JPanel();
    COLIB = new XRiTextField();
    CODGR = new XRiCheckBox();
    OBJ_68 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_67 = new JLabel();
    CONCG1 = new XRiTextField();
    CONCG2 = new XRiTextField();
    CONCG3 = new XRiTextField();
    panel2 = new JPanel();
    COLI1 = new XRiTextField();
    COLI2 = new XRiTextField();
    OBJ_85 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_83 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_34 = new JMenuItem();
    OBJ_35 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_33 = new JMenuItem();
    OBJ_32 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_48 ----
          OBJ_48.setText("Ordre");
          OBJ_48.setName("OBJ_48");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        ((GridBagLayout)p_centrage.getLayout()).columnWidths = new int[] {684};

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(660, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Poste de comptabilisation");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setPreferredSize(new Dimension(610, 404));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder(""));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- COLIB ----
              COLIB.setComponentPopupMenu(BTD);
              COLIB.setName("COLIB");
              panel1.add(COLIB);
              COLIB.setBounds(270, 80, 270, COLIB.getPreferredSize().height);

              //---- CODGR ----
              CODGR.setText("Non regroupement");
              CODGR.setComponentPopupMenu(BTD);
              CODGR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CODGR.setName("CODGR");
              panel1.add(CODGR);
              CODGR.setBounds(270, 120, 138, 20);

              //---- OBJ_68 ----
              OBJ_68.setText("Num\u00e9ro de compte");
              OBJ_68.setName("OBJ_68");
              panel1.add(OBJ_68);
              OBJ_68.setBounds(40, 49, 225, 20);

              //---- OBJ_72 ----
              OBJ_72.setText("Libell\u00e9 \u00e9critures");
              OBJ_72.setName("OBJ_72");
              panel1.add(OBJ_72);
              OBJ_72.setBounds(40, 84, 225, 20);

              //---- OBJ_65 ----
              OBJ_65.setText("March.");
              OBJ_65.setName("OBJ_65");
              panel1.add(OBJ_65);
              OBJ_65.setBounds(270, 20, 49, 20);

              //---- OBJ_66 ----
              OBJ_66.setText("Fr. g\u00e9n.");
              OBJ_66.setName("OBJ_66");
              panel1.add(OBJ_66);
              OBJ_66.setBounds(345, 20, 49, 20);

              //---- OBJ_67 ----
              OBJ_67.setText("Immob.");
              OBJ_67.setName("OBJ_67");
              panel1.add(OBJ_67);
              OBJ_67.setBounds(425, 20, 49, 20);

              //---- CONCG1 ----
              CONCG1.setComponentPopupMenu(BTD);
              CONCG1.setName("CONCG1");
              panel1.add(CONCG1);
              CONCG1.setBounds(270, 45, 58, CONCG1.getPreferredSize().height);

              //---- CONCG2 ----
              CONCG2.setComponentPopupMenu(BTD);
              CONCG2.setName("CONCG2");
              panel1.add(CONCG2);
              CONCG2.setBounds(345, 45, 58, CONCG2.getPreferredSize().height);

              //---- CONCG3 ----
              CONCG3.setComponentPopupMenu(BTD);
              CONCG3.setName("CONCG3");
              panel1.add(CONCG3);
              CONCG3.setBounds(425, 45, 58, CONCG3.getPreferredSize().height);

              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(25, 20, 595, 200);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Incrustation libell\u00e9 si non regroupement"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- COLI1 ----
              COLI1.setToolTipText("Bouton droit pour ins\u00e9rer une variable...");
              COLI1.setComponentPopupMenu(BTD);
              COLI1.setFont(new Font("Courier New", Font.PLAIN, 12));
              COLI1.setName("COLI1");
              panel2.add(COLI1);
              COLI1.setBounds(280, 51, 210, COLI1.getPreferredSize().height);

              //---- COLI2 ----
              COLI2.setToolTipText("Bouton droit pour ins\u00e9rer une variable...");
              COLI2.setComponentPopupMenu(BTD);
              COLI2.setFont(new Font("Courier New", Font.PLAIN, 12));
              COLI2.setName("COLI2");
              panel2.add(COLI2);
              COLI2.setBounds(280, 86, 210, COLI2.getPreferredSize().height);

              //---- OBJ_85 ----
              OBJ_85.setText("Ecriture en devise");
              OBJ_85.setToolTipText("Bouton droit pour ins\u00e9rer une variable...");
              OBJ_85.setName("OBJ_85");
              panel2.add(OBJ_85);
              OBJ_85.setBounds(40, 90, 129, 20);

              //---- OBJ_82 ----
              OBJ_82.setText("Ecriture sans devise");
              OBJ_82.setToolTipText("Bouton droit pour ins\u00e9rer une variable...");
              OBJ_82.setName("OBJ_82");
              panel2.add(OBJ_82);
              OBJ_82.setBounds(40, 55, 124, 20);

              //---- OBJ_83 ----
              OBJ_83.setText("1........5........10........15........20..........26");
              OBJ_83.setName("OBJ_83");
              panel2.add(OBJ_83);
              OBJ_83.setBounds(285, 30, 200, 20);

              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(25, 240, 595, 135);

            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 661, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 426, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("*LIB : Inclure libell\u00e9 CO");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("*LIF : Ins\u00e9rer libell\u00e9 frs");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("*REF  :Ins\u00e9rer r\u00e9f\u00e9rence bon");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_20 ----
      OBJ_20.setText("*DEV : Ins\u00e9rer montant en devise");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("*FAC : Ins\u00e9rer num\u00e9ro facture");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("*DFA : Ins\u00e9rer date facture sous forme JJ.MM.AA");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("*dfa : Ins\u00e9rer date facture sous forme JJMMAA");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_24 ----
      OBJ_24.setText("*FAR : Ins\u00e9rer date facture sous forme JJ.MM");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);

      //---- OBJ_25 ----
      OBJ_25.setText("*far : Ins\u00e9rer date facture sous forme JJMM");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);

      //---- OBJ_26 ----
      OBJ_26.setText("*DLP : Ins\u00e9rer date livraison pr\u00e9vue sous forme JJ.MM.AA");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);

      //---- OBJ_27 ----
      OBJ_27.setText("*dlp : Ins\u00e9rer date livraison pr\u00e9vue sous forme JJMMAA");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);

      //---- OBJ_28 ----
      OBJ_28.setText("*DLR : Ins\u00e9rer date livraison pr\u00e9vue sous forme JJ.MM");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed(e);
        }
      });
      BTD.add(OBJ_28);

      //---- OBJ_29 ----
      OBJ_29.setText("*dlr : Ins\u00e9rer date livraison pr\u00e9vue sous forme JJMM");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD.add(OBJ_29);

      //---- OBJ_34 ----
      OBJ_34.setText("*RBC : Ins\u00e9rer date de r\u00e9ception sous forme JJMMAA");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed(e);
        }
      });
      BTD.add(OBJ_34);

      //---- OBJ_35 ----
      OBJ_35.setText("*RBC : Ins\u00e9rer la r\u00e9f\u00e9rence de commande");
      OBJ_35.setName("OBJ_35");
      OBJ_35.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed(e);
        }
      });
      BTD.add(OBJ_35);
      BTD.addSeparator();

      //---- OBJ_30 ----
      OBJ_30.setText("Aide en ligne");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed(e);
        }
      });
      BTD.add(OBJ_30);

      //---- OBJ_31 ----
      OBJ_31.setText("Invite");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD.add(OBJ_31);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_33 ----
      OBJ_33.setText("Choix possibles");
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_33);

      //---- OBJ_32 ----
      OBJ_32.setText("Aide en ligne");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_32);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_48;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel1;
  private XRiTextField COLIB;
  private XRiCheckBox CODGR;
  private JLabel OBJ_68;
  private JLabel OBJ_72;
  private JLabel OBJ_65;
  private JLabel OBJ_66;
  private JLabel OBJ_67;
  private XRiTextField CONCG1;
  private XRiTextField CONCG2;
  private XRiTextField CONCG3;
  private JPanel panel2;
  private XRiTextField COLI1;
  private XRiTextField COLI2;
  private JLabel OBJ_85;
  private JLabel OBJ_82;
  private JLabel OBJ_83;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_28;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_34;
  private JMenuItem OBJ_35;
  private JMenuItem OBJ_30;
  private JMenuItem OBJ_31;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_33;
  private JMenuItem OBJ_32;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
