
package ri.serien.libecranrpg.vgam.VGAM32FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EmptyBorder;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * Création de l'import d'un catalogue fournisseur.
 * [GAM6726] Gestion des achats -> Editions diverses -> Importations et exportations -> Importations articles -> Imports catalogues.
 * Bouton "Créer".
 */
public class VGAM32FM_A1 extends SNPanelEcranRPG implements ioFrame {
  private static final String BOUTON_ANALYSER = "Analyser";
  
  public VGAM32FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    snBarreBouton.ajouterBouton(BOUTON_ANALYSER, 'a', true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LIBNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBNUM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    // Afficher le message d'erreur s'i ly en a un
    if (lexique.isTrue("19")) {
      String message = lexique.HostFieldGetData("MESERR");
      if (message != null && !message.isEmpty()) {
        DialogueInformation.afficher(message);
      }
    }
    
    if (lexique.HostFieldGetData("P32ETB") != null && !lexique.HostFieldGetData("P32ETB").trim().isEmpty()) {
      IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("P32ETB").trim());
      snFournisseur.setSession(getSession());
      snFournisseur.setIdEtablissement(idEtablissement);
      snFournisseur.charger(false);
      snFournisseur.setSelectionParChampRPG(lexique, "P32COL", "P32FRS");
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snFournisseur.renseignerChampRPG(lexique, "P32COL", "P32FRS");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_ANALYSER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    sNLabel2 = new SNLabelChamp();
    P32NUM = new XRiTextField();
    LIBNUM = new RiZoneSortie();
    snBarreBouton = new SNBarreBouton();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(790, 200));
    setPreferredSize(new Dimension(673, 200));
    setMaximumSize(new Dimension(2147483647, 200));
    setBackground(new Color(238, 238, 210));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlPrincipal.getLayout()).columnWidths = new int[] {0, 0, 393, 0};
      ((GridBagLayout)pnlPrincipal.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)pnlPrincipal.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
      ((GridBagLayout)pnlPrincipal.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

      //---- lbFournisseur ----
      lbFournisseur.setText("Fournisseur");
      lbFournisseur.setName("lbFournisseur");
      pnlPrincipal.add(lbFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

      //---- snFournisseur ----
      snFournisseur.setName("snFournisseur");
      pnlPrincipal.add(snFournisseur, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //---- sNLabel2 ----
      sNLabel2.setText("Configuration d'import");
      sNLabel2.setName("sNLabel2");
      pnlPrincipal.add(sNLabel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 5), 0, 0));

      //---- P32NUM ----
      P32NUM.setComponentPopupMenu(BTD);
      P32NUM.setMaximumSize(new Dimension(92, 30));
      P32NUM.setMinimumSize(new Dimension(92, 30));
      P32NUM.setPreferredSize(new Dimension(92, 30));
      P32NUM.setName("P32NUM");
      pnlPrincipal.add(P32NUM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 5), 0, 0));

      //---- LIBNUM ----
      LIBNUM.setText("@LIBNUM@");
      LIBNUM.setMaximumSize(new Dimension(79, 30));
      LIBNUM.setMinimumSize(new Dimension(79, 30));
      LIBNUM.setPreferredSize(new Dimension(100, 30));
      LIBNUM.setName("LIBNUM");
      pnlPrincipal.add(LIBNUM, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlPrincipal, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNLabelChamp sNLabel2;
  private XRiTextField P32NUM;
  private RiZoneSortie LIBNUM;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
