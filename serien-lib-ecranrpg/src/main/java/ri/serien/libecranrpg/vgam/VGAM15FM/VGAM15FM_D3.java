
package ri.serien.libecranrpg.vgam.VGAM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM15FM_D3 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] _LD01_Title = { "HLD01", };
  private String[][] _LD01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _LD01_Width = { 769, };
  private String libelleDirectOuInterne = "";
  
  /**
   * Constructeur.
   */
  public VGAM15FM_D3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    LD01.setAspectTable(null, _LD01_Title, _LD01_Data, _LD01_Width, false, null, null, null, null);
    
    
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    OBJ_86.setIcon(lexique.chargerImage("images/avert.gif", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP15@")).trim());
    OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E2NOM@")).trim());
    WDATEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDATEX@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@EAMAG@  @WMALB@")).trim()));
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EASAN@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSALBR@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMFR@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMCD@")).trim());
    OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TVA1@")).trim());
    OBJ_79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TVA2@")).trim());
    OBJ_80.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TVA3@")).trim());
    OBJ_83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BAS@")).trim());
    OBJ_84.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EADEV@")).trim());
    LPORTF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LPORTF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_80.setVisible(lexique.isPresent("TVA3"));
    OBJ_79.setVisible(lexique.isPresent("TVA2"));
    OBJ_78.setVisible(lexique.isPresent("TVA1"));
    OBJ_84.setVisible(lexique.isPresent("EADEV"));
    OBJ_52.setVisible(lexique.isPresent("EASAN"));
    OBJ_83.setVisible(lexique.isPresent("BAS"));
    OBJ_81.setVisible(OBJ_83.isVisible());
    OBJ_86.setVisible(!lexique.HostFieldGetData("TEST").trim().equalsIgnoreCase(""));
    OBJ_87.setVisible(lexique.isPresent("LIBMCD"));
    OBJ_89.setVisible(lexique.isPresent("LIBMFR"));
    OBJ_53.setVisible(lexique.isPresent("WSALBR"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@&&G_TITRE@ - @SOUTIT@"));
    
    if (lexique.HostFieldGetData("EAIN9").trim().equals("D")) {
      libelleDirectOuInterne = " (Direct usine)";
    }
    else if (lexique.HostFieldGetData("EAIN9").trim().equals("I")) {
      libelleDirectOuInterne = " (Commande interne)";
    }
    else {
      libelleDirectOuInterne = "";
    }
    p_bpresentation.setText(p_bpresentation.getText() + libelleDirectOuInterne);
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // ScriptCall("G_CONVER")
    // ScriptCall("G_CONVER")
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBouton1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("V06F4");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    EAETB = new XRiTextField();
    WNUM = new XRiTextField();
    WSUF = new XRiTextField();
    OBJ_73 = new RiZoneSortie();
    WDATEX = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_55 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST4 = new JScrollPane();
    LD01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_52 = new JLabel();
    OBJ_53 = new JLabel();
    panel2 = new JPanel();
    OBJ_89 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_72 = new JLabel();
    WTHTX = new XRiTextField();
    WTVAX = new XRiTextField();
    WTTCX = new XRiTextField();
    WSL1X = new XRiTextField();
    WSL2X = new XRiTextField();
    WSL3X = new XRiTextField();
    WTL1X = new XRiTextField();
    WTL2X = new XRiTextField();
    WTL3X = new XRiTextField();
    OBJ_86 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_64 = new JLabel();
    WLT1 = new XRiTextField();
    WLT2 = new XRiTextField();
    WLT3 = new XRiTextField();
    OBJ_78 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_88 = new SNBoutonLeger();
    WPOR1X = new XRiTextField();
    OBJ_65 = new JLabel();
    LPORTF = new JLabel();
    WREP = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TYP15@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- EAETB ----
          EAETB.setComponentPopupMenu(null);
          EAETB.setName("EAETB");
          
          // ---- WNUM ----
          WNUM.setComponentPopupMenu(null);
          WNUM.setName("WNUM");
          
          // ---- WSUF ----
          WSUF.setComponentPopupMenu(null);
          WSUF.setName("WSUF");
          
          // ---- OBJ_73 ----
          OBJ_73.setText("@E2NOM@");
          OBJ_73.setOpaque(false);
          OBJ_73.setComponentPopupMenu(null);
          OBJ_73.setName("OBJ_73");
          
          // ---- WDATEX ----
          WDATEX.setComponentPopupMenu(null);
          WDATEX.setText("@WDATEX@");
          WDATEX.setBorder(new BevelBorder(BevelBorder.LOWERED));
          WDATEX.setHorizontalAlignment(SwingConstants.CENTER);
          WDATEX.setFont(WDATEX.getFont().deriveFont(WDATEX.getFont().getStyle() | Font.BOLD));
          WDATEX.setName("WDATEX");
          
          // ---- OBJ_51 ----
          OBJ_51.setText("Etablissement");
          OBJ_51.setName("OBJ_51");
          
          // ---- OBJ_54 ----
          OBJ_54.setText("Num\u00e9ro");
          OBJ_54.setName("OBJ_54");
          
          // ---- OBJ_55 ----
          OBJ_55.setText("Fournisseur");
          OBJ_55.setName("OBJ_55");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(EAETB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(11, 11, 11)
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
                  .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE).addGap(225, 225, 225)
                  .addComponent(WDATEX, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_51))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(EAETB, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_54))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(WNUM, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(WSUF, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_55))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(3, 3, 3).addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(3, 3, 3).addComponent(WDATEX, GroupLayout.PREFERRED_SIZE, 24,
                  GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("@EAMAG@  @WMALB@"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ======== SCROLLPANE_LIST4 ========
            {
              SCROLLPANE_LIST4.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST4.setName("SCROLLPANE_LIST4");
              
              // ---- LD01 ----
              LD01.setName("LD01");
              SCROLLPANE_LIST4.setViewportView(LD01);
            }
            panel1.add(SCROLLPANE_LIST4);
            SCROLLPANE_LIST4.setBounds(15, 40, 915, 270);
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(935, 40, 25, 125);
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(935, 185, 25, 125);
            
            // ---- OBJ_52 ----
            OBJ_52.setText("@EASAN@");
            OBJ_52.setFont(OBJ_52.getFont().deriveFont(OBJ_52.getFont().getSize() - 1f));
            OBJ_52.setName("OBJ_52");
            panel1.add(OBJ_52);
            OBJ_52.setBounds(325, 0, 40, 18);
            
            // ---- OBJ_53 ----
            OBJ_53.setText("@WSALBR@");
            OBJ_53.setFont(OBJ_53.getFont().deriveFont(OBJ_53.getFont().getSize() - 1f));
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(380, 0, 333, 18);
          }
          
          // ======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ---- OBJ_89 ----
            OBJ_89.setText("@LIBMFR@");
            OBJ_89.setName("OBJ_89");
            panel2.add(OBJ_89);
            OBJ_89.setBounds(110, 145, 342, 20);
            
            // ---- OBJ_87 ----
            OBJ_87.setText("@LIBMCD@");
            OBJ_87.setName("OBJ_87");
            panel2.add(OBJ_87);
            OBJ_87.setBounds(110, 115, 342, 20);
            
            // ---- OBJ_60 ----
            OBJ_60.setText("Bases TVA");
            OBJ_60.setName("OBJ_60");
            panel2.add(OBJ_60);
            OBJ_60.setBounds(20, 19, 90, 20);
            
            // ---- OBJ_66 ----
            OBJ_66.setText("Montants TVA");
            OBJ_66.setName("OBJ_66");
            panel2.add(OBJ_66);
            OBJ_66.setBounds(20, 49, 90, 20);
            
            // ---- OBJ_72 ----
            OBJ_72.setText("Codes TVA");
            OBJ_72.setName("OBJ_72");
            panel2.add(OBJ_72);
            OBJ_72.setBounds(20, 79, 90, 20);
            
            // ---- WTHTX ----
            WTHTX.setComponentPopupMenu(BTD);
            WTHTX.setHorizontalAlignment(SwingConstants.RIGHT);
            WTHTX.setName("WTHTX");
            panel2.add(WTHTX);
            WTHTX.setBounds(785, 10, 140, WTHTX.getPreferredSize().height);
            
            // ---- WTVAX ----
            WTVAX.setComponentPopupMenu(null);
            WTVAX.setHorizontalAlignment(SwingConstants.RIGHT);
            WTVAX.setName("WTVAX");
            panel2.add(WTVAX);
            WTVAX.setBounds(785, 60, 140, WTVAX.getPreferredSize().height);
            
            // ---- WTTCX ----
            WTTCX.setComponentPopupMenu(BTD);
            WTTCX.setHorizontalAlignment(SwingConstants.RIGHT);
            WTTCX.setName("WTTCX");
            panel2.add(WTTCX);
            WTTCX.setBounds(785, 85, 140, WTTCX.getPreferredSize().height);
            
            // ---- WSL1X ----
            WSL1X.setComponentPopupMenu(null);
            WSL1X.setHorizontalAlignment(SwingConstants.RIGHT);
            WSL1X.setName("WSL1X");
            panel2.add(WSL1X);
            WSL1X.setBounds(110, 15, 140, WSL1X.getPreferredSize().height);
            
            // ---- WSL2X ----
            WSL2X.setComponentPopupMenu(null);
            WSL2X.setHorizontalAlignment(SwingConstants.RIGHT);
            WSL2X.setName("WSL2X");
            panel2.add(WSL2X);
            WSL2X.setBounds(250, 15, 140, WSL2X.getPreferredSize().height);
            
            // ---- WSL3X ----
            WSL3X.setComponentPopupMenu(null);
            WSL3X.setHorizontalAlignment(SwingConstants.RIGHT);
            WSL3X.setName("WSL3X");
            panel2.add(WSL3X);
            WSL3X.setBounds(390, 15, 140, WSL3X.getPreferredSize().height);
            
            // ---- WTL1X ----
            WTL1X.setComponentPopupMenu(null);
            WTL1X.setHorizontalAlignment(SwingConstants.RIGHT);
            WTL1X.setName("WTL1X");
            panel2.add(WTL1X);
            WTL1X.setBounds(110, 45, 140, WTL1X.getPreferredSize().height);
            
            // ---- WTL2X ----
            WTL2X.setComponentPopupMenu(null);
            WTL2X.setHorizontalAlignment(SwingConstants.RIGHT);
            WTL2X.setName("WTL2X");
            panel2.add(WTL2X);
            WTL2X.setBounds(250, 45, 140, WTL2X.getPreferredSize().height);
            
            // ---- WTL3X ----
            WTL3X.setComponentPopupMenu(null);
            WTL3X.setHorizontalAlignment(SwingConstants.RIGHT);
            WTL3X.setName("WTL3X");
            panel2.add(WTL3X);
            WTL3X.setBounds(390, 45, 140, WTL3X.getPreferredSize().height);
            
            // ---- OBJ_86 ----
            OBJ_86.setIcon(new ImageIcon("images/avert.gif"));
            OBJ_86.setName("OBJ_86");
            panel2.add(OBJ_86);
            OBJ_86.setBounds(20, 120, 42, 36);
            
            // ---- OBJ_76 ----
            OBJ_76.setText("Total TTC");
            OBJ_76.setName("OBJ_76");
            panel2.add(OBJ_76);
            OBJ_76.setBounds(685, 85, 100, 28);
            
            // ---- OBJ_70 ----
            OBJ_70.setText("Total TVA");
            OBJ_70.setName("OBJ_70");
            panel2.add(OBJ_70);
            OBJ_70.setBounds(685, 60, 100, 28);
            
            // ---- OBJ_64 ----
            OBJ_64.setText("Total H.T");
            OBJ_64.setName("OBJ_64");
            panel2.add(OBJ_64);
            OBJ_64.setBounds(685, 10, 100, 28);
            
            // ---- WLT1 ----
            WLT1.setComponentPopupMenu(null);
            WLT1.setName("WLT1");
            panel2.add(WLT1);
            WLT1.setBounds(110, 75, 58, WLT1.getPreferredSize().height);
            
            // ---- WLT2 ----
            WLT2.setComponentPopupMenu(null);
            WLT2.setName("WLT2");
            panel2.add(WLT2);
            WLT2.setBounds(250, 75, 58, WLT2.getPreferredSize().height);
            
            // ---- WLT3 ----
            WLT3.setComponentPopupMenu(null);
            WLT3.setName("WLT3");
            panel2.add(WLT3);
            WLT3.setBounds(390, 75, 66, WLT3.getPreferredSize().height);
            
            // ---- OBJ_78 ----
            OBJ_78.setText("@TVA1@");
            OBJ_78.setName("OBJ_78");
            panel2.add(OBJ_78);
            OBJ_78.setBounds(175, 79, 18, 20);
            
            // ---- OBJ_79 ----
            OBJ_79.setText("@TVA2@");
            OBJ_79.setName("OBJ_79");
            panel2.add(OBJ_79);
            OBJ_79.setBounds(310, 79, 18, 20);
            
            // ---- OBJ_80 ----
            OBJ_80.setText("@TVA3@");
            OBJ_80.setName("OBJ_80");
            panel2.add(OBJ_80);
            OBJ_80.setBounds(460, 79, 18, 20);
            
            // ---- OBJ_83 ----
            OBJ_83.setText("@BAS@");
            OBJ_83.setName("OBJ_83");
            panel2.add(OBJ_83);
            OBJ_83.setBounds(815, 110, 58, 25);
            
            // ---- OBJ_81 ----
            OBJ_81.setText("en");
            OBJ_81.setName("OBJ_81");
            panel2.add(OBJ_81);
            OBJ_81.setBounds(785, 109, 18, 26);
            
            // ---- OBJ_84 ----
            OBJ_84.setText("@EADEV@");
            OBJ_84.setName("OBJ_84");
            panel2.add(OBJ_84);
            OBJ_84.setBounds(889, 110, 36, 25);
            
            // ---- OBJ_88 ----
            OBJ_88.setText("Options");
            OBJ_88.setName("OBJ_88");
            OBJ_88.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBouton1ActionPerformed(e);
              }
            });
            panel2.add(OBJ_88);
            OBJ_88.setBounds(new Rectangle(new Point(730, 135), OBJ_88.getPreferredSize()));
            
            // ---- WPOR1X ----
            WPOR1X.setComponentPopupMenu(BTD);
            WPOR1X.setHorizontalAlignment(SwingConstants.RIGHT);
            WPOR1X.setName("WPOR1X");
            panel2.add(WPOR1X);
            WPOR1X.setBounds(785, 35, 140, WPOR1X.getPreferredSize().height);
            
            // ---- OBJ_65 ----
            OBJ_65.setText("Dont port H.T");
            OBJ_65.setName("OBJ_65");
            panel2.add(OBJ_65);
            OBJ_65.setBounds(685, 35, 100, 28);
            
            // ---- LPORTF ----
            LPORTF.setText("@LPORTF@");
            LPORTF.setName("LPORTF");
            panel2.add(LPORTF);
            LPORTF.setBounds(465, 145, 307, 20);
            
            // ---- WREP ----
            WREP.setName("WREP");
            panel2.add(WREP);
            WREP.setBounds(875, 136, 50, WREP.getPreferredSize().height);
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addGroup(p_contenuLayout.createParallelGroup().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 974, Short.MAX_VALUE)
                      .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 974, Short.MAX_VALUE))
                  .addContainerGap()));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(panel1, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE).addContainerGap(22, Short.MAX_VALUE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Convertisseur mon\u00e9taire");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField EAETB;
  private XRiTextField WNUM;
  private XRiTextField WSUF;
  private RiZoneSortie OBJ_73;
  private JLabel WDATEX;
  private JLabel OBJ_51;
  private JLabel OBJ_54;
  private JLabel OBJ_55;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST4;
  private XRiTable LD01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_52;
  private JLabel OBJ_53;
  private JPanel panel2;
  private JLabel OBJ_89;
  private JLabel OBJ_87;
  private JLabel OBJ_60;
  private JLabel OBJ_66;
  private JLabel OBJ_72;
  private XRiTextField WTHTX;
  private XRiTextField WTVAX;
  private XRiTextField WTTCX;
  private XRiTextField WSL1X;
  private XRiTextField WSL2X;
  private XRiTextField WSL3X;
  private XRiTextField WTL1X;
  private XRiTextField WTL2X;
  private XRiTextField WTL3X;
  private JLabel OBJ_86;
  private JLabel OBJ_76;
  private JLabel OBJ_70;
  private JLabel OBJ_64;
  private XRiTextField WLT1;
  private XRiTextField WLT2;
  private XRiTextField WLT3;
  private JLabel OBJ_78;
  private JLabel OBJ_79;
  private JLabel OBJ_80;
  private JLabel OBJ_83;
  private JLabel OBJ_81;
  private JLabel OBJ_84;
  private SNBoutonLeger OBJ_88;
  private XRiTextField WPOR1X;
  private JLabel OBJ_65;
  private JLabel LPORTF;
  private XRiTextField WREP;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
