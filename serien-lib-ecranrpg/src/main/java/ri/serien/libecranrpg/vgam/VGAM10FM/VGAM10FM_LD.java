
package ri.serien.libecranrpg.vgam.VGAM10FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM10FM_LD extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _T01_Top =
      { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12", "T13", "T14", "T15", };
  private String[] _T01_Title = { "TETLD", };
  private String[][] _T01_Data = { { "L01", }, { "L02", }, { "L03", }, { "L04", }, { "L05", }, { "L06", }, { "L07", }, { "L08", },
      { "L09", }, { "L10", }, { "L11", }, { "L12", }, { "L13", }, { "L14", }, { "L15", }, };
  private int[] _T01_Width = { 600, };
  
  public VGAM10FM_LD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TOPLIT.setValeursSelection("1", " ");
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITRE@")).trim());
    OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOM@")).trim());
    DBUT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DBUT@")).trim());
    OBJ_110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P10RES@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_112.setVisible(lexique.isPresent("WSELZ"));
    OBJ_85.setVisible(!lexique.HostFieldGetData("DEMA2").trim().equalsIgnoreCase("Demandeur") & lexique.isPresent("WACH"));
    OBJ_114.setVisible(lexique.HostFieldGetData("WSELZ").equalsIgnoreCase(""));
    OBJ_113.setVisible(lexique.HostFieldGetData("WSELZ").equalsIgnoreCase(""));
    OBJ_70.setVisible(lexique.isPresent("WMEX"));
    OBJ_110.setVisible(lexique.isPresent("P10RES"));
    OBJ_90.setVisible(lexique.isPresent("WCCT"));
    OBJ_77.setVisible(lexique.isPresent("WCTR"));
    
    // Titre
    if (lexique.isTrue("87")) {
      p_bpresentation.setText(interpreteurD.analyseExpression("@TITRE@ Imputation de frais"));
    }
    else if (lexique.isTrue("83")) {
      p_bpresentation.setText(interpreteurD.analyseExpression("@TITRE@ Rapprochement"));
      
    }
    else {
      p_bpresentation.setText(interpreteurD.analyseExpression("@TITRE@"));
    }
    
    // Bouton droit sur la liste
    if (lexique.HostFieldGetData("LOPT").contains("Rap._global")) {
      T01.setComponentPopupMenu(BTD);
    }
    else {
      T01.setComponentPopupMenu(BTD2);
    }
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "1", "Enter");
    T01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "8", "Enter");
    T01.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "A", "Enter");
    T01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // libexporexcel = "Exportation du bon achat"
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    // libexporexcel = "Exportation du bon achat"
    // lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void T01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _T01_Top, "1", "Enter", e);
    if (T01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _T01_Top, "6", "Enter");
    T01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void imputActionPerformed(ActionEvent e) {
    T01.setValeurTop("+");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void annimputActionPerformed(ActionEvent e) {
    T01.setValeurTop("-");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_64 = new RiZoneSortie();
    OBJ_57 = new JLabel();
    WNUM = new XRiTextField();
    WETB = new XRiTextField();
    OBJ_55 = new JLabel();
    WSUF = new XRiTextField();
    BT_ChgSoc = new SNBoutonDetail();
    OBJ_56 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_93 = new JLabel();
    OBJ_90 = new JLabel();
    LIBLIT = new XRiTextField();
    WCCT = new XRiTextField();
    TOPLIT = new XRiCheckBox();
    P10DAT = new XRiCalendrier();
    WACH_doublon_73 = new XRiTextField();
    panel3 = new JPanel();
    OBJ_75 = new JLabel();
    OBJ_113 = new JButton();
    OBJ_114 = new JButton();
    OBJ_85 = new JLabel();
    OBJ_72 = new JLabel();
    WBOND = new XRiTextField();
    WFRS = new XRiTextField();
    OBJ_73 = new JLabel();
    WSAN = new XRiTextField();
    OBJ_74 = new JLabel();
    WACT = new XRiTextField();
    WSELZ = new XRiTextField();
    OBJ_112 = new JLabel();
    WACH = new XRiTextField();
    WCOL = new XRiTextField();
    panel4 = new JPanel();
    OBJ_77 = new JLabel();
    OBJ_70 = new JLabel();
    WMEX = new XRiTextField();
    WCTR = new XRiTextField();
    panel2 = new JPanel();
    panel5 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    T01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    menuItem1 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    OBJ_32 = new JMenuItem();
    DBUT = new JLabel();
    OBJ_110 = new JLabel();
    BTD2 = new JPopupMenu();
    imput = new JMenuItem();
    annimput = new JMenuItem();
    OBJ_39 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(970, 660));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITRE@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(940, 40));
          p_tete_gauche.setMinimumSize(new Dimension(940, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_64 ----
          OBJ_64.setText("@FRNOM@");
          OBJ_64.setOpaque(false);
          OBJ_64.setName("OBJ_64");
          
          // ---- OBJ_57 ----
          OBJ_57.setText("Num\u00e9ro");
          OBJ_57.setName("OBJ_57");
          
          // ---- WNUM ----
          WNUM.setComponentPopupMenu(BTD);
          WNUM.setName("WNUM");
          
          // ---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          
          // ---- OBJ_55 ----
          OBJ_55.setText("Etablissement");
          OBJ_55.setName("OBJ_55");
          
          // ---- WSUF ----
          WSUF.setComponentPopupMenu(BTD);
          WSUF.setName("WSUF");
          
          // ---- BT_ChgSoc ----
          BT_ChgSoc.setText("");
          BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
            }
          });
          
          // ---- OBJ_56 ----
          OBJ_56.setText("Fournisseur");
          OBJ_56.setName("OBJ_56");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(10, 10, 10).addComponent(OBJ_55).addGap(10, 10, 10)
                  .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                  .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
                  .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE).addGap(41, 41, 41)
                  .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE).addGap(155, 155, 155)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_55))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(WETB, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE,
                  29, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(WNUM, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(WSUF, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(3, 3, 3).addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Tri par groupe/famille");
              riSousMenu_bt6.setToolTipText("D\u00e9tail des lignes tri\u00e9es par groupe/famille");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Centralisation");
              riSousMenu_bt7.setToolTipText("Centralisation");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Suivant");
              riSousMenu_bt8.setToolTipText("Suivant");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(770, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);
          
          // ======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- OBJ_93 ----
            OBJ_93.setText("Date de traitement");
            OBJ_93.setName("OBJ_93");
            panel1.add(OBJ_93);
            OBJ_93.setBounds(490, 115, 110, 20);
            
            // ---- OBJ_90 ----
            OBJ_90.setText("Commande initiale");
            OBJ_90.setName("OBJ_90");
            panel1.add(OBJ_90);
            OBJ_90.setBounds(200, 115, 135, 20);
            
            // ---- LIBLIT ----
            LIBLIT.setName("LIBLIT");
            panel1.add(LIBLIT);
            LIBLIT.setBounds(80, 111, 99, LIBLIT.getPreferredSize().height);
            
            // ---- WCCT ----
            WCCT.setComponentPopupMenu(BTD);
            WCCT.setName("WCCT");
            panel1.add(WCCT);
            WCCT.setBounds(330, 110, 110, WCCT.getPreferredSize().height);
            
            // ---- TOPLIT ----
            TOPLIT.setText("Litige");
            TOPLIT.setComponentPopupMenu(BTD);
            TOPLIT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TOPLIT.setName("TOPLIT");
            panel1.add(TOPLIT);
            TOPLIT.setBounds(15, 115, 80, 20);
            
            // ---- P10DAT ----
            P10DAT.setName("P10DAT");
            panel1.add(P10DAT);
            P10DAT.setBounds(610, 110, 105, P10DAT.getPreferredSize().height);
            
            // ---- WACH_doublon_73 ----
            WACH_doublon_73.setToolTipText("Acheteur");
            WACH_doublon_73.setComponentPopupMenu(BTD);
            WACH_doublon_73.setName("WACH_doublon_73");
            panel1.add(WACH_doublon_73);
            WACH_doublon_73.setBounds(400, 110, 40, WACH_doublon_73.getPreferredSize().height);
            
            // ======== panel3 ========
            {
              panel3.setBorder(new TitledBorder(""));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);
              
              // ---- OBJ_75 ----
              OBJ_75.setText("Fournisseur");
              OBJ_75.setName("OBJ_75");
              panel3.add(OBJ_75);
              OBJ_75.setBounds(295, 20, 85, 20);
              
              // ---- OBJ_113 ----
              OBJ_113.setText("Autres");
              OBJ_113.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_113.setName("OBJ_113");
              panel3.add(OBJ_113);
              OBJ_113.setBounds(215, 45, 66, 24);
              
              // ---- OBJ_114 ----
              OBJ_114.setText("Autres");
              OBJ_114.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_114.setName("OBJ_114");
              panel3.add(OBJ_114);
              OBJ_114.setBounds(150, 45, 70, 24);
              
              // ---- OBJ_85 ----
              OBJ_85.setText("Acheteur");
              OBJ_85.setName("OBJ_85");
              panel3.add(OBJ_85);
              OBJ_85.setBounds(385, 20, 56, 20);
              
              // ---- OBJ_72 ----
              OBJ_72.setText("N\u00b0 d\u00e9but");
              OBJ_72.setName("OBJ_72");
              panel3.add(OBJ_72);
              OBJ_72.setBounds(20, 20, 54, 20);
              
              // ---- WBOND ----
              WBOND.setComponentPopupMenu(BTD);
              WBOND.setName("WBOND");
              panel3.add(WBOND);
              WBOND.setBounds(20, 43, 60, WBOND.getPreferredSize().height);
              
              // ---- WFRS ----
              WFRS.setComponentPopupMenu(BTD);
              WFRS.setName("WFRS");
              panel3.add(WFRS);
              WFRS.setBounds(310, 43, 60, WFRS.getPreferredSize().height);
              
              // ---- OBJ_73 ----
              OBJ_73.setText("Section");
              OBJ_73.setName("OBJ_73");
              panel3.add(OBJ_73);
              OBJ_73.setBounds(95, 20, 48, 20);
              
              // ---- WSAN ----
              WSAN.setComponentPopupMenu(BTD);
              WSAN.setName("WSAN");
              panel3.add(WSAN);
              WSAN.setBounds(90, 43, 50, WSAN.getPreferredSize().height);
              
              // ---- OBJ_74 ----
              OBJ_74.setText("Affaire");
              OBJ_74.setName("OBJ_74");
              panel3.add(OBJ_74);
              OBJ_74.setBounds(155, 20, 42, 20);
              
              // ---- WACT ----
              WACT.setComponentPopupMenu(BTD);
              WACT.setName("WACT");
              panel3.add(WACT);
              WACT.setBounds(150, 43, 50, WACT.getPreferredSize().height);
              
              // ---- WSELZ ----
              WSELZ.setComponentPopupMenu(BTD);
              WSELZ.setName("WSELZ");
              panel3.add(WSELZ);
              WSELZ.setBounds(215, 43, 60, WSELZ.getPreferredSize().height);
              
              // ---- OBJ_112 ----
              OBJ_112.setText("Etats");
              OBJ_112.setName("OBJ_112");
              panel3.add(OBJ_112);
              OBJ_112.setBounds(220, 20, 45, 20);
              
              // ---- WACH ----
              WACH.setToolTipText("Acheteur");
              WACH.setComponentPopupMenu(BTD);
              WACH.setName("WACH");
              panel3.add(WACH);
              WACH.setBounds(385, 43, 44, WACH.getPreferredSize().height);
              
              // ---- WCOL ----
              WCOL.setComponentPopupMenu(BTD);
              WCOL.setName("WCOL");
              panel3.add(WCOL);
              WCOL.setBounds(290, 43, 20, WCOL.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel3);
            panel3.setBounds(10, 10, 455, 90);
            
            // ======== panel4 ========
            {
              panel4.setBorder(new TitledBorder(""));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);
              
              // ---- OBJ_77 ----
              OBJ_77.setText("Code transporteur");
              OBJ_77.setName("OBJ_77");
              panel4.add(OBJ_77);
              OBJ_77.setBounds(20, 50, 116, 20);
              
              // ---- OBJ_70 ----
              OBJ_70.setText("Mode dexp\u00e9dition");
              OBJ_70.setName("OBJ_70");
              panel4.add(OBJ_70);
              OBJ_70.setBounds(20, 14, 102, 20);
              
              // ---- WMEX ----
              WMEX.setComponentPopupMenu(BTD);
              WMEX.setName("WMEX");
              panel4.add(WMEX);
              WMEX.setBounds(155, 10, 34, WMEX.getPreferredSize().height);
              
              // ---- WCTR ----
              WCTR.setComponentPopupMenu(BTD);
              WCTR.setName("WCTR");
              panel4.add(WCTR);
              WCTR.setBounds(155, 46, 34, WCTR.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel4);
            panel4.setBounds(520, 10, 215, 90);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(11, 20, 749, 157);
          
          // ======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ======== panel5 ========
            {
              panel5.setBorder(new TitledBorder(""));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);
              
              // ======== SCROLLPANE_LIST ========
              {
                SCROLLPANE_LIST.setComponentPopupMenu(BTD);
                SCROLLPANE_LIST.setName("SCROLLPANE_LIST");
                
                // ---- T01 ----
                T01.setComponentPopupMenu(BTD);
                T01.setName("T01");
                T01.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    T01MouseClicked(e);
                  }
                });
                SCROLLPANE_LIST.setViewportView(T01);
              }
              panel5.add(SCROLLPANE_LIST);
              SCROLLPANE_LIST.setBounds(25, 25, 635, 270);
              
              // ---- BT_PGUP ----
              BT_PGUP.setText("");
              BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
              BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGUP.setName("BT_PGUP");
              panel5.add(BT_PGUP);
              BT_PGUP.setBounds(670, 25, 25, 124);
              
              // ---- BT_PGDOWN ----
              BT_PGDOWN.setText("");
              BT_PGDOWN.setToolTipText("Page suivante");
              BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGDOWN.setName("BT_PGDOWN");
              panel5.add(BT_PGDOWN);
              BT_PGDOWN.setBounds(670, 170, 25, 124);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            panel2.add(panel5);
            panel5.setBounds(15, 10, 720, 320);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(10, 190, 749, 360);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
      
      // ---- menuItem1 ----
      menuItem1.setText("Rapprochement global");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD.add(menuItem1);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Dossier");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Affichage");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
      
      // ---- OBJ_27 ----
      OBJ_27.setText("D\u00e9verrouiller");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);
      
      // ---- OBJ_28 ----
      OBJ_28.setText("Conversion");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed(e);
        }
      });
      BTD.add(OBJ_28);
      
      // ---- OBJ_29 ----
      OBJ_29.setText("D\u00e9-rapprochement");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD.add(OBJ_29);
      
      // ---- OBJ_30 ----
      OBJ_30.setText("Exportation du bon vers excel");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed(e);
        }
      });
      BTD.add(OBJ_30);
      
      // ---- OBJ_31 ----
      OBJ_31.setText("Planning des arrivages");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD.add(OBJ_31);
      BTD.addSeparator();
      
      // ---- OBJ_32 ----
      OBJ_32.setText("Aide en ligne");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      BTD.add(OBJ_32);
    }
    
    // ---- DBUT ----
    DBUT.setText("@DBUT@");
    DBUT.setName("DBUT");
    
    // ---- OBJ_110 ----
    OBJ_110.setText("@P10RES@");
    OBJ_110.setName("OBJ_110");
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- imput ----
      imput.setText("Imputation");
      imput.setName("imput");
      imput.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          imputActionPerformed(e);
        }
      });
      BTD2.add(imput);
      
      // ---- annimput ----
      annimput.setText("Annulation d'imputation");
      annimput.setName("annimput");
      annimput.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          annimputActionPerformed(e);
        }
      });
      BTD2.add(annimput);
      BTD2.addSeparator();
      
      // ---- OBJ_39 ----
      OBJ_39.setText("Aide en ligne");
      OBJ_39.setName("OBJ_39");
      OBJ_39.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_39);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie OBJ_64;
  private JLabel OBJ_57;
  private XRiTextField WNUM;
  private XRiTextField WETB;
  private JLabel OBJ_55;
  private XRiTextField WSUF;
  private SNBoutonDetail BT_ChgSoc;
  private JLabel OBJ_56;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_93;
  private JLabel OBJ_90;
  private XRiTextField LIBLIT;
  private XRiTextField WCCT;
  private XRiCheckBox TOPLIT;
  private XRiCalendrier P10DAT;
  private XRiTextField WACH_doublon_73;
  private JPanel panel3;
  private JLabel OBJ_75;
  private JButton OBJ_113;
  private JButton OBJ_114;
  private JLabel OBJ_85;
  private JLabel OBJ_72;
  private XRiTextField WBOND;
  private XRiTextField WFRS;
  private JLabel OBJ_73;
  private XRiTextField WSAN;
  private JLabel OBJ_74;
  private XRiTextField WACT;
  private XRiTextField WSELZ;
  private JLabel OBJ_112;
  private XRiTextField WACH;
  private XRiTextField WCOL;
  private JPanel panel4;
  private JLabel OBJ_77;
  private JLabel OBJ_70;
  private XRiTextField WMEX;
  private XRiTextField WCTR;
  private JPanel panel2;
  private JPanel panel5;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable T01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem menuItem1;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_28;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_30;
  private JMenuItem OBJ_31;
  private JMenuItem OBJ_32;
  private JLabel DBUT;
  private JLabel OBJ_110;
  private JPopupMenu BTD2;
  private JMenuItem imput;
  private JMenuItem annimput;
  private JMenuItem OBJ_39;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
