
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_DG extends SNPanelEcranRPG implements ioFrame {
  
  private String[] DGRON_Value = { "", "1", "2", "3", "4", "5", "6", };
  
  public VGAM01FX_DG(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    DGRON.setValeurs(DGRON_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    DVLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // DGRON.setSelectedIndex(getIndice("DGRON", DGRON_Value));
    DGDEV.setVisible(lexique.isPresent("DGDEV"));
    DVLIB.setVisible(lexique.isPresent("DVLIB"));
    OBJ_62.setVisible(lexique.isPresent("DGDEV"));
    DGRON.setVisible(!lexique.isTrue("09"));
    OBJ_89.setVisible(!lexique.isTrue("09"));
    DGMAGX.setVisible(!lexique.isTrue("09"));
    OBJ_60.setVisible(!lexique.isTrue("09"));
    DGETPX.setVisible(lexique.isTrue("09"));
    OBJ_63.setVisible(lexique.isTrue("09"));
    
    // panel3.setVisible(!lexique.isTrue("16"));
    panel4.setVisible(!lexique.isTrue("09"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("DGRON", 0, DGRON_Value[DGRON.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_46 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_48 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_50 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel1 = new JPanel();
    DGNOM = new XRiTextField();
    DGCPL = new XRiTextField();
    DVLIB = new RiZoneSortie();
    OBJ_56 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_62 = new JLabel();
    DGDEV = new XRiTextField();
    DGMAGX = new XRiTextField();
    OBJ_63 = new JLabel();
    DGETPX = new XRiTextField();
    panel2 = new JPanel();
    OBJ_71 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_76 = new JLabel();
    DGT01 = new XRiTextField();
    DGT02 = new XRiTextField();
    DGT03 = new XRiTextField();
    DGT04 = new XRiTextField();
    DGT05 = new XRiTextField();
    DGT06 = new XRiTextField();
    OBJ_77 = new JLabel();
    OBJ_87 = new JLabel();
    DGTPF = new XRiTextField();
    OBJ_86 = new JLabel();
    OBJ_89 = new JLabel();
    DGRON = new XRiComboBox();
    panel3 = new JPanel();
    OBJ_94 = new JLabel();
    DGCMA = new XRiTextField();
    panel4 = new JPanel();
    OBJ_103 = new JLabel();
    OBJ_106 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_110 = new JLabel();
    DGDEBX = new XRiCalendrier();
    DGFINX = new XRiCalendrier();
    DGDE1X = new XRiCalendrier();
    DGFE1X = new XRiCalendrier();
    DGDE2X = new XRiCalendrier();
    DGFE2X = new XRiCalendrier();
    DGDECX = new XRiCalendrier();
    DGMEX = new XRiTextField();
    OBJ_101 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_112 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_46 ----
          OBJ_46.setText("Etablissement");
          OBJ_46.setName("OBJ_46");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_48 ----
          OBJ_48.setText("Code");
          OBJ_48.setName("OBJ_48");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_50 ----
          OBJ_50.setText("Ordre");
          OBJ_50.setName("OBJ_50");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(820, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Description g\u00e9n\u00e9rale");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder(""));
              panel1.setOpaque(false);
              panel1.setComponentPopupMenu(null);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- DGNOM ----
              DGNOM.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              DGNOM.setComponentPopupMenu(null);
              DGNOM.setName("DGNOM");
              panel1.add(DGNOM);
              DGNOM.setBounds(210, 10, 290, DGNOM.getPreferredSize().height);

              //---- DGCPL ----
              DGCPL.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              DGCPL.setComponentPopupMenu(null);
              DGCPL.setName("DGCPL");
              panel1.add(DGCPL);
              DGCPL.setBounds(210, 45, 290, DGCPL.getPreferredSize().height);

              //---- DVLIB ----
              DVLIB.setText("@DVLIB@");
              DVLIB.setComponentPopupMenu(null);
              DVLIB.setName("DVLIB");
              panel1.add(DVLIB);
              DVLIB.setBounds(255, 82, 245, DVLIB.getPreferredSize().height);

              //---- OBJ_56 ----
              OBJ_56.setText("Nom ou raison sociale");
              OBJ_56.setName("OBJ_56");
              panel1.add(OBJ_56);
              OBJ_56.setBounds(40, 14, 147, 20);

              //---- OBJ_58 ----
              OBJ_58.setText("Compl\u00e9ment de nom");
              OBJ_58.setName("OBJ_58");
              panel1.add(OBJ_58);
              OBJ_58.setBounds(40, 49, 127, 20);

              //---- OBJ_60 ----
              OBJ_60.setText("Magasin g\u00e9n\u00e9ral");
              OBJ_60.setComponentPopupMenu(null);
              OBJ_60.setName("OBJ_60");
              panel1.add(OBJ_60);
              OBJ_60.setBounds(545, 14, 105, 20);

              //---- OBJ_62 ----
              OBJ_62.setText("Devise locale");
              OBJ_62.setComponentPopupMenu(null);
              OBJ_62.setName("OBJ_62");
              panel1.add(OBJ_62);
              OBJ_62.setBounds(40, 84, 100, 20);

              //---- DGDEV ----
              DGDEV.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              DGDEV.setComponentPopupMenu(null);
              DGDEV.setName("DGDEV");
              panel1.add(DGDEV);
              DGDEV.setBounds(210, 80, 40, DGDEV.getPreferredSize().height);

              //---- DGMAGX ----
              DGMAGX.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              DGMAGX.setComponentPopupMenu(null);
              DGMAGX.setName("DGMAGX");
              panel1.add(DGMAGX);
              DGMAGX.setBounds(690, 10, 30, DGMAGX.getPreferredSize().height);

              //---- OBJ_63 ----
              OBJ_63.setText("Etablissement pilote");
              OBJ_63.setName("OBJ_63");
              panel1.add(OBJ_63);
              OBJ_63.setBounds(40, 84, 124, 20);

              //---- DGETPX ----
              DGETPX.setComponentPopupMenu(BTD);
              DGETPX.setName("DGETPX");
              panel1.add(DGETPX);
              DGETPX.setBounds(210, 80, 40, DGETPX.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(15, 10, 745, 123);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("T.V.A."));
              panel2.setOpaque(false);
              panel2.setComponentPopupMenu(null);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_71 ----
              OBJ_71.setText("Code 1");
              OBJ_71.setComponentPopupMenu(null);
              OBJ_71.setName("OBJ_71");
              panel2.add(OBJ_71);
              OBJ_71.setBounds(210, 30, 49, 20);

              //---- OBJ_72 ----
              OBJ_72.setText("Code 2");
              OBJ_72.setComponentPopupMenu(null);
              OBJ_72.setName("OBJ_72");
              panel2.add(OBJ_72);
              OBJ_72.setBounds(285, 30, 49, 20);

              //---- OBJ_73 ----
              OBJ_73.setText("Code 3");
              OBJ_73.setComponentPopupMenu(null);
              OBJ_73.setName("OBJ_73");
              panel2.add(OBJ_73);
              OBJ_73.setBounds(350, 30, 49, 20);

              //---- OBJ_74 ----
              OBJ_74.setText("Code 4");
              OBJ_74.setComponentPopupMenu(null);
              OBJ_74.setName("OBJ_74");
              panel2.add(OBJ_74);
              OBJ_74.setBounds(415, 30, 49, 20);

              //---- OBJ_75 ----
              OBJ_75.setText("Code 5");
              OBJ_75.setComponentPopupMenu(null);
              OBJ_75.setName("OBJ_75");
              panel2.add(OBJ_75);
              OBJ_75.setBounds(480, 30, 49, 20);

              //---- OBJ_76 ----
              OBJ_76.setText("Code 6");
              OBJ_76.setComponentPopupMenu(null);
              OBJ_76.setName("OBJ_76");
              panel2.add(OBJ_76);
              OBJ_76.setBounds(545, 30, 49, 20);

              //---- DGT01 ----
              DGT01.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              DGT01.setComponentPopupMenu(null);
              DGT01.setName("DGT01");
              panel2.add(DGT01);
              DGT01.setBounds(210, 50, 58, DGT01.getPreferredSize().height);

              //---- DGT02 ----
              DGT02.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              DGT02.setComponentPopupMenu(null);
              DGT02.setName("DGT02");
              panel2.add(DGT02);
              DGT02.setBounds(277, 50, 58, DGT02.getPreferredSize().height);

              //---- DGT03 ----
              DGT03.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              DGT03.setComponentPopupMenu(null);
              DGT03.setName("DGT03");
              panel2.add(DGT03);
              DGT03.setBounds(344, 50, 58, DGT03.getPreferredSize().height);

              //---- DGT04 ----
              DGT04.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              DGT04.setComponentPopupMenu(null);
              DGT04.setName("DGT04");
              panel2.add(DGT04);
              DGT04.setBounds(411, 50, 58, DGT04.getPreferredSize().height);

              //---- DGT05 ----
              DGT05.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              DGT05.setComponentPopupMenu(null);
              DGT05.setName("DGT05");
              panel2.add(DGT05);
              DGT05.setBounds(478, 50, 58, DGT05.getPreferredSize().height);

              //---- DGT06 ----
              DGT06.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              DGT06.setComponentPopupMenu(null);
              DGT06.setName("DGT06");
              panel2.add(DGT06);
              DGT06.setBounds(545, 50, 58, DGT06.getPreferredSize().height);

              //---- OBJ_77 ----
              OBJ_77.setText("Taux");
              OBJ_77.setName("OBJ_77");
              panel2.add(OBJ_77);
              OBJ_77.setBounds(40, 54, 33, 20);

              //---- OBJ_87 ----
              OBJ_87.setText("Taxe parafiscale");
              OBJ_87.setName("OBJ_87");
              panel2.add(OBJ_87);
              OBJ_87.setBounds(40, 84, 105, 20);

              //---- DGTPF ----
              DGTPF.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              DGTPF.setComponentPopupMenu(null);
              DGTPF.setName("DGTPF");
              panel2.add(DGTPF);
              DGTPF.setBounds(277, 80, 58, DGTPF.getPreferredSize().height);

              //---- OBJ_86 ----
              OBJ_86.setText("Taux");
              OBJ_86.setComponentPopupMenu(null);
              OBJ_86.setName("OBJ_86");
              panel2.add(OBJ_86);
              OBJ_86.setBounds(210, 86, 33, 16);

              //---- OBJ_89 ----
              OBJ_89.setText("Arrondi");
              OBJ_89.setComponentPopupMenu(null);
              OBJ_89.setName("OBJ_89");
              panel2.add(OBJ_89);
              OBJ_89.setBounds(411, 86, 46, 16);

              //---- DGRON ----
              DGRON.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "0,05",
                "0,10",
                "0,25",
                "0,50",
                "1,00",
                "10,00"
              }));
              DGRON.setComponentPopupMenu(null);
              DGRON.setName("DGRON");
              panel2.add(DGRON);
              DGRON.setBounds(478, 81, 65, DGRON.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(15, 139, 745, 123);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Vente"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_94 ----
              OBJ_94.setText("Coefficient normal");
              OBJ_94.setName("OBJ_94");
              panel3.add(OBJ_94);
              OBJ_94.setBounds(40, 32, 109, 20);

              //---- DGCMA ----
              DGCMA.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
              DGCMA.setComponentPopupMenu(null);
              DGCMA.setName("DGCMA");
              panel3.add(DGCMA);
              DGCMA.setBounds(210, 28, 58, DGCMA.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(15, 268, 745, 70);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("Exercices"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- OBJ_103 ----
              OBJ_103.setText("Nombre de mois/Exercice normal");
              OBJ_103.setName("OBJ_103");
              panel4.add(OBJ_103);
              OBJ_103.setBounds(450, 37, 204, 16);

              //---- OBJ_106 ----
              OBJ_106.setText("En cours,                du");
              OBJ_106.setName("OBJ_106");
              panel4.add(OBJ_106);
              OBJ_106.setBounds(40, 69, 124, 20);

              //---- OBJ_99 ----
              OBJ_99.setText("D\u00e9marrage,           du");
              OBJ_99.setName("OBJ_99");
              panel4.add(OBJ_99);
              OBJ_99.setBounds(40, 35, 123, 20);

              //---- OBJ_115 ----
              OBJ_115.setText("Mois en cours");
              OBJ_115.setName("OBJ_115");
              panel4.add(OBJ_115);
              OBJ_115.setBounds(40, 103, 94, 20);

              //---- OBJ_110 ----
              OBJ_110.setText("Suivant, du");
              OBJ_110.setName("OBJ_110");
              panel4.add(OBJ_110);
              OBJ_110.setBounds(450, 69, 69, 20);

              //---- DGDEBX ----
              DGDEBX.setComponentPopupMenu(null);
              DGDEBX.setTypeSaisie(6);
              DGDEBX.setName("DGDEBX");
              panel4.add(DGDEBX);
              DGDEBX.setBounds(210, 31, 90, DGDEBX.getPreferredSize().height);

              //---- DGFINX ----
              DGFINX.setComponentPopupMenu(null);
              DGFINX.setTypeSaisie(6);
              DGFINX.setName("DGFINX");
              panel4.add(DGFINX);
              DGFINX.setBounds(325, 31, 90, DGFINX.getPreferredSize().height);

              //---- DGDE1X ----
              DGDE1X.setTypeSaisie(6);
              DGDE1X.setComponentPopupMenu(null);
              DGDE1X.setName("DGDE1X");
              panel4.add(DGDE1X);
              DGDE1X.setBounds(210, 65, 90, DGDE1X.getPreferredSize().height);

              //---- DGFE1X ----
              DGFE1X.setTypeSaisie(6);
              DGFE1X.setComponentPopupMenu(null);
              DGFE1X.setName("DGFE1X");
              panel4.add(DGFE1X);
              DGFE1X.setBounds(325, 65, 90, DGFE1X.getPreferredSize().height);

              //---- DGDE2X ----
              DGDE2X.setTypeSaisie(6);
              DGDE2X.setComponentPopupMenu(null);
              DGDE2X.setName("DGDE2X");
              panel4.add(DGDE2X);
              DGDE2X.setBounds(520, 65, 90, DGDE2X.getPreferredSize().height);

              //---- DGFE2X ----
              DGFE2X.setTypeSaisie(6);
              DGFE2X.setComponentPopupMenu(null);
              DGFE2X.setName("DGFE2X");
              panel4.add(DGFE2X);
              DGFE2X.setBounds(640, 65, 90, DGFE2X.getPreferredSize().height);

              //---- DGDECX ----
              DGDECX.setToolTipText("sous la forme : MM.AA");
              DGDECX.setComponentPopupMenu(null);
              DGDECX.setTypeSaisie(6);
              DGDECX.setName("DGDECX");
              panel4.add(DGDECX);
              DGDECX.setBounds(210, 99, 90, DGDECX.getPreferredSize().height);

              //---- DGMEX ----
              DGMEX.setComponentPopupMenu(null);
              DGMEX.setName("DGMEX");
              panel4.add(DGMEX);
              DGMEX.setBounds(700, 31, 30, DGMEX.getPreferredSize().height);

              //---- OBJ_101 ----
              OBJ_101.setText("au");
              OBJ_101.setName("OBJ_101");
              panel4.add(OBJ_101);
              OBJ_101.setBounds(305, 35, 18, 20);

              //---- OBJ_108 ----
              OBJ_108.setText("au");
              OBJ_108.setName("OBJ_108");
              panel4.add(OBJ_108);
              OBJ_108.setBounds(305, 69, 18, 20);

              //---- OBJ_112 ----
              OBJ_112.setText("au");
              OBJ_112.setName("OBJ_112");
              panel4.add(OBJ_112);
              OBJ_112.setBounds(615, 69, 18, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel4);
            panel4.setBounds(15, 344, 745, 141);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 783, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 523, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_46;
  private XRiTextField INDETB;
  private JLabel OBJ_48;
  private XRiTextField INDTYP;
  private JLabel OBJ_50;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel1;
  private XRiTextField DGNOM;
  private XRiTextField DGCPL;
  private RiZoneSortie DVLIB;
  private JLabel OBJ_56;
  private JLabel OBJ_58;
  private JLabel OBJ_60;
  private JLabel OBJ_62;
  private XRiTextField DGDEV;
  private XRiTextField DGMAGX;
  private JLabel OBJ_63;
  private XRiTextField DGETPX;
  private JPanel panel2;
  private JLabel OBJ_71;
  private JLabel OBJ_72;
  private JLabel OBJ_73;
  private JLabel OBJ_74;
  private JLabel OBJ_75;
  private JLabel OBJ_76;
  private XRiTextField DGT01;
  private XRiTextField DGT02;
  private XRiTextField DGT03;
  private XRiTextField DGT04;
  private XRiTextField DGT05;
  private XRiTextField DGT06;
  private JLabel OBJ_77;
  private JLabel OBJ_87;
  private XRiTextField DGTPF;
  private JLabel OBJ_86;
  private JLabel OBJ_89;
  private XRiComboBox DGRON;
  private JPanel panel3;
  private JLabel OBJ_94;
  private XRiTextField DGCMA;
  private JPanel panel4;
  private JLabel OBJ_103;
  private JLabel OBJ_106;
  private JLabel OBJ_99;
  private JLabel OBJ_115;
  private JLabel OBJ_110;
  private XRiCalendrier DGDEBX;
  private XRiCalendrier DGFINX;
  private XRiCalendrier DGDE1X;
  private XRiCalendrier DGFE1X;
  private XRiCalendrier DGDE2X;
  private XRiCalendrier DGFE2X;
  private XRiCalendrier DGDECX;
  private XRiTextField DGMEX;
  private JLabel OBJ_101;
  private JLabel OBJ_108;
  private JLabel OBJ_112;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
