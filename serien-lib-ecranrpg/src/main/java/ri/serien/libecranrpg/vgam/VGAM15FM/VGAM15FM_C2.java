
package ri.serien.libecranrpg.vgam.VGAM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM15FM_C2 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] LAIN1_Value = { "", "0", "1", "2", "3", "4", "5", "6", };
  private String[] WA1REA_Value = { "", "0", "1", "2", "3", "4", "5", "6", "7", "8" };
  
  /**
   * Constructeur.
   */
  public VGAM15FM_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    setCloseKey("ENTER", "F12");
    
    // Ajout
    initDiverses();
    LAIN1.setValeurs(LAIN1_Value, null);
    WSER.setValeursSelection("L", " ");
    WA1REA.setValeurs(WA1REA_Value, null);
    
    OBJ_38.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    OBJ_39.setIcon(lexique.chargerImage("images/moins_petit.png", true));
    OBJ_40.setIcon(lexique.chargerImage("images/egal_petit.png", true));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    // Titre
    setTitle("Détail d'une ligne");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WCOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOD@")).trim());
    WNLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNLI@")).trim());
    LAART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAART@")).trim());
    OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAMAG@")).trim());
    OBJ_84.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AVOI@")).trim());
    LADLDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LADLDX@")).trim());
    LAKACX.setToolTipText(
        lexique.TranslationTable(interpreteurD.analyseExpression("Nombre d'unités d'achat par unité de commande @LAUNC@")).trim());
    LAKSCX.setToolTipText(
        lexique.TranslationTable(interpreteurD.analyseExpression("Nombre d'unités de stock par unité de commande (@LAUNC@)")).trim());
    OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAUNA@")).trim());
    OBJ_79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAUNC@")).trim());
    OBJ_144.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAQMI@")).trim());
    OBJ_146.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAQEC@")).trim());
    OBJ_160.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CANUA@")).trim());
    OBJ_92.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EFLIBR@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLATTN@")).trim());
    OBJ_119.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDNFAG@")).trim());
    OBJ_132.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPL1@")).trim());
    OBJ_134.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPL2@")).trim());
    OBJ_136.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPL3@")).trim());
    OBJ_138.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPL4@")).trim());
    OBJ_140.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPL5@")).trim());
    label_deprecie.setText(lexique.TranslationTable(interpreteurD.analyseExpression("DEPRECIE (@A1PDPX@ %)")).trim());
    WLCNA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLCNA@")).trim());
    OBJ_103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CALIBR@")).trim());
    OBJ_101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTESUI@")).trim());
    OBJ_107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADAPX@")).trim());
    OBJ_109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADAFX@")).trim());
    OBJ_111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DAPSUI@")).trim());
    OBJ_113.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DAFSUI@")).trim());
    UPNETX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UPNETX@")).trim());
    OBJ_70.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UC@")).trim());
    OBJ_83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UC2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    riSousMenu2.setVisible(!lexique.HostFieldGetData("LAIN1").trim().isEmpty());
    
    OBJ_38.setVisible(lexique.isPresent("WSTKX"));
    OBJ_39.setVisible(lexique.isPresent("WSTKX"));
    OBJ_81.setVisible(lexique.isPresent("L2QT3X"));
    OBJ_68.setVisible(lexique.isPresent("L2QT2X"));
    OBJ_40.setVisible(lexique.isPresent("WSTKX"));
    OBJ_59.setVisible(LAIN2.isVisible());
    OBJ_56.setVisible(lexique.isPresent("LAUNC"));
    LATP5.setVisible(!lexique.HostFieldGetData("TIZPL5").trim().isEmpty());
    OBJ_140.setVisible(lexique.isPresent("TIZPL5"));
    LATP4.setVisible(!lexique.HostFieldGetData("TIZPL4").trim().isEmpty());
    OBJ_138.setVisible(lexique.isPresent("TIZPL4"));
    LATP3.setVisible(!lexique.HostFieldGetData("TIZPL3").trim().isEmpty());
    OBJ_136.setVisible(lexique.isPresent("TIZPL3"));
    LATP2.setVisible(!lexique.HostFieldGetData("TIZPL2").trim().isEmpty());
    OBJ_134.setVisible(lexique.isPresent("TIZPL2"));
    LATP1.setVisible(!lexique.HostFieldGetData("TIZPL1").trim().isEmpty());
    OBJ_132.setVisible(lexique.isPresent("TIZPL1"));
    OBJ_79.setVisible(lexique.isPresent("A1UNS"));
    OBJ_66.setVisible(lexique.isPresent("LAUNA"));
    OBJ_73.setVisible(lexique.isPresent("LAMAG"));
    OBJ_83.setVisible(lexique.HostFieldGetData("TIZPL5").isEmpty());
    OBJ_62.setVisible(lexique.isPresent("LAACT"));
    OBJ_61.setVisible(lexique.isPresent("LASAN"));
    
    /// WSER
    // WSER.setVisible(lexique.isPresent("WSER"));
    if (!lexique.HostFieldGetData("A1IN2").trim().isEmpty()) {
      WSER.setText("Lots");
      WSER.setValeursSelection("L", " ");
      WSER.setSelected(lexique.HostFieldGetData("WSER").equalsIgnoreCase("L"));
    }
    else if (lexique.HostFieldGetData("A1TSP").trim().equals("S")) {
      WSER.setText("Série");
      WSER.setValeursSelection("S", " ");
      WSER.setSelected(lexique.HostFieldGetData("WSER").equalsIgnoreCase("S"));
    }
    else {
      WSER.setVisible(false);
    }
    
    // Traitement du type LANAT
    String LANAT = lexique.HostFieldGetData("LANAT").trim();
    if (LANAT.equals("M")) {
      z_LANAT.setText("Marchandise");
    }
    else if (LANAT.equals("F")) {
      z_LANAT.setText("Frais");
    }
    else if (LANAT.equals("I")) {
      z_LANAT.setText("Immobilisation");
    }
    else {
      z_LANAT.setText("");
    }
    
    // Si on est en frais, pas de panel date de livraison ni de panel stock
    panel10.setVisible(LANAT.equals("M"));
    
    z_LABRL.setSelected(lexique.HostFieldGetData("LABRL").trim().equals("M"));
    
    if ((lexique.isTrue("N16")) && (LANAT.equals("M"))) {
      panel6.setVisible(true);
      this.setPreferredSize(new Dimension(1075, 625));
    }
    else {
      panel6.setVisible(false);
      this.setPreferredSize(new Dimension(1075, 520));
    }
    
    WIEBC1.setVisible(lexique.isTrue("35"));
    OBJ_126.setVisible(WIEBC1.isVisible());
    WIEBC2.setVisible(lexique.isTrue("35"));
    WIEBCL.setVisible(lexique.isTrue("35"));
    LARP1.setVisible(!lexique.isTrue("35"));
    LARP2.setVisible(!lexique.isTrue("35"));
    LARP3.setVisible(!lexique.isTrue("35"));
    LADLPX.setVisible(true);
    LADLCX.setVisible(true);
    
    label_deprecie.setVisible(lexique.HostFieldGetData("WNPU").equalsIgnoreCase("D"));
    WA1REA.setEnabled(false);
    OBJ_89.setVisible(!lexique.HostFieldGetData("WA1REA").trim().isEmpty());
    WA1REA.setVisible(!lexique.HostFieldGetData("WA1REA").trim().isEmpty());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    if (z_LABRL.isSelected()) {
      lexique.HostFieldPutData("LANAT", 0, "M");
    }
    else {
      lexique.HostFieldPutData("LANAT", 0, " ");
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("LAQTCX");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("PANX");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSER", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void OBJ_152ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WSTKX");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_88ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSPC", 0, "C");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(FCT1.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(FCT1.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("LAKACX");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    menuBar1 = new JMenuBar();
    panel2 = new JPanel();
    WCOD = new RiZoneSortie();
    label1 = new JLabel();
    WNLI = new RiZoneSortie();
    LAART = new RiZoneSortie();
    label2 = new JLabel();
    label3 = new JLabel();
    z_LANAT = new RiZoneSortie();
    label4 = new JLabel();
    label5 = new JLabel();
    OBJ_73 = new RiZoneSortie();
    OBJ_84 = new JLabel();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel10 = new JPanel();
    OBJ_124 = new JLabel();
    LADLDX = new RiZoneSortie();
    LADLPX = new XRiCalendrier();
    LADLCX = new XRiCalendrier();
    OBJ_122 = new JLabel();
    OBJ_120 = new JLabel();
    LALIB1 = new XRiTextField();
    LALIB2 = new XRiTextField();
    LALIB3 = new XRiTextField();
    LALIB4 = new XRiTextField();
    panel8 = new JPanel();
    LAKACX = new XRiTextField();
    LAKSCX = new XRiTextField();
    OBJ_54 = new JLabel();
    LAQTCX = new XRiTextField();
    LAQTAX = new XRiTextField();
    LAQTSX = new XRiTextField();
    OBJ_64 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_66 = new RiZoneSortie();
    OBJ_79 = new RiZoneSortie();
    OBJ_56 = new RiZoneSortie();
    WSER = new XRiCheckBox();
    OBJ_143 = new JLabel();
    OBJ_144 = new JLabel();
    OBJ_145 = new JLabel();
    OBJ_146 = new JLabel();
    OBJ_159 = new JLabel();
    OBJ_160 = new JLabel();
    L2QT1X = new XRiTextField();
    L2QT2X = new XRiTextField();
    L2QT3X = new XRiTextField();
    OBJ_81 = new JLabel();
    OBJ_68 = new JLabel();
    label6 = new JLabel();
    label11 = new JLabel();
    panel7 = new JPanel();
    OBJ_92 = new RiZoneSortie();
    OBJ_158 = new JLabel();
    WCPR = new XRiTextField();
    LAIN2 = new XRiTextField();
    OBJ_59 = new JLabel();
    LAIN1 = new XRiComboBox();
    OBJ_130 = new JLabel();
    LAMTAX = new XRiTextField();
    panel6 = new JPanel();
    OBJ_152 = new SNBoutonDetail();
    WSTKX = new XRiTextField();
    WATTX = new XRiTextField();
    WRESX = new XRiTextField();
    WDISX = new XRiTextField();
    OBJ_32 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_62 = new JLabel();
    LASAN = new XRiTextField();
    LAACT = new XRiTextField();
    L20NAT = new XRiTextField();
    OBJ_126 = new JLabel();
    WIEBC1 = new XRiTextField();
    WIEBC2 = new XRiTextField();
    WIEBCL = new XRiTextField();
    OBJ_119 = new RiZoneSortie();
    label10 = new JLabel();
    OBJ_132 = new JLabel();
    LATP1 = new XRiTextField();
    OBJ_134 = new JLabel();
    LATP2 = new XRiTextField();
    OBJ_136 = new JLabel();
    LATP3 = new XRiTextField();
    OBJ_138 = new JLabel();
    LATP4 = new XRiTextField();
    OBJ_140 = new JLabel();
    LATP5 = new XRiTextField();
    label_deprecie = new JLabel();
    OBJ_89 = new JLabel();
    WA1REA = new XRiComboBox();
    WLCNA = new JLabel();
    tabbedPane2 = new JTabbedPane();
    panel11 = new JPanel();
    LAPABX = new XRiTextField();
    OBJ_86 = new JLabel();
    LAPANX = new XRiTextField();
    OBJ_88 = new SNBoutonDetail();
    OBJ_90 = new JLabel();
    LATVA = new XRiTextField();
    LAMHTX = new XRiTextField();
    OBJ_94 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_103 = new RiZoneSortie();
    OBJ_110 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_101 = new RiZoneSortie();
    OBJ_107 = new RiZoneSortie();
    OBJ_109 = new RiZoneSortie();
    OBJ_111 = new RiZoneSortie();
    OBJ_113 = new RiZoneSortie();
    OBJ_102 = new JLabel();
    OBJ_106 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_112 = new JLabel();
    LAREM1 = new XRiTextField();
    LAREM2 = new XRiTextField();
    LAREM3 = new XRiTextField();
    LAREM4 = new XRiTextField();
    LAREM5 = new XRiTextField();
    LAREM6 = new XRiTextField();
    label7 = new JLabel();
    label8 = new JLabel();
    LARP6 = new XRiTextField();
    LARP5 = new XRiTextField();
    LARP4 = new XRiTextField();
    LARP3 = new XRiTextField();
    LARP2 = new XRiTextField();
    LARP1 = new XRiTextField();
    textField1 = new XRiTextField();
    label9 = new JLabel();
    z_LABRL = new JCheckBox();
    panel3 = new JPanel();
    OBJ_125 = new JLabel();
    LAFK3 = new XRiTextField();
    OBJ_127 = new JLabel();
    LAFV3 = new XRiTextField();
    panel16 = new JPanel();
    OBJ_147 = new JLabel();
    OBJ_69 = new JLabel();
    LAFP1 = new XRiTextField();
    LAFV1 = new XRiTextField();
    OBJ_148 = new JLabel();
    OBJ_149 = new JLabel();
    LAFK1 = new XRiTextField();
    UPNETX = new RiZoneSortie();
    OBJ_131 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    FCT1 = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    OBJ_70 = new JLabel();
    OBJ_83 = new JLabel();
    riBoutonDetail1 = new SNBoutonDetail();
    
    // ======== this ========
    setMinimumSize(new Dimension(1095, 630));
    setPreferredSize(new Dimension(1095, 630));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== menuBar1 ========
    {
      menuBar1.setPreferredSize(new Dimension(1000, 29));
      menuBar1.setMinimumSize(new Dimension(1000, 29));
      menuBar1.setName("menuBar1");
      
      // ======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setName("panel2");
        panel2.setLayout(null);
        
        // ---- WCOD ----
        WCOD.setComponentPopupMenu(BTD);
        WCOD.setOpaque(false);
        WCOD.setText("@WCOD@");
        WCOD.setName("WCOD");
        panel2.add(WCOD);
        WCOD.setBounds(40, 1, 20, WCOD.getPreferredSize().height);
        
        // ---- label1 ----
        label1.setText("Mode");
        label1.setName("label1");
        panel2.add(label1);
        label1.setBounds(0, 3, 45, 20);
        
        // ---- WNLI ----
        WNLI.setComponentPopupMenu(BTD);
        WNLI.setOpaque(false);
        WNLI.setText("@WNLI@");
        WNLI.setName("WNLI");
        panel2.add(WNLI);
        WNLI.setBounds(140, 1, 44, WNLI.getPreferredSize().height);
        
        // ---- LAART ----
        LAART.setComponentPopupMenu(BTD);
        LAART.setOpaque(false);
        LAART.setText("@LAART@");
        LAART.setName("LAART");
        panel2.add(LAART);
        LAART.setBounds(245, 1, 210, LAART.getPreferredSize().height);
        
        // ---- label2 ----
        label2.setText("N\u00b0 ligne");
        label2.setName("label2");
        panel2.add(label2);
        label2.setBounds(85, 3, 55, 21);
        
        // ---- label3 ----
        label3.setText("Article");
        label3.setName("label3");
        panel2.add(label3);
        label3.setBounds(200, 3, 45, 20);
        
        // ---- z_LANAT ----
        z_LANAT.setText("traduire LANAT");
        z_LANAT.setOpaque(false);
        z_LANAT.setName("z_LANAT");
        panel2.add(z_LANAT);
        z_LANAT.setBounds(515, 1, 140, z_LANAT.getPreferredSize().height);
        
        // ---- label4 ----
        label4.setText("Type");
        label4.setName("label4");
        panel2.add(label4);
        label4.setBounds(475, 3, 40, 20);
        
        // ---- label5 ----
        label5.setText("Magasin");
        label5.setName("label5");
        panel2.add(label5);
        label5.setBounds(675, 3, 65, 21);
        
        // ---- OBJ_73 ----
        OBJ_73.setText("@LAMAG@");
        OBJ_73.setOpaque(false);
        OBJ_73.setName("OBJ_73");
        panel2.add(OBJ_73);
        OBJ_73.setBounds(735, 1, 34, OBJ_73.getPreferredSize().height);
        
        // ---- OBJ_84 ----
        OBJ_84.setText("@AVOI@");
        OBJ_84.setName("OBJ_84");
        panel2.add(OBJ_84);
        OBJ_84.setBounds(800, 3, 65, 20);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      menuBar1.add(panel2);
    }
    add(menuBar1, BorderLayout.NORTH);
    
    // ======== p_principal ========
    {
      p_principal.setPreferredSize(new Dimension(1095, 600));
      p_principal.setMinimumSize(new Dimension(1095, 600));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Options articles");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Conditions d'achat");
              riSousMenu_bt7.setToolTipText("Conditions normales d'achat");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Cout derni\u00e8re entr\u00e9e");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Modification libell\u00e9");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Extension de ligne");
              riSousMenu_bt11.setToolTipText("Extension de ligne commerciale");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Saisie autres unit\u00e9s");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");
              
              // ---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Imputation");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt2ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel10 ========
        {
          panel10.setBorder(new TitledBorder("Dates de livraison"));
          panel10.setOpaque(false);
          panel10.setName("panel10");
          panel10.setLayout(null);
          
          // ---- OBJ_124 ----
          OBJ_124.setText("Disponible");
          OBJ_124.setName("OBJ_124");
          panel10.add(OBJ_124);
          OBJ_124.setBounds(257, 25, 69, 20);
          
          // ---- LADLDX ----
          LADLDX.setComponentPopupMenu(BTD);
          LADLDX.setText("@LADLDX@");
          LADLDX.setHorizontalAlignment(SwingConstants.CENTER);
          LADLDX.setName("LADLDX");
          panel10.add(LADLDX);
          LADLDX.setBounds(255, 47, 65, LADLDX.getPreferredSize().height);
          
          // ---- LADLPX ----
          LADLPX.setComponentPopupMenu(BTD);
          LADLPX.setName("LADLPX");
          panel10.add(LADLPX);
          LADLPX.setBounds(15, 45, 105, LADLPX.getPreferredSize().height);
          
          // ---- LADLCX ----
          LADLCX.setComponentPopupMenu(BTD);
          LADLCX.setName("LADLCX");
          panel10.add(LADLCX);
          LADLCX.setBounds(135, 45, 105, LADLCX.getPreferredSize().height);
          
          // ---- OBJ_122 ----
          OBJ_122.setText("Confirm\u00e9e");
          OBJ_122.setName("OBJ_122");
          panel10.add(OBJ_122);
          OBJ_122.setBounds(135, 25, 70, 20);
          
          // ---- OBJ_120 ----
          OBJ_120.setText("Pr\u00e9vue");
          OBJ_120.setName("OBJ_120");
          panel10.add(OBJ_120);
          OBJ_120.setBounds(17, 25, 50, 20);
        }
        p_contenu.add(panel10);
        panel10.setBounds(10, 180, 340, 95);
        
        // ---- LALIB1 ----
        LALIB1.setComponentPopupMenu(BTD);
        LALIB1.setName("LALIB1");
        p_contenu.add(LALIB1);
        LALIB1.setBounds(25, 35, 310, LALIB1.getPreferredSize().height);
        
        // ---- LALIB2 ----
        LALIB2.setComponentPopupMenu(BTD);
        LALIB2.setName("LALIB2");
        p_contenu.add(LALIB2);
        LALIB2.setBounds(25, 65, 310, LALIB2.getPreferredSize().height);
        
        // ---- LALIB3 ----
        LALIB3.setComponentPopupMenu(BTD);
        LALIB3.setName("LALIB3");
        p_contenu.add(LALIB3);
        LALIB3.setBounds(25, 95, 310, LALIB3.getPreferredSize().height);
        
        // ---- LALIB4 ----
        LALIB4.setComponentPopupMenu(BTD);
        LALIB4.setName("LALIB4");
        p_contenu.add(LALIB4);
        LALIB4.setBounds(25, 125, 310, LALIB4.getPreferredSize().height);
        
        // ======== panel8 ========
        {
          panel8.setBorder(new TitledBorder("Quantit\u00e9s"));
          panel8.setOpaque(false);
          panel8.setName("panel8");
          panel8.setLayout(null);
          
          // ---- LAKACX ----
          LAKACX.setComponentPopupMenu(BTD);
          LAKACX.setHorizontalAlignment(SwingConstants.RIGHT);
          LAKACX.setToolTipText("Nombre d'unit\u00e9s d'achat par unit\u00e9 de commande @LAUNC@");
          LAKACX.setName("LAKACX");
          panel8.add(LAKACX);
          LAKACX.setBounds(230, 60, 80, LAKACX.getPreferredSize().height);
          
          // ---- LAKSCX ----
          LAKSCX.setComponentPopupMenu(BTD);
          LAKSCX.setHorizontalAlignment(SwingConstants.RIGHT);
          LAKSCX.setToolTipText("Nombre d'unit\u00e9s de stock par unit\u00e9 de commande (@LAUNC@)");
          LAKSCX.setName("LAKSCX");
          panel8.add(LAKSCX);
          LAKSCX.setBounds(230, 90, 80, LAKSCX.getPreferredSize().height);
          
          // ---- OBJ_54 ----
          OBJ_54.setText("Commande");
          OBJ_54.setName("OBJ_54");
          panel8.add(OBJ_54);
          OBJ_54.setBounds(15, 33, 75, 25);
          
          // ---- LAQTCX ----
          LAQTCX.setComponentPopupMenu(BTD);
          LAQTCX.setHorizontalAlignment(SwingConstants.RIGHT);
          LAQTCX.setName("LAQTCX");
          panel8.add(LAQTCX);
          LAQTCX.setBounds(90, 30, 100, LAQTCX.getPreferredSize().height);
          
          // ---- LAQTAX ----
          LAQTAX.setComponentPopupMenu(BTD);
          LAQTAX.setHorizontalAlignment(SwingConstants.RIGHT);
          LAQTAX.setName("LAQTAX");
          panel8.add(LAQTAX);
          LAQTAX.setBounds(90, 60, 100, LAQTAX.getPreferredSize().height);
          
          // ---- LAQTSX ----
          LAQTSX.setComponentPopupMenu(BTD);
          LAQTSX.setHorizontalAlignment(SwingConstants.RIGHT);
          LAQTSX.setName("LAQTSX");
          panel8.add(LAQTSX);
          LAQTSX.setBounds(90, 90, 100, LAQTSX.getPreferredSize().height);
          
          // ---- OBJ_64 ----
          OBJ_64.setText("Achat");
          OBJ_64.setName("OBJ_64");
          panel8.add(OBJ_64);
          OBJ_64.setBounds(15, 62, 75, 25);
          
          // ---- OBJ_77 ----
          OBJ_77.setText("Stock");
          OBJ_77.setName("OBJ_77");
          panel8.add(OBJ_77);
          OBJ_77.setBounds(15, 92, 75, 25);
          
          // ---- OBJ_66 ----
          OBJ_66.setText("@LAUNA@");
          OBJ_66.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_66.setName("OBJ_66");
          panel8.add(OBJ_66);
          OBJ_66.setBounds(195, 62, 30, 25);
          
          // ---- OBJ_79 ----
          OBJ_79.setText("@A1UNS@");
          OBJ_79.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_79.setName("OBJ_79");
          panel8.add(OBJ_79);
          OBJ_79.setBounds(195, 92, 30, 25);
          
          // ---- OBJ_56 ----
          OBJ_56.setText("@LAUNC@");
          OBJ_56.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_56.setName("OBJ_56");
          panel8.add(OBJ_56);
          OBJ_56.setBounds(195, 32, 30, 25);
          
          // ---- WSER ----
          WSER.setText("WSER");
          WSER.setComponentPopupMenu(BTD);
          WSER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSER.setHorizontalAlignment(SwingConstants.RIGHT);
          WSER.setName("WSER");
          panel8.add(WSER);
          WSER.setBounds(433, 122, 75, 25);
          
          // ---- OBJ_143 ----
          OBJ_143.setText("Quantit\u00e9 minimum");
          OBJ_143.setName("OBJ_143");
          panel8.add(OBJ_143);
          OBJ_143.setBounds(320, 34, 120, 20);
          
          // ---- OBJ_144 ----
          OBJ_144.setText("@CAQMI@");
          OBJ_144.setBorder(new BevelBorder(BevelBorder.LOWERED));
          OBJ_144.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_144.setName("OBJ_144");
          panel8.add(OBJ_144);
          OBJ_144.setBounds(445, 32, 92, 25);
          
          // ---- OBJ_145 ----
          OBJ_145.setText("Quantit\u00e9 \u00e9conomique");
          OBJ_145.setName("OBJ_145");
          panel8.add(OBJ_145);
          OBJ_145.setBounds(320, 94, 120, 20);
          
          // ---- OBJ_146 ----
          OBJ_146.setText("@CAQEC@");
          OBJ_146.setBorder(new BevelBorder(BevelBorder.LOWERED));
          OBJ_146.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_146.setName("OBJ_146");
          panel8.add(OBJ_146);
          OBJ_146.setBounds(445, 92, 92, 25);
          
          // ---- OBJ_159 ----
          OBJ_159.setText("Conditionnement");
          OBJ_159.setName("OBJ_159");
          panel8.add(OBJ_159);
          OBJ_159.setBounds(320, 64, 120, 20);
          
          // ---- OBJ_160 ----
          OBJ_160.setText("@CANUA@");
          OBJ_160.setBorder(new BevelBorder(BevelBorder.LOWERED));
          OBJ_160.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_160.setName("OBJ_160");
          panel8.add(OBJ_160);
          OBJ_160.setBounds(461, 62, 76, 25);
          
          // ---- L2QT1X ----
          L2QT1X.setComponentPopupMenu(BTD);
          L2QT1X.setHorizontalAlignment(SwingConstants.RIGHT);
          L2QT1X.setName("L2QT1X");
          panel8.add(L2QT1X);
          L2QT1X.setBounds(90, 120, 90, L2QT1X.getPreferredSize().height);
          
          // ---- L2QT2X ----
          L2QT2X.setComponentPopupMenu(BTD);
          L2QT2X.setHorizontalAlignment(SwingConstants.RIGHT);
          L2QT2X.setName("L2QT2X");
          panel8.add(L2QT2X);
          L2QT2X.setBounds(205, 120, 90, L2QT2X.getPreferredSize().height);
          
          // ---- L2QT3X ----
          L2QT3X.setComponentPopupMenu(BTD);
          L2QT3X.setHorizontalAlignment(SwingConstants.RIGHT);
          L2QT3X.setName("L2QT3X");
          panel8.add(L2QT3X);
          L2QT3X.setBounds(320, 120, 90, L2QT3X.getPreferredSize().height);
          
          // ---- OBJ_81 ----
          OBJ_81.setText("x");
          OBJ_81.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_81.setFont(OBJ_81.getFont().deriveFont(OBJ_81.getFont().getStyle() | Font.BOLD));
          OBJ_81.setName("OBJ_81");
          panel8.add(OBJ_81);
          OBJ_81.setBounds(300, 123, 15, 25);
          
          // ---- OBJ_68 ----
          OBJ_68.setText("x");
          OBJ_68.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_68.setFont(OBJ_68.getFont().deriveFont(OBJ_68.getFont().getStyle() | Font.BOLD));
          OBJ_68.setName("OBJ_68");
          panel8.add(OBJ_68);
          OBJ_68.setBounds(185, 123, 12, 25);
          
          // ---- label6 ----
          label6.setText("Dimensions");
          label6.setName("label6");
          panel8.add(label6);
          label6.setBounds(15, 119, 75, 30);
          
          // ---- label11 ----
          label11.setText("Nb Un/1UC");
          label11.setToolTipText("Nombre d'unit\u00e9s par unit\u00e9 de commande");
          label11.setName("label11");
          panel8.add(label11);
          label11.setBounds(232, 35, 75, 20);
        }
        p_contenu.add(panel8);
        panel8.setBounds(355, 5, 550, 165);
        
        // ======== panel7 ========
        {
          panel7.setBorder(new TitledBorder("Frais"));
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);
          
          // ---- OBJ_92 ----
          OBJ_92.setText("@EFLIBR@");
          OBJ_92.setName("OBJ_92");
          panel7.add(OBJ_92);
          OBJ_92.setBounds(140, 32, 180, OBJ_92.getPreferredSize().height);
          
          // ---- OBJ_158 ----
          OBJ_158.setText("Formule PR");
          OBJ_158.setName("OBJ_158");
          panel7.add(OBJ_158);
          OBJ_158.setBounds(15, 34, 85, 20);
          
          // ---- WCPR ----
          WCPR.setComponentPopupMenu(BTD);
          WCPR.setName("WCPR");
          panel7.add(WCPR);
          WCPR.setBounds(95, 30, 40, WCPR.getPreferredSize().height);
          
          // ---- LAIN2 ----
          LAIN2.setToolTipText("Type d'\u00e9l\u00e9ment de frais");
          LAIN2.setComponentPopupMenu(BTD);
          LAIN2.setName("LAIN2");
          panel7.add(LAIN2);
          LAIN2.setBounds(296, 60, 24, LAIN2.getPreferredSize().height);
          
          // ---- OBJ_59 ----
          OBJ_59.setText("Type");
          OBJ_59.setName("OBJ_59");
          panel7.add(OBJ_59);
          OBJ_59.setBounds(250, 64, 45, 20);
          
          // ---- LAIN1 ----
          LAIN1.setModel(new DefaultComboBoxModel(new String[] { "", "Imput\u00e9, non r\u00e9parti",
              "Imput\u00e9, r\u00e9parti au prorata des montants", "Imput\u00e9, r\u00e9parti au prorata des quantit\u00e9s",
              "Imput\u00e9, r\u00e9parti au prorata des poids", "Imput\u00e9, r\u00e9parti au prorata des volumes",
              "Double r\u00e9partition : au poids et au montant", "Double r\u00e9partition : au volume et au montant" }));
          LAIN1.setComponentPopupMenu(BTD);
          LAIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LAIN1.setName("LAIN1");
          panel7.add(LAIN1);
          LAIN1.setBounds(13, 90, 310, LAIN1.getPreferredSize().height);
          
          // ---- OBJ_130 ----
          OBJ_130.setText("Montant");
          OBJ_130.setName("OBJ_130");
          panel7.add(OBJ_130);
          OBJ_130.setBounds(15, 64, 90, 20);
          
          // ---- LAMTAX ----
          LAMTAX.setComponentPopupMenu(FCT1);
          LAMTAX.setHorizontalAlignment(SwingConstants.RIGHT);
          LAMTAX.setName("LAMTAX");
          panel7.add(LAMTAX);
          LAMTAX.setBounds(95, 60, 90, LAMTAX.getPreferredSize().height);
        }
        p_contenu.add(panel7);
        panel7.setBounds(10, 290, 340, 135);
        
        // ======== panel6 ========
        {
          panel6.setBorder(new TitledBorder("Stock"));
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);
          
          // ---- OBJ_152 ----
          OBJ_152.setText("");
          OBJ_152.setToolTipText("Position en stock");
          OBJ_152.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_152.setName("OBJ_152");
          OBJ_152.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_152ActionPerformed(e);
            }
          });
          panel6.add(OBJ_152);
          OBJ_152.setBounds(565, 45, 40, 28);
          
          // ---- WSTKX ----
          WSTKX.setComponentPopupMenu(BTD);
          WSTKX.setHorizontalAlignment(SwingConstants.RIGHT);
          WSTKX.setName("WSTKX");
          panel6.add(WSTKX);
          WSTKX.setBounds(15, 45, 120, WSTKX.getPreferredSize().height);
          
          // ---- WATTX ----
          WATTX.setComponentPopupMenu(BTD);
          WATTX.setHorizontalAlignment(SwingConstants.RIGHT);
          WATTX.setName("WATTX");
          panel6.add(WATTX);
          WATTX.setBounds(160, 45, 120, 28);
          
          // ---- WRESX ----
          WRESX.setComponentPopupMenu(BTD);
          WRESX.setHorizontalAlignment(SwingConstants.RIGHT);
          WRESX.setName("WRESX");
          panel6.add(WRESX);
          WRESX.setBounds(305, 45, 120, 28);
          
          // ---- WDISX ----
          WDISX.setComponentPopupMenu(BTD);
          WDISX.setHorizontalAlignment(SwingConstants.RIGHT);
          WDISX.setName("WDISX");
          panel6.add(WDISX);
          WDISX.setBounds(450, 45, 120, 28);
          
          // ---- OBJ_32 ----
          OBJ_32.setText("Command\u00e9");
          OBJ_32.setName("OBJ_32");
          panel6.add(OBJ_32);
          OBJ_32.setBounds(307, 25, 115, 23);
          
          // ---- OBJ_31 ----
          OBJ_31.setText("Attendu");
          OBJ_31.setName("OBJ_31");
          panel6.add(OBJ_31);
          OBJ_31.setBounds(162, 25, 82, 23);
          
          // ---- OBJ_33 ----
          OBJ_33.setText("Th\u00e9orique");
          OBJ_33.setName("OBJ_33");
          panel6.add(OBJ_33);
          OBJ_33.setBounds(452, 25, 70, 23);
          
          // ---- OBJ_30 ----
          OBJ_30.setText("Physique");
          OBJ_30.setName("OBJ_30");
          panel6.add(OBJ_30);
          OBJ_30.setBounds(17, 25, 67, 23);
          
          // ---- OBJ_40 ----
          OBJ_40.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_40.setName("OBJ_40");
          panel6.add(OBJ_40);
          OBJ_40.setBounds(425, 45, 25, 28);
          
          // ---- OBJ_39 ----
          OBJ_39.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_39.setName("OBJ_39");
          panel6.add(OBJ_39);
          OBJ_39.setBounds(280, 45, 25, 28);
          
          // ---- OBJ_38 ----
          OBJ_38.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_38.setName("OBJ_38");
          panel6.add(OBJ_38);
          OBJ_38.setBounds(135, 45, 30, 28);
          
          // ---- OBJ_27 ----
          OBJ_27.setText("@WLATTN@");
          OBJ_27.setFont(OBJ_27.getFont().deriveFont(OBJ_27.getFont().getStyle() | Font.BOLD));
          OBJ_27.setForeground(new Color(204, 51, 0));
          OBJ_27.setName("OBJ_27");
          panel6.add(OBJ_27);
          OBJ_27.setBounds(15, 70, 820, 20);
        }
        p_contenu.add(panel6);
        panel6.setBounds(10, 485, 895, 95);
        
        // ---- OBJ_63 ----
        OBJ_63.setText("Nature");
        OBJ_63.setName("OBJ_63");
        p_contenu.add(OBJ_63);
        OBJ_63.setBounds(255, 434, 50, 20);
        
        // ---- OBJ_61 ----
        OBJ_61.setText("Section");
        OBJ_61.setName("OBJ_61");
        p_contenu.add(OBJ_61);
        OBJ_61.setBounds(25, 434, 50, 20);
        
        // ---- OBJ_62 ----
        OBJ_62.setText("Affaire");
        OBJ_62.setName("OBJ_62");
        p_contenu.add(OBJ_62);
        OBJ_62.setBounds(145, 434, 45, 20);
        
        // ---- LASAN ----
        LASAN.setComponentPopupMenu(BTD);
        LASAN.setName("LASAN");
        p_contenu.add(LASAN);
        LASAN.setBounds(80, 430, 50, LASAN.getPreferredSize().height);
        
        // ---- LAACT ----
        LAACT.setComponentPopupMenu(BTD);
        LAACT.setName("LAACT");
        p_contenu.add(LAACT);
        LAACT.setBounds(190, 430, 50, LAACT.getPreferredSize().height);
        
        // ---- L20NAT ----
        L20NAT.setComponentPopupMenu(BTD);
        L20NAT.setName("L20NAT");
        p_contenu.add(L20NAT);
        L20NAT.setBounds(300, 430, 60, L20NAT.getPreferredSize().height);
        
        // ---- OBJ_126 ----
        OBJ_126.setText("Lien sur vente");
        OBJ_126.setName("OBJ_126");
        p_contenu.add(OBJ_126);
        OBJ_126.setBounds(380, 434, 100, 20);
        
        // ---- WIEBC1 ----
        WIEBC1.setComponentPopupMenu(BTD);
        WIEBC1.setName("WIEBC1");
        p_contenu.add(WIEBC1);
        WIEBC1.setBounds(490, 430, 60, WIEBC1.getPreferredSize().height);
        
        // ---- WIEBC2 ----
        WIEBC2.setComponentPopupMenu(BTD);
        WIEBC2.setName("WIEBC2");
        p_contenu.add(WIEBC2);
        WIEBC2.setBounds(555, 430, 20, WIEBC2.getPreferredSize().height);
        
        // ---- WIEBCL ----
        WIEBCL.setComponentPopupMenu(BTD);
        WIEBCL.setName("WIEBCL");
        p_contenu.add(WIEBCL);
        WIEBCL.setBounds(575, 430, 44, WIEBCL.getPreferredSize().height);
        
        // ---- OBJ_119 ----
        OBJ_119.setText("@LDNFAG@");
        OBJ_119.setName("OBJ_119");
        p_contenu.add(OBJ_119);
        OBJ_119.setBounds(810, 432, 90, OBJ_119.getPreferredSize().height);
        
        // ---- label10 ----
        label10.setText("Lien sur facture ou bon");
        label10.setName("label10");
        p_contenu.add(label10);
        label10.setBounds(670, 432, 135, 24);
        
        // ---- OBJ_132 ----
        OBJ_132.setText("@TIZPL1@");
        OBJ_132.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_132.setName("OBJ_132");
        p_contenu.add(OBJ_132);
        OBJ_132.setBounds(35, 465, 22, 20);
        
        // ---- LATP1 ----
        LATP1.setComponentPopupMenu(BTD);
        LATP1.setName("LATP1");
        p_contenu.add(LATP1);
        LATP1.setBounds(60, 460, 30, LATP1.getPreferredSize().height);
        
        // ---- OBJ_134 ----
        OBJ_134.setText("@TIZPL2@");
        OBJ_134.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_134.setName("OBJ_134");
        p_contenu.add(OBJ_134);
        OBJ_134.setBounds(95, 465, 22, 20);
        
        // ---- LATP2 ----
        LATP2.setComponentPopupMenu(BTD);
        LATP2.setName("LATP2");
        p_contenu.add(LATP2);
        LATP2.setBounds(120, 460, 30, LATP2.getPreferredSize().height);
        
        // ---- OBJ_136 ----
        OBJ_136.setText("@TIZPL3@");
        OBJ_136.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_136.setName("OBJ_136");
        p_contenu.add(OBJ_136);
        OBJ_136.setBounds(155, 465, 22, 20);
        
        // ---- LATP3 ----
        LATP3.setComponentPopupMenu(BTD);
        LATP3.setName("LATP3");
        p_contenu.add(LATP3);
        LATP3.setBounds(180, 460, 30, LATP3.getPreferredSize().height);
        
        // ---- OBJ_138 ----
        OBJ_138.setText("@TIZPL4@");
        OBJ_138.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_138.setName("OBJ_138");
        p_contenu.add(OBJ_138);
        OBJ_138.setBounds(215, 465, 22, 20);
        
        // ---- LATP4 ----
        LATP4.setComponentPopupMenu(BTD);
        LATP4.setName("LATP4");
        p_contenu.add(LATP4);
        LATP4.setBounds(240, 460, 30, LATP4.getPreferredSize().height);
        
        // ---- OBJ_140 ----
        OBJ_140.setText("@TIZPL5@");
        OBJ_140.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_140.setName("OBJ_140");
        p_contenu.add(OBJ_140);
        OBJ_140.setBounds(275, 465, 22, 20);
        
        // ---- LATP5 ----
        LATP5.setComponentPopupMenu(BTD);
        LATP5.setName("LATP5");
        p_contenu.add(LATP5);
        LATP5.setBounds(300, 460, 30, LATP5.getPreferredSize().height);
        
        // ---- label_deprecie ----
        label_deprecie.setText("DEPRECIE (@A1PDPX@ %)");
        label_deprecie.setForeground(new Color(204, 0, 0));
        label_deprecie.setFont(label_deprecie.getFont().deriveFont(label_deprecie.getFont().getStyle() | Font.BOLD,
            label_deprecie.getFont().getSize() + 3f));
        label_deprecie.setHorizontalAlignment(SwingConstants.CENTER);
        label_deprecie.setName("label_deprecie");
        p_contenu.add(label_deprecie);
        label_deprecie.setBounds(25, 5, 310, 30);
        
        // ---- OBJ_89 ----
        OBJ_89.setText("Type de r\u00e9approvisionnement");
        OBJ_89.setName("OBJ_89");
        p_contenu.add(OBJ_89);
        OBJ_89.setBounds(380, 464, 190, 20);
        
        // ---- WA1REA ----
        WA1REA.setModel(new DefaultComboBoxModel(new String[] { "Pas de r\u00e9approvisionnement", "Rupture sur stock minimum",
            "Consommation moyenne", "R\u00e9approvisionnement manuel", "Pr\u00e9visions de consommation", "Conso moyenne plafonn\u00e9e",
            "G\u00e9r\u00e9 (Plafond pour couverture)", "Compl\u00e9ment stock maximum", "Plafond pour couverture 'Mag'",
            "Produit non g\u00e9r\u00e9" }));
        WA1REA.setComponentPopupMenu(BTD);
        WA1REA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        WA1REA.setName("WA1REA");
        p_contenu.add(WA1REA);
        WA1REA.setBounds(575, 461, 263, WA1REA.getPreferredSize().height);
        
        // ---- WLCNA ----
        WLCNA.setText("@WLCNA@");
        WLCNA.setFont(WLCNA.getFont().deriveFont(WLCNA.getFont().getStyle() | Font.BOLD, WLCNA.getFont().getSize() + 1f));
        WLCNA.setName("WLCNA");
        p_contenu.add(WLCNA);
        WLCNA.setBounds(20, 575, 910, 25);
        
        // ======== tabbedPane2 ========
        {
          tabbedPane2.setName("tabbedPane2");
          
          // ======== panel11 ========
          {
            panel11.setBorder(new TitledBorder(""));
            panel11.setOpaque(false);
            panel11.setName("panel11");
            panel11.setLayout(null);
            
            // ---- LAPABX ----
            LAPABX.setComponentPopupMenu(FCT1);
            LAPABX.setHorizontalAlignment(SwingConstants.RIGHT);
            LAPABX.setName("LAPABX");
            panel11.add(LAPABX);
            LAPABX.setBounds(145, 35, 110, LAPABX.getPreferredSize().height);
            
            // ---- OBJ_86 ----
            OBJ_86.setText("Prix net / unit\u00e9 achat");
            OBJ_86.setName("OBJ_86");
            panel11.add(OBJ_86);
            OBJ_86.setBounds(20, 10, 120, 20);
            
            // ---- LAPANX ----
            LAPANX.setComponentPopupMenu(FCT1);
            LAPANX.setHorizontalAlignment(SwingConstants.RIGHT);
            LAPANX.setName("LAPANX");
            panel11.add(LAPANX);
            LAPANX.setBounds(145, 5, 110, LAPANX.getPreferredSize().height);
            
            // ---- OBJ_88 ----
            OBJ_88.setToolTipText("Modification du prix de base");
            OBJ_88.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_88.setName("OBJ_88");
            OBJ_88.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_88ActionPerformed(e);
              }
            });
            panel11.add(OBJ_88);
            OBJ_88.setBounds(255, 35, 28, 28);
            
            // ---- OBJ_90 ----
            OBJ_90.setText("TVA");
            OBJ_90.setName("OBJ_90");
            panel11.add(OBJ_90);
            OBJ_90.setBounds(460, 10, 30, 20);
            
            // ---- LATVA ----
            LATVA.setComponentPopupMenu(BTD);
            LATVA.setName("LATVA");
            panel11.add(LATVA);
            LATVA.setBounds(495, 10, 20, LATVA.getPreferredSize().height);
            
            // ---- LAMHTX ----
            LAMHTX.setComponentPopupMenu(FCT1);
            LAMHTX.setHorizontalAlignment(SwingConstants.RIGHT);
            LAMHTX.setName("LAMHTX");
            panel11.add(LAMHTX);
            LAMHTX.setBounds(405, 35, 110, LAMHTX.getPreferredSize().height);
            
            // ---- OBJ_94 ----
            OBJ_94.setText("Montant H.T");
            OBJ_94.setName("OBJ_94");
            panel11.add(OBJ_94);
            OBJ_94.setBounds(320, 40, 75, 20);
            
            // ---- OBJ_87 ----
            OBJ_87.setText("Prix de base");
            OBJ_87.setName("OBJ_87");
            panel11.add(OBJ_87);
            OBJ_87.setBounds(20, 40, 120, 20);
            
            // ---- OBJ_103 ----
            OBJ_103.setText("@CALIBR@");
            OBJ_103.setName("OBJ_103");
            panel11.add(OBJ_103);
            OBJ_103.setBounds(95, 125, 225, OBJ_103.getPreferredSize().height);
            
            // ---- OBJ_110 ----
            OBJ_110.setText("Prochaine");
            OBJ_110.setName("OBJ_110");
            panel11.add(OBJ_110);
            OBJ_110.setBounds(275, 155, 70, 20);
            
            // ---- OBJ_104 ----
            OBJ_104.setText("Quantit\u00e9 suivante");
            OBJ_104.setName("OBJ_104");
            panel11.add(OBJ_104);
            OBJ_104.setBounds(350, 125, 110, 20);
            
            // ---- OBJ_101 ----
            OBJ_101.setText("@QTESUI@");
            OBJ_101.setToolTipText("Quantit\u00e9 pour obtenir la condition quantitative suivante");
            OBJ_101.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_101.setName("OBJ_101");
            panel11.add(OBJ_101);
            OBJ_101.setBounds(460, 125, 53, OBJ_101.getPreferredSize().height);
            
            // ---- OBJ_107 ----
            OBJ_107.setText("@CADAPX@");
            OBJ_107.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_107.setName("OBJ_107");
            panel11.add(OBJ_107);
            OBJ_107.setBounds(95, 155, 65, OBJ_107.getPreferredSize().height);
            
            // ---- OBJ_109 ----
            OBJ_109.setText("@CADAFX@");
            OBJ_109.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_109.setName("OBJ_109");
            panel11.add(OBJ_109);
            OBJ_109.setBounds(195, 155, 65, OBJ_109.getPreferredSize().height);
            
            // ---- OBJ_111 ----
            OBJ_111.setText("@DAPSUI@");
            OBJ_111.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_111.setName("OBJ_111");
            panel11.add(OBJ_111);
            OBJ_111.setBounds(345, 155, 65, OBJ_111.getPreferredSize().height);
            
            // ---- OBJ_113 ----
            OBJ_113.setText("@DAFSUI@");
            OBJ_113.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_113.setName("OBJ_113");
            panel11.add(OBJ_113);
            OBJ_113.setBounds(450, 155, 65, OBJ_113.getPreferredSize().height);
            
            // ---- OBJ_102 ----
            OBJ_102.setText("Condition");
            OBJ_102.setName("OBJ_102");
            panel11.add(OBJ_102);
            OBJ_102.setBounds(20, 125, 70, 20);
            
            // ---- OBJ_106 ----
            OBJ_106.setText("Du");
            OBJ_106.setName("OBJ_106");
            panel11.add(OBJ_106);
            OBJ_106.setBounds(20, 155, 70, 20);
            
            // ---- OBJ_108 ----
            OBJ_108.setText("au");
            OBJ_108.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_108.setName("OBJ_108");
            panel11.add(OBJ_108);
            OBJ_108.setBounds(160, 155, 35, 20);
            
            // ---- OBJ_112 ----
            OBJ_112.setText("au");
            OBJ_112.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_112.setName("OBJ_112");
            panel11.add(OBJ_112);
            OBJ_112.setBounds(415, 155, 30, 20);
            
            // ---- LAREM1 ----
            LAREM1.setComponentPopupMenu(BTD);
            LAREM1.setHorizontalAlignment(SwingConstants.RIGHT);
            LAREM1.setName("LAREM1");
            panel11.add(LAREM1);
            LAREM1.setBounds(95, 65, 44, LAREM1.getPreferredSize().height);
            
            // ---- LAREM2 ----
            LAREM2.setComponentPopupMenu(BTD);
            LAREM2.setHorizontalAlignment(SwingConstants.RIGHT);
            LAREM2.setName("LAREM2");
            panel11.add(LAREM2);
            LAREM2.setBounds(140, 65, 44, LAREM2.getPreferredSize().height);
            
            // ---- LAREM3 ----
            LAREM3.setComponentPopupMenu(BTD);
            LAREM3.setHorizontalAlignment(SwingConstants.RIGHT);
            LAREM3.setName("LAREM3");
            panel11.add(LAREM3);
            LAREM3.setBounds(185, 65, 44, LAREM3.getPreferredSize().height);
            
            // ---- LAREM4 ----
            LAREM4.setHorizontalAlignment(SwingConstants.RIGHT);
            LAREM4.setName("LAREM4");
            panel11.add(LAREM4);
            LAREM4.setBounds(230, 65, 44, LAREM4.getPreferredSize().height);
            
            // ---- LAREM5 ----
            LAREM5.setHorizontalAlignment(SwingConstants.RIGHT);
            LAREM5.setName("LAREM5");
            panel11.add(LAREM5);
            LAREM5.setBounds(275, 65, 44, LAREM5.getPreferredSize().height);
            
            // ---- LAREM6 ----
            LAREM6.setHorizontalAlignment(SwingConstants.RIGHT);
            LAREM6.setName("LAREM6");
            panel11.add(LAREM6);
            LAREM6.setBounds(320, 65, 44, LAREM6.getPreferredSize().height);
            
            // ---- label7 ----
            label7.setText("Remises");
            label7.setName("label7");
            panel11.add(label7);
            label7.setBounds(20, 65, 60, 25);
            
            // ---- label8 ----
            label8.setText("Exclusions");
            label8.setName("label8");
            panel11.add(label8);
            label8.setBounds(20, 100, 75, 21);
            
            // ---- LARP6 ----
            LARP6.setComponentPopupMenu(BTD);
            LARP6.setName("LARP6");
            panel11.add(LARP6);
            LARP6.setBounds(195, 95, 20, LARP6.getPreferredSize().height);
            
            // ---- LARP5 ----
            LARP5.setComponentPopupMenu(BTD);
            LARP5.setName("LARP5");
            panel11.add(LARP5);
            LARP5.setBounds(175, 95, 20, LARP5.getPreferredSize().height);
            
            // ---- LARP4 ----
            LARP4.setComponentPopupMenu(BTD);
            LARP4.setName("LARP4");
            panel11.add(LARP4);
            LARP4.setBounds(155, 95, 20, LARP4.getPreferredSize().height);
            
            // ---- LARP3 ----
            LARP3.setComponentPopupMenu(BTD);
            LARP3.setName("LARP3");
            panel11.add(LARP3);
            LARP3.setBounds(135, 95, 20, LARP3.getPreferredSize().height);
            
            // ---- LARP2 ----
            LARP2.setComponentPopupMenu(BTD);
            LARP2.setName("LARP2");
            panel11.add(LARP2);
            LARP2.setBounds(115, 95, 20, LARP2.getPreferredSize().height);
            
            // ---- LARP1 ----
            LARP1.setComponentPopupMenu(BTD);
            LARP1.setName("LARP1");
            panel11.add(LARP1);
            LARP1.setBounds(95, 95, 20, LARP1.getPreferredSize().height);
            
            // ---- textField1 ----
            textField1.setName("textField1");
            panel11.add(textField1);
            textField1.setBounds(405, 65, 20, textField1.getPreferredSize().height);
            
            // ---- label9 ----
            label9.setText("Type");
            label9.setName("label9");
            panel11.add(label9);
            label9.setBounds(370, 70, 35, 20);
            
            // ---- z_LABRL ----
            z_LABRL.setText("Montant");
            z_LABRL.setHorizontalAlignment(SwingConstants.RIGHT);
            z_LABRL.setName("z_LABRL");
            panel11.add(z_LABRL);
            z_LABRL.setBounds(430, 70, 80, z_LABRL.getPreferredSize().height);
          }
          tabbedPane2.addTab("Prix / Conditions d'achat", panel11);
          
          // ======== panel3 ========
          {
            panel3.setBorder(new TitledBorder(""));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);
            
            // ---- OBJ_125 ----
            OBJ_125.setText("Taxe ou majoration (%)");
            OBJ_125.setName("OBJ_125");
            panel3.add(OBJ_125);
            OBJ_125.setBounds(20, 19, 130, 20);
            
            // ---- LAFK3 ----
            LAFK3.setComponentPopupMenu(null);
            LAFK3.setHorizontalAlignment(SwingConstants.RIGHT);
            LAFK3.setName("LAFK3");
            panel3.add(LAFK3);
            LAFK3.setBounds(195, 15, 80, LAFK3.getPreferredSize().height);
            
            // ---- OBJ_127 ----
            OBJ_127.setText("Valeur du conditionnement");
            OBJ_127.setName("OBJ_127");
            panel3.add(OBJ_127);
            OBJ_127.setBounds(20, 55, 155, 20);
            
            // ---- LAFV3 ----
            LAFV3.setComponentPopupMenu(null);
            LAFV3.setHorizontalAlignment(SwingConstants.RIGHT);
            LAFV3.setName("LAFV3");
            panel3.add(LAFV3);
            LAFV3.setBounds(195, 51, 80, LAFV3.getPreferredSize().height);
            
            // ======== panel16 ========
            {
              panel16.setOpaque(false);
              panel16.setBorder(new TitledBorder("Port"));
              panel16.setName("panel16");
              panel16.setLayout(null);
              
              // ---- OBJ_147 ----
              OBJ_147.setText("Valeur X poids");
              OBJ_147.setName("OBJ_147");
              panel16.add(OBJ_147);
              OBJ_147.setBounds(20, 39, 90, 20);
              
              // ---- OBJ_69 ----
              OBJ_69.setText("pourcentage");
              OBJ_69.setName("OBJ_69");
              panel16.add(OBJ_69);
              OBJ_69.setBounds(330, 39, 80, 20);
              
              // ---- LAFP1 ----
              LAFP1.setComponentPopupMenu(null);
              LAFP1.setName("LAFP1");
              panel16.add(LAFP1);
              LAFP1.setBounds(405, 35, 76, LAFP1.getPreferredSize().height);
              
              // ---- LAFV1 ----
              LAFV1.setComponentPopupMenu(null);
              LAFV1.setName("LAFV1");
              panel16.add(LAFV1);
              LAFV1.setBounds(105, 35, 92, LAFV1.getPreferredSize().height);
              
              // ---- OBJ_148 ----
              OBJ_148.setText("OU");
              OBJ_148.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_148.setFont(OBJ_148.getFont().deriveFont(OBJ_148.getFont().getStyle() | Font.BOLD));
              OBJ_148.setName("OBJ_148");
              panel16.add(OBJ_148);
              OBJ_148.setBounds(295, 39, 25, 20);
              
              // ---- OBJ_149 ----
              OBJ_149.setText("X");
              OBJ_149.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_149.setFont(OBJ_149.getFont().deriveFont(OBJ_149.getFont().getStyle() | Font.BOLD));
              OBJ_149.setName("OBJ_149");
              panel16.add(OBJ_149);
              OBJ_149.setBounds(195, 39, 20, 20);
              
              // ---- LAFK1 ----
              LAFK1.setComponentPopupMenu(null);
              LAFK1.setName("LAFK1");
              panel16.add(LAFK1);
              LAFK1.setBounds(215, 35, 60, LAFK1.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel16.getComponentCount(); i++) {
                  Rectangle bounds = panel16.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel16.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel16.setMinimumSize(preferredSize);
                panel16.setPreferredSize(preferredSize);
              }
            }
            panel3.add(panel16);
            panel16.setBounds(20, 90, 495, 85);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          tabbedPane2.addTab("Frais et port", panel3);
        }
        p_contenu.add(tabbedPane2);
        tabbedPane2.setBounds(355, 165, 550, 257);
        
        // ---- UPNETX ----
        UPNETX.setHorizontalAlignment(SwingConstants.RIGHT);
        UPNETX.setFont(UPNETX.getFont().deriveFont(UPNETX.getFont().getStyle() | Font.BOLD));
        UPNETX.setComponentPopupMenu(null);
        UPNETX.setText("@UPNETX@");
        UPNETX.setName("UPNETX");
        p_contenu.add(UPNETX);
        UPNETX.setBounds(555, 380, 105, UPNETX.getPreferredSize().height);
        
        // ---- OBJ_131 ----
        OBJ_131.setText("Prix d'achat net calcul\u00e9");
        OBJ_131.setFont(OBJ_131.getFont().deriveFont(OBJ_131.getFont().getStyle() | Font.BOLD, OBJ_131.getFont().getSize() + 1f));
        OBJ_131.setName("OBJ_131");
        p_contenu.add(OBJ_131);
        OBJ_131.setBounds(380, 380, 175, 24);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Choix possibles");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
      
      // ---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }
    
    // ======== FCT1 ========
    {
      FCT1.setName("FCT1");
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      FCT1.add(OBJ_19);
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      FCT1.add(OBJ_18);
    }
    
    // ======== riSousMenu10 ========
    {
      riSousMenu10.setName("riSousMenu10");
      
      // ---- riSousMenu_bt10 ----
      riSousMenu_bt10.setText("Gestion des lots");
      riSousMenu_bt10.setName("riSousMenu_bt10");
      riSousMenu_bt10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt10ActionPerformed(e);
        }
      });
      riSousMenu10.add(riSousMenu_bt10);
    }
    
    // ---- OBJ_70 ----
    OBJ_70.setText("@UC@");
    OBJ_70.setName("OBJ_70");
    
    // ---- OBJ_83 ----
    OBJ_83.setText("@UC2@");
    OBJ_83.setName("OBJ_83");
    
    // ---- riBoutonDetail1 ----
    riBoutonDetail1.setName("riBoutonDetail1");
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JMenuBar menuBar1;
  private JPanel panel2;
  private RiZoneSortie WCOD;
  private JLabel label1;
  private RiZoneSortie WNLI;
  private RiZoneSortie LAART;
  private JLabel label2;
  private JLabel label3;
  private RiZoneSortie z_LANAT;
  private JLabel label4;
  private JLabel label5;
  private RiZoneSortie OBJ_73;
  private JLabel OBJ_84;
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private JPanel p_contenu;
  private JPanel panel10;
  private JLabel OBJ_124;
  private RiZoneSortie LADLDX;
  private XRiCalendrier LADLPX;
  private XRiCalendrier LADLCX;
  private JLabel OBJ_122;
  private JLabel OBJ_120;
  private XRiTextField LALIB1;
  private XRiTextField LALIB2;
  private XRiTextField LALIB3;
  private XRiTextField LALIB4;
  private JPanel panel8;
  private XRiTextField LAKACX;
  private XRiTextField LAKSCX;
  private JLabel OBJ_54;
  private XRiTextField LAQTCX;
  private XRiTextField LAQTAX;
  private XRiTextField LAQTSX;
  private JLabel OBJ_64;
  private JLabel OBJ_77;
  private RiZoneSortie OBJ_66;
  private RiZoneSortie OBJ_79;
  private RiZoneSortie OBJ_56;
  private XRiCheckBox WSER;
  private JLabel OBJ_143;
  private JLabel OBJ_144;
  private JLabel OBJ_145;
  private JLabel OBJ_146;
  private JLabel OBJ_159;
  private JLabel OBJ_160;
  private XRiTextField L2QT1X;
  private XRiTextField L2QT2X;
  private XRiTextField L2QT3X;
  private JLabel OBJ_81;
  private JLabel OBJ_68;
  private JLabel label6;
  private JLabel label11;
  private JPanel panel7;
  private RiZoneSortie OBJ_92;
  private JLabel OBJ_158;
  private XRiTextField WCPR;
  private XRiTextField LAIN2;
  private JLabel OBJ_59;
  private XRiComboBox LAIN1;
  private JLabel OBJ_130;
  private XRiTextField LAMTAX;
  private JPanel panel6;
  private SNBoutonDetail OBJ_152;
  private XRiTextField WSTKX;
  private XRiTextField WATTX;
  private XRiTextField WRESX;
  private XRiTextField WDISX;
  private JLabel OBJ_32;
  private JLabel OBJ_31;
  private JLabel OBJ_33;
  private JLabel OBJ_30;
  private JLabel OBJ_40;
  private JLabel OBJ_39;
  private JLabel OBJ_38;
  private JLabel OBJ_27;
  private JLabel OBJ_63;
  private JLabel OBJ_61;
  private JLabel OBJ_62;
  private XRiTextField LASAN;
  private XRiTextField LAACT;
  private XRiTextField L20NAT;
  private JLabel OBJ_126;
  private XRiTextField WIEBC1;
  private XRiTextField WIEBC2;
  private XRiTextField WIEBCL;
  private RiZoneSortie OBJ_119;
  private JLabel label10;
  private JLabel OBJ_132;
  private XRiTextField LATP1;
  private JLabel OBJ_134;
  private XRiTextField LATP2;
  private JLabel OBJ_136;
  private XRiTextField LATP3;
  private JLabel OBJ_138;
  private XRiTextField LATP4;
  private JLabel OBJ_140;
  private XRiTextField LATP5;
  private JLabel label_deprecie;
  private JLabel OBJ_89;
  private XRiComboBox WA1REA;
  private JLabel WLCNA;
  private JTabbedPane tabbedPane2;
  private JPanel panel11;
  private XRiTextField LAPABX;
  private JLabel OBJ_86;
  private XRiTextField LAPANX;
  private SNBoutonDetail OBJ_88;
  private JLabel OBJ_90;
  private XRiTextField LATVA;
  private XRiTextField LAMHTX;
  private JLabel OBJ_94;
  private JLabel OBJ_87;
  private RiZoneSortie OBJ_103;
  private JLabel OBJ_110;
  private JLabel OBJ_104;
  private RiZoneSortie OBJ_101;
  private RiZoneSortie OBJ_107;
  private RiZoneSortie OBJ_109;
  private RiZoneSortie OBJ_111;
  private RiZoneSortie OBJ_113;
  private JLabel OBJ_102;
  private JLabel OBJ_106;
  private JLabel OBJ_108;
  private JLabel OBJ_112;
  private XRiTextField LAREM1;
  private XRiTextField LAREM2;
  private XRiTextField LAREM3;
  private XRiTextField LAREM4;
  private XRiTextField LAREM5;
  private XRiTextField LAREM6;
  private JLabel label7;
  private JLabel label8;
  private XRiTextField LARP6;
  private XRiTextField LARP5;
  private XRiTextField LARP4;
  private XRiTextField LARP3;
  private XRiTextField LARP2;
  private XRiTextField LARP1;
  private XRiTextField textField1;
  private JLabel label9;
  private JCheckBox z_LABRL;
  private JPanel panel3;
  private JLabel OBJ_125;
  private XRiTextField LAFK3;
  private JLabel OBJ_127;
  private XRiTextField LAFV3;
  private JPanel panel16;
  private JLabel OBJ_147;
  private JLabel OBJ_69;
  private XRiTextField LAFP1;
  private XRiTextField LAFV1;
  private JLabel OBJ_148;
  private JLabel OBJ_149;
  private XRiTextField LAFK1;
  private RiZoneSortie UPNETX;
  private JLabel OBJ_131;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_15;
  private JPopupMenu FCT1;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private JLabel OBJ_70;
  private JLabel OBJ_83;
  private SNBoutonDetail riBoutonDetail1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
