
package ri.serien.libecranrpg.vgam.VGAM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FX_MA extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] MAGES_Value = { "", "1", "2", };
  private String[] MATREA_Value = { "", "0", "1", "2", "3", "4", "5", "6", "7", };
  
  public VGAM01FX_MA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    MAGES.setValeurs(MAGES_Value, null);
    MATREA.setValeurs(MATREA_Value, null);
    MADAS.setValeursSelection("1", " ");
    MARSA.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // MAGES.setSelectedIndex(getIndice("MAGES", MAGES_Value));
    // MATREA.setSelectedIndex(getIndice("MATREA", MATREA_Value));
    
    // MADAS.setVisible( lexique.isPresent("MADAS"));
    // MADAS.setSelected(lexique.HostFieldGetData("MADAS").equalsIgnoreCase("X"));
    // MARSA.setVisible( lexique.isPresent("MARSA"));
    // MARSA.setSelected(lexique.HostFieldGetData("MARSA").equalsIgnoreCase("1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Personnalisation de @LOCGRP/-1/@"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (MADAS.isSelected())
    // lexique.HostFieldPutData("MADAS", 0, "X");
    // else
    // lexique.HostFieldPutData("MADAS", 0, " ");
    // if (MARSA.isSelected())
    // lexique.HostFieldPutData("MARSA", 0, "1");
    // else
    // lexique.HostFieldPutData("MARSA", 0, " ");
    // lexique.HostFieldPutData("MAGES", 0, MAGES_Value[MAGES.getSelectedIndex()]);
    // lexique.HostFieldPutData("MATREA", 0, MATREA_Value[MATREA.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_48 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel2 = new JXTitledPanel();
    MATREA = new XRiComboBox();
    MAGES = new XRiComboBox();
    OBJ_67 = new JLabel();
    OBJ_74 = new JLabel();
    MALIB = new XRiTextField();
    MARSA = new XRiCheckBox();
    MADAS = new XRiCheckBox();
    OBJ_76 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_59 = new JLabel();
    MAGCD = new XRiTextField();
    MAMSTY = new XRiTextField();
    OBJ_70 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_49 = new JLabel();
    MANCG = new XRiTextField();
    MASAN = new XRiTextField();
    MAACT = new XRiTextField();
    MAPER = new XRiSpinner();
    OBJ_63 = new JLabel();
    OBJ_64 = new JLabel();
    MANCOD = new XRiTextField();
    MANCOC = new XRiTextField();
    MATYP = new XRiTextField();
    MAEBP = new XRiTextField();
    MAADS = new XRiTextField();
    MACOL = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des achats");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");

          //---- INDTYP ----
          INDTYP.setName("INDTYP");

          //---- OBJ_48 ----
          OBJ_48.setText("Ordre");
          OBJ_48.setName("OBJ_48");

          //---- INDIND ----
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(513, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Code magasin");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- MATREA ----
            MATREA.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Rupture sur stock mini",
              "Consommation moyenne",
              "R\u00e9approvisionnement manuel",
              "Pr\u00e9visions de consommation",
              "Consommation moyenne plafonn\u00e9e",
              "Plafond pour couverture 'global'",
              "Compl\u00e9ment stock maximum",
              "Plafond pour couverture 'Mag'"
            }));
            MATREA.setComponentPopupMenu(null);
            MATREA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MATREA.setName("MATREA");
            xTitledPanel2ContentContainer.add(MATREA);
            MATREA.setBounds(260, 245, 192, MATREA.getPreferredSize().height);

            //---- MAGES ----
            MAGES.setModel(new DefaultComboBoxModel(new String[] {
              "Magasin ordinaire ou TTC",
              "Magasin sous douane",
              "Magasin HTGI"
            }));
            MAGES.setComponentPopupMenu(null);
            MAGES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MAGES.setName("MAGES");
            xTitledPanel2ContentContainer.add(MAGES);
            MAGES.setBounds(260, 405, 190, MAGES.getPreferredSize().height);

            //---- OBJ_67 ----
            OBJ_67.setText("Num\u00e9ro postes de comptabilisation pour imputation des mouvements de stock");
            OBJ_67.setName("OBJ_67");
            xTitledPanel2ContentContainer.add(OBJ_67);
            OBJ_67.setBounds(25, 185, 469, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("Edition des bons de pr\u00e9paration GVM \u00e0 la r\u00e9ception des marchandises");
            OBJ_74.setName("OBJ_74");
            xTitledPanel2ContentContainer.add(OBJ_74);
            OBJ_74.setBounds(25, 310, 410, 20);

            //---- MALIB ----
            MALIB.setComponentPopupMenu(BTD);
            MALIB.setName("MALIB");
            xTitledPanel2ContentContainer.add(MALIB);
            MALIB.setBounds(260, 45, 360, MALIB.getPreferredSize().height);

            //---- MARSA ----
            MARSA.setText("Section analytique de la famille ou du groupe");
            MARSA.setComponentPopupMenu(BTD);
            MARSA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MARSA.setName("MARSA");
            xTitledPanel2ContentContainer.add(MARSA);
            MARSA.setBounds(25, 280, 292, 20);

            //---- MADAS ----
            MADAS.setText("Magasin non g\u00e9r\u00e9 par adresse de stockage");
            MADAS.setComponentPopupMenu(BTD);
            MADAS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MADAS.setName("MADAS");
            xTitledPanel2ContentContainer.add(MADAS);
            MADAS.setBounds(25, 375, 283, 20);

            //---- OBJ_76 ----
            OBJ_76.setText("Type d'adresse \u00e0 \u00e9diter en priorit\u00e9");
            OBJ_76.setName("OBJ_76");
            xTitledPanel2ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(25, 345, 209, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("Type de r\u00e9approvisionnement");
            OBJ_71.setName("OBJ_71");
            xTitledPanel2ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(25, 245, 184, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("Gencod \u00e9metteur DILICOM");
            OBJ_61.setName("OBJ_61");
            xTitledPanel2ContentContainer.add(OBJ_61);
            OBJ_61.setBounds(340, 115, 161, 20);

            //---- OBJ_69 ----
            OBJ_69.setText("P\u00e9riodicit\u00e9 commandes");
            OBJ_69.setName("OBJ_69");
            xTitledPanel2ContentContainer.add(OBJ_69);
            OBJ_69.setBounds(25, 215, 147, 20);

            //---- OBJ_79 ----
            OBJ_79.setText("Type gestion magasin");
            OBJ_79.setName("OBJ_79");
            xTitledPanel2ContentContainer.add(OBJ_79);
            OBJ_79.setBounds(25, 405, 137, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("Type de stockage");
            OBJ_82.setName("OBJ_82");
            xTitledPanel2ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(25, 440, 137, 20);

            //---- OBJ_54 ----
            OBJ_54.setText("Code analytique");
            OBJ_54.setName("OBJ_54");
            xTitledPanel2ContentContainer.add(OBJ_54);
            OBJ_54.setBounds(25, 80, 121, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("Num\u00e9ro de tiers");
            OBJ_56.setName("OBJ_56");
            xTitledPanel2ContentContainer.add(OBJ_56);
            OBJ_56.setBounds(340, 80, 99, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("Code affaire");
            OBJ_59.setName("OBJ_59");
            xTitledPanel2ContentContainer.add(OBJ_59);
            OBJ_59.setBounds(25, 115, 96, 20);

            //---- MAGCD ----
            MAGCD.setComponentPopupMenu(BTD);
            MAGCD.setName("MAGCD");
            xTitledPanel2ContentContainer.add(MAGCD);
            MAGCD.setBounds(510, 110, 114, MAGCD.getPreferredSize().height);

            //---- MAMSTY ----
            MAMSTY.setComponentPopupMenu(BTD);
            MAMSTY.setName("MAMSTY");
            xTitledPanel2ContentContainer.add(MAMSTY);
            MAMSTY.setBounds(260, 435, 60, MAMSTY.getPreferredSize().height);

            //---- OBJ_70 ----
            OBJ_70.setText("semaines");
            OBJ_70.setName("OBJ_70");
            xTitledPanel2ContentContainer.add(OBJ_70);
            OBJ_70.setBounds(340, 215, 63, 20);

            //---- OBJ_52 ----
            OBJ_52.setText("Libell\u00e9");
            OBJ_52.setName("OBJ_52");
            xTitledPanel2ContentContainer.add(OBJ_52);
            OBJ_52.setBounds(25, 50, 61, 20);

            //---- OBJ_49 ----
            OBJ_49.setText("Type");
            OBJ_49.setName("OBJ_49");
            xTitledPanel2ContentContainer.add(OBJ_49);
            OBJ_49.setBounds(25, 19, 53, 20);

            //---- MANCG ----
            MANCG.setComponentPopupMenu(BTD);
            MANCG.setName("MANCG");
            xTitledPanel2ContentContainer.add(MANCG);
            MANCG.setBounds(565, 80, 58, MANCG.getPreferredSize().height);

            //---- MASAN ----
            MASAN.setComponentPopupMenu(BTD);
            MASAN.setName("MASAN");
            xTitledPanel2ContentContainer.add(MASAN);
            MASAN.setBounds(260, 80, 50, MASAN.getPreferredSize().height);

            //---- MAACT ----
            MAACT.setComponentPopupMenu(BTD);
            MAACT.setName("MAACT");
            xTitledPanel2ContentContainer.add(MAACT);
            MAACT.setBounds(260, 110, 50, MAACT.getPreferredSize().height);

            //---- MAPER ----
            MAPER.setComponentPopupMenu(null);
            MAPER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MAPER.setModel(new SpinnerListModel(new String[] {"", "1", "2", "3", "4"}));
            MAPER.setName("MAPER");
            xTitledPanel2ContentContainer.add(MAPER);
            MAPER.setBounds(260, 215, 48, 24);

            //---- OBJ_63 ----
            OBJ_63.setText("D");
            OBJ_63.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_63.setName("OBJ_63");
            xTitledPanel2ContentContainer.add(OBJ_63);
            OBJ_63.setBounds(540, 160, 34, 20);

            //---- OBJ_64 ----
            OBJ_64.setText("C");
            OBJ_64.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_64.setName("OBJ_64");
            xTitledPanel2ContentContainer.add(OBJ_64);
            OBJ_64.setBounds(585, 160, 34, 20);

            //---- MANCOD ----
            MANCOD.setComponentPopupMenu(BTD);
            MANCOD.setName("MANCOD");
            xTitledPanel2ContentContainer.add(MANCOD);
            MANCOD.setBounds(540, 180, 34, MANCOD.getPreferredSize().height);

            //---- MANCOC ----
            MANCOC.setComponentPopupMenu(BTD);
            MANCOC.setName("MANCOC");
            xTitledPanel2ContentContainer.add(MANCOC);
            MANCOC.setBounds(585, 180, 34, MANCOC.getPreferredSize().height);

            //---- MATYP ----
            MATYP.setComponentPopupMenu(BTD);
            MATYP.setName("MATYP");
            xTitledPanel2ContentContainer.add(MATYP);
            MATYP.setBounds(260, 15, 30, MATYP.getPreferredSize().height);

            //---- MAEBP ----
            MAEBP.setComponentPopupMenu(BTD);
            MAEBP.setName("MAEBP");
            xTitledPanel2ContentContainer.add(MAEBP);
            MAEBP.setBounds(435, 310, 20, MAEBP.getPreferredSize().height);

            //---- MAADS ----
            MAADS.setComponentPopupMenu(BTD);
            MAADS.setName("MAADS");
            xTitledPanel2ContentContainer.add(MAADS);
            MAADS.setBounds(260, 340, 20, MAADS.getPreferredSize().height);

            //---- MACOL ----
            MACOL.setComponentPopupMenu(BTD);
            MACOL.setName("MACOL");
            xTitledPanel2ContentContainer.add(MACOL);
            MACOL.setBounds(535, 80, 18, MACOL.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 532, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JLabel OBJ_48;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel2;
  private XRiComboBox MATREA;
  private XRiComboBox MAGES;
  private JLabel OBJ_67;
  private JLabel OBJ_74;
  private XRiTextField MALIB;
  private XRiCheckBox MARSA;
  private XRiCheckBox MADAS;
  private JLabel OBJ_76;
  private JLabel OBJ_71;
  private JLabel OBJ_61;
  private JLabel OBJ_69;
  private JLabel OBJ_79;
  private JLabel OBJ_82;
  private JLabel OBJ_54;
  private JLabel OBJ_56;
  private JLabel OBJ_59;
  private XRiTextField MAGCD;
  private XRiTextField MAMSTY;
  private JLabel OBJ_70;
  private JLabel OBJ_52;
  private JLabel OBJ_49;
  private XRiTextField MANCG;
  private XRiTextField MASAN;
  private XRiTextField MAACT;
  private XRiSpinner MAPER;
  private JLabel OBJ_63;
  private JLabel OBJ_64;
  private XRiTextField MANCOD;
  private XRiTextField MANCOC;
  private XRiTextField MATYP;
  private XRiTextField MAEBP;
  private XRiTextField MAADS;
  private XRiTextField MACOL;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
