
package ri.serien.libecranrpg.vgam.VGAM155F;
// Nom Fichier: pop_VGAM155F_FMTB1_471.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM155F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM155F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBP@")).trim());
    OBJ_17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBS@")).trim());
    OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBC@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBA@")).trim());
    QTS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTS@")).trim());
    QTC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTC@")).trim());
    QTA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTA@")).trim());
    A1KCS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1KCS@")).trim());
    CAKSC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAKSC@")).trim());
    CAKAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAKAC@")).trim());
    A1UCS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UCS@")).trim());
    A1UNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    CAUNC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNC@")).trim());
    CAUNA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_27.setVisible(lexique.isPresent("LIBA"));
    OBJ_22.setVisible(lexique.isPresent("LIBC"));
    OBJ_17.setVisible(lexique.isPresent("LIBS"));
    OBJ_14.setVisible(lexique.isPresent("LIBP"));
    
    // Titre
    setTitle("Unités et conditionnements de vente");
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_14 = new RiZoneSortie();
    OBJ_17 = new RiZoneSortie();
    OBJ_22 = new RiZoneSortie();
    OBJ_27 = new RiZoneSortie();
    P_PnlOpts = new JPanel();
    OBJ_24 = new JLabel();
    OBJ_8 = new JLabel();
    QTP = new XRiTextField();
    QTS = new RiZoneSortie();
    QTC = new RiZoneSortie();
    QTA = new RiZoneSortie();
    A1KCS = new RiZoneSortie();
    CAKSC = new RiZoneSortie();
    CAKAC = new RiZoneSortie();
    OBJ_19 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_9 = new JLabel();
    OBJ_10 = new JLabel();
    A1UCS = new RiZoneSortie();
    A1UNS = new RiZoneSortie();
    CAUNC = new RiZoneSortie();
    CAUNA = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(725, 240));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setBorder(new TitledBorder("Autres unit\u00e9s"));
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_14 ----
          OBJ_14.setText("@LIBP@");
          OBJ_14.setName("OBJ_14");
          p_recup.add(OBJ_14);
          OBJ_14.setBounds(180, 52, 126, OBJ_14.getPreferredSize().height);

          //---- OBJ_17 ----
          OBJ_17.setText("@LIBS@");
          OBJ_17.setName("OBJ_17");
          p_recup.add(OBJ_17);
          OBJ_17.setBounds(180, 82, 126, OBJ_17.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("@LIBC@");
          OBJ_22.setName("OBJ_22");
          p_recup.add(OBJ_22);
          OBJ_22.setBounds(180, 112, 126, OBJ_22.getPreferredSize().height);

          //---- OBJ_27 ----
          OBJ_27.setText("@LIBA@");
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(180, 142, 126, OBJ_27.getPreferredSize().height);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_24 ----
          OBJ_24.setText("Commande");
          OBJ_24.setName("OBJ_24");
          p_recup.add(OBJ_24);
          OBJ_24.setBounds(415, 114, 91, 20);

          //---- OBJ_8 ----
          OBJ_8.setText("Quantit\u00e9");
          OBJ_8.setName("OBJ_8");
          p_recup.add(OBJ_8);
          OBJ_8.setBounds(25, 30, 90, 20);

          //---- QTP ----
          QTP.setName("QTP");
          p_recup.add(QTP);
          QTP.setBounds(25, 50, 90, QTP.getPreferredSize().height);

          //---- QTS ----
          QTS.setText("@QTS@");
          QTS.setHorizontalAlignment(SwingConstants.RIGHT);
          QTS.setName("QTS");
          p_recup.add(QTS);
          QTS.setBounds(25, 80, 88, QTS.getPreferredSize().height);

          //---- QTC ----
          QTC.setText("@QTC@");
          QTC.setHorizontalAlignment(SwingConstants.RIGHT);
          QTC.setName("QTC");
          p_recup.add(QTC);
          QTC.setBounds(25, 110, 88, QTC.getPreferredSize().height);

          //---- QTA ----
          QTA.setText("@QTA@");
          QTA.setHorizontalAlignment(SwingConstants.RIGHT);
          QTA.setName("QTA");
          p_recup.add(QTA);
          QTA.setBounds(25, 140, 88, QTA.getPreferredSize().height);

          //---- A1KCS ----
          A1KCS.setText("@A1KCS@");
          A1KCS.setName("A1KCS");
          p_recup.add(A1KCS);
          A1KCS.setBounds(325, 82, 77, A1KCS.getPreferredSize().height);

          //---- CAKSC ----
          CAKSC.setText("@CAKSC@");
          CAKSC.setName("CAKSC");
          p_recup.add(CAKSC);
          CAKSC.setBounds(325, 112, 77, CAKSC.getPreferredSize().height);

          //---- CAKAC ----
          CAKAC.setText("@CAKAC@");
          CAKAC.setName("CAKAC");
          p_recup.add(CAKAC);
          CAKAC.setBounds(325, 142, 77, CAKAC.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setText("Stock");
          OBJ_19.setName("OBJ_19");
          p_recup.add(OBJ_19);
          OBJ_19.setBounds(415, 84, 55, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("Achat");
          OBJ_29.setName("OBJ_29");
          p_recup.add(OBJ_29);
          OBJ_29.setBounds(415, 144, 55, 20);

          //---- OBJ_9 ----
          OBJ_9.setText("Unit\u00e9 ");
          OBJ_9.setName("OBJ_9");
          p_recup.add(OBJ_9);
          OBJ_9.setBounds(135, 30, 52, 20);

          //---- OBJ_10 ----
          OBJ_10.setText("Coefficient");
          OBJ_10.setName("OBJ_10");
          p_recup.add(OBJ_10);
          OBJ_10.setBounds(325, 54, 85, 20);

          //---- A1UCS ----
          A1UCS.setText("@A1UCS@");
          A1UCS.setName("A1UCS");
          p_recup.add(A1UCS);
          A1UCS.setBounds(135, 50, 34, A1UCS.getPreferredSize().height);

          //---- A1UNS ----
          A1UNS.setText("@A1UNS@");
          A1UNS.setName("A1UNS");
          p_recup.add(A1UNS);
          A1UNS.setBounds(135, 80, 34, A1UNS.getPreferredSize().height);

          //---- CAUNC ----
          CAUNC.setText("@CAUNC@");
          CAUNC.setName("CAUNC");
          p_recup.add(CAUNC);
          CAUNC.setBounds(135, 110, 34, CAUNC.getPreferredSize().height);

          //---- CAUNA ----
          CAUNA.setText("@CAUNA@");
          CAUNA.setName("CAUNA");
          p_recup.add(CAUNA);
          CAUNA.setBounds(135, 140, 34, CAUNA.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 525, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie OBJ_14;
  private RiZoneSortie OBJ_17;
  private RiZoneSortie OBJ_22;
  private RiZoneSortie OBJ_27;
  private JPanel P_PnlOpts;
  private JLabel OBJ_24;
  private JLabel OBJ_8;
  private XRiTextField QTP;
  private RiZoneSortie QTS;
  private RiZoneSortie QTC;
  private RiZoneSortie QTA;
  private RiZoneSortie A1KCS;
  private RiZoneSortie CAKSC;
  private RiZoneSortie CAKAC;
  private JLabel OBJ_19;
  private JLabel OBJ_29;
  private JLabel OBJ_9;
  private JLabel OBJ_10;
  private RiZoneSortie A1UCS;
  private RiZoneSortie A1UNS;
  private RiZoneSortie CAUNC;
  private RiZoneSortie CAUNA;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
