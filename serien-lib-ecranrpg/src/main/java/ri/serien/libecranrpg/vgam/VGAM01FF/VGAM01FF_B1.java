
package ri.serien.libecranrpg.vgam.VGAM01FF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGAM01FF_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGAM01FF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOM1@")).trim());
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPL1@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOM2@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPL2@")).trim());
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOM3@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPL3@")).trim());
    OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOM4@")).trim());
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPL4@")).trim());
    OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOM5@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPL5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("PERSONNALISATION"));
    
    

    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_23 = new RiZoneSortie();
    OBJ_24 = new RiZoneSortie();
    OBJ_27 = new RiZoneSortie();
    OBJ_28 = new RiZoneSortie();
    OBJ_31 = new RiZoneSortie();
    OBJ_32 = new RiZoneSortie();
    OBJ_35 = new RiZoneSortie();
    OBJ_36 = new RiZoneSortie();
    OBJ_39 = new RiZoneSortie();
    OBJ_40 = new RiZoneSortie();
    FRS1 = new XRiTextField();
    FRS2 = new XRiTextField();
    FRS3 = new XRiTextField();
    FRS4 = new XRiTextField();
    FRS5 = new XRiTextField();
    COL1 = new XRiTextField();
    COL2 = new XRiTextField();
    COL3 = new XRiTextField();
    COL4 = new XRiTextField();
    COL5 = new XRiTextField();
    separator1 = compFactory.createSeparator("Fournisseurs \u00e0 interroger pour les demandes de prix");
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 245));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- OBJ_23 ----
        OBJ_23.setText("@NOM1@");
        OBJ_23.setName("OBJ_23");
        p_contenu.add(OBJ_23);
        OBJ_23.setBounds(120, 57, 232, OBJ_23.getPreferredSize().height);

        //---- OBJ_24 ----
        OBJ_24.setText("@CPL1@");
        OBJ_24.setName("OBJ_24");
        p_contenu.add(OBJ_24);
        OBJ_24.setBounds(365, 57, 232, OBJ_24.getPreferredSize().height);

        //---- OBJ_27 ----
        OBJ_27.setText("@NOM2@");
        OBJ_27.setName("OBJ_27");
        p_contenu.add(OBJ_27);
        OBJ_27.setBounds(120, 90, 232, OBJ_27.getPreferredSize().height);

        //---- OBJ_28 ----
        OBJ_28.setText("@CPL2@");
        OBJ_28.setName("OBJ_28");
        p_contenu.add(OBJ_28);
        OBJ_28.setBounds(365, 90, 232, OBJ_28.getPreferredSize().height);

        //---- OBJ_31 ----
        OBJ_31.setText("@NOM3@");
        OBJ_31.setName("OBJ_31");
        p_contenu.add(OBJ_31);
        OBJ_31.setBounds(120, 123, 232, OBJ_31.getPreferredSize().height);

        //---- OBJ_32 ----
        OBJ_32.setText("@CPL3@");
        OBJ_32.setName("OBJ_32");
        p_contenu.add(OBJ_32);
        OBJ_32.setBounds(365, 123, 232, OBJ_32.getPreferredSize().height);

        //---- OBJ_35 ----
        OBJ_35.setText("@NOM4@");
        OBJ_35.setName("OBJ_35");
        p_contenu.add(OBJ_35);
        OBJ_35.setBounds(120, 156, 232, OBJ_35.getPreferredSize().height);

        //---- OBJ_36 ----
        OBJ_36.setText("@CPL4@");
        OBJ_36.setName("OBJ_36");
        p_contenu.add(OBJ_36);
        OBJ_36.setBounds(365, 156, 232, OBJ_36.getPreferredSize().height);

        //---- OBJ_39 ----
        OBJ_39.setText("@NOM5@");
        OBJ_39.setName("OBJ_39");
        p_contenu.add(OBJ_39);
        OBJ_39.setBounds(120, 189, 232, OBJ_39.getPreferredSize().height);

        //---- OBJ_40 ----
        OBJ_40.setText("@CPL5@");
        OBJ_40.setName("OBJ_40");
        p_contenu.add(OBJ_40);
        OBJ_40.setBounds(365, 189, 232, OBJ_40.getPreferredSize().height);

        //---- FRS1 ----
        FRS1.setComponentPopupMenu(BTD);
        FRS1.setName("FRS1");
        p_contenu.add(FRS1);
        FRS1.setBounds(55, 55, 58, FRS1.getPreferredSize().height);

        //---- FRS2 ----
        FRS2.setComponentPopupMenu(BTD);
        FRS2.setName("FRS2");
        p_contenu.add(FRS2);
        FRS2.setBounds(55, 88, 58, FRS2.getPreferredSize().height);

        //---- FRS3 ----
        FRS3.setComponentPopupMenu(BTD);
        FRS3.setName("FRS3");
        p_contenu.add(FRS3);
        FRS3.setBounds(55, 121, 58, FRS3.getPreferredSize().height);

        //---- FRS4 ----
        FRS4.setComponentPopupMenu(BTD);
        FRS4.setName("FRS4");
        p_contenu.add(FRS4);
        FRS4.setBounds(55, 154, 58, FRS4.getPreferredSize().height);

        //---- FRS5 ----
        FRS5.setComponentPopupMenu(BTD);
        FRS5.setName("FRS5");
        p_contenu.add(FRS5);
        FRS5.setBounds(55, 187, 58, FRS5.getPreferredSize().height);

        //---- COL1 ----
        COL1.setComponentPopupMenu(BTD);
        COL1.setName("COL1");
        p_contenu.add(COL1);
        COL1.setBounds(25, 55, 20, COL1.getPreferredSize().height);

        //---- COL2 ----
        COL2.setComponentPopupMenu(BTD);
        COL2.setName("COL2");
        p_contenu.add(COL2);
        COL2.setBounds(25, 88, 20, COL2.getPreferredSize().height);

        //---- COL3 ----
        COL3.setComponentPopupMenu(BTD);
        COL3.setName("COL3");
        p_contenu.add(COL3);
        COL3.setBounds(25, 121, 20, COL3.getPreferredSize().height);

        //---- COL4 ----
        COL4.setComponentPopupMenu(BTD);
        COL4.setName("COL4");
        p_contenu.add(COL4);
        COL4.setBounds(25, 154, 20, COL4.getPreferredSize().height);

        //---- COL5 ----
        COL5.setComponentPopupMenu(BTD);
        COL5.setName("COL5");
        p_contenu.add(COL5);
        COL5.setBounds(25, 187, 20, COL5.getPreferredSize().height);

        //---- separator1 ----
        separator1.setName("separator1");
        p_contenu.add(separator1);
        separator1.setBounds(15, 15, 600, separator1.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private JPanel p_contenu;
  private RiZoneSortie OBJ_23;
  private RiZoneSortie OBJ_24;
  private RiZoneSortie OBJ_27;
  private RiZoneSortie OBJ_28;
  private RiZoneSortie OBJ_31;
  private RiZoneSortie OBJ_32;
  private RiZoneSortie OBJ_35;
  private RiZoneSortie OBJ_36;
  private RiZoneSortie OBJ_39;
  private RiZoneSortie OBJ_40;
  private XRiTextField FRS1;
  private XRiTextField FRS2;
  private XRiTextField FRS3;
  private XRiTextField FRS4;
  private XRiTextField FRS5;
  private XRiTextField COL1;
  private XRiTextField COL2;
  private XRiTextField COL3;
  private XRiTextField COL4;
  private XRiTextField COL5;
  private JComponent separator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
