/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.vgam.VGAM3BFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Détail d'une importation de catalogue fournisseur.
 */
public class VGAM3BFM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String IMPORTER = "Importer";
  private static final String[] WTP01_TOP = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10",
      "WTP11", "WTP12", "WTP13", "WTP14", "WTP15" };
  private static final String[] WTP01_TTITLE = { "TIT01" };
  private static final String[][] WTP01_DATA = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", },
      { "LD07", }, { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", } };
  private int[] _WTP01_Width = { 900, };
  private static final String[] WMOD_VALUE = { "1", "2", };
  private static final String[] WRES_VALUE = { "", "1", "2", "3", };
  private static final String[] WEXCL_VALUE = { "", "1", };
  private boolean WRCHhasfocus = false;
  
  public VGAM3BFM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(WTP01_TOP, WTP01_TTITLE, WTP01_DATA, _WTP01_Width, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
    WMOD.setValeurs(WMOD_VALUE, null);
    WRES.setValeurs(WRES_VALUE, null);
    WEXCL.setValeurs(WEXCL_VALUE, null);
    
    WSELG.setValeursSelection("1", "");
    WSEL01.setValeursSelection("N", "");
    WSEL02.setValeursSelection("N", "");
    WSEL03.setValeursSelection("N", "");
    WSEL04.setValeursSelection("N", "");
    WSEL05.setValeursSelection("N", "");
    WSEL06.setValeursSelection("N", "");
    WSEL07.setValeursSelection("N", "");
    WSEL08.setValeursSelection("N", "");
    WSEL09.setValeursSelection("N", "");
    WSEL10.setValeursSelection("N", "");
    WSEL11.setValeursSelection("N", "");
    WSEL12.setValeursSelection("N", "");
    WSEL13.setValeursSelection("N", "");
    WSEL14.setValeursSelection("N", "");
    WSEL15.setValeursSelection("N", "");
    
    // Configurer la barre de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(IMPORTER, 'i', true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    NB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB1@")).trim());
    NB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB2@")).trim());
    NBB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBB@")).trim());
    NBC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBC@")).trim());
    NB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB3@")).trim());
    NB8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB8@")).trim());
    NB4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB4@")).trim());
    NB9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB9@")).trim());
    lbTitreListeArticle.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBA@ articles dans le fichier (le fournisseur a @NB6@ articles)")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Afficher les erreurs dans une boîte de dialogue
    gererLesErreurs("19");
    
    boolean is67 = lexique.isTrue("67");
    
    lbVariation.setVisible(is67);
    lbGroupe.setVisible(is67);
    lbFamille.setVisible(is67);
    lbSousFamille.setVisible(is67);
    pnlVariations.setVisible(is67);
    lbVarGene.setVisible(is67);
    
    // Force le curseur dans la zone de filtre (si celle ci a eut le focus précédemment)
    if (WRCHhasfocus) {
      setRequestComponent(WRCH);
      WRCHhasfocus = false;
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initialiserRecherche() {
    // Initialiser les critères de recherche
    WRES.setSelectedIndex(0);
    WRCH.setText("");
    WEXCL.setSelectedIndex(0);
    WVAR.setText("");
    WGRP.setText("");
    WFAM.setText("");
    WSFAM.setText("");
    
    // Mettre le focus sur la recherche générique
    WRCHhasfocus = true;
    
    // Lancer la valeur
    lancerRecherche();
  }
  
  private void lancerRecherche() {
    // Désélectionner toutes lignes du tableau pour ne pas afficher le détail de la ligne lorsqu'on va envoyer entrée
    WTP01.clearSelection();
    
    // Mémoriser que la recherche générique à le focus
    if (WRCH.hasFocus()) {
      WRCHhasfocus = true;
    }
    
    // Envoyer "ENTER" au programme RPG
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  // Méthode évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        lancerRecherche();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        initialiserRecherche();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(IMPORTER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      WTP01.setValeurTop("1");
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void fichArtActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WMODItemStateChanged(ItemEvent e) {
    if (e.getStateChange() == ItemEvent.SELECTED) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void WRESItemStateChanged(ItemEvent e) {
    if (e.getStateChange() == ItemEvent.SELECTED) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void WRCHFocusGained(FocusEvent e) {
    // Force la position du curseur après le contenu
    WRCH.setCaretPosition(WRCH.getText().length());
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WRAZT", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WRAZN", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void MiErreursActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WSELGItemStateChanged(ItemEvent e) {
    try {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    baTitre = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlHaut = new SNPanel();
    pnlFiltre = new SNPanelTitre();
    pnlFiltreGauche = new SNPanel();
    sNLabel2 = new SNLabelChamp();
    WMOD = new XRiComboBox();
    sNLabel3 = new SNLabelChamp();
    WRES = new XRiComboBox();
    sNLabel4 = new SNLabelChamp();
    WRCH = new XRiTextField();
    lbVariation = new SNLabelChamp();
    pnlVariation = new SNPanel();
    WEXCL = new XRiComboBox();
    WVAR = new XRiTextField();
    pnlFiltreDroite = new SNPanel();
    lbGroupe = new SNLabelChamp();
    WGRP = new XRiTextField();
    lbFamille = new SNLabelChamp();
    WFAM = new XRiTextField();
    lbSousFamille = new SNLabelChamp();
    WSFAM = new XRiTextField();
    snBarreRecherche = new SNBarreRecherche();
    pnlStatistiques = new SNPanelTitre();
    sNLabel1 = new SNLabelChamp();
    sNLabel5 = new SNLabelChamp();
    sNLabel6 = new SNLabelChamp();
    NB1 = new RiZoneSortie();
    NB2 = new RiZoneSortie();
    sNLabel7 = new SNLabelChamp();
    NBB = new RiZoneSortie();
    NBC = new RiZoneSortie();
    sNLabel8 = new SNLabelChamp();
    NB3 = new RiZoneSortie();
    NB8 = new RiZoneSortie();
    sNLabel9 = new SNLabelChamp();
    NB4 = new RiZoneSortie();
    NB9 = new RiZoneSortie();
    pnlTitreListeArticle = new SNPanel();
    lbTitreListeArticle = new SNLabelTitre();
    pnlVariationGenerale = new SNPanel();
    lbVarGene = new SNLabelChamp();
    WINC = new XRiTextField();
    WSELG = new XRiCheckBox();
    sNLabel10 = new SNLabelChamp();
    pnlListeArticle = new SNPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    pnlVariations = new JPanel();
    VR01 = new XRiTextField();
    VR02 = new XRiTextField();
    VR03 = new XRiTextField();
    VR04 = new XRiTextField();
    VR05 = new XRiTextField();
    VR06 = new XRiTextField();
    VR07 = new XRiTextField();
    VR08 = new XRiTextField();
    VR09 = new XRiTextField();
    VR10 = new XRiTextField();
    VR11 = new XRiTextField();
    VR12 = new XRiTextField();
    VR13 = new XRiTextField();
    VR14 = new XRiTextField();
    VR15 = new XRiTextField();
    label1 = new JLabel();
    panel1 = new JPanel();
    label2 = new JLabel();
    WSEL01 = new XRiCheckBox();
    WSEL02 = new XRiCheckBox();
    WSEL03 = new XRiCheckBox();
    WSEL04 = new XRiCheckBox();
    WSEL05 = new XRiCheckBox();
    WSEL06 = new XRiCheckBox();
    WSEL07 = new XRiCheckBox();
    WSEL08 = new XRiCheckBox();
    WSEL09 = new XRiCheckBox();
    WSEL10 = new XRiCheckBox();
    WSEL11 = new XRiCheckBox();
    WSEL12 = new XRiCheckBox();
    WSEL13 = new XRiCheckBox();
    WSEL14 = new XRiCheckBox();
    WSEL15 = new XRiCheckBox();
    sNPanel1 = new SNPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    snBarreBouton = new SNBarreBouton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    fichArt = new JMenuItem();
    MiErreurs = new JMenuItem();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu_bt7 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());

    //---- baTitre ----
    baTitre.setText("D\u00e9tail d'une importation de catalogue fournisseur");
    baTitre.setName("baTitre");
    add(baTitre, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};

      //======== pnlHaut ========
      {
        pnlHaut.setName("pnlHaut");
        pnlHaut.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlHaut.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlHaut.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlHaut.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlHaut.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlFiltre ========
        {
          pnlFiltre.setTitre("Crit\u00e8res de s\u00e9lection");
          pnlFiltre.setName("pnlFiltre");
          pnlFiltre.setLayout(new GridLayout());

          //======== pnlFiltreGauche ========
          {
            pnlFiltreGauche.setName("pnlFiltreGauche");
            pnlFiltreGauche.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlFiltreGauche.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlFiltreGauche.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
            ((GridBagLayout)pnlFiltreGauche.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlFiltreGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- sNLabel2 ----
            sNLabel2.setText("Mode");
            sNLabel2.setName("sNLabel2");
            pnlFiltreGauche.add(sNLabel2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- WMOD ----
            WMOD.setModel(new DefaultComboBoxModel(new String[] {
              "Cr\u00e9ation",
              "Modification"
            }));
            WMOD.setMinimumSize(new Dimension(150, 30));
            WMOD.setPreferredSize(new Dimension(150, 30));
            WMOD.setMaximumSize(new Dimension(150, 30));
            WMOD.setFont(new Font("sansserif", Font.PLAIN, 14));
            WMOD.setName("WMOD");
            WMOD.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                WMODItemStateChanged(e);
              }
            });
            pnlFiltreGauche.add(WMOD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- sNLabel3 ----
            sNLabel3.setText("Etat de l'importation");
            sNLabel3.setName("sNLabel3");
            pnlFiltreGauche.add(sNLabel3, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- WRES ----
            WRES.setModel(new DefaultComboBoxModel(new String[] {
              "Tous",
              "Import\u00e9s",
              "Erron\u00e9s",
              "Non import\u00e9s"
            }));
            WRES.setPreferredSize(new Dimension(150, 30));
            WRES.setMinimumSize(new Dimension(150, 30));
            WRES.setMaximumSize(new Dimension(150, 30));
            WRES.setFont(new Font("sansserif", Font.PLAIN, 14));
            WRES.setName("WRES");
            WRES.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                WRESItemStateChanged(e);
              }
            });
            pnlFiltreGauche.add(WRES, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- sNLabel4 ----
            sNLabel4.setText("Recherche g\u00e9n\u00e9rique");
            sNLabel4.setName("sNLabel4");
            pnlFiltreGauche.add(sNLabel4, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- WRCH ----
            WRCH.setSelectAll(false);
            WRCH.setPreferredSize(new Dimension(200, 30));
            WRCH.setMinimumSize(new Dimension(200, 30));
            WRCH.setMaximumSize(new Dimension(200, 30));
            WRCH.setFont(new Font("sansserif", Font.PLAIN, 14));
            WRCH.setName("WRCH");
            WRCH.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                WRCHFocusGained(e);
              }
            });
            pnlFiltreGauche.add(WRCH, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbVariation ----
            lbVariation.setText("Variation prix de vente");
            lbVariation.setName("lbVariation");
            pnlFiltreGauche.add(lbVariation, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //======== pnlVariation ========
            {
              pnlVariation.setName("pnlVariation");
              pnlVariation.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlVariation.getLayout()).columnWidths = new int[] {0, 0, 0};
              ((GridBagLayout)pnlVariation.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)pnlVariation.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
              ((GridBagLayout)pnlVariation.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- WEXCL ----
              WEXCL.setModel(new DefaultComboBoxModel(new String[] {
                "Inf\u00e9rieure \u00e0",
                "Sup\u00e9rieure \u00e0"
              }));
              WEXCL.setMinimumSize(new Dimension(150, 30));
              WEXCL.setPreferredSize(new Dimension(150, 30));
              WEXCL.setFont(new Font("sansserif", Font.PLAIN, 14));
              WEXCL.setName("WEXCL");
              pnlVariation.add(WEXCL, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- WVAR ----
              WVAR.setPreferredSize(new Dimension(80, 30));
              WVAR.setMinimumSize(new Dimension(80, 30));
              WVAR.setMaximumSize(new Dimension(80, 30));
              WVAR.setFont(new Font("sansserif", Font.PLAIN, 14));
              WVAR.setName("WVAR");
              pnlVariation.add(WVAR, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlFiltreGauche.add(pnlVariation, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlFiltre.add(pnlFiltreGauche);

          //======== pnlFiltreDroite ========
          {
            pnlFiltreDroite.setName("pnlFiltreDroite");
            pnlFiltreDroite.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlFiltreDroite.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlFiltreDroite.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
            ((GridBagLayout)pnlFiltreDroite.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlFiltreDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0, 1.0E-4};

            //---- lbGroupe ----
            lbGroupe.setText("Groupe");
            lbGroupe.setName("lbGroupe");
            pnlFiltreDroite.add(lbGroupe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- WGRP ----
            WGRP.setSelectAll(false);
            WGRP.setPreferredSize(new Dimension(60, 30));
            WGRP.setMinimumSize(new Dimension(60, 30));
            WGRP.setMaximumSize(new Dimension(60, 30));
            WGRP.setComponentPopupMenu(BTD);
            WGRP.setFont(new Font("sansserif", Font.PLAIN, 14));
            WGRP.setName("WGRP");
            WGRP.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                WRCHFocusGained(e);
              }
            });
            pnlFiltreDroite.add(WGRP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbFamille ----
            lbFamille.setText("Famille");
            lbFamille.setName("lbFamille");
            pnlFiltreDroite.add(lbFamille, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- WFAM ----
            WFAM.setSelectAll(false);
            WFAM.setPreferredSize(new Dimension(60, 30));
            WFAM.setMinimumSize(new Dimension(60, 30));
            WFAM.setMaximumSize(new Dimension(60, 30));
            WFAM.setComponentPopupMenu(BTD);
            WFAM.setFont(new Font("sansserif", Font.PLAIN, 14));
            WFAM.setName("WFAM");
            WFAM.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                WRCHFocusGained(e);
              }
            });
            pnlFiltreDroite.add(WFAM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbSousFamille ----
            lbSousFamille.setText("Sous-famille");
            lbSousFamille.setName("lbSousFamille");
            pnlFiltreDroite.add(lbSousFamille, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- WSFAM ----
            WSFAM.setSelectAll(false);
            WSFAM.setPreferredSize(new Dimension(60, 30));
            WSFAM.setMinimumSize(new Dimension(60, 30));
            WSFAM.setMaximumSize(new Dimension(60, 30));
            WSFAM.setComponentPopupMenu(BTD);
            WSFAM.setFont(new Font("sansserif", Font.PLAIN, 14));
            WSFAM.setName("WSFAM");
            WSFAM.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                WRCHFocusGained(e);
              }
            });
            pnlFiltreDroite.add(WSFAM, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- snBarreRecherche ----
            snBarreRecherche.setMaximumSize(new Dimension(305, 65));
            snBarreRecherche.setMinimumSize(new Dimension(305, 65));
            snBarreRecherche.setPreferredSize(new Dimension(305, 65));
            snBarreRecherche.setName("snBarreRecherche");
            pnlFiltreDroite.add(snBarreRecherche, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0,
              GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre.add(pnlFiltreDroite);
        }
        pnlHaut.add(pnlFiltre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlStatistiques ========
        {
          pnlStatistiques.setTitre("R\u00e9sultat de l'importation");
          pnlStatistiques.setName("pnlStatistiques");
          pnlStatistiques.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlStatistiques.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlStatistiques.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlStatistiques.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlStatistiques.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};

          //---- sNLabel1 ----
          sNLabel1.setText("Cr\u00e9ation");
          sNLabel1.setPreferredSize(new Dimension(80, 30));
          sNLabel1.setHorizontalAlignment(SwingConstants.CENTER);
          sNLabel1.setVerticalAlignment(SwingConstants.BOTTOM);
          sNLabel1.setName("sNLabel1");
          pnlStatistiques.add(sNLabel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- sNLabel5 ----
          sNLabel5.setText("Modification");
          sNLabel5.setPreferredSize(new Dimension(80, 30));
          sNLabel5.setHorizontalAlignment(SwingConstants.CENTER);
          sNLabel5.setVerticalAlignment(SwingConstants.BOTTOM);
          sNLabel5.setName("sNLabel5");
          pnlStatistiques.add(sNLabel5, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- sNLabel6 ----
          sNLabel6.setText("Dans le fichier");
          sNLabel6.setPreferredSize(new Dimension(100, 30));
          sNLabel6.setMinimumSize(new Dimension(100, 30));
          sNLabel6.setMaximumSize(new Dimension(100, 30));
          sNLabel6.setName("sNLabel6");
          pnlStatistiques.add(sNLabel6, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- NB1 ----
          NB1.setText("@NB1@");
          NB1.setMinimumSize(new Dimension(80, 30));
          NB1.setMaximumSize(new Dimension(80, 30));
          NB1.setPreferredSize(new Dimension(80, 30));
          NB1.setFont(new Font("sansserif", Font.PLAIN, 14));
          NB1.setHorizontalAlignment(SwingConstants.TRAILING);
          NB1.setName("NB1");
          pnlStatistiques.add(NB1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- NB2 ----
          NB2.setText("@NB2@");
          NB2.setMinimumSize(new Dimension(80, 30));
          NB2.setMaximumSize(new Dimension(80, 30));
          NB2.setPreferredSize(new Dimension(80, 30));
          NB2.setFont(new Font("sansserif", Font.PLAIN, 14));
          NB2.setHorizontalAlignment(SwingConstants.TRAILING);
          NB2.setName("NB2");
          pnlStatistiques.add(NB2, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- sNLabel7 ----
          sNLabel7.setText("Import\u00e9s");
          sNLabel7.setPreferredSize(new Dimension(100, 30));
          sNLabel7.setMinimumSize(new Dimension(100, 30));
          sNLabel7.setMaximumSize(new Dimension(100, 30));
          sNLabel7.setName("sNLabel7");
          pnlStatistiques.add(sNLabel7, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- NBB ----
          NBB.setText("@NBB@");
          NBB.setPreferredSize(new Dimension(80, 30));
          NBB.setMinimumSize(new Dimension(80, 30));
          NBB.setMaximumSize(new Dimension(80, 30));
          NBB.setHorizontalAlignment(SwingConstants.TRAILING);
          NBB.setFont(new Font("sansserif", Font.PLAIN, 14));
          NBB.setName("NBB");
          pnlStatistiques.add(NBB, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- NBC ----
          NBC.setText("@NBC@");
          NBC.setMinimumSize(new Dimension(80, 30));
          NBC.setMaximumSize(new Dimension(80, 30));
          NBC.setPreferredSize(new Dimension(80, 30));
          NBC.setFont(new Font("sansserif", Font.PLAIN, 14));
          NBC.setHorizontalAlignment(SwingConstants.TRAILING);
          NBC.setName("NBC");
          pnlStatistiques.add(NBC, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- sNLabel8 ----
          sNLabel8.setText("Imports r\u00e9ussis");
          sNLabel8.setPreferredSize(new Dimension(100, 30));
          sNLabel8.setMinimumSize(new Dimension(100, 30));
          sNLabel8.setMaximumSize(new Dimension(100, 30));
          sNLabel8.setName("sNLabel8");
          pnlStatistiques.add(sNLabel8, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- NB3 ----
          NB3.setText("@NB3@");
          NB3.setPreferredSize(new Dimension(80, 30));
          NB3.setMinimumSize(new Dimension(80, 30));
          NB3.setMaximumSize(new Dimension(80, 30));
          NB3.setHorizontalAlignment(SwingConstants.TRAILING);
          NB3.setFont(new Font("sansserif", Font.PLAIN, 14));
          NB3.setName("NB3");
          pnlStatistiques.add(NB3, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- NB8 ----
          NB8.setMaximumSize(new Dimension(80, 30));
          NB8.setMinimumSize(new Dimension(80, 30));
          NB8.setPreferredSize(new Dimension(80, 30));
          NB8.setHorizontalAlignment(SwingConstants.TRAILING);
          NB8.setText("@NB8@");
          NB8.setFont(new Font("sansserif", Font.PLAIN, 14));
          NB8.setName("NB8");
          pnlStatistiques.add(NB8, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- sNLabel9 ----
          sNLabel9.setText("Imports erron\u00e9s");
          sNLabel9.setPreferredSize(new Dimension(100, 30));
          sNLabel9.setMinimumSize(new Dimension(100, 30));
          sNLabel9.setMaximumSize(new Dimension(100, 30));
          sNLabel9.setName("sNLabel9");
          pnlStatistiques.add(sNLabel9, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- NB4 ----
          NB4.setText("@NB4@");
          NB4.setPreferredSize(new Dimension(80, 30));
          NB4.setMinimumSize(new Dimension(80, 30));
          NB4.setMaximumSize(new Dimension(80, 30));
          NB4.setHorizontalAlignment(SwingConstants.TRAILING);
          NB4.setFont(new Font("sansserif", Font.PLAIN, 14));
          NB4.setName("NB4");
          pnlStatistiques.add(NB4, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- NB9 ----
          NB9.setText("@NB9@");
          NB9.setMinimumSize(new Dimension(80, 30));
          NB9.setMaximumSize(new Dimension(80, 30));
          NB9.setPreferredSize(new Dimension(80, 30));
          NB9.setFont(new Font("sansserif", Font.PLAIN, 14));
          NB9.setHorizontalAlignment(SwingConstants.TRAILING);
          NB9.setName("NB9");
          pnlStatistiques.add(NB9, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlHaut.add(pnlStatistiques, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlHaut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlTitreListeArticle ========
      {
        pnlTitreListeArticle.setName("pnlTitreListeArticle");
        pnlTitreListeArticle.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlTitreListeArticle.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlTitreListeArticle.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlTitreListeArticle.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlTitreListeArticle.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- lbTitreListeArticle ----
        lbTitreListeArticle.setText("@NBA@ articles dans le fichier (le fournisseur a @NB6@ articles)");
        lbTitreListeArticle.setName("lbTitreListeArticle");
        pnlTitreListeArticle.add(lbTitreListeArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlVariationGenerale ========
        {
          pnlVariationGenerale.setName("pnlVariationGenerale");
          pnlVariationGenerale.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlVariationGenerale.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlVariationGenerale.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlVariationGenerale.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlVariationGenerale.getLayout()).rowWeights = new double[] {1.0, 1.0E-4};

          //---- lbVarGene ----
          lbVarGene.setText("D\u00e9calage du pourcentage de variation du prix de vente");
          lbVarGene.setMaximumSize(new Dimension(350, 30));
          lbVarGene.setMinimumSize(new Dimension(350, 30));
          lbVarGene.setPreferredSize(new Dimension(350, 30));
          lbVarGene.setName("lbVarGene");
          pnlVariationGenerale.add(lbVarGene, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WINC ----
          WINC.setSelectAll(false);
          WINC.setPreferredSize(new Dimension(55, 30));
          WINC.setMinimumSize(new Dimension(55, 30));
          WINC.setMaximumSize(new Dimension(50, 30));
          WINC.setFont(new Font("sansserif", Font.PLAIN, 14));
          WINC.setName("WINC");
          WINC.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariationGenerale.add(WINC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WSELG ----
          WSELG.setText("S\u00e9lection globale");
          WSELG.setName("WSELG");
          WSELG.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              WSELGItemStateChanged(e);
            }
          });
          pnlVariationGenerale.add(WSELG, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- sNLabel10 ----
          sNLabel10.setMaximumSize(new Dimension(28, 30));
          sNLabel10.setMinimumSize(new Dimension(28, 30));
          sNLabel10.setPreferredSize(new Dimension(28, 30));
          sNLabel10.setName("sNLabel10");
          pnlVariationGenerale.add(sNLabel10, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTitreListeArticle.add(pnlVariationGenerale, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlTitreListeArticle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlListeArticle ========
      {
        pnlListeArticle.setName("pnlListeArticle");
        pnlListeArticle.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlListeArticle.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
        ((GridBagLayout)pnlListeArticle.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlListeArticle.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlListeArticle.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setComponentPopupMenu(BTD);
          SCROLLPANE_LIST.setPreferredSize(new Dimension(600, 330));
          SCROLLPANE_LIST.setMinimumSize(new Dimension(600, 330));
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- WTP01 ----
          WTP01.setComponentPopupMenu(BTD);
          WTP01.setRowHeight(20);
          WTP01.setPreferredScrollableViewportSize(new Dimension(500, 400));
          WTP01.setPreferredSize(new Dimension(600, 300));
          WTP01.setMinimumSize(new Dimension(600, 300));
          WTP01.setName("WTP01");
          WTP01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTP01MouseClicked(e);
            }
          });
          SCROLLPANE_LIST.setViewportView(WTP01);
        }
        pnlListeArticle.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlVariations ========
        {
          pnlVariations.setMinimumSize(new Dimension(55, 100));
          pnlVariations.setMaximumSize(new Dimension(55, 100));
          pnlVariations.setPreferredSize(new Dimension(55, 100));
          pnlVariations.setOpaque(false);
          pnlVariations.setName("pnlVariations");
          pnlVariations.setLayout(null);

          //---- VR01 ----
          VR01.setSelectAll(false);
          VR01.setPreferredSize(new Dimension(50, 22));
          VR01.setMinimumSize(new Dimension(50, 22));
          VR01.setMaximumSize(new Dimension(50, 22));
          VR01.setName("VR01");
          VR01.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR01);
          VR01.setBounds(0, 20, 56, VR01.getPreferredSize().height);

          //---- VR02 ----
          VR02.setSelectAll(false);
          VR02.setPreferredSize(new Dimension(50, 22));
          VR02.setMinimumSize(new Dimension(50, 22));
          VR02.setMaximumSize(new Dimension(50, 22));
          VR02.setName("VR02");
          VR02.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR02);
          VR02.setBounds(0, 40, 56, VR02.getPreferredSize().height);

          //---- VR03 ----
          VR03.setSelectAll(false);
          VR03.setPreferredSize(new Dimension(50, 22));
          VR03.setMinimumSize(new Dimension(50, 22));
          VR03.setMaximumSize(new Dimension(50, 22));
          VR03.setName("VR03");
          VR03.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR03);
          VR03.setBounds(0, 60, 56, VR03.getPreferredSize().height);

          //---- VR04 ----
          VR04.setSelectAll(false);
          VR04.setPreferredSize(new Dimension(50, 22));
          VR04.setMinimumSize(new Dimension(50, 22));
          VR04.setMaximumSize(new Dimension(50, 22));
          VR04.setName("VR04");
          VR04.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR04);
          VR04.setBounds(0, 80, 56, VR04.getPreferredSize().height);

          //---- VR05 ----
          VR05.setSelectAll(false);
          VR05.setPreferredSize(new Dimension(50, 22));
          VR05.setMinimumSize(new Dimension(50, 22));
          VR05.setMaximumSize(new Dimension(50, 22));
          VR05.setName("VR05");
          VR05.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR05);
          VR05.setBounds(0, 100, 56, VR05.getPreferredSize().height);

          //---- VR06 ----
          VR06.setSelectAll(false);
          VR06.setPreferredSize(new Dimension(50, 22));
          VR06.setMinimumSize(new Dimension(50, 22));
          VR06.setMaximumSize(new Dimension(50, 22));
          VR06.setName("VR06");
          VR06.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR06);
          VR06.setBounds(0, 120, 56, VR06.getPreferredSize().height);

          //---- VR07 ----
          VR07.setSelectAll(false);
          VR07.setPreferredSize(new Dimension(50, 22));
          VR07.setMinimumSize(new Dimension(50, 22));
          VR07.setMaximumSize(new Dimension(50, 22));
          VR07.setName("VR07");
          VR07.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR07);
          VR07.setBounds(0, 140, 56, VR07.getPreferredSize().height);

          //---- VR08 ----
          VR08.setSelectAll(false);
          VR08.setPreferredSize(new Dimension(50, 22));
          VR08.setMinimumSize(new Dimension(50, 22));
          VR08.setMaximumSize(new Dimension(50, 22));
          VR08.setName("VR08");
          VR08.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR08);
          VR08.setBounds(0, 160, 56, VR08.getPreferredSize().height);

          //---- VR09 ----
          VR09.setSelectAll(false);
          VR09.setPreferredSize(new Dimension(50, 22));
          VR09.setMinimumSize(new Dimension(50, 22));
          VR09.setMaximumSize(new Dimension(50, 22));
          VR09.setName("VR09");
          VR09.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR09);
          VR09.setBounds(0, 180, 56, VR09.getPreferredSize().height);

          //---- VR10 ----
          VR10.setSelectAll(false);
          VR10.setPreferredSize(new Dimension(50, 22));
          VR10.setMinimumSize(new Dimension(50, 22));
          VR10.setMaximumSize(new Dimension(50, 22));
          VR10.setName("VR10");
          VR10.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR10);
          VR10.setBounds(0, 200, 56, VR10.getPreferredSize().height);

          //---- VR11 ----
          VR11.setSelectAll(false);
          VR11.setPreferredSize(new Dimension(50, 22));
          VR11.setMinimumSize(new Dimension(50, 22));
          VR11.setMaximumSize(new Dimension(50, 22));
          VR11.setName("VR11");
          VR11.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR11);
          VR11.setBounds(0, 220, 56, VR11.getPreferredSize().height);

          //---- VR12 ----
          VR12.setSelectAll(false);
          VR12.setPreferredSize(new Dimension(50, 22));
          VR12.setMinimumSize(new Dimension(50, 22));
          VR12.setMaximumSize(new Dimension(50, 22));
          VR12.setName("VR12");
          VR12.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR12);
          VR12.setBounds(0, 240, 56, VR12.getPreferredSize().height);

          //---- VR13 ----
          VR13.setSelectAll(false);
          VR13.setPreferredSize(new Dimension(50, 22));
          VR13.setMinimumSize(new Dimension(50, 22));
          VR13.setMaximumSize(new Dimension(50, 22));
          VR13.setName("VR13");
          VR13.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR13);
          VR13.setBounds(0, 260, 56, VR13.getPreferredSize().height);

          //---- VR14 ----
          VR14.setSelectAll(false);
          VR14.setPreferredSize(new Dimension(50, 22));
          VR14.setMinimumSize(new Dimension(50, 22));
          VR14.setMaximumSize(new Dimension(50, 22));
          VR14.setName("VR14");
          VR14.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR14);
          VR14.setBounds(0, 280, 56, VR14.getPreferredSize().height);

          //---- VR15 ----
          VR15.setSelectAll(false);
          VR15.setPreferredSize(new Dimension(50, 22));
          VR15.setMinimumSize(new Dimension(50, 22));
          VR15.setMaximumSize(new Dimension(50, 22));
          VR15.setName("VR15");
          VR15.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WRCHFocusGained(e);
            }
          });
          pnlVariations.add(VR15);
          VR15.setBounds(0, 300, 56, VR15.getPreferredSize().height);

          //---- label1 ----
          label1.setText("% var. PV");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() - 2f));
          label1.setToolTipText("% de variation du prix de vente");
          label1.setName("label1");
          pnlVariations.add(label1);
          label1.setBounds(5, 0, 50, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < pnlVariations.getComponentCount(); i++) {
              Rectangle bounds = pnlVariations.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlVariations.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlVariations.setMinimumSize(preferredSize);
            pnlVariations.setPreferredSize(preferredSize);
          }
        }
        pnlListeArticle.add(pnlVariations, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(new GridBagLayout());
          ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- label2 ----
          label2.setText("S\u00e9lection");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getSize() - 2f));
          label2.setToolTipText("% de variation du prix de vente");
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setMinimumSize(new Dimension(46, 20));
          label2.setPreferredSize(new Dimension(46, 20));
          label2.setName("label2");
          panel1.add(label2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL01 ----
          WSEL01.setName("WSEL01");
          panel1.add(WSEL01, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL02 ----
          WSEL02.setName("WSEL02");
          panel1.add(WSEL02, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL03 ----
          WSEL03.setName("WSEL03");
          panel1.add(WSEL03, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL04 ----
          WSEL04.setName("WSEL04");
          panel1.add(WSEL04, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL05 ----
          WSEL05.setName("WSEL05");
          panel1.add(WSEL05, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL06 ----
          WSEL06.setName("WSEL06");
          panel1.add(WSEL06, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL07 ----
          WSEL07.setName("WSEL07");
          panel1.add(WSEL07, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL08 ----
          WSEL08.setName("WSEL08");
          panel1.add(WSEL08, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL09 ----
          WSEL09.setName("WSEL09");
          panel1.add(WSEL09, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL10 ----
          WSEL10.setName("WSEL10");
          panel1.add(WSEL10, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL11 ----
          WSEL11.setName("WSEL11");
          panel1.add(WSEL11, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL12 ----
          WSEL12.setName("WSEL12");
          panel1.add(WSEL12, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL13 ----
          WSEL13.setName("WSEL13");
          panel1.add(WSEL13, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL14 ----
          WSEL14.setName("WSEL14");
          panel1.add(WSEL14, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 2, 0), 0, 0));

          //---- WSEL15 ----
          WSEL15.setName("WSEL15");
          panel1.add(WSEL15, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlListeArticle.add(panel1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== sNPanel1 ========
        {
          sNPanel1.setName("sNPanel1");
          sNPanel1.setLayout(new GridLayout(2, 0, 5, 5));

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setPreferredSize(new Dimension(28, 155));
          BT_PGUP.setMaximumSize(new Dimension(28, 155));
          BT_PGUP.setMinimumSize(new Dimension(28, 155));
          BT_PGUP.setName("BT_PGUP");
          sNPanel1.add(BT_PGUP);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setMaximumSize(new Dimension(28, 155));
          BT_PGDOWN.setMinimumSize(new Dimension(28, 155));
          BT_PGDOWN.setPreferredSize(new Dimension(28, 155));
          BT_PGDOWN.setName("BT_PGDOWN");
          sNPanel1.add(BT_PGDOWN);
        }
        pnlListeArticle.add(sNPanel1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlListeArticle, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("D\u00e9tail");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- fichArt ----
      fichArt.setText("Fiche article");
      fichArt.setName("fichArt");
      fichArt.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          fichArtActionPerformed(e);
        }
      });
      BTD.add(fichArt);

      //---- MiErreurs ----
      MiErreurs.setText("Voir les erreurs");
      MiErreurs.setName("MiErreurs");
      MiErreurs.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MiErreursActionPerformed(e);
        }
      });
      BTD.add(MiErreurs);
    }

    //---- riSousMenu_bt6 ----
    riSousMenu_bt6.setText("Tous % variation \u00e0 0");
    riSousMenu_bt6.setToolTipText("Remise \u00e0 z\u00e9ro de tous les pourcentage de variation du prix de vente");
    riSousMenu_bt6.setName("riSousMenu_bt6");
    riSousMenu_bt6.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        riSousMenu_bt6ActionPerformed(e);
      }
    });

    //---- riSousMenu_bt7 ----
    riSousMenu_bt7.setText("% variation n\u00e9gatifs \u00e0 0");
    riSousMenu_bt7.setToolTipText("Remise \u00e0 z\u00e9ro des pourcentage de variation n\u00e9gatifs du prix de vente");
    riSousMenu_bt7.setName("riSousMenu_bt7");
    riSousMenu_bt7.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        riSousMenu_bt7ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre baTitre;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlHaut;
  private SNPanelTitre pnlFiltre;
  private SNPanel pnlFiltreGauche;
  private SNLabelChamp sNLabel2;
  private XRiComboBox WMOD;
  private SNLabelChamp sNLabel3;
  private XRiComboBox WRES;
  private SNLabelChamp sNLabel4;
  private XRiTextField WRCH;
  private SNLabelChamp lbVariation;
  private SNPanel pnlVariation;
  private XRiComboBox WEXCL;
  private XRiTextField WVAR;
  private SNPanel pnlFiltreDroite;
  private SNLabelChamp lbGroupe;
  private XRiTextField WGRP;
  private SNLabelChamp lbFamille;
  private XRiTextField WFAM;
  private SNLabelChamp lbSousFamille;
  private XRiTextField WSFAM;
  private SNBarreRecherche snBarreRecherche;
  private SNPanelTitre pnlStatistiques;
  private SNLabelChamp sNLabel1;
  private SNLabelChamp sNLabel5;
  private SNLabelChamp sNLabel6;
  private RiZoneSortie NB1;
  private RiZoneSortie NB2;
  private SNLabelChamp sNLabel7;
  private RiZoneSortie NBB;
  private RiZoneSortie NBC;
  private SNLabelChamp sNLabel8;
  private RiZoneSortie NB3;
  private RiZoneSortie NB8;
  private SNLabelChamp sNLabel9;
  private RiZoneSortie NB4;
  private RiZoneSortie NB9;
  private SNPanel pnlTitreListeArticle;
  private SNLabelTitre lbTitreListeArticle;
  private SNPanel pnlVariationGenerale;
  private SNLabelChamp lbVarGene;
  private XRiTextField WINC;
  private XRiCheckBox WSELG;
  private SNLabelChamp sNLabel10;
  private SNPanel pnlListeArticle;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JPanel pnlVariations;
  private XRiTextField VR01;
  private XRiTextField VR02;
  private XRiTextField VR03;
  private XRiTextField VR04;
  private XRiTextField VR05;
  private XRiTextField VR06;
  private XRiTextField VR07;
  private XRiTextField VR08;
  private XRiTextField VR09;
  private XRiTextField VR10;
  private XRiTextField VR11;
  private XRiTextField VR12;
  private XRiTextField VR13;
  private XRiTextField VR14;
  private XRiTextField VR15;
  private JLabel label1;
  private JPanel panel1;
  private JLabel label2;
  private XRiCheckBox WSEL01;
  private XRiCheckBox WSEL02;
  private XRiCheckBox WSEL03;
  private XRiCheckBox WSEL04;
  private XRiCheckBox WSEL05;
  private XRiCheckBox WSEL06;
  private XRiCheckBox WSEL07;
  private XRiCheckBox WSEL08;
  private XRiCheckBox WSEL09;
  private XRiCheckBox WSEL10;
  private XRiCheckBox WSEL11;
  private XRiCheckBox WSEL12;
  private XRiCheckBox WSEL13;
  private XRiCheckBox WSEL14;
  private XRiCheckBox WSEL15;
  private SNPanel sNPanel1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem fichArt;
  private JMenuItem MiErreurs;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu_bt riSousMenu_bt7;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
