
package ri.serien.libecranrpg.vgam.VGAM18FM;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.achat.documentachat.snacheteur.SNAcheteur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGAM18FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private Message V03F = null;
  private static final String BOUTON_LANCER_TRAITEMENT = "Lancer le traitement";
  private static final String BOUTON_LIGNE_CENTRALISATION = "Lignes de centralisation";
  
  public VGAM18FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    initDiverses();
    
    WDPR.setValeurs("X");
    WGEN.setValeurs("X");
    WDPRM.setValeursSelection("X", "");
    WDPRF.setValeursSelection("X", "");
    WHOM.setValeursSelection("X", "");
    WEDT.setValeursSelection("X", "");
    WEDTF.setValeursSelection("X", "");
    WEDTM.setValeursSelection("X", "");
    
    xriBarreBouton.ajouterBouton(BOUTON_LANCER_TRAITEMENT, 't', true);
    xriBarreBouton.ajouterBouton(BOUTON_LIGNE_CENTRALISATION, 'c', true);
    xriBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    xriBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    xriBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbV03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Active les bouton liée au V01F
    xriBarreBouton.rafraichir(lexique);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialise les radio bouton
    if (WGEN.isSelected()) {
      WDPRM.setEnabled(true);
      WDPRF.setEnabled(true);
      
      WHOM.setEnabled(false);
      WEDT.setEnabled(false);
      WEDTF.setEnabled(false);
      WEDTM.setEnabled(false);
      
      WHOM.setSelected(false);
      WEDT.setSelected(false);
      WEDTF.setSelected(false);
      WEDTM.setSelected(false);
    }
    else if (WDPR.isSelected()) {
      WDPRM.setEnabled(false);
      WDPRF.setEnabled(false);
      WDPRM.setSelected(false);
      WDPRF.setSelected(false);
      
      WHOM.setEnabled(true);
      WEDT.setEnabled(true);
      WEDTF.setEnabled(true);
      WEDTM.setEnabled(true);
    }
    
    // Gestion de V01F
    lbV03F.setVisible(!lexique.HostFieldGetData("V03F").trim().isEmpty());
    V03F = V03F.getMessageNormal(lexique.HostFieldGetData("V03F"));
    lbV03F.setMessage(V03F);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialise les composants
    chargerMagasin();
    chargerAcheteur();
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snAcheteur.renseignerChampRPG(lexique, "WACH");
  }
  
  /**
   * Initialise le composant magasin
   */
  private void chargerMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
  }
  
  /**
   * Initialise le composant acheteur
   */
  private void chargerAcheteur() {
    snAcheteur.setSession(getSession());
    snAcheteur.setIdEtablissement(snEtablissement.getIdSelection());
    snAcheteur.setTousAutorise(true);
    snAcheteur.charger(false);
    snAcheteur.setSelectionParChampRPG(lexique, "WACH");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    xriBarreBouton.isTraiterClickBouton(pSNBouton);
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_LANCER_TRAITEMENT)) {
        lexique.HostScreenSendKey(this, "F13");
      }
      else if (pSNBouton.isBouton(BOUTON_LIGNE_CENTRALISATION)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void boutonActionPerformed(ActionEvent e) {
    if (WGEN.isSelected()) {
      WDPRM.setEnabled(true);
      WDPRF.setEnabled(true);
      
      WHOM.setEnabled(false);
      WEDT.setEnabled(false);
      WEDTF.setEnabled(false);
      WEDTM.setEnabled(false);
      
      WHOM.setSelected(false);
      WEDT.setSelected(false);
      WEDTF.setSelected(false);
      WEDTM.setSelected(false);
    }
    else if (WDPR.isSelected()) {
      WDPRM.setEnabled(false);
      WDPRF.setEnabled(false);
      WDPRM.setSelected(false);
      WDPRF.setSelected(false);
      
      WHOM.setEnabled(true);
      WEDT.setEnabled(true);
      WEDTF.setEnabled(true);
      WEDTM.setEnabled(true);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissmenet = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbDate = new SNLabelChamp();
    WDATX = new XRiCalendrier();
    lbV03F = new SNMessage();
    pnlAcheteurMagasin = new SNPanelTitre();
    lbAcheteur = new SNLabelChamp();
    snAcheteur = new SNAcheteur();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    pnlOptionTraitement = new SNPanelTitre();
    pnlCommandeAchat = new SNPanel();
    WGEN = new XRiRadioButton();
    pnlOptionCommandeAchat = new SNPanel();
    WDPRF = new XRiCheckBox();
    WDPRM = new XRiCheckBox();
    pnlEditionPrix = new SNPanel();
    WDPR = new XRiRadioButton();
    pnlOptionEditionPrix = new SNPanel();
    WHOM = new XRiCheckBox();
    WEDT = new XRiCheckBox();
    WEDTM = new XRiCheckBox();
    WEDTF = new XRiCheckBox();
    xriBarreBouton = new XRiBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Centralisation de demandes d'achats");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlEtablissement ========
      {
        pnlEtablissement.setOpaque(false);
        pnlEtablissement.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        pnlEtablissement.setName("pnlEtablissement");
        pnlEtablissement.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbEtablissmenet ----
        lbEtablissmenet.setText("Etablissement");
        lbEtablissmenet.setPreferredSize(new Dimension(200, 30));
        lbEtablissmenet.setMinimumSize(new Dimension(200, 30));
        lbEtablissmenet.setMaximumSize(new Dimension(200, 30));
        lbEtablissmenet.setName("lbEtablissmenet");
        pnlEtablissement.add(lbEtablissmenet, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setEnabled(false);
        snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
        snEtablissement.setName("snEtablissement");
        pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbDate ----
        lbDate.setText("Date");
        lbDate.setName("lbDate");
        pnlEtablissement.add(lbDate, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WDATX ----
        WDATX.setPreferredSize(new Dimension(110, 30));
        WDATX.setMinimumSize(new Dimension(110, 30));
        WDATX.setMaximumSize(new Dimension(110, 30));
        WDATX.setFont(new Font("sansserif", Font.PLAIN, 14));
        WDATX.setName("WDATX");
        pnlEtablissement.add(WDATX, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbV03F ----
        lbV03F.setText("@V03F@");
        lbV03F.setName("lbV03F");
        pnlEtablissement.add(lbV03F, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlEtablissement,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlAcheteurMagasin ========
      {
        pnlAcheteurMagasin.setBorder(new TitledBorder(""));
        pnlAcheteurMagasin.setOpaque(false);
        pnlAcheteurMagasin.setName("pnlAcheteurMagasin");
        pnlAcheteurMagasin.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlAcheteurMagasin.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlAcheteurMagasin.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlAcheteurMagasin.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlAcheteurMagasin.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbAcheteur ----
        lbAcheteur.setText("Acheteur pour la  centralisation");
        lbAcheteur.setPreferredSize(new Dimension(200, 30));
        lbAcheteur.setMinimumSize(new Dimension(200, 30));
        lbAcheteur.setMaximumSize(new Dimension(200, 30));
        lbAcheteur.setName("lbAcheteur");
        pnlAcheteurMagasin.add(lbAcheteur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snAcheteur ----
        snAcheteur.setEnabled(false);
        snAcheteur.setFont(new Font("sansserif", Font.PLAIN, 14));
        snAcheteur.setName("snAcheteur");
        pnlAcheteurMagasin.add(snAcheteur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMagasin ----
        lbMagasin.setText("Magasin d'achat");
        lbMagasin.setPreferredSize(new Dimension(200, 30));
        lbMagasin.setMinimumSize(new Dimension(200, 30));
        lbMagasin.setMaximumSize(new Dimension(200, 30));
        lbMagasin.setName("lbMagasin");
        pnlAcheteurMagasin.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snMagasin ----
        snMagasin.setEnabled(false);
        snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
        snMagasin.setName("snMagasin");
        pnlAcheteurMagasin.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlAcheteurMagasin,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlOptionTraitement ========
      {
        pnlOptionTraitement.setOpaque(false);
        pnlOptionTraitement.setTitre("Option de traitement");
        pnlOptionTraitement.setName("pnlOptionTraitement");
        pnlOptionTraitement.setLayout(new GridLayout(1, 1));
        
        // ======== pnlCommandeAchat ========
        {
          pnlCommandeAchat.setName("pnlCommandeAchat");
          pnlCommandeAchat.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCommandeAchat.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlCommandeAchat.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCommandeAchat.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlCommandeAchat.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- WGEN ----
          WGEN.setText("G\u00e9n\u00e9ration de commandes d'achats");
          WGEN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WGEN.setSelected(true);
          WGEN.setFont(new Font("sansserif", Font.PLAIN, 14));
          WGEN.setPreferredSize(new Dimension(255, 30));
          WGEN.setMinimumSize(new Dimension(255, 30));
          WGEN.setMaximumSize(new Dimension(255, 30));
          WGEN.setName("WGEN");
          WGEN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              boutonActionPerformed(e);
            }
          });
          pnlCommandeAchat.add(WGEN, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionCommandeAchat ========
          {
            pnlOptionCommandeAchat.setOpaque(false);
            pnlOptionCommandeAchat.setName("pnlOptionCommandeAchat");
            pnlOptionCommandeAchat.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionCommandeAchat.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionCommandeAchat.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptionCommandeAchat.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionCommandeAchat.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- WDPRF ----
            WDPRF.setText("Envoi par fax");
            WDPRF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WDPRF.setPreferredSize(new Dimension(150, 30));
            WDPRF.setMinimumSize(new Dimension(150, 30));
            WDPRF.setMaximumSize(new Dimension(150, 30));
            WDPRF.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDPRF.setName("WDPRF");
            pnlOptionCommandeAchat.add(WDPRF, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- WDPRM ----
            WDPRM.setText("Envoi par mail");
            WDPRM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WDPRM.setPreferredSize(new Dimension(150, 30));
            WDPRM.setMinimumSize(new Dimension(150, 30));
            WDPRM.setMaximumSize(new Dimension(150, 30));
            WDPRM.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDPRM.setName("WDPRM");
            pnlOptionCommandeAchat.add(WDPRM, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlCommandeAchat.add(pnlOptionCommandeAchat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlOptionTraitement.add(pnlCommandeAchat);
        
        // ======== pnlEditionPrix ========
        {
          pnlEditionPrix.setName("pnlEditionPrix");
          pnlEditionPrix.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEditionPrix.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlEditionPrix.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEditionPrix.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlEditionPrix.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- WDPR ----
          WDPR.setText("Edition de demandes de prix");
          WDPR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WDPR.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDPR.setPreferredSize(new Dimension(255, 30));
          WDPR.setMinimumSize(new Dimension(255, 30));
          WDPR.setMaximumSize(new Dimension(255, 30));
          WDPR.setName("WDPR");
          WDPR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              boutonActionPerformed(e);
            }
          });
          pnlEditionPrix.add(WDPR, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionEditionPrix ========
          {
            pnlOptionEditionPrix.setOpaque(false);
            pnlOptionEditionPrix.setName("pnlOptionEditionPrix");
            pnlOptionEditionPrix.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionEditionPrix.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionEditionPrix.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionEditionPrix.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionEditionPrix.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- WHOM ----
            WHOM.setText("Homologation");
            WHOM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WHOM.setPreferredSize(new Dimension(150, 30));
            WHOM.setMinimumSize(new Dimension(150, 30));
            WHOM.setMaximumSize(new Dimension(150, 30));
            WHOM.setFont(new Font("sansserif", Font.PLAIN, 14));
            WHOM.setName("WHOM");
            pnlOptionEditionPrix.add(WHOM, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WEDT ----
            WEDT.setText("Edition");
            WEDT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WEDT.setPreferredSize(new Dimension(150, 30));
            WEDT.setMinimumSize(new Dimension(150, 30));
            WEDT.setMaximumSize(new Dimension(150, 30));
            WEDT.setFont(new Font("sansserif", Font.PLAIN, 14));
            WEDT.setName("WEDT");
            pnlOptionEditionPrix.add(WEDT, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WEDTM ----
            WEDTM.setText("Envoi par mail");
            WEDTM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WEDTM.setPreferredSize(new Dimension(150, 30));
            WEDTM.setMinimumSize(new Dimension(150, 30));
            WEDTM.setMaximumSize(new Dimension(150, 30));
            WEDTM.setFont(new Font("sansserif", Font.PLAIN, 14));
            WEDTM.setName("WEDTM");
            pnlOptionEditionPrix.add(WEDTM, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- WEDTF ----
            WEDTF.setText("Envoi par fax");
            WEDTF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WEDTF.setPreferredSize(new Dimension(150, 30));
            WEDTF.setMinimumSize(new Dimension(150, 30));
            WEDTF.setMaximumSize(new Dimension(150, 30));
            WEDTF.setFont(new Font("sansserif", Font.PLAIN, 14));
            WEDTF.setName("WEDTF");
            pnlOptionEditionPrix.add(WEDTF, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEditionPrix.add(pnlOptionEditionPrix, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlOptionTraitement.add(pnlEditionPrix);
      }
      pnlContenu.add(pnlOptionTraitement,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- xriBarreBouton ----
    xriBarreBouton.setName("xriBarreBouton");
    add(xriBarreBouton, BorderLayout.SOUTH);
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(WGEN);
    buttonGroup1.add(WDPR);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissmenet;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbDate;
  private XRiCalendrier WDATX;
  private SNMessage lbV03F;
  private SNPanelTitre pnlAcheteurMagasin;
  private SNLabelChamp lbAcheteur;
  private SNAcheteur snAcheteur;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNPanelTitre pnlOptionTraitement;
  private SNPanel pnlCommandeAchat;
  private XRiRadioButton WGEN;
  private SNPanel pnlOptionCommandeAchat;
  private XRiCheckBox WDPRF;
  private XRiCheckBox WDPRM;
  private SNPanel pnlEditionPrix;
  private XRiRadioButton WDPR;
  private SNPanel pnlOptionEditionPrix;
  private XRiCheckBox WHOM;
  private XRiCheckBox WEDT;
  private XRiCheckBox WEDTM;
  private XRiCheckBox WEDTF;
  private XRiBarreBouton xriBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
