
package ri.serien.libecranrpg.vgam.VGAM09FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * [GAM13] Gestion des achats -> Fiches permanentes -> Adresses fournisseurs
 * Indicateur : 001000000
 * Titre :Saisie adresse fournisseur (Boites dialogue)
 */
public class VGAM09FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] FENAT_Value = { "", "C", "E", "F", "R", };
  private String BOUTON_ACCES_CONTACT = "Accéder aux contacts";
  
  public VGAM09FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    FECMD.setValeursSelection("1", "");
    FEFAC.setValeursSelection("1", "");
    FEEXP.setValeursSelection("1", "");
    
    xriBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    xriBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    xriBarreBouton.ajouterBouton(BOUTON_ACCES_CONTACT, 'c', true);
    xriBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
        xriBarreBouton.isTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("Adresse fournisseur"));
    
    // Active les bouton utiliser par l'ecran
    xriBarreBouton.rafraichir(lexique);
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    // Traite les clique des bouton spéciaux
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_ACCES_CONTACT)) {
        lexique.HostScreenSendKey(this, "F20");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlType = new SNPanel();
    lbNumero = new SNLabelChamp();
    INDEXT = new XRiTextField();
    lbType = new SNLabelChamp();
    FECMD = new XRiCheckBox();
    FEFAC = new XRiCheckBox();
    FEEXP = new XRiCheckBox();
    pnlCoordonneesFournisseur = new SNPanelTitre();
    lbNom = new JLabel();
    FENOM = new XRiTextField();
    lbComplementNom = new JLabel();
    FECPL = new XRiTextField();
    lbLocalisation = new JLabel();
    FERUE = new XRiTextField();
    lbRue = new JLabel();
    FELOC = new XRiTextField();
    lbVille = new JLabel();
    lbCodePostal = new JLabel();
    FECDP = new XRiTextField();
    FEVIL = new XRiTextField();
    lbCodePostal2 = new JLabel();
    FEPAYR = new XRiTextField();
    FECOP = new XRiTextField();
    lbEmail = new JLabel();
    FEOBSR = new XRiTextField();
    lbTelephone = new JLabel();
    FETEL = new XRiTextField();
    lbContact = new JLabel();
    FEPAC = new XRiTextField();
    lbFax = new JLabel();
    FEFAX = new XRiTextField();
    xriBarreBouton = new XRiBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(1000, 455));
    setPreferredSize(new Dimension(1000, 455));
    setMaximumSize(new Dimension(1000, 455));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout(5, 5));

      //======== pnlType ========
      {
        pnlType.setOpaque(false);
        pnlType.setName("pnlType");
        pnlType.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlType.getLayout()).columnWidths = new int[] {71, 102, 58, 110, 110, 100, 0};
        ((GridBagLayout)pnlType.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlType.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlType.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- lbNumero ----
        lbNumero.setText("Num\u00e9ro");
        lbNumero.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbNumero.setHorizontalAlignment(SwingConstants.TRAILING);
        lbNumero.setPreferredSize(new Dimension(140, 30));
        lbNumero.setMinimumSize(new Dimension(140, 30));
        lbNumero.setMaximumSize(new Dimension(140, 30));
        lbNumero.setName("lbNumero");
        pnlType.add(lbNumero, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 10), 0, 0));

        //---- INDEXT ----
        INDEXT.setPreferredSize(new Dimension(60, 30));
        INDEXT.setMinimumSize(new Dimension(60, 30));
        INDEXT.setFont(new Font("sansserif", Font.PLAIN, 14));
        INDEXT.setName("INDEXT");
        pnlType.add(INDEXT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 10), 0, 0));

        //---- lbType ----
        lbType.setText("Type :");
        lbType.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbType.setHorizontalAlignment(SwingConstants.TRAILING);
        lbType.setMinimumSize(new Dimension(32, 30));
        lbType.setPreferredSize(new Dimension(32, 30));
        lbType.setMaximumSize(new Dimension(32, 30));
        lbType.setName("lbType");
        pnlType.add(lbType, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 10), 0, 0));

        //---- FECMD ----
        FECMD.setText("Commande");
        FECMD.setFont(new Font("sansserif", Font.PLAIN, 14));
        FECMD.setName("FECMD");
        pnlType.add(FECMD, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 10), 0, 0));

        //---- FEFAC ----
        FEFAC.setText("Facturation");
        FEFAC.setFont(new Font("sansserif", Font.PLAIN, 14));
        FEFAC.setName("FEFAC");
        pnlType.add(FEFAC, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 10), 0, 0));

        //---- FEEXP ----
        FEEXP.setText("Exp\u00e9dition");
        FEEXP.setFont(new Font("sansserif", Font.PLAIN, 14));
        FEEXP.setName("FEEXP");
        pnlType.add(FEEXP, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlType, BorderLayout.NORTH);

      //======== pnlCoordonneesFournisseur ========
      {
        pnlCoordonneesFournisseur.setOpaque(false);
        pnlCoordonneesFournisseur.setFont(new Font("sansserif", Font.PLAIN, 14));
        pnlCoordonneesFournisseur.setTitre("Coordonn\u00e9es fournisseur");
        pnlCoordonneesFournisseur.setName("pnlCoordonneesFournisseur");
        pnlCoordonneesFournisseur.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlCoordonneesFournisseur.getLayout()).columnWidths = new int[] {135, 0, 0, 0, 0, 0, 130, 0};
        ((GridBagLayout)pnlCoordonneesFournisseur.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlCoordonneesFournisseur.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlCoordonneesFournisseur.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- lbNom ----
        lbNom.setText("Raison sociale");
        lbNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbNom.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNom.setName("lbNom");
        pnlCoordonneesFournisseur.add(lbNom, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- FENOM ----
        FENOM.setComponentPopupMenu(null);
        FENOM.setFont(new Font("sansserif", Font.PLAIN, 14));
        FENOM.setName("FENOM");
        pnlCoordonneesFournisseur.add(FENOM, new GridBagConstraints(1, 0, 6, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbComplementNom ----
        lbComplementNom.setText("Compl\u00e9ment");
        lbComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbComplementNom.setHorizontalAlignment(SwingConstants.RIGHT);
        lbComplementNom.setName("lbComplementNom");
        pnlCoordonneesFournisseur.add(lbComplementNom, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- FECPL ----
        FECPL.setComponentPopupMenu(null);
        FECPL.setFont(new Font("sansserif", Font.PLAIN, 14));
        FECPL.setName("FECPL");
        pnlCoordonneesFournisseur.add(FECPL, new GridBagConstraints(1, 1, 6, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbLocalisation ----
        lbLocalisation.setText("Adresse 1");
        lbLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbLocalisation.setHorizontalAlignment(SwingConstants.RIGHT);
        lbLocalisation.setName("lbLocalisation");
        pnlCoordonneesFournisseur.add(lbLocalisation, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- FERUE ----
        FERUE.setComponentPopupMenu(null);
        FERUE.setFont(new Font("sansserif", Font.PLAIN, 14));
        FERUE.setName("FERUE");
        pnlCoordonneesFournisseur.add(FERUE, new GridBagConstraints(1, 2, 6, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbRue ----
        lbRue.setText("Adresse 2");
        lbRue.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbRue.setHorizontalAlignment(SwingConstants.RIGHT);
        lbRue.setName("lbRue");
        pnlCoordonneesFournisseur.add(lbRue, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- FELOC ----
        FELOC.setComponentPopupMenu(null);
        FELOC.setFont(new Font("sansserif", Font.PLAIN, 14));
        FELOC.setName("FELOC");
        pnlCoordonneesFournisseur.add(FELOC, new GridBagConstraints(1, 3, 6, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbVille ----
        lbVille.setText("Ville");
        lbVille.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbVille.setHorizontalAlignment(SwingConstants.RIGHT);
        lbVille.setName("lbVille");
        pnlCoordonneesFournisseur.add(lbVille, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbCodePostal ----
        lbCodePostal.setText("Code postal");
        lbCodePostal.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCodePostal.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCodePostal.setName("lbCodePostal");
        pnlCoordonneesFournisseur.add(lbCodePostal, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- FECDP ----
        FECDP.setComponentPopupMenu(null);
        FECDP.setToolTipText("Code postal");
        FECDP.setMinimumSize(new Dimension(55, 30));
        FECDP.setPreferredSize(new Dimension(55, 30));
        FECDP.setFont(new Font("sansserif", Font.PLAIN, 14));
        FECDP.setName("FECDP");
        pnlCoordonneesFournisseur.add(FECDP, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- FEVIL ----
        FEVIL.setComponentPopupMenu(null);
        FEVIL.setFont(new Font("sansserif", Font.PLAIN, 14));
        FEVIL.setName("FEVIL");
        pnlCoordonneesFournisseur.add(FEVIL, new GridBagConstraints(3, 4, 4, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbCodePostal2 ----
        lbCodePostal2.setText("Pays");
        lbCodePostal2.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCodePostal2.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCodePostal2.setName("lbCodePostal2");
        pnlCoordonneesFournisseur.add(lbCodePostal2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- FEPAYR ----
        FEPAYR.setComponentPopupMenu(null);
        FEPAYR.setFont(new Font("sansserif", Font.PLAIN, 14));
        FEPAYR.setName("FEPAYR");
        pnlCoordonneesFournisseur.add(FEPAYR, new GridBagConstraints(1, 5, 5, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- FECOP ----
        FECOP.setFont(new Font("sansserif", Font.PLAIN, 14));
        FECOP.setName("FECOP");
        pnlCoordonneesFournisseur.add(FECOP, new GridBagConstraints(6, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbEmail ----
        lbEmail.setText("Observations");
        lbEmail.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbEmail.setHorizontalAlignment(SwingConstants.RIGHT);
        lbEmail.setName("lbEmail");
        pnlCoordonneesFournisseur.add(lbEmail, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- FEOBSR ----
        FEOBSR.setComponentPopupMenu(null);
        FEOBSR.setFont(new Font("sansserif", Font.PLAIN, 14));
        FEOBSR.setName("FEOBSR");
        pnlCoordonneesFournisseur.add(FEOBSR, new GridBagConstraints(1, 6, 3, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbTelephone ----
        lbTelephone.setText("T\u00e9l\u00e9phone");
        lbTelephone.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbTelephone.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTelephone.setName("lbTelephone");
        pnlCoordonneesFournisseur.add(lbTelephone, new GridBagConstraints(5, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- FETEL ----
        FETEL.setComponentPopupMenu(null);
        FETEL.setToolTipText("T\u00e9l\u00e9phone");
        FETEL.setFont(new Font("sansserif", Font.PLAIN, 14));
        FETEL.setName("FETEL");
        pnlCoordonneesFournisseur.add(FETEL, new GridBagConstraints(6, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbContact ----
        lbContact.setText("Contact");
        lbContact.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbContact.setHorizontalAlignment(SwingConstants.RIGHT);
        lbContact.setName("lbContact");
        pnlCoordonneesFournisseur.add(lbContact, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- FEPAC ----
        FEPAC.setComponentPopupMenu(null);
        FEPAC.setMinimumSize(new Dimension(12, 30));
        FEPAC.setFont(new Font("sansserif", Font.PLAIN, 14));
        FEPAC.setName("FEPAC");
        pnlCoordonneesFournisseur.add(FEPAC, new GridBagConstraints(1, 7, 3, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- lbFax ----
        lbFax.setText("Fax");
        lbFax.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbFax.setHorizontalAlignment(SwingConstants.RIGHT);
        lbFax.setName("lbFax");
        pnlCoordonneesFournisseur.add(lbFax, new GridBagConstraints(5, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- FEFAX ----
        FEFAX.setComponentPopupMenu(null);
        FEFAX.setToolTipText("Fax");
        FEFAX.setFont(new Font("sansserif", Font.PLAIN, 14));
        FEFAX.setName("FEFAX");
        pnlCoordonneesFournisseur.add(FEFAX, new GridBagConstraints(6, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCoordonneesFournisseur, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- xriBarreBouton ----
    xriBarreBouton.setName("xriBarreBouton");
    add(xriBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanel pnlType;
  private SNLabelChamp lbNumero;
  private XRiTextField INDEXT;
  private SNLabelChamp lbType;
  private XRiCheckBox FECMD;
  private XRiCheckBox FEFAC;
  private XRiCheckBox FEEXP;
  private SNPanelTitre pnlCoordonneesFournisseur;
  private JLabel lbNom;
  private XRiTextField FENOM;
  private JLabel lbComplementNom;
  private XRiTextField FECPL;
  private JLabel lbLocalisation;
  private XRiTextField FERUE;
  private JLabel lbRue;
  private XRiTextField FELOC;
  private JLabel lbVille;
  private JLabel lbCodePostal;
  private XRiTextField FECDP;
  private XRiTextField FEVIL;
  private JLabel lbCodePostal2;
  private XRiTextField FEPAYR;
  private XRiTextField FECOP;
  private JLabel lbEmail;
  private XRiTextField FEOBSR;
  private JLabel lbTelephone;
  private XRiTextField FETEL;
  private JLabel lbContact;
  private XRiTextField FEPAC;
  private JLabel lbFax;
  private XRiTextField FEFAX;
  private XRiBarreBouton xriBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
