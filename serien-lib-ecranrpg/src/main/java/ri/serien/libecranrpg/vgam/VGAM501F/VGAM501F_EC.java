/*
 * Created by JFormDesigner on Wed Dec 19 10:06:19 CET 2018
 */

package ri.serien.libecranrpg.vgam.VGAM501F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGAM501F_EC extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGAM501F_EC(ArrayList param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO Icones
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    try {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F3");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel1 = new SNPanelTitre();
    ETB01 = new XRiTextField();
    LIB01 = new XRiTextField();
    ETB02 = new XRiTextField();
    LIB02 = new XRiTextField();
    ETB03 = new XRiTextField();
    LIB03 = new XRiTextField();
    ETB04 = new XRiTextField();
    LIB04 = new XRiTextField();
    ETB05 = new XRiTextField();
    LIB05 = new XRiTextField();
    ETB06 = new XRiTextField();
    LIB06 = new XRiTextField();
    ETB07 = new XRiTextField();
    LIB07 = new XRiTextField();
    ETB08 = new XRiTextField();
    LIB08 = new XRiTextField();
    ETB09 = new XRiTextField();
    LIB09 = new XRiTextField();
    ETB10 = new XRiTextField();
    LIB10 = new XRiTextField();
    ETB11 = new XRiTextField();
    LIB11 = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(585, 435));
    setPreferredSize(new Dimension(585, 435));
    setMaximumSize(new Dimension(585, 435));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 260));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(new GridBagLayout());
        ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {1.0, 1.0E-4};

        //======== panel1 ========
        {
          panel1.setTitre("Compte rendu de duplication");
          panel1.setName("panel1");
          panel1.setLayout(new GridBagLayout());
          ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- ETB01 ----
          ETB01.setPreferredSize(new Dimension(40, 28));
          ETB01.setMinimumSize(new Dimension(40, 28));
          ETB01.setName("ETB01");
          panel1.add(ETB01, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB01 ----
          LIB01.setMinimumSize(new Dimension(310, 28));
          LIB01.setPreferredSize(new Dimension(310, 28));
          LIB01.setName("LIB01");
          panel1.add(LIB01, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ETB02 ----
          ETB02.setPreferredSize(new Dimension(40, 28));
          ETB02.setMinimumSize(new Dimension(40, 28));
          ETB02.setName("ETB02");
          panel1.add(ETB02, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB02 ----
          LIB02.setMinimumSize(new Dimension(310, 28));
          LIB02.setPreferredSize(new Dimension(310, 28));
          LIB02.setName("LIB02");
          panel1.add(LIB02, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ETB03 ----
          ETB03.setPreferredSize(new Dimension(40, 28));
          ETB03.setMinimumSize(new Dimension(40, 28));
          ETB03.setName("ETB03");
          panel1.add(ETB03, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB03 ----
          LIB03.setMinimumSize(new Dimension(310, 28));
          LIB03.setPreferredSize(new Dimension(310, 28));
          LIB03.setName("LIB03");
          panel1.add(LIB03, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ETB04 ----
          ETB04.setPreferredSize(new Dimension(40, 28));
          ETB04.setMinimumSize(new Dimension(40, 28));
          ETB04.setName("ETB04");
          panel1.add(ETB04, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB04 ----
          LIB04.setMinimumSize(new Dimension(310, 28));
          LIB04.setPreferredSize(new Dimension(310, 28));
          LIB04.setName("LIB04");
          panel1.add(LIB04, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ETB05 ----
          ETB05.setPreferredSize(new Dimension(40, 28));
          ETB05.setMinimumSize(new Dimension(40, 28));
          ETB05.setName("ETB05");
          panel1.add(ETB05, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB05 ----
          LIB05.setMinimumSize(new Dimension(310, 28));
          LIB05.setPreferredSize(new Dimension(310, 28));
          LIB05.setName("LIB05");
          panel1.add(LIB05, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ETB06 ----
          ETB06.setPreferredSize(new Dimension(40, 28));
          ETB06.setMinimumSize(new Dimension(40, 28));
          ETB06.setName("ETB06");
          panel1.add(ETB06, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB06 ----
          LIB06.setMinimumSize(new Dimension(310, 28));
          LIB06.setPreferredSize(new Dimension(310, 28));
          LIB06.setName("LIB06");
          panel1.add(LIB06, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ETB07 ----
          ETB07.setPreferredSize(new Dimension(40, 28));
          ETB07.setMinimumSize(new Dimension(40, 28));
          ETB07.setName("ETB07");
          panel1.add(ETB07, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB07 ----
          LIB07.setMinimumSize(new Dimension(310, 28));
          LIB07.setPreferredSize(new Dimension(310, 28));
          LIB07.setName("LIB07");
          panel1.add(LIB07, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ETB08 ----
          ETB08.setPreferredSize(new Dimension(40, 28));
          ETB08.setMinimumSize(new Dimension(40, 28));
          ETB08.setName("ETB08");
          panel1.add(ETB08, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB08 ----
          LIB08.setMinimumSize(new Dimension(310, 28));
          LIB08.setPreferredSize(new Dimension(310, 28));
          LIB08.setName("LIB08");
          panel1.add(LIB08, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ETB09 ----
          ETB09.setPreferredSize(new Dimension(40, 28));
          ETB09.setMinimumSize(new Dimension(40, 28));
          ETB09.setName("ETB09");
          panel1.add(ETB09, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB09 ----
          LIB09.setMinimumSize(new Dimension(310, 28));
          LIB09.setPreferredSize(new Dimension(310, 28));
          LIB09.setName("LIB09");
          panel1.add(LIB09, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ETB10 ----
          ETB10.setPreferredSize(new Dimension(40, 28));
          ETB10.setMinimumSize(new Dimension(40, 28));
          ETB10.setName("ETB10");
          panel1.add(ETB10, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB10 ----
          LIB10.setMinimumSize(new Dimension(310, 28));
          LIB10.setPreferredSize(new Dimension(310, 28));
          LIB10.setName("LIB10");
          panel1.add(LIB10, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- ETB11 ----
          ETB11.setPreferredSize(new Dimension(40, 28));
          ETB11.setMinimumSize(new Dimension(40, 28));
          ETB11.setName("ETB11");
          panel1.add(ETB11, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- LIB11 ----
          LIB11.setMinimumSize(new Dimension(310, 28));
          LIB11.setPreferredSize(new Dimension(310, 28));
          LIB11.setName("LIB11");
          panel1.add(LIB11, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(panel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private SNPanelTitre panel1;
  private XRiTextField ETB01;
  private XRiTextField LIB01;
  private XRiTextField ETB02;
  private XRiTextField LIB02;
  private XRiTextField ETB03;
  private XRiTextField LIB03;
  private XRiTextField ETB04;
  private XRiTextField LIB04;
  private XRiTextField ETB05;
  private XRiTextField LIB05;
  private XRiTextField ETB06;
  private XRiTextField LIB06;
  private XRiTextField ETB07;
  private XRiTextField LIB07;
  private XRiTextField ETB08;
  private XRiTextField LIB08;
  private XRiTextField ETB09;
  private XRiTextField LIB09;
  private XRiTextField ETB10;
  private XRiTextField LIB10;
  private XRiTextField ETB11;
  private XRiTextField LIB11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
