
package ri.serien.libecranrpg.vgam.VGAM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGAM15FM_DD extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] _LD01_Title = { "HLD01", };
  private String[][] _LD01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _LD01_Width = { 769, };
  private String libelleDirectOuInterne = "";
  
  /**
   * Constructeur.
   */
  public VGAM15FM_DD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    LD01.setAspectTable(null, _LD01_Title, _LD01_Data, _LD01_Width, false, null, null, null, null);
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP15@")).trim());
    OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E2NOM@")).trim());
    OBJ_76.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EASAN@")).trim());
    OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSALBR@")).trim());
    WDATEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDATEX@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@EAMAG@  @WMALB@")).trim()));
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EASAN@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSALBR@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMFR@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMCD@")).trim());
    OBJ_79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TVA1@")).trim());
    OBJ_80.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TVA2@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TVA3@")).trim());
    OBJ_83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BAS@")).trim());
    OBJ_84.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EADEV@")).trim());
    lbWTTCX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTTCX@")).trim());
    OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMFR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_80.setVisible(lexique.isPresent("TVA3"));
    OBJ_79.setVisible(lexique.isPresent("TVA2"));
    OBJ_78.setVisible(lexique.isPresent("TVA1"));
    OBJ_84.setVisible(lexique.isPresent("EADEV"));
    OBJ_52.setVisible(lexique.isPresent("EASAN"));
    OBJ_83.setVisible(lexique.isPresent("BAS"));
    OBJ_82.setVisible(OBJ_83.isVisible());
    OBJ_86.setVisible(!lexique.HostFieldGetData("TEST").trim().equalsIgnoreCase(""));
    OBJ_87.setVisible(lexique.isPresent("LIBMCD"));
    OBJ_89.setVisible(lexique.isPresent("LIBMFR"));
    OBJ_53.setVisible(lexique.isPresent("WSALBR"));
    
    if (lexique.HostFieldGetData("EAIN9").trim().equals("D")) {
      libelleDirectOuInterne = " (Direct usine)";
    }
    else if (lexique.HostFieldGetData("EAIN9").trim().equals("I")) {
      libelleDirectOuInterne = " (Commande interne)";
    }
    else {
      libelleDirectOuInterne = "";
    }
    p_bpresentation.setText(p_bpresentation.getText() + libelleDirectOuInterne);
    
    boolean is27 = lexique.isTrue("27");
    lbDiffTTC.setVisible(!is27);
    lbWTTCX.setVisible(!is27);
    
    boolean is28 = lexique.isTrue("28");
    lbEscompte.setVisible(!is28);
    ZMESX.setVisible(!is28);
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_88ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("V06F");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    EAETB = new XRiTextField();
    WNUM = new XRiTextField();
    WSUF = new XRiTextField();
    OBJ_73 = new RiZoneSortie();
    OBJ_76 = new RiZoneSortie();
    OBJ_78 = new RiZoneSortie();
    WDATEX = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_53 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST4 = new JScrollPane();
    LD01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_54 = new JLabel();
    OBJ_55 = new JLabel();
    panel2 = new JPanel();
    OBJ_89 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_72 = new JLabel();
    WSL1X = new XRiTextField();
    WSL2X = new XRiTextField();
    WSL3X = new XRiTextField();
    ZTL1X = new XRiTextField();
    ZTL2X = new XRiTextField();
    ZTL3X = new XRiTextField();
    OBJ_86 = new JLabel();
    OBJ_88 = new SNBoutonLeger();
    WLT1 = new XRiTextField();
    WLT2 = new XRiTextField();
    WLT3 = new XRiTextField();
    WREP = new XRiTextField();
    OBJ_79 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_84 = new JLabel();
    ZMESX = new XRiTextField();
    lbDiffTTC = new JLabel();
    lbWTTCX = new JLabel();
    lbEscompte = new JLabel();
    OBJ_90 = new JLabel();
    WTHTX = new XRiTextField();
    WTVAX = new XRiTextField();
    ZTTCX = new XRiTextField();
    OBJ_77 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_64 = new JLabel();
    WPOR1X = new XRiTextField();
    OBJ_65 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TYP15@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- EAETB ----
          EAETB.setComponentPopupMenu(BTD);
          EAETB.setName("EAETB");

          //---- WNUM ----
          WNUM.setComponentPopupMenu(BTD);
          WNUM.setName("WNUM");

          //---- WSUF ----
          WSUF.setComponentPopupMenu(BTD);
          WSUF.setName("WSUF");

          //---- OBJ_73 ----
          OBJ_73.setText("@E2NOM@");
          OBJ_73.setOpaque(false);
          OBJ_73.setName("OBJ_73");

          //---- OBJ_76 ----
          OBJ_76.setText("@EASAN@");
          OBJ_76.setOpaque(false);
          OBJ_76.setName("OBJ_76");

          //---- OBJ_78 ----
          OBJ_78.setText("@WSALBR@");
          OBJ_78.setOpaque(false);
          OBJ_78.setName("OBJ_78");

          //---- WDATEX ----
          WDATEX.setComponentPopupMenu(BTD);
          WDATEX.setText("@WDATEX@");
          WDATEX.setBorder(new BevelBorder(BevelBorder.LOWERED));
          WDATEX.setHorizontalAlignment(SwingConstants.CENTER);
          WDATEX.setFont(WDATEX.getFont().deriveFont(WDATEX.getFont().getStyle() | Font.BOLD));
          WDATEX.setName("WDATEX");

          //---- OBJ_51 ----
          OBJ_51.setText("Etablissement");
          OBJ_51.setName("OBJ_51");

          //---- OBJ_52 ----
          OBJ_52.setText("Num\u00e9ro");
          OBJ_52.setName("OBJ_52");

          //---- OBJ_53 ----
          OBJ_53.setText("Fournisseur");
          OBJ_53.setName("OBJ_53");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(EAETB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WDATEX, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_51))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(EAETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_52))
              .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_53))
              .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WDATEX, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Saisie code barre");
              riSousMenu_bt6.setToolTipText("Mise en/hors fonction de la saisie code barre");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 570));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1000, 571));
          p_contenu.setMaximumSize(new Dimension(1050, 571));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("@EAMAG@  @WMALB@"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST4 ========
            {
              SCROLLPANE_LIST4.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST4.setName("SCROLLPANE_LIST4");

              //---- LD01 ----
              LD01.setName("LD01");
              SCROLLPANE_LIST4.setViewportView(LD01);
            }
            panel1.add(SCROLLPANE_LIST4);
            SCROLLPANE_LIST4.setBounds(15, 40, 915, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(935, 40, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(935, 185, 25, 125);

            //---- OBJ_54 ----
            OBJ_54.setText("@EASAN@");
            OBJ_54.setFont(OBJ_54.getFont().deriveFont(OBJ_54.getFont().getSize() - 1f));
            OBJ_54.setName("OBJ_54");
            panel1.add(OBJ_54);
            OBJ_54.setBounds(325, 0, 40, 18);

            //---- OBJ_55 ----
            OBJ_55.setText("@WSALBR@");
            OBJ_55.setFont(OBJ_55.getFont().deriveFont(OBJ_55.getFont().getSize() - 1f));
            OBJ_55.setName("OBJ_55");
            panel1.add(OBJ_55);
            OBJ_55.setBounds(380, 0, 333, 18);
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_89 ----
            OBJ_89.setText("@LIBMFR@");
            OBJ_89.setName("OBJ_89");
            panel2.add(OBJ_89);
            OBJ_89.setBounds(65, 140, 342, 20);

            //---- OBJ_87 ----
            OBJ_87.setText("@LIBMCD@");
            OBJ_87.setName("OBJ_87");
            panel2.add(OBJ_87);
            OBJ_87.setBounds(65, 110, 342, 20);

            //---- OBJ_60 ----
            OBJ_60.setText("Bases TVA");
            OBJ_60.setName("OBJ_60");
            panel2.add(OBJ_60);
            OBJ_60.setBounds(20, 19, 90, 20);

            //---- OBJ_66 ----
            OBJ_66.setText("Montants TVA");
            OBJ_66.setName("OBJ_66");
            panel2.add(OBJ_66);
            OBJ_66.setBounds(20, 49, 90, 20);

            //---- OBJ_72 ----
            OBJ_72.setText("Codes TVA");
            OBJ_72.setName("OBJ_72");
            panel2.add(OBJ_72);
            OBJ_72.setBounds(20, 79, 90, 20);

            //---- WSL1X ----
            WSL1X.setComponentPopupMenu(BTD);
            WSL1X.setHorizontalAlignment(SwingConstants.RIGHT);
            WSL1X.setName("WSL1X");
            panel2.add(WSL1X);
            WSL1X.setBounds(115, 15, 110, WSL1X.getPreferredSize().height);

            //---- WSL2X ----
            WSL2X.setComponentPopupMenu(BTD);
            WSL2X.setHorizontalAlignment(SwingConstants.RIGHT);
            WSL2X.setName("WSL2X");
            panel2.add(WSL2X);
            WSL2X.setBounds(250, 15, 110, WSL2X.getPreferredSize().height);

            //---- WSL3X ----
            WSL3X.setComponentPopupMenu(BTD);
            WSL3X.setHorizontalAlignment(SwingConstants.RIGHT);
            WSL3X.setName("WSL3X");
            panel2.add(WSL3X);
            WSL3X.setBounds(385, 15, 110, WSL3X.getPreferredSize().height);

            //---- ZTL1X ----
            ZTL1X.setComponentPopupMenu(BTD);
            ZTL1X.setHorizontalAlignment(SwingConstants.RIGHT);
            ZTL1X.setName("ZTL1X");
            panel2.add(ZTL1X);
            ZTL1X.setBounds(115, 45, 110, ZTL1X.getPreferredSize().height);

            //---- ZTL2X ----
            ZTL2X.setComponentPopupMenu(BTD);
            ZTL2X.setHorizontalAlignment(SwingConstants.RIGHT);
            ZTL2X.setName("ZTL2X");
            panel2.add(ZTL2X);
            ZTL2X.setBounds(250, 45, 110, ZTL2X.getPreferredSize().height);

            //---- ZTL3X ----
            ZTL3X.setComponentPopupMenu(BTD);
            ZTL3X.setHorizontalAlignment(SwingConstants.RIGHT);
            ZTL3X.setName("ZTL3X");
            panel2.add(ZTL3X);
            ZTL3X.setBounds(385, 45, 110, ZTL3X.getPreferredSize().height);

            //---- OBJ_86 ----
            OBJ_86.setIcon(new ImageIcon("images/avert.gif"));
            OBJ_86.setName("OBJ_86");
            panel2.add(OBJ_86);
            OBJ_86.setBounds(20, 120, 42, 36);

            //---- OBJ_88 ----
            OBJ_88.setText("Options");
            OBJ_88.setToolTipText("Choix des options de fin de bon");
            OBJ_88.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_88.setName("OBJ_88");
            OBJ_88.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_88ActionPerformed(e);
              }
            });
            panel2.add(OBJ_88);
            OBJ_88.setBounds(new Rectangle(new Point(730, 135), OBJ_88.getPreferredSize()));

            //---- WLT1 ----
            WLT1.setComponentPopupMenu(BTD);
            WLT1.setHorizontalAlignment(SwingConstants.RIGHT);
            WLT1.setName("WLT1");
            panel2.add(WLT1);
            WLT1.setBounds(115, 75, 58, WLT1.getPreferredSize().height);

            //---- WLT2 ----
            WLT2.setComponentPopupMenu(BTD);
            WLT2.setHorizontalAlignment(SwingConstants.RIGHT);
            WLT2.setName("WLT2");
            panel2.add(WLT2);
            WLT2.setBounds(250, 75, 58, WLT2.getPreferredSize().height);

            //---- WLT3 ----
            WLT3.setComponentPopupMenu(BTD);
            WLT3.setHorizontalAlignment(SwingConstants.RIGHT);
            WLT3.setName("WLT3");
            panel2.add(WLT3);
            WLT3.setBounds(385, 75, 66, WLT3.getPreferredSize().height);

            //---- WREP ----
            WREP.setComponentPopupMenu(BTD);
            WREP.setName("WREP");
            panel2.add(WREP);
            WREP.setBounds(875, 135, 50, WREP.getPreferredSize().height);

            //---- OBJ_79 ----
            OBJ_79.setText("@TVA1@");
            OBJ_79.setName("OBJ_79");
            panel2.add(OBJ_79);
            OBJ_79.setBounds(175, 79, 18, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("@TVA2@");
            OBJ_80.setName("OBJ_80");
            panel2.add(OBJ_80);
            OBJ_80.setBounds(310, 79, 18, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("@TVA3@");
            OBJ_81.setName("OBJ_81");
            panel2.add(OBJ_81);
            OBJ_81.setBounds(450, 79, 18, 20);

            //---- OBJ_83 ----
            OBJ_83.setText("@BAS@");
            OBJ_83.setName("OBJ_83");
            panel2.add(OBJ_83);
            OBJ_83.setBounds(820, 110, 58, 28);

            //---- OBJ_82 ----
            OBJ_82.setText("en");
            OBJ_82.setName("OBJ_82");
            panel2.add(OBJ_82);
            OBJ_82.setBounds(790, 109, 18, 28);

            //---- OBJ_84 ----
            OBJ_84.setText("@EADEV@");
            OBJ_84.setName("OBJ_84");
            panel2.add(OBJ_84);
            OBJ_84.setBounds(889, 110, 36, 28);

            //---- ZMESX ----
            ZMESX.setComponentPopupMenu(BTD);
            ZMESX.setHorizontalAlignment(SwingConstants.RIGHT);
            ZMESX.setName("ZMESX");
            panel2.add(ZMESX);
            ZMESX.setBounds(575, 15, 110, ZMESX.getPreferredSize().height);

            //---- lbDiffTTC ----
            lbDiffTTC.setText("Diff./TTC");
            lbDiffTTC.setForeground(Color.red);
            lbDiffTTC.setName("lbDiffTTC");
            panel2.add(lbDiffTTC);
            lbDiffTTC.setBounds(510, 85, 100, 28);

            //---- lbWTTCX ----
            lbWTTCX.setText("@WTTCX@");
            lbWTTCX.setMaximumSize(new Dimension(140, 16));
            lbWTTCX.setMinimumSize(new Dimension(140, 16));
            lbWTTCX.setPreferredSize(new Dimension(140, 16));
            lbWTTCX.setName("lbWTTCX");
            panel2.add(lbWTTCX);
            lbWTTCX.setBounds(575, 85, 130, 28);

            //---- lbEscompte ----
            lbEscompte.setText("Escompte");
            lbEscompte.setName("lbEscompte");
            panel2.add(lbEscompte);
            lbEscompte.setBounds(510, 15, 65, 28);

            //---- OBJ_90 ----
            OBJ_90.setText("@LIBMFR@");
            OBJ_90.setName("OBJ_90");
            panel2.add(OBJ_90);
            OBJ_90.setBounds(435, 140, 342, 20);

            //---- WTHTX ----
            WTHTX.setComponentPopupMenu(BTD);
            WTHTX.setHorizontalAlignment(SwingConstants.RIGHT);
            WTHTX.setName("WTHTX");
            panel2.add(WTHTX);
            WTHTX.setBounds(810, 10, 140, WTHTX.getPreferredSize().height);

            //---- WTVAX ----
            WTVAX.setComponentPopupMenu(null);
            WTVAX.setHorizontalAlignment(SwingConstants.RIGHT);
            WTVAX.setName("WTVAX");
            panel2.add(WTVAX);
            WTVAX.setBounds(810, 60, 140, WTVAX.getPreferredSize().height);

            //---- ZTTCX ----
            ZTTCX.setComponentPopupMenu(BTD);
            ZTTCX.setHorizontalAlignment(SwingConstants.RIGHT);
            ZTTCX.setName("ZTTCX");
            panel2.add(ZTTCX);
            ZTTCX.setBounds(810, 85, 140, ZTTCX.getPreferredSize().height);

            //---- OBJ_77 ----
            OBJ_77.setText("Total TTC");
            OBJ_77.setName("OBJ_77");
            panel2.add(OBJ_77);
            OBJ_77.setBounds(710, 85, 100, 28);

            //---- OBJ_70 ----
            OBJ_70.setText("Total TVA");
            OBJ_70.setName("OBJ_70");
            panel2.add(OBJ_70);
            OBJ_70.setBounds(710, 60, 100, 28);

            //---- OBJ_64 ----
            OBJ_64.setText("Total H.T");
            OBJ_64.setName("OBJ_64");
            panel2.add(OBJ_64);
            OBJ_64.setBounds(710, 10, 100, 28);

            //---- WPOR1X ----
            WPOR1X.setComponentPopupMenu(BTD);
            WPOR1X.setHorizontalAlignment(SwingConstants.RIGHT);
            WPOR1X.setName("WPOR1X");
            panel2.add(WPOR1X);
            WPOR1X.setBounds(810, 35, 140, WPOR1X.getPreferredSize().height);

            //---- OBJ_65 ----
            OBJ_65.setText("Dont port H.T");
            OBJ_65.setName("OBJ_65");
            panel2.add(OBJ_65);
            OBJ_65.setBounds(710, 35, 100, 28);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 974, Short.MAX_VALUE)
                  .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 974, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Choix possibles");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_8 ----
      OBJ_8.setText("Convertisseur mon\u00e9taire");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      BTD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField EAETB;
  private XRiTextField WNUM;
  private XRiTextField WSUF;
  private RiZoneSortie OBJ_73;
  private RiZoneSortie OBJ_76;
  private RiZoneSortie OBJ_78;
  private JLabel WDATEX;
  private JLabel OBJ_51;
  private JLabel OBJ_52;
  private JLabel OBJ_53;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST4;
  private XRiTable LD01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_54;
  private JLabel OBJ_55;
  private JPanel panel2;
  private JLabel OBJ_89;
  private JLabel OBJ_87;
  private JLabel OBJ_60;
  private JLabel OBJ_66;
  private JLabel OBJ_72;
  private XRiTextField WSL1X;
  private XRiTextField WSL2X;
  private XRiTextField WSL3X;
  private XRiTextField ZTL1X;
  private XRiTextField ZTL2X;
  private XRiTextField ZTL3X;
  private JLabel OBJ_86;
  private SNBoutonLeger OBJ_88;
  private XRiTextField WLT1;
  private XRiTextField WLT2;
  private XRiTextField WLT3;
  private XRiTextField WREP;
  private JLabel OBJ_79;
  private JLabel OBJ_80;
  private JLabel OBJ_81;
  private JLabel OBJ_83;
  private JLabel OBJ_82;
  private JLabel OBJ_84;
  private XRiTextField ZMESX;
  private JLabel lbDiffTTC;
  private JLabel lbWTTCX;
  private JLabel lbEscompte;
  private JLabel OBJ_90;
  private XRiTextField WTHTX;
  private XRiTextField WTVAX;
  private XRiTextField ZTTCX;
  private JLabel OBJ_77;
  private JLabel OBJ_70;
  private JLabel OBJ_64;
  private XRiTextField WPOR1X;
  private JLabel OBJ_65;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
