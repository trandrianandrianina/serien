
package ri.serien.libecranrpg.recs.RECSTNFM;
// Nom Fichier: pop_VCGM05FM_FMTERR_367.java

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class RECSTNFM_T8 extends SNPanelEcranRPG implements ioFrame {
  
  public RECSTNFM_T8(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }
  
  @Override
  public void setData() {
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    sNLabelTitre1 = new SNLabelTitre();
    
    // ======== this ========
    setDialog(true);
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- sNLabelTitre1 ----
    sNLabelTitre1.setText("Traitement en cours...");
    sNLabelTitre1.setHorizontalAlignment(SwingConstants.CENTER);
    sNLabelTitre1.setVerticalAlignment(SwingConstants.CENTER);
    sNLabelTitre1.setName("sNLabelTitre1");
    add(sNLabelTitre1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelTitre sNLabelTitre1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
