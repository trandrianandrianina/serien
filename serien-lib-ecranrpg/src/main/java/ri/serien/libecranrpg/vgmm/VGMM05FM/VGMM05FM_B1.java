
package ri.serien.libecranrpg.vgmm.VGMM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGMM05FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] MCIN1_Value = { "", "1", "2", };
  
  public VGMM05FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    MCTFC.setValeursSelection("1", "");
    MCFIN.setValeursSelection("1", "");
    MCIN2.setValeursSelection("1", "");
    MCIN1.setValeurs(MCIN1_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    SULIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SULIBR@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    INLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INLIBR@")).trim());
    TCLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TCLIBR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_66 = new JLabel();
    OBJ_68 = new JLabel();
    INDNCE = new XRiTextField();
    INDETB = new XRiTextField();
    OBJ_86 = new JLabel();
    MCCSC = new XRiTextField();
    SULIBR = new RiZoneSortie();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    CLNOML = new XRiTextField();
    CLCPLL = new XRiTextField();
    CLRUEL = new XRiTextField();
    CLLOCL = new XRiTextField();
    MCCLI1 = new XRiTextField();
    CLVILL = new XRiTextField();
    MCLIV = new XRiTextField();
    OBJ_69 = new JLabel();
    p_TCI1 = new JXTitledPanel();
    INLIBR = new RiZoneSortie();
    MCDES = new XRiTextField();
    OBJ_71 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_76 = new JLabel();
    panel5 = new JPanel();
    MCNBV = new XRiTextField();
    MCNBV2 = new XRiTextField();
    DPVX = new XRiCalendrier();
    DPV2X = new XRiCalendrier();
    MCTPV = new XRiTextField();
    MCTPV2 = new XRiTextField();
    OBJ_78 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    MCTYC = new XRiTextField();
    TCLIBR = new RiZoneSortie();
    MCINT = new XRiTextField();
    OBJ_72 = new JLabel();
    MCDUR = new XRiTextField();
    OBJ_73 = new JLabel();
    DDCX = new XRiCalendrier();
    OBJ_74 = new JLabel();
    MCDTVX = new XRiCalendrier();
    p_TCI2 = new JXTitledPanel();
    OBJ_83 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_84 = new JLabel();
    MCMTT = new XRiTextField();
    MCFIN = new XRiCheckBox();
    panel6 = new JPanel();
    MCDAFX = new XRiCalendrier();
    MCNFA = new XRiTextField();
    OBJ_91 = new JLabel();
    OBJ_93 = new JLabel();
    DPRX = new XRiCalendrier();
    MCIN2 = new XRiCheckBox();
    MCPEM = new XRiTextField();
    MCTFC = new XRiCheckBox();
    MCIN1 = new XRiComboBox();
    panel4 = new JPanel();
    CLNOMF = new XRiTextField();
    CLCPLF = new XRiTextField();
    CLRUEF = new XRiTextField();
    CLLOCF = new XRiTextField();
    OBJ_70 = new JLabel();
    MCCLF1 = new XRiTextField();
    MCLIVF = new XRiTextField();
    CLVILF = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Fiche contrat");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_66 ----
          OBJ_66.setText("Etablissement");
          OBJ_66.setName("OBJ_66");
          p_tete_gauche.add(OBJ_66);
          OBJ_66.setBounds(5, 5, 95, 20);

          //---- OBJ_68 ----
          OBJ_68.setText("Num\u00e9ro");
          OBJ_68.setName("OBJ_68");
          p_tete_gauche.add(OBJ_68);
          OBJ_68.setBounds(200, 5, 68, 20);

          //---- INDNCE ----
          INDNCE.setComponentPopupMenu(BTD);
          INDNCE.setName("INDNCE");
          p_tete_gauche.add(INDNCE);
          INDNCE.setBounds(265, 1, 60, INDNCE.getPreferredSize().height);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(115, 1, 40, INDETB.getPreferredSize().height);

          //---- OBJ_86 ----
          OBJ_86.setText("Code suspension");
          OBJ_86.setName("OBJ_86");
          p_tete_gauche.add(OBJ_86);
          OBJ_86.setBounds(375, 5, 120, 20);

          //---- MCCSC ----
          MCCSC.setName("MCCSC");
          p_tete_gauche.add(MCCSC);
          MCCSC.setBounds(500, 1, 40, MCCSC.getPreferredSize().height);

          //---- SULIBR ----
          SULIBR.setText("@SULIBR@");
          SULIBR.setOpaque(false);
          SULIBR.setName("SULIBR");
          p_tete_gauche.add(SULIBR);
          SULIBR.setBounds(545, 3, 220, SULIBR.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Observations");
            riSousMenu_bt7.setToolTipText("Fiche observations");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Facturation");
            riSousMenu_bt8.setToolTipText("Facturer ce contrat");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);

          //======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");

            //---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Edition du contrat");
            riSousMenu_bt9.setToolTipText("Editer ce contrat");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);

          //======== riSousMenu10 ========
          {
            riSousMenu10.setName("riSousMenu10");

            //---- riSousMenu_bt10 ----
            riSousMenu_bt10.setText("Documents li\u00e9s");
            riSousMenu_bt10.setToolTipText("Documents li\u00e9s");
            riSousMenu_bt10.setName("riSousMenu_bt10");
            riSousMenu_bt10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt10ActionPerformed(e);
              }
            });
            riSousMenu10.add(riSousMenu_bt10);
          }
          menus_haut.add(riSousMenu10);

          //======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");

            //---- riMenu_bt3 ----
            riMenu_bt3.setText("Compl\u00e9ment");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);

          //======== riSousMenu14 ========
          {
            riSousMenu14.setName("riSousMenu14");

            //---- riSousMenu_bt14 ----
            riSousMenu_bt14.setText("Mat\u00e9riels li\u00e9s au contrat");
            riSousMenu_bt14.setToolTipText("Mat\u00e9riels li\u00e9s \u00e0 ce contrat");
            riSousMenu_bt14.setName("riSousMenu_bt14");
            riSousMenu_bt14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt14ActionPerformed(e);
              }
            });
            riSousMenu14.add(riSousMenu_bt14);
          }
          menus_haut.add(riSousMenu14);

          //======== riSousMenu15 ========
          {
            riSousMenu15.setName("riSousMenu15");

            //---- riSousMenu_bt15 ----
            riSousMenu_bt15.setText("Interventions li\u00e9es");
            riSousMenu_bt15.setToolTipText("Interventions li\u00e9es \u00e0 ce contrat");
            riSousMenu_bt15.setName("riSousMenu_bt15");
            riSousMenu_bt15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt15ActionPerformed(e);
              }
            });
            riSousMenu15.add(riSousMenu_bt15);
          }
          menus_haut.add(riSousMenu15);

          //======== riSousMenu16 ========
          {
            riSousMenu16.setName("riSousMenu16");

            //---- riSousMenu_bt16 ----
            riSousMenu_bt16.setText("Affectation mat\u00e9riel");
            riSousMenu_bt16.setToolTipText("Affectation de mat\u00e9riel sur ce contrat");
            riSousMenu_bt16.setName("riSousMenu_bt16");
            riSousMenu_bt16.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt16ActionPerformed(e);
              }
            });
            riSousMenu16.add(riSousMenu_bt16);
          }
          menus_haut.add(riSousMenu16);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        ((GridBagLayout)p_centrage.getLayout()).columnWidths = new int[] {600};
        ((GridBagLayout)p_centrage.getLayout()).rowHeights = new int[] {465, 0};

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(950, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(950, 600));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setBorder(new TitledBorder("Client"));
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- CLNOML ----
            CLNOML.setFont(CLNOML.getFont().deriveFont(CLNOML.getFont().getStyle() | Font.BOLD));
            CLNOML.setName("CLNOML");
            panel3.add(CLNOML);
            CLNOML.setBounds(15, 65, 310, CLNOML.getPreferredSize().height);

            //---- CLCPLL ----
            CLCPLL.setName("CLCPLL");
            panel3.add(CLCPLL);
            CLCPLL.setBounds(15, 95, 310, CLCPLL.getPreferredSize().height);

            //---- CLRUEL ----
            CLRUEL.setName("CLRUEL");
            panel3.add(CLRUEL);
            CLRUEL.setBounds(15, 125, 310, CLRUEL.getPreferredSize().height);

            //---- CLLOCL ----
            CLLOCL.setName("CLLOCL");
            panel3.add(CLLOCL);
            CLLOCL.setBounds(15, 155, 310, CLLOCL.getPreferredSize().height);

            //---- MCCLI1 ----
            MCCLI1.setName("MCCLI1");
            panel3.add(MCCLI1);
            MCCLI1.setBounds(225, 35, 60, MCCLI1.getPreferredSize().height);

            //---- CLVILL ----
            CLVILL.setName("CLVILL");
            panel3.add(CLVILL);
            CLVILL.setBounds(15, 185, 310, CLVILL.getPreferredSize().height);

            //---- MCLIV ----
            MCLIV.setName("MCLIV");
            panel3.add(MCLIV);
            MCLIV.setBounds(289, 35, 36, MCLIV.getPreferredSize().height);

            //---- OBJ_69 ----
            OBJ_69.setText("Num\u00e9ro");
            OBJ_69.setName("OBJ_69");
            panel3.add(OBJ_69);
            OBJ_69.setBounds(150, 39, 68, 20);
          }
          p_contenu.add(panel3);
          panel3.setBounds(15, 5, 345, 225);

          //======== p_TCI1 ========
          {
            p_TCI1.setTitle("El\u00e9ments du contact");
            p_TCI1.setBorder(new DropShadowBorder());
            p_TCI1.setTitleFont(p_TCI1.getTitleFont().deriveFont(p_TCI1.getTitleFont().getStyle() | Font.BOLD));
            p_TCI1.setName("p_TCI1");
            Container p_TCI1ContentContainer = p_TCI1.getContentContainer();
            p_TCI1ContentContainer.setLayout(null);

            //---- INLIBR ----
            INLIBR.setText("@INLIBR@");
            INLIBR.setName("INLIBR");
            p_TCI1ContentContainer.add(INLIBR);
            INLIBR.setBounds(635, 44, 260, INLIBR.getPreferredSize().height);

            //---- MCDES ----
            MCDES.setName("MCDES");
            p_TCI1ContentContainer.add(MCDES);
            MCDES.setBounds(115, 5, 620, MCDES.getPreferredSize().height);

            //---- OBJ_71 ----
            OBJ_71.setText("Descriptif");
            OBJ_71.setName("OBJ_71");
            p_TCI1ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(15, 9, 65, 20);

            //---- OBJ_75 ----
            OBJ_75.setText("Type de contrat");
            OBJ_75.setName("OBJ_75");
            p_TCI1ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(15, 46, 95, 20);

            //---- OBJ_76 ----
            OBJ_76.setText("Intervenant");
            OBJ_76.setName("OBJ_76");
            p_TCI1ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(505, 46, 75, 20);

            //======== panel5 ========
            {
              panel5.setBorder(new TitledBorder("Visites"));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- MCNBV ----
              MCNBV.setName("MCNBV");
              panel5.add(MCNBV);
              MCNBV.setBounds(70, 25, 28, MCNBV.getPreferredSize().height);

              //---- MCNBV2 ----
              MCNBV2.setName("MCNBV2");
              panel5.add(MCNBV2);
              MCNBV2.setBounds(70, 55, 28, MCNBV2.getPreferredSize().height);

              //---- DPVX ----
              DPVX.setName("DPVX");
              panel5.add(DPVX);
              DPVX.setBounds(190, 25, 105, DPVX.getPreferredSize().height);

              //---- DPV2X ----
              DPV2X.setName("DPV2X");
              panel5.add(DPV2X);
              DPV2X.setBounds(190, 55, 105, DPV2X.getPreferredSize().height);

              //---- MCTPV ----
              MCTPV.setComponentPopupMenu(BTD);
              MCTPV.setName("MCTPV");
              panel5.add(MCTPV);
              MCTPV.setBounds(370, 25, 60, MCTPV.getPreferredSize().height);

              //---- MCTPV2 ----
              MCTPV2.setComponentPopupMenu(BTD);
              MCTPV2.setName("MCTPV2");
              panel5.add(MCTPV2);
              MCTPV2.setBounds(370, 55, 60, MCTPV2.getPreferredSize().height);

              //---- OBJ_78 ----
              OBJ_78.setText("Nombre");
              OBJ_78.setName("OBJ_78");
              panel5.add(OBJ_78);
              OBJ_78.setBounds(15, 29, 50, 20);

              //---- OBJ_79 ----
              OBJ_79.setText("Nombre");
              OBJ_79.setName("OBJ_79");
              panel5.add(OBJ_79);
              OBJ_79.setBounds(15, 59, 50, 20);

              //---- OBJ_77 ----
              OBJ_77.setText("Prochaine");
              OBJ_77.setName("OBJ_77");
              panel5.add(OBJ_77);
              OBJ_77.setBounds(120, 29, 75, 20);

              //---- OBJ_80 ----
              OBJ_80.setText("Prochaine");
              OBJ_80.setName("OBJ_80");
              panel5.add(OBJ_80);
              OBJ_80.setBounds(120, 59, 75, 20);

              //---- OBJ_81 ----
              OBJ_81.setText("Type");
              OBJ_81.setName("OBJ_81");
              panel5.add(OBJ_81);
              OBJ_81.setBounds(315, 29, 45, 20);

              //---- OBJ_82 ----
              OBJ_82.setText("Type");
              OBJ_82.setName("OBJ_82");
              panel5.add(OBJ_82);
              OBJ_82.setBounds(315, 59, 45, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            p_TCI1ContentContainer.add(panel5);
            panel5.setBounds(455, 70, 445, 95);

            //---- MCTYC ----
            MCTYC.setName("MCTYC");
            p_TCI1ContentContainer.add(MCTYC);
            MCTYC.setBounds(115, 42, 60, MCTYC.getPreferredSize().height);

            //---- TCLIBR ----
            TCLIBR.setText("@TCLIBR@");
            TCLIBR.setName("TCLIBR");
            p_TCI1ContentContainer.add(TCLIBR);
            TCLIBR.setBounds(180, 44, 250, TCLIBR.getPreferredSize().height);

            //---- MCINT ----
            MCINT.setName("MCINT");
            p_TCI1ContentContainer.add(MCINT);
            MCINT.setBounds(590, 42, 40, MCINT.getPreferredSize().height);

            //---- OBJ_72 ----
            OBJ_72.setText("Dur\u00e9e");
            OBJ_72.setName("OBJ_72");
            p_TCI1ContentContainer.add(OBJ_72);
            OBJ_72.setBounds(15, 89, 65, 20);

            //---- MCDUR ----
            MCDUR.setName("MCDUR");
            p_TCI1ContentContainer.add(MCDUR);
            MCDUR.setBounds(115, 85, 28, MCDUR.getPreferredSize().height);

            //---- OBJ_73 ----
            OBJ_73.setText("Date de d\u00e9but");
            OBJ_73.setName("OBJ_73");
            p_TCI1ContentContainer.add(OBJ_73);
            OBJ_73.setBounds(180, 89, 90, 20);

            //---- DDCX ----
            DDCX.setName("DDCX");
            p_TCI1ContentContainer.add(DDCX);
            DDCX.setBounds(270, 85, 105, DDCX.getPreferredSize().height);

            //---- OBJ_74 ----
            OBJ_74.setText("Validit\u00e9");
            OBJ_74.setName("OBJ_74");
            p_TCI1ContentContainer.add(OBJ_74);
            OBJ_74.setBounds(180, 130, 55, 20);

            //---- MCDTVX ----
            MCDTVX.setName("MCDTVX");
            p_TCI1ContentContainer.add(MCDTVX);
            MCDTVX.setBounds(270, 126, 105, MCDTVX.getPreferredSize().height);
          }
          p_contenu.add(p_TCI1);
          p_TCI1.setBounds(15, 235, 915, 200);

          //======== p_TCI2 ========
          {
            p_TCI2.setTitle("Facturation");
            p_TCI2.setBorder(new DropShadowBorder());
            p_TCI2.setTitleFont(p_TCI2.getTitleFont().deriveFont(p_TCI2.getTitleFont().getStyle() | Font.BOLD));
            p_TCI2.setName("p_TCI2");
            Container p_TCI2ContentContainer = p_TCI2.getContentContainer();
            p_TCI2ContentContainer.setLayout(null);

            //---- OBJ_83 ----
            OBJ_83.setText("Montant");
            OBJ_83.setName("OBJ_83");
            p_TCI2ContentContainer.add(OBJ_83);
            OBJ_83.setBounds(15, 9, 70, 20);

            //---- OBJ_85 ----
            OBJ_85.setText("Renouvellement");
            OBJ_85.setName("OBJ_85");
            p_TCI2ContentContainer.add(OBJ_85);
            OBJ_85.setBounds(240, 47, 100, 20);

            //---- OBJ_84 ----
            OBJ_84.setText("P\u00e9riodicit\u00e9");
            OBJ_84.setName("OBJ_84");
            p_TCI2ContentContainer.add(OBJ_84);
            OBJ_84.setBounds(15, 47, 70, 20);

            //---- MCMTT ----
            MCMTT.setName("MCMTT");
            p_TCI2ContentContainer.add(MCMTT);
            MCMTT.setBounds(105, 5, 100, MCMTT.getPreferredSize().height);

            //---- MCFIN ----
            MCFIN.setText("Facturation \u00e0 l'intervention");
            MCFIN.setName("MCFIN");
            p_TCI2ContentContainer.add(MCFIN);
            MCFIN.setBounds(455, 10, 185, MCFIN.getPreferredSize().height);

            //======== panel6 ========
            {
              panel6.setBorder(new TitledBorder("Derni\u00e8re facture"));
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);

              //---- MCDAFX ----
              MCDAFX.setName("MCDAFX");
              panel6.add(MCDAFX);
              MCDAFX.setBounds(95, 31, 105, MCDAFX.getPreferredSize().height);

              //---- MCNFA ----
              MCNFA.setName("MCNFA");
              panel6.add(MCNFA);
              MCNFA.setBounds(305, 31, 68, MCNFA.getPreferredSize().height);

              //---- OBJ_91 ----
              OBJ_91.setText("Date");
              OBJ_91.setName("OBJ_91");
              panel6.add(OBJ_91);
              OBJ_91.setBounds(25, 35, 75, 20);

              //---- OBJ_93 ----
              OBJ_93.setText("Num\u00e9ro");
              OBJ_93.setName("OBJ_93");
              panel6.add(OBJ_93);
              OBJ_93.setBounds(245, 35, 45, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel6.getComponentCount(); i++) {
                  Rectangle bounds = panel6.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel6.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel6.setMinimumSize(preferredSize);
                panel6.setPreferredSize(preferredSize);
              }
            }
            p_TCI2ContentContainer.add(panel6);
            panel6.setBounds(455, 35, 445, 80);

            //---- DPRX ----
            DPRX.setName("DPRX");
            p_TCI2ContentContainer.add(DPRX);
            DPRX.setBounds(345, 43, 105, DPRX.getPreferredSize().height);

            //---- MCIN2 ----
            MCIN2.setText("\u00e9chue");
            MCIN2.setName("MCIN2");
            p_TCI2ContentContainer.add(MCIN2);
            MCIN2.setBounds(145, 48, 70, MCIN2.getPreferredSize().height);

            //---- MCPEM ----
            MCPEM.setName("MCPEM");
            p_TCI2ContentContainer.add(MCPEM);
            MCPEM.setBounds(105, 43, 28, MCPEM.getPreferredSize().height);

            //---- MCTFC ----
            MCTFC.setText("Factures regroup\u00e9es");
            MCTFC.setName("MCTFC");
            p_TCI2ContentContainer.add(MCTFC);
            MCTFC.setBounds(240, 10, 185, MCTFC.getPreferredSize().height);

            //---- MCIN1 ----
            MCIN1.setModel(new DefaultComboBoxModel(new String[] {
              "Pas de g\u00e9n\u00e9ration de ligne",
              "G\u00e9n\u00e9ration de ligne li\u00e9e au mat\u00e9riel",
              "Idem et g\u00e9n\u00e9ration d'une ligne par num\u00e9ro de s\u00e9rie"
            }));
            MCIN1.setName("MCIN1");
            p_TCI2ContentContainer.add(MCIN1);
            MCIN1.setBounds(15, 80, 420, MCIN1.getPreferredSize().height);
          }
          p_contenu.add(p_TCI2);
          p_TCI2.setBounds(15, 440, 915, 145);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setBorder(new TitledBorder("Client factur\u00e9"));
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- CLNOMF ----
            CLNOMF.setFont(CLNOMF.getFont().deriveFont(CLNOMF.getFont().getStyle() | Font.BOLD));
            CLNOMF.setName("CLNOMF");
            panel4.add(CLNOMF);
            CLNOMF.setBounds(15, 65, 310, CLNOMF.getPreferredSize().height);

            //---- CLCPLF ----
            CLCPLF.setName("CLCPLF");
            panel4.add(CLCPLF);
            CLCPLF.setBounds(15, 95, 310, CLCPLF.getPreferredSize().height);

            //---- CLRUEF ----
            CLRUEF.setName("CLRUEF");
            panel4.add(CLRUEF);
            CLRUEF.setBounds(15, 125, 310, CLRUEF.getPreferredSize().height);

            //---- CLLOCF ----
            CLLOCF.setName("CLLOCF");
            panel4.add(CLLOCF);
            CLLOCF.setBounds(15, 155, 310, CLLOCF.getPreferredSize().height);

            //---- OBJ_70 ----
            OBJ_70.setText("Num\u00e9ro");
            OBJ_70.setName("OBJ_70");
            panel4.add(OBJ_70);
            OBJ_70.setBounds(150, 39, 68, 20);

            //---- MCCLF1 ----
            MCCLF1.setName("MCCLF1");
            panel4.add(MCCLF1);
            MCCLF1.setBounds(225, 35, 60, MCCLF1.getPreferredSize().height);

            //---- MCLIVF ----
            MCLIVF.setName("MCLIVF");
            panel4.add(MCLIVF);
            MCLIVF.setBounds(289, 35, 36, MCLIVF.getPreferredSize().height);

            //---- CLVILF ----
            CLVILF.setName("CLVILF");
            panel4.add(CLVILF);
            CLVILF.setBounds(15, 185, 310, CLVILF.getPreferredSize().height);
          }
          p_contenu.add(panel4);
          panel4.setBounds(365, 5, 345, 225);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_66;
  private JLabel OBJ_68;
  private XRiTextField INDNCE;
  private XRiTextField INDETB;
  private JLabel OBJ_86;
  private XRiTextField MCCSC;
  private RiZoneSortie SULIBR;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel3;
  private XRiTextField CLNOML;
  private XRiTextField CLCPLL;
  private XRiTextField CLRUEL;
  private XRiTextField CLLOCL;
  private XRiTextField MCCLI1;
  private XRiTextField CLVILL;
  private XRiTextField MCLIV;
  private JLabel OBJ_69;
  private JXTitledPanel p_TCI1;
  private RiZoneSortie INLIBR;
  private XRiTextField MCDES;
  private JLabel OBJ_71;
  private JLabel OBJ_75;
  private JLabel OBJ_76;
  private JPanel panel5;
  private XRiTextField MCNBV;
  private XRiTextField MCNBV2;
  private XRiCalendrier DPVX;
  private XRiCalendrier DPV2X;
  private XRiTextField MCTPV;
  private XRiTextField MCTPV2;
  private JLabel OBJ_78;
  private JLabel OBJ_79;
  private JLabel OBJ_77;
  private JLabel OBJ_80;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private XRiTextField MCTYC;
  private RiZoneSortie TCLIBR;
  private XRiTextField MCINT;
  private JLabel OBJ_72;
  private XRiTextField MCDUR;
  private JLabel OBJ_73;
  private XRiCalendrier DDCX;
  private JLabel OBJ_74;
  private XRiCalendrier MCDTVX;
  private JXTitledPanel p_TCI2;
  private JLabel OBJ_83;
  private JLabel OBJ_85;
  private JLabel OBJ_84;
  private XRiTextField MCMTT;
  private XRiCheckBox MCFIN;
  private JPanel panel6;
  private XRiCalendrier MCDAFX;
  private XRiTextField MCNFA;
  private JLabel OBJ_91;
  private JLabel OBJ_93;
  private XRiCalendrier DPRX;
  private XRiCheckBox MCIN2;
  private XRiTextField MCPEM;
  private XRiCheckBox MCTFC;
  private XRiComboBox MCIN1;
  private JPanel panel4;
  private XRiTextField CLNOMF;
  private XRiTextField CLCPLF;
  private XRiTextField CLRUEF;
  private XRiTextField CLLOCF;
  private JLabel OBJ_70;
  private XRiTextField MCCLF1;
  private XRiTextField MCLIVF;
  private XRiTextField CLVILF;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
