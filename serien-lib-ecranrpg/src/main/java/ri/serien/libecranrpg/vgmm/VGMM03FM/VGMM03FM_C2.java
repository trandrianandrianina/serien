
package ri.serien.libecranrpg.vgmm.VGMM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGMM03FM_C2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGMM03FM_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    MACHAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MACHAX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    gererLesErreurs("19");
    
    
    
    panelDevise.setVisible(lexique.isTrue("N86"));
    
    // TODO Icones
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label3 = new JLabel();
    FRTEL = new XRiTextField();
    FRFAX = new XRiTextField();
    label4 = new JLabel();
    label6 = new JLabel();
    panel2 = new JPanel();
    label7 = new JLabel();
    label8 = new JLabel();
    label10 = new JLabel();
    label9 = new JLabel();
    MADBAX = new XRiCalendrier();
    MANBA = new XRiTextField();
    MASBA = new XRiTextField();
    MALBA = new XRiTextField();
    MAMAG = new XRiTextField();
    label17 = new JLabel();
    LIBTGE = new XRiTextField();
    panel3 = new JPanel();
    label11 = new JLabel();
    MADFAX = new XRiCalendrier();
    label12 = new JLabel();
    MANFA = new XRiTextField();
    MALFA = new XRiTextField();
    label13 = new JLabel();
    label15 = new JLabel();
    MANFA2 = new XRiTextField();
    MATAS = new XRiCheckBox();
    CLNOM = new XRiTextField();
    CLCPL = new XRiTextField();
    CLRUE = new XRiTextField();
    CLLOC = new XRiTextField();
    CLPAY = new XRiTextField();
    CLVILR = new XRiTextField();
    CLCDP = new XRiTextField();
    CLCOP = new XRiTextField();
    MACLI = new XRiTextField();
    MALIV = new XRiTextField();
    CLREP = new XRiTextField();
    OBJ_138 = new JLabel();
    CLMAG = new XRiTextField();
    OBJ_139 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    panelDevise = new JPanel();
    label14 = new JLabel();
    MALFA2 = new XRiTextField();
    label16 = new JLabel();
    MACHAX = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Affichage du bon");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Vente"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- label3 ----
          label3.setText("T\u00e9l\u00e9phone");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(395, 135, 80, 28);

          //---- FRTEL ----
          FRTEL.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
          FRTEL.setComponentPopupMenu(null);
          FRTEL.setName("FRTEL");
          panel1.add(FRTEL);
          FRTEL.setBounds(395, 160, 170, FRTEL.getPreferredSize().height);

          //---- FRFAX ----
          FRFAX.setToolTipText("Num\u00e9ro de fax");
          FRFAX.setComponentPopupMenu(null);
          FRFAX.setName("FRFAX");
          panel1.add(FRFAX);
          FRFAX.setBounds(395, 219, 170, FRFAX.getPreferredSize().height);

          //---- label4 ----
          label4.setText("Fax");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(395, 195, 80, 28);

          //---- label6 ----
          label6.setText("Client");
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(25, 45, 80, 28);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("R\u00e9ception"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- label7 ----
            label7.setText("Date");
            label7.setName("label7");
            panel2.add(label7);
            label7.setBounds(15, 30, 40, 28);

            //---- label8 ----
            label8.setText("Num\u00e9ro de bon");
            label8.setName("label8");
            panel2.add(label8);
            label8.setBounds(210, 30, 110, 28);

            //---- label10 ----
            label10.setText("Magasin");
            label10.setName("label10");
            panel2.add(label10);
            label10.setBounds(210, 60, 60, 28);

            //---- label9 ----
            label9.setText("Num\u00e9ro de ligne");
            label9.setName("label9");
            panel2.add(label9);
            label9.setBounds(425, 30, 100, 28);

            //---- MADBAX ----
            MADBAX.setName("MADBAX");
            panel2.add(MADBAX);
            MADBAX.setBounds(70, 30, 105, MADBAX.getPreferredSize().height);

            //---- MANBA ----
            MANBA.setComponentPopupMenu(BTD);
            MANBA.setName("MANBA");
            panel2.add(MANBA);
            MANBA.setBounds(325, 30, 60, MANBA.getPreferredSize().height);

            //---- MASBA ----
            MASBA.setComponentPopupMenu(BTD);
            MASBA.setName("MASBA");
            panel2.add(MASBA);
            MASBA.setBounds(385, 30, 20, MASBA.getPreferredSize().height);

            //---- MALBA ----
            MALBA.setComponentPopupMenu(BTD);
            MALBA.setName("MALBA");
            panel2.add(MALBA);
            MALBA.setBounds(525, 30, 44, MALBA.getPreferredSize().height);

            //---- MAMAG ----
            MAMAG.setComponentPopupMenu(BTD);
            MAMAG.setName("MAMAG");
            panel2.add(MAMAG);
            MAMAG.setBounds(325, 60, 34, MAMAG.getPreferredSize().height);

            //---- label17 ----
            label17.setText("Grat.");
            label17.setName("label17");
            panel2.add(label17);
            label17.setBounds(425, 60, 50, 28);

            //---- LIBTGE ----
            LIBTGE.setComponentPopupMenu(BTD);
            LIBTGE.setName("LIBTGE");
            panel2.add(LIBTGE);
            LIBTGE.setBounds(485, 60, 84, LIBTGE.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(20, 255, 585, 105);

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Facturation"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- label11 ----
            label11.setText("Date");
            label11.setName("label11");
            panel3.add(label11);
            label11.setBounds(15, 30, 40, 28);

            //---- MADFAX ----
            MADFAX.setName("MADFAX");
            panel3.add(MADFAX);
            MADFAX.setBounds(70, 30, 105, MADFAX.getPreferredSize().height);

            //---- label12 ----
            label12.setText("Num\u00e9ro de facture");
            label12.setName("label12");
            panel3.add(label12);
            label12.setBounds(210, 30, 110, 28);

            //---- MANFA ----
            MANFA.setComponentPopupMenu(BTD);
            MANFA.setName("MANFA");
            panel3.add(MANFA);
            MANFA.setBounds(325, 30, 70, MANFA.getPreferredSize().height);

            //---- MALFA ----
            MALFA.setComponentPopupMenu(BTD);
            MALFA.setName("MALFA");
            panel3.add(MALFA);
            MALFA.setBounds(525, 30, 44, MALFA.getPreferredSize().height);

            //---- label13 ----
            label13.setText("Num\u00e9ro de ligne");
            label13.setName("label13");
            panel3.add(label13);
            label13.setBounds(425, 30, 100, 28);

            //---- label15 ----
            label15.setText("Montant");
            label15.setName("label15");
            panel3.add(label15);
            label15.setBounds(425, 60, 55, 28);

            //---- MANFA2 ----
            MANFA2.setComponentPopupMenu(BTD);
            MANFA2.setName("MANFA2");
            panel3.add(MANFA2);
            MANFA2.setBounds(485, 60, 84, MANFA2.getPreferredSize().height);

            //---- MATAS ----
            MATAS.setText("Assurance");
            MATAS.setName("MATAS");
            panel3.add(MATAS);
            MATAS.setBounds(20, 65, 85, 23);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel3);
          panel3.setBounds(20, 365, 585, 105);

          //---- CLNOM ----
          CLNOM.setBackground(Color.white);
          CLNOM.setFont(CLNOM.getFont().deriveFont(CLNOM.getFont().getStyle() | Font.BOLD, CLNOM.getFont().getSize() + 1f));
          CLNOM.setName("CLNOM");
          panel1.add(CLNOM);
          CLNOM.setBounds(25, 70, 310, CLNOM.getPreferredSize().height);

          //---- CLCPL ----
          CLCPL.setBackground(Color.white);
          CLCPL.setName("CLCPL");
          panel1.add(CLCPL);
          CLCPL.setBounds(25, 101, 310, CLCPL.getPreferredSize().height);

          //---- CLRUE ----
          CLRUE.setBackground(Color.white);
          CLRUE.setName("CLRUE");
          panel1.add(CLRUE);
          CLRUE.setBounds(25, 130, 310, CLRUE.getPreferredSize().height);

          //---- CLLOC ----
          CLLOC.setBackground(Color.white);
          CLLOC.setName("CLLOC");
          panel1.add(CLLOC);
          CLLOC.setBounds(25, 159, 310, CLLOC.getPreferredSize().height);

          //---- CLPAY ----
          CLPAY.setBackground(Color.white);
          CLPAY.setName("CLPAY");
          panel1.add(CLPAY);
          CLPAY.setBounds(25, 219, 270, CLPAY.getPreferredSize().height);

          //---- CLVILR ----
          CLVILR.setBackground(Color.white);
          CLVILR.setFont(CLVILR.getFont().deriveFont(CLVILR.getFont().getStyle() | Font.BOLD, CLVILR.getFont().getSize() + 1f));
          CLVILR.setComponentPopupMenu(BTD);
          CLVILR.setName("CLVILR");
          panel1.add(CLVILR);
          CLVILR.setBounds(85, 188, 250, CLVILR.getPreferredSize().height);

          //---- CLCDP ----
          CLCDP.setBackground(Color.white);
          CLCDP.setFont(CLCDP.getFont().deriveFont(CLCDP.getFont().getStyle() | Font.BOLD, CLCDP.getFont().getSize() + 1f));
          CLCDP.setComponentPopupMenu(BTD);
          CLCDP.setName("CLCDP");
          panel1.add(CLCDP);
          CLCDP.setBounds(25, 188, 52, CLCDP.getPreferredSize().height);

          //---- CLCOP ----
          CLCOP.setBackground(Color.white);
          CLCOP.setComponentPopupMenu(BTD);
          CLCOP.setToolTipText("Code pays");
          CLCOP.setName("CLCOP");
          panel1.add(CLCOP);
          CLCOP.setBounds(295, 219, 40, CLCOP.getPreferredSize().height);

          //---- MACLI ----
          MACLI.setName("MACLI");
          panel1.add(MACLI);
          MACLI.setBounds(235, 40, 60, MACLI.getPreferredSize().height);

          //---- MALIV ----
          MALIV.setName("MALIV");
          panel1.add(MALIV);
          MALIV.setBounds(299, 40, 36, MALIV.getPreferredSize().height);

          //---- CLREP ----
          CLREP.setComponentPopupMenu(BTD);
          CLREP.setName("CLREP");
          panel1.add(CLREP);
          CLREP.setBounds(531, 105, 34, CLREP.getPreferredSize().height);

          //---- OBJ_138 ----
          OBJ_138.setText("Magasin");
          OBJ_138.setName("OBJ_138");
          panel1.add(OBJ_138);
          OBJ_138.setBounds(395, 71, 90, 28);

          //---- CLMAG ----
          CLMAG.setComponentPopupMenu(BTD);
          CLMAG.setName("CLMAG");
          panel1.add(CLMAG);
          CLMAG.setBounds(531, 71, 34, CLMAG.getPreferredSize().height);

          //---- OBJ_139 ----
          OBJ_139.setText("Repr\u00e9sentant");
          OBJ_139.setName("OBJ_139");
          panel1.add(OBJ_139);
          OBJ_139.setBounds(395, 105, 90, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 625, 485);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //======== panelDevise ========
    {
      panelDevise.setOpaque(false);
      panelDevise.setName("panelDevise");
      panelDevise.setLayout(null);

      //---- label14 ----
      label14.setText("Devise");
      label14.setName("label14");
      panelDevise.add(label14);
      label14.setBounds(10, 0, 55, 28);

      //---- MALFA2 ----
      MALFA2.setComponentPopupMenu(BTD);
      MALFA2.setName("MALFA2");
      panelDevise.add(MALFA2);
      MALFA2.setBounds(65, 0, 44, MALFA2.getPreferredSize().height);

      //---- label16 ----
      label16.setText("Taux");
      label16.setName("label16");
      panelDevise.add(label16);
      label16.setBounds(205, 0, 35, 28);

      //---- MACHAX ----
      MACHAX.setText("@MACHAX@");
      MACHAX.setName("MACHAX");
      panelDevise.add(MACHAX);
      MACHAX.setBounds(new Rectangle(new Point(287, 2), MACHAX.getPreferredSize()));

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panelDevise.getComponentCount(); i++) {
          Rectangle bounds = panelDevise.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panelDevise.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panelDevise.setMinimumSize(preferredSize);
        panelDevise.setPreferredSize(preferredSize);
      }
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label3;
  private XRiTextField FRTEL;
  private XRiTextField FRFAX;
  private JLabel label4;
  private JLabel label6;
  private JPanel panel2;
  private JLabel label7;
  private JLabel label8;
  private JLabel label10;
  private JLabel label9;
  private XRiCalendrier MADBAX;
  private XRiTextField MANBA;
  private XRiTextField MASBA;
  private XRiTextField MALBA;
  private XRiTextField MAMAG;
  private JLabel label17;
  private XRiTextField LIBTGE;
  private JPanel panel3;
  private JLabel label11;
  private XRiCalendrier MADFAX;
  private JLabel label12;
  private XRiTextField MANFA;
  private XRiTextField MALFA;
  private JLabel label13;
  private JLabel label15;
  private XRiTextField MANFA2;
  private XRiCheckBox MATAS;
  private XRiTextField CLNOM;
  private XRiTextField CLCPL;
  private XRiTextField CLRUE;
  private XRiTextField CLLOC;
  private XRiTextField CLPAY;
  private XRiTextField CLVILR;
  private XRiTextField CLCDP;
  private XRiTextField CLCOP;
  private XRiTextField MACLI;
  private XRiTextField MALIV;
  private XRiTextField CLREP;
  private JLabel OBJ_138;
  private XRiTextField CLMAG;
  private JLabel OBJ_139;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  private JPanel panelDevise;
  private JLabel label14;
  private XRiTextField MALFA2;
  private JLabel label16;
  private RiZoneSortie MACHAX;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
