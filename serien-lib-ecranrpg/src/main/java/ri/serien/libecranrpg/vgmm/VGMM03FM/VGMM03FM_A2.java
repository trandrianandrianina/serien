
package ri.serien.libecranrpg.vgmm.VGMM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGMM03FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "UTIT", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 541, };
  private String[] TOPSC_Value = { "", "2", "1" };
  
  public VGMM03FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    TOPSC.setValeurs(TOPSC_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    UETAL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UETAL@")).trim());
    CART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CART@")).trim());
    CLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    label1.setVisible(lexique.isTrue("(73) AND (N74)"));
    label2.setVisible(lexique.isTrue("(N73) AND (74)"));
    label3.setVisible(lexique.isTrue("(73) AND (74)"));
    barre_dup.setVisible(lexique.isTrue("56"));
    
    if (lexique.isTrue("57")) {
      p_bpresentation.setText("Fiche matériel pour affectation de numéro de contrat");
    }
    else {
      p_bpresentation.setText("Fiche matériel");
    }
    
    panel2.setVisible(lexique.isTrue("91"));
    
    if (lexique.HostFieldGetData("WCOMPO").trim().equalsIgnoreCase("1")) {
      label6.setText("Liste avec composants");
    }
    else {
      label6.setText("Liste sans composants");
    }
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void CHOISIR2ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CHOISIR3ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_66 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_68 = new JLabel();
    INDNUS = new XRiTextField();
    INDMAR = new XRiTextField();
    OBJ_69 = new JLabel();
    p_tete_droite = new JPanel();
    label3 = new JLabel();
    label2 = new JLabel();
    label1 = new JLabel();
    barre_dup = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_67 = new JLabel();
    IN3ETB = new XRiTextField();
    OBJ_70 = new JLabel();
    IN3NUS = new XRiTextField();
    IN3MAR = new XRiTextField();
    OBJ_71 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    label6 = new JLabel();
    panel1 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    label4 = new JLabel();
    xRiTextField1 = new XRiTextField();
    UETAL = new RiZoneSortie();
    label5 = new JLabel();
    UTCO = new XRiTextField();
    TOPSC = new XRiComboBox();
    panel2 = new JPanel();
    CART = new RiZoneSortie();
    CLIB = new RiZoneSortie();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    CHOISIR2 = new JMenuItem();
    CHOISIR3 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Fiche mat\u00e9riel");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_66 ----
          OBJ_66.setText("Etablissement");
          OBJ_66.setName("OBJ_66");
          p_tete_gauche.add(OBJ_66);
          OBJ_66.setBounds(5, 5, 95, 20);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(115, 1, 40, INDETB.getPreferredSize().height);

          //---- OBJ_68 ----
          OBJ_68.setText("Num\u00e9ro de s\u00e9rie");
          OBJ_68.setName("OBJ_68");
          p_tete_gauche.add(OBJ_68);
          OBJ_68.setBounds(200, 5, 105, 20);

          //---- INDNUS ----
          INDNUS.setName("INDNUS");
          p_tete_gauche.add(INDNUS);
          INDNUS.setBounds(310, 1, 310, INDNUS.getPreferredSize().height);

          //---- INDMAR ----
          INDMAR.setName("INDMAR");
          p_tete_gauche.add(INDMAR);
          INDMAR.setBounds(705, 1, 80, INDMAR.getPreferredSize().height);

          //---- OBJ_69 ----
          OBJ_69.setText("Marque");
          OBJ_69.setName("OBJ_69");
          p_tete_gauche.add(OBJ_69);
          OBJ_69.setBounds(645, 5, 55, 20);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- label3 ----
          label3.setText("Achet\u00e9/vendu");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD, label3.getFont().getSize() + 1f));
          label3.setName("label3");
          p_tete_droite.add(label3);

          //---- label2 ----
          label2.setText("Vendu");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD, label2.getFont().getSize() + 1f));
          label2.setName("label2");
          p_tete_droite.add(label2);

          //---- label1 ----
          label1.setText("Achet\u00e9");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 1f));
          label1.setName("label1");
          p_tete_droite.add(label1);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);

      //======== barre_dup ========
      {
        barre_dup.setMinimumSize(new Dimension(111, 34));
        barre_dup.setPreferredSize(new Dimension(111, 34));
        barre_dup.setName("barre_dup");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche2.setName("p_tete_gauche2");
          p_tete_gauche2.setLayout(null);

          //---- OBJ_67 ----
          OBJ_67.setText("Par duplication de");
          OBJ_67.setName("OBJ_67");
          p_tete_gauche2.add(OBJ_67);
          OBJ_67.setBounds(5, 5, 110, 20);

          //---- IN3ETB ----
          IN3ETB.setComponentPopupMenu(BTD);
          IN3ETB.setName("IN3ETB");
          p_tete_gauche2.add(IN3ETB);
          IN3ETB.setBounds(115, 1, 40, IN3ETB.getPreferredSize().height);

          //---- OBJ_70 ----
          OBJ_70.setText("Num\u00e9ro de s\u00e9rie");
          OBJ_70.setName("OBJ_70");
          p_tete_gauche2.add(OBJ_70);
          OBJ_70.setBounds(200, 5, 105, 20);

          //---- IN3NUS ----
          IN3NUS.setName("IN3NUS");
          p_tete_gauche2.add(IN3NUS);
          IN3NUS.setBounds(310, 1, 310, IN3NUS.getPreferredSize().height);

          //---- IN3MAR ----
          IN3MAR.setName("IN3MAR");
          p_tete_gauche2.add(IN3MAR);
          IN3MAR.setBounds(705, 1, 80, IN3MAR.getPreferredSize().height);

          //---- OBJ_71 ----
          OBJ_71.setText("Marque");
          OBJ_71.setName("OBJ_71");
          p_tete_gauche2.add(OBJ_71);
          OBJ_71.setBounds(645, 5, 55, 20);
        }
        barre_dup.add(p_tete_gauche2);
      }
      p_nord.add(barre_dup);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Autres vues");
              riSousMenu_bt6.setToolTipText("Autres vues");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Mat\u00e9riel achet\u00e9 ou vendu");
              riSousMenu_bt7.setToolTipText("Fiche mat\u00e9riel achet\u00e9");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Affichage composants");
              riSousMenu_bt8.setToolTipText("Affichage avec ou sans les composants");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- label6 ----
          label6.setText("Liste sans composants");
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setName("label6");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("L"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(25, 95, 660, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(700, 95, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(700, 240, 25, 125);

            //---- label4 ----
            label4.setText("Etat");
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(25, 30, 100, 28);

            //---- xRiTextField1 ----
            xRiTextField1.setName("xRiTextField1");
            panel1.add(xRiTextField1);
            xRiTextField1.setBounds(135, 30, 24, xRiTextField1.getPreferredSize().height);

            //---- UETAL ----
            UETAL.setText("@UETAL@");
            UETAL.setName("UETAL");
            panel1.add(UETAL);
            UETAL.setBounds(165, 32, 160, UETAL.getPreferredSize().height);

            //---- label5 ----
            label5.setText("Type de contrat");
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(25, 60, 100, 28);

            //---- UTCO ----
            UTCO.setName("UTCO");
            panel1.add(UTCO);
            UTCO.setBounds(135, 60, 60, UTCO.getPreferredSize().height);

            //---- TOPSC ----
            TOPSC.setModel(new DefaultComboBoxModel(new String[] {
              "Toutes les fiches mat\u00e9riel",
              "Fiches mat\u00e9riel sous contrat",
              "Fiches mat\u00e9riel sans contrat"
            }));
            TOPSC.setName("TOPSC");
            panel1.add(TOPSC);
            TOPSC.setBounds(200, 61, 200, TOPSC.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Article compos\u00e9"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- CART ----
            CART.setText("@CART@");
            CART.setName("CART");
            panel2.add(CART);
            CART.setBounds(25, 35, 210, CART.getPreferredSize().height);

            //---- CLIB ----
            CLIB.setText("@CLIB@");
            CLIB.setName("CLIB");
            panel2.add(CLIB);
            CLIB.setBounds(255, 35, 310, CLIB.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 754, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 754, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(label6, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 388, GroupLayout.PREFERRED_SIZE))
                  .addComponent(label6, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- CHOISIR2 ----
      CHOISIR2.setText("Modifier");
      CHOISIR2.setName("CHOISIR2");
      CHOISIR2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIR2ActionPerformed(e);
        }
      });
      BTD.add(CHOISIR2);

      //---- CHOISIR3 ----
      CHOISIR3.setText("Annuler");
      CHOISIR3.setName("CHOISIR3");
      CHOISIR3.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIR3ActionPerformed(e);
        }
      });
      BTD.add(CHOISIR3);
      BTD.addSeparator();

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("Invite");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_66;
  private XRiTextField INDETB;
  private JLabel OBJ_68;
  private XRiTextField INDNUS;
  private XRiTextField INDMAR;
  private JLabel OBJ_69;
  private JPanel p_tete_droite;
  private JLabel label3;
  private JLabel label2;
  private JLabel label1;
  private JMenuBar barre_dup;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_67;
  private XRiTextField IN3ETB;
  private JLabel OBJ_70;
  private XRiTextField IN3NUS;
  private XRiTextField IN3MAR;
  private JLabel OBJ_71;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel label6;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel label4;
  private XRiTextField xRiTextField1;
  private RiZoneSortie UETAL;
  private JLabel label5;
  private XRiTextField UTCO;
  private XRiComboBox TOPSC;
  private JPanel panel2;
  private RiZoneSortie CART;
  private RiZoneSortie CLIB;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem CHOISIR2;
  private JMenuItem CHOISIR3;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
