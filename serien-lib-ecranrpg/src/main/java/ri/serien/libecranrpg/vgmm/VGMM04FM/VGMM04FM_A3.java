
package ri.serien.libecranrpg.vgmm.VGMM04FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGMM04FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTR01_Top = { "WTR01", "WTR02", "WTR03", "WTR04", "WTR05", "WTR06", "WTR07", "WTR08", "WTR09", "WTR10", "WTR11",
      "WTR12", "WTR13", "WTR14", "WTR15", };
  private String[] _WTR01_Title = { "HLD02", };
  private String[][] _WTR01_Data = { { "LR01", "DMS01", "INT01", }, { "LR02", "DMS02", "INT02", }, { "LR03", "DMS03", "INT03", },
      { "LR04", "DMS04", "INT04", }, { "LR05", "DMS05", "INT05", }, { "LR06", "DMS06", "INT06", }, { "LR07", "DMS07", "INT07", },
      { "LR08", "DMS08", "INT08", }, { "LR09", "DMS09", "INT09", }, { "LR10", "DMS10", "INT10", }, { "LR11", "DMS11", "INT11", },
      { "LR12", "DMS12", "INT12", }, { "LR13", "DMS13", "INT13", }, { "LR14", "DMS14", "INT14", }, { "LR15", "DMS15", "INT15", }, };
  private int[] _WTR01_Width = { 700, };
  
  public VGMM04FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTR01.setAspectTable(_WTR01_Top, _WTR01_Title, _WTR01_Data, _WTR01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTR01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTR01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void CHOISIR2ActionPerformed(ActionEvent e) {
    WTR01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_66 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_68 = new JLabel();
    INDCLI = new XRiTextField();
    INDLIV = new XRiTextField();
    OBJ_69 = new JLabel();
    WDSM = new XRiCalendrier();
    OBJ_70 = new JLabel();
    WINT = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    INT01 = new XRiTextField();
    SCROLLPANE_LIST = new JScrollPane();
    WTR01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    DMS01 = new XRiCalendrier();
    DMS02 = new XRiCalendrier();
    INT02 = new XRiTextField();
    DMS03 = new XRiCalendrier();
    INT03 = new XRiTextField();
    DMS04 = new XRiCalendrier();
    INT04 = new XRiTextField();
    DMS05 = new XRiCalendrier();
    INT05 = new XRiTextField();
    DMS06 = new XRiCalendrier();
    INT06 = new XRiTextField();
    DMS07 = new XRiCalendrier();
    INT07 = new XRiTextField();
    DMS08 = new XRiCalendrier();
    INT08 = new XRiTextField();
    DMS09 = new XRiCalendrier();
    INT09 = new XRiTextField();
    DMS10 = new XRiCalendrier();
    INT10 = new XRiTextField();
    DMS11 = new XRiCalendrier();
    INT11 = new XRiTextField();
    DMS12 = new XRiCalendrier();
    INT12 = new XRiTextField();
    DMS13 = new XRiCalendrier();
    INT13 = new XRiTextField();
    DMS14 = new XRiCalendrier();
    INT14 = new XRiTextField();
    DMS15 = new XRiCalendrier();
    INT15 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    CHOISIR2 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_25 = new JMenuItem();
    OBJ_24 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Mise en service de mat\u00e9riels");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_66 ----
          OBJ_66.setText("Etablissement");
          OBJ_66.setName("OBJ_66");
          p_tete_gauche.add(OBJ_66);
          OBJ_66.setBounds(5, 5, 95, 20);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(115, 1, 40, INDETB.getPreferredSize().height);

          //---- OBJ_68 ----
          OBJ_68.setText("Client");
          OBJ_68.setName("OBJ_68");
          p_tete_gauche.add(OBJ_68);
          OBJ_68.setBounds(200, 5, 68, 20);

          //---- INDCLI ----
          INDCLI.setComponentPopupMenu(BTD);
          INDCLI.setName("INDCLI");
          p_tete_gauche.add(INDCLI);
          INDCLI.setBounds(265, 1, 60, INDCLI.getPreferredSize().height);

          //---- INDLIV ----
          INDLIV.setComponentPopupMenu(BTD);
          INDLIV.setName("INDLIV");
          p_tete_gauche.add(INDLIV);
          INDLIV.setBounds(325, 1, 40, INDLIV.getPreferredSize().height);

          //---- OBJ_69 ----
          OBJ_69.setText("Date");
          OBJ_69.setName("OBJ_69");
          p_tete_gauche.add(OBJ_69);
          OBJ_69.setBounds(395, 5, 60, 20);

          //---- WDSM ----
          WDSM.setName("WDSM");
          p_tete_gauche.add(WDSM);
          WDSM.setBounds(455, 1, 105, WDSM.getPreferredSize().height);

          //---- OBJ_70 ----
          OBJ_70.setText("Int");
          OBJ_70.setName("OBJ_70");
          p_tete_gauche.add(OBJ_70);
          OBJ_70.setBounds(580, 5, 45, 20);

          //---- WINT ----
          WINT.setComponentPopupMenu(BTD);
          WINT.setName("WINT");
          p_tete_gauche.add(WINT);
          WINT.setBounds(625, 1, 40, WINT.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Mat\u00e9riels en service");
            riSousMenu_bt7.setToolTipText("Activer/d\u00e9sactiver la vue des mat\u00e9riels en service");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- INT01 ----
            INT01.setComponentPopupMenu(BTD2);
            INT01.setName("INT01");
            panel1.add(INT01);
            INT01.setBounds(650, 70, 40, 28);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTR01 ----
              WTR01.setComponentPopupMenu(BTD);
              WTR01.setRowHeight(30);
              WTR01.setName("WTR01");
              WTR01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTR01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(30, 45, 495, 480);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(715, 45, 25, 205);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(715, 320, 25, 205);

            //---- DMS01 ----
            DMS01.setName("DMS01");
            panel1.add(DMS01);
            DMS01.setBounds(540, 70, 105, DMS01.getPreferredSize().height);

            //---- DMS02 ----
            DMS02.setName("DMS02");
            panel1.add(DMS02);
            DMS02.setBounds(540, 100, 105, DMS02.getPreferredSize().height);

            //---- INT02 ----
            INT02.setComponentPopupMenu(BTD2);
            INT02.setName("INT02");
            panel1.add(INT02);
            INT02.setBounds(650, 100, 40, 28);

            //---- DMS03 ----
            DMS03.setName("DMS03");
            panel1.add(DMS03);
            DMS03.setBounds(540, 130, 105, DMS03.getPreferredSize().height);

            //---- INT03 ----
            INT03.setComponentPopupMenu(BTD2);
            INT03.setName("INT03");
            panel1.add(INT03);
            INT03.setBounds(650, 130, 40, 28);

            //---- DMS04 ----
            DMS04.setName("DMS04");
            panel1.add(DMS04);
            DMS04.setBounds(540, 160, 105, DMS04.getPreferredSize().height);

            //---- INT04 ----
            INT04.setComponentPopupMenu(BTD2);
            INT04.setName("INT04");
            panel1.add(INT04);
            INT04.setBounds(650, 160, 40, 28);

            //---- DMS05 ----
            DMS05.setName("DMS05");
            panel1.add(DMS05);
            DMS05.setBounds(540, 190, 105, DMS05.getPreferredSize().height);

            //---- INT05 ----
            INT05.setComponentPopupMenu(BTD2);
            INT05.setName("INT05");
            panel1.add(INT05);
            INT05.setBounds(650, 190, 40, 28);

            //---- DMS06 ----
            DMS06.setName("DMS06");
            panel1.add(DMS06);
            DMS06.setBounds(540, 220, 105, DMS06.getPreferredSize().height);

            //---- INT06 ----
            INT06.setComponentPopupMenu(BTD2);
            INT06.setName("INT06");
            panel1.add(INT06);
            INT06.setBounds(650, 220, 40, 28);

            //---- DMS07 ----
            DMS07.setName("DMS07");
            panel1.add(DMS07);
            DMS07.setBounds(540, 250, 105, DMS07.getPreferredSize().height);

            //---- INT07 ----
            INT07.setComponentPopupMenu(BTD2);
            INT07.setName("INT07");
            panel1.add(INT07);
            INT07.setBounds(650, 250, 40, 28);

            //---- DMS08 ----
            DMS08.setName("DMS08");
            panel1.add(DMS08);
            DMS08.setBounds(540, 280, 105, DMS08.getPreferredSize().height);

            //---- INT08 ----
            INT08.setComponentPopupMenu(BTD2);
            INT08.setName("INT08");
            panel1.add(INT08);
            INT08.setBounds(650, 280, 40, 28);

            //---- DMS09 ----
            DMS09.setName("DMS09");
            panel1.add(DMS09);
            DMS09.setBounds(540, 310, 105, DMS09.getPreferredSize().height);

            //---- INT09 ----
            INT09.setComponentPopupMenu(BTD2);
            INT09.setName("INT09");
            panel1.add(INT09);
            INT09.setBounds(650, 310, 40, 28);

            //---- DMS10 ----
            DMS10.setName("DMS10");
            panel1.add(DMS10);
            DMS10.setBounds(540, 340, 105, DMS10.getPreferredSize().height);

            //---- INT10 ----
            INT10.setComponentPopupMenu(BTD2);
            INT10.setName("INT10");
            panel1.add(INT10);
            INT10.setBounds(650, 340, 40, 28);

            //---- DMS11 ----
            DMS11.setName("DMS11");
            panel1.add(DMS11);
            DMS11.setBounds(540, 370, 105, DMS11.getPreferredSize().height);

            //---- INT11 ----
            INT11.setComponentPopupMenu(BTD2);
            INT11.setName("INT11");
            panel1.add(INT11);
            INT11.setBounds(650, 370, 40, 28);

            //---- DMS12 ----
            DMS12.setName("DMS12");
            panel1.add(DMS12);
            DMS12.setBounds(540, 400, 105, DMS12.getPreferredSize().height);

            //---- INT12 ----
            INT12.setComponentPopupMenu(BTD2);
            INT12.setName("INT12");
            panel1.add(INT12);
            INT12.setBounds(650, 400, 40, 28);

            //---- DMS13 ----
            DMS13.setName("DMS13");
            panel1.add(DMS13);
            DMS13.setBounds(540, 430, 105, DMS13.getPreferredSize().height);

            //---- INT13 ----
            INT13.setComponentPopupMenu(BTD2);
            INT13.setName("INT13");
            panel1.add(INT13);
            INT13.setBounds(650, 430, 40, 28);

            //---- DMS14 ----
            DMS14.setName("DMS14");
            panel1.add(DMS14);
            DMS14.setBounds(540, 460, 105, DMS14.getPreferredSize().height);

            //---- INT14 ----
            INT14.setComponentPopupMenu(BTD2);
            INT14.setName("INT14");
            panel1.add(INT14);
            INT14.setBounds(650, 460, 40, 28);

            //---- DMS15 ----
            DMS15.setName("DMS15");
            panel1.add(DMS15);
            DMS15.setBounds(540, 490, 105, DMS15.getPreferredSize().height);

            //---- INT15 ----
            INT15.setComponentPopupMenu(BTD2);
            INT15.setName("INT15");
            panel1.add(INT15);
            INT15.setBounds(650, 490, 40, 28);

            //---- label1 ----
            label1.setText("Date");
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(540, 45, 65, 25);

            //---- label2 ----
            label2.setText("Intervenant");
            label2.setHorizontalAlignment(SwingConstants.CENTER);
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(635, 45, 70, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(13, 14, 754, 551);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Fiche mat\u00e9riel");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- CHOISIR2 ----
      CHOISIR2.setText("Modifier");
      CHOISIR2.setName("CHOISIR2");
      CHOISIR2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIR2ActionPerformed(e);
        }
      });
      BTD.add(CHOISIR2);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_25 ----
      OBJ_25.setText("Choix possibles");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_25);

      //---- OBJ_24 ----
      OBJ_24.setText("Aide en ligne");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_24);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_66;
  private XRiTextField INDETB;
  private JLabel OBJ_68;
  private XRiTextField INDCLI;
  private XRiTextField INDLIV;
  private JLabel OBJ_69;
  private XRiCalendrier WDSM;
  private JLabel OBJ_70;
  private XRiTextField WINT;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField INT01;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTR01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiCalendrier DMS01;
  private XRiCalendrier DMS02;
  private XRiTextField INT02;
  private XRiCalendrier DMS03;
  private XRiTextField INT03;
  private XRiCalendrier DMS04;
  private XRiTextField INT04;
  private XRiCalendrier DMS05;
  private XRiTextField INT05;
  private XRiCalendrier DMS06;
  private XRiTextField INT06;
  private XRiCalendrier DMS07;
  private XRiTextField INT07;
  private XRiCalendrier DMS08;
  private XRiTextField INT08;
  private XRiCalendrier DMS09;
  private XRiTextField INT09;
  private XRiCalendrier DMS10;
  private XRiTextField INT10;
  private XRiCalendrier DMS11;
  private XRiTextField INT11;
  private XRiCalendrier DMS12;
  private XRiTextField INT12;
  private XRiCalendrier DMS13;
  private XRiTextField INT13;
  private XRiCalendrier DMS14;
  private XRiTextField INT14;
  private XRiCalendrier DMS15;
  private XRiTextField INT15;
  private JLabel label1;
  private JLabel label2;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem CHOISIR2;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_24;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
