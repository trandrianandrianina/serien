
package ri.serien.libecranrpg.vgmm.VGMM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGMM03FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGMM03FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    A1LIB.activerModeFantome("Désignation principale");
    A1LB1.activerModeFantome("Désignation complémentaire");
    A1LB2.activerModeFantome("Désignation complémentaire");
    A1LB3.activerModeFantome("Désignation complémentaire");
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    TYPMAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYPMAT@")).trim());
    riMenu_bt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    FRNOMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOMR@")).trim());
    CLNOMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOMR@")).trim());
    CADEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADEL@")).trim());
    TCLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TCLIBR@")).trim());
    INLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INLIBR@")).trim());
    MAETAL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAETAL@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIB@")).trim());
    WTI1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    WTI2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    WTI3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    WTI4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    WTI5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
  }
  
  @Override
  public void setData() {
    // TODO set Data
    
    super.setData();
    
    
    setDiverses();
    


    gererLesErreurs("19");
    

    
    
    // Les panels +++++++++++++++++++++++++++++++++
    
    ACHAT.setRightDecoration(bt_achat);
    VENTE.setRightDecoration(bt_vente);
    EXPLO.setRightDecoration(bt_explo);
    SAV.setRightDecoration(bt_sav);
    SUIVI.setRightDecoration(bt_suivi);
    OBS.setRightDecoration(bt_obs);
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(riMenu_bt2);
    
    // TODO Icones

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed() {
    lexique.HostCursorPut(10, 2);
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void riSousMenu_bt7ActionPerformed() {
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt8ActionPerformed() {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt9ActionPerformed() {
    if (lexique.isTrue("52")) {
      lexique.HostCursorPut("WXMA");
      lexique.HostScreenSendKey(this, "F4");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "E");
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void riSousMenu_bt14ActionPerformed() {
    lexique.HostCursorPut(12, 2);
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void riSousMenu_bt15ActionPerformed() {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("A1RTA");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void changeDesignationActionPerformed(ActionEvent e) {
    if (lexique.getMode() == Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("V06FO", 0, "L");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostCursorPut("A1LIB");
      lexique.HostScreenSendKey(this, "F4");
    }
  }
  
  private void bt_achatActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI1").trim().equals("")) {
      lexique.HostFieldPutData("TCI1", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI1"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_venteActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI2").trim().equals("")) {
      lexique.HostFieldPutData("TCI2", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI2"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_exploActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI3").trim().equals("")) {
      lexique.HostFieldPutData("TCI3", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI3"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_savActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI4").trim().equals("")) {
      lexique.HostFieldPutData("TCI4", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI4"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_suiviActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI5").trim().equals("")) {
      lexique.HostFieldPutData("TCI5", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI5"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_obsActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI6").trim().equals("")) {
      lexique.HostFieldPutData("TCI6", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI6"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_66 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_68 = new JLabel();
    INDNUS = new XRiTextField();
    INDMAR = new XRiTextField();
    OBJ_69 = new JLabel();
    TYPMAT = new RiZoneSortie();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    layeredPane1 = new JLayeredPane();
    ACHAT = new JXTitledPanel();
    OBJ_143_OBJ_144 = new JLabel();
    MAMAG = new XRiTextField();
    FRNOMR = new RiZoneSortie();
    OBJ_143_OBJ_143 = new JLabel();
    OBJ_143_OBJ_145 = new JLabel();
    MAFRS = new XRiTextField();
    MAFRC = new XRiTextField();
    MADBAX = new XRiCalendrier();
    VENTE = new JXTitledPanel();
    MADBVX = new XRiCalendrier();
    OBJ_143_OBJ_146 = new JLabel();
    OBJ_143_OBJ_147 = new JLabel();
    WMAG = new XRiTextField();
    OBJ_143_OBJ_148 = new JLabel();
    MALIV = new XRiTextField();
    MACLI = new XRiTextField();
    CLNOMR = new RiZoneSortie();
    SAV = new JXTitledPanel();
    OBJ_170_OBJ_170 = new JLabel();
    CADEL = new RiZoneSortie();
    TCLIBR = new RiZoneSortie();
    MANCE = new XRiTextField();
    MATCO = new XRiTextField();
    OBJ_170_OBJ_171 = new JLabel();
    MACGA = new XRiTextField();
    OBJ_170_OBJ_172 = new JLabel();
    OBJ_170_OBJ_173 = new JLabel();
    MADUG = new XRiTextField();
    OBJ_170_OBJ_174 = new JLabel();
    MADDGX = new XRiCalendrier();
    MADFGX = new XRiCalendrier();
    OBJ_170_OBJ_175 = new JLabel();
    OBJ_170_OBJ_176 = new JLabel();
    MAPRO = new XRiTextField();
    SUIVI = new JXTitledPanel();
    voir_stats = new JPanel();
    OBJ_170_OBJ_177 = new JLabel();
    MITYI = new XRiTextField();
    MIINT = new XRiTextField();
    OBJ_170_OBJ_178 = new JLabel();
    INLIBR = new RiZoneSortie();
    OBJ_170_OBJ_179 = new JLabel();
    MIDINX = new XRiCalendrier();
    OBJ_170_OBJ_180 = new JLabel();
    MADP1X = new XRiCalendrier();
    MADP2X = new XRiCalendrier();
    OBJ_170_OBJ_181 = new JLabel();
    OBJ_170_OBJ_182 = new JLabel();
    MACPV = new XRiTextField();
    MADVI = new XRiTextField();
    OBJ_170_OBJ_183 = new JLabel();
    EXPLO = new JXTitledPanel();
    OBJ_170_OBJ_184 = new JLabel();
    WPROD = new XRiTextField();
    WCHAR = new XRiTextField();
    OBJ_170_OBJ_185 = new JLabel();
    OBJ_170_OBJ_186 = new JLabel();
    WRESL = new XRiTextField();
    OBS = new JXTitledPanel();
    OB1R = new XRiTextField();
    OBJ_170_OBJ_187 = new JLabel();
    MAETA = new XRiTextField();
    MAETAL = new RiZoneSortie();
    OBJ_126_OBJ_126 = new JLabel();
    MAFAM = new XRiTextField();
    riZoneSortie2 = new RiZoneSortie();
    OBJ_126_OBJ_127 = new JLabel();
    MAMOV = new XRiTextField();
    OBJ_126_OBJ_128 = new JLabel();
    MARAT = new XRiTextField();
    OBJ_126_OBJ_129 = new JLabel();
    MARAN = new XRiTextField();
    panel1 = new JPanel();
    WTI1 = new RiZoneSortie();
    WTI2 = new RiZoneSortie();
    WTI3 = new RiZoneSortie();
    WTI4 = new RiZoneSortie();
    WTI5 = new RiZoneSortie();
    MAZP1 = new XRiTextField();
    MAZP2 = new XRiTextField();
    MAZP3 = new XRiTextField();
    MAZP4 = new XRiTextField();
    MAZP5 = new XRiTextField();
    MAART = new XRiTextField();
    OBJ_73_OBJ_73 = new JLabel();
    btnChangeDesignation = new SNBoutonDetail();
    A1LIB = new XRiTextField();
    A1LB1 = new XRiTextField();
    A1LB2 = new XRiTextField();
    A1LB3 = new XRiTextField();
    bt_achat = new SNBoutonDetail();
    bt_vente = new SNBoutonDetail();
    bt_explo = new SNBoutonDetail();
    bt_sav = new SNBoutonDetail();
    bt_suivi = new SNBoutonDetail();
    bt_obs = new SNBoutonDetail();
    BTD = new JPopupMenu();
    OBJ_22 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(1080, 700));
    setPreferredSize(new Dimension(1080, 700));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Fiche mat\u00e9riel");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_66 ----
          OBJ_66.setText("Etablissement");
          OBJ_66.setName("OBJ_66");
          p_tete_gauche.add(OBJ_66);
          OBJ_66.setBounds(5, 5, 95, 20);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(115, 1, 40, INDETB.getPreferredSize().height);

          //---- OBJ_68 ----
          OBJ_68.setText("Num\u00e9ro de s\u00e9rie");
          OBJ_68.setName("OBJ_68");
          p_tete_gauche.add(OBJ_68);
          OBJ_68.setBounds(200, 5, 105, 20);

          //---- INDNUS ----
          INDNUS.setName("INDNUS");
          p_tete_gauche.add(INDNUS);
          INDNUS.setBounds(310, 1, 310, INDNUS.getPreferredSize().height);

          //---- INDMAR ----
          INDMAR.setName("INDMAR");
          p_tete_gauche.add(INDMAR);
          INDMAR.setBounds(705, 1, 80, INDMAR.getPreferredSize().height);

          //---- OBJ_69 ----
          OBJ_69.setText("Marque");
          OBJ_69.setName("OBJ_69");
          p_tete_gauche.add(OBJ_69);
          OBJ_69.setBounds(645, 5, 55, 20);

          //---- TYPMAT ----
          TYPMAT.setHorizontalAlignment(SwingConstants.LEADING);
          TYPMAT.setText("@TYPMAT@");
          TYPMAT.setHorizontalTextPosition(SwingConstants.LEADING);
          TYPMAT.setName("TYPMAT");
          p_tete_gauche.add(TYPMAT);
          TYPMAT.setBounds(795, 3, 100, TYPMAT.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setBackground(new Color(214, 217, 223));
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt1 ----
              riMenu_bt1.setText("@V01F@");
              riMenu_bt1.setPreferredSize(new Dimension(167, 50));
              riMenu_bt1.setMinimumSize(new Dimension(167, 50));
              riMenu_bt1.setMaximumSize(new Dimension(170, 50));
              riMenu_bt1.setFont(riMenu_bt1.getFont().deriveFont(riMenu_bt1.getFont().getSize() + 2f));
              riMenu_bt1.setName("riMenu_bt1");
              riMenu_V01F.add(riMenu_bt1);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setToolTipText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Bloc-notes");
              riSousMenu_bt15.setToolTipText("Bloc-notes : page d'informations li\u00e9es \u00e0 l'article et articles li\u00e9s (Shift + F10)");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed();
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Lib\u00e9ration vente");
              riSousMenu_bt14.setToolTipText("Lib\u00e9ration vente");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed();
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Lib\u00e9ration achat");
              riSousMenu_bt6.setToolTipText("Lib\u00e9ration achat");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed();
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Compl\u00e9ments");
              riMenu_bt3.setToolTipText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Options article");
              riSousMenu_bt12.setToolTipText("R\u00e9capitulatif des unit\u00e9s utilis\u00e9es : tableau des unit\u00e9s vente/achat/stock et relations (F6)");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Options client");
              riSousMenu_bt13.setToolTipText("R\u00e9capitulatif des unit\u00e9s utilis\u00e9es : tableau des unit\u00e9s vente/achat/stock et relations (F6)");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Options");
              riSousMenu_bt7.setToolTipText("Activation/d\u00e9sactivation de l'affichage personnalis\u00e9 : fiche article param\u00e9tr\u00e9e (Shift F12)");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed();
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Documents li\u00e9s");
              riSousMenu_bt8.setToolTipText("Fiche r\u00e9approvisionnement : informations de gestion li\u00e9es au r\u00e9approvisionnement");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed();
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Acc\u00e9s extension");
              riSousMenu_bt9.setToolTipText("Gestion des libell\u00e9s : gestion des d\u00e9signations compl\u00e9mentaires par code langue");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed();
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(880, 600));
          p_contenu.setBorder(null);
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(880, 600));
          p_contenu.setName("p_contenu");

          //======== layeredPane1 ========
          {
            layeredPane1.setBorder(LineBorder.createGrayLineBorder());
            layeredPane1.setPreferredSize(new Dimension(880, 620));
            layeredPane1.setMaximumSize(new Dimension(880, 620));
            layeredPane1.setMinimumSize(new Dimension(880, 620));
            layeredPane1.setName("layeredPane1");

            //======== ACHAT ========
            {
              ACHAT.setTitle("Achat");
              ACHAT.setBorder(new DropShadowBorder());
              ACHAT.setName("ACHAT");
              Container ACHATContentContainer = ACHAT.getContentContainer();
              ACHATContentContainer.setLayout(null);

              //---- OBJ_143_OBJ_144 ----
              OBJ_143_OBJ_144.setText("Magasin");
              OBJ_143_OBJ_144.setName("OBJ_143_OBJ_144");
              ACHATContentContainer.add(OBJ_143_OBJ_144);
              OBJ_143_OBJ_144.setBounds(275, 7, 60, 24);

              //---- MAMAG ----
              MAMAG.setComponentPopupMenu(BTD);
              MAMAG.setToolTipText("Unit\u00e9 de stock");
              MAMAG.setName("MAMAG");
              ACHATContentContainer.add(MAMAG);
              MAMAG.setBounds(340, 5, 34, MAMAG.getPreferredSize().height);

              //---- FRNOMR ----
              FRNOMR.setHorizontalAlignment(SwingConstants.LEADING);
              FRNOMR.setText("@FRNOMR@");
              FRNOMR.setName("FRNOMR");
              ACHATContentContainer.add(FRNOMR);
              FRNOMR.setBounds(210, 40, 160, FRNOMR.getPreferredSize().height);

              //---- OBJ_143_OBJ_143 ----
              OBJ_143_OBJ_143.setText("Fournisseur");
              OBJ_143_OBJ_143.setName("OBJ_143_OBJ_143");
              ACHATContentContainer.add(OBJ_143_OBJ_143);
              OBJ_143_OBJ_143.setBounds(10, 40, 95, 24);

              //---- OBJ_143_OBJ_145 ----
              OBJ_143_OBJ_145.setText("R\u00e9ception");
              OBJ_143_OBJ_145.setName("OBJ_143_OBJ_145");
              ACHATContentContainer.add(OBJ_143_OBJ_145);
              OBJ_143_OBJ_145.setBounds(10, 7, 75, 24);

              //---- MAFRS ----
              MAFRS.setComponentPopupMenu(BTD);
              MAFRS.setName("MAFRS");
              ACHATContentContainer.add(MAFRS);
              MAFRS.setBounds(130, 38, 60, MAFRS.getPreferredSize().height);

              //---- MAFRC ----
              MAFRC.setComponentPopupMenu(BTD);
              MAFRC.setName("MAFRC");
              ACHATContentContainer.add(MAFRC);
              MAFRC.setBounds(105, 38, 24, MAFRC.getPreferredSize().height);

              //---- MADBAX ----
              MADBAX.setName("MADBAX");
              ACHATContentContainer.add(MADBAX);
              MADBAX.setBounds(105, 5, 105, MADBAX.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < ACHATContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = ACHATContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = ACHATContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                ACHATContentContainer.setMinimumSize(preferredSize);
                ACHATContentContainer.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(ACHAT, JLayeredPane.DEFAULT_LAYER);
            ACHAT.setBounds(20, 190, 390, 99);

            //======== VENTE ========
            {
              VENTE.setTitle("Vente");
              VENTE.setBorder(new DropShadowBorder());
              VENTE.setName("VENTE");
              Container VENTEContentContainer = VENTE.getContentContainer();
              VENTEContentContainer.setLayout(null);

              //---- MADBVX ----
              MADBVX.setName("MADBVX");
              VENTEContentContainer.add(MADBVX);
              MADBVX.setBounds(105, 5, 105, MADBVX.getPreferredSize().height);

              //---- OBJ_143_OBJ_146 ----
              OBJ_143_OBJ_146.setText("Exp\u00e9dition");
              OBJ_143_OBJ_146.setName("OBJ_143_OBJ_146");
              VENTEContentContainer.add(OBJ_143_OBJ_146);
              OBJ_143_OBJ_146.setBounds(10, 5, 75, 24);

              //---- OBJ_143_OBJ_147 ----
              OBJ_143_OBJ_147.setText("Magasin");
              OBJ_143_OBJ_147.setName("OBJ_143_OBJ_147");
              VENTEContentContainer.add(OBJ_143_OBJ_147);
              OBJ_143_OBJ_147.setBounds(275, 5, 60, 24);

              //---- WMAG ----
              WMAG.setComponentPopupMenu(BTD);
              WMAG.setToolTipText("Unit\u00e9 de stock");
              WMAG.setName("WMAG");
              VENTEContentContainer.add(WMAG);
              WMAG.setBounds(340, 5, 34, WMAG.getPreferredSize().height);

              //---- OBJ_143_OBJ_148 ----
              OBJ_143_OBJ_148.setText("Client");
              OBJ_143_OBJ_148.setName("OBJ_143_OBJ_148");
              VENTEContentContainer.add(OBJ_143_OBJ_148);
              OBJ_143_OBJ_148.setBounds(10, 37, 95, 24);

              //---- MALIV ----
              MALIV.setComponentPopupMenu(BTD);
              MALIV.setName("MALIV");
              VENTEContentContainer.add(MALIV);
              MALIV.setBounds(170, 35, 40, MALIV.getPreferredSize().height);

              //---- MACLI ----
              MACLI.setComponentPopupMenu(BTD);
              MACLI.setName("MACLI");
              VENTEContentContainer.add(MACLI);
              MACLI.setBounds(105, 35, 60, MACLI.getPreferredSize().height);

              //---- CLNOMR ----
              CLNOMR.setHorizontalAlignment(SwingConstants.LEADING);
              CLNOMR.setText("@CLNOMR@");
              CLNOMR.setName("CLNOMR");
              VENTEContentContainer.add(CLNOMR);
              CLNOMR.setBounds(215, 37, 160, CLNOMR.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < VENTEContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = VENTEContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = VENTEContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                VENTEContentContainer.setMinimumSize(preferredSize);
                VENTEContentContainer.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(VENTE, JLayeredPane.DEFAULT_LAYER);
            VENTE.setBounds(20, 290, 390, 99);

            //======== SAV ========
            {
              SAV.setTitle("S.A.V.");
              SAV.setBorder(new DropShadowBorder());
              SAV.setName("SAV");
              Container SAVContentContainer = SAV.getContentContainer();
              SAVContentContainer.setLayout(null);

              //---- OBJ_170_OBJ_170 ----
              OBJ_170_OBJ_170.setText("N\u00b0 de contrat");
              OBJ_170_OBJ_170.setName("OBJ_170_OBJ_170");
              SAVContentContainer.add(OBJ_170_OBJ_170);
              OBJ_170_OBJ_170.setBounds(15, 5, 80, 28);

              //---- CADEL ----
              CADEL.setHorizontalAlignment(SwingConstants.RIGHT);
              CADEL.setComponentPopupMenu(null);
              CADEL.setText("@CADEL@");
              CADEL.setName("CADEL");
              SAVContentContainer.add(CADEL);
              CADEL.setBounds(507, 38, 28, CADEL.getPreferredSize().height);

              //---- TCLIBR ----
              TCLIBR.setText("@TCLIBR@");
              TCLIBR.setName("TCLIBR");
              SAVContentContainer.add(TCLIBR);
              TCLIBR.setBounds(170, 37, 270, TCLIBR.getPreferredSize().height);

              //---- MANCE ----
              MANCE.setComponentPopupMenu(BTD);
              MANCE.setName("MANCE");
              SAVContentContainer.add(MANCE);
              MANCE.setBounds(100, 5, 60, MANCE.getPreferredSize().height);

              //---- MATCO ----
              MATCO.setComponentPopupMenu(BTD);
              MATCO.setName("MATCO");
              SAVContentContainer.add(MATCO);
              MATCO.setBounds(100, 35, 60, MATCO.getPreferredSize().height);

              //---- OBJ_170_OBJ_171 ----
              OBJ_170_OBJ_171.setText("Type");
              OBJ_170_OBJ_171.setName("OBJ_170_OBJ_171");
              SAVContentContainer.add(OBJ_170_OBJ_171);
              OBJ_170_OBJ_171.setBounds(15, 35, 80, 28);

              //---- MACGA ----
              MACGA.setComponentPopupMenu(BTD);
              MACGA.setName("MACGA");
              SAVContentContainer.add(MACGA);
              MACGA.setBounds(100, 65, 60, 28);

              //---- OBJ_170_OBJ_172 ----
              OBJ_170_OBJ_172.setText("Garantie");
              OBJ_170_OBJ_172.setName("OBJ_170_OBJ_172");
              SAVContentContainer.add(OBJ_170_OBJ_172);
              OBJ_170_OBJ_172.setBounds(15, 65, 80, 28);

              //---- OBJ_170_OBJ_173 ----
              OBJ_170_OBJ_173.setText("Dur\u00e9e");
              OBJ_170_OBJ_173.setName("OBJ_170_OBJ_173");
              SAVContentContainer.add(OBJ_170_OBJ_173);
              OBJ_170_OBJ_173.setBounds(170, 65, 45, 28);

              //---- MADUG ----
              MADUG.setComponentPopupMenu(BTD);
              MADUG.setName("MADUG");
              SAVContentContainer.add(MADUG);
              MADUG.setBounds(215, 65, 34, MADUG.getPreferredSize().height);

              //---- OBJ_170_OBJ_174 ----
              OBJ_170_OBJ_174.setText("du");
              OBJ_170_OBJ_174.setName("OBJ_170_OBJ_174");
              SAVContentContainer.add(OBJ_170_OBJ_174);
              OBJ_170_OBJ_174.setBounds(170, 95, 25, 28);

              //---- MADDGX ----
              MADDGX.setName("MADDGX");
              SAVContentContainer.add(MADDGX);
              MADDGX.setBounds(215, 95, 105, MADDGX.getPreferredSize().height);

              //---- MADFGX ----
              MADFGX.setName("MADFGX");
              SAVContentContainer.add(MADFGX);
              MADFGX.setBounds(335, 95, 105, MADFGX.getPreferredSize().height);

              //---- OBJ_170_OBJ_175 ----
              OBJ_170_OBJ_175.setText("au");
              OBJ_170_OBJ_175.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_170_OBJ_175.setName("OBJ_170_OBJ_175");
              SAVContentContainer.add(OBJ_170_OBJ_175);
              OBJ_170_OBJ_175.setBounds(310, 95, 25, 28);

              //---- OBJ_170_OBJ_176 ----
              OBJ_170_OBJ_176.setText("mois");
              OBJ_170_OBJ_176.setName("OBJ_170_OBJ_176");
              SAVContentContainer.add(OBJ_170_OBJ_176);
              OBJ_170_OBJ_176.setBounds(255, 65, 45, 28);

              //---- MAPRO ----
              MAPRO.setComponentPopupMenu(BTD);
              MAPRO.setName("MAPRO");
              SAVContentContainer.add(MAPRO);
              MAPRO.setBounds(335, 65, 24, MAPRO.getPreferredSize().height);
            }
            layeredPane1.add(SAV, JLayeredPane.DEFAULT_LAYER);
            SAV.setBounds(415, 190, 455, 160);

            //======== SUIVI ========
            {
              SUIVI.setTitle("Suivi");
              SUIVI.setBorder(new DropShadowBorder());
              SUIVI.setMinimumSize(new Dimension(280, 112));
              SUIVI.setName("SUIVI");
              Container SUIVIContentContainer = SUIVI.getContentContainer();
              SUIVIContentContainer.setLayout(null);

              //======== voir_stats ========
              {
                voir_stats.setBackground(new Color(239, 239, 222));
                voir_stats.setName("voir_stats");
                voir_stats.setLayout(null);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < voir_stats.getComponentCount(); i++) {
                    Rectangle bounds = voir_stats.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = voir_stats.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  voir_stats.setMinimumSize(preferredSize);
                  voir_stats.setPreferredSize(preferredSize);
                }
              }
              SUIVIContentContainer.add(voir_stats);
              voir_stats.setBounds(new Rectangle(new Point(0, 0), voir_stats.getPreferredSize()));

              //---- OBJ_170_OBJ_177 ----
              OBJ_170_OBJ_177.setText("Derni\u00e8re op\u00e9ration");
              OBJ_170_OBJ_177.setName("OBJ_170_OBJ_177");
              SUIVIContentContainer.add(OBJ_170_OBJ_177);
              OBJ_170_OBJ_177.setBounds(15, 5, 120, 28);

              //---- MITYI ----
              MITYI.setComponentPopupMenu(BTD);
              MITYI.setName("MITYI");
              SUIVIContentContainer.add(MITYI);
              MITYI.setBounds(170, 5, 60, MITYI.getPreferredSize().height);

              //---- MIINT ----
              MIINT.setComponentPopupMenu(BTD);
              MIINT.setName("MIINT");
              SUIVIContentContainer.add(MIINT);
              MIINT.setBounds(170, 35, 40, MIINT.getPreferredSize().height);

              //---- OBJ_170_OBJ_178 ----
              OBJ_170_OBJ_178.setText("Intervenant");
              OBJ_170_OBJ_178.setName("OBJ_170_OBJ_178");
              SUIVIContentContainer.add(OBJ_170_OBJ_178);
              OBJ_170_OBJ_178.setBounds(15, 35, 70, 28);

              //---- INLIBR ----
              INLIBR.setHorizontalAlignment(SwingConstants.LEADING);
              INLIBR.setText("@INLIBR@");
              INLIBR.setName("INLIBR");
              SUIVIContentContainer.add(INLIBR);
              INLIBR.setBounds(215, 37, 185, INLIBR.getPreferredSize().height);

              //---- OBJ_170_OBJ_179 ----
              OBJ_170_OBJ_179.setText("le");
              OBJ_170_OBJ_179.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_170_OBJ_179.setName("OBJ_170_OBJ_179");
              SUIVIContentContainer.add(OBJ_170_OBJ_179);
              OBJ_170_OBJ_179.setBounds(300, 5, 25, 28);

              //---- MIDINX ----
              MIDINX.setName("MIDINX");
              SUIVIContentContainer.add(MIDINX);
              MIDINX.setBounds(335, 5, 105, MIDINX.getPreferredSize().height);

              //---- OBJ_170_OBJ_180 ----
              OBJ_170_OBJ_180.setText("Prochaine visite");
              OBJ_170_OBJ_180.setName("OBJ_170_OBJ_180");
              SUIVIContentContainer.add(OBJ_170_OBJ_180);
              OBJ_170_OBJ_180.setBounds(15, 65, 120, 28);

              //---- MADP1X ----
              MADP1X.setName("MADP1X");
              SUIVIContentContainer.add(MADP1X);
              MADP1X.setBounds(170, 65, 105, MADP1X.getPreferredSize().height);

              //---- MADP2X ----
              MADP2X.setName("MADP2X");
              SUIVIContentContainer.add(MADP2X);
              MADP2X.setBounds(335, 65, 105, MADP2X.getPreferredSize().height);

              //---- OBJ_170_OBJ_181 ----
              OBJ_170_OBJ_181.setText("P\u00e9riodicit\u00e9");
              OBJ_170_OBJ_181.setName("OBJ_170_OBJ_181");
              SUIVIContentContainer.add(OBJ_170_OBJ_181);
              OBJ_170_OBJ_181.setBounds(15, 95, 70, 28);

              //---- OBJ_170_OBJ_182 ----
              OBJ_170_OBJ_182.setText("Dur\u00e9e");
              OBJ_170_OBJ_182.setName("OBJ_170_OBJ_182");
              SUIVIContentContainer.add(OBJ_170_OBJ_182);
              OBJ_170_OBJ_182.setBounds(285, 95, 50, 28);

              //---- MACPV ----
              MACPV.setComponentPopupMenu(BTD);
              MACPV.setName("MACPV");
              SUIVIContentContainer.add(MACPV);
              MACPV.setBounds(170, 95, 60, MACPV.getPreferredSize().height);

              //---- MADVI ----
              MADVI.setComponentPopupMenu(BTD);
              MADVI.setName("MADVI");
              SUIVIContentContainer.add(MADVI);
              MADVI.setBounds(335, 95, 44, MADVI.getPreferredSize().height);

              //---- OBJ_170_OBJ_183 ----
              OBJ_170_OBJ_183.setText("/");
              OBJ_170_OBJ_183.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_170_OBJ_183.setName("OBJ_170_OBJ_183");
              SUIVIContentContainer.add(OBJ_170_OBJ_183);
              OBJ_170_OBJ_183.setBounds(270, 65, 60, 28);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < SUIVIContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = SUIVIContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = SUIVIContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                SUIVIContentContainer.setMinimumSize(preferredSize);
                SUIVIContentContainer.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(SUIVI, JLayeredPane.DEFAULT_LAYER);
            SUIVI.setBounds(420, 350, 450, 160);

            //======== EXPLO ========
            {
              EXPLO.setTitle("Exploitation");
              EXPLO.setBorder(new DropShadowBorder());
              EXPLO.setName("EXPLO");
              Container EXPLOContentContainer = EXPLO.getContentContainer();
              EXPLOContentContainer.setLayout(null);

              //---- OBJ_170_OBJ_184 ----
              OBJ_170_OBJ_184.setText("Produits");
              OBJ_170_OBJ_184.setName("OBJ_170_OBJ_184");
              EXPLOContentContainer.add(OBJ_170_OBJ_184);
              OBJ_170_OBJ_184.setBounds(10, 5, 80, 28);

              //---- WPROD ----
              WPROD.setComponentPopupMenu(BTD);
              WPROD.setName("WPROD");
              EXPLOContentContainer.add(WPROD);
              WPROD.setBounds(105, 5, 69, WPROD.getPreferredSize().height);

              //---- WCHAR ----
              WCHAR.setComponentPopupMenu(BTD);
              WCHAR.setName("WCHAR");
              EXPLOContentContainer.add(WCHAR);
              WCHAR.setBounds(105, 33, 69, WCHAR.getPreferredSize().height);

              //---- OBJ_170_OBJ_185 ----
              OBJ_170_OBJ_185.setText("Charges");
              OBJ_170_OBJ_185.setName("OBJ_170_OBJ_185");
              EXPLOContentContainer.add(OBJ_170_OBJ_185);
              OBJ_170_OBJ_185.setBounds(10, 33, 80, 28);

              //---- OBJ_170_OBJ_186 ----
              OBJ_170_OBJ_186.setText("R\u00e9sultat");
              OBJ_170_OBJ_186.setName("OBJ_170_OBJ_186");
              EXPLOContentContainer.add(OBJ_170_OBJ_186);
              OBJ_170_OBJ_186.setBounds(10, 61, 80, 28);

              //---- WRESL ----
              WRESL.setComponentPopupMenu(BTD);
              WRESL.setName("WRESL");
              EXPLOContentContainer.add(WRESL);
              WRESL.setBounds(105, 61, 69, WRESL.getPreferredSize().height);
            }
            layeredPane1.add(EXPLO, JLayeredPane.DEFAULT_LAYER);
            EXPLO.setBounds(20, 390, 390, 120);

            //======== OBS ========
            {
              OBS.setTitle("Observations");
              OBS.setBorder(new DropShadowBorder());
              OBS.setName("OBS");
              Container OBSContentContainer = OBS.getContentContainer();
              OBSContentContainer.setLayout(null);

              //---- OB1R ----
              OB1R.setFont(OB1R.getFont().deriveFont(OB1R.getFont().getStyle() | Font.BOLD));
              OB1R.setComponentPopupMenu(BTD);
              OB1R.setName("OB1R");
              OBSContentContainer.add(OB1R);
              OB1R.setBounds(10, 5, 440, OB1R.getPreferredSize().height);

              //---- OBJ_170_OBJ_187 ----
              OBJ_170_OBJ_187.setText("Etat");
              OBJ_170_OBJ_187.setName("OBJ_170_OBJ_187");
              OBSContentContainer.add(OBJ_170_OBJ_187);
              OBJ_170_OBJ_187.setBounds(520, 5, 50, 28);

              //---- MAETA ----
              MAETA.setComponentPopupMenu(BTD);
              MAETA.setName("MAETA");
              OBSContentContainer.add(MAETA);
              MAETA.setBounds(570, 5, 24, MAETA.getPreferredSize().height);

              //---- MAETAL ----
              MAETAL.setHorizontalAlignment(SwingConstants.LEADING);
              MAETAL.setText("@MAETAL@");
              MAETAL.setName("MAETAL");
              OBSContentContainer.add(MAETAL);
              MAETAL.setBounds(610, 7, 185, MAETAL.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < OBSContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = OBSContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBSContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBSContentContainer.setMinimumSize(preferredSize);
                OBSContentContainer.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(OBS, JLayeredPane.DEFAULT_LAYER);
            OBS.setBounds(20, 515, 850, 70);

            //---- OBJ_126_OBJ_126 ----
            OBJ_126_OBJ_126.setText("Famille");
            OBJ_126_OBJ_126.setName("OBJ_126_OBJ_126");
            layeredPane1.add(OBJ_126_OBJ_126, JLayeredPane.DEFAULT_LAYER);
            OBJ_126_OBJ_126.setBounds(395, 150, 110, 28);

            //---- MAFAM ----
            MAFAM.setComponentPopupMenu(BTD);
            MAFAM.setName("MAFAM");
            layeredPane1.add(MAFAM, JLayeredPane.DEFAULT_LAYER);
            MAFAM.setBounds(505, 150, 40, MAFAM.getPreferredSize().height);

            //---- riZoneSortie2 ----
            riZoneSortie2.setText("@FALIB@");
            riZoneSortie2.setName("riZoneSortie2");
            layeredPane1.add(riZoneSortie2, JLayeredPane.DEFAULT_LAYER);
            riZoneSortie2.setBounds(550, 152, 310, riZoneSortie2.getPreferredSize().height);

            //---- OBJ_126_OBJ_127 ----
            OBJ_126_OBJ_127.setText("Mod\u00e8le ou version");
            OBJ_126_OBJ_127.setName("OBJ_126_OBJ_127");
            layeredPane1.add(OBJ_126_OBJ_127, JLayeredPane.DEFAULT_LAYER);
            OBJ_126_OBJ_127.setBounds(395, 90, 110, 28);

            //---- MAMOV ----
            MAMOV.setComponentPopupMenu(BTD);
            MAMOV.setName("MAMOV");
            layeredPane1.add(MAMOV, JLayeredPane.DEFAULT_LAYER);
            MAMOV.setBounds(505, 90, 90, MAMOV.getPreferredSize().height);

            //---- OBJ_126_OBJ_128 ----
            OBJ_126_OBJ_128.setText("Rattachement");
            OBJ_126_OBJ_128.setName("OBJ_126_OBJ_128");
            layeredPane1.add(OBJ_126_OBJ_128, JLayeredPane.DEFAULT_LAYER);
            OBJ_126_OBJ_128.setBounds(395, 120, 110, 28);

            //---- MARAT ----
            MARAT.setComponentPopupMenu(BTD);
            MARAT.setName("MARAT");
            layeredPane1.add(MARAT, JLayeredPane.DEFAULT_LAYER);
            MARAT.setBounds(505, 120, 90, MARAT.getPreferredSize().height);

            //---- OBJ_126_OBJ_129 ----
            OBJ_126_OBJ_129.setText("Niveau");
            OBJ_126_OBJ_129.setName("OBJ_126_OBJ_129");
            layeredPane1.add(OBJ_126_OBJ_129, JLayeredPane.DEFAULT_LAYER);
            OBJ_126_OBJ_129.setBounds(620, 120, 65, 28);

            //---- MARAN ----
            MARAN.setComponentPopupMenu(BTD);
            MARAN.setName("MARAN");
            layeredPane1.add(MARAN, JLayeredPane.DEFAULT_LAYER);
            MARAN.setBounds(685, 120, 34, MARAN.getPreferredSize().height);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("Zones personnalis\u00e9es"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- WTI1 ----
              WTI1.setText("@WTI1@");
              WTI1.setName("WTI1");
              panel1.add(WTI1);
              WTI1.setBounds(20, 35, 35, WTI1.getPreferredSize().height);

              //---- WTI2 ----
              WTI2.setText("@WTI2@");
              WTI2.setName("WTI2");
              panel1.add(WTI2);
              WTI2.setBounds(110, 35, 35, WTI2.getPreferredSize().height);

              //---- WTI3 ----
              WTI3.setText("@WTI3@");
              WTI3.setName("WTI3");
              panel1.add(WTI3);
              WTI3.setBounds(200, 35, 35, WTI3.getPreferredSize().height);

              //---- WTI4 ----
              WTI4.setText("@WTI4@");
              WTI4.setName("WTI4");
              panel1.add(WTI4);
              WTI4.setBounds(290, 35, 35, WTI4.getPreferredSize().height);

              //---- WTI5 ----
              WTI5.setText("@WTI5@");
              WTI5.setName("WTI5");
              panel1.add(WTI5);
              WTI5.setBounds(380, 35, 35, WTI5.getPreferredSize().height);

              //---- MAZP1 ----
              MAZP1.setComponentPopupMenu(BTD);
              MAZP1.setName("MAZP1");
              panel1.add(MAZP1);
              MAZP1.setBounds(60, 33, 34, MAZP1.getPreferredSize().height);

              //---- MAZP2 ----
              MAZP2.setComponentPopupMenu(BTD);
              MAZP2.setName("MAZP2");
              panel1.add(MAZP2);
              MAZP2.setBounds(150, 33, 34, MAZP2.getPreferredSize().height);

              //---- MAZP3 ----
              MAZP3.setComponentPopupMenu(BTD);
              MAZP3.setName("MAZP3");
              panel1.add(MAZP3);
              MAZP3.setBounds(240, 33, 34, MAZP3.getPreferredSize().height);

              //---- MAZP4 ----
              MAZP4.setComponentPopupMenu(BTD);
              MAZP4.setName("MAZP4");
              panel1.add(MAZP4);
              MAZP4.setBounds(330, 33, 34, MAZP4.getPreferredSize().height);

              //---- MAZP5 ----
              MAZP5.setComponentPopupMenu(BTD);
              MAZP5.setName("MAZP5");
              panel1.add(MAZP5);
              MAZP5.setBounds(420, 33, 34, MAZP5.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(panel1, JLayeredPane.DEFAULT_LAYER);
            panel1.setBounds(395, 5, 475, 80);

            //---- MAART ----
            MAART.setFont(MAART.getFont().deriveFont(MAART.getFont().getStyle() | Font.BOLD));
            MAART.setComponentPopupMenu(BTD);
            MAART.setName("MAART");
            layeredPane1.add(MAART, JLayeredPane.DEFAULT_LAYER);
            MAART.setBounds(145, 25, 210, MAART.getPreferredSize().height);

            //---- OBJ_73_OBJ_73 ----
            OBJ_73_OBJ_73.setText("Article");
            OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");
            layeredPane1.add(OBJ_73_OBJ_73, JLayeredPane.DEFAULT_LAYER);
            OBJ_73_OBJ_73.setBounds(45, 25, 60, 28);

            //---- btnChangeDesignation ----
            btnChangeDesignation.setName("btnChangeDesignation");
            btnChangeDesignation.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                changeDesignationActionPerformed(e);
              }
            });
            layeredPane1.add(btnChangeDesignation, JLayeredPane.DEFAULT_LAYER);
            btnChangeDesignation.setBounds(100, 25, 25, 28);

            //---- A1LIB ----
            A1LIB.setFont(A1LIB.getFont().deriveFont(A1LIB.getFont().getStyle() | Font.BOLD));
            A1LIB.setComponentPopupMenu(BTD);
            A1LIB.setName("A1LIB");
            layeredPane1.add(A1LIB, JLayeredPane.DEFAULT_LAYER);
            A1LIB.setBounds(45, 60, 310, A1LIB.getPreferredSize().height);

            //---- A1LB1 ----
            A1LB1.setComponentPopupMenu(BTD);
            A1LB1.setName("A1LB1");
            layeredPane1.add(A1LB1, JLayeredPane.DEFAULT_LAYER);
            A1LB1.setBounds(45, 90, 310, A1LB1.getPreferredSize().height);

            //---- A1LB2 ----
            A1LB2.setComponentPopupMenu(BTD);
            A1LB2.setName("A1LB2");
            layeredPane1.add(A1LB2, JLayeredPane.DEFAULT_LAYER);
            A1LB2.setBounds(45, 120, 310, A1LB2.getPreferredSize().height);

            //---- A1LB3 ----
            A1LB3.setComponentPopupMenu(BTD);
            A1LB3.setName("A1LB3");
            layeredPane1.add(A1LB3, JLayeredPane.DEFAULT_LAYER);
            A1LB3.setBounds(45, 150, 310, A1LB3.getPreferredSize().height);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addComponent(layeredPane1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addComponent(layeredPane1, GroupLayout.DEFAULT_SIZE, 593, Short.MAX_VALUE)
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //---- bt_achat ----
    bt_achat.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_achat.setToolTipText("Stock");
    bt_achat.setName("bt_achat");
    bt_achat.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_achatActionPerformed(e);
      }
    });

    //---- bt_vente ----
    bt_vente.setPreferredSize(new Dimension(18, 18));
    bt_vente.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_vente.setIconTextGap(5);
    bt_vente.setToolTipText("Tarif");
    bt_vente.setName("bt_vente");
    bt_vente.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_venteActionPerformed(e);
      }
    });

    //---- bt_explo ----
    bt_explo.setPreferredSize(new Dimension(18, 18));
    bt_explo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_explo.setToolTipText("Conditions d'achat");
    bt_explo.setName("bt_explo");
    bt_explo.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_exploActionPerformed(e);
      }
    });

    //---- bt_sav ----
    bt_sav.setPreferredSize(new Dimension(18, 18));
    bt_sav.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_sav.setToolTipText("Statistiques");
    bt_sav.setName("bt_sav");
    bt_sav.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_savActionPerformed(e);
      }
    });

    //---- bt_suivi ----
    bt_suivi.setPreferredSize(new Dimension(18, 18));
    bt_suivi.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_suivi.setToolTipText("Chiffrage");
    bt_suivi.setName("bt_suivi");
    bt_suivi.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_suiviActionPerformed(e);
      }
    });

    //---- bt_obs ----
    bt_obs.setPreferredSize(new Dimension(18, 18));
    bt_obs.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_obs.setToolTipText("Divers");
    bt_obs.setName("bt_obs");
    bt_obs.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_obsActionPerformed(e);
      }
    });

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_22 ----
      OBJ_22.setText("Choix possibles");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_23);

      //---- OBJ_24 ----
      OBJ_24.setText("Aide en ligne");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_24);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_66;
  private XRiTextField INDETB;
  private JLabel OBJ_68;
  private XRiTextField INDNUS;
  private XRiTextField INDMAR;
  private JLabel OBJ_69;
  private RiZoneSortie TYPMAT;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLayeredPane layeredPane1;
  private JXTitledPanel ACHAT;
  private JLabel OBJ_143_OBJ_144;
  private XRiTextField MAMAG;
  private RiZoneSortie FRNOMR;
  private JLabel OBJ_143_OBJ_143;
  private JLabel OBJ_143_OBJ_145;
  private XRiTextField MAFRS;
  private XRiTextField MAFRC;
  private XRiCalendrier MADBAX;
  private JXTitledPanel VENTE;
  private XRiCalendrier MADBVX;
  private JLabel OBJ_143_OBJ_146;
  private JLabel OBJ_143_OBJ_147;
  private XRiTextField WMAG;
  private JLabel OBJ_143_OBJ_148;
  private XRiTextField MALIV;
  private XRiTextField MACLI;
  private RiZoneSortie CLNOMR;
  private JXTitledPanel SAV;
  private JLabel OBJ_170_OBJ_170;
  private RiZoneSortie CADEL;
  private RiZoneSortie TCLIBR;
  private XRiTextField MANCE;
  private XRiTextField MATCO;
  private JLabel OBJ_170_OBJ_171;
  private XRiTextField MACGA;
  private JLabel OBJ_170_OBJ_172;
  private JLabel OBJ_170_OBJ_173;
  private XRiTextField MADUG;
  private JLabel OBJ_170_OBJ_174;
  private XRiCalendrier MADDGX;
  private XRiCalendrier MADFGX;
  private JLabel OBJ_170_OBJ_175;
  private JLabel OBJ_170_OBJ_176;
  private XRiTextField MAPRO;
  private JXTitledPanel SUIVI;
  private JPanel voir_stats;
  private JLabel OBJ_170_OBJ_177;
  private XRiTextField MITYI;
  private XRiTextField MIINT;
  private JLabel OBJ_170_OBJ_178;
  private RiZoneSortie INLIBR;
  private JLabel OBJ_170_OBJ_179;
  private XRiCalendrier MIDINX;
  private JLabel OBJ_170_OBJ_180;
  private XRiCalendrier MADP1X;
  private XRiCalendrier MADP2X;
  private JLabel OBJ_170_OBJ_181;
  private JLabel OBJ_170_OBJ_182;
  private XRiTextField MACPV;
  private XRiTextField MADVI;
  private JLabel OBJ_170_OBJ_183;
  private JXTitledPanel EXPLO;
  private JLabel OBJ_170_OBJ_184;
  private XRiTextField WPROD;
  private XRiTextField WCHAR;
  private JLabel OBJ_170_OBJ_185;
  private JLabel OBJ_170_OBJ_186;
  private XRiTextField WRESL;
  private JXTitledPanel OBS;
  private XRiTextField OB1R;
  private JLabel OBJ_170_OBJ_187;
  private XRiTextField MAETA;
  private RiZoneSortie MAETAL;
  private JLabel OBJ_126_OBJ_126;
  private XRiTextField MAFAM;
  private RiZoneSortie riZoneSortie2;
  private JLabel OBJ_126_OBJ_127;
  private XRiTextField MAMOV;
  private JLabel OBJ_126_OBJ_128;
  private XRiTextField MARAT;
  private JLabel OBJ_126_OBJ_129;
  private XRiTextField MARAN;
  private JPanel panel1;
  private RiZoneSortie WTI1;
  private RiZoneSortie WTI2;
  private RiZoneSortie WTI3;
  private RiZoneSortie WTI4;
  private RiZoneSortie WTI5;
  private XRiTextField MAZP1;
  private XRiTextField MAZP2;
  private XRiTextField MAZP3;
  private XRiTextField MAZP4;
  private XRiTextField MAZP5;
  private XRiTextField MAART;
  private JLabel OBJ_73_OBJ_73;
  private SNBoutonDetail btnChangeDesignation;
  private XRiTextField A1LIB;
  private XRiTextField A1LB1;
  private XRiTextField A1LB2;
  private XRiTextField A1LB3;
  private SNBoutonDetail bt_achat;
  private SNBoutonDetail bt_vente;
  private SNBoutonDetail bt_explo;
  private SNBoutonDetail bt_sav;
  private SNBoutonDetail bt_suivi;
  private SNBoutonDetail bt_obs;
  private JPopupMenu BTD;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_21;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
