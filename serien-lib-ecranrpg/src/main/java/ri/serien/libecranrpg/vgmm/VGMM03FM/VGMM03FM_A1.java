
package ri.serien.libecranrpg.vgmm.VGMM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGMM03FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  public VGMM03FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDX1.setValeurs("1", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX6.setValeurs("6", "RB");
    TIDX7.setValeurs("7", "RB");
    TITX10.setValeurs("A", "RB");
    SCAN1.setValeursSelection("1", "");
    WEXT.setValeursSelection("X", "");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    label1.setVisible(lexique.isTrue("(73) AND (N74)"));
    label2.setVisible(lexique.isTrue("(N73) AND (74)"));
    label3.setVisible(lexique.isTrue("(73) AND (74)"));
    barre_dup.setVisible(lexique.isTrue("56"));
    
    if (lexique.isTrue("57")) {
      p_bpresentation.setText("Fiche matériel pour affectation de numéro de contrat");
    }
    else {
      p_bpresentation.setText("Fiche matériel");
    }
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(12, 54);
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_66 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_68 = new JLabel();
    INDNUS = new XRiTextField();
    INDMAR = new XRiTextField();
    OBJ_69 = new JLabel();
    p_tete_droite = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    barre_dup = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_67 = new JLabel();
    IN3ETB = new XRiTextField();
    OBJ_70 = new JLabel();
    IN3NUS = new XRiTextField();
    IN3MAR = new XRiTextField();
    OBJ_71 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX1 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    ARG1 = new XRiTextField();
    ARG5 = new XRiTextField();
    TIDX2 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    ARG3 = new XRiTextField();
    TIDX4 = new XRiRadioButton();
    ARG4 = new XRiTextField();
    TIDX5 = new XRiRadioButton();
    ARG6 = new XRiTextField();
    TITX10 = new XRiRadioButton();
    ARG101 = new XRiTextField();
    TIDX7 = new XRiRadioButton();
    ARG7 = new XRiTextField();
    SCAN1 = new XRiCheckBox();
    ARG2A = new XRiTextField();
    ARG2B = new XRiTextField();
    WEXT = new XRiCheckBox();
    ARG102 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Fiche mat\u00e9riel");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
          
          // ---- OBJ_66 ----
          OBJ_66.setText("Etablissement");
          OBJ_66.setName("OBJ_66");
          p_tete_gauche.add(OBJ_66);
          OBJ_66.setBounds(5, 5, 95, 20);
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(115, 1, 40, INDETB.getPreferredSize().height);
          
          // ---- OBJ_68 ----
          OBJ_68.setText("Num\u00e9ro de s\u00e9rie");
          OBJ_68.setName("OBJ_68");
          p_tete_gauche.add(OBJ_68);
          OBJ_68.setBounds(200, 5, 105, 20);
          
          // ---- INDNUS ----
          INDNUS.setName("INDNUS");
          p_tete_gauche.add(INDNUS);
          INDNUS.setBounds(310, 1, 310, INDNUS.getPreferredSize().height);
          
          // ---- INDMAR ----
          INDMAR.setName("INDMAR");
          p_tete_gauche.add(INDMAR);
          INDMAR.setBounds(705, 1, 80, INDMAR.getPreferredSize().height);
          
          // ---- OBJ_69 ----
          OBJ_69.setText("Marque");
          OBJ_69.setName("OBJ_69");
          p_tete_gauche.add(OBJ_69);
          OBJ_69.setBounds(645, 5, 55, 20);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- label1 ----
          label1.setText("Achet\u00e9");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 1f));
          label1.setName("label1");
          p_tete_droite.add(label1);
          
          // ---- label2 ----
          label2.setText("Vendu");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD, label2.getFont().getSize() + 1f));
          label2.setName("label2");
          p_tete_droite.add(label2);
          
          // ---- label3 ----
          label3.setText("Achet\u00e9/vendu");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD, label3.getFont().getSize() + 1f));
          label3.setName("label3");
          p_tete_droite.add(label3);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
      
      // ======== barre_dup ========
      {
        barre_dup.setMinimumSize(new Dimension(111, 34));
        barre_dup.setPreferredSize(new Dimension(111, 34));
        barre_dup.setName("barre_dup");
        
        // ======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche2.setName("p_tete_gauche2");
          p_tete_gauche2.setLayout(null);
          
          // ---- OBJ_67 ----
          OBJ_67.setText("Par duplication de");
          OBJ_67.setName("OBJ_67");
          p_tete_gauche2.add(OBJ_67);
          OBJ_67.setBounds(5, 5, 110, 20);
          
          // ---- IN3ETB ----
          IN3ETB.setComponentPopupMenu(BTD);
          IN3ETB.setName("IN3ETB");
          p_tete_gauche2.add(IN3ETB);
          IN3ETB.setBounds(115, 1, 40, IN3ETB.getPreferredSize().height);
          
          // ---- OBJ_70 ----
          OBJ_70.setText("Num\u00e9ro de s\u00e9rie");
          OBJ_70.setName("OBJ_70");
          p_tete_gauche2.add(OBJ_70);
          OBJ_70.setBounds(200, 5, 105, 20);
          
          // ---- IN3NUS ----
          IN3NUS.setName("IN3NUS");
          p_tete_gauche2.add(IN3NUS);
          IN3NUS.setBounds(310, 1, 310, IN3NUS.getPreferredSize().height);
          
          // ---- IN3MAR ----
          IN3MAR.setName("IN3MAR");
          p_tete_gauche2.add(IN3MAR);
          IN3MAR.setBounds(705, 1, 80, IN3MAR.getPreferredSize().height);
          
          // ---- OBJ_71 ----
          OBJ_71.setText("Marque");
          OBJ_71.setName("OBJ_71");
          p_tete_gauche2.add(OBJ_71);
          OBJ_71.setBounds(645, 5, 55, 20);
        }
        barre_dup.add(p_tete_gauche2);
      }
      p_nord.add(barre_dup);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");
            
            // ---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);
          
          // ======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");
            
            // ---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);
          
          // ======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");
            
            // ---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);
          
          // ======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");
            
            // ---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);
          
          // ======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");
            
            // ---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
          
          // ======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");
            
            // ---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);
          
          // ======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");
            
            // ---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);
          
          // ======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");
            
            // ---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);
          
          // ======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");
            
            // ---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
          
          // ======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");
            
            // ---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);
          
          // ======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");
            
            // ---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Autres crit\u00e8res");
            riSousMenu_bt6.setToolTipText("Autres crit\u00e8res de recherche");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(680, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(680, 380));
          p_contenu.setName("p_contenu");
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- TIDX1 ----
            TIDX1.setText("Num\u00e9ro de s\u00e9rie");
            TIDX1.setToolTipText("Tri\u00e9 par");
            TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX1.setName("TIDX1");
            panel1.add(TIDX1);
            TIDX1.setBounds(40, 45, 200, 25);
            
            // ---- TIDX6 ----
            TIDX6.setText("Num\u00e9ro de facture");
            TIDX6.setToolTipText("Tri\u00e9 par");
            TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX6.setName("TIDX6");
            panel1.add(TIDX6);
            TIDX6.setBounds(40, 230, 200, 25);
            
            // ---- ARG1 ----
            ARG1.setComponentPopupMenu(BTD);
            ARG1.setName("ARG1");
            panel1.add(ARG1);
            ARG1.setBounds(245, 43, 160, ARG1.getPreferredSize().height);
            
            // ---- ARG5 ----
            ARG5.setComponentPopupMenu(BTD);
            ARG5.setName("ARG5");
            panel1.add(ARG5);
            ARG5.setBounds(245, 191, 70, ARG5.getPreferredSize().height);
            
            // ---- TIDX2 ----
            TIDX2.setText("Num\u00e9ro client");
            TIDX2.setToolTipText("Tri\u00e9 par");
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setName("TIDX2");
            panel1.add(TIDX2);
            TIDX2.setBounds(40, 82, 200, 25);
            
            // ---- TIDX3 ----
            TIDX3.setText("Article");
            TIDX3.setToolTipText("Tri\u00e9 par");
            TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX3.setName("TIDX3");
            panel1.add(TIDX3);
            TIDX3.setBounds(40, 119, 200, 25);
            
            // ---- ARG3 ----
            ARG3.setComponentPopupMenu(BTD);
            ARG3.setName("ARG3");
            panel1.add(ARG3);
            ARG3.setBounds(245, 117, 160, ARG3.getPreferredSize().height);
            
            // ---- TIDX4 ----
            TIDX4.setText("Famille d'articles");
            TIDX4.setToolTipText("Tri\u00e9 par");
            TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX4.setName("TIDX4");
            panel1.add(TIDX4);
            TIDX4.setBounds(40, 156, 200, 25);
            
            // ---- ARG4 ----
            ARG4.setComponentPopupMenu(BTD);
            ARG4.setName("ARG4");
            panel1.add(ARG4);
            ARG4.setBounds(245, 154, 40, ARG4.getPreferredSize().height);
            
            // ---- TIDX5 ----
            TIDX5.setText("Num\u00e9ro de contrat");
            TIDX5.setToolTipText("Tri\u00e9 par");
            TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX5.setName("TIDX5");
            panel1.add(TIDX5);
            TIDX5.setBounds(40, 193, 200, 25);
            
            // ---- ARG6 ----
            ARG6.setComponentPopupMenu(BTD);
            ARG6.setName("ARG6");
            panel1.add(ARG6);
            ARG6.setBounds(245, 228, 80, ARG6.getPreferredSize().height);
            
            // ---- TITX10 ----
            TITX10.setText("Bon de vente");
            TITX10.setToolTipText("Tri\u00e9 par");
            TITX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TITX10.setName("TITX10");
            panel1.add(TITX10);
            TITX10.setBounds(40, 267, 200, 25);
            
            // ---- ARG101 ----
            ARG101.setComponentPopupMenu(BTD);
            ARG101.setName("ARG101");
            panel1.add(ARG101);
            ARG101.setBounds(245, 265, 70, ARG101.getPreferredSize().height);
            
            // ---- TIDX7 ----
            TIDX7.setText("Rattachement");
            TIDX7.setToolTipText("Tri\u00e9 par");
            TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX7.setName("TIDX7");
            panel1.add(TIDX7);
            TIDX7.setBounds(40, 304, 200, 25);
            
            // ---- ARG7 ----
            ARG7.setComponentPopupMenu(BTD);
            ARG7.setName("ARG7");
            panel1.add(ARG7);
            ARG7.setBounds(245, 302, 90, ARG7.getPreferredSize().height);
            
            // ---- SCAN1 ----
            SCAN1.setText("scan");
            SCAN1.setName("SCAN1");
            panel1.add(SCAN1);
            SCAN1.setBounds(415, 48, 65, SCAN1.getPreferredSize().height);
            
            // ---- ARG2A ----
            ARG2A.setComponentPopupMenu(BTD);
            ARG2A.setName("ARG2A");
            panel1.add(ARG2A);
            ARG2A.setBounds(245, 80, 60, ARG2A.getPreferredSize().height);
            
            // ---- ARG2B ----
            ARG2B.setComponentPopupMenu(BTD);
            ARG2B.setName("ARG2B");
            panel1.add(ARG2B);
            ARG2B.setBounds(310, 80, 40, ARG2B.getPreferredSize().height);
            
            // ---- WEXT ----
            WEXT.setText("Extension");
            WEXT.setName("WEXT");
            panel1.add(WEXT);
            WEXT.setBounds(310, 160, 145, WEXT.getPreferredSize().height);
            
            // ---- ARG102 ----
            ARG102.setComponentPopupMenu(BTD);
            ARG102.setName("ARG102");
            panel1.add(ARG102);
            ARG102.setBounds(320, 265, 24, ARG102.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 654, Short.MAX_VALUE).addContainerGap()));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE).addContainerGap()));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    
    // ---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TITX10);
    RB_GRP.add(TIDX7);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_66;
  private XRiTextField INDETB;
  private JLabel OBJ_68;
  private XRiTextField INDNUS;
  private XRiTextField INDMAR;
  private JLabel OBJ_69;
  private JPanel p_tete_droite;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JMenuBar barre_dup;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_67;
  private XRiTextField IN3ETB;
  private JLabel OBJ_70;
  private XRiTextField IN3NUS;
  private XRiTextField IN3MAR;
  private JLabel OBJ_71;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX6;
  private XRiTextField ARG1;
  private XRiTextField ARG5;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG3;
  private XRiRadioButton TIDX4;
  private XRiTextField ARG4;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG6;
  private XRiRadioButton TITX10;
  private XRiTextField ARG101;
  private XRiRadioButton TIDX7;
  private XRiTextField ARG7;
  private XRiCheckBox SCAN1;
  private XRiTextField ARG2A;
  private XRiTextField ARG2B;
  private XRiCheckBox WEXT;
  private XRiTextField ARG102;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
