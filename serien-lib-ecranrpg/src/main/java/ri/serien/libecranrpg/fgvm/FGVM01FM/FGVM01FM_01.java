
package ri.serien.libecranrpg.fgvm.FGVM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;

/**
 * @author Stéphane Vénéri
 */
public class FGVM01FM_01 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public FGVM01FM_01(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB@")).trim());
    OBJ_12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WAS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    
    // Titre
    // setTitle(???);
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WREP", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WREP", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    OBJ_14 = new JLabel();
    OBJ_11 = new JLabel();
    OBJ_13 = new JLabel();
    OBJ_12 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(800, 200));
    setPreferredSize(new Dimension(800, 200));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Oui");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Non");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- OBJ_14 ----
        OBJ_14.setText("D\u00e9sirez vous mettre \u00e0 jour les plafonds des fiches clients correspondantes ?");
        OBJ_14.setFont(OBJ_14.getFont().deriveFont(OBJ_14.getFont().getSize() + 3f));
        OBJ_14.setName("OBJ_14");
        p_contenu.add(OBJ_14);
        OBJ_14.setBounds(70, 135, 510, 20);

        //---- OBJ_11 ----
        OBJ_11.setText("Vous venez de modifier les plafonds de couverture de l'assurance :");
        OBJ_11.setFont(OBJ_11.getFont().deriveFont(OBJ_11.getFont().getSize() + 3f));
        OBJ_11.setName("OBJ_11");
        p_contenu.add(OBJ_11);
        OBJ_11.setBounds(70, 35, 465, 20);

        //---- OBJ_13 ----
        OBJ_13.setText("@WLIB@");
        OBJ_13.setFont(OBJ_13.getFont().deriveFont(OBJ_13.getFont().getStyle() | Font.BOLD, OBJ_13.getFont().getSize() + 3f));
        OBJ_13.setName("OBJ_13");
        p_contenu.add(OBJ_13);
        OBJ_13.setBounds(210, 85, 345, 20);

        //---- OBJ_12 ----
        OBJ_12.setText("@WAS@");
        OBJ_12.setFont(OBJ_12.getFont().deriveFont(OBJ_12.getFont().getStyle() | Font.BOLD, OBJ_12.getFont().getSize() + 3f));
        OBJ_12.setName("OBJ_12");
        p_contenu.add(OBJ_12);
        OBJ_12.setBounds(70, 85, 120, 20);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JLabel OBJ_14;
  private JLabel OBJ_11;
  private JLabel OBJ_13;
  private JLabel OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
