
package ri.serien.libecranrpg.vgvx.VGVX60FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;

/**
 * @author Stéphane Vénéri
 */
public class VGVX60FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX60FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1ART@")).trim());
    OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MEGBAR@")).trim());
    OBJ_17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTX01@")).trim());
    OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTX02@")).trim());
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTX03@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTX08@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQTSX@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    QTRX02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTRX02@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    panel2.setVisible(!lexique.HostFieldGetData("QTRX02").trim().equalsIgnoreCase(""));
    
    
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des affectations"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_28 = new RiZoneSortie();
    OBJ_27 = new JLabel();
    OBJ_16 = new JLabel();
    OBJ_18 = new JLabel();
    OBJ_20 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_17 = new RiZoneSortie();
    OBJ_19 = new RiZoneSortie();
    OBJ_21 = new RiZoneSortie();
    OBJ_23 = new RiZoneSortie();
    OBJ_25 = new RiZoneSortie();
    DLX02 = new XRiCalendrier();
    DLX03 = new XRiCalendrier();
    OBJ_29 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_26 = new RiZoneSortie();
    panel2 = new JPanel();
    QTRX02 = new RiZoneSortie();
    OBJ_30 = new JLabel();
    DRX02 = new XRiCalendrier();
    OBJ_31 = new JLabel();
    OBJ_32 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(850, 260));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- OBJ_28 ----
          OBJ_28.setText("@A1LIB@");
          OBJ_28.setName("OBJ_28");

          //---- OBJ_27 ----
          OBJ_27.setText("@L1ART@");
          OBJ_27.setName("OBJ_27");

          //---- OBJ_16 ----
          OBJ_16.setText("Affectation sur stock");
          OBJ_16.setName("OBJ_16");

          //---- OBJ_18 ----
          OBJ_18.setText("Affectation sur attendu");
          OBJ_18.setName("OBJ_18");

          //---- OBJ_20 ----
          OBJ_20.setText("@MEGBAR@");
          OBJ_20.setName("OBJ_20");

          //---- OBJ_22 ----
          OBJ_22.setText("Non affect\u00e9e");
          OBJ_22.setName("OBJ_22");

          //---- OBJ_24 ----
          OBJ_24.setText("Quantit\u00e9 command\u00e9e");
          OBJ_24.setName("OBJ_24");

          //---- OBJ_17 ----
          OBJ_17.setText("@QTX01@");
          OBJ_17.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_17.setName("OBJ_17");

          //---- OBJ_19 ----
          OBJ_19.setText("@QTX02@");
          OBJ_19.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_19.setName("OBJ_19");

          //---- OBJ_21 ----
          OBJ_21.setText("@QTX03@");
          OBJ_21.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_21.setName("OBJ_21");

          //---- OBJ_23 ----
          OBJ_23.setText("@QTX08@");
          OBJ_23.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_23.setName("OBJ_23");

          //---- OBJ_25 ----
          OBJ_25.setText("@WQTSX@");
          OBJ_25.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_25.setName("OBJ_25");

          //---- DLX02 ----
          DLX02.setName("DLX02");

          //---- DLX03 ----
          DLX03.setName("DLX03");

          //---- OBJ_29 ----
          OBJ_29.setText("Le");
          OBJ_29.setName("OBJ_29");

          //---- OBJ_34 ----
          OBJ_34.setText("Le");
          OBJ_34.setName("OBJ_34");

          //---- OBJ_26 ----
          OBJ_26.setText("@A1UNS@");
          OBJ_26.setName("OBJ_26");

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- QTRX02 ----
            QTRX02.setText("@QTRX02@");
            QTRX02.setHorizontalAlignment(SwingConstants.RIGHT);
            QTRX02.setName("QTRX02");
            panel2.add(QTRX02);
            QTRX02.setBounds(20, 5, 120, 25);

            //---- OBJ_30 ----
            OBJ_30.setText("re\u00e7u le");
            OBJ_30.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_30.setName("OBJ_30");
            panel2.add(OBJ_30);
            OBJ_30.setBounds(145, 7, 55, 20);

            //---- DRX02 ----
            DRX02.setName("DRX02");
            panel2.add(DRX02);
            DRX02.setBounds(205, 2, 110, 30);

            //---- OBJ_31 ----
            OBJ_31.setText("(");
            OBJ_31.setFont(OBJ_31.getFont().deriveFont(OBJ_31.getFont().getSize() + 3f));
            OBJ_31.setName("OBJ_31");
            panel2.add(OBJ_31);
            OBJ_31.setBounds(5, 7, 15, 20);

            //---- OBJ_32 ----
            OBJ_32.setText(")");
            OBJ_32.setFont(OBJ_32.getFont().deriveFont(OBJ_32.getFont().getSize() + 3f));
            OBJ_32.setName("OBJ_32");
            panel2.add(OBJ_32);
            OBJ_32.setBounds(320, 7, 15, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(OBJ_16, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)
                    .addComponent(OBJ_17, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)
                    .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                    .addGap(24, 24, 24)
                    .addComponent(DLX02, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)
                    .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                    .addGap(24, 24, 24)
                    .addComponent(DLX03, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)
                    .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 152, GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)
                    .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(7, 7, 7)
                    .addComponent(OBJ_16, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_17, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DLX02, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DLX03, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 660, 240);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie OBJ_28;
  private JLabel OBJ_27;
  private JLabel OBJ_16;
  private JLabel OBJ_18;
  private JLabel OBJ_20;
  private JLabel OBJ_22;
  private JLabel OBJ_24;
  private RiZoneSortie OBJ_17;
  private RiZoneSortie OBJ_19;
  private RiZoneSortie OBJ_21;
  private RiZoneSortie OBJ_23;
  private RiZoneSortie OBJ_25;
  private XRiCalendrier DLX02;
  private XRiCalendrier DLX03;
  private JLabel OBJ_29;
  private JLabel OBJ_34;
  private RiZoneSortie OBJ_26;
  private JPanel panel2;
  private RiZoneSortie QTRX02;
  private JLabel OBJ_30;
  private XRiCalendrier DRX02;
  private JLabel OBJ_31;
  private JLabel OBJ_32;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
