
package ri.serien.libecranrpg.vgvx.VGVX14FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.article.CritereArticle;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleStock;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX14FX_C1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WPT01_Top = { "WPT01", "WPT02", "WPT03", "WPT04", "WPT05", "WPT06", "WPT07", "WPT08", "WPT09", "WPT10", "WPT11",
      "WPT12", "WPT13", "WPT14", "WPT15", };
  private String[] _WPT01_Title = { "HLD01", };
  private String[][] _WPT01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WPT01_Width = { 37, };
  private String[] listeCellules =
      { "LD01", "LD02", "LD03", "LD04", "LD05", "LD06", "LD07", "LD08", "LD09", "LD10", "LD11", "LD12", "LD13", "LD14", "LD15" };
  private Color[][] couleurLigne = new Color[listeCellules.length][1];
  private CritereArticle critereArticle = new CritereArticle();
  
  // Boutons
  private static final String BOUTON_SELECTION_CODE_BARRE = "Sélectionner par code barres";
  private static final String BOUTON_PLEINE_PAGE = "Saisir en pleine page";
  private static final String BOUTON_CLASSIQUE = "Saisir par article";
  private static final String BOUTON_SAISIE_ARTICLE = "Sélectionner par article";
  private static final String BOUTON_MODIFIER_QUANTITE = "Modifier quantités";
  private static final String BOUTON_MODIFIER_VUE_LISTE = "Modifier vue liste";
  
  public VGVX14FX_C1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WPT01.setAspectTable(_WPT01_Top, _WPT01_Title, _WPT01_Data, _WPT01_Width, false, null, couleurLigne, null, null);
    WSGN.setValeurs("+", WSGN_GRP);
    WSGN_MOINS.setValeurs("-");
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_SELECTION_CODE_BARRE, 'b', false);
    snBarreBouton.ajouterBouton(BOUTON_SAISIE_ARTICLE, 'a', true);
    snBarreBouton.ajouterBouton(BOUTON_PLEINE_PAGE, 'l', true);
    snBarreBouton.ajouterBouton(BOUTON_CLASSIQUE, 'l', false);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER_QUANTITE, 'p', true);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER_VUE_LISTE, 'v', false);
    snBarreBouton.ajouterBouton(EnumBouton.PIED, true);
    snBarreBouton.ajouterBouton(EnumBouton.RETOURNER_ENTETE, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
    
    WARTT.addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        if (e.getKeyChar() == KeyEvent.VK_ENTER) {
          valider();
        }
      }
    });
    WLOT.addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        if (e.getKeyChar() == KeyEvent.VK_ENTER) {
          valider();
        }
      }
    });
  }
  
  private void valider() {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Lignes de bordereau de stock : @TITB@")).trim());
    lbArticle.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCHPA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    gererCouleurListe();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // On ne montre le sens du mouvement de stock que si:
    // On est en divers et qu'on n'est pas en entrée, sortie et inventaire
    pnlSens.setVisible(lexique.isTrue("32") && !lexique.isTrue("31") && !lexique.isTrue("33") && !lexique.isTrue("35"));
    lbSens.setVisible(pnlSens.isVisible());
    
    // date
    WDTRTX.setEnabled(false);
    // Code établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "E1ETB");
    snEtablissement.setEnabled(false);
    
    // Article
    snArticle.setSession(getSession());
    snArticle.setIdEtablissement(snEtablissement.getIdSelection());
    snArticle.setFiltreArticleStock(EnumFiltreArticleStock.OPTION_GERE_STOCK);
    snArticle.charger(false);
    snArticle.setSelectionParChampRPG(lexique, "WARTT");
    // Il faudra expliquer le N85 et 41 pour les mecs qui passent derrière sinon dans peu de temps ce sera complètement inmaintenable
    snArticle.setVisible(lexique.isTrue("N85 AND 41"));
    WARTT.setVisible(!snArticle.isVisible());
    if (snArticle.isVisible()) {
      setRequestComponent(snArticle);
    }
    
    // Magasins
    snMagasinEmetteur.setSession(getSession());
    snMagasinEmetteur.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinEmetteur.charger(false);
    snMagasinEmetteur.setSelectionParChampRPG(lexique, "E1MAG");
    snMagasinEmetteur.setEnabled(false);
    
    snMagasinRecepteur.setSession(getSession());
    snMagasinRecepteur.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinRecepteur.charger(false);
    snMagasinRecepteur.setSelectionParChampRPG(lexique, "E1MAG2");
    snMagasinRecepteur.setEnabled(false);
    snMagasinRecepteur.setVisible(lexique.isTrue("34"));
    lbMagasinEmetteur.setVisible(snMagasinEmetteur.isVisible());
    lbMagasinRecepteur.setVisible(snMagasinRecepteur.isVisible());
    
    WSGN.setSelected(lexique.HostFieldGetData("WSGN").trim().equalsIgnoreCase(""));
    lbWLOT.setVisible(lexique.isTrue("N46 AND 81 AND 83"));
    if (lexique.isTrue("41 AND 85")) {
      lbArticle.setText("Code barre");
    }
    else if (lexique.isTrue("N41")) {
      lbArticle.setText("Code article");
    }
    else {
      lbArticle.setText("Article");
    }
    
    traiterVisibiliteBouton();
    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "E1ETB");
    snMagasinEmetteur.renseignerChampRPG(lexique, "E1MAG");
    snMagasinRecepteur.renseignerChampRPG(lexique, "E1MAG2");
    if (snArticle.isVisible()) {
      snArticle.renseignerChampRPG(lexique, "WARTT");
    }
  }
  
  /**
   * Traiter la visibilité des boutons
   */
  private void traiterVisibiliteBouton() {
    boolean actif = true;
    if (lexique.HostFieldGetData("LD01").trim().isEmpty()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_MODIFIER_QUANTITE, actif);
    
    snBarreBouton.activerBouton(BOUTON_SAISIE_ARTICLE, lexique.isTrue("(41) AND (85)"));
    snBarreBouton.activerBouton(BOUTON_SELECTION_CODE_BARRE, lexique.isTrue("(41) AND (N85)"));
    snBarreBouton.activerBouton(BOUTON_PLEINE_PAGE, lexique.isTrue("41"));
    snBarreBouton.activerBouton(BOUTON_CLASSIQUE, lexique.isTrue("N41"));
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.PIED)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(EnumBouton.RETOURNER_ENTETE)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      
      // Bouton Saisie code barres (F5)
      else if (pSNBouton.isBouton(BOUTON_SELECTION_CODE_BARRE)) {
        lexique.HostScreenSendKey(this, "F5");
      }
      
      // Bouton Saisie quantité (F7)
      else if (pSNBouton.isBouton(BOUTON_PLEINE_PAGE)) {
        lexique.HostScreenSendKey(this, "F7");
      }
      else if (pSNBouton.isBouton(BOUTON_CLASSIQUE)) {
        lexique.HostScreenSendKey(this, "F7");
      }
      
      else if (pSNBouton.isBouton(BOUTON_SAISIE_ARTICLE)) {
        lexique.HostScreenSendKey(this, "F5");
      }
      
      else if (pSNBouton.isBouton(BOUTON_MODIFIER_QUANTITE)) {
        lexique.HostScreenSendKey(this, "F8");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER_VUE_LISTE)) {
        lexique.HostScreenSendKey(this, "F24");
      }
      
      traiterVisibiliteBouton();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rafraichirBoutons() {
    
  }
  
  // permet de gérer la couleur des lignes de la liste en fonction du type de ligne (article, commentaire , edition,
  // annulé...)
  private void gererCouleurListe() {
    // couleurLigne = new Color[listeCellules.length][1];
    String chaineTest = null;
    for (int i = 0; i < listeCellules.length; i++) {
      chaineTest = lexique.HostFieldGetData(listeCellules[i]).trim();
      if ((chaineTest != null) && (!chaineTest.equals("") && chaineTest.length() > 7)) {
        if (chaineTest.substring(4, 7).trim().equals("")) {
          couleurLigne[i][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
        }
        else {
          couleurLigne[i][0] = Color.BLACK;
        }
      }
      else {
        couleurLigne[i][0] = Color.BLACK;
      }
    }
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WPT01_Top, "1", "enter");
    WPT01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WPT01_Top, "2", "enter");
    WPT01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WPT01_Top, "4", "enter");
    WPT01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WPT01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WPT01_Top, "1", "enter", e);
    if (WPT01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "enter");
    }
  }
  
  private void snArticleValueChanged(SNComposantEvent e) {
    if (WLOT.isVisible()) {
      WLOT.requestFocus();
    }
    else {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void thisFocusGained(FocusEvent e) {
    try {
      snArticle.requestFocusInWindow();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlInformations = new SNPanel();
    pnlGauche = new SNPanel();
    lbNumero = new SNLabelChamp();
    WNUM = new XRiTextField();
    lbDateTraitement = new SNLabelChamp();
    WDTRTX = new XRiCalendrier();
    lbMagasinEmetteur = new SNLabelChamp();
    snMagasinEmetteur = new SNMagasin();
    pnlDroite = new SNPanel();
    lbE1ETB = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbMagasinRecepteur = new SNLabelChamp();
    snMagasinRecepteur = new SNMagasin();
    pnDétailLignes = new SNPanelTitre();
    SCROLLPANE_LIST7 = new JScrollPane();
    WPT01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    pnlSaisieLigne = new SNPanelTitre();
    lbWCOD = new SNLabelChamp();
    WCOD = new XRiTextField();
    lbWNLI = new SNLabelChamp();
    WNLI = new XRiTextField();
    lbArticle = new SNLabelChamp();
    pnlArticle = new SNPanel();
    snArticle = new SNArticle();
    WARTT = new XRiTextField();
    lbSens = new SNLabelChamp();
    pnlSens = new SNPanel();
    WSGN = new XRiRadioButton();
    WSGN_MOINS = new XRiRadioButton();
    lbWLOT = new SNLabelChamp();
    WLOT = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    BTD2 = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    WSGN_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setFocusable(false);
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Lignes de bordereau de stock : @TITB@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

      //======== pnlInformations ========
      {
        pnlInformations.setName("pnlInformations");
        pnlInformations.setLayout(new GridLayout(1, 2, 5, 0));

        //======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- lbNumero ----
          lbNumero.setText("Num\u00e9ro");
          lbNumero.setMaximumSize(new Dimension(200, 30));
          lbNumero.setMinimumSize(new Dimension(200, 30));
          lbNumero.setPreferredSize(new Dimension(200, 30));
          lbNumero.setInheritsPopupMenu(false);
          lbNumero.setName("lbNumero");
          pnlGauche.add(lbNumero, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- WNUM ----
          WNUM.setComponentPopupMenu(null);
          WNUM.setMinimumSize(new Dimension(70, 30));
          WNUM.setMaximumSize(new Dimension(70, 30));
          WNUM.setPreferredSize(new Dimension(70, 30));
          WNUM.setFont(new Font("sansserif", Font.PLAIN, 14));
          WNUM.setName("WNUM");
          pnlGauche.add(WNUM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbDateTraitement ----
          lbDateTraitement.setText("Date de traitement");
          lbDateTraitement.setMaximumSize(new Dimension(200, 30));
          lbDateTraitement.setMinimumSize(new Dimension(200, 30));
          lbDateTraitement.setPreferredSize(new Dimension(200, 30));
          lbDateTraitement.setName("lbDateTraitement");
          pnlGauche.add(lbDateTraitement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- WDTRTX ----
          WDTRTX.setComponentPopupMenu(null);
          WDTRTX.setOpaque(false);
          WDTRTX.setMaximumSize(new Dimension(110, 30));
          WDTRTX.setMinimumSize(new Dimension(110, 30));
          WDTRTX.setPreferredSize(new Dimension(110, 30));
          WDTRTX.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDTRTX.setName("WDTRTX");
          pnlGauche.add(WDTRTX, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbMagasinEmetteur ----
          lbMagasinEmetteur.setText("Magasin \u00e9metteur");
          lbMagasinEmetteur.setPreferredSize(new Dimension(200, 30));
          lbMagasinEmetteur.setMaximumSize(new Dimension(200, 30));
          lbMagasinEmetteur.setMinimumSize(new Dimension(200, 30));
          lbMagasinEmetteur.setName("lbMagasinEmetteur");
          pnlGauche.add(lbMagasinEmetteur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snMagasinEmetteur ----
          snMagasinEmetteur.setPreferredSize(new Dimension(400, 30));
          snMagasinEmetteur.setMinimumSize(new Dimension(400, 30));
          snMagasinEmetteur.setMaximumSize(new Dimension(350, 30));
          snMagasinEmetteur.setName("snMagasinEmetteur");
          pnlGauche.add(snMagasinEmetteur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformations.add(pnlGauche);

        //======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- lbE1ETB ----
          lbE1ETB.setText("Etablissement");
          lbE1ETB.setMaximumSize(new Dimension(200, 30));
          lbE1ETB.setMinimumSize(new Dimension(200, 30));
          lbE1ETB.setPreferredSize(new Dimension(200, 30));
          lbE1ETB.setInheritsPopupMenu(false);
          lbE1ETB.setName("lbE1ETB");
          pnlDroite.add(lbE1ETB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          pnlDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbMagasinRecepteur ----
          lbMagasinRecepteur.setText(" Magasin r\u00e9cepteur");
          lbMagasinRecepteur.setMaximumSize(new Dimension(200, 30));
          lbMagasinRecepteur.setMinimumSize(new Dimension(200, 30));
          lbMagasinRecepteur.setPreferredSize(new Dimension(200, 30));
          lbMagasinRecepteur.setName("lbMagasinRecepteur");
          pnlDroite.add(lbMagasinRecepteur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snMagasinRecepteur ----
          snMagasinRecepteur.setMaximumSize(new Dimension(500, 30));
          snMagasinRecepteur.setMinimumSize(new Dimension(400, 30));
          snMagasinRecepteur.setPreferredSize(new Dimension(400, 30));
          snMagasinRecepteur.setName("snMagasinRecepteur");
          pnlDroite.add(snMagasinRecepteur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformations.add(pnlDroite);
      }
      pnlContenu.add(pnlInformations, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnDétailLignes ========
      {
        pnDétailLignes.setMaximumSize(new Dimension(700, 320));
        pnDétailLignes.setPreferredSize(new Dimension(700, 320));
        pnDétailLignes.setTitre(" Lignes");
        pnDétailLignes.setName("pnD\u00e9tailLignes");
        pnDétailLignes.setLayout(new GridBagLayout());
        ((GridBagLayout)pnDétailLignes.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnDétailLignes.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnDétailLignes.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnDétailLignes.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //======== SCROLLPANE_LIST7 ========
        {
          SCROLLPANE_LIST7.setComponentPopupMenu(BTD2);
          SCROLLPANE_LIST7.setMaximumSize(new Dimension(600, 270));
          SCROLLPANE_LIST7.setMinimumSize(new Dimension(600, 270));
          SCROLLPANE_LIST7.setPreferredSize(new Dimension(600, 270));
          SCROLLPANE_LIST7.setName("SCROLLPANE_LIST7");

          //---- WPT01 ----
          WPT01.setComponentPopupMenu(BTD2);
          WPT01.setPreferredScrollableViewportSize(new Dimension(543, 240));
          WPT01.setMaximumSize(new Dimension(543, 240));
          WPT01.setMinimumSize(new Dimension(543, 240));
          WPT01.setPreferredSize(new Dimension(543, 240));
          WPT01.setName("WPT01");
          WPT01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WPT01MouseClicked(e);
            }
          });
          SCROLLPANE_LIST7.setViewportView(WPT01);
        }
        pnDétailLignes.add(SCROLLPANE_LIST7, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setMaximumSize(new Dimension(25, 125));
        BT_PGUP.setMinimumSize(new Dimension(25, 125));
        BT_PGUP.setPreferredSize(new Dimension(25, 125));
        BT_PGUP.setName("BT_PGUP");
        pnDétailLignes.add(BT_PGUP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setToolTipText("Page suivante");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setMaximumSize(new Dimension(25, 125));
        BT_PGDOWN.setMinimumSize(new Dimension(25, 125));
        BT_PGDOWN.setPreferredSize(new Dimension(25, 125));
        BT_PGDOWN.setName("BT_PGDOWN");
        pnDétailLignes.add(BT_PGDOWN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnDétailLignes, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlSaisieLigne ========
      {
        pnlSaisieLigne.setTitre("Saisie de ligne");
        pnlSaisieLigne.setFocusCycleRoot(true);
        pnlSaisieLigne.setFocusTraversalPolicyProvider(true);
        pnlSaisieLigne.setFocusable(false);
        pnlSaisieLigne.setName("pnlSaisieLigne");
        pnlSaisieLigne.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlSaisieLigne.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlSaisieLigne.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlSaisieLigne.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlSaisieLigne.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbWCOD ----
        lbWCOD.setText("C ");
        lbWCOD.setMaximumSize(new Dimension(24, 28));
        lbWCOD.setMinimumSize(new Dimension(24, 28));
        lbWCOD.setPreferredSize(new Dimension(24, 28));
        lbWCOD.setName("lbWCOD");
        pnlSaisieLigne.add(lbWCOD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WCOD ----
        WCOD.setComponentPopupMenu(BTD);
        WCOD.setMaximumSize(new Dimension(24, 28));
        WCOD.setMinimumSize(new Dimension(24, 28));
        WCOD.setPreferredSize(new Dimension(24, 28));
        WCOD.setName("WCOD");
        pnlSaisieLigne.add(WCOD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbWNLI ----
        lbWNLI.setText("N\u00b0Ligne");
        lbWNLI.setRequestFocusEnabled(false);
        lbWNLI.setPreferredSize(new Dimension(60, 30));
        lbWNLI.setMinimumSize(new Dimension(60, 30));
        lbWNLI.setMaximumSize(new Dimension(60, 30));
        lbWNLI.setName("lbWNLI");
        pnlSaisieLigne.add(lbWNLI, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WNLI ----
        WNLI.setComponentPopupMenu(BTD);
        WNLI.setMaximumSize(new Dimension(44, 28));
        WNLI.setMinimumSize(new Dimension(44, 28));
        WNLI.setPreferredSize(new Dimension(44, 28));
        WNLI.setRequestFocusEnabled(false);
        WNLI.setName("WNLI");
        pnlSaisieLigne.add(WNLI, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbArticle ----
        lbArticle.setMaximumSize(new Dimension(200, 30));
        lbArticle.setMinimumSize(new Dimension(200, 30));
        lbArticle.setPreferredSize(new Dimension(200, 30));
        lbArticle.setText("@WCHPA@");
        lbArticle.setName("lbArticle");
        pnlSaisieLigne.add(lbArticle, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //======== pnlArticle ========
        {
          pnlArticle.setMaximumSize(new Dimension(720, 30));
          pnlArticle.setMinimumSize(new Dimension(720, 30));
          pnlArticle.setPreferredSize(new Dimension(720, 30));
          pnlArticle.setName("pnlArticle");
          pnlArticle.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlArticle.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlArticle.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlArticle.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlArticle.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- snArticle ----
          snArticle.setVerifyInputWhenFocusTarget(false);
          snArticle.setFocusTraversalPolicyProvider(true);
          snArticle.setFocusCycleRoot(true);
          snArticle.setMaximumSize(new Dimension(500, 30));
          snArticle.setMinimumSize(new Dimension(500, 30));
          snArticle.setPreferredSize(new Dimension(500, 30));
          snArticle.setName("snArticle");
          snArticle.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snArticleValueChanged(e);
            }
          });
          pnlArticle.add(snArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WARTT ----
          WARTT.setComponentPopupMenu(BTD);
          WARTT.setPreferredSize(new Dimension(210, 28));
          WARTT.setMaximumSize(new Dimension(210, 28));
          WARTT.setMinimumSize(new Dimension(210, 28));
          WARTT.setName("WARTT");
          pnlArticle.add(WARTT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSaisieLigne.add(pnlArticle, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbSens ----
        lbSens.setText("Sens");
        lbSens.setName("lbSens");
        pnlSaisieLigne.add(lbSens, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlSens ========
        {
          pnlSens.setOpaque(false);
          pnlSens.setMaximumSize(new Dimension(105, 30));
          pnlSens.setMinimumSize(new Dimension(105, 30));
          pnlSens.setPreferredSize(new Dimension(80, 30));
          pnlSens.setName("pnlSens");
          pnlSens.setLayout(new FlowLayout());

          //---- WSGN ----
          WSGN.setText("+");
          WSGN.setToolTipText("Signe op\u00e9ration");
          WSGN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSGN.setFont(new Font("sansserif", Font.PLAIN, 14));
          WSGN.setName("WSGN");
          pnlSens.add(WSGN);

          //---- WSGN_MOINS ----
          WSGN_MOINS.setText("-");
          WSGN_MOINS.setToolTipText("Signe op\u00e9ration");
          WSGN_MOINS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSGN_MOINS.setFont(new Font("sansserif", Font.PLAIN, 14));
          WSGN_MOINS.setName("WSGN_MOINS");
          pnlSens.add(WSGN_MOINS);
        }
        pnlSaisieLigne.add(pnlSens, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- lbWLOT ----
        lbWLOT.setText("Lot");
        lbWLOT.setMaximumSize(new Dimension(50, 30));
        lbWLOT.setMinimumSize(new Dimension(50, 30));
        lbWLOT.setPreferredSize(new Dimension(50, 28));
        lbWLOT.setName("lbWLOT");
        pnlSaisieLigne.add(lbWLOT, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WLOT ----
        WLOT.setComponentPopupMenu(BTD);
        WLOT.setMaximumSize(new Dimension(210, 28));
        WLOT.setMinimumSize(new Dimension(210, 28));
        WLOT.setPreferredSize(new Dimension(210, 28));
        WLOT.setName("WLOT");
        pnlSaisieLigne.add(WLOT, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlSaisieLigne, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD2.add(CHOISIR);

      //---- OBJ_26 ----
      OBJ_26.setText("Modifier");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_26);

      //---- OBJ_27 ----
      OBJ_27.setText("Annuler");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_27);
    }

    //---- WSGN_GRP ----
    WSGN_GRP.add(WSGN);
    WSGN_GRP.add(WSGN_MOINS);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlInformations;
  private SNPanel pnlGauche;
  private SNLabelChamp lbNumero;
  private XRiTextField WNUM;
  private SNLabelChamp lbDateTraitement;
  private XRiCalendrier WDTRTX;
  private SNLabelChamp lbMagasinEmetteur;
  private SNMagasin snMagasinEmetteur;
  private SNPanel pnlDroite;
  private SNLabelChamp lbE1ETB;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbMagasinRecepteur;
  private SNMagasin snMagasinRecepteur;
  private SNPanelTitre pnDétailLignes;
  private JScrollPane SCROLLPANE_LIST7;
  private XRiTable WPT01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private SNPanelTitre pnlSaisieLigne;
  private SNLabelChamp lbWCOD;
  private XRiTextField WCOD;
  private SNLabelChamp lbWNLI;
  private XRiTextField WNLI;
  private SNLabelChamp lbArticle;
  private SNPanel pnlArticle;
  private SNArticle snArticle;
  private XRiTextField WARTT;
  private SNLabelChamp lbSens;
  private SNPanel pnlSens;
  private XRiRadioButton WSGN;
  private XRiRadioButton WSGN_MOINS;
  private SNLabelChamp lbWLOT;
  private XRiTextField WLOT;
  private JPopupMenu BTD;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_22;
  private JPopupMenu BTD2;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_27;
  private ButtonGroup WSGN_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
