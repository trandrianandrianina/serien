
package ri.serien.libecranrpg.vgvx.VGVX22FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX22FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HL201", };
  private String[][] _WTP01_Data = { { "L201", }, { "L202", }, { "L203", }, { "L204", }, { "L205", }, { "L206", }, { "L207", }, { "L208", },
      { "L209", }, { "L210", }, { "L211", }, { "L212", }, { "L213", }, { "L214", }, { "L215", }, };
  private int[] _WTP01_Width = { 694, };
  
  public VGVX22FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    WSLMTA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSLMTA@")).trim());
    SLZPL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLZPL@")).trim());
    WSLMTV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSLMTV@")).trim());
    WMARGE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMARGE@")).trim());
    WPMAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMAR@")).trim());
    SLCCT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLCCT@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZPL@")).trim());
    SLDT1X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLDT1X@")).trim());
    OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITZP1@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITZP2@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITZP3@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITZP4@")).trim());
    OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITZP5@")).trim());
    SLTP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLTP1@")).trim());
    SLTP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLTP2@")).trim());
    SLTP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLTP3@")).trim());
    SLTP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLTP4@")).trim());
    SLTP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLTP5@")).trim());
    P22MAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P22MAG@")).trim());
    A1ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1ART@")).trim());
    SLLOT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLLOT@")).trim());
    SLDPEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLDPEX@")).trim());
    SLLOTF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLLOTF@")).trim());
    WQTE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQTE@")).trim());
    WQTS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQTS@")).trim());
    WSOL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST2, LIST2.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    SLDT1X.setVisible(lexique.isPresent("SLDT1X"));
    SLDPEX.setVisible(lexique.isPresent("SLDPEX"));
    OBJ_68.setVisible(lexique.isPresent("P22MAG"));
    OBJ_46.setVisible(lexique.isPresent("LIBZPL"));
    OBJ_53.setVisible(lexique.HostFieldGetData("P22FLI").equalsIgnoreCase("1"));
    OBJ_52.setVisible(lexique.HostFieldGetData("P22FLI").equalsIgnoreCase("3"));
    SLCCT.setVisible(lexique.isPresent("SLCCT"));
    WPMAR.setVisible(!lexique.HostFieldGetData("WPMAR").trim().equalsIgnoreCase(""));
    WMARGE.setVisible(!lexique.HostFieldGetData("WMARGE").trim().equalsIgnoreCase(""));
    WSLMTV.setVisible(lexique.isPresent("WSLMTV"));
    SLZPL.setVisible(lexique.isPresent("SLZPL"));
    WSLMTA.setVisible(lexique.isPresent("WSLMTA"));
    OBJ_61.setVisible(!lexique.HostFieldGetData("WPMAR").trim().equalsIgnoreCase(""));
    OBJ_50.setVisible(!lexique.HostFieldGetData("WMARGE").trim().equalsIgnoreCase(""));
    WSOL.setVisible(lexique.isPresent("WSOL"));
    WQTS.setVisible(lexique.isPresent("WQTS"));
    WQTE.setVisible(lexique.isPresent("WQTE"));
    OBJ_85.setVisible(lexique.HostFieldGetData("P22FLI").equalsIgnoreCase("4"));
    OBJ_54.setVisible(lexique.HostFieldGetData("P22FLI").equalsIgnoreCase("2"));
    SLLOT.setVisible(lexique.isPresent("SLLOT"));
    A1ART.setVisible(lexique.isPresent("A1ART"));
    SLDPEX.setText(lexique.HostFieldGetData("SLDPEX"));
    SLDT1X.setText(lexique.HostFieldGetData("SLDT1X"));
    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "R", "Enter");
    WTP01.setValeurTop("R");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "P", "Enter");
    WTP01.setValeurTop("P");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "I", "Enter");
    WTP01.setValeurTop("I");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_84 = new JLabel();
    WETB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    A1LIB = new RiZoneSortie();
    OBJ_54 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_41 = new JLabel();
    WSLMTA = new RiZoneSortie();
    SLZPL = new RiZoneSortie();
    WSLMTV = new RiZoneSortie();
    WMARGE = new RiZoneSortie();
    WPMAR = new RiZoneSortie();
    SLCCT = new RiZoneSortie();
    OBJ_52 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_92 = new JLabel();
    SLDT1X = new RiZoneSortie();
    OBJ_86 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_90 = new JLabel();
    SLTP1 = new RiZoneSortie();
    SLTP2 = new RiZoneSortie();
    SLTP3 = new RiZoneSortie();
    SLTP4 = new RiZoneSortie();
    SLTP5 = new RiZoneSortie();
    P22MAG = new RiZoneSortie();
    OBJ_34 = new JLabel();
    A1ART = new RiZoneSortie();
    SLLOT = new RiZoneSortie();
    SLDPEX = new RiZoneSortie();
    SLLOTF = new RiZoneSortie();
    panel2 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel3 = new JPanel();
    OBJ_56 = new JLabel();
    WQTE = new RiZoneSortie();
    WQTS = new RiZoneSortie();
    OBJ_59 = new JLabel();
    WSOL = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Article lot ");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_84 ----
          OBJ_84.setText("Etablissement");
          OBJ_84.setName("OBJ_84");
          p_tete_gauche.add(OBJ_84);
          OBJ_84.setBounds(5, 5, 95, 18);

          //---- WETB ----
          WETB.setOpaque(false);
          WETB.setText("@WETB@");
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(100, 2, 40, WETB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 150));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Conditionnement");
              riSousMenu_bt6.setToolTipText("On/Off Qt\u00e9s en U,Stock/ U. conditionnement/M\u00e8tres lin\u00e9aires");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Tarif");
              riSousMenu_bt7.setToolTipText("Tarif");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes");
              riSousMenu_bt14.setToolTipText("Bloc-notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1015, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1015, 600));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- A1LIB ----
            A1LIB.setText("@A1LIB@");
            A1LIB.setName("A1LIB");
            panel1.add(A1LIB);
            A1LIB.setBounds(15, 45, 420, A1LIB.getPreferredSize().height);

            //---- OBJ_54 ----
            OBJ_54.setText("En unite de conditionnement");
            OBJ_54.setComponentPopupMenu(BTD);
            OBJ_54.setName("OBJ_54");
            panel1.add(OBJ_54);
            OBJ_54.setBounds(17, 140, 198, 15);

            //---- OBJ_85 ----
            OBJ_85.setText("En nombre de palette");
            OBJ_85.setComponentPopupMenu(BTD);
            OBJ_85.setName("OBJ_85");
            panel1.add(OBJ_85);
            OBJ_85.setBounds(17, 140, 170, 15);

            //---- OBJ_43 ----
            OBJ_43.setText("Date limite de vente");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(17, 77, 121, 20);

            //---- OBJ_51 ----
            OBJ_51.setText("Date de fabrication");
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(17, 107, 121, 20);

            //---- OBJ_50 ----
            OBJ_50.setText("Marge actuelle");
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(730, 107, 92, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("Pourcentage");
            OBJ_61.setName("OBJ_61");
            panel1.add(OBJ_61);
            OBJ_61.setBounds(730, 137, 92, 20);

            //---- OBJ_41 ----
            OBJ_41.setText("Montant achat");
            OBJ_41.setName("OBJ_41");
            panel1.add(OBJ_41);
            OBJ_41.setBounds(730, 47, 92, 20);

            //---- WSLMTA ----
            WSLMTA.setText("@WSLMTA@");
            WSLMTA.setHorizontalAlignment(SwingConstants.RIGHT);
            WSLMTA.setName("WSLMTA");
            panel1.add(WSLMTA);
            WSLMTA.setBounds(830, 45, 105, WSLMTA.getPreferredSize().height);

            //---- SLZPL ----
            SLZPL.setText("@SLZPL@");
            SLZPL.setName("SLZPL");
            panel1.add(SLZPL);
            SLZPL.setBounds(325, 75, 110, SLZPL.getPreferredSize().height);

            //---- WSLMTV ----
            WSLMTV.setText("@WSLMTV@");
            WSLMTV.setHorizontalAlignment(SwingConstants.RIGHT);
            WSLMTV.setName("WSLMTV");
            panel1.add(WSLMTV);
            WSLMTV.setBounds(830, 75, 105, WSLMTV.getPreferredSize().height);

            //---- WMARGE ----
            WMARGE.setText("@WMARGE@");
            WMARGE.setHorizontalAlignment(SwingConstants.RIGHT);
            WMARGE.setName("WMARGE");
            panel1.add(WMARGE);
            WMARGE.setBounds(830, 105, 105, WMARGE.getPreferredSize().height);

            //---- WPMAR ----
            WPMAR.setText("@WPMAR@");
            WPMAR.setHorizontalAlignment(SwingConstants.RIGHT);
            WPMAR.setName("WPMAR");
            panel1.add(WPMAR);
            WPMAR.setBounds(830, 135, 105, WPMAR.getPreferredSize().height);

            //---- SLCCT ----
            SLCCT.setText("@SLCCT@");
            SLCCT.setName("SLCCT");
            panel1.add(SLCCT);
            SLCCT.setBounds(325, 105, 110, SLCCT.getPreferredSize().height);

            //---- OBJ_52 ----
            OBJ_52.setText("En m\u00e8tres lineaires");
            OBJ_52.setComponentPopupMenu(BTD);
            OBJ_52.setName("OBJ_52");
            panel1.add(OBJ_52);
            OBJ_52.setBounds(17, 140, 117, 15);

            //---- OBJ_48 ----
            OBJ_48.setText("Montant vente");
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(730, 77, 92, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("En unit\u00e9 de stock");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(17, 140, 104, 15);

            //---- OBJ_46 ----
            OBJ_46.setText("@LIBZPL@");
            OBJ_46.setName("OBJ_46");
            panel1.add(OBJ_46);
            OBJ_46.setBounds(230, 77, 85, 20);

            //---- OBJ_68 ----
            OBJ_68.setText("Magasin");
            OBJ_68.setName("OBJ_68");
            panel1.add(OBJ_68);
            OBJ_68.setBounds(230, 137, 85, 20);

            //---- OBJ_92 ----
            OBJ_92.setText("Dossier");
            OBJ_92.setName("OBJ_92");
            panel1.add(OBJ_92);
            OBJ_92.setBounds(230, 107, 85, 20);

            //---- SLDT1X ----
            SLDT1X.setText("@SLDT1X@");
            SLDT1X.setName("SLDT1X");
            panel1.add(SLDT1X);
            SLDT1X.setBounds(140, 105, 72, SLDT1X.getPreferredSize().height);

            //---- OBJ_86 ----
            OBJ_86.setText("@TITZP1@");
            OBJ_86.setFont(OBJ_86.getFont().deriveFont(OBJ_86.getFont().getStyle() | Font.BOLD));
            OBJ_86.setName("OBJ_86");
            panel1.add(OBJ_86);
            OBJ_86.setBounds(485, 80, 34, 24);

            //---- OBJ_87 ----
            OBJ_87.setText("@TITZP2@");
            OBJ_87.setFont(OBJ_87.getFont().deriveFont(OBJ_87.getFont().getStyle() | Font.BOLD));
            OBJ_87.setName("OBJ_87");
            panel1.add(OBJ_87);
            OBJ_87.setBounds(525, 80, 34, 24);

            //---- OBJ_88 ----
            OBJ_88.setText("@TITZP3@");
            OBJ_88.setFont(OBJ_88.getFont().deriveFont(OBJ_88.getFont().getStyle() | Font.BOLD));
            OBJ_88.setName("OBJ_88");
            panel1.add(OBJ_88);
            OBJ_88.setBounds(565, 80, 34, 24);

            //---- OBJ_89 ----
            OBJ_89.setText("@TITZP4@");
            OBJ_89.setFont(OBJ_89.getFont().deriveFont(OBJ_89.getFont().getStyle() | Font.BOLD));
            OBJ_89.setName("OBJ_89");
            panel1.add(OBJ_89);
            OBJ_89.setBounds(605, 80, 34, 24);

            //---- OBJ_90 ----
            OBJ_90.setText("@TITZP5@");
            OBJ_90.setFont(OBJ_90.getFont().deriveFont(OBJ_90.getFont().getStyle() | Font.BOLD));
            OBJ_90.setName("OBJ_90");
            panel1.add(OBJ_90);
            OBJ_90.setBounds(645, 80, 34, 24);

            //---- SLTP1 ----
            SLTP1.setText("@SLTP1@");
            SLTP1.setName("SLTP1");
            panel1.add(SLTP1);
            SLTP1.setBounds(485, 105, 34, SLTP1.getPreferredSize().height);

            //---- SLTP2 ----
            SLTP2.setText("@SLTP2@");
            SLTP2.setName("SLTP2");
            panel1.add(SLTP2);
            SLTP2.setBounds(525, 105, 34, SLTP2.getPreferredSize().height);

            //---- SLTP3 ----
            SLTP3.setText("@SLTP3@");
            SLTP3.setName("SLTP3");
            panel1.add(SLTP3);
            SLTP3.setBounds(565, 105, 34, SLTP3.getPreferredSize().height);

            //---- SLTP4 ----
            SLTP4.setText("@SLTP4@");
            SLTP4.setName("SLTP4");
            panel1.add(SLTP4);
            SLTP4.setBounds(605, 105, 34, SLTP4.getPreferredSize().height);

            //---- SLTP5 ----
            SLTP5.setText("@SLTP5@");
            SLTP5.setName("SLTP5");
            panel1.add(SLTP5);
            SLTP5.setBounds(645, 105, 34, SLTP5.getPreferredSize().height);

            //---- P22MAG ----
            P22MAG.setText("@P22MAG@");
            P22MAG.setName("P22MAG");
            panel1.add(P22MAG);
            P22MAG.setBounds(325, 135, 34, P22MAG.getPreferredSize().height);

            //---- OBJ_34 ----
            OBJ_34.setText("Article");
            OBJ_34.setName("OBJ_34");
            panel1.add(OBJ_34);
            OBJ_34.setBounds(17, 15, 73, 24);

            //---- A1ART ----
            A1ART.setText("@A1ART@");
            A1ART.setName("A1ART");
            panel1.add(A1ART);
            A1ART.setBounds(95, 15, 162, A1ART.getPreferredSize().height);

            //---- SLLOT ----
            SLLOT.setText("@SLLOT@");
            SLLOT.setName("SLLOT");
            panel1.add(SLLOT);
            SLLOT.setBounds(273, 15, 162, SLLOT.getPreferredSize().height);

            //---- SLDPEX ----
            SLDPEX.setText("@SLDPEX@");
            SLDPEX.setName("SLDPEX");
            panel1.add(SLDPEX);
            SLDPEX.setBounds(140, 75, 72, SLDPEX.getPreferredSize().height);

            //---- SLLOTF ----
            SLLOTF.setText("@SLLOTF@");
            SLLOTF.setToolTipText("Lot fournisseur");
            SLLOTF.setName("SLLOTF");
            panel1.add(SLLOTF);
            SLLOTF.setBounds(445, 15, 260, SLLOTF.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(15, 20, 985, 185);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST2.setViewportView(WTP01);
            }
            panel2.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(15, 15, 925, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(945, 15, 25, 130);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(945, 155, 25, 130);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(15, 215, 985, 300);

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder(""));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_56 ----
            OBJ_56.setText("Total entr\u00e9e/sortie");
            OBJ_56.setName("OBJ_56");
            panel3.add(OBJ_56);
            OBJ_56.setBounds(17, 17, 125, 20);

            //---- WQTE ----
            WQTE.setText("@WQTE@");
            WQTE.setName("WQTE");
            panel3.add(WQTE);
            WQTE.setBounds(155, 15, 114, WQTE.getPreferredSize().height);

            //---- WQTS ----
            WQTS.setText("@WQTS@");
            WQTS.setName("WQTS");
            panel3.add(WQTS);
            WQTS.setBounds(280, 15, 114, WQTS.getPreferredSize().height);

            //---- OBJ_59 ----
            OBJ_59.setText("Stock");
            OBJ_59.setName("OBJ_59");
            panel3.add(OBJ_59);
            OBJ_59.setBounds(420, 17, 45, 20);

            //---- WSOL ----
            WSOL.setText("@WSOL@");
            WSOL.setName("WSOL");
            panel3.add(WSOL);
            WSOL.setBounds(470, 15, 114, WSOL.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel3);
          panel3.setBounds(15, 525, 985, 55);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("R\u00e9partition");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Calcul prix de revient et tarif");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Info lots");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_84;
  private RiZoneSortie WETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie A1LIB;
  private JLabel OBJ_54;
  private JLabel OBJ_85;
  private JLabel OBJ_43;
  private JLabel OBJ_51;
  private JLabel OBJ_50;
  private JLabel OBJ_61;
  private JLabel OBJ_41;
  private RiZoneSortie WSLMTA;
  private RiZoneSortie SLZPL;
  private RiZoneSortie WSLMTV;
  private RiZoneSortie WMARGE;
  private RiZoneSortie WPMAR;
  private RiZoneSortie SLCCT;
  private JLabel OBJ_52;
  private JLabel OBJ_48;
  private JLabel OBJ_53;
  private JLabel OBJ_46;
  private JLabel OBJ_68;
  private JLabel OBJ_92;
  private RiZoneSortie SLDT1X;
  private JLabel OBJ_86;
  private JLabel OBJ_87;
  private JLabel OBJ_88;
  private JLabel OBJ_89;
  private JLabel OBJ_90;
  private RiZoneSortie SLTP1;
  private RiZoneSortie SLTP2;
  private RiZoneSortie SLTP3;
  private RiZoneSortie SLTP4;
  private RiZoneSortie SLTP5;
  private RiZoneSortie P22MAG;
  private JLabel OBJ_34;
  private RiZoneSortie A1ART;
  private RiZoneSortie SLLOT;
  private RiZoneSortie SLDPEX;
  private RiZoneSortie SLLOTF;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel3;
  private JLabel OBJ_56;
  private RiZoneSortie WQTE;
  private RiZoneSortie WQTS;
  private JLabel OBJ_59;
  private RiZoneSortie WSOL;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
