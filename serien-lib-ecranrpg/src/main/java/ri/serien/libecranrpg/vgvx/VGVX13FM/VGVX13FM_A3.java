//$$david$$ ££16/12/10££ -> new look

package ri.serien.libecranrpg.vgvx.VGVX13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX13FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WT301_Top = { "WT301", "WT302", "WT303", "WT304", "WT305", "WT306", "WT307", "WT308", "WT309", "WT310", "WT311",
      "WT312", "WT313", "WT314", "WT315", "WT316", "WT317", "WT318", "WT319", "WT320", "WT321", "WT322" };
  private String[] _WT301_Title = { "HLD01", };
  private String[][] _WT301_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, { "LD16", }, { "LD17", }, { "LD18", },
      { "LD19", }, { "LD20", }, { "LD21", }, { "LD22", }, };
  private int[] _WT301_Width = { 58, };
  boolean flipAffaire = false;
  
  public VGVX13FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WOPE5.setValeursSelection("X", " ");
    WOPE4.setValeursSelection("X", " ");
    WOPE3.setValeursSelection("X", " ");
    WOPE2.setValeursSelection("X", " ");
    WOPE1.setValeursSelection("X", " ");
    WTYPE3.setValeursSelection("X", " ");
    WTYPE2.setValeursSelection("X", " ");
    WTYPE1.setValeursSelection("X", " ");
    WTYPE4.setValeursSelection("X", " ");
    WT301.setAspectTable(_WT301_Top, _WT301_Title, _WT301_Data, _WT301_Width, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    MALIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    panel2.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@ @A1LB1@ @A1LB2@ @A1LB3@")).trim()));
    UNLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNLIB@")).trim());
    A1UNL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNL@")).trim());
    ULLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULLIB@")).trim());
    OBJ_99.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1CL2@")).trim());
    OBJ_105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DSAC@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    WOPE4.setVisible(!lexique.HostFieldGetData("WTYPE3").trim().equalsIgnoreCase(""));
    WOPE1.setVisible(!lexique.HostFieldGetData("WTYPE3").trim().equalsIgnoreCase(""));
    WOPE2.setVisible(!lexique.HostFieldGetData("WTYPE3").trim().equalsIgnoreCase(""));
    WOPE5.setVisible(!lexique.HostFieldGetData("WTYPE3").trim().equalsIgnoreCase(""));
    WOPE3.setVisible(!lexique.HostFieldGetData("WTYPE3").trim().equalsIgnoreCase(""));
    OBJ_48.setVisible(lexique.HostFieldGetData("FLIPST").equalsIgnoreCase("3"));
    OBJ_49.setVisible(lexique.HostFieldGetData("FLIPST").equalsIgnoreCase("4"));
    X13DVX.setVisible(lexique.HostFieldGetData("DIS").equalsIgnoreCase("Dis"));
    OBJ_68.setVisible(lexique.HostFieldGetData("A1TSP").equalsIgnoreCase("Q"));
    OBJ_74.setVisible(lexique.HostFieldGetData("FLIPST").equalsIgnoreCase("2"));
    OBJ_99.setVisible(lexique.isPresent("A1CL2"));
    OBJ_124.setVisible(lexique.isPresent("SERDEB"));
    
    // affaire
    WAFFR.setVisible(!lexique.HostFieldGetData("WAFFR").trim().equals(""));
    label3.setVisible(WAFFR.isVisible());
    
    riSousMenu1.setEnabled(lexique.HostFieldGetData("A1TSP").equalsIgnoreCase("Q"));
    riSousMenu21.setEnabled(lexique.isTrue("(92) AND (59)"));
    
    // double quantité
    // double quantité
    if (lexique.isTrue("N20")) {
      OBJ_94.setText("En unité de stock");
    }
    else {
      OBJ_94.setText("Stock en double quantité");
    }
    
    

    
    p_bpresentation.setCodeEtablissement(H2ETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(H2ETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17", false);
  }
  
  private void riSousMenu_bt21ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "1", "Enter");
    WT301.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "5", "Enter");
    WT301.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "C", "Enter");
    WT301.setValeurTop("C");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "H", "Enter");
    WT301.setValeurTop("H");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "X", "Enter");
    WT301.setValeurTop("X");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_38ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_46ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void WTYPE3ActionPerformed(ActionEvent e) {
    if (WTYPE3.isSelected()) {
      p_stock.setVisible(true);
    }
    else {
      p_stock.setVisible(false);
    }
  }
  
  private void WT301MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WT301_Top, "1", "ENTER", e);
    if (WT301.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void riSousMenu_bt3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void riSousMenu_bt4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void OBJ_39ActionPerformed(ActionEvent e) {
    WT301.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    H2ART = new XRiTextField();
    OBJ_67 = new JLabel();
    OBJ_66 = new JLabel();
    H2ETB = new XRiTextField();
    OBJ_69 = new JLabel();
    H2MAG = new XRiTextField();
    MALIB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_94 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    riSousMenu4 = new RiSousMenu();
    riSousMenu_bt4 = new RiSousMenu_bt();
    riSousMenu5 = new RiSousMenu();
    riSousMenu_bt5 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    WMIN = new XRiTextField();
    WMAX = new XRiTextField();
    WTIE2 = new XRiTextField();
    OBJ_88 = new JLabel();
    WTIE3 = new XRiTextField();
    WTIE1 = new XRiTextField();
    OBJ_108 = new JLabel();
    DATDEB = new XRiCalendrier();
    label1 = new JLabel();
    label2 = new JLabel();
    OBJ_71 = new JLabel();
    A1UNS = new XRiTextField();
    UNLIB = new RiZoneSortie();
    OBJ_110 = new JLabel();
    A1UNL = new RiZoneSortie();
    ULLIB = new RiZoneSortie();
    OBJ_103 = new JPanel();
    WTYPE4 = new XRiCheckBox();
    WTYPE1 = new XRiCheckBox();
    WTYPE2 = new XRiCheckBox();
    WTYPE3 = new XRiCheckBox();
    p_stock = new JPanel();
    WOPE1 = new XRiCheckBox();
    WOPE2 = new XRiCheckBox();
    WOPE3 = new XRiCheckBox();
    WOPE4 = new XRiCheckBox();
    WOPE5 = new XRiCheckBox();
    panel1 = new JPanel();
    SCROLLPANE_LIST1 = new JScrollPane();
    WT301 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_99 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_105 = new JLabel();
    riBoutonDetail1 = new SNBoutonDetail();
    SERDEB = new XRiTextField();
    OBJ_74 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_48 = new JLabel();
    MARDEB = new XRiTextField();
    OBJ_124 = new JLabel();
    label3 = new JLabel();
    WAFFR = new XRiTextField();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_34 = new JMenuItem();
    OBJ_35 = new JMenuItem();
    OBJ_36 = new JMenuItem();
    OBJ_39 = new JMenuItem();
    OBJ_37 = new JMenuItem();
    OBJ_38 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_46 = new JMenuItem();
    OBJ_45 = new JMenuItem();
    X13DVX = new XRiTextField();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Fiche stock");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- H2ART ----
          H2ART.setToolTipText("Code article");
          H2ART.setComponentPopupMenu(BTDA);
          H2ART.setName("H2ART");
          p_tete_gauche.add(H2ART);
          H2ART.setBounds(205, 1, 210, H2ART.getPreferredSize().height);

          //---- OBJ_67 ----
          OBJ_67.setText("Article");
          OBJ_67.setName("OBJ_67");
          p_tete_gauche.add(OBJ_67);
          OBJ_67.setBounds(160, 6, 48, 18);

          //---- OBJ_66 ----
          OBJ_66.setText("Etablissement");
          OBJ_66.setName("OBJ_66");
          p_tete_gauche.add(OBJ_66);
          OBJ_66.setBounds(5, 6, 90, 18);

          //---- H2ETB ----
          H2ETB.setToolTipText("Code \u00e9tablissement");
          H2ETB.setComponentPopupMenu(BTDA);
          H2ETB.setName("H2ETB");
          p_tete_gauche.add(H2ETB);
          H2ETB.setBounds(100, 1, 40, H2ETB.getPreferredSize().height);

          //---- OBJ_69 ----
          OBJ_69.setText("Magasin");
          OBJ_69.setName("OBJ_69");
          p_tete_gauche.add(OBJ_69);
          OBJ_69.setBounds(435, 6, 55, 18);

          //---- H2MAG ----
          H2MAG.setToolTipText("Magasin");
          H2MAG.setComponentPopupMenu(BTDA);
          H2MAG.setName("H2MAG");
          p_tete_gauche.add(H2MAG);
          H2MAG.setBounds(495, 1, 34, H2MAG.getPreferredSize().height);

          //---- MALIB ----
          MALIB.setText("@MALIB@");
          MALIB.setOpaque(false);
          MALIB.setName("MALIB");
          p_tete_gauche.add(MALIB);
          MALIB.setBounds(540, 3, 315, MALIB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_94 ----
          OBJ_94.setText("Stock en double quantit\u00e9");
          OBJ_94.setFont(OBJ_94.getFont().deriveFont(OBJ_94.getFont().getStyle() | Font.BOLD));
          OBJ_94.setName("OBJ_94");
          p_tete_droite.add(OBJ_94);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Historique consommation");
              riSousMenu_bt6.setToolTipText("Historique consommation");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Mouvements sur p\u00e9riode");
              riSousMenu_bt7.setToolTipText("Analyse des mouvements sur une p\u00e9riode");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Saisie \u00e9l\u00e9ments de stock");
              riSousMenu_bt8.setToolTipText("Saisie \u00e9l\u00e9ments de stock");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Saisie mouvement");
              riSousMenu_bt9.setToolTipText("Saisie mouvement");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Historique achats - PUMP");
              riSousMenu_bt10.setToolTipText("Historique achats - PUMP");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Mouvements inventaires");
              riSousMenu_bt11.setToolTipText("R\u00e9capitulatif Mouvements entre Inventaires");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Analyse sur une p\u00e9riode");
              riSousMenu_bt12.setToolTipText("Analyse des mouvements sur une p\u00e9riode");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Autres vues");
              riSousMenu_bt13.setToolTipText("Autres vues");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("conditionnement/m\u00e8tres l.");
              riSousMenu_bt14.setToolTipText("On/Off Qt\u00e9s en U,Stock/ U. conditionnement/m\u00e8tres lineaires");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");

              //---- riMenu_bt4 ----
              riMenu_bt4.setText("Affichage");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);

            //======== riSousMenu20 ========
            {
              riSousMenu20.setName("riSousMenu20");

              //---- riSousMenu_bt20 ----
              riSousMenu_bt20.setText("Affichage fiche article");
              riSousMenu_bt20.setToolTipText("Affichage de la fiche article");
              riSousMenu_bt20.setName("riSousMenu_bt20");
              riSousMenu_bt20.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt20ActionPerformed(e);
                }
              });
              riSousMenu20.add(riSousMenu_bt20);
            }
            menus_haut.add(riSousMenu20);

            //======== riSousMenu21 ========
            {
              riSousMenu21.setName("riSousMenu21");

              //---- riSousMenu_bt21 ----
              riSousMenu_bt21.setText("Autres unit\u00e9s de stocks");
              riSousMenu_bt21.setToolTipText("Affichage autres unit\u00e9s de stocks");
              riSousMenu_bt21.setName("riSousMenu_bt21");
              riSousMenu_bt21.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt21ActionPerformed(e);
                }
              });
              riSousMenu21.add(riSousMenu_bt21);
            }
            menus_haut.add(riSousMenu21);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Stock en double quantit\u00e9");
              riSousMenu_bt1.setToolTipText("R\u00e9capitulatif stock en double quantit\u00e9");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);

            //======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");

              //---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Adresses de stockage");
              riSousMenu_bt2.setToolTipText("Adresses de stockage");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt2ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);

            //======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");

              //---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Affichage du r\u00e9serv\u00e9");
              riSousMenu_bt19.setToolTipText("Affichage du r\u00e9serv\u00e9");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);

            //======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");

              //---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("Attendu et command\u00e9");
              riSousMenu_bt3.setToolTipText("Affichage d\u00e9tail attendu et command\u00e9");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt3ActionPerformed(e);
                }
              });
              riSousMenu3.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu3);

            //======== riSousMenu4 ========
            {
              riSousMenu4.setName("riSousMenu4");

              //---- riSousMenu_bt4 ----
              riSousMenu_bt4.setText("Affichage d\u00e9tail attendu");
              riSousMenu_bt4.setToolTipText("Affichage d\u00e9tail attendu...");
              riSousMenu_bt4.setName("riSousMenu_bt4");
              riSousMenu_bt4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt4ActionPerformed(e);
                }
              });
              riSousMenu4.add(riSousMenu_bt4);
            }
            menus_haut.add(riSousMenu4);

            //======== riSousMenu5 ========
            {
              riSousMenu5.setName("riSousMenu5");

              //---- riSousMenu_bt5 ----
              riSousMenu_bt5.setText("D\u00e9tail command\u00e9");
              riSousMenu_bt5.setToolTipText("Affichage d\u00e9tail command\u00e9");
              riSousMenu_bt5.setName("riSousMenu_bt5");
              riSousMenu_bt5.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt5ActionPerformed(e);
                }
              });
              riSousMenu5.add(riSousMenu_bt5);
            }
            menus_haut.add(riSousMenu5);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("@A1LIB@ @A1LB1@ @A1LB2@ @A1LB3@"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- WMIN ----
            WMIN.setComponentPopupMenu(BTD);
            WMIN.setHorizontalAlignment(SwingConstants.RIGHT);
            WMIN.setName("WMIN");
            panel2.add(WMIN);
            WMIN.setBounds(130, 41, 81, WMIN.getPreferredSize().height);

            //---- WMAX ----
            WMAX.setComponentPopupMenu(BTD);
            WMAX.setHorizontalAlignment(SwingConstants.RIGHT);
            WMAX.setName("WMAX");
            panel2.add(WMAX);
            WMAX.setBounds(130, 71, 81, WMAX.getPreferredSize().height);

            //---- WTIE2 ----
            WTIE2.setComponentPopupMenu(BTD2);
            WTIE2.setToolTipText("Compte client ou fournisseur");
            WTIE2.setName("WTIE2");
            panel2.add(WTIE2);
            WTIE2.setBounds(685, 41, 74, WTIE2.getPreferredSize().height);

            //---- OBJ_88 ----
            OBJ_88.setText("Tiers");
            OBJ_88.setName("OBJ_88");
            panel2.add(OBJ_88);
            OBJ_88.setBounds(620, 47, 40, 17);

            //---- WTIE3 ----
            WTIE3.setComponentPopupMenu(BTD2);
            WTIE3.setToolTipText("Suffixe de livraison client ");
            WTIE3.setName("WTIE3");
            panel2.add(WTIE3);
            WTIE3.setBounds(765, 41, 44, WTIE3.getPreferredSize().height);

            //---- WTIE1 ----
            WTIE1.setComponentPopupMenu(BTD2);
            WTIE1.setToolTipText("Collectif fournisseur");
            WTIE1.setName("WTIE1");
            panel2.add(WTIE1);
            WTIE1.setBounds(660, 41, 24, WTIE1.getPreferredSize().height);

            //---- OBJ_108 ----
            OBJ_108.setText("A partir de");
            OBJ_108.setName("OBJ_108");
            panel2.add(OBJ_108);
            OBJ_108.setBounds(620, 77, 67, 17);

            //---- DATDEB ----
            DATDEB.setName("DATDEB");
            panel2.add(DATDEB);
            DATDEB.setBounds(685, 71, 105, DATDEB.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Stock minimum");
            label1.setName("label1");
            panel2.add(label1);
            label1.setBounds(25, 45, 100, 20);

            //---- label2 ----
            label2.setText("Stock maximum");
            label2.setName("label2");
            panel2.add(label2);
            label2.setBounds(25, 75, 100, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("Unit\u00e9 de stock");
            OBJ_71.setName("OBJ_71");
            panel2.add(OBJ_71);
            OBJ_71.setBounds(260, 45, 88, 20);

            //---- A1UNS ----
            A1UNS.setHorizontalAlignment(SwingConstants.CENTER);
            A1UNS.setName("A1UNS");
            panel2.add(A1UNS);
            A1UNS.setBounds(370, 41, 34, A1UNS.getPreferredSize().height);

            //---- UNLIB ----
            UNLIB.setText("@UNLIB@");
            UNLIB.setName("UNLIB");
            panel2.add(UNLIB);
            UNLIB.setBounds(410, 43, 126, UNLIB.getPreferredSize().height);

            //---- OBJ_110 ----
            OBJ_110.setText("conditionnement");
            OBJ_110.setName("OBJ_110");
            panel2.add(OBJ_110);
            OBJ_110.setBounds(260, 75, 110, 20);

            //---- A1UNL ----
            A1UNL.setText("@A1UNL@");
            A1UNL.setBorder(new BevelBorder(BevelBorder.LOWERED));
            A1UNL.setAlignmentX(0.5F);
            A1UNL.setHorizontalAlignment(SwingConstants.CENTER);
            A1UNL.setName("A1UNL");
            panel2.add(A1UNL);
            A1UNL.setBounds(370, 73, 32, A1UNL.getPreferredSize().height);

            //---- ULLIB ----
            ULLIB.setText("@ULLIB@");
            ULLIB.setName("ULLIB");
            panel2.add(ULLIB);
            ULLIB.setBounds(410, 73, 126, ULLIB.getPreferredSize().height);

            //======== OBJ_103 ========
            {
              OBJ_103.setBorder(new TitledBorder("Mouvements"));
              OBJ_103.setOpaque(false);
              OBJ_103.setName("OBJ_103");
              OBJ_103.setLayout(null);

              //---- WTYPE4 ----
              WTYPE4.setText("Fabrication");
              WTYPE4.setComponentPopupMenu(BTD);
              WTYPE4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WTYPE4.setName("WTYPE4");
              OBJ_103.add(WTYPE4);
              WTYPE4.setBounds(20, 90, 92, 20);

              //---- WTYPE1 ----
              WTYPE1.setText("Achat");
              WTYPE1.setComponentPopupMenu(BTD);
              WTYPE1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WTYPE1.setName("WTYPE1");
              OBJ_103.add(WTYPE1);
              WTYPE1.setBounds(20, 30, 59, 20);

              //---- WTYPE2 ----
              WTYPE2.setText("Vente");
              WTYPE2.setComponentPopupMenu(BTD);
              WTYPE2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WTYPE2.setName("WTYPE2");
              OBJ_103.add(WTYPE2);
              WTYPE2.setBounds(20, 60, 57, 20);

              //---- WTYPE3 ----
              WTYPE3.setText("Stock");
              WTYPE3.setComponentPopupMenu(BTD);
              WTYPE3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WTYPE3.setName("WTYPE3");
              WTYPE3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  WTYPE3ActionPerformed(e);
                }
              });
              OBJ_103.add(WTYPE3);
              WTYPE3.setBounds(20, 120, 57, 20);

              //======== p_stock ========
              {
                p_stock.setOpaque(false);
                p_stock.setName("p_stock");
                p_stock.setLayout(null);

                //---- WOPE1 ----
                WOPE1.setText("Divers");
                WOPE1.setComponentPopupMenu(BTD);
                WOPE1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                WOPE1.setName("WOPE1");
                p_stock.add(WOPE1);
                WOPE1.setBounds(10, 10, 60, 20);

                //---- WOPE2 ----
                WOPE2.setText("Entr\u00e9e");
                WOPE2.setComponentPopupMenu(BTD);
                WOPE2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                WOPE2.setName("WOPE2");
                p_stock.add(WOPE2);
                WOPE2.setBounds(10, 40, 66, 20);

                //---- WOPE3 ----
                WOPE3.setText("Inventaire");
                WOPE3.setComponentPopupMenu(BTD);
                WOPE3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                WOPE3.setName("WOPE3");
                p_stock.add(WOPE3);
                WOPE3.setBounds(10, 70, 83, 20);

                //---- WOPE4 ----
                WOPE4.setText("Sortie");
                WOPE4.setComponentPopupMenu(BTD);
                WOPE4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                WOPE4.setName("WOPE4");
                p_stock.add(WOPE4);
                WOPE4.setBounds(10, 100, 60, 20);

                //---- WOPE5 ----
                WOPE5.setText("Transfert");
                WOPE5.setComponentPopupMenu(BTD);
                WOPE5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                WOPE5.setName("WOPE5");
                p_stock.add(WOPE5);
                WOPE5.setBounds(10, 130, 75, 20);
              }
              OBJ_103.add(p_stock);
              p_stock.setBounds(10, 140, 125, 155);
            }
            panel2.add(OBJ_103);
            OBJ_103.setBounds(665, 135, 160, 310);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //======== SCROLLPANE_LIST1 ========
              {
                SCROLLPANE_LIST1.setComponentPopupMenu(BTD);
                SCROLLPANE_LIST1.setName("SCROLLPANE_LIST1");

                //---- WT301 ----
                WT301.setComponentPopupMenu(BTD);
                WT301.setName("WT301");
                WT301.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    WT301MouseClicked(e);
                  }
                });
                SCROLLPANE_LIST1.setViewportView(WT301);
              }
              panel1.add(SCROLLPANE_LIST1);
              SCROLLPANE_LIST1.setBounds(15, 5, 595, 380);

              //---- BT_PGUP ----
              BT_PGUP.setText("");
              BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
              BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGUP.setName("BT_PGUP");
              panel1.add(BT_PGUP);
              BT_PGUP.setBounds(615, 5, 25, 155);

              //---- BT_PGDOWN ----
              BT_PGDOWN.setText("");
              BT_PGDOWN.setToolTipText("Page suivante");
              BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGDOWN.setName("BT_PGDOWN");
              panel1.add(BT_PGDOWN);
              BT_PGDOWN.setBounds(615, 230, 25, 155);
            }
            panel2.add(panel1);
            panel1.setBounds(5, 125, 655, 390);

            //---- OBJ_99 ----
            OBJ_99.setText("@A1CL2@");
            OBJ_99.setFont(new Font("sansserif", Font.BOLD, 11));
            OBJ_99.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_99.setName("OBJ_99");
            panel2.add(OBJ_99);
            OBJ_99.setBounds(710, 0, 135, 20);

            //---- OBJ_68 ----
            OBJ_68.setText("(en double quantit\u00e9)");
            OBJ_68.setFont(new Font("sansserif", Font.BOLD, 11));
            OBJ_68.setName("OBJ_68");
            panel2.add(OBJ_68);
            OBJ_68.setBounds(710, 20, 127, 18);

            //---- OBJ_105 ----
            OBJ_105.setText("@DSAC@");
            OBJ_105.setName("OBJ_105");
            panel2.add(OBJ_105);
            OBJ_105.setBounds(25, 20, 114, 20);

            //---- riBoutonDetail1 ----
            riBoutonDetail1.setName("riBoutonDetail1");
            riBoutonDetail1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetail1ActionPerformed(e);
              }
            });
            panel2.add(riBoutonDetail1);
            riBoutonDetail1.setBounds(210, 43, 30, 25);

            //---- SERDEB ----
            SERDEB.setName("SERDEB");
            panel2.add(SERDEB);
            SERDEB.setBounds(320, 515, 210, SERDEB.getPreferredSize().height);

            //---- OBJ_74 ----
            OBJ_74.setText("En unite de conditionnement");
            OBJ_74.setComponentPopupMenu(BTD);
            OBJ_74.setName("OBJ_74");
            panel2.add(OBJ_74);
            OBJ_74.setBounds(20, 515, 210, 28);

            //---- OBJ_49 ----
            OBJ_49.setText("En nombre de palette");
            OBJ_49.setComponentPopupMenu(BTD);
            OBJ_49.setName("OBJ_49");
            panel2.add(OBJ_49);
            OBJ_49.setBounds(20, 515, 210, 28);

            //---- OBJ_48 ----
            OBJ_48.setText("En m\u00e8tres lineaires");
            OBJ_48.setComponentPopupMenu(BTD);
            OBJ_48.setName("OBJ_48");
            panel2.add(OBJ_48);
            OBJ_48.setBounds(20, 515, 190, 28);

            //---- MARDEB ----
            MARDEB.setName("MARDEB");
            panel2.add(MARDEB);
            MARDEB.setBounds(535, 515, 80, MARDEB.getPreferredSize().height);

            //---- OBJ_124 ----
            OBJ_124.setText("N\u00b0 S\u00e9rie");
            OBJ_124.setName("OBJ_124");
            panel2.add(OBJ_124);
            OBJ_124.setBounds(260, 515, 61, 28);

            //---- label3 ----
            label3.setText("Affaire");
            label3.setName("label3");
            panel2.add(label3);
            label3.setBounds(25, 105, 100, 20);

            //---- WAFFR ----
            WAFFR.setComponentPopupMenu(BTD);
            WAFFR.setName("WAFFR");
            panel2.add(WAFFR);
            WAFFR.setBounds(130, 101, 160, WAFFR.getPreferredSize().height);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 850, GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 556, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_29 ----
      OBJ_29.setText("Premi\u00e8re valeur");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD.add(OBJ_29);

      //---- OBJ_34 ----
      OBJ_34.setText("Re-chiffrage");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed(e);
        }
      });
      BTD.add(OBJ_34);

      //---- OBJ_35 ----
      OBJ_35.setText("Historique de vente");
      OBJ_35.setName("OBJ_35");
      OBJ_35.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed(e);
        }
      });
      BTD.add(OBJ_35);

      //---- OBJ_36 ----
      OBJ_36.setText("Lien vente / achat");
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      BTD.add(OBJ_36);

      //---- OBJ_39 ----
      OBJ_39.setText("Restriction sur affaire");
      OBJ_39.setName("OBJ_39");
      OBJ_39.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_39ActionPerformed(e);
        }
      });
      BTD.add(OBJ_39);
      BTD.addSeparator();

      //---- OBJ_37 ----
      OBJ_37.setText("Aide en ligne");
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_37ActionPerformed(e);
        }
      });
      BTD.add(OBJ_37);

      //---- OBJ_38 ----
      OBJ_38.setText("Invite");
      OBJ_38.setName("OBJ_38");
      OBJ_38.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_38ActionPerformed(e);
        }
      });
      BTD.add(OBJ_38);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_46 ----
      OBJ_46.setText("Choix possibles");
      OBJ_46.setName("OBJ_46");
      OBJ_46.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_46ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_46);

      //---- OBJ_45 ----
      OBJ_45.setText("Aide en ligne");
      OBJ_45.setName("OBJ_45");
      OBJ_45.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_45ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_45);
    }

    //---- X13DVX ----
    X13DVX.setName("X13DVX");

    //======== riSousMenu18 ========
    {
      riSousMenu18.setName("riSousMenu18");

      //---- riSousMenu_bt18 ----
      riSousMenu_bt18.setText("Affichage des lots");
      riSousMenu_bt18.setToolTipText("Affichage des lot");
      riSousMenu_bt18.setName("riSousMenu_bt18");
      riSousMenu_bt18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt18ActionPerformed(e);
        }
      });
      riSousMenu18.add(riSousMenu_bt18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField H2ART;
  private JLabel OBJ_67;
  private JLabel OBJ_66;
  private XRiTextField H2ETB;
  private JLabel OBJ_69;
  private XRiTextField H2MAG;
  private RiZoneSortie MALIB;
  private JPanel p_tete_droite;
  private JLabel OBJ_94;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt3;
  private RiSousMenu riSousMenu4;
  private RiSousMenu_bt riSousMenu_bt4;
  private RiSousMenu riSousMenu5;
  private RiSousMenu_bt riSousMenu_bt5;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField WMIN;
  private XRiTextField WMAX;
  private XRiTextField WTIE2;
  private JLabel OBJ_88;
  private XRiTextField WTIE3;
  private XRiTextField WTIE1;
  private JLabel OBJ_108;
  private XRiCalendrier DATDEB;
  private JLabel label1;
  private JLabel label2;
  private JLabel OBJ_71;
  private XRiTextField A1UNS;
  private RiZoneSortie UNLIB;
  private JLabel OBJ_110;
  private RiZoneSortie A1UNL;
  private RiZoneSortie ULLIB;
  private JPanel OBJ_103;
  private XRiCheckBox WTYPE4;
  private XRiCheckBox WTYPE1;
  private XRiCheckBox WTYPE2;
  private XRiCheckBox WTYPE3;
  private JPanel p_stock;
  private XRiCheckBox WOPE1;
  private XRiCheckBox WOPE2;
  private XRiCheckBox WOPE3;
  private XRiCheckBox WOPE4;
  private XRiCheckBox WOPE5;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST1;
  private XRiTable WT301;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_99;
  private JLabel OBJ_68;
  private JLabel OBJ_105;
  private SNBoutonDetail riBoutonDetail1;
  private XRiTextField SERDEB;
  private JLabel OBJ_74;
  private JLabel OBJ_49;
  private JLabel OBJ_48;
  private XRiTextField MARDEB;
  private JLabel OBJ_124;
  private JLabel label3;
  private XRiTextField WAFFR;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_34;
  private JMenuItem OBJ_35;
  private JMenuItem OBJ_36;
  private JMenuItem OBJ_39;
  private JMenuItem OBJ_37;
  private JMenuItem OBJ_38;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_46;
  private JMenuItem OBJ_45;
  private XRiTextField X13DVX;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
