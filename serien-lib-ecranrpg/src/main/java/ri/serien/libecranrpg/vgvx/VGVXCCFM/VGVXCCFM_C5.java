
package ri.serien.libecranrpg.vgvx.VGVXCCFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;

/**
 * @author Stéphane Vénéri
 */
public class VGVXCCFM_C5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXCCFM_C5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // CCDAFX.setEnabled( lexique.isPresent("CCDAFX"));
    // CCDADX.setEnabled( lexique.isPresent("CCDADX"));
    // CCVEFX.setEnabled( lexique.isPresent("CCVEFX"));
    // CCVEDX.setEnabled( lexique.isPresent("CCVEDX"));
    // CCVIFX.setEnabled( lexique.isPresent("CCVIFX"));
    // CCVIDX.setEnabled( lexique.isPresent("CCVIDX"));
    // CCMDFX.setEnabled( lexique.isPresent("CCMDFX"));
    // CCMDDX.setEnabled( lexique.isPresent("CCMDDX"));
    // CCCRFX.setEnabled( lexique.isPresent("CCCRFX"));
    // CCCRDX.setEnabled( lexique.isPresent("CCCRDX"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Critères de sélection"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_ann = new RiSousMenu();
    riSousMenu_bt_ann = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_25 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_21 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_33 = new JLabel();
    OBJ_29 = new JLabel();
    CCCRDX = new XRiCalendrier();
    CCCRFX = new XRiCalendrier();
    CCMDDX = new XRiCalendrier();
    CCMDFX = new XRiCalendrier();
    CCVIDX = new XRiCalendrier();
    CCVIFX = new XRiCalendrier();
    CCVEDX = new XRiCalendrier();
    CCVEFX = new XRiCalendrier();
    CCDADX = new XRiCalendrier();
    CCDAFX = new XRiCalendrier();
    OBJ_23 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_39 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(700, 250));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_ann ========
          {
            riSousMenu_ann.setName("riSousMenu_ann");

            //---- riSousMenu_bt_ann ----
            riSousMenu_bt_ann.setText("Annulation");
            riSousMenu_bt_ann.setToolTipText("Annulation");
            riSousMenu_bt_ann.setName("riSousMenu_bt_ann");
            riSousMenu_ann.add(riSousMenu_bt_ann);
          }
          menus_haut.add(riSousMenu_ann);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setToolTipText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Dates"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_25 ----
          OBJ_25.setText("Date de modification");
          OBJ_25.setName("OBJ_25");
          p_recup.add(OBJ_25);
          OBJ_25.setBounds(25, 76, 150, 20);

          //---- OBJ_37 ----
          OBJ_37.setText("Date anniversaire");
          OBJ_37.setName("OBJ_37");
          p_recup.add(OBJ_37);
          OBJ_37.setBounds(25, 172, 150, 20);

          //---- OBJ_21 ----
          OBJ_21.setText("Date de cr\u00e9ation");
          OBJ_21.setName("OBJ_21");
          p_recup.add(OBJ_21);
          OBJ_21.setBounds(25, 44, 150, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_33 ----
          OBJ_33.setText("Derni\u00e8re vente");
          OBJ_33.setName("OBJ_33");
          p_recup.add(OBJ_33);
          OBJ_33.setBounds(25, 140, 150, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("Derni\u00e8re visite");
          OBJ_29.setName("OBJ_29");
          p_recup.add(OBJ_29);
          OBJ_29.setBounds(25, 108, 150, 20);

          //---- CCCRDX ----
          CCCRDX.setComponentPopupMenu(BTD);
          CCCRDX.setName("CCCRDX");
          p_recup.add(CCCRDX);
          CCCRDX.setBounds(175, 40, 105, CCCRDX.getPreferredSize().height);

          //---- CCCRFX ----
          CCCRFX.setComponentPopupMenu(BTD);
          CCCRFX.setName("CCCRFX");
          p_recup.add(CCCRFX);
          CCCRFX.setBounds(374, 40, 105, CCCRFX.getPreferredSize().height);

          //---- CCMDDX ----
          CCMDDX.setComponentPopupMenu(BTD);
          CCMDDX.setName("CCMDDX");
          p_recup.add(CCMDDX);
          CCMDDX.setBounds(175, 72, 105, CCMDDX.getPreferredSize().height);

          //---- CCMDFX ----
          CCMDFX.setComponentPopupMenu(BTD);
          CCMDFX.setName("CCMDFX");
          p_recup.add(CCMDFX);
          CCMDFX.setBounds(374, 72, 105, CCMDFX.getPreferredSize().height);

          //---- CCVIDX ----
          CCVIDX.setComponentPopupMenu(BTD);
          CCVIDX.setName("CCVIDX");
          p_recup.add(CCVIDX);
          CCVIDX.setBounds(175, 104, 105, CCVIDX.getPreferredSize().height);

          //---- CCVIFX ----
          CCVIFX.setComponentPopupMenu(BTD);
          CCVIFX.setName("CCVIFX");
          p_recup.add(CCVIFX);
          CCVIFX.setBounds(374, 104, 105, CCVIFX.getPreferredSize().height);

          //---- CCVEDX ----
          CCVEDX.setComponentPopupMenu(BTD);
          CCVEDX.setName("CCVEDX");
          p_recup.add(CCVEDX);
          CCVEDX.setBounds(175, 136, 105, CCVEDX.getPreferredSize().height);

          //---- CCVEFX ----
          CCVEFX.setComponentPopupMenu(BTD);
          CCVEFX.setName("CCVEFX");
          p_recup.add(CCVEFX);
          CCVEFX.setBounds(374, 136, 105, CCVEFX.getPreferredSize().height);

          //---- CCDADX ----
          CCDADX.setComponentPopupMenu(BTD);
          CCDADX.setName("CCDADX");
          p_recup.add(CCDADX);
          CCDADX.setBounds(175, 168, 105, CCDADX.getPreferredSize().height);

          //---- CCDAFX ----
          CCDAFX.setComponentPopupMenu(BTD);
          CCDAFX.setName("CCDAFX");
          p_recup.add(CCDAFX);
          CCDAFX.setBounds(374, 168, 105, CCDAFX.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("\u00e0");
          OBJ_23.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_23.setName("OBJ_23");
          p_recup.add(OBJ_23);
          OBJ_23.setBounds(287, 44, 80, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("\u00e0");
          OBJ_27.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(287, 76, 80, 20);

          //---- OBJ_31 ----
          OBJ_31.setText("\u00e0");
          OBJ_31.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_31.setName("OBJ_31");
          p_recup.add(OBJ_31);
          OBJ_31.setBounds(287, 108, 80, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("\u00e0");
          OBJ_35.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_35.setName("OBJ_35");
          p_recup.add(OBJ_35);
          OBJ_35.setBounds(287, 140, 80, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("\u00e0");
          OBJ_39.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_39.setName("OBJ_39");
          p_recup.add(OBJ_39);
          OBJ_39.setBounds(287, 172, 80, 20);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(10, 10, 510, 225);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_ann;
  private RiSousMenu_bt riSousMenu_bt_ann;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_25;
  private JLabel OBJ_37;
  private JLabel OBJ_21;
  private JPanel P_PnlOpts;
  private JLabel OBJ_33;
  private JLabel OBJ_29;
  private XRiCalendrier CCCRDX;
  private XRiCalendrier CCCRFX;
  private XRiCalendrier CCMDDX;
  private XRiCalendrier CCMDFX;
  private XRiCalendrier CCVIDX;
  private XRiCalendrier CCVIFX;
  private XRiCalendrier CCVEDX;
  private XRiCalendrier CCVEFX;
  private XRiCalendrier CCDADX;
  private XRiCalendrier CCDAFX;
  private JLabel OBJ_23;
  private JLabel OBJ_27;
  private JLabel OBJ_31;
  private JLabel OBJ_35;
  private JLabel OBJ_39;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
