
package ri.serien.libecranrpg.vgvx.VGVX81FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class VGVX81FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX81FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    HLART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLART@")).trim());
    OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    WORI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WORI@")).trim());
    HLMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLMAG@")).trim());
    HLORI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLORI@")).trim());
    WOPE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WOPE@")).trim());
    HLQTE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLQTE@")).trim());
    HLQTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLQTP@")).trim());
    HLPRI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLPRI@")).trim());
    HLDATX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLDATX@")).trim());
    HLORD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLORD@")).trim());
    HLOPE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLOPE@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNOMFR@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLDTEX@")).trim());
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLDPEX@")).trim());
    WNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNOM@")).trim());
    HLTIEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLTIEX@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIER@")).trim());
    HLNUM0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLNUM0@")).trim());
    HLNLI0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLNI0@")).trim());
    HLSUF0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLSUF0@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    


    
    
    HLSUF0.setEnabled(lexique.isPresent("HLSUF0"));
    HLOPE.setEnabled(lexique.isPresent("HLOPE"));
    HLORI.setEnabled(lexique.isPresent("HLORI"));
    HLMAG.setEnabled(lexique.isPresent("HLMAG"));
    HLNLI0.setEnabled(lexique.isPresent("HLNLI0"));
    HLNUM0.setEnabled(lexique.isPresent("HLNUM0"));
    HLORD.setEnabled(lexique.isPresent("HLORD"));
    HLDATX.setEnabled(lexique.isPresent("HLDATX"));
    OBJ_54.setVisible(lexique.isPresent("HLDPEX"));
    OBJ_46.setVisible(lexique.isPresent("HLDTEX"));
    HLPRI.setEnabled(lexique.isPresent("HLPRI"));
    HLQTP.setEnabled(lexique.isPresent("HLQTP"));
    HLQTE.setEnabled(lexique.isPresent("HLQTE"));
    HLTIEX.setEnabled(lexique.isPresent("HLTIEX"));
    WOPE.setEnabled(lexique.isPresent("WOPE"));
    WORI.setEnabled(lexique.isPresent("WORI"));
    OBJ_37.setVisible(lexique.isPresent("A1LIB"));
    OBJ_42.setVisible(lexique.isPresent("WNOMFR"));
    WNOM.setEnabled(lexique.isPresent("WNOM"));
    HLART.setEnabled(lexique.isPresent("HLART"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@ @HLLOT@"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", true);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    HLART = new RiZoneSortie();
    OBJ_37 = new RiZoneSortie();
    WORI = new RiZoneSortie();
    OBJ_23 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_67 = new JLabel();
    HLMAG = new RiZoneSortie();
    HLORI = new RiZoneSortie();
    label1 = new JLabel();
    panel3 = new JPanel();
    WOPE = new RiZoneSortie();
    HLQTE = new RiZoneSortie();
    HLQTP = new RiZoneSortie();
    OBJ_52 = new JLabel();
    HLPRI = new RiZoneSortie();
    OBJ_44 = new JLabel();
    HLDATX = new RiZoneSortie();
    HLORD = new RiZoneSortie();
    OBJ_58 = new JLabel();
    OBJ_39 = new JLabel();
    HLOPE = new RiZoneSortie();
    panel2 = new JPanel();
    OBJ_42 = new RiZoneSortie();
    OBJ_45 = new JLabel();
    OBJ_46 = new RiZoneSortie();
    OBJ_53 = new JLabel();
    OBJ_54 = new RiZoneSortie();
    panel1 = new JPanel();
    WNOM = new RiZoneSortie();
    OBJ_47 = new JLabel();
    HLTIEX = new RiZoneSortie();
    OBJ_55 = new JLabel();
    HLNUM0 = new RiZoneSortie();
    HLNLI0 = new RiZoneSortie();
    OBJ_50 = new JLabel();
    HLSUF0 = new RiZoneSortie();
    OBJ_66 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_29 = new JLabel();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(870, 440));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Affichage \u00e9tendu");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Acc\u00e8s \u00e0 l'historique du lot");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel4 ========
        {
          panel4.setBorder(new TitledBorder(""));
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);

          //---- HLART ----
          HLART.setText("@HLART@");
          HLART.setName("HLART");
          panel4.add(HLART);
          HLART.setBounds(120, 15, 210, HLART.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("@A1LIB@");
          OBJ_37.setName("OBJ_37");
          panel4.add(OBJ_37);
          OBJ_37.setBounds(120, 45, 231, OBJ_37.getPreferredSize().height);

          //---- WORI ----
          WORI.setText("@WORI@");
          WORI.setName("WORI");
          panel4.add(WORI);
          WORI.setBounds(480, 15, 140, WORI.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("Code article");
          OBJ_23.setName("OBJ_23");
          panel4.add(OBJ_23);
          OBJ_23.setBounds(15, 17, 100, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("Magasin");
          OBJ_26.setName("OBJ_26");
          panel4.add(OBJ_26);
          OBJ_26.setBounds(390, 47, 65, 20);

          //---- OBJ_67 ----
          OBJ_67.setText("Type");
          OBJ_67.setName("OBJ_67");
          panel4.add(OBJ_67);
          OBJ_67.setBounds(390, 17, 65, 20);

          //---- HLMAG ----
          HLMAG.setText("@HLMAG@");
          HLMAG.setName("HLMAG");
          panel4.add(HLMAG);
          HLMAG.setBounds(455, 45, 34, HLMAG.getPreferredSize().height);

          //---- HLORI ----
          HLORI.setText("@HLORI@");
          HLORI.setName("HLORI");
          panel4.add(HLORI);
          HLORI.setBounds(455, 15, 20, HLORI.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Libell\u00e9 article");
          label1.setName("label1");
          panel4.add(label1);
          label1.setBounds(15, 47, 100, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel4);
        panel4.setBounds(15, 20, 665, 90);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("D\u00e9tail"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- WOPE ----
          WOPE.setText("@WOPE@");
          WOPE.setName("WOPE");
          panel3.add(WOPE);
          WOPE.setBounds(40, 35, 140, WOPE.getPreferredSize().height);

          //---- HLQTE ----
          HLQTE.setText("@HLQTE@");
          HLQTE.setHorizontalAlignment(SwingConstants.RIGHT);
          HLQTE.setName("HLQTE");
          panel3.add(HLQTE);
          HLQTE.setBounds(120, 70, 114, HLQTE.getPreferredSize().height);

          //---- HLQTP ----
          HLQTP.setText("@HLQTP@");
          HLQTP.setHorizontalAlignment(SwingConstants.RIGHT);
          HLQTP.setName("HLQTP");
          panel3.add(HLQTP);
          HLQTP.setBounds(120, 100, 114, HLQTP.getPreferredSize().height);

          //---- OBJ_52 ----
          OBJ_52.setText("Quantit\u00e9 2");
          OBJ_52.setName("OBJ_52");
          panel3.add(OBJ_52);
          OBJ_52.setBounds(15, 104, 85, 20);

          //---- HLPRI ----
          HLPRI.setText("@HLPRI@");
          HLPRI.setHorizontalAlignment(SwingConstants.RIGHT);
          HLPRI.setName("HLPRI");
          panel3.add(HLPRI);
          HLPRI.setBounds(120, 130, 98, HLPRI.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Quantit\u00e9");
          OBJ_44.setName("OBJ_44");
          panel3.add(OBJ_44);
          OBJ_44.setBounds(15, 74, 85, 20);

          //---- HLDATX ----
          HLDATX.setText("@HLDATX@");
          HLDATX.setName("HLDATX");
          panel3.add(HLDATX);
          HLDATX.setBounds(285, 35, 75, HLDATX.getPreferredSize().height);

          //---- HLORD ----
          HLORD.setText("@HLORD@");
          HLORD.setName("HLORD");
          panel3.add(HLORD);
          HLORD.setBounds(185, 35, 66, HLORD.getPreferredSize().height);

          //---- OBJ_58 ----
          OBJ_58.setText("Prix");
          OBJ_58.setName("OBJ_58");
          panel3.add(OBJ_58);
          OBJ_58.setBounds(15, 134, 85, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("du");
          OBJ_39.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_39.setName("OBJ_39");
          panel3.add(OBJ_39);
          OBJ_39.setBounds(255, 37, 25, 20);

          //---- HLOPE ----
          HLOPE.setText("@HLOPE@");
          HLOPE.setName("HLOPE");
          panel3.add(HLOPE);
          HLOPE.setBounds(15, 35, 20, HLOPE.getPreferredSize().height);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Fournisseur"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_42 ----
            OBJ_42.setText("@WNOMFR@");
            OBJ_42.setName("OBJ_42");
            panel2.add(OBJ_42);
            OBJ_42.setBounds(15, 30, 232, OBJ_42.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setText("Date entr\u00e9e");
            OBJ_45.setName("OBJ_45");
            panel2.add(OBJ_45);
            OBJ_45.setBounds(15, 62, 95, 20);

            //---- OBJ_46 ----
            OBJ_46.setText("@HLDTEX@");
            OBJ_46.setName("OBJ_46");
            panel2.add(OBJ_46);
            OBJ_46.setBounds(115, 60, 75, OBJ_46.getPreferredSize().height);

            //---- OBJ_53 ----
            OBJ_53.setText("Date limite");
            OBJ_53.setName("OBJ_53");
            panel2.add(OBJ_53);
            OBJ_53.setBounds(15, 92, 95, 20);

            //---- OBJ_54 ----
            OBJ_54.setText("@HLDPEX@");
            OBJ_54.setName("OBJ_54");
            panel2.add(OBJ_54);
            OBJ_54.setBounds(115, 90, 75, OBJ_54.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel3.add(panel2);
          panel2.setBounds(380, 40, 270, 130);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(15, 115, 665, 185);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Document d'origine du mouvement"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WNOM ----
          WNOM.setText("@WNOM@");
          WNOM.setName("WNOM");
          panel1.add(WNOM);
          WNOM.setBounds(285, 65, 310, WNOM.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setText("Num\u00e9ro de bon");
          OBJ_47.setName("OBJ_47");
          panel1.add(OBJ_47);
          OBJ_47.setBounds(15, 37, 100, 20);

          //---- HLTIEX ----
          HLTIEX.setText("@HLTIEX@");
          HLTIEX.setName("HLTIEX");
          panel1.add(HLTIEX);
          HLTIEX.setBounds(120, 65, 110, HLTIEX.getPreferredSize().height);

          //---- OBJ_55 ----
          OBJ_55.setText("@TIER@");
          OBJ_55.setName("OBJ_55");
          panel1.add(OBJ_55);
          OBJ_55.setBounds(15, 67, 90, 20);

          //---- HLNUM0 ----
          HLNUM0.setText("@HLNUM0@");
          HLNUM0.setName("HLNUM0");
          panel1.add(HLNUM0);
          HLNUM0.setBounds(120, 35, 58, HLNUM0.getPreferredSize().height);

          //---- HLNLI0 ----
          HLNLI0.setText("@HLNI0@");
          HLNLI0.setName("HLNLI0");
          panel1.add(HLNLI0);
          HLNLI0.setBounds(285, 35, 42, HLNLI0.getPreferredSize().height);

          //---- OBJ_50 ----
          OBJ_50.setText("Ligne");
          OBJ_50.setName("OBJ_50");
          panel1.add(OBJ_50);
          OBJ_50.setBounds(235, 37, 50, 20);

          //---- HLSUF0 ----
          HLSUF0.setText("@HLSUF0@");
          HLSUF0.setName("HLSUF0");
          panel1.add(HLSUF0);
          HLSUF0.setBounds(185, 35, 20, HLSUF0.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 310, 665, 115);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- OBJ_66 ----
    OBJ_66.setText("Document d'origine du mouvement");
    OBJ_66.setName("OBJ_66");

    //---- OBJ_40 ----
    OBJ_40.setText("Fournisseur");
    OBJ_40.setName("OBJ_40");

    //---- OBJ_29 ----
    OBJ_29.setText("D\u00e9tail");
    OBJ_29.setName("OBJ_29");

    //======== riSousMenu6 ========
    {
      riSousMenu6.setName("riSousMenu6");

      //---- riSousMenu_bt6 ----
      riSousMenu_bt6.setText("Documents li\u00e9s");
      riSousMenu_bt6.setName("riSousMenu_bt6");
      riSousMenu_bt6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt6ActionPerformed(e);
        }
      });
      riSousMenu6.add(riSousMenu_bt6);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private JPanel p_contenu;
  private JPanel panel4;
  private RiZoneSortie HLART;
  private RiZoneSortie OBJ_37;
  private RiZoneSortie WORI;
  private JLabel OBJ_23;
  private JLabel OBJ_26;
  private JLabel OBJ_67;
  private RiZoneSortie HLMAG;
  private RiZoneSortie HLORI;
  private JLabel label1;
  private JPanel panel3;
  private RiZoneSortie WOPE;
  private RiZoneSortie HLQTE;
  private RiZoneSortie HLQTP;
  private JLabel OBJ_52;
  private RiZoneSortie HLPRI;
  private JLabel OBJ_44;
  private RiZoneSortie HLDATX;
  private RiZoneSortie HLORD;
  private JLabel OBJ_58;
  private JLabel OBJ_39;
  private RiZoneSortie HLOPE;
  private JPanel panel2;
  private RiZoneSortie OBJ_42;
  private JLabel OBJ_45;
  private RiZoneSortie OBJ_46;
  private JLabel OBJ_53;
  private RiZoneSortie OBJ_54;
  private JPanel panel1;
  private RiZoneSortie WNOM;
  private JLabel OBJ_47;
  private RiZoneSortie HLTIEX;
  private JLabel OBJ_55;
  private RiZoneSortie HLNUM0;
  private RiZoneSortie HLNLI0;
  private JLabel OBJ_50;
  private RiZoneSortie HLSUF0;
  private JLabel OBJ_66;
  private JLabel OBJ_40;
  private JLabel OBJ_29;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
