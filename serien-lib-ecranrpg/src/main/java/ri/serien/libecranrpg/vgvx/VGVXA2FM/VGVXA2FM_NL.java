
package ri.serien.libecranrpg.vgvx.VGVXA2FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGVXA2FM_NL extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXA2FM_NL(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("Saisie des adresses de stockage pour l'article @PA2ART@ @A1LIBR@")).trim()));
    PA2MAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2MAG@")).trim());
    PA2QTX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2QTX@")).trim());
    NUM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM1@")).trim());
    QTSX1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX1@")).trim());
    QTRX1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTRX1@")).trim());
    NUM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM2@")).trim());
    NUM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM3@")).trim());
    NUM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM4@")).trim());
    NUM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM5@")).trim());
    NUM6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM6@")).trim());
    NUM7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM7@")).trim());
    NUM8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM8@")).trim());
    NUM9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM9@")).trim());
    NUM10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM10@")).trim());
    QTSX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX2@")).trim());
    QTRX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTRX2@")).trim());
    QTSX3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX3@")).trim());
    QTRX3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTRX3@")).trim());
    QTSX4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX4@")).trim());
    QTRX4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTRX4@")).trim());
    QTSX5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX5@")).trim());
    QTRX5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTRX5@")).trim());
    QTSX6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX6@")).trim());
    QTRX6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTRX6@")).trim());
    QTSX7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX7@")).trim());
    QTRX7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTRX7@")).trim());
    QTSX8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX8@")).trim());
    QTRX8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTRX8@")).trim());
    QTSX9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX9@")).trim());
    QTRX9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTRX9@")).trim());
    QTSX10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX10@")).trim());
    QTRX10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTRX10@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AD1@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AD2@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AD3@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AD4@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    if (lexique.isTrue("(40) AND (N23)")) {
      labelEtat.setText("(ENTREE)");
    }
    else if (lexique.isTrue("(N40) AND (N23)")) {
      labelEtat.setText("(SORTIE)");
    }
    else if (lexique.isTrue("23")) {
      labelEtat.setText("(AVOIR)");
    }
    
    // Titre
    setTitle("Gestion des historiques de stock par adresses");
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_22 = new JLabel();
    PA2MAG = new RiZoneSortie();
    PA2QTX = new RiZoneSortie();
    OBJ_23 = new JLabel();
    NUM1 = new RiZoneSortie();
    AD101 = new XRiTextField();
    AD201 = new XRiTextField();
    AD301 = new XRiTextField();
    AD401 = new XRiTextField();
    AD102 = new XRiTextField();
    AD202 = new XRiTextField();
    AD302 = new XRiTextField();
    AD402 = new XRiTextField();
    AD103 = new XRiTextField();
    AD203 = new XRiTextField();
    AD303 = new XRiTextField();
    AD403 = new XRiTextField();
    AD104 = new XRiTextField();
    AD204 = new XRiTextField();
    AD304 = new XRiTextField();
    AD404 = new XRiTextField();
    AD105 = new XRiTextField();
    AD205 = new XRiTextField();
    AD305 = new XRiTextField();
    AD405 = new XRiTextField();
    AD106 = new XRiTextField();
    AD206 = new XRiTextField();
    AD306 = new XRiTextField();
    AD406 = new XRiTextField();
    AD107 = new XRiTextField();
    AD207 = new XRiTextField();
    AD307 = new XRiTextField();
    AD407 = new XRiTextField();
    AD108 = new XRiTextField();
    AD208 = new XRiTextField();
    AD308 = new XRiTextField();
    AD408 = new XRiTextField();
    AD109 = new XRiTextField();
    AD209 = new XRiTextField();
    AD309 = new XRiTextField();
    AD409 = new XRiTextField();
    AD110 = new XRiTextField();
    AD210 = new XRiTextField();
    AD310 = new XRiTextField();
    AD410 = new XRiTextField();
    TYP01 = new XRiTextField();
    TYP02 = new XRiTextField();
    TYP03 = new XRiTextField();
    TYP04 = new XRiTextField();
    TYP05 = new XRiTextField();
    TYP06 = new XRiTextField();
    TYP07 = new XRiTextField();
    TYP08 = new XRiTextField();
    TYP09 = new XRiTextField();
    TYP10 = new XRiTextField();
    QTSX1 = new RiZoneSortie();
    QTRX1 = new RiZoneSortie();
    QTEX1 = new XRiTextField();
    NUM2 = new RiZoneSortie();
    NUM3 = new RiZoneSortie();
    NUM4 = new RiZoneSortie();
    NUM5 = new RiZoneSortie();
    NUM6 = new RiZoneSortie();
    NUM7 = new RiZoneSortie();
    NUM8 = new RiZoneSortie();
    NUM9 = new RiZoneSortie();
    NUM10 = new RiZoneSortie();
    QTSX2 = new RiZoneSortie();
    QTRX2 = new RiZoneSortie();
    QTEX2 = new XRiTextField();
    QTSX3 = new RiZoneSortie();
    QTRX3 = new RiZoneSortie();
    QTEX3 = new XRiTextField();
    QTSX4 = new RiZoneSortie();
    QTRX4 = new RiZoneSortie();
    QTEX4 = new XRiTextField();
    QTSX5 = new RiZoneSortie();
    QTRX5 = new RiZoneSortie();
    QTEX5 = new XRiTextField();
    QTSX6 = new RiZoneSortie();
    QTRX6 = new RiZoneSortie();
    QTEX6 = new XRiTextField();
    QTSX7 = new RiZoneSortie();
    QTRX7 = new RiZoneSortie();
    QTEX7 = new XRiTextField();
    QTSX8 = new RiZoneSortie();
    QTRX8 = new RiZoneSortie();
    QTEX8 = new XRiTextField();
    QTSX9 = new RiZoneSortie();
    QTRX9 = new RiZoneSortie();
    QTEX9 = new XRiTextField();
    QTSX10 = new RiZoneSortie();
    QTRX10 = new RiZoneSortie();
    QTEX10 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    labelEtat = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(870, 480));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Saisie des adresses de stockage pour l'article @PA2ART@ @A1LIBR@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_22 ----
          OBJ_22.setText("Magasin");
          OBJ_22.setName("OBJ_22");
          panel1.add(OBJ_22);
          OBJ_22.setBounds(530, 30, 60, 28);

          //---- PA2MAG ----
          PA2MAG.setText("@PA2MAG@");
          PA2MAG.setName("PA2MAG");
          panel1.add(PA2MAG);
          PA2MAG.setBounds(596, 32, 34, PA2MAG.getPreferredSize().height);

          //---- PA2QTX ----
          PA2QTX.setHorizontalAlignment(SwingConstants.RIGHT);
          PA2QTX.setText("@PA2QTX@");
          PA2QTX.setName("PA2QTX");
          panel1.add(PA2QTX);
          PA2QTX.setBounds(530, 405, 100, PA2QTX.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("Quantit\u00e9 totale");
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(420, 403, 110, 28);

          //---- NUM1 ----
          NUM1.setText("@NUM1@");
          NUM1.setName("NUM1");
          panel1.add(NUM1);
          NUM1.setBounds(25, 95, 36, NUM1.getPreferredSize().height);

          //---- AD101 ----
          AD101.setName("AD101");
          panel1.add(AD101);
          AD101.setBounds(70, 93, 40, AD101.getPreferredSize().height);

          //---- AD201 ----
          AD201.setName("AD201");
          panel1.add(AD201);
          AD201.setBounds(125, 93, 40, 28);

          //---- AD301 ----
          AD301.setName("AD301");
          panel1.add(AD301);
          AD301.setBounds(175, 93, 40, 28);

          //---- AD401 ----
          AD401.setName("AD401");
          panel1.add(AD401);
          AD401.setBounds(225, 93, 40, 28);

          //---- AD102 ----
          AD102.setName("AD102");
          panel1.add(AD102);
          AD102.setBounds(70, 125, 40, AD102.getPreferredSize().height);

          //---- AD202 ----
          AD202.setName("AD202");
          panel1.add(AD202);
          AD202.setBounds(125, 125, 40, AD202.getPreferredSize().height);

          //---- AD302 ----
          AD302.setName("AD302");
          panel1.add(AD302);
          AD302.setBounds(175, 125, 40, AD302.getPreferredSize().height);

          //---- AD402 ----
          AD402.setName("AD402");
          panel1.add(AD402);
          AD402.setBounds(225, 125, 40, AD402.getPreferredSize().height);

          //---- AD103 ----
          AD103.setName("AD103");
          panel1.add(AD103);
          AD103.setBounds(70, 155, 40, AD103.getPreferredSize().height);

          //---- AD203 ----
          AD203.setName("AD203");
          panel1.add(AD203);
          AD203.setBounds(125, 155, 40, AD203.getPreferredSize().height);

          //---- AD303 ----
          AD303.setName("AD303");
          panel1.add(AD303);
          AD303.setBounds(175, 155, 40, AD303.getPreferredSize().height);

          //---- AD403 ----
          AD403.setName("AD403");
          panel1.add(AD403);
          AD403.setBounds(225, 155, 40, AD403.getPreferredSize().height);

          //---- AD104 ----
          AD104.setName("AD104");
          panel1.add(AD104);
          AD104.setBounds(70, 185, 40, AD104.getPreferredSize().height);

          //---- AD204 ----
          AD204.setName("AD204");
          panel1.add(AD204);
          AD204.setBounds(125, 185, 40, AD204.getPreferredSize().height);

          //---- AD304 ----
          AD304.setName("AD304");
          panel1.add(AD304);
          AD304.setBounds(175, 185, 40, AD304.getPreferredSize().height);

          //---- AD404 ----
          AD404.setName("AD404");
          panel1.add(AD404);
          AD404.setBounds(225, 185, 40, AD404.getPreferredSize().height);

          //---- AD105 ----
          AD105.setName("AD105");
          panel1.add(AD105);
          AD105.setBounds(70, 215, 40, AD105.getPreferredSize().height);

          //---- AD205 ----
          AD205.setName("AD205");
          panel1.add(AD205);
          AD205.setBounds(125, 215, 40, AD205.getPreferredSize().height);

          //---- AD305 ----
          AD305.setName("AD305");
          panel1.add(AD305);
          AD305.setBounds(175, 215, 40, AD305.getPreferredSize().height);

          //---- AD405 ----
          AD405.setName("AD405");
          panel1.add(AD405);
          AD405.setBounds(225, 215, 40, AD405.getPreferredSize().height);

          //---- AD106 ----
          AD106.setName("AD106");
          panel1.add(AD106);
          AD106.setBounds(70, 245, 40, AD106.getPreferredSize().height);

          //---- AD206 ----
          AD206.setName("AD206");
          panel1.add(AD206);
          AD206.setBounds(125, 245, 40, AD206.getPreferredSize().height);

          //---- AD306 ----
          AD306.setName("AD306");
          panel1.add(AD306);
          AD306.setBounds(175, 245, 40, AD306.getPreferredSize().height);

          //---- AD406 ----
          AD406.setName("AD406");
          panel1.add(AD406);
          AD406.setBounds(225, 245, 40, AD406.getPreferredSize().height);

          //---- AD107 ----
          AD107.setName("AD107");
          panel1.add(AD107);
          AD107.setBounds(70, 275, 40, AD107.getPreferredSize().height);

          //---- AD207 ----
          AD207.setName("AD207");
          panel1.add(AD207);
          AD207.setBounds(125, 275, 40, AD207.getPreferredSize().height);

          //---- AD307 ----
          AD307.setName("AD307");
          panel1.add(AD307);
          AD307.setBounds(175, 275, 40, AD307.getPreferredSize().height);

          //---- AD407 ----
          AD407.setName("AD407");
          panel1.add(AD407);
          AD407.setBounds(225, 275, 40, AD407.getPreferredSize().height);

          //---- AD108 ----
          AD108.setName("AD108");
          panel1.add(AD108);
          AD108.setBounds(70, 305, 40, AD108.getPreferredSize().height);

          //---- AD208 ----
          AD208.setName("AD208");
          panel1.add(AD208);
          AD208.setBounds(125, 305, 40, AD208.getPreferredSize().height);

          //---- AD308 ----
          AD308.setName("AD308");
          panel1.add(AD308);
          AD308.setBounds(175, 305, 40, AD308.getPreferredSize().height);

          //---- AD408 ----
          AD408.setName("AD408");
          panel1.add(AD408);
          AD408.setBounds(225, 305, 40, AD408.getPreferredSize().height);

          //---- AD109 ----
          AD109.setName("AD109");
          panel1.add(AD109);
          AD109.setBounds(70, 335, 40, AD109.getPreferredSize().height);

          //---- AD209 ----
          AD209.setName("AD209");
          panel1.add(AD209);
          AD209.setBounds(125, 335, 40, AD209.getPreferredSize().height);

          //---- AD309 ----
          AD309.setName("AD309");
          panel1.add(AD309);
          AD309.setBounds(175, 335, 40, AD309.getPreferredSize().height);

          //---- AD409 ----
          AD409.setName("AD409");
          panel1.add(AD409);
          AD409.setBounds(225, 335, 40, AD409.getPreferredSize().height);

          //---- AD110 ----
          AD110.setName("AD110");
          panel1.add(AD110);
          AD110.setBounds(70, 365, 40, AD110.getPreferredSize().height);

          //---- AD210 ----
          AD210.setName("AD210");
          panel1.add(AD210);
          AD210.setBounds(125, 365, 40, AD210.getPreferredSize().height);

          //---- AD310 ----
          AD310.setName("AD310");
          panel1.add(AD310);
          AD310.setBounds(175, 365, 40, AD310.getPreferredSize().height);

          //---- AD410 ----
          AD410.setName("AD410");
          panel1.add(AD410);
          AD410.setBounds(225, 365, 40, AD410.getPreferredSize().height);

          //---- TYP01 ----
          TYP01.setName("TYP01");
          panel1.add(TYP01);
          TYP01.setBounds(275, 93, 24, TYP01.getPreferredSize().height);

          //---- TYP02 ----
          TYP02.setName("TYP02");
          panel1.add(TYP02);
          TYP02.setBounds(275, 125, 24, TYP02.getPreferredSize().height);

          //---- TYP03 ----
          TYP03.setName("TYP03");
          panel1.add(TYP03);
          TYP03.setBounds(275, 155, 24, TYP03.getPreferredSize().height);

          //---- TYP04 ----
          TYP04.setName("TYP04");
          panel1.add(TYP04);
          TYP04.setBounds(275, 185, 24, TYP04.getPreferredSize().height);

          //---- TYP05 ----
          TYP05.setName("TYP05");
          panel1.add(TYP05);
          TYP05.setBounds(275, 215, 24, TYP05.getPreferredSize().height);

          //---- TYP06 ----
          TYP06.setName("TYP06");
          panel1.add(TYP06);
          TYP06.setBounds(275, 245, 24, TYP06.getPreferredSize().height);

          //---- TYP07 ----
          TYP07.setName("TYP07");
          panel1.add(TYP07);
          TYP07.setBounds(275, 275, 24, TYP07.getPreferredSize().height);

          //---- TYP08 ----
          TYP08.setName("TYP08");
          panel1.add(TYP08);
          TYP08.setBounds(275, 305, 24, TYP08.getPreferredSize().height);

          //---- TYP09 ----
          TYP09.setName("TYP09");
          panel1.add(TYP09);
          TYP09.setBounds(275, 335, 24, TYP09.getPreferredSize().height);

          //---- TYP10 ----
          TYP10.setName("TYP10");
          panel1.add(TYP10);
          TYP10.setBounds(275, 365, 24, TYP10.getPreferredSize().height);

          //---- QTSX1 ----
          QTSX1.setText("@QTSX1@");
          QTSX1.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX1.setName("QTSX1");
          panel1.add(QTSX1);
          QTSX1.setBounds(310, 95, 100, QTSX1.getPreferredSize().height);

          //---- QTRX1 ----
          QTRX1.setText("@QTRX1@");
          QTRX1.setHorizontalAlignment(SwingConstants.RIGHT);
          QTRX1.setName("QTRX1");
          panel1.add(QTRX1);
          QTRX1.setBounds(new Rectangle(new Point(420, 95), QTRX1.getPreferredSize()));

          //---- QTEX1 ----
          QTEX1.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX1.setName("QTEX1");
          panel1.add(QTEX1);
          QTEX1.setBounds(530, 93, 100, QTEX1.getPreferredSize().height);

          //---- NUM2 ----
          NUM2.setText("@NUM2@");
          NUM2.setName("NUM2");
          panel1.add(NUM2);
          NUM2.setBounds(25, 127, 36, NUM2.getPreferredSize().height);

          //---- NUM3 ----
          NUM3.setText("@NUM3@");
          NUM3.setName("NUM3");
          panel1.add(NUM3);
          NUM3.setBounds(25, 157, 36, NUM3.getPreferredSize().height);

          //---- NUM4 ----
          NUM4.setText("@NUM4@");
          NUM4.setName("NUM4");
          panel1.add(NUM4);
          NUM4.setBounds(25, 187, 36, NUM4.getPreferredSize().height);

          //---- NUM5 ----
          NUM5.setText("@NUM5@");
          NUM5.setName("NUM5");
          panel1.add(NUM5);
          NUM5.setBounds(25, 217, 36, NUM5.getPreferredSize().height);

          //---- NUM6 ----
          NUM6.setText("@NUM6@");
          NUM6.setName("NUM6");
          panel1.add(NUM6);
          NUM6.setBounds(25, 247, 36, NUM6.getPreferredSize().height);

          //---- NUM7 ----
          NUM7.setText("@NUM7@");
          NUM7.setName("NUM7");
          panel1.add(NUM7);
          NUM7.setBounds(25, 277, 36, NUM7.getPreferredSize().height);

          //---- NUM8 ----
          NUM8.setText("@NUM8@");
          NUM8.setName("NUM8");
          panel1.add(NUM8);
          NUM8.setBounds(25, 307, 36, NUM8.getPreferredSize().height);

          //---- NUM9 ----
          NUM9.setText("@NUM9@");
          NUM9.setName("NUM9");
          panel1.add(NUM9);
          NUM9.setBounds(25, 337, 36, NUM9.getPreferredSize().height);

          //---- NUM10 ----
          NUM10.setText("@NUM10@");
          NUM10.setName("NUM10");
          panel1.add(NUM10);
          NUM10.setBounds(25, 367, 36, NUM10.getPreferredSize().height);

          //---- QTSX2 ----
          QTSX2.setText("@QTSX2@");
          QTSX2.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX2.setName("QTSX2");
          panel1.add(QTSX2);
          QTSX2.setBounds(new Rectangle(new Point(310, 127), QTSX2.getPreferredSize()));

          //---- QTRX2 ----
          QTRX2.setText("@QTRX2@");
          QTRX2.setHorizontalAlignment(SwingConstants.RIGHT);
          QTRX2.setName("QTRX2");
          panel1.add(QTRX2);
          QTRX2.setBounds(new Rectangle(new Point(420, 127), QTRX2.getPreferredSize()));

          //---- QTEX2 ----
          QTEX2.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX2.setName("QTEX2");
          panel1.add(QTEX2);
          QTEX2.setBounds(530, 125, 100, QTEX2.getPreferredSize().height);

          //---- QTSX3 ----
          QTSX3.setText("@QTSX3@");
          QTSX3.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX3.setName("QTSX3");
          panel1.add(QTSX3);
          QTSX3.setBounds(new Rectangle(new Point(310, 157), QTSX3.getPreferredSize()));

          //---- QTRX3 ----
          QTRX3.setText("@QTRX3@");
          QTRX3.setHorizontalAlignment(SwingConstants.RIGHT);
          QTRX3.setName("QTRX3");
          panel1.add(QTRX3);
          QTRX3.setBounds(new Rectangle(new Point(420, 157), QTRX3.getPreferredSize()));

          //---- QTEX3 ----
          QTEX3.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX3.setName("QTEX3");
          panel1.add(QTEX3);
          QTEX3.setBounds(530, 155, 100, QTEX3.getPreferredSize().height);

          //---- QTSX4 ----
          QTSX4.setText("@QTSX4@");
          QTSX4.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX4.setName("QTSX4");
          panel1.add(QTSX4);
          QTSX4.setBounds(new Rectangle(new Point(310, 187), QTSX4.getPreferredSize()));

          //---- QTRX4 ----
          QTRX4.setText("@QTRX4@");
          QTRX4.setHorizontalAlignment(SwingConstants.RIGHT);
          QTRX4.setName("QTRX4");
          panel1.add(QTRX4);
          QTRX4.setBounds(new Rectangle(new Point(420, 187), QTRX4.getPreferredSize()));

          //---- QTEX4 ----
          QTEX4.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX4.setName("QTEX4");
          panel1.add(QTEX4);
          QTEX4.setBounds(530, 185, 100, QTEX4.getPreferredSize().height);

          //---- QTSX5 ----
          QTSX5.setText("@QTSX5@");
          QTSX5.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX5.setName("QTSX5");
          panel1.add(QTSX5);
          QTSX5.setBounds(new Rectangle(new Point(310, 217), QTSX5.getPreferredSize()));

          //---- QTRX5 ----
          QTRX5.setText("@QTRX5@");
          QTRX5.setHorizontalAlignment(SwingConstants.RIGHT);
          QTRX5.setName("QTRX5");
          panel1.add(QTRX5);
          QTRX5.setBounds(new Rectangle(new Point(420, 217), QTRX5.getPreferredSize()));

          //---- QTEX5 ----
          QTEX5.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX5.setName("QTEX5");
          panel1.add(QTEX5);
          QTEX5.setBounds(530, 215, 100, QTEX5.getPreferredSize().height);

          //---- QTSX6 ----
          QTSX6.setText("@QTSX6@");
          QTSX6.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX6.setName("QTSX6");
          panel1.add(QTSX6);
          QTSX6.setBounds(new Rectangle(new Point(310, 247), QTSX6.getPreferredSize()));

          //---- QTRX6 ----
          QTRX6.setText("@QTRX6@");
          QTRX6.setHorizontalAlignment(SwingConstants.RIGHT);
          QTRX6.setName("QTRX6");
          panel1.add(QTRX6);
          QTRX6.setBounds(new Rectangle(new Point(420, 247), QTRX6.getPreferredSize()));

          //---- QTEX6 ----
          QTEX6.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX6.setName("QTEX6");
          panel1.add(QTEX6);
          QTEX6.setBounds(530, 245, 100, QTEX6.getPreferredSize().height);

          //---- QTSX7 ----
          QTSX7.setText("@QTSX7@");
          QTSX7.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX7.setName("QTSX7");
          panel1.add(QTSX7);
          QTSX7.setBounds(new Rectangle(new Point(310, 277), QTSX7.getPreferredSize()));

          //---- QTRX7 ----
          QTRX7.setText("@QTRX7@");
          QTRX7.setHorizontalAlignment(SwingConstants.RIGHT);
          QTRX7.setName("QTRX7");
          panel1.add(QTRX7);
          QTRX7.setBounds(new Rectangle(new Point(420, 277), QTRX7.getPreferredSize()));

          //---- QTEX7 ----
          QTEX7.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX7.setName("QTEX7");
          panel1.add(QTEX7);
          QTEX7.setBounds(530, 275, 100, QTEX7.getPreferredSize().height);

          //---- QTSX8 ----
          QTSX8.setText("@QTSX8@");
          QTSX8.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX8.setName("QTSX8");
          panel1.add(QTSX8);
          QTSX8.setBounds(new Rectangle(new Point(310, 307), QTSX8.getPreferredSize()));

          //---- QTRX8 ----
          QTRX8.setText("@QTRX8@");
          QTRX8.setHorizontalAlignment(SwingConstants.RIGHT);
          QTRX8.setName("QTRX8");
          panel1.add(QTRX8);
          QTRX8.setBounds(new Rectangle(new Point(420, 307), QTRX8.getPreferredSize()));

          //---- QTEX8 ----
          QTEX8.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX8.setName("QTEX8");
          panel1.add(QTEX8);
          QTEX8.setBounds(530, 305, 100, QTEX8.getPreferredSize().height);

          //---- QTSX9 ----
          QTSX9.setText("@QTSX9@");
          QTSX9.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX9.setName("QTSX9");
          panel1.add(QTSX9);
          QTSX9.setBounds(new Rectangle(new Point(310, 337), QTSX9.getPreferredSize()));

          //---- QTRX9 ----
          QTRX9.setText("@QTRX9@");
          QTRX9.setHorizontalAlignment(SwingConstants.RIGHT);
          QTRX9.setName("QTRX9");
          panel1.add(QTRX9);
          QTRX9.setBounds(new Rectangle(new Point(420, 337), QTRX9.getPreferredSize()));

          //---- QTEX9 ----
          QTEX9.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX9.setName("QTEX9");
          panel1.add(QTEX9);
          QTEX9.setBounds(530, 335, 100, QTEX9.getPreferredSize().height);

          //---- QTSX10 ----
          QTSX10.setText("@QTSX10@");
          QTSX10.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX10.setName("QTSX10");
          panel1.add(QTSX10);
          QTSX10.setBounds(new Rectangle(new Point(310, 367), QTSX10.getPreferredSize()));

          //---- QTRX10 ----
          QTRX10.setText("@QTRX10@");
          QTRX10.setHorizontalAlignment(SwingConstants.RIGHT);
          QTRX10.setName("QTRX10");
          panel1.add(QTRX10);
          QTRX10.setBounds(new Rectangle(new Point(420, 367), QTRX10.getPreferredSize()));

          //---- QTEX10 ----
          QTEX10.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX10.setName("QTEX10");
          panel1.add(QTEX10);
          QTEX10.setBounds(530, 365, 100, QTEX10.getPreferredSize().height);

          //---- label1 ----
          label1.setText("@AD1@");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(68, 70, 45, 26);

          //---- label2 ----
          label2.setText("@AD2@");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(123, 70, 45, 26);

          //---- label3 ----
          label3.setText("@AD3@");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setHorizontalAlignment(SwingConstants.CENTER);
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(173, 70, 45, 26);

          //---- label4 ----
          label4.setText("@AD4@");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(223, 70, 45, 26);

          //---- label5 ----
          label5.setText("T");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(275, 70, 24, 26);

          //---- label6 ----
          label6.setText("Capacit\u00e9");
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setHorizontalAlignment(SwingConstants.CENTER);
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(310, 70, 100, 26);

          //---- label7 ----
          label7.setText("R\u00e9serv\u00e9");
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setHorizontalAlignment(SwingConstants.CENTER);
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(420, 70, 100, 26);

          //---- label8 ----
          label8.setText("Quantit\u00e9");
          label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
          label8.setHorizontalAlignment(SwingConstants.CENTER);
          label8.setName("label8");
          panel1.add(label8);
          label8.setBounds(530, 70, 100, 26);

          //---- labelEtat ----
          labelEtat.setText("(ENTREE)");
          labelEtat.setFont(labelEtat.getFont().deriveFont(labelEtat.getFont().getStyle() | Font.BOLD, labelEtat.getFont().getSize() + 3f));
          labelEtat.setName("labelEtat");
          panel1.add(labelEtat);
          labelEtat.setBounds(25, 32, 190, 25);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel1.add(BT_PGUP);
          BT_PGUP.setBounds(635, 93, 25, 125);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel1.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(635, 268, 25, 125);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 454, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_22;
  private RiZoneSortie PA2MAG;
  private RiZoneSortie PA2QTX;
  private JLabel OBJ_23;
  private RiZoneSortie NUM1;
  private XRiTextField AD101;
  private XRiTextField AD201;
  private XRiTextField AD301;
  private XRiTextField AD401;
  private XRiTextField AD102;
  private XRiTextField AD202;
  private XRiTextField AD302;
  private XRiTextField AD402;
  private XRiTextField AD103;
  private XRiTextField AD203;
  private XRiTextField AD303;
  private XRiTextField AD403;
  private XRiTextField AD104;
  private XRiTextField AD204;
  private XRiTextField AD304;
  private XRiTextField AD404;
  private XRiTextField AD105;
  private XRiTextField AD205;
  private XRiTextField AD305;
  private XRiTextField AD405;
  private XRiTextField AD106;
  private XRiTextField AD206;
  private XRiTextField AD306;
  private XRiTextField AD406;
  private XRiTextField AD107;
  private XRiTextField AD207;
  private XRiTextField AD307;
  private XRiTextField AD407;
  private XRiTextField AD108;
  private XRiTextField AD208;
  private XRiTextField AD308;
  private XRiTextField AD408;
  private XRiTextField AD109;
  private XRiTextField AD209;
  private XRiTextField AD309;
  private XRiTextField AD409;
  private XRiTextField AD110;
  private XRiTextField AD210;
  private XRiTextField AD310;
  private XRiTextField AD410;
  private XRiTextField TYP01;
  private XRiTextField TYP02;
  private XRiTextField TYP03;
  private XRiTextField TYP04;
  private XRiTextField TYP05;
  private XRiTextField TYP06;
  private XRiTextField TYP07;
  private XRiTextField TYP08;
  private XRiTextField TYP09;
  private XRiTextField TYP10;
  private RiZoneSortie QTSX1;
  private RiZoneSortie QTRX1;
  private XRiTextField QTEX1;
  private RiZoneSortie NUM2;
  private RiZoneSortie NUM3;
  private RiZoneSortie NUM4;
  private RiZoneSortie NUM5;
  private RiZoneSortie NUM6;
  private RiZoneSortie NUM7;
  private RiZoneSortie NUM8;
  private RiZoneSortie NUM9;
  private RiZoneSortie NUM10;
  private RiZoneSortie QTSX2;
  private RiZoneSortie QTRX2;
  private XRiTextField QTEX2;
  private RiZoneSortie QTSX3;
  private RiZoneSortie QTRX3;
  private XRiTextField QTEX3;
  private RiZoneSortie QTSX4;
  private RiZoneSortie QTRX4;
  private XRiTextField QTEX4;
  private RiZoneSortie QTSX5;
  private RiZoneSortie QTRX5;
  private XRiTextField QTEX5;
  private RiZoneSortie QTSX6;
  private RiZoneSortie QTRX6;
  private XRiTextField QTEX6;
  private RiZoneSortie QTSX7;
  private RiZoneSortie QTRX7;
  private XRiTextField QTEX7;
  private RiZoneSortie QTSX8;
  private RiZoneSortie QTRX8;
  private XRiTextField QTEX8;
  private RiZoneSortie QTSX9;
  private RiZoneSortie QTRX9;
  private XRiTextField QTEX9;
  private RiZoneSortie QTSX10;
  private RiZoneSortie QTRX10;
  private XRiTextField QTEX10;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel labelEtat;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
