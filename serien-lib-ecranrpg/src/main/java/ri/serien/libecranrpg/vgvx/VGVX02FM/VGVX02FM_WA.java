
package ri.serien.libecranrpg.vgvx.VGVX02FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class VGVX02FM_WA extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX02FM_WA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    T08.setValeursSelection("1", " ");
    T07.setValeursSelection("1", " ");
    T06.setValeursSelection("1", " ");
    T05.setValeursSelection("1", " ");
    T04.setValeursSelection("1", " ");
    T03.setValeursSelection("1", " ");
    T02.setValeursSelection("1", " ");
    T01.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("Paramètres   @LIBPAR@")).trim()));
    T01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L01@")).trim());
    T02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L02@")).trim());
    T03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L03@")).trim());
    T04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L04@")).trim());
    T05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L05@")).trim());
    T06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L06@")).trim());
    T07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L07@")).trim());
    T08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L08@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    T01.setVisible(!lexique.HostFieldGetData("L01").trim().equals(""));
    T02.setVisible(!lexique.HostFieldGetData("L02").trim().equals(""));
    T03.setVisible(!lexique.HostFieldGetData("L03").trim().equals(""));
    T04.setVisible(!lexique.HostFieldGetData("L04").trim().equals(""));
    T05.setVisible(!lexique.HostFieldGetData("L05").trim().equals(""));
    T06.setVisible(!lexique.HostFieldGetData("L06").trim().equals(""));
    T07.setVisible(!lexique.HostFieldGetData("L07").trim().equals(""));
    T08.setVisible(!lexique.HostFieldGetData("L08").trim().equals(""));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix multi-paramètres"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (T01.isSelected())
    // lexique.HostFieldPutData("T01", 0, "1");
    // else
    // lexique.HostFieldPutData("T01", 0, " ");
    // if (T02.isSelected())
    // lexique.HostFieldPutData("T02", 0, "1");
    // else
    // lexique.HostFieldPutData("T02", 0, " ");
    // if (T03.isSelected())
    // lexique.HostFieldPutData("T03", 0, "1");
    // else
    // lexique.HostFieldPutData("T03", 0, " ");
    // if (T05.isSelected())
    // lexique.HostFieldPutData("T05", 0, "1");
    // else
    // lexique.HostFieldPutData("T05", 0, " ");
    // if (T04.isSelected())
    // lexique.HostFieldPutData("T04", 0, "1");
    // else
    // lexique.HostFieldPutData("T04", 0, " ");
    // if (T06.isSelected())
    // lexique.HostFieldPutData("T06", 0, "1");
    // else
    // lexique.HostFieldPutData("T06", 0, " ");
    // if (T07.isSelected())
    // lexique.HostFieldPutData("T07", 0, "1");
    // else
    // lexique.HostFieldPutData("T07", 0, " ");
    // if (T08.isSelected())
    // lexique.HostFieldPutData("T08", 0, "1");
    // else
    // lexique.HostFieldPutData("T08", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    T01 = new XRiCheckBox();
    T02 = new XRiCheckBox();
    T03 = new XRiCheckBox();
    T04 = new XRiCheckBox();
    T05 = new XRiCheckBox();
    T06 = new XRiCheckBox();
    T07 = new XRiCheckBox();
    T08 = new XRiCheckBox();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(680, 370));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Param\u00e8tres   @LIBPAR@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- T01 ----
          T01.setText("@L01@");
          T01.setComponentPopupMenu(null);
          T01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T01.setName("T01");
          panel1.add(T01);
          T01.setBounds(25, 35, 363, 20);

          //---- T02 ----
          T02.setText("@L02@");
          T02.setComponentPopupMenu(null);
          T02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T02.setName("T02");
          panel1.add(T02);
          T02.setBounds(25, 70, 363, 20);

          //---- T03 ----
          T03.setText("@L03@");
          T03.setComponentPopupMenu(null);
          T03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T03.setName("T03");
          panel1.add(T03);
          T03.setBounds(25, 100, 363, 20);

          //---- T04 ----
          T04.setText("@L04@");
          T04.setComponentPopupMenu(null);
          T04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T04.setName("T04");
          panel1.add(T04);
          T04.setBounds(25, 135, 363, 20);

          //---- T05 ----
          T05.setText("@L05@");
          T05.setComponentPopupMenu(null);
          T05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T05.setName("T05");
          panel1.add(T05);
          T05.setBounds(25, 170, 363, 20);

          //---- T06 ----
          T06.setText("@L06@");
          T06.setComponentPopupMenu(null);
          T06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T06.setName("T06");
          panel1.add(T06);
          T06.setBounds(25, 200, 363, 20);

          //---- T07 ----
          T07.setText("@L07@");
          T07.setComponentPopupMenu(null);
          T07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T07.setName("T07");
          panel1.add(T07);
          T07.setBounds(25, 235, 363, 20);

          //---- T08 ----
          T08.setText("@L08@");
          T08.setComponentPopupMenu(null);
          T08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T08.setName("T08");
          panel1.add(T08);
          T08.setBounds(25, 265, 363, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //---- BT_PGUP ----
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setToolTipText("Pr\u00e9c\u00e9dents");
        BT_PGUP.setName("BT_PGUP");

        //---- BT_PGDOWN ----
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setToolTipText("Suivants");
        BT_PGDOWN.setName("BT_PGDOWN");

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 415, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(45, 45, 45)
              .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
              .addGap(35, 35, 35)
              .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiCheckBox T01;
  private XRiCheckBox T02;
  private XRiCheckBox T03;
  private XRiCheckBox T04;
  private XRiCheckBox T05;
  private XRiCheckBox T06;
  private XRiCheckBox T07;
  private XRiCheckBox T08;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
