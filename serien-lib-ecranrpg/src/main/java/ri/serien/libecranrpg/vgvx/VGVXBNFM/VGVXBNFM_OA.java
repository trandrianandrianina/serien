
package ri.serien.libecranrpg.vgvx.VGVXBNFM;
// MON PACKAGE

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVXBNFM_OA extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXBNFM_OA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OB185.setEnabled(lexique.isPresent("OB185"));
    OB184.setEnabled(lexique.isPresent("OB184"));
    OB183.setEnabled(lexique.isPresent("OB183"));
    OB182.setEnabled(lexique.isPresent("OB182"));
    OB181.setEnabled(lexique.isPresent("OB181"));
    OBZP4.setVisible(lexique.isPresent("OBZP4"));
    OBZP3.setVisible(lexique.isPresent("OBZP3"));
    OBZP2.setVisible(lexique.isPresent("OBZP2"));
    OBZP1.setVisible(lexique.isPresent("OBZP1"));
    WZP34.setVisible(lexique.isPresent("WZP34"));
    WZP33.setVisible(lexique.isPresent("WZP33"));
    WZP32.setVisible(lexique.isPresent("WZP32"));
    WZP31.setVisible(lexique.isPresent("WZP31"));
    OB171.setVisible(lexique.isPresent("OB171"));
    OB161.setVisible(lexique.isPresent("OB161"));
    OB151.setVisible(lexique.isPresent("OB151"));
    OB141.setVisible(lexique.isPresent("OB141"));
    OB13.setVisible(lexique.isPresent("OB13"));
    OB11.setVisible(lexique.isPresent("OB11"));
    OB10.setVisible(lexique.isPresent("OB10"));
    OB9.setVisible(lexique.isPresent("OB9"));
    OB8.setVisible(lexique.isPresent("OB8"));
    OB7.setVisible(lexique.isPresent("OB7"));
    OB6.setVisible(lexique.isPresent("OB6"));
    OB5.setVisible(lexique.isPresent("OB5"));
    OB4.setVisible(lexique.isPresent("OB4"));
    OB3.setVisible(lexique.isPresent("OB3"));
    OB2.setVisible(lexique.isPresent("OB2"));
    OB1.setVisible(lexique.isPresent("OB1"));
    OB12.setVisible(lexique.isPresent("OB12"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvx05"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    OB1 = new XRiTextField();
    OB2 = new XRiTextField();
    OB3 = new XRiTextField();
    OB4 = new XRiTextField();
    OB5 = new XRiTextField();
    OB6 = new XRiTextField();
    OB7 = new XRiTextField();
    OB8 = new XRiTextField();
    OB9 = new XRiTextField();
    OB10 = new XRiTextField();
    OB11 = new XRiTextField();
    OB12 = new XRiTextField();
    OB13 = new XRiTextField();
    separator1 = compFactory.createSeparator("Informations compl\u00e9mentaires article");
    OB141 = new XRiTextField();
    OB151 = new XRiTextField();
    OB161 = new XRiTextField();
    OB171 = new XRiTextField();
    WZP31 = new XRiTextField();
    WZP32 = new XRiTextField();
    WZP33 = new XRiTextField();
    WZP34 = new XRiTextField();
    OBZP1 = new XRiTextField();
    OBZP2 = new XRiTextField();
    OBZP3 = new XRiTextField();
    OBZP4 = new XRiTextField();
    separator2 = compFactory.createSeparator("Articles commentaires pour edition sur bons et factures :");
    OB181 = new XRiTextField();
    OB182 = new XRiTextField();
    OB183 = new XRiTextField();
    OB184 = new XRiTextField();
    OB185 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(810, 560));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- OB1 ----
        OB1.setName("OB1");
        p_contenu.add(OB1);
        OB1.setBounds(10, 30, 610, OB1.getPreferredSize().height);

        //---- OB2 ----
        OB2.setName("OB2");
        p_contenu.add(OB2);
        OB2.setBounds(10, 55, 610, OB2.getPreferredSize().height);

        //---- OB3 ----
        OB3.setName("OB3");
        p_contenu.add(OB3);
        OB3.setBounds(10, 80, 610, OB3.getPreferredSize().height);

        //---- OB4 ----
        OB4.setName("OB4");
        p_contenu.add(OB4);
        OB4.setBounds(10, 105, 610, OB4.getPreferredSize().height);

        //---- OB5 ----
        OB5.setName("OB5");
        p_contenu.add(OB5);
        OB5.setBounds(10, 130, 610, OB5.getPreferredSize().height);

        //---- OB6 ----
        OB6.setName("OB6");
        p_contenu.add(OB6);
        OB6.setBounds(10, 155, 610, OB6.getPreferredSize().height);

        //---- OB7 ----
        OB7.setName("OB7");
        p_contenu.add(OB7);
        OB7.setBounds(10, 180, 610, OB7.getPreferredSize().height);

        //---- OB8 ----
        OB8.setName("OB8");
        p_contenu.add(OB8);
        OB8.setBounds(10, 205, 610, OB8.getPreferredSize().height);

        //---- OB9 ----
        OB9.setName("OB9");
        p_contenu.add(OB9);
        OB9.setBounds(10, 230, 610, OB9.getPreferredSize().height);

        //---- OB10 ----
        OB10.setName("OB10");
        p_contenu.add(OB10);
        OB10.setBounds(10, 255, 610, OB10.getPreferredSize().height);

        //---- OB11 ----
        OB11.setName("OB11");
        p_contenu.add(OB11);
        OB11.setBounds(10, 280, 610, OB11.getPreferredSize().height);

        //---- OB12 ----
        OB12.setName("OB12");
        p_contenu.add(OB12);
        OB12.setBounds(10, 305, 610, OB12.getPreferredSize().height);

        //---- OB13 ----
        OB13.setName("OB13");
        p_contenu.add(OB13);
        OB13.setBounds(10, 330, 610, OB13.getPreferredSize().height);

        //---- separator1 ----
        separator1.setName("separator1");
        p_contenu.add(separator1);
        separator1.setBounds(5, 10, 615, separator1.getPreferredSize().height);

        //---- OB141 ----
        OB141.setComponentPopupMenu(BTD);
        OB141.setName("OB141");
        p_contenu.add(OB141);
        OB141.setBounds(310, 360, 310, OB141.getPreferredSize().height);

        //---- OB151 ----
        OB151.setComponentPopupMenu(BTD);
        OB151.setName("OB151");
        p_contenu.add(OB151);
        OB151.setBounds(310, 385, 310, OB151.getPreferredSize().height);

        //---- OB161 ----
        OB161.setComponentPopupMenu(BTD);
        OB161.setName("OB161");
        p_contenu.add(OB161);
        OB161.setBounds(310, 410, 310, OB161.getPreferredSize().height);

        //---- OB171 ----
        OB171.setComponentPopupMenu(BTD);
        OB171.setName("OB171");
        p_contenu.add(OB171);
        OB171.setBounds(310, 435, 310, OB171.getPreferredSize().height);

        //---- WZP31 ----
        WZP31.setName("WZP31");
        p_contenu.add(WZP31);
        WZP31.setBounds(10, 360, 262, WZP31.getPreferredSize().height);

        //---- WZP32 ----
        WZP32.setName("WZP32");
        p_contenu.add(WZP32);
        WZP32.setBounds(10, 385, 262, WZP32.getPreferredSize().height);

        //---- WZP33 ----
        WZP33.setName("WZP33");
        p_contenu.add(WZP33);
        WZP33.setBounds(10, 410, 262, WZP33.getPreferredSize().height);

        //---- WZP34 ----
        WZP34.setName("WZP34");
        p_contenu.add(WZP34);
        WZP34.setBounds(10, 435, 262, WZP34.getPreferredSize().height);

        //---- OBZP1 ----
        OBZP1.setComponentPopupMenu(BTD);
        OBZP1.setName("OBZP1");
        p_contenu.add(OBZP1);
        OBZP1.setBounds(310, 360, 160, OBZP1.getPreferredSize().height);

        //---- OBZP2 ----
        OBZP2.setComponentPopupMenu(BTD);
        OBZP2.setName("OBZP2");
        p_contenu.add(OBZP2);
        OBZP2.setBounds(310, 380, 160, OBZP2.getPreferredSize().height);

        //---- OBZP3 ----
        OBZP3.setComponentPopupMenu(BTD);
        OBZP3.setName("OBZP3");
        p_contenu.add(OBZP3);
        OBZP3.setBounds(310, 405, 160, OBZP3.getPreferredSize().height);

        //---- OBZP4 ----
        OBZP4.setComponentPopupMenu(BTD);
        OBZP4.setName("OBZP4");
        p_contenu.add(OBZP4);
        OBZP4.setBounds(310, 430, 160, OBZP4.getPreferredSize().height);

        //---- separator2 ----
        separator2.setName("separator2");
        p_contenu.add(separator2);
        separator2.setBounds(5, 485, 615, separator2.getPreferredSize().height);

        //---- OB181 ----
        OB181.setComponentPopupMenu(BTD);
        OB181.setName("OB181");
        p_contenu.add(OB181);
        OB181.setBounds(10, 515, 110, OB181.getPreferredSize().height);

        //---- OB182 ----
        OB182.setComponentPopupMenu(BTD);
        OB182.setName("OB182");
        p_contenu.add(OB182);
        OB182.setBounds(135, 515, 110, OB182.getPreferredSize().height);

        //---- OB183 ----
        OB183.setComponentPopupMenu(BTD);
        OB183.setName("OB183");
        p_contenu.add(OB183);
        OB183.setBounds(260, 515, 110, OB183.getPreferredSize().height);

        //---- OB184 ----
        OB184.setComponentPopupMenu(BTD);
        OB184.setName("OB184");
        p_contenu.add(OB184);
        OB184.setBounds(385, 515, 110, OB184.getPreferredSize().height);

        //---- OB185 ----
        OB185.setComponentPopupMenu(BTD);
        OB185.setName("OB185");
        p_contenu.add(OB185);
        OB185.setBounds(510, 515, 110, OB185.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD.add(OBJ_6);

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private XRiTextField OB1;
  private XRiTextField OB2;
  private XRiTextField OB3;
  private XRiTextField OB4;
  private XRiTextField OB5;
  private XRiTextField OB6;
  private XRiTextField OB7;
  private XRiTextField OB8;
  private XRiTextField OB9;
  private XRiTextField OB10;
  private XRiTextField OB11;
  private XRiTextField OB12;
  private XRiTextField OB13;
  private JComponent separator1;
  private XRiTextField OB141;
  private XRiTextField OB151;
  private XRiTextField OB161;
  private XRiTextField OB171;
  private XRiTextField WZP31;
  private XRiTextField WZP32;
  private XRiTextField WZP33;
  private XRiTextField WZP34;
  private XRiTextField OBZP1;
  private XRiTextField OBZP2;
  private XRiTextField OBZP3;
  private XRiTextField OBZP4;
  private JComponent separator2;
  private XRiTextField OB181;
  private XRiTextField OB182;
  private XRiTextField OB183;
  private XRiTextField OB184;
  private XRiTextField OB185;
  private JPopupMenu BTD;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
