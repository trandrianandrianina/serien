
package ri.serien.libecranrpg.vgvx.VGVX051F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX051F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX051F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LIB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB1@")).trim());
    LIB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB2@")).trim());
    LIB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB3@")).trim());
    LIB4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB4@")).trim());
    A1UNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNV@")).trim());
    WLIB5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB5@")).trim());
    LIB5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB5@")).trim());
    LIB6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB6@")).trim());
    LIB7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB7@")).trim());
    LIB8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB8@")).trim());
    UNL5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNL5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    
    // TODO Icones
    // V07F
    
    // Titre
    setTitle("Unités et conditionnements de vente");
    
    ;
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    OBJ_18 = new JLabel();
    LIB1 = new RiZoneSortie();
    LIB2 = new RiZoneSortie();
    LIB3 = new RiZoneSortie();
    LIB4 = new RiZoneSortie();
    A1UNL = new XRiTextField();
    A1UNL2 = new XRiTextField();
    A1UNL3 = new XRiTextField();
    A1UNL4 = new XRiTextField();
    A1UNV = new RiZoneSortie();
    WLIB5 = new RiZoneSortie();
    OBJ_45 = new JLabel();
    OBJ_19 = new JLabel();
    A1CNDX = new XRiTextField();
    A1CN2X = new XRiTextField();
    A1CN3X = new XRiTextField();
    A1CN4X = new XRiTextField();
    OBJ_20 = new JLabel();
    LIB5 = new RiZoneSortie();
    LIB6 = new RiZoneSortie();
    LIB7 = new RiZoneSortie();
    LIB8 = new RiZoneSortie();
    UNL5 = new RiZoneSortie();
    UNL6 = new XRiTextField();
    UNL7 = new XRiTextField();
    UNL8 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_22 = new JMenuItem();
    OBJ_21 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(745, 240));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel4 ========
        {
          panel4.setBorder(new TitledBorder(""));
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);

          //---- OBJ_18 ----
          OBJ_18.setText("Unit\u00e9 de saisie");
          OBJ_18.setFont(OBJ_18.getFont().deriveFont(OBJ_18.getFont().getStyle() | Font.BOLD));
          OBJ_18.setBackground(new Color(247, 247, 224));
          OBJ_18.setOpaque(true);
          OBJ_18.setName("OBJ_18");
          panel4.add(OBJ_18);
          OBJ_18.setBounds(20, 15, 195, 20);

          //---- LIB1 ----
          LIB1.setText("@LIB1@");
          LIB1.setName("LIB1");
          panel4.add(LIB1);
          LIB1.setBounds(52, 72, 160, LIB1.getPreferredSize().height);

          //---- LIB2 ----
          LIB2.setText("@LIB2@");
          LIB2.setName("LIB2");
          panel4.add(LIB2);
          LIB2.setBounds(52, 102, 160, LIB2.getPreferredSize().height);

          //---- LIB3 ----
          LIB3.setText("@LIB3@");
          LIB3.setName("LIB3");
          panel4.add(LIB3);
          LIB3.setBounds(52, 132, 160, LIB3.getPreferredSize().height);

          //---- LIB4 ----
          LIB4.setText("@LIB4@");
          LIB4.setName("LIB4");
          panel4.add(LIB4);
          LIB4.setBounds(52, 162, 160, LIB4.getPreferredSize().height);

          //---- A1UNL ----
          A1UNL.setComponentPopupMenu(BTD);
          A1UNL.setName("A1UNL");
          panel4.add(A1UNL);
          A1UNL.setBounds(15, 70, 30, A1UNL.getPreferredSize().height);

          //---- A1UNL2 ----
          A1UNL2.setComponentPopupMenu(BTD);
          A1UNL2.setName("A1UNL2");
          panel4.add(A1UNL2);
          A1UNL2.setBounds(15, 100, 30, A1UNL2.getPreferredSize().height);

          //---- A1UNL3 ----
          A1UNL3.setComponentPopupMenu(BTD);
          A1UNL3.setName("A1UNL3");
          panel4.add(A1UNL3);
          A1UNL3.setBounds(15, 130, 30, A1UNL3.getPreferredSize().height);

          //---- A1UNL4 ----
          A1UNL4.setComponentPopupMenu(BTD);
          A1UNL4.setName("A1UNL4");
          panel4.add(A1UNL4);
          A1UNL4.setBounds(15, 160, 30, A1UNL4.getPreferredSize().height);

          //---- A1UNV ----
          A1UNV.setComponentPopupMenu(null);
          A1UNV.setText("@A1UNV@");
          A1UNV.setName("A1UNV");
          panel4.add(A1UNV);
          A1UNV.setBounds(15, 42, 30, A1UNV.getPreferredSize().height);

          //---- WLIB5 ----
          WLIB5.setText("@WLIB5@");
          WLIB5.setName("WLIB5");
          panel4.add(WLIB5);
          WLIB5.setBounds(52, 42, 160, WLIB5.getPreferredSize().height);

          //---- OBJ_45 ----
          OBJ_45.setText("Unit\u00e9 de prix");
          OBJ_45.setName("OBJ_45");
          panel4.add(OBJ_45);
          OBJ_45.setBounds(225, 44, 91, 20);

          //---- OBJ_19 ----
          OBJ_19.setText("Coefficient");
          OBJ_19.setFont(OBJ_19.getFont().deriveFont(OBJ_19.getFont().getStyle() | Font.BOLD));
          OBJ_19.setBackground(new Color(247, 247, 224));
          OBJ_19.setOpaque(true);
          OBJ_19.setName("OBJ_19");
          panel4.add(OBJ_19);
          OBJ_19.setBounds(220, 15, 90, 20);

          //---- A1CNDX ----
          A1CNDX.setName("A1CNDX");
          panel4.add(A1CNDX);
          A1CNDX.setBounds(220, 70, 90, A1CNDX.getPreferredSize().height);

          //---- A1CN2X ----
          A1CN2X.setName("A1CN2X");
          panel4.add(A1CN2X);
          A1CN2X.setBounds(220, 100, 90, A1CN2X.getPreferredSize().height);

          //---- A1CN3X ----
          A1CN3X.setName("A1CN3X");
          panel4.add(A1CN3X);
          A1CN3X.setBounds(220, 130, 90, A1CN3X.getPreferredSize().height);

          //---- A1CN4X ----
          A1CN4X.setName("A1CN4X");
          panel4.add(A1CN4X);
          A1CN4X.setBounds(220, 160, 90, A1CN4X.getPreferredSize().height);

          //---- OBJ_20 ----
          OBJ_20.setText("Rattachement");
          OBJ_20.setFont(OBJ_20.getFont().deriveFont(OBJ_20.getFont().getStyle() | Font.BOLD));
          OBJ_20.setBackground(new Color(247, 247, 224));
          OBJ_20.setOpaque(true);
          OBJ_20.setName("OBJ_20");
          panel4.add(OBJ_20);
          OBJ_20.setBounds(315, 15, 195, 20);

          //---- LIB5 ----
          LIB5.setText("@LIB5@");
          LIB5.setName("LIB5");
          panel4.add(LIB5);
          LIB5.setBounds(350, 72, 160, LIB5.getPreferredSize().height);

          //---- LIB6 ----
          LIB6.setText("@LIB6@");
          LIB6.setName("LIB6");
          panel4.add(LIB6);
          LIB6.setBounds(350, 102, 160, LIB6.getPreferredSize().height);

          //---- LIB7 ----
          LIB7.setText("@LIB7@");
          LIB7.setName("LIB7");
          panel4.add(LIB7);
          LIB7.setBounds(350, 132, 160, LIB7.getPreferredSize().height);

          //---- LIB8 ----
          LIB8.setText("@LIB8@");
          LIB8.setName("LIB8");
          panel4.add(LIB8);
          LIB8.setBounds(350, 162, 160, LIB8.getPreferredSize().height);

          //---- UNL5 ----
          UNL5.setComponentPopupMenu(BTD);
          UNL5.setText("@UNL5@");
          UNL5.setName("UNL5");
          panel4.add(UNL5);
          UNL5.setBounds(315, 72, 28, UNL5.getPreferredSize().height);

          //---- UNL6 ----
          UNL6.setComponentPopupMenu(BTD);
          UNL6.setName("UNL6");
          panel4.add(UNL6);
          UNL6.setBounds(315, 100, 30, UNL6.getPreferredSize().height);

          //---- UNL7 ----
          UNL7.setComponentPopupMenu(BTD);
          UNL7.setName("UNL7");
          panel4.add(UNL7);
          UNL7.setBounds(315, 130, 30, UNL7.getPreferredSize().height);

          //---- UNL8 ----
          UNL8.setComponentPopupMenu(BTD);
          UNL8.setName("UNL8");
          panel4.add(UNL8);
          UNL8.setBounds(315, 160, 30, UNL8.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel4);
        panel4.setBounds(13, 13, 550, 214);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_22 ----
      OBJ_22.setText("Choix possibles");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel4;
  private JLabel OBJ_18;
  private RiZoneSortie LIB1;
  private RiZoneSortie LIB2;
  private RiZoneSortie LIB3;
  private RiZoneSortie LIB4;
  private XRiTextField A1UNL;
  private XRiTextField A1UNL2;
  private XRiTextField A1UNL3;
  private XRiTextField A1UNL4;
  private RiZoneSortie A1UNV;
  private RiZoneSortie WLIB5;
  private JLabel OBJ_45;
  private JLabel OBJ_19;
  private XRiTextField A1CNDX;
  private XRiTextField A1CN2X;
  private XRiTextField A1CN3X;
  private XRiTextField A1CN4X;
  private JLabel OBJ_20;
  private RiZoneSortie LIB5;
  private RiZoneSortie LIB6;
  private RiZoneSortie LIB7;
  private RiZoneSortie LIB8;
  private RiZoneSortie UNL5;
  private XRiTextField UNL6;
  private XRiTextField UNL7;
  private XRiTextField UNL8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_21;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
