
package ri.serien.libecranrpg.vgvx.VGVX50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX50FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX50FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("Gestion des libellés : @A1LIB@")).trim()));
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    panel1.setBorder(new TitledBorder("Gestion des libellés : " + lexique.HostFieldGetData("A1LIB")));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    A1ART = new XRiTextField();
    OBJ_41 = new JLabel();
    OBJ_45 = new JLabel();
    WETB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    LIB01 = new XRiTextField();
    LIB02 = new XRiTextField();
    LIB03 = new XRiTextField();
    LIB04 = new XRiTextField();
    LIB05 = new XRiTextField();
    LIB06 = new XRiTextField();
    LIB07 = new XRiTextField();
    LIB08 = new XRiTextField();
    LIB09 = new XRiTextField();
    LIB10 = new XRiTextField();
    LIB11 = new XRiTextField();
    LIB12 = new XRiTextField();
    LIB13 = new XRiTextField();
    LIB14 = new XRiTextField();
    LIB15 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    NL01 = new XRiTextField();
    NL02 = new XRiTextField();
    NL03 = new XRiTextField();
    NL04 = new XRiTextField();
    NL05 = new XRiTextField();
    NL06 = new XRiTextField();
    NL07 = new XRiTextField();
    NL08 = new XRiTextField();
    NL09 = new XRiTextField();
    NL10 = new XRiTextField();
    NL11 = new XRiTextField();
    NL12 = new XRiTextField();
    NL13 = new XRiTextField();
    NL14 = new XRiTextField();
    NL15 = new XRiTextField();
    panel2 = new JPanel();
    A1LIB = new XRiTextField();
    A1LB1 = new XRiTextField();
    A1LB2 = new XRiTextField();
    A1LB3 = new XRiTextField();
    OBJ_52 = new JLabel();
    WNUM = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_22 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des libell\u00e9s");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- A1ART ----
          A1ART.setComponentPopupMenu(BTD);
          A1ART.setName("A1ART");
          p_tete_gauche.add(A1ART);
          A1ART.setBounds(255, 0, 214, A1ART.getPreferredSize().height);

          //---- OBJ_41 ----
          OBJ_41.setText("Etablissement");
          OBJ_41.setName("OBJ_41");
          p_tete_gauche.add(OBJ_41);
          OBJ_41.setBounds(5, 0, 93, 28);

          //---- OBJ_45 ----
          OBJ_45.setText("Article");
          OBJ_45.setName("OBJ_45");
          p_tete_gauche.add(OBJ_45);
          OBJ_45.setBounds(195, 0, 48, 28);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(125, 0, 40, WETB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Affectations libell\u00e9s");
              riSousMenu_bt6.setToolTipText("Affecte les libell\u00e9s \u00e0 une \u00e9dition ou une langue donn\u00e9e");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(900, 600));
          p_contenu.setMaximumSize(new Dimension(900, 600));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Gestion des libell\u00e9s : @A1LIB@"));
            panel1.setOpaque(false);
            panel1.setPreferredSize(new Dimension(780, 559));
            panel1.setMinimumSize(new Dimension(780, 559));
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- LIB01 ----
            LIB01.setComponentPopupMenu(BTD);
            LIB01.setName("LIB01");
            panel1.add(LIB01);
            LIB01.setBounds(80, 165, 740, LIB01.getPreferredSize().height);

            //---- LIB02 ----
            LIB02.setComponentPopupMenu(BTD);
            LIB02.setName("LIB02");
            panel1.add(LIB02);
            LIB02.setBounds(80, 190, 740, LIB02.getPreferredSize().height);

            //---- LIB03 ----
            LIB03.setComponentPopupMenu(BTD);
            LIB03.setName("LIB03");
            panel1.add(LIB03);
            LIB03.setBounds(80, 215, 740, LIB03.getPreferredSize().height);

            //---- LIB04 ----
            LIB04.setComponentPopupMenu(BTD);
            LIB04.setName("LIB04");
            panel1.add(LIB04);
            LIB04.setBounds(80, 240, 740, LIB04.getPreferredSize().height);

            //---- LIB05 ----
            LIB05.setComponentPopupMenu(BTD);
            LIB05.setName("LIB05");
            panel1.add(LIB05);
            LIB05.setBounds(80, 265, 740, LIB05.getPreferredSize().height);

            //---- LIB06 ----
            LIB06.setComponentPopupMenu(BTD);
            LIB06.setName("LIB06");
            panel1.add(LIB06);
            LIB06.setBounds(80, 290, 740, LIB06.getPreferredSize().height);

            //---- LIB07 ----
            LIB07.setComponentPopupMenu(BTD);
            LIB07.setName("LIB07");
            panel1.add(LIB07);
            LIB07.setBounds(80, 315, 740, LIB07.getPreferredSize().height);

            //---- LIB08 ----
            LIB08.setComponentPopupMenu(BTD);
            LIB08.setName("LIB08");
            panel1.add(LIB08);
            LIB08.setBounds(80, 340, 740, LIB08.getPreferredSize().height);

            //---- LIB09 ----
            LIB09.setComponentPopupMenu(BTD);
            LIB09.setName("LIB09");
            panel1.add(LIB09);
            LIB09.setBounds(80, 365, 740, LIB09.getPreferredSize().height);

            //---- LIB10 ----
            LIB10.setComponentPopupMenu(BTD);
            LIB10.setName("LIB10");
            panel1.add(LIB10);
            LIB10.setBounds(80, 390, 740, LIB10.getPreferredSize().height);

            //---- LIB11 ----
            LIB11.setComponentPopupMenu(BTD);
            LIB11.setName("LIB11");
            panel1.add(LIB11);
            LIB11.setBounds(80, 415, 740, LIB11.getPreferredSize().height);

            //---- LIB12 ----
            LIB12.setComponentPopupMenu(BTD);
            LIB12.setName("LIB12");
            panel1.add(LIB12);
            LIB12.setBounds(80, 440, 740, LIB12.getPreferredSize().height);

            //---- LIB13 ----
            LIB13.setComponentPopupMenu(BTD);
            LIB13.setName("LIB13");
            panel1.add(LIB13);
            LIB13.setBounds(80, 465, 740, LIB13.getPreferredSize().height);

            //---- LIB14 ----
            LIB14.setComponentPopupMenu(BTD);
            LIB14.setName("LIB14");
            panel1.add(LIB14);
            LIB14.setBounds(80, 490, 740, LIB14.getPreferredSize().height);

            //---- LIB15 ----
            LIB15.setComponentPopupMenu(BTD);
            LIB15.setName("LIB15");
            panel1.add(LIB15);
            LIB15.setBounds(80, 515, 740, LIB15.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Num\u00e9ro");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(20, 140, 60, 25);

            //---- label2 ----
            label2.setText("Libell\u00e9");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(80, 140, 740, 25);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(830, 165, 25, 165);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(830, 380, 25, 165);

            //---- NL01 ----
            NL01.setComponentPopupMenu(BTD);
            NL01.setName("NL01");
            panel1.add(NL01);
            NL01.setBounds(32, 165, 36, NL01.getPreferredSize().height);

            //---- NL02 ----
            NL02.setComponentPopupMenu(BTD);
            NL02.setName("NL02");
            panel1.add(NL02);
            NL02.setBounds(32, 190, 36, NL02.getPreferredSize().height);

            //---- NL03 ----
            NL03.setComponentPopupMenu(BTD);
            NL03.setName("NL03");
            panel1.add(NL03);
            NL03.setBounds(32, 215, 36, NL03.getPreferredSize().height);

            //---- NL04 ----
            NL04.setComponentPopupMenu(BTD);
            NL04.setName("NL04");
            panel1.add(NL04);
            NL04.setBounds(32, 240, 36, NL04.getPreferredSize().height);

            //---- NL05 ----
            NL05.setComponentPopupMenu(BTD);
            NL05.setName("NL05");
            panel1.add(NL05);
            NL05.setBounds(32, 265, 36, NL05.getPreferredSize().height);

            //---- NL06 ----
            NL06.setComponentPopupMenu(BTD);
            NL06.setName("NL06");
            panel1.add(NL06);
            NL06.setBounds(32, 290, 36, NL06.getPreferredSize().height);

            //---- NL07 ----
            NL07.setComponentPopupMenu(BTD);
            NL07.setName("NL07");
            panel1.add(NL07);
            NL07.setBounds(32, 315, 36, NL07.getPreferredSize().height);

            //---- NL08 ----
            NL08.setComponentPopupMenu(BTD);
            NL08.setName("NL08");
            panel1.add(NL08);
            NL08.setBounds(32, 340, 36, NL08.getPreferredSize().height);

            //---- NL09 ----
            NL09.setComponentPopupMenu(BTD);
            NL09.setName("NL09");
            panel1.add(NL09);
            NL09.setBounds(32, 365, 36, NL09.getPreferredSize().height);

            //---- NL10 ----
            NL10.setComponentPopupMenu(BTD);
            NL10.setName("NL10");
            panel1.add(NL10);
            NL10.setBounds(32, 390, 36, NL10.getPreferredSize().height);

            //---- NL11 ----
            NL11.setComponentPopupMenu(BTD);
            NL11.setName("NL11");
            panel1.add(NL11);
            NL11.setBounds(32, 415, 36, NL11.getPreferredSize().height);

            //---- NL12 ----
            NL12.setComponentPopupMenu(BTD);
            NL12.setName("NL12");
            panel1.add(NL12);
            NL12.setBounds(32, 440, 36, NL12.getPreferredSize().height);

            //---- NL13 ----
            NL13.setComponentPopupMenu(BTD);
            NL13.setName("NL13");
            panel1.add(NL13);
            NL13.setBounds(32, 465, 36, NL13.getPreferredSize().height);

            //---- NL14 ----
            NL14.setComponentPopupMenu(BTD);
            NL14.setName("NL14");
            panel1.add(NL14);
            NL14.setBounds(32, 490, 36, NL14.getPreferredSize().height);

            //---- NL15 ----
            NL15.setComponentPopupMenu(BTD);
            NL15.setName("NL15");
            panel1.add(NL15);
            NL15.setBounds(32, 515, 36, NL15.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Article"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- A1LIB ----
              A1LIB.setName("A1LIB");
              panel2.add(A1LIB);
              A1LIB.setBounds(25, 30, 276, A1LIB.getPreferredSize().height);

              //---- A1LB1 ----
              A1LB1.setName("A1LB1");
              panel2.add(A1LB1);
              A1LB1.setBounds(25, 55, 276, A1LB1.getPreferredSize().height);

              //---- A1LB2 ----
              A1LB2.setName("A1LB2");
              panel2.add(A1LB2);
              A1LB2.setBounds(310, 30, 276, A1LB2.getPreferredSize().height);

              //---- A1LB3 ----
              A1LB3.setName("A1LB3");
              panel2.add(A1LB3);
              A1LB3.setBounds(310, 55, 276, A1LB3.getPreferredSize().height);

              //---- OBJ_52 ----
              OBJ_52.setText("Afficher \u00e0 partir du num\u00e9ro");
              OBJ_52.setName("OBJ_52");
              panel2.add(OBJ_52);
              OBJ_52.setBounds(615, 55, 165, 28);

              //---- WNUM ----
              WNUM.setComponentPopupMenu(BTD);
              WNUM.setName("WNUM");
              panel2.add(WNUM);
              WNUM.setBounds(785, 55, 34, WNUM.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(20, 30, 835, 100);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 870, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 575, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(3, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField A1ART;
  private JLabel OBJ_41;
  private JLabel OBJ_45;
  private XRiTextField WETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField LIB01;
  private XRiTextField LIB02;
  private XRiTextField LIB03;
  private XRiTextField LIB04;
  private XRiTextField LIB05;
  private XRiTextField LIB06;
  private XRiTextField LIB07;
  private XRiTextField LIB08;
  private XRiTextField LIB09;
  private XRiTextField LIB10;
  private XRiTextField LIB11;
  private XRiTextField LIB12;
  private XRiTextField LIB13;
  private XRiTextField LIB14;
  private XRiTextField LIB15;
  private JLabel label1;
  private JLabel label2;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiTextField NL01;
  private XRiTextField NL02;
  private XRiTextField NL03;
  private XRiTextField NL04;
  private XRiTextField NL05;
  private XRiTextField NL06;
  private XRiTextField NL07;
  private XRiTextField NL08;
  private XRiTextField NL09;
  private XRiTextField NL10;
  private XRiTextField NL11;
  private XRiTextField NL12;
  private XRiTextField NL13;
  private XRiTextField NL14;
  private XRiTextField NL15;
  private JPanel panel2;
  private XRiTextField A1LIB;
  private XRiTextField A1LB1;
  private XRiTextField A1LB2;
  private XRiTextField A1LB3;
  private JLabel OBJ_52;
  private XRiTextField WNUM;
  private JPopupMenu BTD;
  private JMenuItem OBJ_22;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
