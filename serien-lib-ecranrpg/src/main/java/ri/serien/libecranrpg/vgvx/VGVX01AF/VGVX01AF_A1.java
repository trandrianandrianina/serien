
package ri.serien.libecranrpg.vgvx.VGVX01AF;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX01AF_A1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGVX01AF_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    ARG1.setVisible(lexique.isPresent("ARG1"));
    SCAN.setVisible(lexique.isPresent("SCAN"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - PERSONNALISATION"));
    
    

    ;
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanel();
    pnlContenu = new SNPanelContenu();
    lbCode = new SNLabelChamp();
    ARG1 = new XRiTextField();
    lbRecherche = new SNLabelChamp();
    SCAN = new XRiTextField();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(430, 150));
    setPreferredSize(new Dimension(430, 150));
    setMaximumSize(new Dimension(430, 150));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      //======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbCode ----
        lbCode.setText("Code");
        lbCode.setName("lbCode");
        pnlContenu.add(lbCode, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- ARG1 ----
        ARG1.setPreferredSize(new Dimension(35, 30));
        ARG1.setMinimumSize(new Dimension(35, 30));
        ARG1.setMaximumSize(new Dimension(35, 30));
        ARG1.setName("ARG1");
        pnlContenu.add(ARG1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbRecherche ----
        lbRecherche.setText("Recherche g\u00e9n\u00e9rique");
        lbRecherche.setName("lbRecherche");
        pnlContenu.add(lbRecherche, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- SCAN ----
        SCAN.setMinimumSize(new Dimension(200, 30));
        SCAN.setPreferredSize(new Dimension(200, 30));
        SCAN.setMaximumSize(new Dimension(200, 30));
        SCAN.setName("SCAN");
        pnlContenu.add(SCAN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);

      //---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private SNLabelChamp lbCode;
  private XRiTextField ARG1;
  private SNLabelChamp lbRecherche;
  private XRiTextField SCAN;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
