
package ri.serien.libecranrpg.vgvx.VGVX15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX15FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX15FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P15LIB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LD01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD01@")).trim());
    LD02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD02@")).trim());
    LD03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD03@")).trim());
    LD04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD04@")).trim());
    LD05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD05@")).trim());
    LD06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD06@")).trim());
    LD07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD07@")).trim());
    LD08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD08@")).trim());
    LD09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD09@")).trim());
    LD10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD10@")).trim());
    LD11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD11@")).trim());
    LD12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD12@")).trim());
    LD13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD13@")).trim());
    LD14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD14@")).trim());
    LD15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD15@")).trim());
    HLD01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UTIT@")).trim());
    MG01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG01@")).trim());
    MG02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG02@")).trim());
    MG03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG03@")).trim());
    MG04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG04@")).trim());
    MG05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG05@")).trim());
    MG06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG06@")).trim());
    MG07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG07@")).trim());
    MG08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG08@")).trim());
    MG09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG09@")).trim());
    MG10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG10@")).trim());
    MG11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG11@")).trim());
    MG12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG12@")).trim());
    MG13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG13@")).trim());
    MG14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG14@")).trim());
    MG15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MG15@")).trim());
    PL01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL01@")).trim());
    PL02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL02@")).trim());
    PL03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL03@")).trim());
    PL04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL04@")).trim());
    PL05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL05@")).trim());
    PL06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL06@")).trim());
    PL07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL07@")).trim());
    PL08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL08@")).trim());
    PL09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL09@")).trim());
    PL10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL10@")).trim());
    PL11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL11@")).trim());
    PL12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL12@")).trim());
    PL13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL13@")).trim());
    PL14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL14@")).trim());
    PL15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_61.setVisible(lexique.isPresent("MALIB"));
    OBJ_66.setVisible(lexique.isPresent("P15LIB"));
    MG01.setVisible(TAD1.isVisible());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    LD01.setVisible(!lexique.HostFieldGetData("LD01").trim().equals(""));
    LD02.setVisible(!lexique.HostFieldGetData("LD02").trim().equals(""));
    LD03.setVisible(!lexique.HostFieldGetData("LD03").trim().equals(""));
    LD04.setVisible(!lexique.HostFieldGetData("LD04").trim().equals(""));
    LD05.setVisible(!lexique.HostFieldGetData("LD05").trim().equals(""));
    LD06.setVisible(!lexique.HostFieldGetData("LD06").trim().equals(""));
    LD07.setVisible(!lexique.HostFieldGetData("LD07").trim().equals(""));
    LD08.setVisible(!lexique.HostFieldGetData("LD08").trim().equals(""));
    LD09.setVisible(!lexique.HostFieldGetData("LD09").trim().equals(""));
    LD10.setVisible(!lexique.HostFieldGetData("LD10").trim().equals(""));
    LD11.setVisible(!lexique.HostFieldGetData("LD11").trim().equals(""));
    LD12.setVisible(!lexique.HostFieldGetData("LD12").trim().equals(""));
    LD13.setVisible(!lexique.HostFieldGetData("LD13").trim().equals(""));
    LD14.setVisible(!lexique.HostFieldGetData("LD14").trim().equals(""));
    LD15.setVisible(!lexique.HostFieldGetData("LD15").trim().equals(""));
    
    MG01.setVisible(!lexique.HostFieldGetData("MG01").trim().equals(""));
    MG02.setVisible(!lexique.HostFieldGetData("MG02").trim().equals(""));
    MG03.setVisible(!lexique.HostFieldGetData("MG03").trim().equals(""));
    MG04.setVisible(!lexique.HostFieldGetData("MG04").trim().equals(""));
    MG05.setVisible(!lexique.HostFieldGetData("MG05").trim().equals(""));
    MG06.setVisible(!lexique.HostFieldGetData("MG06").trim().equals(""));
    MG07.setVisible(!lexique.HostFieldGetData("MG07").trim().equals(""));
    MG08.setVisible(!lexique.HostFieldGetData("MG08").trim().equals(""));
    MG09.setVisible(!lexique.HostFieldGetData("MG09").trim().equals(""));
    MG10.setVisible(!lexique.HostFieldGetData("MG10").trim().equals(""));
    MG11.setVisible(!lexique.HostFieldGetData("MG11").trim().equals(""));
    MG12.setVisible(!lexique.HostFieldGetData("MG12").trim().equals(""));
    MG13.setVisible(!lexique.HostFieldGetData("MG13").trim().equals(""));
    MG14.setVisible(!lexique.HostFieldGetData("MG14").trim().equals(""));
    MG15.setVisible(!lexique.HostFieldGetData("MG15").trim().equals(""));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void DETAILActionPerformed(ActionEvent e) {
    clicDroitLigne(BTD.getInvoker().getY(), "1");
  }
  
  private void clicDroitLigne(int position, String valeur) {
    String top = null;
    if (position == LD01.getY() || position == LD01.getY() - 2) {
      top = "WTP01";
    }
    else if (position == LD02.getY() || position == LD02.getY() - 2) {
      top = "WTP02";
    }
    else if (position == LD03.getY() || position == LD03.getY() - 2) {
      top = "WTP03";
    }
    else if (position == LD04.getY() || position == LD04.getY() - 2) {
      top = "WTP04";
    }
    else if (position == LD05.getY() || position == LD05.getY() - 2) {
      top = "WTP05";
    }
    else if (position == LD06.getY() || position == LD06.getY() - 2) {
      top = "WTP06";
    }
    else if (position == LD07.getY() || position == LD07.getY() - 2) {
      top = "WTP07";
    }
    else if (position == LD08.getY() || position == LD08.getY() - 2) {
      top = "WTP08";
    }
    else if (position == LD09.getY() || position == LD09.getY() - 2) {
      top = "WTP09";
    }
    else if (position == LD10.getY() || position == LD10.getY() - 2) {
      top = "WTP10";
    }
    else if (position == LD11.getY() || position == LD11.getY() - 2) {
      top = "WTP11";
    }
    else if (position == LD12.getY() || position == LD12.getY() - 2) {
      top = "WTP12";
    }
    else if (position == LD13.getY() || position == LD13.getY() - 2) {
      top = "WTP13";
    }
    else if (position == LD14.getY() || position == LD14.getY() - 2) {
      top = "WTP14";
    }
    else if (position == LD15.getY() || position == LD15.getY() - 2) {
      top = "WTP15";
    }
    
    if (top != null) {
      lexique.HostFieldPutData(top, 1, valeur);
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    clicDroitLigne(BTD.getInvoker().getY(), "6");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_61 = new RiZoneSortie();
    OBJ_57 = new JLabel();
    OBJ_59 = new JLabel();
    WETB = new XRiTextField();
    WMAG = new XRiTextField();
    OBJ_66 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    LD01 = new RiZoneSortie();
    LD02 = new RiZoneSortie();
    LD03 = new RiZoneSortie();
    LD04 = new RiZoneSortie();
    LD05 = new RiZoneSortie();
    LD06 = new RiZoneSortie();
    LD07 = new RiZoneSortie();
    LD08 = new RiZoneSortie();
    LD09 = new RiZoneSortie();
    LD10 = new RiZoneSortie();
    LD11 = new RiZoneSortie();
    LD12 = new RiZoneSortie();
    LD13 = new RiZoneSortie();
    LD14 = new RiZoneSortie();
    LD15 = new RiZoneSortie();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    TAD1 = new XRiTextField();
    TAD2 = new XRiTextField();
    TAD3 = new XRiTextField();
    TAD4 = new XRiTextField();
    TAD5 = new XRiTextField();
    TAD6 = new XRiTextField();
    TAD7 = new XRiTextField();
    TAD8 = new XRiTextField();
    TAD9 = new XRiTextField();
    TAD10 = new XRiTextField();
    TAD11 = new XRiTextField();
    TAD12 = new XRiTextField();
    TAD13 = new XRiTextField();
    TAD14 = new XRiTextField();
    TAD15 = new XRiTextField();
    HLD01 = new RiZoneSortie();
    MG01 = new RiZoneSortie();
    MG02 = new RiZoneSortie();
    MG03 = new RiZoneSortie();
    MG04 = new RiZoneSortie();
    MG05 = new RiZoneSortie();
    MG06 = new RiZoneSortie();
    MG07 = new RiZoneSortie();
    MG08 = new RiZoneSortie();
    MG09 = new RiZoneSortie();
    MG10 = new RiZoneSortie();
    MG11 = new RiZoneSortie();
    MG12 = new RiZoneSortie();
    MG13 = new RiZoneSortie();
    MG14 = new RiZoneSortie();
    MG15 = new RiZoneSortie();
    AD11 = new XRiTextField();
    AD12 = new XRiTextField();
    AD13 = new XRiTextField();
    AD14 = new XRiTextField();
    AD15 = new XRiTextField();
    AD16 = new XRiTextField();
    AD17 = new XRiTextField();
    AD18 = new XRiTextField();
    AD19 = new XRiTextField();
    AD110 = new XRiTextField();
    AD111 = new XRiTextField();
    AD112 = new XRiTextField();
    AD113 = new XRiTextField();
    AD114 = new XRiTextField();
    AD115 = new XRiTextField();
    AD1 = new XRiTextField();
    riZoneSortie1 = new RiZoneSortie();
    AD2 = new XRiTextField();
    AD3 = new XRiTextField();
    AD4 = new XRiTextField();
    AD21 = new XRiTextField();
    AD31 = new XRiTextField();
    AD41 = new XRiTextField();
    AD22 = new XRiTextField();
    AD23 = new XRiTextField();
    AD24 = new XRiTextField();
    AD25 = new XRiTextField();
    AD26 = new XRiTextField();
    AD27 = new XRiTextField();
    AD28 = new XRiTextField();
    AD29 = new XRiTextField();
    AD210 = new XRiTextField();
    AD211 = new XRiTextField();
    AD212 = new XRiTextField();
    AD213 = new XRiTextField();
    AD214 = new XRiTextField();
    AD215 = new XRiTextField();
    AD32 = new XRiTextField();
    AD33 = new XRiTextField();
    AD34 = new XRiTextField();
    AD35 = new XRiTextField();
    AD36 = new XRiTextField();
    AD37 = new XRiTextField();
    AD38 = new XRiTextField();
    AD39 = new XRiTextField();
    AD310 = new XRiTextField();
    AD311 = new XRiTextField();
    AD312 = new XRiTextField();
    AD313 = new XRiTextField();
    AD314 = new XRiTextField();
    AD315 = new XRiTextField();
    AD42 = new XRiTextField();
    AD43 = new XRiTextField();
    AD44 = new XRiTextField();
    AD45 = new XRiTextField();
    AD46 = new XRiTextField();
    AD47 = new XRiTextField();
    AD48 = new XRiTextField();
    AD49 = new XRiTextField();
    AD410 = new XRiTextField();
    AD411 = new XRiTextField();
    AD412 = new XRiTextField();
    AD413 = new XRiTextField();
    AD414 = new XRiTextField();
    AD415 = new XRiTextField();
    PL01 = new JLabel();
    PL02 = new JLabel();
    PL03 = new JLabel();
    PL04 = new JLabel();
    PL05 = new JLabel();
    PL06 = new JLabel();
    PL07 = new JLabel();
    PL08 = new JLabel();
    PL09 = new JLabel();
    PL10 = new JLabel();
    PL11 = new JLabel();
    PL12 = new JLabel();
    PL13 = new JLabel();
    PL14 = new JLabel();
    PL15 = new JLabel();
    BTD = new JPopupMenu();
    DETAIL = new JMenuItem();
    menuItem1 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Adresses de stockage");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(770, 32));
          p_tete_gauche.setMinimumSize(new Dimension(770, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_61 ----
          OBJ_61.setText("@MALIB@");
          OBJ_61.setOpaque(false);
          OBJ_61.setName("OBJ_61");
          p_tete_gauche.add(OBJ_61);
          OBJ_61.setBounds(265, 2, 244, OBJ_61.getPreferredSize().height);

          //---- OBJ_57 ----
          OBJ_57.setText("Etablissement");
          OBJ_57.setName("OBJ_57");
          p_tete_gauche.add(OBJ_57);
          OBJ_57.setBounds(5, 5, 100, 18);

          //---- OBJ_59 ----
          OBJ_59.setText("Magasin");
          OBJ_59.setName("OBJ_59");
          p_tete_gauche.add(OBJ_59);
          OBJ_59.setBounds(160, 5, 60, 18);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(100, 0, 40, WETB.getPreferredSize().height);

          //---- WMAG ----
          WMAG.setComponentPopupMenu(BTD);
          WMAG.setName("WMAG");
          p_tete_gauche.add(WMAG);
          WMAG.setBounds(220, 0, 34, WMAG.getPreferredSize().height);

          //---- OBJ_66 ----
          OBJ_66.setText("@P15LIB@");
          OBJ_66.setOpaque(false);
          OBJ_66.setName("OBJ_66");
          p_tete_gauche.add(OBJ_66);
          OBJ_66.setBounds(515, 2, 375, OBJ_66.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(100, 0));
          p_tete_droite.setMinimumSize(new Dimension(100, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Modification d\u00e9nomination");
              riSousMenu_bt6.setToolTipText("Modification d\u00e9nomination");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- LD01 ----
            LD01.setText("@LD01@");
            LD01.setComponentPopupMenu(BTD);
            LD01.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD01.setName("LD01");
            panel1.add(LD01);
            LD01.setBounds(30, 70, 410, LD01.getPreferredSize().height);

            //---- LD02 ----
            LD02.setText("@LD02@");
            LD02.setComponentPopupMenu(BTD);
            LD02.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD02.setName("LD02");
            panel1.add(LD02);
            LD02.setBounds(30, 95, 410, 24);

            //---- LD03 ----
            LD03.setText("@LD03@");
            LD03.setComponentPopupMenu(BTD);
            LD03.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD03.setName("LD03");
            panel1.add(LD03);
            LD03.setBounds(30, 120, 410, 24);

            //---- LD04 ----
            LD04.setText("@LD04@");
            LD04.setComponentPopupMenu(BTD);
            LD04.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD04.setName("LD04");
            panel1.add(LD04);
            LD04.setBounds(30, 145, 410, 24);

            //---- LD05 ----
            LD05.setText("@LD05@");
            LD05.setComponentPopupMenu(BTD);
            LD05.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD05.setName("LD05");
            panel1.add(LD05);
            LD05.setBounds(30, 170, 410, 24);

            //---- LD06 ----
            LD06.setText("@LD06@");
            LD06.setComponentPopupMenu(BTD);
            LD06.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD06.setName("LD06");
            panel1.add(LD06);
            LD06.setBounds(30, 195, 410, 24);

            //---- LD07 ----
            LD07.setText("@LD07@");
            LD07.setComponentPopupMenu(BTD);
            LD07.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD07.setName("LD07");
            panel1.add(LD07);
            LD07.setBounds(30, 220, 410, 24);

            //---- LD08 ----
            LD08.setText("@LD08@");
            LD08.setComponentPopupMenu(BTD);
            LD08.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD08.setName("LD08");
            panel1.add(LD08);
            LD08.setBounds(30, 245, 410, 24);

            //---- LD09 ----
            LD09.setText("@LD09@");
            LD09.setComponentPopupMenu(BTD);
            LD09.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD09.setName("LD09");
            panel1.add(LD09);
            LD09.setBounds(30, 270, 410, 24);

            //---- LD10 ----
            LD10.setText("@LD10@");
            LD10.setComponentPopupMenu(BTD);
            LD10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD10.setName("LD10");
            panel1.add(LD10);
            LD10.setBounds(30, 295, 410, 24);

            //---- LD11 ----
            LD11.setText("@LD11@");
            LD11.setComponentPopupMenu(BTD);
            LD11.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD11.setName("LD11");
            panel1.add(LD11);
            LD11.setBounds(30, 320, 410, 24);

            //---- LD12 ----
            LD12.setText("@LD12@");
            LD12.setComponentPopupMenu(BTD);
            LD12.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD12.setName("LD12");
            panel1.add(LD12);
            LD12.setBounds(30, 345, 410, 24);

            //---- LD13 ----
            LD13.setText("@LD13@");
            LD13.setComponentPopupMenu(BTD);
            LD13.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD13.setName("LD13");
            panel1.add(LD13);
            LD13.setBounds(30, 370, 410, 24);

            //---- LD14 ----
            LD14.setText("@LD14@");
            LD14.setComponentPopupMenu(BTD);
            LD14.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD14.setName("LD14");
            panel1.add(LD14);
            LD14.setBounds(30, 395, 410, 24);

            //---- LD15 ----
            LD15.setText("@LD15@");
            LD15.setComponentPopupMenu(BTD);
            LD15.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LD15.setName("LD15");
            panel1.add(LD15);
            LD15.setBounds(30, 420, 410, 24);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(817, 70, 25, 165);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(817, 280, 25, 165);

            //---- TAD1 ----
            TAD1.setComponentPopupMenu(BTD);
            TAD1.setName("TAD1");
            panel1.add(TAD1);
            TAD1.setBounds(445, 68, 20, TAD1.getPreferredSize().height);

            //---- TAD2 ----
            TAD2.setComponentPopupMenu(BTD);
            TAD2.setName("TAD2");
            panel1.add(TAD2);
            TAD2.setBounds(445, 93, 20, 28);

            //---- TAD3 ----
            TAD3.setComponentPopupMenu(BTD);
            TAD3.setName("TAD3");
            panel1.add(TAD3);
            TAD3.setBounds(445, 118, 20, 28);

            //---- TAD4 ----
            TAD4.setComponentPopupMenu(BTD);
            TAD4.setName("TAD4");
            panel1.add(TAD4);
            TAD4.setBounds(445, 143, 20, 28);

            //---- TAD5 ----
            TAD5.setComponentPopupMenu(BTD);
            TAD5.setName("TAD5");
            panel1.add(TAD5);
            TAD5.setBounds(445, 168, 20, 28);

            //---- TAD6 ----
            TAD6.setComponentPopupMenu(BTD);
            TAD6.setName("TAD6");
            panel1.add(TAD6);
            TAD6.setBounds(445, 193, 20, 28);

            //---- TAD7 ----
            TAD7.setComponentPopupMenu(BTD);
            TAD7.setName("TAD7");
            panel1.add(TAD7);
            TAD7.setBounds(445, 218, 20, 28);

            //---- TAD8 ----
            TAD8.setComponentPopupMenu(BTD);
            TAD8.setName("TAD8");
            panel1.add(TAD8);
            TAD8.setBounds(445, 243, 20, 28);

            //---- TAD9 ----
            TAD9.setComponentPopupMenu(BTD);
            TAD9.setName("TAD9");
            panel1.add(TAD9);
            TAD9.setBounds(445, 268, 20, 28);

            //---- TAD10 ----
            TAD10.setComponentPopupMenu(BTD);
            TAD10.setName("TAD10");
            panel1.add(TAD10);
            TAD10.setBounds(445, 293, 20, 28);

            //---- TAD11 ----
            TAD11.setComponentPopupMenu(BTD);
            TAD11.setName("TAD11");
            panel1.add(TAD11);
            TAD11.setBounds(445, 318, 20, 28);

            //---- TAD12 ----
            TAD12.setComponentPopupMenu(BTD);
            TAD12.setName("TAD12");
            panel1.add(TAD12);
            TAD12.setBounds(445, 343, 20, 28);

            //---- TAD13 ----
            TAD13.setComponentPopupMenu(BTD);
            TAD13.setName("TAD13");
            panel1.add(TAD13);
            TAD13.setBounds(445, 368, 20, 28);

            //---- TAD14 ----
            TAD14.setComponentPopupMenu(BTD);
            TAD14.setName("TAD14");
            panel1.add(TAD14);
            TAD14.setBounds(445, 393, 20, 28);

            //---- TAD15 ----
            TAD15.setComponentPopupMenu(BTD);
            TAD15.setName("TAD15");
            panel1.add(TAD15);
            TAD15.setBounds(445, 418, 20, 28);

            //---- HLD01 ----
            HLD01.setText("@UTIT@");
            HLD01.setBackground(new Color(250, 250, 239));
            HLD01.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            HLD01.setName("HLD01");
            panel1.add(HLD01);
            HLD01.setBounds(30, 40, 410, 24);

            //---- MG01 ----
            MG01.setText("@MG01@");
            MG01.setComponentPopupMenu(BTD);
            MG01.setName("MG01");
            panel1.add(MG01);
            MG01.setBounds(470, 70, 34, MG01.getPreferredSize().height);

            //---- MG02 ----
            MG02.setText("@MG02@");
            MG02.setComponentPopupMenu(BTD);
            MG02.setName("MG02");
            panel1.add(MG02);
            MG02.setBounds(470, 95, 34, 24);

            //---- MG03 ----
            MG03.setText("@MG03@");
            MG03.setComponentPopupMenu(BTD);
            MG03.setName("MG03");
            panel1.add(MG03);
            MG03.setBounds(470, 120, 34, 24);

            //---- MG04 ----
            MG04.setText("@MG04@");
            MG04.setComponentPopupMenu(BTD);
            MG04.setName("MG04");
            panel1.add(MG04);
            MG04.setBounds(470, 145, 34, 24);

            //---- MG05 ----
            MG05.setText("@MG05@");
            MG05.setComponentPopupMenu(BTD);
            MG05.setName("MG05");
            panel1.add(MG05);
            MG05.setBounds(470, 170, 34, 24);

            //---- MG06 ----
            MG06.setText("@MG06@");
            MG06.setComponentPopupMenu(BTD);
            MG06.setName("MG06");
            panel1.add(MG06);
            MG06.setBounds(470, 195, 34, 24);

            //---- MG07 ----
            MG07.setText("@MG07@");
            MG07.setComponentPopupMenu(BTD);
            MG07.setName("MG07");
            panel1.add(MG07);
            MG07.setBounds(470, 220, 34, 24);

            //---- MG08 ----
            MG08.setText("@MG08@");
            MG08.setComponentPopupMenu(BTD);
            MG08.setName("MG08");
            panel1.add(MG08);
            MG08.setBounds(470, 245, 34, 24);

            //---- MG09 ----
            MG09.setText("@MG09@");
            MG09.setComponentPopupMenu(BTD);
            MG09.setName("MG09");
            panel1.add(MG09);
            MG09.setBounds(470, 270, 34, 24);

            //---- MG10 ----
            MG10.setText("@MG10@");
            MG10.setComponentPopupMenu(BTD);
            MG10.setName("MG10");
            panel1.add(MG10);
            MG10.setBounds(470, 295, 34, 24);

            //---- MG11 ----
            MG11.setText("@MG11@");
            MG11.setComponentPopupMenu(BTD);
            MG11.setName("MG11");
            panel1.add(MG11);
            MG11.setBounds(470, 320, 34, 24);

            //---- MG12 ----
            MG12.setText("@MG12@");
            MG12.setComponentPopupMenu(BTD);
            MG12.setName("MG12");
            panel1.add(MG12);
            MG12.setBounds(470, 345, 34, 24);

            //---- MG13 ----
            MG13.setText("@MG13@");
            MG13.setComponentPopupMenu(BTD);
            MG13.setName("MG13");
            panel1.add(MG13);
            MG13.setBounds(470, 370, 34, 24);

            //---- MG14 ----
            MG14.setText("@MG14@");
            MG14.setComponentPopupMenu(BTD);
            MG14.setName("MG14");
            panel1.add(MG14);
            MG14.setBounds(470, 395, 34, 24);

            //---- MG15 ----
            MG15.setText("@MG15@");
            MG15.setComponentPopupMenu(BTD);
            MG15.setName("MG15");
            panel1.add(MG15);
            MG15.setBounds(470, 420, 34, 24);

            //---- AD11 ----
            AD11.setComponentPopupMenu(BTD);
            AD11.setName("AD11");
            panel1.add(AD11);
            AD11.setBounds(510, 68, 40, AD11.getPreferredSize().height);

            //---- AD12 ----
            AD12.setComponentPopupMenu(BTD);
            AD12.setName("AD12");
            panel1.add(AD12);
            AD12.setBounds(510, 93, 40, 28);

            //---- AD13 ----
            AD13.setComponentPopupMenu(BTD);
            AD13.setName("AD13");
            panel1.add(AD13);
            AD13.setBounds(510, 118, 40, 28);

            //---- AD14 ----
            AD14.setComponentPopupMenu(BTD);
            AD14.setName("AD14");
            panel1.add(AD14);
            AD14.setBounds(510, 143, 40, 28);

            //---- AD15 ----
            AD15.setComponentPopupMenu(BTD);
            AD15.setName("AD15");
            panel1.add(AD15);
            AD15.setBounds(510, 168, 40, 28);

            //---- AD16 ----
            AD16.setComponentPopupMenu(BTD);
            AD16.setName("AD16");
            panel1.add(AD16);
            AD16.setBounds(510, 193, 40, 28);

            //---- AD17 ----
            AD17.setComponentPopupMenu(BTD);
            AD17.setName("AD17");
            panel1.add(AD17);
            AD17.setBounds(510, 218, 40, 28);

            //---- AD18 ----
            AD18.setComponentPopupMenu(BTD);
            AD18.setName("AD18");
            panel1.add(AD18);
            AD18.setBounds(510, 243, 40, 28);

            //---- AD19 ----
            AD19.setComponentPopupMenu(BTD);
            AD19.setName("AD19");
            panel1.add(AD19);
            AD19.setBounds(510, 268, 40, 28);

            //---- AD110 ----
            AD110.setComponentPopupMenu(BTD);
            AD110.setName("AD110");
            panel1.add(AD110);
            AD110.setBounds(510, 293, 40, 28);

            //---- AD111 ----
            AD111.setComponentPopupMenu(BTD);
            AD111.setName("AD111");
            panel1.add(AD111);
            AD111.setBounds(510, 318, 40, 28);

            //---- AD112 ----
            AD112.setComponentPopupMenu(BTD);
            AD112.setName("AD112");
            panel1.add(AD112);
            AD112.setBounds(510, 343, 40, 28);

            //---- AD113 ----
            AD113.setComponentPopupMenu(BTD);
            AD113.setName("AD113");
            panel1.add(AD113);
            AD113.setBounds(510, 368, 40, 28);

            //---- AD114 ----
            AD114.setComponentPopupMenu(BTD);
            AD114.setName("AD114");
            panel1.add(AD114);
            AD114.setBounds(510, 393, 40, 28);

            //---- AD115 ----
            AD115.setComponentPopupMenu(BTD);
            AD115.setName("AD115");
            panel1.add(AD115);
            AD115.setBounds(510, 418, 40, 28);

            //---- AD1 ----
            AD1.setName("AD1");
            panel1.add(AD1);
            AD1.setBounds(470, 38, 80, AD1.getPreferredSize().height);

            //---- riZoneSortie1 ----
            riZoneSortie1.setText("T");
            riZoneSortie1.setHorizontalAlignment(SwingConstants.CENTER);
            riZoneSortie1.setName("riZoneSortie1");
            panel1.add(riZoneSortie1);
            riZoneSortie1.setBounds(445, 40, 20, riZoneSortie1.getPreferredSize().height);

            //---- AD2 ----
            AD2.setName("AD2");
            panel1.add(AD2);
            AD2.setBounds(555, 38, 80, 28);

            //---- AD3 ----
            AD3.setName("AD3");
            panel1.add(AD3);
            AD3.setBounds(640, 38, 80, 28);

            //---- AD4 ----
            AD4.setName("AD4");
            panel1.add(AD4);
            AD4.setBounds(725, 38, 80, 28);

            //---- AD21 ----
            AD21.setComponentPopupMenu(BTD);
            AD21.setName("AD21");
            panel1.add(AD21);
            AD21.setBounds(575, 68, 40, 28);

            //---- AD31 ----
            AD31.setComponentPopupMenu(BTD);
            AD31.setName("AD31");
            panel1.add(AD31);
            AD31.setBounds(658, 68, 40, 28);

            //---- AD41 ----
            AD41.setComponentPopupMenu(BTD);
            AD41.setName("AD41");
            panel1.add(AD41);
            AD41.setBounds(740, 68, 40, 28);

            //---- AD22 ----
            AD22.setComponentPopupMenu(BTD);
            AD22.setName("AD22");
            panel1.add(AD22);
            AD22.setBounds(575, 93, 40, 28);

            //---- AD23 ----
            AD23.setComponentPopupMenu(BTD);
            AD23.setName("AD23");
            panel1.add(AD23);
            AD23.setBounds(575, 118, 40, 28);

            //---- AD24 ----
            AD24.setComponentPopupMenu(BTD);
            AD24.setName("AD24");
            panel1.add(AD24);
            AD24.setBounds(575, 143, 40, 28);

            //---- AD25 ----
            AD25.setComponentPopupMenu(BTD);
            AD25.setName("AD25");
            panel1.add(AD25);
            AD25.setBounds(575, 168, 40, 28);

            //---- AD26 ----
            AD26.setComponentPopupMenu(BTD);
            AD26.setName("AD26");
            panel1.add(AD26);
            AD26.setBounds(575, 193, 40, 28);

            //---- AD27 ----
            AD27.setComponentPopupMenu(BTD);
            AD27.setName("AD27");
            panel1.add(AD27);
            AD27.setBounds(575, 218, 40, 28);

            //---- AD28 ----
            AD28.setComponentPopupMenu(BTD);
            AD28.setName("AD28");
            panel1.add(AD28);
            AD28.setBounds(575, 243, 40, 28);

            //---- AD29 ----
            AD29.setComponentPopupMenu(BTD);
            AD29.setName("AD29");
            panel1.add(AD29);
            AD29.setBounds(575, 268, 40, 28);

            //---- AD210 ----
            AD210.setComponentPopupMenu(BTD);
            AD210.setName("AD210");
            panel1.add(AD210);
            AD210.setBounds(575, 293, 40, 28);

            //---- AD211 ----
            AD211.setComponentPopupMenu(BTD);
            AD211.setName("AD211");
            panel1.add(AD211);
            AD211.setBounds(575, 318, 40, 28);

            //---- AD212 ----
            AD212.setComponentPopupMenu(BTD);
            AD212.setName("AD212");
            panel1.add(AD212);
            AD212.setBounds(575, 343, 40, 28);

            //---- AD213 ----
            AD213.setComponentPopupMenu(BTD);
            AD213.setName("AD213");
            panel1.add(AD213);
            AD213.setBounds(575, 368, 40, 28);

            //---- AD214 ----
            AD214.setComponentPopupMenu(BTD);
            AD214.setName("AD214");
            panel1.add(AD214);
            AD214.setBounds(575, 393, 40, 28);

            //---- AD215 ----
            AD215.setComponentPopupMenu(BTD);
            AD215.setName("AD215");
            panel1.add(AD215);
            AD215.setBounds(575, 418, 40, 28);

            //---- AD32 ----
            AD32.setComponentPopupMenu(BTD);
            AD32.setName("AD32");
            panel1.add(AD32);
            AD32.setBounds(658, 93, 40, 28);

            //---- AD33 ----
            AD33.setComponentPopupMenu(BTD);
            AD33.setName("AD33");
            panel1.add(AD33);
            AD33.setBounds(658, 118, 40, 28);

            //---- AD34 ----
            AD34.setComponentPopupMenu(BTD);
            AD34.setName("AD34");
            panel1.add(AD34);
            AD34.setBounds(658, 143, 40, 28);

            //---- AD35 ----
            AD35.setComponentPopupMenu(BTD);
            AD35.setName("AD35");
            panel1.add(AD35);
            AD35.setBounds(658, 168, 40, 28);

            //---- AD36 ----
            AD36.setComponentPopupMenu(BTD);
            AD36.setName("AD36");
            panel1.add(AD36);
            AD36.setBounds(658, 193, 40, 28);

            //---- AD37 ----
            AD37.setComponentPopupMenu(BTD);
            AD37.setName("AD37");
            panel1.add(AD37);
            AD37.setBounds(658, 218, 40, 28);

            //---- AD38 ----
            AD38.setComponentPopupMenu(BTD);
            AD38.setName("AD38");
            panel1.add(AD38);
            AD38.setBounds(658, 243, 40, 28);

            //---- AD39 ----
            AD39.setComponentPopupMenu(BTD);
            AD39.setName("AD39");
            panel1.add(AD39);
            AD39.setBounds(658, 268, 40, 28);

            //---- AD310 ----
            AD310.setComponentPopupMenu(BTD);
            AD310.setName("AD310");
            panel1.add(AD310);
            AD310.setBounds(658, 293, 40, 28);

            //---- AD311 ----
            AD311.setComponentPopupMenu(BTD);
            AD311.setName("AD311");
            panel1.add(AD311);
            AD311.setBounds(658, 318, 40, 28);

            //---- AD312 ----
            AD312.setComponentPopupMenu(BTD);
            AD312.setName("AD312");
            panel1.add(AD312);
            AD312.setBounds(658, 343, 40, 28);

            //---- AD313 ----
            AD313.setComponentPopupMenu(BTD);
            AD313.setName("AD313");
            panel1.add(AD313);
            AD313.setBounds(658, 368, 40, 28);

            //---- AD314 ----
            AD314.setComponentPopupMenu(BTD);
            AD314.setName("AD314");
            panel1.add(AD314);
            AD314.setBounds(658, 393, 40, 28);

            //---- AD315 ----
            AD315.setComponentPopupMenu(BTD);
            AD315.setName("AD315");
            panel1.add(AD315);
            AD315.setBounds(658, 418, 40, 28);

            //---- AD42 ----
            AD42.setComponentPopupMenu(BTD);
            AD42.setName("AD42");
            panel1.add(AD42);
            AD42.setBounds(740, 93, 40, 28);

            //---- AD43 ----
            AD43.setComponentPopupMenu(BTD);
            AD43.setName("AD43");
            panel1.add(AD43);
            AD43.setBounds(740, 118, 40, 28);

            //---- AD44 ----
            AD44.setComponentPopupMenu(BTD);
            AD44.setName("AD44");
            panel1.add(AD44);
            AD44.setBounds(740, 143, 40, 28);

            //---- AD45 ----
            AD45.setComponentPopupMenu(BTD);
            AD45.setName("AD45");
            panel1.add(AD45);
            AD45.setBounds(740, 168, 40, 28);

            //---- AD46 ----
            AD46.setComponentPopupMenu(BTD);
            AD46.setName("AD46");
            panel1.add(AD46);
            AD46.setBounds(740, 193, 40, 28);

            //---- AD47 ----
            AD47.setComponentPopupMenu(BTD);
            AD47.setName("AD47");
            panel1.add(AD47);
            AD47.setBounds(740, 218, 40, 28);

            //---- AD48 ----
            AD48.setComponentPopupMenu(BTD);
            AD48.setName("AD48");
            panel1.add(AD48);
            AD48.setBounds(740, 243, 40, 28);

            //---- AD49 ----
            AD49.setComponentPopupMenu(BTD);
            AD49.setName("AD49");
            panel1.add(AD49);
            AD49.setBounds(740, 268, 40, 28);

            //---- AD410 ----
            AD410.setComponentPopupMenu(BTD);
            AD410.setName("AD410");
            panel1.add(AD410);
            AD410.setBounds(740, 293, 40, 28);

            //---- AD411 ----
            AD411.setComponentPopupMenu(BTD);
            AD411.setName("AD411");
            panel1.add(AD411);
            AD411.setBounds(740, 318, 40, 28);

            //---- AD412 ----
            AD412.setComponentPopupMenu(BTD);
            AD412.setName("AD412");
            panel1.add(AD412);
            AD412.setBounds(740, 343, 40, 28);

            //---- AD413 ----
            AD413.setComponentPopupMenu(BTD);
            AD413.setName("AD413");
            panel1.add(AD413);
            AD413.setBounds(740, 368, 40, 28);

            //---- AD414 ----
            AD414.setComponentPopupMenu(BTD);
            AD414.setName("AD414");
            panel1.add(AD414);
            AD414.setBounds(740, 393, 40, 28);

            //---- AD415 ----
            AD415.setComponentPopupMenu(BTD);
            AD415.setName("AD415");
            panel1.add(AD415);
            AD415.setBounds(740, 418, 40, 28);

            //---- PL01 ----
            PL01.setText("@PL01@");
            PL01.setComponentPopupMenu(BTD);
            PL01.setName("PL01");
            panel1.add(PL01);
            PL01.setBounds(783, 68, 20, 28);

            //---- PL02 ----
            PL02.setText("@PL02@");
            PL02.setComponentPopupMenu(BTD);
            PL02.setName("PL02");
            panel1.add(PL02);
            PL02.setBounds(783, 93, 20, 28);

            //---- PL03 ----
            PL03.setText("@PL03@");
            PL03.setComponentPopupMenu(BTD);
            PL03.setName("PL03");
            panel1.add(PL03);
            PL03.setBounds(783, 118, 20, 28);

            //---- PL04 ----
            PL04.setText("@PL04@");
            PL04.setComponentPopupMenu(BTD);
            PL04.setName("PL04");
            panel1.add(PL04);
            PL04.setBounds(783, 143, 20, 28);

            //---- PL05 ----
            PL05.setText("@PL05@");
            PL05.setComponentPopupMenu(BTD);
            PL05.setName("PL05");
            panel1.add(PL05);
            PL05.setBounds(783, 168, 20, 28);

            //---- PL06 ----
            PL06.setText("@PL06@");
            PL06.setComponentPopupMenu(BTD);
            PL06.setName("PL06");
            panel1.add(PL06);
            PL06.setBounds(783, 193, 20, 28);

            //---- PL07 ----
            PL07.setText("@PL07@");
            PL07.setComponentPopupMenu(BTD);
            PL07.setName("PL07");
            panel1.add(PL07);
            PL07.setBounds(783, 218, 20, 28);

            //---- PL08 ----
            PL08.setText("@PL08@");
            PL08.setComponentPopupMenu(BTD);
            PL08.setName("PL08");
            panel1.add(PL08);
            PL08.setBounds(783, 243, 20, 28);

            //---- PL09 ----
            PL09.setText("@PL09@");
            PL09.setComponentPopupMenu(BTD);
            PL09.setName("PL09");
            panel1.add(PL09);
            PL09.setBounds(783, 268, 20, 28);

            //---- PL10 ----
            PL10.setText("@PL10@");
            PL10.setComponentPopupMenu(BTD);
            PL10.setName("PL10");
            panel1.add(PL10);
            PL10.setBounds(783, 293, 20, 28);

            //---- PL11 ----
            PL11.setText("@PL11@");
            PL11.setComponentPopupMenu(BTD);
            PL11.setName("PL11");
            panel1.add(PL11);
            PL11.setBounds(783, 318, 20, 28);

            //---- PL12 ----
            PL12.setText("@PL12@");
            PL12.setComponentPopupMenu(BTD);
            PL12.setName("PL12");
            panel1.add(PL12);
            PL12.setBounds(783, 343, 20, 28);

            //---- PL13 ----
            PL13.setText("@PL13@");
            PL13.setComponentPopupMenu(BTD);
            PL13.setName("PL13");
            panel1.add(PL13);
            PL13.setBounds(783, 368, 20, 28);

            //---- PL14 ----
            PL14.setText("@PL14@");
            PL14.setComponentPopupMenu(BTD);
            PL14.setName("PL14");
            panel1.add(PL14);
            PL14.setBounds(783, 393, 20, 28);

            //---- PL15 ----
            PL15.setText("@PL15@");
            PL15.setComponentPopupMenu(BTD);
            PL15.setName("PL15");
            panel1.add(PL15);
            PL15.setBounds(783, 418, 20, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 865, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 475, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- DETAIL ----
      DETAIL.setText("D\u00e9tail");
      DETAIL.setName("DETAIL");
      DETAIL.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          DETAILActionPerformed(e);
        }
      });
      BTD.add(DETAIL);

      //---- menuItem1 ----
      menuItem1.setText("Article lot");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD.add(menuItem1);
    }

    //---- OBJ_8 ----
    OBJ_8.setText("Modification des d\u00e9nomination de zones de stockage");
    OBJ_8.setName("OBJ_8");
    OBJ_8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_8ActionPerformed(e);
      }
    });

    //---- OBJ_11 ----
    OBJ_11.setText("Saisie");
    OBJ_11.setName("OBJ_11");
    OBJ_11.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_11ActionPerformed(e);
      }
    });

    //---- OBJ_10 ----
    OBJ_10.setText("Cr\u00e9ation");
    OBJ_10.setName("OBJ_10");
    OBJ_10.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_10ActionPerformed(e);
      }
    });

    //---- OBJ_12 ----
    OBJ_12.setText("Visualisation");
    OBJ_12.setName("OBJ_12");
    OBJ_12.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_12ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie OBJ_61;
  private JLabel OBJ_57;
  private JLabel OBJ_59;
  private XRiTextField WETB;
  private XRiTextField WMAG;
  private RiZoneSortie OBJ_66;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie LD01;
  private RiZoneSortie LD02;
  private RiZoneSortie LD03;
  private RiZoneSortie LD04;
  private RiZoneSortie LD05;
  private RiZoneSortie LD06;
  private RiZoneSortie LD07;
  private RiZoneSortie LD08;
  private RiZoneSortie LD09;
  private RiZoneSortie LD10;
  private RiZoneSortie LD11;
  private RiZoneSortie LD12;
  private RiZoneSortie LD13;
  private RiZoneSortie LD14;
  private RiZoneSortie LD15;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiTextField TAD1;
  private XRiTextField TAD2;
  private XRiTextField TAD3;
  private XRiTextField TAD4;
  private XRiTextField TAD5;
  private XRiTextField TAD6;
  private XRiTextField TAD7;
  private XRiTextField TAD8;
  private XRiTextField TAD9;
  private XRiTextField TAD10;
  private XRiTextField TAD11;
  private XRiTextField TAD12;
  private XRiTextField TAD13;
  private XRiTextField TAD14;
  private XRiTextField TAD15;
  private RiZoneSortie HLD01;
  private RiZoneSortie MG01;
  private RiZoneSortie MG02;
  private RiZoneSortie MG03;
  private RiZoneSortie MG04;
  private RiZoneSortie MG05;
  private RiZoneSortie MG06;
  private RiZoneSortie MG07;
  private RiZoneSortie MG08;
  private RiZoneSortie MG09;
  private RiZoneSortie MG10;
  private RiZoneSortie MG11;
  private RiZoneSortie MG12;
  private RiZoneSortie MG13;
  private RiZoneSortie MG14;
  private RiZoneSortie MG15;
  private XRiTextField AD11;
  private XRiTextField AD12;
  private XRiTextField AD13;
  private XRiTextField AD14;
  private XRiTextField AD15;
  private XRiTextField AD16;
  private XRiTextField AD17;
  private XRiTextField AD18;
  private XRiTextField AD19;
  private XRiTextField AD110;
  private XRiTextField AD111;
  private XRiTextField AD112;
  private XRiTextField AD113;
  private XRiTextField AD114;
  private XRiTextField AD115;
  private XRiTextField AD1;
  private RiZoneSortie riZoneSortie1;
  private XRiTextField AD2;
  private XRiTextField AD3;
  private XRiTextField AD4;
  private XRiTextField AD21;
  private XRiTextField AD31;
  private XRiTextField AD41;
  private XRiTextField AD22;
  private XRiTextField AD23;
  private XRiTextField AD24;
  private XRiTextField AD25;
  private XRiTextField AD26;
  private XRiTextField AD27;
  private XRiTextField AD28;
  private XRiTextField AD29;
  private XRiTextField AD210;
  private XRiTextField AD211;
  private XRiTextField AD212;
  private XRiTextField AD213;
  private XRiTextField AD214;
  private XRiTextField AD215;
  private XRiTextField AD32;
  private XRiTextField AD33;
  private XRiTextField AD34;
  private XRiTextField AD35;
  private XRiTextField AD36;
  private XRiTextField AD37;
  private XRiTextField AD38;
  private XRiTextField AD39;
  private XRiTextField AD310;
  private XRiTextField AD311;
  private XRiTextField AD312;
  private XRiTextField AD313;
  private XRiTextField AD314;
  private XRiTextField AD315;
  private XRiTextField AD42;
  private XRiTextField AD43;
  private XRiTextField AD44;
  private XRiTextField AD45;
  private XRiTextField AD46;
  private XRiTextField AD47;
  private XRiTextField AD48;
  private XRiTextField AD49;
  private XRiTextField AD410;
  private XRiTextField AD411;
  private XRiTextField AD412;
  private XRiTextField AD413;
  private XRiTextField AD414;
  private XRiTextField AD415;
  private JLabel PL01;
  private JLabel PL02;
  private JLabel PL03;
  private JLabel PL04;
  private JLabel PL05;
  private JLabel PL06;
  private JLabel PL07;
  private JLabel PL08;
  private JLabel PL09;
  private JLabel PL10;
  private JLabel PL11;
  private JLabel PL12;
  private JLabel PL13;
  private JLabel PL14;
  private JLabel PL15;
  private JPopupMenu BTD;
  private JMenuItem DETAIL;
  private JMenuItem menuItem1;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
