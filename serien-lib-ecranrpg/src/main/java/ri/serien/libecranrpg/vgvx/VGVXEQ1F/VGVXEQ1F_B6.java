
package ri.serien.libecranrpg.vgvx.VGVXEQ1F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXEQ1F_B6 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _S6S1_Top =
      { "S6S1", "S6S2", "S6S3", "S6S4", "S6S5", "S6S6", "S6S7", "S6S8", "S6S9", "S6S10", "S6S11", "S6S12", "S6S13", "S6S14", "S6S15", };
  private String[] _S6S1_Title = { "Type", "Numéro", "Référence", "Désignation", };
  private String[][] _S6S1_Data =
      { { "S6T1", "S6E1", "S6F1", "S6L1", }, { "S6T2", "S6E2", "S6F2", "S6L2", }, { "S6T3", "S6E3", "S6F3", "S6L3", },
          { "S6T4", "S6E4", "S6F4", "S6L4", }, { "S6T5", "S6E5", "S6F5", "S6L5", }, { "S6T6", "S6E6", "S6F6", "S6L6", },
          { "S6T7", "S6E7", "S6F7", "S6L7", }, { "S6T8", "S6E8", "S6F8", "S6L8", }, { "S6T9", "S6E9", "S6F9", "S6L9", },
          { "S6T10", "S6E10", "S6F10", "S6L10", }, { "S6T11", "S6E11", "S6F11", "S6L11", }, { "S6T12", "S6E12", "S6F12", "S6L12", },
          { "S6T13", "S6E13", "S6F13", "S6L13", }, { "S6T14", "S6E14", "S6F14", "S6L14", }, { "S6T15", "S6E15", "S6F15", "S6L15", }, };
  private int[] _S6S1_Width = { 50, 50, 70, 210, };
  private JButton[] boutons = null;
  private apercu visuEtq = null;
  
  public VGVXEQ1F_B6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    S6S1.setAspectTable(_S6S1_Top, _S6S1_Title, _S6S1_Data, _S6S1_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _S6S1_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    boutons = new JButton[] { S01, S02, S03, S04, S05, S06, S07, S08, S09, S10, S11, S12, S13, S14, S15 };
    
    // TODO Icones
    for (int i = 0; i < boutons.length; i++) {
      int indic = 21 + i;
      if (lexique.isTrue("" + indic)) {
        boutons[i].setEnabled(false);
        boutons[i].setIcon(lexique.chargerImage("images/vert3.gif", true));
        boutons[i].setToolTipText("Etiquette active");
      }
      else {
        boutons[i].setEnabled(true);
        boutons[i].setIcon(lexique.chargerImage("images/rouge3.gif", true));
        boutons[i].setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
      }
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
    boutons = new JButton[] { S01, S02, S03, S04, S05, S06, S07, S08, S09, S10, S11, S12, S13, S14, S15 };
    
    for (int i = 0; i < boutons.length; i++) {
      int indic = 21 + i;
      if ((!lexique.isTrue("" + indic)) && (boutons[i].getToolTipText().equals("Etiquette active après validation"))) {
        lexique.HostFieldPutData("S6S" + (i + 1), 0, "N");
      }
    }
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void S01ActionPerformed(ActionEvent e) {
    if (S01.getIcon().toString().substring(S01.getIcon().toString().length() - 9, S01.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S01.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S01.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S01.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S01.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S02ActionPerformed(ActionEvent e) {
    if (S02.getIcon().toString().substring(S02.getIcon().toString().length() - 9, S02.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S02.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S02.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S02.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S02.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S03ActionPerformed(ActionEvent e) {
    if (S03.getIcon().toString().substring(S03.getIcon().toString().length() - 9, S03.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S03.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S03.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S03.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S03.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S04ActionPerformed(ActionEvent e) {
    if (S04.getIcon().toString().substring(S04.getIcon().toString().length() - 9, S04.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S04.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S04.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S04.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S04.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S05ActionPerformed(ActionEvent e) {
    if (S05.getIcon().toString().substring(S05.getIcon().toString().length() - 9, S05.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S05.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S05.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S05.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S05.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S06ActionPerformed(ActionEvent e) {
    if (S06.getIcon().toString().substring(S06.getIcon().toString().length() - 9, S06.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S06.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S06.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S06.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S06.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S07ActionPerformed(ActionEvent e) {
    if (S07.getIcon().toString().substring(S07.getIcon().toString().length() - 9, S07.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S07.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S07.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S07.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S07.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S08ActionPerformed(ActionEvent e) {
    if (S08.getIcon().toString().substring(S08.getIcon().toString().length() - 9, S08.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S08.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S08.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S08.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S08.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S09ActionPerformed(ActionEvent e) {
    if (S09.getIcon().toString().substring(S09.getIcon().toString().length() - 9, S09.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S09.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S09.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S09.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S09.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S10ActionPerformed(ActionEvent e) {
    if (S10.getIcon().toString().substring(S10.getIcon().toString().length() - 9, S10.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S10.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S10.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S10.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S10.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S11ActionPerformed(ActionEvent e) {
    if (S11.getIcon().toString().substring(S11.getIcon().toString().length() - 9, S11.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S11.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S11.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S11.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S11.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S12ActionPerformed(ActionEvent e) {
    if (S12.getIcon().toString().substring(S12.getIcon().toString().length() - 9, S12.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S12.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S12.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S12.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S12.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S13ActionPerformed(ActionEvent e) {
    if (S13.getIcon().toString().substring(S13.getIcon().toString().length() - 9, S13.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S13.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S13.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S13.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S13.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S14ActionPerformed(ActionEvent e) {
    if (S14.getIcon().toString().substring(S14.getIcon().toString().length() - 9, S14.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S14.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S14.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S14.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S14.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void S15ActionPerformed(ActionEvent e) {
    if (S15.getIcon().toString().substring(S15.getIcon().toString().length() - 9, S15.getIcon().toString().length())
        .equalsIgnoreCase("vert3.gif")) {
      S15.setIcon(lexique.chargerImage("images/rouge3.gif", true));
      S15.setToolTipText("Etiquette désactivée, cliquez pour l'ajouter");
    }
    else {
      S15.setIcon(lexique.chargerImage("images/vert3.gif", true));
      S15.setToolTipText("Etiquette active après validation");
    }
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    String chemin = null;
    int ligne = S6S1.getSelectedRow() + 1;
    
    // constitution du chemin de l'image
    chemin = lexique.HostFieldGetData("XCRAC").trim() + lexique.HostFieldGetData("XCIFS").trim() + File.separator + "Graphics"
        + File.separator + lexique.HostFieldGetData("S6F" + ligne) + ".JPG";
    visuEtq = new apercu(this, lexique, interpreteurD, chemin);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_53 = new JLabel();
    WETB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    panel1 = new JPanel();
    S01 = new JButton();
    S02 = new JButton();
    S03 = new JButton();
    S04 = new JButton();
    S05 = new JButton();
    S06 = new JButton();
    S07 = new JButton();
    S08 = new JButton();
    S09 = new JButton();
    S10 = new JButton();
    S11 = new JButton();
    S12 = new JButton();
    S13 = new JButton();
    S14 = new JButton();
    S15 = new JButton();
    SCROLLPANE_LIST2 = new JScrollPane();
    S6S1 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    separator1 = new JSeparator();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(880, 640));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_53 ----
          OBJ_53.setText("Etablissement");
          OBJ_53.setName("OBJ_53");

          //---- WETB ----
          WETB.setText("@WETB@");
          WETB.setOpaque(false);
          WETB.setName("WETB");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(680, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- S01 ----
              S01.setText("");
              S01.setToolTipText("Ajouter cette \u00e9tiquette");
              S01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S01.setName("S01");
              S01.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S01ActionPerformed(e);
                }
              });
              panel1.add(S01);
              S01.setBounds(10, 10, 30, 30);

              //---- S02 ----
              S02.setText("");
              S02.setToolTipText("Ajouter cette \u00e9tiquette");
              S02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S02.setName("S02");
              S02.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S02ActionPerformed(e);
                }
              });
              panel1.add(S02);
              S02.setBounds(10, 40, 30, 30);

              //---- S03 ----
              S03.setText("");
              S03.setToolTipText("Ajouter cette \u00e9tiquette");
              S03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S03.setName("S03");
              S03.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S03ActionPerformed(e);
                }
              });
              panel1.add(S03);
              S03.setBounds(10, 70, 30, 30);

              //---- S04 ----
              S04.setText("");
              S04.setToolTipText("Ajouter cette \u00e9tiquette");
              S04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S04.setName("S04");
              S04.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S04ActionPerformed(e);
                }
              });
              panel1.add(S04);
              S04.setBounds(10, 100, 30, 30);

              //---- S05 ----
              S05.setText("");
              S05.setToolTipText("Ajouter cette \u00e9tiquette");
              S05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S05.setName("S05");
              S05.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S05ActionPerformed(e);
                }
              });
              panel1.add(S05);
              S05.setBounds(10, 130, 30, 30);

              //---- S06 ----
              S06.setText("");
              S06.setToolTipText("Ajouter cette \u00e9tiquette");
              S06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S06.setName("S06");
              S06.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S06ActionPerformed(e);
                }
              });
              panel1.add(S06);
              S06.setBounds(10, 160, 30, 30);

              //---- S07 ----
              S07.setText("");
              S07.setToolTipText("Ajouter cette \u00e9tiquette");
              S07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S07.setName("S07");
              S07.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S07ActionPerformed(e);
                }
              });
              panel1.add(S07);
              S07.setBounds(10, 190, 30, 30);

              //---- S08 ----
              S08.setText("");
              S08.setToolTipText("Ajouter cette \u00e9tiquette");
              S08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S08.setName("S08");
              S08.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S08ActionPerformed(e);
                }
              });
              panel1.add(S08);
              S08.setBounds(10, 220, 30, 30);

              //---- S09 ----
              S09.setText("");
              S09.setToolTipText("Ajouter cette \u00e9tiquette");
              S09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S09.setName("S09");
              S09.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S09ActionPerformed(e);
                }
              });
              panel1.add(S09);
              S09.setBounds(10, 250, 30, 30);

              //---- S10 ----
              S10.setText("");
              S10.setToolTipText("Ajouter cette \u00e9tiquette");
              S10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S10.setName("S10");
              S10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S10ActionPerformed(e);
                }
              });
              panel1.add(S10);
              S10.setBounds(10, 280, 30, 30);

              //---- S11 ----
              S11.setText("");
              S11.setToolTipText("Ajouter cette \u00e9tiquette");
              S11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S11.setName("S11");
              S11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S11ActionPerformed(e);
                }
              });
              panel1.add(S11);
              S11.setBounds(10, 310, 30, 30);

              //---- S12 ----
              S12.setText("");
              S12.setToolTipText("Ajouter cette \u00e9tiquette");
              S12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S12.setName("S12");
              S12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S12ActionPerformed(e);
                }
              });
              panel1.add(S12);
              S12.setBounds(10, 340, 30, 30);

              //---- S13 ----
              S13.setText("");
              S13.setToolTipText("Ajouter cette \u00e9tiquette");
              S13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S13.setName("S13");
              S13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S13ActionPerformed(e);
                }
              });
              panel1.add(S13);
              S13.setBounds(10, 370, 30, 30);

              //---- S14 ----
              S14.setText("");
              S14.setToolTipText("Ajouter cette \u00e9tiquette");
              S14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S14.setName("S14");
              S14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S14ActionPerformed(e);
                }
              });
              panel1.add(S14);
              S14.setBounds(10, 400, 30, 30);

              //---- S15 ----
              S15.setText("");
              S15.setToolTipText("Ajouter cette \u00e9tiquette");
              S15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              S15.setName("S15");
              S15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  S15ActionPerformed(e);
                }
              });
              panel1.add(S15);
              S15.setBounds(10, 430, 30, 30);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel2.add(panel1);
            panel1.setBounds(20, 30, panel1.getPreferredSize().width, 471);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- S6S1 ----
              S6S1.setRowHeight(30);
              S6S1.setComponentPopupMenu(BTD);
              S6S1.setName("S6S1");
              SCROLLPANE_LIST2.setViewportView(S6S1);
            }
            panel2.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(65, 15, 545, 480);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(615, 15, 25, 225);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(615, 270, 25, 225);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 654, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choisir");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_23 ----
      OBJ_23.setText("Aper\u00e7u");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- separator1 ----
      separator1.setName("separator1");
      BTD.add(separator1);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_53;
  private RiZoneSortie WETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JPanel panel1;
  private JButton S01;
  private JButton S02;
  private JButton S03;
  private JButton S04;
  private JButton S05;
  private JButton S06;
  private JButton S07;
  private JButton S08;
  private JButton S09;
  private JButton S10;
  private JButton S11;
  private JButton S12;
  private JButton S13;
  private JButton S14;
  private JButton S15;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable S6S1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_23;
  private JSeparator separator1;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
