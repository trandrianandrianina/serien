
package ri.serien.libecranrpg.vgvx.VGVXLDFL;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author StÃ©phane VÃ©nÃ©ri
 */
public class VGVXLDFL_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  /*  private String[][] _WTP01_Data={{"R01","Z01","N01"},{"R02","Z02","N02" },{"R03","Z03","N03"},{"R04","Z04","N04" }, {"R05","Z05","N05"},{"R06","Z06","N06" }, {"R07","Z07","N07"},{"R08","Z08","N08" }, {"R09","Z09","N09"},{"R10","Z10","N10"},{"R11","Z11","N11"},
        {"R12","Z12","N12" },{"R13","Z13","N13"},{"R14","Z14","N14" },{"R15","Z15","N15"},{"R16","Z16","N16" },{"R17","Z17","N17"},{"R18","Z18","N18" },{"R19","Z19","N19"},{"R20","Z20","N20" },{"R21","Z21","N21"},{"R22","Z22","N22" },{"R23","Z23","N23"},
        {"R24","Z24","N24" },{"R25","Z25","N25"},{"R26","Z26","N26" },{"R27","Z27","N27"},{"R28","Z28","N28" },{"R29","Z29","N29"},{"R30","Z30","N30" },{"R31","Z31","N31"},{"R32","Z32","N32" },{"R33","Z33","N33"},{"R34","Z34","N34" },{"R35","Z35","N35"},
        {"R36","Z36","N36" },{"R37","Z37","N37"},{"R38","Z38","N38" },{"R39","Z39","N39"},{"R40","Z40","N40" },{"R41","Z41","N41"},{"R42","Z42","N42" },{"R43","Z43","N43"},{"R44","Z44","N44" },{"R45","Z45","N45"},{"R46","Z46","N46" },{"R47","Z47","N47"},
        {"R48","Z48","N48" },{"R49","Z49","N49"},{"R50","Z50","N50" },{"R51","Z51","N51"},{"R52","Z52","N52"},{"R53","Z53","N53"},{"R54","Z54","N54"},{"R55","Z55","N55"},{"R56","Z56","N56"},{"R57","Z57","N57"},{"R58","Z58","N58"},{"R59","Z59","N59"},
        {"R60","Z60","N60"},{"R61","Z61","N61"},{"R62","Z62","N62"},{"R63","Z63","N63"},{"R64","Z64","N64"},{"R65","Z65","N65"},{"R66","Z66","N66"},{"R67","Z67","N67"},{"R68","Z68","N68"},{"R69","Z69","N69"},{"R70","Z70","N70"},{"R71","Z71","N71"},
        {"R72","Z72","N72"},{"R73","Z73","N73"},{"R74","Z74","N74"},{"R75","Z75","N75"},{"R76","Z76","N76"},{"R77","Z77","N77"},{"R78","Z78","N78"},{"R79","Z79","N79"},{"R80","Z80","N80"},{"R81","Z81","N81"},{"R82","Z82","N82"},{"R83","Z83","N83"},
        {"R84","Z84","N84"},{"R85","Z85","N85"},{"R86","Z86","N86"},{"R87","Z87","N87"},{"R88","Z88","N88"},{"R89","Z89","N89"},{"R90","Z90","N90"},{"R91","Z91","N91"},{"R92","Z92","N92"},{"R93","Z93","N93"},{"R94","Z94","N94"},{"R95","Z95","N95"},
        {"R96","Z96","N96"},{"R97","Z97","N97"},{"R98","Z98","N98"},{"R99","Z99","N99"},{"R99","Z99","N99"},};
        */
  private JCheckBox[] casesCol = null;
  private JTextField[] tailleCol = null;
  private XRiTextField[] zonesCol = null;
  private XRiTextField[] zonesSaisie = null;
  private XRiTextField[] zonesTaille = null;
  private String[] DLDN01_Value = { "", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", };
  
  public VGVXLDFL_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    initDiverses();
    DLDN01.setValeurs(DLDN01_Value, null);
    DLDN02.setValeurs(DLDN01_Value, null);
    DLDN03.setValeurs(DLDN01_Value, null);
    DLDN04.setValeurs(DLDN01_Value, null);
    DLDN05.setValeurs(DLDN01_Value, null);
    DLDN06.setValeurs(DLDN01_Value, null);
    DLDN07.setValeurs(DLDN01_Value, null);
    DLDN08.setValeurs(DLDN01_Value, null);
    DLDN09.setValeurs(DLDN01_Value, null);
    DLDN10.setValeurs(DLDN01_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    gererLesErreurs("19");
    /*
      //mise Ã  blanc des zones avant init + visibilitÃ©
      for (int i = 0; i < casesCol.length; i++)
      {
        //casesCol[i].setSelected(false);
        zonesCol[i].setText("");
        tailleCol[i].setEnabled(false);
        zonesCol[i].setEnabled(false);
      //  casesCol[i].setVisible(!(casesCol[i].getText().trim().equalsIgnoreCase("")));
        tailleCol[i].setVisible(!(casesCol[i].getText().trim().equalsIgnoreCase("")));
        zonesCol[i].setVisible(!(casesCol[i].getText().trim().equalsIgnoreCase("")));
        tailleCol[i].setText(lexique.HostFieldGetData(tailleCol[i].getName()));
      }
      
      //initialisation des zones de choix colonne et cases Ã  cocher
      for (int i = 0; i < col.length; i++)
      {
        if (!lexique.HostFieldGetData(col[i]).trim().equalsIgnoreCase("")){
          //casesCol[Integer.parseInt(lexique.HostFieldGetData(col[i]))-1].setSelected(true);
          zonesCol[Integer.parseInt(lexique.HostFieldGetData(col[i]))-1].setText(""+(i+1));
          tailleCol[Integer.parseInt(lexique.HostFieldGetData(col[i]))-1].setEnabled(true);
          zonesCol[Integer.parseInt(lexique.HostFieldGetData(col[i]))-1].setEnabled(true);
        }
      }
      int total = 0;
      for (int i = 0; i < zonesTaille.length; i++)
      {
        for (int j = 0; j < tailleCol.length; j++)
        {
          if(!zonesCol[j].getText().trim().equalsIgnoreCase("")) {
            if (Integer.parseInt(zonesCol[j].getText()) == (i+1)){
              if (i>0) {
                total = (Integer.parseInt(zonesTaille[i].getText()) - Integer.parseInt(zonesTaille[(i-1)].getText()));
                if (total != Integer.parseInt(tailleCol[j].getText()))
                  casesCol[j].setText(casesCol[j].getText() + " (dÃ©fini Ã  " + total +")");
              }
              else {
                total = (Integer.parseInt(zonesTaille[0].getText()));
                if (total != Integer.parseInt(tailleCol[j].getText()))
                  casesCol[j].setText(casesCol[j].getText() + " (dÃ©fini Ã  " + total +")");
              }
            }
          }
        }
      }
      
      */
    
    dest.setText(lexique.HostFieldGetData("TITZ"));
    
    /*  JButton[] sources = {Z01, Z02, Z03, Z04, Z05, Z06, Z07, Z08, Z09, Z10, Z11, Z12, Z13, Z14, Z15, Z16, Z17, Z18, Z19, Z20, Z21, Z22,
     *  Z23, Z24, Z25, Z26, Z27, Z28, Z29, Z30, Z31, Z32, Z33, Z34, Z35, Z36, Z37, Z38, Z39, Z40, Z41, Z42
          , Z43, Z44, Z45, Z46, Z47, Z48, Z49, Z50, Z51, Z52, Z53, Z54, Z55, Z56, Z57, Z58, Z59, Z60, Z61, Z62, Z63, Z64, Z65, Z66, Z67,
           Z68, Z69, Z70, Z71, Z72, Z73, Z74, Z75, Z76, Z77, Z78, Z79, Z80, Z81, Z82, Z83, Z84, Z85, Z86
          , Z87, Z88, Z89, Z90, Z91, Z92, Z93, Z94, Z95, Z96, Z97, Z98, Z99};
      
      
      
        //-------------------------------------------------------------------
      for (int i = 0; i < sources.length; i++)
      {
        if(lexique.isPresent(sources[i].getName())){
          //On adapte les Ã©tiquettes Ã  la taille de la zone
          SetTaille(sources[i]);
          //On crÃ©e le nouvel objet pour activer le drag'n drop
          sources[i].setTransferHandler(new TransferHandler("text"));
        }
      }
      //On crÃ©e le nouvel objet pour activer le drag'n drop
     // Z01.setTransferHandler(new TransferHandler("text"));
      
      for (int i = 0; i < sources.length; i++)
      {
          //On spÃ©cifie au composant qu'il doit envoyer ses donnÃ©es via son objet TransferHandler
        sources[i].addMouseListener(new MouseAdapter(){
            //On utilise cet Ã©vÃ©nement pour que les actions soient visibles dÃ¨s le clic de sourisâ€¦
            //Nous aurions pu utiliser mouseReleased, mais, niveau IHM, nous n'aurions rien vu
            public void mousePressed(MouseEvent e){
              //On rÃ©cupÃ¨re le JComponent
              JComponent lab = (JComponent)e.getSource();
              //Du composant, on rÃ©cupÃ¨re l'objet de transfert : le nÃ´tre
              TransferHandler handle = lab.getTransferHandler();
              //On lui ordonne d'amorcer la procÃ©dure de drag'n drop
              handle.exportAsDrag(lab, e, TransferHandler.COPY);
            }
          });
      }
      //-------------------------------------------------------------------
        
    
    
      //On active le comportement par dÃ©faut de ce composant
      dest01.setDragEnabled(true);
      dest02.setDragEnabled(true);
      dest03.setDragEnabled(true);
      dest04.setDragEnabled(true);
      dest05.setDragEnabled(true);
      dest06.setDragEnabled(true);
      dest07.setDragEnabled(true);
      dest08.setDragEnabled(true);
      dest09.setDragEnabled(true);
      dest10.setDragEnabled(true);
    */
    
    bouton_vide.setIcon(lexique.chargerImage("images/button_cancel_p.png", true));
    
    setTitle("Paramétrage des colonnes visibles");
    
  }
  
  @Override
  public void getData() {
    super.getData();
    // TODO RÃ©cup spÃ©cifiques
    
    String[] retour = dest.getText().split("\\x7C");
    
    lexique.HostFieldPutData("DLDN01", 0, "00");
    lexique.HostFieldPutData("DLDN02", 0, "00");
    lexique.HostFieldPutData("DLDN03", 0, "00");
    lexique.HostFieldPutData("DLDN04", 0, "00");
    lexique.HostFieldPutData("DLDN05", 0, "00");
    lexique.HostFieldPutData("DLDN06", 0, "00");
    lexique.HostFieldPutData("DLDN07", 0, "00");
    lexique.HostFieldPutData("DLDN08", 0, "00");
    lexique.HostFieldPutData("DLDN09", 0, "00");
    lexique.HostFieldPutData("DLDN10", 0, "00");
    
    for (int i = 0; i < retour.length; i++) {
      
      for (int j = 0; j < 99; j++) {
        if (retour[i].contains("-")) {
          if (retour[i].substring(0, retour[i].indexOf("-")).trim()
              .equalsIgnoreCase(lexique.HostFieldGetData("Z" + ((j + 1) < 10 ? "0" + (j + 1) : (j + 1))).trim())) {
            lexique.HostFieldPutData("DLDN" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)), 0,
                lexique.HostFieldGetData("R" + ((j + 1) < 10 ? "0" + (j + 1) : (j + 1))));
          }
        }
        else {
          if (retour[i].trim().equalsIgnoreCase(lexique.HostFieldGetData("Z" + ((j + 1) < 10 ? "0" + (j + 1) : (j + 1))).trim())) {
            String idx = lexique.HostFieldGetData("R" + ((j + 1) < 10 ? "0" + (j + 1) : (j + 1)));
            lexique.HostFieldPutData("DLDN" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)), 1, idx);
          }
        }
      }
    }
    
  }
  
  private void SetTaille(JButton lab) {
    String nomZone = "N" + lab.getName().substring(1);
    int taille = 0;
    if (lexique.isPresent(nomZone) && lexique.HostFieldGetData(nomZone) != null && !lexique.HostFieldGetData(nomZone).isEmpty()) {
      taille = Integer.parseInt(lexique.HostFieldGetData(nomZone));
      if (taille > lab.getText().length() + 1) {
        while (taille > lab.getText().length()) {
          lab.setText(lab.getText() + '-');
        }
      }
      lab.setText(lab.getText() + '|');
      lab.setSize((((taille - 1) * 10) + 10), lab.getHeight());
      lab.setBackground(Color.red);
    }
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  /*
  private void Z01MouseClicked(MouseEvent e) {
    int numObjet = (Integer.parseInt(e.getComponent().getName().substring(1))-1);
    
    if (!casesCol[numObjet].isSelected()) {
      for (int i = 0; i < col.length; i++)
      {
        if (lexique.HostFieldGetData(col[i]).equalsIgnoreCase(""+(numObjet+1)))
          zonesSaisie[i].setText("");
      }
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      tailleCol[numObjet].setEnabled(true);
      zonesCol[numObjet].setEnabled(true);
      zonesSaisie[derniereColVide()].setText(""+(numObjet+1));
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private int derniereColVide(){
    for (int i = 0; i < col.length; i++)
    {
      if (lexique.HostFieldGetData(col[i]).trim().equalsIgnoreCase(""))
        return (i);
    }
    return 0;
  }
  
  */
  
  private void bouton_videActionPerformed(ActionEvent e) {
    dest.setText("");
    lexique.HostFieldPutData("DLDN01", 0, "00");
    lexique.HostFieldPutData("DLDN02", 0, "00");
    lexique.HostFieldPutData("DLDN03", 0, "00");
    lexique.HostFieldPutData("DLDN04", 0, "00");
    lexique.HostFieldPutData("DLDN05", 0, "00");
    lexique.HostFieldPutData("DLDN06", 0, "00");
    lexique.HostFieldPutData("DLDN07", 0, "00");
    lexique.HostFieldPutData("DLDN08", 0, "00");
    lexique.HostFieldPutData("DLDN09", 0, "00");
    lexique.HostFieldPutData("DLDN10", 0, "00");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel7 = new JPanel();
    dest = new JTextField();
    bouton_vide = new JButton();
    DLDN01 = new XRiComboBox();
    DLDN02 = new XRiComboBox();
    DLDN03 = new XRiComboBox();
    DLDN04 = new XRiComboBox();
    DLDN05 = new XRiComboBox();
    DLDN06 = new XRiComboBox();
    DLDN07 = new XRiComboBox();
    DLDN08 = new XRiComboBox();
    DLDN09 = new XRiComboBox();
    DLDN10 = new XRiComboBox();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1170, 690));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel7 ========
        {
          panel7.setBorder(new TitledBorder("Ent\u00eate"));
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);

          //---- dest ----
          dest.setDragEnabled(true);
          dest.setFont(new Font("Courier New", Font.PLAIN, 10));
          dest.setName("dest");
          panel7.add(dest);
          dest.setBounds(10, 25, 935, 32);

          //---- bouton_vide ----
          bouton_vide.setBorder(new LineBorder(UIManager.getColor("info")));
          bouton_vide.setToolTipText("Effacer cette ent\u00eate");
          bouton_vide.setBackground(UIManager.getColor("menu"));
          bouton_vide.setName("bouton_vide");
          bouton_vide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_videActionPerformed(e);
            }
          });
          panel7.add(bouton_vide);
          bouton_vide.setBounds(950, 25, 32, 32);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel7.getComponentCount(); i++) {
              Rectangle bounds = panel7.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel7.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel7.setMinimumSize(preferredSize);
            panel7.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel7);
        panel7.setBounds(5, 10, 990, 75);

        //---- DLDN01 ----
        DLDN01.setModel(new DefaultComboBoxModel(new String[] {
          "Aucune",
          "@Z01@",
          "@Z02@",
          "@Z03@",
          "@Z04@",
          "@Z05@",
          "@Z06@",
          "@Z07@",
          "@Z08@",
          "@Z09@",
          "@Z10@"
        }));
        DLDN01.setName("DLDN01");
        p_contenu.add(DLDN01);
        DLDN01.setBounds(15, 110, 215, DLDN01.getPreferredSize().height);

        //---- DLDN02 ----
        DLDN02.setModel(new DefaultComboBoxModel(new String[] {
          "Aucune",
          "@Z01@",
          "@Z02@",
          "@Z03@",
          "@Z04@",
          "@Z05@",
          "@Z06@",
          "@Z07@",
          "@Z08@",
          "@Z09@",
          "@Z10@"
        }));
        DLDN02.setName("DLDN02");
        p_contenu.add(DLDN02);
        DLDN02.setBounds(15, 150, 215, DLDN02.getPreferredSize().height);

        //---- DLDN03 ----
        DLDN03.setModel(new DefaultComboBoxModel(new String[] {
          "Aucune",
          "@Z01@",
          "@Z02@",
          "@Z03@",
          "@Z04@",
          "@Z05@",
          "@Z06@",
          "@Z07@",
          "@Z08@",
          "@Z09@",
          "@Z10@"
        }));
        DLDN03.setName("DLDN03");
        p_contenu.add(DLDN03);
        DLDN03.setBounds(15, 185, 215, DLDN03.getPreferredSize().height);

        //---- DLDN04 ----
        DLDN04.setModel(new DefaultComboBoxModel(new String[] {
          "Aucune",
          "@Z01@",
          "@Z02@",
          "@Z03@",
          "@Z04@",
          "@Z05@",
          "@Z06@",
          "@Z07@",
          "@Z08@",
          "@Z09@",
          "@Z10@"
        }));
        DLDN04.setName("DLDN04");
        p_contenu.add(DLDN04);
        DLDN04.setBounds(15, 220, 215, DLDN04.getPreferredSize().height);

        //---- DLDN05 ----
        DLDN05.setModel(new DefaultComboBoxModel(new String[] {
          "Aucune",
          "@Z01@",
          "@Z02@",
          "@Z03@",
          "@Z04@",
          "@Z05@",
          "@Z06@",
          "@Z07@",
          "@Z08@",
          "@Z09@",
          "@Z10@"
        }));
        DLDN05.setName("DLDN05");
        p_contenu.add(DLDN05);
        DLDN05.setBounds(15, 255, 215, DLDN05.getPreferredSize().height);

        //---- DLDN06 ----
        DLDN06.setModel(new DefaultComboBoxModel(new String[] {
          "Aucune",
          "@Z01@",
          "@Z02@",
          "@Z03@",
          "@Z04@",
          "@Z05@",
          "@Z06@",
          "@Z07@",
          "@Z08@",
          "@Z09@",
          "@Z10@"
        }));
        DLDN06.setName("DLDN06");
        p_contenu.add(DLDN06);
        DLDN06.setBounds(15, 295, 215, DLDN06.getPreferredSize().height);

        //---- DLDN07 ----
        DLDN07.setModel(new DefaultComboBoxModel(new String[] {
          "Aucune",
          "@Z01@",
          "@Z02@",
          "@Z03@",
          "@Z04@",
          "@Z05@",
          "@Z06@",
          "@Z07@",
          "@Z08@",
          "@Z09@",
          "@Z10@"
        }));
        DLDN07.setName("DLDN07");
        p_contenu.add(DLDN07);
        DLDN07.setBounds(15, 330, 215, DLDN07.getPreferredSize().height);

        //---- DLDN08 ----
        DLDN08.setModel(new DefaultComboBoxModel(new String[] {
          "Aucune",
          "@Z01@",
          "@Z02@",
          "@Z03@",
          "@Z04@",
          "@Z05@",
          "@Z06@",
          "@Z07@",
          "@Z08@",
          "@Z09@",
          "@Z10@"
        }));
        DLDN08.setName("DLDN08");
        p_contenu.add(DLDN08);
        DLDN08.setBounds(15, 365, 215, DLDN08.getPreferredSize().height);

        //---- DLDN09 ----
        DLDN09.setModel(new DefaultComboBoxModel(new String[] {
          "Aucune",
          "@Z01@",
          "@Z02@",
          "@Z03@",
          "@Z04@",
          "@Z05@",
          "@Z06@",
          "@Z07@",
          "@Z08@",
          "@Z09@",
          "@Z10@"
        }));
        DLDN09.setName("DLDN09");
        p_contenu.add(DLDN09);
        DLDN09.setBounds(15, 400, 215, DLDN09.getPreferredSize().height);

        //---- DLDN10 ----
        DLDN10.setModel(new DefaultComboBoxModel(new String[] {
          "Aucune",
          "@Z01@",
          "@Z02@",
          "@Z03@",
          "@Z04@",
          "@Z05@",
          "@Z06@",
          "@Z07@",
          "@Z08@",
          "@Z09@",
          "@Z10@"
        }));
        DLDN10.setName("DLDN10");
        p_contenu.add(DLDN10);
        DLDN10.setBounds(15, 435, 215, DLDN10.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel7;
  private JTextField dest;
  private JButton bouton_vide;
  private XRiComboBox DLDN01;
  private XRiComboBox DLDN02;
  private XRiComboBox DLDN03;
  private XRiComboBox DLDN04;
  private XRiComboBox DLDN05;
  private XRiComboBox DLDN06;
  private XRiComboBox DLDN07;
  private XRiComboBox DLDN08;
  private XRiComboBox DLDN09;
  private XRiComboBox DLDN10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
