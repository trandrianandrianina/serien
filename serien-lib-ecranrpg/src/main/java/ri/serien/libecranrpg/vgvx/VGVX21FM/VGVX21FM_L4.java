
package ri.serien.libecranrpg.vgvx.VGVX21FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX21FM_L4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX21FM_L4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB05@")).trim()));
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    // Titre
    setTitle("Lots");
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    CNDX01 = new XRiTextField();
    CNDX02 = new XRiTextField();
    CNDX03 = new XRiTextField();
    CNDX04 = new XRiTextField();
    CNDX05 = new XRiTextField();
    CNDX06 = new XRiTextField();
    CNDX07 = new XRiTextField();
    CNDX08 = new XRiTextField();
    CNDX09 = new XRiTextField();
    CNDX10 = new XRiTextField();
    CNDX11 = new XRiTextField();
    CNDX12 = new XRiTextField();
    CNDX13 = new XRiTextField();
    CNDX14 = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 160));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("@WLIB05@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- CNDX01 ----
          CNDX01.setName("CNDX01");
          panel1.add(CNDX01);
          CNDX01.setBounds(25, 45, 64, CNDX01.getPreferredSize().height);

          //---- CNDX02 ----
          CNDX02.setName("CNDX02");
          panel1.add(CNDX02);
          CNDX02.setBounds(108, 45, 64, CNDX02.getPreferredSize().height);

          //---- CNDX03 ----
          CNDX03.setName("CNDX03");
          panel1.add(CNDX03);
          CNDX03.setBounds(191, 45, 64, CNDX03.getPreferredSize().height);

          //---- CNDX04 ----
          CNDX04.setName("CNDX04");
          panel1.add(CNDX04);
          CNDX04.setBounds(274, 45, 64, CNDX04.getPreferredSize().height);

          //---- CNDX05 ----
          CNDX05.setName("CNDX05");
          panel1.add(CNDX05);
          CNDX05.setBounds(357, 45, 64, CNDX05.getPreferredSize().height);

          //---- CNDX06 ----
          CNDX06.setName("CNDX06");
          panel1.add(CNDX06);
          CNDX06.setBounds(440, 45, 64, CNDX06.getPreferredSize().height);

          //---- CNDX07 ----
          CNDX07.setName("CNDX07");
          panel1.add(CNDX07);
          CNDX07.setBounds(523, 45, 64, CNDX07.getPreferredSize().height);

          //---- CNDX08 ----
          CNDX08.setName("CNDX08");
          panel1.add(CNDX08);
          CNDX08.setBounds(25, 85, 64, CNDX08.getPreferredSize().height);

          //---- CNDX09 ----
          CNDX09.setName("CNDX09");
          panel1.add(CNDX09);
          CNDX09.setBounds(108, 85, 64, CNDX09.getPreferredSize().height);

          //---- CNDX10 ----
          CNDX10.setName("CNDX10");
          panel1.add(CNDX10);
          CNDX10.setBounds(191, 85, 64, CNDX10.getPreferredSize().height);

          //---- CNDX11 ----
          CNDX11.setName("CNDX11");
          panel1.add(CNDX11);
          CNDX11.setBounds(274, 85, 64, CNDX11.getPreferredSize().height);

          //---- CNDX12 ----
          CNDX12.setName("CNDX12");
          panel1.add(CNDX12);
          CNDX12.setBounds(357, 85, 64, CNDX12.getPreferredSize().height);

          //---- CNDX13 ----
          CNDX13.setName("CNDX13");
          panel1.add(CNDX13);
          CNDX13.setBounds(440, 85, 64, CNDX13.getPreferredSize().height);

          //---- CNDX14 ----
          CNDX14.setName("CNDX14");
          panel1.add(CNDX14);
          CNDX14.setBounds(523, 85, 64, CNDX14.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 5, 615, 145);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField CNDX01;
  private XRiTextField CNDX02;
  private XRiTextField CNDX03;
  private XRiTextField CNDX04;
  private XRiTextField CNDX05;
  private XRiTextField CNDX06;
  private XRiTextField CNDX07;
  private XRiTextField CNDX08;
  private XRiTextField CNDX09;
  private XRiTextField CNDX10;
  private XRiTextField CNDX11;
  private XRiTextField CNDX12;
  private XRiTextField CNDX13;
  private XRiTextField CNDX14;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
