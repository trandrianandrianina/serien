
package ri.serien.libecranrpg.vgvx.VGVX40FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX40FM_C2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX40FM_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    xTitledPanel1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("Client  @CLCLI@ @CLLIV@")).trim());
    CLNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    CLCPL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCPL@")).trim());
    CLRUE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLRUE@")).trim());
    CLLOC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLLOC@")).trim());
    CLPAY.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLPAY@")).trim());
    CLVILR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLVILR@")).trim());
    CLTEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLTEL@")).trim());
    CLFAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLFAX@")).trim());
    CLTLX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLTLX@")).trim());
    CLCDP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCDP@")).trim());
    CLCOP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCOP@")).trim());
    E1DEVX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1DEVX@")).trim());
    L1MHT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1MHT@")).trim());
    E1FACX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1FACX@")).trim());
    E1NFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NFA@")).trim());
    E1DATX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1DATX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    ELSBV.setEnabled(lexique.isPresent("ELSBV"));
    ELLBV.setEnabled(lexique.isPresent("ELLBV"));
    ELLDV.setEnabled(lexique.isPresent("ELLDV"));
    CLCOP.setVisible(lexique.isPresent("CLCOP"));
    E1NFA.setEnabled(lexique.isPresent("E1NFA"));
    ELNBV.setEnabled(lexique.isPresent("ELNBV"));
    ELNDV.setEnabled(lexique.isPresent("ELNDV"));
    CLCDP.setVisible(lexique.isPresent("CLCDP"));
    E1FACX.setEnabled(lexique.isPresent("E1FACX"));
    E1DATX.setEnabled(lexique.isPresent("E1DATX"));
    E1DEVX.setEnabled(lexique.isPresent("E1DEVX"));
    L1MHT.setEnabled(lexique.isPresent("L1MHT"));
    CLTLX.setVisible(lexique.isPresent("CLTLX"));
    CLFAX.setVisible(lexique.isPresent("CLFAX"));
    CLTEL.setVisible(lexique.isPresent("CLTEL"));
    CLVILR.setVisible(lexique.isPresent("CLVILR"));
    CLPAY.setVisible(lexique.isPresent("CLPAY"));
    CLLOC.setVisible(lexique.isPresent("CLLOC"));
    CLRUE.setVisible(lexique.isPresent("CLRUE"));
    CLCPL.setVisible(lexique.isPresent("CLCPL"));
    CLNOM.setVisible(lexique.isPresent("CLNOM"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Vente"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    CLNOM = new RiZoneSortie();
    CLCPL = new RiZoneSortie();
    CLRUE = new RiZoneSortie();
    CLLOC = new RiZoneSortie();
    CLPAY = new RiZoneSortie();
    CLVILR = new RiZoneSortie();
    CLTEL = new RiZoneSortie();
    CLFAX = new RiZoneSortie();
    CLTLX = new RiZoneSortie();
    OBJ_33 = new JLabel();
    CLCDP = new RiZoneSortie();
    CLCOP = new RiZoneSortie();
    OBJ_40 = new JLabel();
    OBJ_36 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_45 = new JLabel();
    OBJ_47 = new JLabel();
    E1DEVX = new RiZoneSortie();
    ELNDV = new XRiTextField();
    OBJ_49 = new JLabel();
    ELLDV = new XRiTextField();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_69 = new JLabel();
    L1MHT = new RiZoneSortie();
    E1FACX = new RiZoneSortie();
    OBJ_56 = new JLabel();
    E1NFA = new RiZoneSortie();
    OBJ_67 = new JLabel();
    xTitledPanel5 = new JXTitledPanel();
    OBJ_70 = new JLabel();
    OBJ_68 = new JLabel();
    E1DATX = new RiZoneSortie();
    ELNBV = new XRiTextField();
    OBJ_66 = new JLabel();
    ELLBV = new XRiTextField();
    ELSBV = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(780, 540));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Image secondaire");
            riSousMenu_bt6.setToolTipText("Image secondaire");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Client  @CLCLI@ @CLLIV@");
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //---- CLNOM ----
          CLNOM.setText("@CLNOM@");
          CLNOM.setName("CLNOM");
          xTitledPanel1ContentContainer.add(CLNOM);
          CLNOM.setBounds(15, 20, 310, CLNOM.getPreferredSize().height);

          //---- CLCPL ----
          CLCPL.setText("@CLCPL@");
          CLCPL.setName("CLCPL");
          xTitledPanel1ContentContainer.add(CLCPL);
          CLCPL.setBounds(15, 50, 310, CLCPL.getPreferredSize().height);

          //---- CLRUE ----
          CLRUE.setText("@CLRUE@");
          CLRUE.setName("CLRUE");
          xTitledPanel1ContentContainer.add(CLRUE);
          CLRUE.setBounds(15, 80, 310, CLRUE.getPreferredSize().height);

          //---- CLLOC ----
          CLLOC.setText("@CLLOC@");
          CLLOC.setName("CLLOC");
          xTitledPanel1ContentContainer.add(CLLOC);
          CLLOC.setBounds(15, 110, 310, CLLOC.getPreferredSize().height);

          //---- CLPAY ----
          CLPAY.setText("@CLPAY@");
          CLPAY.setName("CLPAY");
          xTitledPanel1ContentContainer.add(CLPAY);
          CLPAY.setBounds(15, 170, 264, CLPAY.getPreferredSize().height);

          //---- CLVILR ----
          CLVILR.setText("@CLVILR@");
          CLVILR.setName("CLVILR");
          xTitledPanel1ContentContainer.add(CLVILR);
          CLVILR.setBounds(71, 140, 254, CLVILR.getPreferredSize().height);

          //---- CLTEL ----
          CLTEL.setText("@CLTEL@");
          CLTEL.setName("CLTEL");
          xTitledPanel1ContentContainer.add(CLTEL);
          CLTEL.setBounds(440, 20, 105, CLTEL.getPreferredSize().height);

          //---- CLFAX ----
          CLFAX.setText("@CLFAX@");
          CLFAX.setName("CLFAX");
          xTitledPanel1ContentContainer.add(CLFAX);
          CLFAX.setBounds(440, 50, 105, CLFAX.getPreferredSize().height);

          //---- CLTLX ----
          CLTLX.setText("@CLTLX@");
          CLTLX.setName("CLTLX");
          xTitledPanel1ContentContainer.add(CLTLX);
          CLTLX.setBounds(440, 80, 105, CLTLX.getPreferredSize().height);

          //---- OBJ_33 ----
          OBJ_33.setText("T\u00e9l\u00e9phone");
          OBJ_33.setName("OBJ_33");
          xTitledPanel1ContentContainer.add(OBJ_33);
          OBJ_33.setBounds(360, 22, 75, 20);

          //---- CLCDP ----
          CLCDP.setText("@CLCDP@");
          CLCDP.setName("CLCDP");
          xTitledPanel1ContentContainer.add(CLCDP);
          CLCDP.setBounds(15, 140, 52, CLCDP.getPreferredSize().height);

          //---- CLCOP ----
          CLCOP.setText("@CLCOP@");
          CLCOP.setName("CLCOP");
          xTitledPanel1ContentContainer.add(CLCOP);
          CLCOP.setBounds(285, 170, 40, CLCOP.getPreferredSize().height);

          //---- OBJ_40 ----
          OBJ_40.setText("T\u00e9lex");
          OBJ_40.setName("OBJ_40");
          xTitledPanel1ContentContainer.add(OBJ_40);
          OBJ_40.setBounds(360, 82, 75, 20);

          //---- OBJ_36 ----
          OBJ_36.setText("Fax");
          OBJ_36.setName("OBJ_36");
          xTitledPanel1ContentContainer.add(OBJ_36);
          OBJ_36.setBounds(360, 52, 75, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(15, 10, 580, 245);

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setTitle("Devis");
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
          xTitledPanel2ContentContainer.setLayout(null);

          //---- OBJ_45 ----
          OBJ_45.setText("Num\u00e9ro de devis");
          OBJ_45.setName("OBJ_45");
          xTitledPanel2ContentContainer.add(OBJ_45);
          OBJ_45.setBounds(165, 16, 105, 26);

          //---- OBJ_47 ----
          OBJ_47.setText("Num\u00e9ro de ligne");
          OBJ_47.setName("OBJ_47");
          xTitledPanel2ContentContainer.add(OBJ_47);
          OBJ_47.setBounds(400, 19, 105, 20);

          //---- E1DEVX ----
          E1DEVX.setText("@E1DEVX@");
          E1DEVX.setName("E1DEVX");
          xTitledPanel2ContentContainer.add(E1DEVX);
          E1DEVX.setBounds(65, 17, 70, E1DEVX.getPreferredSize().height);

          //---- ELNDV ----
          ELNDV.setComponentPopupMenu(BTD);
          ELNDV.setName("ELNDV");
          xTitledPanel2ContentContainer.add(ELNDV);
          ELNDV.setBounds(280, 15, 60, ELNDV.getPreferredSize().height);

          //---- OBJ_49 ----
          OBJ_49.setText("Date");
          OBJ_49.setName("OBJ_49");
          xTitledPanel2ContentContainer.add(OBJ_49);
          OBJ_49.setBounds(15, 19, 40, 20);

          //---- ELLDV ----
          ELLDV.setComponentPopupMenu(BTD);
          ELLDV.setName("ELLDV");
          xTitledPanel2ContentContainer.add(ELLDV);
          ELLDV.setBounds(510, 15, 44, ELLDV.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel2ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel2);
        xTitledPanel2.setBounds(15, 260, 580, 85);

        //======== xTitledPanel4 ========
        {
          xTitledPanel4.setTitle("Facturation");
          xTitledPanel4.setName("xTitledPanel4");
          Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
          xTitledPanel4ContentContainer.setLayout(null);

          //---- OBJ_69 ----
          OBJ_69.setText("Num\u00e9ro de facture");
          OBJ_69.setName("OBJ_69");
          xTitledPanel4ContentContainer.add(OBJ_69);
          OBJ_69.setBounds(165, 17, 115, 20);

          //---- L1MHT ----
          L1MHT.setText("@L1MHT@");
          L1MHT.setHorizontalAlignment(SwingConstants.RIGHT);
          L1MHT.setName("L1MHT");
          xTitledPanel4ContentContainer.add(L1MHT);
          L1MHT.setBounds(470, 15, 82, L1MHT.getPreferredSize().height);

          //---- E1FACX ----
          E1FACX.setText("@E1FACX@");
          E1FACX.setName("E1FACX");
          xTitledPanel4ContentContainer.add(E1FACX);
          E1FACX.setBounds(65, 15, 65, E1FACX.getPreferredSize().height);

          //---- OBJ_56 ----
          OBJ_56.setText("Montant");
          OBJ_56.setName("OBJ_56");
          xTitledPanel4ContentContainer.add(OBJ_56);
          OBJ_56.setBounds(400, 17, 70, 20);

          //---- E1NFA ----
          E1NFA.setText("@E1NFA@");
          E1NFA.setName("E1NFA");
          xTitledPanel4ContentContainer.add(E1NFA);
          E1NFA.setBounds(280, 15, 68, E1NFA.getPreferredSize().height);

          //---- OBJ_67 ----
          OBJ_67.setText("Date");
          OBJ_67.setName("OBJ_67");
          xTitledPanel4ContentContainer.add(OBJ_67);
          OBJ_67.setBounds(15, 17, 45, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel4ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel4);
        xTitledPanel4.setBounds(15, 440, 580, 85);

        //======== xTitledPanel5 ========
        {
          xTitledPanel5.setTitle("Exp\u00e9dition");
          xTitledPanel5.setName("xTitledPanel5");
          Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
          xTitledPanel5ContentContainer.setLayout(null);

          //---- OBJ_70 ----
          OBJ_70.setText("Num\u00e9ro de ligne");
          OBJ_70.setName("OBJ_70");
          xTitledPanel5ContentContainer.add(OBJ_70);
          OBJ_70.setBounds(400, 19, 110, 20);

          //---- OBJ_68 ----
          OBJ_68.setText("Num\u00e9ro de bon");
          OBJ_68.setName("OBJ_68");
          xTitledPanel5ContentContainer.add(OBJ_68);
          OBJ_68.setBounds(165, 19, 105, 20);

          //---- E1DATX ----
          E1DATX.setText("@E1DATX@");
          E1DATX.setName("E1DATX");
          xTitledPanel5ContentContainer.add(E1DATX);
          E1DATX.setBounds(65, 17, 70, E1DATX.getPreferredSize().height);

          //---- ELNBV ----
          ELNBV.setComponentPopupMenu(BTD);
          ELNBV.setName("ELNBV");
          xTitledPanel5ContentContainer.add(ELNBV);
          ELNBV.setBounds(280, 15, 60, ELNBV.getPreferredSize().height);

          //---- OBJ_66 ----
          OBJ_66.setText("Date");
          OBJ_66.setName("OBJ_66");
          xTitledPanel5ContentContainer.add(OBJ_66);
          OBJ_66.setBounds(15, 19, 45, 20);

          //---- ELLBV ----
          ELLBV.setComponentPopupMenu(BTD);
          ELLBV.setName("ELLBV");
          xTitledPanel5ContentContainer.add(ELLBV);
          ELLBV.setBounds(510, 15, 44, ELLBV.getPreferredSize().height);

          //---- ELSBV ----
          ELSBV.setComponentPopupMenu(BTD);
          ELSBV.setName("ELSBV");
          xTitledPanel5ContentContainer.add(ELSBV);
          ELSBV.setBounds(345, 15, 20, ELSBV.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel5ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel5ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel5ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel5ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel5ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel5);
        xTitledPanel5.setBounds(15, 350, 580, 85);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie CLNOM;
  private RiZoneSortie CLCPL;
  private RiZoneSortie CLRUE;
  private RiZoneSortie CLLOC;
  private RiZoneSortie CLPAY;
  private RiZoneSortie CLVILR;
  private RiZoneSortie CLTEL;
  private RiZoneSortie CLFAX;
  private RiZoneSortie CLTLX;
  private JLabel OBJ_33;
  private RiZoneSortie CLCDP;
  private RiZoneSortie CLCOP;
  private JLabel OBJ_40;
  private JLabel OBJ_36;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_45;
  private JLabel OBJ_47;
  private RiZoneSortie E1DEVX;
  private XRiTextField ELNDV;
  private JLabel OBJ_49;
  private XRiTextField ELLDV;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_69;
  private RiZoneSortie L1MHT;
  private RiZoneSortie E1FACX;
  private JLabel OBJ_56;
  private RiZoneSortie E1NFA;
  private JLabel OBJ_67;
  private JXTitledPanel xTitledPanel5;
  private JLabel OBJ_70;
  private JLabel OBJ_68;
  private RiZoneSortie E1DATX;
  private XRiTextField ELNBV;
  private JLabel OBJ_66;
  private XRiTextField ELLBV;
  private XRiTextField ELSBV;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
