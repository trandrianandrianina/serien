
package ri.serien.libecranrpg.vgvx.VGVX10FM;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNNombreDecimal;
import ri.serien.libswing.composantrpg.autonome.Calculatrice;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX10FM_A3 extends SNPanelEcranRPG implements ioFrame {
  // Boutons
  private static final String BOUTON_RECHERCHE = "Recherche multi critères";
  private static final String BOUTON_ARTICLES_DESACTIVES = "Articles désactivés";
  private static final String BOUTON_GAMME_HORS_GAMME = "Gamme/Hors gamme";
  private static final String BOUTON_AUTRES_VUES = "Autres vues";
  
  // Variables
  private ArrayList<SNNombreDecimal> listeSaisieQuantite = null;
  private ArrayList<JLabel> listeUnite = null;
  private ArrayList<JLabel> listeLibelle = null;
  private Calculatrice calculatrice = null;
  private int ligneCourante = 0;
  private String[] fonctions = { "", "F16", "F17", "F18", "F5" };
  private String module = "";
  
  public VGVX10FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    gererLeClavier();
    
    // Ajout
    initDiverses();
    
    snNombreDecimal01.setLongueur(8);
    snNombreDecimal02.setLongueur(8);
    snNombreDecimal03.setLongueur(8);
    snNombreDecimal04.setLongueur(8);
    snNombreDecimal05.setLongueur(8);
    snNombreDecimal06.setLongueur(8);
    snNombreDecimal07.setLongueur(8);
    snNombreDecimal08.setLongueur(8);
    snNombreDecimal09.setLongueur(8);
    snNombreDecimal10.setLongueur(8);
    snNombreDecimal11.setLongueur(8);
    snNombreDecimal12.setLongueur(8);
    snNombreDecimal13.setLongueur(8);
    snNombreDecimal14.setLongueur(8);
    snNombreDecimal15.setLongueur(8);
    
    // Initialisation de la liste avec les composants pour les quantités
    listeSaisieQuantite = new ArrayList<SNNombreDecimal>();
    listeSaisieQuantite.add(snNombreDecimal01);
    listeSaisieQuantite.add(snNombreDecimal02);
    listeSaisieQuantite.add(snNombreDecimal03);
    listeSaisieQuantite.add(snNombreDecimal04);
    listeSaisieQuantite.add(snNombreDecimal05);
    listeSaisieQuantite.add(snNombreDecimal06);
    listeSaisieQuantite.add(snNombreDecimal07);
    listeSaisieQuantite.add(snNombreDecimal08);
    listeSaisieQuantite.add(snNombreDecimal09);
    listeSaisieQuantite.add(snNombreDecimal10);
    listeSaisieQuantite.add(snNombreDecimal11);
    listeSaisieQuantite.add(snNombreDecimal12);
    listeSaisieQuantite.add(snNombreDecimal13);
    listeSaisieQuantite.add(snNombreDecimal14);
    listeSaisieQuantite.add(snNombreDecimal15);
    // Initialisation de la liste avec les composants pour les unités
    listeUnite = new ArrayList<JLabel>();
    listeUnite.add(lbUniteLigne1);
    listeUnite.add(lbUniteLigne2);
    listeUnite.add(lbUniteLigne3);
    listeUnite.add(lbUniteLigne4);
    listeUnite.add(lbUniteLigne5);
    listeUnite.add(lbUniteLigne6);
    listeUnite.add(lbUniteLigne7);
    listeUnite.add(lbUniteLigne8);
    listeUnite.add(lbUniteLigne9);
    listeUnite.add(lbUniteLigne10);
    listeUnite.add(lbUniteLigne11);
    listeUnite.add(lbUniteLigne12);
    listeUnite.add(lbUniteLigne13);
    listeUnite.add(lbUniteLigne14);
    listeUnite.add(lbUniteLigne15);
    // Initialisation de la liste avec les composants pour les libellés
    listeLibelle = new ArrayList<JLabel>();
    listeLibelle.add(lbLibelleLigne1);
    listeLibelle.add(lbLibelleLigne2);
    listeLibelle.add(lbLibelleLigne3);
    listeLibelle.add(lbLibelleLigne4);
    listeLibelle.add(lbLibelleLigne5);
    listeLibelle.add(lbLibelleLigne6);
    listeLibelle.add(lbLibelleLigne7);
    listeLibelle.add(lbLibelleLigne8);
    listeLibelle.add(lbLibelleLigne9);
    listeLibelle.add(lbLibelleLigne10);
    listeLibelle.add(lbLibelleLigne11);
    listeLibelle.add(lbLibelleLigne12);
    listeLibelle.add(lbLibelleLigne13);
    listeLibelle.add(lbLibelleLigne14);
    listeLibelle.add(lbLibelleLigne15);
    
    // Initialiser les boutons
    snBarreBouton.ajouterBouton(BOUTON_RECHERCHE, 'r', false);
    snBarreBouton.ajouterBouton(BOUTON_ARTICLES_DESACTIVES, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_GAMME_HORS_GAMME, 'g', true);
    snBarreBouton.ajouterBouton(BOUTON_AUTRES_VUES, 'v', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Icones
    BT_FIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    BT_DEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbUniteLigne1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN01@")).trim());
    lbUniteLigne2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN02@")).trim());
    lbUniteLigne3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN03@")).trim());
    lbUniteLigne4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN04@")).trim());
    lbUniteLigne5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN05@")).trim());
    lbUniteLigne6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN06@")).trim());
    lbUniteLigne7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN07@")).trim());
    lbUniteLigne8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN08@")).trim());
    lbUniteLigne9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN09@")).trim());
    lbUniteLigne10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN10@")).trim());
    lbUniteLigne11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN11@")).trim());
    lbUniteLigne12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN12@")).trim());
    lbUniteLigne13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN13@")).trim());
    lbUniteLigne14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN14@")).trim());
    lbUniteLigne15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN15@")).trim());
    lbLibelleLigne1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX01@")).trim());
    lbLibelleLigne2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX02@")).trim());
    lbLibelleLigne3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX03@")).trim());
    lbLibelleLigne4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX04@")).trim());
    lbLibelleLigne5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX05@")).trim());
    lbLibelleLigne6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX06@")).trim());
    lbLibelleLigne7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX07@")).trim());
    lbLibelleLigne8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX08@")).trim());
    lbLibelleLigne9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX09@")).trim());
    lbLibelleLigne10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX10@")).trim());
    lbLibelleLigne11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX11@")).trim());
    lbLibelleLigne12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX12@")).trim());
    lbLibelleLigne13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX13@")).trim());
    lbLibelleLigne14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX14@")).trim());
    lbLibelleLigne15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LX15@")).trim());
    lbTitreLibelle.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITZ@")).trim());
    K09TOT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@K09TOT@")).trim());
    WPDS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPDS@")).trim());
    WVOL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WVOL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    pnlTotaPoids.setVisible(lexique.isTrue("N41"));
    pnlRecherche.setVisible(lexique.isTrue("N41"));
    
    if (lexique.HostFieldGetData("TPDS").trim().isEmpty()) {
      WPDS.setBackground(Color.white);
    }
    else {
      WPDS.setBackground(Color.red);
    }
    
    if (lexique.HostFieldGetData("TVOL").trim().isEmpty()) {
      WVOL.setBackground(Color.white);
    }
    else {
      WVOL.setBackground(Color.red);
    }
    
    for (int i = 0; i < listeSaisieQuantite.size(); i++) {
      // Initialisation de la valeur du composant
      String nomComposantSaisieQuantite = String.format("WTQ%02d", i + 1);
      listeSaisieQuantite.get(i).setText(lexique.HostFieldGetData(nomComposantSaisieQuantite).trim());
      
      // Ajout des menus contextuels
      if (lexique.HostFieldGetData("P08MOD").trim().equals("GAM")) {
        listeSaisieQuantite.get(i).setComponentPopupMenu(BTD2);
        listeLibelle.get(i).setComponentPopupMenu(BTD2);
      }
      else {
        listeSaisieQuantite.get(i).setComponentPopupMenu(BTD);
        listeLibelle.get(i).setComponentPopupMenu(BTD);
      }
      
      // Gestion de la visibilité et de la couleur
      if (!Constantes.normerTexte(listeLibelle.get(i).getText()).isEmpty()) {
        listeSaisieQuantite.get(i).setVisible(true);
        listeUnite.get(i).setVisible(true);
        listeLibelle.get(i).setVisible(true);
        // Selon indicateur 51 à 65 pour changement couleur sur stock négatif
        if (lexique.isTrue(Integer.toString(51 + i))) {
          listeLibelle.get(i).setForeground(Constantes.COULEUR_NEGATIF);
        }
        else {
          listeLibelle.get(i).setForeground(Color.BLACK);
        }
      }
      // Les lignes sont rendues invisibles
      else {
        listeSaisieQuantite.get(i).setVisible(false);
        listeUnite.get(i).setVisible(false);
        listeLibelle.get(i).setVisible(false);
      }
    }
    
    if (lexique.HostFieldGetData("WGHG").trim().equalsIgnoreCase("H")) {
      lbHorsGamme.setText("Hors gamme");
    }
    else if (lexique.HostFieldGetData("WGHG").trim().equalsIgnoreCase("G")) {
      lbHorsGamme.setText("En gamme");
    }
    else {
      lbHorsGamme.setText("");
    }
    
    if (lexique.HostFieldGetData("TPDS").trim().equals("1")) {
      lbPoids.setText("Poids (*)");
      lbPoids.setToolTipText("au moins un article sans poids");
      WPDS.setToolTipText("au moins un article sans poids");
    }
    else {
      lbPoids.setText("Poids");
      lbPoids.setToolTipText("");
      WPDS.setToolTipText("");
    }
    
    if (lexique.HostFieldGetData("TVOL").trim().equals("1")) {
      lbVolume.setText("Volume (*)");
      lbVolume.setToolTipText("au moins un article sans volume");
      WVOL.setToolTipText("au moins un article sans volume");
    }
    else {
      lbVolume.setText("Volume");
      lbVolume.setToolTipText("");
      WVOL.setToolTipText("");
    }
    
    // Les zones ne sont affichées que si elles ne sont pas vides
    
    // Gestion du type d'arguments recherchés ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    String etatRecherche = lexique.HostFieldGetData("P08TYP");
    
    if (etatRecherche.equals("CL1")) {
      bt_argument.setText(lexique.HostFieldGetData("TMC1"));
      fonctions[0] = "F18";
    }
    else if (etatRecherche.equals("CL2")) {
      bt_argument.setText(lexique.HostFieldGetData("TMC2"));
      fonctions[0] = "F18";
    }
    else if (etatRecherche.equals("FAM")) {
      bt_argument.setText("Famille");
      fonctions[0] = "F18";
    }
    else if (etatRecherche.equals("FRS")) {
      bt_argument.setText("Fournisseur");
      fonctions[0] = "F18";
    }
    else {
      bt_argument.setText("Article");
      fonctions[0] = "F18";
    }
    
    gererArguments(etatRecherche);
    
    setTitle("Saisie de quantité sur liste " + lexique.HostFieldGetData("TITCPL"));
    // Option recherche muti critère
    snBarreBouton.activerBouton(BOUTON_RECHERCHE, lexique.isTrue("N41"));
    // Option recherche article desactivé
    snBarreBouton.activerBouton(BOUTON_ARTICLES_DESACTIVES, lexique.isTrue("N41"));
    // Option recherche article gamme non gamme
    snBarreBouton.activerBouton(BOUTON_GAMME_HORS_GAMME, lexique.isTrue("N41"));
    riBoutonDetail1.setVisible(lexique.isTrue("N41"));
    
    rafraichirBoutonValider();
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Mise à jour des valeurs du buffer avec les valeurs saisies dans les composants
    int i = 1;
    for (SNNombreDecimal component : listeSaisieQuantite) {
      lexique.HostFieldPutData(String.format("WTQ%02d", i), 0, Constantes.normerTexte(component.getText()));
      i++;
    }
  }
  
  private void gererLeClavier() {
    Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
      @Override
      public void eventDispatched(AWTEvent event) {
        KeyEvent key = (KeyEvent) event;
        if (key.getID() == KeyEvent.KEY_PRESSED) {
          if (key.getKeyCode() == KeyEvent.VK_F2) {
            lexique.HostCursorPut(ligneCourante, 2);
          }
          else if (key.getKeyCode() == KeyEvent.VK_F14) {
            lexique.HostCursorPut(ligneCourante, 2);
          }
        }
      }
    }, AWTEvent.KEY_EVENT_MASK);
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_RECHERCHE)) {
        lexique.HostScreenSendKey(this, "F8");
      }
      else if (pSNBouton.isBouton(BOUTON_GAMME_HORS_GAMME)) {
        lexique.HostScreenSendKey(this, "F11");
      }
      else if (pSNBouton.isBouton(BOUTON_ARTICLES_DESACTIVES)) {
        lexique.HostScreenSendKey(this, "F23");
      }
      else if (pSNBouton.isBouton(BOUTON_AUTRES_VUES)) {
        lexique.HostScreenSendKey(this, "F24");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(lexique.getPanel(), "F14");
  }
  
  private void MODIFActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(lexique.getPanel(), "F2");
  }
  
  private void menu_calculActionPerformed(ActionEvent e) {
    if (calculatrice == null) {
      calculatrice = new Calculatrice(BTD.getInvoker());
    }
    else {
      calculatrice.reveiller(BTD.getInvoker());
    }
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(lexique.getPanel(), "F4");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(lexique.getPanel(), "F4");
  }
  
  /**
   * Evènement appelé par tous les composants nommés snNombreDecimal??.
   * 
   * @param e
   */
  private void WTQXXFocusLost(FocusEvent e) {
    rafraichirBoutonValider();
  }
  
  /**
   * Activer le bouton de validation.
   */
  private void rafraichirBoutonValider() {
    // Parcours des composants de saisies des quantités à la recherche d'un champ non vide
    for (SNNombreDecimal component : listeSaisieQuantite) {
      if (!component.getText().trim().isEmpty()) {
        snBarreBouton.activerBouton(EnumBouton.VALIDER, true);
        return;
      }
    }
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void gererArguments(String argEnCours) {
    arg_article.setVisible(!argEnCours.equals("ART"));
    arg_famille.setVisible(!argEnCours.equals("FAM"));
    arg_classement.setVisible(!argEnCours.equals("CL1"));
    arg_classement2.setVisible(!argEnCours.equals("CL2"));
    arg_gencod.setVisible(!argEnCours.equals("FRS"));
  }
  
  private void bt_argumentActionPerformed(ActionEvent e) {
    arguments.show(bt_argument, 0, -150);
  }
  
  private void arg_articleActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, fonctions[0]);
  }
  
  private void arg_familleActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[1];
    lexique.HostScreenSendKey(this, fonctions[1]);
  }
  
  private void arg_classementActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[2];
    lexique.HostScreenSendKey(this, fonctions[2]);
  }
  
  private void arg_classement2ActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[3];
    lexique.HostScreenSendKey(this, fonctions[3]);
  }
  
  private void arg_gencodActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[4];
    lexique.HostScreenSendKey(this, fonctions[4]);
  }
  
  private void BT_DEBActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void BT_FINActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void thisMouseWheelMoved(MouseWheelEvent e) {
    lexique.defileListe(e.getWheelRotation());
  }
  
  private void miOptionLieeArticleActionPerformed(ActionEvent e) {
    String nomChamp = BTD2.getInvoker().getName();
    nomChamp = nomChamp.replaceFirst("snNombreDecimal", "WTQ");
    lexique.HostCursorPut(nomChamp);
    lexique.HostScreenSendKey(lexique.getPanel(), "F14");
  }
  
  private void menu_calcul2ActionPerformed(ActionEvent e) {
    if (calculatrice == null) {
      calculatrice = new Calculatrice(BTD2.getInvoker());
    }
    else {
      calculatrice.reveiller(BTD2.getInvoker());
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlRecherche = new SNPanelTitre();
    bt_argument = new SNBoutonLeger();
    riBoutonDetail1 = new SNBoutonDetail();
    P08ARG = new XRiTextField();
    lbHorsGamme = new JLabel();
    pnlArticles = new SNPanelTitre();
    lbTitreQuantite = new JLabel();
    lbTitreUnite = new JLabel();
    lbTitreLibelle = new JLabel();
    snNombreDecimal01 = new SNNombreDecimal();
    lbUniteLigne1 = new SNLabelUnite();
    lbLibelleLigne1 = new JLabel();
    sNPanel1 = new SNPanel();
    BT_DEB = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_FIN = new JButton();
    snNombreDecimal02 = new SNNombreDecimal();
    lbUniteLigne2 = new SNLabelUnite();
    lbLibelleLigne2 = new JLabel();
    snNombreDecimal03 = new SNNombreDecimal();
    lbUniteLigne3 = new SNLabelUnite();
    lbLibelleLigne3 = new JLabel();
    snNombreDecimal04 = new SNNombreDecimal();
    lbUniteLigne4 = new SNLabelUnite();
    lbLibelleLigne4 = new JLabel();
    snNombreDecimal05 = new SNNombreDecimal();
    lbUniteLigne5 = new SNLabelUnite();
    lbLibelleLigne5 = new JLabel();
    snNombreDecimal06 = new SNNombreDecimal();
    lbUniteLigne6 = new SNLabelUnite();
    lbLibelleLigne6 = new JLabel();
    snNombreDecimal07 = new SNNombreDecimal();
    lbUniteLigne7 = new SNLabelUnite();
    lbLibelleLigne7 = new JLabel();
    snNombreDecimal08 = new SNNombreDecimal();
    lbUniteLigne8 = new SNLabelUnite();
    lbLibelleLigne8 = new JLabel();
    snNombreDecimal09 = new SNNombreDecimal();
    lbUniteLigne9 = new SNLabelUnite();
    lbLibelleLigne9 = new JLabel();
    snNombreDecimal10 = new SNNombreDecimal();
    lbUniteLigne10 = new SNLabelUnite();
    lbLibelleLigne10 = new JLabel();
    snNombreDecimal11 = new SNNombreDecimal();
    lbUniteLigne11 = new SNLabelUnite();
    lbLibelleLigne11 = new JLabel();
    snNombreDecimal12 = new SNNombreDecimal();
    lbUniteLigne12 = new SNLabelUnite();
    lbLibelleLigne12 = new JLabel();
    snNombreDecimal13 = new SNNombreDecimal();
    lbUniteLigne13 = new SNLabelUnite();
    lbLibelleLigne13 = new JLabel();
    snNombreDecimal14 = new SNNombreDecimal();
    lbUniteLigne14 = new SNLabelUnite();
    lbLibelleLigne14 = new JLabel();
    snNombreDecimal15 = new SNNombreDecimal();
    lbUniteLigne15 = new SNLabelUnite();
    lbLibelleLigne15 = new JLabel();
    pnlTotaux = new SNPanel();
    pnlTotalQuantite = new SNPanelTitre();
    K09TOT = new SNNombreDecimal();
    lab_qte = new SNLabelUnite();
    pnlTotaPoids = new SNPanelTitre();
    lbPoids = new SNLabelChamp();
    WPDS = new SNNombreDecimal();
    lbVolume = new SNLabelChamp();
    WVOL = new SNNombreDecimal();
    BTD = new JPopupMenu();
    menuItem1 = new JMenuItem();
    MODIF = new JMenuItem();
    OBJ_16 = new JMenuItem();
    menu_calcul = new JMenuItem();
    arguments = new JPopupMenu();
    menuItem2 = new JMenuItem();
    arg_article = new JMenuItem();
    arg_famille = new JMenuItem();
    arg_classement = new JMenuItem();
    arg_classement2 = new JMenuItem();
    arg_gencod = new JMenuItem();
    arg_art_comm = new JMenuItem();
    arg_comm = new JMenuItem();
    BTD2 = new JPopupMenu();
    miOptionLieeArticle = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(1160, 775));
    setPreferredSize(new Dimension(1160, 775));
    setMaximumSize(new Dimension(1160, 775));
    setName("this");
    addMouseWheelListener(new MouseWheelListener() {
      @Override
      public void mouseWheelMoved(MouseWheelEvent e) {
        thisMouseWheelMoved(e);
      }
    });
    setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlRecherche ========
        {
          pnlRecherche.setTitre("Recherche");
          pnlRecherche.setName("pnlRecherche");
          pnlRecherche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRecherche.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlRecherche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRecherche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlRecherche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- bt_argument ----
          bt_argument.setText("Article");
          bt_argument.setComponentPopupMenu(null);
          bt_argument.setToolTipText("Type d'argument de recherche");
          bt_argument.setHorizontalAlignment(SwingConstants.LEADING);
          bt_argument.setVerifyInputWhenFocusTarget(false);
          bt_argument.setMargin(new Insets(0, 0, 0, 0));
          bt_argument.setName("bt_argument");
          bt_argument.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_argumentActionPerformed(e);
            }
          });
          pnlRecherche.add(bt_argument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- riBoutonDetail1 ----
          riBoutonDetail1.setToolTipText("Recherche avanc\u00e9e");
          riBoutonDetail1.setName("riBoutonDetail1");
          riBoutonDetail1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetail1ActionPerformed(e);
            }
          });
          pnlRecherche.add(riBoutonDetail1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- P08ARG ----
          P08ARG.setComponentPopupMenu(null);
          P08ARG.setToolTipText("Argument de recherche");
          P08ARG.setMinimumSize(new Dimension(250, 30));
          P08ARG.setPreferredSize(new Dimension(250, 30));
          P08ARG.setMaximumSize(new Dimension(250, 30));
          P08ARG.setName("P08ARG");
          pnlRecherche.add(P08ARG, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbHorsGamme ----
          lbHorsGamme.setText("Hors gamme ");
          lbHorsGamme.setFont(
              lbHorsGamme.getFont().deriveFont(lbHorsGamme.getFont().getStyle() | Font.BOLD, lbHorsGamme.getFont().getSize() + 4f));
          lbHorsGamme.setHorizontalAlignment(SwingConstants.RIGHT);
          lbHorsGamme.setForeground(new Color(0, 0, 92));
          lbHorsGamme.setBackground(new Color(0, 0, 92));
          lbHorsGamme.setName("lbHorsGamme");
          pnlRecherche.add(lbHorsGamme, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlArticles ========
        {
          pnlArticles.setTitre("Articles");
          pnlArticles.setName("pnlArticles");
          pnlArticles.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlArticles.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlArticles.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlArticles.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlArticles.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbTitreQuantite ----
          lbTitreQuantite.setText("Quantite");
          lbTitreQuantite.setPreferredSize(new Dimension(100, 30));
          lbTitreQuantite.setMinimumSize(new Dimension(100, 30));
          lbTitreQuantite.setMaximumSize(new Dimension(100, 30));
          lbTitreQuantite.setFont(new Font("Courier New", Font.BOLD, 14));
          lbTitreQuantite.setName("lbTitreQuantite");
          pnlArticles.add(lbTitreQuantite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbTitreUnite ----
          lbTitreUnite.setText("Unit\u00e9");
          lbTitreUnite.setPreferredSize(new Dimension(50, 30));
          lbTitreUnite.setMinimumSize(new Dimension(50, 30));
          lbTitreUnite.setMaximumSize(new Dimension(50, 30));
          lbTitreUnite.setFont(new Font("Courier New", Font.BOLD, 14));
          lbTitreUnite.setName("lbTitreUnite");
          pnlArticles.add(lbTitreUnite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbTitreLibelle ----
          lbTitreLibelle.setText("@TITZ@");
          lbTitreLibelle.setPreferredSize(new Dimension(100, 30));
          lbTitreLibelle.setMinimumSize(new Dimension(100, 30));
          lbTitreLibelle.setMaximumSize(new Dimension(100, 30));
          lbTitreLibelle.setFont(new Font("Courier New", Font.BOLD, 14));
          lbTitreLibelle.setName("lbTitreLibelle");
          pnlArticles.add(lbTitreLibelle, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal01 ----
          snNombreDecimal01.setName("snNombreDecimal01");
          snNombreDecimal01.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal01, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne1 ----
          lbUniteLigne1.setText("@WUN01@");
          lbUniteLigne1.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne1.setName("lbUniteLigne1");
          pnlArticles.add(lbUniteLigne1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne1 ----
          lbLibelleLigne1.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne1.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne1.setText("libelle 1");
          lbLibelleLigne1.setName("lbLibelleLigne1");
          pnlArticles.add(lbLibelleLigne1, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ======== sNPanel1 ========
          {
            sNPanel1.setName("sNPanel1");
            sNPanel1.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0, 0.0, 1.0E-4 };
            
            // ---- BT_DEB ----
            BT_DEB.setText("");
            BT_DEB.setToolTipText("D\u00e9but de la liste");
            BT_DEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_DEB.setMaximumSize(new Dimension(25, 30));
            BT_DEB.setMinimumSize(new Dimension(25, 30));
            BT_DEB.setPreferredSize(new Dimension(25, 30));
            BT_DEB.setFocusable(false);
            BT_DEB.setName("BT_DEB");
            BT_DEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_DEBActionPerformed(e);
              }
            });
            sNPanel1.add(BT_DEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_PGUP ----
            BT_PGUP.setToolTipText("page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setFocusable(false);
            BT_PGUP.setMaximumSize(new Dimension(25, 110));
            BT_PGUP.setMinimumSize(new Dimension(25, 110));
            BT_PGUP.setPreferredSize(new Dimension(25, 110));
            BT_PGUP.setName("BT_PGUP");
            sNPanel1.add(BT_PGUP, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setToolTipText("page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setFocusable(false);
            BT_PGDOWN.setMaximumSize(new Dimension(25, 110));
            BT_PGDOWN.setMinimumSize(new Dimension(25, 110));
            BT_PGDOWN.setPreferredSize(new Dimension(25, 110));
            BT_PGDOWN.setName("BT_PGDOWN");
            sNPanel1.add(BT_PGDOWN, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_FIN ----
            BT_FIN.setText("");
            BT_FIN.setToolTipText("Fin de la liste");
            BT_FIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_FIN.setMaximumSize(new Dimension(25, 30));
            BT_FIN.setMinimumSize(new Dimension(25, 30));
            BT_FIN.setPreferredSize(new Dimension(25, 30));
            BT_FIN.setFocusable(false);
            BT_FIN.setName("BT_FIN");
            BT_FIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_FINActionPerformed(e);
              }
            });
            sNPanel1.add(BT_FIN, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlArticles.add(sNPanel1, new GridBagConstraints(3, 1, 1, 15, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- snNombreDecimal02 ----
          snNombreDecimal02.setName("snNombreDecimal02");
          snNombreDecimal02.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal02, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne2 ----
          lbUniteLigne2.setText("@WUN02@");
          lbUniteLigne2.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne2.setName("lbUniteLigne2");
          pnlArticles.add(lbUniteLigne2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne2 ----
          lbLibelleLigne2.setText("@LX02@");
          lbLibelleLigne2.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne2.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne2.setName("lbLibelleLigne2");
          pnlArticles.add(lbLibelleLigne2, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal03 ----
          snNombreDecimal03.setName("snNombreDecimal03");
          snNombreDecimal03.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal03, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne3 ----
          lbUniteLigne3.setText("@WUN03@");
          lbUniteLigne3.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne3.setName("lbUniteLigne3");
          pnlArticles.add(lbUniteLigne3, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne3 ----
          lbLibelleLigne3.setText("@LX03@");
          lbLibelleLigne3.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne3.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne3.setName("lbLibelleLigne3");
          pnlArticles.add(lbLibelleLigne3, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal04 ----
          snNombreDecimal04.setName("snNombreDecimal04");
          snNombreDecimal04.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal04, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne4 ----
          lbUniteLigne4.setText("@WUN04@");
          lbUniteLigne4.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne4.setName("lbUniteLigne4");
          pnlArticles.add(lbUniteLigne4, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne4 ----
          lbLibelleLigne4.setText("@LX04@");
          lbLibelleLigne4.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne4.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne4.setName("lbLibelleLigne4");
          pnlArticles.add(lbLibelleLigne4, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal05 ----
          snNombreDecimal05.setName("snNombreDecimal05");
          snNombreDecimal05.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal05, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne5 ----
          lbUniteLigne5.setText("@WUN05@");
          lbUniteLigne5.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne5.setName("lbUniteLigne5");
          pnlArticles.add(lbUniteLigne5, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne5 ----
          lbLibelleLigne5.setText("@LX05@");
          lbLibelleLigne5.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne5.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne5.setName("lbLibelleLigne5");
          pnlArticles.add(lbLibelleLigne5, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal06 ----
          snNombreDecimal06.setName("snNombreDecimal06");
          snNombreDecimal06.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal06, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne6 ----
          lbUniteLigne6.setText("@WUN06@");
          lbUniteLigne6.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne6.setName("lbUniteLigne6");
          pnlArticles.add(lbUniteLigne6, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne6 ----
          lbLibelleLigne6.setText("@LX06@");
          lbLibelleLigne6.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne6.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne6.setName("lbLibelleLigne6");
          pnlArticles.add(lbLibelleLigne6, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal07 ----
          snNombreDecimal07.setName("snNombreDecimal07");
          snNombreDecimal07.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal07, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne7 ----
          lbUniteLigne7.setText("@WUN07@");
          lbUniteLigne7.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne7.setName("lbUniteLigne7");
          pnlArticles.add(lbUniteLigne7, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne7 ----
          lbLibelleLigne7.setText("@LX07@");
          lbLibelleLigne7.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne7.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne7.setName("lbLibelleLigne7");
          pnlArticles.add(lbLibelleLigne7, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal08 ----
          snNombreDecimal08.setName("snNombreDecimal08");
          snNombreDecimal08.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal08, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne8 ----
          lbUniteLigne8.setText("@WUN08@");
          lbUniteLigne8.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne8.setName("lbUniteLigne8");
          pnlArticles.add(lbUniteLigne8, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne8 ----
          lbLibelleLigne8.setText("@LX08@");
          lbLibelleLigne8.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne8.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne8.setName("lbLibelleLigne8");
          pnlArticles.add(lbLibelleLigne8, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal09 ----
          snNombreDecimal09.setName("snNombreDecimal09");
          snNombreDecimal09.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal09, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne9 ----
          lbUniteLigne9.setText("@WUN09@");
          lbUniteLigne9.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne9.setName("lbUniteLigne9");
          pnlArticles.add(lbUniteLigne9, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne9 ----
          lbLibelleLigne9.setText("@LX09@");
          lbLibelleLigne9.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne9.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne9.setName("lbLibelleLigne9");
          pnlArticles.add(lbLibelleLigne9, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal10 ----
          snNombreDecimal10.setName("snNombreDecimal10");
          snNombreDecimal10.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal10, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne10 ----
          lbUniteLigne10.setText("@WUN10@");
          lbUniteLigne10.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne10.setName("lbUniteLigne10");
          pnlArticles.add(lbUniteLigne10, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne10 ----
          lbLibelleLigne10.setText("@LX10@");
          lbLibelleLigne10.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne10.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne10.setName("lbLibelleLigne10");
          pnlArticles.add(lbLibelleLigne10, new GridBagConstraints(2, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal11 ----
          snNombreDecimal11.setName("snNombreDecimal11");
          snNombreDecimal11.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal11, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne11 ----
          lbUniteLigne11.setText("@WUN11@");
          lbUniteLigne11.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne11.setName("lbUniteLigne11");
          pnlArticles.add(lbUniteLigne11, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne11 ----
          lbLibelleLigne11.setText("@LX11@");
          lbLibelleLigne11.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne11.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne11.setName("lbLibelleLigne11");
          pnlArticles.add(lbLibelleLigne11, new GridBagConstraints(2, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal12 ----
          snNombreDecimal12.setName("snNombreDecimal12");
          snNombreDecimal12.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal12, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne12 ----
          lbUniteLigne12.setText("@WUN12@");
          lbUniteLigne12.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne12.setName("lbUniteLigne12");
          pnlArticles.add(lbUniteLigne12, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne12 ----
          lbLibelleLigne12.setText("@LX12@");
          lbLibelleLigne12.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne12.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne12.setName("lbLibelleLigne12");
          pnlArticles.add(lbLibelleLigne12, new GridBagConstraints(2, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal13 ----
          snNombreDecimal13.setName("snNombreDecimal13");
          snNombreDecimal13.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal13, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne13 ----
          lbUniteLigne13.setText("@WUN13@");
          lbUniteLigne13.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne13.setName("lbUniteLigne13");
          pnlArticles.add(lbUniteLigne13, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne13 ----
          lbLibelleLigne13.setText("@LX13@");
          lbLibelleLigne13.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne13.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne13.setName("lbLibelleLigne13");
          pnlArticles.add(lbLibelleLigne13, new GridBagConstraints(2, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal14 ----
          snNombreDecimal14.setName("snNombreDecimal14");
          snNombreDecimal14.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal14, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbUniteLigne14 ----
          lbUniteLigne14.setText("@WUN14@");
          lbUniteLigne14.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne14.setName("lbUniteLigne14");
          pnlArticles.add(lbUniteLigne14, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- lbLibelleLigne14 ----
          lbLibelleLigne14.setText("@LX14@");
          lbLibelleLigne14.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne14.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne14.setName("lbLibelleLigne14");
          pnlArticles.add(lbLibelleLigne14, new GridBagConstraints(2, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 1, 5), 0, 0));
          
          // ---- snNombreDecimal15 ----
          snNombreDecimal15.setName("snNombreDecimal15");
          snNombreDecimal15.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              WTQXXFocusLost(e);
            }
          });
          pnlArticles.add(snNombreDecimal15, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbUniteLigne15 ----
          lbUniteLigne15.setText("@WUN15@");
          lbUniteLigne15.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbUniteLigne15.setName("lbUniteLigne15");
          pnlArticles.add(lbUniteLigne15, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbLibelleLigne15 ----
          lbLibelleLigne15.setText("@LX15@");
          lbLibelleLigne15.setFont(new Font("Courier New", Font.PLAIN, 14));
          lbLibelleLigne15.setHorizontalAlignment(SwingConstants.LEADING);
          lbLibelleLigne15.setName("lbLibelleLigne15");
          pnlArticles.add(lbLibelleLigne15, new GridBagConstraints(2, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlContenu.add(pnlArticles, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlTotaux ========
        {
          pnlTotaux.setName("pnlTotaux");
          pnlTotaux.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTotaux.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTotaux.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTotaux.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTotaux.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlTotalQuantite ========
          {
            pnlTotalQuantite.setName("pnlTotalQuantite");
            pnlTotalQuantite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTotalQuantite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTotalQuantite.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlTotalQuantite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlTotalQuantite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- K09TOT ----
            K09TOT.setEditable(false);
            K09TOT.setEnabled(false);
            K09TOT.setName("K09TOT");
            pnlTotalQuantite.add(K09TOT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lab_qte ----
            lab_qte.setText("Quantit\u00e9 totale");
            lab_qte.setName("lab_qte");
            pnlTotalQuantite.add(lab_qte, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlTotaux.add(pnlTotalQuantite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlTotaPoids ========
          {
            pnlTotaPoids.setName("pnlTotaPoids");
            pnlTotaPoids.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTotaPoids.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlTotaPoids.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlTotaPoids.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTotaPoids.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbPoids ----
            lbPoids.setText("Poids");
            lbPoids.setName("lbPoids");
            pnlTotaPoids.add(lbPoids, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WPDS ----
            WPDS.setName("WPDS");
            pnlTotaPoids.add(WPDS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbVolume ----
            lbVolume.setText("Volume");
            lbVolume.setName("lbVolume");
            pnlTotaPoids.add(lbVolume, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WVOL ----
            WVOL.setName("WVOL");
            pnlTotaPoids.add(WVOL, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlTotaux.add(pnlTotaPoids, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlTotaux, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- menuItem1 ----
      menuItem1.setText("Choix unit\u00e9");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD.add(menuItem1);
      
      // ---- MODIF ----
      MODIF.setText("Modification");
      MODIF.setName("MODIF");
      MODIF.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MODIFActionPerformed(e);
        }
      });
      BTD.add(MODIF);
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Options li\u00e9es \u00e0 l'article");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
      
      // ---- menu_calcul ----
      menu_calcul.setText("Calculatrice");
      menu_calcul.setName("menu_calcul");
      menu_calcul.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menu_calculActionPerformed(e);
        }
      });
      BTD.add(menu_calcul);
    }
    
    // ======== arguments ========
    {
      arguments.setName("arguments");
      
      // ---- menuItem2 ----
      menuItem2.setText("Recherche");
      menuItem2.setFont(menuItem2.getFont().deriveFont(Font.BOLD));
      menuItem2.setBackground(new Color(204, 204, 204));
      menuItem2.setOpaque(true);
      menuItem2.setRequestFocusEnabled(false);
      menuItem2.setName("menuItem2");
      arguments.add(menuItem2);
      
      // ---- arg_article ----
      arg_article.setText("Par code article");
      arg_article.setName("arg_article");
      arg_article.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          arg_articleActionPerformed(e);
        }
      });
      arguments.add(arg_article);
      
      // ---- arg_famille ----
      arg_famille.setText("Par famille");
      arg_famille.setName("arg_famille");
      arg_famille.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          arg_familleActionPerformed(e);
        }
      });
      arguments.add(arg_famille);
      
      // ---- arg_classement ----
      arg_classement.setText("Par mot de classement 1");
      arg_classement.setName("arg_classement");
      arg_classement.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          arg_classementActionPerformed(e);
        }
      });
      arguments.add(arg_classement);
      
      // ---- arg_classement2 ----
      arg_classement2.setText("Par mot de classement 2");
      arg_classement2.setName("arg_classement2");
      arg_classement2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          arg_classement2ActionPerformed(e);
        }
      });
      arguments.add(arg_classement2);
      
      // ---- arg_gencod ----
      arg_gencod.setText("Par fournisseur");
      arg_gencod.setName("arg_gencod");
      arg_gencod.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          arg_gencodActionPerformed(e);
        }
      });
      arguments.add(arg_gencod);
      
      // ---- arg_art_comm ----
      arg_art_comm.setText("Article commentaire");
      arg_art_comm.setToolTipText("Recherche d'article commentaires par le code");
      arg_art_comm.setFont(arg_art_comm.getFont().deriveFont(arg_art_comm.getFont().getStyle() | Font.ITALIC));
      arg_art_comm.setName("arg_art_comm");
      arguments.add(arg_art_comm);
      
      // ---- arg_comm ----
      arg_comm.setText("Saisie commentaire");
      arg_comm.setBackground(new Color(217, 217, 217));
      arg_comm.setOpaque(true);
      arg_comm.setToolTipText("saisie commentaire libre");
      arg_comm.setFont(arg_comm.getFont().deriveFont(arg_comm.getFont().getStyle() | Font.ITALIC));
      arg_comm.setName("arg_comm");
      arguments.add(arg_comm);
    }
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- miOptionLieeArticle ----
      miOptionLieeArticle.setText("Options li\u00e9es \u00e0 l'article");
      miOptionLieeArticle.setName("miOptionLieeArticle");
      miOptionLieeArticle.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miOptionLieeArticleActionPerformed(e);
        }
      });
      BTD2.add(miOptionLieeArticle);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlPrincipal;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlRecherche;
  private SNBoutonLeger bt_argument;
  private SNBoutonDetail riBoutonDetail1;
  private XRiTextField P08ARG;
  private JLabel lbHorsGamme;
  private SNPanelTitre pnlArticles;
  private JLabel lbTitreQuantite;
  private JLabel lbTitreUnite;
  private JLabel lbTitreLibelle;
  private SNNombreDecimal snNombreDecimal01;
  private SNLabelUnite lbUniteLigne1;
  private JLabel lbLibelleLigne1;
  private SNPanel sNPanel1;
  private JButton BT_DEB;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_FIN;
  private SNNombreDecimal snNombreDecimal02;
  private SNLabelUnite lbUniteLigne2;
  private JLabel lbLibelleLigne2;
  private SNNombreDecimal snNombreDecimal03;
  private SNLabelUnite lbUniteLigne3;
  private JLabel lbLibelleLigne3;
  private SNNombreDecimal snNombreDecimal04;
  private SNLabelUnite lbUniteLigne4;
  private JLabel lbLibelleLigne4;
  private SNNombreDecimal snNombreDecimal05;
  private SNLabelUnite lbUniteLigne5;
  private JLabel lbLibelleLigne5;
  private SNNombreDecimal snNombreDecimal06;
  private SNLabelUnite lbUniteLigne6;
  private JLabel lbLibelleLigne6;
  private SNNombreDecimal snNombreDecimal07;
  private SNLabelUnite lbUniteLigne7;
  private JLabel lbLibelleLigne7;
  private SNNombreDecimal snNombreDecimal08;
  private SNLabelUnite lbUniteLigne8;
  private JLabel lbLibelleLigne8;
  private SNNombreDecimal snNombreDecimal09;
  private SNLabelUnite lbUniteLigne9;
  private JLabel lbLibelleLigne9;
  private SNNombreDecimal snNombreDecimal10;
  private SNLabelUnite lbUniteLigne10;
  private JLabel lbLibelleLigne10;
  private SNNombreDecimal snNombreDecimal11;
  private SNLabelUnite lbUniteLigne11;
  private JLabel lbLibelleLigne11;
  private SNNombreDecimal snNombreDecimal12;
  private SNLabelUnite lbUniteLigne12;
  private JLabel lbLibelleLigne12;
  private SNNombreDecimal snNombreDecimal13;
  private SNLabelUnite lbUniteLigne13;
  private JLabel lbLibelleLigne13;
  private SNNombreDecimal snNombreDecimal14;
  private SNLabelUnite lbUniteLigne14;
  private JLabel lbLibelleLigne14;
  private SNNombreDecimal snNombreDecimal15;
  private SNLabelUnite lbUniteLigne15;
  private JLabel lbLibelleLigne15;
  private SNPanel pnlTotaux;
  private SNPanelTitre pnlTotalQuantite;
  private SNNombreDecimal K09TOT;
  private SNLabelUnite lab_qte;
  private SNPanelTitre pnlTotaPoids;
  private SNLabelChamp lbPoids;
  private SNNombreDecimal WPDS;
  private SNLabelChamp lbVolume;
  private SNNombreDecimal WVOL;
  private JPopupMenu BTD;
  private JMenuItem menuItem1;
  private JMenuItem MODIF;
  private JMenuItem OBJ_16;
  private JMenuItem menu_calcul;
  private JPopupMenu arguments;
  private JMenuItem menuItem2;
  private JMenuItem arg_article;
  private JMenuItem arg_famille;
  private JMenuItem arg_classement;
  private JMenuItem arg_classement2;
  private JMenuItem arg_gencod;
  private JMenuItem arg_art_comm;
  private JMenuItem arg_comm;
  private JPopupMenu BTD2;
  private JMenuItem miOptionLieeArticle;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
