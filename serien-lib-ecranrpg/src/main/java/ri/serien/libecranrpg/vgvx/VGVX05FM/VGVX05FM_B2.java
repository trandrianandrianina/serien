
package ri.serien.libecranrpg.vgvx.VGVX05FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * [GVM132] Gestion des ventes -> Fiches permanentes -> Articles -> Articles commentaires
 * Indicateur : 100001010
 * Titre : Articles commentaires
 */
public class VGVX05FM_B2 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] A1IN3_Value = { "", "V", "A", };
  private String[] A1IN1_Value = { "", "1", "2", };
  boolean isLibelleLong = false;
  private Message creation = null;
  private static String BOUTON_MODIFIER = "Modifier";
  private static String BOUTON_SUPPRIMER = "Supprimer";
  
  /**
   * Constructeur.
   */
  public VGVX05FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    A1IN3.setValeurs(A1IN3_Value, null);
    A1IN1.setValeurs(A1IN1_Value, null);
    A1DSG4.setValeursSelection("I", "");
    A1DSG3.setValeursSelection("I", "");
    A1DSG2.setValeursSelection("I", "");
    A1DSG1.setValeursSelection("I", "");
    A1EDT6.setValeursSelection("X", "");
    A1EDT5.setValeursSelection("X", "");
    A1EDT4.setValeursSelection("X", "");
    A1EDT3.setValeursSelection("X", "");
    A1EDT2.setValeursSelection("X", "");
    A1EDT1.setValeursSelection("X", "");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.RETOURNER_RECHERCHE, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER, 'm', false);
    snBarreBouton.ajouterBouton(BOUTON_SUPPRIMER, 's', false);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Article commentaire @INDART@")).trim());
  }
  
  @Override
  public void setData() {
    
    super.setData();
    
    setDiverses();
    
    // Mode
    boolean isModification = lexique.getMode() == Lexical.MODE_MODIFICATION;
    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    boolean isCreation = lexique.getMode() == Lexical.MODE_CREATION;
    
    // Titre
    bpPresentation.setText("Article commentaire (" + lexique.HostFieldGetData("INDART").trim() + ")");
    
    // Les deux checkbox partagant le meme champ, vérifie lequel doit etre selectionner
    A1EDT1_OBJ_75_34.setSelected(lexique.HostFieldGetData("A1EDT1").equals("9"));
    // A1EDT1.setSelected(!lexique.HostFieldGetData("A1EDT1").trim().isEmpty());
    A1EDT1.setSelected(!lexique.HostFieldGetData("A1EDT1").equals("9"));
    
    // Gestion de la sélection de aucune édition
    if (!A1EDT1_OBJ_75_34.isSelected() && !isConsultation) {
      A1EDT1.setSelected(!lexique.HostFieldGetData("A1EDT1").trim().isEmpty());
      A1EDT1.setEnabled(true);
      A1EDT3.setSelected(!lexique.HostFieldGetData("A1EDT3").trim().isEmpty());
      A1EDT3.setEnabled(true);
      A1EDT4.setSelected(!lexique.HostFieldGetData("A1EDT4").trim().isEmpty());
      A1EDT4.setEnabled(true);
      A1EDT2.setSelected(!lexique.HostFieldGetData("A1EDT2").trim().isEmpty());
      A1EDT2.setEnabled(true);
      A1EDT5.setSelected(!lexique.HostFieldGetData("A1EDT5").trim().isEmpty());
      A1EDT5.setEnabled(true);
      A1EDT6.setSelected(!lexique.HostFieldGetData("A1EDT6").trim().isEmpty());
      A1EDT6.setEnabled(true);
    }
    else if (A1EDT1_OBJ_75_34.isSelected() && !isConsultation) {
      A1EDT1.setSelected(false);
      A1EDT3.setSelected(false);
      A1EDT4.setSelected(false);
      A1EDT2.setSelected(false);
      A1EDT5.setSelected(false);
      A1EDT6.setSelected(false);
    }
    // Si aucune des options est sélectionne, sélectionne "Aucune édition"
    if (!A1EDT1.isSelected() && !A1EDT2.isSelected() && !A1EDT3.isSelected() && !A1EDT4.isSelected() && !A1EDT5.isSelected()
        && !A1EDT6.isSelected()) {
      A1EDT1_OBJ_75_34.setSelected(true);
      A1EDT1.setEnabled(false);
      A1EDT2.setEnabled(false);
      A1EDT3.setEnabled(false);
      A1EDT4.setEnabled(false);
      A1EDT5.setEnabled(false);
      A1EDT6.setEnabled(false);
    }
    
    // Si on est en mode création, ne sélectionne aucune option d'édition
    if (isCreation) {
      A1EDT1_OBJ_75_34.setSelected(false);
      A1EDT1.setSelected(false);
      A1EDT3.setSelected(false);
      A1EDT4.setSelected(false);
      A1EDT2.setSelected(false);
      A1EDT5.setSelected(false);
      A1EDT6.setSelected(false);
      A1EDT1_OBJ_75_34.setEnabled(true);
      A1EDT1.setEnabled(true);
      A1EDT3.setEnabled(true);
      A1EDT4.setEnabled(true);
      A1EDT2.setEnabled(true);
      A1EDT5.setEnabled(true);
      A1EDT6.setEnabled(true);
    }
    
    // DESIGNATION 4 zones ou 2 zones
    isLibelleLong = (lexique.HostFieldGetData("PST39").equals("4"));
    A1LIB.setVisible(!isLibelleLong);
    A1LB1.setVisible(!isLibelleLong);
    A1LB2.setVisible(!isLibelleLong);
    A1LB3.setVisible(!isLibelleLong);
    A1DSG1.setVisible(!isLibelleLong);
    A1DSG2.setVisible(!isLibelleLong);
    A1DSG3.setVisible(!isLibelleLong);
    A1DSG4.setVisible(!isLibelleLong);
    A1LB12.setVisible(isLibelleLong);
    A1LB34.setVisible(isLibelleLong);
    
    // Permet d'activer le bouton valider si l'ecran n'est pas en consultation
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      snBarreBouton.activerBouton(EnumBouton.VALIDER, true);
      A1EDT1_OBJ_75_34.setEnabled(true);
    }
    else {
      snBarreBouton.activerBouton(EnumBouton.VALIDER, false);
      A1EDT1_OBJ_75_34.setEnabled(false);
    }
    
    // Visiblité
    lbLigneUnCommentaire.setVisible(A1LB12.isVisible());
    lbLigneDeuxCommentaire.setVisible(A1LB34.isVisible());
    pnlCommentaireLong.setVisible(A1LIB.isVisible() || A1LB1.isVisible() || A1LB2.isVisible() || A1LB3.isVisible());
    lbLigneUn.setVisible(A1LIB.isVisible());
    lbLigneDeux.setVisible(A1LB1.isVisible());
    lbLigneTrois.setVisible(A1LB2.isVisible());
    lbLigneQuatre.setVisible(A1LB3.isVisible());
    
    // Visibilité des boutons dans la consultation
    if (isConsultation) {
      snBarreBouton.activerBouton(BOUTON_MODIFIER, true);
      snBarreBouton.activerBouton(BOUTON_SUPPRIMER, true);
    }
    else {
      snBarreBouton.activerBouton(BOUTON_MODIFIER, false);
      snBarreBouton.activerBouton(BOUTON_SUPPRIMER, false);
    }
    
    // Visibilité des boutons annuler et retourner recherche
    if (isModification || isCreation) {
      snBarreBouton.activerBouton(EnumBouton.RETOURNER_RECHERCHE, false);
      snBarreBouton.activerBouton(EnumBouton.ANNULER, true);
    }
    else {
      snBarreBouton.activerBouton(EnumBouton.RETOURNER_RECHERCHE, true);
      snBarreBouton.activerBouton(EnumBouton.ANNULER, false);
    }
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("INDETB"));
    
    // Charge l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    if (A1EDT1_OBJ_75_34.isSelected()) {
      lexique.HostFieldPutData("A1EDT1", 0, "9");
    }
    else {
      if (A1EDT1.isSelected()) {
        lexique.HostFieldPutData("A1EDT1", 0, "X");
      }
      else {
        lexique.HostFieldPutData("A1EDT1", 0, "");
      }
    }
  }
  
  // EVENEMENTIEL
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.RETOURNER_RECHERCHE)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_SUPPRIMER)) {
        lexique.HostScreenSendKey(this, "F16");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void aucuneEditionActionPerformed(ActionEvent e) {
    if (A1EDT1_OBJ_75_34.isSelected()) {
      A1EDT1.setSelected(false);
      A1EDT1.setEnabled(false);
      A1EDT3.setSelected(false);
      A1EDT3.setEnabled(false);
      A1EDT4.setSelected(false);
      A1EDT4.setEnabled(false);
      A1EDT2.setSelected(false);
      A1EDT2.setEnabled(false);
      A1EDT5.setSelected(false);
      A1EDT5.setEnabled(false);
      A1EDT6.setSelected(false);
      A1EDT6.setEnabled(false);
    }
    else {
      A1EDT1.setSelected(true);
      A1EDT1.setEnabled(true);
      A1EDT3.setSelected(true);
      A1EDT3.setEnabled(true);
      A1EDT4.setSelected(true);
      A1EDT4.setEnabled(true);
      A1EDT2.setSelected(true);
      A1EDT2.setEnabled(true);
      A1EDT5.setSelected(true);
      A1EDT5.setEnabled(true);
      A1EDT6.setSelected(true);
      A1EDT6.setEnabled(true);
    }
  }
  
  private void selectionAucuneEditionActionPerformed(ActionEvent e) {
    if (!A1EDT1.isSelected() && !A1EDT2.isSelected() && !A1EDT3.isSelected() && !A1EDT4.isSelected() && !A1EDT5.isSelected()
        && !A1EDT6.isSelected()) {
      A1EDT1_OBJ_75_34.setSelected(true);
      A1EDT1.setEnabled(false);
      A1EDT2.setEnabled(false);
      A1EDT3.setEnabled(false);
      A1EDT4.setEnabled(false);
      A1EDT5.setEnabled(false);
      A1EDT6.setEnabled(false);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlCommentaire = new SNPanelTitre();
    pnlCommentaireLong = new SNPanel();
    lbLigneUn = new SNLabelChamp();
    A1LIB = new XRiTextField();
    A1DSG1 = new XRiCheckBox();
    lbLigneDeux = new SNLabelChamp();
    A1LB1 = new XRiTextField();
    A1DSG2 = new XRiCheckBox();
    lbLigneTrois = new SNLabelChamp();
    A1LB2 = new XRiTextField();
    A1DSG3 = new XRiCheckBox();
    lbLigneQuatre = new SNLabelChamp();
    A1LB3 = new XRiTextField();
    A1DSG4 = new XRiCheckBox();
    lbLigneUnCommentaire = new SNLabelChamp();
    A1LB12 = new XRiTextField();
    lbLigneDeuxCommentaire = new SNLabelChamp();
    A1LB34 = new XRiTextField();
    pnlInformationGenerale = new SNPanelTitre();
    lbSaisiePlage = new SNLabelChamp();
    A1IN1 = new XRiComboBox();
    lbType = new SNLabelChamp();
    A1IN3 = new XRiComboBox();
    lbMotsDeClassement1 = new SNLabelChamp();
    A1CL1 = new XRiTextField();
    lbMotsDeClassement2 = new SNLabelChamp();
    A1CL2 = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlOptionEdition = new SNPanelTitre();
    A1EDT1_OBJ_75_34 = new JCheckBox();
    A1EDT1 = new XRiCheckBox();
    A1EDT3 = new XRiCheckBox();
    A1EDT4 = new XRiCheckBox();
    A1EDT2 = new XRiCheckBox();
    A1EDT5 = new XRiCheckBox();
    A1EDT6 = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Article commentaire @INDART@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout(1, 2));
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlCommentaire ========
        {
          pnlCommentaire.setTitre("Commentaire");
          pnlCommentaire.setName("pnlCommentaire");
          pnlCommentaire.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCommentaire.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCommentaire.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlCommentaire.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCommentaire.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCommentaireLong ========
          {
            pnlCommentaireLong.setName("pnlCommentaireLong");
            pnlCommentaireLong.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCommentaireLong.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCommentaireLong.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCommentaireLong.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCommentaireLong.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbLigneUn ----
            lbLigneUn.setText("Ligne 1");
            lbLigneUn.setName("lbLigneUn");
            pnlCommentaireLong.add(lbLigneUn, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- A1LIB ----
            A1LIB.setFont(new Font("sansserif", Font.PLAIN, 14));
            A1LIB.setPreferredSize(new Dimension(368, 30));
            A1LIB.setMinimumSize(new Dimension(368, 30));
            A1LIB.setMaximumSize(new Dimension(368, 30));
            A1LIB.setName("A1LIB");
            pnlCommentaireLong.add(A1LIB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- A1DSG1 ----
            A1DSG1.setToolTipText("?B2.A1DSG1.toolTipText_2?");
            A1DSG1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1DSG1.setText("Commentaire interne");
            A1DSG1.setFont(new Font("sansserif", Font.PLAIN, 14));
            A1DSG1.setPreferredSize(new Dimension(156, 30));
            A1DSG1.setMinimumSize(new Dimension(156, 30));
            A1DSG1.setMaximumSize(new Dimension(156, 30));
            A1DSG1.setName("A1DSG1");
            pnlCommentaireLong.add(A1DSG1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLigneDeux ----
            lbLigneDeux.setText("Ligne 2");
            lbLigneDeux.setName("lbLigneDeux");
            pnlCommentaireLong.add(lbLigneDeux, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- A1LB1 ----
            A1LB1.setFont(new Font("sansserif", Font.PLAIN, 14));
            A1LB1.setPreferredSize(new Dimension(368, 30));
            A1LB1.setMinimumSize(new Dimension(368, 30));
            A1LB1.setMaximumSize(new Dimension(368, 30));
            A1LB1.setName("A1LB1");
            pnlCommentaireLong.add(A1LB1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- A1DSG2 ----
            A1DSG2.setToolTipText("?B2.A1DSG2.toolTipText_2?");
            A1DSG2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1DSG2.setFont(new Font("sansserif", Font.PLAIN, 14));
            A1DSG2.setText("Commentaire interne");
            A1DSG2.setPreferredSize(new Dimension(156, 30));
            A1DSG2.setMinimumSize(new Dimension(156, 30));
            A1DSG2.setMaximumSize(new Dimension(156, 30));
            A1DSG2.setName("A1DSG2");
            pnlCommentaireLong.add(A1DSG2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLigneTrois ----
            lbLigneTrois.setText("Ligne 3");
            lbLigneTrois.setName("lbLigneTrois");
            pnlCommentaireLong.add(lbLigneTrois, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- A1LB2 ----
            A1LB2.setFont(new Font("sansserif", Font.PLAIN, 14));
            A1LB2.setPreferredSize(new Dimension(368, 30));
            A1LB2.setMinimumSize(new Dimension(368, 30));
            A1LB2.setMaximumSize(new Dimension(368, 30));
            A1LB2.setName("A1LB2");
            pnlCommentaireLong.add(A1LB2, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- A1DSG3 ----
            A1DSG3.setToolTipText("?B2.A1DSG3.toolTipText_2?");
            A1DSG3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1DSG3.setFont(new Font("sansserif", Font.PLAIN, 14));
            A1DSG3.setText("Commentaire interne");
            A1DSG3.setPreferredSize(new Dimension(156, 30));
            A1DSG3.setMinimumSize(new Dimension(156, 30));
            A1DSG3.setMaximumSize(new Dimension(156, 30));
            A1DSG3.setName("A1DSG3");
            pnlCommentaireLong.add(A1DSG3, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLigneQuatre ----
            lbLigneQuatre.setText("Ligne 4");
            lbLigneQuatre.setName("lbLigneQuatre");
            pnlCommentaireLong.add(lbLigneQuatre, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- A1LB3 ----
            A1LB3.setFont(new Font("sansserif", Font.PLAIN, 14));
            A1LB3.setPreferredSize(new Dimension(368, 30));
            A1LB3.setMinimumSize(new Dimension(368, 30));
            A1LB3.setMaximumSize(new Dimension(368, 30));
            A1LB3.setName("A1LB3");
            pnlCommentaireLong.add(A1LB3, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- A1DSG4 ----
            A1DSG4.setToolTipText("?B2.A1DSG4.toolTipText_2?");
            A1DSG4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1DSG4.setFont(new Font("sansserif", Font.PLAIN, 14));
            A1DSG4.setText("Commentaire interne");
            A1DSG4.setPreferredSize(new Dimension(156, 30));
            A1DSG4.setMinimumSize(new Dimension(156, 30));
            A1DSG4.setMaximumSize(new Dimension(156, 30));
            A1DSG4.setName("A1DSG4");
            pnlCommentaireLong.add(A1DSG4, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCommentaire.add(pnlCommentaireLong, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbLigneUnCommentaire ----
          lbLigneUnCommentaire.setText("Ligne 1");
          lbLigneUnCommentaire.setName("lbLigneUnCommentaire");
          pnlCommentaire.add(lbLigneUnCommentaire, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1LB12 ----
          A1LB12.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1LB12.setName("A1LB12");
          pnlCommentaire.add(A1LB12, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbLigneDeuxCommentaire ----
          lbLigneDeuxCommentaire.setText("Ligne 2");
          lbLigneDeuxCommentaire.setName("lbLigneDeuxCommentaire");
          pnlCommentaire.add(lbLigneDeuxCommentaire, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1LB34 ----
          A1LB34.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1LB34.setName("A1LB34");
          pnlCommentaire.add(A1LB34, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCommentaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlInformationGenerale ========
        {
          pnlInformationGenerale.setTitre("Informations g\u00e9n\u00e9rales");
          pnlInformationGenerale.setName("pnlInformationGenerale");
          pnlInformationGenerale.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlInformationGenerale.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlInformationGenerale.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlInformationGenerale.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlInformationGenerale.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbSaisiePlage ----
          lbSaisiePlage.setText("Saisie pleine plage");
          lbSaisiePlage.setName("lbSaisiePlage");
          pnlInformationGenerale.add(lbSaisiePlage, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1IN1 ----
          A1IN1.setModel(new DefaultComboBoxModel(new String[] { "Aucune", "30 caract\u00e8res", "60 caract\u00e8res" }));
          A1IN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1IN1.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1IN1.setPreferredSize(new Dimension(150, 30));
          A1IN1.setMinimumSize(new Dimension(150, 30));
          A1IN1.setMaximumSize(new Dimension(150, 30));
          A1IN1.setName("A1IN1");
          pnlInformationGenerale.add(A1IN1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbType ----
          lbType.setText("Type");
          lbType.setName("lbType");
          pnlInformationGenerale.add(lbType, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1IN3 ----
          A1IN3.setModel(new DefaultComboBoxModel(new String[] { "Pas de restriction", "Vente seulement", "Achat seulement" }));
          A1IN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1IN3.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1IN3.setPreferredSize(new Dimension(150, 30));
          A1IN3.setMinimumSize(new Dimension(150, 30));
          A1IN3.setMaximumSize(new Dimension(150, 30));
          A1IN3.setName("A1IN3");
          pnlInformationGenerale.add(A1IN3, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMotsDeClassement1 ----
          lbMotsDeClassement1.setText("Mots de classement 1");
          lbMotsDeClassement1.setName("lbMotsDeClassement1");
          pnlInformationGenerale.add(lbMotsDeClassement1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1CL1 ----
          A1CL1.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1CL1.setPreferredSize(new Dimension(260, 30));
          A1CL1.setMinimumSize(new Dimension(260, 30));
          A1CL1.setMaximumSize(new Dimension(260, 30));
          A1CL1.setName("A1CL1");
          pnlInformationGenerale.add(A1CL1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMotsDeClassement2 ----
          lbMotsDeClassement2.setText("Mots de classement 2");
          lbMotsDeClassement2.setName("lbMotsDeClassement2");
          pnlInformationGenerale.add(lbMotsDeClassement2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1CL2 ----
          A1CL2.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1CL2.setPreferredSize(new Dimension(260, 30));
          A1CL2.setMinimumSize(new Dimension(260, 30));
          A1CL2.setMaximumSize(new Dimension(260, 30));
          A1CL2.setName("A1CL2");
          pnlInformationGenerale.add(A1CL2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlInformationGenerale, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbEtablissementEnCours ----
          lbEtablissementEnCours.setText("Etablissement");
          lbEtablissementEnCours.setName("lbEtablissementEnCours");
          pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setEnabled(false);
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlOptionEdition ========
        {
          pnlOptionEdition.setTitre("Options d'\u00e9ditions");
          pnlOptionEdition.setName("pnlOptionEdition");
          pnlOptionEdition.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlOptionEdition.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlOptionEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlOptionEdition.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlOptionEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- A1EDT1_OBJ_75_34 ----
          A1EDT1_OBJ_75_34.setText("Aucune \u00e9dition ");
          A1EDT1_OBJ_75_34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1EDT1_OBJ_75_34.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1EDT1_OBJ_75_34.setMinimumSize(new Dimension(253, 30));
          A1EDT1_OBJ_75_34.setMaximumSize(new Dimension(253, 30));
          A1EDT1_OBJ_75_34.setPreferredSize(new Dimension(253, 30));
          A1EDT1_OBJ_75_34.setName("A1EDT1_OBJ_75_34");
          A1EDT1_OBJ_75_34.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              aucuneEditionActionPerformed(e);
            }
          });
          pnlOptionEdition.add(A1EDT1_OBJ_75_34, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- A1EDT1 ----
          A1EDT1.setText("Bons de pr\u00e9paration");
          A1EDT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1EDT1.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1EDT1.setMinimumSize(new Dimension(253, 30));
          A1EDT1.setMaximumSize(new Dimension(253, 30));
          A1EDT1.setPreferredSize(new Dimension(253, 30));
          A1EDT1.setName("A1EDT1");
          A1EDT1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              selectionAucuneEditionActionPerformed(e);
            }
          });
          pnlOptionEdition.add(A1EDT1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- A1EDT3 ----
          A1EDT3.setText("Bons d'exp\u00e9dition");
          A1EDT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1EDT3.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1EDT3.setMinimumSize(new Dimension(253, 30));
          A1EDT3.setMaximumSize(new Dimension(253, 30));
          A1EDT3.setPreferredSize(new Dimension(253, 30));
          A1EDT3.setName("A1EDT3");
          A1EDT3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              selectionAucuneEditionActionPerformed(e);
            }
          });
          pnlOptionEdition.add(A1EDT3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- A1EDT4 ----
          A1EDT4.setText("Bons transporteurs");
          A1EDT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1EDT4.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1EDT4.setMinimumSize(new Dimension(253, 30));
          A1EDT4.setMaximumSize(new Dimension(253, 30));
          A1EDT4.setPreferredSize(new Dimension(253, 30));
          A1EDT4.setName("A1EDT4");
          A1EDT4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              selectionAucuneEditionActionPerformed(e);
            }
          });
          pnlOptionEdition.add(A1EDT4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- A1EDT2 ----
          A1EDT2.setText("Accus\u00e9s de r\u00e9ception de commande");
          A1EDT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1EDT2.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1EDT2.setMinimumSize(new Dimension(253, 30));
          A1EDT2.setMaximumSize(new Dimension(253, 30));
          A1EDT2.setPreferredSize(new Dimension(253, 30));
          A1EDT2.setName("A1EDT2");
          A1EDT2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              selectionAucuneEditionActionPerformed(e);
            }
          });
          pnlOptionEdition.add(A1EDT2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- A1EDT5 ----
          A1EDT5.setText("Factures");
          A1EDT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1EDT5.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1EDT5.setMinimumSize(new Dimension(253, 30));
          A1EDT5.setMaximumSize(new Dimension(253, 30));
          A1EDT5.setPreferredSize(new Dimension(253, 30));
          A1EDT5.setName("A1EDT5");
          A1EDT5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              selectionAucuneEditionActionPerformed(e);
            }
          });
          pnlOptionEdition.add(A1EDT5, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- A1EDT6 ----
          A1EDT6.setText("Devis");
          A1EDT6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1EDT6.setFont(new Font("sansserif", Font.PLAIN, 14));
          A1EDT6.setMinimumSize(new Dimension(253, 30));
          A1EDT6.setMaximumSize(new Dimension(253, 30));
          A1EDT6.setPreferredSize(new Dimension(253, 30));
          A1EDT6.setName("A1EDT6");
          A1EDT6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              selectionAucuneEditionActionPerformed(e);
            }
          });
          pnlOptionEdition.add(A1EDT6, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlOptionEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCommentaire;
  private SNPanel pnlCommentaireLong;
  private SNLabelChamp lbLigneUn;
  private XRiTextField A1LIB;
  private XRiCheckBox A1DSG1;
  private SNLabelChamp lbLigneDeux;
  private XRiTextField A1LB1;
  private XRiCheckBox A1DSG2;
  private SNLabelChamp lbLigneTrois;
  private XRiTextField A1LB2;
  private XRiCheckBox A1DSG3;
  private SNLabelChamp lbLigneQuatre;
  private XRiTextField A1LB3;
  private XRiCheckBox A1DSG4;
  private SNLabelChamp lbLigneUnCommentaire;
  private XRiTextField A1LB12;
  private SNLabelChamp lbLigneDeuxCommentaire;
  private XRiTextField A1LB34;
  private SNPanelTitre pnlInformationGenerale;
  private SNLabelChamp lbSaisiePlage;
  private XRiComboBox A1IN1;
  private SNLabelChamp lbType;
  private XRiComboBox A1IN3;
  private SNLabelChamp lbMotsDeClassement1;
  private XRiTextField A1CL1;
  private SNLabelChamp lbMotsDeClassement2;
  private XRiTextField A1CL2;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNPanelTitre pnlOptionEdition;
  private JCheckBox A1EDT1_OBJ_75_34;
  private XRiCheckBox A1EDT1;
  private XRiCheckBox A1EDT3;
  private XRiCheckBox A1EDT4;
  private XRiCheckBox A1EDT2;
  private XRiCheckBox A1EDT5;
  private XRiCheckBox A1EDT6;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
