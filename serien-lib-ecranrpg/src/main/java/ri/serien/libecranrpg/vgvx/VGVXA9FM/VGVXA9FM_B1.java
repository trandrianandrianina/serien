
package ri.serien.libecranrpg.vgvx.VGVXA9FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Emmanuel MARCQ
 */
public class VGVXA9FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXA9FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    VALMIS.setValeursSelection("OUI", "NON");
    ANNMIS.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDNPA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDNPA@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    MES2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MES2@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    OBJ_83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBFRS@")).trim());
    LIBCLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCLI@")).trim());
    MES1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MES1@")).trim());
    WNPART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPART@")).trim());
    WNPNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPNUM@")).trim());
    VTENUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VTENUM@")).trim());
    TPAD1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPAD1@")).trim());
    TPAD2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPAD2@")).trim());
    TPAD3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPAD3@")).trim());
    TPAD4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPAD4@")).trim());
    TPAA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPAA1@")).trim());
    TPAA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPAA2@")).trim());
    TPAA3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPAA2@")).trim());
    TPAA4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPAA3@")).trim());
    WNPNLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPNLI@")).trim());
    VTENLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VTENLI@")).trim());
    TPMAGD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPMAGD@")).trim());
    TPMAGA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPMAGA@")).trim());
    VTESUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VTESUF@")).trim());
    WNPSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPSUF@")).trim());
    TPUSRE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPUSRE@")).trim());
    WMVTE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMVTE@")).trim());
    TPUSRT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPUSRT@")).trim());
    WMVTT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMVTT@")).trim());
    TPUSRS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TPUSRS@")).trim());
    WMVTS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMVTS@")).trim());
    WNPQTE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPQTE@")).trim());
    WHAQST.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WHAQST@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    riSousMenu8.setVisible(lexique.getMode() == Lexical.MODE_CONSULTATION);
    riSousMenu9.setVisible(lexique.isTrue("(70) AND (71)"));
    riSousMenu10.setVisible(lexique.isTrue("(70) AND (71)"));
    
    if (lexique.isTrue("64")) {
      OBJ_38.setText("Affectation cariste");
    }
    else if (lexique.isTrue("65")) {
      OBJ_38.setText("Validation mission");
    }
    else if (lexique.isTrue("66")) {
      OBJ_38.setText("Validation situation");
    }
    else {
      OBJ_38.setText("");
    }
    
    OBJ_85.setVisible(lexique.isTrue("80"));
    VTENUM.setVisible(lexique.isTrue("80"));
    OBJ_87.setVisible(lexique.isTrue("80"));
    VTESUF.setVisible(lexique.isTrue("80"));
    VTENLI.setVisible(lexique.isTrue("80"));
    LIBCLI.setVisible(lexique.isTrue("80"));
    
    OBJ_97.setVisible(lexique.isTrue("81"));
    TPUSRE.setVisible(lexique.isTrue("81"));
    WMVTE.setVisible(lexique.isTrue("81"));
    
    OBJ_101.setVisible(lexique.isTrue("82"));
    TPUSRT.setVisible(lexique.isTrue("82"));
    WMVTT.setVisible(lexique.isTrue("82"));
    
    OBJ_105.setVisible(lexique.isTrue("83"));
    TPUSRS.setVisible(lexique.isTrue("83"));
    WMVTS.setVisible(lexique.isTrue("83"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  FICHE PALETTE"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (VALMIS.isSelected()) {
      lexique.HostFieldPutData("VALMIS", 0, "OUI");
    }
    else {
      lexique.HostFieldPutData("VALMIS", 0, "NON");
    }
    if (ANNMIS.isSelected()) {
      lexique.HostFieldPutData("ANNMIS", 0, "OUI");
    }
    else {
      lexique.HostFieldPutData("ANNMIS", 0, "NON");
    }
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "A");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_32 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_33 = new JLabel();
    INDNPA = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_38 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    MES2 = new RiZoneSortie();
    A1LIB = new RiZoneSortie();
    OBJ_83 = new RiZoneSortie();
    LIBCLI = new RiZoneSortie();
    MES1 = new RiZoneSortie();
    ANNMIS = new XRiCheckBox();
    WNPART = new RiZoneSortie();
    VALMIS = new XRiCheckBox();
    USRAFF = new XRiTextField();
    WNPNUM = new RiZoneSortie();
    VTENUM = new RiZoneSortie();
    OBJ_58 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_85 = new JLabel();
    TPAD1 = new RiZoneSortie();
    TPAD2 = new RiZoneSortie();
    TPAD3 = new RiZoneSortie();
    TPAD4 = new RiZoneSortie();
    TPAA1 = new RiZoneSortie();
    TPAA2 = new RiZoneSortie();
    TPAA3 = new RiZoneSortie();
    TPAA4 = new RiZoneSortie();
    WNPNLI = new RiZoneSortie();
    VTENLI = new RiZoneSortie();
    OBJ_65 = new JLabel();
    TPMAGD = new RiZoneSortie();
    TPMAGA = new RiZoneSortie();
    TPPTY = new XRiTextField();
    VTESUF = new RiZoneSortie();
    WNPSUF = new RiZoneSortie();
    OBJ_60 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_87 = new JLabel();
    panel2 = new JPanel();
    OBJ_92 = new JLabel();
    OBJ_95 = new JLabel();
    TPUSRE = new RiZoneSortie();
    WMVTE = new RiZoneSortie();
    TPUSRT = new RiZoneSortie();
    WMVTT = new RiZoneSortie();
    TPUSRS = new RiZoneSortie();
    WMVTS = new RiZoneSortie();
    WNPQTE = new RiZoneSortie();
    WHAQST = new RiZoneSortie();
    OBJ_97 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_105 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Fiche palette");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_32 ----
          OBJ_32.setText("Etablissement");
          OBJ_32.setName("OBJ_32");
          p_tete_gauche.add(OBJ_32);
          OBJ_32.setBounds(5, 0, 95, 28);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(125, 2, 40, INDETB.getPreferredSize().height);

          //---- OBJ_33 ----
          OBJ_33.setText("Palette");
          OBJ_33.setName("OBJ_33");
          p_tete_gauche.add(OBJ_33);
          OBJ_33.setBounds(210, 0, 65, 28);

          //---- INDNPA ----
          INDNPA.setComponentPopupMenu(BTD);
          INDNPA.setText("@INDNPA@");
          INDNPA.setName("INDNPA");
          p_tete_gauche.add(INDNPA);
          INDNPA.setBounds(280, 2, 74, INDNPA.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_38 ----
          OBJ_38.setText("Etat");
          OBJ_38.setFont(OBJ_38.getFont().deriveFont(OBJ_38.getFont().getStyle() | Font.BOLD, OBJ_38.getFont().getSize() + 3f));
          OBJ_38.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_38.setName("OBJ_38");
          p_tete_droite.add(OBJ_38);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Affectation mission");
              riSousMenu_bt6.setToolTipText("Affectation mission");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Validation mission");
              riSousMenu_bt7.setToolTipText("Validation mission");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Options article");
              riSousMenu_bt8.setToolTipText("Options article");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Affichage des lots");
              riSousMenu_bt9.setToolTipText("Affichage des lots");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Informations sur les lots");
              riSousMenu_bt10.setToolTipText("Informations sur les lots");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(770, 530));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Transfert"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- MES2 ----
            MES2.setText("@MES2@");
            MES2.setName("MES2");
            panel1.add(MES2);
            MES2.setBounds(375, 180, 310, MES2.getPreferredSize().height);

            //---- A1LIB ----
            A1LIB.setText("@A1LIB@");
            A1LIB.setName("A1LIB");
            panel1.add(A1LIB);
            A1LIB.setBounds(310, 230, 310, 24);

            //---- OBJ_83 ----
            OBJ_83.setText("@LIBFRS@");
            OBJ_83.setName("OBJ_83");
            panel1.add(OBJ_83);
            OBJ_83.setBounds(230, 285, 390, 24);

            //---- LIBCLI ----
            LIBCLI.setText("@LIBCLI@");
            LIBCLI.setName("LIBCLI");
            panel1.add(LIBCLI);
            LIBCLI.setBounds(230, 315, 390, 24);

            //---- MES1 ----
            MES1.setText("@MES1@");
            MES1.setName("MES1");
            panel1.add(MES1);
            MES1.setBounds(25, 180, 310, MES1.getPreferredSize().height);

            //---- ANNMIS ----
            ANNMIS.setText("Annulation mission");
            ANNMIS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ANNMIS.setName("ANNMIS");
            panel1.add(ANNMIS);
            ANNMIS.setBounds(409, 32, 170, 24);

            //---- WNPART ----
            WNPART.setText("@WNPART@");
            WNPART.setName("WNPART");
            panel1.add(WNPART);
            WNPART.setBounds(85, 230, 210, 24);

            //---- VALMIS ----
            VALMIS.setText("Validation mission");
            VALMIS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VALMIS.setName("VALMIS");
            panel1.add(VALMIS);
            VALMIS.setBounds(409, 32, 171, 24);

            //---- USRAFF ----
            USRAFF.setComponentPopupMenu(BTD);
            USRAFF.setName("USRAFF");
            panel1.add(USRAFF);
            USRAFF.setBounds(95, 63, 110, USRAFF.getPreferredSize().height);

            //---- WNPNUM ----
            WNPNUM.setText("@WNPNUM@");
            WNPNUM.setName("WNPNUM");
            panel1.add(WNPNUM);
            WNPNUM.setBounds(85, 285, 57, 24);

            //---- VTENUM ----
            VTENUM.setText("@VTENUM@");
            VTENUM.setName("VTENUM");
            panel1.add(VTENUM);
            VTENUM.setBounds(85, 315, 57, 24);

            //---- OBJ_58 ----
            OBJ_58.setText("Magasin");
            OBJ_58.setName("OBJ_58");
            panel1.add(OBJ_58);
            OBJ_58.setBounds(25, 125, 55, 24);

            //---- OBJ_66 ----
            OBJ_66.setText("Magasin");
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(409, 125, 55, 24);

            //---- OBJ_54 ----
            OBJ_54.setText("Cariste");
            OBJ_54.setName("OBJ_54");
            panel1.add(OBJ_54);
            OBJ_54.setBounds(25, 65, 54, 24);

            //---- OBJ_50 ----
            OBJ_50.setText("Priorit\u00e9");
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(25, 32, 45, 24);

            //---- OBJ_79 ----
            OBJ_79.setText("Entr\u00e9e");
            OBJ_79.setName("OBJ_79");
            panel1.add(OBJ_79);
            OBJ_79.setBounds(25, 285, 42, 24);

            //---- OBJ_76 ----
            OBJ_76.setText("Article");
            OBJ_76.setName("OBJ_76");
            panel1.add(OBJ_76);
            OBJ_76.setBounds(25, 230, 40, 24);

            //---- OBJ_85 ----
            OBJ_85.setText("Vente");
            OBJ_85.setName("OBJ_85");
            panel1.add(OBJ_85);
            OBJ_85.setBounds(25, 315, 38, 24);

            //---- TPAD1 ----
            TPAD1.setText("@TPAD1@");
            TPAD1.setName("TPAD1");
            panel1.add(TPAD1);
            TPAD1.setBounds(147, 125, 34, 24);

            //---- TPAD2 ----
            TPAD2.setText("@TPAD2@");
            TPAD2.setName("TPAD2");
            panel1.add(TPAD2);
            TPAD2.setBounds(185, 125, 34, 24);

            //---- TPAD3 ----
            TPAD3.setText("@TPAD3@");
            TPAD3.setName("TPAD3");
            panel1.add(TPAD3);
            TPAD3.setBounds(223, 125, 34, 24);

            //---- TPAD4 ----
            TPAD4.setText("@TPAD4@");
            TPAD4.setName("TPAD4");
            panel1.add(TPAD4);
            TPAD4.setBounds(261, 125, 34, 24);

            //---- TPAA1 ----
            TPAA1.setText("@TPAA1@");
            TPAA1.setName("TPAA1");
            panel1.add(TPAA1);
            TPAA1.setBounds(531, 125, 34, 24);

            //---- TPAA2 ----
            TPAA2.setText("@TPAA2@");
            TPAA2.setName("TPAA2");
            panel1.add(TPAA2);
            TPAA2.setBounds(571, 125, 34, 24);

            //---- TPAA3 ----
            TPAA3.setText("@TPAA2@");
            TPAA3.setName("TPAA3");
            panel1.add(TPAA3);
            TPAA3.setBounds(611, 125, 34, 24);

            //---- TPAA4 ----
            TPAA4.setText("@TPAA3@");
            TPAA4.setName("TPAA4");
            panel1.add(TPAA4);
            TPAA4.setBounds(651, 125, 34, 24);

            //---- WNPNLI ----
            WNPNLI.setText("@WNPNLI@");
            WNPNLI.setName("WNPNLI");
            panel1.add(WNPNLI);
            WNPNLI.setBounds(190, 285, 34, 24);

            //---- VTENLI ----
            VTENLI.setText("@VTENLI@");
            VTENLI.setName("VTENLI");
            panel1.add(VTENLI);
            VTENLI.setBounds(190, 315, 34, 24);

            //---- OBJ_65 ----
            OBJ_65.setText("vers");
            OBJ_65.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_65.setFont(OBJ_65.getFont().deriveFont(OBJ_65.getFont().getStyle() | Font.BOLD));
            OBJ_65.setName("OBJ_65");
            panel1.add(OBJ_65);
            OBJ_65.setBounds(327, 125, 50, 24);

            //---- TPMAGD ----
            TPMAGD.setText("@TPMAGD@");
            TPMAGD.setName("TPMAGD");
            panel1.add(TPMAGD);
            TPMAGD.setBounds(95, 125, 29, TPMAGD.getPreferredSize().height);

            //---- TPMAGA ----
            TPMAGA.setText("@TPMAGA@");
            TPMAGA.setName("TPMAGA");
            panel1.add(TPMAGA);
            TPMAGA.setBounds(475, 125, 29, 24);

            //---- TPPTY ----
            TPPTY.setComponentPopupMenu(BTD);
            TPPTY.setName("TPPTY");
            panel1.add(TPPTY);
            TPPTY.setBounds(95, 30, 28, TPPTY.getPreferredSize().height);

            //---- VTESUF ----
            VTESUF.setText("@VTESUF@");
            VTESUF.setName("VTESUF");
            panel1.add(VTESUF);
            VTESUF.setBounds(165, 315, 21, 24);

            //---- WNPSUF ----
            WNPSUF.setText("@WNPSUF@");
            WNPSUF.setName("WNPSUF");
            panel1.add(WNPSUF);
            WNPSUF.setBounds(165, 285, 18, 24);

            //---- OBJ_60 ----
            OBJ_60.setText("/");
            OBJ_60.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_60.setName("OBJ_60");
            panel1.add(OBJ_60);
            OBJ_60.setBounds(128, 125, 15, 24);

            //---- OBJ_68 ----
            OBJ_68.setText("/");
            OBJ_68.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_68.setName("OBJ_68");
            panel1.add(OBJ_68);
            OBJ_68.setBounds(510, 125, 15, 24);

            //---- OBJ_81 ----
            OBJ_81.setText("/");
            OBJ_81.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_81.setName("OBJ_81");
            panel1.add(OBJ_81);
            OBJ_81.setBounds(145, 285, 14, 24);

            //---- OBJ_87 ----
            OBJ_87.setText("/");
            OBJ_87.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_87.setName("OBJ_87");
            panel1.add(OBJ_87);
            OBJ_87.setBounds(145, 315, 14, 24);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_92 ----
              OBJ_92.setText("Quantit\u00e9 palette");
              OBJ_92.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_92.setFont(OBJ_92.getFont().deriveFont(OBJ_92.getFont().getStyle() & ~Font.BOLD));
              OBJ_92.setName("OBJ_92");
              panel2.add(OBJ_92);
              OBJ_92.setBounds(120, 25, 97, 24);

              //---- OBJ_95 ----
              OBJ_95.setText("Quantit\u00e9 stock");
              OBJ_95.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_95.setFont(OBJ_95.getFont().deriveFont(OBJ_95.getFont().getStyle() & ~Font.BOLD));
              OBJ_95.setName("OBJ_95");
              panel2.add(OBJ_95);
              OBJ_95.setBounds(15, 25, 97, 24);

              //---- TPUSRE ----
              TPUSRE.setText("@TPUSRE@");
              TPUSRE.setName("TPUSRE");
              panel2.add(TPUSRE);
              TPUSRE.setBounds(335, 20, 101, 24);

              //---- WMVTE ----
              WMVTE.setText("@WMVTE@");
              WMVTE.setHorizontalAlignment(SwingConstants.RIGHT);
              WMVTE.setName("WMVTE");
              panel2.add(WMVTE);
              WMVTE.setBounds(455, 20, 101, 24);

              //---- TPUSRT ----
              TPUSRT.setText("@TPUSRT@");
              TPUSRT.setName("TPUSRT");
              panel2.add(TPUSRT);
              TPUSRT.setBounds(335, 50, 101, 24);

              //---- WMVTT ----
              WMVTT.setText("@WMVTT@");
              WMVTT.setHorizontalAlignment(SwingConstants.RIGHT);
              WMVTT.setName("WMVTT");
              panel2.add(WMVTT);
              WMVTT.setBounds(455, 50, 101, 24);

              //---- TPUSRS ----
              TPUSRS.setText("@TPUSRS@");
              TPUSRS.setName("TPUSRS");
              panel2.add(TPUSRS);
              TPUSRS.setBounds(335, 80, 101, 24);

              //---- WMVTS ----
              WMVTS.setText("@WMVTS@");
              WMVTS.setHorizontalAlignment(SwingConstants.RIGHT);
              WMVTS.setName("WMVTS");
              panel2.add(WMVTS);
              WMVTS.setBounds(455, 80, 101, 24);

              //---- WNPQTE ----
              WNPQTE.setText("@WNPQTE@");
              WNPQTE.setHorizontalAlignment(SwingConstants.RIGHT);
              WNPQTE.setFont(WNPQTE.getFont().deriveFont(WNPQTE.getFont().getStyle() & ~Font.BOLD));
              WNPQTE.setName("WNPQTE");
              panel2.add(WNPQTE);
              WNPQTE.setBounds(120, 50, 97, 24);

              //---- WHAQST ----
              WHAQST.setText("@WHAQST@");
              WHAQST.setHorizontalAlignment(SwingConstants.RIGHT);
              WHAQST.setFont(WHAQST.getFont().deriveFont(WHAQST.getFont().getStyle() & ~Font.BOLD));
              WHAQST.setName("WHAQST");
              panel2.add(WHAQST);
              WHAQST.setBounds(15, 50, 97, 24);

              //---- OBJ_97 ----
              OBJ_97.setText("Entr\u00e9e");
              OBJ_97.setName("OBJ_97");
              panel2.add(OBJ_97);
              OBJ_97.setBounds(255, 20, 64, 24);

              //---- OBJ_101 ----
              OBJ_101.setText("R\u00e9appro");
              OBJ_101.setName("OBJ_101");
              panel2.add(OBJ_101);
              OBJ_101.setBounds(255, 50, 64, 24);

              //---- OBJ_105 ----
              OBJ_105.setText("Vente");
              OBJ_105.setName("OBJ_105");
              panel2.add(OBJ_105);
              OBJ_105.setBounds(255, 80, 64, 24);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(65, 350, 560, 120);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 744, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_32;
  private RiZoneSortie INDETB;
  private JLabel OBJ_33;
  private RiZoneSortie INDNPA;
  private JPanel p_tete_droite;
  private JLabel OBJ_38;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie MES2;
  private RiZoneSortie A1LIB;
  private RiZoneSortie OBJ_83;
  private RiZoneSortie LIBCLI;
  private RiZoneSortie MES1;
  private XRiCheckBox ANNMIS;
  private RiZoneSortie WNPART;
  private XRiCheckBox VALMIS;
  private XRiTextField USRAFF;
  private RiZoneSortie WNPNUM;
  private RiZoneSortie VTENUM;
  private JLabel OBJ_58;
  private JLabel OBJ_66;
  private JLabel OBJ_54;
  private JLabel OBJ_50;
  private JLabel OBJ_79;
  private JLabel OBJ_76;
  private JLabel OBJ_85;
  private RiZoneSortie TPAD1;
  private RiZoneSortie TPAD2;
  private RiZoneSortie TPAD3;
  private RiZoneSortie TPAD4;
  private RiZoneSortie TPAA1;
  private RiZoneSortie TPAA2;
  private RiZoneSortie TPAA3;
  private RiZoneSortie TPAA4;
  private RiZoneSortie WNPNLI;
  private RiZoneSortie VTENLI;
  private JLabel OBJ_65;
  private RiZoneSortie TPMAGD;
  private RiZoneSortie TPMAGA;
  private XRiTextField TPPTY;
  private RiZoneSortie VTESUF;
  private RiZoneSortie WNPSUF;
  private JLabel OBJ_60;
  private JLabel OBJ_68;
  private JLabel OBJ_81;
  private JLabel OBJ_87;
  private JPanel panel2;
  private JLabel OBJ_92;
  private JLabel OBJ_95;
  private RiZoneSortie TPUSRE;
  private RiZoneSortie WMVTE;
  private RiZoneSortie TPUSRT;
  private RiZoneSortie WMVTT;
  private RiZoneSortie TPUSRS;
  private RiZoneSortie WMVTS;
  private RiZoneSortie WNPQTE;
  private RiZoneSortie WHAQST;
  private JLabel OBJ_97;
  private JLabel OBJ_101;
  private JLabel OBJ_105;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
