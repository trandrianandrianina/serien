
package ri.serien.libecranrpg.vgvx.VGVXCCFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVXCCFM_C7 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXCCFM_C7(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT1@")).trim());
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT2@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT3@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT4@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    CVNQPF.setEnabled(lexique.isPresent("CVNQPF"));
    CVNQPD.setEnabled(lexique.isPresent("CVNQPD"));
    CVNQAF.setEnabled(lexique.isPresent("CVNQAF"));
    CVNQAD.setEnabled(lexique.isPresent("CVNQAD"));
    CVNBED.setEnabled(lexique.isPresent("CVNBED"));
    CVCVCF.setEnabled(lexique.isPresent("CVCVCF"));
    CVCVCD.setEnabled(lexique.isPresent("CVCVCD"));
    CVCVAF.setEnabled(lexique.isPresent("CVCVAF"));
    CVCVAD.setEnabled(lexique.isPresent("CVCVAD"));
    CVNBEF.setEnabled(lexique.isPresent("CVNBEF"));
    CVZP5.setEnabled(lexique.isPresent("CVZP5"));
    CVZP4.setEnabled(lexique.isPresent("CVZP4"));
    CVZP3.setEnabled(lexique.isPresent("CVZP3"));
    CVZP2.setEnabled(lexique.isPresent("CVZP2"));
    CVZP1.setEnabled(lexique.isPresent("CVZP1"));
    OBJ_27.setVisible(lexique.isPresent("TIT5"));
    OBJ_26.setVisible(lexique.isPresent("TIT4"));
    OBJ_25.setVisible(lexique.isPresent("TIT3"));
    OBJ_24.setVisible(lexique.isPresent("TIT2"));
    OBJ_23.setVisible(lexique.isPresent("TIT1"));
    // CVDCFX.setEnabled( lexique.isPresent("CVDCFX"));
    // CVDCDX.setEnabled( lexique.isPresent("CVDCDX"));
    // CVDAFX.setEnabled( lexique.isPresent("CVDAFX"));
    // CVDADX.setEnabled( lexique.isPresent("CVDADX"));
    // CVPAFX.setEnabled( lexique.isPresent("CVPAFX"));
    // CVPADX.setEnabled( lexique.isPresent("CVPADX"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Critères de sélection"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_ann = new RiSousMenu();
    riSousMenu_bt_ann = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_28 = new JLabel();
    OBJ_35 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_62 = new JLabel();
    OBJ_42 = new JLabel();
    CVPADX = new XRiCalendrier();
    CVPAFX = new XRiCalendrier();
    CVDADX = new XRiCalendrier();
    CVDAFX = new XRiCalendrier();
    CVDCDX = new XRiCalendrier();
    CVDCFX = new XRiCalendrier();
    OBJ_58 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_23 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_27 = new JLabel();
    CVZP1 = new XRiTextField();
    CVZP2 = new XRiTextField();
    CVZP3 = new XRiTextField();
    CVZP4 = new XRiTextField();
    CVZP5 = new XRiTextField();
    CVNBEF = new XRiTextField();
    CVCVAD = new XRiTextField();
    CVCVAF = new XRiTextField();
    CVCVCD = new XRiTextField();
    CVCVCF = new XRiTextField();
    OBJ_40 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_56 = new JLabel();
    CVNBED = new XRiTextField();
    CVNQAD = new XRiTextField();
    CVNQAF = new XRiTextField();
    CVNQPD = new XRiTextField();
    CVNQPF = new XRiTextField();
    OBJ_37 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_64 = new JLabel();
    separator1 = compFactory.createSeparator("Validit\u00e9");
    separator2 = compFactory.createSeparator("Niveau de qualit\u00e9");
    separator3 = compFactory.createSeparator("Date de premier achat");
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 400));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_ann ========
          {
            riSousMenu_ann.setName("riSousMenu_ann");

            //---- riSousMenu_bt_ann ----
            riSousMenu_bt_ann.setText("Annulation");
            riSousMenu_bt_ann.setToolTipText("Annulation");
            riSousMenu_bt_ann.setName("riSousMenu_bt_ann");
            riSousMenu_ann.add(riSousMenu_bt_ann);
          }
          menus_haut.add(riSousMenu_ann);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setToolTipText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Vente par correspondance"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_28 ----
          OBJ_28.setText("Zones personnalis\u00e9es");
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(37, 45, 138, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("Nombre d'enfants");
          OBJ_35.setName("OBJ_35");
          p_recup.add(OBJ_35);
          OBJ_35.setBounds(37, 138, 109, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_62 ----
          OBJ_62.setText("Paiements");
          OBJ_62.setName("OBJ_62");
          p_recup.add(OBJ_62);
          OBJ_62.setBounds(339, 328, 67, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("Adresse");
          OBJ_42.setName("OBJ_42");
          p_recup.add(OBJ_42);
          OBJ_42.setBounds(37, 212, 54, 20);

          //---- CVPADX ----
          CVPADX.setComponentPopupMenu(BTD);
          CVPADX.setName("CVPADX");
          p_recup.add(CVPADX);
          CVPADX.setBounds(339, 134, 105, CVPADX.getPreferredSize().height);

          //---- CVPAFX ----
          CVPAFX.setComponentPopupMenu(BTD);
          CVPAFX.setName("CVPAFX");
          p_recup.add(CVPAFX);
          CVPAFX.setBounds(482, 134, 105, CVPAFX.getPreferredSize().height);

          //---- CVDADX ----
          CVDADX.setComponentPopupMenu(BTD);
          CVDADX.setName("CVDADX");
          p_recup.add(CVDADX);
          CVDADX.setBounds(339, 208, 105, CVDADX.getPreferredSize().height);

          //---- CVDAFX ----
          CVDAFX.setComponentPopupMenu(BTD);
          CVDAFX.setName("CVDAFX");
          p_recup.add(CVDAFX);
          CVDAFX.setBounds(482, 208, 105, CVDAFX.getPreferredSize().height);

          //---- CVDCDX ----
          CVDCDX.setComponentPopupMenu(BTD);
          CVDCDX.setName("CVDCDX");
          p_recup.add(CVDCDX);
          CVDCDX.setBounds(339, 244, 105, CVDCDX.getPreferredSize().height);

          //---- CVDCFX ----
          CVDCFX.setComponentPopupMenu(BTD);
          CVDCFX.setName("CVDCFX");
          p_recup.add(CVDCFX);
          CVDCFX.setBounds(482, 244, 105, CVDCFX.getPreferredSize().height);

          //---- OBJ_58 ----
          OBJ_58.setText("Achats");
          OBJ_58.setName("OBJ_58");
          p_recup.add(OBJ_58);
          OBJ_58.setBounds(37, 328, 45, 20);

          //---- OBJ_50 ----
          OBJ_50.setText("Client");
          OBJ_50.setName("OBJ_50");
          p_recup.add(OBJ_50);
          OBJ_50.setBounds(37, 248, 36, 20);

          //---- OBJ_46 ----
          OBJ_46.setText("Date");
          OBJ_46.setName("OBJ_46");
          p_recup.add(OBJ_46);
          OBJ_46.setBounds(306, 212, 33, 20);

          //---- OBJ_54 ----
          OBJ_54.setText("Date");
          OBJ_54.setName("OBJ_54");
          p_recup.add(OBJ_54);
          OBJ_54.setBounds(306, 248, 33, 20);

          //---- OBJ_23 ----
          OBJ_23.setText("@TIT1@");
          OBJ_23.setName("OBJ_23");
          p_recup.add(OBJ_23);
          OBJ_23.setBounds(199, 45, 24, 20);

          //---- OBJ_24 ----
          OBJ_24.setText("@TIT2@");
          OBJ_24.setName("OBJ_24");
          p_recup.add(OBJ_24);
          OBJ_24.setBounds(271, 45, 24, 20);

          //---- OBJ_25 ----
          OBJ_25.setText("@TIT3@");
          OBJ_25.setName("OBJ_25");
          p_recup.add(OBJ_25);
          OBJ_25.setBounds(344, 45, 24, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("@TIT4@");
          OBJ_26.setName("OBJ_26");
          p_recup.add(OBJ_26);
          OBJ_26.setBounds(412, 45, 24, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("@TIT5@");
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(482, 45, 24, 20);

          //---- CVZP1 ----
          CVZP1.setComponentPopupMenu(BTD);
          CVZP1.setName("CVZP1");
          p_recup.add(CVZP1);
          CVZP1.setBounds(199, 76, 30, CVZP1.getPreferredSize().height);

          //---- CVZP2 ----
          CVZP2.setComponentPopupMenu(BTD);
          CVZP2.setName("CVZP2");
          p_recup.add(CVZP2);
          CVZP2.setBounds(269, 76, 30, CVZP2.getPreferredSize().height);

          //---- CVZP3 ----
          CVZP3.setComponentPopupMenu(BTD);
          CVZP3.setName("CVZP3");
          p_recup.add(CVZP3);
          CVZP3.setBounds(339, 76, 30, CVZP3.getPreferredSize().height);

          //---- CVZP4 ----
          CVZP4.setComponentPopupMenu(BTD);
          CVZP4.setName("CVZP4");
          p_recup.add(CVZP4);
          CVZP4.setBounds(409, 76, 30, CVZP4.getPreferredSize().height);

          //---- CVZP5 ----
          CVZP5.setComponentPopupMenu(BTD);
          CVZP5.setName("CVZP5");
          p_recup.add(CVZP5);
          CVZP5.setBounds(479, 76, 30, CVZP5.getPreferredSize().height);

          //---- CVNBEF ----
          CVNBEF.setComponentPopupMenu(BTD);
          CVNBEF.setName("CVNBEF");
          p_recup.add(CVNBEF);
          CVNBEF.setBounds(269, 134, 26, CVNBEF.getPreferredSize().height);

          //---- CVCVAD ----
          CVCVAD.setComponentPopupMenu(BTD);
          CVCVAD.setName("CVCVAD");
          p_recup.add(CVCVAD);
          CVCVAD.setBounds(199, 208, 30, CVCVAD.getPreferredSize().height);

          //---- CVCVAF ----
          CVCVAF.setComponentPopupMenu(BTD);
          CVCVAF.setName("CVCVAF");
          p_recup.add(CVCVAF);
          CVCVAF.setBounds(269, 208, 30, CVCVAF.getPreferredSize().height);

          //---- CVCVCD ----
          CVCVCD.setComponentPopupMenu(BTD);
          CVCVCD.setName("CVCVCD");
          p_recup.add(CVCVCD);
          CVCVCD.setBounds(199, 244, 30, CVCVCD.getPreferredSize().height);

          //---- CVCVCF ----
          CVCVCF.setComponentPopupMenu(BTD);
          CVCVCF.setName("CVCVCF");
          p_recup.add(CVCVCF);
          CVCVCF.setBounds(269, 244, 30, CVCVCF.getPreferredSize().height);

          //---- OBJ_40 ----
          OBJ_40.setText("au");
          OBJ_40.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_40.setName("OBJ_40");
          p_recup.add(OBJ_40);
          OBJ_40.setBounds(445, 138, 35, 20);

          //---- OBJ_48 ----
          OBJ_48.setText("au");
          OBJ_48.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_48.setName("OBJ_48");
          p_recup.add(OBJ_48);
          OBJ_48.setBounds(445, 212, 35, 20);

          //---- OBJ_56 ----
          OBJ_56.setText("au");
          OBJ_56.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_56.setName("OBJ_56");
          p_recup.add(OBJ_56);
          OBJ_56.setBounds(445, 248, 35, 20);

          //---- CVNBED ----
          CVNBED.setComponentPopupMenu(BTD);
          CVNBED.setName("CVNBED");
          p_recup.add(CVNBED);
          CVNBED.setBounds(199, 134, 24, CVNBED.getPreferredSize().height);

          //---- CVNQAD ----
          CVNQAD.setComponentPopupMenu(BTD);
          CVNQAD.setName("CVNQAD");
          p_recup.add(CVNQAD);
          CVNQAD.setBounds(199, 324, 24, CVNQAD.getPreferredSize().height);

          //---- CVNQAF ----
          CVNQAF.setComponentPopupMenu(BTD);
          CVNQAF.setName("CVNQAF");
          p_recup.add(CVNQAF);
          CVNQAF.setBounds(269, 324, 24, CVNQAF.getPreferredSize().height);

          //---- CVNQPD ----
          CVNQPD.setComponentPopupMenu(BTD);
          CVNQPD.setName("CVNQPD");
          p_recup.add(CVNQPD);
          CVNQPD.setBounds(482, 324, 24, CVNQPD.getPreferredSize().height);

          //---- CVNQPF ----
          CVNQPF.setComponentPopupMenu(BTD);
          CVNQPF.setName("CVNQPF");
          p_recup.add(CVNQPF);
          CVNQPF.setBounds(562, 324, 24, CVNQPF.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("\u00e0");
          OBJ_37.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_37.setName("OBJ_37");
          p_recup.add(OBJ_37);
          OBJ_37.setBounds(230, 138, 35, 20);

          //---- OBJ_44 ----
          OBJ_44.setText("\u00e0");
          OBJ_44.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_44.setName("OBJ_44");
          p_recup.add(OBJ_44);
          OBJ_44.setBounds(230, 212, 35, 20);

          //---- OBJ_52 ----
          OBJ_52.setText("\u00e0");
          OBJ_52.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_52.setName("OBJ_52");
          p_recup.add(OBJ_52);
          OBJ_52.setBounds(230, 248, 35, 20);

          //---- OBJ_60 ----
          OBJ_60.setText("\u00e0");
          OBJ_60.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_60.setName("OBJ_60");
          p_recup.add(OBJ_60);
          OBJ_60.setBounds(230, 328, 35, 20);

          //---- OBJ_64 ----
          OBJ_64.setText("\u00e0");
          OBJ_64.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_64.setName("OBJ_64");
          p_recup.add(OBJ_64);
          OBJ_64.setBounds(513, 328, 42, 20);

          //---- separator1 ----
          separator1.setName("separator1");
          p_recup.add(separator1);
          separator1.setBounds(25, 180, 560, separator1.getPreferredSize().height);

          //---- separator2 ----
          separator2.setName("separator2");
          p_recup.add(separator2);
          separator2.setBounds(25, 290, 560, separator2.getPreferredSize().height);

          //---- separator3 ----
          separator3.setName("separator3");
          p_recup.add(separator3);
          separator3.setBounds(340, 115, 245, separator3.getPreferredSize().height);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(10, 10, 605, 380);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_ann;
  private RiSousMenu_bt riSousMenu_bt_ann;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_28;
  private JLabel OBJ_35;
  private JPanel P_PnlOpts;
  private JLabel OBJ_62;
  private JLabel OBJ_42;
  private XRiCalendrier CVPADX;
  private XRiCalendrier CVPAFX;
  private XRiCalendrier CVDADX;
  private XRiCalendrier CVDAFX;
  private XRiCalendrier CVDCDX;
  private XRiCalendrier CVDCFX;
  private JLabel OBJ_58;
  private JLabel OBJ_50;
  private JLabel OBJ_46;
  private JLabel OBJ_54;
  private JLabel OBJ_23;
  private JLabel OBJ_24;
  private JLabel OBJ_25;
  private JLabel OBJ_26;
  private JLabel OBJ_27;
  private XRiTextField CVZP1;
  private XRiTextField CVZP2;
  private XRiTextField CVZP3;
  private XRiTextField CVZP4;
  private XRiTextField CVZP5;
  private XRiTextField CVNBEF;
  private XRiTextField CVCVAD;
  private XRiTextField CVCVAF;
  private XRiTextField CVCVCD;
  private XRiTextField CVCVCF;
  private JLabel OBJ_40;
  private JLabel OBJ_48;
  private JLabel OBJ_56;
  private XRiTextField CVNBED;
  private XRiTextField CVNQAD;
  private XRiTextField CVNQAF;
  private XRiTextField CVNQPD;
  private XRiTextField CVNQPF;
  private JLabel OBJ_37;
  private JLabel OBJ_44;
  private JLabel OBJ_52;
  private JLabel OBJ_60;
  private JLabel OBJ_64;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
