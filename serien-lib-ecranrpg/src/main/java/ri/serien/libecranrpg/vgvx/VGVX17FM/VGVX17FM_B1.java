
package ri.serien.libecranrpg.vgvx.VGVX17FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX17FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX17FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNLIB@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_43.setVisible(lexique.isPresent("A1UNS"));
    P13DAX.setVisible(lexique.isPresent("P13DAX"));
    P13REX.setVisible(lexique.isPresent("P13REX"));
    P13ATX.setVisible(lexique.isPresent("P13ATX"));
    OBJ_44.setVisible(lexique.isPresent("UNLIB"));
    
    // TODO Icones
    label6.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    label7.setIcon(lexique.chargerImage("images/moins_petit.png", true));
    label8.setIcon(lexique.chargerImage("images/egal_petit.png", true));
    
    // Titre
    if (lexique.isTrue("91")) {
      setTitle(interpreteurD.analyseExpression("Prévisions de fabrications"));
    }
    else {
      setTitle(interpreteurD.analyseExpression("Prévisions de consommations"));
    }
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", true);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    OBJ_44 = new RiZoneSortie();
    OBJ_37 = new JLabel();
    P13STX = new XRiTextField();
    P13ATX = new XRiTextField();
    P13REX = new XRiTextField();
    P13DAX = new XRiTextField();
    OBJ_42 = new JLabel();
    WTOT = new XRiTextField();
    OBJ_31 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_43 = new RiZoneSortie();
    M01 = new XRiTextField();
    M02 = new XRiTextField();
    M03 = new XRiTextField();
    M04 = new XRiTextField();
    M05 = new XRiTextField();
    M06 = new XRiTextField();
    M07 = new XRiTextField();
    M08 = new XRiTextField();
    M09 = new XRiTextField();
    M10 = new XRiTextField();
    M11 = new XRiTextField();
    M12 = new XRiTextField();
    PM201 = new XRiTextField();
    PM202 = new XRiTextField();
    PM203 = new XRiTextField();
    PM204 = new XRiTextField();
    PM205 = new XRiTextField();
    PM206 = new XRiTextField();
    PM207 = new XRiTextField();
    PM208 = new XRiTextField();
    PM209 = new XRiTextField();
    PM210 = new XRiTextField();
    PM211 = new XRiTextField();
    PM212 = new XRiTextField();
    PM2011 = new XRiTextField();
    PM2012 = new XRiTextField();
    PM2013 = new XRiTextField();
    PM2021 = new XRiTextField();
    PM2031 = new XRiTextField();
    PM2041 = new XRiTextField();
    PM2051 = new XRiTextField();
    PM2061 = new XRiTextField();
    PM2071 = new XRiTextField();
    PM2081 = new XRiTextField();
    PM2091 = new XRiTextField();
    PM2101 = new XRiTextField();
    PM2111 = new XRiTextField();
    PM2121 = new XRiTextField();
    PM2022 = new XRiTextField();
    PM2032 = new XRiTextField();
    PM2042 = new XRiTextField();
    PM2052 = new XRiTextField();
    PM2062 = new XRiTextField();
    PM2072 = new XRiTextField();
    PM2082 = new XRiTextField();
    PM2092 = new XRiTextField();
    PM2102 = new XRiTextField();
    PM2112 = new XRiTextField();
    PM2122 = new XRiTextField();
    PM2023 = new XRiTextField();
    PM2033 = new XRiTextField();
    PM2043 = new XRiTextField();
    PM2053 = new XRiTextField();
    PM2063 = new XRiTextField();
    PM2073 = new XRiTextField();
    PM2083 = new XRiTextField();
    PM2093 = new XRiTextField();
    PM2103 = new XRiTextField();
    PM2113 = new XRiTextField();
    PM2123 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(910, 430));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("R\u00e9percussion  valeur");
              riSousMenu_bt6.setToolTipText("R\u00e9percussion d'une valeur sur zones sous jacentes");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("R\u00e9partition saisie");
              riSousMenu_bt7.setToolTipText("R\u00e9partition saisie des mois / d\u00e9cades");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel4 ========
        {
          panel4.setBorder(new TitledBorder(""));
          panel4.setOpaque(false);
          panel4.setName("panel4");

          //---- OBJ_44 ----
          OBJ_44.setText("@UNLIB@");
          OBJ_44.setName("OBJ_44");

          //---- OBJ_37 ----
          OBJ_37.setText("Commande client");
          OBJ_37.setName("OBJ_37");

          //---- P13STX ----
          P13STX.setHorizontalAlignment(SwingConstants.RIGHT);
          P13STX.setName("P13STX");

          //---- P13ATX ----
          P13ATX.setHorizontalAlignment(SwingConstants.RIGHT);
          P13ATX.setName("P13ATX");

          //---- P13REX ----
          P13REX.setHorizontalAlignment(SwingConstants.RIGHT);
          P13REX.setName("P13REX");

          //---- P13DAX ----
          P13DAX.setHorizontalAlignment(SwingConstants.RIGHT);
          P13DAX.setFont(P13DAX.getFont().deriveFont(P13DAX.getFont().getStyle() | Font.BOLD));
          P13DAX.setName("P13DAX");

          //---- OBJ_42 ----
          OBJ_42.setText("Unit\u00e9 de stock");
          OBJ_42.setName("OBJ_42");

          //---- WTOT ----
          WTOT.setFont(WTOT.getFont().deriveFont(WTOT.getFont().getStyle() | Font.BOLD));
          WTOT.setName("WTOT");

          //---- OBJ_31 ----
          OBJ_31.setText("En stock");
          OBJ_31.setName("OBJ_31");

          //---- OBJ_34 ----
          OBJ_34.setText("Attendu");
          OBJ_34.setName("OBJ_34");

          //---- OBJ_40 ----
          OBJ_40.setText("Position");
          OBJ_40.setFont(OBJ_40.getFont().deriveFont(OBJ_40.getFont().getStyle() | Font.BOLD));
          OBJ_40.setName("OBJ_40");

          //---- OBJ_49 ----
          OBJ_49.setText("Total");
          OBJ_49.setFont(OBJ_49.getFont().deriveFont(OBJ_49.getFont().getStyle() | Font.BOLD));
          OBJ_49.setName("OBJ_49");

          //---- OBJ_43 ----
          OBJ_43.setText("@A1UNS@");
          OBJ_43.setName("OBJ_43");

          //---- M01 ----
          M01.setFont(M01.getFont().deriveFont(M01.getFont().getStyle() | Font.BOLD));
          M01.setName("M01");

          //---- M02 ----
          M02.setFont(M02.getFont().deriveFont(M02.getFont().getStyle() | Font.BOLD));
          M02.setName("M02");

          //---- M03 ----
          M03.setFont(M03.getFont().deriveFont(M03.getFont().getStyle() | Font.BOLD));
          M03.setName("M03");

          //---- M04 ----
          M04.setFont(M04.getFont().deriveFont(M04.getFont().getStyle() | Font.BOLD));
          M04.setName("M04");

          //---- M05 ----
          M05.setFont(M05.getFont().deriveFont(M05.getFont().getStyle() | Font.BOLD));
          M05.setName("M05");

          //---- M06 ----
          M06.setFont(M06.getFont().deriveFont(M06.getFont().getStyle() | Font.BOLD));
          M06.setName("M06");

          //---- M07 ----
          M07.setFont(M07.getFont().deriveFont(M07.getFont().getStyle() | Font.BOLD));
          M07.setName("M07");

          //---- M08 ----
          M08.setFont(M08.getFont().deriveFont(M08.getFont().getStyle() | Font.BOLD));
          M08.setName("M08");

          //---- M09 ----
          M09.setFont(M09.getFont().deriveFont(M09.getFont().getStyle() | Font.BOLD));
          M09.setName("M09");

          //---- M10 ----
          M10.setFont(M10.getFont().deriveFont(M10.getFont().getStyle() | Font.BOLD));
          M10.setName("M10");

          //---- M11 ----
          M11.setFont(M11.getFont().deriveFont(M11.getFont().getStyle() | Font.BOLD));
          M11.setName("M11");

          //---- M12 ----
          M12.setFont(M12.getFont().deriveFont(M12.getFont().getStyle() | Font.BOLD));
          M12.setName("M12");

          //---- PM201 ----
          PM201.setName("PM201");

          //---- PM202 ----
          PM202.setName("PM202");

          //---- PM203 ----
          PM203.setName("PM203");

          //---- PM204 ----
          PM204.setName("PM204");

          //---- PM205 ----
          PM205.setName("PM205");

          //---- PM206 ----
          PM206.setName("PM206");

          //---- PM207 ----
          PM207.setName("PM207");

          //---- PM208 ----
          PM208.setName("PM208");

          //---- PM209 ----
          PM209.setName("PM209");

          //---- PM210 ----
          PM210.setName("PM210");

          //---- PM211 ----
          PM211.setName("PM211");

          //---- PM212 ----
          PM212.setName("PM212");

          //---- PM2011 ----
          PM2011.setName("PM2011");

          //---- PM2012 ----
          PM2012.setName("PM2012");

          //---- PM2013 ----
          PM2013.setName("PM2013");

          //---- PM2021 ----
          PM2021.setName("PM2021");

          //---- PM2031 ----
          PM2031.setName("PM2031");

          //---- PM2041 ----
          PM2041.setName("PM2041");

          //---- PM2051 ----
          PM2051.setName("PM2051");

          //---- PM2061 ----
          PM2061.setName("PM2061");

          //---- PM2071 ----
          PM2071.setName("PM2071");

          //---- PM2081 ----
          PM2081.setName("PM2081");

          //---- PM2091 ----
          PM2091.setName("PM2091");

          //---- PM2101 ----
          PM2101.setName("PM2101");

          //---- PM2111 ----
          PM2111.setName("PM2111");

          //---- PM2121 ----
          PM2121.setName("PM2121");

          //---- PM2022 ----
          PM2022.setName("PM2022");

          //---- PM2032 ----
          PM2032.setName("PM2032");

          //---- PM2042 ----
          PM2042.setName("PM2042");

          //---- PM2052 ----
          PM2052.setName("PM2052");

          //---- PM2062 ----
          PM2062.setName("PM2062");

          //---- PM2072 ----
          PM2072.setName("PM2072");

          //---- PM2082 ----
          PM2082.setName("PM2082");

          //---- PM2092 ----
          PM2092.setName("PM2092");

          //---- PM2102 ----
          PM2102.setName("PM2102");

          //---- PM2112 ----
          PM2112.setName("PM2112");

          //---- PM2122 ----
          PM2122.setName("PM2122");

          //---- PM2023 ----
          PM2023.setName("PM2023");

          //---- PM2033 ----
          PM2033.setName("PM2033");

          //---- PM2043 ----
          PM2043.setName("PM2043");

          //---- PM2053 ----
          PM2053.setName("PM2053");

          //---- PM2063 ----
          PM2063.setName("PM2063");

          //---- PM2073 ----
          PM2073.setName("PM2073");

          //---- PM2083 ----
          PM2083.setName("PM2083");

          //---- PM2093 ----
          PM2093.setName("PM2093");

          //---- PM2103 ----
          PM2103.setName("PM2103");

          //---- PM2113 ----
          PM2113.setName("PM2113");

          //---- PM2123 ----
          PM2123.setName("PM2123");

          //---- label1 ----
          label1.setText("Mois");
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");

          //---- label2 ----
          label2.setText("Conso.");
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");

          //---- label3 ----
          label3.setText("D\u00e9cade 1");
          label3.setHorizontalAlignment(SwingConstants.CENTER);
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");

          //---- label4 ----
          label4.setText("D\u00e9cade 2");
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setName("label4");

          //---- label5 ----
          label5.setText("D\u00e9cade 3");
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setName("label5");

          //---- label6 ----
          label6.setName("label6");

          //---- label7 ----
          label7.setName("label7");

          //---- label8 ----
          label8.setName("label8");

          GroupLayout panel4Layout = new GroupLayout(panel4);
          panel4.setLayout(panel4Layout);
          panel4Layout.setHorizontalGroup(
            panel4Layout.createParallelGroup()
              .addGroup(panel4Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(panel4Layout.createParallelGroup()
                  .addComponent(label6, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addComponent(label7, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addComponent(label8, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
                    .addGap(29, 29, 29)
                    .addComponent(P13STX, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                    .addGap(54, 54, 54)
                    .addComponent(P13ATX, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(115, 115, 115)
                    .addComponent(P13REX, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(P13DAX, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 191, GroupLayout.PREFERRED_SIZE)))
                .addGap(64, 64, 64)
                .addGroup(panel4Layout.createParallelGroup()
                  .addComponent(label1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(M04, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(M06, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(M02, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(M07, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(M12, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(M05, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(M11, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(M09, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(M03, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(M10, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(M08, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(M01, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel4Layout.createParallelGroup()
                  .addComponent(PM207, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM203, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(label2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM202, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM205, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM208, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM204, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM206, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM201, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM210, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM211, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM212, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM209, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel4Layout.createParallelGroup()
                  .addComponent(PM2051, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2031, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2081, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(label3, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2091, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2061, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2041, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2011, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2071, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2021, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2111, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2121, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2101, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel4Layout.createParallelGroup()
                  .addComponent(PM2052, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2042, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2022, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2032, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2012, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(label4, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2082, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2122, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2072, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2102, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2112, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2092, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2062, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel4Layout.createParallelGroup()
                  .addComponent(label5, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2013, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2033, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2043, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2053, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2023, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2123, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2083, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2073, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2103, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2093, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2113, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PM2063, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)))
              .addGroup(panel4Layout.createSequentialGroup()
                .addGap(334, 334, 334)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(WTOT, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
          );
          panel4Layout.setVerticalGroup(
            panel4Layout.createParallelGroup()
              .addGroup(panel4Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(81, 81, 81)
                    .addComponent(label6, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(label7, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(label8, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(40, 40, 40)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(P13STX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(13, 13, 13)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(P13ATX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(13, 13, 13)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(P13REX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                    .addGap(13, 13, 13)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(P13DAX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(29, 29, 29)
                    .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addComponent(label1)
                    .addGap(24, 24, 24)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(M04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(M06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(M02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addComponent(M07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(250, 250, 250)
                        .addComponent(M12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(M05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(225, 225, 225)
                        .addComponent(M11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addComponent(M09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(M03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(200, 200, 200)
                        .addComponent(M10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addComponent(M08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(M01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(165, 165, 165)
                        .addComponent(PM207, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(65, 65, 65)
                        .addComponent(PM203, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(label2)
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(PM202, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addComponent(PM205, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(190, 190, 190)
                        .addComponent(PM208, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(PM204, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(140, 140, 140)
                        .addComponent(PM206, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(PM201, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGap(22, 22, 22)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(PM210, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(PM211, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(290, 290, 290)
                    .addComponent(PM212, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(215, 215, 215)
                    .addComponent(PM209, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addComponent(PM2051, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(65, 65, 65)
                        .addComponent(PM2031, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(190, 190, 190)
                        .addComponent(PM2081, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(label3)
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(215, 215, 215)
                        .addComponent(PM2091, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(140, 140, 140)
                        .addComponent(PM2061, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(PM2041, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(PM2011, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(165, 165, 165)
                        .addComponent(PM2071, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(PM2021, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGap(22, 22, 22)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(PM2111, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(PM2121, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(240, 240, 240)
                    .addComponent(PM2101, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addComponent(PM2052, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(PM2042, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(PM2022, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(65, 65, 65)
                        .addComponent(PM2032, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(PM2012, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(label4))
                    .addGap(22, 22, 22)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(PM2082, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addComponent(PM2122, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(PM2072, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(PM2102, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(PM2112, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(PM2092, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(140, 140, 140)
                    .addComponent(PM2062, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(label5)
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(PM2013, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(65, 65, 65)
                        .addComponent(PM2033, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(90, 90, 90)
                        .addComponent(PM2043, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addComponent(PM2053, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(PM2023, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGap(22, 22, 22)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addComponent(PM2123, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(PM2083, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(PM2073, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(PM2103, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(PM2093, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(PM2113, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(140, 140, 140)
                    .addComponent(PM2063, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WTOT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(panel4);
        panel4.setBounds(10, 5, 720, 415);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel4;
  private RiZoneSortie OBJ_44;
  private JLabel OBJ_37;
  private XRiTextField P13STX;
  private XRiTextField P13ATX;
  private XRiTextField P13REX;
  private XRiTextField P13DAX;
  private JLabel OBJ_42;
  private XRiTextField WTOT;
  private JLabel OBJ_31;
  private JLabel OBJ_34;
  private JLabel OBJ_40;
  private JLabel OBJ_49;
  private RiZoneSortie OBJ_43;
  private XRiTextField M01;
  private XRiTextField M02;
  private XRiTextField M03;
  private XRiTextField M04;
  private XRiTextField M05;
  private XRiTextField M06;
  private XRiTextField M07;
  private XRiTextField M08;
  private XRiTextField M09;
  private XRiTextField M10;
  private XRiTextField M11;
  private XRiTextField M12;
  private XRiTextField PM201;
  private XRiTextField PM202;
  private XRiTextField PM203;
  private XRiTextField PM204;
  private XRiTextField PM205;
  private XRiTextField PM206;
  private XRiTextField PM207;
  private XRiTextField PM208;
  private XRiTextField PM209;
  private XRiTextField PM210;
  private XRiTextField PM211;
  private XRiTextField PM212;
  private XRiTextField PM2011;
  private XRiTextField PM2012;
  private XRiTextField PM2013;
  private XRiTextField PM2021;
  private XRiTextField PM2031;
  private XRiTextField PM2041;
  private XRiTextField PM2051;
  private XRiTextField PM2061;
  private XRiTextField PM2071;
  private XRiTextField PM2081;
  private XRiTextField PM2091;
  private XRiTextField PM2101;
  private XRiTextField PM2111;
  private XRiTextField PM2121;
  private XRiTextField PM2022;
  private XRiTextField PM2032;
  private XRiTextField PM2042;
  private XRiTextField PM2052;
  private XRiTextField PM2062;
  private XRiTextField PM2072;
  private XRiTextField PM2082;
  private XRiTextField PM2092;
  private XRiTextField PM2102;
  private XRiTextField PM2112;
  private XRiTextField PM2122;
  private XRiTextField PM2023;
  private XRiTextField PM2033;
  private XRiTextField PM2043;
  private XRiTextField PM2053;
  private XRiTextField PM2063;
  private XRiTextField PM2073;
  private XRiTextField PM2083;
  private XRiTextField PM2093;
  private XRiTextField PM2103;
  private XRiTextField PM2113;
  private XRiTextField PM2123;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
