
package ri.serien.libecranrpg.vgvx.VGVX56FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VGVX56FM_F2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX56FM_F2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    // setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    p_contenu.setBackground(ri.serien.libcommun.outils.Constantes.COULEUR_F1);
    
    // TODO Icones
    
    // V07F
    
    // Titre
    // setTitle(???);
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    OBJ_8 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(330, 75));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(90, 90, 90));
        p_contenu.setForeground(Color.white);
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- OBJ_8 ----
        OBJ_8.setText("Pr\u00e9paration de l'affichage en cours ....");
        OBJ_8.setFont(OBJ_8.getFont().deriveFont(OBJ_8.getFont().getStyle() | Font.BOLD, OBJ_8.getFont().getSize() + 3f));
        OBJ_8.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_8.setForeground(Color.white);
        OBJ_8.setName("OBJ_8");
        p_contenu.add(OBJ_8);
        OBJ_8.setBounds(0, 25, 330, 20);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JLabel OBJ_8;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
