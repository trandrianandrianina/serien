
package ri.serien.libecranrpg.vgvx.VGVX33FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVX33FM_01 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _SEM01_Title = { "", "", };
  private String[][] _SEM01_Data = { { "SEM01", "S1X01", }, { "SEM02", "S1X02", }, { "SEM03", "S1X03", }, { "SEM04", "S1X04", },
      { "SEM05", "S1X05", }, { "SEM06", "S1X06", }, { "SEM07", "S1X07", }, { "SEM08", "S1X08", }, { "SEM09", "S1X09", },
      { "SEM10", "S1X10", }, { "SEM11", "S1X11", }, { "SEM12", "S1X12", }, };
  private int[] _SEM01_justification = {Constantes.RIGHT, Constantes.RIGHT};
  private int[] _SEM01_Width = { 25, 82, };
  private String[] WCDISP_Value = { "", "1", "2", "3", "5", "6", "8", };
  private String[] WIN2_Value = { "", "0", "1", "2", "3", "4", "5", "6", "7", "8" };
  private String[] WIN4_Value = { "", "5", "8" };
  private String[] WTYA_Value = { " ", "A", "F" };
  private String[] WREA_Value = { " ", "G" };
  
  public VGVX33FM_01(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WIN4.setValeurs(WIN4_Value, null);
    WCDISP.setValeurs(WCDISP_Value, null);
    WIN2.setValeurs(WIN2_Value, null);
    WFORC.setValeursSelection("1", " ");
    PROPLF.setValeursSelection("1", " ");
    PROMAX.setValeursSelection("1", " ");
    PROMIN.setValeursSelection("1", " ");
    SAILOT.setValeursSelection("1", " ");
    SAIMAX.setValeursSelection("1", " ");
    SAIMIN.setValeursSelection("1", " ");
    WAPD.setValeursSelection("M", "");
    WTYA.setValeurs(WTYA_Value, null);
    WREA.setValeurs(WREA_Value, null);
    SEM01.setAspectTable(null, _SEM01_Title, _SEM01_Data, _SEM01_Width, false, _SEM01_justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMOY1@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMOY2@")).trim());
    WDAT1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDAT1@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMINC2@")).trim());
    WMINR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMINR@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMINC1@")).trim());
    OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAXC2@")).trim());
    WMAXR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAXR@")).trim());
    OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAXC1@")).trim());
    OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQT1C2@")).trim());
    WLOTR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLOTR@")).trim());
    OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQT1C1@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPLAF@")).trim());
    OBJ_83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPLFC2@")).trim());
    WPLFR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPLFR@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPLFC1@")).trim());
    MANSA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MANSA@")).trim());
    OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SMINC2@")).trim());
    OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SMINC1@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SMAXC2@")).trim());
    OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SMAXC1@")).trim());
    OBJ_74.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SQT1C2@")).trim());
    OBJ_79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SQECC1@")).trim());
    OBJ_84.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SPLFC2@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SPLFC1@")).trim());
    SMINR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SMINR@")).trim());
    SMAXR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SMAXR@")).trim());
    SLOTR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLOTR@")).trim());
    SPLFR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SPLFR@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMES@")).trim());
    S1MAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S1MAG@")).trim());
    LIBMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMAG@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1ART@")).trim());
    A1UNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    MANSO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MANSO@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    PROPLF.setVisible(!lexique.HostFieldGetData("SAILOT").trim().equalsIgnoreCase(""));
    PROMAX.setVisible(!lexique.HostFieldGetData("SAIMAX").trim().equalsIgnoreCase(""));
    PROMIN.setVisible(!lexique.HostFieldGetData("SAIMIN").trim().equalsIgnoreCase(""));
    OBJ_100.setVisible(lexique.isPresent("WCDISP"));
    OBJ_44.setVisible(lexique.isPresent("MANSA"));
    WIN2.setVisible(!lexique.HostFieldGetData("PST279").trim().equalsIgnoreCase("1"));
    WIN4.setVisible(lexique.HostFieldGetData("PST279").trim().equalsIgnoreCase("1"));
    
    PROMIN.setVisible(SAIMIN.isSelected());
    PROMAX.setVisible(SAIMAX.isSelected());
    PROPLF.setVisible(SAILOT.isSelected());
    WMIN.setVisible(SAIMIN.isSelected());
    WMAX.setVisible(SAIMAX.isSelected());
    WQTE1.setVisible(SAILOT.isSelected());
    
    panel7.setVisible(lexique.isTrue("21"));
    
    if (lexique.isTrue("21")) {
      // this.setBounds(this.getX(), this.getY(), this.getWidth(), 610);
      this.setPreferredSize(new Dimension(1070, 610));
    }
    else {
      // this.setBounds(this.getX(), this.getY(), this.getWidth(), 525);
      this.setPreferredSize(new Dimension(1070, 525));
    }
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void SAIMINActionPerformed(ActionEvent e) {
    WMIN.setVisible(!WMIN.isVisible());
    PROMIN.setVisible(!PROMIN.isVisible());
  }
  
  private void SAIMAXActionPerformed(ActionEvent e) {
    WMAX.setVisible(!WMAX.isVisible());
    PROMAX.setVisible(!PROMAX.isVisible());
  }
  
  private void SAILOTActionPerformed(ActionEvent e) {
    WQTE1.setVisible(!WQTE1.isVisible());
    PROPLF.setVisible(!PROPLF.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_13 = new JTabbedPane();
    OBJ_14 = new JPanel();
    panel1 = new JPanel();
    OBJ_42 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_90 = new RiZoneSortie();
    OBJ_91 = new RiZoneSortie();
    OBJ_54 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_65 = new JLabel();
    WDAT1 = new RiZoneSortie();
    OBJ_50 = new RiZoneSortie();
    WMINR = new RiZoneSortie();
    OBJ_57 = new RiZoneSortie();
    OBJ_61 = new RiZoneSortie();
    WMAXR = new RiZoneSortie();
    OBJ_66 = new RiZoneSortie();
    OBJ_73 = new RiZoneSortie();
    WLOTR = new RiZoneSortie();
    OBJ_78 = new RiZoneSortie();
    OBJ_81 = new RiZoneSortie();
    OBJ_83 = new RiZoneSortie();
    WPLFR = new RiZoneSortie();
    OBJ_88 = new RiZoneSortie();
    OBJ_82 = new JLabel();
    MANSA = new RiZoneSortie();
    OBJ_51 = new RiZoneSortie();
    OBJ_58 = new RiZoneSortie();
    OBJ_62 = new RiZoneSortie();
    OBJ_67 = new RiZoneSortie();
    OBJ_74 = new RiZoneSortie();
    OBJ_79 = new RiZoneSortie();
    OBJ_84 = new RiZoneSortie();
    OBJ_89 = new RiZoneSortie();
    SAIMIN = new XRiCheckBox();
    SAIMAX = new XRiCheckBox();
    SAILOT = new XRiCheckBox();
    OBJ_47 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_48 = new JLabel();
    SMINR = new RiZoneSortie();
    SMAXR = new RiZoneSortie();
    SLOTR = new RiZoneSortie();
    SPLFR = new RiZoneSortie();
    separator1 = compFactory.createSeparator("Saisie", SwingConstants.CENTER);
    separator2 = compFactory.createSeparator("Calcul", SwingConstants.CENTER);
    separator3 = compFactory.createSeparator("Tendance", SwingConstants.CENTER);
    separator4 = compFactory.createSeparator("R\u00e9appro", SwingConstants.CENTER);
    separator5 = compFactory.createSeparator("Quantit\u00e9s et nombres de semaines de stock", SwingConstants.CENTER);
    panel5 = new JPanel();
    WMIN = new XRiTextField();
    WMAX = new XRiTextField();
    WQTE1 = new XRiTextField();
    PROMIN = new XRiCheckBox();
    PROMAX = new XRiCheckBox();
    PROPLF = new XRiCheckBox();
    label1 = new JLabel();
    panel2 = new JPanel();
    SCROLLPANE_OBJ_31 = new JScrollPane();
    SEM01 = new XRiTable();
    OBJ_93 = new JLabel();
    WMOY2S = new XRiTextField();
    OBJ_55 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_60 = new JLabel();
    panel3 = new JPanel();
    WIN2 = new XRiComboBox();
    WCDISP = new XRiComboBox();
    OBJ_102 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_101 = new JLabel();
    PNVNBV = new XRiTextField();
    WIN3 = new XRiTextField();
    WIN4 = new XRiComboBox();
    panel7 = new JPanel();
    WCOUV = new XRiTextField();
    OBJ_104 = new JLabel();
    WTYA = new XRiComboBox();
    OBJ_105 = new JLabel();
    WREA = new XRiComboBox();
    WAPD = new XRiCheckBox();
    label2 = new JLabel();
    WFORC = new XRiCheckBox();
    label3 = new JLabel();
    WJREG = new XRiTextField();
    OBJ_103 = new JPanel();
    panel4 = new JPanel();
    OBJ_111 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_117 = new JLabel();
    WDAT = new XRiCalendrier();
    WHRS = new XRiTextField();
    WJIT = new XRiTextField();
    FAJIT = new XRiTextField();
    WJIR = new XRiTextField();
    S1MAG = new RiZoneSortie();
    OBJ_110 = new JLabel();
    LIBMAG = new RiZoneSortie();
    barre_tete = new JMenuBar();
    p_recup = new JPanel();
    A1LIB = new RiZoneSortie();
    A1ART = new RiZoneSortie();
    OBJ_119 = new JLabel();
    A1UNS = new RiZoneSortie();
    OBJ_122 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();
    MANSO = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(1070, 525));
    setPreferredSize(new Dimension(1070, 610));
    setMaximumSize(new Dimension(1070, 610));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Historique sur 12 mois");
            riSousMenu_bt6.setToolTipText("Historique sur 12 mois");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== OBJ_13 ========
        {
          OBJ_13.setName("OBJ_13");

          //======== OBJ_14 ========
          {
            OBJ_14.setOpaque(false);
            OBJ_14.setName("OBJ_14");
            OBJ_14.setLayout(null);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("Saisie des limites de stock  en US"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_42 ----
              OBJ_42.setText("Nombre de semaines d'analyse");
              OBJ_42.setName("OBJ_42");
              panel1.add(OBJ_42);
              OBJ_42.setBounds(15, 79, 240, 20);

              //---- OBJ_92 ----
              OBJ_92.setText("Consommations moyennes");
              OBJ_92.setName("OBJ_92");
              panel1.add(OBJ_92);
              OBJ_92.setBounds(15, 265, 169, 20);

              //---- OBJ_94 ----
              OBJ_94.setText("Dernier calcul en date du");
              OBJ_94.setName("OBJ_94");
              panel1.add(OBJ_94);
              OBJ_94.setBounds(15, 297, 157, 20);

              //---- OBJ_87 ----
              OBJ_87.setText("Plafond de couverture");
              OBJ_87.setName("OBJ_87");
              panel1.add(OBJ_87);
              OBJ_87.setBounds(15, 231, 134, 20);

              //---- OBJ_77 ----
              OBJ_77.setText("Lot de commande");
              OBJ_77.setName("OBJ_77");
              panel1.add(OBJ_77);
              OBJ_77.setBounds(15, 197, 111, 20);

              //---- OBJ_90 ----
              OBJ_90.setText("@WMOY1@");
              OBJ_90.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_90.setToolTipText("consommation moyenne par semaine");
              OBJ_90.setName("OBJ_90");
              panel1.add(OBJ_90);
              OBJ_90.setBounds(290, 263, 91, OBJ_90.getPreferredSize().height);

              //---- OBJ_91 ----
              OBJ_91.setText("@WMOY2@");
              OBJ_91.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_91.setToolTipText("consommation moyenne sur les douze derni\u00e8res semaines");
              OBJ_91.setName("OBJ_91");
              panel1.add(OBJ_91);
              OBJ_91.setBounds(400, 263, 91, OBJ_91.getPreferredSize().height);

              //---- OBJ_54 ----
              OBJ_54.setText("Stock mini");
              OBJ_54.setName("OBJ_54");
              panel1.add(OBJ_54);
              OBJ_54.setBounds(15, 129, 73, 20);

              //---- OBJ_44 ----
              OBJ_44.setText("semaines");
              OBJ_44.setName("OBJ_44");
              panel1.add(OBJ_44);
              OBJ_44.setBounds(320, 77, 65, 24);

              //---- OBJ_46 ----
              OBJ_46.setText("(sur 12 semaines)");
              OBJ_46.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_46.setName("OBJ_46");
              panel1.add(OBJ_46);
              OBJ_46.setBounds(383, 75, 118, 28);

              //---- OBJ_65 ----
              OBJ_65.setText("Stock maxi");
              OBJ_65.setName("OBJ_65");
              panel1.add(OBJ_65);
              OBJ_65.setBounds(15, 163, 71, 20);

              //---- WDAT1 ----
              WDAT1.setText("@WDAT1@");
              WDAT1.setToolTipText("date de lancement du calcul du stock minimum, stock maximum et lot de commandes");
              WDAT1.setName("WDAT1");
              panel1.add(WDAT1);
              WDAT1.setBounds(180, 295, 65, WDAT1.getPreferredSize().height);

              //---- OBJ_50 ----
              OBJ_50.setText("@WMINC2@");
              OBJ_50.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_50.setToolTipText("stock minimum calcul\u00e9 \u00e0 partir des consommations moyennes des 12 derni\u00e8res semaines");
              OBJ_50.setName("OBJ_50");
              panel1.add(OBJ_50);
              OBJ_50.setBounds(400, 127, 62, OBJ_50.getPreferredSize().height);

              //---- WMINR ----
              WMINR.setText("@WMINR@");
              WMINR.setHorizontalAlignment(SwingConstants.RIGHT);
              WMINR.setToolTipText("stock mini saisi");
              WMINR.setName("WMINR");
              panel1.add(WMINR);
              WMINR.setBounds(510, 127, 62, WMINR.getPreferredSize().height);

              //---- OBJ_57 ----
              OBJ_57.setText("@WMINC1@");
              OBJ_57.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_57.setToolTipText("stock minimum calcul\u00e9");
              OBJ_57.setName("OBJ_57");
              panel1.add(OBJ_57);
              OBJ_57.setBounds(290, 127, 62, OBJ_57.getPreferredSize().height);

              //---- OBJ_61 ----
              OBJ_61.setText("@WMAXC2@");
              OBJ_61.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_61.setName("OBJ_61");
              panel1.add(OBJ_61);
              OBJ_61.setBounds(400, 161, 62, OBJ_61.getPreferredSize().height);

              //---- WMAXR ----
              WMAXR.setText("@WMAXR@");
              WMAXR.setHorizontalAlignment(SwingConstants.RIGHT);
              WMAXR.setName("WMAXR");
              panel1.add(WMAXR);
              WMAXR.setBounds(510, 161, 62, WMAXR.getPreferredSize().height);

              //---- OBJ_66 ----
              OBJ_66.setText("@WMAXC1@");
              OBJ_66.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_66.setName("OBJ_66");
              panel1.add(OBJ_66);
              OBJ_66.setBounds(290, 161, 62, OBJ_66.getPreferredSize().height);

              //---- OBJ_73 ----
              OBJ_73.setText("@WQT1C2@");
              OBJ_73.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_73.setName("OBJ_73");
              panel1.add(OBJ_73);
              OBJ_73.setBounds(400, 195, 62, OBJ_73.getPreferredSize().height);

              //---- WLOTR ----
              WLOTR.setText("@WLOTR@");
              WLOTR.setHorizontalAlignment(SwingConstants.RIGHT);
              WLOTR.setName("WLOTR");
              panel1.add(WLOTR);
              WLOTR.setBounds(510, 195, 62, WLOTR.getPreferredSize().height);

              //---- OBJ_78 ----
              OBJ_78.setText("@WQT1C1@");
              OBJ_78.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_78.setName("OBJ_78");
              panel1.add(OBJ_78);
              OBJ_78.setBounds(290, 195, 62, OBJ_78.getPreferredSize().height);

              //---- OBJ_81 ----
              OBJ_81.setText("@WPLAF@");
              OBJ_81.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_81.setName("OBJ_81");
              panel1.add(OBJ_81);
              OBJ_81.setBounds(180, 229, 82, OBJ_81.getPreferredSize().height);

              //---- OBJ_83 ----
              OBJ_83.setText("@WPLFC2@");
              OBJ_83.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_83.setName("OBJ_83");
              panel1.add(OBJ_83);
              OBJ_83.setBounds(400, 229, 62, OBJ_83.getPreferredSize().height);

              //---- WPLFR ----
              WPLFR.setText("@WPLFR@");
              WPLFR.setHorizontalAlignment(SwingConstants.RIGHT);
              WPLFR.setName("WPLFR");
              panel1.add(WPLFR);
              WPLFR.setBounds(510, 229, 62, WPLFR.getPreferredSize().height);

              //---- OBJ_88 ----
              OBJ_88.setText("@WPLFC1@");
              OBJ_88.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_88.setName("OBJ_88");
              panel1.add(OBJ_88);
              OBJ_88.setBounds(290, 229, 62, OBJ_88.getPreferredSize().height);

              //---- OBJ_82 ----
              OBJ_82.setText("a+b");
              OBJ_82.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_82.setName("OBJ_82");
              panel1.add(OBJ_82);
              OBJ_82.setBounds(265, 230, 25, 22);

              //---- MANSA ----
              MANSA.setText("@MANSA@");
              MANSA.setHorizontalAlignment(SwingConstants.RIGHT);
              MANSA.setToolTipText("Nombre de semaines d'analyse de la fiche magasin pour le calcul du stock mini ");
              MANSA.setName("MANSA");
              panel1.add(MANSA);
              MANSA.setBounds(290, 77, 25, MANSA.getPreferredSize().height);

              //---- OBJ_51 ----
              OBJ_51.setText("@SMINC2@");
              OBJ_51.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_51.setToolTipText("nombre de semaines de consommation saisi pour la mise \u00e0 jour du stock mini  ");
              OBJ_51.setName("OBJ_51");
              panel1.add(OBJ_51);
              OBJ_51.setBounds(470, 127, 25, OBJ_51.getPreferredSize().height);

              //---- OBJ_58 ----
              OBJ_58.setText("@SMINC1@");
              OBJ_58.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_58.setToolTipText("nombre de semaines de consommation saisi pour la mise \u00e0 jour du stock mini ");
              OBJ_58.setName("OBJ_58");
              panel1.add(OBJ_58);
              OBJ_58.setBounds(360, 127, 25, OBJ_58.getPreferredSize().height);

              //---- OBJ_62 ----
              OBJ_62.setText("@SMAXC2@");
              OBJ_62.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_62.setName("OBJ_62");
              panel1.add(OBJ_62);
              OBJ_62.setBounds(470, 161, 25, OBJ_62.getPreferredSize().height);

              //---- OBJ_67 ----
              OBJ_67.setText("@SMAXC1@");
              OBJ_67.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_67.setName("OBJ_67");
              panel1.add(OBJ_67);
              OBJ_67.setBounds(360, 161, 25, OBJ_67.getPreferredSize().height);

              //---- OBJ_74 ----
              OBJ_74.setText("@SQT1C2@");
              OBJ_74.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_74.setName("OBJ_74");
              panel1.add(OBJ_74);
              OBJ_74.setBounds(470, 195, 25, OBJ_74.getPreferredSize().height);

              //---- OBJ_79 ----
              OBJ_79.setText("@SQECC1@");
              OBJ_79.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_79.setName("OBJ_79");
              panel1.add(OBJ_79);
              OBJ_79.setBounds(360, 195, 25, OBJ_79.getPreferredSize().height);

              //---- OBJ_84 ----
              OBJ_84.setText("@SPLFC2@");
              OBJ_84.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_84.setName("OBJ_84");
              panel1.add(OBJ_84);
              OBJ_84.setBounds(470, 229, 25, OBJ_84.getPreferredSize().height);

              //---- OBJ_89 ----
              OBJ_89.setText("@SPLFC1@");
              OBJ_89.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_89.setName("OBJ_89");
              panel1.add(OBJ_89);
              OBJ_89.setBounds(360, 229, 25, OBJ_89.getPreferredSize().height);

              //---- SAIMIN ----
              SAIMIN.setText("");
              SAIMIN.setToolTipText("Saisie autoris\u00e9e");
              SAIMIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SAIMIN.setName("SAIMIN");
              SAIMIN.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  SAIMINActionPerformed(e);
                }
              });
              panel1.add(SAIMIN);
              SAIMIN.setBounds(130, 129, 20, 20);

              //---- SAIMAX ----
              SAIMAX.setText("");
              SAIMAX.setToolTipText("Saisie autoris\u00e9e");
              SAIMAX.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SAIMAX.setName("SAIMAX");
              SAIMAX.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  SAIMAXActionPerformed(e);
                }
              });
              panel1.add(SAIMAX);
              SAIMAX.setBounds(130, 163, 20, 20);

              //---- SAILOT ----
              SAILOT.setText("");
              SAILOT.setToolTipText("Saisie autoris\u00e9e");
              SAILOT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SAILOT.setName("SAILOT");
              SAILOT.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  SAILOTActionPerformed(e);
                }
              });
              panel1.add(SAILOT);
              SAILOT.setBounds(130, 197, 20, 20);

              //---- OBJ_47 ----
              OBJ_47.setText("S");
              OBJ_47.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_47.setName("OBJ_47");
              panel1.add(OBJ_47);
              OBJ_47.setBounds(130, 109, 12, 17);

              //---- OBJ_59 ----
              OBJ_59.setText("a");
              OBJ_59.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_59.setName("OBJ_59");
              panel1.add(OBJ_59);
              OBJ_59.setBounds(265, 131, 21, 18);

              //---- OBJ_80 ----
              OBJ_80.setText("b");
              OBJ_80.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_80.setName("OBJ_80");
              panel1.add(OBJ_80);
              OBJ_80.setBounds(265, 199, 21, 18);

              //---- OBJ_48 ----
              OBJ_48.setText("P");
              OBJ_48.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_48.setName("OBJ_48");
              panel1.add(OBJ_48);
              OBJ_48.setBounds(155, 109, 12, 17);

              //---- SMINR ----
              SMINR.setText("@SMINR@");
              SMINR.setHorizontalAlignment(SwingConstants.RIGHT);
              SMINR.setToolTipText("nombre de semaines de stock");
              SMINR.setName("SMINR");
              panel1.add(SMINR);
              SMINR.setBounds(580, 127, 25, SMINR.getPreferredSize().height);

              //---- SMAXR ----
              SMAXR.setText("@SMAXR@");
              SMAXR.setHorizontalAlignment(SwingConstants.RIGHT);
              SMAXR.setName("SMAXR");
              panel1.add(SMAXR);
              SMAXR.setBounds(580, 161, 25, SMAXR.getPreferredSize().height);

              //---- SLOTR ----
              SLOTR.setText("@SLOTR@");
              SLOTR.setHorizontalAlignment(SwingConstants.RIGHT);
              SLOTR.setName("SLOTR");
              panel1.add(SLOTR);
              SLOTR.setBounds(580, 195, 25, SLOTR.getPreferredSize().height);

              //---- SPLFR ----
              SPLFR.setText("@SPLFR@");
              SPLFR.setHorizontalAlignment(SwingConstants.RIGHT);
              SPLFR.setName("SPLFR");
              panel1.add(SPLFR);
              SPLFR.setBounds(580, 229, 25, SPLFR.getPreferredSize().height);

              //---- separator1 ----
              separator1.setName("separator1");
              panel1.add(separator1);
              separator1.setBounds(175, 59, 95, separator1.getPreferredSize().height);

              //---- separator2 ----
              separator2.setName("separator2");
              panel1.add(separator2);
              separator2.setBounds(285, 59, 95, separator2.getPreferredSize().height);

              //---- separator3 ----
              separator3.setName("separator3");
              panel1.add(separator3);
              separator3.setBounds(395, 59, 95, separator3.getPreferredSize().height);

              //---- separator4 ----
              separator4.setName("separator4");
              panel1.add(separator4);
              separator4.setBounds(505, 59, 95, separator4.getPreferredSize().height);

              //---- separator5 ----
              separator5.setName("separator5");
              panel1.add(separator5);
              separator5.setBounds(175, 34, 425, separator5.getPreferredSize().height);

              //======== panel5 ========
              {
                panel5.setOpaque(false);
                panel5.setName("panel5");
                panel5.setLayout(null);

                //---- WMIN ----
                WMIN.setComponentPopupMenu(BTD);
                WMIN.setToolTipText("stock minimum saisi");
                WMIN.setName("WMIN");
                panel5.add(WMIN);
                WMIN.setBounds(30, 6, 82, WMIN.getPreferredSize().height);

                //---- WMAX ----
                WMAX.setComponentPopupMenu(BTD);
                WMAX.setName("WMAX");
                panel5.add(WMAX);
                WMAX.setBounds(30, 39, 82, WMAX.getPreferredSize().height);

                //---- WQTE1 ----
                WQTE1.setComponentPopupMenu(BTD);
                WQTE1.setName("WQTE1");
                panel5.add(WQTE1);
                WQTE1.setBounds(30, 74, 82, WQTE1.getPreferredSize().height);

                //---- PROMIN ----
                PROMIN.setText("");
                PROMIN.setToolTipText("Protection de la saisie");
                PROMIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                PROMIN.setName("PROMIN");
                panel5.add(PROMIN);
                PROMIN.setBounds(5, 10, 20, 20);

                //---- PROMAX ----
                PROMAX.setText("");
                PROMAX.setToolTipText("Protection de la saisie");
                PROMAX.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                PROMAX.setName("PROMAX");
                panel5.add(PROMAX);
                PROMAX.setBounds(5, 43, 20, 20);

                //---- PROPLF ----
                PROPLF.setText("");
                PROPLF.setToolTipText("Protection de la saisie");
                PROPLF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                PROPLF.setName("PROPLF");
                panel5.add(PROPLF);
                PROPLF.setBounds(5, 78, 20, 20);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel5.getComponentCount(); i++) {
                    Rectangle bounds = panel5.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel5.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel5.setMinimumSize(preferredSize);
                  panel5.setPreferredSize(preferredSize);
                }
              }
              panel1.add(panel5);
              panel5.setBounds(150, 119, 120, 110);

              //---- label1 ----
              label1.setText("@LIBMES@");
              label1.setForeground(new Color(158, 0, 0));
              label1.setName("label1");
              panel1.add(label1);
              label1.setBounds(255, 298, 318, 18);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            OBJ_14.add(panel1);
            panel1.setBounds(10, 10, 620, 335);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Conso. des 12 derni\u00e8res semaines"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //======== SCROLLPANE_OBJ_31 ========
              {
                SCROLLPANE_OBJ_31.setComponentPopupMenu(BTD);
                SCROLLPANE_OBJ_31.setName("SCROLLPANE_OBJ_31");

                //---- SEM01 ----
                SEM01.setName("SEM01");
                SCROLLPANE_OBJ_31.setViewportView(SEM01);
              }
              panel2.add(SCROLLPANE_OBJ_31);
              SCROLLPANE_OBJ_31.setBounds(25, 60, 175, 210);

              //---- OBJ_93 ----
              OBJ_93.setText("Moyenne");
              OBJ_93.setName("OBJ_93");
              panel2.add(OBJ_93);
              OBJ_93.setBounds(25, 283, 90, 23);

              //---- WMOY2S ----
              WMOY2S.setComponentPopupMenu(BTD);
              WMOY2S.setHorizontalAlignment(SwingConstants.RIGHT);
              WMOY2S.setToolTipText("moyenne de la consommation des douze derni\u00e8res semaines");
              WMOY2S.setName("WMOY2S");
              panel2.add(WMOY2S);
              WMOY2S.setBounds(117, 280, 83, WMOY2S.getPreferredSize().height);

              //---- OBJ_55 ----
              OBJ_55.setText("Num\u00e9ro");
              OBJ_55.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_55.setName("OBJ_55");
              panel2.add(OBJ_55);
              OBJ_55.setBounds(25, 25, 60, 20);

              //---- OBJ_56 ----
              OBJ_56.setText("semaine");
              OBJ_56.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_56.setName("OBJ_56");
              panel2.add(OBJ_56);
              OBJ_56.setBounds(25, 40, 60, 20);

              //---- OBJ_60 ----
              OBJ_60.setText("Consomation");
              OBJ_60.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_60.setName("OBJ_60");
              panel2.add(OBJ_60);
              OBJ_60.setBounds(85, 25, 115, 35);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            OBJ_14.add(panel2);
            panel2.setBounds(635, 10, 220, 335);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Divers"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- WIN2 ----
              WIN2.setModel(new DefaultComboBoxModel(new String[] {
                " ",
                "Rupture sur stock minimum",
                "Consommation moyenne",
                "R\u00e9approvisionnement manuel",
                "Pr\u00e9visions de consommation",
                "Conso moyenne plafonn\u00e9e",
                "G\u00e9r\u00e9 (PLafond pour couverture)",
                "Compl\u00e9ment stock maximum",
                "Plafond pour couverture 'Mag'",
                "Produit non g\u00e9r\u00e9"
              }));
              WIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WIN2.setName("WIN2");
              panel3.add(WIN2);
              WIN2.setBounds(185, 25, 215, WIN2.getPreferredSize().height);

              //---- WCDISP ----
              WCDISP.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Disponible",
                "R\u00e9impression-not\u00e9",
                "R\u00e9impression",
                "A para\u00eetre",
                "A para\u00eetre-not\u00e9",
                "Retir\u00e9 du catalogue"
              }));
              WCDISP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WCDISP.setName("WCDISP");
              panel3.add(WCDISP);
              WCDISP.setBounds(185, 55, 165, WCDISP.getPreferredSize().height);

              //---- OBJ_102 ----
              OBJ_102.setText("Nombre de ventes sur 52 derni\u00e8res semaines");
              OBJ_102.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_102.setName("OBJ_102");
              panel3.add(OBJ_102);
              OBJ_102.setBounds(435, 58, 274, 20);

              //---- OBJ_99 ----
              OBJ_99.setText("Type r\u00e9approvisionement");
              OBJ_99.setName("OBJ_99");
              panel3.add(OBJ_99);
              OBJ_99.setBounds(15, 28, 165, 20);

              //---- OBJ_100 ----
              OBJ_100.setText("Disponibilit\u00e9");
              OBJ_100.setComponentPopupMenu(BTD);
              OBJ_100.setName("OBJ_100");
              panel3.add(OBJ_100);
              OBJ_100.setBounds(15, 58, 165, 20);

              //---- OBJ_101 ----
              OBJ_101.setText("Cat\u00e9gorie ABC");
              OBJ_101.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_101.setName("OBJ_101");
              panel3.add(OBJ_101);
              OBJ_101.setBounds(435, 28, 274, 20);

              //---- PNVNBV ----
              PNVNBV.setComponentPopupMenu(BTD);
              PNVNBV.setHorizontalAlignment(SwingConstants.RIGHT);
              PNVNBV.setToolTipText("nombre de ventes sur les 52 derni\u00e8res semaines ");
              PNVNBV.setName("PNVNBV");
              panel3.add(PNVNBV);
              PNVNBV.setBounds(740, 54, 42, PNVNBV.getPreferredSize().height);

              //---- WIN3 ----
              WIN3.setComponentPopupMenu(BTD);
              WIN3.setName("WIN3");
              panel3.add(WIN3);
              WIN3.setBounds(740, 24, 25, WIN3.getPreferredSize().height);

              //---- WIN4 ----
              WIN4.setModel(new DefaultComboBoxModel(new String[] {
                " ",
                "G\u00e9r\u00e9",
                "Non g\u00e9r\u00e9"
              }));
              WIN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WIN4.setName("WIN4");
              panel3.add(WIN4);
              WIN4.setBounds(185, 25, 215, WIN4.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            OBJ_14.add(panel3);
            panel3.setBounds(10, 345, 845, 95);

            //======== panel7 ========
            {
              panel7.setBorder(new TitledBorder("MRP"));
              panel7.setOpaque(false);
              panel7.setName("panel7");
              panel7.setLayout(null);

              //---- WCOUV ----
              WCOUV.setName("WCOUV");
              panel7.add(WCOUV);
              WCOUV.setBounds(195, 55, 69, WCOUV.getPreferredSize().height);

              //---- OBJ_104 ----
              OBJ_104.setText("Type achat");
              OBJ_104.setComponentPopupMenu(BTD);
              OBJ_104.setName("OBJ_104");
              panel7.add(OBJ_104);
              OBJ_104.setBounds(25, 28, 165, 20);

              //---- WTYA ----
              WTYA.setModel(new DefaultComboBoxModel(new String[] {
                " ",
                "Achet\u00e9",
                "Fabriqu\u00e9"
              }));
              WTYA.setName("WTYA");
              panel7.add(WTYA);
              WTYA.setBounds(195, 25, 105, WTYA.getPreferredSize().height);

              //---- OBJ_105 ----
              OBJ_105.setText("Type r\u00e9appro.");
              OBJ_105.setComponentPopupMenu(BTD);
              OBJ_105.setName("OBJ_105");
              panel7.add(OBJ_105);
              OBJ_105.setBounds(335, 28, 105, 20);

              //---- WREA ----
              WREA.setModel(new DefaultComboBoxModel(new String[] {
                " ",
                "G\u00e9n\u00e9ration besoin"
              }));
              WREA.setName("WREA");
              panel7.add(WREA);
              WREA.setBounds(440, 25, 165, WREA.getPreferredSize().height);

              //---- WAPD ----
              WAPD.setText("Appartenance au plan directeur");
              WAPD.setName("WAPD");
              panel7.add(WAPD);
              WAPD.setBounds(635, 29, 205, WAPD.getPreferredSize().height);

              //---- label2 ----
              label2.setText("Couverture pour stock s\u00e9cu.");
              label2.setName("label2");
              panel7.add(label2);
              label2.setBounds(25, 55, 165, 25);

              //---- WFORC ----
              WFORC.setText("For\u00e7age couverture");
              WFORC.setName("WFORC");
              panel7.add(WFORC);
              WFORC.setBounds(335, 55, 170, 28);

              //---- label3 ----
              label3.setText("Nombre de jours de regroupement");
              label3.setName("label3");
              panel7.add(label3);
              label3.setBounds(530, 55, 210, 25);

              //---- WJREG ----
              WJREG.setName("WJREG");
              panel7.add(WJREG);
              WJREG.setBounds(740, 55, 46, WJREG.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel7.getComponentCount(); i++) {
                  Rectangle bounds = panel7.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel7.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel7.setMinimumSize(preferredSize);
                panel7.setPreferredSize(preferredSize);
              }
            }
            OBJ_14.add(panel7);
            panel7.setBounds(10, 440, 845, 95);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < OBJ_14.getComponentCount(); i++) {
                Rectangle bounds = OBJ_14.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = OBJ_14.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              OBJ_14.setMinimumSize(preferredSize);
              OBJ_14.setPreferredSize(preferredSize);
            }
          }
          OBJ_13.addTab("Limites de stock", OBJ_14);

          //======== OBJ_103 ========
          {
            OBJ_103.setOpaque(false);
            OBJ_103.setName("OBJ_103");
            OBJ_103.setLayout(null);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("Inventaires tournants"));
              panel4.setOpaque(false);
              panel4.setName("panel4");

              //---- OBJ_111 ----
              OBJ_111.setText("Nombre sur la famille (par d\u00e9faut)");
              OBJ_111.setName("OBJ_111");

              //---- OBJ_109 ----
              OBJ_109.setText("Nombre \u00e0 faire cette ann\u00e9e");
              OBJ_109.setName("OBJ_109");

              //---- OBJ_113 ----
              OBJ_113.setText("Nombre d\u00e9j\u00e0 effectu\u00e9s");
              OBJ_113.setName("OBJ_113");

              //---- OBJ_115 ----
              OBJ_115.setText("Date du dernier inventaire");
              OBJ_115.setName("OBJ_115");

              //---- OBJ_117 ----
              OBJ_117.setText("Heure du dernier inventaire");
              OBJ_117.setName("OBJ_117");

              //---- WDAT ----
              WDAT.setName("WDAT");

              //---- WHRS ----
              WHRS.setName("WHRS");

              //---- WJIT ----
              WJIT.setComponentPopupMenu(BTD);
              WJIT.setName("WJIT");

              //---- FAJIT ----
              FAJIT.setName("FAJIT");

              //---- WJIR ----
              WJIR.setName("WJIR");

              //---- S1MAG ----
              S1MAG.setComponentPopupMenu(BTD);
              S1MAG.setText("@S1MAG@");
              S1MAG.setName("S1MAG");

              //---- OBJ_110 ----
              OBJ_110.setText("Magasin");
              OBJ_110.setName("OBJ_110");

              //---- LIBMAG ----
              LIBMAG.setComponentPopupMenu(BTD);
              LIBMAG.setText("@LIBMAG@");
              LIBMAG.setName("LIBMAG");

              GroupLayout panel4Layout = new GroupLayout(panel4);
              panel4.setLayout(panel4Layout);
              panel4Layout.setHorizontalGroup(
                panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(16, 16, 16)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addComponent(OBJ_110, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(S1MAG, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addComponent(LIBMAG, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addComponent(OBJ_109, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(WJIT, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addComponent(OBJ_111, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(FAJIT, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addComponent(OBJ_113, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(WJIR, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addComponent(OBJ_115, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(WDAT, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addComponent(OBJ_117, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(WHRS, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE))))
              );
              panel4Layout.setVerticalGroup(
                panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_110, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(S1MAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(LIBMAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(14, 14, 14)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_109, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WJIT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(14, 14, 14)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_111, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(FAJIT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(14, 14, 14)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_113, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WJIR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(14, 14, 14)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_115, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WDAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(14, 14, 14)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(OBJ_117, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WHRS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }
            OBJ_103.add(panel4);
            panel4.setBounds(25, 20, 640, 325);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < OBJ_103.getComponentCount(); i++) {
                Rectangle bounds = OBJ_103.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = OBJ_103.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              OBJ_103.setMinimumSize(preferredSize);
              OBJ_103.setPreferredSize(preferredSize);
            }
          }
          OBJ_13.addTab("Inventaires tournants", OBJ_103);
        }
        p_contenu.add(OBJ_13);
        OBJ_13.setBounds(15, 10, 870, 565);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //======== p_recup ========
      {
        p_recup.setOpaque(false);
        p_recup.setName("p_recup");
        p_recup.setLayout(null);

        //---- A1LIB ----
        A1LIB.setComponentPopupMenu(null);
        A1LIB.setOpaque(false);
        A1LIB.setText("@A1LIB@");
        A1LIB.setName("A1LIB");
        p_recup.add(A1LIB);
        A1LIB.setBounds(230, 0, 251, A1LIB.getPreferredSize().height);

        //---- A1ART ----
        A1ART.setComponentPopupMenu(null);
        A1ART.setOpaque(false);
        A1ART.setText("@A1ART@");
        A1ART.setName("A1ART");
        p_recup.add(A1ART);
        A1ART.setBounds(60, 0, 161, A1ART.getPreferredSize().height);

        //---- OBJ_119 ----
        OBJ_119.setText("Article");
        OBJ_119.setName("OBJ_119");
        p_recup.add(OBJ_119);
        OBJ_119.setBounds(10, 2, 58, 20);

        //---- A1UNS ----
        A1UNS.setComponentPopupMenu(null);
        A1UNS.setOpaque(false);
        A1UNS.setText("@A1UNS@");
        A1UNS.setName("A1UNS");
        p_recup.add(A1UNS);
        A1UNS.setBounds(635, 0, 34, A1UNS.getPreferredSize().height);

        //---- OBJ_122 ----
        OBJ_122.setText("Unit\u00e9 de stock");
        OBJ_122.setName("OBJ_122");
        p_recup.add(OBJ_122);
        OBJ_122.setBounds(540, 2, 95, 20);
      }
      barre_tete.add(p_recup);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }

    //---- MANSO ----
    MANSO.setText("@MANSO@");
    MANSO.setHorizontalAlignment(SwingConstants.RIGHT);
    MANSO.setName("MANSO");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JTabbedPane OBJ_13;
  private JPanel OBJ_14;
  private JPanel panel1;
  private JLabel OBJ_42;
  private JLabel OBJ_92;
  private JLabel OBJ_94;
  private JLabel OBJ_87;
  private JLabel OBJ_77;
  private RiZoneSortie OBJ_90;
  private RiZoneSortie OBJ_91;
  private JLabel OBJ_54;
  private JLabel OBJ_44;
  private JLabel OBJ_46;
  private JLabel OBJ_65;
  private RiZoneSortie WDAT1;
  private RiZoneSortie OBJ_50;
  private RiZoneSortie WMINR;
  private RiZoneSortie OBJ_57;
  private RiZoneSortie OBJ_61;
  private RiZoneSortie WMAXR;
  private RiZoneSortie OBJ_66;
  private RiZoneSortie OBJ_73;
  private RiZoneSortie WLOTR;
  private RiZoneSortie OBJ_78;
  private RiZoneSortie OBJ_81;
  private RiZoneSortie OBJ_83;
  private RiZoneSortie WPLFR;
  private RiZoneSortie OBJ_88;
  private JLabel OBJ_82;
  private RiZoneSortie MANSA;
  private RiZoneSortie OBJ_51;
  private RiZoneSortie OBJ_58;
  private RiZoneSortie OBJ_62;
  private RiZoneSortie OBJ_67;
  private RiZoneSortie OBJ_74;
  private RiZoneSortie OBJ_79;
  private RiZoneSortie OBJ_84;
  private RiZoneSortie OBJ_89;
  private XRiCheckBox SAIMIN;
  private XRiCheckBox SAIMAX;
  private XRiCheckBox SAILOT;
  private JLabel OBJ_47;
  private JLabel OBJ_59;
  private JLabel OBJ_80;
  private JLabel OBJ_48;
  private RiZoneSortie SMINR;
  private RiZoneSortie SMAXR;
  private RiZoneSortie SLOTR;
  private RiZoneSortie SPLFR;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  private JComponent separator4;
  private JComponent separator5;
  private JPanel panel5;
  private XRiTextField WMIN;
  private XRiTextField WMAX;
  private XRiTextField WQTE1;
  private XRiCheckBox PROMIN;
  private XRiCheckBox PROMAX;
  private XRiCheckBox PROPLF;
  private JLabel label1;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_OBJ_31;
  private XRiTable SEM01;
  private JLabel OBJ_93;
  private XRiTextField WMOY2S;
  private JLabel OBJ_55;
  private JLabel OBJ_56;
  private JLabel OBJ_60;
  private JPanel panel3;
  private XRiComboBox WIN2;
  private XRiComboBox WCDISP;
  private JLabel OBJ_102;
  private JLabel OBJ_99;
  private JLabel OBJ_100;
  private JLabel OBJ_101;
  private XRiTextField PNVNBV;
  private XRiTextField WIN3;
  private XRiComboBox WIN4;
  private JPanel panel7;
  private XRiTextField WCOUV;
  private JLabel OBJ_104;
  private XRiComboBox WTYA;
  private JLabel OBJ_105;
  private XRiComboBox WREA;
  private XRiCheckBox WAPD;
  private JLabel label2;
  private XRiCheckBox WFORC;
  private JLabel label3;
  private XRiTextField WJREG;
  private JPanel OBJ_103;
  private JPanel panel4;
  private JLabel OBJ_111;
  private JLabel OBJ_109;
  private JLabel OBJ_113;
  private JLabel OBJ_115;
  private JLabel OBJ_117;
  private XRiCalendrier WDAT;
  private XRiTextField WHRS;
  private XRiTextField WJIT;
  private XRiTextField FAJIT;
  private XRiTextField WJIR;
  private RiZoneSortie S1MAG;
  private JLabel OBJ_110;
  private RiZoneSortie LIBMAG;
  private JMenuBar barre_tete;
  private JPanel p_recup;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1ART;
  private JLabel OBJ_119;
  private RiZoneSortie A1UNS;
  private JLabel OBJ_122;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  private RiZoneSortie MANSO;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
