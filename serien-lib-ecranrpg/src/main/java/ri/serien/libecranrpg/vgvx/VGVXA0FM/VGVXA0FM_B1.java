
package ri.serien.libecranrpg.vgvx.VGVXA0FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGVXA0FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXA0FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    LSPENT.setValeursSelection("1", " ");
    LSPPIE.setValeursSelection("1", " ");
    LSO05.setValeursSelection("1", " ");
    LSK05.setValeursSelection("1", " ");
    LSAFZ5.setValeursSelection("1", " ");
    LSO04.setValeursSelection("1", " ");
    LSK04.setValeursSelection("1", " ");
    LSAFZ4.setValeursSelection("1", " ");
    LSO03.setValeursSelection("1", " ");
    LSK03.setValeursSelection("1", " ");
    LSAFZ3.setValeursSelection("1", " ");
    LSO02.setValeursSelection("1", " ");
    LSK02.setValeursSelection("1", " ");
    LSAFZ2.setValeursSelection("1", " ");
    LSO01.setValeursSelection("1", " ");
    LSK01.setValeursSelection("1", " ");
    LSAFZ1.setValeursSelection("1", " ");
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    LSPPIE.setVisible(LSPENT.isSelected());
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void LSPENTActionPerformed(ActionEvent e) {
    LSPPIE.setVisible(!LSPPIE.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WETB = new XRiTextField();
    OBJ_33 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    LSTPZ1 = new XRiTextField();
    LSTPZ2 = new XRiTextField();
    LSTPZ3 = new XRiTextField();
    LSTPZ4 = new XRiTextField();
    LSTPZ5 = new XRiTextField();
    LSAFZ1 = new XRiCheckBox();
    LSK01 = new XRiCheckBox();
    LSO01 = new XRiCheckBox();
    LSAFZ2 = new XRiCheckBox();
    LSK02 = new XRiCheckBox();
    LSO02 = new XRiCheckBox();
    LSAFZ3 = new XRiCheckBox();
    LSK03 = new XRiCheckBox();
    LSO03 = new XRiCheckBox();
    LSAFZ4 = new XRiCheckBox();
    LSK04 = new XRiCheckBox();
    LSO04 = new XRiCheckBox();
    LSAFZ5 = new XRiCheckBox();
    LSK05 = new XRiCheckBox();
    LSO05 = new XRiCheckBox();
    OBJ_102 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_105 = new JLabel();
    OBJ_106 = new JLabel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    LSUZ1 = new XRiCheckBox();
    LSUZ2 = new XRiCheckBox();
    LSUZ3 = new XRiCheckBox();
    LSUZ4 = new XRiCheckBox();
    LSUZ5 = new XRiCheckBox();
    xTitledPanel2 = new JXTitledPanel();
    LSL1 = new XRiTextField();
    LSL2 = new XRiTextField();
    LSL3 = new XRiTextField();
    LSL4 = new XRiTextField();
    OBJ_107 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_110 = new JLabel();
    xTitledPanel3 = new JXTitledPanel();
    OBJ_114 = new JLabel();
    OBJ_118 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_117 = new JLabel();
    LSP11 = new XRiTextField();
    LSP12 = new XRiTextField();
    LSP13 = new XRiTextField();
    LSP14 = new XRiTextField();
    LSP21 = new XRiTextField();
    LSP22 = new XRiTextField();
    LSP23 = new XRiTextField();
    LSP24 = new XRiTextField();
    LSP31 = new XRiTextField();
    LSP32 = new XRiTextField();
    LSP33 = new XRiTextField();
    LSP41 = new XRiTextField();
    LSP42 = new XRiTextField();
    LSP43 = new XRiTextField();
    separator1 = compFactory.createSeparator("Types de zones", SwingConstants.CENTER);
    separator4 = compFactory.createSeparator("Mouvement de stock", SwingConstants.CENTER);
    xTitledPanel4 = new JXTitledPanel();
    OBJ_134 = new JLabel();
    OBJ_132 = new JLabel();
    OBJ_133 = new JLabel();
    OBJ_136 = new JLabel();
    OBJ_137 = new JLabel();
    OBJ_131 = new JLabel();
    OBJ_126 = new JLabel();
    OBJ_130 = new JLabel();
    OBJ_124 = new JLabel();
    OBJ_129 = new JLabel();
    OBJ_143 = new JLabel();
    LSF1D = new XRiTextField();
    LSF1A = new XRiTextField();
    LSM01 = new XRiTextField();
    LSF2D = new XRiTextField();
    LSF2A = new XRiTextField();
    LSM02 = new XRiTextField();
    LSF3D = new XRiTextField();
    LSF3A = new XRiTextField();
    LSM03 = new XRiTextField();
    LSF4D = new XRiTextField();
    LSF4A = new XRiTextField();
    LSM04 = new XRiTextField();
    LSF5D = new XRiTextField();
    LSF5A = new XRiTextField();
    LSM05 = new XRiTextField();
    LSF6D = new XRiTextField();
    LSF6A = new XRiTextField();
    LSM06 = new XRiTextField();
    OBJ_144 = new JLabel();
    separator2 = compFactory.createSeparator("Ordre des transferts");
    separator3 = compFactory.createSeparator("Ordre des transferts");
    xTitledPanel5 = new JXTitledPanel();
    LSPENT = new XRiCheckBox();
    LSLEB = new XRiTextField();
    LSQEB = new XRiTextField();
    LSLMO = new XRiTextField();
    LSQMO = new XRiTextField();
    LSLFB = new XRiTextField();
    LSQFB = new XRiTextField();
    LSPPIE = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des stocks par adresses");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(125, 1, 40, WETB.getPreferredSize().height);

          //---- OBJ_33 ----
          OBJ_33.setText("Etablissement");
          OBJ_33.setName("OBJ_33");
          p_tete_gauche.add(OBJ_33);
          OBJ_33.setBounds(5, 5, 93, 20);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(810, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== xTitledPanel1 ========
            {
              xTitledPanel1.setTitle("Zones de stock");
              xTitledPanel1.setName("xTitledPanel1");
              Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
              xTitledPanel1ContentContainer.setLayout(null);

              //---- LSTPZ1 ----
              LSTPZ1.setComponentPopupMenu(BTD);
              LSTPZ1.setName("LSTPZ1");
              xTitledPanel1ContentContainer.add(LSTPZ1);
              LSTPZ1.setBounds(40, 20, 160, LSTPZ1.getPreferredSize().height);

              //---- LSTPZ2 ----
              LSTPZ2.setComponentPopupMenu(BTD);
              LSTPZ2.setName("LSTPZ2");
              xTitledPanel1ContentContainer.add(LSTPZ2);
              LSTPZ2.setBounds(40, 45, 160, LSTPZ2.getPreferredSize().height);

              //---- LSTPZ3 ----
              LSTPZ3.setComponentPopupMenu(BTD);
              LSTPZ3.setName("LSTPZ3");
              xTitledPanel1ContentContainer.add(LSTPZ3);
              LSTPZ3.setBounds(40, 70, 160, LSTPZ3.getPreferredSize().height);

              //---- LSTPZ4 ----
              LSTPZ4.setComponentPopupMenu(BTD);
              LSTPZ4.setName("LSTPZ4");
              xTitledPanel1ContentContainer.add(LSTPZ4);
              LSTPZ4.setBounds(40, 95, 160, LSTPZ4.getPreferredSize().height);

              //---- LSTPZ5 ----
              LSTPZ5.setComponentPopupMenu(BTD);
              LSTPZ5.setName("LSTPZ5");
              xTitledPanel1ContentContainer.add(LSTPZ5);
              LSTPZ5.setBounds(40, 120, 160, LSTPZ5.getPreferredSize().height);

              //---- LSAFZ1 ----
              LSAFZ1.setText("");
              LSAFZ1.setToolTipText("article affect\u00e9 sur adresse");
              LSAFZ1.setComponentPopupMenu(BTD);
              LSAFZ1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSAFZ1.setName("LSAFZ1");
              xTitledPanel1ContentContainer.add(LSAFZ1);
              LSAFZ1.setBounds(221, 24, LSAFZ1.getPreferredSize().width, 20);

              //---- LSK01 ----
              LSK01.setText("");
              LSK01.setToolTipText("zone transtockeur");
              LSK01.setComponentPopupMenu(BTD);
              LSK01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSK01.setName("LSK01");
              xTitledPanel1ContentContainer.add(LSK01);
              LSK01.setBounds(276, 24, LSK01.getPreferredSize().width, 20);

              //---- LSO01 ----
              LSO01.setText("");
              LSO01.setToolTipText("last in first out (le dernier entr\u00e9 est le premier sorti)");
              LSO01.setComponentPopupMenu(BTD);
              LSO01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSO01.setName("LSO01");
              xTitledPanel1ContentContainer.add(LSO01);
              LSO01.setBounds(331, 24, LSO01.getPreferredSize().width, 20);

              //---- LSAFZ2 ----
              LSAFZ2.setText("");
              LSAFZ2.setToolTipText("article affect\u00e9 sur adresse");
              LSAFZ2.setComponentPopupMenu(BTD);
              LSAFZ2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSAFZ2.setName("LSAFZ2");
              xTitledPanel1ContentContainer.add(LSAFZ2);
              LSAFZ2.setBounds(221, 49, LSAFZ2.getPreferredSize().width, 20);

              //---- LSK02 ----
              LSK02.setText("");
              LSK02.setToolTipText("zone transtockeur");
              LSK02.setComponentPopupMenu(BTD);
              LSK02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSK02.setName("LSK02");
              xTitledPanel1ContentContainer.add(LSK02);
              LSK02.setBounds(276, 49, LSK02.getPreferredSize().width, 20);

              //---- LSO02 ----
              LSO02.setText("");
              LSO02.setToolTipText("last in first out (le dernier entr\u00e9 est le premier sorti)");
              LSO02.setComponentPopupMenu(BTD);
              LSO02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSO02.setName("LSO02");
              xTitledPanel1ContentContainer.add(LSO02);
              LSO02.setBounds(331, 49, LSO02.getPreferredSize().width, 20);

              //---- LSAFZ3 ----
              LSAFZ3.setText("");
              LSAFZ3.setToolTipText("article affect\u00e9 sur adresse");
              LSAFZ3.setComponentPopupMenu(BTD);
              LSAFZ3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSAFZ3.setName("LSAFZ3");
              xTitledPanel1ContentContainer.add(LSAFZ3);
              LSAFZ3.setBounds(221, 74, LSAFZ3.getPreferredSize().width, 20);

              //---- LSK03 ----
              LSK03.setText("");
              LSK03.setToolTipText("zone transtockeur");
              LSK03.setComponentPopupMenu(BTD);
              LSK03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSK03.setName("LSK03");
              xTitledPanel1ContentContainer.add(LSK03);
              LSK03.setBounds(276, 74, LSK03.getPreferredSize().width, 20);

              //---- LSO03 ----
              LSO03.setText("");
              LSO03.setToolTipText("last in first out (le dernier entr\u00e9 est le premier sorti)");
              LSO03.setComponentPopupMenu(BTD);
              LSO03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSO03.setName("LSO03");
              xTitledPanel1ContentContainer.add(LSO03);
              LSO03.setBounds(331, 74, LSO03.getPreferredSize().width, 20);

              //---- LSAFZ4 ----
              LSAFZ4.setText("");
              LSAFZ4.setToolTipText("article affect\u00e9 sur adresse");
              LSAFZ4.setComponentPopupMenu(BTD);
              LSAFZ4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSAFZ4.setName("LSAFZ4");
              xTitledPanel1ContentContainer.add(LSAFZ4);
              LSAFZ4.setBounds(221, 99, LSAFZ4.getPreferredSize().width, 20);

              //---- LSK04 ----
              LSK04.setText("");
              LSK04.setToolTipText("zone transtockeur");
              LSK04.setComponentPopupMenu(BTD);
              LSK04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSK04.setName("LSK04");
              xTitledPanel1ContentContainer.add(LSK04);
              LSK04.setBounds(276, 99, LSK04.getPreferredSize().width, 20);

              //---- LSO04 ----
              LSO04.setText("");
              LSO04.setToolTipText("last in first out (le dernier entr\u00e9 est le premier sorti)");
              LSO04.setComponentPopupMenu(BTD);
              LSO04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSO04.setName("LSO04");
              xTitledPanel1ContentContainer.add(LSO04);
              LSO04.setBounds(331, 99, LSO04.getPreferredSize().width, 20);

              //---- LSAFZ5 ----
              LSAFZ5.setText("");
              LSAFZ5.setToolTipText("article affect\u00e9 sur adresse");
              LSAFZ5.setComponentPopupMenu(BTD);
              LSAFZ5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSAFZ5.setName("LSAFZ5");
              xTitledPanel1ContentContainer.add(LSAFZ5);
              LSAFZ5.setBounds(221, 124, LSAFZ5.getPreferredSize().width, 20);

              //---- LSK05 ----
              LSK05.setText("");
              LSK05.setToolTipText("zone transtockeur");
              LSK05.setComponentPopupMenu(BTD);
              LSK05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSK05.setName("LSK05");
              xTitledPanel1ContentContainer.add(LSK05);
              LSK05.setBounds(276, 124, LSK05.getPreferredSize().width, 20);

              //---- LSO05 ----
              LSO05.setText("");
              LSO05.setToolTipText("last in first out (le dernier entr\u00e9 est le premier sorti)");
              LSO05.setComponentPopupMenu(BTD);
              LSO05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSO05.setName("LSO05");
              xTitledPanel1ContentContainer.add(LSO05);
              LSO05.setBounds(331, 124, LSO05.getPreferredSize().width, 20);

              //---- OBJ_102 ----
              OBJ_102.setText("1");
              OBJ_102.setFont(OBJ_102.getFont().deriveFont(OBJ_102.getFont().getStyle() | Font.BOLD));
              OBJ_102.setName("OBJ_102");
              xTitledPanel1ContentContainer.add(OBJ_102);
              OBJ_102.setBounds(15, 20, 20, 28);

              //---- OBJ_103 ----
              OBJ_103.setText("4");
              OBJ_103.setFont(OBJ_103.getFont().deriveFont(OBJ_103.getFont().getStyle() | Font.BOLD));
              OBJ_103.setName("OBJ_103");
              xTitledPanel1ContentContainer.add(OBJ_103);
              OBJ_103.setBounds(15, 95, 20, 28);

              //---- OBJ_104 ----
              OBJ_104.setText("2");
              OBJ_104.setFont(OBJ_104.getFont().deriveFont(OBJ_104.getFont().getStyle() | Font.BOLD));
              OBJ_104.setName("OBJ_104");
              xTitledPanel1ContentContainer.add(OBJ_104);
              OBJ_104.setBounds(15, 45, 20, 28);

              //---- OBJ_105 ----
              OBJ_105.setText("5");
              OBJ_105.setFont(OBJ_105.getFont().deriveFont(OBJ_105.getFont().getStyle() | Font.BOLD));
              OBJ_105.setName("OBJ_105");
              xTitledPanel1ContentContainer.add(OBJ_105);
              OBJ_105.setBounds(15, 120, 20, 28);

              //---- OBJ_106 ----
              OBJ_106.setText("3");
              OBJ_106.setFont(OBJ_106.getFont().deriveFont(OBJ_106.getFont().getStyle() | Font.BOLD));
              OBJ_106.setName("OBJ_106");
              xTitledPanel1ContentContainer.add(OBJ_106);
              OBJ_106.setBounds(15, 70, 20, 28);

              //---- label1 ----
              label1.setText("Aff. art.");
              label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
              label1.setHorizontalAlignment(SwingConstants.CENTER);
              label1.setName("label1");
              xTitledPanel1ContentContainer.add(label1);
              label1.setBounds(205, 0, 50, 25);

              //---- label2 ----
              label2.setText("Trans.");
              label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
              label2.setHorizontalAlignment(SwingConstants.CENTER);
              label2.setName("label2");
              xTitledPanel1ContentContainer.add(label2);
              label2.setBounds(260, 0, 50, 25);

              //---- label3 ----
              label3.setText("LIFO");
              label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
              label3.setHorizontalAlignment(SwingConstants.CENTER);
              label3.setName("label3");
              xTitledPanel1ContentContainer.add(label3);
              label3.setBounds(315, 0, 50, 25);

              //---- label4 ----
              label4.setText("Pr\u00e9l. unit.");
              label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
              label4.setHorizontalAlignment(SwingConstants.CENTER);
              label4.setName("label4");
              xTitledPanel1ContentContainer.add(label4);
              label4.setBounds(367, 0, 55, 25);

              //---- LSUZ1 ----
              LSUZ1.setText("");
              LSUZ1.setToolTipText("picking \u00e0 l'unit\u00e9 sur adresse affect\u00e9 \u00e0 l'article");
              LSUZ1.setComponentPopupMenu(BTD);
              LSUZ1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSUZ1.setName("LSUZ1");
              xTitledPanel1ContentContainer.add(LSUZ1);
              LSUZ1.setBounds(385, 24, LSUZ1.getPreferredSize().width, 20);

              //---- LSUZ2 ----
              LSUZ2.setText("");
              LSUZ2.setToolTipText("picking \u00e0 l'unit\u00e9 sur adresse affect\u00e9 \u00e0 l'article");
              LSUZ2.setComponentPopupMenu(BTD);
              LSUZ2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSUZ2.setName("LSUZ2");
              xTitledPanel1ContentContainer.add(LSUZ2);
              LSUZ2.setBounds(385, 49, LSUZ2.getPreferredSize().width, 20);

              //---- LSUZ3 ----
              LSUZ3.setText("");
              LSUZ3.setToolTipText("picking \u00e0 l'unit\u00e9 sur adresse affect\u00e9 \u00e0 l'article");
              LSUZ3.setComponentPopupMenu(BTD);
              LSUZ3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSUZ3.setName("LSUZ3");
              xTitledPanel1ContentContainer.add(LSUZ3);
              LSUZ3.setBounds(385, 74, LSUZ3.getPreferredSize().width, 20);

              //---- LSUZ4 ----
              LSUZ4.setText("");
              LSUZ4.setToolTipText("picking \u00e0 l'unit\u00e9 sur adresse affect\u00e9 \u00e0 l'article");
              LSUZ4.setComponentPopupMenu(BTD);
              LSUZ4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSUZ4.setName("LSUZ4");
              xTitledPanel1ContentContainer.add(LSUZ4);
              LSUZ4.setBounds(385, 99, LSUZ4.getPreferredSize().width, 20);

              //---- LSUZ5 ----
              LSUZ5.setText("");
              LSUZ5.setToolTipText("picking \u00e0 l'unit\u00e9 sur adresse affect\u00e9 \u00e0 l'article");
              LSUZ5.setComponentPopupMenu(BTD);
              LSUZ5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSUZ5.setName("LSUZ5");
              xTitledPanel1ContentContainer.add(LSUZ5);
              LSUZ5.setBounds(385, 124, LSUZ5.getPreferredSize().width, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel1ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel1.add(xTitledPanel1);
            xTitledPanel1.setBounds(10, 10, 480, 185);

            //======== xTitledPanel2 ========
            {
              xTitledPanel2.setTitle("Description des adresses de stockage");
              xTitledPanel2.setName("xTitledPanel2");
              Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
              xTitledPanel2ContentContainer.setLayout(null);

              //---- LSL1 ----
              LSL1.setComponentPopupMenu(BTD);
              LSL1.setName("LSL1");
              xTitledPanel2ContentContainer.add(LSL1);
              LSL1.setBounds(50, 20, 80, LSL1.getPreferredSize().height);

              //---- LSL2 ----
              LSL2.setComponentPopupMenu(BTD);
              LSL2.setName("LSL2");
              xTitledPanel2ContentContainer.add(LSL2);
              LSL2.setBounds(50, 53, 80, LSL2.getPreferredSize().height);

              //---- LSL3 ----
              LSL3.setComponentPopupMenu(BTD);
              LSL3.setName("LSL3");
              xTitledPanel2ContentContainer.add(LSL3);
              LSL3.setBounds(50, 86, 80, LSL3.getPreferredSize().height);

              //---- LSL4 ----
              LSL4.setComponentPopupMenu(BTD);
              LSL4.setName("LSL4");
              xTitledPanel2ContentContainer.add(LSL4);
              LSL4.setBounds(50, 119, 80, LSL4.getPreferredSize().height);

              //---- OBJ_107 ----
              OBJ_107.setText("1");
              OBJ_107.setFont(OBJ_107.getFont().deriveFont(OBJ_107.getFont().getStyle() | Font.BOLD));
              OBJ_107.setName("OBJ_107");
              xTitledPanel2ContentContainer.add(OBJ_107);
              OBJ_107.setBounds(15, 20, 20, 28);

              //---- OBJ_108 ----
              OBJ_108.setText("2");
              OBJ_108.setFont(OBJ_108.getFont().deriveFont(OBJ_108.getFont().getStyle() | Font.BOLD));
              OBJ_108.setName("OBJ_108");
              xTitledPanel2ContentContainer.add(OBJ_108);
              OBJ_108.setBounds(15, 53, 20, 28);

              //---- OBJ_109 ----
              OBJ_109.setText("3");
              OBJ_109.setFont(OBJ_109.getFont().deriveFont(OBJ_109.getFont().getStyle() | Font.BOLD));
              OBJ_109.setName("OBJ_109");
              xTitledPanel2ContentContainer.add(OBJ_109);
              OBJ_109.setBounds(15, 86, 20, 28);

              //---- OBJ_110 ----
              OBJ_110.setText("4");
              OBJ_110.setFont(OBJ_110.getFont().deriveFont(OBJ_110.getFont().getStyle() | Font.BOLD));
              OBJ_110.setName("OBJ_110");
              xTitledPanel2ContentContainer.add(OBJ_110);
              OBJ_110.setBounds(15, 119, 20, 28);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel2ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel1.add(xTitledPanel2);
            xTitledPanel2.setBounds(505, 10, 250, 185);

            //======== xTitledPanel3 ========
            {
              xTitledPanel3.setTitle("Priorit\u00e9s de s\u00e9lection des zones de stock");
              xTitledPanel3.setName("xTitledPanel3");
              Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
              xTitledPanel3ContentContainer.setLayout(null);

              //---- OBJ_114 ----
              OBJ_114.setText("Adresse incompl\u00e8te");
              OBJ_114.setName("OBJ_114");
              xTitledPanel3ContentContainer.add(OBJ_114);
              OBJ_114.setBounds(385, 20, 123, 28);

              //---- OBJ_118 ----
              OBJ_118.setText("Palette compl\u00e8te");
              OBJ_118.setName("OBJ_118");
              xTitledPanel3ContentContainer.add(OBJ_118);
              OBJ_118.setBounds(385, 50, 104, 28);

              //---- OBJ_113 ----
              OBJ_113.setText("En r\u00e9ception");
              OBJ_113.setFont(OBJ_113.getFont().deriveFont(OBJ_113.getFont().getStyle() | Font.BOLD));
              OBJ_113.setName("OBJ_113");
              xTitledPanel3ContentContainer.add(OBJ_113);
              OBJ_113.setBounds(40, 20, 175, 28);

              //---- OBJ_117 ----
              OBJ_117.setText("En exp\u00e9dition");
              OBJ_117.setFont(OBJ_117.getFont().deriveFont(OBJ_117.getFont().getStyle() | Font.BOLD));
              OBJ_117.setName("OBJ_117");
              xTitledPanel3ContentContainer.add(OBJ_117);
              OBJ_117.setBounds(40, 50, 180, 28);

              //---- LSP11 ----
              LSP11.setComponentPopupMenu(BTD);
              LSP11.setName("LSP11");
              xTitledPanel3ContentContainer.add(LSP11);
              LSP11.setBounds(220, 20, 20, LSP11.getPreferredSize().height);

              //---- LSP12 ----
              LSP12.setComponentPopupMenu(BTD);
              LSP12.setName("LSP12");
              xTitledPanel3ContentContainer.add(LSP12);
              LSP12.setBounds(258, 20, 20, LSP12.getPreferredSize().height);

              //---- LSP13 ----
              LSP13.setComponentPopupMenu(BTD);
              LSP13.setName("LSP13");
              xTitledPanel3ContentContainer.add(LSP13);
              LSP13.setBounds(296, 20, 20, LSP13.getPreferredSize().height);

              //---- LSP14 ----
              LSP14.setComponentPopupMenu(BTD);
              LSP14.setName("LSP14");
              xTitledPanel3ContentContainer.add(LSP14);
              LSP14.setBounds(334, 20, 20, LSP14.getPreferredSize().height);

              //---- LSP21 ----
              LSP21.setComponentPopupMenu(BTD);
              LSP21.setName("LSP21");
              xTitledPanel3ContentContainer.add(LSP21);
              LSP21.setBounds(220, 50, 20, LSP21.getPreferredSize().height);

              //---- LSP22 ----
              LSP22.setComponentPopupMenu(BTD);
              LSP22.setName("LSP22");
              xTitledPanel3ContentContainer.add(LSP22);
              LSP22.setBounds(258, 50, 20, LSP22.getPreferredSize().height);

              //---- LSP23 ----
              LSP23.setComponentPopupMenu(BTD);
              LSP23.setName("LSP23");
              xTitledPanel3ContentContainer.add(LSP23);
              LSP23.setBounds(296, 50, 20, LSP23.getPreferredSize().height);

              //---- LSP24 ----
              LSP24.setComponentPopupMenu(BTD);
              LSP24.setName("LSP24");
              xTitledPanel3ContentContainer.add(LSP24);
              LSP24.setBounds(334, 50, 20, LSP24.getPreferredSize().height);

              //---- LSP31 ----
              LSP31.setComponentPopupMenu(BTD);
              LSP31.setName("LSP31");
              xTitledPanel3ContentContainer.add(LSP31);
              LSP31.setBounds(550, 20, 20, LSP31.getPreferredSize().height);

              //---- LSP32 ----
              LSP32.setComponentPopupMenu(BTD);
              LSP32.setName("LSP32");
              xTitledPanel3ContentContainer.add(LSP32);
              LSP32.setBounds(590, 20, 20, LSP32.getPreferredSize().height);

              //---- LSP33 ----
              LSP33.setComponentPopupMenu(BTD);
              LSP33.setName("LSP33");
              xTitledPanel3ContentContainer.add(LSP33);
              LSP33.setBounds(630, 20, 20, LSP33.getPreferredSize().height);

              //---- LSP41 ----
              LSP41.setComponentPopupMenu(BTD);
              LSP41.setName("LSP41");
              xTitledPanel3ContentContainer.add(LSP41);
              LSP41.setBounds(550, 50, 20, LSP41.getPreferredSize().height);

              //---- LSP42 ----
              LSP42.setComponentPopupMenu(BTD);
              LSP42.setName("LSP42");
              xTitledPanel3ContentContainer.add(LSP42);
              LSP42.setBounds(590, 50, 20, LSP42.getPreferredSize().height);

              //---- LSP43 ----
              LSP43.setComponentPopupMenu(BTD);
              LSP43.setName("LSP43");
              xTitledPanel3ContentContainer.add(LSP43);
              LSP43.setBounds(630, 50, 20, LSP43.getPreferredSize().height);

              //---- separator1 ----
              separator1.setName("separator1");
              xTitledPanel3ContentContainer.add(separator1);
              separator1.setBounds(220, 0, 132, 22);

              //---- separator4 ----
              separator4.setName("separator4");
              xTitledPanel3ContentContainer.add(separator4);
              separator4.setBounds(25, 0, 155, 22);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel3ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel1.add(xTitledPanel3);
            xTitledPanel3.setBounds(10, 200, 745, 115);

            //======== xTitledPanel4 ========
            {
              xTitledPanel4.setTitle("Flux r\u00e9approvisionnement des zones de stockage");
              xTitledPanel4.setName("xTitledPanel4");
              Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
              xTitledPanel4ContentContainer.setLayout(null);

              //---- OBJ_134 ----
              OBJ_134.setText("Cinqui\u00e8me transfert");
              OBJ_134.setName("OBJ_134");
              xTitledPanel4ContentContainer.add(OBJ_134);
              OBJ_134.setBounds(385, 59, 117, 20);

              //---- OBJ_132 ----
              OBJ_132.setText("Quatri\u00e8me transfert");
              OBJ_132.setName("OBJ_132");
              xTitledPanel4ContentContainer.add(OBJ_132);
              OBJ_132.setBounds(385, 34, 115, 20);

              //---- OBJ_133 ----
              OBJ_133.setText("Deuxi\u00e8me transfert");
              OBJ_133.setName("OBJ_133");
              xTitledPanel4ContentContainer.add(OBJ_133);
              OBJ_133.setBounds(40, 59, 114, 20);

              //---- OBJ_136 ----
              OBJ_136.setText("Troisi\u00e8me transfert");
              OBJ_136.setName("OBJ_136");
              xTitledPanel4ContentContainer.add(OBJ_136);
              OBJ_136.setBounds(40, 84, 114, 20);

              //---- OBJ_137 ----
              OBJ_137.setText("Sixi\u00e8me transfert");
              OBJ_137.setName("OBJ_137");
              xTitledPanel4ContentContainer.add(OBJ_137);
              OBJ_137.setBounds(385, 84, 114, 20);

              //---- OBJ_131 ----
              OBJ_131.setText("Premier transfert");
              OBJ_131.setName("OBJ_131");
              xTitledPanel4ContentContainer.add(OBJ_131);
              OBJ_131.setBounds(40, 34, 100, 20);

              //---- OBJ_126 ----
              OBJ_126.setText("Arriv\u00e9e");
              OBJ_126.setFont(OBJ_126.getFont().deriveFont(OBJ_126.getFont().getSize() - 1f));
              OBJ_126.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_126.setName("OBJ_126");
              xTitledPanel4ContentContainer.add(OBJ_126);
              OBJ_126.setBounds(250, 5, 40, 23);

              //---- OBJ_130 ----
              OBJ_130.setText("Arriv\u00e9e");
              OBJ_130.setFont(OBJ_130.getFont().deriveFont(OBJ_130.getFont().getSize() - 1f));
              OBJ_130.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_130.setName("OBJ_130");
              xTitledPanel4ContentContainer.add(OBJ_130);
              OBJ_130.setBounds(580, 5, 40, 23);

              //---- OBJ_124 ----
              OBJ_124.setText("D\u00e9part");
              OBJ_124.setFont(OBJ_124.getFont().deriveFont(OBJ_124.getFont().getSize() - 1f));
              OBJ_124.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_124.setName("OBJ_124");
              xTitledPanel4ContentContainer.add(OBJ_124);
              OBJ_124.setBounds(213, 5, 35, 23);

              //---- OBJ_129 ----
              OBJ_129.setText("D\u00e9part");
              OBJ_129.setFont(OBJ_129.getFont().deriveFont(OBJ_129.getFont().getSize() - 1f));
              OBJ_129.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_129.setName("OBJ_129");
              xTitledPanel4ContentContainer.add(OBJ_129);
              OBJ_129.setBounds(543, 5, 35, 23);

              //---- OBJ_143 ----
              OBJ_143.setText("Nb");
              OBJ_143.setFont(OBJ_143.getFont().deriveFont(OBJ_143.getFont().getSize() - 1f));
              OBJ_143.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_143.setName("OBJ_143");
              xTitledPanel4ContentContainer.add(OBJ_143);
              OBJ_143.setBounds(295, 5, 21, 23);

              //---- LSF1D ----
              LSF1D.setComponentPopupMenu(BTD);
              LSF1D.setName("LSF1D");
              xTitledPanel4ContentContainer.add(LSF1D);
              LSF1D.setBounds(220, 30, 20, LSF1D.getPreferredSize().height);

              //---- LSF1A ----
              LSF1A.setComponentPopupMenu(BTD);
              LSF1A.setName("LSF1A");
              xTitledPanel4ContentContainer.add(LSF1A);
              LSF1A.setBounds(260, 30, 20, LSF1A.getPreferredSize().height);

              //---- LSM01 ----
              LSM01.setComponentPopupMenu(BTD);
              LSM01.setName("LSM01");
              xTitledPanel4ContentContainer.add(LSM01);
              LSM01.setBounds(295, 30, 20, LSM01.getPreferredSize().height);

              //---- LSF2D ----
              LSF2D.setComponentPopupMenu(BTD);
              LSF2D.setName("LSF2D");
              xTitledPanel4ContentContainer.add(LSF2D);
              LSF2D.setBounds(220, 55, 20, LSF2D.getPreferredSize().height);

              //---- LSF2A ----
              LSF2A.setComponentPopupMenu(BTD);
              LSF2A.setName("LSF2A");
              xTitledPanel4ContentContainer.add(LSF2A);
              LSF2A.setBounds(260, 55, 20, LSF2A.getPreferredSize().height);

              //---- LSM02 ----
              LSM02.setComponentPopupMenu(BTD);
              LSM02.setName("LSM02");
              xTitledPanel4ContentContainer.add(LSM02);
              LSM02.setBounds(295, 55, 20, LSM02.getPreferredSize().height);

              //---- LSF3D ----
              LSF3D.setComponentPopupMenu(BTD);
              LSF3D.setName("LSF3D");
              xTitledPanel4ContentContainer.add(LSF3D);
              LSF3D.setBounds(220, 80, 20, LSF3D.getPreferredSize().height);

              //---- LSF3A ----
              LSF3A.setComponentPopupMenu(BTD);
              LSF3A.setName("LSF3A");
              xTitledPanel4ContentContainer.add(LSF3A);
              LSF3A.setBounds(260, 80, 20, LSF3A.getPreferredSize().height);

              //---- LSM03 ----
              LSM03.setComponentPopupMenu(BTD);
              LSM03.setName("LSM03");
              xTitledPanel4ContentContainer.add(LSM03);
              LSM03.setBounds(295, 80, 20, LSM03.getPreferredSize().height);

              //---- LSF4D ----
              LSF4D.setComponentPopupMenu(BTD);
              LSF4D.setName("LSF4D");
              xTitledPanel4ContentContainer.add(LSF4D);
              LSF4D.setBounds(550, 30, 20, LSF4D.getPreferredSize().height);

              //---- LSF4A ----
              LSF4A.setComponentPopupMenu(BTD);
              LSF4A.setName("LSF4A");
              xTitledPanel4ContentContainer.add(LSF4A);
              LSF4A.setBounds(590, 30, 20, LSF4A.getPreferredSize().height);

              //---- LSM04 ----
              LSM04.setComponentPopupMenu(BTD);
              LSM04.setName("LSM04");
              xTitledPanel4ContentContainer.add(LSM04);
              LSM04.setBounds(630, 30, 20, LSM04.getPreferredSize().height);

              //---- LSF5D ----
              LSF5D.setComponentPopupMenu(BTD);
              LSF5D.setName("LSF5D");
              xTitledPanel4ContentContainer.add(LSF5D);
              LSF5D.setBounds(550, 55, 20, LSF5D.getPreferredSize().height);

              //---- LSF5A ----
              LSF5A.setComponentPopupMenu(BTD);
              LSF5A.setName("LSF5A");
              xTitledPanel4ContentContainer.add(LSF5A);
              LSF5A.setBounds(590, 55, 20, LSF5A.getPreferredSize().height);

              //---- LSM05 ----
              LSM05.setComponentPopupMenu(BTD);
              LSM05.setName("LSM05");
              xTitledPanel4ContentContainer.add(LSM05);
              LSM05.setBounds(630, 55, 20, LSM05.getPreferredSize().height);

              //---- LSF6D ----
              LSF6D.setComponentPopupMenu(BTD);
              LSF6D.setName("LSF6D");
              xTitledPanel4ContentContainer.add(LSF6D);
              LSF6D.setBounds(550, 80, 20, LSF6D.getPreferredSize().height);

              //---- LSF6A ----
              LSF6A.setComponentPopupMenu(BTD);
              LSF6A.setName("LSF6A");
              xTitledPanel4ContentContainer.add(LSF6A);
              LSF6A.setBounds(590, 80, 20, LSF6A.getPreferredSize().height);

              //---- LSM06 ----
              LSM06.setComponentPopupMenu(BTD);
              LSM06.setName("LSM06");
              xTitledPanel4ContentContainer.add(LSM06);
              LSM06.setBounds(630, 80, 20, LSM06.getPreferredSize().height);

              //---- OBJ_144 ----
              OBJ_144.setText("Nb");
              OBJ_144.setFont(OBJ_144.getFont().deriveFont(OBJ_144.getFont().getSize() - 1f));
              OBJ_144.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_144.setName("OBJ_144");
              xTitledPanel4ContentContainer.add(OBJ_144);
              OBJ_144.setBounds(630, 5, 21, 23);

              //---- separator2 ----
              separator2.setName("separator2");
              xTitledPanel4ContentContainer.add(separator2);
              separator2.setBounds(40, 5, 150, 22);

              //---- separator3 ----
              separator3.setName("separator3");
              xTitledPanel4ContentContainer.add(separator3);
              separator3.setBounds(385, 5, 150, 22);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel4ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel1.add(xTitledPanel4);
            xTitledPanel4.setBounds(10, 320, 745, 140);

            //======== xTitledPanel5 ========
            {
              xTitledPanel5.setTitle("Divers");
              xTitledPanel5.setName("xTitledPanel5");
              Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
              xTitledPanel5ContentContainer.setLayout(null);

              //---- LSPENT ----
              LSPENT.setText("Gestion des palettes entam\u00e9es");
              LSPENT.setComponentPopupMenu(BTD);
              LSPENT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSPENT.setName("LSPENT");
              LSPENT.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  LSPENTActionPerformed(e);
                }
              });
              xTitledPanel5ContentContainer.add(LSPENT);
              LSPENT.setBounds(15, 35, 260, 25);

              //---- LSLEB ----
              LSLEB.setComponentPopupMenu(BTD);
              LSLEB.setName("LSLEB");
              xTitledPanel5ContentContainer.add(LSLEB);
              LSLEB.setBounds(13, 8, 160, LSLEB.getPreferredSize().height);

              //---- LSQEB ----
              LSQEB.setComponentPopupMenu(BTD);
              LSQEB.setName("LSQEB");
              xTitledPanel5ContentContainer.add(LSQEB);
              LSQEB.setBounds(173, 8, 40, LSQEB.getPreferredSize().height);

              //---- LSLMO ----
              LSLMO.setComponentPopupMenu(BTD);
              LSLMO.setName("LSLMO");
              xTitledPanel5ContentContainer.add(LSLMO);
              LSLMO.setBounds(269, 8, 160, LSLMO.getPreferredSize().height);

              //---- LSQMO ----
              LSQMO.setComponentPopupMenu(BTD);
              LSQMO.setName("LSQMO");
              xTitledPanel5ContentContainer.add(LSQMO);
              LSQMO.setBounds(430, 8, 40, LSQMO.getPreferredSize().height);

              //---- LSLFB ----
              LSLFB.setComponentPopupMenu(BTD);
              LSLFB.setName("LSLFB");
              xTitledPanel5ContentContainer.add(LSLFB);
              LSLFB.setBounds(525, 8, 160, LSLFB.getPreferredSize().height);

              //---- LSQFB ----
              LSQFB.setComponentPopupMenu(BTD);
              LSQFB.setName("LSQFB");
              xTitledPanel5ContentContainer.add(LSQFB);
              LSQFB.setBounds(685, 8, 40, LSQFB.getPreferredSize().height);

              //---- LSPPIE ----
              LSPPIE.setText("Si gestion des palettes entam\u00e9es : interdiction de pr\u00e9l\u00e8vement \u00e0 l'unit\u00e9");
              LSPPIE.setComponentPopupMenu(BTD);
              LSPPIE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              LSPPIE.setName("LSPPIE");
              xTitledPanel5ContentContainer.add(LSPPIE);
              LSPPIE.setBounds(269, 35, 455, 25);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < xTitledPanel5ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel5ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel5ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel5ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel5ContentContainer.setPreferredSize(preferredSize);
              }
            }
            panel1.add(xTitledPanel5);
            xTitledPanel5.setBounds(10, 465, 745, 105);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 561, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField WETB;
  private JLabel OBJ_33;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField LSTPZ1;
  private XRiTextField LSTPZ2;
  private XRiTextField LSTPZ3;
  private XRiTextField LSTPZ4;
  private XRiTextField LSTPZ5;
  private XRiCheckBox LSAFZ1;
  private XRiCheckBox LSK01;
  private XRiCheckBox LSO01;
  private XRiCheckBox LSAFZ2;
  private XRiCheckBox LSK02;
  private XRiCheckBox LSO02;
  private XRiCheckBox LSAFZ3;
  private XRiCheckBox LSK03;
  private XRiCheckBox LSO03;
  private XRiCheckBox LSAFZ4;
  private XRiCheckBox LSK04;
  private XRiCheckBox LSO04;
  private XRiCheckBox LSAFZ5;
  private XRiCheckBox LSK05;
  private XRiCheckBox LSO05;
  private JLabel OBJ_102;
  private JLabel OBJ_103;
  private JLabel OBJ_104;
  private JLabel OBJ_105;
  private JLabel OBJ_106;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private XRiCheckBox LSUZ1;
  private XRiCheckBox LSUZ2;
  private XRiCheckBox LSUZ3;
  private XRiCheckBox LSUZ4;
  private XRiCheckBox LSUZ5;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField LSL1;
  private XRiTextField LSL2;
  private XRiTextField LSL3;
  private XRiTextField LSL4;
  private JLabel OBJ_107;
  private JLabel OBJ_108;
  private JLabel OBJ_109;
  private JLabel OBJ_110;
  private JXTitledPanel xTitledPanel3;
  private JLabel OBJ_114;
  private JLabel OBJ_118;
  private JLabel OBJ_113;
  private JLabel OBJ_117;
  private XRiTextField LSP11;
  private XRiTextField LSP12;
  private XRiTextField LSP13;
  private XRiTextField LSP14;
  private XRiTextField LSP21;
  private XRiTextField LSP22;
  private XRiTextField LSP23;
  private XRiTextField LSP24;
  private XRiTextField LSP31;
  private XRiTextField LSP32;
  private XRiTextField LSP33;
  private XRiTextField LSP41;
  private XRiTextField LSP42;
  private XRiTextField LSP43;
  private JComponent separator1;
  private JComponent separator4;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_134;
  private JLabel OBJ_132;
  private JLabel OBJ_133;
  private JLabel OBJ_136;
  private JLabel OBJ_137;
  private JLabel OBJ_131;
  private JLabel OBJ_126;
  private JLabel OBJ_130;
  private JLabel OBJ_124;
  private JLabel OBJ_129;
  private JLabel OBJ_143;
  private XRiTextField LSF1D;
  private XRiTextField LSF1A;
  private XRiTextField LSM01;
  private XRiTextField LSF2D;
  private XRiTextField LSF2A;
  private XRiTextField LSM02;
  private XRiTextField LSF3D;
  private XRiTextField LSF3A;
  private XRiTextField LSM03;
  private XRiTextField LSF4D;
  private XRiTextField LSF4A;
  private XRiTextField LSM04;
  private XRiTextField LSF5D;
  private XRiTextField LSF5A;
  private XRiTextField LSM05;
  private XRiTextField LSF6D;
  private XRiTextField LSF6A;
  private XRiTextField LSM06;
  private JLabel OBJ_144;
  private JComponent separator2;
  private JComponent separator3;
  private JXTitledPanel xTitledPanel5;
  private XRiCheckBox LSPENT;
  private XRiTextField LSLEB;
  private XRiTextField LSQEB;
  private XRiTextField LSLMO;
  private XRiTextField LSQMO;
  private XRiTextField LSLFB;
  private XRiTextField LSQFB;
  private XRiCheckBox LSPPIE;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
