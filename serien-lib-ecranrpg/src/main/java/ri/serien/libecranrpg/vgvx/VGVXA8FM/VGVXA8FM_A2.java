
package ri.serien.libecranrpg.vgvx.VGVXA8FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGVXA8FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 541, };
  
  public VGVXA8FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LSL1R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL1R@")).trim());
    LSL2R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL2R@")).trim());
    LSL3R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL3R@")).trim());
    LSL4R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL4R@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    String titres = lexique.HostFieldGetData("HLD01");
    AD1.setText(titres.substring(10, 14));
    AD2.setText(titres.substring(14, 18));
    AD3.setText(titres.substring(18, 22));
    AD4.setText(titres.substring(22, 26));
    
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void DetailLotActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    INDETB = new XRiTextField();
    OBJ_57 = new JLabel();
    OBJ_67 = new JLabel();
    WMAG = new XRiTextField();
    WAD1 = new XRiTextField();
    WAD2 = new XRiTextField();
    WAD3 = new XRiTextField();
    WAD4 = new XRiTextField();
    LSL1R = new JLabel();
    LSL2R = new JLabel();
    LSL3R = new JLabel();
    LSL4R = new JLabel();
    OBJ_58 = new JLabel();
    WETB1 = new XRiTextField();
    WCPE = new XRiTextField();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel2 = new JPanel();
    INDCPE = new XRiTextField();
    INDET1 = new XRiTextField();
    INDMAG = new XRiTextField();
    INDAD1 = new XRiTextField();
    INDAD2 = new XRiTextField();
    INDAD3 = new XRiTextField();
    INDAD4 = new XRiTextField();
    OBJ_68 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_70 = new JLabel();
    AD1 = new JLabel();
    AD2 = new JLabel();
    AD3 = new JLabel();
    AD4 = new JLabel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    DetailLot = new JMenuItem();
    OBJ_27 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des adresses interdites");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(120, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_57 ----
          OBJ_57.setText("Etablissement");
          OBJ_57.setName("OBJ_57");
          p_tete_gauche.add(OBJ_57);
          OBJ_57.setBounds(5, 0, 90, 28);

          //---- OBJ_67 ----
          OBJ_67.setText("Magasin");
          OBJ_67.setName("OBJ_67");
          p_tete_gauche.add(OBJ_67);
          OBJ_67.setBounds(420, 0, 55, 28);

          //---- WMAG ----
          WMAG.setComponentPopupMenu(BTD);
          WMAG.setName("WMAG");
          p_tete_gauche.add(WMAG);
          WMAG.setBounds(475, 0, 34, WMAG.getPreferredSize().height);

          //---- WAD1 ----
          WAD1.setComponentPopupMenu(BTD);
          WAD1.setName("WAD1");
          p_tete_gauche.add(WAD1);
          WAD1.setBounds(555, 0, 40, WAD1.getPreferredSize().height);

          //---- WAD2 ----
          WAD2.setComponentPopupMenu(BTD);
          WAD2.setName("WAD2");
          p_tete_gauche.add(WAD2);
          WAD2.setBounds(640, 0, 40, WAD2.getPreferredSize().height);

          //---- WAD3 ----
          WAD3.setComponentPopupMenu(BTD);
          WAD3.setName("WAD3");
          p_tete_gauche.add(WAD3);
          WAD3.setBounds(725, 0, 40, WAD3.getPreferredSize().height);

          //---- WAD4 ----
          WAD4.setComponentPopupMenu(BTD);
          WAD4.setName("WAD4");
          p_tete_gauche.add(WAD4);
          WAD4.setBounds(810, 0, 40, WAD4.getPreferredSize().height);

          //---- LSL1R ----
          LSL1R.setText("@LSL1R@");
          LSL1R.setName("LSL1R");
          p_tete_gauche.add(LSL1R);
          LSL1R.setBounds(515, 0, 40, 28);

          //---- LSL2R ----
          LSL2R.setText("@LSL2R@");
          LSL2R.setName("LSL2R");
          p_tete_gauche.add(LSL2R);
          LSL2R.setBounds(600, 0, 40, 28);

          //---- LSL3R ----
          LSL3R.setText("@LSL3R@");
          LSL3R.setName("LSL3R");
          p_tete_gauche.add(LSL3R);
          LSL3R.setBounds(685, 0, 40, 28);

          //---- LSL4R ----
          LSL4R.setText("@LSL4R@");
          LSL4R.setName("LSL4R");
          p_tete_gauche.add(LSL4R);
          LSL4R.setBounds(770, 0, 40, 28);

          //---- OBJ_58 ----
          OBJ_58.setText("Etabl.");
          OBJ_58.setName("OBJ_58");
          p_tete_gauche.add(OBJ_58);
          OBJ_58.setBounds(320, 0, 50, 28);

          //---- WETB1 ----
          WETB1.setComponentPopupMenu(BTD);
          WETB1.setName("WETB1");
          p_tete_gauche.add(WETB1);
          WETB1.setBounds(370, 0, 40, WETB1.getPreferredSize().height);

          //---- WCPE ----
          WCPE.setComponentPopupMenu(BTD);
          WCPE.setName("WCPE");
          p_tete_gauche.add(WCPE);
          WCPE.setBounds(245, 0, 40, WCPE.getPreferredSize().height);

          //---- OBJ_59 ----
          OBJ_59.setText("Code PE");
          OBJ_59.setName("OBJ_59");
          p_tete_gauche.add(OBJ_59);
          OBJ_59.setBounds(185, 0, 60, 28);

          //---- OBJ_60 ----
          OBJ_60.setText("/");
          OBJ_60.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_60.setName("OBJ_60");
          p_tete_gauche.add(OBJ_60);
          OBJ_60.setBounds(295, 0, 15, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(670, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(25, 150, 558, 269);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(590, 150, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(590, 295, 25, 125);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Saisie"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- INDCPE ----
              INDCPE.setComponentPopupMenu(BTD);
              INDCPE.setName("INDCPE");
              panel2.add(INDCPE);
              INDCPE.setBounds(20, 50, 40, INDCPE.getPreferredSize().height);

              //---- INDET1 ----
              INDET1.setComponentPopupMenu(BTD);
              INDET1.setName("INDET1");
              panel2.add(INDET1);
              INDET1.setBounds(85, 50, 40, INDET1.getPreferredSize().height);

              //---- INDMAG ----
              INDMAG.setComponentPopupMenu(BTD);
              INDMAG.setName("INDMAG");
              panel2.add(INDMAG);
              INDMAG.setBounds(130, 50, 34, INDMAG.getPreferredSize().height);

              //---- INDAD1 ----
              INDAD1.setComponentPopupMenu(BTD);
              INDAD1.setName("INDAD1");
              panel2.add(INDAD1);
              INDAD1.setBounds(185, 50, 40, INDAD1.getPreferredSize().height);

              //---- INDAD2 ----
              INDAD2.setComponentPopupMenu(BTD);
              INDAD2.setName("INDAD2");
              panel2.add(INDAD2);
              INDAD2.setBounds(238, 50, 40, INDAD2.getPreferredSize().height);

              //---- INDAD3 ----
              INDAD3.setComponentPopupMenu(BTD);
              INDAD3.setName("INDAD3");
              panel2.add(INDAD3);
              INDAD3.setBounds(291, 50, 40, INDAD3.getPreferredSize().height);

              //---- INDAD4 ----
              INDAD4.setComponentPopupMenu(BTD);
              INDAD4.setName("INDAD4");
              panel2.add(INDAD4);
              INDAD4.setBounds(344, 50, 40, INDAD4.getPreferredSize().height);

              //---- OBJ_68 ----
              OBJ_68.setText("Mag");
              OBJ_68.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_68.setFont(OBJ_68.getFont().deriveFont(OBJ_68.getFont().getStyle() | Font.BOLD));
              OBJ_68.setName("OBJ_68");
              panel2.add(OBJ_68);
              OBJ_68.setBounds(130, 25, 34, 28);

              //---- OBJ_69 ----
              OBJ_69.setText("PE");
              OBJ_69.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_69.setFont(OBJ_69.getFont().deriveFont(OBJ_69.getFont().getStyle() | Font.BOLD));
              OBJ_69.setName("OBJ_69");
              panel2.add(OBJ_69);
              OBJ_69.setBounds(20, 25, 40, 28);

              //---- OBJ_70 ----
              OBJ_70.setText("Etb");
              OBJ_70.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_70.setFont(OBJ_70.getFont().deriveFont(OBJ_70.getFont().getStyle() | Font.BOLD));
              OBJ_70.setName("OBJ_70");
              panel2.add(OBJ_70);
              OBJ_70.setBounds(85, 25, 40, 28);

              //---- AD1 ----
              AD1.setText("AD1");
              AD1.setHorizontalAlignment(SwingConstants.CENTER);
              AD1.setFont(AD1.getFont().deriveFont(AD1.getFont().getStyle() | Font.BOLD));
              AD1.setName("AD1");
              panel2.add(AD1);
              AD1.setBounds(185, 25, 40, 28);

              //---- AD2 ----
              AD2.setText("AD2");
              AD2.setHorizontalAlignment(SwingConstants.CENTER);
              AD2.setFont(AD2.getFont().deriveFont(AD2.getFont().getStyle() | Font.BOLD));
              AD2.setName("AD2");
              panel2.add(AD2);
              AD2.setBounds(238, 25, 40, 28);

              //---- AD3 ----
              AD3.setText("AD3");
              AD3.setHorizontalAlignment(SwingConstants.CENTER);
              AD3.setFont(AD3.getFont().deriveFont(AD3.getFont().getStyle() | Font.BOLD));
              AD3.setName("AD3");
              panel2.add(AD3);
              AD3.setBounds(291, 25, 40, 28);

              //---- AD4 ----
              AD4.setText("AD4");
              AD4.setHorizontalAlignment(SwingConstants.CENTER);
              AD4.setFont(AD4.getFont().deriveFont(AD4.getFont().getStyle() | Font.BOLD));
              AD4.setName("AD4");
              panel2.add(AD4);
              AD4.setBounds(344, 25, 40, 28);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(25, 40, 555, 100);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 644, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- DetailLot ----
      DetailLot.setText("Supprimer");
      DetailLot.setName("DetailLot");
      DetailLot.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          DetailLotActionPerformed(e);
        }
      });
      BTD.add(DetailLot);
      BTD.addSeparator();

      //---- OBJ_27 ----
      OBJ_27.setText("Aide en ligne");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField INDETB;
  private JLabel OBJ_57;
  private JLabel OBJ_67;
  private XRiTextField WMAG;
  private XRiTextField WAD1;
  private XRiTextField WAD2;
  private XRiTextField WAD3;
  private XRiTextField WAD4;
  private JLabel LSL1R;
  private JLabel LSL2R;
  private JLabel LSL3R;
  private JLabel LSL4R;
  private JLabel OBJ_58;
  private XRiTextField WETB1;
  private XRiTextField WCPE;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel2;
  private XRiTextField INDCPE;
  private XRiTextField INDET1;
  private XRiTextField INDMAG;
  private XRiTextField INDAD1;
  private XRiTextField INDAD2;
  private XRiTextField INDAD3;
  private XRiTextField INDAD4;
  private JLabel OBJ_68;
  private JLabel OBJ_69;
  private JLabel OBJ_70;
  private JLabel AD1;
  private JLabel AD2;
  private JLabel AD3;
  private JLabel AD4;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem DetailLot;
  private JMenuItem OBJ_27;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
