
package ri.serien.libecranrpg.vgvx.VGVXEQ1F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXEQ1F_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXEQ1F_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WEXE5.setValeursSelection("O", "N");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    WMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAG@")).trim());
    WUSER.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUSER@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_80.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLMAG5@")).trim());
    OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WFILE5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    WNPAR5.setEnabled(lexique.isPresent("WNPAR5"));
    WCOPY5.setEnabled(lexique.isPresent("WCOPY5"));
    WMAG5.setEnabled(lexique.isPresent("WMAG5"));
    WTYP5.setEnabled(lexique.isPresent("WTYP5"));
    WNETQ5.setEnabled(lexique.isPresent("WNETQ5"));
    WSUF5.setEnabled(lexique.isPresent("WSUF5"));
    WPGM5.setEnabled(lexique.isPresent("WPGM5"));
    OBJ_58.setVisible(lexique.isPresent("WFILE5"));
    // WEXE5.setEnabled( lexique.isPresent("WEXE5"));
    // WEXE5.setSelected(lexique.HostFieldGetData("WEXE5").equalsIgnoreCase("O"));
    WUSER5.setEnabled(lexique.isPresent("WUSER5"));
    WPAR25.setEnabled(lexique.isPresent("WPAR25"));
    WPAR15.setEnabled(lexique.isPresent("WPAR15"));
    WLETQ5.setEnabled(lexique.isPresent("WLETQ5"));
    OBJ_80.setVisible(lexique.isPresent("WLMAG5"));
    WPRT5.setEnabled(lexique.isPresent("WPRT5"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WEXE5.isSelected())
    // lexique.HostFieldPutData("WEXE5", 0, "O");
    // else
    // lexique.HostFieldPutData("WEXE5", 0, "N");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_53 = new JLabel();
    WETB = new RiZoneSortie();
    OBJ_54 = new JLabel();
    WMAG = new RiZoneSortie();
    OBJ_62 = new JLabel();
    WUSER = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WPRT5 = new XRiTextField();
    OBJ_80 = new RiZoneSortie();
    OBJ_83 = new JLabel();
    WLETQ5 = new XRiTextField();
    OBJ_71 = new JLabel();
    WPAR15 = new XRiTextField();
    WPAR25 = new XRiTextField();
    WUSER5 = new XRiTextField();
    WEXE5 = new XRiCheckBox();
    OBJ_58 = new RiZoneSortie();
    WPGM5 = new XRiTextField();
    OBJ_74 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_59 = new JLabel();
    WSUF5 = new XRiTextField();
    WNETQ5 = new XRiTextField();
    WTYP5 = new XRiTextField();
    WMAG5 = new XRiTextField();
    OBJ_63 = new JLabel();
    WCOPY5 = new XRiTextField();
    WNPAR5 = new XRiTextField();
    OBJ_57 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_64 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1040, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_53 ----
          OBJ_53.setText("Etablissement");
          OBJ_53.setName("OBJ_53");

          //---- WETB ----
          WETB.setText("@WETB@");
          WETB.setOpaque(false);
          WETB.setName("WETB");

          //---- OBJ_54 ----
          OBJ_54.setText("Magasin");
          OBJ_54.setName("OBJ_54");

          //---- WMAG ----
          WMAG.setText("@WMAG@");
          WMAG.setOpaque(false);
          WMAG.setName("WMAG");

          //---- OBJ_62 ----
          OBJ_62.setText("Utilisateur");
          OBJ_62.setName("OBJ_62");

          //---- WUSER ----
          WUSER.setText("@WUSER@");
          WUSER.setOpaque(false);
          WUSER.setName("WUSER");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addComponent(WMAG, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addComponent(WUSER, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WMAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WUSER, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(840, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WPRT5 ----
            WPRT5.setComponentPopupMenu(null);
            WPRT5.setName("WPRT5");
            panel1.add(WPRT5);
            WPRT5.setBounds(10, 335, 775, WPRT5.getPreferredSize().height);

            //---- OBJ_80 ----
            OBJ_80.setText("@WLMAG5@");
            OBJ_80.setName("OBJ_80");
            panel1.add(OBJ_80);
            OBJ_80.setBounds(240, 239, 231, OBJ_80.getPreferredSize().height);

            //---- OBJ_83 ----
            OBJ_83.setText("Nom de l'imprimante sous Windows");
            OBJ_83.setName("OBJ_83");
            panel1.add(OBJ_83);
            OBJ_83.setBounds(10, 310, 405, 20);

            //---- WLETQ5 ----
            WLETQ5.setComponentPopupMenu(null);
            WLETQ5.setName("WLETQ5");
            panel1.add(WLETQ5);
            WLETQ5.setBounds(475, 105, 310, WLETQ5.getPreferredSize().height);

            //---- OBJ_71 ----
            OBJ_71.setText("Nombre de param\u00e9tres");
            OBJ_71.setName("OBJ_71");
            panel1.add(OBJ_71);
            OBJ_71.setBounds(380, 175, 161, 20);

            //---- WPAR15 ----
            WPAR15.setComponentPopupMenu(BTD);
            WPAR15.setName("WPAR15");
            panel1.add(WPAR15);
            WPAR15.setBounds(200, 204, 210, WPAR15.getPreferredSize().height);

            //---- WPAR25 ----
            WPAR25.setComponentPopupMenu(BTD);
            WPAR25.setName("WPAR25");
            panel1.add(WPAR25);
            WPAR25.setBounds(575, 204, 210, WPAR25.getPreferredSize().height);

            //---- WUSER5 ----
            WUSER5.setComponentPopupMenu(null);
            WUSER5.setName("WUSER5");
            panel1.add(WUSER5);
            WUSER5.setBounds(200, 270, 110, WUSER5.getPreferredSize().height);

            //---- WEXE5 ----
            WEXE5.setText("Ex\u00e9cutable ici");
            WEXE5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WEXE5.setName("WEXE5");
            panel1.add(WEXE5);
            WEXE5.setBounds(665, 176, 118, 19);

            //---- OBJ_58 ----
            OBJ_58.setText("@WFILE5@");
            OBJ_58.setName("OBJ_58");
            panel1.add(OBJ_58);
            OBJ_58.setBounds(470, 7, 97, OBJ_58.getPreferredSize().height);

            //---- WPGM5 ----
            WPGM5.setComponentPopupMenu(BTD);
            WPGM5.setName("WPGM5");
            panel1.add(WPGM5);
            WPGM5.setBounds(200, 171, 110, WPGM5.getPreferredSize().height);

            //---- OBJ_74 ----
            OBJ_74.setText("Param\u00e9tre 1");
            OBJ_74.setName("OBJ_74");
            panel1.add(OBJ_74);
            OBJ_74.setBounds(10, 208, 185, 20);

            //---- OBJ_76 ----
            OBJ_76.setText("Param\u00e9tre 2");
            OBJ_76.setName("OBJ_76");
            panel1.add(OBJ_76);
            OBJ_76.setBounds(435, 208, 94, 20);

            //---- OBJ_69 ----
            OBJ_69.setText("Programme");
            OBJ_69.setName("OBJ_69");
            panel1.add(OBJ_69);
            OBJ_69.setBounds(10, 175, 185, 20);

            //---- OBJ_67 ----
            OBJ_67.setText("Nombre d'exemplaires");
            OBJ_67.setName("OBJ_67");
            panel1.add(OBJ_67);
            OBJ_67.setBounds(10, 142, 185, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("Nom d'\u00e9tiquette = Type + num\u00e9ro d'\u00e9tiquette + & + Suffixe");
            OBJ_56.setFont(OBJ_56.getFont().deriveFont(OBJ_56.getFont().getSize() + 3f));
            OBJ_56.setName("OBJ_56");
            panel1.add(OBJ_56);
            OBJ_56.setBounds(50, 9, 410, OBJ_56.getPreferredSize().height);

            //---- OBJ_81 ----
            OBJ_81.setText("Utilisateur");
            OBJ_81.setName("OBJ_81");
            panel1.add(OBJ_81);
            OBJ_81.setBounds(10, 274, 185, 20);

            //---- OBJ_65 ----
            OBJ_65.setText("D\u00e9signation");
            OBJ_65.setName("OBJ_65");
            panel1.add(OBJ_65);
            OBJ_65.setBounds(380, 109, 79, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("Magasin");
            OBJ_78.setName("OBJ_78");
            panel1.add(OBJ_78);
            OBJ_78.setBounds(10, 241, 185, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("N\u00b0 \u00e9tiquette");
            OBJ_61.setName("OBJ_61");
            panel1.add(OBJ_61);
            OBJ_61.setBounds(120, 109, 75, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("Type");
            OBJ_59.setName("OBJ_59");
            panel1.add(OBJ_59);
            OBJ_59.setBounds(10, 109, 53, 20);

            //---- WSUF5 ----
            WSUF5.setComponentPopupMenu(BTD);
            WSUF5.setName("WSUF5");
            panel1.add(WSUF5);
            WSUF5.setBounds(315, 105, 50, WSUF5.getPreferredSize().height);

            //---- WNETQ5 ----
            WNETQ5.setComponentPopupMenu(null);
            WNETQ5.setName("WNETQ5");
            panel1.add(WNETQ5);
            WNETQ5.setBounds(200, 105, 34, WNETQ5.getPreferredSize().height);

            //---- WTYP5 ----
            WTYP5.setComponentPopupMenu(BTD);
            WTYP5.setName("WTYP5");
            panel1.add(WTYP5);
            WTYP5.setBounds(60, 105, 44, WTYP5.getPreferredSize().height);

            //---- WMAG5 ----
            WMAG5.setComponentPopupMenu(BTD);
            WMAG5.setName("WMAG5");
            panel1.add(WMAG5);
            WMAG5.setBounds(200, 237, 34, WMAG5.getPreferredSize().height);

            //---- OBJ_63 ----
            OBJ_63.setText("Suffixe");
            OBJ_63.setName("OBJ_63");
            panel1.add(OBJ_63);
            OBJ_63.setBounds(265, 109, 50, 20);

            //---- WCOPY5 ----
            WCOPY5.setComponentPopupMenu(null);
            WCOPY5.setName("WCOPY5");
            panel1.add(WCOPY5);
            WCOPY5.setBounds(200, 138, 26, WCOPY5.getPreferredSize().height);

            //---- WNPAR5 ----
            WNPAR5.setComponentPopupMenu(null);
            WNPAR5.setName("WNPAR5");
            panel1.add(WNPAR5);
            WNPAR5.setBounds(575, 171, 24, WNPAR5.getPreferredSize().height);

            //---- OBJ_57 ----
            OBJ_57.setText("(& = sp\u00e9cifique)");
            OBJ_57.setFont(OBJ_57.getFont().deriveFont(OBJ_57.getFont().getSize() + 3f));
            OBJ_57.setName("OBJ_57");
            panel1.add(OBJ_57);
            OBJ_57.setBounds(585, 9, 135, OBJ_57.getPreferredSize().height);

            //---- OBJ_60 ----
            OBJ_60.setText("Le num\u00e9ro d'\u00e9tiquette est important. ");
            OBJ_60.setFont(OBJ_60.getFont().deriveFont(OBJ_60.getFont().getStyle() | Font.BOLD, OBJ_60.getFont().getSize() + 3f));
            OBJ_60.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_60.setName("OBJ_60");
            panel1.add(OBJ_60);
            OBJ_60.setBounds(10, 45, 770, OBJ_60.getPreferredSize().height);

            //---- OBJ_64 ----
            OBJ_64.setText("Utiliser un type et num\u00e9ro existants permet de b\u00e9n\u00e9ficier de la structure CVS existante.");
            OBJ_64.setFont(OBJ_64.getFont().deriveFont(OBJ_64.getFont().getStyle() | Font.BOLD, OBJ_64.getFont().getSize() + 3f));
            OBJ_64.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_64.setName("OBJ_64");
            panel1.add(OBJ_64);
            OBJ_64.setBounds(10, 65, 770, OBJ_64.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 814, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_53;
  private RiZoneSortie WETB;
  private JLabel OBJ_54;
  private RiZoneSortie WMAG;
  private JLabel OBJ_62;
  private RiZoneSortie WUSER;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField WPRT5;
  private RiZoneSortie OBJ_80;
  private JLabel OBJ_83;
  private XRiTextField WLETQ5;
  private JLabel OBJ_71;
  private XRiTextField WPAR15;
  private XRiTextField WPAR25;
  private XRiTextField WUSER5;
  private XRiCheckBox WEXE5;
  private RiZoneSortie OBJ_58;
  private XRiTextField WPGM5;
  private JLabel OBJ_74;
  private JLabel OBJ_76;
  private JLabel OBJ_69;
  private JLabel OBJ_67;
  private JLabel OBJ_56;
  private JLabel OBJ_81;
  private JLabel OBJ_65;
  private JLabel OBJ_78;
  private JLabel OBJ_61;
  private JLabel OBJ_59;
  private XRiTextField WSUF5;
  private XRiTextField WNETQ5;
  private XRiTextField WTYP5;
  private XRiTextField WMAG5;
  private JLabel OBJ_63;
  private XRiTextField WCOPY5;
  private XRiTextField WNPAR5;
  private JLabel OBJ_57;
  private JLabel OBJ_60;
  private JLabel OBJ_64;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
