
package ri.serien.libecranrpg.vgvx.VGVX14FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX14FX_CA extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] L1MDP_OBJ_65_13_Value={"","1","2",};
  // private String[] L1MDP_Value={"","1",};
  
  public VGVX14FX_CA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WSER.setValeursSelection("L", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WARTT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WARTT@")).trim());
    WSTKX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKX@")).trim());
    WRESX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRESX@")).trim());
    WAFFX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WAFFX@")).trim());
    WDISX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    WATTX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTX@")).trim());
    X15PM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@X15PM1@")).trim());
    WNLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNLI@")).trim());
    WCOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOD@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    WLBMA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLBMA2@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UTITP@")).trim());
    L1UNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1UNS@")).trim());
    OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DATINV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    if (!lexique.isTrue("76")) {
      WSER.setText("Lots");
      WSER.setValeursSelection("L", " ");
      // WSER.setSelected(lexique.HostFieldGetData("WSER").equalsIgnoreCase("L"));
    }
    else {
      WSER.setText("Série");
      WSER.setValeursSelection("S", " ");
      // WSER.setSelected(lexique.HostFieldGetData("WSER").equalsIgnoreCase("S"));
    }
    
    WSER.setVisible(!lexique.HostFieldGetData("A1IN2").trim().equalsIgnoreCase(""));
    
    checkBox1.setSelected(lexique.HostFieldGetData("L1MDP").trim().equals("1"));
    checkBox2.setVisible(lexique.isTrue("(N22) AND (N26) AND (N72)"));
    checkBox1.setVisible(checkBox2.isVisible());
    checkBox2.setSelected(lexique.HostFieldGetData("L1MDP").trim().equals("2"));
    
    L1CNDR.setVisible(lexique.isTrue("(N23) AND (N26) AND (N72)"));
    L1QTPX.setVisible(lexique.isTrue("(N08) AND (23)"));
    if (L1CNDR.isVisible()) {
      OBJ_44.setText("Coefficient");
    }
    else if (L1QTPX.isVisible()) {
      OBJ_44.setText("Pièces");
    }
    else {
      OBJ_44.setText("");
    }
    OBJ_43.setVisible(L1CNDR.isVisible());
    WLBMA2.setVisible(lexique.isTrue("(23) AND (26)"));
    L1PHTX.setVisible(lexique.isTrue("N22"));
    OBJ_47.setVisible(L1PHTX.isVisible());
    L1QTEX.setVisible(lexique.isTrue("(N26) AND (N72)"));
    OBJ_45.setVisible(L1QTEX.isVisible());
    L1UNS.setVisible(L1QTEX.isVisible());
    OBJ_46.setVisible(L1UNS.isVisible());
    WASB.setVisible(lexique.isPresent("WASB"));
    OBJ_66.setVisible(WASB.isVisible());
    OBJ_19.setVisible(lexique.HostFieldGetData("TOPINV").equalsIgnoreCase("1") & lexique.isPresent("DATINV"));
    OBJ_61.setVisible(OBJ_19.isVisible());
    OBJ_30.setVisible(lexique.isTrue("N22"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("STOCKS"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    /*  if (WSER.isSelected())
      {
        if (lexique.isTrue("76"))
          lexique.HostFieldPutData("WSER", 0, "L");
        else
          lexique.HostFieldPutData("WSER", 0, "S");
      }
      else
        lexique.HostFieldPutData("WSER", 0, " ");*/
    
    if (checkBox1.isSelected()) {
      lexique.HostFieldPutData("L1MDP", 0, "1");
    }
    else if (checkBox2.isSelected()) {
      lexique.HostFieldPutData("L1MDP", 0, "2");
    }
    else {
      lexique.HostFieldPutData("L1MDP", 0, " ");
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(15, 2);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void checkBox1ActionPerformed(ActionEvent e) {
    if (checkBox1.isSelected()) {
      checkBox2.setSelected(false);
    }
  }
  
  private void checkBox2ActionPerformed(ActionEvent e) {
    if (checkBox2.isSelected()) {
      checkBox1.setSelected(false);
    }
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSER", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    L1OBS = new XRiTextField();
    WASB = new XRiTextField();
    WARTT = new RiZoneSortie();
    WSTKX = new RiZoneSortie();
    WRESX = new RiZoneSortie();
    WAFFX = new RiZoneSortie();
    WDISX = new RiZoneSortie();
    WATTX = new RiZoneSortie();
    X15PM1 = new RiZoneSortie();
    OBJ_25 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_40 = new JLabel();
    WSER = new XRiCheckBox();
    OBJ_66 = new JLabel();
    WNLI = new RiZoneSortie();
    WCOD = new RiZoneSortie();
    OBJ_37 = new JLabel();
    OBJ_38 = new JLabel();
    A1LIB = new RiZoneSortie();
    L1DATX = new XRiCalendrier();
    OBJ_43 = new JLabel();
    WLBMA2 = new RiZoneSortie();
    L1PHTX = new XRiTextField();
    OBJ_47 = new JLabel();
    L1CNDR = new XRiTextField();
    OBJ_44 = new JLabel();
    L1QTEX = new XRiTextField();
    OBJ_45 = new JLabel();
    L1QTPX = new XRiTextField();
    L1UNS = new RiZoneSortie();
    OBJ_46 = new JLabel();
    checkBox1 = new JCheckBox();
    checkBox2 = new JCheckBox();
    panel2 = new JPanel();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_50 = new JLabel();
    L1SAN = new XRiTextField();
    L1ACT = new XRiTextField();
    L1SAN3 = new XRiTextField();
    barre_tete = new JMenuBar();
    panel1 = new JPanel();
    OBJ_61 = new JLabel();
    OBJ_19 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1090, 340));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Acc\u00e8s aux stocks");
            riSousMenu_bt6.setToolTipText("Acc\u00e8s aux stocks");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Gestion des lots");
            riSousMenu_bt7.setToolTipText("Gestion des lots");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- L1OBS ----
          L1OBS.setComponentPopupMenu(null);
          L1OBS.setToolTipText("Libell\u00e9 visible dans les mouvements de stock");
          L1OBS.setName("L1OBS");
          p_recup.add(L1OBS);
          L1OBS.setBounds(335, 140, 310, L1OBS.getPreferredSize().height);

          //---- WASB ----
          WASB.setComponentPopupMenu(BTD);
          WASB.setName("WASB");
          p_recup.add(WASB);
          WASB.setBounds(100, 170, 210, WASB.getPreferredSize().height);

          //---- WARTT ----
          WARTT.setComponentPopupMenu(BTD);
          WARTT.setText("@WARTT@");
          WARTT.setName("WARTT");
          p_recup.add(WARTT);
          WARTT.setBounds(95, 112, 210, WARTT.getPreferredSize().height);

          //---- WSTKX ----
          WSTKX.setComponentPopupMenu(null);
          WSTKX.setText("@WSTKX@");
          WSTKX.setHorizontalAlignment(SwingConstants.RIGHT);
          WSTKX.setName("WSTKX");
          p_recup.add(WSTKX);
          WSTKX.setBounds(20, 45, 100, WSTKX.getPreferredSize().height);

          //---- WRESX ----
          WRESX.setComponentPopupMenu(null);
          WRESX.setText("@WRESX@");
          WRESX.setHorizontalAlignment(SwingConstants.RIGHT);
          WRESX.setName("WRESX");
          p_recup.add(WRESX);
          WRESX.setBounds(150, 45, 100, WRESX.getPreferredSize().height);

          //---- WAFFX ----
          WAFFX.setComponentPopupMenu(null);
          WAFFX.setText("@WAFFX@");
          WAFFX.setHorizontalAlignment(SwingConstants.RIGHT);
          WAFFX.setName("WAFFX");
          p_recup.add(WAFFX);
          WAFFX.setBounds(265, 45, 100, WAFFX.getPreferredSize().height);

          //---- WDISX ----
          WDISX.setComponentPopupMenu(null);
          WDISX.setText("@WDISX@");
          WDISX.setHorizontalAlignment(SwingConstants.RIGHT);
          WDISX.setName("WDISX");
          p_recup.add(WDISX);
          WDISX.setBounds(400, 45, 100, WDISX.getPreferredSize().height);

          //---- WATTX ----
          WATTX.setComponentPopupMenu(null);
          WATTX.setText("@WATTX@");
          WATTX.setHorizontalAlignment(SwingConstants.RIGHT);
          WATTX.setName("WATTX");
          p_recup.add(WATTX);
          WATTX.setBounds(515, 45, 100, WATTX.getPreferredSize().height);

          //---- X15PM1 ----
          X15PM1.setComponentPopupMenu(null);
          X15PM1.setText("@X15PM1@");
          X15PM1.setHorizontalAlignment(SwingConstants.RIGHT);
          X15PM1.setName("X15PM1");
          p_recup.add(X15PM1);
          X15PM1.setBounds(630, 45, 100, X15PM1.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setText("En stock");
          OBJ_25.setName("OBJ_25");
          p_recup.add(OBJ_25);
          OBJ_25.setBounds(20, 25, 99, 18);

          //---- OBJ_26 ----
          OBJ_26.setText("Command\u00e9");
          OBJ_26.setName("OBJ_26");
          p_recup.add(OBJ_26);
          OBJ_26.setBounds(150, 25, 99, 18);

          //---- OBJ_27 ----
          OBJ_27.setText("(dont r\u00e9serv\u00e9)");
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(265, 25, 99, 18);

          //---- OBJ_28 ----
          OBJ_28.setText("Disponible");
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(400, 25, 99, 18);

          //---- OBJ_30 ----
          OBJ_30.setText("PUMP");
          OBJ_30.setName("OBJ_30");
          p_recup.add(OBJ_30);
          OBJ_30.setBounds(630, 25, 70, 18);

          //---- OBJ_29 ----
          OBJ_29.setText("Attendu");
          OBJ_29.setName("OBJ_29");
          p_recup.add(OBJ_29);
          OBJ_29.setBounds(515, 25, 67, 18);

          //---- OBJ_40 ----
          OBJ_40.setText("C     N\u00b0Li");
          OBJ_40.setName("OBJ_40");
          p_recup.add(OBJ_40);
          OBJ_40.setBounds(25, 90, 65, 20);

          //---- WSER ----
          WSER.setText("WSER");
          WSER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSER.setName("WSER");
          p_recup.add(WSER);
          WSER.setBounds(100, 90, 205, 20);

          //---- OBJ_66 ----
          OBJ_66.setText("Substitution");
          OBJ_66.setName("OBJ_66");
          p_recup.add(OBJ_66);
          OBJ_66.setBounds(20, 174, 80, 20);

          //---- WNLI ----
          WNLI.setComponentPopupMenu(BTD);
          WNLI.setText("@WNLI@");
          WNLI.setName("WNLI");
          p_recup.add(WNLI);
          WNLI.setBounds(45, 112, 45, WNLI.getPreferredSize().height);

          //---- WCOD ----
          WCOD.setComponentPopupMenu(BTD);
          WCOD.setText("@WCOD@");
          WCOD.setName("WCOD");
          p_recup.add(WCOD);
          WCOD.setBounds(20, 112, 20, WCOD.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("-");
          OBJ_37.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_37.setFont(OBJ_37.getFont().deriveFont(OBJ_37.getFont().getStyle() | Font.BOLD, OBJ_37.getFont().getSize() + 2f));
          OBJ_37.setName("OBJ_37");
          p_recup.add(OBJ_37);
          OBJ_37.setBounds(125, 50, 20, 16);

          //---- OBJ_38 ----
          OBJ_38.setText("=");
          OBJ_38.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_38.setFont(OBJ_38.getFont().deriveFont(OBJ_38.getFont().getStyle() | Font.BOLD, OBJ_38.getFont().getSize() + 1f));
          OBJ_38.setName("OBJ_38");
          p_recup.add(OBJ_38);
          OBJ_38.setBounds(375, 50, 15, 16);

          //---- A1LIB ----
          A1LIB.setText("@A1LIB@");
          A1LIB.setName("A1LIB");
          p_recup.add(A1LIB);
          A1LIB.setBounds(20, 142, 310, A1LIB.getPreferredSize().height);

          //---- L1DATX ----
          L1DATX.setComponentPopupMenu(null);
          L1DATX.setName("L1DATX");
          p_recup.add(L1DATX);
          L1DATX.setBounds(310, 110, 105, L1DATX.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setText("Date");
          OBJ_43.setName("OBJ_43");
          p_recup.add(OBJ_43);
          OBJ_43.setBounds(315, 90, 50, 20);

          //---- WLBMA2 ----
          WLBMA2.setText("@WLBMA2@");
          WLBMA2.setName("WLBMA2");
          p_recup.add(WLBMA2);
          WLBMA2.setBounds(420, 112, 310, WLBMA2.getPreferredSize().height);

          //---- L1PHTX ----
          L1PHTX.setComponentPopupMenu(BTD);
          L1PHTX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1PHTX.setName("L1PHTX");
          p_recup.add(L1PHTX);
          L1PHTX.setBounds(539, 170, 106, L1PHTX.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setText("@UTITP@");
          OBJ_47.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_47.setName("OBJ_47");
          p_recup.add(OBJ_47);
          OBJ_47.setBounds(395, 174, 135, 20);

          //---- L1CNDR ----
          L1CNDR.setComponentPopupMenu(BTD);
          L1CNDR.setName("L1CNDR");
          p_recup.add(L1CNDR);
          L1CNDR.setBounds(420, 110, 70, L1CNDR.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Coefficient");
          OBJ_44.setName("OBJ_44");
          p_recup.add(OBJ_44);
          OBJ_44.setBounds(425, 90, 64, 20);

          //---- L1QTEX ----
          L1QTEX.setComponentPopupMenu(null);
          L1QTEX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1QTEX.setName("L1QTEX");
          p_recup.add(L1QTEX);
          L1QTEX.setBounds(515, 110, 110, L1QTEX.getPreferredSize().height);

          //---- OBJ_45 ----
          OBJ_45.setText("Quantit\u00e9");
          OBJ_45.setName("OBJ_45");
          p_recup.add(OBJ_45);
          OBJ_45.setBounds(515, 90, 67, 20);

          //---- L1QTPX ----
          L1QTPX.setComponentPopupMenu(BTD);
          L1QTPX.setName("L1QTPX");
          p_recup.add(L1QTPX);
          L1QTPX.setBounds(420, 110, 90, L1QTPX.getPreferredSize().height);

          //---- L1UNS ----
          L1UNS.setComponentPopupMenu(BTD);
          L1UNS.setText("@L1UNS@");
          L1UNS.setName("L1UNS");
          p_recup.add(L1UNS);
          L1UNS.setBounds(633, 112, 30, L1UNS.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Un");
          OBJ_46.setName("OBJ_46");
          p_recup.add(OBJ_46);
          OBJ_46.setBounds(635, 90, 30, 20);

          //---- checkBox1 ----
          checkBox1.setText("Modificateur de PUMP");
          checkBox1.setName("checkBox1");
          checkBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              checkBox1ActionPerformed(e);
            }
          });
          p_recup.add(checkBox1);
          checkBox1.setBounds(20, 210, 165, checkBox1.getPreferredSize().height);

          //---- checkBox2 ----
          checkBox2.setText("PUMP impos\u00e9");
          checkBox2.setName("checkBox2");
          checkBox2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              checkBox2ActionPerformed(e);
            }
          });
          p_recup.add(checkBox2);
          checkBox2.setBounds(205, 210, 130, checkBox2.getPreferredSize().height);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Analytique"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_48 ----
            OBJ_48.setText("Section");
            OBJ_48.setName("OBJ_48");
            panel2.add(OBJ_48);
            OBJ_48.setBounds(15, 35, 55, 28);

            //---- OBJ_49 ----
            OBJ_49.setText("Affaire");
            OBJ_49.setName("OBJ_49");
            panel2.add(OBJ_49);
            OBJ_49.setBounds(170, 35, 48, 28);

            //---- OBJ_50 ----
            OBJ_50.setText("Nature");
            OBJ_50.setName("OBJ_50");
            panel2.add(OBJ_50);
            OBJ_50.setBounds(315, 35, 46, 28);

            //---- L1SAN ----
            L1SAN.setComponentPopupMenu(BTD);
            L1SAN.setHorizontalAlignment(SwingConstants.RIGHT);
            L1SAN.setName("L1SAN");
            panel2.add(L1SAN);
            L1SAN.setBounds(85, 35, 54, L1SAN.getPreferredSize().height);

            //---- L1ACT ----
            L1ACT.setComponentPopupMenu(BTD);
            L1ACT.setHorizontalAlignment(SwingConstants.RIGHT);
            L1ACT.setName("L1ACT");
            panel2.add(L1ACT);
            L1ACT.setBounds(225, 35, 54, 28);

            //---- L1SAN3 ----
            L1SAN3.setComponentPopupMenu(BTD);
            L1SAN3.setHorizontalAlignment(SwingConstants.RIGHT);
            L1SAN3.setName("L1SAN3");
            panel2.add(L1SAN3);
            L1SAN3.setBounds(365, 35, 64, L1SAN3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_recup.add(panel2);
          panel2.setBounds(420, 210, 445, 80);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 896, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setPreferredSize(new Dimension(700, 30));
        panel1.setMinimumSize(new Dimension(700, 30));
        panel1.setMaximumSize(new Dimension(700, 30));
        panel1.setName("panel1");

        //---- OBJ_61 ----
        OBJ_61.setText("Dernier inventaire le");
        OBJ_61.setName("OBJ_61");

        //---- OBJ_19 ----
        OBJ_19.setText("@DATINV@");
        OBJ_19.setOpaque(false);
        OBJ_19.setName("OBJ_19");

        GroupLayout panel1Layout = new GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
          panel1Layout.createParallelGroup()
            .addGroup(panel1Layout.createSequentialGroup()
              .addContainerGap()
              .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(493, Short.MAX_VALUE))
        );
        panel1Layout.setVerticalGroup(
          panel1Layout.createParallelGroup()
            .addGroup(panel1Layout.createSequentialGroup()
              .addGroup(panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(2, 2, 2)
                  .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addContainerGap(2, Short.MAX_VALUE))
        );
      }
      barre_tete.add(panel1);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiTextField L1OBS;
  private XRiTextField WASB;
  private RiZoneSortie WARTT;
  private RiZoneSortie WSTKX;
  private RiZoneSortie WRESX;
  private RiZoneSortie WAFFX;
  private RiZoneSortie WDISX;
  private RiZoneSortie WATTX;
  private RiZoneSortie X15PM1;
  private JLabel OBJ_25;
  private JLabel OBJ_26;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private JLabel OBJ_30;
  private JLabel OBJ_29;
  private JLabel OBJ_40;
  private XRiCheckBox WSER;
  private JLabel OBJ_66;
  private RiZoneSortie WNLI;
  private RiZoneSortie WCOD;
  private JLabel OBJ_37;
  private JLabel OBJ_38;
  private RiZoneSortie A1LIB;
  private XRiCalendrier L1DATX;
  private JLabel OBJ_43;
  private RiZoneSortie WLBMA2;
  private XRiTextField L1PHTX;
  private JLabel OBJ_47;
  private XRiTextField L1CNDR;
  private JLabel OBJ_44;
  private XRiTextField L1QTEX;
  private JLabel OBJ_45;
  private XRiTextField L1QTPX;
  private RiZoneSortie L1UNS;
  private JLabel OBJ_46;
  private JCheckBox checkBox1;
  private JCheckBox checkBox2;
  private JPanel panel2;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private JLabel OBJ_50;
  private XRiTextField L1SAN;
  private XRiTextField L1ACT;
  private XRiTextField L1SAN3;
  private JMenuBar barre_tete;
  private JPanel panel1;
  private JLabel OBJ_61;
  private RiZoneSortie OBJ_19;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
