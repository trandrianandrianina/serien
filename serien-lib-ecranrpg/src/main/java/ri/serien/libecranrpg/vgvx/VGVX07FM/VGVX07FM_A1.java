
package ri.serien.libecranrpg.vgvx.VGVX07FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX07FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] WTA2_Value = { "", "A", "1", };
  private String[] WTA1_Value = { "", "C", "G", };
  
  public VGVX07FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX1.setValeurs("1", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX7.setValeurs("7", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX6.setValeurs("6", "RB");
    WTA2.setValeurs(WTA2_Value, null);
    WTA1.setValeurs(WTA1_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    ARG4.setEnabled(lexique.isPresent("ARG4"));
    WAR1.setEnabled(lexique.isPresent("WAR1"));
    ARG5.setEnabled(lexique.isPresent("ARG5"));
    ARG7.setEnabled(lexique.isPresent("ARG7"));
    // TIDX1.setVisible( lexique.isPresent("RB"));
    // TIDX1.setEnabled( lexique.isPresent("RB"));
    // TIDX1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // TIDX5.setVisible( lexique.isPresent("RB"));
    // TIDX5.setEnabled( lexique.isPresent("RB"));
    // TIDX5.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    // TIDX4.setVisible( lexique.isPresent("RB"));
    // TIDX4.setEnabled( lexique.isPresent("RB"));
    // TIDX4.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("4"));
    ARG1.setEnabled(lexique.isPresent("ARG1"));
    ARG6.setEnabled(lexique.isPresent("ARG6"));
    ARG3.setEnabled(lexique.isPresent("ARG3"));
    ARG2.setEnabled(lexique.isPresent("ARG2"));
    // TIDX7.setVisible( lexique.isPresent("RB"));
    // TIDX7.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("7"));
    // TIDX3.setVisible( lexique.isPresent("RB"));
    // TIDX3.setEnabled( lexique.isPresent("RB"));
    // TIDX3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // TIDX2.setVisible( lexique.isPresent("RB"));
    // TIDX2.setEnabled( lexique.isPresent("RB"));
    // TIDX2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // TIDX6.setVisible( lexique.isPresent("RB"));
    // TIDX6.setEnabled( lexique.isPresent("RB"));
    // TIDX6.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("6"));
    // WTA2.setEnabled( lexique.isPresent("WTA2"));
    // WTA1.setEnabled( lexique.isPresent("WTA1"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ @LIBPG@"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TIDX1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (TIDX5.isSelected())
    // lexique.HostFieldPutData("RB", 0, "5");
    // if (TIDX4.isSelected())
    // lexique.HostFieldPutData("RB", 0, "4");
    // if (TIDX7.isSelected())
    // lexique.HostFieldPutData("RB", 0, "7");
    // if (TIDX3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (TIDX6.isSelected())
    // lexique.HostFieldPutData("RB", 0, "6");
    // lexique.HostFieldPutData("WTA2", 0, WTA2_Value[WTA2.getSelectedIndex()]);
    // lexique.HostFieldPutData("WTA1", 0, WTA1_Value[WTA1.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_47 = new JLabel();
    WETB = new XRiTextField();
    BT_ChgSoc = new SNBoutonRecherche();
    OBJ_45 = new JLabel();
    WAR1 = new XRiTextField();
    OBJ_46 = new JLabel();
    WTA1 = new XRiComboBox();
    OBJ_74 = new JLabel();
    WTA2 = new XRiComboBox();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX6 = new XRiRadioButton();
    TIDX2 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    ARG2 = new XRiTextField();
    ARG3 = new XRiTextField();
    ARG6 = new XRiTextField();
    ARG1 = new XRiTextField();
    TIDX4 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    ARG7 = new XRiTextField();
    ARG5 = new XRiTextField();
    ARG4 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Conditions de r\u00e9mun\u00e9ration");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_47 ----
          OBJ_47.setText("Etablissement");
          OBJ_47.setName("OBJ_47");
          p_tete_gauche.add(OBJ_47);
          OBJ_47.setBounds(5, 4, 100, 20);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(110, 0, 40, WETB.getPreferredSize().height);

          //---- BT_ChgSoc ----
          BT_ChgSoc.setText("");
          BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
            }
          });
          p_tete_gauche.add(BT_ChgSoc);
          BT_ChgSoc.setBounds(155, 0, 32, 29);

          //---- OBJ_45 ----
          OBJ_45.setText("Cod CN");
          OBJ_45.setName("OBJ_45");
          p_tete_gauche.add(OBJ_45);
          OBJ_45.setBounds(195, 6, 51, 16);

          //---- WAR1 ----
          WAR1.setToolTipText("Code condition");
          WAR1.setComponentPopupMenu(BTD);
          WAR1.setName("WAR1");
          p_tete_gauche.add(WAR1);
          WAR1.setBounds(250, 0, 70, WAR1.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Type");
          OBJ_46.setName("OBJ_46");
          p_tete_gauche.add(OBJ_46);
          OBJ_46.setBounds(350, 5, 36, 16);

          //---- WTA1 ----
          WTA1.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Client",
            "Groupement statistique"
          }));
          WTA1.setComponentPopupMenu(BTD);
          WTA1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTA1.setName("WTA1");
          p_tete_gauche.add(WTA1);
          WTA1.setBounds(390, 0, 151, WTA1.getPreferredSize().height);

          //---- OBJ_74 ----
          OBJ_74.setText("Type d'argument");
          OBJ_74.setName("OBJ_74");
          p_tete_gauche.add(OBJ_74);
          OBJ_74.setBounds(585, 5, 109, 16);

          //---- WTA2 ----
          WTA2.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Code article",
            "Mot de classement 1"
          }));
          WTA2.setComponentPopupMenu(BTD);
          WTA2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTA2.setName("WTA2");
          p_tete_gauche.add(WTA2);
          WTA2.setBounds(700, 0, 151, WTA2.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- TIDX6 ----
            TIDX6.setText("Mot de classement fournisseur");
            TIDX6.setToolTipText("tri\u00e9 par");
            TIDX6.setComponentPopupMenu(BTD);
            TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX6.setName("TIDX6");
            panel1.add(TIDX6);
            TIDX6.setBounds(90, 205, 207, 20);

            //---- TIDX2 ----
            TIDX2.setText("Recherche alphab\u00e9tique");
            TIDX2.setToolTipText("tri\u00e9 par");
            TIDX2.setComponentPopupMenu(BTD);
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setName("TIDX2");
            panel1.add(TIDX2);
            TIDX2.setBounds(90, 65, 174, 20);

            //---- TIDX3 ----
            TIDX3.setText("Mot de classement 2");
            TIDX3.setToolTipText("tri\u00e9 par");
            TIDX3.setComponentPopupMenu(BTD);
            TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX3.setName("TIDX3");
            panel1.add(TIDX3);
            TIDX3.setBounds(90, 100, 148, 20);

            //---- TIDX7 ----
            TIDX7.setText("Num\u00e9ro fournisseur");
            TIDX7.setToolTipText("Tri\u00e9 par");
            TIDX7.setComponentPopupMenu(BTD);
            TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX7.setName("TIDX7");
            panel1.add(TIDX7);
            TIDX7.setBounds(90, 240, 141, 20);

            //---- ARG2 ----
            ARG2.setComponentPopupMenu(BTD);
            ARG2.setName("ARG2");
            panel1.add(ARG2);
            ARG2.setBounds(360, 61, 160, ARG2.getPreferredSize().height);

            //---- ARG3 ----
            ARG3.setComponentPopupMenu(BTD);
            ARG3.setName("ARG3");
            panel1.add(ARG3);
            ARG3.setBounds(360, 96, 160, ARG3.getPreferredSize().height);

            //---- ARG6 ----
            ARG6.setComponentPopupMenu(BTD);
            ARG6.setName("ARG6");
            panel1.add(ARG6);
            ARG6.setBounds(360, 201, 160, ARG6.getPreferredSize().height);

            //---- ARG1 ----
            ARG1.setComponentPopupMenu(BTD);
            ARG1.setName("ARG1");
            panel1.add(ARG1);
            ARG1.setBounds(360, 271, 160, ARG1.getPreferredSize().height);

            //---- TIDX4 ----
            TIDX4.setText("Groupe, famille");
            TIDX4.setToolTipText("tri\u00e9 par");
            TIDX4.setComponentPopupMenu(BTD);
            TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX4.setName("TIDX4");
            panel1.add(TIDX4);
            TIDX4.setBounds(90, 135, 115, 20);

            //---- TIDX5 ----
            TIDX5.setText("R\u00e9f\u00e9rence tarif");
            TIDX5.setToolTipText("tri\u00e9 par");
            TIDX5.setComponentPopupMenu(BTD);
            TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX5.setName("TIDX5");
            panel1.add(TIDX5);
            TIDX5.setBounds(90, 170, 112, 20);

            //---- TIDX1 ----
            TIDX1.setText("Code article");
            TIDX1.setToolTipText("tri\u00e9 par");
            TIDX1.setComponentPopupMenu(BTD);
            TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX1.setName("TIDX1");
            panel1.add(TIDX1);
            TIDX1.setBounds(90, 275, 97, 20);

            //---- ARG7 ----
            ARG7.setComponentPopupMenu(BTD);
            ARG7.setName("ARG7");
            panel1.add(ARG7);
            ARG7.setBounds(360, 236, 66, ARG7.getPreferredSize().height);

            //---- ARG5 ----
            ARG5.setComponentPopupMenu(BTD);
            ARG5.setName("ARG5");
            panel1.add(ARG5);
            ARG5.setBounds(360, 166, 60, ARG5.getPreferredSize().height);

            //---- ARG4 ----
            ARG4.setComponentPopupMenu(BTD);
            ARG4.setName("ARG4");
            panel1.add(ARG4);
            ARG4.setBounds(360, 131, 40, ARG4.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 635, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 365, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Aide en ligne");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Invite");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TIDX1);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_47;
  private XRiTextField WETB;
  private SNBoutonRecherche BT_ChgSoc;
  private JLabel OBJ_45;
  private XRiTextField WAR1;
  private JLabel OBJ_46;
  private XRiComboBox WTA1;
  private JLabel OBJ_74;
  private XRiComboBox WTA2;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX7;
  private XRiTextField ARG2;
  private XRiTextField ARG3;
  private XRiTextField ARG6;
  private XRiTextField ARG1;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX5;
  private XRiRadioButton TIDX1;
  private XRiTextField ARG7;
  private XRiTextField ARG5;
  private XRiTextField ARG4;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
