//$$david$$ ££17/12/10££ -> new look

package ri.serien.libecranrpg.vgvx.VGVX052F;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.snunite.SNUnite;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author David Biason
 */
public class VGVX052F_B1 extends SNPanelEcranRPG implements ioFrame {
  boolean isConsultation = true;
  
  public VGVX052F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    // Charger et rafraichir les composants unité
    rafraichirUnite();
    
    lbEgal.setVisible(P5KCS.isVisible());
    lbUS.setVisible(P5KCS.isVisible());
    lbEgal2.setVisible(P5KACX.isVisible());
    lbUA.setVisible(P5KACX.isVisible());
    lbEgal3.setVisible(P5KSC.isVisible());
    lbUS1.setVisible(P5KSC.isVisible());
    lbEgal4.setVisible(P5KSA.isVisible());
    lbUS2.setVisible(P5KSA.isVisible());
    lbEgal5.setVisible(P5KSVX.isVisible());
    lbUS3.setVisible(P5KSVX.isVisible());
    lbEgal6.setVisible(P5KTS.isVisible());
    lbUS4.setVisible(P5KTS.isVisible());
    
    // Titre
    setTitle("Stocks : synthèse des unités");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Unités
    snUniteS.renseignerChampRPG(lexique, "P5UNS");
    snUniteCS.renseignerChampRPG(lexique, "P5UCS");
    snUniteCA.renseignerChampRPG(lexique, "P5UNC");
    snUniteA.renseignerChampRPG(lexique, "P5UNA");
    snUniteV.renseignerChampRPG(lexique, "P5UNV");
    snUniteT.renseignerChampRPG(lexique, "P5UNT");
    snUniteP.renseignerChampRPG(lexique, "P5UNP");
    snUniteCN1.renseignerChampRPG(lexique, "P5UNL");
    snUniteNL1.renseignerChampRPG(lexique, "UNL5");
    snUniteCN2.renseignerChampRPG(lexique, "P5UNL2");
    snUniteNL2.renseignerChampRPG(lexique, "UNL6");
    snUniteCN3.renseignerChampRPG(lexique, "P5UNL3");
    snUniteNL3.renseignerChampRPG(lexique, "UNL7");
    snUniteCN4.renseignerChampRPG(lexique, "P5UNL4");
    snUniteNL4.renseignerChampRPG(lexique, "UNL8");
    
    lexique.ClosePanel();
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charger et rafraichir les composants unité
   */
  private void rafraichirUnite() {
    // Unité de stock
    snUniteS.setSession(getSession());
    snUniteS.charger(false);
    snUniteS.setSelectionParChampRPG(lexique, "P5UNS");
    snUniteS.setEnabled(!isConsultation);
    
    // Unité de conditionnement de stock
    snUniteCS.setSession(getSession());
    snUniteCS.charger(false);
    snUniteCS.setSelectionParChampRPG(lexique, "P5UCS");
    lbUS.setText("Unités de stock");
    if (snUniteS.getSelection() != null) {
      lbUS.setText(snUniteS.getSelection().getTexte());
    }
    snUniteCS.setEnabled(!isConsultation);
    
    // Unité de conditionnement d'achat
    snUniteCA.setSession(getSession());
    snUniteCA.charger(false);
    snUniteCA.setSelectionParChampRPG(lexique, "P5UNC");
    lbUS1.setText("Unités de stock");
    if (snUniteS.getSelection() != null) {
      lbUS1.setText(snUniteS.getSelection().getTexte());
    }
    lbUA.setText("Unités d'achat");
    if (snUniteA.getSelection() != null) {
      lbUA.setText(snUniteA.getSelection().getTexte());
    }
    snUniteCA.setEnabled(!isConsultation);
    
    // Unité d'achat
    snUniteA.setSession(getSession());
    snUniteA.charger(false);
    snUniteA.setSelectionParChampRPG(lexique, "P5UNA");
    lbUS2.setText("Unités de stock");
    if (snUniteS.getSelection() != null) {
      lbUS2.setText(snUniteS.getSelection().getTexte());
    }
    snUniteA.setEnabled(!isConsultation);
    
    // Unité de vente
    snUniteV.setSession(getSession());
    snUniteV.charger(false);
    snUniteV.setSelectionParChampRPG(lexique, "P5UNV");
    lbUS3.setText("Unités de stock");
    if (snUniteS.getSelection() != null) {
      lbUS3.setText(snUniteS.getSelection().getTexte());
    }
    snUniteV.setEnabled(!isConsultation);
    
    // Unité de transport
    snUniteT.setSession(getSession());
    snUniteT.charger(false);
    snUniteT.setSelectionParChampRPG(lexique, "P5UNT");
    lbUS4.setText("Unités de stock");
    if (snUniteS.getSelection() != null) {
      lbUS4.setText(snUniteS.getSelection().getTexte());
    }
    snUniteT.setEnabled(!isConsultation);
    
    // Unité de présentation
    snUniteP.setSession(getSession());
    snUniteP.charger(false);
    snUniteP.setSelectionParChampRPG(lexique, "P5UNP");
    snUniteP.setEnabled(!isConsultation);
    
    // Unité conditionnement 1
    snUniteCN1.setSession(getSession());
    snUniteCN1.charger(false);
    snUniteCN1.setSelectionParChampRPG(lexique, "P5UNL");
    snUniteCN1.setEnabled(!isConsultation);
    
    snUniteNL1.setSession(getSession());
    snUniteNL1.charger(false);
    snUniteNL1.setSelectionParChampRPG(lexique, "UNL5");
    snUniteNL1.setEnabled(!isConsultation);
    
    // Unité conditionnement 2
    snUniteCN2.setSession(getSession());
    snUniteCN2.charger(false);
    snUniteCN2.setSelectionParChampRPG(lexique, "P5UNL2");
    snUniteCN2.setEnabled(!isConsultation);
    
    snUniteNL2.setSession(getSession());
    snUniteNL2.charger(false);
    snUniteNL2.setSelectionParChampRPG(lexique, "UNL6");
    snUniteNL2.setEnabled(!isConsultation);
    
    // Unité conditionnement 3
    snUniteCN3.setSession(getSession());
    snUniteCN3.charger(false);
    snUniteCN3.setSelectionParChampRPG(lexique, "P5UNL3");
    snUniteCN3.setEnabled(!isConsultation);
    
    snUniteNL3.setSession(getSession());
    snUniteNL3.charger(false);
    snUniteNL3.setSelectionParChampRPG(lexique, "UNL7");
    snUniteNL3.setEnabled(!isConsultation);
    
    // Unité conditionnement 4
    snUniteCN4.setSession(getSession());
    snUniteCN4.charger(false);
    snUniteCN4.setSelectionParChampRPG(lexique, "P5UNL4");
    snUniteCN4.setEnabled(!isConsultation);
    
    snUniteNL4.setSession(getSession());
    snUniteNL4.charger(false);
    snUniteNL4.setSelectionParChampRPG(lexique, "UNL8");
    snUniteNL4.setEnabled(!isConsultation);
  }
  
  private void snUniteSValueChanged(SNComposantEvent e) {
    try {
      lbUS.setText("Unités de stock");
      lbUS1.setText("Unités de stock");
      lbUS2.setText("Unités de stock");
      lbUS3.setText("Unités de stock");
      lbUS4.setText("Unités de stock");
      if (snUniteS.getSelection() != null) {
        lbUS.setText(snUniteS.getSelection().getTexte());
        lbUS1.setText(snUniteS.getSelection().getTexte());
        lbUS2.setText(snUniteS.getSelection().getTexte());
        lbUS3.setText(snUniteS.getSelection().getTexte());
        lbUS4.setText(snUniteS.getSelection().getTexte());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snUniteAValueChanged(SNComposantEvent e) {
    try {
      lbUA.setText("Unités d'achat");
      if (snUniteA.getSelection() != null) {
        lbUA.setText(snUniteA.getSelection().getTexte());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlUnite = new SNPanelTitre();
    lbUniteStockage = new SNLabelChamp();
    pnlUS = new SNPanel();
    snUniteS = new SNUnite();
    lbCodeBarres = new SNLabelChamp();
    P5GCD0 = new XRiTextField();
    lbUniteCS = new SNLabelChamp();
    pnlUCS = new SNPanel();
    snUniteCS = new SNUnite();
    lbEgal = new SNLabelChamp();
    P5KCS = new XRiTextField();
    lbUS = new SNLabelUnite();
    lbUniteCA = new SNLabelChamp();
    pnlUCA = new SNPanel();
    snUniteCA = new SNUnite();
    lbEgal2 = new SNLabelChamp();
    P5KACX = new XRiTextField();
    lbUA = new SNLabelUnite();
    lbEgal3 = new SNLabelChamp();
    P5KSC = new XRiTextField();
    lbUS1 = new SNLabelUnite();
    lbUniteAchat = new SNLabelChamp();
    pnlUA = new SNPanel();
    snUniteA = new SNUnite();
    lbEgal4 = new SNLabelChamp();
    P5KSA = new XRiTextField();
    lbUS2 = new SNLabelUnite();
    lbUniteVente = new SNLabelChamp();
    pnlUV = new SNPanel();
    snUniteV = new SNUnite();
    lbEgal5 = new SNLabelChamp();
    P5KSVX = new XRiTextField();
    lbUS3 = new SNLabelUnite();
    lbUniteTransport = new SNLabelChamp();
    pnlUT = new SNPanel();
    snUniteT = new SNUnite();
    lbEgal6 = new SNLabelChamp();
    P5KTS = new XRiTextField();
    lbUS4 = new SNLabelUnite();
    lbUniteCS6 = new SNLabelChamp();
    pnlUP = new SNPanel();
    snUniteP = new SNUnite();
    pnlCNV = new SNPanelTitre();
    pnlCV1 = new SNPanel();
    lbCNV1 = new SNLabelChamp();
    snUniteCN1 = new SNUnite();
    lbEgal7 = new SNLabelChamp();
    P5CNDX = new XRiTextField();
    snUniteNL1 = new SNUnite();
    lbCodeBarres1 = new SNLabelChamp();
    P5GCD1 = new XRiTextField();
    pnlCV2 = new SNPanel();
    lbCNV2 = new SNLabelChamp();
    snUniteCN2 = new SNUnite();
    lbEgal8 = new SNLabelChamp();
    P5CN2X = new XRiTextField();
    snUniteNL2 = new SNUnite();
    lbCodeBarres2 = new SNLabelChamp();
    P5GCD2 = new XRiTextField();
    pnlCV3 = new SNPanel();
    lbCNV3 = new SNLabelChamp();
    snUniteCN3 = new SNUnite();
    lbEgal9 = new SNLabelChamp();
    P5CN3X = new XRiTextField();
    snUniteNL3 = new SNUnite();
    lbCodeBarres3 = new SNLabelChamp();
    P5GCD3 = new XRiTextField();
    pnlCV4 = new SNPanel();
    lbCNV4 = new SNLabelChamp();
    snUniteCN4 = new SNUnite();
    lbEgal10 = new SNLabelChamp();
    P5CN4X = new XRiTextField();
    snUniteNL4 = new SNUnite();
    lbCodeBarres4 = new SNLabelChamp();
    P5GCD4 = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(1150, 600));
    setPreferredSize(new Dimension(1150, 600));
    setMaximumSize(new Dimension(1150, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 1.0, 1.0, 1.0E-4 };
        
        // ======== pnlUnite ========
        {
          pnlUnite.setTitre("Unit\u00e9s utilis\u00e9es");
          pnlUnite.setName("pnlUnite");
          pnlUnite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlUnite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlUnite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlUnite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlUnite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbUniteStockage ----
          lbUniteStockage.setText("Unit\u00e9 de stockage (US)");
          lbUniteStockage.setName("lbUniteStockage");
          pnlUnite.add(lbUniteStockage, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlUS ========
          {
            pnlUS.setName("pnlUS");
            pnlUS.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlUS.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlUS.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlUS.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlUS.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- snUniteS ----
            snUniteS.setName("snUniteS");
            snUniteS.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snUniteSValueChanged(e);
              }
            });
            pnlUS.add(snUniteS, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbCodeBarres ----
            lbCodeBarres.setText("Code \u00e0 barres");
            lbCodeBarres.setMaximumSize(new Dimension(120, 30));
            lbCodeBarres.setMinimumSize(new Dimension(120, 30));
            lbCodeBarres.setPreferredSize(new Dimension(120, 30));
            lbCodeBarres.setName("lbCodeBarres");
            pnlUS.add(lbCodeBarres, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5GCD0 ----
            P5GCD0.setPreferredSize(new Dimension(150, 30));
            P5GCD0.setMinimumSize(new Dimension(150, 30));
            P5GCD0.setMaximumSize(new Dimension(150, 30));
            P5GCD0.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5GCD0.setName("P5GCD0");
            pnlUS.add(P5GCD0, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlUnite.add(pnlUS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbUniteCS ----
          lbUniteCS.setText("Unit\u00e9 de conditionnement de stock (UCS)");
          lbUniteCS.setMaximumSize(new Dimension(300, 30));
          lbUniteCS.setMinimumSize(new Dimension(300, 30));
          lbUniteCS.setPreferredSize(new Dimension(300, 30));
          lbUniteCS.setName("lbUniteCS");
          pnlUnite.add(lbUniteCS, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlUCS ========
          {
            pnlUCS.setName("pnlUCS");
            pnlUCS.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlUCS.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlUCS.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlUCS.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlUCS.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- snUniteCS ----
            snUniteCS.setName("snUniteCS");
            pnlUCS.add(snUniteCS, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbEgal ----
            lbEgal.setText("= en US");
            lbEgal.setMaximumSize(new Dimension(60, 30));
            lbEgal.setMinimumSize(new Dimension(60, 30));
            lbEgal.setPreferredSize(new Dimension(60, 30));
            lbEgal.setHorizontalAlignment(SwingConstants.CENTER);
            lbEgal.setFont(lbEgal.getFont().deriveFont(lbEgal.getFont().getStyle() | Font.BOLD));
            lbEgal.setName("lbEgal");
            pnlUCS.add(lbEgal, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5KCS ----
            P5KCS.setPreferredSize(new Dimension(80, 30));
            P5KCS.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5KCS.setName("P5KCS");
            pnlUCS.add(P5KCS, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUS ----
            lbUS.setText("US");
            lbUS.setName("lbUS");
            pnlUCS.add(lbUS, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlUnite.add(pnlUCS, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbUniteCA ----
          lbUniteCA.setText("Unit\u00e9 de conditionnement d'achat (UCA)");
          lbUniteCA.setMaximumSize(new Dimension(300, 30));
          lbUniteCA.setMinimumSize(new Dimension(300, 30));
          lbUniteCA.setPreferredSize(new Dimension(300, 30));
          lbUniteCA.setName("lbUniteCA");
          pnlUnite.add(lbUniteCA, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlUCA ========
          {
            pnlUCA.setName("pnlUCA");
            pnlUCA.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlUCA.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlUCA.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlUCA.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlUCA.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- snUniteCA ----
            snUniteCA.setName("snUniteCA");
            pnlUCA.add(snUniteCA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbEgal2 ----
            lbEgal2.setText("= en UA");
            lbEgal2.setMaximumSize(new Dimension(60, 30));
            lbEgal2.setMinimumSize(new Dimension(60, 30));
            lbEgal2.setPreferredSize(new Dimension(60, 30));
            lbEgal2.setHorizontalAlignment(SwingConstants.CENTER);
            lbEgal2.setFont(lbEgal2.getFont().deriveFont(lbEgal2.getFont().getStyle() | Font.BOLD));
            lbEgal2.setName("lbEgal2");
            pnlUCA.add(lbEgal2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- P5KACX ----
            P5KACX.setMinimumSize(new Dimension(80, 30));
            P5KACX.setMaximumSize(new Dimension(80, 30));
            P5KACX.setPreferredSize(new Dimension(80, 30));
            P5KACX.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5KACX.setName("P5KACX");
            pnlUCA.add(P5KACX, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbUA ----
            lbUA.setText("UA");
            lbUA.setName("lbUA");
            pnlUCA.add(lbUA, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbEgal3 ----
            lbEgal3.setText("= en US");
            lbEgal3.setMaximumSize(new Dimension(60, 30));
            lbEgal3.setMinimumSize(new Dimension(60, 30));
            lbEgal3.setPreferredSize(new Dimension(60, 30));
            lbEgal3.setHorizontalAlignment(SwingConstants.CENTER);
            lbEgal3.setFont(lbEgal3.getFont().deriveFont(lbEgal3.getFont().getStyle() | Font.BOLD));
            lbEgal3.setName("lbEgal3");
            pnlUCA.add(lbEgal3, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5KSC ----
            P5KSC.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5KSC.setName("P5KSC");
            pnlUCA.add(P5KSC, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUS1 ----
            lbUS1.setText("US");
            lbUS1.setName("lbUS1");
            pnlUCA.add(lbUS1, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlUnite.add(pnlUCA, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbUniteAchat ----
          lbUniteAchat.setText("Unit\u00e9 d'achat (UA)");
          lbUniteAchat.setMaximumSize(new Dimension(300, 30));
          lbUniteAchat.setMinimumSize(new Dimension(300, 30));
          lbUniteAchat.setPreferredSize(new Dimension(300, 30));
          lbUniteAchat.setName("lbUniteAchat");
          pnlUnite.add(lbUniteAchat, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlUA ========
          {
            pnlUA.setName("pnlUA");
            pnlUA.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlUA.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlUA.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlUA.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlUA.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- snUniteA ----
            snUniteA.setName("snUniteA");
            snUniteA.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snUniteAValueChanged(e);
              }
            });
            pnlUA.add(snUniteA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbEgal4 ----
            lbEgal4.setText("= en US");
            lbEgal4.setMaximumSize(new Dimension(60, 30));
            lbEgal4.setMinimumSize(new Dimension(60, 30));
            lbEgal4.setPreferredSize(new Dimension(60, 30));
            lbEgal4.setHorizontalAlignment(SwingConstants.CENTER);
            lbEgal4.setFont(lbEgal4.getFont().deriveFont(lbEgal4.getFont().getStyle() | Font.BOLD));
            lbEgal4.setName("lbEgal4");
            pnlUA.add(lbEgal4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5KSA ----
            P5KSA.setMinimumSize(new Dimension(80, 30));
            P5KSA.setMaximumSize(new Dimension(80, 30));
            P5KSA.setPreferredSize(new Dimension(80, 30));
            P5KSA.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5KSA.setName("P5KSA");
            pnlUA.add(P5KSA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUS2 ----
            lbUS2.setText("US");
            lbUS2.setName("lbUS2");
            pnlUA.add(lbUS2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlUnite.add(pnlUA, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbUniteVente ----
          lbUniteVente.setText("Unit\u00e9 de vente (UV)");
          lbUniteVente.setMaximumSize(new Dimension(300, 30));
          lbUniteVente.setMinimumSize(new Dimension(300, 30));
          lbUniteVente.setPreferredSize(new Dimension(300, 30));
          lbUniteVente.setName("lbUniteVente");
          pnlUnite.add(lbUniteVente, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlUV ========
          {
            pnlUV.setName("pnlUV");
            pnlUV.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlUV.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlUV.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlUV.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlUV.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- snUniteV ----
            snUniteV.setName("snUniteV");
            pnlUV.add(snUniteV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbEgal5 ----
            lbEgal5.setText("= en US");
            lbEgal5.setMaximumSize(new Dimension(60, 30));
            lbEgal5.setMinimumSize(new Dimension(60, 30));
            lbEgal5.setPreferredSize(new Dimension(60, 30));
            lbEgal5.setHorizontalAlignment(SwingConstants.CENTER);
            lbEgal5.setFont(lbEgal5.getFont().deriveFont(lbEgal5.getFont().getStyle() | Font.BOLD));
            lbEgal5.setName("lbEgal5");
            pnlUV.add(lbEgal5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5KSVX ----
            P5KSVX.setMinimumSize(new Dimension(80, 30));
            P5KSVX.setMaximumSize(new Dimension(80, 30));
            P5KSVX.setPreferredSize(new Dimension(80, 30));
            P5KSVX.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5KSVX.setName("P5KSVX");
            pnlUV.add(P5KSVX, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUS3 ----
            lbUS3.setText("US");
            lbUS3.setName("lbUS3");
            pnlUV.add(lbUS3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlUnite.add(pnlUV, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbUniteTransport ----
          lbUniteTransport.setText("Unit\u00e9 de transport (UT)");
          lbUniteTransport.setMaximumSize(new Dimension(300, 30));
          lbUniteTransport.setMinimumSize(new Dimension(300, 30));
          lbUniteTransport.setPreferredSize(new Dimension(300, 30));
          lbUniteTransport.setName("lbUniteTransport");
          pnlUnite.add(lbUniteTransport, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlUT ========
          {
            pnlUT.setName("pnlUT");
            pnlUT.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlUT.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlUT.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlUT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlUT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- snUniteT ----
            snUniteT.setName("snUniteT");
            pnlUT.add(snUniteT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbEgal6 ----
            lbEgal6.setText("= en US");
            lbEgal6.setMaximumSize(new Dimension(60, 30));
            lbEgal6.setMinimumSize(new Dimension(60, 30));
            lbEgal6.setPreferredSize(new Dimension(60, 30));
            lbEgal6.setHorizontalAlignment(SwingConstants.CENTER);
            lbEgal6.setFont(lbEgal6.getFont().deriveFont(lbEgal6.getFont().getStyle() | Font.BOLD));
            lbEgal6.setName("lbEgal6");
            pnlUT.add(lbEgal6, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5KTS ----
            P5KTS.setMinimumSize(new Dimension(80, 30));
            P5KTS.setMaximumSize(new Dimension(80, 30));
            P5KTS.setPreferredSize(new Dimension(80, 30));
            P5KTS.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5KTS.setName("P5KTS");
            pnlUT.add(P5KTS, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUS4 ----
            lbUS4.setText("US");
            lbUS4.setName("lbUS4");
            pnlUT.add(lbUS4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlUnite.add(pnlUT, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbUniteCS6 ----
          lbUniteCS6.setText("Unit\u00e9 de pr\u00e9sentation (UP)");
          lbUniteCS6.setMaximumSize(new Dimension(300, 30));
          lbUniteCS6.setMinimumSize(new Dimension(300, 30));
          lbUniteCS6.setPreferredSize(new Dimension(300, 30));
          lbUniteCS6.setName("lbUniteCS6");
          pnlUnite.add(lbUniteCS6, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlUP ========
          {
            pnlUP.setName("pnlUP");
            pnlUP.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlUP.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlUP.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlUP.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlUP.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- snUniteP ----
            snUniteP.setName("snUniteP");
            pnlUP.add(snUniteP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlUnite.add(pnlUP, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlUnite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlCNV ========
        {
          pnlCNV.setTitre("Conditionnements de vente");
          pnlCNV.setName("pnlCNV");
          pnlCNV.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCNV.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlCNV.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCNV.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCNV.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCV1 ========
          {
            pnlCV1.setName("pnlCV1");
            pnlCV1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCV1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCV1.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlCV1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCV1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbCNV1 ----
            lbCNV1.setText("Conditionnement de vente 1 (UCV)");
            lbCNV1.setMaximumSize(new Dimension(250, 30));
            lbCNV1.setMinimumSize(new Dimension(250, 30));
            lbCNV1.setPreferredSize(new Dimension(250, 30));
            lbCNV1.setName("lbCNV1");
            pnlCV1.add(lbCNV1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snUniteCN1 ----
            snUniteCN1.setName("snUniteCN1");
            pnlCV1.add(snUniteCN1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbEgal7 ----
            lbEgal7.setText("= en UV");
            lbEgal7.setMaximumSize(new Dimension(60, 30));
            lbEgal7.setMinimumSize(new Dimension(60, 30));
            lbEgal7.setPreferredSize(new Dimension(60, 30));
            lbEgal7.setHorizontalAlignment(SwingConstants.CENTER);
            lbEgal7.setFont(lbEgal7.getFont().deriveFont(lbEgal7.getFont().getStyle() | Font.BOLD));
            lbEgal7.setName("lbEgal7");
            pnlCV1.add(lbEgal7, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5CNDX ----
            P5CNDX.setPreferredSize(new Dimension(80, 30));
            P5CNDX.setMinimumSize(new Dimension(80, 30));
            P5CNDX.setMaximumSize(new Dimension(80, 30));
            P5CNDX.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5CNDX.setName("P5CNDX");
            pnlCV1.add(P5CNDX, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snUniteNL1 ----
            snUniteNL1.setName("snUniteNL1");
            pnlCV1.add(snUniteNL1, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbCodeBarres1 ----
            lbCodeBarres1.setText("Code \u00e0 barres");
            lbCodeBarres1.setMaximumSize(new Dimension(120, 30));
            lbCodeBarres1.setMinimumSize(new Dimension(120, 30));
            lbCodeBarres1.setPreferredSize(new Dimension(120, 30));
            lbCodeBarres1.setName("lbCodeBarres1");
            pnlCV1.add(lbCodeBarres1, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5GCD1 ----
            P5GCD1.setPreferredSize(new Dimension(150, 30));
            P5GCD1.setMinimumSize(new Dimension(150, 30));
            P5GCD1.setMaximumSize(new Dimension(150, 30));
            P5GCD1.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5GCD1.setName("P5GCD1");
            pnlCV1.add(P5GCD1, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCNV.add(pnlCV1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlCV2 ========
          {
            pnlCV2.setName("pnlCV2");
            pnlCV2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCV2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCV2.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlCV2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCV2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbCNV2 ----
            lbCNV2.setText("Conditionnement de vente 2 (UCV)");
            lbCNV2.setMaximumSize(new Dimension(250, 30));
            lbCNV2.setMinimumSize(new Dimension(250, 30));
            lbCNV2.setPreferredSize(new Dimension(250, 30));
            lbCNV2.setName("lbCNV2");
            pnlCV2.add(lbCNV2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snUniteCN2 ----
            snUniteCN2.setName("snUniteCN2");
            pnlCV2.add(snUniteCN2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbEgal8 ----
            lbEgal8.setText("= en UV");
            lbEgal8.setMaximumSize(new Dimension(60, 30));
            lbEgal8.setMinimumSize(new Dimension(60, 30));
            lbEgal8.setPreferredSize(new Dimension(60, 30));
            lbEgal8.setHorizontalAlignment(SwingConstants.CENTER);
            lbEgal8.setFont(lbEgal8.getFont().deriveFont(lbEgal8.getFont().getStyle() | Font.BOLD));
            lbEgal8.setName("lbEgal8");
            pnlCV2.add(lbEgal8, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5CN2X ----
            P5CN2X.setMaximumSize(new Dimension(80, 30));
            P5CN2X.setMinimumSize(new Dimension(80, 30));
            P5CN2X.setPreferredSize(new Dimension(80, 30));
            P5CN2X.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5CN2X.setName("P5CN2X");
            pnlCV2.add(P5CN2X, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snUniteNL2 ----
            snUniteNL2.setName("snUniteNL2");
            pnlCV2.add(snUniteNL2, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbCodeBarres2 ----
            lbCodeBarres2.setText("Code \u00e0 barres");
            lbCodeBarres2.setMaximumSize(new Dimension(120, 30));
            lbCodeBarres2.setMinimumSize(new Dimension(120, 30));
            lbCodeBarres2.setPreferredSize(new Dimension(120, 30));
            lbCodeBarres2.setName("lbCodeBarres2");
            pnlCV2.add(lbCodeBarres2, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5GCD2 ----
            P5GCD2.setPreferredSize(new Dimension(150, 30));
            P5GCD2.setMinimumSize(new Dimension(150, 30));
            P5GCD2.setMaximumSize(new Dimension(150, 30));
            P5GCD2.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5GCD2.setName("P5GCD2");
            pnlCV2.add(P5GCD2, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCNV.add(pnlCV2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlCV3 ========
          {
            pnlCV3.setName("pnlCV3");
            pnlCV3.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCV3.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCV3.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlCV3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCV3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbCNV3 ----
            lbCNV3.setText("Conditionnement de vente 3 (UCV)");
            lbCNV3.setMaximumSize(new Dimension(250, 30));
            lbCNV3.setMinimumSize(new Dimension(250, 30));
            lbCNV3.setPreferredSize(new Dimension(250, 30));
            lbCNV3.setName("lbCNV3");
            pnlCV3.add(lbCNV3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snUniteCN3 ----
            snUniteCN3.setName("snUniteCN3");
            pnlCV3.add(snUniteCN3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbEgal9 ----
            lbEgal9.setText("= en UV");
            lbEgal9.setMaximumSize(new Dimension(60, 30));
            lbEgal9.setMinimumSize(new Dimension(60, 30));
            lbEgal9.setPreferredSize(new Dimension(60, 30));
            lbEgal9.setHorizontalAlignment(SwingConstants.CENTER);
            lbEgal9.setFont(lbEgal9.getFont().deriveFont(lbEgal9.getFont().getStyle() | Font.BOLD));
            lbEgal9.setName("lbEgal9");
            pnlCV3.add(lbEgal9, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5CN3X ----
            P5CN3X.setMaximumSize(new Dimension(80, 30));
            P5CN3X.setMinimumSize(new Dimension(80, 30));
            P5CN3X.setPreferredSize(new Dimension(80, 30));
            P5CN3X.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5CN3X.setName("P5CN3X");
            pnlCV3.add(P5CN3X, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snUniteNL3 ----
            snUniteNL3.setName("snUniteNL3");
            pnlCV3.add(snUniteNL3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbCodeBarres3 ----
            lbCodeBarres3.setText("Code \u00e0 barres");
            lbCodeBarres3.setMaximumSize(new Dimension(120, 30));
            lbCodeBarres3.setMinimumSize(new Dimension(120, 30));
            lbCodeBarres3.setPreferredSize(new Dimension(120, 30));
            lbCodeBarres3.setName("lbCodeBarres3");
            pnlCV3.add(lbCodeBarres3, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5GCD3 ----
            P5GCD3.setPreferredSize(new Dimension(150, 30));
            P5GCD3.setMinimumSize(new Dimension(150, 30));
            P5GCD3.setMaximumSize(new Dimension(150, 30));
            P5GCD3.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5GCD3.setName("P5GCD3");
            pnlCV3.add(P5GCD3, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCNV.add(pnlCV3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlCV4 ========
          {
            pnlCV4.setName("pnlCV4");
            pnlCV4.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCV4.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCV4.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlCV4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCV4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbCNV4 ----
            lbCNV4.setText("Conditionnement de vente 4 (UCV)");
            lbCNV4.setMaximumSize(new Dimension(250, 30));
            lbCNV4.setMinimumSize(new Dimension(250, 30));
            lbCNV4.setPreferredSize(new Dimension(250, 30));
            lbCNV4.setName("lbCNV4");
            pnlCV4.add(lbCNV4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snUniteCN4 ----
            snUniteCN4.setName("snUniteCN4");
            pnlCV4.add(snUniteCN4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbEgal10 ----
            lbEgal10.setText("= en UV");
            lbEgal10.setMaximumSize(new Dimension(60, 30));
            lbEgal10.setMinimumSize(new Dimension(60, 30));
            lbEgal10.setPreferredSize(new Dimension(60, 30));
            lbEgal10.setHorizontalAlignment(SwingConstants.CENTER);
            lbEgal10.setFont(lbEgal10.getFont().deriveFont(lbEgal10.getFont().getStyle() | Font.BOLD));
            lbEgal10.setName("lbEgal10");
            pnlCV4.add(lbEgal10, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5CN4X ----
            P5CN4X.setMaximumSize(new Dimension(80, 30));
            P5CN4X.setMinimumSize(new Dimension(80, 30));
            P5CN4X.setPreferredSize(new Dimension(80, 30));
            P5CN4X.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5CN4X.setName("P5CN4X");
            pnlCV4.add(P5CN4X, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snUniteNL4 ----
            snUniteNL4.setName("snUniteNL4");
            pnlCV4.add(snUniteNL4, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbCodeBarres4 ----
            lbCodeBarres4.setText("Code \u00e0 barres");
            lbCodeBarres4.setMaximumSize(new Dimension(120, 30));
            lbCodeBarres4.setMinimumSize(new Dimension(120, 30));
            lbCodeBarres4.setPreferredSize(new Dimension(120, 30));
            lbCodeBarres4.setName("lbCodeBarres4");
            pnlCV4.add(lbCodeBarres4, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- P5GCD4 ----
            P5GCD4.setPreferredSize(new Dimension(150, 30));
            P5GCD4.setMinimumSize(new Dimension(150, 30));
            P5GCD4.setMaximumSize(new Dimension(150, 30));
            P5GCD4.setFont(new Font("sansserif", Font.PLAIN, 14));
            P5GCD4.setName("P5GCD4");
            pnlCV4.add(P5GCD4, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCNV.add(pnlCV4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlCNV, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlPrincipal;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlUnite;
  private SNLabelChamp lbUniteStockage;
  private SNPanel pnlUS;
  private SNUnite snUniteS;
  private SNLabelChamp lbCodeBarres;
  private XRiTextField P5GCD0;
  private SNLabelChamp lbUniteCS;
  private SNPanel pnlUCS;
  private SNUnite snUniteCS;
  private SNLabelChamp lbEgal;
  private XRiTextField P5KCS;
  private SNLabelUnite lbUS;
  private SNLabelChamp lbUniteCA;
  private SNPanel pnlUCA;
  private SNUnite snUniteCA;
  private SNLabelChamp lbEgal2;
  private XRiTextField P5KACX;
  private SNLabelUnite lbUA;
  private SNLabelChamp lbEgal3;
  private XRiTextField P5KSC;
  private SNLabelUnite lbUS1;
  private SNLabelChamp lbUniteAchat;
  private SNPanel pnlUA;
  private SNUnite snUniteA;
  private SNLabelChamp lbEgal4;
  private XRiTextField P5KSA;
  private SNLabelUnite lbUS2;
  private SNLabelChamp lbUniteVente;
  private SNPanel pnlUV;
  private SNUnite snUniteV;
  private SNLabelChamp lbEgal5;
  private XRiTextField P5KSVX;
  private SNLabelUnite lbUS3;
  private SNLabelChamp lbUniteTransport;
  private SNPanel pnlUT;
  private SNUnite snUniteT;
  private SNLabelChamp lbEgal6;
  private XRiTextField P5KTS;
  private SNLabelUnite lbUS4;
  private SNLabelChamp lbUniteCS6;
  private SNPanel pnlUP;
  private SNUnite snUniteP;
  private SNPanelTitre pnlCNV;
  private SNPanel pnlCV1;
  private SNLabelChamp lbCNV1;
  private SNUnite snUniteCN1;
  private SNLabelChamp lbEgal7;
  private XRiTextField P5CNDX;
  private SNUnite snUniteNL1;
  private SNLabelChamp lbCodeBarres1;
  private XRiTextField P5GCD1;
  private SNPanel pnlCV2;
  private SNLabelChamp lbCNV2;
  private SNUnite snUniteCN2;
  private SNLabelChamp lbEgal8;
  private XRiTextField P5CN2X;
  private SNUnite snUniteNL2;
  private SNLabelChamp lbCodeBarres2;
  private XRiTextField P5GCD2;
  private SNPanel pnlCV3;
  private SNLabelChamp lbCNV3;
  private SNUnite snUniteCN3;
  private SNLabelChamp lbEgal9;
  private XRiTextField P5CN3X;
  private SNUnite snUniteNL3;
  private SNLabelChamp lbCodeBarres3;
  private XRiTextField P5GCD3;
  private SNPanel pnlCV4;
  private SNLabelChamp lbCNV4;
  private SNUnite snUniteCN4;
  private SNLabelChamp lbEgal10;
  private XRiTextField P5CN4X;
  private SNUnite snUniteNL4;
  private SNLabelChamp lbCodeBarres4;
  private XRiTextField P5GCD4;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
