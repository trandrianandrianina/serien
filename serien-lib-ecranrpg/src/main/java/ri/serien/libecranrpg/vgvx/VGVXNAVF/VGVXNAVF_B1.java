
package ri.serien.libecranrpg.vgvx.VGVXNAVF;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXNAVF_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String uniteAchat = "";
  
  public VGVXNAVF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    // Configuration de la barre de bouton
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      // Traitement des actions bouton
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    ULBUNC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("1  @ULBUNC@  @CAUNC@")).trim());
    lbUniteStock.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    lbUniteAchat3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCAUNA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Unité d'achat
    uniteAchat = lexique.HostFieldGetData("WCAUNA");
    lbUniteAchat.setText(uniteAchat);
    lbUniteAchat2.setText(uniteAchat);
    
    // IdEtablissement pour composants SN
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("NVETB"));
    
    // Article
    snArticle.setSession(getSession());
    snArticle.setIdEtablissement(idEtablissement);
    snArticle.charger(false);
    snArticle.setSelectionParChampRPG(lexique, "A1ART");
    snArticle.setEnabled(false);
    
    // Fournisseur
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(idEtablissement);
    snFournisseur.charger(false);
    snFournisseur.setSelectionParChampRPG(lexique, "CACOL", "CAFRS");
    snFournisseur.setEnabled(false);
    
    // RadioBouttons
    rbCoeffPort.setSelected(lexique.HostFieldGetData("NVFP1").equals("0000"));
    rbMontantPort.setSelected(!rbCoeffPort.isSelected());
    traiterActivationPort();
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  /**
   * Actions des boutons
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void traiterActivationPort() {
    NVFV1.setEnabled(rbMontantPort.isSelected());
    NVFK1.setEnabled(rbMontantPort.isSelected());
    NVFP1.setEnabled(rbCoeffPort.isSelected());
  }
  
  private void rbMontantPortActionPerformed(ActionEvent e) {
    try {
      NVFP1.setText("");
      traiterActivationPort();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbCoeffPortActionPerformed(ActionEvent e) {
    try {
      NVFV1.setText("");
      NVFK1.setText("");
      traiterActivationPort();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPrixAchatBrutFocusLost(FocusEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBandeau = new SNPanel();
    pbPresentation = new SNBandeauTitre();
    pnlPrincipal = new SNPanelContenu();
    sNPanel1 = new SNPanel();
    lbArticle = new SNLabelChamp();
    snArticle = new SNArticle();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    pnlNegociationAchat = new SNPanelTitre();
    lbDateApplicationAchat = new SNLabelChamp();
    sNPanel3 = new SNPanel();
    NVDAPX = new XRiCalendrier();
    lbDateApplicationAchat2 = new JLabel();
    NVDAFX = new XRiCalendrier();
    lbPrixAchatBrut = new SNLabelChamp();
    sNPanel2 = new SNPanel();
    NVPRAX = new XRiTextField();
    sNLabelChamp2 = new SNLabelChamp();
    lbUniteAchat = new SNLabelUnite();
    lbRemisesFournisseur = new SNLabelChamp();
    pnlRemises = new SNPanel();
    NVREX1 = new XRiTextField();
    lbPourcentage1 = new SNLabelUnite();
    NVREX2 = new XRiTextField();
    lbPourcentage2 = new SNLabelUnite();
    NVREX3 = new XRiTextField();
    lbPourcentage3 = new SNLabelUnite();
    NVREX4 = new XRiTextField();
    lbPourcentage4 = new SNLabelUnite();
    lbTaxesAchat = new SNLabelChamp();
    pnlMajoration = new SNPanel();
    NVFK3 = new XRiTextField();
    lbPourcentage6 = new SNLabelUnite();
    lbTaxesAchat2 = new SNLabelChamp();
    NVFV3 = new XRiTextField();
    rbMontantPort = new JRadioButton();
    pnlMontantPort = new SNPanel();
    NVFV1 = new XRiTextField();
    lbSymboleMultiplication = new JLabel();
    NVFK1 = new XRiTextField();
    sNLabelChamp3 = new SNLabelChamp();
    rbCoeffPort = new JRadioButton();
    NVFP1 = new XRiTextField();
    lbPourcentage5 = new SNLabelUnite();
    pnlPourcentagePort = new SNPanel();
    lbPRVAchat = new SNLabelChamp();
    pnlPRS = new SNPanel();
    WPRSA = new XRiTextField();
    sNLabelChamp1 = new SNLabelChamp();
    lbUniteAchat2 = new SNLabelUnite();
    sNPanelTitre1 = new SNPanelTitre();
    lbTitreUCA = new SNLabelTitre();
    lbTitreUCA2 = new SNLabelTitre();
    lbTitreUCA3 = new SNLabelTitre();
    ULBUNC = new SNLabelUnite();
    lbEgal = new SNLabelChamp();
    CAKSC = new XRiTextField();
    lbUniteStock = new SNLabelUnite();
    lbEgal3 = new SNLabelChamp();
    CAKAC = new XRiTextField();
    lbUniteAchat3 = new SNLabelUnite();
    snBarreBouton = new SNBarreBouton();
    buttonGroup1 = new ButtonGroup();
    
    // ======== this ========
    setPreferredSize(new Dimension(800, 600));
    setMinimumSize(new Dimension(800, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlBandeau ========
    {
      pnlBandeau.setName("pnlBandeau");
      pnlBandeau.setLayout(new VerticalLayout());
      
      // ---- pbPresentation ----
      pbPresentation.setText("N\u00e9gociation d'achats");
      pbPresentation.setName("pbPresentation");
      pnlBandeau.add(pbPresentation);
    }
    add(pnlBandeau, BorderLayout.PAGE_START);
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlPrincipal.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlPrincipal.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlPrincipal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlPrincipal.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== sNPanel1 ========
      {
        sNPanel1.setName("sNPanel1");
        sNPanel1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbArticle ----
        lbArticle.setText("Article");
        lbArticle.setName("lbArticle");
        sNPanel1.add(lbArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snArticle ----
        snArticle.setName("snArticle");
        sNPanel1.add(snArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbFournisseur ----
        lbFournisseur.setText("Fournisseur");
        lbFournisseur.setName("lbFournisseur");
        sNPanel1.add(lbFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snFournisseur ----
        snFournisseur.setName("snFournisseur");
        sNPanel1.add(snFournisseur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlPrincipal.add(sNPanel1,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlNegociationAchat ========
      {
        pnlNegociationAchat.setTitre("N\u00e9gociation");
        pnlNegociationAchat.setName("pnlNegociationAchat");
        pnlNegociationAchat.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlNegociationAchat.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlNegociationAchat.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlNegociationAchat.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlNegociationAchat.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbDateApplicationAchat ----
        lbDateApplicationAchat.setText("Valide du");
        lbDateApplicationAchat.setMinimumSize(new Dimension(250, 30));
        lbDateApplicationAchat.setPreferredSize(new Dimension(250, 30));
        lbDateApplicationAchat.setMaximumSize(new Dimension(250, 30));
        lbDateApplicationAchat.setName("lbDateApplicationAchat");
        pnlNegociationAchat.add(lbDateApplicationAchat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== sNPanel3 ========
        {
          sNPanel3.setName("sNPanel3");
          sNPanel3.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel3.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) sNPanel3.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanel3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- NVDAPX ----
          NVDAPX.setComponentPopupMenu(null);
          NVDAPX.setPreferredSize(new Dimension(115, 30));
          NVDAPX.setMinimumSize(new Dimension(115, 30));
          NVDAPX.setMaximumSize(new Dimension(105, 30));
          NVDAPX.setFont(new Font("sansserif", Font.PLAIN, 14));
          NVDAPX.setEnabled(false);
          NVDAPX.setName("NVDAPX");
          sNPanel3.add(NVDAPX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbDateApplicationAchat2 ----
          lbDateApplicationAchat2.setText("jusqu'au");
          lbDateApplicationAchat2.setFont(lbDateApplicationAchat2.getFont().deriveFont(lbDateApplicationAchat2.getFont().getSize() + 2f));
          lbDateApplicationAchat2.setHorizontalAlignment(SwingConstants.RIGHT);
          lbDateApplicationAchat2.setMinimumSize(new Dimension(53, 30));
          lbDateApplicationAchat2.setPreferredSize(new Dimension(53, 19));
          lbDateApplicationAchat2.setName("lbDateApplicationAchat2");
          sNPanel3.add(lbDateApplicationAchat2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- NVDAFX ----
          NVDAFX.setComponentPopupMenu(null);
          NVDAFX.setPreferredSize(new Dimension(115, 30));
          NVDAFX.setMinimumSize(new Dimension(115, 30));
          NVDAFX.setMaximumSize(new Dimension(105, 30));
          NVDAFX.setFont(new Font("sansserif", Font.PLAIN, 14));
          NVDAFX.setEnabled(false);
          NVDAFX.setName("NVDAFX");
          sNPanel3.add(NVDAFX, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlNegociationAchat.add(sNPanel3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPrixAchatBrut ----
        lbPrixAchatBrut.setText("Prix d'achat brut ");
        lbPrixAchatBrut.setFont(lbPrixAchatBrut.getFont().deriveFont(lbPrixAchatBrut.getFont().getStyle() | Font.BOLD,
            lbPrixAchatBrut.getFont().getSize() + 2f));
        lbPrixAchatBrut.setHorizontalAlignment(SwingConstants.RIGHT);
        lbPrixAchatBrut.setComponentPopupMenu(null);
        lbPrixAchatBrut.setPreferredSize(new Dimension(250, 30));
        lbPrixAchatBrut.setMinimumSize(new Dimension(250, 30));
        lbPrixAchatBrut.setMaximumSize(new Dimension(250, 30));
        lbPrixAchatBrut.setName("lbPrixAchatBrut");
        pnlNegociationAchat.add(lbPrixAchatBrut, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== sNPanel2 ========
        {
          sNPanel2.setName("sNPanel2");
          sNPanel2.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- NVPRAX ----
          NVPRAX.setHorizontalAlignment(SwingConstants.RIGHT);
          NVPRAX.setFont(NVPRAX.getFont().deriveFont(NVPRAX.getFont().getSize() + 2f));
          NVPRAX.setComponentPopupMenu(null);
          NVPRAX.setMinimumSize(new Dimension(110, 30));
          NVPRAX.setPreferredSize(new Dimension(110, 30));
          NVPRAX.setName("NVPRAX");
          sNPanel2.add(NVPRAX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- sNLabelChamp2 ----
          sNLabelChamp2.setText("par");
          sNLabelChamp2.setMaximumSize(new Dimension(30, 30));
          sNLabelChamp2.setMinimumSize(new Dimension(30, 30));
          sNLabelChamp2.setPreferredSize(new Dimension(30, 30));
          sNLabelChamp2.setName("sNLabelChamp2");
          sNPanel2.add(sNLabelChamp2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbUniteAchat ----
          lbUniteAchat.setText("UA");
          lbUniteAchat.setComponentPopupMenu(null);
          lbUniteAchat.setName("lbUniteAchat");
          sNPanel2.add(lbUniteAchat, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlNegociationAchat.add(sNPanel2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbRemisesFournisseur ----
        lbRemisesFournisseur.setText("Remises ");
        lbRemisesFournisseur.setFont(lbRemisesFournisseur.getFont().deriveFont(lbRemisesFournisseur.getFont().getStyle() & ~Font.BOLD,
            lbRemisesFournisseur.getFont().getSize() + 2f));
        lbRemisesFournisseur.setHorizontalAlignment(SwingConstants.RIGHT);
        lbRemisesFournisseur.setComponentPopupMenu(null);
        lbRemisesFournisseur.setPreferredSize(new Dimension(250, 30));
        lbRemisesFournisseur.setMinimumSize(new Dimension(250, 30));
        lbRemisesFournisseur.setMaximumSize(new Dimension(250, 30));
        lbRemisesFournisseur.setName("lbRemisesFournisseur");
        pnlNegociationAchat.add(lbRemisesFournisseur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlRemises ========
        {
          pnlRemises.setName("pnlRemises");
          pnlRemises.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRemises.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlRemises.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRemises.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRemises.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- NVREX1 ----
          NVREX1.setHorizontalAlignment(SwingConstants.RIGHT);
          NVREX1.setFont(NVREX1.getFont().deriveFont(NVREX1.getFont().getSize() + 2f));
          NVREX1.setComponentPopupMenu(null);
          NVREX1.setPreferredSize(new Dimension(60, 30));
          NVREX1.setMinimumSize(new Dimension(60, 30));
          NVREX1.setName("NVREX1");
          pnlRemises.add(NVREX1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage1 ----
          lbPourcentage1.setText("% ");
          lbPourcentage1.setHorizontalAlignment(SwingConstants.LEFT);
          lbPourcentage1.setComponentPopupMenu(null);
          lbPourcentage1.setName("lbPourcentage1");
          pnlRemises.add(lbPourcentage1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- NVREX2 ----
          NVREX2.setHorizontalAlignment(SwingConstants.RIGHT);
          NVREX2.setFont(NVREX2.getFont().deriveFont(NVREX2.getFont().getSize() + 2f));
          NVREX2.setComponentPopupMenu(null);
          NVREX2.setPreferredSize(new Dimension(60, 30));
          NVREX2.setMinimumSize(new Dimension(60, 30));
          NVREX2.setName("NVREX2");
          pnlRemises.add(NVREX2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage2 ----
          lbPourcentage2.setText("% ");
          lbPourcentage2.setHorizontalAlignment(SwingConstants.LEFT);
          lbPourcentage2.setComponentPopupMenu(null);
          lbPourcentage2.setName("lbPourcentage2");
          pnlRemises.add(lbPourcentage2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- NVREX3 ----
          NVREX3.setHorizontalAlignment(SwingConstants.RIGHT);
          NVREX3.setFont(NVREX3.getFont().deriveFont(NVREX3.getFont().getSize() + 2f));
          NVREX3.setComponentPopupMenu(null);
          NVREX3.setPreferredSize(new Dimension(60, 30));
          NVREX3.setMinimumSize(new Dimension(60, 30));
          NVREX3.setName("NVREX3");
          pnlRemises.add(NVREX3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage3 ----
          lbPourcentage3.setText("% ");
          lbPourcentage3.setHorizontalAlignment(SwingConstants.LEFT);
          lbPourcentage3.setComponentPopupMenu(null);
          lbPourcentage3.setName("lbPourcentage3");
          pnlRemises.add(lbPourcentage3, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- NVREX4 ----
          NVREX4.setHorizontalAlignment(SwingConstants.RIGHT);
          NVREX4.setFont(NVREX4.getFont().deriveFont(NVREX4.getFont().getSize() + 2f));
          NVREX4.setComponentPopupMenu(null);
          NVREX4.setPreferredSize(new Dimension(60, 30));
          NVREX4.setMinimumSize(new Dimension(60, 30));
          NVREX4.setName("NVREX4");
          pnlRemises.add(NVREX4, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage4 ----
          lbPourcentage4.setText("% ");
          lbPourcentage4.setHorizontalAlignment(SwingConstants.LEFT);
          lbPourcentage4.setComponentPopupMenu(null);
          lbPourcentage4.setName("lbPourcentage4");
          pnlRemises.add(lbPourcentage4, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlNegociationAchat.add(pnlRemises, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbTaxesAchat ----
        lbTaxesAchat.setText("Taxe ou majoration");
        lbTaxesAchat.setFont(
            lbTaxesAchat.getFont().deriveFont(lbTaxesAchat.getFont().getStyle() & ~Font.BOLD, lbTaxesAchat.getFont().getSize() + 2f));
        lbTaxesAchat.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTaxesAchat.setComponentPopupMenu(null);
        lbTaxesAchat.setPreferredSize(new Dimension(250, 30));
        lbTaxesAchat.setMinimumSize(new Dimension(250, 30));
        lbTaxesAchat.setMaximumSize(new Dimension(250, 30));
        lbTaxesAchat.setName("lbTaxesAchat");
        pnlNegociationAchat.add(lbTaxesAchat, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlMajoration ========
        {
          pnlMajoration.setName("pnlMajoration");
          pnlMajoration.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMajoration.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlMajoration.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlMajoration.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlMajoration.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- NVFK3 ----
          NVFK3.setHorizontalAlignment(SwingConstants.RIGHT);
          NVFK3.setFont(NVFK3.getFont().deriveFont(NVFK3.getFont().getSize() + 2f));
          NVFK3.setComponentPopupMenu(null);
          NVFK3.setMinimumSize(new Dimension(110, 30));
          NVFK3.setPreferredSize(new Dimension(110, 30));
          NVFK3.setName("NVFK3");
          pnlMajoration.add(NVFK3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage6 ----
          lbPourcentage6.setText("% ");
          lbPourcentage6.setHorizontalAlignment(SwingConstants.LEFT);
          lbPourcentage6.setComponentPopupMenu(null);
          lbPourcentage6.setName("lbPourcentage6");
          pnlMajoration.add(lbPourcentage6, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlNegociationAchat.add(pnlMajoration, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbTaxesAchat2 ----
        lbTaxesAchat2.setText("Montant conditionnement");
        lbTaxesAchat2.setFont(
            lbTaxesAchat2.getFont().deriveFont(lbTaxesAchat2.getFont().getStyle() & ~Font.BOLD, lbTaxesAchat2.getFont().getSize() + 2f));
        lbTaxesAchat2.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTaxesAchat2.setComponentPopupMenu(null);
        lbTaxesAchat2.setPreferredSize(new Dimension(250, 30));
        lbTaxesAchat2.setMinimumSize(new Dimension(250, 30));
        lbTaxesAchat2.setMaximumSize(new Dimension(250, 30));
        lbTaxesAchat2.setName("lbTaxesAchat2");
        pnlNegociationAchat.add(lbTaxesAchat2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- NVFV3 ----
        NVFV3.setHorizontalAlignment(SwingConstants.RIGHT);
        NVFV3.setFont(NVFV3.getFont().deriveFont(NVFV3.getFont().getSize() + 2f));
        NVFV3.setComponentPopupMenu(null);
        NVFV3.setMinimumSize(new Dimension(110, 30));
        NVFV3.setPreferredSize(new Dimension(110, 30));
        NVFV3.setName("NVFV3");
        pnlNegociationAchat.add(NVFV3, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- rbMontantPort ----
        rbMontantPort.setText("Montant port");
        rbMontantPort.setFont(rbMontantPort.getFont().deriveFont(rbMontantPort.getFont().getSize() + 2f));
        rbMontantPort.setMaximumSize(new Dimension(120, 19));
        rbMontantPort.setMinimumSize(new Dimension(120, 19));
        rbMontantPort.setPreferredSize(new Dimension(120, 19));
        rbMontantPort.setName("rbMontantPort");
        rbMontantPort.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            rbMontantPortActionPerformed(e);
          }
        });
        pnlNegociationAchat.add(rbMontantPort, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlMontantPort ========
        {
          pnlMontantPort.setName("pnlMontantPort");
          pnlMontantPort.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMontantPort.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlMontantPort.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlMontantPort.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlMontantPort.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- NVFV1 ----
          NVFV1.setHorizontalAlignment(SwingConstants.RIGHT);
          NVFV1.setFont(NVFV1.getFont().deriveFont(NVFV1.getFont().getSize() + 2f));
          NVFV1.setComponentPopupMenu(null);
          NVFV1.setMinimumSize(new Dimension(80, 30));
          NVFV1.setPreferredSize(new Dimension(80, 30));
          NVFV1.setName("NVFV1");
          pnlMontantPort.add(NVFV1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbSymboleMultiplication ----
          lbSymboleMultiplication.setText("x poids");
          lbSymboleMultiplication.setFont(lbSymboleMultiplication.getFont()
              .deriveFont(lbSymboleMultiplication.getFont().getStyle() & ~Font.BOLD, lbSymboleMultiplication.getFont().getSize() + 2f));
          lbSymboleMultiplication.setHorizontalAlignment(SwingConstants.CENTER);
          lbSymboleMultiplication.setPreferredSize(new Dimension(50, 30));
          lbSymboleMultiplication.setMinimumSize(new Dimension(50, 30));
          lbSymboleMultiplication.setMaximumSize(new Dimension(50, 30));
          lbSymboleMultiplication.setName("lbSymboleMultiplication");
          pnlMontantPort.add(lbSymboleMultiplication, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- NVFK1 ----
          NVFK1.setHorizontalAlignment(SwingConstants.RIGHT);
          NVFK1.setFont(NVFK1.getFont().deriveFont(NVFK1.getFont().getSize() + 2f));
          NVFK1.setComponentPopupMenu(null);
          NVFK1.setMinimumSize(new Dimension(80, 30));
          NVFK1.setPreferredSize(new Dimension(80, 30));
          NVFK1.setName("NVFK1");
          pnlMontantPort.add(NVFK1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- sNLabelChamp3 ----
          sNLabelChamp3.setText("ou");
          sNLabelChamp3.setMaximumSize(new Dimension(30, 30));
          sNLabelChamp3.setMinimumSize(new Dimension(30, 30));
          sNLabelChamp3.setPreferredSize(new Dimension(30, 30));
          sNLabelChamp3.setName("sNLabelChamp3");
          pnlMontantPort.add(sNLabelChamp3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- rbCoeffPort ----
          rbCoeffPort.setText("Pourcentage");
          rbCoeffPort.setFont(rbCoeffPort.getFont().deriveFont(rbCoeffPort.getFont().getSize() + 2f));
          rbCoeffPort.setName("rbCoeffPort");
          rbCoeffPort.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              rbCoeffPortActionPerformed(e);
            }
          });
          pnlMontantPort.add(rbCoeffPort, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- NVFP1 ----
          NVFP1.setHorizontalAlignment(SwingConstants.RIGHT);
          NVFP1.setFont(NVFP1.getFont().deriveFont(NVFP1.getFont().getSize() + 2f));
          NVFP1.setComponentPopupMenu(null);
          NVFP1.setMinimumSize(new Dimension(80, 30));
          NVFP1.setPreferredSize(new Dimension(80, 30));
          NVFP1.setName("NVFP1");
          pnlMontantPort.add(NVFP1, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlNegociationAchat.add(pnlMontantPort, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPourcentage5 ----
        lbPourcentage5.setText("% ");
        lbPourcentage5.setHorizontalAlignment(SwingConstants.LEFT);
        lbPourcentage5.setComponentPopupMenu(null);
        lbPourcentage5.setName("lbPourcentage5");
        pnlNegociationAchat.add(lbPourcentage5, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlPourcentagePort ========
        {
          pnlPourcentagePort.setName("pnlPourcentagePort");
          pnlPourcentagePort.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPourcentagePort.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPourcentagePort.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPourcentagePort.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPourcentagePort.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        }
        pnlNegociationAchat.add(pnlPourcentagePort, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPRVAchat ----
        lbPRVAchat.setText("Prix de revient standard ");
        lbPRVAchat
            .setFont(lbPRVAchat.getFont().deriveFont(lbPRVAchat.getFont().getStyle() | Font.BOLD, lbPRVAchat.getFont().getSize() + 2f));
        lbPRVAchat.setHorizontalAlignment(SwingConstants.RIGHT);
        lbPRVAchat.setComponentPopupMenu(null);
        lbPRVAchat.setPreferredSize(new Dimension(250, 30));
        lbPRVAchat.setMinimumSize(new Dimension(250, 30));
        lbPRVAchat.setMaximumSize(new Dimension(250, 30));
        lbPRVAchat.setName("lbPRVAchat");
        pnlNegociationAchat.add(lbPRVAchat, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlPRS ========
        {
          pnlPRS.setName("pnlPRS");
          pnlPRS.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPRS.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlPRS.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPRS.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPRS.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WPRSA ----
          WPRSA.setHorizontalAlignment(SwingConstants.RIGHT);
          WPRSA.setFont(WPRSA.getFont().deriveFont(WPRSA.getFont().getSize() + 2f));
          WPRSA.setComponentPopupMenu(null);
          WPRSA.setMinimumSize(new Dimension(110, 30));
          WPRSA.setPreferredSize(new Dimension(110, 30));
          WPRSA.setEnabled(false);
          WPRSA.setName("WPRSA");
          pnlPRS.add(WPRSA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- sNLabelChamp1 ----
          sNLabelChamp1.setText("par");
          sNLabelChamp1.setMaximumSize(new Dimension(30, 30));
          sNLabelChamp1.setMinimumSize(new Dimension(30, 30));
          sNLabelChamp1.setPreferredSize(new Dimension(30, 30));
          sNLabelChamp1.setName("sNLabelChamp1");
          pnlPRS.add(sNLabelChamp1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbUniteAchat2 ----
          lbUniteAchat2.setText("UA");
          lbUniteAchat2.setComponentPopupMenu(null);
          lbUniteAchat2.setName("lbUniteAchat2");
          pnlPRS.add(lbUniteAchat2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlNegociationAchat.add(pnlPRS, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlPrincipal.add(pnlNegociationAchat,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== sNPanelTitre1 ========
      {
        sNPanelTitre1.setTitre("Unit\u00e9s");
        sNPanelTitre1.setName("sNPanelTitre1");
        sNPanelTitre1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelTitre1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) sNPanelTitre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbTitreUCA ----
        lbTitreUCA.setText("Unit\u00e9s de commande (UCA)");
        lbTitreUCA.setPreferredSize(new Dimension(200, 30));
        lbTitreUCA.setMinimumSize(new Dimension(200, 30));
        lbTitreUCA.setMaximumSize(new Dimension(200, 30));
        lbTitreUCA.setName("lbTitreUCA");
        sNPanelTitre1.add(lbTitreUCA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbTitreUCA2 ----
        lbTitreUCA2.setText("Unit\u00e9s de stock (US)");
        lbTitreUCA2.setPreferredSize(new Dimension(200, 30));
        lbTitreUCA2.setMinimumSize(new Dimension(200, 30));
        lbTitreUCA2.setMaximumSize(new Dimension(200, 30));
        lbTitreUCA2.setName("lbTitreUCA2");
        sNPanelTitre1.add(lbTitreUCA2, new GridBagConstraints(2, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbTitreUCA3 ----
        lbTitreUCA3.setText("Unit\u00e9s d'achat (UA)");
        lbTitreUCA3.setPreferredSize(new Dimension(200, 30));
        lbTitreUCA3.setMinimumSize(new Dimension(200, 30));
        lbTitreUCA3.setMaximumSize(new Dimension(200, 30));
        lbTitreUCA3.setName("lbTitreUCA3");
        sNPanelTitre1.add(lbTitreUCA3, new GridBagConstraints(5, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- ULBUNC ----
        ULBUNC.setComponentPopupMenu(null);
        ULBUNC.setMinimumSize(new Dimension(110, 30));
        ULBUNC.setPreferredSize(new Dimension(110, 30));
        ULBUNC.setText("1  @ULBUNC@  @CAUNC@");
        ULBUNC.setName("ULBUNC");
        ULBUNC.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfPrixAchatBrutFocusLost(e);
          }
        });
        sNPanelTitre1.add(ULBUNC, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbEgal ----
        lbEgal.setText("=");
        lbEgal.setMaximumSize(new Dimension(40, 30));
        lbEgal.setMinimumSize(new Dimension(40, 30));
        lbEgal.setPreferredSize(new Dimension(40, 30));
        lbEgal.setHorizontalAlignment(SwingConstants.CENTER);
        lbEgal.setName("lbEgal");
        sNPanelTitre1.add(lbEgal, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- CAKSC ----
        CAKSC.setHorizontalAlignment(SwingConstants.RIGHT);
        CAKSC.setFont(CAKSC.getFont().deriveFont(CAKSC.getFont().getSize() + 2f));
        CAKSC.setComponentPopupMenu(null);
        CAKSC.setMinimumSize(new Dimension(110, 30));
        CAKSC.setPreferredSize(new Dimension(110, 30));
        CAKSC.setName("CAKSC");
        sNPanelTitre1.add(CAKSC, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbUniteStock ----
        lbUniteStock.setText("@A1UNS@");
        lbUniteStock.setComponentPopupMenu(null);
        lbUniteStock.setName("lbUniteStock");
        sNPanelTitre1.add(lbUniteStock, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbEgal3 ----
        lbEgal3.setText("=");
        lbEgal3.setMaximumSize(new Dimension(40, 30));
        lbEgal3.setMinimumSize(new Dimension(40, 30));
        lbEgal3.setPreferredSize(new Dimension(40, 30));
        lbEgal3.setHorizontalAlignment(SwingConstants.CENTER);
        lbEgal3.setName("lbEgal3");
        sNPanelTitre1.add(lbEgal3, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- CAKAC ----
        CAKAC.setHorizontalAlignment(SwingConstants.RIGHT);
        CAKAC.setFont(CAKAC.getFont().deriveFont(CAKAC.getFont().getSize() + 2f));
        CAKAC.setComponentPopupMenu(null);
        CAKAC.setMinimumSize(new Dimension(110, 30));
        CAKAC.setPreferredSize(new Dimension(110, 30));
        CAKAC.setName("CAKAC");
        sNPanelTitre1.add(CAKAC, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbUniteAchat3 ----
        lbUniteAchat3.setText("@WCAUNA@");
        lbUniteAchat3.setComponentPopupMenu(null);
        lbUniteAchat3.setName("lbUniteAchat3");
        sNPanelTitre1.add(lbUniteAchat3, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(sNPanelTitre1,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.PAGE_END);
    
    // ---- buttonGroup1 ----
    buttonGroup1.add(rbMontantPort);
    buttonGroup1.add(rbCoeffPort);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlBandeau;
  private SNBandeauTitre pbPresentation;
  private SNPanelContenu pnlPrincipal;
  private SNPanel sNPanel1;
  private SNLabelChamp lbArticle;
  private SNArticle snArticle;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNPanelTitre pnlNegociationAchat;
  private SNLabelChamp lbDateApplicationAchat;
  private SNPanel sNPanel3;
  private XRiCalendrier NVDAPX;
  private JLabel lbDateApplicationAchat2;
  private XRiCalendrier NVDAFX;
  private SNLabelChamp lbPrixAchatBrut;
  private SNPanel sNPanel2;
  private XRiTextField NVPRAX;
  private SNLabelChamp sNLabelChamp2;
  private SNLabelUnite lbUniteAchat;
  private SNLabelChamp lbRemisesFournisseur;
  private SNPanel pnlRemises;
  private XRiTextField NVREX1;
  private SNLabelUnite lbPourcentage1;
  private XRiTextField NVREX2;
  private SNLabelUnite lbPourcentage2;
  private XRiTextField NVREX3;
  private SNLabelUnite lbPourcentage3;
  private XRiTextField NVREX4;
  private SNLabelUnite lbPourcentage4;
  private SNLabelChamp lbTaxesAchat;
  private SNPanel pnlMajoration;
  private XRiTextField NVFK3;
  private SNLabelUnite lbPourcentage6;
  private SNLabelChamp lbTaxesAchat2;
  private XRiTextField NVFV3;
  private JRadioButton rbMontantPort;
  private SNPanel pnlMontantPort;
  private XRiTextField NVFV1;
  private JLabel lbSymboleMultiplication;
  private XRiTextField NVFK1;
  private SNLabelChamp sNLabelChamp3;
  private JRadioButton rbCoeffPort;
  private XRiTextField NVFP1;
  private SNLabelUnite lbPourcentage5;
  private SNPanel pnlPourcentagePort;
  private SNLabelChamp lbPRVAchat;
  private SNPanel pnlPRS;
  private XRiTextField WPRSA;
  private SNLabelChamp sNLabelChamp1;
  private SNLabelUnite lbUniteAchat2;
  private SNPanelTitre sNPanelTitre1;
  private SNLabelTitre lbTitreUCA;
  private SNLabelTitre lbTitreUCA2;
  private SNLabelTitre lbTitreUCA3;
  private SNLabelUnite ULBUNC;
  private SNLabelChamp lbEgal;
  private XRiTextField CAKSC;
  private SNLabelUnite lbUniteStock;
  private SNLabelChamp lbEgal3;
  private XRiTextField CAKAC;
  private SNLabelUnite lbUniteAchat3;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
