
package ri.serien.libecranrpg.vgvx.VGVX05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.stock.ListeStock;
import ri.serien.libcommun.gescom.commun.stock.Stock;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snphotoarticle.SNPhotoArticle;
import ri.serien.libswing.composant.metier.stock.detailstock.DialogueDetailStock;
import ri.serien.libswing.composant.metier.stock.detailstock.ModeleDetailStock;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composant.primitif.saisie.SNNombreDecimal;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVX05FM_B1 extends SNPanelEcranRPG implements ioFrame {
  private String[] A1IN3_Value = { "", "A", "V", "F", };
  private String[] A1NPU_Value = { "", "1", "2", "4", "3", "5", "6", };
  private String[] A1IN4_Value = { "", "A", "E", "I", "M", "R", "V", "T", "P" };
  private String[] incrustation_Value = { "", "*", "Q", "^", "X", };
  private OPTION_PANEL optpanel = null;
  private Icon bloc_couleur = null;
  private Icon bloc_neutre = null;
  private boolean isLibelleLong = false;
  private boolean isArticleNomenclature = false;
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private IdArticle idArticle = null;
  private UtilisateurGescom utilisateurGescom = null;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public VGVX05FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    initDiverses();
    A1NPU.setValeurs(A1NPU_Value, null);
    A1IN4.setValeurs(A1IN4_Value, null);
    A1IN3.setValeurs(A1IN3_Value, null);
    A1STK.setValeursSelection("1", "0");
    A1DSG4.setValeursSelection("I", " ");
    A1DSG3.setValeursSelection("I", " ");
    A1DSG2.setValeursSelection("I", " ");
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
    labelNav.setIcon(lexique.chargerImage("images/palette.png", true));
    bloc_couleur = lexique.chargerImage("images/bloc_notes.png", true);
    bloc_neutre = lexique.chargerImage("images/bloc_notes_r.png", true);
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_46_OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCOD@")).trim());
    INDART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDART@")).trim());
    labelHisto.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Historique modifications @LIBPG@")).trim());
    riMenu_bt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    X13STX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@X13STX@")).trim());
    A1KSVX.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBKSV@")).trim());
    A1UNV2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNV@")).trim());
    X13DVX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@X13DVX@")).trim());
    lbTarifDateApplique.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATDAPX@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALB1@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALIB@")).trim());
    label11.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("Référence tarif : @A1RTA@")).trim());
    CAUNA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNA@")).trim());
    A1COF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1COF@")).trim());
    A1FRS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1FRS@")).trim());
    CAPRAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAPRAX@")).trim());
    CADEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADEL@")).trim());
    riZoneSortie4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOMR@")).trim());
    CADEV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADEV@")).trim());
    SVCR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SVCR1@")).trim());
    SVCRMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SVCRMR@")).trim());
    SVQTMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SVQTMR@")).trim());
    SVQT1R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SVQT1R@")).trim());
    WPMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMP@")).trim());
    WPSTD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPSTD@")).trim());
    OBJ_226_OBJ_226.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TTVAX@")).trim());
    A1UNS2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    OBJ_231_OBJ_231.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NSLIBR@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WAIGUL@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GRLIBR@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIB@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SFLIB@")).trim());
    TMC1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TMC1@")).trim());
    TMC2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TMC2@")).trim());
    OBJ_97_OBJ_97.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP1@")).trim());
    OBJ_97_OBJ_98.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    setDiverses();
    
    lexique.addVariableGlobale("CODE_ARTICLE_COURS", lexique.HostFieldGetData("INDART"));
    
    gererLesErreurs("19");
    
    labelHisto.setVisible(lexique.isTrue("75"));
    
    // Les panels
    STOCK.setRightDecoration(bt_stock);
    TARIF.setRightDecoration(bt_tarif);
    CNA.setRightDecoration(bt_CNA);
    STAT.setRightDecoration(bt_stat);
    CHIFFR.setRightDecoration(bt_chiffrage);
    DIVERS.setRightDecoration(bt_divers);
    boolean v_stock = lexique.isTrue("N34");
    boolean v_stats = lexique.isTrue("N36");
    boolean v_chiffr = lexique.isTrue("26");
    boolean is_interro = lexique.isTrue("53");
    isArticleNomenclature = lexique.isTrue("N33");
    
    lbArticlesLiesPresents.setVisible(!lexique.HostFieldGetData("WARL").trim().isEmpty());
    
    A1SPE.setVisible(!lexique.HostFieldGetData("A1SPE").trim().isEmpty());
    
    en_stock.setVisible(v_stock);
    non_stock.setVisible(!v_stock);
    voir_stats.setVisible(v_stats);
    pas_stats.setVisible(!v_stats);
    labelNavBt4.setVisible(v_stats);
    
    navig_valid.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    
    label9.setVisible(lexique.HostFieldGetData("A1ABC").trim().equalsIgnoreCase("R"));
    label12.setVisible(lexique.HostFieldGetData("A1ABC").trim().equalsIgnoreCase("R"));
    
    OBJ_179_OBJ_179.setVisible(lexique.isTrue("09"));
    CAPRAX.setVisible(lexique.isTrue("09"));
    // CHIFFR.setVisible(v_chiffr); // <-- Pourquoi cela a été conditionné ?
    
    // DESIGNATION 4 zones ou 2 zones
    isLibelleLong = (lexique.HostFieldGetData("PST39").equalsIgnoreCase("4"));
    panel2.setVisible(!isLibelleLong);
    panel5.setVisible(isLibelleLong);
    
    incrustation.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    
    if (lexique.HostFieldGetData("A1LIB").substring(22, 23).equals("*") || lexique.HostFieldGetData("A1LIB").substring(22, 23).equals("Q")
        || lexique.HostFieldGetData("A1LIB").substring(22, 23).equals("^")
        || lexique.HostFieldGetData("A1LIB").substring(22, 23).equals("X")) {
      incrustation.setSelectedIndex(getIndice(lexique.HostFieldGetData("A1LIB").substring(22, 23), incrustation_Value));
    }
    else {
      incrustation.setSelectedIndex(0);
    }
    
    // Les boutons
    bt_stat.setVisible(v_stats);
    
    // Les sélections
    
    // A1DSG2.setSelected(lexique.HostFieldGetData("A1DSG2").equalsIgnoreCase("I"));
    // A1DSG3.setSelected(lexique.HostFieldGetData("A1DSG3").equalsIgnoreCase("I"));
    // A1DSG4.setSelected(lexique.HostFieldGetData("A1DSG4").equalsIgnoreCase("I"));
    // A1SPE.setSelected(lexique.HostFieldGetData("A1SPE").equalsIgnoreCase("1"));
    // A1STK.setSelected(lexique.HostFieldGetData("A1STK").equalsIgnoreCase("1"));
    
    // A1IN3.setSelectedIndex(getIndice("A1IN3", A1IN3_Value));
    // A1NPU.setSelectedIndex(getIndice("A1NPU", A1NPU_Value));
    // A1IN4.setSelectedIndex(getIndice("A1IN4", A1IN4_Value));
    
    // Les composants
    titSub.setVisible(!lexique.HostFieldGetData("WAIGUL").trim().isEmpty());
    label1.setVisible(titSub.isVisible());
    
    btnChangeDesignation.setVisible(lexique.isTrue("82"));
    
    A1DSG2.setEnabled(true);
    A1DSG3.setEnabled(true);
    A1DSG4.setEnabled(true);
    
    riSousMenu8.setVisible(is_interro);
    
    riSousMenu28.setEnabled(isArticleNomenclature);
    riSousMenu29.setEnabled(isArticleNomenclature);
    riSousMenu30.setEnabled(isArticleNomenclature);
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(riMenu_bt2);
    
    // On n'affiche pas les zones pour dire combien d'unités de ventes par unités de stock si on a la même unité
    OBJ_143_OBJ_144.setVisible(!lexique.HostFieldGetData("LIBKSV").trim().isEmpty());
    A1KSVX.setVisible(!lexique.HostFieldGetData("LIBKSV").trim().isEmpty());
    A1UNV2.setVisible(!lexique.HostFieldGetData("LIBKSV").trim().isEmpty());
    
    // Tarifs
    lbTarifPar.setVisible(U1P11.isVisible() || TU1P11.isVisible());
    boolean atdapx = lexique.isTrue("(22) AND (41) AND (N43)");
    lbTarifApplique.setVisible(atdapx);
    lbTarifDateApplique.setVisible(atdapx);
    lbTarifAppliqueParentheseFermante.setVisible(atdapx);
    
    // Chiffrage
    WPSTD.setVisible(v_chiffr);
    lbPRV.setVisible(lexique.isPresent("A1PRV"));
    lbA1UNS2.setVisible(lexique.isPresent("A1PRV"));
    A1UNS2.setVisible(lexique.isPresent("A1PRV"));
    COEMAR.setVisible(lexique.isPresent("COEMAR"));
    lbCOEMAR.setVisible(COEMAR.isVisible());
    lbDeprecie.setVisible(!lexique.HostFieldGetData("DEPREC").trim().isEmpty());
    lbWPSTD.setVisible(v_chiffr);
    
    WPMP.setVisible(v_chiffr);
    lbPMP.setVisible(v_chiffr);
    // Divers
    label7.setVisible(lexique.isPresent("A1TNS"));
    OBJ_231_OBJ_231.setVisible(lexique.isPresent("A1TNS"));
    
    // Achats
    CADEL.setText(lexique.HostFieldGetData("CADELX"));
    
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      label11.setComponentPopupMenu(BTD2);
    }
    else {
      label11.setComponentPopupMenu(null);
    }
    
    riSousMenu21.setEnabled(is_interro);
    
    changeTarif.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    
    etat_article.setText(A1NPU.getSelectedItem().toString());
    switch (A1NPU.getSelectedIndex()) {
      case 1:
        etat_article.setForeground(Color.red);
        etat_article.setFont(new Font(INDART.getFont().getFontName(), Font.BOLD, INDART.getFont().getSize() + 2));
        break;
      case 2:
        etat_article.setForeground(Color.red);
        etat_article.setFont(new Font(INDART.getFont().getFontName(), Font.BOLD, INDART.getFont().getSize() + 2));
        break;
      case 6:
        etat_article.setForeground(Color.red);
        etat_article.setFont(new Font(INDART.getFont().getFontName(), Font.BOLD, INDART.getFont().getSize() + 2));
        break;
      default:
        etat_article.setForeground(Color.black);
        etat_article.setFont(new Font(INDART.getFont().getFontName(), Font.BOLD, INDART.getFont().getSize()));
    }
    etat_article.setVisible(A1NPU.getSelectedIndex() > 0);
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    button1.setVisible(lexique.isTrue("N76"));
    button1.setIcon(bloc_neutre);
    
    // STOCKS
    
    // Construire l'identifiant de l'établissement
    idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB"));
    
    // Récupérer le magasin par défaut de l'utilisateur
    UtilisateurGescom utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getSession().getIdSession(),
        ManagerSessionClient.getInstance().getProfil(), ManagerSessionClient.getInstance().getCurlib(), idEtablissement);
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    idMagasin = utilisateurGescom.getIdMagasin();
    
    // Construire l'identifiant de l'article
    idArticle = IdArticle.getInstance(idEtablissement, lexique.HostFieldGetData("INDART"));
    
    // Charger le stock de l'article
    Stock stock = ListeStock.charger(getIdSession(), idEtablissement, idMagasin, idArticle);
    
    // Mettre à jour les champs stock
    if (stock != null) {
      tfStockPhysique.setText(Constantes.formater(stock.getStockPhysique(), false));
      tfStockDisponible.setText(Constantes.formater(stock.getStockDisponible(), false));
    }
    else {
      tfStockPhysique.setText("0");
      tfStockDisponible.setText("0");
    }
    tfStockPhysique.setEnabled(false);
    tfStockDisponible.setEnabled(false);
    
    // Affichage de l'image
    snPhotoArticle.chargerImageArticle(
        lexique.HostFieldGetData("RACINE").trim() + lexique.HostFieldGetData("CHEM1").trim() + lexique.HostFieldGetData("CHEM2").trim(),
        INDART.getText(), lexique.isTrue("N51"), is_interro);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    if (incrustation.getSelectedIndex() > 0) {
      lexique.HostFieldPutData("A1LIB", 0, lexique.HostFieldGetData("A1LIB").substring(0, 21)
          + incrustation_Value[incrustation.getSelectedIndex()] + lexique.HostFieldGetData("A1LIB").substring(23, 30));
    }
    
    if (optpanel != null) {
      optpanel.tuer();
    }
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes auto-génrées par JFormdesigner
  // ---------------------------------------------------------------------------------------------------------------------------------------
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_41_OBJ_41 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_46_OBJ_46 = new JLabel();
    INDART = new RiZoneSortie();
    labelHisto = new JLabel();
    OBJ_228_OBJ_228 = new JLabel();
    A1IN3 = new XRiComboBox();
    A1SPE = new JLabel();
    lbArticlesLiesPresents = new JLabel();
    p_tete_droite = new JPanel();
    etat_article = new RiZoneSortie();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu22 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu17 = new RiSousMenu();
    riSousMenu_bt17 = new RiSousMenu_bt();
    riSousMenu26 = new RiSousMenu();
    riSousMenu_bt25 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu23 = new RiSousMenu();
    riSousMenu_bt22 = new RiSousMenu_bt();
    riSousMenu24 = new RiSousMenu();
    riSousMenu_bt23 = new RiSousMenu_bt();
    riSousMenu25 = new RiSousMenu();
    riSousMenu_bt24 = new RiSousMenu_bt();
    riSousMenu27 = new RiSousMenu();
    riSousMenu_bt26 = new RiSousMenu_bt();
    riSousMenu28 = new RiSousMenu();
    riSousMenu_bt27 = new RiSousMenu_bt();
    riSousMenu29 = new RiSousMenu();
    riSousMenu_bt28 = new RiSousMenu_bt();
    riSousMenu30 = new RiSousMenu();
    riSousMenu_bt29 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    panelNav = new JPanel();
    labelNavBt1 = new JLabel();
    labelNavBt2 = new JLabel();
    labelNavBt3 = new JLabel();
    labelNavBt4 = new JLabel();
    labelNavBt5 = new JLabel();
    labelNavBt6 = new JLabel();
    labelNav = new JLabel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    layeredPane1 = new JLayeredPane();
    STOCK = new JXTitledPanel();
    non_stock = new JLabel();
    en_stock = new JPanel();
    lbStockPhysique = new JLabel();
    A1UNS = new XRiTextField();
    OBJ_143_OBJ_144 = new JLabel();
    A1KSVX = new XRiTextField();
    A1UNV2 = new RiZoneSortie();
    A1STK = new XRiCheckBox();
    lbStockDisponible = new JLabel();
    tfStockPhysique = new SNNombreDecimal();
    tfStockDisponible = new SNNombreDecimal();
    btnDetailStock = new SNBoutonDetail();
    TARIF = new JXTitledPanel();
    A1UNV = new XRiTextField();
    U1P11 = new XRiTextField();
    TU1P11 = new XRiTextField();
    lbTarifPar = new JLabel();
    lbTarifDateApplique = new JLabel();
    lbTarifApplique = new JLabel();
    U1P21 = new XRiTextField();
    TU1P21 = new XRiTextField();
    lbTarifAppliqueParentheseFermante = new JLabel();
    label8 = new JLabel();
    label11 = new RiZoneSortie();
    changeTarif = new SNBoutonDetail();
    A1RTA = new XRiTextField();
    CNA = new JXTitledPanel();
    OBJ_183_OBJ_183 = new JLabel();
    OBJ_170_OBJ_170 = new JLabel();
    CAUNA = new RiZoneSortie();
    A1COF = new RiZoneSortie();
    A1FRS = new RiZoneSortie();
    CAPRAX = new RiZoneSortie();
    OBJ_179_OBJ_179 = new JLabel();
    OBJ_168_OBJ_168 = new JLabel();
    CADEL = new RiZoneSortie();
    riZoneSortie4 = new RiZoneSortie();
    CADEV = new JLabel();
    STAT = new JXTitledPanel();
    voir_stats = new JPanel();
    SVCR1 = new RiZoneSortie();
    SVCRMR = new RiZoneSortie();
    OBJ_193_OBJ_193 = new JLabel();
    SVQTMR = new RiZoneSortie();
    OBJ_198_OBJ_198 = new JLabel();
    SVQT1R = new RiZoneSortie();
    pas_stats = new JLabel();
    CHIFFR = new JXTitledPanel();
    lbCOEMAR = new JLabel();
    A1PRV = new XRiTextField();
    COEMAR = new XRiTextField();
    WPMP = new RiZoneSortie();
    A1TVA = new XRiTextField();
    WPSTD = new RiZoneSortie();
    lbPRV = new JLabel();
    lbWPSTD = new JLabel();
    lbPMP = new JLabel();
    lbDeprecie = new JLabel();
    OBJ_226_OBJ_226 = new RiZoneSortie();
    OBJ_225_OBJ_225 = new JLabel();
    A1UNS2 = new RiZoneSortie();
    lbA1UNS2 = new JLabel();
    DIVERS = new JXTitledPanel();
    OBJ_227_OBJ_227 = new JLabel();
    label7 = new JLabel();
    A1TNS = new XRiTextField();
    OBJ_231_OBJ_231 = new RiZoneSortie();
    A1NPU = new XRiComboBox();
    panel4 = new JPanel();
    label6 = new JLabel();
    A1ASB = new XRiTextField();
    A1IN4 = new XRiComboBox();
    titSub = new JLabel();
    label1 = new RiZoneSortie();
    OBJ_99 = new SNBoutonDetail();
    A1GCDX = new XRiTextField();
    label2 = new JLabel();
    separator1 = new JSeparator();
    snPhotoArticle = new SNPhotoArticle();
    label10 = new JLabel();
    A1OBS = new XRiTextField();
    button1 = new JButton();
    lbTypeProduit = new JLabel();
    A1IN20 = new XRiTextField();
    panel3 = new JPanel();
    A1GRP = new XRiTextField();
    A1FAM = new XRiTextField();
    WSFA = new XRiTextField();
    A1SFA = new XRiTextField();
    OBJ_130_OBJ_130 = new JLabel();
    OBJ_112_OBJ_112 = new JLabel();
    OBJ_126_OBJ_126 = new JLabel();
    riZoneSortie1 = new RiZoneSortie();
    riZoneSortie2 = new RiZoneSortie();
    riZoneSortie3 = new RiZoneSortie();
    TMC1 = new JLabel();
    TMC2 = new JLabel();
    A1CL1 = new XRiTextField();
    A1CL2 = new XRiTextField();
    panel2 = new JPanel();
    incrustation = new JComboBox<>();
    OBJ_73_OBJ_73 = new JLabel();
    btnChangeDesignation = new SNBoutonDetail();
    OBJ_97_OBJ_97 = new JLabel();
    A1LIB = new XRiTextField();
    A1LB1 = new XRiTextField();
    A1DSG3 = new XRiCheckBox();
    A1DSG2 = new XRiCheckBox();
    A1LB3 = new XRiTextField();
    A1DSG4 = new XRiCheckBox();
    label9 = new JLabel();
    A1LB2 = new XRiTextField();
    label13 = new JLabel();
    panel5 = new JPanel();
    OBJ_73_OBJ_74 = new JLabel();
    btnChangeDesignation2 = new SNBoutonDetail();
    OBJ_97_OBJ_98 = new JLabel();
    label12 = new JLabel();
    A1LB12 = new XRiTextField();
    A1LB34 = new XRiTextField();
    bt_stock = new SNBoutonDetail();
    bt_tarif = new SNBoutonDetail();
    bt_CNA = new SNBoutonDetail();
    bt_stat = new SNBoutonDetail();
    bt_chiffrage = new SNBoutonDetail();
    bt_divers = new SNBoutonDetail();
    BTD = new JPopupMenu();
    OBJ_22 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    BTD2 = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    X13STX = new RiZoneSortie();
    X13DVX = new RiZoneSortie();
    
    // ======== this ========
    setMinimumSize(new Dimension(1080, 700));
    setPreferredSize(new Dimension(1080, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Fiche article");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(1000, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("Etablissement");
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
          
          // ---- INDETB ----
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");
          
          // ---- OBJ_46_OBJ_46 ----
          OBJ_46_OBJ_46.setText("@LIBCOD@");
          OBJ_46_OBJ_46.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");
          
          // ---- INDART ----
          INDART.setText("@INDART@");
          INDART.setOpaque(false);
          INDART.setName("INDART");
          
          // ---- labelHisto ----
          labelHisto.setText("Historique modifications @LIBPG@");
          labelHisto.setForeground(new Color(44, 74, 116));
          labelHisto.setFont(labelHisto.getFont().deriveFont(labelHisto.getFont().getStyle() | Font.BOLD));
          labelHisto.setName("labelHisto");
          
          // ---- OBJ_228_OBJ_228 ----
          OBJ_228_OBJ_228.setText("Type");
          OBJ_228_OBJ_228.setName("OBJ_228_OBJ_228");
          
          // ---- A1IN3 ----
          A1IN3.setModel(new DefaultComboBoxModel<>(
              new String[] { "Article n\u00e9goce", "Achet\u00e9 non vendu", "Vendu non achet\u00e9", "Produit fabriqu\u00e9" }));
          A1IN3.setComponentPopupMenu(BTD);
          A1IN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1IN3.setFont(A1IN3.getFont().deriveFont(A1IN3.getFont().getStyle() & ~Font.BOLD));
          A1IN3.setName("A1IN3");
          
          // ---- A1SPE ----
          A1SPE.setText("Article sp\u00e9cial");
          A1SPE.setHorizontalAlignment(SwingConstants.CENTER);
          A1SPE.setFont(A1SPE.getFont().deriveFont(A1SPE.getFont().getStyle() | Font.BOLD));
          A1SPE.setForeground(Color.red);
          A1SPE.setName("A1SPE");
          
          // ---- lbArticlesLiesPresents ----
          lbArticlesLiesPresents.setText("Poss\u00e8de des articles li\u00e9s");
          lbArticlesLiesPresents.setHorizontalAlignment(SwingConstants.CENTER);
          lbArticlesLiesPresents
              .setFont(lbArticlesLiesPresents.getFont().deriveFont(lbArticlesLiesPresents.getFont().getStyle() | Font.BOLD));
          lbArticlesLiesPresents.setForeground(Color.red);
          lbArticlesLiesPresents.setName("lbArticlesLiesPresents");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(INDART, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(40, 40, 40).addComponent(A1IN3,
                          GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
                      .addComponent(OBJ_228_OBJ_228, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(lbArticlesLiesPresents)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(A1SPE, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(labelHisto, GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(3, 3, 3).addComponent(OBJ_41_OBJ_41,
                          GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1)
                          .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(INDART,
                          GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(A1IN3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(3, 3, 3).addComponent(OBJ_228_OBJ_228,
                          GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2)
                          .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                              .addComponent(lbArticlesLiesPresents, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(A1SPE, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(labelHisto,
                          GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(130, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new GridBagLayout());
          ((GridBagLayout) p_tete_droite.getLayout()).columnWidths = new int[] { 181, 0 };
          ((GridBagLayout) p_tete_droite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) p_tete_droite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) p_tete_droite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- etat_article ----
          etat_article.setText("etat article");
          etat_article.setFont(etat_article.getFont().deriveFont(etat_article.getFont().getStyle() | Font.BOLD));
          etat_article.setPreferredSize(new Dimension(170, 29));
          etat_article.setHorizontalAlignment(SwingConstants.TRAILING);
          etat_article.setMinimumSize(new Dimension(180, 29));
          etat_article.setMaximumSize(new Dimension(280, 22));
          etat_article.setName("etat_article");
          p_tete_droite.add(etat_article, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.VERTICAL, new Insets(3, 3, 4, 3), 0, 0));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setBackground(new Color(214, 217, 223));
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 620));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setMinimumSize(new Dimension(19, 620));
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 620));
            menus_haut.setPreferredSize(new Dimension(160, 620));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt1 ----
              riMenu_bt1.setText("@V01F@");
              riMenu_bt1.setPreferredSize(new Dimension(167, 50));
              riMenu_bt1.setMinimumSize(new Dimension(167, 50));
              riMenu_bt1.setMaximumSize(new Dimension(170, 50));
              riMenu_bt1.setFont(riMenu_bt1.getFont().deriveFont(riMenu_bt1.getFont().getSize() + 2f));
              riMenu_bt1.setName("riMenu_bt1");
              riMenu_V01F.add(riMenu_bt1);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setToolTipText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Bloc-notes");
              riSousMenu_bt15
                  .setToolTipText("Bloc-notes : page d'informations li\u00e9es \u00e0 l'article et articles li\u00e9s (Shift + F10)");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(e -> riSousMenu_bt15ActionPerformed());
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("M\u00e9mo");
              riSousMenu_bt14.setToolTipText("M\u00e9mo : saisie d'\u00e9v\u00e9nements g\u00e9r\u00e9s par leurs types (Shift + F7)");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(e -> riSousMenu_bt14ActionPerformed());
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu22 ========
            {
              riSousMenu22.setName("riSousMenu22");
              
              // ---- riSousMenu_bt20 ----
              riSousMenu_bt20.setText("Documents li\u00e9s");
              riSousMenu_bt20.setToolTipText("Documents li\u00e9s : dossiers des documents associ\u00e9s \u00e0 l'article (F11)");
              riSousMenu_bt20.setName("riSousMenu_bt20");
              riSousMenu_bt20.addActionListener(e -> riSousMenu_bt20ActionPerformed());
              riSousMenu22.add(riSousMenu_bt20);
            }
            menus_haut.add(riSousMenu22);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Extension fiche");
              riSousMenu_bt6.setToolTipText("Extensions : zones de texte param\u00e9tr\u00e9es, libres en saisie");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed());
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu17 ========
            {
              riSousMenu17.setName("riSousMenu17");
              
              // ---- riSousMenu_bt17 ----
              riSousMenu_bt17.setText("Lien internet");
              riSousMenu_bt17.setToolTipText("Liens internet : pages internet en lien avec cet article");
              riSousMenu_bt17.setName("riSousMenu_bt17");
              riSousMenu_bt17.addActionListener(e -> riSousMenu_bt17ActionPerformed(e));
              riSousMenu17.add(riSousMenu_bt17);
            }
            menus_haut.add(riSousMenu17);
            
            // ======== riSousMenu26 ========
            {
              riSousMenu26.setName("riSousMenu26");
              
              // ---- riSousMenu_bt25 ----
              riSousMenu_bt25.setText("Fiches techniques");
              riSousMenu_bt25.setToolTipText("Liens internet : pages internet en lien avec cet article");
              riSousMenu_bt25.setName("riSousMenu_bt25");
              riSousMenu_bt25.addActionListener(e -> riSousMenu_bt25ActionPerformed(e));
              riSousMenu26.add(riSousMenu_bt25);
            }
            menus_haut.add(riSousMenu26);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Compl\u00e9ments");
              riMenu_bt3.setToolTipText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");
              
              // ---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("R\u00e9cap. unit\u00e9s utilis\u00e9es");
              riSousMenu_bt12.setToolTipText(
                  "R\u00e9capitulatif des unit\u00e9s utilis\u00e9es : tableau des unit\u00e9s vente/achat/stock et relations (F6)");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(e -> riSousMenu_bt12ActionPerformed(e));
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Affichage personnalis\u00e9");
              riSousMenu_bt7.setToolTipText(
                  "Activation/d\u00e9sactivation de l'affichage personnalis\u00e9 : fiche article param\u00e9tr\u00e9e (Shift F12)");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(e -> riSousMenu_bt7ActionPerformed());
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Fiche r\u00e9aprovisionnement");
              riSousMenu_bt8
                  .setToolTipText("Fiche r\u00e9approvisionnement : informations de gestion li\u00e9es au r\u00e9approvisionnement");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(e -> riSousMenu_bt8ActionPerformed());
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Gestion des libell\u00e9s");
              riSousMenu_bt9
                  .setToolTipText("Gestion des libell\u00e9s : gestion des d\u00e9signations compl\u00e9mentaires par code langue");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(e -> riSousMenu_bt9ActionPerformed());
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");
              
              // ---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Attendu/command\u00e9");
              riSousMenu_bt13.setToolTipText(
                  "Attendu / command\u00e9 : commandes clients et fournisseurs tri\u00e9es par date de mouvement pr\u00e9vu (Shift F9)");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(e -> riSousMenu_bt13ActionPerformed(e));
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);
            
            // ======== riSousMenu23 ========
            {
              riSousMenu23.setName("riSousMenu23");
              
              // ---- riSousMenu_bt22 ----
              riSousMenu_bt22.setText("Visibilit\u00e9 sur le web");
              riSousMenu_bt22.setToolTipText("Visibilit\u00e9 sur le web (Shift F5)");
              riSousMenu_bt22.setName("riSousMenu_bt22");
              riSousMenu_bt22.addActionListener(e -> riSousMenu_bt22ActionPerformed(e));
              riSousMenu23.add(riSousMenu_bt22);
            }
            menus_haut.add(riSousMenu23);
            
            // ======== riSousMenu24 ========
            {
              riSousMenu24.setName("riSousMenu24");
              
              // ---- riSousMenu_bt23 ----
              riSousMenu_bt23.setText("Art. li\u00e9s automatiquement");
              riSousMenu_bt23.setToolTipText("Articles li\u00e9s automatiquement");
              riSousMenu_bt23.setName("riSousMenu_bt23");
              riSousMenu_bt23.addActionListener(e -> riSousMenu_bt23ActionPerformed(e));
              riSousMenu24.add(riSousMenu_bt23);
            }
            menus_haut.add(riSousMenu24);
            
            // ======== riSousMenu25 ========
            {
              riSousMenu25.setName("riSousMenu25");
              
              // ---- riSousMenu_bt24 ----
              riSousMenu_bt24.setText("Articles li\u00e9s propos\u00e9s");
              riSousMenu_bt24.setToolTipText("Articles li\u00e9s propos\u00e9s");
              riSousMenu_bt24.setName("riSousMenu_bt24");
              riSousMenu_bt24.addActionListener(e -> riSousMenu_bt24ActionPerformed(e));
              riSousMenu25.add(riSousMenu_bt24);
            }
            menus_haut.add(riSousMenu25);
            
            // ======== riSousMenu27 ========
            {
              riSousMenu27.setName("riSousMenu27");
              
              // ---- riSousMenu_bt26 ----
              riSousMenu_bt26.setText("Autres articles li\u00e9s");
              riSousMenu_bt26.setToolTipText("Articles li\u00e9s propos\u00e9s");
              riSousMenu_bt26.setName("riSousMenu_bt26");
              riSousMenu_bt26.addActionListener(e -> riSousMenu_bt26ActionPerformed(e));
              riSousMenu27.add(riSousMenu_bt26);
            }
            menus_haut.add(riSousMenu27);
            
            // ======== riSousMenu28 ========
            {
              riSousMenu28.setName("riSousMenu28");
              
              // ---- riSousMenu_bt27 ----
              riSousMenu_bt27.setText("Nomenclature");
              riSousMenu_bt27.setToolTipText("Nomenclature");
              riSousMenu_bt27.setName("riSousMenu_bt27");
              riSousMenu_bt27.addActionListener(e -> riSousMenu_bt27ActionPerformed(e));
              riSousMenu28.add(riSousMenu_bt27);
            }
            menus_haut.add(riSousMenu28);
            
            // ======== riSousMenu29 ========
            {
              riSousMenu29.setName("riSousMenu29");
              
              // ---- riSousMenu_bt28 ----
              riSousMenu_bt28.setText("Ordres de fabrication");
              riSousMenu_bt28.setToolTipText("Ordres de fabrication");
              riSousMenu_bt28.setName("riSousMenu_bt28");
              riSousMenu_bt28.addActionListener(e -> riSousMenu_bt28ActionPerformed(e));
              riSousMenu29.add(riSousMenu_bt28);
            }
            menus_haut.add(riSousMenu29);
            
            // ======== riSousMenu30 ========
            {
              riSousMenu30.setName("riSousMenu30");
              
              // ---- riSousMenu_bt29 ----
              riSousMenu_bt29.setText("Cas d'emploi");
              riSousMenu_bt29.setToolTipText("Cas d'emploi");
              riSousMenu_bt29.setName("riSousMenu_bt29");
              riSousMenu_bt29.addActionListener(e -> riSousMenu_bt29ActionPerformed(e));
              riSousMenu30.add(riSousMenu_bt29);
            }
            menus_haut.add(riSousMenu30);
            
            // ======== riSousMenu21 ========
            {
              riSousMenu21.setName("riSousMenu21");
              
              // ---- riSousMenu_bt21 ----
              riSousMenu_bt21.setText("Options avanc\u00e9es");
              riSousMenu_bt21.setToolTipText("Options avanc\u00e9es");
              riSousMenu_bt21.setName("riSousMenu_bt21");
              riSousMenu_bt21.addActionListener(e -> riSousMenu_bt21ActionPerformed());
              riSousMenu21.add(riSousMenu_bt21);
            }
            menus_haut.add(riSousMenu21);
            
            // ======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");
              
              // ---- riMenu_bt4 ----
              riMenu_bt4.setText("Navigation");
              riMenu_bt4.setToolTipText("Navigation graphique");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);
            
            // ======== riSousMenu18 ========
            {
              riSousMenu18.setPreferredSize(new Dimension(170, 260));
              riSousMenu18.setMinimumSize(new Dimension(170, 260));
              riSousMenu18.setMaximumSize(new Dimension(170, 260));
              riSousMenu18.setMargin(new Insets(0, -2, 0, 0));
              riSousMenu18.setOpaque(true);
              riSousMenu18.setName("riSousMenu18");
              
              // ======== panelNav ========
              {
                panelNav.setOpaque(false);
                panelNav.setName("panelNav");
                panelNav.setLayout(null);
                
                // ---- labelNavBt1 ----
                labelNavBt1.setToolTipText("conditions d'achat");
                labelNavBt1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                labelNavBt1.setName("labelNavBt1");
                labelNavBt1.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    labelNavBt1MouseClicked();
                  }
                  
                  @Override
                  public void mouseEntered(MouseEvent e) {
                    labelNavBt1MouseEntered(e);
                  }
                  
                  @Override
                  public void mouseExited(MouseEvent e) {
                    labelNavBt1MouseExited(e);
                  }
                });
                panelNav.add(labelNavBt1);
                labelNavBt1.setBounds(5, 60, 60, 60);
                
                // ---- labelNavBt2 ----
                labelNavBt2.setToolTipText("tarifs");
                labelNavBt2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                labelNavBt2.setName("labelNavBt2");
                labelNavBt2.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    labelNavBt2MouseClicked();
                  }
                  
                  @Override
                  public void mouseEntered(MouseEvent e) {
                    labelNavBt2MouseEntered(e);
                  }
                  
                  @Override
                  public void mouseExited(MouseEvent e) {
                    labelNavBt2MouseExited(e);
                  }
                });
                panelNav.add(labelNavBt2);
                labelNavBt2.setBounds(70, 75, 60, 60);
                
                // ---- labelNavBt3 ----
                labelNavBt3.setToolTipText("divers");
                labelNavBt3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                labelNavBt3.setName("labelNavBt3");
                labelNavBt3.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    labelNavBt3MouseClicked();
                  }
                  
                  @Override
                  public void mouseEntered(MouseEvent e) {
                    labelNavBt3MouseEntered(e);
                  }
                  
                  @Override
                  public void mouseExited(MouseEvent e) {
                    labelNavBt3MouseExited(e);
                  }
                });
                panelNav.add(labelNavBt3);
                labelNavBt3.setBounds(5, 125, 60, 60);
                
                // ---- labelNavBt4 ----
                labelNavBt4.setToolTipText("statistiques");
                labelNavBt4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                labelNavBt4.setName("labelNavBt4");
                labelNavBt4.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    labelNavBt4MouseClicked();
                  }
                  
                  @Override
                  public void mouseEntered(MouseEvent e) {
                    labelNavBt4MouseEntered(e);
                  }
                  
                  @Override
                  public void mouseExited(MouseEvent e) {
                    labelNavBt4MouseExited(e);
                  }
                });
                panelNav.add(labelNavBt4);
                labelNavBt4.setBounds(70, 130, 60, 60);
                
                // ---- labelNavBt5 ----
                labelNavBt5.setToolTipText("stocks");
                labelNavBt5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                labelNavBt5.setName("labelNavBt5");
                labelNavBt5.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    labelNavBt5MouseClicked();
                  }
                  
                  @Override
                  public void mouseEntered(MouseEvent e) {
                    labelNavBt5MouseEntered(e);
                  }
                  
                  @Override
                  public void mouseExited(MouseEvent e) {
                    labelNavBt5MouseExited(e);
                  }
                });
                panelNav.add(labelNavBt5);
                labelNavBt5.setBounds(45, 195, 60, 60);
                
                // ---- labelNavBt6 ----
                labelNavBt6.setToolTipText("chiffrage");
                labelNavBt6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                labelNavBt6.setName("labelNavBt6");
                labelNavBt6.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    labelNavBt6MouseClicked();
                  }
                  
                  @Override
                  public void mouseEntered(MouseEvent e) {
                    labelNavBt6MouseEntered(e);
                  }
                  
                  @Override
                  public void mouseExited(MouseEvent e) {
                    labelNavBt6MouseExited(e);
                  }
                });
                panelNav.add(labelNavBt6);
                labelNavBt6.setBounds(40, 0, 60, 60);
                
                // ---- labelNav ----
                labelNav.setPreferredSize(new Dimension(160, 250));
                labelNav.setMinimumSize(new Dimension(160, 250));
                labelNav.setMaximumSize(new Dimension(160, 250));
                labelNav.setName("labelNav");
                panelNav.add(labelNav);
                labelNav.setBounds(new Rectangle(new Point(0, 0), labelNav.getPreferredSize()));
                
                { // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < panelNav.getComponentCount(); i++) {
                    Rectangle bounds = panelNav.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panelNav.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panelNav.setMinimumSize(preferredSize);
                  panelNav.setPreferredSize(preferredSize);
                }
              }
              riSousMenu18.add(panelNav);
            }
            menus_haut.add(riSousMenu18);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setBorder(null);
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(880, 620));
          p_contenu.setBorder(null);
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(880, 620));
          p_contenu.setName("p_contenu");
          
          // ======== layeredPane1 ========
          {
            layeredPane1.setBorder(LineBorder.createGrayLineBorder());
            layeredPane1.setPreferredSize(new Dimension(880, 630));
            layeredPane1.setName("layeredPane1");
            
            // ======== STOCK ========
            {
              STOCK.setTitle("Stock");
              STOCK.setBorder(new DropShadowBorder());
              STOCK.setName("STOCK");
              Container STOCKContentContainer = STOCK.getContentContainer();
              STOCKContentContainer.setLayout(null);
              
              // ---- non_stock ----
              non_stock.setText("Article non g\u00e9r\u00e9 en stocks");
              non_stock.setFont(
                  non_stock.getFont().deriveFont(non_stock.getFont().getStyle() | Font.BOLD, non_stock.getFont().getSize() + 3f));
              non_stock.setHorizontalAlignment(SwingConstants.CENTER);
              non_stock.setName("non_stock");
              STOCKContentContainer.add(non_stock);
              non_stock.setBounds(70, 15, 252, 40);
              
              // ======== en_stock ========
              {
                en_stock.setOpaque(false);
                en_stock.setName("en_stock");
                en_stock.setLayout(null);
                
                // ---- lbStockPhysique ----
                lbStockPhysique.setText("Physique");
                lbStockPhysique.setName("lbStockPhysique");
                en_stock.add(lbStockPhysique);
                lbStockPhysique.setBounds(5, 7, 70, 20);
                
                // ---- A1UNS ----
                A1UNS.setComponentPopupMenu(BTD);
                A1UNS.setToolTipText("Unit\u00e9 de stock");
                A1UNS.setName("A1UNS");
                en_stock.add(A1UNS);
                A1UNS.setBounds(170, 3, 34, A1UNS.getPreferredSize().height);
                
                // ---- OBJ_143_OBJ_144 ----
                OBJ_143_OBJ_144.setText("de");
                OBJ_143_OBJ_144.setName("OBJ_143_OBJ_144");
                en_stock.add(OBJ_143_OBJ_144);
                OBJ_143_OBJ_144.setBounds(220, 7, 30, 20);
                
                // ---- A1KSVX ----
                A1KSVX.setComponentPopupMenu(null);
                A1KSVX.setToolTipText("@LIBKSV@");
                A1KSVX.setHorizontalAlignment(SwingConstants.RIGHT);
                A1KSVX.setName("A1KSVX");
                en_stock.add(A1KSVX);
                A1KSVX.setBounds(265, 3, 74, A1KSVX.getPreferredSize().height);
                
                // ---- A1UNV2 ----
                A1UNV2.setComponentPopupMenu(BTD);
                A1UNV2.setText("@A1UNV@");
                A1UNV2.setToolTipText("Unit\u00e9 de vente");
                A1UNV2.setName("A1UNV2");
                en_stock.add(A1UNV2);
                A1UNV2.setBounds(345, 5, 34, A1UNV2.getPreferredSize().height);
                
                // ---- A1STK ----
                A1STK.setText("Non stock\u00e9");
                A1STK.setComponentPopupMenu(BTD);
                A1STK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                A1STK.setToolTipText("Non g\u00e9r\u00e9 en stock");
                A1STK.setName("A1STK");
                en_stock.add(A1STK);
                A1STK.setBounds(290, 40, 90, 20);
                
                // ---- lbStockDisponible ----
                lbStockDisponible.setText("Disponible");
                lbStockDisponible.setName("lbStockDisponible");
                en_stock.add(lbStockDisponible);
                lbStockDisponible.setBounds(5, 40, 70, 20);
                
                // ---- tfStockPhysique ----
                tfStockPhysique.setFont(new Font("sansserif", Font.PLAIN, 12));
                tfStockPhysique.setName("tfStockPhysique");
                en_stock.add(tfStockPhysique);
                tfStockPhysique.setBounds(70, 3, 100, 28);
                
                // ---- tfStockDisponible ----
                tfStockDisponible.setFont(new Font("sansserif", Font.PLAIN, 12));
                tfStockDisponible.setName("tfStockDisponible");
                en_stock.add(tfStockDisponible);
                tfStockDisponible.setBounds(70, 36, 100, 28);
                
                // ---- btnDetailStock ----
                btnDetailStock.setName("btnDetailStock");
                btnDetailStock.addActionListener(e -> btnDetailStockActionPerformed(e));
                en_stock.add(btnDetailStock);
                btnDetailStock.setBounds(172, 36, 30, 28);
                
                { // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < en_stock.getComponentCount(); i++) {
                    Rectangle bounds = en_stock.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = en_stock.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  en_stock.setMinimumSize(preferredSize);
                  en_stock.setPreferredSize(preferredSize);
                }
              }
              STOCKContentContainer.add(en_stock);
              en_stock.setBounds(0, 0, 385, 80);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < STOCKContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = STOCKContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = STOCKContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                STOCKContentContainer.setMinimumSize(preferredSize);
                STOCKContentContainer.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(STOCK, JLayeredPane.DEFAULT_LAYER);
            STOCK.setBounds(20, 285, 390, 99);
            
            // ======== TARIF ========
            {
              TARIF.setTitle("Tarif");
              TARIF.setBorder(new DropShadowBorder());
              TARIF.setName("TARIF");
              Container TARIFContentContainer = TARIF.getContentContainer();
              TARIFContentContainer.setLayout(null);
              
              // ---- A1UNV ----
              A1UNV.setComponentPopupMenu(BTD);
              A1UNV.setToolTipText("Unit\u00e9 de vente");
              A1UNV.setName("A1UNV");
              TARIFContentContainer.add(A1UNV);
              A1UNV.setBounds(140, 35, 34, A1UNV.getPreferredSize().height);
              
              // ---- U1P11 ----
              U1P11.setName("U1P11");
              TARIFContentContainer.add(U1P11);
              U1P11.setBounds(5, 35, 90, 28);
              
              // ---- TU1P11 ----
              TU1P11.setName("TU1P11");
              TARIFContentContainer.add(TU1P11);
              TU1P11.setBounds(5, 35, 90, TU1P11.getPreferredSize().height);
              
              // ---- lbTarifPar ----
              lbTarifPar.setText("\u20ac   par");
              lbTarifPar.setName("lbTarifPar");
              TARIFContentContainer.add(lbTarifPar);
              lbTarifPar.setBounds(100, 39, 40, 20);
              
              // ---- lbTarifDateApplique ----
              lbTarifDateApplique.setText("@ATDAPX@");
              lbTarifDateApplique.setName("lbTarifDateApplique");
              TARIFContentContainer.add(lbTarifDateApplique);
              lbTarifDateApplique.setBounds(255, 39, 51, 20);
              
              // ---- lbTarifApplique ----
              lbTarifApplique.setText("(appliqu\u00e9 le");
              lbTarifApplique.setName("lbTarifApplique");
              TARIFContentContainer.add(lbTarifApplique);
              lbTarifApplique.setBounds(180, 39, 75, 20);
              
              // ---- U1P21 ----
              U1P21.setHorizontalAlignment(SwingConstants.RIGHT);
              U1P21.setName("U1P21");
              TARIFContentContainer.add(U1P21);
              U1P21.setBounds(330, 35, 90, 28);
              
              // ---- TU1P21 ----
              TU1P21.setHorizontalAlignment(SwingConstants.RIGHT);
              TU1P21.setName("TU1P21");
              TARIFContentContainer.add(TU1P21);
              TU1P21.setBounds(330, 35, 90, TU1P21.getPreferredSize().height);
              
              // ---- lbTarifAppliqueParentheseFermante ----
              lbTarifAppliqueParentheseFermante.setText(")");
              lbTarifAppliqueParentheseFermante.setHorizontalAlignment(SwingConstants.CENTER);
              lbTarifAppliqueParentheseFermante.setName("lbTarifAppliqueParentheseFermante");
              TARIFContentContainer.add(lbTarifAppliqueParentheseFermante);
              lbTarifAppliqueParentheseFermante.setBounds(430, 39, 9, 20);
              
              // ---- label8 ----
              label8.setText("@TALB1@");
              label8.setName("label8");
              TARIFContentContainer.add(label8);
              label8.setBounds(325, 5, 105, 24);
              
              // ---- label11 ----
              label11.setText("@TALIB@");
              label11.setToolTipText("R\u00e9f\u00e9rence tarif : @A1RTA@");
              label11.setComponentPopupMenu(BTD2);
              label11.setName("label11");
              TARIFContentContainer.add(label11);
              label11.setBounds(140, 5, 155, label11.getPreferredSize().height);
              
              // ---- changeTarif ----
              changeTarif.setName("changeTarif");
              changeTarif.addActionListener(e -> changeTarifActionPerformed(e));
              TARIFContentContainer.add(changeTarif);
              changeTarif.setBounds(70, 5, 25, 25);
              
              // ---- A1RTA ----
              A1RTA.setName("A1RTA");
              TARIFContentContainer.add(A1RTA);
              A1RTA.setBounds(5, 3, 64, A1RTA.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < TARIFContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = TARIFContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = TARIFContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                TARIFContentContainer.setMinimumSize(preferredSize);
                TARIFContentContainer.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(TARIF, JLayeredPane.DEFAULT_LAYER);
            TARIF.setBounds(410, 285, 450, 99);
            
            // ======== CNA ========
            {
              CNA.setTitle("Conditions d'achat");
              CNA.setBorder(new DropShadowBorder());
              CNA.setName("CNA");
              Container CNAContentContainer = CNA.getContentContainer();
              CNAContentContainer.setLayout(null);
              
              // ---- OBJ_183_OBJ_183 ----
              OBJ_183_OBJ_183.setText("D\u00e9lai de r\u00e9approvisionnement");
              OBJ_183_OBJ_183.setName("OBJ_183_OBJ_183");
              CNAContentContainer.add(OBJ_183_OBJ_183);
              OBJ_183_OBJ_183.setBounds(325, 40, 175, 20);
              
              // ---- OBJ_170_OBJ_170 ----
              OBJ_170_OBJ_170.setText("Fournisseur");
              OBJ_170_OBJ_170.setName("OBJ_170_OBJ_170");
              CNAContentContainer.add(OBJ_170_OBJ_170);
              OBJ_170_OBJ_170.setBounds(15, 10, 80, 20);
              
              // ---- CAUNA ----
              CAUNA.setToolTipText("Unit\u00e9 d'achat");
              CAUNA.setComponentPopupMenu(null);
              CAUNA.setText("@CAUNA@");
              CAUNA.setName("CAUNA");
              CNAContentContainer.add(CAUNA);
              CAUNA.setBounds(235, 38, 34, CAUNA.getPreferredSize().height);
              
              // ---- A1COF ----
              A1COF.setText("@A1COF@");
              A1COF.setHorizontalAlignment(SwingConstants.RIGHT);
              A1COF.setName("A1COF");
              CNAContentContainer.add(A1COF);
              A1COF.setBounds(110, 8, 20, A1COF.getPreferredSize().height);
              
              // ---- A1FRS ----
              A1FRS.setText("@A1FRS@");
              A1FRS.setHorizontalAlignment(SwingConstants.RIGHT);
              A1FRS.setName("A1FRS");
              CNAContentContainer.add(A1FRS);
              A1FRS.setBounds(140, 8, 60, A1FRS.getPreferredSize().height);
              
              // ---- CAPRAX ----
              CAPRAX.setHorizontalAlignment(SwingConstants.RIGHT);
              CAPRAX.setText("@CAPRAX@");
              CAPRAX.setName("CAPRAX");
              CNAContentContainer.add(CAPRAX);
              CAPRAX.setBounds(110, 38, 90, CAPRAX.getPreferredSize().height);
              
              // ---- OBJ_179_OBJ_179 ----
              OBJ_179_OBJ_179.setText("Prix d'achat");
              OBJ_179_OBJ_179.setName("OBJ_179_OBJ_179");
              CNAContentContainer.add(OBJ_179_OBJ_179);
              OBJ_179_OBJ_179.setBounds(15, 40, 72, 20);
              
              // ---- OBJ_168_OBJ_168 ----
              OBJ_168_OBJ_168.setText("par");
              OBJ_168_OBJ_168.setToolTipText("Unit\u00e9 d'achat");
              OBJ_168_OBJ_168.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_168_OBJ_168.setName("OBJ_168_OBJ_168");
              CNAContentContainer.add(OBJ_168_OBJ_168);
              OBJ_168_OBJ_168.setBounds(200, 40, 35, 20);
              
              // ---- CADEL ----
              CADEL.setHorizontalAlignment(SwingConstants.RIGHT);
              CADEL.setComponentPopupMenu(null);
              CADEL.setText("@CADEL@");
              CADEL.setName("CADEL");
              CNAContentContainer.add(CADEL);
              CADEL.setBounds(507, 38, 28, CADEL.getPreferredSize().height);
              
              // ---- riZoneSortie4 ----
              riZoneSortie4.setText("@FRNOMR@");
              riZoneSortie4.setName("riZoneSortie4");
              CNAContentContainer.add(riZoneSortie4);
              riZoneSortie4.setBounds(235, 8, 270, riZoneSortie4.getPreferredSize().height);
              
              // ---- CADEV ----
              CADEV.setText("@CADEV@");
              CADEV.setName("CADEV");
              CNAContentContainer.add(CADEV);
              CADEV.setBounds(275, 38, 40, 24);
            }
            layeredPane1.add(CNA, JLayeredPane.DEFAULT_LAYER);
            CNA.setBounds(20, 390, 555, 100);
            
            // ======== STAT ========
            {
              STAT.setTitle("Statistiques");
              STAT.setBorder(new DropShadowBorder());
              STAT.setMinimumSize(new Dimension(280, 112));
              STAT.setName("STAT");
              Container STATContentContainer = STAT.getContentContainer();
              STATContentContainer.setLayout(null);
              
              // ======== voir_stats ========
              {
                voir_stats.setBackground(new Color(239, 239, 222));
                voir_stats.setName("voir_stats");
                voir_stats.setLayout(null);
                
                // ---- SVCR1 ----
                SVCR1.setText("@SVCR1@");
                SVCR1.setHorizontalAlignment(SwingConstants.RIGHT);
                SVCR1.setName("SVCR1");
                voir_stats.add(SVCR1);
                SVCR1.setBounds(110, 8, 75, SVCR1.getPreferredSize().height);
                
                // ---- SVCRMR ----
                SVCRMR.setText("@SVCRMR@");
                SVCRMR.setHorizontalAlignment(SwingConstants.RIGHT);
                SVCRMR.setName("SVCRMR");
                voir_stats.add(SVCRMR);
                SVCRMR.setBounds(110, 38, 75, SVCRMR.getPreferredSize().height);
                
                // ---- OBJ_193_OBJ_193 ----
                OBJ_193_OBJ_193.setText("Exercice CA /Qt\u00e9");
                OBJ_193_OBJ_193.setName("OBJ_193_OBJ_193");
                voir_stats.add(OBJ_193_OBJ_193);
                OBJ_193_OBJ_193.setBounds(15, 10, 95, 20);
                
                // ---- SVQTMR ----
                SVQTMR.setText("@SVQTMR@");
                SVQTMR.setHorizontalAlignment(SwingConstants.RIGHT);
                SVQTMR.setName("SVQTMR");
                voir_stats.add(SVQTMR);
                SVQTMR.setBounds(190, 38, 60, SVQTMR.getPreferredSize().height);
                
                // ---- OBJ_198_OBJ_198 ----
                OBJ_198_OBJ_198.setText("Mois CA/Qt\u00e9");
                OBJ_198_OBJ_198.setName("OBJ_198_OBJ_198");
                voir_stats.add(OBJ_198_OBJ_198);
                OBJ_198_OBJ_198.setBounds(15, 40, 90, 20);
                
                // ---- SVQT1R ----
                SVQT1R.setText("@SVQT1R@");
                SVQT1R.setHorizontalAlignment(SwingConstants.RIGHT);
                SVQT1R.setName("SVQT1R");
                voir_stats.add(SVQT1R);
                SVQT1R.setBounds(190, 8, 60, SVQT1R.getPreferredSize().height);
                
                { // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < voir_stats.getComponentCount(); i++) {
                    Rectangle bounds = voir_stats.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = voir_stats.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  voir_stats.setMinimumSize(preferredSize);
                  voir_stats.setPreferredSize(preferredSize);
                }
              }
              STATContentContainer.add(voir_stats);
              voir_stats.setBounds(new Rectangle(new Point(0, 0), voir_stats.getPreferredSize()));
              
              // ---- pas_stats ----
              pas_stats.setHorizontalAlignment(SwingConstants.CENTER);
              pas_stats.setFont(
                  pas_stats.getFont().deriveFont(pas_stats.getFont().getStyle() | Font.BOLD, pas_stats.getFont().getSize() + 3f));
              pas_stats.setText("Pas de statistiques");
              pas_stats.setName("pas_stats");
              STATContentContainer.add(pas_stats);
              pas_stats.setBounds(40, 5, 180, 60);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < STATContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = STATContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = STATContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                STATContentContainer.setMinimumSize(preferredSize);
                STATContentContainer.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(STAT, JLayeredPane.DEFAULT_LAYER);
            STAT.setBounds(590, 390, 270, 100);
            
            // ======== CHIFFR ========
            {
              CHIFFR.setTitle("Chiffrage stock");
              CHIFFR.setBorder(new DropShadowBorder());
              CHIFFR.setName("CHIFFR");
              Container CHIFFRContentContainer = CHIFFR.getContentContainer();
              CHIFFRContentContainer.setLayout(null);
              
              // ---- lbCOEMAR ----
              lbCOEMAR.setText("Prix vente/P.U.M.P.");
              lbCOEMAR.setName("lbCOEMAR");
              CHIFFRContentContainer.add(lbCOEMAR);
              lbCOEMAR.setBounds(330, 9, 110, 20);
              
              // ---- A1PRV ----
              A1PRV.setHorizontalAlignment(SwingConstants.RIGHT);
              A1PRV.setName("A1PRV");
              A1PRV.addKeyListener(new KeyAdapter() {
                @Override
                public void keyTyped(KeyEvent e) {
                  A1PRVKeyTyped(e);
                }
              });
              CHIFFRContentContainer.add(A1PRV);
              A1PRV.setBounds(110, 6, 90, A1PRV.getPreferredSize().height);
              
              // ---- COEMAR ----
              COEMAR.setHorizontalAlignment(SwingConstants.RIGHT);
              COEMAR.setName("COEMAR");
              CHIFFRContentContainer.add(COEMAR);
              COEMAR.setBounds(442, 5, 78, COEMAR.getPreferredSize().height);
              
              // ---- WPMP ----
              WPMP.setHorizontalAlignment(SwingConstants.RIGHT);
              WPMP.setText("@WPMP@");
              WPMP.setName("WPMP");
              CHIFFRContentContainer.add(WPMP);
              WPMP.setBounds(110, 38, 90, WPMP.getPreferredSize().height);
              
              // ---- A1TVA ----
              A1TVA.setComponentPopupMenu(BTD);
              A1TVA.setName("A1TVA");
              CHIFFRContentContainer.add(A1TVA);
              A1TVA.setBounds(245, 36, 20, A1TVA.getPreferredSize().height);
              
              // ---- WPSTD ----
              WPSTD.setHorizontalAlignment(SwingConstants.RIGHT);
              WPSTD.setText("@WPSTD@");
              WPSTD.setName("WPSTD");
              CHIFFRContentContainer.add(WPSTD);
              WPSTD.setBounds(442, 36, 78, WPSTD.getPreferredSize().height);
              
              // ---- lbPRV ----
              lbPRV.setText("Prix revient");
              lbPRV.setName("lbPRV");
              CHIFFRContentContainer.add(lbPRV);
              lbPRV.setBounds(15, 10, 85, 20);
              
              // ---- lbWPSTD ----
              lbWPSTD.setText("Prix standard");
              lbWPSTD.setName("lbWPSTD");
              CHIFFRContentContainer.add(lbWPSTD);
              lbWPSTD.setBounds(330, 40, 110, 20);
              
              // ---- lbPMP ----
              lbPMP.setText("P.U.M.P.");
              lbPMP.setName("lbPMP");
              CHIFFRContentContainer.add(lbPMP);
              lbPMP.setBounds(15, 40, 69, 20);
              
              // ---- lbDeprecie ----
              lbDeprecie.setText("D\u00e9pr\u00e9ci\u00e9");
              lbDeprecie.setForeground(Color.red);
              lbDeprecie.setFont(lbDeprecie.getFont().deriveFont(lbDeprecie.getFont().getStyle() | Font.BOLD));
              lbDeprecie.setName("lbDeprecie");
              CHIFFRContentContainer.add(lbDeprecie);
              lbDeprecie.setBounds(270, 10, 55, 20);
              
              // ---- OBJ_226_OBJ_226 ----
              OBJ_226_OBJ_226.setText("@TTVAX@");
              OBJ_226_OBJ_226.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_226_OBJ_226.setName("OBJ_226_OBJ_226");
              CHIFFRContentContainer.add(OBJ_226_OBJ_226);
              OBJ_226_OBJ_226.setBounds(270, 38, 45, OBJ_226_OBJ_226.getPreferredSize().height);
              
              // ---- OBJ_225_OBJ_225 ----
              OBJ_225_OBJ_225.setText("TVA");
              OBJ_225_OBJ_225.setName("OBJ_225_OBJ_225");
              CHIFFRContentContainer.add(OBJ_225_OBJ_225);
              OBJ_225_OBJ_225.setBounds(210, 40, 30, 20);
              
              // ---- A1UNS2 ----
              A1UNS2.setComponentPopupMenu(BTD);
              A1UNS2.setText("@A1UNS@");
              A1UNS2.setToolTipText("Unit\u00e9 de stock");
              A1UNS2.setName("A1UNS2");
              CHIFFRContentContainer.add(A1UNS2);
              A1UNS2.setBounds(231, 8, 34, A1UNS2.getPreferredSize().height);
              
              // ---- lbA1UNS2 ----
              lbA1UNS2.setText("par");
              lbA1UNS2.setToolTipText("Unit\u00e9 d'achat");
              lbA1UNS2.setHorizontalAlignment(SwingConstants.CENTER);
              lbA1UNS2.setName("lbA1UNS2");
              CHIFFRContentContainer.add(lbA1UNS2);
              lbA1UNS2.setBounds(203, 10, 25, 20);
            }
            layeredPane1.add(CHIFFR, JLayeredPane.DEFAULT_LAYER);
            CHIFFR.setBounds(20, 495, 555, 100);
            
            // ======== DIVERS ========
            {
              DIVERS.setTitle("Divers");
              DIVERS.setBorder(new DropShadowBorder());
              DIVERS.setName("DIVERS");
              Container DIVERSContentContainer = DIVERS.getContentContainer();
              DIVERSContentContainer.setLayout(null);
              
              // ---- OBJ_227_OBJ_227 ----
              OBJ_227_OBJ_227.setText("Etat");
              OBJ_227_OBJ_227.setName("OBJ_227_OBJ_227");
              DIVERSContentContainer.add(OBJ_227_OBJ_227);
              OBJ_227_OBJ_227.setBounds(15, 11, 40, 20);
              
              // ---- label7 ----
              label7.setText("Non suivi");
              label7.setName("label7");
              DIVERSContentContainer.add(label7);
              label7.setBounds(15, 41, 60, 18);
              
              // ---- A1TNS ----
              A1TNS.setComponentPopupMenu(BTD);
              A1TNS.setName("A1TNS");
              DIVERSContentContainer.add(A1TNS);
              A1TNS.setBounds(75, 36, 34, A1TNS.getPreferredSize().height);
              
              // ---- OBJ_231_OBJ_231 ----
              OBJ_231_OBJ_231.setText("@NSLIBR@");
              OBJ_231_OBJ_231.setName("OBJ_231_OBJ_231");
              DIVERSContentContainer.add(OBJ_231_OBJ_231);
              OBJ_231_OBJ_231.setBounds(120, 38, 129, OBJ_231_OBJ_231.getPreferredSize().height);
              
              // ---- A1NPU ----
              A1NPU.setModel(new DefaultComboBoxModel<>(new String[] { "Article actif", "Article d\u00e9sactiv\u00e9",
                  "Article \u00e9puis\u00e9", "Syst\u00e8me variable principal", "Syst\u00e8me variable secondaire",
                  "Pr\u00e9-'fin de serie'", "Fin de serie" }));
              A1NPU.setComponentPopupMenu(BTD);
              A1NPU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              A1NPU.setName("A1NPU");
              DIVERSContentContainer.add(A1NPU);
              A1NPU.setBounds(54, 8, 195, A1NPU.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < DIVERSContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = DIVERSContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = DIVERSContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                DIVERSContentContainer.setMinimumSize(preferredSize);
                DIVERSContentContainer.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(DIVERS, JLayeredPane.DEFAULT_LAYER);
            DIVERS.setBounds(590, 495, 270, 100);
            
            // ======== panel4 ========
            {
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);
              
              // ---- label6 ----
              label6.setText("Substitution");
              label6.setHorizontalAlignment(SwingConstants.RIGHT);
              label6.setName("label6");
              panel4.add(label6);
              label6.setBounds(5, 5, 90, 22);
              
              // ---- A1ASB ----
              A1ASB.setComponentPopupMenu(null);
              A1ASB.setToolTipText("Article de remplacement ou aiguillage");
              A1ASB.setName("A1ASB");
              panel4.add(A1ASB);
              A1ASB.setBounds(245, 2, 225, A1ASB.getPreferredSize().height);
              
              // ---- A1IN4 ----
              A1IN4.setModel(new DefaultComboBoxModel<>(new String[] { "Rupture de stock", "Aiguillage", "Equivalences", "Interne",
                  "Equivalent ma\u00eetre", "Remplacement", "Variante", "Remplac. avec tarif", "Tarif" }));
              A1IN4.setComponentPopupMenu(BTD);
              A1IN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              A1IN4.setName("A1IN4");
              panel4.add(A1IN4);
              A1IN4.setBounds(100, 3, 145, A1IN4.getPreferredSize().height);
              
              // ---- titSub ----
              titSub.setText("Origine");
              titSub.setToolTipText("Historique/Actuel");
              titSub.setHorizontalAlignment(SwingConstants.RIGHT);
              titSub.setName("titSub");
              panel4.add(titSub);
              titSub.setBounds(5, 32, 90, 20);
              
              // ---- label1 ----
              label1.setText("@WAIGUL@");
              label1.setName("label1");
              panel4.add(label1);
              label1.setBounds(100, 30, 145, label1.getPreferredSize().height);
              
              // ---- OBJ_99 ----
              OBJ_99.setText("");
              OBJ_99.setToolTipText("Num\u00e9ro de code barres");
              OBJ_99.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_99.setName("OBJ_99");
              OBJ_99.addActionListener(e -> OBJ_99ActionPerformed(e));
              panel4.add(OBJ_99);
              OBJ_99.setBounds(213, 83, 32, 30);
              
              // ---- A1GCDX ----
              A1GCDX.setToolTipText("Num\u00e9ro de code barres");
              A1GCDX.setComponentPopupMenu(BTD);
              A1GCDX.setName("A1GCDX");
              panel4.add(A1GCDX);
              A1GCDX.setBounds(100, 84, 114, A1GCDX.getPreferredSize().height);
              
              // ---- label2 ----
              label2.setText("Code barres");
              label2.setHorizontalAlignment(SwingConstants.RIGHT);
              label2.setName("label2");
              panel4.add(label2);
              label2.setBounds(5, 87, 90, 22);
              
              // ---- separator1 ----
              separator1.setOrientation(SwingConstants.VERTICAL);
              separator1.setName("separator1");
              panel4.add(separator1);
              separator1.setBounds(0, 0, 10, 150);
              
              // ---- snPhotoArticle ----
              snPhotoArticle.setBorder(null);
              snPhotoArticle.setBackground(new Color(239, 239, 222));
              snPhotoArticle.setForeground(Color.black);
              snPhotoArticle.setOpaque(false);
              snPhotoArticle.setName("snPhotoArticle");
              panel4.add(snPhotoArticle);
              snPhotoArticle.setBounds(345, 45, 125, 100);
              
              // ---- label10 ----
              label10.setText("Observation");
              label10.setHorizontalAlignment(SwingConstants.RIGHT);
              label10.setName("label10");
              panel4.add(label10);
              label10.setBounds(5, 59, 90, 20);
              
              // ---- A1OBS ----
              A1OBS.setName("A1OBS");
              panel4.add(A1OBS);
              A1OBS.setBounds(100, 55, 110, A1OBS.getPreferredSize().height);
              
              // ---- button1 ----
              button1.setBorderPainted(false);
              button1.setContentAreaFilled(false);
              button1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              button1.setToolTipText("Bloc - notes");
              button1.setName("button1");
              button1.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                  button1MouseEntered(e);
                }
                
                @Override
                public void mouseExited(MouseEvent e) {
                  button1MouseExited(e);
                }
              });
              button1.addActionListener(e -> button1ActionPerformed(e));
              panel4.add(button1);
              button1.setBounds(205, 60, 27, 27);
              
              // ---- lbTypeProduit ----
              lbTypeProduit.setText("Type de produit ");
              lbTypeProduit.setHorizontalAlignment(SwingConstants.RIGHT);
              lbTypeProduit.setName("lbTypeProduit");
              panel4.add(lbTypeProduit);
              lbTypeProduit.setBounds(5, 116, 90, 22);
              
              // ---- A1IN20 ----
              A1IN20.setComponentPopupMenu(BTD);
              A1IN20.setName("A1IN20");
              panel4.add(A1IN20);
              A1IN20.setBounds(100, 113, 24, A1IN20.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(panel4, JLayeredPane.DEFAULT_LAYER);
            panel4.setBounds(380, 100, 480, panel4.getPreferredSize().height);
            
            // ======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);
              
              // ---- A1GRP ----
              A1GRP.setComponentPopupMenu(BTD);
              A1GRP.setName("A1GRP");
              panel3.add(A1GRP);
              A1GRP.setBounds(100, 63, 24, A1GRP.getPreferredSize().height);
              
              // ---- A1FAM ----
              A1FAM.setComponentPopupMenu(BTD);
              A1FAM.setName("A1FAM");
              panel3.add(A1FAM);
              A1FAM.setBounds(100, 89, 40, A1FAM.getPreferredSize().height);
              
              // ---- WSFA ----
              WSFA.setComponentPopupMenu(BTD);
              WSFA.setName("WSFA");
              panel3.add(WSFA);
              WSFA.setBounds(100, 115, 34, WSFA.getPreferredSize().height);
              
              // ---- A1SFA ----
              A1SFA.setComponentPopupMenu(BTD);
              A1SFA.setName("A1SFA");
              panel3.add(A1SFA);
              A1SFA.setBounds(100, 115, 60, A1SFA.getPreferredSize().height);
              
              // ---- OBJ_130_OBJ_130 ----
              OBJ_130_OBJ_130.setText("Sous-famille");
              OBJ_130_OBJ_130.setName("OBJ_130_OBJ_130");
              panel3.add(OBJ_130_OBJ_130);
              OBJ_130_OBJ_130.setBounds(10, 120, 90, 18);
              
              // ---- OBJ_112_OBJ_112 ----
              OBJ_112_OBJ_112.setText("Groupe");
              OBJ_112_OBJ_112.setName("OBJ_112_OBJ_112");
              panel3.add(OBJ_112_OBJ_112);
              OBJ_112_OBJ_112.setBounds(10, 68, 52, 18);
              
              // ---- OBJ_126_OBJ_126 ----
              OBJ_126_OBJ_126.setText("Famille");
              OBJ_126_OBJ_126.setName("OBJ_126_OBJ_126");
              panel3.add(OBJ_126_OBJ_126);
              OBJ_126_OBJ_126.setBounds(10, 94, 52, 18);
              
              // ---- riZoneSortie1 ----
              riZoneSortie1.setText("@GRLIBR@");
              riZoneSortie1.setName("riZoneSortie1");
              panel3.add(riZoneSortie1);
              riZoneSortie1.setBounds(170, 65, 180, riZoneSortie1.getPreferredSize().height);
              
              // ---- riZoneSortie2 ----
              riZoneSortie2.setText("@FALIB@");
              riZoneSortie2.setName("riZoneSortie2");
              panel3.add(riZoneSortie2);
              riZoneSortie2.setBounds(170, 91, 180, riZoneSortie2.getPreferredSize().height);
              
              // ---- riZoneSortie3 ----
              riZoneSortie3.setText("@SFLIB@");
              riZoneSortie3.setName("riZoneSortie3");
              panel3.add(riZoneSortie3);
              riZoneSortie3.setBounds(170, 117, 180, riZoneSortie3.getPreferredSize().height);
              
              // ---- TMC1 ----
              TMC1.setText("@TMC1@");
              TMC1.setName("TMC1");
              panel3.add(TMC1);
              TMC1.setBounds(10, 8, 130, 22);
              
              // ---- TMC2 ----
              TMC2.setText("@TMC2@");
              TMC2.setName("TMC2");
              panel3.add(TMC2);
              TMC2.setBounds(10, 38, 130, 22);
              
              // ---- A1CL1 ----
              A1CL1.setComponentPopupMenu(BTD);
              A1CL1.setToolTipText("Argument de recherche alphab\u00e9tique n\u00b01");
              A1CL1.setName("A1CL1");
              panel3.add(A1CL1);
              A1CL1.setBounds(140, 5, 210, A1CL1.getPreferredSize().height);
              
              // ---- A1CL2 ----
              A1CL2.setComponentPopupMenu(BTD);
              A1CL2.setToolTipText("Argument de recherche alphab\u00e9tique n\u00b02");
              A1CL2.setName("A1CL2");
              panel3.add(A1CL2);
              A1CL2.setBounds(140, 35, 210, A1CL2.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(panel3, JLayeredPane.DEFAULT_LAYER);
            panel3.setBounds(new Rectangle(new Point(20, 100), panel3.getPreferredSize()));
            
            // ======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setBorder(new TitledBorder(""));
              panel2.setName("panel2");
              panel2.setLayout(null);
              
              // ---- incrustation ----
              incrustation.setModel(new DefaultComboBoxModel<>(new String[] { "aucune", "compl\u00e9ment", "nombre de pi\u00e8ces",
                  "rattachement tarif", "titre param\u00e9tr\u00e9" }));
              incrustation.setName("incrustation");
              panel2.add(incrustation);
              incrustation.setBounds(525, 2, 155, incrustation.getPreferredSize().height);
              
              // ---- OBJ_73_OBJ_73 ----
              OBJ_73_OBJ_73.setText("D\u00e9signations");
              OBJ_73_OBJ_73.setFont(OBJ_73_OBJ_73.getFont().deriveFont(OBJ_73_OBJ_73.getFont().getStyle() | Font.BOLD));
              OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");
              panel2.add(OBJ_73_OBJ_73);
              OBJ_73_OBJ_73.setBounds(40, 5, 90, 21);
              
              // ---- btnChangeDesignation ----
              btnChangeDesignation.setName("btnChangeDesignation");
              btnChangeDesignation.addActionListener(e -> changeDesignationActionPerformed(e));
              panel2.add(btnChangeDesignation);
              btnChangeDesignation.setBounds(120, 5, btnChangeDesignation.getPreferredSize().width, 21);
              
              // ---- OBJ_97_OBJ_97 ----
              OBJ_97_OBJ_97.setText("@LIBZP1@");
              OBJ_97_OBJ_97.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_97_OBJ_97.setName("OBJ_97_OBJ_97");
              panel2.add(OBJ_97_OBJ_97);
              OBJ_97_OBJ_97.setBounds(286, 5, OBJ_97_OBJ_97.getPreferredSize().width, 21);
              
              // ---- A1LIB ----
              A1LIB.setFont(A1LIB.getFont().deriveFont(A1LIB.getFont().getStyle() | Font.BOLD));
              A1LIB.setComponentPopupMenu(BTD);
              A1LIB.setName("A1LIB");
              panel2.add(A1LIB);
              A1LIB.setBounds(40, 25, 310, A1LIB.getPreferredSize().height);
              
              // ---- A1LB1 ----
              A1LB1.setComponentPopupMenu(BTD);
              A1LB1.setName("A1LB1");
              panel2.add(A1LB1);
              A1LB1.setBounds(450, 25, 310, A1LB1.getPreferredSize().height);
              
              // ---- A1DSG3 ----
              A1DSG3.setText("");
              A1DSG3.setToolTipText("Usage en interne");
              A1DSG3.setComponentPopupMenu(BTD);
              A1DSG3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              A1DSG3.setName("A1DSG3");
              panel2.add(A1DSG3);
              A1DSG3.setBounds(new Rectangle(new Point(10, 55), A1DSG3.getPreferredSize()));
              
              // ---- A1DSG2 ----
              A1DSG2.setText("");
              A1DSG2.setToolTipText("Usage en interne");
              A1DSG2.setComponentPopupMenu(BTD);
              A1DSG2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              A1DSG2.setName("A1DSG2");
              panel2.add(A1DSG2);
              A1DSG2.setBounds(415, 29, 24, 20);
              
              // ---- A1LB3 ----
              A1LB3.setComponentPopupMenu(BTD);
              A1LB3.setName("A1LB3");
              panel2.add(A1LB3);
              A1LB3.setBounds(450, 50, 310, A1LB3.getPreferredSize().height);
              
              // ---- A1DSG4 ----
              A1DSG4.setText("");
              A1DSG4.setToolTipText("Usage en interne");
              A1DSG4.setComponentPopupMenu(BTD);
              A1DSG4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              A1DSG4.setName("A1DSG4");
              panel2.add(A1DSG4);
              A1DSG4.setBounds(415, 54, 24, 20);
              
              // ---- label9 ----
              label9.setText("Hors gamme     ");
              label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD, label9.getFont().getSize() + 4f));
              label9.setHorizontalAlignment(SwingConstants.RIGHT);
              label9.setForeground(new Color(0, 0, 92));
              label9.setBackground(new Color(0, 0, 92));
              label9.setName("label9");
              panel2.add(label9);
              label9.setBounds(655, 2, 175, 26);
              
              // ---- A1LB2 ----
              A1LB2.setComponentPopupMenu(BTD);
              A1LB2.setName("A1LB2");
              panel2.add(A1LB2);
              A1LB2.setBounds(40, 50, 310, A1LB2.getPreferredSize().height);
              
              // ---- label13 ----
              label13.setText("incrustation");
              label13.setName("label13");
              panel2.add(label13);
              label13.setBounds(450, 5, 90, 20);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(panel2, JLayeredPane.DEFAULT_LAYER);
            panel2.setBounds(10, 5, panel2.getPreferredSize().width, 90);
            
            // ======== panel5 ========
            {
              panel5.setOpaque(false);
              panel5.setBorder(new TitledBorder(""));
              panel5.setName("panel5");
              panel5.setLayout(null);
              
              // ---- OBJ_73_OBJ_74 ----
              OBJ_73_OBJ_74.setText("D\u00e9signations");
              OBJ_73_OBJ_74.setFont(OBJ_73_OBJ_74.getFont().deriveFont(OBJ_73_OBJ_74.getFont().getStyle() | Font.BOLD));
              OBJ_73_OBJ_74.setName("OBJ_73_OBJ_74");
              panel5.add(OBJ_73_OBJ_74);
              OBJ_73_OBJ_74.setBounds(40, 5, 90, 21);
              
              // ---- btnChangeDesignation2 ----
              btnChangeDesignation2.setName("btnChangeDesignation2");
              btnChangeDesignation2.addActionListener(e -> changeDesignationActionPerformed(e));
              panel5.add(btnChangeDesignation2);
              btnChangeDesignation2.setBounds(135, 5, btnChangeDesignation2.getPreferredSize().width, 21);
              
              // ---- OBJ_97_OBJ_98 ----
              OBJ_97_OBJ_98.setText("@LIBZP1@");
              OBJ_97_OBJ_98.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_97_OBJ_98.setName("OBJ_97_OBJ_98");
              panel5.add(OBJ_97_OBJ_98);
              OBJ_97_OBJ_98.setBounds(285, 5, OBJ_97_OBJ_98.getPreferredSize().width, 21);
              
              // ---- label12 ----
              label12.setText("Hors gamme     ");
              label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD, label12.getFont().getSize() + 4f));
              label12.setHorizontalAlignment(SwingConstants.RIGHT);
              label12.setForeground(new Color(0, 0, 92));
              label12.setBackground(new Color(0, 0, 92));
              label12.setName("label12");
              panel5.add(label12);
              label12.setBounds(415, 2, 415, 26);
              
              // ---- A1LB12 ----
              A1LB12.setFont(A1LB12.getFont().deriveFont(A1LB12.getFont().getStyle() | Font.BOLD));
              A1LB12.setComponentPopupMenu(BTD);
              A1LB12.setName("A1LB12");
              panel5.add(A1LB12);
              A1LB12.setBounds(40, 25, 610, A1LB12.getPreferredSize().height);
              
              // ---- A1LB34 ----
              A1LB34.setFont(A1LB34.getFont().deriveFont(A1LB34.getFont().getStyle() | Font.BOLD));
              A1LB34.setComponentPopupMenu(BTD);
              A1LB34.setName("A1LB34");
              panel5.add(A1LB34);
              A1LB34.setBounds(40, 50, 610, A1LB34.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            layeredPane1.add(panel5, JLayeredPane.DEFAULT_LAYER);
            panel5.setBounds(10, 5, 845, 88);
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addComponent(layeredPane1, GroupLayout.DEFAULT_SIZE,
              GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
          p_contenuLayout.setVerticalGroup(
              p_contenuLayout.createParallelGroup().addComponent(layeredPane1, GroupLayout.DEFAULT_SIZE, 603, Short.MAX_VALUE));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ---- bt_stock ----
    bt_stock.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_stock.setToolTipText("Stock");
    bt_stock.setName("bt_stock");
    bt_stock.addActionListener(e -> bt_stockActionPerformed());
    
    // ---- bt_tarif ----
    bt_tarif.setPreferredSize(new Dimension(18, 18));
    bt_tarif.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_tarif.setIconTextGap(5);
    bt_tarif.setToolTipText("Tarif");
    bt_tarif.setName("bt_tarif");
    bt_tarif.addActionListener(e -> bt_tarifActionPerformed());
    
    // ---- bt_CNA ----
    bt_CNA.setPreferredSize(new Dimension(18, 18));
    bt_CNA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_CNA.setToolTipText("Conditions d'achat");
    bt_CNA.setName("bt_CNA");
    bt_CNA.addActionListener(e -> bt_CNAActionPerformed());
    
    // ---- bt_stat ----
    bt_stat.setPreferredSize(new Dimension(18, 18));
    bt_stat.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_stat.setToolTipText("Statistiques");
    bt_stat.setName("bt_stat");
    bt_stat.addActionListener(e -> bt_statActionPerformed());
    
    // ---- bt_chiffrage ----
    bt_chiffrage.setPreferredSize(new Dimension(18, 18));
    bt_chiffrage.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_chiffrage.setToolTipText("Chiffrage");
    bt_chiffrage.setName("bt_chiffrage");
    bt_chiffrage.addActionListener(e -> bt_chiffrageActionPerformed());
    
    // ---- bt_divers ----
    bt_divers.setPreferredSize(new Dimension(18, 18));
    bt_divers.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_divers.setToolTipText("Divers");
    bt_divers.setName("bt_divers");
    bt_divers.addActionListener(e -> bt_diversActionPerformed());
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_22 ----
      OBJ_22.setText("Choix possibles");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(e -> OBJ_22ActionPerformed(e));
      BTD.add(OBJ_22);
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(e -> OBJ_21ActionPerformed(e));
      BTD.add(OBJ_21);
    }
    
    // ======== riSousMenu16 ========
    {
      riSousMenu16.setName("riSousMenu16");
      
      // ---- riSousMenu_bt16 ----
      riSousMenu_bt16.setText("Configurateur");
      riSousMenu_bt16.setName("riSousMenu_bt16");
      riSousMenu_bt16.addActionListener(e -> riSousMenu_bt16ActionPerformed());
      riSousMenu16.add(riSousMenu_bt16);
    }
    
    // ======== riSousMenu20 ========
    {
      riSousMenu20.setName("riSousMenu20");
      
      // ---- riSousMenu_bt19 ----
      riSousMenu_bt19.setText("Syst\u00e8me variable");
      riSousMenu_bt19.setToolTipText("Syst\u00e8me variable");
      riSousMenu_bt19.setName("riSousMenu_bt19");
      riSousMenu_bt19.addActionListener(e -> riSousMenu_bt19ActionPerformed(e));
      riSousMenu20.add(riSousMenu_bt19);
    }
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(e -> OBJ_23ActionPerformed(e));
      BTD2.add(OBJ_23);
      
      // ---- OBJ_24 ----
      OBJ_24.setText("Aide en ligne");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(e -> OBJ_21ActionPerformed(e));
      BTD2.add(OBJ_24);
    }
    
    // ---- X13STX ----
    X13STX.setHorizontalAlignment(SwingConstants.RIGHT);
    X13STX.setText("@X13STX@");
    X13STX.setName("X13STX");
    
    // ---- X13DVX ----
    X13DVX.setHorizontalAlignment(SwingConstants.RIGHT);
    X13DVX.setText("@X13DVX@");
    X13DVX.setName("X13DVX");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  private void riSousMenu_bt6ActionPerformed() {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "E");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostFieldPutData("TCI11", 0, "X");
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void riSousMenu_bt7ActionPerformed() {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt8ActionPerformed() {
    lexique.HostFieldPutData("V06FO", 0, "0");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt9ActionPerformed() {
    if (lexique.isTrue("52")) {
      lexique.HostCursorPut("A1LIB");
      lexique.HostScreenSendKey(this, "F4");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "L");
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void riSousMenu_bt20ActionPerformed() {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt14ActionPerformed() {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt15ActionPerformed() {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt16ActionPerformed() {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  /* Plus utilisé car double emploi avec boutonPhoto
  private void riSousMenu_bt17ActionPerformed() {
    lexique.HostScreenSendKey(this, "F11");
  }*/
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt21ActionPerformed() {
    if (optpanel != null) {
      optpanel.reveiller();
    }
    else {
      optpanel = new OPTION_PANEL(this, lexique, interpreteurD);
    }
  }
  
  private void OBJ_99ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "K");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostCursorPut(6, 67);
      lexique.HostScreenSendKey(this, "F4");
    }
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void labelNavBt1MouseClicked() {
    bt_CNAActionPerformed();
  }
  
  private void labelNavBt1MouseEntered(MouseEvent e) {
    labelNavBt1.setIcon(lexique.chargerImage("images/navselec.png", true));
  }
  
  private void labelNavBt1MouseExited(MouseEvent e) {
    labelNavBt1.setIcon(lexique.chargerImage("images/blank.png", true));
  }
  
  private void labelNavBt2MouseClicked() {
    bt_tarifActionPerformed();
  }
  
  private void labelNavBt2MouseEntered(MouseEvent e) {
    labelNavBt2.setIcon(lexique.chargerImage("images/navselec.png", true));
  }
  
  private void labelNavBt2MouseExited(MouseEvent e) {
    labelNavBt2.setIcon(lexique.chargerImage("images/blank.png", true));
  }
  
  private void labelNavBt3MouseClicked() {
    bt_diversActionPerformed();
  }
  
  private void labelNavBt3MouseEntered(MouseEvent e) {
    labelNavBt3.setIcon(lexique.chargerImage("images/navselec.png", true));
  }
  
  private void labelNavBt3MouseExited(MouseEvent e) {
    labelNavBt3.setIcon(lexique.chargerImage("images/blank.png", true));
  }
  
  private void labelNavBt4MouseClicked() {
    bt_statActionPerformed();
  }
  
  private void labelNavBt4MouseEntered(MouseEvent e) {
    labelNavBt4.setIcon(lexique.chargerImage("images/navselec.png", true));
  }
  
  private void labelNavBt4MouseExited(MouseEvent e) {
    labelNavBt4.setIcon(lexique.chargerImage("images/blank.png", true));
  }
  
  private void labelNavBt5MouseClicked() {
    bt_stockActionPerformed();
  }
  
  private void labelNavBt5MouseEntered(MouseEvent e) {
    labelNavBt5.setIcon(lexique.chargerImage("images/navselec.png", true));
  }
  
  private void labelNavBt5MouseExited(MouseEvent e) {
    labelNavBt5.setIcon(lexique.chargerImage("images/blank.png", true));
  }
  
  private void labelNavBt6MouseClicked() {
    bt_chiffrageActionPerformed();
  }
  
  private void labelNavBt6MouseEntered(MouseEvent e) {
    labelNavBt6.setIcon(lexique.chargerImage("images/navselec.png", true));
  }
  
  private void labelNavBt6MouseExited(MouseEvent e) {
    labelNavBt6.setIcon(lexique.chargerImage("images/blank.png", true));
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(24, 79);
    lexique.HostFieldPutData("V06FO", 0, "T");
  }
  
  private void bt_stockActionPerformed() {
    if (lexique.HostFieldGetData("TCI1").trim().isEmpty()) {
      lexique.HostFieldPutData("TCI1", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI1"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_tarifActionPerformed() {
    if (lexique.HostFieldGetData("TCI2").trim().isEmpty()) {
      lexique.HostFieldPutData("TCI2", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI2"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_CNAActionPerformed() {
    if (lexique.HostFieldGetData("TCI3").trim().isEmpty()) {
      lexique.HostFieldPutData("TCI3", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI3"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_statActionPerformed() {
    if (lexique.HostFieldGetData("LOCGRP").subSequence(0, 3).equals("GVM")) {
      if (lexique.HostFieldGetData("TCI4").trim().isEmpty()) {
        lexique.HostFieldPutData("TCI4", 0, "X");
      }
      else {
        lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI4"));
      }
    }
    else {
      if (lexique.HostFieldGetData("TCI5").trim().isEmpty()) {
        lexique.HostFieldPutData("TCI5", 0, "X");
      }
      else {
        lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI5"));
      }
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_chiffrageActionPerformed() {
    if (lexique.HostFieldGetData("TCI6").trim().isEmpty()) {
      lexique.HostFieldPutData("TCI6", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI6"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_diversActionPerformed() {
    if (lexique.HostFieldGetData("TCI7").trim().isEmpty()) {
      lexique.HostFieldPutData("TCI7", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI7"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button1MouseEntered(MouseEvent e) {
    button1.setIcon(bloc_couleur);
  }
  
  private void button1MouseExited(MouseEvent e) {
    button1.setIcon(bloc_neutre);
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    if (lexique.getMode() == Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("V06FO", 0, "U");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostCursorPut("WURL");
      lexique.HostScreenSendKey(this, "F4");
    }
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("A1RTA");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void changeDesignationActionPerformed(ActionEvent e) {
    if (lexique.getMode() == Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("V06FO", 0, "L");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostCursorPut("A1LIB");
      lexique.HostScreenSendKey(this, "F4");
    }
  }
  
  private void changeTarifActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("A1RTA");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt22ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void riSousMenu_bt23ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "À");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt24ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "@");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt25ActionPerformed(ActionEvent e) {
    if (lexique.getMode() == Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("V06FO", 0, "F");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostCursorPut("WFTC");
      lexique.HostScreenSendKey(this, "F4");
    }
  }
  
  private void riSousMenu_bt26ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "Â");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt27ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "N");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt28ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "O");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt29ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, ">");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btnDetailStockActionPerformed(ActionEvent e) {
    try {
      ModeleDetailStock modele = new ModeleDetailStock(getSession(), idEtablissement, idMagasin, idArticle);
      DialogueDetailStock vue = new DialogueDetailStock(modele);
      vue.afficher();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Capturer l'évènement lorsque l'utilisateur tape des caractères.
   * @param e L'évènement
   */
  private void A1PRVKeyTyped(KeyEvent e) {
    try {
      char caractereTaper = e.getKeyChar();
      int longueurMaxPartieEntiere = 7;
      int longueurMaxPartieDecimale = 2;
      // Vérifier si le montant tapé par l'utilisateur matche avec le format défini
      if (!A1PRV.verifierFormatMontant(longueurMaxPartieEntiere, longueurMaxPartieDecimale, caractereTaper)) {
        e.consume();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_41_OBJ_41;
  private RiZoneSortie INDETB;
  private JLabel OBJ_46_OBJ_46;
  private RiZoneSortie INDART;
  private JLabel labelHisto;
  private JLabel OBJ_228_OBJ_228;
  private XRiComboBox A1IN3;
  private JLabel A1SPE;
  private JLabel lbArticlesLiesPresents;
  private JPanel p_tete_droite;
  private RiZoneSortie etat_article;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu22;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu17;
  private RiSousMenu_bt riSousMenu_bt17;
  private RiSousMenu riSousMenu26;
  private RiSousMenu_bt riSousMenu_bt25;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu23;
  private RiSousMenu_bt riSousMenu_bt22;
  private RiSousMenu riSousMenu24;
  private RiSousMenu_bt riSousMenu_bt23;
  private RiSousMenu riSousMenu25;
  private RiSousMenu_bt riSousMenu_bt24;
  private RiSousMenu riSousMenu27;
  private RiSousMenu_bt riSousMenu_bt26;
  private RiSousMenu riSousMenu28;
  private RiSousMenu_bt riSousMenu_bt27;
  private RiSousMenu riSousMenu29;
  private RiSousMenu_bt riSousMenu_bt28;
  private RiSousMenu riSousMenu30;
  private RiSousMenu_bt riSousMenu_bt29;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private JPanel panelNav;
  private JLabel labelNavBt1;
  private JLabel labelNavBt2;
  private JLabel labelNavBt3;
  private JLabel labelNavBt4;
  private JLabel labelNavBt5;
  private JLabel labelNavBt6;
  private JLabel labelNav;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLayeredPane layeredPane1;
  private JXTitledPanel STOCK;
  private JLabel non_stock;
  private JPanel en_stock;
  private JLabel lbStockPhysique;
  private XRiTextField A1UNS;
  private JLabel OBJ_143_OBJ_144;
  private XRiTextField A1KSVX;
  private RiZoneSortie A1UNV2;
  private XRiCheckBox A1STK;
  private JLabel lbStockDisponible;
  private SNNombreDecimal tfStockPhysique;
  private SNNombreDecimal tfStockDisponible;
  private SNBoutonDetail btnDetailStock;
  private JXTitledPanel TARIF;
  private XRiTextField A1UNV;
  private XRiTextField U1P11;
  private XRiTextField TU1P11;
  private JLabel lbTarifPar;
  private JLabel lbTarifDateApplique;
  private JLabel lbTarifApplique;
  private XRiTextField U1P21;
  private XRiTextField TU1P21;
  private JLabel lbTarifAppliqueParentheseFermante;
  private JLabel label8;
  private RiZoneSortie label11;
  private SNBoutonDetail changeTarif;
  private XRiTextField A1RTA;
  private JXTitledPanel CNA;
  private JLabel OBJ_183_OBJ_183;
  private JLabel OBJ_170_OBJ_170;
  private RiZoneSortie CAUNA;
  private RiZoneSortie A1COF;
  private RiZoneSortie A1FRS;
  private RiZoneSortie CAPRAX;
  private JLabel OBJ_179_OBJ_179;
  private JLabel OBJ_168_OBJ_168;
  private RiZoneSortie CADEL;
  private RiZoneSortie riZoneSortie4;
  private JLabel CADEV;
  private JXTitledPanel STAT;
  private JPanel voir_stats;
  private RiZoneSortie SVCR1;
  private RiZoneSortie SVCRMR;
  private JLabel OBJ_193_OBJ_193;
  private RiZoneSortie SVQTMR;
  private JLabel OBJ_198_OBJ_198;
  private RiZoneSortie SVQT1R;
  private JLabel pas_stats;
  private JXTitledPanel CHIFFR;
  private JLabel lbCOEMAR;
  private XRiTextField A1PRV;
  private XRiTextField COEMAR;
  private RiZoneSortie WPMP;
  private XRiTextField A1TVA;
  private RiZoneSortie WPSTD;
  private JLabel lbPRV;
  private JLabel lbWPSTD;
  private JLabel lbPMP;
  private JLabel lbDeprecie;
  private RiZoneSortie OBJ_226_OBJ_226;
  private JLabel OBJ_225_OBJ_225;
  private RiZoneSortie A1UNS2;
  private JLabel lbA1UNS2;
  private JXTitledPanel DIVERS;
  private JLabel OBJ_227_OBJ_227;
  private JLabel label7;
  private XRiTextField A1TNS;
  private RiZoneSortie OBJ_231_OBJ_231;
  private XRiComboBox A1NPU;
  private JPanel panel4;
  private JLabel label6;
  private XRiTextField A1ASB;
  private XRiComboBox A1IN4;
  private JLabel titSub;
  private RiZoneSortie label1;
  private SNBoutonDetail OBJ_99;
  private XRiTextField A1GCDX;
  private JLabel label2;
  private JSeparator separator1;
  private SNPhotoArticle snPhotoArticle;
  private JLabel label10;
  private XRiTextField A1OBS;
  private JButton button1;
  private JLabel lbTypeProduit;
  private XRiTextField A1IN20;
  private JPanel panel3;
  private XRiTextField A1GRP;
  private XRiTextField A1FAM;
  private XRiTextField WSFA;
  private XRiTextField A1SFA;
  private JLabel OBJ_130_OBJ_130;
  private JLabel OBJ_112_OBJ_112;
  private JLabel OBJ_126_OBJ_126;
  private RiZoneSortie riZoneSortie1;
  private RiZoneSortie riZoneSortie2;
  private RiZoneSortie riZoneSortie3;
  private JLabel TMC1;
  private JLabel TMC2;
  private XRiTextField A1CL1;
  private XRiTextField A1CL2;
  private JPanel panel2;
  private JComboBox<String> incrustation;
  private JLabel OBJ_73_OBJ_73;
  private SNBoutonDetail btnChangeDesignation;
  private JLabel OBJ_97_OBJ_97;
  private XRiTextField A1LIB;
  private XRiTextField A1LB1;
  private XRiCheckBox A1DSG3;
  private XRiCheckBox A1DSG2;
  private XRiTextField A1LB3;
  private XRiCheckBox A1DSG4;
  private JLabel label9;
  private XRiTextField A1LB2;
  private JLabel label13;
  private JPanel panel5;
  private JLabel OBJ_73_OBJ_74;
  private SNBoutonDetail btnChangeDesignation2;
  private JLabel OBJ_97_OBJ_98;
  private JLabel label12;
  private XRiTextField A1LB12;
  private XRiTextField A1LB34;
  private SNBoutonDetail bt_stock;
  private SNBoutonDetail bt_tarif;
  private SNBoutonDetail bt_CNA;
  private SNBoutonDetail bt_stat;
  private SNBoutonDetail bt_chiffrage;
  private SNBoutonDetail bt_divers;
  private JPopupMenu BTD;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_21;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt19;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private RiZoneSortie X13STX;
  private RiZoneSortie X13DVX;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
