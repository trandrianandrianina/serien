
package ri.serien.libecranrpg.vgvx.VGVXSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXSEFM_B6 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] A_Value = { "", "0", "1", "2", "3", };
  
  public VGVXSEFM_B6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SET135.setValeurs(A_Value, null);
    SET133.setValeurs(A_Value, null);
    SET131.setValeurs(A_Value, null);
    SET136.setValeursSelection("0", "1");
    SET163.setValeursSelection("0", "1");
    SET158.setValeursSelection("0", "1");
    SET138.setValeursSelection("0", "1");
    SET137.setValeursSelection("0", "1");
    SET134.setValeursSelection("0", "1");
    SET132.setValeursSelection("0", "1");
    SET178.setValeursSelection("0", "1");
    SET180.setValeursSelection("0", "1");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(
        lexique.TranslationTable(interpreteurD.analyseExpression("Sécurité de la gestion des ventes et des achats (@LOCUSR@)")).trim());
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_96.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    riSousMenu6.setEnabled(lexique.isTrue("51"));
    
    // V06F.setVisible(!lexique.HostFieldGetData("V01F").trim().equalsIgnoreCase("MODIFIC."));
    V06F1.setVisible(lexique.isTrue("N53"));
    V06F.setVisible(!V06F1.isVisible());
    
    // Gestion des tops mals initialisés : si on a une case à cocher, on lit la zone correspondante. Si blanc décoche
    // comme avec la valeur 1.
    // SEUL LE 0 COCHE (autorisé).
    for (int i = 0; i < getAllComponents(this).size(); i++) {
      if (getAllComponents(this).get(i) instanceof XRiCheckBox) {
        if (lexique.HostFieldGetData(getAllComponents(this).get(i).getName()).trim().equals("")) {
          ((XRiCheckBox) getAllComponents(this).get(i)).setSelected(false);
        }
      }
    }
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (V06F1.isVisible()) {
      V06F1.setText("F");
    }
    else {
      lexique.HostFieldPutData("V06F1", 0, "F");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  public static List<Component> getAllComponents(final Container c) {
    Component[] comps = c.getComponents();
    List<Component> compList = new ArrayList<Component>();
    for (Component comp : comps) {
      compList.add(comp);
      if (comp instanceof Container) {
        compList.addAll(getAllComponents((Container) comp));
      }
    }
    return compList;
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    INDUSR = new RiZoneSortie();
    OBJ_36 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_40 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_96 = new JLabel();
    OBJ_95 = new JLabel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    scroll_droite3 = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    pnlBas = new SNPanel();
    V06F = new XRiTextField();
    V06F1 = new XRiTextField();
    OBJ_79 = new SNBoutonLeger();
    xTitledPanel1 = new JXTitledPanel();
    pnlGauche = new SNPanel();
    OBJ_97 = new JLabel();
    OBJ_123 = new JLabel();
    OBJ_124 = new JLabel();
    OBJ_186 = new JLabel();
    OBJ_189 = new JLabel();
    OBJ_193 = new JLabel();
    OBJ_294 = new JLabel();
    OBJ_195 = new JLabel();
    OBJ_196 = new JLabel();
    OBJ_101 = new JLabel();
    SET132 = new XRiCheckBox();
    SET134 = new XRiCheckBox();
    SET137 = new XRiCheckBox();
    SET138 = new XRiCheckBox();
    SET158 = new XRiCheckBox();
    SET163 = new XRiCheckBox();
    SET131 = new XRiComboBox();
    OBJ_200 = new JLabel();
    SET133 = new XRiComboBox();
    SET135 = new XRiComboBox();
    SET136 = new XRiCheckBox();
    OBJ_201 = new JLabel();
    OBJ_202 = new JLabel();
    pnlDroite = new SNPanel();
    OBJ_198 = new JLabel();
    SET178 = new XRiCheckBox();
    OBJ_157 = new JLabel();
    SET180 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 de la gestion des ventes et des achats (@LOCUSR@)");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_34 ----
          OBJ_34.setText("Utilisateur");
          OBJ_34.setName("OBJ_34");
          
          // ---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setOpaque(false);
          INDUSR.setText("@INDUSR@");
          INDUSR.setName("INDUSR");
          
          // ---- OBJ_36 ----
          OBJ_36.setText("Etablissement");
          OBJ_36.setName("OBJ_36");
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");
          
          // ---- OBJ_40 ----
          OBJ_40.setText("@DGNOM@");
          OBJ_40.setOpaque(false);
          OBJ_40.setName("OBJ_40");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE).addGap(7, 7, 7)
                  .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE).addGap(33, 33, 33)
                  .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE).addGap(3, 3, 3)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(9, 9, 9)
                  .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 339, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- OBJ_96 ----
          OBJ_96.setText("@WPAGE@");
          OBJ_96.setName("OBJ_96");
          p_tete_droite.add(OBJ_96);
          
          // ---- OBJ_95 ----
          OBJ_95.setText("Page");
          OBJ_95.setName("OBJ_95");
          p_tete_droite.add(OBJ_95);
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);
    
    // ======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());
      
      // ======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== scroll_droite3 ========
          {
            scroll_droite3.setBackground(new Color(238, 239, 241));
            scroll_droite3.setPreferredSize(new Dimension(16, 520));
            scroll_droite3.setBorder(null);
            scroll_droite3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            scroll_droite3.setName("scroll_droite3");
            
            // ======== menus_haut ========
            {
              menus_haut.setMinimumSize(new Dimension(160, 520));
              menus_haut.setPreferredSize(new Dimension(160, 520));
              menus_haut.setBackground(new Color(238, 239, 241));
              menus_haut.setAutoscrolls(true);
              menus_haut.setName("menus_haut");
              menus_haut.setLayout(new VerticalLayout());
              
              // ======== riMenu_V01F ========
              {
                riMenu_V01F.setMinimumSize(new Dimension(104, 50));
                riMenu_V01F.setPreferredSize(new Dimension(170, 50));
                riMenu_V01F.setMaximumSize(new Dimension(104, 50));
                riMenu_V01F.setName("riMenu_V01F");
                
                // ---- riMenu_bt_V01F ----
                riMenu_bt_V01F.setText("@V01F@");
                riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
                riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
                riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
                riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
                riMenu_bt_V01F.setName("riMenu_bt_V01F");
                riMenu_V01F.add(riMenu_bt_V01F);
              }
              menus_haut.add(riMenu_V01F);
              
              // ======== riSousMenu_consult ========
              {
                riSousMenu_consult.setName("riSousMenu_consult");
                
                // ---- riSousMenu_bt_consult ----
                riSousMenu_bt_consult.setText("Consultation");
                riSousMenu_bt_consult.setToolTipText("Consultation");
                riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
                riSousMenu_consult.add(riSousMenu_bt_consult);
              }
              menus_haut.add(riSousMenu_consult);
              
              // ======== riSousMenu_modif ========
              {
                riSousMenu_modif.setName("riSousMenu_modif");
                
                // ---- riSousMenu_bt_modif ----
                riSousMenu_bt_modif.setText("Modification");
                riSousMenu_bt_modif.setToolTipText("Modification");
                riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
                riSousMenu_modif.add(riSousMenu_bt_modif);
              }
              menus_haut.add(riSousMenu_modif);
              
              // ======== riSousMenu_crea ========
              {
                riSousMenu_crea.setName("riSousMenu_crea");
                
                // ---- riSousMenu_bt_crea ----
                riSousMenu_bt_crea.setText("Cr\u00e9ation");
                riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
                riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
                riSousMenu_crea.add(riSousMenu_bt_crea);
              }
              menus_haut.add(riSousMenu_crea);
              
              // ======== riSousMenu_suppr ========
              {
                riSousMenu_suppr.setName("riSousMenu_suppr");
                
                // ---- riSousMenu_bt_suppr ----
                riSousMenu_bt_suppr.setText("Annulation");
                riSousMenu_bt_suppr.setToolTipText("Annulation");
                riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
                riSousMenu_suppr.add(riSousMenu_bt_suppr);
              }
              menus_haut.add(riSousMenu_suppr);
              
              // ======== riSousMenuF_dupli ========
              {
                riSousMenuF_dupli.setName("riSousMenuF_dupli");
                
                // ---- riSousMenu_bt_dupli ----
                riSousMenu_bt_dupli.setText("Duplication");
                riSousMenu_bt_dupli.setToolTipText("Duplication");
                riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
                riSousMenuF_dupli.add(riSousMenu_bt_dupli);
              }
              menus_haut.add(riSousMenuF_dupli);
              
              // ======== riSousMenu_rappel ========
              {
                riSousMenu_rappel.setName("riSousMenu_rappel");
                
                // ---- riSousMenu_bt_rappel ----
                riSousMenu_bt_rappel.setText("Rappel");
                riSousMenu_bt_rappel.setToolTipText("Rappel");
                riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
                riSousMenu_rappel.add(riSousMenu_bt_rappel);
              }
              menus_haut.add(riSousMenu_rappel);
              
              // ======== riSousMenu_reac ========
              {
                riSousMenu_reac.setName("riSousMenu_reac");
                
                // ---- riSousMenu_bt_reac ----
                riSousMenu_bt_reac.setText("R\u00e9activation");
                riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
                riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
                riSousMenu_reac.add(riSousMenu_bt_reac);
              }
              menus_haut.add(riSousMenu_reac);
              
              // ======== riSousMenu_destr ========
              {
                riSousMenu_destr.setName("riSousMenu_destr");
                
                // ---- riSousMenu_bt_destr ----
                riSousMenu_bt_destr.setText("Suppression");
                riSousMenu_bt_destr.setToolTipText("Suppression");
                riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
                riSousMenu_destr.add(riSousMenu_bt_destr);
              }
              menus_haut.add(riSousMenu_destr);
              
              // ======== riMenu2 ========
              {
                riMenu2.setName("riMenu2");
                
                // ---- riMenu_bt2 ----
                riMenu_bt2.setText("Options");
                riMenu_bt2.setName("riMenu_bt2");
                riMenu2.add(riMenu_bt2);
              }
              menus_haut.add(riMenu2);
              
              // ======== riSousMenu6 ========
              {
                riSousMenu6.setName("riSousMenu6");
                
                // ---- riSousMenu_bt6 ----
                riSousMenu_bt6.setText("Tous droits sur tout");
                riSousMenu_bt6.setToolTipText("Tous droits sur tout");
                riSousMenu_bt6.setName("riSousMenu_bt6");
                riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed(e));
                riSousMenu6.add(riSousMenu_bt6);
              }
              menus_haut.add(riSousMenu6);
              
              // ======== riSousMenu7 ========
              {
                riSousMenu7.setName("riSousMenu7");
                
                // ---- riSousMenu_bt7 ----
                riSousMenu_bt7.setText("Valider les modifications");
                riSousMenu_bt7.setToolTipText("Valider les modifications");
                riSousMenu_bt7.setName("riSousMenu_bt7");
                riSousMenu_bt7.addActionListener(e -> riSousMenu_bt7ActionPerformed(e));
                riSousMenu7.add(riSousMenu_bt7);
              }
              menus_haut.add(riSousMenu7);
            }
            scroll_droite3.setViewportView(menus_haut);
          }
          scroll_droite.setViewportView(scroll_droite3);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(1000, 600));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);
        
        // ======== pnlBas ========
        {
          pnlBas.setOpaque(false);
          pnlBas.setName("pnlBas");
          pnlBas.setLayout(null);
          
          // ---- V06F ----
          V06F.setComponentPopupMenu(BTD);
          V06F.setName("V06F");
          pnlBas.add(V06F);
          V06F.setBounds(220, 0, 25, V06F.getPreferredSize().height);
          
          // ---- V06F1 ----
          V06F1.setComponentPopupMenu(BTD);
          V06F1.setName("V06F1");
          pnlBas.add(V06F1);
          V06F1.setBounds(220, 0, 25, V06F1.getPreferredSize().height);
          
          // ---- OBJ_79 ----
          OBJ_79.setText("Aller \u00e0 la page");
          OBJ_79.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_79.setName("OBJ_79");
          OBJ_79.addActionListener(e -> OBJ_69ActionPerformed(e));
          pnlBas.add(OBJ_79);
          OBJ_79.setBounds(new Rectangle(new Point(75, 0), OBJ_79.getPreferredSize()));
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < pnlBas.getComponentCount(); i++) {
              Rectangle bounds = pnlBas.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlBas.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlBas.setMinimumSize(preferredSize);
            pnlBas.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(pnlBas);
        pnlBas.setBounds(732, 560, 267, 34);
        
        // ======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Objet de la s\u00e9curit\u00e9 (GMM)");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);
          
          // ======== pnlGauche ========
          {
            pnlGauche.setBorder(new TitledBorder("Objet de la s\u00e9curit\u00e9 (G.M.M)"));
            pnlGauche.setOpaque(false);
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(null);
            
            // ---- OBJ_97 ----
            OBJ_97.setText("132");
            OBJ_97.setFont(OBJ_97.getFont().deriveFont(OBJ_97.getFont().getStyle() | Font.BOLD));
            OBJ_97.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_97.setName("OBJ_97");
            pnlGauche.add(OBJ_97);
            OBJ_97.setBounds(10, 71, 21, 16);
            
            // ---- OBJ_123 ----
            OBJ_123.setText("133");
            OBJ_123.setFont(OBJ_123.getFont().deriveFont(OBJ_123.getFont().getStyle() | Font.BOLD));
            OBJ_123.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_123.setName("OBJ_123");
            pnlGauche.add(OBJ_123);
            OBJ_123.setBounds(10, 102, 21, 16);
            
            // ---- OBJ_124 ----
            OBJ_124.setText("134");
            OBJ_124.setFont(OBJ_124.getFont().deriveFont(OBJ_124.getFont().getStyle() | Font.BOLD));
            OBJ_124.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_124.setName("OBJ_124");
            pnlGauche.add(OBJ_124);
            OBJ_124.setBounds(10, 133, 21, 16);
            
            // ---- OBJ_186 ----
            OBJ_186.setText("135");
            OBJ_186.setFont(OBJ_186.getFont().deriveFont(OBJ_186.getFont().getStyle() | Font.BOLD));
            OBJ_186.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_186.setName("OBJ_186");
            pnlGauche.add(OBJ_186);
            OBJ_186.setBounds(10, 164, 21, 16);
            
            // ---- OBJ_189 ----
            OBJ_189.setText("136");
            OBJ_189.setFont(OBJ_189.getFont().deriveFont(OBJ_189.getFont().getStyle() | Font.BOLD));
            OBJ_189.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_189.setName("OBJ_189");
            pnlGauche.add(OBJ_189);
            OBJ_189.setBounds(10, 195, 21, 16);
            
            // ---- OBJ_193 ----
            OBJ_193.setText("137");
            OBJ_193.setFont(OBJ_193.getFont().deriveFont(OBJ_193.getFont().getStyle() | Font.BOLD));
            OBJ_193.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_193.setName("OBJ_193");
            pnlGauche.add(OBJ_193);
            OBJ_193.setBounds(10, 226, 21, 16);
            
            // ---- OBJ_294 ----
            OBJ_294.setText("138");
            OBJ_294.setFont(OBJ_294.getFont().deriveFont(OBJ_294.getFont().getStyle() | Font.BOLD));
            OBJ_294.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_294.setName("OBJ_294");
            pnlGauche.add(OBJ_294);
            OBJ_294.setBounds(10, 257, 21, 16);
            
            // ---- OBJ_195 ----
            OBJ_195.setText("158");
            OBJ_195.setFont(OBJ_195.getFont().deriveFont(OBJ_195.getFont().getStyle() | Font.BOLD));
            OBJ_195.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_195.setName("OBJ_195");
            pnlGauche.add(OBJ_195);
            OBJ_195.setBounds(10, 288, 21, 16);
            
            // ---- OBJ_196 ----
            OBJ_196.setText("163");
            OBJ_196.setFont(OBJ_196.getFont().deriveFont(OBJ_196.getFont().getStyle() | Font.BOLD));
            OBJ_196.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_196.setName("OBJ_196");
            pnlGauche.add(OBJ_196);
            OBJ_196.setBounds(10, 319, 21, 16);
            
            // ---- OBJ_101 ----
            OBJ_101.setText("131");
            OBJ_101.setFont(OBJ_101.getFont().deriveFont(OBJ_101.getFont().getStyle() | Font.BOLD));
            OBJ_101.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_101.setName("OBJ_101");
            pnlGauche.add(OBJ_101);
            OBJ_101.setBounds(10, 40, 21, 16);
            
            // ---- SET132 ----
            SET132.setToolTipText("Valeur entre 0 et 9");
            SET132.setComponentPopupMenu(BTD);
            SET132.setText("Liste des Contrats");
            SET132.setName("SET132");
            pnlGauche.add(SET132);
            SET132.setBounds(40, 70, 405, SET132.getPreferredSize().height);
            
            // ---- SET134 ----
            SET134.setToolTipText("Valeur entre 0 et 9");
            SET134.setComponentPopupMenu(BTD);
            SET134.setText("Liste du suivis mat\u00e9riels");
            SET134.setName("SET134");
            pnlGauche.add(SET134);
            SET134.setBounds(40, 132, 405, SET134.getPreferredSize().height);
            
            // ---- SET137 ----
            SET137.setToolTipText("Valeur entre 0 et 9");
            SET137.setComponentPopupMenu(BTD);
            SET137.setText("Planning des entretiens");
            SET137.setName("SET137");
            pnlGauche.add(SET137);
            SET137.setBounds(40, 225, 405, SET137.getPreferredSize().height);
            
            // ---- SET138 ----
            SET138.setToolTipText("Valeur entre 0 et 9");
            SET138.setComponentPopupMenu(BTD);
            SET138.setText("Purge des fiches de r\u00e9paration");
            SET138.setName("SET138");
            pnlGauche.add(SET138);
            SET138.setBounds(40, 256, 405, SET138.getPreferredSize().height);
            
            // ---- SET158 ----
            SET158.setToolTipText("Valeur entre 0 et 9");
            SET158.setComponentPopupMenu(BTD);
            SET158.setText("Autorisation de la facturation des interventions");
            SET158.setName("SET158");
            pnlGauche.add(SET158);
            SET158.setBounds(40, 287, 405, SET158.getPreferredSize().height);
            
            // ---- SET163 ----
            SET163.setToolTipText("Valeur entre 0 et 9");
            SET163.setComponentPopupMenu(BTD);
            SET163.setText("Autorisation d'annulation d'intervention");
            SET163.setName("SET163");
            pnlGauche.add(SET163);
            SET163.setBounds(40, 318, 405, SET163.getPreferredSize().height);
            
            // ---- SET131 ----
            SET131.setComponentPopupMenu(BTD);
            SET131.setModel(new DefaultComboBoxModel<>(
                new String[] { " ", "Tous les droits", "consultation et modification", "consultation seulement", "Aucun droit" }));
            SET131.setName("SET131");
            pnlGauche.add(SET131);
            SET131.setBounds(40, 34, 210, SET131.getPreferredSize().height);
            
            // ---- OBJ_200 ----
            OBJ_200.setText("Gestion des contrats");
            OBJ_200.setName("OBJ_200");
            pnlGauche.add(OBJ_200);
            OBJ_200.setBounds(255, 34, 205, 26);
            
            // ---- SET133 ----
            SET133.setComponentPopupMenu(BTD);
            SET133.setModel(new DefaultComboBoxModel<>(
                new String[] { " ", "Tous les droits", "consultation et modification", "consultation seulement", "Aucun droit" }));
            SET133.setName("SET133");
            pnlGauche.add(SET133);
            SET133.setBounds(40, 96, 210, SET133.getPreferredSize().height);
            
            // ---- SET135 ----
            SET135.setComponentPopupMenu(BTD);
            SET135.setModel(new DefaultComboBoxModel<>(
                new String[] { " ", "Tous les droits", "consultation et modification", "consultation seulement", "Aucun droit" }));
            SET135.setName("SET135");
            pnlGauche.add(SET135);
            SET135.setBounds(40, 158, 210, SET135.getPreferredSize().height);
            
            // ---- SET136 ----
            SET136.setToolTipText("Valeur entre 0 et 9");
            SET136.setComponentPopupMenu(BTD);
            SET136.setText("Liste des interventions");
            SET136.setName("SET136");
            pnlGauche.add(SET136);
            SET136.setBounds(40, 194, 405, SET136.getPreferredSize().height);
            
            // ---- OBJ_201 ----
            OBJ_201.setText("Gestion du suivis mat\u00e9riels");
            OBJ_201.setName("OBJ_201");
            pnlGauche.add(OBJ_201);
            OBJ_201.setBounds(255, 96, 205, 26);
            
            // ---- OBJ_202 ----
            OBJ_202.setText("Gestion des interventions");
            OBJ_202.setName("OBJ_202");
            pnlGauche.add(OBJ_202);
            OBJ_202.setBounds(255, 158, 205, 26);
          }
          xTitledPanel1ContentContainer.add(pnlGauche);
          pnlGauche.setBounds(10, 55, 470, 450);
          
          // ======== pnlDroite ========
          {
            pnlDroite.setBorder(new TitledBorder("Objet de la s\u00e9curit\u00e9 pour le comptoir"));
            pnlDroite.setOpaque(false);
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(null);
            
            // ---- OBJ_198 ----
            OBJ_198.setText("178");
            OBJ_198.setFont(OBJ_198.getFont().deriveFont(OBJ_198.getFont().getStyle() | Font.BOLD));
            OBJ_198.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_198.setName("OBJ_198");
            pnlDroite.add(OBJ_198);
            OBJ_198.setBounds(15, 40, 21, 16);
            
            // ---- SET178 ----
            SET178.setToolTipText("Valeur entre 0 et 9");
            SET178.setComponentPopupMenu(BTD);
            SET178.setText("Modification type de facturation");
            SET178.setName("SET178");
            pnlDroite.add(SET178);
            SET178.setBounds(45, 40, 405, SET178.getPreferredSize().height);
            
            // ---- OBJ_157 ----
            OBJ_157.setText("180");
            OBJ_157.setFont(OBJ_157.getFont().deriveFont(OBJ_157.getFont().getStyle() | Font.BOLD));
            OBJ_157.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_157.setName("OBJ_157");
            pnlDroite.add(OBJ_157);
            OBJ_157.setBounds(7, 62, 29, 22);
            
            // ---- SET180 ----
            SET180.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET180.setComponentPopupMenu(BTD);
            SET180.setText("Modification du repr\u00e9sentant");
            SET180.setName("SET180");
            pnlDroite.add(SET180);
            SET180.setBounds(47, 65, 405, SET180.getPreferredSize().height);
          }
          xTitledPanel1ContentContainer.add(pnlDroite);
          pnlDroite.setBounds(490, 55, 470, 450);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(15, 10, 974, 550);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(e -> OBJ_16ActionPerformed(e));
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_34;
  private RiZoneSortie INDUSR;
  private JLabel OBJ_36;
  private RiZoneSortie INDETB;
  private RiZoneSortie OBJ_40;
  private JPanel p_tete_droite;
  private JLabel OBJ_96;
  private JLabel OBJ_95;
  private SNPanelFond pnlSud;
  private JPanel pnlMenus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JScrollPane scroll_droite3;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlBas;
  private XRiTextField V06F;
  private XRiTextField V06F1;
  private SNBoutonLeger OBJ_79;
  private JXTitledPanel xTitledPanel1;
  private SNPanel pnlGauche;
  private JLabel OBJ_97;
  private JLabel OBJ_123;
  private JLabel OBJ_124;
  private JLabel OBJ_186;
  private JLabel OBJ_189;
  private JLabel OBJ_193;
  private JLabel OBJ_294;
  private JLabel OBJ_195;
  private JLabel OBJ_196;
  private JLabel OBJ_101;
  private XRiCheckBox SET132;
  private XRiCheckBox SET134;
  private XRiCheckBox SET137;
  private XRiCheckBox SET138;
  private XRiCheckBox SET158;
  private XRiCheckBox SET163;
  private XRiComboBox SET131;
  private JLabel OBJ_200;
  private XRiComboBox SET133;
  private XRiComboBox SET135;
  private XRiCheckBox SET136;
  private JLabel OBJ_201;
  private JLabel OBJ_202;
  private SNPanel pnlDroite;
  private JLabel OBJ_198;
  private XRiCheckBox SET178;
  private JLabel OBJ_157;
  private XRiCheckBox SET180;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
