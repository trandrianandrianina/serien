
package ri.serien.libecranrpg.vgvx.VGVX14FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX14FX_AD extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] E1MA21_Value = { " ", "V", "%" };
  private String[] E1MA22_Value = { " ", "P", "R", "E" };
  
  public VGVX14FX_AD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut (Ne pas réactiver car soucis à cause du choix multiple ENTER ou F3)
    // setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    E1MA21.setValeurs(E1MA21_Value, null);
    E1MA22.setValeurs(E1MA22_Value, null);
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Code établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "E1ETB");
    snEtablissement.setEnabled(false);
    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "E1ETB");
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    sNPanel1 = new SNPanel();
    lbE1ETB = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbNumero = new SNLabelChamp();
    WNUM = new XRiTextField();
    lbDateTraitement = new SNLabelChamp();
    WDATEX = new XRiCalendrier();
    sNPanelTitre1 = new SNPanelTitre();
    lbType = new SNLabelChamp();
    E1MA21 = new XRiComboBox();
    lbPrix = new SNLabelChamp();
    E1MA22 = new XRiComboBox();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Depr\u00e9ciation de stock");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

      //======== sNPanel1 ========
      {
        sNPanel1.setName("sNPanel1");
        sNPanel1.setLayout(new GridBagLayout());
        ((GridBagLayout)sNPanel1.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)sNPanel1.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)sNPanel1.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)sNPanel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //---- lbE1ETB ----
        lbE1ETB.setText("Etablissement");
        lbE1ETB.setMaximumSize(new Dimension(200, 30));
        lbE1ETB.setMinimumSize(new Dimension(200, 30));
        lbE1ETB.setPreferredSize(new Dimension(200, 30));
        lbE1ETB.setInheritsPopupMenu(false);
        lbE1ETB.setName("lbE1ETB");
        sNPanel1.add(lbE1ETB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snEtablissement ----
        snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
        snEtablissement.setEnabled(false);
        snEtablissement.setName("snEtablissement");
        sNPanel1.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbNumero ----
        lbNumero.setText("Num\u00e9ro");
        lbNumero.setMaximumSize(new Dimension(200, 30));
        lbNumero.setMinimumSize(new Dimension(200, 30));
        lbNumero.setPreferredSize(new Dimension(200, 30));
        lbNumero.setInheritsPopupMenu(false);
        lbNumero.setName("lbNumero");
        sNPanel1.add(lbNumero, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WNUM ----
        WNUM.setComponentPopupMenu(null);
        WNUM.setMinimumSize(new Dimension(70, 30));
        WNUM.setMaximumSize(new Dimension(70, 30));
        WNUM.setPreferredSize(new Dimension(70, 30));
        WNUM.setFont(new Font("sansserif", Font.PLAIN, 14));
        WNUM.setEnabled(false);
        WNUM.setName("WNUM");
        sNPanel1.add(WNUM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbDateTraitement ----
        lbDateTraitement.setText("Date de traitement");
        lbDateTraitement.setMaximumSize(new Dimension(200, 30));
        lbDateTraitement.setMinimumSize(new Dimension(200, 30));
        lbDateTraitement.setPreferredSize(new Dimension(200, 30));
        lbDateTraitement.setInheritsPopupMenu(false);
        lbDateTraitement.setName("lbDateTraitement");
        sNPanel1.add(lbDateTraitement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WDATEX ----
        WDATEX.setComponentPopupMenu(null);
        WDATEX.setMaximumSize(new Dimension(110, 30));
        WDATEX.setMinimumSize(new Dimension(110, 30));
        WDATEX.setPreferredSize(new Dimension(110, 30));
        WDATEX.setFont(new Font("sansserif", Font.PLAIN, 14));
        WDATEX.setEnabled(false);
        WDATEX.setName("WDATEX");
        sNPanel1.add(WDATEX, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(sNPanel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== sNPanelTitre1 ========
      {
        sNPanelTitre1.setTitre("Crit\u00e8res de cr\u00e9ation");
        sNPanelTitre1.setName("sNPanelTitre1");
        sNPanelTitre1.setLayout(new GridBagLayout());
        ((GridBagLayout)sNPanelTitre1.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)sNPanelTitre1.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)sNPanelTitre1.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)sNPanelTitre1.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbType ----
        lbType.setText("Type");
        lbType.setName("lbType");
        sNPanelTitre1.add(lbType, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- E1MA21 ----
        E1MA21.setModel(new DefaultComboBoxModel(new String[] {
          " ",
          "D\u00e9pr\u00e9ciation en Valeur sur le prix choisi",
          "D\u00e9pr\u00e9ciation en Pourcentage"
        }));
        E1MA21.setBackground(Color.white);
        E1MA21.setFont(new Font("sansserif", Font.PLAIN, 14));
        E1MA21.setName("E1MA21");
        sNPanelTitre1.add(E1MA21, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbPrix ----
        lbPrix.setText("Prix");
        lbPrix.setName("lbPrix");
        sNPanelTitre1.add(lbPrix, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- E1MA22 ----
        E1MA22.setModel(new DefaultComboBoxModel(new String[] {
          " ",
          "Prix Unitaire Moyen Pond\u00e9r\u00e9",
          "Prix de Revient",
          "Prix de derni\u00e8re Entr\u00e9e"
        }));
        E1MA22.setBackground(Color.white);
        E1MA22.setFont(new Font("sansserif", Font.PLAIN, 14));
        E1MA22.setName("E1MA22");
        sNPanelTitre1.add(E1MA22, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(sNPanelTitre1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel sNPanel1;
  private SNLabelChamp lbE1ETB;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbNumero;
  private XRiTextField WNUM;
  private SNLabelChamp lbDateTraitement;
  private XRiCalendrier WDATEX;
  private SNPanelTitre sNPanelTitre1;
  private SNLabelChamp lbType;
  private XRiComboBox E1MA21;
  private SNLabelChamp lbPrix;
  private XRiComboBox E1MA22;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
