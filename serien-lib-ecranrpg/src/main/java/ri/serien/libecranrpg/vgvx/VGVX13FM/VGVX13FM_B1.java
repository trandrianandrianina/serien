
package ri.serien.libecranrpg.vgvx.VGVX13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.Borders;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX13FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVX13FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1ART@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAGA@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FOUR@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBORI@")).trim());
    H1NUM0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H1NUM0@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEB@")).trim());
    H1NLI0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H1NLI0@")).trim());
    OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H1MAG2@")).trim());
    H1SUF0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H1SUF0@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H1LB2X@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H1LB1X@")).trim());
    OBJ_68.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PRIX2@")).trim());
    OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IRPRX@")).trim());
    HFIDA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HFIDA@")).trim());
    L23AUT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L23AUT@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H1ORD@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H1OPE@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBTYP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    H1TI1.setVisible(lexique.isPresent("H1TI1"));
    H1SUF0.setVisible(lexique.isPresent("H1SUF0"));
    OBJ_34.setVisible(lexique.isPresent("H1MAG2"));
    H1TI3.setVisible(lexique.isPresent("H1TI3"));
    OBJ_26.setVisible(lexique.isPresent("H1ORD"));
    H1NLI0.setVisible(lexique.isPresent("H1NLI0"));
    OBJ_32.setVisible(lexique.isPresent("WEB"));
    H1NUM0.setVisible(lexique.isPresent("H1NUM0"));
    H1TI2.setVisible(lexique.isPresent("H1TI2"));
    H1TI22.setVisible(lexique.isPresent("H1TI22"));
    OBJ_43.setVisible(lexique.isPresent("LBORI"));
    OBJ_67.setVisible(lexique.isPresent("IRPRX"));
    H1PRD.setVisible(lexique.isPresent("H1PRD"));
    H1PRX.setVisible(lexique.isPresent("H1PRX"));
    H1PRV.setVisible(lexique.isPresent("H1PRV"));
    H1PMP.setVisible(lexique.isPresent("H1PMP"));
    H1QT4X.setVisible(lexique.isPresent("H1QT4X"));
    H1QT3X.setVisible(lexique.isPresent("H1QT3X"));
    H1QT2X.setVisible(lexique.isPresent("H1QT2X"));
    H1QT1X.setVisible(lexique.isPresent("H1QT1X"));
    OBJ_35.setVisible(lexique.HostFieldGetData("MODIF").equalsIgnoreCase("1") & lexique.isPresent("MODIF"));
    OBJ_29.setVisible(lexique.isPresent("LBTYP"));
    OBJ_49.setVisible(lexique.isPresent("H1LB1X"));
    OBJ_50.setVisible(lexique.isPresent("H1LB2X"));
    OBJ_31.setVisible(lexique.isPresent("L1ART"));
    OBJ_62.setVisible(lexique.isPresent("H1PRD"));
    OBJ_51.setVisible(lexique.isPresent("H1QT1X"));
    OBJ_59.setVisible(lexique.isPresent("H1PMP"));
    OBJ_60.setVisible(lexique.isPresent("H1PRV"));
    
    if (lexique.isTrue("(14) AND (N15)")) {
      OBJ_46.setText("Client");
      H1TI22.setVisible(true);
      H1TI3.setVisible(true);
      H1TI2.setVisible(false);
      H1TI1.setVisible(false);
    }
    
    if (lexique.isTrue("(14) AND (15)")) {
      OBJ_46.setText("Fournisseur");
      H1TI22.setVisible(false);
      H1TI3.setVisible(false);
      H1TI2.setVisible(true);
      H1TI1.setVisible(true);
    }
    
    // TODO Icones
    OBJ_35.setIcon(lexique.chargerImage("images/avert.gif", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("DETAIL DU MOUVEMENT"));
    
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 65);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Scr
    lexique.HostScreenSendKey(this, "F20", true);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", true);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", true);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_31 = new RiZoneSortie();
    OBJ_33 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_43 = new RiZoneSortie();
    OBJ_44 = new JLabel();
    H1NUM0 = new JLabel();
    OBJ_32 = new JLabel();
    H1NLI0 = new JLabel();
    H1TI3 = new XRiTextField();
    OBJ_34 = new JLabel();
    OBJ_45 = new JLabel();
    H1SUF0 = new JLabel();
    H1TI1 = new XRiTextField();
    panel1 = new JPanel();
    OBJ_50 = new RiZoneSortie();
    OBJ_49 = new RiZoneSortie();
    OBJ_68 = new JLabel();
    H1QT1X = new XRiTextField();
    H1QT2X = new XRiTextField();
    H1QT3X = new XRiTextField();
    H1QT4X = new XRiTextField();
    OBJ_54 = new JLabel();
    OBJ_61 = new JLabel();
    H1PMP = new XRiTextField();
    H1PRV = new XRiTextField();
    H1PRX = new XRiTextField();
    H1PRD = new XRiTextField();
    OBJ_67 = new RiZoneSortie();
    OBJ_62 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_35 = new JButton();
    H1TI22 = new XRiTextField();
    H1TI2 = new XRiTextField();
    label1 = new JLabel();
    OBJ_55 = new JLabel();
    HFIDA = new RiZoneSortie();
    OBJ_57 = new JLabel();
    L23AUT = new RiZoneSortie();
    barre_tete = new JMenuBar();
    panel2 = new JPanel();
    OBJ_23 = new JLabel();
    H1DATX = new XRiCalendrier();
    OBJ_25 = new JLabel();
    OBJ_26 = new RiZoneSortie();
    OBJ_27 = new JLabel();
    OBJ_30 = new RiZoneSortie();
    OBJ_29 = new RiZoneSortie();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(850, 390));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Origine du mouvement");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ---- OBJ_31 ----
        OBJ_31.setText("@L1ART@");
        OBJ_31.setName("OBJ_31");
        p_contenu.add(OBJ_31);
        OBJ_31.setBounds(15, 15, 345, 28);
        
        // ---- OBJ_33 ----
        OBJ_33.setText("@MAGA@");
        OBJ_33.setName("OBJ_33");
        p_contenu.add(OBJ_33);
        OBJ_33.setBounds(445, 21, 111, 16);
        
        // ---- OBJ_46 ----
        OBJ_46.setText("@FOUR@");
        OBJ_46.setName("OBJ_46");
        p_contenu.add(OBJ_46);
        OBJ_46.setBounds(415, 55, 85, 16);
        
        // ---- OBJ_43 ----
        OBJ_43.setText("@LBORI@");
        OBJ_43.setName("OBJ_43");
        p_contenu.add(OBJ_43);
        OBJ_43.setBounds(15, 49, 85, 28);
        
        // ---- OBJ_44 ----
        OBJ_44.setText("Bon N\u00b0");
        OBJ_44.setName("OBJ_44");
        p_contenu.add(OBJ_44);
        OBJ_44.setBounds(140, 55, 47, 16);
        
        // ---- H1NUM0 ----
        H1NUM0.setComponentPopupMenu(BTD);
        H1NUM0.setText("@H1NUM0@");
        H1NUM0.setHorizontalAlignment(SwingConstants.RIGHT);
        H1NUM0.setName("H1NUM0");
        p_contenu.add(H1NUM0);
        H1NUM0.setBounds(190, 49, 58, 28);
        
        // ---- OBJ_32 ----
        OBJ_32.setText("@WEB@");
        OBJ_32.setName("OBJ_32");
        p_contenu.add(OBJ_32);
        OBJ_32.setBounds(390, 19, 33, 20);
        
        // ---- H1NLI0 ----
        H1NLI0.setComponentPopupMenu(BTD);
        H1NLI0.setText("@H1NLI0@");
        H1NLI0.setName("H1NLI0");
        p_contenu.add(H1NLI0);
        H1NLI0.setBounds(320, 49, 34, 28);
        
        // ---- H1TI3 ----
        H1TI3.setComponentPopupMenu(BTD);
        H1TI3.setName("H1TI3");
        p_contenu.add(H1TI3);
        H1TI3.setBounds(562, 49, 34, H1TI3.getPreferredSize().height);
        
        // ---- OBJ_34 ----
        OBJ_34.setText("@H1MAG2@");
        OBJ_34.setName("OBJ_34");
        p_contenu.add(OBJ_34);
        OBJ_34.setBounds(575, 21, 21, 16);
        
        // ---- OBJ_45 ----
        OBJ_45.setText("/");
        OBJ_45.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_45.setName("OBJ_45");
        p_contenu.add(OBJ_45);
        OBJ_45.setBounds(245, 55, 21, 16);
        
        // ---- H1SUF0 ----
        H1SUF0.setComponentPopupMenu(BTD);
        H1SUF0.setText("@H1SUF0@");
        H1SUF0.setName("H1SUF0");
        p_contenu.add(H1SUF0);
        H1SUF0.setBounds(265, 49, 18, 28);
        
        // ---- H1TI1 ----
        H1TI1.setComponentPopupMenu(BTD);
        H1TI1.setName("H1TI1");
        p_contenu.add(H1TI1);
        H1TI1.setBounds(500, 49, 18, H1TI1.getPreferredSize().height);
        
        // ======== panel1 ========
        {
          panel1.setBorder(new CompoundBorder(new TitledBorder(""), Borders.DLU2_BORDER));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- OBJ_50 ----
          OBJ_50.setText("@H1LB2X@");
          OBJ_50.setName("OBJ_50");
          panel1.add(OBJ_50);
          OBJ_50.setBounds(340, 20, 291, OBJ_50.getPreferredSize().height);
          
          // ---- OBJ_49 ----
          OBJ_49.setText("@H1LB1X@");
          OBJ_49.setName("OBJ_49");
          panel1.add(OBJ_49);
          OBJ_49.setBounds(20, 20, 282, OBJ_49.getPreferredSize().height);
          
          // ---- OBJ_68 ----
          OBJ_68.setText("@PRIX2@");
          OBJ_68.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_68.setName("OBJ_68");
          panel1.add(OBJ_68);
          OBJ_68.setBounds(497, 171, 137, 16);
          
          // ---- H1QT1X ----
          H1QT1X.setComponentPopupMenu(BTD);
          H1QT1X.setHorizontalAlignment(SwingConstants.RIGHT);
          H1QT1X.setName("H1QT1X");
          panel1.add(H1QT1X);
          H1QT1X.setBounds(20, 80, 84, H1QT1X.getPreferredSize().height);
          
          // ---- H1QT2X ----
          H1QT2X.setComponentPopupMenu(BTD);
          H1QT2X.setHorizontalAlignment(SwingConstants.RIGHT);
          H1QT2X.setName("H1QT2X");
          panel1.add(H1QT2X);
          H1QT2X.setBounds(195, 80, 84, H1QT2X.getPreferredSize().height);
          
          // ---- H1QT3X ----
          H1QT3X.setComponentPopupMenu(BTD);
          H1QT3X.setHorizontalAlignment(SwingConstants.RIGHT);
          H1QT3X.setName("H1QT3X");
          panel1.add(H1QT3X);
          H1QT3X.setBounds(375, 80, 84, H1QT3X.getPreferredSize().height);
          
          // ---- H1QT4X ----
          H1QT4X.setComponentPopupMenu(BTD);
          H1QT4X.setHorizontalAlignment(SwingConstants.RIGHT);
          H1QT4X.setName("H1QT4X");
          panel1.add(H1QT4X);
          H1QT4X.setBounds(550, 80, 84, H1QT4X.getPreferredSize().height);
          
          // ---- OBJ_54 ----
          OBJ_54.setText("Physique");
          OBJ_54.setHorizontalAlignment(SwingConstants.LEFT);
          OBJ_54.setName("OBJ_54");
          panel1.add(OBJ_54);
          OBJ_54.setBounds(550, 60, 93, 21);
          
          // ---- OBJ_61 ----
          OBJ_61.setText("Prix unitaire");
          OBJ_61.setName("OBJ_61");
          panel1.add(OBJ_61);
          OBJ_61.setBounds(375, 115, 90, 21);
          
          // ---- H1PMP ----
          H1PMP.setComponentPopupMenu(BTD);
          H1PMP.setHorizontalAlignment(SwingConstants.RIGHT);
          H1PMP.setName("H1PMP");
          panel1.add(H1PMP);
          H1PMP.setBounds(20, 135, 84, H1PMP.getPreferredSize().height);
          
          // ---- H1PRV ----
          H1PRV.setComponentPopupMenu(BTD);
          H1PRV.setHorizontalAlignment(SwingConstants.RIGHT);
          H1PRV.setName("H1PRV");
          panel1.add(H1PRV);
          H1PRV.setBounds(195, 135, 84, H1PRV.getPreferredSize().height);
          
          // ---- H1PRX ----
          H1PRX.setComponentPopupMenu(BTD);
          H1PRX.setHorizontalAlignment(SwingConstants.RIGHT);
          H1PRX.setName("H1PRX");
          panel1.add(H1PRX);
          H1PRX.setBounds(375, 135, 84, H1PRX.getPreferredSize().height);
          
          // ---- H1PRD ----
          H1PRD.setComponentPopupMenu(BTD);
          H1PRD.setHorizontalAlignment(SwingConstants.RIGHT);
          H1PRD.setName("H1PRD");
          panel1.add(H1PRD);
          H1PRD.setBounds(550, 135, 84, H1PRD.getPreferredSize().height);
          
          // ---- OBJ_67 ----
          OBJ_67.setText("@IRPRX@");
          OBJ_67.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_67.setName("OBJ_67");
          panel1.add(OBJ_67);
          OBJ_67.setBounds(375, 165, 84, 28);
          
          // ---- OBJ_62 ----
          OBJ_62.setText("En devises");
          OBJ_62.setName("OBJ_62");
          panel1.add(OBJ_62);
          OBJ_62.setBounds(550, 115, 93, 21);
          
          // ---- OBJ_59 ----
          OBJ_59.setText("P.U.M.P.");
          OBJ_59.setName("OBJ_59");
          panel1.add(OBJ_59);
          OBJ_59.setBounds(20, 115, 65, 21);
          
          // ---- OBJ_60 ----
          OBJ_60.setText("P.R.V.");
          OBJ_60.setName("OBJ_60");
          panel1.add(OBJ_60);
          OBJ_60.setBounds(195, 115, 65, 21);
          
          // ---- OBJ_51 ----
          OBJ_51.setText("Entr\u00e9es");
          OBJ_51.setName("OBJ_51");
          panel1.add(OBJ_51);
          OBJ_51.setBounds(20, 60, 65, 21);
          
          // ---- OBJ_52 ----
          OBJ_52.setText("Sorties");
          OBJ_52.setName("OBJ_52");
          panel1.add(OBJ_52);
          OBJ_52.setBounds(195, 60, 65, 21);
          
          // ---- OBJ_53 ----
          OBJ_53.setText("Divers");
          OBJ_53.setName("OBJ_53");
          panel1.add(OBJ_53);
          OBJ_53.setBounds(375, 55, 90, 21);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 135, 655, 210);
        
        // ---- OBJ_35 ----
        OBJ_35.setText("");
        OBJ_35.setToolTipText("Mouvement modifi\u00e9");
        OBJ_35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_35.setName("OBJ_35");
        OBJ_35.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_35ActionPerformed(e);
          }
        });
        p_contenu.add(OBJ_35);
        OBJ_35.setBounds(620, 30, 44, 39);
        
        // ---- H1TI22 ----
        H1TI22.setComponentPopupMenu(BTD);
        H1TI22.setName("H1TI22");
        p_contenu.add(H1TI22);
        H1TI22.setBounds(500, 49, 58, H1TI22.getPreferredSize().height);
        
        // ---- H1TI2 ----
        H1TI2.setComponentPopupMenu(BTD);
        H1TI2.setName("H1TI2");
        p_contenu.add(H1TI2);
        H1TI2.setBounds(535, 49, 58, H1TI2.getPreferredSize().height);
        
        // ---- label1 ----
        label1.setText("ligne");
        label1.setName("label1");
        p_contenu.add(label1);
        label1.setBounds(285, 53, 35, 21);
        
        // ---- OBJ_55 ----
        OBJ_55.setText("Affaire");
        OBJ_55.setName("OBJ_55");
        p_contenu.add(OBJ_55);
        OBJ_55.setBounds(15, 85, 115, 21);
        
        // ---- HFIDA ----
        HFIDA.setText("@HFIDA@");
        HFIDA.setName("HFIDA");
        p_contenu.add(HFIDA);
        HFIDA.setBounds(140, 83, 160, HFIDA.getPreferredSize().height);
        
        // ---- OBJ_57 ----
        OBJ_57.setText("Autorisation");
        OBJ_57.setName("OBJ_57");
        p_contenu.add(OBJ_57);
        OBJ_57.setBounds(415, 85, 80, 21);
        
        // ---- L23AUT ----
        L23AUT.setText("@L23AUT@");
        L23AUT.setName("L23AUT");
        p_contenu.add(L23AUT);
        L23AUT.setBounds(500, 83, 90, L23AUT.getPreferredSize().height);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");
      
      // ======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setName("panel2");
        panel2.setLayout(null);
        
        // ---- OBJ_23 ----
        OBJ_23.setText("D\u00e9tail du mouvement du");
        OBJ_23.setName("OBJ_23");
        panel2.add(OBJ_23);
        OBJ_23.setBounds(10, 3, 145, 21);
        
        // ---- H1DATX ----
        H1DATX.setOpaque(false);
        H1DATX.setName("H1DATX");
        panel2.add(H1DATX);
        H1DATX.setBounds(155, 1, 105, 24);
        
        // ---- OBJ_25 ----
        OBJ_25.setText("N\u00b0 d'ordre");
        OBJ_25.setName("OBJ_25");
        panel2.add(OBJ_25);
        OBJ_25.setBounds(280, 3, 69, 21);
        
        // ---- OBJ_26 ----
        OBJ_26.setText("@H1ORD@");
        OBJ_26.setOpaque(false);
        OBJ_26.setHorizontalAlignment(SwingConstants.RIGHT);
        OBJ_26.setName("OBJ_26");
        panel2.add(OBJ_26);
        OBJ_26.setBounds(350, 3, 77, 20);
        
        // ---- OBJ_27 ----
        OBJ_27.setText("Type");
        OBJ_27.setName("OBJ_27");
        panel2.add(OBJ_27);
        OBJ_27.setBounds(440, 3, 35, 21);
        
        // ---- OBJ_30 ----
        OBJ_30.setText("@H1OPE@");
        OBJ_30.setOpaque(false);
        OBJ_30.setName("OBJ_30");
        panel2.add(OBJ_30);
        OBJ_30.setBounds(477, 3, 25, 20);
        
        // ---- OBJ_29 ----
        OBJ_29.setText("@LBTYP@");
        OBJ_29.setOpaque(false);
        OBJ_29.setName("OBJ_29");
        panel2.add(OBJ_29);
        OBJ_29.setBounds(508, 3, 71, 20);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(panel2);
    }
    add(barre_tete, BorderLayout.NORTH);
    
    // ======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");
      
      // ---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);
      
      // ---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);
      
      // ---- OBJ_8 ----
      OBJ_8.setText("Recherche multi-crit\u00e8res");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
      
      // ---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
      
      // ---- OBJ_10 ----
      OBJ_10.setText("Modification prix mouvement");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Affichage R\u00e9capitulatif Mouvements entre Inventaires");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_12);
      
      // ---- OBJ_13 ----
      OBJ_13.setText("Affichage Historique Achats ou PUMP");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_13);
      
      // ---- OBJ_14 ----
      OBJ_14.setText("Affichage d\u00e9tail Attendu et Command\u00e9");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_14);
      
      // ---- OBJ_15 ----
      OBJ_15.setText("Affichage d\u00e9tail Attendu");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_15);
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Affichage d\u00e9tail Command\u00e9");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_16);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Autres affichages des mouvements de stock");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_17);
      OBJ_4.addSeparator();
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Exploitation");
      OBJ_18.setName("OBJ_18");
      OBJ_4.add(OBJ_18);
    }
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Choix possibles");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }
    
    // ---- OBJ_11 ----
    OBJ_11.setText("Affichage de la Fiche Article");
    OBJ_11.setName("OBJ_11");
    OBJ_11.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_11ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private JPanel p_contenu;
  private RiZoneSortie OBJ_31;
  private JLabel OBJ_33;
  private JLabel OBJ_46;
  private RiZoneSortie OBJ_43;
  private JLabel OBJ_44;
  private JLabel H1NUM0;
  private JLabel OBJ_32;
  private JLabel H1NLI0;
  private XRiTextField H1TI3;
  private JLabel OBJ_34;
  private JLabel OBJ_45;
  private JLabel H1SUF0;
  private XRiTextField H1TI1;
  private JPanel panel1;
  private RiZoneSortie OBJ_50;
  private RiZoneSortie OBJ_49;
  private JLabel OBJ_68;
  private XRiTextField H1QT1X;
  private XRiTextField H1QT2X;
  private XRiTextField H1QT3X;
  private XRiTextField H1QT4X;
  private JLabel OBJ_54;
  private JLabel OBJ_61;
  private XRiTextField H1PMP;
  private XRiTextField H1PRV;
  private XRiTextField H1PRX;
  private XRiTextField H1PRD;
  private RiZoneSortie OBJ_67;
  private JLabel OBJ_62;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private JLabel OBJ_51;
  private JLabel OBJ_52;
  private JLabel OBJ_53;
  private JButton OBJ_35;
  private XRiTextField H1TI22;
  private XRiTextField H1TI2;
  private JLabel label1;
  private JLabel OBJ_55;
  private RiZoneSortie HFIDA;
  private JLabel OBJ_57;
  private RiZoneSortie L23AUT;
  private JMenuBar barre_tete;
  private JPanel panel2;
  private JLabel OBJ_23;
  private XRiCalendrier H1DATX;
  private JLabel OBJ_25;
  private RiZoneSortie OBJ_26;
  private JLabel OBJ_27;
  private RiZoneSortie OBJ_30;
  private RiZoneSortie OBJ_29;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
