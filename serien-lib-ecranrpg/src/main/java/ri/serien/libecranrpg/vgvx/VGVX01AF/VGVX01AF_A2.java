
package ri.serien.libecranrpg.vgvx.VGVX01AF;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX01AF_A2 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "Code Libellé", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 37, };
  // Boutons
  private String BOUTON_RECHERCHE = "Rechercher personnalisation";
  
  public VGVX01AF_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_RECHERCHE, 's', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.setEtablissementBlanc(true);
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    
    riBoutonDetailListe1.setVisible(lexique.HostFieldGetData("LD01").contains("?"));
    riBoutonDetailListe2.setVisible(lexique.HostFieldGetData("LD02").contains("?"));
    riBoutonDetailListe3.setVisible(lexique.HostFieldGetData("LD03").contains("?"));
    riBoutonDetailListe4.setVisible(lexique.HostFieldGetData("LD04").contains("?"));
    riBoutonDetailListe5.setVisible(lexique.HostFieldGetData("LD05").contains("?"));
    riBoutonDetailListe6.setVisible(lexique.HostFieldGetData("LD06").contains("?"));
    riBoutonDetailListe7.setVisible(lexique.HostFieldGetData("LD07").contains("?"));
    riBoutonDetailListe8.setVisible(lexique.HostFieldGetData("LD08").contains("?"));
    riBoutonDetailListe9.setVisible(lexique.HostFieldGetData("LD09").contains("?"));
    riBoutonDetailListe10.setVisible(lexique.HostFieldGetData("LD10").contains("?"));
    riBoutonDetailListe11.setVisible(lexique.HostFieldGetData("LD11").contains("?"));
    riBoutonDetailListe12.setVisible(lexique.HostFieldGetData("LD12").contains("?"));
    riBoutonDetailListe13.setVisible(lexique.HostFieldGetData("LD13").contains("?"));
    riBoutonDetailListe14.setVisible(lexique.HostFieldGetData("LD14").contains("?"));
    riBoutonDetailListe15.setVisible(lexique.HostFieldGetData("LD15").contains("?"));
    
    // titre change suivant que l'on est en achats ou en ventes
    if (lexique.HostFieldGetData("LOCGRP").contains("GAM")) {
      p_bpresentation.setText("Personnalisation de la gestion des achats");
    }
    else {
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
    }
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_RECHERCHE)) {
        lexique.HostScreenSendKey(this, "F8");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    snBarreBouton.activerBouton(EnumBouton.VALIDER, WTP01.getSelectedRowCount() > 0);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void riBoutonDetailListe1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP02", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP03", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP04", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe5ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP05", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP06", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP07", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe8ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP08", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP09", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP10", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe11ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP11", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe12ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP12", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe13ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP13", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe14ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP14", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe15ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP15", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void LD4ActionPerformed(ActionEvent e) {
    try {
      lexique.HostFieldPutData("WTP01", 0, "1");
      lexique.HostScreenSendKey(this, "Enter");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    p_sud = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    sNPanelPrincipal1 = new SNPanelContenu();
    sNPanel3 = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbEtablissement2 = new SNLabelChamp();
    WPAR = new XRiTextField();
    sNLabelTitre1 = new SNLabelTitre();
    sNPanel1 = new SNPanel();
    riBoutonDetailListe1 = new SNBoutonDetail();
    riBoutonDetailListe2 = new SNBoutonDetail();
    riBoutonDetailListe3 = new SNBoutonDetail();
    riBoutonDetailListe4 = new SNBoutonDetail();
    riBoutonDetailListe5 = new SNBoutonDetail();
    riBoutonDetailListe6 = new SNBoutonDetail();
    riBoutonDetailListe7 = new SNBoutonDetail();
    riBoutonDetailListe8 = new SNBoutonDetail();
    riBoutonDetailListe9 = new SNBoutonDetail();
    riBoutonDetailListe10 = new SNBoutonDetail();
    riBoutonDetailListe11 = new SNBoutonDetail();
    riBoutonDetailListe12 = new SNBoutonDetail();
    riBoutonDetailListe13 = new SNBoutonDetail();
    riBoutonDetailListe14 = new SNBoutonDetail();
    riBoutonDetailListe15 = new SNBoutonDetail();
    SCROLLPANE_LIST2 = new JScrollPane();
    WTP01 = new XRiTable();
    pnlScroll = new SNPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion commerciale");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      p_sud.add(snBarreBouton, BorderLayout.SOUTH);

      //======== sNPanelPrincipal1 ========
      {
        sNPanelPrincipal1.setName("sNPanelPrincipal1");
        sNPanelPrincipal1.setLayout(new GridBagLayout());
        ((GridBagLayout)sNPanelPrincipal1.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
        ((GridBagLayout)sNPanelPrincipal1.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
        ((GridBagLayout)sNPanelPrincipal1.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 1.0E-4};
        ((GridBagLayout)sNPanelPrincipal1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //======== sNPanel3 ========
        {
          sNPanel3.setName("sNPanel3");
          sNPanel3.setLayout(new GridBagLayout());
          ((GridBagLayout)sNPanel3.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)sNPanel3.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)sNPanel3.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)sNPanel3.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          sNPanel3.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          sNPanel3.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbEtablissement2 ----
          lbEtablissement2.setText("Code");
          lbEtablissement2.setName("lbEtablissement2");
          sNPanel3.add(lbEtablissement2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WPAR ----
          WPAR.setNextFocusableComponent(null);
          WPAR.setFont(new Font("sansserif", Font.PLAIN, 14));
          WPAR.setMinimumSize(new Dimension(35, 30));
          WPAR.setPreferredSize(new Dimension(35, 30));
          WPAR.setMaximumSize(new Dimension(35, 30));
          WPAR.setName("WPAR");
          sNPanel3.add(WPAR, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelPrincipal1.add(sNPanel3, new GridBagConstraints(0, 0, 3, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- sNLabelTitre1 ----
        sNLabelTitre1.setText("Liste des personnalisations disponibles");
        sNLabelTitre1.setName("sNLabelTitre1");
        sNPanelPrincipal1.add(sNLabelTitre1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //======== sNPanel1 ========
        {
          sNPanel1.setName("sNPanel1");
          sNPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout)sNPanel1.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)sNPanel1.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)sNPanel1.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)sNPanel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- riBoutonDetailListe1 ----
          riBoutonDetailListe1.setName("riBoutonDetailListe1");
          riBoutonDetailListe1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe1ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe2 ----
          riBoutonDetailListe2.setName("riBoutonDetailListe2");
          riBoutonDetailListe2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe2ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe3 ----
          riBoutonDetailListe3.setName("riBoutonDetailListe3");
          riBoutonDetailListe3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe3ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe4 ----
          riBoutonDetailListe4.setName("riBoutonDetailListe4");
          riBoutonDetailListe4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe4ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe5 ----
          riBoutonDetailListe5.setName("riBoutonDetailListe5");
          riBoutonDetailListe5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe5ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe6 ----
          riBoutonDetailListe6.setName("riBoutonDetailListe6");
          riBoutonDetailListe6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe6ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe7 ----
          riBoutonDetailListe7.setName("riBoutonDetailListe7");
          riBoutonDetailListe7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe7ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe7, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe8 ----
          riBoutonDetailListe8.setName("riBoutonDetailListe8");
          riBoutonDetailListe8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe8ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe8, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe9 ----
          riBoutonDetailListe9.setName("riBoutonDetailListe9");
          riBoutonDetailListe9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe9ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe9, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe10 ----
          riBoutonDetailListe10.setName("riBoutonDetailListe10");
          riBoutonDetailListe10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe10ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe10, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe11 ----
          riBoutonDetailListe11.setName("riBoutonDetailListe11");
          riBoutonDetailListe11.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe11ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe11, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe12 ----
          riBoutonDetailListe12.setName("riBoutonDetailListe12");
          riBoutonDetailListe12.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe12ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe12, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe13 ----
          riBoutonDetailListe13.setName("riBoutonDetailListe13");
          riBoutonDetailListe13.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe13ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe13, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe14 ----
          riBoutonDetailListe14.setName("riBoutonDetailListe14");
          riBoutonDetailListe14.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe14ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe14, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 1, 0), 0, 0));

          //---- riBoutonDetailListe15 ----
          riBoutonDetailListe15.setName("riBoutonDetailListe15");
          riBoutonDetailListe15.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe15ActionPerformed(e);
            }
          });
          sNPanel1.add(riBoutonDetailListe15, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelPrincipal1.add(sNPanel1, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== SCROLLPANE_LIST2 ========
        {
          SCROLLPANE_LIST2.setPreferredSize(new Dimension(200, 270));
          SCROLLPANE_LIST2.setMinimumSize(new Dimension(200, 270));
          SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

          //---- WTP01 ----
          WTP01.setMaximumSize(new Dimension(2147483647, 238));
          WTP01.setPreferredSize(new Dimension(150, 240));
          WTP01.setPreferredScrollableViewportSize(new Dimension(450, 238));
          WTP01.setRequestFocusEnabled(false);
          WTP01.setMinimumSize(new Dimension(150, 240));
          WTP01.setName("WTP01");
          WTP01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTP01MouseClicked(e);
            }
          });
          SCROLLPANE_LIST2.setViewportView(WTP01);
        }
        sNPanelPrincipal1.add(SCROLLPANE_LIST2, new GridBagConstraints(1, 2, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlScroll ========
        {
          pnlScroll.setMinimumSize(new Dimension(28, 100));
          pnlScroll.setPreferredSize(new Dimension(28, 100));
          pnlScroll.setMaximumSize(new Dimension(28, 100));
          pnlScroll.setName("pnlScroll");
          pnlScroll.setLayout(new GridLayout(2, 1));

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setMaximumSize(new Dimension(28, 100));
          BT_PGUP.setMinimumSize(new Dimension(28, 100));
          BT_PGUP.setPreferredSize(new Dimension(28, 100));
          BT_PGUP.setName("BT_PGUP");
          pnlScroll.add(BT_PGUP);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setMaximumSize(new Dimension(28, 50));
          BT_PGDOWN.setMinimumSize(new Dimension(28, 50));
          BT_PGDOWN.setPreferredSize(new Dimension(28, 50));
          BT_PGDOWN.setName("BT_PGDOWN");
          pnlScroll.add(BT_PGDOWN);
        }
        sNPanelPrincipal1.add(pnlScroll, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(sNPanelPrincipal1, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNPanel p_sud;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelPrincipal1;
  private SNPanel sNPanel3;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement2;
  private XRiTextField WPAR;
  private SNLabelTitre sNLabelTitre1;
  private SNPanel sNPanel1;
  private SNBoutonDetail riBoutonDetailListe1;
  private SNBoutonDetail riBoutonDetailListe2;
  private SNBoutonDetail riBoutonDetailListe3;
  private SNBoutonDetail riBoutonDetailListe4;
  private SNBoutonDetail riBoutonDetailListe5;
  private SNBoutonDetail riBoutonDetailListe6;
  private SNBoutonDetail riBoutonDetailListe7;
  private SNBoutonDetail riBoutonDetailListe8;
  private SNBoutonDetail riBoutonDetailListe9;
  private SNBoutonDetail riBoutonDetailListe10;
  private SNBoutonDetail riBoutonDetailListe11;
  private SNBoutonDetail riBoutonDetailListe12;
  private SNBoutonDetail riBoutonDetailListe13;
  private SNBoutonDetail riBoutonDetailListe14;
  private SNBoutonDetail riBoutonDetailListe15;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WTP01;
  private SNPanel pnlScroll;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
