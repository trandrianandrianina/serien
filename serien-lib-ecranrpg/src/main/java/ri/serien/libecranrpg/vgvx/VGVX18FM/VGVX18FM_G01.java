
package ri.serien.libecranrpg.vgvx.VGVX18FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.DefaultKeyedValues;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.general.DatasetUtilities;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class VGVX18FM_G01 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  // tables de données
  private String[] libelle = new String[8];
  private String[] ligneStk = new String[8];
  private String[] lignePre = new String[8];
  private String[] ligneCom = new String[8];
  private String[] ligneAtt = new String[8];
  private String[] ligneBes = new String[8];
  
  public VGVX18FM_G01(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    // Préparation des données
    DefaultKeyedValues data = new DefaultKeyedValues();
    DefaultKeyedValues data2 = new DefaultKeyedValues();
    DefaultKeyedValues data3 = new DefaultKeyedValues();
    DefaultKeyedValues data4 = new DefaultKeyedValues();
    DefaultKeyedValues data5 = new DefaultKeyedValues();
    
    // GRAPHE
    
    // On crée la table de libellés qui nous intéresse depuis l'EBCDIC
    for (int i = 0; i < libelle.length; i++) {
      libelle[i] = lexique.HostFieldGetData("M0" + (i + 1));
    }
    
    // on charge toutes les données en tables
    // Stocks
    for (int i = 0; i < ligneStk.length; i++) {
      ligneStk[i] = lexique.HostFieldGetNumericData("LD01" + (i + 1));
    }
    // prévisions
    for (int i = 0; i < lignePre.length; i++) {
      lignePre[i] = lexique.HostFieldGetNumericData("LD06" + (i + 1));
    }
    // commandes
    for (int i = 0; i < ligneCom.length; i++) {
      ligneCom[i] = lexique.HostFieldGetNumericData("LD10" + (i + 1));
    }
    // attendu
    for (int i = 0; i < ligneAtt.length; i++) {
      ligneAtt[i] = lexique.HostFieldGetNumericData("LD14" + (i + 1));
    }
    // besoins
    for (int i = 0; i < ligneBes.length; i++) {
      ligneBes[i] = lexique.HostFieldGetNumericData("LD17" + (i + 1));
    }
    
    for (int i = 0; i < libelle.length; i++) {
      data.addValue(libelle[i], Double.parseDouble(ligneStk[i]));
      data2.addValue(libelle[i], Double.parseDouble(lignePre[i]));
      data3.addValue(libelle[i], Double.parseDouble(ligneCom[i]));
      data4.addValue(libelle[i], Double.parseDouble(ligneAtt[i]));
      data5.addValue(libelle[i], Double.parseDouble(ligneBes[i]));
    }
    
    // Chargement des données
    CategoryDataset dataset = DatasetUtilities.createCategoryDataset("Stocks", data);
    CategoryDataset dataset2 = DatasetUtilities.createCategoryDataset("Prévisions", data2);
    CategoryDataset dataset3 = DatasetUtilities.createCategoryDataset("Commandé", data3);
    CategoryDataset dataset4 = DatasetUtilities.createCategoryDataset("Attendu", data4);
    CategoryDataset dataset5 = DatasetUtilities.createCategoryDataset("Besoins", data5);
    
    CategoryDataset[] d = { dataset, dataset2, dataset3, dataset4, dataset5 };
    
    JFreeChart graphe = createChart(d);
    
    l_graphe.setIcon(new ImageIcon(graphe.createBufferedImage(l_graphe.getWidth(), l_graphe.getHeight())));
    
    
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Graphe statistique"));
    
  }
  
  public static JFreeChart createChart(CategoryDataset[] datasets) {
    // create the chart...
    
    JFreeChart chart = ChartFactory.createBarChart("Position article à venir", // chart title
        "Date", // domain axis label
        " ", // range axis label
        datasets[0], // data
        PlotOrientation.VERTICAL, true, // include legend
        true, false);
    
    CategoryPlot plot = (CategoryPlot) chart.getPlot();
    CategoryAxis domainAxis = plot.getDomainAxis();
    
    domainAxis.setLowerMargin(0.02);
    domainAxis.setUpperMargin(0.02);
    domainAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_90);
    
    LineAndShapeRenderer renderer1 = new LineAndShapeRenderer();
    LineAndShapeRenderer renderer2 = new LineAndShapeRenderer();
    LineAndShapeRenderer renderer3 = new LineAndShapeRenderer();
    LineAndShapeRenderer renderer4 = new LineAndShapeRenderer();
    LineAndShapeRenderer renderer5 = new LineAndShapeRenderer();
    
    NumberAxis axis1 = new NumberAxis("");
    // axis2.setNumberFormatOverride(NumberFormat.getPercentInstance());
    plot.setRangeAxis(0, axis1);
    plot.setDataset(0, datasets[0]);
    plot.setRenderer(0, renderer1);
    plot.mapDatasetToRangeAxis(0, 0);
    plot.getRangeAxis(0).setVisible(true);
    axis1.setLabelPaint(Color.black);
    axis1.setTickLabelPaint(Color.black);
    
    NumberAxis axis2 = new NumberAxis("");
    // axis2.setNumberFormatOverride(NumberFormat.getPercentInstance());
    plot.setRangeAxis(1, axis2);
    plot.setDataset(1, datasets[1]);
    plot.setRenderer(1, renderer2);
    plot.mapDatasetToRangeAxis(1, 0);
    plot.getRangeAxis(1).setVisible(false);
    axis2.setLabelPaint(Color.red);
    axis2.setTickLabelPaint(Color.red);
    
    NumberAxis axis3 = new NumberAxis("");
    plot.setRangeAxis(2, axis3);
    plot.setDataset(2, datasets[2]);
    plot.setRenderer(2, renderer3);
    plot.mapDatasetToRangeAxis(2, 0);
    plot.getRangeAxis(2).setVisible(false);
    axis3.setLabelPaint(Color.cyan);
    axis3.setTickLabelPaint(Color.cyan);
    
    NumberAxis axis4 = new NumberAxis("");
    plot.setRangeAxis(3, axis4);
    plot.setDataset(3, datasets[3]);
    plot.setRenderer(3, renderer4);
    plot.mapDatasetToRangeAxis(3, 0);
    plot.getRangeAxis(3).setVisible(false);
    axis4.setLabelPaint(Color.orange);
    axis4.setTickLabelPaint(Color.orange);
    
    NumberAxis axis5 = new NumberAxis("");
    plot.setRangeAxis(4, axis5);
    plot.setDataset(4, datasets[4]);
    plot.setRenderer(4, renderer5);
    plot.mapDatasetToRangeAxis(4, 0);
    plot.getRangeAxis(4).setVisible(false);
    axis5.setLabelPaint(Color.black);
    axis5.setTickLabelPaint(Color.black);
    
    plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
    
    // ChartUtilities.applyCurrentTheme(chart);
    
    return chart;
  }
  
  public void getData() {
    
  }
  
  public void reveiller() {
    setVisible(true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    setVisible(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    l_graphe = new JLabel();
    OBJ_10 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(335, 220));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      
      // ---- l_graphe ----
      l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe.setComponentPopupMenu(null);
      l_graphe.setBackground(new Color(214, 217, 223));
      l_graphe.setName("l_graphe");
      
      // ---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setToolTipText("Retour");
      OBJ_10.setText("Retour");
      OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD, OBJ_10.getFont().getSize() + 3f));
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(12, 12, 12).addComponent(l_graphe, GroupLayout.DEFAULT_SIZE, 1080,
              Short.MAX_VALUE))
          .addGroup(GroupLayout.Alignment.TRAILING, P_CentreLayout.createSequentialGroup().addContainerGap(940, Short.MAX_VALUE)
              .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE).addContainerGap()));
      P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(15, 15, 15)
              .addComponent(l_graphe, GroupLayout.DEFAULT_SIZE, 593, Short.MAX_VALUE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(8, 8, 8)));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JLabel l_graphe;
  private JButton OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
