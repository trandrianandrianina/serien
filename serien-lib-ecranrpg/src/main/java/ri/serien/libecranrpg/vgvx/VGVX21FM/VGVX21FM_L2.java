
package ri.serien.libecranrpg.vgvx.VGVX21FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX21FM_L2 extends SNPanelEcranRPG implements ioFrame {
  
  private CONF_PANEL confpanel = null;
  
  public VGVX21FM_L2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    A1CNDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1CNDX@")).trim());
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNL@")).trim());
    WDISX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    WDISUN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISUN@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLDALA@")).trim());
    OBJ_50.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLDALA@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1QT2@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1QT3@")).trim());
    WCLPL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCLPL@")).trim());
    X1PPC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@X1PPC@")).trim());
    X1PPL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@X1PPL@")).trim());
    X1PPP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@X1PPP@")).trim());
    OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNL@")).trim());
    NUM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM1@")).trim());
    QTSX1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX1@")).trim());
    NUM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM2@")).trim());
    QTSX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX2@")).trim());
    NUM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM3@")).trim());
    QTSX3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX3@")).trim());
    NUM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM4@")).trim());
    QTSX4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX4@")).trim());
    NUM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM5@")).trim());
    QTSX5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX5@")).trim());
    NUM6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM6@")).trim());
    QTSX6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX6@")).trim());
    NUM7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM7@")).trim());
    QTSX7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX7@")).trim());
    NUM8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM8@")).trim());
    QTSX8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX8@")).trim());
    NUM9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM9@")).trim());
    QTSX9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX9@")).trim());
    NUM10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM10@")).trim());
    QTSX10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX10@")).trim());
    P21QPX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P21QPX@")).trim());
    LD2X1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LD2X2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X2@")).trim());
    LD2X3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X3@")).trim());
    LD2X4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X4@")).trim());
    LD2X5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X5@")).trim());
    LD2X6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X6@")).trim());
    LD2X7.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X7@")).trim());
    LD2X8.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X8@")).trim());
    LD2X9.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X9@")).trim());
    LD2X10.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X10@")).trim());
    P21QTX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P21QTX@")).trim());
    lb_a_virer.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // riBoutonInfo1.gererAffichageV03F(lexique.isTrue("19"),lexique.getConvertInfosFiche(lexique.HostFieldGetData("V03F").trim()));
    
    riSousMenu15.setVisible(!lexique.HostFieldGetData("WCF15").trim().equalsIgnoreCase(""));
    OBJ_13.setVisible(!lexique.HostFieldGetData("WCF13").trim().equalsIgnoreCase(""));
    OBJ_14.setVisible(!lexique.HostFieldGetData("WCF14").trim().equalsIgnoreCase(""));
    
    OBJ_34.setVisible(A1CNDX.isVisible()); // Conditionnement
    OBJ_46.setVisible(lexique.isTrue("((29) AND (11)) OR ((29) AND (12))")); // Laize
    OBJ_47.setVisible(OBJ_46.isVisible());
    OBJ_48.setVisible(lexique.isTrue("(29) AND (11)")); // Longueur
    OBJ_49.setVisible(OBJ_48.isVisible());
    
    boolean n42 = lexique.isTrue("N42");
    QTSX1.setVisible(n42);
    QTSX2.setVisible(n42);
    QTSX3.setVisible(n42);
    QTSX4.setVisible(n42);
    QTSX5.setVisible(n42);
    QTSX6.setVisible(n42);
    QTSX7.setVisible(n42);
    QTSX8.setVisible(n42);
    QTSX9.setVisible(n42);
    QTSX10.setVisible(n42);
    
    // boolean in31 = lexique.isTrue("31"); // PAs encore utilisé mais pour le > à la place des ES0X
    P21QPX.setVisible(lexique.isTrue("((42) AND (N29)) OR ((42) AND (29) AND (N11))"));
    
    // entête
    label1.setText(lexique.HostFieldGetData("HLD01").substring(0, 4));
    label2.setText(lexique.HostFieldGetData("HLD01").substring(4, 27));
    label3.setText(lexique.HostFieldGetData("HLD01").substring(27, 29));
    label4.setText(lexique.HostFieldGetData("HLD01").substring(29, 39));
    label5.setText(lexique.HostFieldGetData("HLD01").substring(39, 49));
    label6.setText(lexique.HostFieldGetData("HLD01").substring(49, 51));
    label7.setText(lexique.HostFieldGetData("HLD01").substring(51, 56).trim());
    label8.setText(lexique.HostFieldGetData("HLD01").substring(56, 63).trim());
    label9.setText(lexique.HostFieldGetData("HLD01").substring(63, 70).trim());
    label10.setText(lexique.HostFieldGetData("HLD01").substring(70, 76).trim());
    label11.setText(lexique.HostFieldGetData("HLD01").substring(76, lexique.HostFieldGetData("HLD01").length()));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Saisie de numéros de lots pour l'article @P21ART@"));
    
    // Gestion du focus avec les touches flèches
    final XRiTextField[] ENL1table = { ENL101, ENL102, ENL103, ENL104, ENL105, ENL106, ENL107, ENL108, ENL109, ENL110 };
    final XRiTextField[] ENL2table = { ENL201, ENL202, ENL203, ENL204, ENL205, ENL206, ENL207, ENL208, ENL209, ENL210 };
    final XRiTextField[] QTEXtable = { QTEX1, QTEX2, QTEX3, QTEX4, QTEX5, QTEX6, QTEX7, QTEX8, QTEX9, QTEX10 };
    final XRiTextField[] AD1table = { AD11, AD12, AD13, AD14, AD15, AD16, AD17, AD18, AD19, AD110 };
    final XRiTextField[] AD2table = { AD21, AD22, AD23, AD24, AD25, AD26, AD27, AD28, AD29, AD210 };
    final XRiTextField[] AD3table = { AD31, AD32, AD33, AD34, AD35, AD36, AD37, AD38, AD39, AD310 };
    final XRiTextField[] AD4table = { AD41, AD42, AD43, AD44, AD45, AD46, AD47, AD48, AD49, AD410 };
    final XRiTextField[] LD2Xtable = { LD2X1, LD2X2, LD2X3, LD2X4, LD2X5, LD2X6, LD2X7, LD2X8, LD2X9, LD2X10 };
    
    for (int i = 0; i < ENL1table.length; i++) {
      final int indice = i;
      
      ENL1table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              ENL2table[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                ENL1table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                ENL1table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        @Override
        public void keyReleased(KeyEvent e) {
        }
        
        @Override
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      ENL2table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              QTEXtable[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              ENL1table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                ENL2table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                ENL2table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        @Override
        public void keyReleased(KeyEvent e) {
        }
        
        @Override
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      QTEXtable[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              AD1table[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              ENL2table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                QTEXtable[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                QTEXtable[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        @Override
        public void keyReleased(KeyEvent e) {
        }
        
        @Override
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      AD1table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              AD2table[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              QTEXtable[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                AD1table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                AD1table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        @Override
        public void keyReleased(KeyEvent e) {
        }
        
        @Override
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      AD2table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              AD3table[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              AD1table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                AD2table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                AD2table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        @Override
        public void keyReleased(KeyEvent e) {
        }
        
        @Override
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      AD3table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              AD4table[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              AD2table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                AD3table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                AD3table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        @Override
        public void keyReleased(KeyEvent e) {
        }
        
        @Override
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      AD4table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              LD2Xtable[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              AD3table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                AD4table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                AD4table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        @Override
        public void keyReleased(KeyEvent e) {
        }
        
        @Override
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      LD2Xtable[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              
              break;
            case KeyEvent.VK_LEFT:
              AD4table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                LD2Xtable[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                LD2Xtable[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        @Override
        public void keyReleased(KeyEvent e) {
        }
        
        @Override
        public void keyTyped(KeyEvent e) {
        }
        
      });
    }
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    // riMenu_bt4.setIcon(lexique.getImage("images/fonctions.png", true));
    
    if (!lexique.HostFieldGetData("V06F").trim().equals("")) {
      if (confpanel != null) {
        confpanel.reveiller();
      }
      else {
        confpanel = new CONF_PANEL(this, lexique, interpreteurD);
      }
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
    if (confpanel != null) {
      confpanel.tuer();
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_34 = new JLabel();
    A1CNDX = new RiZoneSortie();
    OBJ_36 = new RiZoneSortie();
    OBJ_31 = new JLabel();
    WDISX = new RiZoneSortie();
    WDISUN = new RiZoneSortie();
    OBJ_50 = new RiZoneSortie();
    OBJ_37 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_47 = new RiZoneSortie();
    OBJ_49 = new RiZoneSortie();
    WCLPL = new RiZoneSortie();
    OBJ_46 = new JLabel();
    X1PPC = new RiZoneSortie();
    X1PPL = new RiZoneSortie();
    X1PPP = new RiZoneSortie();
    OBJ_45 = new RiZoneSortie();
    OBJ_43 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_41 = new JLabel();
    panel3 = new JPanel();
    NUM1 = new RiZoneSortie();
    ENL101 = new XRiTextField();
    ENL201 = new XRiTextField();
    QTSX1 = new RiZoneSortie();
    QTPX1 = new XRiTextField();
    QTEX1 = new XRiTextField();
    ES01 = new XRiTextField();
    AD11 = new XRiTextField();
    NUM2 = new RiZoneSortie();
    ENL102 = new XRiTextField();
    ENL202 = new XRiTextField();
    QTSX2 = new RiZoneSortie();
    QTPX2 = new XRiTextField();
    QTEX2 = new XRiTextField();
    ES2 = new XRiTextField();
    AD12 = new XRiTextField();
    NUM3 = new RiZoneSortie();
    ENL103 = new XRiTextField();
    ENL203 = new XRiTextField();
    QTSX3 = new RiZoneSortie();
    QTPX3 = new XRiTextField();
    QTEX3 = new XRiTextField();
    ES3 = new XRiTextField();
    AD13 = new XRiTextField();
    NUM4 = new RiZoneSortie();
    ENL104 = new XRiTextField();
    ENL204 = new XRiTextField();
    QTSX4 = new RiZoneSortie();
    QTPX4 = new XRiTextField();
    QTEX4 = new XRiTextField();
    ES4 = new XRiTextField();
    AD14 = new XRiTextField();
    NUM5 = new RiZoneSortie();
    ENL105 = new XRiTextField();
    ENL205 = new XRiTextField();
    QTSX5 = new RiZoneSortie();
    QTPX5 = new XRiTextField();
    QTEX5 = new XRiTextField();
    ES5 = new XRiTextField();
    AD15 = new XRiTextField();
    NUM6 = new RiZoneSortie();
    ENL106 = new XRiTextField();
    ENL206 = new XRiTextField();
    QTSX6 = new RiZoneSortie();
    QTPX6 = new XRiTextField();
    QTEX6 = new XRiTextField();
    ES6 = new XRiTextField();
    AD16 = new XRiTextField();
    NUM7 = new RiZoneSortie();
    ENL107 = new XRiTextField();
    ENL207 = new XRiTextField();
    QTSX7 = new RiZoneSortie();
    QTPX7 = new XRiTextField();
    QTEX7 = new XRiTextField();
    ES7 = new XRiTextField();
    AD17 = new XRiTextField();
    NUM8 = new RiZoneSortie();
    ENL108 = new XRiTextField();
    ENL208 = new XRiTextField();
    QTSX8 = new RiZoneSortie();
    QTPX8 = new XRiTextField();
    QTEX8 = new XRiTextField();
    ES8 = new XRiTextField();
    AD18 = new XRiTextField();
    NUM9 = new RiZoneSortie();
    ENL109 = new XRiTextField();
    ENL209 = new XRiTextField();
    QTSX9 = new RiZoneSortie();
    QTPX9 = new XRiTextField();
    QTEX9 = new XRiTextField();
    ES9 = new XRiTextField();
    AD19 = new XRiTextField();
    NUM10 = new RiZoneSortie();
    ENL110 = new XRiTextField();
    ENL210 = new XRiTextField();
    QTSX10 = new RiZoneSortie();
    QTPX10 = new XRiTextField();
    QTEX10 = new XRiTextField();
    ES10 = new XRiTextField();
    AD110 = new XRiTextField();
    P21QPX = new RiZoneSortie();
    AD21 = new XRiTextField();
    AD22 = new XRiTextField();
    AD23 = new XRiTextField();
    AD24 = new XRiTextField();
    AD25 = new XRiTextField();
    AD26 = new XRiTextField();
    AD27 = new XRiTextField();
    AD28 = new XRiTextField();
    AD29 = new XRiTextField();
    AD210 = new XRiTextField();
    AD31 = new XRiTextField();
    AD32 = new XRiTextField();
    AD33 = new XRiTextField();
    AD34 = new XRiTextField();
    AD35 = new XRiTextField();
    AD36 = new XRiTextField();
    AD37 = new XRiTextField();
    AD38 = new XRiTextField();
    AD39 = new XRiTextField();
    AD310 = new XRiTextField();
    AD41 = new XRiTextField();
    AD42 = new XRiTextField();
    AD43 = new XRiTextField();
    AD44 = new XRiTextField();
    AD45 = new XRiTextField();
    AD46 = new XRiTextField();
    AD47 = new XRiTextField();
    AD48 = new XRiTextField();
    AD49 = new XRiTextField();
    AD410 = new XRiTextField();
    LD2X1 = new XRiTextField();
    LD2X2 = new XRiTextField();
    LD2X3 = new XRiTextField();
    LD2X4 = new XRiTextField();
    LD2X5 = new XRiTextField();
    LD2X6 = new XRiTextField();
    LD2X7 = new XRiTextField();
    LD2X8 = new XRiTextField();
    LD2X9 = new XRiTextField();
    LD2X10 = new XRiTextField();
    P21QTX = new RiZoneSortie();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    barre_tete = new JMenuBar();
    lb_a_virer = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1240, 525));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");
            
            // ---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);
          
          // ======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");
            
            // ---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);
          
          // ======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");
            
            // ---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Autre vue");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
          
          // ======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");
            
            // ---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Detail conditionnement");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
          
          // ======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");
            
            // ---- riMenu_bt3 ----
            riMenu_bt3.setText("Outils");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);
          
          // ======== riSousMenu14 ========
          {
            riSousMenu14.setName("riSousMenu14");
            
            // ---- riSousMenu_bt14 ----
            riSousMenu_bt14.setText("Bloc notes");
            riSousMenu_bt14.setName("riSousMenu_bt14");
            riSousMenu_bt14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt14ActionPerformed(e);
              }
            });
            riSousMenu14.add(riSousMenu_bt14);
          }
          menus_haut.add(riSousMenu14);
          
          // ======== riSousMenu15 ========
          {
            riSousMenu15.setName("riSousMenu15");
            
            // ---- riSousMenu_bt15 ----
            riSousMenu_bt15.setText("Fabrication");
            riSousMenu_bt15.setToolTipText("Fabrication");
            riSousMenu_bt15.setName("riSousMenu_bt15");
            riSousMenu_bt15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt15ActionPerformed(e);
              }
            });
            riSousMenu15.add(riSousMenu_bt15);
          }
          menus_haut.add(riSousMenu15);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setMinimumSize(new Dimension(1024, 465));
        p_contenu.setPreferredSize(new Dimension(1024, 465));
        p_contenu.setName("p_contenu");
        
        // ======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);
          
          // ---- OBJ_34 ----
          OBJ_34.setText("Conditionnement");
          OBJ_34.setComponentPopupMenu(null);
          OBJ_34.setName("OBJ_34");
          panel2.add(OBJ_34);
          OBJ_34.setBounds(20, 21, 115, 24);
          
          // ---- A1CNDX ----
          A1CNDX.setComponentPopupMenu(null);
          A1CNDX.setHorizontalAlignment(SwingConstants.RIGHT);
          A1CNDX.setText("@A1CNDX@");
          A1CNDX.setName("A1CNDX");
          panel2.add(A1CNDX);
          A1CNDX.setBounds(145, 21, 84, 24);
          
          // ---- OBJ_36 ----
          OBJ_36.setText("@A1UNL@");
          OBJ_36.setComponentPopupMenu(null);
          OBJ_36.setName("OBJ_36");
          panel2.add(OBJ_36);
          OBJ_36.setBounds(235, 21, 34, 24);
          
          // ---- OBJ_31 ----
          OBJ_31.setText("Disponible");
          OBJ_31.setComponentPopupMenu(null);
          OBJ_31.setName("OBJ_31");
          panel2.add(OBJ_31);
          OBJ_31.setBounds(435, 21, 86, 24);
          
          // ---- WDISX ----
          WDISX.setComponentPopupMenu(null);
          WDISX.setText("@WDISX@");
          WDISX.setName("WDISX");
          panel2.add(WDISX);
          WDISX.setBounds(530, 21, 110, 24);
          
          // ---- WDISUN ----
          WDISUN.setComponentPopupMenu(null);
          WDISUN.setText("@WDISUN@");
          WDISUN.setName("WDISUN");
          panel2.add(WDISUN);
          WDISUN.setBounds(645, 21, 34, 24);
          
          // ---- OBJ_50 ----
          OBJ_50.setText("@WLDALA@");
          OBJ_50.setToolTipText("@WLDALA@");
          OBJ_50.setComponentPopupMenu(null);
          OBJ_50.setName("OBJ_50");
          panel2.add(OBJ_50);
          OBJ_50.setBounds(20, 85, 910, 20);
          
          // ---- OBJ_37 ----
          OBJ_37.setText("Plan palettisation");
          OBJ_37.setComponentPopupMenu(null);
          OBJ_37.setName("OBJ_37");
          panel2.add(OBJ_37);
          OBJ_37.setBounds(20, 53, 123, 24);
          
          // ---- OBJ_48 ----
          OBJ_48.setText("Longueur");
          OBJ_48.setComponentPopupMenu(null);
          OBJ_48.setName("OBJ_48");
          panel2.add(OBJ_48);
          OBJ_48.setBounds(560, 53, 59, 24);
          
          // ---- OBJ_47 ----
          OBJ_47.setText("@A1QT2@");
          OBJ_47.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_47.setComponentPopupMenu(null);
          OBJ_47.setName("OBJ_47");
          panel2.add(OBJ_47);
          OBJ_47.setBounds(480, 53, 68, 24);
          
          // ---- OBJ_49 ----
          OBJ_49.setText("@A1QT3@");
          OBJ_49.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_49.setComponentPopupMenu(null);
          OBJ_49.setName("OBJ_49");
          panel2.add(OBJ_49);
          OBJ_49.setBounds(635, 53, 52, 24);
          
          // ---- WCLPL ----
          WCLPL.setComponentPopupMenu(null);
          WCLPL.setText("@WCLPL@");
          WCLPL.setName("WCLPL");
          panel2.add(WCLPL);
          WCLPL.setBounds(320, 53, 52, WCLPL.getPreferredSize().height);
          
          // ---- OBJ_46 ----
          OBJ_46.setText("Laize");
          OBJ_46.setComponentPopupMenu(null);
          OBJ_46.setName("OBJ_46");
          panel2.add(OBJ_46);
          OBJ_46.setBounds(435, 53, 40, 24);
          
          // ---- X1PPC ----
          X1PPC.setComponentPopupMenu(null);
          X1PPC.setText("@X1PPC@");
          X1PPC.setName("X1PPC");
          panel2.add(X1PPC);
          X1PPC.setBounds(145, 53, 28, X1PPC.getPreferredSize().height);
          
          // ---- X1PPL ----
          X1PPL.setComponentPopupMenu(null);
          X1PPL.setText("@X1PPL@");
          X1PPL.setName("X1PPL");
          panel2.add(X1PPL);
          X1PPL.setBounds(200, 53, 28, X1PPL.getPreferredSize().height);
          
          // ---- X1PPP ----
          X1PPP.setComponentPopupMenu(null);
          X1PPP.setText("@X1PPP@");
          X1PPP.setName("X1PPP");
          panel2.add(X1PPP);
          X1PPP.setBounds(260, 53, 28, X1PPP.getPreferredSize().height);
          
          // ---- OBJ_45 ----
          OBJ_45.setText("@A1UNL@");
          OBJ_45.setComponentPopupMenu(null);
          OBJ_45.setName("OBJ_45");
          panel2.add(OBJ_45);
          OBJ_45.setBounds(380, 53, 34, 24);
          
          // ---- OBJ_43 ----
          OBJ_43.setText("=");
          OBJ_43.setComponentPopupMenu(null);
          OBJ_43.setName("OBJ_43");
          panel2.add(OBJ_43);
          OBJ_43.setBounds(300, 53, 10, 24);
          
          // ---- OBJ_39 ----
          OBJ_39.setText("x");
          OBJ_39.setComponentPopupMenu(null);
          OBJ_39.setName("OBJ_39");
          panel2.add(OBJ_39);
          OBJ_39.setBounds(180, 53, 9, 24);
          
          // ---- OBJ_41 ----
          OBJ_41.setText("x");
          OBJ_41.setComponentPopupMenu(null);
          OBJ_41.setName("OBJ_41");
          panel2.add(OBJ_41);
          OBJ_41.setBounds(240, 53, 9, 24);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        
        // ======== panel3 ========
        {
          panel3.setBorder(new TitledBorder(""));
          panel3.setMinimumSize(new Dimension(1600, 333));
          panel3.setPreferredSize(new Dimension(1600, 333));
          panel3.setBackground(new Color(238, 238, 210));
          panel3.setMaximumSize(new Dimension(1600, 333));
          panel3.setName("panel3");
          panel3.setLayout(null);
          
          // ---- NUM1 ----
          NUM1.setText("@NUM1@");
          NUM1.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          NUM1.setComponentPopupMenu(BTD);
          NUM1.setName("NUM1");
          panel3.add(NUM1);
          NUM1.setBounds(25, 35, 35, 24);
          
          // ---- ENL101 ----
          ENL101.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL101.setComponentPopupMenu(BTD);
          ENL101.setName("ENL101");
          panel3.add(ENL101);
          ENL101.setBounds(60, 33, 185, ENL101.getPreferredSize().height);
          
          // ---- ENL201 ----
          ENL201.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL201.setComponentPopupMenu(BTD);
          ENL201.setName("ENL201");
          panel3.add(ENL201);
          ENL201.setBounds(245, 33, 30, ENL201.getPreferredSize().height);
          
          // ---- QTSX1 ----
          QTSX1.setText("@QTSX1@");
          QTSX1.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTSX1.setComponentPopupMenu(BTD);
          QTSX1.setName("QTSX1");
          panel3.add(QTSX1);
          QTSX1.setBounds(275, 35, 75, 24);
          
          // ---- QTPX1 ----
          QTPX1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTPX1.setComponentPopupMenu(BTD);
          QTPX1.setName("QTPX1");
          panel3.add(QTPX1);
          QTPX1.setBounds(275, 33, 75, 28);
          
          // ---- QTEX1 ----
          QTEX1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTEX1.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX1.setComponentPopupMenu(BTD);
          QTEX1.setName("QTEX1");
          panel3.add(QTEX1);
          QTEX1.setBounds(350, 33, 75, QTEX1.getPreferredSize().height);
          
          // ---- ES01 ----
          ES01.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ES01.setComponentPopupMenu(BTD);
          ES01.setName("ES01");
          panel3.add(ES01);
          ES01.setBounds(425, 33, 20, ES01.getPreferredSize().height);
          
          // ---- AD11 ----
          AD11.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD11.setComponentPopupMenu(BTD);
          AD11.setName("AD11");
          panel3.add(AD11);
          AD11.setBounds(449, 33, 34, AD11.getPreferredSize().height);
          
          // ---- NUM2 ----
          NUM2.setText("@NUM2@");
          NUM2.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          NUM2.setComponentPopupMenu(BTD);
          NUM2.setName("NUM2");
          panel3.add(NUM2);
          NUM2.setBounds(25, 61, 35, 24);
          
          // ---- ENL102 ----
          ENL102.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL102.setComponentPopupMenu(BTD);
          ENL102.setName("ENL102");
          panel3.add(ENL102);
          ENL102.setBounds(60, 59, 185, 28);
          
          // ---- ENL202 ----
          ENL202.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL202.setComponentPopupMenu(BTD);
          ENL202.setName("ENL202");
          panel3.add(ENL202);
          ENL202.setBounds(245, 59, 30, 28);
          
          // ---- QTSX2 ----
          QTSX2.setText("@QTSX2@");
          QTSX2.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTSX2.setComponentPopupMenu(BTD);
          QTSX2.setName("QTSX2");
          panel3.add(QTSX2);
          QTSX2.setBounds(275, 61, 75, 24);
          
          // ---- QTPX2 ----
          QTPX2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTPX2.setComponentPopupMenu(BTD);
          QTPX2.setName("QTPX2");
          panel3.add(QTPX2);
          QTPX2.setBounds(275, 59, 75, 28);
          
          // ---- QTEX2 ----
          QTEX2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTEX2.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX2.setComponentPopupMenu(BTD);
          QTEX2.setName("QTEX2");
          panel3.add(QTEX2);
          QTEX2.setBounds(350, 59, 75, 28);
          
          // ---- ES2 ----
          ES2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ES2.setComponentPopupMenu(BTD);
          ES2.setName("ES2");
          panel3.add(ES2);
          ES2.setBounds(425, 59, 20, 28);
          
          // ---- AD12 ----
          AD12.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD12.setComponentPopupMenu(BTD);
          AD12.setName("AD12");
          panel3.add(AD12);
          AD12.setBounds(449, 59, 34, 28);
          
          // ---- NUM3 ----
          NUM3.setText("@NUM3@");
          NUM3.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          NUM3.setComponentPopupMenu(BTD);
          NUM3.setName("NUM3");
          panel3.add(NUM3);
          NUM3.setBounds(25, 87, 35, 24);
          
          // ---- ENL103 ----
          ENL103.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL103.setComponentPopupMenu(BTD);
          ENL103.setName("ENL103");
          panel3.add(ENL103);
          ENL103.setBounds(60, 85, 185, 28);
          
          // ---- ENL203 ----
          ENL203.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL203.setComponentPopupMenu(BTD);
          ENL203.setName("ENL203");
          panel3.add(ENL203);
          ENL203.setBounds(245, 85, 30, 28);
          
          // ---- QTSX3 ----
          QTSX3.setText("@QTSX3@");
          QTSX3.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTSX3.setComponentPopupMenu(BTD);
          QTSX3.setName("QTSX3");
          panel3.add(QTSX3);
          QTSX3.setBounds(275, 87, 75, 24);
          
          // ---- QTPX3 ----
          QTPX3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTPX3.setComponentPopupMenu(BTD);
          QTPX3.setName("QTPX3");
          panel3.add(QTPX3);
          QTPX3.setBounds(275, 85, 75, 28);
          
          // ---- QTEX3 ----
          QTEX3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTEX3.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX3.setComponentPopupMenu(BTD);
          QTEX3.setName("QTEX3");
          panel3.add(QTEX3);
          QTEX3.setBounds(350, 85, 75, 28);
          
          // ---- ES3 ----
          ES3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ES3.setComponentPopupMenu(BTD);
          ES3.setName("ES3");
          panel3.add(ES3);
          ES3.setBounds(425, 85, 20, 28);
          
          // ---- AD13 ----
          AD13.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD13.setComponentPopupMenu(BTD);
          AD13.setName("AD13");
          panel3.add(AD13);
          AD13.setBounds(449, 85, 34, 28);
          
          // ---- NUM4 ----
          NUM4.setText("@NUM4@");
          NUM4.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM4.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          NUM4.setComponentPopupMenu(BTD);
          NUM4.setName("NUM4");
          panel3.add(NUM4);
          NUM4.setBounds(25, 113, 35, 24);
          
          // ---- ENL104 ----
          ENL104.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL104.setComponentPopupMenu(BTD);
          ENL104.setName("ENL104");
          panel3.add(ENL104);
          ENL104.setBounds(60, 111, 185, 28);
          
          // ---- ENL204 ----
          ENL204.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL204.setComponentPopupMenu(BTD);
          ENL204.setName("ENL204");
          panel3.add(ENL204);
          ENL204.setBounds(245, 111, 30, 28);
          
          // ---- QTSX4 ----
          QTSX4.setText("@QTSX4@");
          QTSX4.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX4.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTSX4.setComponentPopupMenu(BTD);
          QTSX4.setName("QTSX4");
          panel3.add(QTSX4);
          QTSX4.setBounds(275, 113, 75, 24);
          
          // ---- QTPX4 ----
          QTPX4.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTPX4.setComponentPopupMenu(BTD);
          QTPX4.setName("QTPX4");
          panel3.add(QTPX4);
          QTPX4.setBounds(275, 111, 75, 28);
          
          // ---- QTEX4 ----
          QTEX4.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTEX4.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX4.setComponentPopupMenu(BTD);
          QTEX4.setName("QTEX4");
          panel3.add(QTEX4);
          QTEX4.setBounds(350, 111, 75, 28);
          
          // ---- ES4 ----
          ES4.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ES4.setComponentPopupMenu(BTD);
          ES4.setName("ES4");
          panel3.add(ES4);
          ES4.setBounds(425, 111, 20, 28);
          
          // ---- AD14 ----
          AD14.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD14.setComponentPopupMenu(BTD);
          AD14.setName("AD14");
          panel3.add(AD14);
          AD14.setBounds(449, 111, 34, 28);
          
          // ---- NUM5 ----
          NUM5.setText("@NUM5@");
          NUM5.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM5.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          NUM5.setComponentPopupMenu(BTD);
          NUM5.setName("NUM5");
          panel3.add(NUM5);
          NUM5.setBounds(25, 139, 35, 24);
          
          // ---- ENL105 ----
          ENL105.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL105.setComponentPopupMenu(BTD);
          ENL105.setName("ENL105");
          panel3.add(ENL105);
          ENL105.setBounds(60, 137, 185, 28);
          
          // ---- ENL205 ----
          ENL205.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL205.setComponentPopupMenu(BTD);
          ENL205.setName("ENL205");
          panel3.add(ENL205);
          ENL205.setBounds(245, 137, 30, 28);
          
          // ---- QTSX5 ----
          QTSX5.setText("@QTSX5@");
          QTSX5.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX5.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTSX5.setComponentPopupMenu(BTD);
          QTSX5.setName("QTSX5");
          panel3.add(QTSX5);
          QTSX5.setBounds(275, 139, 75, 24);
          
          // ---- QTPX5 ----
          QTPX5.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTPX5.setComponentPopupMenu(BTD);
          QTPX5.setName("QTPX5");
          panel3.add(QTPX5);
          QTPX5.setBounds(275, 137, 75, 28);
          
          // ---- QTEX5 ----
          QTEX5.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTEX5.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX5.setComponentPopupMenu(BTD);
          QTEX5.setName("QTEX5");
          panel3.add(QTEX5);
          QTEX5.setBounds(350, 137, 75, 28);
          
          // ---- ES5 ----
          ES5.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ES5.setComponentPopupMenu(BTD);
          ES5.setName("ES5");
          panel3.add(ES5);
          ES5.setBounds(425, 137, 20, 28);
          
          // ---- AD15 ----
          AD15.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD15.setComponentPopupMenu(BTD);
          AD15.setName("AD15");
          panel3.add(AD15);
          AD15.setBounds(449, 137, 34, 28);
          
          // ---- NUM6 ----
          NUM6.setText("@NUM6@");
          NUM6.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM6.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          NUM6.setComponentPopupMenu(BTD);
          NUM6.setName("NUM6");
          panel3.add(NUM6);
          NUM6.setBounds(25, 165, 35, 24);
          
          // ---- ENL106 ----
          ENL106.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL106.setComponentPopupMenu(BTD);
          ENL106.setName("ENL106");
          panel3.add(ENL106);
          ENL106.setBounds(60, 163, 185, 28);
          
          // ---- ENL206 ----
          ENL206.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL206.setComponentPopupMenu(BTD);
          ENL206.setName("ENL206");
          panel3.add(ENL206);
          ENL206.setBounds(245, 163, 30, 28);
          
          // ---- QTSX6 ----
          QTSX6.setText("@QTSX6@");
          QTSX6.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX6.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTSX6.setComponentPopupMenu(BTD);
          QTSX6.setName("QTSX6");
          panel3.add(QTSX6);
          QTSX6.setBounds(275, 165, 75, 24);
          
          // ---- QTPX6 ----
          QTPX6.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTPX6.setComponentPopupMenu(BTD);
          QTPX6.setName("QTPX6");
          panel3.add(QTPX6);
          QTPX6.setBounds(275, 163, 75, 28);
          
          // ---- QTEX6 ----
          QTEX6.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTEX6.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX6.setComponentPopupMenu(BTD);
          QTEX6.setName("QTEX6");
          panel3.add(QTEX6);
          QTEX6.setBounds(350, 163, 75, 28);
          
          // ---- ES6 ----
          ES6.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ES6.setComponentPopupMenu(BTD);
          ES6.setName("ES6");
          panel3.add(ES6);
          ES6.setBounds(425, 163, 20, 28);
          
          // ---- AD16 ----
          AD16.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD16.setComponentPopupMenu(BTD);
          AD16.setName("AD16");
          panel3.add(AD16);
          AD16.setBounds(449, 163, 34, 28);
          
          // ---- NUM7 ----
          NUM7.setText("@NUM7@");
          NUM7.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM7.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          NUM7.setComponentPopupMenu(BTD);
          NUM7.setName("NUM7");
          panel3.add(NUM7);
          NUM7.setBounds(25, 191, 35, 24);
          
          // ---- ENL107 ----
          ENL107.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL107.setComponentPopupMenu(BTD);
          ENL107.setName("ENL107");
          panel3.add(ENL107);
          ENL107.setBounds(60, 189, 185, 28);
          
          // ---- ENL207 ----
          ENL207.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL207.setComponentPopupMenu(BTD);
          ENL207.setName("ENL207");
          panel3.add(ENL207);
          ENL207.setBounds(245, 189, 30, 28);
          
          // ---- QTSX7 ----
          QTSX7.setText("@QTSX7@");
          QTSX7.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX7.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTSX7.setComponentPopupMenu(BTD);
          QTSX7.setName("QTSX7");
          panel3.add(QTSX7);
          QTSX7.setBounds(275, 191, 75, 24);
          
          // ---- QTPX7 ----
          QTPX7.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTPX7.setComponentPopupMenu(BTD);
          QTPX7.setName("QTPX7");
          panel3.add(QTPX7);
          QTPX7.setBounds(275, 189, 75, 28);
          
          // ---- QTEX7 ----
          QTEX7.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTEX7.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX7.setComponentPopupMenu(BTD);
          QTEX7.setName("QTEX7");
          panel3.add(QTEX7);
          QTEX7.setBounds(350, 189, 75, 28);
          
          // ---- ES7 ----
          ES7.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ES7.setComponentPopupMenu(BTD);
          ES7.setName("ES7");
          panel3.add(ES7);
          ES7.setBounds(425, 189, 20, 28);
          
          // ---- AD17 ----
          AD17.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD17.setComponentPopupMenu(BTD);
          AD17.setName("AD17");
          panel3.add(AD17);
          AD17.setBounds(449, 189, 34, 28);
          
          // ---- NUM8 ----
          NUM8.setText("@NUM8@");
          NUM8.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM8.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          NUM8.setComponentPopupMenu(BTD);
          NUM8.setName("NUM8");
          panel3.add(NUM8);
          NUM8.setBounds(25, 217, 35, 24);
          
          // ---- ENL108 ----
          ENL108.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL108.setComponentPopupMenu(BTD);
          ENL108.setName("ENL108");
          panel3.add(ENL108);
          ENL108.setBounds(60, 215, 185, 28);
          
          // ---- ENL208 ----
          ENL208.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL208.setComponentPopupMenu(BTD);
          ENL208.setName("ENL208");
          panel3.add(ENL208);
          ENL208.setBounds(245, 215, 30, 28);
          
          // ---- QTSX8 ----
          QTSX8.setText("@QTSX8@");
          QTSX8.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX8.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTSX8.setComponentPopupMenu(BTD);
          QTSX8.setName("QTSX8");
          panel3.add(QTSX8);
          QTSX8.setBounds(275, 217, 75, 24);
          
          // ---- QTPX8 ----
          QTPX8.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTPX8.setComponentPopupMenu(BTD);
          QTPX8.setName("QTPX8");
          panel3.add(QTPX8);
          QTPX8.setBounds(275, 215, 75, 28);
          
          // ---- QTEX8 ----
          QTEX8.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTEX8.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX8.setComponentPopupMenu(BTD);
          QTEX8.setName("QTEX8");
          panel3.add(QTEX8);
          QTEX8.setBounds(350, 215, 75, 28);
          
          // ---- ES8 ----
          ES8.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ES8.setComponentPopupMenu(BTD);
          ES8.setName("ES8");
          panel3.add(ES8);
          ES8.setBounds(425, 215, 20, 28);
          
          // ---- AD18 ----
          AD18.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD18.setComponentPopupMenu(BTD);
          AD18.setName("AD18");
          panel3.add(AD18);
          AD18.setBounds(449, 215, 34, 28);
          
          // ---- NUM9 ----
          NUM9.setText("@NUM9@");
          NUM9.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM9.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          NUM9.setComponentPopupMenu(BTD);
          NUM9.setName("NUM9");
          panel3.add(NUM9);
          NUM9.setBounds(25, 243, 35, 24);
          
          // ---- ENL109 ----
          ENL109.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL109.setComponentPopupMenu(BTD);
          ENL109.setName("ENL109");
          panel3.add(ENL109);
          ENL109.setBounds(60, 241, 185, 28);
          
          // ---- ENL209 ----
          ENL209.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL209.setComponentPopupMenu(BTD);
          ENL209.setName("ENL209");
          panel3.add(ENL209);
          ENL209.setBounds(245, 241, 30, 28);
          
          // ---- QTSX9 ----
          QTSX9.setText("@QTSX9@");
          QTSX9.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX9.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTSX9.setComponentPopupMenu(BTD);
          QTSX9.setName("QTSX9");
          panel3.add(QTSX9);
          QTSX9.setBounds(275, 243, 75, 24);
          
          // ---- QTPX9 ----
          QTPX9.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTPX9.setComponentPopupMenu(BTD);
          QTPX9.setName("QTPX9");
          panel3.add(QTPX9);
          QTPX9.setBounds(275, 241, 75, 28);
          
          // ---- QTEX9 ----
          QTEX9.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTEX9.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX9.setComponentPopupMenu(BTD);
          QTEX9.setName("QTEX9");
          panel3.add(QTEX9);
          QTEX9.setBounds(350, 241, 75, 28);
          
          // ---- ES9 ----
          ES9.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ES9.setComponentPopupMenu(BTD);
          ES9.setName("ES9");
          panel3.add(ES9);
          ES9.setBounds(425, 241, 20, 28);
          
          // ---- AD19 ----
          AD19.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD19.setComponentPopupMenu(BTD);
          AD19.setName("AD19");
          panel3.add(AD19);
          AD19.setBounds(449, 241, 34, 28);
          
          // ---- NUM10 ----
          NUM10.setText("@NUM10@");
          NUM10.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          NUM10.setComponentPopupMenu(BTD);
          NUM10.setName("NUM10");
          panel3.add(NUM10);
          NUM10.setBounds(25, 269, 35, 24);
          
          // ---- ENL110 ----
          ENL110.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL110.setComponentPopupMenu(BTD);
          ENL110.setName("ENL110");
          panel3.add(ENL110);
          ENL110.setBounds(60, 267, 185, 28);
          
          // ---- ENL210 ----
          ENL210.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ENL210.setComponentPopupMenu(BTD);
          ENL210.setName("ENL210");
          panel3.add(ENL210);
          ENL210.setBounds(245, 267, 30, 28);
          
          // ---- QTSX10 ----
          QTSX10.setText("@QTSX10@");
          QTSX10.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTSX10.setComponentPopupMenu(BTD);
          QTSX10.setName("QTSX10");
          panel3.add(QTSX10);
          QTSX10.setBounds(275, 269, 75, 24);
          
          // ---- QTPX10 ----
          QTPX10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTPX10.setComponentPopupMenu(BTD);
          QTPX10.setName("QTPX10");
          panel3.add(QTPX10);
          QTPX10.setBounds(275, 267, 75, 28);
          
          // ---- QTEX10 ----
          QTEX10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          QTEX10.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX10.setComponentPopupMenu(BTD);
          QTEX10.setName("QTEX10");
          panel3.add(QTEX10);
          QTEX10.setBounds(350, 267, 75, 28);
          
          // ---- ES10 ----
          ES10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          ES10.setComponentPopupMenu(BTD);
          ES10.setName("ES10");
          panel3.add(ES10);
          ES10.setBounds(425, 267, 20, 28);
          
          // ---- AD110 ----
          AD110.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD110.setComponentPopupMenu(BTD);
          AD110.setName("AD110");
          panel3.add(AD110);
          AD110.setBounds(449, 267, 34, 28);
          
          // ---- P21QPX ----
          P21QPX.setComponentPopupMenu(BTD);
          P21QPX.setText("@P21QPX@");
          P21QPX.setHorizontalAlignment(SwingConstants.RIGHT);
          P21QPX.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          P21QPX.setName("P21QPX");
          panel3.add(P21QPX);
          P21QPX.setBounds(275, 295, 75, P21QPX.getPreferredSize().height);
          
          // ---- AD21 ----
          AD21.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD21.setComponentPopupMenu(BTD);
          AD21.setName("AD21");
          panel3.add(AD21);
          AD21.setBounds(502, 33, 34, 28);
          
          // ---- AD22 ----
          AD22.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD22.setComponentPopupMenu(BTD);
          AD22.setName("AD22");
          panel3.add(AD22);
          AD22.setBounds(502, 59, 34, 28);
          
          // ---- AD23 ----
          AD23.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD23.setComponentPopupMenu(BTD);
          AD23.setName("AD23");
          panel3.add(AD23);
          AD23.setBounds(502, 85, 34, 28);
          
          // ---- AD24 ----
          AD24.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD24.setComponentPopupMenu(BTD);
          AD24.setName("AD24");
          panel3.add(AD24);
          AD24.setBounds(502, 111, 34, 28);
          
          // ---- AD25 ----
          AD25.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD25.setComponentPopupMenu(BTD);
          AD25.setName("AD25");
          panel3.add(AD25);
          AD25.setBounds(502, 137, 34, 28);
          
          // ---- AD26 ----
          AD26.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD26.setComponentPopupMenu(BTD);
          AD26.setName("AD26");
          panel3.add(AD26);
          AD26.setBounds(502, 163, 34, 28);
          
          // ---- AD27 ----
          AD27.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD27.setComponentPopupMenu(BTD);
          AD27.setName("AD27");
          panel3.add(AD27);
          AD27.setBounds(502, 189, 34, 28);
          
          // ---- AD28 ----
          AD28.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD28.setComponentPopupMenu(BTD);
          AD28.setName("AD28");
          panel3.add(AD28);
          AD28.setBounds(502, 215, 34, 28);
          
          // ---- AD29 ----
          AD29.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD29.setComponentPopupMenu(BTD);
          AD29.setName("AD29");
          panel3.add(AD29);
          AD29.setBounds(502, 240, 34, 28);
          
          // ---- AD210 ----
          AD210.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD210.setComponentPopupMenu(BTD);
          AD210.setName("AD210");
          panel3.add(AD210);
          AD210.setBounds(502, 265, 34, 28);
          
          // ---- AD31 ----
          AD31.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD31.setComponentPopupMenu(BTD);
          AD31.setName("AD31");
          panel3.add(AD31);
          AD31.setBounds(555, 33, 34, 28);
          
          // ---- AD32 ----
          AD32.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD32.setComponentPopupMenu(BTD);
          AD32.setName("AD32");
          panel3.add(AD32);
          AD32.setBounds(555, 59, 34, 28);
          
          // ---- AD33 ----
          AD33.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD33.setComponentPopupMenu(BTD);
          AD33.setName("AD33");
          panel3.add(AD33);
          AD33.setBounds(555, 85, 34, 28);
          
          // ---- AD34 ----
          AD34.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD34.setComponentPopupMenu(BTD);
          AD34.setName("AD34");
          panel3.add(AD34);
          AD34.setBounds(555, 111, 34, 28);
          
          // ---- AD35 ----
          AD35.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD35.setComponentPopupMenu(BTD);
          AD35.setName("AD35");
          panel3.add(AD35);
          AD35.setBounds(555, 137, 34, 28);
          
          // ---- AD36 ----
          AD36.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD36.setComponentPopupMenu(BTD);
          AD36.setName("AD36");
          panel3.add(AD36);
          AD36.setBounds(555, 163, 34, 28);
          
          // ---- AD37 ----
          AD37.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD37.setComponentPopupMenu(BTD);
          AD37.setName("AD37");
          panel3.add(AD37);
          AD37.setBounds(555, 189, 34, 28);
          
          // ---- AD38 ----
          AD38.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD38.setComponentPopupMenu(BTD);
          AD38.setName("AD38");
          panel3.add(AD38);
          AD38.setBounds(555, 215, 34, 28);
          
          // ---- AD39 ----
          AD39.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD39.setComponentPopupMenu(BTD);
          AD39.setName("AD39");
          panel3.add(AD39);
          AD39.setBounds(555, 240, 34, 28);
          
          // ---- AD310 ----
          AD310.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD310.setComponentPopupMenu(BTD);
          AD310.setName("AD310");
          panel3.add(AD310);
          AD310.setBounds(555, 265, 34, 28);
          
          // ---- AD41 ----
          AD41.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD41.setComponentPopupMenu(BTD);
          AD41.setName("AD41");
          panel3.add(AD41);
          AD41.setBounds(608, 33, 34, 28);
          
          // ---- AD42 ----
          AD42.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD42.setComponentPopupMenu(BTD);
          AD42.setName("AD42");
          panel3.add(AD42);
          AD42.setBounds(608, 59, 34, 28);
          
          // ---- AD43 ----
          AD43.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD43.setComponentPopupMenu(BTD);
          AD43.setName("AD43");
          panel3.add(AD43);
          AD43.setBounds(608, 85, 34, 28);
          
          // ---- AD44 ----
          AD44.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD44.setComponentPopupMenu(BTD);
          AD44.setName("AD44");
          panel3.add(AD44);
          AD44.setBounds(608, 111, 34, 28);
          
          // ---- AD45 ----
          AD45.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD45.setComponentPopupMenu(BTD);
          AD45.setName("AD45");
          panel3.add(AD45);
          AD45.setBounds(608, 137, 34, 28);
          
          // ---- AD46 ----
          AD46.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD46.setComponentPopupMenu(BTD);
          AD46.setName("AD46");
          panel3.add(AD46);
          AD46.setBounds(608, 163, 34, 28);
          
          // ---- AD47 ----
          AD47.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD47.setComponentPopupMenu(BTD);
          AD47.setName("AD47");
          panel3.add(AD47);
          AD47.setBounds(608, 189, 34, 28);
          
          // ---- AD48 ----
          AD48.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD48.setComponentPopupMenu(BTD);
          AD48.setName("AD48");
          panel3.add(AD48);
          AD48.setBounds(608, 215, 34, 28);
          
          // ---- AD49 ----
          AD49.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD49.setComponentPopupMenu(BTD);
          AD49.setName("AD49");
          panel3.add(AD49);
          AD49.setBounds(608, 240, 34, 28);
          
          // ---- AD410 ----
          AD410.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          AD410.setComponentPopupMenu(BTD);
          AD410.setName("AD410");
          panel3.add(AD410);
          AD410.setBounds(608, 265, 34, 28);
          
          // ---- LD2X1 ----
          LD2X1.setToolTipText("@LD2X1@");
          LD2X1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          LD2X1.setComponentPopupMenu(BTD);
          LD2X1.setName("LD2X1");
          panel3.add(LD2X1);
          LD2X1.setBounds(655, 33, 350, 28);
          
          // ---- LD2X2 ----
          LD2X2.setToolTipText("@LD2X2@");
          LD2X2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          LD2X2.setComponentPopupMenu(BTD);
          LD2X2.setName("LD2X2");
          panel3.add(LD2X2);
          LD2X2.setBounds(655, 59, 350, 28);
          
          // ---- LD2X3 ----
          LD2X3.setToolTipText("@LD2X3@");
          LD2X3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          LD2X3.setComponentPopupMenu(BTD);
          LD2X3.setName("LD2X3");
          panel3.add(LD2X3);
          LD2X3.setBounds(655, 85, 350, 28);
          
          // ---- LD2X4 ----
          LD2X4.setToolTipText("@LD2X4@");
          LD2X4.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          LD2X4.setComponentPopupMenu(BTD);
          LD2X4.setName("LD2X4");
          panel3.add(LD2X4);
          LD2X4.setBounds(655, 111, 350, 28);
          
          // ---- LD2X5 ----
          LD2X5.setToolTipText("@LD2X5@");
          LD2X5.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          LD2X5.setComponentPopupMenu(BTD);
          LD2X5.setName("LD2X5");
          panel3.add(LD2X5);
          LD2X5.setBounds(655, 137, 350, 28);
          
          // ---- LD2X6 ----
          LD2X6.setToolTipText("@LD2X6@");
          LD2X6.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          LD2X6.setComponentPopupMenu(BTD);
          LD2X6.setName("LD2X6");
          panel3.add(LD2X6);
          LD2X6.setBounds(655, 163, 350, 28);
          
          // ---- LD2X7 ----
          LD2X7.setToolTipText("@LD2X7@");
          LD2X7.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          LD2X7.setComponentPopupMenu(BTD);
          LD2X7.setName("LD2X7");
          panel3.add(LD2X7);
          LD2X7.setBounds(655, 189, 350, 28);
          
          // ---- LD2X8 ----
          LD2X8.setToolTipText("@LD2X8@");
          LD2X8.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          LD2X8.setComponentPopupMenu(BTD);
          LD2X8.setName("LD2X8");
          panel3.add(LD2X8);
          LD2X8.setBounds(655, 215, 350, 28);
          
          // ---- LD2X9 ----
          LD2X9.setToolTipText("@LD2X9@");
          LD2X9.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          LD2X9.setComponentPopupMenu(BTD);
          LD2X9.setName("LD2X9");
          panel3.add(LD2X9);
          LD2X9.setBounds(655, 240, 350, 28);
          
          // ---- LD2X10 ----
          LD2X10.setToolTipText("@LD2X10@");
          LD2X10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          LD2X10.setComponentPopupMenu(BTD);
          LD2X10.setName("LD2X10");
          panel3.add(LD2X10);
          LD2X10.setBounds(655, 265, 350, 28);
          
          // ---- P21QTX ----
          P21QTX.setComponentPopupMenu(BTD);
          P21QTX.setText("@P21QTX@");
          P21QTX.setHorizontalAlignment(SwingConstants.RIGHT);
          P21QTX.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          P21QTX.setName("P21QTX");
          panel3.add(P21QTX);
          P21QTX.setBounds(350, 295, 75, 24);
          
          // ---- BT_PGUP ----
          BT_PGUP.setName("BT_PGUP");
          panel3.add(BT_PGUP);
          BT_PGUP.setBounds(1015, 35, 25, 110);
          
          // ---- BT_PGDOWN ----
          BT_PGDOWN.setName("BT_PGDOWN");
          panel3.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(1015, 180, 25, 110);
          
          // ---- label1 ----
          label1.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label1.setText("label1");
          label1.setName("label1");
          panel3.add(label1);
          label1.setBounds(25, 15, 35, 20);
          
          // ---- label2 ----
          label2.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label2.setText("label1");
          label2.setName("label2");
          panel3.add(label2);
          label2.setBounds(60, 15, 185, 20);
          
          // ---- label3 ----
          label3.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label3.setText("label1");
          label3.setName("label3");
          panel3.add(label3);
          label3.setBounds(245, 15, 30, 20);
          
          // ---- label4 ----
          label4.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label4.setText("label1");
          label4.setName("label4");
          panel3.add(label4);
          label4.setBounds(275, 15, 75, 20);
          
          // ---- label5 ----
          label5.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label5.setText("label1");
          label5.setName("label5");
          panel3.add(label5);
          label5.setBounds(350, 15, 75, 20);
          
          // ---- label6 ----
          label6.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label6.setText("label1");
          label6.setHorizontalAlignment(SwingConstants.CENTER);
          label6.setName("label6");
          panel3.add(label6);
          label6.setBounds(425, 15, 20, 20);
          
          // ---- label7 ----
          label7.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label7.setText("label1");
          label7.setHorizontalAlignment(SwingConstants.CENTER);
          label7.setName("label7");
          panel3.add(label7);
          label7.setBounds(441, 15, 50, 20);
          
          // ---- label8 ----
          label8.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label8.setText("label1");
          label8.setHorizontalAlignment(SwingConstants.CENTER);
          label8.setName("label8");
          panel3.add(label8);
          label8.setBounds(494, 15, 50, 20);
          
          // ---- label9 ----
          label9.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label9.setText("label1");
          label9.setHorizontalAlignment(SwingConstants.CENTER);
          label9.setName("label9");
          panel3.add(label9);
          label9.setBounds(547, 15, 50, 20);
          
          // ---- label10 ----
          label10.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label10.setText("label1");
          label10.setHorizontalAlignment(SwingConstants.CENTER);
          label10.setName("label10");
          panel3.add(label10);
          label10.setBounds(600, 15, 50, 20);
          
          // ---- label11 ----
          label11.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label11.setText("label1");
          label11.setName("label11");
          panel3.add(label11);
          label11.setBounds(660, 15, 345, 20);
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING,
            p_contenuLayout.createSequentialGroup().addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(panel3, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 1055, Short.MAX_VALUE)
                    .addComponent(panel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 1055, Short.MAX_VALUE))
                .addContainerGap()));
        p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup().addGap(10, 10, 10)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 339, GroupLayout.PREFERRED_SIZE).addGap(16, 16, 16)));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");
      
      // ---- lb_a_virer ----
      lb_a_virer.setText("@A1LIB@");
      lb_a_virer.setForeground(Color.black);
      lb_a_virer.setFont(lb_a_virer.getFont().deriveFont(lb_a_virer.getFont().getStyle() | Font.BOLD));
      lb_a_virer.setName("lb_a_virer");
      barre_tete.add(lb_a_virer);
    }
    add(barre_tete, BorderLayout.NORTH);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_13 ----
      OBJ_13.setText("D\u00e9coupe");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
      
      // ---- OBJ_14 ----
      OBJ_14.setText("Annulation de d\u00e9coupe");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
      BTD.addSeparator();
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_contenu;
  private JPanel panel2;
  private JLabel OBJ_34;
  private RiZoneSortie A1CNDX;
  private RiZoneSortie OBJ_36;
  private JLabel OBJ_31;
  private RiZoneSortie WDISX;
  private RiZoneSortie WDISUN;
  private RiZoneSortie OBJ_50;
  private JLabel OBJ_37;
  private JLabel OBJ_48;
  private RiZoneSortie OBJ_47;
  private RiZoneSortie OBJ_49;
  private RiZoneSortie WCLPL;
  private JLabel OBJ_46;
  private RiZoneSortie X1PPC;
  private RiZoneSortie X1PPL;
  private RiZoneSortie X1PPP;
  private RiZoneSortie OBJ_45;
  private JLabel OBJ_43;
  private JLabel OBJ_39;
  private JLabel OBJ_41;
  private JPanel panel3;
  private RiZoneSortie NUM1;
  private XRiTextField ENL101;
  private XRiTextField ENL201;
  private RiZoneSortie QTSX1;
  private XRiTextField QTPX1;
  private XRiTextField QTEX1;
  private XRiTextField ES01;
  private XRiTextField AD11;
  private RiZoneSortie NUM2;
  private XRiTextField ENL102;
  private XRiTextField ENL202;
  private RiZoneSortie QTSX2;
  private XRiTextField QTPX2;
  private XRiTextField QTEX2;
  private XRiTextField ES2;
  private XRiTextField AD12;
  private RiZoneSortie NUM3;
  private XRiTextField ENL103;
  private XRiTextField ENL203;
  private RiZoneSortie QTSX3;
  private XRiTextField QTPX3;
  private XRiTextField QTEX3;
  private XRiTextField ES3;
  private XRiTextField AD13;
  private RiZoneSortie NUM4;
  private XRiTextField ENL104;
  private XRiTextField ENL204;
  private RiZoneSortie QTSX4;
  private XRiTextField QTPX4;
  private XRiTextField QTEX4;
  private XRiTextField ES4;
  private XRiTextField AD14;
  private RiZoneSortie NUM5;
  private XRiTextField ENL105;
  private XRiTextField ENL205;
  private RiZoneSortie QTSX5;
  private XRiTextField QTPX5;
  private XRiTextField QTEX5;
  private XRiTextField ES5;
  private XRiTextField AD15;
  private RiZoneSortie NUM6;
  private XRiTextField ENL106;
  private XRiTextField ENL206;
  private RiZoneSortie QTSX6;
  private XRiTextField QTPX6;
  private XRiTextField QTEX6;
  private XRiTextField ES6;
  private XRiTextField AD16;
  private RiZoneSortie NUM7;
  private XRiTextField ENL107;
  private XRiTextField ENL207;
  private RiZoneSortie QTSX7;
  private XRiTextField QTPX7;
  private XRiTextField QTEX7;
  private XRiTextField ES7;
  private XRiTextField AD17;
  private RiZoneSortie NUM8;
  private XRiTextField ENL108;
  private XRiTextField ENL208;
  private RiZoneSortie QTSX8;
  private XRiTextField QTPX8;
  private XRiTextField QTEX8;
  private XRiTextField ES8;
  private XRiTextField AD18;
  private RiZoneSortie NUM9;
  private XRiTextField ENL109;
  private XRiTextField ENL209;
  private RiZoneSortie QTSX9;
  private XRiTextField QTPX9;
  private XRiTextField QTEX9;
  private XRiTextField ES9;
  private XRiTextField AD19;
  private RiZoneSortie NUM10;
  private XRiTextField ENL110;
  private XRiTextField ENL210;
  private RiZoneSortie QTSX10;
  private XRiTextField QTPX10;
  private XRiTextField QTEX10;
  private XRiTextField ES10;
  private XRiTextField AD110;
  private RiZoneSortie P21QPX;
  private XRiTextField AD21;
  private XRiTextField AD22;
  private XRiTextField AD23;
  private XRiTextField AD24;
  private XRiTextField AD25;
  private XRiTextField AD26;
  private XRiTextField AD27;
  private XRiTextField AD28;
  private XRiTextField AD29;
  private XRiTextField AD210;
  private XRiTextField AD31;
  private XRiTextField AD32;
  private XRiTextField AD33;
  private XRiTextField AD34;
  private XRiTextField AD35;
  private XRiTextField AD36;
  private XRiTextField AD37;
  private XRiTextField AD38;
  private XRiTextField AD39;
  private XRiTextField AD310;
  private XRiTextField AD41;
  private XRiTextField AD42;
  private XRiTextField AD43;
  private XRiTextField AD44;
  private XRiTextField AD45;
  private XRiTextField AD46;
  private XRiTextField AD47;
  private XRiTextField AD48;
  private XRiTextField AD49;
  private XRiTextField AD410;
  private XRiTextField LD2X1;
  private XRiTextField LD2X2;
  private XRiTextField LD2X3;
  private XRiTextField LD2X4;
  private XRiTextField LD2X5;
  private XRiTextField LD2X6;
  private XRiTextField LD2X7;
  private XRiTextField LD2X8;
  private XRiTextField LD2X9;
  private XRiTextField LD2X10;
  private RiZoneSortie P21QTX;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JMenuBar barre_tete;
  private JLabel lb_a_virer;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
