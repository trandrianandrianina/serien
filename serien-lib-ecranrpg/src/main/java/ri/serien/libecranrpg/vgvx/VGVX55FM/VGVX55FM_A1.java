
package ri.serien.libecranrpg.vgvx.VGVX55FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX55FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX55FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX4.setValeurs("4", "RB");
    TIDXB.setValeurs("11", "RB");
    TIDX9.setValeurs("9", "RB");
    TIDX7.setValeurs("7", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX1.setValeurs("1", "RB");
    TIDXA.setValeurs("10", "RB");
    TIDXC.setValeurs("12", "RB");
    TIDX6.setValeurs("6", "RB");
    TIDX8.setValeurs("8", "RB");
    SELPAB.setValeursSelection("X", " ");
    SELPOI.setValeursSelection("X", " ");
    SELPAA.setValeursSelection("X", " ");
    SELPXA.setValeursSelection("X", " ");
    SELPOS.setValeursSelection("X", " ");
    SELPUM.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    


    
    
    // TIDX4.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("4"));
    // TIDXB.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("11"));
    // TIDX9.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("9"));
    // TIDX7.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("7"));
    // TIDX5.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    // TIDX2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // TIDX3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // TIDX1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // TIDXA.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("10"));
    // TIDXC.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("12"));
    // TIDX6.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("6"));
    // SELPAB.setSelected(lexique.HostFieldGetData("SELPAB").equalsIgnoreCase("X"));
    // SELPOI.setSelected(lexique.HostFieldGetData("SELPOI").equalsIgnoreCase("X"));
    // SELPAA.setSelected(lexique.HostFieldGetData("SELPAA").equalsIgnoreCase("X"));
    // SELPXA.setSelected(lexique.HostFieldGetData("SELPXA").equalsIgnoreCase("X"));
    // SELPOS.setSelected(lexique.HostFieldGetData("SELPOS").equalsIgnoreCase("X"));
    // TIDX8.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("8"));
    // SELPUM.setSelected(lexique.HostFieldGetData("SELPUM").equalsIgnoreCase("X"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TIDX4.isSelected())
    // lexique.HostFieldPutData("RB", 0, "4");
    // if (TIDXB.isSelected())
    // lexique.HostFieldPutData("RB", 0, "11");
    // if (TIDX9.isSelected())
    // lexique.HostFieldPutData("RB", 0, "9");
    // if (TIDX7.isSelected())
    // lexique.HostFieldPutData("RB", 0, "7");
    // if (TIDX5.isSelected())
    // lexique.HostFieldPutData("RB", 0, "5");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (TIDX3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (TIDX1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (TIDXA.isSelected())
    // lexique.HostFieldPutData("RB", 0, "10");
    // if (TIDXC.isSelected())
    // lexique.HostFieldPutData("RB", 0, "12");
    // if (TIDX6.isSelected())
    // lexique.HostFieldPutData("RB", 0, "6");
    // if (SELPAB.isSelected())
    // lexique.HostFieldPutData("SELPAB", 0, "X");
    // else
    // lexique.HostFieldPutData("SELPAB", 0, " ");
    // if (SELPOI.isSelected())
    // lexique.HostFieldPutData("SELPOI", 0, "X");
    // else
    // lexique.HostFieldPutData("SELPOI", 0, " ");
    // if (SELPAA.isSelected())
    // lexique.HostFieldPutData("SELPAA", 0, "X");
    // else
    // lexique.HostFieldPutData("SELPAA", 0, " ");
    // if (SELPXA.isSelected())
    // lexique.HostFieldPutData("SELPXA", 0, "X");
    // else
    // lexique.HostFieldPutData("SELPXA", 0, " ");
    // if (SELPOS.isSelected())
    // lexique.HostFieldPutData("SELPOS", 0, "X");
    // else
    // lexique.HostFieldPutData("SELPOS", 0, " ");
    // if (TIDX8.isSelected())
    // lexique.HostFieldPutData("RB", 0, "8");
    // if (SELPUM.isSelected())
    // lexique.HostFieldPutData("SELPUM", 0, "X");
    // else
    // lexique.HostFieldPutData("SELPUM", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_29 = new JLabel();
    DEMETB = new XRiTextField();
    OBJ_30 = new JLabel();
    INITIA = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SELPUM = new XRiCheckBox();
    TIDX8 = new XRiRadioButton();
    SELPOS = new XRiCheckBox();
    SELPXA = new XRiCheckBox();
    SELPAA = new XRiCheckBox();
    SELPOI = new XRiCheckBox();
    SELPAB = new XRiCheckBox();
    TIDX6 = new XRiRadioButton();
    TIDXC = new XRiRadioButton();
    TIDXA = new XRiRadioButton();
    ARTDEB = new XRiTextField();
    ARTFIN = new XRiTextField();
    TIDX1 = new XRiRadioButton();
    OBJ_75 = new JLabel();
    TIDX3 = new XRiRadioButton();
    TIDX2 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    TIDX9 = new XRiRadioButton();
    TIDXB = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    PERDEB = new XRiCalendrier();
    PERFIN = new XRiCalendrier();
    FRSDEB = new XRiTextField();
    FRSFIN = new XRiTextField();
    GFADEB = new XRiTextField();
    GFAFIN = new XRiTextField();
    SABC1 = new XRiTextField();
    SABC2 = new XRiTextField();
    SABC3 = new XRiTextField();
    SABC4 = new XRiTextField();
    SABC5 = new XRiTextField();
    separator1 = compFactory.createSeparator("Prix net d'achat ");
    separator4 = compFactory.createSeparator("P.U.M.P");
    separator3 = compFactory.createSeparator("Pourcentage prix de vente");
    separator2 = compFactory.createSeparator("Prix de vente");
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Validation des tarifs de vente");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_29 ----
          OBJ_29.setText("Etablissement");
          OBJ_29.setName("OBJ_29");

          //---- DEMETB ----
          DEMETB.setComponentPopupMenu(BTD);
          DEMETB.setName("DEMETB");

          //---- OBJ_30 ----
          OBJ_30.setText("Initiales validation");
          OBJ_30.setName("OBJ_30");

          //---- INITIA ----
          INITIA.setComponentPopupMenu(BTD);
          INITIA.setName("INITIA");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INITIA, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_29))
              .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_30))
              .addComponent(INITIA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Options de recherche"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- SELPUM ----
            SELPUM.setText("S\u00e9lection \u00e9cart PUMP prix de vente nouveau");
            SELPUM.setComponentPopupMenu(BTD);
            SELPUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SELPUM.setName("SELPUM");
            panel1.add(SELPUM);
            SELPUM.setBounds(350, 435, 265, 20);

            //---- TIDX8 ----
            TIDX8.setText("Croissant");
            TIDX8.setComponentPopupMenu(null);
            TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX8.setName("TIDX8");
            panel1.add(TIDX8);
            TIDX8.setBounds(115, 355, 219, 20);

            //---- SELPOS ----
            SELPOS.setText("S\u00e9lection hausse de prix");
            SELPOS.setComponentPopupMenu(BTD);
            SELPOS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SELPOS.setName("SELPOS");
            panel1.add(SELPOS);
            SELPOS.setBounds(350, 355, 219, 20);

            //---- SELPXA ----
            SELPXA.setText("Sans articles \"nouveaux\"");
            SELPXA.setComponentPopupMenu(BTD);
            SELPXA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SELPXA.setName("SELPXA");
            panel1.add(SELPXA);
            SELPXA.setBounds(350, 97, 214, 20);

            //---- SELPAA ----
            SELPAA.setText("S\u00e9lection hausse prix net achat");
            SELPAA.setComponentPopupMenu(BTD);
            SELPAA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SELPAA.setName("SELPAA");
            panel1.add(SELPAA);
            SELPAA.setBounds(350, 205, 214, 20);

            //---- SELPOI ----
            SELPOI.setText("S\u00e9lection baisse de prix");
            SELPOI.setComponentPopupMenu(BTD);
            SELPOI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SELPOI.setName("SELPOI");
            panel1.add(SELPOI);
            SELPOI.setBounds(350, 380, 210, 20);

            //---- SELPAB ----
            SELPAB.setText("S\u00e9lection baisse prix net achat");
            SELPAB.setComponentPopupMenu(BTD);
            SELPAB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SELPAB.setName("SELPAB");
            panel1.add(SELPAB);
            SELPAB.setBounds(350, 230, 200, 20);

            //---- TIDX6 ----
            TIDX6.setText("Croissant");
            TIDX6.setComponentPopupMenu(null);
            TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX6.setName("TIDX6");
            panel1.add(TIDX6);
            TIDX6.setBounds(110, 275, 219, 20);

            //---- TIDXC ----
            TIDXC.setText("P\u00e9riode date d'application");
            TIDXC.setComponentPopupMenu(null);
            TIDXC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDXC.setName("TIDXC");
            panel1.add(TIDXC);
            TIDXC.setBounds(110, 35, 219, 20);

            //---- TIDXA ----
            TIDXA.setText("Croissant");
            TIDXA.setComponentPopupMenu(null);
            TIDXA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDXA.setName("TIDXA");
            panel1.add(TIDXA);
            TIDXA.setBounds(115, 435, 219, 20);

            //---- ARTDEB ----
            ARTDEB.setComponentPopupMenu(BTD);
            ARTDEB.setName("ARTDEB");
            panel1.add(ARTDEB);
            ARTDEB.setBounds(350, 62, 210, ARTDEB.getPreferredSize().height);

            //---- ARTFIN ----
            ARTFIN.setComponentPopupMenu(BTD);
            ARTFIN.setName("ARTFIN");
            panel1.add(ARTFIN);
            ARTFIN.setBounds(565, 62, 210, ARTFIN.getPreferredSize().height);

            //---- TIDX1 ----
            TIDX1.setText("Code article interne");
            TIDX1.setComponentPopupMenu(null);
            TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX1.setName("TIDX1");
            panel1.add(TIDX1);
            TIDX1.setBounds(110, 66, 219, 20);

            //---- OBJ_75 ----
            OBJ_75.setText("S\u00e9lection code abc");
            OBJ_75.setComponentPopupMenu(null);
            OBJ_75.setName("OBJ_75");
            panel1.add(OBJ_75);
            OBJ_75.setBounds(350, 465, 137, 20);

            //---- TIDX3 ----
            TIDX3.setText("Code fournisseur");
            TIDX3.setComponentPopupMenu(null);
            TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX3.setName("TIDX3");
            panel1.add(TIDX3);
            TIDX3.setBounds(110, 159, 219, 20);

            //---- TIDX2 ----
            TIDX2.setText("Code famille");
            TIDX2.setComponentPopupMenu(null);
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setName("TIDX2");
            panel1.add(TIDX2);
            TIDX2.setBounds(110, 128, 219, 20);

            //---- TIDX5 ----
            TIDX5.setText("D\u00e9croissant");
            TIDX5.setComponentPopupMenu(null);
            TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX5.setName("TIDX5");
            panel1.add(TIDX5);
            TIDX5.setBounds(110, 230, 219, 20);

            //---- TIDX7 ----
            TIDX7.setText("D\u00e9croissant");
            TIDX7.setComponentPopupMenu(null);
            TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX7.setName("TIDX7");
            panel1.add(TIDX7);
            TIDX7.setBounds(110, 300, 219, 20);

            //---- TIDX9 ----
            TIDX9.setText("D\u00e9croissant");
            TIDX9.setComponentPopupMenu(null);
            TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX9.setName("TIDX9");
            panel1.add(TIDX9);
            TIDX9.setBounds(115, 380, 219, 20);

            //---- TIDXB ----
            TIDXB.setText("D\u00e9croissant");
            TIDXB.setComponentPopupMenu(null);
            TIDXB.setName("TIDXB");
            panel1.add(TIDXB);
            TIDXB.setBounds(115, 460, 219, 20);

            //---- TIDX4 ----
            TIDX4.setText("Croissant");
            TIDX4.setComponentPopupMenu(null);
            TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX4.setName("TIDX4");
            panel1.add(TIDX4);
            TIDX4.setBounds(110, 205, 219, 20);

            //---- PERDEB ----
            PERDEB.setComponentPopupMenu(BTD);
            PERDEB.setName("PERDEB");
            panel1.add(PERDEB);
            PERDEB.setBounds(350, 30, 105, PERDEB.getPreferredSize().height);

            //---- PERFIN ----
            PERFIN.setComponentPopupMenu(BTD);
            PERFIN.setName("PERFIN");
            panel1.add(PERFIN);
            PERFIN.setBounds(480, 30, 105, PERFIN.getPreferredSize().height);

            //---- FRSDEB ----
            FRSDEB.setComponentPopupMenu(BTD);
            FRSDEB.setName("FRSDEB");
            panel1.add(FRSDEB);
            FRSDEB.setBounds(350, 155, 66, FRSDEB.getPreferredSize().height);

            //---- FRSFIN ----
            FRSFIN.setComponentPopupMenu(BTD);
            FRSFIN.setName("FRSFIN");
            panel1.add(FRSFIN);
            FRSFIN.setBounds(425, 155, 66, FRSFIN.getPreferredSize().height);

            //---- GFADEB ----
            GFADEB.setComponentPopupMenu(BTD);
            GFADEB.setName("GFADEB");
            panel1.add(GFADEB);
            GFADEB.setBounds(350, 124, 40, GFADEB.getPreferredSize().height);

            //---- GFAFIN ----
            GFAFIN.setComponentPopupMenu(BTD);
            GFAFIN.setName("GFAFIN");
            panel1.add(GFAFIN);
            GFAFIN.setBounds(425, 124, 40, GFAFIN.getPreferredSize().height);

            //---- SABC1 ----
            SABC1.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            SABC1.setComponentPopupMenu(null);
            SABC1.setName("SABC1");
            panel1.add(SABC1);
            SABC1.setBounds(505, 461, 20, SABC1.getPreferredSize().height);

            //---- SABC2 ----
            SABC2.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            SABC2.setComponentPopupMenu(null);
            SABC2.setName("SABC2");
            panel1.add(SABC2);
            SABC2.setBounds(533, 461, 20, SABC2.getPreferredSize().height);

            //---- SABC3 ----
            SABC3.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            SABC3.setComponentPopupMenu(null);
            SABC3.setName("SABC3");
            panel1.add(SABC3);
            SABC3.setBounds(561, 461, 20, SABC3.getPreferredSize().height);

            //---- SABC4 ----
            SABC4.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            SABC4.setComponentPopupMenu(null);
            SABC4.setName("SABC4");
            panel1.add(SABC4);
            SABC4.setBounds(589, 461, 20, SABC4.getPreferredSize().height);

            //---- SABC5 ----
            SABC5.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            SABC5.setComponentPopupMenu(null);
            SABC5.setName("SABC5");
            panel1.add(SABC5);
            SABC5.setBounds(617, 461, 20, SABC5.getPreferredSize().height);

            //---- separator1 ----
            separator1.setName("separator1");
            panel1.add(separator1);
            separator1.setBounds(30, 185, 755, separator1.getPreferredSize().height);

            //---- separator4 ----
            separator4.setName("separator4");
            panel1.add(separator4);
            separator4.setBounds(30, 415, 755, separator4.getPreferredSize().height);

            //---- separator3 ----
            separator3.setName("separator3");
            panel1.add(separator3);
            separator3.setBounds(30, 335, 755, separator3.getPreferredSize().height);

            //---- separator2 ----
            separator2.setName("separator2");
            panel1.add(separator2);
            separator2.setBounds(30, 255, 755, separator2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 813, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 518, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Invite");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //---- RB ----
    ButtonGroup RB = new ButtonGroup();
    RB.add(TIDX8);
    RB.add(TIDX6);
    RB.add(TIDXC);
    RB.add(TIDXA);
    RB.add(TIDX1);
    RB.add(TIDX3);
    RB.add(TIDX2);
    RB.add(TIDX5);
    RB.add(TIDX7);
    RB.add(TIDX9);
    RB.add(TIDXB);
    RB.add(TIDX4);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_29;
  private XRiTextField DEMETB;
  private JLabel OBJ_30;
  private XRiTextField INITIA;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiCheckBox SELPUM;
  private XRiRadioButton TIDX8;
  private XRiCheckBox SELPOS;
  private XRiCheckBox SELPXA;
  private XRiCheckBox SELPAA;
  private XRiCheckBox SELPOI;
  private XRiCheckBox SELPAB;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDXC;
  private XRiRadioButton TIDXA;
  private XRiTextField ARTDEB;
  private XRiTextField ARTFIN;
  private XRiRadioButton TIDX1;
  private JLabel OBJ_75;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX5;
  private XRiRadioButton TIDX7;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDXB;
  private XRiRadioButton TIDX4;
  private XRiCalendrier PERDEB;
  private XRiCalendrier PERFIN;
  private XRiTextField FRSDEB;
  private XRiTextField FRSFIN;
  private XRiTextField GFADEB;
  private XRiTextField GFAFIN;
  private XRiTextField SABC1;
  private XRiTextField SABC2;
  private XRiTextField SABC3;
  private XRiTextField SABC4;
  private XRiTextField SABC5;
  private JComponent separator1;
  private JComponent separator4;
  private JComponent separator3;
  private JComponent separator2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
