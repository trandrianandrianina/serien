
package ri.serien.libecranrpg.vgvx.VGVX13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX13FM_A7 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WT301_Title = { "HLD01", };
  private String[] _WT301_Top = { "WT301", "WT302", "WT303", "WT304", "WT305", "WT306", "WT307", "WT308", "WT309", "WT310", "WT311",
      "WT312", "WT313", "WT314", "WT315", "WT316", "WT317", "WT318", "WT319", "WT320", "WT321", "WT322" };
  private String[][] _WT301_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, { "LD16", }, { "LD17", }, { "LD18", },
      { "LD19", }, { "LD20", }, { "LD21", }, { "LD22", }, };
  private int[] _WT301_Width = { 600, };
  
  public VGVX13FM_A7(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTYPE3.setValeursSelection("X", " ");
    WTYPE2.setValeursSelection("X", " ");
    WTYPE1.setValeursSelection("X", " ");
    WOPE4.setValeursSelection("X", " ");
    WOPE1.setValeursSelection("X", " ");
    WOPE2.setValeursSelection("X", " ");
    WOPE5.setValeursSelection("X", " ");
    WOPE3.setValeursSelection("X", " ");
    WTYPE4.setValeursSelection("X", " ");
    WT301.setAspectTable(_WT301_Top, _WT301_Title, _WT301_Data, _WT301_Width, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel2.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("Tous les magasins - article @A1LIB@ @A1LB1@ @A1LB2@ @A1LB3@")).trim()));
    OBJ_98.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEB@")).trim());
    OBJ_99.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1CL2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    OBJ_98.setVisible(lexique.isPresent("WEB"));
    OBJ_88.setVisible(lexique.isPresent("SERDEB"));
    WOPE4.setVisible(!lexique.HostFieldGetData("WTYPE3").trim().equalsIgnoreCase(""));
    WOPE1.setVisible(!lexique.HostFieldGetData("WTYPE3").trim().equalsIgnoreCase(""));
    WOPE2.setVisible(!lexique.HostFieldGetData("WTYPE3").trim().equalsIgnoreCase(""));
    WOPE5.setVisible(!lexique.HostFieldGetData("WTYPE3").trim().equalsIgnoreCase(""));
    OBJ_50.setVisible(lexique.HostFieldGetData("DIS").equalsIgnoreCase("Dis"));
    WOPE3.setVisible(!lexique.HostFieldGetData("WTYPE3").trim().equalsIgnoreCase(""));
    X13DVX.setVisible(lexique.HostFieldGetData("DIS").equalsIgnoreCase("Dis"));
    
    // double quantité
    if (lexique.isTrue("N20")) {
      OBJ_48.setText("En unité de stock");
    }
    else {
      OBJ_48.setText("Stock en double quantité");
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WT301.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    WT301.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void WT301MouseClicked(MouseEvent e) {
    if (WT301.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    H2ART = new XRiTextField();
    OBJ_67 = new JLabel();
    OBJ_66 = new JLabel();
    H2ETB = new XRiTextField();
    OBJ_49 = new JLabel();
    A1UNS = new XRiTextField();
    UNLIB = new XRiTextField();
    OBJ_50 = new JLabel();
    X13DVX = new XRiTextField();
    p_tete_droite = new JPanel();
    OBJ_48 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WTIE1 = new XRiTextField();
    WTIE2 = new XRiTextField();
    WTIE3 = new XRiTextField();
    label1 = new JLabel();
    panel2 = new JPanel();
    OBJ_88 = new JLabel();
    SERDEB = new XRiTextField();
    MARDEB = new XRiTextField();
    SCROLLPANE_LIST1 = new JScrollPane();
    WT301 = new XRiTable();
    OBJ_98 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_99 = new JLabel();
    panel3 = new JPanel();
    WTYPE4 = new XRiCheckBox();
    WOPE3 = new XRiCheckBox();
    WOPE5 = new XRiCheckBox();
    WOPE2 = new XRiCheckBox();
    WOPE1 = new XRiCheckBox();
    WOPE4 = new XRiCheckBox();
    WTYPE1 = new XRiCheckBox();
    WTYPE2 = new XRiCheckBox();
    WTYPE3 = new XRiCheckBox();
    OBJ_76 = new JLabel();
    DATDEB = new XRiCalendrier();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Stocks tous magasins");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- H2ART ----
          H2ART.setToolTipText("Code article");
          H2ART.setName("H2ART");
          p_tete_gauche.add(H2ART);
          H2ART.setBounds(205, 1, 210, H2ART.getPreferredSize().height);

          //---- OBJ_67 ----
          OBJ_67.setText("Article");
          OBJ_67.setName("OBJ_67");
          p_tete_gauche.add(OBJ_67);
          OBJ_67.setBounds(160, 6, 48, 18);

          //---- OBJ_66 ----
          OBJ_66.setText("Etablissement");
          OBJ_66.setName("OBJ_66");
          p_tete_gauche.add(OBJ_66);
          OBJ_66.setBounds(5, 6, 90, 18);

          //---- H2ETB ----
          H2ETB.setToolTipText("Code \u00e9tablissement");
          H2ETB.setName("H2ETB");
          p_tete_gauche.add(H2ETB);
          H2ETB.setBounds(100, 1, 40, H2ETB.getPreferredSize().height);

          //---- OBJ_49 ----
          OBJ_49.setText("Unit\u00e9 de stock");
          OBJ_49.setName("OBJ_49");
          p_tete_gauche.add(OBJ_49);
          OBJ_49.setBounds(430, 6, 89, 18);

          //---- A1UNS ----
          A1UNS.setName("A1UNS");
          p_tete_gauche.add(A1UNS);
          A1UNS.setBounds(525, 1, 34, A1UNS.getPreferredSize().height);

          //---- UNLIB ----
          UNLIB.setName("UNLIB");
          p_tete_gauche.add(UNLIB);
          UNLIB.setBounds(560, 1, 95, UNLIB.getPreferredSize().height);

          //---- OBJ_50 ----
          OBJ_50.setText("Disponible");
          OBJ_50.setName("OBJ_50");
          p_tete_gauche.add(OBJ_50);
          OBJ_50.setBounds(700, 6, 70, 18);

          //---- X13DVX ----
          X13DVX.setName("X13DVX");
          p_tete_gauche.add(X13DVX);
          X13DVX.setBounds(775, 1, 95, X13DVX.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_48 ----
          OBJ_48.setText("Stock en double quantit\u00e9");
          OBJ_48.setForeground(new Color(11, 11, 98));
          OBJ_48.setFont(OBJ_48.getFont().deriveFont(OBJ_48.getFont().getStyle() | Font.BOLD));
          OBJ_48.setName("OBJ_48");
          p_tete_droite.add(OBJ_48);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setDoubleBuffered(false);
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WTIE1 ----
            WTIE1.setToolTipText("Collectif fournisseur ");
            WTIE1.setName("WTIE1");
            panel1.add(WTIE1);
            WTIE1.setBounds(95, 20, 18, WTIE1.getPreferredSize().height);

            //---- WTIE2 ----
            WTIE2.setToolTipText("Compte client ou fournisseur");
            WTIE2.setName("WTIE2");
            panel1.add(WTIE2);
            WTIE2.setBounds(115, 20, 58, WTIE2.getPreferredSize().height);

            //---- WTIE3 ----
            WTIE3.setToolTipText("Suffixe de livraison client ");
            WTIE3.setName("WTIE3");
            panel1.add(WTIE3);
            WTIE3.setBounds(180, 20, 34, WTIE3.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Tiers");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(15, 20, 65, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Tous les magasins - article @A1LIB@ @A1LB1@ @A1LB2@ @A1LB3@"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_88 ----
            OBJ_88.setText("N\u00b0 S\u00e9rie");
            OBJ_88.setName("OBJ_88");
            panel2.add(OBJ_88);
            OBJ_88.setBounds(275, 29, 61, 21);

            //---- SERDEB ----
            SERDEB.setName("SERDEB");
            panel2.add(SERDEB);
            SERDEB.setBounds(350, 25, 210, SERDEB.getPreferredSize().height);

            //---- MARDEB ----
            MARDEB.setName("MARDEB");
            panel2.add(MARDEB);
            MARDEB.setBounds(535, 25, 80, MARDEB.getPreferredSize().height);

            //======== SCROLLPANE_LIST1 ========
            {
              SCROLLPANE_LIST1.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST1.setName("SCROLLPANE_LIST1");

              //---- WT301 ----
              WT301.setComponentPopupMenu(BTD);
              WT301.setName("WT301");
              WT301.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WT301MouseClicked(e);
                }
              });
              SCROLLPANE_LIST1.setViewportView(WT301);
            }
            panel2.add(SCROLLPANE_LIST1);
            SCROLLPANE_LIST1.setBounds(20, 60, 595, 385);

            //---- OBJ_98 ----
            OBJ_98.setText("@WEB@");
            OBJ_98.setName("OBJ_98");
            panel2.add(OBJ_98);
            OBJ_98.setBounds(20, 25, 50, 28);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(630, 65, 25, 155);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(630, 290, 25, 155);

            //---- OBJ_99 ----
            OBJ_99.setText("@A1CL2@");
            OBJ_99.setFont(new Font("sansserif", Font.BOLD, 11));
            OBJ_99.setName("OBJ_99");
            panel2.add(OBJ_99);
            OBJ_99.setBounds(540, 0, 135, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Mouvements"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- WTYPE4 ----
            WTYPE4.setText("Fabrication");
            WTYPE4.setComponentPopupMenu(BTD);
            WTYPE4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTYPE4.setName("WTYPE4");
            panel3.add(WTYPE4);
            WTYPE4.setBounds(25, 190, 92, 20);

            //---- WOPE3 ----
            WOPE3.setText("Inventaire");
            WOPE3.setComponentPopupMenu(BTD);
            WOPE3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WOPE3.setName("WOPE3");
            panel3.add(WOPE3);
            WOPE3.setBounds(25, 140, 83, 20);

            //---- WOPE5 ----
            WOPE5.setText("Transfert");
            WOPE5.setComponentPopupMenu(BTD);
            WOPE5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WOPE5.setName("WOPE5");
            panel3.add(WOPE5);
            WOPE5.setBounds(25, 165, 75, 20);

            //---- WOPE2 ----
            WOPE2.setText("Entr\u00e9e");
            WOPE2.setComponentPopupMenu(BTD);
            WOPE2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WOPE2.setName("WOPE2");
            panel3.add(WOPE2);
            WOPE2.setBounds(25, 240, 66, 20);

            //---- WOPE1 ----
            WOPE1.setText("Divers");
            WOPE1.setComponentPopupMenu(BTD);
            WOPE1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WOPE1.setName("WOPE1");
            panel3.add(WOPE1);
            WOPE1.setBounds(25, 115, 60, 20);

            //---- WOPE4 ----
            WOPE4.setText("Sortie");
            WOPE4.setComponentPopupMenu(BTD);
            WOPE4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WOPE4.setName("WOPE4");
            panel3.add(WOPE4);
            WOPE4.setBounds(25, 265, 60, 20);

            //---- WTYPE1 ----
            WTYPE1.setText("Achat");
            WTYPE1.setComponentPopupMenu(BTD);
            WTYPE1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTYPE1.setName("WTYPE1");
            panel3.add(WTYPE1);
            WTYPE1.setBounds(25, 65, 59, 20);

            //---- WTYPE2 ----
            WTYPE2.setText("Vente");
            WTYPE2.setComponentPopupMenu(BTD);
            WTYPE2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTYPE2.setName("WTYPE2");
            panel3.add(WTYPE2);
            WTYPE2.setBounds(25, 90, 57, 20);

            //---- WTYPE3 ----
            WTYPE3.setText("Stock");
            WTYPE3.setComponentPopupMenu(BTD);
            WTYPE3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTYPE3.setName("WTYPE3");
            panel3.add(WTYPE3);
            WTYPE3.setBounds(25, 215, 57, 20);

            //---- OBJ_76 ----
            OBJ_76.setText("A partir de");
            OBJ_76.setName("OBJ_76");
            panel3.add(OBJ_76);
            OBJ_76.setBounds(25, 340, 67, 25);

            //---- DATDEB ----
            DATDEB.setName("DATDEB");
            panel3.add(DATDEB);
            DATDEB.setBounds(25, 365, 110, DATDEB.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 874, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 675, GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 460, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 460, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("D\u00e9tail");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_14 ----
      OBJ_14.setText("Historique de ventes");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
      BTD.addSeparator();

      //---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("Invite");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField H2ART;
  private JLabel OBJ_67;
  private JLabel OBJ_66;
  private XRiTextField H2ETB;
  private JLabel OBJ_49;
  private XRiTextField A1UNS;
  private XRiTextField UNLIB;
  private JLabel OBJ_50;
  private XRiTextField X13DVX;
  private JPanel p_tete_droite;
  private JLabel OBJ_48;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField WTIE1;
  private XRiTextField WTIE2;
  private XRiTextField WTIE3;
  private JLabel label1;
  private JPanel panel2;
  private JLabel OBJ_88;
  private XRiTextField SERDEB;
  private XRiTextField MARDEB;
  private JScrollPane SCROLLPANE_LIST1;
  private XRiTable WT301;
  private JLabel OBJ_98;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_99;
  private JPanel panel3;
  private XRiCheckBox WTYPE4;
  private XRiCheckBox WOPE3;
  private XRiCheckBox WOPE5;
  private XRiCheckBox WOPE2;
  private XRiCheckBox WOPE1;
  private XRiCheckBox WOPE4;
  private XRiCheckBox WTYPE1;
  private XRiCheckBox WTYPE2;
  private XRiCheckBox WTYPE3;
  private JLabel OBJ_76;
  private XRiCalendrier DATDEB;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
