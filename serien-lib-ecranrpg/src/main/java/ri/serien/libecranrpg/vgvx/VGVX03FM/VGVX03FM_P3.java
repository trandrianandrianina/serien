
package ri.serien.libecranrpg.vgvx.VGVX03FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX03FM_P3 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVX03FM_P3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Mets l'écran sous forme de boite de dialogue
    setDialog(true);
    // Ajout
    initDiverses();
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Indicateur
    Boolean is96 = lexique.isTrue("96");
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !is96);
    
    // Titre du panel
    pnlInformation.setTitre("Type :" + lexique.HostFieldGetData("PRLIB"));
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Mode de calcul du prix de revient"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelEcranRPG();
    pnlInformation = new SNPanelTitre();
    TE01 = new XRiTextField();
    TE02 = new XRiTextField();
    TE03 = new XRiTextField();
    TE04 = new XRiTextField();
    TE05 = new XRiTextField();
    TE06 = new XRiTextField();
    TE07 = new XRiTextField();
    TE08 = new XRiTextField();
    TE09 = new XRiTextField();
    TE10 = new XRiTextField();
    lbValeurAchat = new SNLabelChamp();
    PX3PRE = new XRiTextField();
    LI01 = new XRiTextField();
    TY01 = new XRiTextField();
    PR01 = new XRiTextField();
    MT01 = new XRiTextField();
    LI02 = new XRiTextField();
    TY02 = new XRiTextField();
    MT02 = new XRiTextField();
    PR02 = new XRiTextField();
    LI03 = new XRiTextField();
    TY03 = new XRiTextField();
    MT03 = new XRiTextField();
    PR03 = new XRiTextField();
    LI04 = new XRiTextField();
    TY04 = new XRiTextField();
    MT04 = new XRiTextField();
    PR04 = new XRiTextField();
    LI05 = new XRiTextField();
    TY05 = new XRiTextField();
    MT05 = new XRiTextField();
    PR05 = new XRiTextField();
    LI06 = new XRiTextField();
    TY06 = new XRiTextField();
    MT06 = new XRiTextField();
    PR06 = new XRiTextField();
    LI07 = new XRiTextField();
    TY07 = new XRiTextField();
    MT07 = new XRiTextField();
    PR07 = new XRiTextField();
    LI08 = new XRiTextField();
    TY08 = new XRiTextField();
    MT08 = new XRiTextField();
    PR08 = new XRiTextField();
    LI09 = new XRiTextField();
    TY09 = new XRiTextField();
    MT09 = new XRiTextField();
    PR09 = new XRiTextField();
    LI10 = new XRiTextField();
    TY10 = new XRiTextField();
    MT10 = new XRiTextField();
    PR10 = new XRiTextField();
    MTTOT = new XRiTextField();
    PRTOT = new XRiTextField();
    lbLibelle = new SNLabelTitre();
    lbCoefficientValeur = new SNLabelTitre();
    lbU = new SNLabelTitre();
    lbValeur = new SNLabelTitre();
    lbPrixRevient = new SNLabelTitre();
    pnlCoefficientValeur = new SNPanel();
    COE01 = new XRiTextField();
    TKM1 = new XRiTextField();
    COE02 = new XRiTextField();
    TKM2 = new XRiTextField();
    COE03 = new XRiTextField();
    TKM3 = new XRiTextField();
    COE04 = new XRiTextField();
    TKM4 = new XRiTextField();
    COE05 = new XRiTextField();
    TKM5 = new XRiTextField();
    COE06 = new XRiTextField();
    TKM6 = new XRiTextField();
    COE07 = new XRiTextField();
    TKM7 = new XRiTextField();
    COE08 = new XRiTextField();
    TKM8 = new XRiTextField();
    COE09 = new XRiTextField();
    TKM9 = new XRiTextField();
    COE10 = new XRiTextField();
    TKM10 = new XRiTextField();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(770, 560));
    setPreferredSize(new Dimension(770, 560));
    setMaximumSize(new Dimension(770, 560));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlInformation ========
      {
        pnlInformation.setOpaque(false);
        pnlInformation.setTitre("Type : Voir code");
        pnlInformation.setName("pnlInformation");
        pnlInformation.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlInformation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlInformation.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlInformation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlInformation.getLayout()).rowWeights =
            new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- TE01 ----
        TE01.setPreferredSize(new Dimension(45, 30));
        TE01.setMinimumSize(new Dimension(45, 30));
        TE01.setMaximumSize(new Dimension(45, 30));
        TE01.setName("TE01");
        pnlInformation.add(TE01, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TE02 ----
        TE02.setPreferredSize(new Dimension(45, 30));
        TE02.setMinimumSize(new Dimension(45, 30));
        TE02.setMaximumSize(new Dimension(45, 30));
        TE02.setName("TE02");
        pnlInformation.add(TE02, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TE03 ----
        TE03.setPreferredSize(new Dimension(45, 30));
        TE03.setMinimumSize(new Dimension(45, 30));
        TE03.setMaximumSize(new Dimension(45, 30));
        TE03.setName("TE03");
        pnlInformation.add(TE03, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TE04 ----
        TE04.setPreferredSize(new Dimension(45, 30));
        TE04.setMinimumSize(new Dimension(45, 30));
        TE04.setMaximumSize(new Dimension(45, 30));
        TE04.setName("TE04");
        pnlInformation.add(TE04, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TE05 ----
        TE05.setPreferredSize(new Dimension(45, 30));
        TE05.setMinimumSize(new Dimension(45, 30));
        TE05.setMaximumSize(new Dimension(45, 30));
        TE05.setName("TE05");
        pnlInformation.add(TE05, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TE06 ----
        TE06.setPreferredSize(new Dimension(45, 30));
        TE06.setMinimumSize(new Dimension(45, 30));
        TE06.setMaximumSize(new Dimension(45, 30));
        TE06.setName("TE06");
        pnlInformation.add(TE06, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TE07 ----
        TE07.setPreferredSize(new Dimension(45, 30));
        TE07.setMinimumSize(new Dimension(45, 30));
        TE07.setMaximumSize(new Dimension(45, 30));
        TE07.setName("TE07");
        pnlInformation.add(TE07, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TE08 ----
        TE08.setPreferredSize(new Dimension(45, 30));
        TE08.setMinimumSize(new Dimension(45, 30));
        TE08.setMaximumSize(new Dimension(45, 30));
        TE08.setName("TE08");
        pnlInformation.add(TE08, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TE09 ----
        TE09.setPreferredSize(new Dimension(45, 30));
        TE09.setMinimumSize(new Dimension(45, 30));
        TE09.setMaximumSize(new Dimension(45, 30));
        TE09.setName("TE09");
        pnlInformation.add(TE09, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TE10 ----
        TE10.setPreferredSize(new Dimension(45, 30));
        TE10.setMinimumSize(new Dimension(45, 30));
        TE10.setMaximumSize(new Dimension(45, 30));
        TE10.setName("TE10");
        pnlInformation.add(TE10, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbValeurAchat ----
        lbValeurAchat.setText("Valeur d'achat");
        lbValeurAchat.setHorizontalAlignment(SwingConstants.RIGHT);
        lbValeurAchat.setName("lbValeurAchat");
        pnlInformation.add(lbValeurAchat, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- PX3PRE ----
        PX3PRE.setHorizontalAlignment(SwingConstants.RIGHT);
        PX3PRE.setPreferredSize(new Dimension(150, 30));
        PX3PRE.setMinimumSize(new Dimension(150, 30));
        PX3PRE.setMaximumSize(new Dimension(150, 30));
        PX3PRE.setName("PX3PRE");
        pnlInformation.add(PX3PRE, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- LI01 ----
        LI01.setPreferredSize(new Dimension(130, 30));
        LI01.setMinimumSize(new Dimension(130, 30));
        LI01.setMaximumSize(new Dimension(130, 30));
        LI01.setName("LI01");
        pnlInformation.add(LI01, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TY01 ----
        TY01.setPreferredSize(new Dimension(25, 30));
        TY01.setMinimumSize(new Dimension(25, 30));
        TY01.setMaximumSize(new Dimension(25, 30));
        TY01.setName("TY01");
        pnlInformation.add(TY01, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- PR01 ----
        PR01.setHorizontalAlignment(SwingConstants.RIGHT);
        PR01.setPreferredSize(new Dimension(150, 30));
        PR01.setMinimumSize(new Dimension(150, 30));
        PR01.setMaximumSize(new Dimension(150, 30));
        PR01.setName("PR01");
        pnlInformation.add(PR01, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- MT01 ----
        MT01.setHorizontalAlignment(SwingConstants.RIGHT);
        MT01.setPreferredSize(new Dimension(175, 30));
        MT01.setMinimumSize(new Dimension(175, 30));
        MT01.setMaximumSize(new Dimension(175, 30));
        MT01.setName("MT01");
        pnlInformation.add(MT01, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- LI02 ----
        LI02.setPreferredSize(new Dimension(130, 30));
        LI02.setMinimumSize(new Dimension(130, 30));
        LI02.setMaximumSize(new Dimension(130, 30));
        LI02.setName("LI02");
        pnlInformation.add(LI02, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TY02 ----
        TY02.setPreferredSize(new Dimension(25, 30));
        TY02.setMinimumSize(new Dimension(25, 30));
        TY02.setMaximumSize(new Dimension(25, 30));
        TY02.setName("TY02");
        pnlInformation.add(TY02, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- MT02 ----
        MT02.setHorizontalAlignment(SwingConstants.RIGHT);
        MT02.setPreferredSize(new Dimension(175, 30));
        MT02.setMinimumSize(new Dimension(175, 30));
        MT02.setMaximumSize(new Dimension(175, 30));
        MT02.setName("MT02");
        pnlInformation.add(MT02, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- PR02 ----
        PR02.setHorizontalAlignment(SwingConstants.RIGHT);
        PR02.setPreferredSize(new Dimension(150, 30));
        PR02.setMinimumSize(new Dimension(150, 30));
        PR02.setMaximumSize(new Dimension(150, 30));
        PR02.setName("PR02");
        pnlInformation.add(PR02, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- LI03 ----
        LI03.setPreferredSize(new Dimension(130, 30));
        LI03.setMinimumSize(new Dimension(130, 30));
        LI03.setMaximumSize(new Dimension(130, 30));
        LI03.setName("LI03");
        pnlInformation.add(LI03, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TY03 ----
        TY03.setPreferredSize(new Dimension(25, 30));
        TY03.setMinimumSize(new Dimension(25, 30));
        TY03.setMaximumSize(new Dimension(25, 30));
        TY03.setName("TY03");
        pnlInformation.add(TY03, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- MT03 ----
        MT03.setHorizontalAlignment(SwingConstants.RIGHT);
        MT03.setPreferredSize(new Dimension(175, 30));
        MT03.setMinimumSize(new Dimension(175, 30));
        MT03.setMaximumSize(new Dimension(175, 30));
        MT03.setName("MT03");
        pnlInformation.add(MT03, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- PR03 ----
        PR03.setHorizontalAlignment(SwingConstants.RIGHT);
        PR03.setPreferredSize(new Dimension(150, 30));
        PR03.setMinimumSize(new Dimension(150, 30));
        PR03.setMaximumSize(new Dimension(150, 30));
        PR03.setName("PR03");
        pnlInformation.add(PR03, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- LI04 ----
        LI04.setPreferredSize(new Dimension(130, 30));
        LI04.setMinimumSize(new Dimension(130, 30));
        LI04.setMaximumSize(new Dimension(130, 30));
        LI04.setName("LI04");
        pnlInformation.add(LI04, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TY04 ----
        TY04.setPreferredSize(new Dimension(25, 30));
        TY04.setMinimumSize(new Dimension(25, 30));
        TY04.setMaximumSize(new Dimension(25, 30));
        TY04.setName("TY04");
        pnlInformation.add(TY04, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- MT04 ----
        MT04.setHorizontalAlignment(SwingConstants.RIGHT);
        MT04.setPreferredSize(new Dimension(175, 30));
        MT04.setMinimumSize(new Dimension(175, 30));
        MT04.setMaximumSize(new Dimension(175, 30));
        MT04.setName("MT04");
        pnlInformation.add(MT04, new GridBagConstraints(4, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- PR04 ----
        PR04.setHorizontalAlignment(SwingConstants.RIGHT);
        PR04.setPreferredSize(new Dimension(150, 30));
        PR04.setMinimumSize(new Dimension(150, 30));
        PR04.setMaximumSize(new Dimension(150, 30));
        PR04.setName("PR04");
        pnlInformation.add(PR04, new GridBagConstraints(5, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- LI05 ----
        LI05.setPreferredSize(new Dimension(130, 30));
        LI05.setMinimumSize(new Dimension(130, 30));
        LI05.setMaximumSize(new Dimension(130, 30));
        LI05.setName("LI05");
        pnlInformation.add(LI05, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TY05 ----
        TY05.setPreferredSize(new Dimension(25, 30));
        TY05.setMinimumSize(new Dimension(25, 30));
        TY05.setMaximumSize(new Dimension(25, 30));
        TY05.setName("TY05");
        pnlInformation.add(TY05, new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- MT05 ----
        MT05.setHorizontalAlignment(SwingConstants.RIGHT);
        MT05.setPreferredSize(new Dimension(175, 30));
        MT05.setMinimumSize(new Dimension(175, 30));
        MT05.setMaximumSize(new Dimension(175, 30));
        MT05.setName("MT05");
        pnlInformation.add(MT05, new GridBagConstraints(4, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- PR05 ----
        PR05.setHorizontalAlignment(SwingConstants.RIGHT);
        PR05.setPreferredSize(new Dimension(150, 30));
        PR05.setMinimumSize(new Dimension(150, 30));
        PR05.setMaximumSize(new Dimension(150, 30));
        PR05.setName("PR05");
        pnlInformation.add(PR05, new GridBagConstraints(5, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- LI06 ----
        LI06.setPreferredSize(new Dimension(130, 30));
        LI06.setMinimumSize(new Dimension(130, 30));
        LI06.setMaximumSize(new Dimension(130, 30));
        LI06.setName("LI06");
        pnlInformation.add(LI06, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TY06 ----
        TY06.setPreferredSize(new Dimension(25, 30));
        TY06.setMinimumSize(new Dimension(25, 30));
        TY06.setMaximumSize(new Dimension(25, 30));
        TY06.setName("TY06");
        pnlInformation.add(TY06, new GridBagConstraints(3, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- MT06 ----
        MT06.setHorizontalAlignment(SwingConstants.RIGHT);
        MT06.setPreferredSize(new Dimension(175, 30));
        MT06.setMinimumSize(new Dimension(175, 30));
        MT06.setMaximumSize(new Dimension(175, 30));
        MT06.setName("MT06");
        pnlInformation.add(MT06, new GridBagConstraints(4, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- PR06 ----
        PR06.setHorizontalAlignment(SwingConstants.RIGHT);
        PR06.setPreferredSize(new Dimension(150, 30));
        PR06.setMinimumSize(new Dimension(150, 30));
        PR06.setMaximumSize(new Dimension(150, 30));
        PR06.setName("PR06");
        pnlInformation.add(PR06, new GridBagConstraints(5, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- LI07 ----
        LI07.setPreferredSize(new Dimension(130, 30));
        LI07.setMinimumSize(new Dimension(130, 30));
        LI07.setMaximumSize(new Dimension(130, 30));
        LI07.setName("LI07");
        pnlInformation.add(LI07, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TY07 ----
        TY07.setPreferredSize(new Dimension(25, 30));
        TY07.setMinimumSize(new Dimension(25, 30));
        TY07.setMaximumSize(new Dimension(25, 30));
        TY07.setName("TY07");
        pnlInformation.add(TY07, new GridBagConstraints(3, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- MT07 ----
        MT07.setHorizontalAlignment(SwingConstants.RIGHT);
        MT07.setPreferredSize(new Dimension(175, 30));
        MT07.setMinimumSize(new Dimension(175, 30));
        MT07.setMaximumSize(new Dimension(175, 30));
        MT07.setName("MT07");
        pnlInformation.add(MT07, new GridBagConstraints(4, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- PR07 ----
        PR07.setHorizontalAlignment(SwingConstants.RIGHT);
        PR07.setPreferredSize(new Dimension(150, 30));
        PR07.setMinimumSize(new Dimension(150, 30));
        PR07.setMaximumSize(new Dimension(150, 30));
        PR07.setName("PR07");
        pnlInformation.add(PR07, new GridBagConstraints(5, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- LI08 ----
        LI08.setPreferredSize(new Dimension(130, 30));
        LI08.setMinimumSize(new Dimension(130, 30));
        LI08.setMaximumSize(new Dimension(130, 30));
        LI08.setName("LI08");
        pnlInformation.add(LI08, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TY08 ----
        TY08.setPreferredSize(new Dimension(25, 30));
        TY08.setMinimumSize(new Dimension(25, 30));
        TY08.setMaximumSize(new Dimension(25, 30));
        TY08.setName("TY08");
        pnlInformation.add(TY08, new GridBagConstraints(3, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- MT08 ----
        MT08.setHorizontalAlignment(SwingConstants.RIGHT);
        MT08.setPreferredSize(new Dimension(175, 30));
        MT08.setMinimumSize(new Dimension(175, 30));
        MT08.setMaximumSize(new Dimension(175, 30));
        MT08.setName("MT08");
        pnlInformation.add(MT08, new GridBagConstraints(4, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- PR08 ----
        PR08.setHorizontalAlignment(SwingConstants.RIGHT);
        PR08.setPreferredSize(new Dimension(150, 30));
        PR08.setMinimumSize(new Dimension(150, 30));
        PR08.setMaximumSize(new Dimension(150, 30));
        PR08.setName("PR08");
        pnlInformation.add(PR08, new GridBagConstraints(5, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- LI09 ----
        LI09.setPreferredSize(new Dimension(130, 30));
        LI09.setMinimumSize(new Dimension(130, 30));
        LI09.setMaximumSize(new Dimension(130, 30));
        LI09.setName("LI09");
        pnlInformation.add(LI09, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TY09 ----
        TY09.setPreferredSize(new Dimension(25, 30));
        TY09.setMinimumSize(new Dimension(25, 30));
        TY09.setMaximumSize(new Dimension(25, 30));
        TY09.setName("TY09");
        pnlInformation.add(TY09, new GridBagConstraints(3, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- MT09 ----
        MT09.setHorizontalAlignment(SwingConstants.RIGHT);
        MT09.setPreferredSize(new Dimension(175, 30));
        MT09.setMinimumSize(new Dimension(175, 30));
        MT09.setMaximumSize(new Dimension(175, 30));
        MT09.setName("MT09");
        pnlInformation.add(MT09, new GridBagConstraints(4, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- PR09 ----
        PR09.setHorizontalAlignment(SwingConstants.RIGHT);
        PR09.setPreferredSize(new Dimension(150, 30));
        PR09.setMinimumSize(new Dimension(150, 30));
        PR09.setMaximumSize(new Dimension(150, 30));
        PR09.setName("PR09");
        pnlInformation.add(PR09, new GridBagConstraints(5, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- LI10 ----
        LI10.setPreferredSize(new Dimension(130, 30));
        LI10.setMinimumSize(new Dimension(130, 30));
        LI10.setMaximumSize(new Dimension(130, 30));
        LI10.setName("LI10");
        pnlInformation.add(LI10, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TY10 ----
        TY10.setPreferredSize(new Dimension(25, 30));
        TY10.setMinimumSize(new Dimension(25, 30));
        TY10.setMaximumSize(new Dimension(25, 30));
        TY10.setName("TY10");
        pnlInformation.add(TY10, new GridBagConstraints(3, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- MT10 ----
        MT10.setHorizontalAlignment(SwingConstants.RIGHT);
        MT10.setPreferredSize(new Dimension(175, 30));
        MT10.setMinimumSize(new Dimension(175, 30));
        MT10.setMaximumSize(new Dimension(175, 30));
        MT10.setName("MT10");
        pnlInformation.add(MT10, new GridBagConstraints(4, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- PR10 ----
        PR10.setHorizontalAlignment(SwingConstants.RIGHT);
        PR10.setPreferredSize(new Dimension(150, 30));
        PR10.setMinimumSize(new Dimension(150, 30));
        PR10.setMaximumSize(new Dimension(150, 30));
        PR10.setName("PR10");
        pnlInformation.add(PR10, new GridBagConstraints(5, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- MTTOT ----
        MTTOT.setHorizontalAlignment(SwingConstants.RIGHT);
        MTTOT.setPreferredSize(new Dimension(175, 30));
        MTTOT.setMinimumSize(new Dimension(175, 30));
        MTTOT.setMaximumSize(new Dimension(175, 30));
        MTTOT.setName("MTTOT");
        pnlInformation.add(MTTOT, new GridBagConstraints(4, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- PRTOT ----
        PRTOT.setHorizontalAlignment(SwingConstants.RIGHT);
        PRTOT.setPreferredSize(new Dimension(150, 30));
        PRTOT.setMinimumSize(new Dimension(150, 30));
        PRTOT.setMaximumSize(new Dimension(150, 30));
        PRTOT.setName("PRTOT");
        pnlInformation.add(PRTOT, new GridBagConstraints(5, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- lbLibelle ----
        lbLibelle.setText("Libell\u00e9");
        lbLibelle.setFont(lbLibelle.getFont().deriveFont(lbLibelle.getFont().getStyle() | Font.BOLD));
        lbLibelle.setHorizontalAlignment(SwingConstants.CENTER);
        lbLibelle.setPreferredSize(new Dimension(50, 30));
        lbLibelle.setMinimumSize(new Dimension(50, 30));
        lbLibelle.setMaximumSize(new Dimension(50, 30));
        lbLibelle.setName("lbLibelle");
        pnlInformation.add(lbLibelle, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbCoefficientValeur ----
        lbCoefficientValeur.setText("Coefficient/valeur");
        lbCoefficientValeur.setFont(lbCoefficientValeur.getFont().deriveFont(lbCoefficientValeur.getFont().getStyle() | Font.BOLD));
        lbCoefficientValeur.setHorizontalAlignment(SwingConstants.CENTER);
        lbCoefficientValeur.setMinimumSize(new Dimension(125, 30));
        lbCoefficientValeur.setPreferredSize(new Dimension(125, 30));
        lbCoefficientValeur.setMaximumSize(new Dimension(125, 30));
        lbCoefficientValeur.setName("lbCoefficientValeur");
        pnlInformation.add(lbCoefficientValeur, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbU ----
        lbU.setText("U");
        lbU.setFont(lbU.getFont().deriveFont(lbU.getFont().getStyle() | Font.BOLD));
        lbU.setHorizontalAlignment(SwingConstants.CENTER);
        lbU.setPreferredSize(new Dimension(25, 30));
        lbU.setMinimumSize(new Dimension(25, 30));
        lbU.setMaximumSize(new Dimension(25, 30));
        lbU.setName("lbU");
        pnlInformation.add(lbU, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbValeur ----
        lbValeur.setText("Valeur");
        lbValeur.setFont(lbValeur.getFont().deriveFont(lbValeur.getFont().getStyle() | Font.BOLD));
        lbValeur.setHorizontalAlignment(SwingConstants.CENTER);
        lbValeur.setPreferredSize(new Dimension(175, 30));
        lbValeur.setMinimumSize(new Dimension(175, 30));
        lbValeur.setMaximumSize(new Dimension(175, 30));
        lbValeur.setName("lbValeur");
        pnlInformation.add(lbValeur, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPrixRevient ----
        lbPrixRevient.setText("Prix de revient");
        lbPrixRevient.setFont(lbPrixRevient.getFont().deriveFont(lbPrixRevient.getFont().getStyle() | Font.BOLD));
        lbPrixRevient.setHorizontalAlignment(SwingConstants.CENTER);
        lbPrixRevient.setName("lbPrixRevient");
        pnlInformation.add(lbPrixRevient, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlCoefficientValeur ========
        {
          pnlCoefficientValeur.setName("pnlCoefficientValeur");
          pnlCoefficientValeur.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCoefficientValeur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCoefficientValeur.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCoefficientValeur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlCoefficientValeur.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- COE01 ----
          COE01.setHorizontalAlignment(SwingConstants.RIGHT);
          COE01.setMinimumSize(new Dimension(85, 30));
          COE01.setPreferredSize(new Dimension(85, 30));
          COE01.setName("COE01");
          pnlCoefficientValeur.add(COE01, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- TKM1 ----
          TKM1.setPreferredSize(new Dimension(25, 30));
          TKM1.setMinimumSize(new Dimension(25, 30));
          TKM1.setMaximumSize(new Dimension(25, 30));
          TKM1.setName("TKM1");
          pnlCoefficientValeur.add(TKM1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- COE02 ----
          COE02.setHorizontalAlignment(SwingConstants.RIGHT);
          COE02.setMinimumSize(new Dimension(85, 30));
          COE02.setPreferredSize(new Dimension(85, 30));
          COE02.setName("COE02");
          pnlCoefficientValeur.add(COE02, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- TKM2 ----
          TKM2.setPreferredSize(new Dimension(25, 30));
          TKM2.setMinimumSize(new Dimension(25, 30));
          TKM2.setMaximumSize(new Dimension(25, 30));
          TKM2.setName("TKM2");
          pnlCoefficientValeur.add(TKM2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- COE03 ----
          COE03.setHorizontalAlignment(SwingConstants.RIGHT);
          COE03.setMinimumSize(new Dimension(85, 30));
          COE03.setPreferredSize(new Dimension(85, 30));
          COE03.setName("COE03");
          pnlCoefficientValeur.add(COE03, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- TKM3 ----
          TKM3.setPreferredSize(new Dimension(25, 30));
          TKM3.setMinimumSize(new Dimension(25, 30));
          TKM3.setMaximumSize(new Dimension(25, 30));
          TKM3.setName("TKM3");
          pnlCoefficientValeur.add(TKM3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- COE04 ----
          COE04.setHorizontalAlignment(SwingConstants.RIGHT);
          COE04.setMinimumSize(new Dimension(85, 30));
          COE04.setPreferredSize(new Dimension(85, 30));
          COE04.setName("COE04");
          pnlCoefficientValeur.add(COE04, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- TKM4 ----
          TKM4.setPreferredSize(new Dimension(25, 30));
          TKM4.setMinimumSize(new Dimension(25, 30));
          TKM4.setMaximumSize(new Dimension(25, 30));
          TKM4.setName("TKM4");
          pnlCoefficientValeur.add(TKM4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- COE05 ----
          COE05.setHorizontalAlignment(SwingConstants.RIGHT);
          COE05.setMinimumSize(new Dimension(85, 30));
          COE05.setPreferredSize(new Dimension(85, 30));
          COE05.setName("COE05");
          pnlCoefficientValeur.add(COE05, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- TKM5 ----
          TKM5.setPreferredSize(new Dimension(25, 30));
          TKM5.setMinimumSize(new Dimension(25, 30));
          TKM5.setMaximumSize(new Dimension(25, 30));
          TKM5.setName("TKM5");
          pnlCoefficientValeur.add(TKM5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- COE06 ----
          COE06.setHorizontalAlignment(SwingConstants.RIGHT);
          COE06.setMinimumSize(new Dimension(85, 30));
          COE06.setPreferredSize(new Dimension(85, 30));
          COE06.setName("COE06");
          pnlCoefficientValeur.add(COE06, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- TKM6 ----
          TKM6.setPreferredSize(new Dimension(25, 30));
          TKM6.setMinimumSize(new Dimension(25, 30));
          TKM6.setMaximumSize(new Dimension(25, 30));
          TKM6.setName("TKM6");
          pnlCoefficientValeur.add(TKM6, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- COE07 ----
          COE07.setHorizontalAlignment(SwingConstants.RIGHT);
          COE07.setMinimumSize(new Dimension(85, 30));
          COE07.setPreferredSize(new Dimension(85, 30));
          COE07.setName("COE07");
          pnlCoefficientValeur.add(COE07, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- TKM7 ----
          TKM7.setPreferredSize(new Dimension(25, 30));
          TKM7.setMinimumSize(new Dimension(25, 30));
          TKM7.setMaximumSize(new Dimension(25, 30));
          TKM7.setName("TKM7");
          pnlCoefficientValeur.add(TKM7, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- COE08 ----
          COE08.setHorizontalAlignment(SwingConstants.RIGHT);
          COE08.setMinimumSize(new Dimension(85, 30));
          COE08.setPreferredSize(new Dimension(85, 30));
          COE08.setName("COE08");
          pnlCoefficientValeur.add(COE08, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- TKM8 ----
          TKM8.setPreferredSize(new Dimension(25, 30));
          TKM8.setMinimumSize(new Dimension(25, 30));
          TKM8.setMaximumSize(new Dimension(25, 30));
          TKM8.setName("TKM8");
          pnlCoefficientValeur.add(TKM8, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- COE09 ----
          COE09.setHorizontalAlignment(SwingConstants.RIGHT);
          COE09.setMinimumSize(new Dimension(85, 30));
          COE09.setPreferredSize(new Dimension(85, 30));
          COE09.setName("COE09");
          pnlCoefficientValeur.add(COE09, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- TKM9 ----
          TKM9.setPreferredSize(new Dimension(25, 30));
          TKM9.setMinimumSize(new Dimension(25, 30));
          TKM9.setMaximumSize(new Dimension(25, 30));
          TKM9.setName("TKM9");
          pnlCoefficientValeur.add(TKM9, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- COE10 ----
          COE10.setHorizontalAlignment(SwingConstants.RIGHT);
          COE10.setMinimumSize(new Dimension(85, 30));
          COE10.setPreferredSize(new Dimension(85, 30));
          COE10.setName("COE10");
          pnlCoefficientValeur.add(COE10, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- TKM10 ----
          TKM10.setPreferredSize(new Dimension(25, 30));
          TKM10.setMinimumSize(new Dimension(25, 30));
          TKM10.setMaximumSize(new Dimension(25, 30));
          TKM10.setName("TKM10");
          pnlCoefficientValeur.add(TKM10, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformation.add(pnlCoefficientValeur, new GridBagConstraints(2, 2, 1, 11, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlContenu.add(pnlInformation, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelEcranRPG pnlContenu;
  private SNPanelTitre pnlInformation;
  private XRiTextField TE01;
  private XRiTextField TE02;
  private XRiTextField TE03;
  private XRiTextField TE04;
  private XRiTextField TE05;
  private XRiTextField TE06;
  private XRiTextField TE07;
  private XRiTextField TE08;
  private XRiTextField TE09;
  private XRiTextField TE10;
  private SNLabelChamp lbValeurAchat;
  private XRiTextField PX3PRE;
  private XRiTextField LI01;
  private XRiTextField TY01;
  private XRiTextField PR01;
  private XRiTextField MT01;
  private XRiTextField LI02;
  private XRiTextField TY02;
  private XRiTextField MT02;
  private XRiTextField PR02;
  private XRiTextField LI03;
  private XRiTextField TY03;
  private XRiTextField MT03;
  private XRiTextField PR03;
  private XRiTextField LI04;
  private XRiTextField TY04;
  private XRiTextField MT04;
  private XRiTextField PR04;
  private XRiTextField LI05;
  private XRiTextField TY05;
  private XRiTextField MT05;
  private XRiTextField PR05;
  private XRiTextField LI06;
  private XRiTextField TY06;
  private XRiTextField MT06;
  private XRiTextField PR06;
  private XRiTextField LI07;
  private XRiTextField TY07;
  private XRiTextField MT07;
  private XRiTextField PR07;
  private XRiTextField LI08;
  private XRiTextField TY08;
  private XRiTextField MT08;
  private XRiTextField PR08;
  private XRiTextField LI09;
  private XRiTextField TY09;
  private XRiTextField MT09;
  private XRiTextField PR09;
  private XRiTextField LI10;
  private XRiTextField TY10;
  private XRiTextField MT10;
  private XRiTextField PR10;
  private XRiTextField MTTOT;
  private XRiTextField PRTOT;
  private SNLabelTitre lbLibelle;
  private SNLabelTitre lbCoefficientValeur;
  private SNLabelTitre lbU;
  private SNLabelTitre lbValeur;
  private SNLabelTitre lbPrixRevient;
  private SNPanel pnlCoefficientValeur;
  private XRiTextField COE01;
  private XRiTextField TKM1;
  private XRiTextField COE02;
  private XRiTextField TKM2;
  private XRiTextField COE03;
  private XRiTextField TKM3;
  private XRiTextField COE04;
  private XRiTextField TKM4;
  private XRiTextField COE05;
  private XRiTextField TKM5;
  private XRiTextField COE06;
  private XRiTextField TKM6;
  private XRiTextField COE07;
  private XRiTextField TKM7;
  private XRiTextField COE08;
  private XRiTextField TKM8;
  private XRiTextField COE09;
  private XRiTextField TKM9;
  private XRiTextField COE10;
  private XRiTextField TKM10;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
