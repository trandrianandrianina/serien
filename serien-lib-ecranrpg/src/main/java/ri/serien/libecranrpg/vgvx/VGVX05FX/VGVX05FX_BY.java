
package ri.serien.libecranrpg.vgvx.VGVX05FX;
// Nom Fichier: pop_VGVX05FX_FMTBY_FMTFX_706.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX05FX_BY extends SNPanelEcranRPG implements ioFrame {
  
   
  private int nbrLigneMax = 0;
  private int nbrColonneMax = 0;
  
  public VGVX05FX_BY(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(VAL);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    AGR.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("<HTML>Cliquez pour photo<BR>@&M000363@</HTML>")).trim());
    OBJ_9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L01@")).trim());
    OBJ_12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L02@")).trim());
    OBJ_15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L03@")).trim());
    OBJ_18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L04@")).trim());
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L05@")).trim());
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L06@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L07@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L08@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L10@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L11@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L12@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L13@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L14@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L15@")).trim());
    OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L09@")).trim());
    OBJ_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T01@")).trim());
    OBJ_10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T02@")).trim());
    OBJ_13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T03@")).trim());
    OBJ_16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T04@")).trim());
    OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T05@")).trim());
    OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T06@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T07@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T08@")).trim());
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T09@")).trim());
    OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T10@")).trim());
    OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T11@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T12@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T13@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T14@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T15@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@INDART@")).trim()));
    OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@&&NUMIMGC@/@&&NBRIMAGE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    // traitement des JTextArea
    miseEnForme(V01, "01");
    miseEnForme(V02, "02");
    miseEnForme(V03, "03");
    miseEnForme(V04, "04");
    miseEnForme(V05, "05");
    miseEnForme(V06, "06");
    miseEnForme(V07, "07");
    miseEnForme(V08, "08");
    miseEnForme(V09, "09");
    miseEnForme(V10, "10");
    miseEnForme(V11, "11");
    miseEnForme(V12, "12");
    miseEnForme(V13, "13");
    miseEnForme(V14, "14");
    miseEnForme(V15, "15");
    
    VAL.setBounds(5, (nbrLigneMax + 50), 40, 40);
    
    // panel décors
    panel1.setBounds(5, 5, (nbrColonneMax + 30), (nbrLigneMax + 35));
    
    // taille de la popup
    this.setPreferredSize(new Dimension((nbrColonneMax + 40), (nbrLigneMax + 100)));
    this.repaint();
    
    // TODO Icones
    AVA.setIcon(lexique.chargerImage("images/avant.gif", true));
    RET.setIcon(lexique.chargerImage("images/arriere.gif", true));
    OBJ_34.setIcon(lexique.chargerImage("images/plann.gif", true));
    OBJ_57.setIcon(lexique.chargerImage("images/photo.gif", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@DFTIT1@ @DFTIT2@"));
  }
  
  public int calculLongueur(String longueurDeBase, String typeDeZone) {
    if (typeDeZone == "N") {
      return ((Integer.parseInt(longueurDeBase) - 1) * 8) + 20;
    }
    else if (typeDeZone == "D") {
      return 118;
    }
    else {
      return ((Integer.parseInt(longueurDeBase) - 1) * 10) + 20;
    }
  }
  
  public void miseEnForme(XRiTextField zoneSGM, String numeroZone) {
    int longueur = 0;
    int hauteur = V01.getHeight();
    int ligne = 0;
    int colonne = 0;
    if (lexique.isPresent("T" + numeroZone)) {
      ligne = 10 + (Integer.parseInt(numeroZone) * 10);
      colonne = 154;
      longueur = calculLongueur(lexique.HostFieldGetData("LG" + numeroZone), lexique.HostFieldGetData("TP" + numeroZone));
      // formeZone = formeZone_value[Integer.parseInt(lexique.HostFieldGetData("XFD"+numeroZone))-1];
      if (lexique.HostFieldGetData("C" + numeroZone).equalsIgnoreCase("")) {
        zoneSGM.setBackground(Color.white);
        zoneSGM.setFocusable(true);
      }
      if (lexique.HostFieldGetData("C" + numeroZone).equalsIgnoreCase("2")) {
        zoneSGM.setBackground(new Color(224, 227, 233));
        zoneSGM.setFocusable(false);
      }
      if (lexique.HostFieldGetData("C" + numeroZone).equalsIgnoreCase("3")) {
        zoneSGM.setBackground(new Color(214, 217, 223));
        zoneSGM.setFocusable(false);
      }
      if (lexique.HostFieldGetData("C" + numeroZone).equalsIgnoreCase("4")) {
        zoneSGM.setForeground(Color.blue);
        zoneSGM.setBackground(new Color(214, 217, 223));
        zoneSGM.setFocusable(false);
      }
      if (lexique.HostFieldGetData("C" + numeroZone).equalsIgnoreCase("5")) {
        zoneSGM.setForeground(Color.red);
        zoneSGM.setBackground(new Color(214, 217, 223));
        zoneSGM.setFocusable(false);
      }
      zoneSGM.setBounds(colonne, ligne, longueur, hauteur);
      // zoneSGM.setBorder(formeZone);
      zoneSGM.setVisible(true);
      
      // stockage taille max
      if ((colonne + longueur) > nbrColonneMax) {
        nbrColonneMax = colonne + longueur;
      }
      if (ligne > nbrLigneMax) {
        nbrLigneMax = ligne;
      }
    }
    else {
      zoneSGM.setVisible(false);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_57ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // if NBRIMAGE = 0 then
    // ScriptCall("G_INIPHO")
    // else
    // NBRIMAGE = 0
    // touche="F5"
    // Scriptcall ("G_TOUCHE")
    // end if
    // if NBRIMAGE = 0 then
    // ScriptCall("G_INIPHO")
    // else
    // NBRIMAGE = 0
    // lexique.HostScreenSendKey(this, "F5", false);
    // end if
  }
  
  private void VALActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="ENTER"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void RETActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Scriptcall ("G_PHMOIN")
    // touche="F5"
    // Scriptcall ("G_TOUCHE")
    // Scriptcall ("G_PHMOIN")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void AVAActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Scriptcall ("G_PHPLUS")
    // touche="F5"
    // Scriptcall ("G_TOUCHE")
    // Scriptcall ("G_PHPLUS")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    AGR = new JButton();
    VAL = new JButton();
    OBJ_34 = new JButton();
    RET = new JButton();
    AVA = new JButton();
    V01 = new XRiTextField();
    V02 = new XRiTextField();
    V03 = new XRiTextField();
    V04 = new XRiTextField();
    V05 = new XRiTextField();
    V06 = new XRiTextField();
    V07 = new XRiTextField();
    V08 = new XRiTextField();
    V10 = new XRiTextField();
    V11 = new XRiTextField();
    V12 = new XRiTextField();
    V13 = new XRiTextField();
    V14 = new XRiTextField();
    V15 = new XRiTextField();
    V09 = new XRiTextField();
    OBJ_9 = new JLabel();
    OBJ_12 = new JLabel();
    OBJ_15 = new JLabel();
    OBJ_18 = new JLabel();
    OBJ_21 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_7 = new JLabel();
    OBJ_10 = new JLabel();
    OBJ_13 = new JLabel();
    OBJ_16 = new JLabel();
    OBJ_19 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_53 = new JLabel();
    panel1 = new JPanel();
    P_PnlOpts = new JPanel();
    OBJ_57 = new JButton();
    OBJ_37 = new JButton();
    PHO = new JPanel();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- AGR ----
    AGR.setText("");
    AGR.setToolTipText("<HTML>Cliquez pour photo<BR>@&M000363@</HTML>");
    AGR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    AGR.setName("AGR");
    add(AGR);
    AGR.setBounds(46, -172, 336, 134);

    //---- VAL ----
    VAL.setText("Quitter");
    VAL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    VAL.setName("VAL");
    VAL.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        VALActionPerformed(e);
      }
    });
    add(VAL);
    VAL.setBounds(589, 475, 82, 24);

    //---- OBJ_34 ----
    OBJ_34.setText("");
    OBJ_34.setToolTipText("Agrandir cette photographie");
    OBJ_34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_34.setName("OBJ_34");
    add(OBJ_34);
    OBJ_34.setBounds(500, 475, 31, 26);

    //---- RET ----
    RET.setText("");
    RET.setToolTipText("Photographie pr\u00e9c\u00e9dente");
    RET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    RET.setName("RET");
    RET.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        RETActionPerformed(e);
      }
    });
    add(RET);
    RET.setBounds(440, 485, 25, 18);

    //---- AVA ----
    AVA.setText("");
    AVA.setToolTipText("Photographie suivante");
    AVA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    AVA.setName("AVA");
    AVA.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        AVAActionPerformed(e);
      }
    });
    add(AVA);
    AVA.setBounds(470, 485, 25, 18);

    //---- V01 ----
    V01.setName("V01");
    add(V01);
    V01.setBounds(165, 45, 222, V01.getPreferredSize().height);

    //---- V02 ----
    V02.setName("V02");
    add(V02);
    V02.setBounds(165, 70, 222, V02.getPreferredSize().height);

    //---- V03 ----
    V03.setName("V03");
    add(V03);
    V03.setBounds(165, 100, 222, V03.getPreferredSize().height);

    //---- V04 ----
    V04.setName("V04");
    add(V04);
    V04.setBounds(165, 130, 222, V04.getPreferredSize().height);

    //---- V05 ----
    V05.setName("V05");
    add(V05);
    V05.setBounds(165, 160, 222, V05.getPreferredSize().height);

    //---- V06 ----
    V06.setName("V06");
    add(V06);
    V06.setBounds(165, 185, 222, V06.getPreferredSize().height);

    //---- V07 ----
    V07.setName("V07");
    add(V07);
    V07.setBounds(165, 215, 222, V07.getPreferredSize().height);

    //---- V08 ----
    V08.setName("V08");
    add(V08);
    V08.setBounds(165, 245, 222, V08.getPreferredSize().height);

    //---- V10 ----
    V10.setName("V10");
    add(V10);
    V10.setBounds(165, 300, 222, V10.getPreferredSize().height);

    //---- V11 ----
    V11.setName("V11");
    add(V11);
    V11.setBounds(165, 330, 222, V11.getPreferredSize().height);

    //---- V12 ----
    V12.setName("V12");
    add(V12);
    V12.setBounds(165, 360, 222, V12.getPreferredSize().height);

    //---- V13 ----
    V13.setName("V13");
    add(V13);
    V13.setBounds(165, 390, 222, V13.getPreferredSize().height);

    //---- V14 ----
    V14.setName("V14");
    add(V14);
    V14.setBounds(165, 415, 222, V14.getPreferredSize().height);

    //---- V15 ----
    V15.setName("V15");
    add(V15);
    V15.setBounds(165, 445, 222, V15.getPreferredSize().height);

    //---- V09 ----
    V09.setName("V09");
    add(V09);
    V09.setBounds(165, 275, 222, V09.getPreferredSize().height);

    //---- OBJ_9 ----
    OBJ_9.setText("@L01@");
    OBJ_9.setName("OBJ_9");
    add(OBJ_9);
    OBJ_9.setBounds(415, 45, 228, 16);

    //---- OBJ_12 ----
    OBJ_12.setText("@L02@");
    OBJ_12.setName("OBJ_12");
    add(OBJ_12);
    OBJ_12.setBounds(415, 70, 228, 16);

    //---- OBJ_15 ----
    OBJ_15.setText("@L03@");
    OBJ_15.setName("OBJ_15");
    add(OBJ_15);
    OBJ_15.setBounds(415, 100, 228, 16);

    //---- OBJ_18 ----
    OBJ_18.setText("@L04@");
    OBJ_18.setName("OBJ_18");
    add(OBJ_18);
    OBJ_18.setBounds(415, 130, 228, 16);

    //---- OBJ_21 ----
    OBJ_21.setText("@L05@");
    OBJ_21.setName("OBJ_21");
    add(OBJ_21);
    OBJ_21.setBounds(415, 160, 228, 16);

    //---- OBJ_24 ----
    OBJ_24.setText("@L06@");
    OBJ_24.setName("OBJ_24");
    add(OBJ_24);
    OBJ_24.setBounds(415, 185, 228, 16);

    //---- OBJ_27 ----
    OBJ_27.setText("@L07@");
    OBJ_27.setName("OBJ_27");
    add(OBJ_27);
    OBJ_27.setBounds(415, 215, 228, 16);

    //---- OBJ_30 ----
    OBJ_30.setText("@L08@");
    OBJ_30.setName("OBJ_30");
    add(OBJ_30);
    OBJ_30.setBounds(415, 245, 228, 16);

    //---- OBJ_40 ----
    OBJ_40.setText("@L10@");
    OBJ_40.setName("OBJ_40");
    add(OBJ_40);
    OBJ_40.setBounds(415, 300, 228, 16);

    //---- OBJ_43 ----
    OBJ_43.setText("@L11@");
    OBJ_43.setName("OBJ_43");
    add(OBJ_43);
    OBJ_43.setBounds(415, 330, 228, 16);

    //---- OBJ_46 ----
    OBJ_46.setText("@L12@");
    OBJ_46.setName("OBJ_46");
    add(OBJ_46);
    OBJ_46.setBounds(415, 360, 228, 16);

    //---- OBJ_49 ----
    OBJ_49.setText("@L13@");
    OBJ_49.setName("OBJ_49");
    add(OBJ_49);
    OBJ_49.setBounds(415, 390, 228, 16);

    //---- OBJ_52 ----
    OBJ_52.setText("@L14@");
    OBJ_52.setName("OBJ_52");
    add(OBJ_52);
    OBJ_52.setBounds(415, 415, 228, 16);

    //---- OBJ_55 ----
    OBJ_55.setText("@L15@");
    OBJ_55.setName("OBJ_55");
    add(OBJ_55);
    OBJ_55.setBounds(415, 445, 228, 16);

    //---- OBJ_59 ----
    OBJ_59.setText("@L09@");
    OBJ_59.setName("OBJ_59");
    add(OBJ_59);
    OBJ_59.setBounds(415, 275, 228, 16);

    //---- OBJ_7 ----
    OBJ_7.setText("@T01@");
    OBJ_7.setName("OBJ_7");
    add(OBJ_7);
    OBJ_7.setBounds(15, 45, 123, 16);

    //---- OBJ_10 ----
    OBJ_10.setText("@T02@");
    OBJ_10.setName("OBJ_10");
    add(OBJ_10);
    OBJ_10.setBounds(15, 70, 123, 16);

    //---- OBJ_13 ----
    OBJ_13.setText("@T03@");
    OBJ_13.setName("OBJ_13");
    add(OBJ_13);
    OBJ_13.setBounds(15, 100, 123, 16);

    //---- OBJ_16 ----
    OBJ_16.setText("@T04@");
    OBJ_16.setName("OBJ_16");
    add(OBJ_16);
    OBJ_16.setBounds(15, 130, 123, 16);

    //---- OBJ_19 ----
    OBJ_19.setText("@T05@");
    OBJ_19.setName("OBJ_19");
    add(OBJ_19);
    OBJ_19.setBounds(15, 160, 123, 16);

    //---- OBJ_22 ----
    OBJ_22.setText("@T06@");
    OBJ_22.setName("OBJ_22");
    add(OBJ_22);
    OBJ_22.setBounds(15, 185, 123, 16);

    //---- OBJ_25 ----
    OBJ_25.setText("@T07@");
    OBJ_25.setName("OBJ_25");
    add(OBJ_25);
    OBJ_25.setBounds(15, 215, 123, 16);

    //---- OBJ_28 ----
    OBJ_28.setText("@T08@");
    OBJ_28.setName("OBJ_28");
    add(OBJ_28);
    OBJ_28.setBounds(15, 245, 123, 16);

    //---- OBJ_31 ----
    OBJ_31.setText("@T09@");
    OBJ_31.setName("OBJ_31");
    add(OBJ_31);
    OBJ_31.setBounds(15, 275, 123, 16);

    //---- OBJ_38 ----
    OBJ_38.setText("@T10@");
    OBJ_38.setName("OBJ_38");
    add(OBJ_38);
    OBJ_38.setBounds(15, 300, 123, 16);

    //---- OBJ_41 ----
    OBJ_41.setText("@T11@");
    OBJ_41.setName("OBJ_41");
    add(OBJ_41);
    OBJ_41.setBounds(15, 330, 123, 16);

    //---- OBJ_44 ----
    OBJ_44.setText("@T12@");
    OBJ_44.setName("OBJ_44");
    add(OBJ_44);
    OBJ_44.setBounds(15, 360, 123, 16);

    //---- OBJ_47 ----
    OBJ_47.setText("@T13@");
    OBJ_47.setName("OBJ_47");
    add(OBJ_47);
    OBJ_47.setBounds(15, 390, 123, 16);

    //---- OBJ_50 ----
    OBJ_50.setText("@T14@");
    OBJ_50.setName("OBJ_50");
    add(OBJ_50);
    OBJ_50.setBounds(15, 415, 123, 16);

    //---- OBJ_53 ----
    OBJ_53.setText("@T15@");
    OBJ_53.setName("OBJ_53");
    add(OBJ_53);
    OBJ_53.setBounds(15, 445, 123, 16);

    //======== panel1 ========
    {
      panel1.setBorder(new TitledBorder("@INDART@"));
      panel1.setName("panel1");
      panel1.setLayout(null);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel1.getComponentCount(); i++) {
          Rectangle bounds = panel1.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel1.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel1.setMinimumSize(preferredSize);
        panel1.setPreferredSize(preferredSize);
      }
    }
    add(panel1);
    panel1.setBounds(475, 10, 200, 200);

    setPreferredSize(new Dimension(689, 522));

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);

      //---- OBJ_57 ----
      OBJ_57.setText("");
      OBJ_57.setToolTipText("Photographies de l'article");
      OBJ_57.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_57.setName("OBJ_57");
      OBJ_57.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_57ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_57);
      OBJ_57.setBounds(5, 6, 40, 40);

      //---- OBJ_37 ----
      OBJ_37.setText("@&&NUMIMGC@/@&&NBRIMAGE@");
      OBJ_37.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_37.setName("OBJ_37");
      P_PnlOpts.add(OBJ_37);
      OBJ_37.setBounds(5, 60, 40, 40);

      P_PnlOpts.setPreferredSize(new Dimension(55, 516));
    }

    //======== PHO ========
    {
      PHO.setName("PHO");
      PHO.setLayout(null);

      PHO.setPreferredSize(new Dimension(374, 194));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JButton AGR;
  private JButton VAL;
  private JButton OBJ_34;
  private JButton RET;
  private JButton AVA;
  private XRiTextField V01;
  private XRiTextField V02;
  private XRiTextField V03;
  private XRiTextField V04;
  private XRiTextField V05;
  private XRiTextField V06;
  private XRiTextField V07;
  private XRiTextField V08;
  private XRiTextField V10;
  private XRiTextField V11;
  private XRiTextField V12;
  private XRiTextField V13;
  private XRiTextField V14;
  private XRiTextField V15;
  private XRiTextField V09;
  private JLabel OBJ_9;
  private JLabel OBJ_12;
  private JLabel OBJ_15;
  private JLabel OBJ_18;
  private JLabel OBJ_21;
  private JLabel OBJ_24;
  private JLabel OBJ_27;
  private JLabel OBJ_30;
  private JLabel OBJ_40;
  private JLabel OBJ_43;
  private JLabel OBJ_46;
  private JLabel OBJ_49;
  private JLabel OBJ_52;
  private JLabel OBJ_55;
  private JLabel OBJ_59;
  private JLabel OBJ_7;
  private JLabel OBJ_10;
  private JLabel OBJ_13;
  private JLabel OBJ_16;
  private JLabel OBJ_19;
  private JLabel OBJ_22;
  private JLabel OBJ_25;
  private JLabel OBJ_28;
  private JLabel OBJ_31;
  private JLabel OBJ_38;
  private JLabel OBJ_41;
  private JLabel OBJ_44;
  private JLabel OBJ_47;
  private JLabel OBJ_50;
  private JLabel OBJ_53;
  private JPanel panel1;
  private JPanel P_PnlOpts;
  private JButton OBJ_57;
  private JButton OBJ_37;
  private JPanel PHO;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
