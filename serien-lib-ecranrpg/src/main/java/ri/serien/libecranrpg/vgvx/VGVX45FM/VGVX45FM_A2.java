
package ri.serien.libecranrpg.vgvx.VGVX45FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVX45FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _LD01_Title = { "HLD01", };
  private String[][] _LD01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _LD01_Width = { 562, };
  
  public VGVX45FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    LD01.setAspectTable(null, _LD01_Title, _LD01_Data, _LD01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WSTKX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKX@")).trim());
    WATTX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTX@")).trim());
    WRESX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRESX@")).trim());
    WPOSX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPOSX@")).trim());
    WAFFX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WAFFX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), null);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    WAFFX.setVisible(lexique.isPresent("WAFFX"));
    WPOSX.setVisible(lexique.isPresent("WPOSX"));
    WRESX.setVisible(lexique.isPresent("WRESX"));
    WATTX.setVisible(lexique.isPresent("WATTX"));
    WSTKX.setVisible(lexique.isPresent("WSTKX"));
    
    setTitle("Stocks autres établissements");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void LD01MouseClicked(MouseEvent e) {
    if (LD01.doubleClicSelection(e)) {
      int ligne = LD01.getSelectedRow();
      String ligneSelectionnee = "LD" + ((ligne + 1) < 10 ? "0" + (ligne + 1) : (ligne + 1));
      lexique.HostCursorPut(ligneSelectionnee);
      lexique.HostScreenSendKey(this, "F4", false);
    }
  }
  
  private void choisirActionPerformed(ActionEvent e) {
    int ligne = LD01.getSelectedRow();
    String ligneSelectionnee = "LD" + ((ligne + 1) < 10 ? "0" + (ligne + 1) : (ligne + 1));
    lexique.HostCursorPut(ligneSelectionnee);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    LD01 = new XRiTable();
    OBJ_15 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_18 = new JLabel();
    WSTKX = new RiZoneSortie();
    WATTX = new RiZoneSortie();
    WRESX = new RiZoneSortie();
    WPOSX = new RiZoneSortie();
    WAFFX = new RiZoneSortie();
    OBJ_27 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_26 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    choisir = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(870, 475));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Stocks autres \u00e9tablissements"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- LD01 ----
            LD01.setComponentPopupMenu(BTD);
            LD01.setName("LD01");
            LD01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(LD01);
          }
          p_recup.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(25, 40, 579, 270);

          //---- OBJ_15 ----
          OBJ_15.setText("dont r\u00e9serv\u00e9");
          OBJ_15.setName("OBJ_15");
          p_recup.add(OBJ_15);
          OBJ_15.setBounds(285, 372, 95, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_18 ----
          OBJ_18.setText("Total article");
          OBJ_18.setName("OBJ_18");
          p_recup.add(OBJ_18);
          OBJ_18.setBounds(25, 340, 75, 20);

          //---- WSTKX ----
          WSTKX.setText("@WSTKX@");
          WSTKX.setHorizontalAlignment(SwingConstants.RIGHT);
          WSTKX.setName("WSTKX");
          p_recup.add(WSTKX);
          WSTKX.setBounds(100, 335, 120, WSTKX.getPreferredSize().height);

          //---- WATTX ----
          WATTX.setText("@WATTX@");
          WATTX.setHorizontalAlignment(SwingConstants.RIGHT);
          WATTX.setName("WATTX");
          p_recup.add(WATTX);
          WATTX.setBounds(240, 335, 120, WATTX.getPreferredSize().height);

          //---- WRESX ----
          WRESX.setText("@WRESX@");
          WRESX.setHorizontalAlignment(SwingConstants.RIGHT);
          WRESX.setName("WRESX");
          p_recup.add(WRESX);
          WRESX.setBounds(380, 335, 120, WRESX.getPreferredSize().height);

          //---- WPOSX ----
          WPOSX.setText("@WPOSX@");
          WPOSX.setHorizontalAlignment(SwingConstants.RIGHT);
          WPOSX.setName("WPOSX");
          p_recup.add(WPOSX);
          WPOSX.setBounds(520, 335, 120, WPOSX.getPreferredSize().height);

          //---- WAFFX ----
          WAFFX.setText("@WAFFX@");
          WAFFX.setHorizontalAlignment(SwingConstants.RIGHT);
          WAFFX.setName("WAFFX");
          p_recup.add(WAFFX);
          WAFFX.setBounds(380, 370, 120, WAFFX.getPreferredSize().height);

          //---- OBJ_27 ----
          OBJ_27.setText("=");
          OBJ_27.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_27.setFont(OBJ_27.getFont().deriveFont(OBJ_27.getFont().getStyle() | Font.BOLD, OBJ_27.getFont().getSize() + 1f));
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(500, 338, 20, 18);

          //---- OBJ_25 ----
          OBJ_25.setText("+");
          OBJ_25.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_25.setFont(OBJ_25.getFont().deriveFont(OBJ_25.getFont().getStyle() | Font.BOLD, OBJ_25.getFont().getSize() + 1f));
          OBJ_25.setName("OBJ_25");
          p_recup.add(OBJ_25);
          OBJ_25.setBounds(220, 340, 20, 16);

          //---- OBJ_26 ----
          OBJ_26.setText("-");
          OBJ_26.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_26.setFont(OBJ_26.getFont().deriveFont(OBJ_26.getFont().getStyle() | Font.BOLD, OBJ_26.getFont().getSize() + 3f));
          OBJ_26.setName("OBJ_26");
          p_recup.add(OBJ_26);
          OBJ_26.setBounds(360, 339, 20, 16);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          p_recup.add(BT_PGUP);
          BT_PGUP.setBounds(610, 40, 25, 129);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          p_recup.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(610, 180, 25, 129);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 665, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 425, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- choisir ----
      choisir.setText("attendu/command\u00e9");
      choisir.setName("choisir");
      choisir.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          choisirActionPerformed(e);
        }
      });
      BTD.add(choisir);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LD01;
  private JLabel OBJ_15;
  private JPanel P_PnlOpts;
  private JLabel OBJ_18;
  private RiZoneSortie WSTKX;
  private RiZoneSortie WATTX;
  private RiZoneSortie WRESX;
  private RiZoneSortie WPOSX;
  private RiZoneSortie WAFFX;
  private JLabel OBJ_27;
  private JLabel OBJ_25;
  private JLabel OBJ_26;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem choisir;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
