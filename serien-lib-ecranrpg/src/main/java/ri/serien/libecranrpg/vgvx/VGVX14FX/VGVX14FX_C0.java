
package ri.serien.libecranrpg.vgvx.VGVX14FX;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX14FX_C0 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WPT01_Top = { "WPT01", "WPT02", "WPT03", "WPT04", "WPT05", "WPT06", "WPT07", "WPT08", "WPT09", "WPT10", "WPT11",
      "WPT12", "WPT13", "WPT14", "WPT15", };
  private String[] _WPT01_Title = { "HLD01", };
  private String[][] _WPT01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WPT01_Width = { 543, };
  
  public VGVX14FX_C0(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WPT01.setAspectTable(_WPT01_Top, _WPT01_Title, _WPT01_Data, _WPT01_Width, false, null, null, null, null);
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.PIED, true);
    snBarreBouton.ajouterBouton(EnumBouton.RETOURNER_ENTETE, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WPT01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Code établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "E1ETB");
    snEtablissement.setEnabled(false);
    
    // Code Article
    snArticle.setSession(getSession());
    snArticle.setIdEtablissement(snEtablissement.getIdSelection());
    snArticle.charger(false);
    snArticle.setSelectionParChampRPG(lexique, "WARTT");
    
    WCOD.setVisible(lexique.isPresent("WCOD"));
    WNLI.setVisible(lexique.isPresent("WNLI"));
    snEtablissement.setVisible(lexique.isPresent("E1ETB"));
    WNUM.setVisible(lexique.isPresent("WNUM"));
    lbWARTT.setVisible(lexique.isPresent("WARTT"));
    lbWNLI.setVisible(lexique.isPresent("WNLI"));
    lbWLOT.setVisible(lexique.isTrue("(N46) AND (81) AND (83)"));
    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    snEtablissement.renseignerChampRPG(lexique, "E1ETB");
    snArticle.renseignerChampRPG(lexique, "WARTT");
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.PIED)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(EnumBouton.RETOURNER_ENTETE)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WPT01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    WPT01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WPT01MouseClicked(MouseEvent e) {
    if (WPT01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBandeau = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlInformations = new SNPanel();
    pnlGauche = new SNPanel();
    lbNumero = new SNLabelChamp();
    WNUM = new XRiTextField();
    pnlDroite = new SNPanel();
    lbE1ETB = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlListe = new SNPanelTitre();
    btPageUp = new JButton();
    scpListeLigne = new JScrollPane();
    WPT01 = new XRiTable();
    btPageDown = new JButton();
    pnlSaisieLigne = new SNPanelTitre();
    lbWCOD = new SNLabelUnite();
    lbWNLI = new SNLabelUnite();
    lbWARTT = new SNLabelUnite();
    WCOD = new XRiTextField();
    WNLI = new XRiTextField();
    snArticle = new SNArticle();
    lbWLOT = new SNLabelChamp();
    WLOT = new XRiTextField();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlBandeau ========
    {
      pnlBandeau.setName("pnlBandeau");
      pnlBandeau.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITB@");
      p_bpresentation.setName("p_bpresentation");
      pnlBandeau.add(p_bpresentation);
    }
    add(pnlBandeau, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

      //======== pnlInformations ========
      {
        pnlInformations.setName("pnlInformations");
        pnlInformations.setLayout(new GridLayout(1, 2, 5, 0));

        //======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbNumero ----
          lbNumero.setText("Num\u00e9ro");
          lbNumero.setMaximumSize(new Dimension(200, 30));
          lbNumero.setMinimumSize(new Dimension(200, 30));
          lbNumero.setPreferredSize(new Dimension(200, 30));
          lbNumero.setInheritsPopupMenu(false);
          lbNumero.setName("lbNumero");
          pnlGauche.add(lbNumero, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WNUM ----
          WNUM.setComponentPopupMenu(null);
          WNUM.setMinimumSize(new Dimension(70, 30));
          WNUM.setMaximumSize(new Dimension(70, 30));
          WNUM.setPreferredSize(new Dimension(70, 30));
          WNUM.setFont(new Font("sansserif", Font.PLAIN, 14));
          WNUM.setName("WNUM");
          pnlGauche.add(WNUM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformations.add(pnlGauche);

        //======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbE1ETB ----
          lbE1ETB.setText("Etablissement");
          lbE1ETB.setMaximumSize(new Dimension(200, 30));
          lbE1ETB.setMinimumSize(new Dimension(200, 30));
          lbE1ETB.setPreferredSize(new Dimension(200, 30));
          lbE1ETB.setInheritsPopupMenu(false);
          lbE1ETB.setName("lbE1ETB");
          pnlDroite.add(lbE1ETB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          pnlDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformations.add(pnlDroite);
      }
      pnlContenu.add(pnlInformations, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlListe ========
      {
        pnlListe.setMaximumSize(new Dimension(700, 320));
        pnlListe.setMinimumSize(new Dimension(700, 320));
        pnlListe.setPreferredSize(new Dimension(700, 320));
        pnlListe.setTitre("D\u00e9tail lignes");
        pnlListe.setName("pnlListe");
        pnlListe.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlListe.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlListe.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlListe.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlListe.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- btPageUp ----
        btPageUp.setText("");
        btPageUp.setToolTipText("Page pr\u00e9c\u00e9dente");
        btPageUp.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        btPageUp.setMaximumSize(new Dimension(25, 125));
        btPageUp.setMinimumSize(new Dimension(25, 125));
        btPageUp.setPreferredSize(new Dimension(25, 125));
        btPageUp.setName("btPageUp");
        pnlListe.add(btPageUp, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== scpListeLigne ========
        {
          scpListeLigne.setComponentPopupMenu(BTD);
          scpListeLigne.setMaximumSize(new Dimension(600, 270));
          scpListeLigne.setMinimumSize(new Dimension(600, 270));
          scpListeLigne.setPreferredSize(new Dimension(600, 270));
          scpListeLigne.setName("scpListeLigne");

          //---- WPT01 ----
          WPT01.setComponentPopupMenu(BTD);
          WPT01.setMaximumSize(new Dimension(543, 240));
          WPT01.setMinimumSize(new Dimension(543, 240));
          WPT01.setName("WPT01");
          WPT01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WPT01MouseClicked(e);
            }
          });
          scpListeLigne.setViewportView(WPT01);
        }
        pnlListe.add(scpListeLigne, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- btPageDown ----
        btPageDown.setText("");
        btPageDown.setToolTipText("Page suivante");
        btPageDown.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        btPageDown.setMaximumSize(new Dimension(25, 125));
        btPageDown.setMinimumSize(new Dimension(25, 125));
        btPageDown.setPreferredSize(new Dimension(25, 125));
        btPageDown.setName("btPageDown");
        pnlListe.add(btPageDown, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlListe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlSaisieLigne ========
      {
        pnlSaisieLigne.setMaximumSize(new Dimension(750, 150));
        pnlSaisieLigne.setMinimumSize(new Dimension(750, 150));
        pnlSaisieLigne.setPreferredSize(new Dimension(750, 150));
        pnlSaisieLigne.setTitre("Saisie de ligne");
        pnlSaisieLigne.setName("pnlSaisieLigne");
        pnlSaisieLigne.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlSaisieLigne.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
        ((GridBagLayout)pnlSaisieLigne.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlSaisieLigne.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlSaisieLigne.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //---- lbWCOD ----
        lbWCOD.setText("C ");
        lbWCOD.setMaximumSize(new Dimension(24, 28));
        lbWCOD.setMinimumSize(new Dimension(24, 28));
        lbWCOD.setPreferredSize(new Dimension(24, 28));
        lbWCOD.setName("lbWCOD");
        pnlSaisieLigne.add(lbWCOD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbWNLI ----
        lbWNLI.setText("N\u00b0Ligne");
        lbWNLI.setRequestFocusEnabled(false);
        lbWNLI.setPreferredSize(new Dimension(60, 30));
        lbWNLI.setMinimumSize(new Dimension(60, 30));
        lbWNLI.setMaximumSize(new Dimension(60, 30));
        lbWNLI.setName("lbWNLI");
        pnlSaisieLigne.add(lbWNLI, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbWARTT ----
        lbWARTT.setText("Article");
        lbWARTT.setMaximumSize(new Dimension(80, 28));
        lbWARTT.setMinimumSize(new Dimension(80, 28));
        lbWARTT.setPreferredSize(new Dimension(80, 28));
        lbWARTT.setName("lbWARTT");
        pnlSaisieLigne.add(lbWARTT, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WCOD ----
        WCOD.setComponentPopupMenu(BTD);
        WCOD.setMaximumSize(new Dimension(24, 28));
        WCOD.setMinimumSize(new Dimension(24, 28));
        WCOD.setPreferredSize(new Dimension(24, 28));
        WCOD.setName("WCOD");
        pnlSaisieLigne.add(WCOD, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- WNLI ----
        WNLI.setComponentPopupMenu(BTD);
        WNLI.setMaximumSize(new Dimension(44, 28));
        WNLI.setMinimumSize(new Dimension(44, 28));
        WNLI.setPreferredSize(new Dimension(44, 28));
        WNLI.setName("WNLI");
        pnlSaisieLigne.add(WNLI, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snArticle ----
        snArticle.setName("snArticle");
        pnlSaisieLigne.add(snArticle, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- lbWLOT ----
        lbWLOT.setText("Lot");
        lbWLOT.setMaximumSize(new Dimension(50, 30));
        lbWLOT.setMinimumSize(new Dimension(50, 30));
        lbWLOT.setPreferredSize(new Dimension(50, 28));
        lbWLOT.setName("lbWLOT");
        pnlSaisieLigne.add(lbWLOT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WLOT ----
        WLOT.setComponentPopupMenu(BTD);
        WLOT.setMaximumSize(new Dimension(210, 28));
        WLOT.setMinimumSize(new Dimension(210, 28));
        WLOT.setPreferredSize(new Dimension(210, 28));
        WLOT.setName("WLOT");
        pnlSaisieLigne.add(WLOT, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlContenu.add(pnlSaisieLigne, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Modifier");
      CHOISIR.setFont(new Font("sansserif", Font.PLAIN, 14));
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_12 ----
      OBJ_12.setText("Annuler");
      OBJ_12.setFont(new Font("sansserif", Font.PLAIN, 14));
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlBandeau;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlInformations;
  private SNPanel pnlGauche;
  private SNLabelChamp lbNumero;
  private XRiTextField WNUM;
  private SNPanel pnlDroite;
  private SNLabelChamp lbE1ETB;
  private SNEtablissement snEtablissement;
  private SNPanelTitre pnlListe;
  private JButton btPageUp;
  private JScrollPane scpListeLigne;
  private XRiTable WPT01;
  private JButton btPageDown;
  private SNPanelTitre pnlSaisieLigne;
  private SNLabelUnite lbWCOD;
  private SNLabelUnite lbWNLI;
  private SNLabelUnite lbWARTT;
  private XRiTextField WCOD;
  private XRiTextField WNLI;
  private SNArticle snArticle;
  private SNLabelChamp lbWLOT;
  private XRiTextField WLOT;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
