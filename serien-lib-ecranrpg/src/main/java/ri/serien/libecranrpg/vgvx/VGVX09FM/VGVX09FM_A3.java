
package ri.serien.libecranrpg.vgvx.VGVX09FM;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.Calculatrice;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX09FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  ArrayList<XRiTextField> listeSaisie = null;
  ArrayList<JLabel> listeUnite = null;
  ArrayList<JLabel> listeLibelle = null;
  private Calculatrice calculatrice = null;
  private int ligneCourante = 1;
  private int hauteurPanel = 355;
  
  public VGVX09FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    listeSaisie = new ArrayList<XRiTextField>();
    listeSaisie.add(WTQ01);
    listeSaisie.add(WTQ02);
    listeSaisie.add(WTQ03);
    listeSaisie.add(WTQ04);
    listeSaisie.add(WTQ05);
    listeSaisie.add(WTQ06);
    listeSaisie.add(WTQ07);
    listeSaisie.add(WTQ08);
    listeSaisie.add(WTQ09);
    listeSaisie.add(WTQ10);
    listeSaisie.add(WTQ11);
    listeSaisie.add(WTQ12);
    listeSaisie.add(WTQ13);
    listeSaisie.add(WTQ14);
    listeSaisie.add(WTQ15);
    listeUnite = new ArrayList<JLabel>();
    listeUnite.add(label1);
    listeUnite.add(label2);
    listeUnite.add(label3);
    listeUnite.add(label4);
    listeUnite.add(label5);
    listeUnite.add(label6);
    listeUnite.add(label7);
    listeUnite.add(label8);
    listeUnite.add(label9);
    listeUnite.add(label10);
    listeUnite.add(label11);
    listeUnite.add(label12);
    listeUnite.add(label13);
    listeUnite.add(label14);
    listeUnite.add(label15);
    listeLibelle = new ArrayList<JLabel>();
    listeLibelle.add(L301);
    listeLibelle.add(L302);
    listeLibelle.add(L303);
    listeLibelle.add(L304);
    listeLibelle.add(L305);
    listeLibelle.add(L306);
    listeLibelle.add(L307);
    listeLibelle.add(L308);
    listeLibelle.add(L309);
    listeLibelle.add(L310);
    listeLibelle.add(L311);
    listeLibelle.add(L312);
    listeLibelle.add(L313);
    listeLibelle.add(L314);
    listeLibelle.add(L315);
    gererLeClavier();
    
    // Ajout
    initDiverses();
    
    // $$travaux$$ OBJ_19 débranché pour le momen
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN01@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN02@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN03@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN04@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN05@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN06@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN07@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN08@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN09@")).trim());
    label10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN10@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN11@")).trim());
    label12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN12@")).trim());
    label13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN13@")).trim());
    label14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN14@")).trim());
    label15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN15@")).trim());
    L301.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L301@")).trim());
    L302.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L302@")).trim());
    L303.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L303@")).trim());
    L304.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L304@")).trim());
    L305.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L305@")).trim());
    L306.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L306@")).trim());
    L307.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L307@")).trim());
    L308.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L308@")).trim());
    L309.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L309@")).trim());
    L310.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L310@")).trim());
    L311.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L311@")).trim());
    L312.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L312@")).trim());
    L313.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L313@")).trim());
    L314.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L314@")).trim());
    L315.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L315@")).trim());
    label16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UTIT1@")).trim());
    total.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@K09TOT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    K09TOT.setVisible(lexique.isPresent("K09TOT"));
    
    for (int i = 0; i < listeSaisie.size(); i++) {
      if (listeSaisie.get(i).isVisible()) {
        listeUnite.get(i).setVisible(true);
        listeLibelle.get(i).setVisible(true);
        // selon indicateur 51 à 65 pour changement couleur sur stock négatif
        if (lexique.isTrue(Integer.toString(51 + i))) {
          listeLibelle.get(i).setForeground(Constantes.COULEUR_NEGATIF);
        }
        else {
          listeLibelle.get(i).setForeground(Color.BLACK);
        }
      }
      else {
        listeUnite.get(i).setVisible(false);
        listeLibelle.get(i).setVisible(false);
        hauteurPanel -= 20;
      }
    }
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    setTitle(interpreteurD.analyseExpression("@TITSAI@"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void gererLeClavier() {
    Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
      @Override
      public void eventDispatched(AWTEvent event) {
        KeyEvent key = (KeyEvent) event;
        if (key.getID() == KeyEvent.KEY_PRESSED) {
          if (key.getKeyCode() == KeyEvent.VK_F2) {
            lexique.HostCursorPut(ligneCourante, 2);
          }
          else if (key.getKeyCode() == KeyEvent.VK_F14) {
            lexique.HostCursorPut(ligneCourante, 2);
          }
        }
      }
    }, AWTEvent.KEY_EVENT_MASK);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(lexique.getPanel(), "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(lexique.getPanel(), "F24");
  }
  
  private void WTQ01FocusGained(FocusEvent e) {
    if (e.getSource() instanceof XRiTextField) {
      ((XRiTextField) e.getSource()).selectAll();
    }
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(lexique.getPanel(), "F2");
  }
  
  private void MODIFActionPerformed(ActionEvent e) {
    
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(lexique.getPanel(), "F14");
  }
  
  private void menu_calculActionPerformed(ActionEvent e) {
    if (calculatrice == null) {
      calculatrice = new Calculatrice(BTD.getInvoker());
    }
    else {
      calculatrice.reveiller(BTD.getInvoker());
    }
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(lexique.getPanel(), "F4");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(lexique.getPanel(), "F4");
  }
  
  private void WTQ01FocusLost(FocusEvent e) {
    Double tot = 0.0;
    XRiTextField[] wtqlist = { WTQ01, WTQ02, WTQ03, WTQ04, WTQ05, WTQ06, WTQ07, WTQ08, WTQ09, WTQ10, WTQ11, WTQ12, WTQ13, WTQ14, WTQ15 };
    String contenu = "0";
    for (int i = 0; i < wtqlist.length; i++) {
      if (wtqlist[i].isVisible()) {
        if ((wtqlist[i].getText() == null) || (wtqlist[i].getText().isEmpty())) {
          contenu = "0";
        }
        else {
          contenu = wtqlist[i].getText();
        }
        tot += Double.parseDouble(contenu.replace(",", "."));
      }
    }
    
    total.setText("" + tot);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(lexique.getPanel(), "F8");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    WTQ03 = new XRiTextField();
    WTQ04 = new XRiTextField();
    WTQ05 = new XRiTextField();
    WTQ06 = new XRiTextField();
    WTQ07 = new XRiTextField();
    WTQ08 = new XRiTextField();
    WTQ09 = new XRiTextField();
    WTQ10 = new XRiTextField();
    WTQ11 = new XRiTextField();
    WTQ12 = new XRiTextField();
    WTQ13 = new XRiTextField();
    WTQ14 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    WTQ15 = new XRiTextField();
    label14 = new JLabel();
    label15 = new JLabel();
    L301 = new JLabel();
    L302 = new JLabel();
    L303 = new JLabel();
    L304 = new JLabel();
    L305 = new JLabel();
    L306 = new JLabel();
    L307 = new JLabel();
    L308 = new JLabel();
    L309 = new JLabel();
    L310 = new JLabel();
    L311 = new JLabel();
    L312 = new JLabel();
    L313 = new JLabel();
    L314 = new JLabel();
    L315 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    label18 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    WTQ01 = new XRiTextField();
    WTQ02 = new XRiTextField();
    fd_tetiere = new JLabel();
    panel5 = new JPanel();
    lab_qte = new JLabel();
    total = new RiZoneSortie();
    BTD = new JPopupMenu();
    menuItem1 = new JMenuItem();
    MODIF = new JMenuItem();
    OBJ_16 = new JMenuItem();
    menu_calcul = new JMenuItem();
    OBJ_19 = new JMenuItem();
    K09TOT = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(885, 460));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 300));
            menus_haut.setPreferredSize(new Dimension(160, 300));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Autre vue");
              riSousMenu_bt6.setToolTipText("Autre vue");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Tri");
              riSousMenu_bt7.setToolTipText("Tri (F8)");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- WTQ03 ----
          WTQ03.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ03.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ03.setComponentPopupMenu(BTD);
          WTQ03.setName("WTQ03");
          WTQ03.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ03);
          WTQ03.setBounds(20, 75, 70, 22);

          //---- WTQ04 ----
          WTQ04.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ04.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ04.setComponentPopupMenu(BTD);
          WTQ04.setName("WTQ04");
          WTQ04.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ04);
          WTQ04.setBounds(20, 95, 70, 22);

          //---- WTQ05 ----
          WTQ05.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ05.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ05.setComponentPopupMenu(BTD);
          WTQ05.setName("WTQ05");
          WTQ05.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ05);
          WTQ05.setBounds(20, 115, 70, 22);

          //---- WTQ06 ----
          WTQ06.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ06.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ06.setComponentPopupMenu(BTD);
          WTQ06.setName("WTQ06");
          WTQ06.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ06);
          WTQ06.setBounds(20, 135, 70, 22);

          //---- WTQ07 ----
          WTQ07.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ07.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ07.setComponentPopupMenu(BTD);
          WTQ07.setName("WTQ07");
          WTQ07.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ07);
          WTQ07.setBounds(20, 155, 70, 22);

          //---- WTQ08 ----
          WTQ08.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ08.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ08.setComponentPopupMenu(BTD);
          WTQ08.setName("WTQ08");
          WTQ08.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ08);
          WTQ08.setBounds(20, 175, 70, 22);

          //---- WTQ09 ----
          WTQ09.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ09.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ09.setComponentPopupMenu(BTD);
          WTQ09.setName("WTQ09");
          WTQ09.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ09);
          WTQ09.setBounds(20, 195, 70, 22);

          //---- WTQ10 ----
          WTQ10.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ10.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ10.setComponentPopupMenu(BTD);
          WTQ10.setName("WTQ10");
          WTQ10.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ10);
          WTQ10.setBounds(20, 215, 70, 22);

          //---- WTQ11 ----
          WTQ11.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ11.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ11.setComponentPopupMenu(BTD);
          WTQ11.setName("WTQ11");
          WTQ11.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ11);
          WTQ11.setBounds(20, 235, 70, 22);

          //---- WTQ12 ----
          WTQ12.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ12.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ12.setComponentPopupMenu(BTD);
          WTQ12.setName("WTQ12");
          WTQ12.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ12);
          WTQ12.setBounds(20, 255, 70, 22);

          //---- WTQ13 ----
          WTQ13.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ13.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ13.setComponentPopupMenu(BTD);
          WTQ13.setName("WTQ13");
          WTQ13.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ13);
          WTQ13.setBounds(20, 275, 70, 22);

          //---- WTQ14 ----
          WTQ14.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ14.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ14.setComponentPopupMenu(BTD);
          WTQ14.setName("WTQ14");
          WTQ14.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ14);
          WTQ14.setBounds(20, 295, 70, 22);

          //---- label1 ----
          label1.setText("@WUN01@");
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label1.setFont(new Font("sansserif", Font.PLAIN, 11));
          label1.setName("label1");
          panel2.add(label1);
          label1.setBounds(95, 35, 26, 20);

          //---- label2 ----
          label2.setText("@WUN02@");
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label2.setFont(new Font("sansserif", Font.PLAIN, 11));
          label2.setName("label2");
          panel2.add(label2);
          label2.setBounds(95, 55, 26, 20);

          //---- label3 ----
          label3.setText("@WUN03@");
          label3.setHorizontalAlignment(SwingConstants.CENTER);
          label3.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label3.setFont(new Font("sansserif", Font.PLAIN, 11));
          label3.setName("label3");
          panel2.add(label3);
          label3.setBounds(95, 75, 26, 20);

          //---- label4 ----
          label4.setText("@WUN04@");
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label4.setFont(new Font("sansserif", Font.PLAIN, 11));
          label4.setName("label4");
          panel2.add(label4);
          label4.setBounds(95, 95, 26, 20);

          //---- label5 ----
          label5.setText("@WUN05@");
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label5.setFont(new Font("sansserif", Font.PLAIN, 11));
          label5.setName("label5");
          panel2.add(label5);
          label5.setBounds(95, 115, 26, 20);

          //---- label6 ----
          label6.setText("@WUN06@");
          label6.setHorizontalAlignment(SwingConstants.CENTER);
          label6.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label6.setFont(new Font("sansserif", Font.PLAIN, 11));
          label6.setName("label6");
          panel2.add(label6);
          label6.setBounds(95, 135, 26, 20);

          //---- label7 ----
          label7.setText("@WUN07@");
          label7.setHorizontalAlignment(SwingConstants.CENTER);
          label7.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label7.setFont(new Font("sansserif", Font.PLAIN, 11));
          label7.setName("label7");
          panel2.add(label7);
          label7.setBounds(95, 155, 26, 20);

          //---- label8 ----
          label8.setText("@WUN08@");
          label8.setHorizontalAlignment(SwingConstants.CENTER);
          label8.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label8.setFont(new Font("sansserif", Font.PLAIN, 11));
          label8.setName("label8");
          panel2.add(label8);
          label8.setBounds(95, 175, 26, 20);

          //---- label9 ----
          label9.setText("@WUN09@");
          label9.setHorizontalAlignment(SwingConstants.CENTER);
          label9.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label9.setFont(new Font("sansserif", Font.PLAIN, 11));
          label9.setName("label9");
          panel2.add(label9);
          label9.setBounds(95, 195, 26, 20);

          //---- label10 ----
          label10.setText("@WUN10@");
          label10.setHorizontalAlignment(SwingConstants.CENTER);
          label10.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label10.setFont(new Font("sansserif", Font.PLAIN, 11));
          label10.setName("label10");
          panel2.add(label10);
          label10.setBounds(95, 215, 26, 20);

          //---- label11 ----
          label11.setText("@WUN11@");
          label11.setHorizontalAlignment(SwingConstants.CENTER);
          label11.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label11.setFont(new Font("sansserif", Font.PLAIN, 11));
          label11.setName("label11");
          panel2.add(label11);
          label11.setBounds(95, 235, 26, 20);

          //---- label12 ----
          label12.setText("@WUN12@");
          label12.setHorizontalAlignment(SwingConstants.CENTER);
          label12.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label12.setFont(new Font("sansserif", Font.PLAIN, 11));
          label12.setName("label12");
          panel2.add(label12);
          label12.setBounds(95, 255, 26, 20);

          //---- label13 ----
          label13.setText("@WUN13@");
          label13.setHorizontalAlignment(SwingConstants.CENTER);
          label13.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label13.setFont(new Font("sansserif", Font.PLAIN, 11));
          label13.setName("label13");
          panel2.add(label13);
          label13.setBounds(95, 275, 26, 20);

          //---- WTQ15 ----
          WTQ15.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ15.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ15.setComponentPopupMenu(BTD);
          WTQ15.setName("WTQ15");
          WTQ15.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ15);
          WTQ15.setBounds(20, 315, 70, 22);

          //---- label14 ----
          label14.setText("@WUN14@");
          label14.setHorizontalAlignment(SwingConstants.CENTER);
          label14.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label14.setFont(new Font("sansserif", Font.PLAIN, 11));
          label14.setName("label14");
          panel2.add(label14);
          label14.setBounds(95, 295, 26, 20);

          //---- label15 ----
          label15.setText("@WUN15@");
          label15.setHorizontalAlignment(SwingConstants.CENTER);
          label15.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label15.setFont(new Font("sansserif", Font.PLAIN, 11));
          label15.setName("label15");
          panel2.add(label15);
          label15.setBounds(95, 315, 26, 20);

          //---- L301 ----
          L301.setText("@L301@");
          L301.setFont(new Font("Courier New", Font.PLAIN, 12));
          L301.setBackground(new Color(242, 242, 230));
          L301.setVerticalAlignment(SwingConstants.BOTTOM);
          L301.setOpaque(true);
          L301.setComponentPopupMenu(BTD);
          L301.setName("L301");
          panel2.add(L301);
          L301.setBounds(130, 35, 485, 20);

          //---- L302 ----
          L302.setText("@L302@");
          L302.setFont(new Font("Courier New", Font.PLAIN, 12));
          L302.setVerticalAlignment(SwingConstants.BOTTOM);
          L302.setBackground(new Color(242, 242, 230));
          L302.setComponentPopupMenu(BTD);
          L302.setName("L302");
          panel2.add(L302);
          L302.setBounds(130, 55, 485, 20);

          //---- L303 ----
          L303.setText("@L303@");
          L303.setFont(new Font("Courier New", Font.PLAIN, 12));
          L303.setBackground(new Color(242, 242, 230));
          L303.setVerticalAlignment(SwingConstants.BOTTOM);
          L303.setOpaque(true);
          L303.setComponentPopupMenu(BTD);
          L303.setName("L303");
          panel2.add(L303);
          L303.setBounds(130, 75, 485, 20);

          //---- L304 ----
          L304.setText("@L304@");
          L304.setFont(new Font("Courier New", Font.PLAIN, 12));
          L304.setVerticalAlignment(SwingConstants.BOTTOM);
          L304.setBackground(new Color(242, 242, 230));
          L304.setComponentPopupMenu(BTD);
          L304.setName("L304");
          panel2.add(L304);
          L304.setBounds(130, 95, 485, 20);

          //---- L305 ----
          L305.setText("@L305@");
          L305.setFont(new Font("Courier New", Font.PLAIN, 12));
          L305.setBackground(new Color(242, 242, 230));
          L305.setVerticalAlignment(SwingConstants.BOTTOM);
          L305.setOpaque(true);
          L305.setComponentPopupMenu(BTD);
          L305.setName("L305");
          panel2.add(L305);
          L305.setBounds(130, 115, 485, 20);

          //---- L306 ----
          L306.setText("@L306@");
          L306.setFont(new Font("Courier New", Font.PLAIN, 12));
          L306.setVerticalAlignment(SwingConstants.BOTTOM);
          L306.setBackground(new Color(242, 242, 230));
          L306.setComponentPopupMenu(BTD);
          L306.setName("L306");
          panel2.add(L306);
          L306.setBounds(130, 135, 485, 20);

          //---- L307 ----
          L307.setText("@L307@");
          L307.setFont(new Font("Courier New", Font.PLAIN, 12));
          L307.setBackground(new Color(242, 242, 230));
          L307.setVerticalAlignment(SwingConstants.BOTTOM);
          L307.setOpaque(true);
          L307.setComponentPopupMenu(BTD);
          L307.setName("L307");
          panel2.add(L307);
          L307.setBounds(130, 155, 485, 20);

          //---- L308 ----
          L308.setText("@L308@");
          L308.setFont(new Font("Courier New", Font.PLAIN, 12));
          L308.setVerticalAlignment(SwingConstants.BOTTOM);
          L308.setBackground(new Color(242, 242, 230));
          L308.setComponentPopupMenu(BTD);
          L308.setName("L308");
          panel2.add(L308);
          L308.setBounds(130, 175, 485, 20);

          //---- L309 ----
          L309.setText("@L309@");
          L309.setFont(new Font("Courier New", Font.PLAIN, 12));
          L309.setBackground(new Color(242, 242, 230));
          L309.setVerticalAlignment(SwingConstants.BOTTOM);
          L309.setOpaque(true);
          L309.setComponentPopupMenu(BTD);
          L309.setName("L309");
          panel2.add(L309);
          L309.setBounds(130, 195, 485, 20);

          //---- L310 ----
          L310.setText("@L310@");
          L310.setFont(new Font("Courier New", Font.PLAIN, 12));
          L310.setVerticalAlignment(SwingConstants.BOTTOM);
          L310.setBackground(new Color(242, 242, 230));
          L310.setComponentPopupMenu(BTD);
          L310.setName("L310");
          panel2.add(L310);
          L310.setBounds(130, 215, 485, 20);

          //---- L311 ----
          L311.setText("@L311@");
          L311.setFont(new Font("Courier New", Font.PLAIN, 12));
          L311.setBackground(new Color(242, 242, 230));
          L311.setVerticalAlignment(SwingConstants.BOTTOM);
          L311.setOpaque(true);
          L311.setComponentPopupMenu(BTD);
          L311.setName("L311");
          panel2.add(L311);
          L311.setBounds(130, 235, 485, 20);

          //---- L312 ----
          L312.setText("@L312@");
          L312.setFont(new Font("Courier New", Font.PLAIN, 12));
          L312.setVerticalAlignment(SwingConstants.BOTTOM);
          L312.setBackground(new Color(242, 242, 230));
          L312.setComponentPopupMenu(BTD);
          L312.setName("L312");
          panel2.add(L312);
          L312.setBounds(130, 255, 485, 20);

          //---- L313 ----
          L313.setText("@L313@");
          L313.setFont(new Font("Courier New", Font.PLAIN, 12));
          L313.setBackground(new Color(242, 242, 230));
          L313.setVerticalAlignment(SwingConstants.BOTTOM);
          L313.setOpaque(true);
          L313.setComponentPopupMenu(BTD);
          L313.setName("L313");
          panel2.add(L313);
          L313.setBounds(130, 275, 485, 20);

          //---- L314 ----
          L314.setText("@L314@");
          L314.setFont(new Font("Courier New", Font.PLAIN, 12));
          L314.setVerticalAlignment(SwingConstants.BOTTOM);
          L314.setBackground(new Color(242, 242, 230));
          L314.setComponentPopupMenu(BTD);
          L314.setName("L314");
          panel2.add(L314);
          L314.setBounds(130, 295, 485, 20);

          //---- L315 ----
          L315.setText("@L315@");
          L315.setFont(new Font("Courier New", Font.PLAIN, 12));
          L315.setBackground(new Color(242, 242, 230));
          L315.setVerticalAlignment(SwingConstants.BOTTOM);
          L315.setOpaque(true);
          L315.setComponentPopupMenu(BTD);
          L315.setName("L315");
          panel2.add(L315);
          L315.setBounds(130, 315, 485, 20);

          //---- label16 ----
          label16.setText("@UTIT1@");
          label16.setFont(new Font("Courier New", Font.BOLD, 12));
          label16.setBackground(new Color(225, 225, 172));
          label16.setName("label16");
          panel2.add(label16);
          label16.setBounds(130, 11, 485, 20);

          //---- label17 ----
          label17.setText("Quantit\u00e9");
          label17.setFont(new Font("Courier New", Font.BOLD, 12));
          label17.setName("label17");
          panel2.add(label17);
          label17.setBounds(25, 11, 65, 20);

          //---- label18 ----
          label18.setText("Un.");
          label18.setFont(new Font("Courier New", Font.BOLD, 12));
          label18.setName("label18");
          panel2.add(label18);
          label18.setBounds(96, 11, 26, 20);

          //---- BT_PGUP ----
          BT_PGUP.setToolTipText("page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setFocusable(false);
          BT_PGUP.setName("BT_PGUP");
          panel2.add(BT_PGUP);
          BT_PGUP.setBounds(625, 35, 25, 135);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setToolTipText("page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setFocusable(false);
          BT_PGDOWN.setName("BT_PGDOWN");
          panel2.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(625, 200, 25, 138);

          //---- WTQ01 ----
          WTQ01.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ01.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ01.setComponentPopupMenu(BTD);
          WTQ01.setName("WTQ01");
          WTQ01.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ01);
          WTQ01.setBounds(20, 34, 70, 22);

          //---- WTQ02 ----
          WTQ02.setFont(new Font("sansserif", Font.PLAIN, 11));
          WTQ02.setHorizontalAlignment(SwingConstants.RIGHT);
          WTQ02.setComponentPopupMenu(BTD);
          WTQ02.setName("WTQ02");
          WTQ02.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
              WTQ01FocusGained(e);
            }
            @Override
            public void focusLost(FocusEvent e) {
              WTQ01FocusLost(e);
            }
          });
          panel2.add(WTQ02);
          WTQ02.setBounds(20, 54, 70, 22);

          //---- fd_tetiere ----
          fd_tetiere.setOpaque(true);
          fd_tetiere.setBackground(new Color(227, 220, 220));
          fd_tetiere.setName("fd_tetiere");
          panel2.add(fd_tetiere);
          fd_tetiere.setBounds(22, 10, 593, 22);
        }

        //======== panel5 ========
        {
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);

          //---- lab_qte ----
          lab_qte.setText("Quantit\u00e9 totale");
          lab_qte.setFont(lab_qte.getFont().deriveFont(lab_qte.getFont().getStyle() | Font.BOLD));
          lab_qte.setName("lab_qte");
          panel5.add(lab_qte);
          lab_qte.setBounds(110, 14, 190, 20);

          //---- total ----
          total.setText("@K09TOT@");
          total.setHorizontalAlignment(SwingConstants.RIGHT);
          total.setName("total");
          panel5.add(total);
          total.setBounds(10, 10, 80, 28);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 670, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 670, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 365, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- menuItem1 ----
      menuItem1.setText("Choix unit\u00e9");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD.add(menuItem1);

      //---- MODIF ----
      MODIF.setText("Modification");
      MODIF.setName("MODIF");
      MODIF.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MODIFActionPerformed(e);
        }
      });
      BTD.add(MODIF);

      //---- OBJ_16 ----
      OBJ_16.setText("Options li\u00e9es \u00e0 l'article");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);

      //---- menu_calcul ----
      menu_calcul.setText("Calculatrice");
      menu_calcul.setName("menu_calcul");
      menu_calcul.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menu_calculActionPerformed(e);
        }
      });
      BTD.add(menu_calcul);
    }

    //---- OBJ_19 ----
    OBJ_19.setText("Choix possibles");
    OBJ_19.setName("OBJ_19");
    OBJ_19.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_19ActionPerformed(e);
      }
    });

    //---- K09TOT ----
    K09TOT.setHorizontalAlignment(SwingConstants.RIGHT);
    K09TOT.setName("K09TOT");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField WTQ03;
  private XRiTextField WTQ04;
  private XRiTextField WTQ05;
  private XRiTextField WTQ06;
  private XRiTextField WTQ07;
  private XRiTextField WTQ08;
  private XRiTextField WTQ09;
  private XRiTextField WTQ10;
  private XRiTextField WTQ11;
  private XRiTextField WTQ12;
  private XRiTextField WTQ13;
  private XRiTextField WTQ14;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private XRiTextField WTQ15;
  private JLabel label14;
  private JLabel label15;
  private JLabel L301;
  private JLabel L302;
  private JLabel L303;
  private JLabel L304;
  private JLabel L305;
  private JLabel L306;
  private JLabel L307;
  private JLabel L308;
  private JLabel L309;
  private JLabel L310;
  private JLabel L311;
  private JLabel L312;
  private JLabel L313;
  private JLabel L314;
  private JLabel L315;
  private JLabel label16;
  private JLabel label17;
  private JLabel label18;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiTextField WTQ01;
  private XRiTextField WTQ02;
  private JLabel fd_tetiere;
  private JPanel panel5;
  private JLabel lab_qte;
  private RiZoneSortie total;
  private JPopupMenu BTD;
  private JMenuItem menuItem1;
  private JMenuItem MODIF;
  private JMenuItem OBJ_16;
  private JMenuItem menu_calcul;
  private JMenuItem OBJ_19;
  private XRiTextField K09TOT;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
