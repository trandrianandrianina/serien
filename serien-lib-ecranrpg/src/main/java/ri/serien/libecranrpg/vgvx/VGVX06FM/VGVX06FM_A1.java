
package ri.serien.libecranrpg.vgvx.VGVX06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX06FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX06FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Interrogation article @PC6LB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITMC@")).trim());
    OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCNV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    WMAG.setEnabled(lexique.isPresent("WMAG"));
    WLIV.setEnabled(lexique.isPresent("WLIV"));
    WETB.setEnabled(lexique.isPresent("WETB"));
    WCLI.setEnabled(lexique.isPresent("WCLI"));
    WCNV.setEnabled(lexique.isPresent("WCNV"));
    WCLA.setEnabled(lexique.isPresent("WCLA"));
    WART.setEnabled(lexique.isPresent("WART"));
    WCLI1.setEnabled(lexique.isPresent("WCLI1"));
    OBJ_56.setVisible(lexique.isPresent("CLNOM"));
    OBJ_53.setVisible(lexique.isPresent("LIBCNV"));
    OBJ_51.setVisible(lexique.isPresent("MALIB"));
    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_59 = new JLabel();
    WETB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    p_client = new JPanel();
    OBJ_54 = new JLabel();
    WCLI = new XRiTextField();
    WLIV = new XRiTextField();
    OBJ_56 = new RiZoneSortie();
    OBJ_55 = new JLabel();
    WCLI1 = new XRiTextField();
    panel3 = new JPanel();
    WART = new XRiTextField();
    OBJ_60 = new JLabel();
    WCLA = new XRiTextField();
    OBJ_27 = new JLabel();
    panel2 = new JPanel();
    OBJ_50 = new JLabel();
    WMAG = new XRiTextField();
    OBJ_51 = new RiZoneSortie();
    OBJ_52 = new JLabel();
    WCNV = new XRiTextField();
    OBJ_53 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Interrogation article @PC6LB@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setMaximumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_59 ----
          OBJ_59.setText("Etablissement");
          OBJ_59.setName("OBJ_59");

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(478, 478, 478))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(1, 1, 1))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(620, 430));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== p_client ========
          {
            p_client.setBorder(new TitledBorder("Client"));
            p_client.setOpaque(false);
            p_client.setName("p_client");
            p_client.setLayout(null);

            //---- OBJ_54 ----
            OBJ_54.setText("Num\u00e9ro client");
            OBJ_54.setName("OBJ_54");
            p_client.add(OBJ_54);
            OBJ_54.setBounds(25, 39, 170, 20);

            //---- WCLI ----
            WCLI.setComponentPopupMenu(BTD);
            WCLI.setName("WCLI");
            p_client.add(WCLI);
            WCLI.setBounds(185, 35, 60, WCLI.getPreferredSize().height);

            //---- WLIV ----
            WLIV.setComponentPopupMenu(BTD);
            WLIV.setName("WLIV");
            p_client.add(WLIV);
            WLIV.setBounds(250, 35, 36, WLIV.getPreferredSize().height);

            //---- OBJ_56 ----
            OBJ_56.setText("@CLNOM@");
            OBJ_56.setName("OBJ_56");
            p_client.add(OBJ_56);
            OBJ_56.setBounds(295, 37, 240, OBJ_56.getPreferredSize().height);

            //---- OBJ_55 ----
            OBJ_55.setText("Recherche alphab\u00e9tique");
            OBJ_55.setName("OBJ_55");
            p_client.add(OBJ_55);
            OBJ_55.setBounds(25, 79, 170, 20);

            //---- WCLI1 ----
            WCLI1.setComponentPopupMenu(null);
            WCLI1.setName("WCLI1");
            p_client.add(WCLI1);
            WCLI1.setBounds(185, 75, 210, WCLI1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < p_client.getComponentCount(); i++) {
                Rectangle bounds = p_client.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = p_client.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              p_client.setMinimumSize(preferredSize);
              p_client.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Article"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- WART ----
            WART.setComponentPopupMenu(BTD);
            WART.setName("WART");
            panel3.add(WART);
            WART.setBounds(185, 35, 210, WART.getPreferredSize().height);

            //---- OBJ_60 ----
            OBJ_60.setText("Code article");
            OBJ_60.setName("OBJ_60");
            panel3.add(OBJ_60);
            OBJ_60.setBounds(25, 40, 145, 18);

            //---- WCLA ----
            WCLA.setComponentPopupMenu(BTD);
            WCLA.setName("WCLA");
            panel3.add(WCLA);
            WCLA.setBounds(185, 75, 210, WCLA.getPreferredSize().height);

            //---- OBJ_27 ----
            OBJ_27.setText("@TITMC@");
            OBJ_27.setName("OBJ_27");
            panel3.add(OBJ_27);
            OBJ_27.setBounds(25, 80, 145, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Options"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_50 ----
            OBJ_50.setText("Code magasin");
            OBJ_50.setName("OBJ_50");
            panel2.add(OBJ_50);
            OBJ_50.setBounds(25, 39, 140, 20);

            //---- WMAG ----
            WMAG.setComponentPopupMenu(BTD);
            WMAG.setName("WMAG");
            panel2.add(WMAG);
            WMAG.setBounds(185, 35, 34, WMAG.getPreferredSize().height);

            //---- OBJ_51 ----
            OBJ_51.setText("@MALIB@");
            OBJ_51.setName("OBJ_51");
            panel2.add(OBJ_51);
            OBJ_51.setBounds(225, 37, 308, OBJ_51.getPreferredSize().height);

            //---- OBJ_52 ----
            OBJ_52.setText("Code condition");
            OBJ_52.setName("OBJ_52");
            panel2.add(OBJ_52);
            OBJ_52.setBounds(25, 79, 140, 20);

            //---- WCNV ----
            WCNV.setComponentPopupMenu(BTD);
            WCNV.setName("WCNV");
            panel2.add(WCNV);
            WCNV.setBounds(185, 75, 60, WCNV.getPreferredSize().height);

            //---- OBJ_53 ----
            OBJ_53.setText("@LIBCNV@");
            OBJ_53.setName("OBJ_53");
            panel2.add(OBJ_53);
            OBJ_53.setBounds(250, 77, 283, OBJ_53.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(p_client, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 575, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(p_client, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_59;
  private XRiTextField WETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel p_client;
  private JLabel OBJ_54;
  private XRiTextField WCLI;
  private XRiTextField WLIV;
  private RiZoneSortie OBJ_56;
  private JLabel OBJ_55;
  private XRiTextField WCLI1;
  private JPanel panel3;
  private XRiTextField WART;
  private JLabel OBJ_60;
  private XRiTextField WCLA;
  private JLabel OBJ_27;
  private JPanel panel2;
  private JLabel OBJ_50;
  private XRiTextField WMAG;
  private RiZoneSortie OBJ_51;
  private JLabel OBJ_52;
  private XRiTextField WCNV;
  private RiZoneSortie OBJ_53;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
