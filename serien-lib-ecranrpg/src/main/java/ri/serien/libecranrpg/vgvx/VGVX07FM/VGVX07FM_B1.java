
package ri.serien.libecranrpg.vgvx.VGVX07FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * @author Stéphane Vénéri
 */
public class VGVX07FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGVX07FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBT01@")).trim());
    OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBT02@")).trim());
    OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBT03@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBT04@")).trim());
    OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBT05@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBT06@")).trim());
    OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBT07@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBT08@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBT09@")).trim());
    OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBT10@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    panel1.setBackground(SNCharteGraphique.COULEUR_FOND);
    panel1.setVisible(lexique.isTrue("40"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Conditions de rémunération"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    panel1 = new JPanel();
    CRAR2 = new XRiTextField();
    label1 = new JLabel();
    A1LIB = new XRiTextField();
    A1LB1 = new XRiTextField();
    A1LB2 = new XRiTextField();
    A1LB3 = new XRiTextField();
    P_PnlOpts = new JPanel();
    OBJ_37 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_48 = new JLabel();
    CRV101 = new XRiTextField();
    CRV102 = new XRiTextField();
    CRV103 = new XRiTextField();
    CRV104 = new XRiTextField();
    CRV105 = new XRiTextField();
    CRV106 = new XRiTextField();
    CRV107 = new XRiTextField();
    CRV108 = new XRiTextField();
    CRV109 = new XRiTextField();
    CRV110 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("D\u00e9tail"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //======== panel1 ========
          {
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- CRAR2 ----
            CRAR2.setName("CRAR2");
            panel1.add(CRAR2);
            CRAR2.setBounds(15, 25, 162, CRAR2.getPreferredSize().height);

            //---- label1 ----
            label1.setText("R\u00e9f\u00e9rence");
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(15, 5, 105, label1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_recup.add(panel1);
          panel1.setBounds(15, 85, 285, 60);

          //---- A1LIB ----
          A1LIB.setName("A1LIB");
          p_recup.add(A1LIB);
          A1LIB.setBounds(30, 57, 260, A1LIB.getPreferredSize().height);

          //---- A1LB1 ----
          A1LB1.setName("A1LB1");
          p_recup.add(A1LB1);
          A1LB1.setBounds(30, 85, 260, A1LB1.getPreferredSize().height);

          //---- A1LB2 ----
          A1LB2.setName("A1LB2");
          p_recup.add(A1LB2);
          A1LB2.setBounds(30, 115, 260, A1LB2.getPreferredSize().height);

          //---- A1LB3 ----
          A1LB3.setName("A1LB3");
          p_recup.add(A1LB3);
          A1LB3.setBounds(30, 144, 260, A1LB3.getPreferredSize().height);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_37 ----
          OBJ_37.setText("@LBT01@");
          OBJ_37.setName("OBJ_37");
          p_recup.add(OBJ_37);
          OBJ_37.setBounds(311, 55, 63, 20);

          //---- OBJ_38 ----
          OBJ_38.setText("@LBT02@");
          OBJ_38.setName("OBJ_38");
          p_recup.add(OBJ_38);
          OBJ_38.setBounds(390, 55, 63, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("@LBT03@");
          OBJ_39.setName("OBJ_39");
          p_recup.add(OBJ_39);
          OBJ_39.setBounds(469, 55, 63, 20);

          //---- OBJ_40 ----
          OBJ_40.setText("@LBT04@");
          OBJ_40.setName("OBJ_40");
          p_recup.add(OBJ_40);
          OBJ_40.setBounds(548, 55, 63, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("@LBT05@");
          OBJ_41.setName("OBJ_41");
          p_recup.add(OBJ_41);
          OBJ_41.setBounds(628, 55, 63, 20);

          //---- OBJ_44 ----
          OBJ_44.setText("@LBT06@");
          OBJ_44.setName("OBJ_44");
          p_recup.add(OBJ_44);
          OBJ_44.setBounds(311, 112, 63, 20);

          //---- OBJ_45 ----
          OBJ_45.setText("@LBT07@");
          OBJ_45.setName("OBJ_45");
          p_recup.add(OBJ_45);
          OBJ_45.setBounds(390, 112, 63, 20);

          //---- OBJ_46 ----
          OBJ_46.setText("@LBT08@");
          OBJ_46.setName("OBJ_46");
          p_recup.add(OBJ_46);
          OBJ_46.setBounds(469, 112, 63, 20);

          //---- OBJ_47 ----
          OBJ_47.setText("@LBT09@");
          OBJ_47.setName("OBJ_47");
          p_recup.add(OBJ_47);
          OBJ_47.setBounds(548, 112, 63, 20);

          //---- OBJ_48 ----
          OBJ_48.setText("@LBT10@");
          OBJ_48.setName("OBJ_48");
          p_recup.add(OBJ_48);
          OBJ_48.setBounds(628, 112, 63, 20);

          //---- CRV101 ----
          CRV101.setComponentPopupMenu(BTD);
          CRV101.setName("CRV101");
          p_recup.add(CRV101);
          CRV101.setBounds(311, 84, 66, CRV101.getPreferredSize().height);

          //---- CRV102 ----
          CRV102.setComponentPopupMenu(BTD);
          CRV102.setName("CRV102");
          p_recup.add(CRV102);
          CRV102.setBounds(390, 84, 66, CRV102.getPreferredSize().height);

          //---- CRV103 ----
          CRV103.setComponentPopupMenu(BTD);
          CRV103.setName("CRV103");
          p_recup.add(CRV103);
          CRV103.setBounds(469, 84, 66, CRV103.getPreferredSize().height);

          //---- CRV104 ----
          CRV104.setComponentPopupMenu(BTD);
          CRV104.setName("CRV104");
          p_recup.add(CRV104);
          CRV104.setBounds(548, 84, 66, CRV104.getPreferredSize().height);

          //---- CRV105 ----
          CRV105.setComponentPopupMenu(BTD);
          CRV105.setName("CRV105");
          p_recup.add(CRV105);
          CRV105.setBounds(628, 84, 66, CRV105.getPreferredSize().height);

          //---- CRV106 ----
          CRV106.setComponentPopupMenu(BTD);
          CRV106.setName("CRV106");
          p_recup.add(CRV106);
          CRV106.setBounds(311, 141, 66, CRV106.getPreferredSize().height);

          //---- CRV107 ----
          CRV107.setComponentPopupMenu(BTD);
          CRV107.setName("CRV107");
          p_recup.add(CRV107);
          CRV107.setBounds(390, 141, 66, CRV107.getPreferredSize().height);

          //---- CRV108 ----
          CRV108.setComponentPopupMenu(BTD);
          CRV108.setName("CRV108");
          p_recup.add(CRV108);
          CRV108.setBounds(469, 141, 66, CRV108.getPreferredSize().height);

          //---- CRV109 ----
          CRV109.setComponentPopupMenu(BTD);
          CRV109.setName("CRV109");
          p_recup.add(CRV109);
          CRV109.setBounds(548, 141, 66, CRV109.getPreferredSize().height);

          //---- CRV110 ----
          CRV110.setComponentPopupMenu(BTD);
          CRV110.setName("CRV110");
          p_recup.add(CRV110);
          CRV110.setBounds(628, 141, 66, CRV110.getPreferredSize().height);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(15, 15, 720, 223);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_16 ----
      OBJ_16.setText("Invite");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JPanel panel1;
  private XRiTextField CRAR2;
  private JLabel label1;
  private XRiTextField A1LIB;
  private XRiTextField A1LB1;
  private XRiTextField A1LB2;
  private XRiTextField A1LB3;
  private JPanel P_PnlOpts;
  private JLabel OBJ_37;
  private JLabel OBJ_38;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private JLabel OBJ_41;
  private JLabel OBJ_44;
  private JLabel OBJ_45;
  private JLabel OBJ_46;
  private JLabel OBJ_47;
  private JLabel OBJ_48;
  private XRiTextField CRV101;
  private XRiTextField CRV102;
  private XRiTextField CRV103;
  private XRiTextField CRV104;
  private XRiTextField CRV105;
  private XRiTextField CRV106;
  private XRiTextField CRV107;
  private XRiTextField CRV108;
  private XRiTextField CRV109;
  private XRiTextField CRV110;
  private JPopupMenu BTD;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
