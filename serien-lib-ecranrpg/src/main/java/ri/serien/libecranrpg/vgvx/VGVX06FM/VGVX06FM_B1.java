
package ri.serien.libecranrpg.vgvx.VGVX06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX06FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGVX06FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WART@")).trim());
    WETB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    CLNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    CLCLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCLI@")).trim());
    CLLIV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLLIV@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1LB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB1@")).trim());
    A1LB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB2@")).trim());
    A1LB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB3@")).trim());
    OBJ_80.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIBR@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SFLIBR@")).trim());
    A1CL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1CL1@")).trim());
    A1CL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1CL2@")).trim());
    A1PRV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1PRV@")).trim());
    XPUMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XPUMP@")).trim());
    XPDA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XPDA@")).trim());
    A1SFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1SFA@")).trim());
    A1FAM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1FAM@")).trim());
    WDTDAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDTDAX@")).trim());
    UCNVA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCNVA@")).trim());
    TYPMAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYPMAR@")).trim());
    WPRX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPRX@")).trim());
    WPVB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPVB@")).trim());
    WMMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMMR@")).trim());
    WQTEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQTEX@")).trim());
    WPMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("( @WPMR@ %)")).trim());
    UREM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UREM1@")).trim());
    UREM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UREM2@")).trim());
    UREM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UREM3@")).trim());
    WCOE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOE@")).trim());
    UREM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UREM4@")).trim());
    UREM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UREM5@")).trim());
    UREM6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UREM6@")).trim());
    A1UNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNV@")).trim());
    UCNVB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCNVB@")).trim());
    LIBCNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCNV@")).trim());
    WPRX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPRX2@")).trim());
    WDISX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    WSTKX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKX@")).trim());
    WRESX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRESX@")).trim());
    WATTX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTX@")).trim());
    S1MIN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@S1MIN@")).trim());
    A1UNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    MALIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    CAREF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAREF@")).trim());
    UPNDVX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UPNDVX@")).trim());
    CAPRAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAPRAX@")).trim());
    CADAPX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADAPX@")).trim());
    CANUA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CANUA@")).trim());
    OBJ_145.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADEV@")).trim());
    CAREM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAREM1@")).trim());
    CAUNA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNA@")).trim());
    CADEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADEL@")).trim());
    CAUNC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CAUNC@")).trim());
    FRSNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    OBJ_155.setVisible(lexique.isPresent("CAUNC"));
    OBJ_145.setVisible(lexique.isPresent("CADEV"));
    OBJ_144.setVisible(OBJ_145.isVisible());
    OBJ_153.setVisible(lexique.isPresent("CADEL"));
    OBJ_97.setVisible(lexique.isPresent("WPVB"));
    OBJ_154.setVisible(lexique.isPresent("CANUA"));
    OBJ_100.setVisible(lexique.isPresent("WPVB"));
    OBJ_152.setVisible(lexique.isPresent("CAREM1"));
    OBJ_149.setVisible(lexique.isPresent("CAREF"));
    OBJ_150.setVisible(lexique.isPresent("CADAPX"));
    OBJ_148.setVisible(lexique.isTrue("(N22) AND (N86) AND (61)"));
    UPNDVX.setVisible(lexique.isTrue("(N22) AND (N86) AND (61)"));
    OBJ_151.setVisible(lexique.isPresent("CAPRAX"));
    OBJ_81.setVisible(lexique.isPresent("SFLIBR"));
    OBJ_80.setVisible(lexique.isPresent("FALIBR"));
    OBJ_91.setVisible(lexique.isTrue("24"));
    UCNVA.setVisible(lexique.isTrue("24"));
    OBJ_104.setVisible(lexique.isPresent("WCOE"));
    OBJ_57.setVisible(A1PRV.isVisible());
    OBJ_62.setVisible(XPUMP.isVisible());
    OBJ_67.setVisible(XPDA.isVisible());
    OBJ_75.setVisible(WDTDAX.isVisible());
    xtp1.setVisible(UPNDVX.isVisible() || CAREF.isVisible() || CADAPX.isVisible());
    UCNVB.setVisible(lexique.isTrue("N24"));
    
    TYPMAR.setVisible(lexique.isTrue("63"));
    WMMR.setVisible(lexique.isTrue("63"));
    WPMR.setVisible(lexique.isTrue("63"));
    
    xtp3.setRightDecoration(panel_cnv);
    xtp2.setRightDecoration(panel_mag);
    xtp1.setRightDecoration(panel_frs);
    
    labelPrixNet.setVisible(lexique.isTrue("N66"));
    WPRX.setVisible(lexique.isTrue("N66"));
    labelPrixTTC.setVisible(lexique.isTrue("66"));
    WPRX2.setVisible(lexique.isTrue("66"));
    
    bouton_valider.setVisible(lexique.isTrue("N66"));
    bouton_retour.setVisible(lexique.isTrue("N66"));
    bouton_retour2.setVisible(lexique.isTrue("66"));
    
    p_bpresentation.setCodeEtablissement(WETB2.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB2.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    bouton_retour2.setIcon(lexique.chargerImage("images/loupe.png", true));
    bouton_valider2.setIcon(lexique.chargerImage("images/OK_p.png", true));
    bouton_retour4.setIcon(lexique.chargerImage("images/retour_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void bouton_valider2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void bouton_retour2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17", false);
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WART = new RiZoneSortie();
    OBJ_61 = new JLabel();
    WETB2 = new RiZoneSortie();
    OBJ_63 = new JLabel();
    OBJ_27 = new JLabel();
    CLNOM = new RiZoneSortie();
    CLCLI = new RiZoneSortie();
    CLLIV = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    bouton_valider2 = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    bouton_retour2 = new RiMenu_bt();
    navig_retour2 = new RiMenu();
    bouton_retour4 = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xtp4 = new JXTitledPanel();
    A1LIB = new RiZoneSortie();
    A1LB1 = new RiZoneSortie();
    A1LB2 = new RiZoneSortie();
    A1LB3 = new RiZoneSortie();
    OBJ_80 = new RiZoneSortie();
    OBJ_81 = new RiZoneSortie();
    A1CL1 = new RiZoneSortie();
    A1CL2 = new RiZoneSortie();
    OBJ_67 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_72 = new JLabel();
    A1PRV = new RiZoneSortie();
    XPUMP = new RiZoneSortie();
    XPDA = new RiZoneSortie();
    OBJ_57 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_62 = new JLabel();
    A1SFA = new RiZoneSortie();
    A1FAM = new RiZoneSortie();
    OBJ_55 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_65 = new JLabel();
    WDTDAX = new RiZoneSortie();
    xtp3 = new JXTitledPanel();
    UCNVA = new RiZoneSortie();
    TYPMAR = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_89 = new JLabel();
    WPRX = new RiZoneSortie();
    WPVB = new RiZoneSortie();
    OBJ_110 = new JLabel();
    OBJ_104 = new JLabel();
    WMMR = new RiZoneSortie();
    WQTEX = new RiZoneSortie();
    OBJ_100 = new JLabel();
    labelPrixNet = new JLabel();
    WPMR = new RiZoneSortie();
    UREM1 = new RiZoneSortie();
    UREM2 = new RiZoneSortie();
    UREM3 = new RiZoneSortie();
    WCOE = new RiZoneSortie();
    UREM4 = new RiZoneSortie();
    UREM5 = new RiZoneSortie();
    UREM6 = new RiZoneSortie();
    OBJ_97 = new JLabel();
    A1UNV = new RiZoneSortie();
    UCNVB = new RiZoneSortie();
    panel_cnv = new JPanel();
    WCNV = new XRiTextField();
    LIBCNV = new RiZoneSortie();
    labelPrixTTC = new JLabel();
    WPRX2 = new RiZoneSortie();
    xtp2 = new JXTitledPanel();
    OBJ_128 = new JLabel();
    OBJ_124 = new JLabel();
    OBJ_127 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_126 = new JLabel();
    WDISX = new RiZoneSortie();
    WSTKX = new RiZoneSortie();
    WRESX = new RiZoneSortie();
    WATTX = new RiZoneSortie();
    S1MIN = new RiZoneSortie();
    OBJ_129 = new JLabel();
    A1UNS = new RiZoneSortie();
    OBJ_131 = new JLabel();
    OBJ_133 = new JLabel();
    panel_mag = new JPanel();
    WMAG = new XRiTextField();
    MALIB = new RiZoneSortie();
    xtp1 = new JXTitledPanel();
    CAREF = new RiZoneSortie();
    OBJ_151 = new JLabel();
    UPNDVX = new RiZoneSortie();
    CAPRAX = new RiZoneSortie();
    OBJ_150 = new JLabel();
    CADAPX = new RiZoneSortie();
    OBJ_149 = new JLabel();
    OBJ_152 = new JLabel();
    OBJ_144 = new JLabel();
    OBJ_148 = new JLabel();
    OBJ_154 = new JLabel();
    CANUA = new RiZoneSortie();
    OBJ_153 = new JLabel();
    OBJ_145 = new RiZoneSortie();
    CAREM1 = new RiZoneSortie();
    OBJ_146 = new JLabel();
    CAUNA = new RiZoneSortie();
    OBJ_155 = new JLabel();
    CADEL = new RiZoneSortie();
    CAUNC = new RiZoneSortie();
    panel_frs = new JPanel();
    WFRS = new XRiTextField();
    FRSNOM = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(980, 720));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Interrogation de condition article");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- WART ----
          WART.setComponentPopupMenu(BTD);
          WART.setOpaque(false);
          WART.setText("@WART@");
          WART.setName("WART");

          //---- OBJ_61 ----
          OBJ_61.setText("Etablissement");
          OBJ_61.setName("OBJ_61");

          //---- WETB2 ----
          WETB2.setComponentPopupMenu(BTD);
          WETB2.setOpaque(false);
          WETB2.setText("@WETB@");
          WETB2.setName("WETB2");

          //---- OBJ_63 ----
          OBJ_63.setText("Article");
          OBJ_63.setName("OBJ_63");

          //---- OBJ_27 ----
          OBJ_27.setText("Client");
          OBJ_27.setName("OBJ_27");

          //---- CLNOM ----
          CLNOM.setComponentPopupMenu(BTD);
          CLNOM.setOpaque(false);
          CLNOM.setText("@CLNOM@");
          CLNOM.setName("CLNOM");

          //---- CLCLI ----
          CLCLI.setComponentPopupMenu(BTD);
          CLCLI.setOpaque(false);
          CLCLI.setText("@CLCLI@");
          CLCLI.setHorizontalAlignment(SwingConstants.RIGHT);
          CLCLI.setHorizontalTextPosition(SwingConstants.RIGHT);
          CLCLI.setName("CLCLI");

          //---- CLLIV ----
          CLLIV.setComponentPopupMenu(BTD);
          CLLIV.setOpaque(false);
          CLLIV.setText("@CLLIV@");
          CLLIV.setHorizontalAlignment(SwingConstants.RIGHT);
          CLLIV.setHorizontalTextPosition(SwingConstants.RIGHT);
          CLLIV.setName("CLLIV");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(90, 90, 90)
                    .addComponent(WETB2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(WART, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(CLCLI, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CLLIV, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CLNOM, GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(WETB2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addComponent(WART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addComponent(CLCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(CLLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(CLNOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);

            //---- bouton_valider2 ----
            bouton_valider2.setText("Valider prix");
            bouton_valider2.setName("bouton_valider2");
            bouton_valider2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_valider2ActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider2);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);

            //---- bouton_retour2 ----
            bouton_retour2.setText("Autre article");
            bouton_retour2.setName("bouton_retour2");
            bouton_retour2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retour2ActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour2);
          }
          menus_bas.add(navig_retour);

          //======== navig_retour2 ========
          {
            navig_retour2.setName("navig_retour2");

            //---- bouton_retour4 ----
            bouton_retour4.setText("Autre client");
            bouton_retour4.setToolTipText("Retour");
            bouton_retour4.setName("bouton_retour4");
            bouton_retour4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour2.add(bouton_retour4);
          }
          menus_bas.add(navig_retour2);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Options article");
              riSousMenu_bt6.setToolTipText("Options article");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Prix flash");
              riSousMenu_bt1.setToolTipText("Prix flash");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);

            //======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");

              //---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Conditions vente client");
              riSousMenu_bt16.setToolTipText("Conditions de vente du client");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Attendu/command\u00e9");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Calcul de la marge");
              riSousMenu_bt8.setToolTipText("On/Off affichage du calcul de la marge");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Saisie de bon");
              riSousMenu_bt9.setToolTipText("Saisie de bon");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Recherche de bon");
              riSousMenu_bt10.setToolTipText("Recherche de bon");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Historique de conso.");
              riSousMenu_bt11.setToolTipText("Historique de consommation");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Infos achat");
              riSousMenu_bt15.setToolTipText("Informations achat");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc notes");
              riSousMenu_bt14.setToolTipText("Bloc notes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 620));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setForeground(Color.black);
          p_contenu.setMinimumSize(new Dimension(780, 620));
          p_contenu.setName("p_contenu");

          //======== xtp4 ========
          {
            xtp4.setTitle("Article");
            xtp4.setBorder(new DropShadowBorder());
            xtp4.setName("xtp4");
            Container xtp4ContentContainer = xtp4.getContentContainer();
            xtp4ContentContainer.setLayout(null);

            //---- A1LIB ----
            A1LIB.setText("@A1LIB@");
            A1LIB.setFont(new Font("sansserif", Font.BOLD, 12));
            A1LIB.setName("A1LIB");
            xtp4ContentContainer.add(A1LIB);
            A1LIB.setBounds(15, 35, 232, A1LIB.getPreferredSize().height);

            //---- A1LB1 ----
            A1LB1.setText("@A1LB1@");
            A1LB1.setName("A1LB1");
            xtp4ContentContainer.add(A1LB1);
            A1LB1.setBounds(15, 60, 232, A1LB1.getPreferredSize().height);

            //---- A1LB2 ----
            A1LB2.setText("@A1LB2@");
            A1LB2.setName("A1LB2");
            xtp4ContentContainer.add(A1LB2);
            A1LB2.setBounds(15, 85, 232, A1LB2.getPreferredSize().height);

            //---- A1LB3 ----
            A1LB3.setText("@A1LB3@");
            A1LB3.setName("A1LB3");
            xtp4ContentContainer.add(A1LB3);
            A1LB3.setBounds(15, 110, 232, A1LB3.getPreferredSize().height);

            //---- OBJ_80 ----
            OBJ_80.setText("@FALIBR@");
            OBJ_80.setName("OBJ_80");
            xtp4ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(270, 110, 210, OBJ_80.getPreferredSize().height);

            //---- OBJ_81 ----
            OBJ_81.setText("@SFLIBR@");
            OBJ_81.setName("OBJ_81");
            xtp4ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(489, 110, 215, OBJ_81.getPreferredSize().height);

            //---- A1CL1 ----
            A1CL1.setText("@A1CL1@");
            A1CL1.setName("A1CL1");
            xtp4ContentContainer.add(A1CL1);
            A1CL1.setBounds(300, 35, 180, A1CL1.getPreferredSize().height);

            //---- A1CL2 ----
            A1CL2.setText("@A1CL2@");
            A1CL2.setName("A1CL2");
            xtp4ContentContainer.add(A1CL2);
            A1CL2.setBounds(300, 60, 180, A1CL2.getPreferredSize().height);

            //---- OBJ_67 ----
            OBJ_67.setText("Prix dernier achat");
            OBJ_67.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_67.setName("OBJ_67");
            xtp4ContentContainer.add(OBJ_67);
            OBJ_67.setBounds(500, 62, 110, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("Classement");
            OBJ_56.setName("OBJ_56");
            xtp4ContentContainer.add(OBJ_56);
            OBJ_56.setBounds(270, 12, 93, 20);

            //---- OBJ_72 ----
            OBJ_72.setText("Fam./Ss Fam.");
            OBJ_72.setName("OBJ_72");
            xtp4ContentContainer.add(OBJ_72);
            OBJ_72.setBounds(270, 87, 88, 20);

            //---- A1PRV ----
            A1PRV.setText("@A1PRV@");
            A1PRV.setHorizontalAlignment(SwingConstants.RIGHT);
            A1PRV.setName("A1PRV");
            xtp4ContentContainer.add(A1PRV);
            A1PRV.setBounds(620, 10, 84, A1PRV.getPreferredSize().height);

            //---- XPUMP ----
            XPUMP.setText("@XPUMP@");
            XPUMP.setHorizontalAlignment(SwingConstants.RIGHT);
            XPUMP.setName("XPUMP");
            xtp4ContentContainer.add(XPUMP);
            XPUMP.setBounds(620, 35, 84, XPUMP.getPreferredSize().height);

            //---- XPDA ----
            XPDA.setText("@XPDA@");
            XPDA.setHorizontalAlignment(SwingConstants.RIGHT);
            XPDA.setName("XPDA");
            xtp4ContentContainer.add(XPDA);
            XPDA.setBounds(620, 60, 84, XPDA.getPreferredSize().height);

            //---- OBJ_57 ----
            OBJ_57.setText("Prix revient");
            OBJ_57.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_57.setName("OBJ_57");
            xtp4ContentContainer.add(OBJ_57);
            OBJ_57.setBounds(525, 12, 85, 20);

            //---- OBJ_75 ----
            OBJ_75.setText("Date achat");
            OBJ_75.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_75.setName("OBJ_75");
            xtp4ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(525, 87, 85, 20);

            //---- OBJ_62 ----
            OBJ_62.setText("P.U.M.P.");
            OBJ_62.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_62.setName("OBJ_62");
            xtp4ContentContainer.add(OBJ_62);
            OBJ_62.setBounds(525, 37, 85, 20);

            //---- A1SFA ----
            A1SFA.setText("@A1SFA@");
            A1SFA.setName("A1SFA");
            xtp4ContentContainer.add(A1SFA);
            A1SFA.setBounds(420, 85, 60, A1SFA.getPreferredSize().height);

            //---- A1FAM ----
            A1FAM.setText("@A1FAM@");
            A1FAM.setName("A1FAM");
            xtp4ContentContainer.add(A1FAM);
            A1FAM.setBounds(380, 85, 36, A1FAM.getPreferredSize().height);

            //---- OBJ_55 ----
            OBJ_55.setText("Libell\u00e9s");
            OBJ_55.setName("OBJ_55");
            xtp4ContentContainer.add(OBJ_55);
            OBJ_55.setBounds(15, 12, 75, 20);

            //---- OBJ_60 ----
            OBJ_60.setText("1");
            OBJ_60.setName("OBJ_60");
            xtp4ContentContainer.add(OBJ_60);
            OBJ_60.setBounds(270, 37, 15, 20);

            //---- OBJ_65 ----
            OBJ_65.setText("2");
            OBJ_65.setName("OBJ_65");
            xtp4ContentContainer.add(OBJ_65);
            OBJ_65.setBounds(270, 62, 15, 20);

            //---- WDTDAX ----
            WDTDAX.setText("@WDTDAX@");
            WDTDAX.setName("WDTDAX");
            xtp4ContentContainer.add(WDTDAX);
            WDTDAX.setBounds(642, 85, 62, WDTDAX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xtp4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xtp4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xtp4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xtp4ContentContainer.setMinimumSize(preferredSize);
              xtp4ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xtp3 ========
          {
            xtp3.setTitle("Condition de vente (client/param\u00e8tre)");
            xtp3.setBorder(new DropShadowBorder());
            xtp3.setName("xtp3");
            Container xtp3ContentContainer = xtp3.getContentContainer();
            xtp3ContentContainer.setLayout(null);

            //---- UCNVA ----
            UCNVA.setText("@UCNVA@");
            UCNVA.setName("UCNVA");
            xtp3ContentContainer.add(UCNVA);
            UCNVA.setBounds(435, 25, 270, UCNVA.getPreferredSize().height);

            //---- TYPMAR ----
            TYPMAR.setText("@TYPMAR@");
            TYPMAR.setName("TYPMAR");
            xtp3ContentContainer.add(TYPMAR);
            TYPMAR.setBounds(15, 80, 195, 20);

            //---- OBJ_91 ----
            OBJ_91.setText("Condition appliqu\u00e9e");
            OBJ_91.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_91.setName("OBJ_91");
            xtp3ContentContainer.add(OBJ_91);
            OBJ_91.setBounds(280, 30, 142, 20);

            //---- OBJ_89 ----
            OBJ_89.setText("Quantit\u00e9 (UV)");
            OBJ_89.setName("OBJ_89");
            xtp3ContentContainer.add(OBJ_89);
            OBJ_89.setBounds(135, 30, 82, 20);

            //---- WPRX ----
            WPRX.setText("@WPRX@");
            WPRX.setHorizontalAlignment(SwingConstants.RIGHT);
            WPRX.setFont(WPRX.getFont().deriveFont(WPRX.getFont().getStyle() | Font.BOLD));
            WPRX.setName("WPRX");
            xtp3ContentContainer.add(WPRX);
            WPRX.setBounds(15, 50, 72, WPRX.getPreferredSize().height);

            //---- WPVB ----
            WPVB.setText("@WPVB@");
            WPVB.setHorizontalAlignment(SwingConstants.RIGHT);
            WPVB.setName("WPVB");
            xtp3ContentContainer.add(WPVB);
            WPVB.setBounds(285, 50, 72, WPVB.getPreferredSize().height);

            //---- OBJ_110 ----
            OBJ_110.setText("Unit\u00e9 vente");
            OBJ_110.setName("OBJ_110");
            xtp3ContentContainer.add(OBJ_110);
            OBJ_110.setBounds(600, 80, 70, 20);

            //---- OBJ_104 ----
            OBJ_104.setText("Coefficient");
            OBJ_104.setName("OBJ_104");
            xtp3ContentContainer.add(OBJ_104);
            OBJ_104.setBounds(560, 105, 66, 20);

            //---- WMMR ----
            WMMR.setText("@WMMR@");
            WMMR.setName("WMMR");
            xtp3ContentContainer.add(WMMR);
            WMMR.setBounds(15, 100, 72, WMMR.getPreferredSize().height);

            //---- WQTEX ----
            WQTEX.setComponentPopupMenu(BTD);
            WQTEX.setText("@WQTEX@");
            WQTEX.setHorizontalAlignment(SwingConstants.RIGHT);
            WQTEX.setName("WQTEX");
            xtp3ContentContainer.add(WQTEX);
            WQTEX.setBounds(135, 50, 74, WQTEX.getPreferredSize().height);

            //---- OBJ_100 ----
            OBJ_100.setText("Remise");
            OBJ_100.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_100.setName("OBJ_100");
            xtp3ContentContainer.add(OBJ_100);
            OBJ_100.setBounds(375, 55, 49, 20);

            //---- labelPrixNet ----
            labelPrixNet.setText("Prix net");
            labelPrixNet.setFont(labelPrixNet.getFont().deriveFont(labelPrixNet.getFont().getStyle() | Font.BOLD));
            labelPrixNet.setName("labelPrixNet");
            xtp3ContentContainer.add(labelPrixNet);
            labelPrixNet.setBounds(15, 30, 70, 20);

            //---- WPMR ----
            WPMR.setText("( @WPMR@ %)");
            WPMR.setName("WPMR");
            xtp3ContentContainer.add(WPMR);
            WPMR.setBounds(135, 100, 60, WPMR.getPreferredSize().height);

            //---- UREM1 ----
            UREM1.setText("@UREM1@");
            UREM1.setName("UREM1");
            xtp3ContentContainer.add(UREM1);
            UREM1.setBounds(435, 50, 40, UREM1.getPreferredSize().height);

            //---- UREM2 ----
            UREM2.setText("@UREM2@");
            UREM2.setName("UREM2");
            xtp3ContentContainer.add(UREM2);
            UREM2.setBounds(485, 50, 40, UREM2.getPreferredSize().height);

            //---- UREM3 ----
            UREM3.setText("@UREM3@");
            UREM3.setName("UREM3");
            xtp3ContentContainer.add(UREM3);
            UREM3.setBounds(535, 50, 40, UREM3.getPreferredSize().height);

            //---- WCOE ----
            WCOE.setText("@WCOE@");
            WCOE.setName("WCOE");
            xtp3ContentContainer.add(WCOE);
            WCOE.setBounds(645, 100, 60, WCOE.getPreferredSize().height);

            //---- UREM4 ----
            UREM4.setText("@UREM4@");
            UREM4.setName("UREM4");
            xtp3ContentContainer.add(UREM4);
            UREM4.setBounds(435, 75, 40, UREM4.getPreferredSize().height);

            //---- UREM5 ----
            UREM5.setText("@UREM5@");
            UREM5.setName("UREM5");
            xtp3ContentContainer.add(UREM5);
            UREM5.setBounds(485, 75, 40, UREM5.getPreferredSize().height);

            //---- UREM6 ----
            UREM6.setText("@UREM6@");
            UREM6.setName("UREM6");
            xtp3ContentContainer.add(UREM6);
            UREM6.setBounds(535, 75, 40, UREM6.getPreferredSize().height);

            //---- OBJ_97 ----
            OBJ_97.setText("Base");
            OBJ_97.setName("OBJ_97");
            xtp3ContentContainer.add(OBJ_97);
            OBJ_97.setBounds(230, 52, 36, 20);

            //---- A1UNV ----
            A1UNV.setText("@A1UNV@");
            A1UNV.setName("A1UNV");
            xtp3ContentContainer.add(A1UNV);
            A1UNV.setBounds(675, 75, 30, A1UNV.getPreferredSize().height);

            //---- UCNVB ----
            UCNVB.setText("@UCNVB@");
            UCNVB.setName("UCNVB");
            xtp3ContentContainer.add(UCNVB);
            UCNVB.setBounds(230, 52, 288, 20);

            //======== panel_cnv ========
            {
              panel_cnv.setOpaque(false);
              panel_cnv.setName("panel_cnv");
              panel_cnv.setLayout(null);

              //---- WCNV ----
              WCNV.setComponentPopupMenu(null);
              WCNV.setHorizontalAlignment(SwingConstants.RIGHT);
              WCNV.setName("WCNV");
              panel_cnv.add(WCNV);
              WCNV.setBounds(15, 2, 70, 25);

              //---- LIBCNV ----
              LIBCNV.setText("@LIBCNV@");
              LIBCNV.setName("LIBCNV");
              panel_cnv.add(LIBCNV);
              LIBCNV.setBounds(90, 5, 232, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel_cnv.getComponentCount(); i++) {
                  Rectangle bounds = panel_cnv.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel_cnv.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel_cnv.setMinimumSize(preferredSize);
                panel_cnv.setPreferredSize(preferredSize);
              }
            }
            xtp3ContentContainer.add(panel_cnv);
            panel_cnv.setBounds(50, 0, 335, 30);

            //---- labelPrixTTC ----
            labelPrixTTC.setText("Prix TTC");
            labelPrixTTC.setFont(labelPrixTTC.getFont().deriveFont(labelPrixTTC.getFont().getStyle() | Font.BOLD));
            labelPrixTTC.setName("labelPrixTTC");
            xtp3ContentContainer.add(labelPrixTTC);
            labelPrixTTC.setBounds(15, 30, 70, 20);

            //---- WPRX2 ----
            WPRX2.setText("@WPRX2@");
            WPRX2.setHorizontalAlignment(SwingConstants.RIGHT);
            WPRX2.setFont(WPRX2.getFont().deriveFont(WPRX2.getFont().getStyle() | Font.BOLD));
            WPRX2.setName("WPRX2");
            xtp3ContentContainer.add(WPRX2);
            WPRX2.setBounds(15, 50, 72, WPRX2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xtp3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xtp3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xtp3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xtp3ContentContainer.setMinimumSize(preferredSize);
              xtp3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xtp2 ========
          {
            xtp2.setTitle("Position stock globale sur magasin");
            xtp2.setBorder(new DropShadowBorder());
            xtp2.setName("xtp2");
            Container xtp2ContentContainer = xtp2.getContentContainer();
            xtp2ContentContainer.setLayout(null);

            //---- OBJ_128 ----
            OBJ_128.setText("Stock minimum");
            OBJ_128.setName("OBJ_128");
            xtp2ContentContainer.add(OBJ_128);
            OBJ_128.setBounds(560, 25, 93, 20);

            //---- OBJ_124 ----
            OBJ_124.setText("Disponible");
            OBJ_124.setName("OBJ_124");
            xtp2ContentContainer.add(OBJ_124);
            OBJ_124.setBounds(15, 25, 87, 20);

            //---- OBJ_127 ----
            OBJ_127.setText("Attendu");
            OBJ_127.setName("OBJ_127");
            xtp2ContentContainer.add(OBJ_127);
            OBJ_127.setBounds(435, 25, 76, 20);

            //---- OBJ_125 ----
            OBJ_125.setText("En stock");
            OBJ_125.setName("OBJ_125");
            xtp2ContentContainer.add(OBJ_125);
            OBJ_125.setBounds(140, 25, 75, 20);

            //---- OBJ_126 ----
            OBJ_126.setText("Reserv\u00e9");
            OBJ_126.setName("OBJ_126");
            xtp2ContentContainer.add(OBJ_126);
            OBJ_126.setBounds(290, 25, 73, 20);

            //---- WDISX ----
            WDISX.setText("@WDISX@");
            WDISX.setHorizontalAlignment(SwingConstants.RIGHT);
            WDISX.setName("WDISX");
            xtp2ContentContainer.add(WDISX);
            WDISX.setBounds(15, 45, 72, WDISX.getPreferredSize().height);

            //---- WSTKX ----
            WSTKX.setText("@WSTKX@");
            WSTKX.setHorizontalAlignment(SwingConstants.RIGHT);
            WSTKX.setName("WSTKX");
            xtp2ContentContainer.add(WSTKX);
            WSTKX.setBounds(140, 45, 72, WSTKX.getPreferredSize().height);

            //---- WRESX ----
            WRESX.setText("@WRESX@");
            WRESX.setHorizontalAlignment(SwingConstants.RIGHT);
            WRESX.setName("WRESX");
            xtp2ContentContainer.add(WRESX);
            WRESX.setBounds(285, 45, 72, WRESX.getPreferredSize().height);

            //---- WATTX ----
            WATTX.setText("@WATTX@");
            WATTX.setHorizontalAlignment(SwingConstants.RIGHT);
            WATTX.setName("WATTX");
            xtp2ContentContainer.add(WATTX);
            WATTX.setBounds(435, 45, 72, WATTX.getPreferredSize().height);

            //---- S1MIN ----
            S1MIN.setText("@S1MIN@");
            S1MIN.setHorizontalAlignment(SwingConstants.RIGHT);
            S1MIN.setName("S1MIN");
            xtp2ContentContainer.add(S1MIN);
            S1MIN.setBounds(560, 45, 72, S1MIN.getPreferredSize().height);

            //---- OBJ_129 ----
            OBJ_129.setText("US");
            OBJ_129.setName("OBJ_129");
            xtp2ContentContainer.add(OBJ_129);
            OBJ_129.setBounds(680, 25, 30, 20);

            //---- A1UNS ----
            A1UNS.setText("@A1UNS@");
            A1UNS.setName("A1UNS");
            xtp2ContentContainer.add(A1UNS);
            A1UNS.setBounds(680, 45, 30, A1UNS.getPreferredSize().height);

            //---- OBJ_131 ----
            OBJ_131.setText("=");
            OBJ_131.setName("OBJ_131");
            xtp2ContentContainer.add(OBJ_131);
            OBJ_131.setBounds(110, 47, 12, 20);

            //---- OBJ_133 ----
            OBJ_133.setText("-");
            OBJ_133.setName("OBJ_133");
            xtp2ContentContainer.add(OBJ_133);
            OBJ_133.setBounds(250, 47, 12, 20);

            //======== panel_mag ========
            {
              panel_mag.setOpaque(false);
              panel_mag.setPreferredSize(new Dimension(322, 27));
              panel_mag.setName("panel_mag");
              panel_mag.setLayout(null);

              //---- WMAG ----
              WMAG.setComponentPopupMenu(null);
              WMAG.setName("WMAG");
              panel_mag.add(WMAG);
              WMAG.setBounds(5, 2, 34, 25);

              //---- MALIB ----
              MALIB.setText("@MALIB@");
              MALIB.setName("MALIB");
              panel_mag.add(MALIB);
              MALIB.setBounds(50, 4, 232, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel_mag.getComponentCount(); i++) {
                  Rectangle bounds = panel_mag.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel_mag.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel_mag.setMinimumSize(preferredSize);
                panel_mag.setPreferredSize(preferredSize);
              }
            }
            xtp2ContentContainer.add(panel_mag);
            panel_mag.setBounds(10, 0, 335, 30);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xtp2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xtp2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xtp2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xtp2ContentContainer.setMinimumSize(preferredSize);
              xtp2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xtp1 ========
          {
            xtp1.setTitle("Conditions d'achat principales");
            xtp1.setBorder(new DropShadowBorder());
            xtp1.setName("xtp1");
            Container xtp1ContentContainer = xtp1.getContentContainer();
            xtp1ContentContainer.setLayout(null);

            //---- CAREF ----
            CAREF.setComponentPopupMenu(BTD);
            CAREF.setText("@CAREF@");
            CAREF.setName("CAREF");
            xtp1ContentContainer.add(CAREF);
            CAREF.setBounds(100, 60, 150, CAREF.getPreferredSize().height);

            //---- OBJ_151 ----
            OBJ_151.setText("Prix catalogue");
            OBJ_151.setName("OBJ_151");
            xtp1ContentContainer.add(OBJ_151);
            OBJ_151.setBounds(360, 40, 88, 20);

            //---- UPNDVX ----
            UPNDVX.setText("@UPNDVX@");
            UPNDVX.setHorizontalAlignment(SwingConstants.RIGHT);
            UPNDVX.setName("UPNDVX");
            xtp1ContentContainer.add(UPNDVX);
            UPNDVX.setBounds(15, 60, 72, UPNDVX.getPreferredSize().height);

            //---- CAPRAX ----
            CAPRAX.setText("@CAPRAX@");
            CAPRAX.setHorizontalAlignment(SwingConstants.RIGHT);
            CAPRAX.setName("CAPRAX");
            xtp1ContentContainer.add(CAPRAX);
            CAPRAX.setBounds(360, 60, 72, CAPRAX.getPreferredSize().height);

            //---- OBJ_150 ----
            OBJ_150.setText("Application");
            OBJ_150.setName("OBJ_150");
            xtp1ContentContainer.add(OBJ_150);
            OBJ_150.setBounds(270, 40, 70, 20);

            //---- CADAPX ----
            CADAPX.setText("@CADAPX@");
            CADAPX.setHorizontalAlignment(SwingConstants.CENTER);
            CADAPX.setName("CADAPX");
            xtp1ContentContainer.add(CADAPX);
            CADAPX.setBounds(270, 60, 65, CADAPX.getPreferredSize().height);

            //---- OBJ_149 ----
            OBJ_149.setText("R\u00e9f\u00e9rence");
            OBJ_149.setName("OBJ_149");
            xtp1ContentContainer.add(OBJ_149);
            OBJ_149.setBounds(100, 40, 75, 20);

            //---- OBJ_152 ----
            OBJ_152.setText("Remise(s)");
            OBJ_152.setName("OBJ_152");
            xtp1ContentContainer.add(OBJ_152);
            OBJ_152.setBounds(480, 40, 70, 20);

            //---- OBJ_144 ----
            OBJ_144.setText("Devise");
            OBJ_144.setName("OBJ_144");
            xtp1ContentContainer.add(OBJ_144);
            OBJ_144.setBounds(430, 12, 53, 20);

            //---- OBJ_148 ----
            OBJ_148.setText("Prix net");
            OBJ_148.setName("OBJ_148");
            xtp1ContentContainer.add(OBJ_148);
            OBJ_148.setBounds(15, 40, 55, 20);

            //---- OBJ_154 ----
            OBJ_154.setText("Condt");
            OBJ_154.setName("OBJ_154");
            xtp1ContentContainer.add(OBJ_154);
            OBJ_154.setBounds(595, 40, 45, 20);

            //---- CANUA ----
            CANUA.setText("@CANUA@");
            CANUA.setName("CANUA");
            xtp1ContentContainer.add(CANUA);
            CANUA.setBounds(595, 60, 76, CANUA.getPreferredSize().height);

            //---- OBJ_153 ----
            OBJ_153.setText("D\u00e9lai");
            OBJ_153.setName("OBJ_153");
            xtp1ContentContainer.add(OBJ_153);
            OBJ_153.setBounds(555, 40, 40, 20);

            //---- OBJ_145 ----
            OBJ_145.setText("@CADEV@");
            OBJ_145.setName("OBJ_145");
            xtp1ContentContainer.add(OBJ_145);
            OBJ_145.setBounds(479, 10, 34, OBJ_145.getPreferredSize().height);

            //---- CAREM1 ----
            CAREM1.setText("@CAREM1@");
            CAREM1.setName("CAREM1");
            xtp1ContentContainer.add(CAREM1);
            CAREM1.setBounds(480, 60, 33, CAREM1.getPreferredSize().height);

            //---- OBJ_146 ----
            OBJ_146.setText("UA");
            OBJ_146.setName("OBJ_146");
            xtp1ContentContainer.add(OBJ_146);
            OBJ_146.setBounds(565, 12, 25, 20);

            //---- CAUNA ----
            CAUNA.setText("@CAUNA@");
            CAUNA.setName("CAUNA");
            xtp1ContentContainer.add(CAUNA);
            CAUNA.setBounds(595, 10, 24, CAUNA.getPreferredSize().height);

            //---- OBJ_155 ----
            OBJ_155.setText("UC");
            OBJ_155.setName("OBJ_155");
            xtp1ContentContainer.add(OBJ_155);
            OBJ_155.setBounds(680, 40, 30, 20);

            //---- CADEL ----
            CADEL.setText("@CADEL@");
            CADEL.setName("CADEL");
            xtp1ContentContainer.add(CADEL);
            CADEL.setBounds(555, 60, 22, CADEL.getPreferredSize().height);

            //---- CAUNC ----
            CAUNC.setText("@CAUNC@");
            CAUNC.setName("CAUNC");
            xtp1ContentContainer.add(CAUNC);
            CAUNC.setBounds(680, 60, 30, CAUNC.getPreferredSize().height);

            //======== panel_frs ========
            {
              panel_frs.setOpaque(false);
              panel_frs.setMinimumSize(new Dimension(322, 27));
              panel_frs.setName("panel_frs");
              panel_frs.setLayout(null);

              //---- WFRS ----
              WFRS.setComponentPopupMenu(null);
              WFRS.setHorizontalAlignment(SwingConstants.RIGHT);
              WFRS.setName("WFRS");
              panel_frs.add(WFRS);
              WFRS.setBounds(20, 0, 70, 25);

              //---- FRSNOM ----
              FRSNOM.setText("@FRNOM@");
              FRSNOM.setName("FRSNOM");
              panel_frs.add(FRSNOM);
              FRSNOM.setBounds(95, 2, 232, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel_frs.getComponentCount(); i++) {
                  Rectangle bounds = panel_frs.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel_frs.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel_frs.setMinimumSize(preferredSize);
                panel_frs.setPreferredSize(preferredSize);
              }
            }
            xtp1ContentContainer.add(panel_frs);
            panel_frs.setBounds(80, 5, 335, 30);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xtp1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xtp1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xtp1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xtp1ContentContainer.setMinimumSize(preferredSize);
              xtp1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xtp4, GroupLayout.DEFAULT_SIZE, 754, Short.MAX_VALUE)
                  .addComponent(xtp3, GroupLayout.DEFAULT_SIZE, 754, Short.MAX_VALUE)
                  .addComponent(xtp1, GroupLayout.DEFAULT_SIZE, 754, Short.MAX_VALUE)
                  .addComponent(xtp2, GroupLayout.DEFAULT_SIZE, 754, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(xtp4, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(xtp3, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(xtp2, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(xtp1, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie WART;
  private JLabel OBJ_61;
  private RiZoneSortie WETB2;
  private JLabel OBJ_63;
  private JLabel OBJ_27;
  private RiZoneSortie CLNOM;
  private RiZoneSortie CLCLI;
  private RiZoneSortie CLLIV;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu_bt bouton_valider2;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private RiMenu_bt bouton_retour2;
  private RiMenu navig_retour2;
  private RiMenu_bt bouton_retour4;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xtp4;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1LB1;
  private RiZoneSortie A1LB2;
  private RiZoneSortie A1LB3;
  private RiZoneSortie OBJ_80;
  private RiZoneSortie OBJ_81;
  private RiZoneSortie A1CL1;
  private RiZoneSortie A1CL2;
  private JLabel OBJ_67;
  private JLabel OBJ_56;
  private JLabel OBJ_72;
  private RiZoneSortie A1PRV;
  private RiZoneSortie XPUMP;
  private RiZoneSortie XPDA;
  private JLabel OBJ_57;
  private JLabel OBJ_75;
  private JLabel OBJ_62;
  private RiZoneSortie A1SFA;
  private RiZoneSortie A1FAM;
  private JLabel OBJ_55;
  private JLabel OBJ_60;
  private JLabel OBJ_65;
  private RiZoneSortie WDTDAX;
  private JXTitledPanel xtp3;
  private RiZoneSortie UCNVA;
  private JLabel TYPMAR;
  private JLabel OBJ_91;
  private JLabel OBJ_89;
  private RiZoneSortie WPRX;
  private RiZoneSortie WPVB;
  private JLabel OBJ_110;
  private JLabel OBJ_104;
  private RiZoneSortie WMMR;
  private RiZoneSortie WQTEX;
  private JLabel OBJ_100;
  private JLabel labelPrixNet;
  private RiZoneSortie WPMR;
  private RiZoneSortie UREM1;
  private RiZoneSortie UREM2;
  private RiZoneSortie UREM3;
  private RiZoneSortie WCOE;
  private RiZoneSortie UREM4;
  private RiZoneSortie UREM5;
  private RiZoneSortie UREM6;
  private JLabel OBJ_97;
  private RiZoneSortie A1UNV;
  private RiZoneSortie UCNVB;
  private JPanel panel_cnv;
  private XRiTextField WCNV;
  private RiZoneSortie LIBCNV;
  private JLabel labelPrixTTC;
  private RiZoneSortie WPRX2;
  private JXTitledPanel xtp2;
  private JLabel OBJ_128;
  private JLabel OBJ_124;
  private JLabel OBJ_127;
  private JLabel OBJ_125;
  private JLabel OBJ_126;
  private RiZoneSortie WDISX;
  private RiZoneSortie WSTKX;
  private RiZoneSortie WRESX;
  private RiZoneSortie WATTX;
  private RiZoneSortie S1MIN;
  private JLabel OBJ_129;
  private RiZoneSortie A1UNS;
  private JLabel OBJ_131;
  private JLabel OBJ_133;
  private JPanel panel_mag;
  private XRiTextField WMAG;
  private RiZoneSortie MALIB;
  private JXTitledPanel xtp1;
  private RiZoneSortie CAREF;
  private JLabel OBJ_151;
  private RiZoneSortie UPNDVX;
  private RiZoneSortie CAPRAX;
  private JLabel OBJ_150;
  private RiZoneSortie CADAPX;
  private JLabel OBJ_149;
  private JLabel OBJ_152;
  private JLabel OBJ_144;
  private JLabel OBJ_148;
  private JLabel OBJ_154;
  private RiZoneSortie CANUA;
  private JLabel OBJ_153;
  private RiZoneSortie OBJ_145;
  private RiZoneSortie CAREM1;
  private JLabel OBJ_146;
  private RiZoneSortie CAUNA;
  private JLabel OBJ_155;
  private RiZoneSortie CADEL;
  private RiZoneSortie CAUNC;
  private JPanel panel_frs;
  private XRiTextField WFRS;
  private RiZoneSortie FRSNOM;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
