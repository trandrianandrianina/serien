
package ri.serien.libecranrpg.vgvx.VGVXAIFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXAIFM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 541, };
  
  public VGVXAIFM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    barre_dupl.setVisible(lexique.isTrue("56"));
    lb_dupl.setVisible(lexique.isTrue("56"));
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    INDMAG.setEnabled(lexique.isPresent("INDMAG"));
    INDETB.setEnabled(lexique.isPresent("INDETB"));
    INDIMP.setEnabled(lexique.isPresent("INDIMP"));
    INDECR.setEnabled(lexique.isPresent("INDECR"));
    OBJ_52.setVisible(lexique.isPresent("V01F"));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "2", "Enter");
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "4", "Enter");
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "5", "Enter");
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "ENTER", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    INDECR = new XRiTextField();
    OBJ_49 = new JLabel();
    INDIMP = new XRiTextField();
    INDETB = new XRiTextField();
    OBJ_48 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_51 = new JLabel();
    INDMAG = new XRiTextField();
    lb_dupl = new JLabel();
    OBJ_56 = new JLabel();
    INDREF = new XRiTextField();
    p_tete_droite = new JPanel();
    barre_dupl = new JMenuBar();
    p_tete_dupl = new JPanel();
    IN3ECR = new XRiTextField();
    OBJ_52 = new JLabel();
    IN3IMP = new XRiTextField();
    IN3ETB = new XRiTextField();
    OBJ_53 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_55 = new JLabel();
    IN3MAG = new XRiTextField();
    lb_dupl2 = new JLabel();
    OBJ_57 = new JLabel();
    IN3REF = new XRiTextField();
    p_tete_droite2 = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_27 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("AFFECTATIONS IMPRESSIONS");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(880, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- INDECR ----
          INDECR.setComponentPopupMenu(BTD);
          INDECR.setName("INDECR");

          //---- OBJ_49 ----
          OBJ_49.setText("Profil");
          OBJ_49.setName("OBJ_49");

          //---- INDIMP ----
          INDIMP.setComponentPopupMenu(BTD);
          INDIMP.setName("INDIMP");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_48 ----
          OBJ_48.setText("Etat");
          OBJ_48.setName("OBJ_48");

          //---- OBJ_50 ----
          OBJ_50.setText("Etablissement");
          OBJ_50.setName("OBJ_50");

          //---- OBJ_51 ----
          OBJ_51.setText("Magasin");
          OBJ_51.setName("OBJ_51");

          //---- INDMAG ----
          INDMAG.setComponentPopupMenu(BTD);
          INDMAG.setName("INDMAG");

          //---- lb_dupl ----
          lb_dupl.setText("Par duplication de");
          lb_dupl.setName("lb_dupl");

          //---- OBJ_56 ----
          OBJ_56.setText("R\u00e9f\u00e9rence");
          OBJ_56.setName("OBJ_56");

          //---- INDREF ----
          INDREF.setComponentPopupMenu(BTD);
          INDREF.setName("INDREF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(lb_dupl, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(30, 30, 30)
                    .addComponent(INDIMP, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDECR, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDMAG, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDREF, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(lb_dupl, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDIMP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_48))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_49))
              .addComponent(INDECR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_50))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_51))
              .addComponent(INDMAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_56))
              .addComponent(INDREF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);

      //======== barre_dupl ========
      {
        barre_dupl.setMinimumSize(new Dimension(111, 34));
        barre_dupl.setPreferredSize(new Dimension(111, 34));
        barre_dupl.setName("barre_dupl");

        //======== p_tete_dupl ========
        {
          p_tete_dupl.setOpaque(false);
          p_tete_dupl.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_dupl.setPreferredSize(new Dimension(880, 32));
          p_tete_dupl.setMinimumSize(new Dimension(700, 32));
          p_tete_dupl.setName("p_tete_dupl");

          //---- IN3ECR ----
          IN3ECR.setComponentPopupMenu(BTD);
          IN3ECR.setName("IN3ECR");

          //---- OBJ_52 ----
          OBJ_52.setText("Profil");
          OBJ_52.setName("OBJ_52");

          //---- IN3IMP ----
          IN3IMP.setComponentPopupMenu(BTD);
          IN3IMP.setName("IN3IMP");

          //---- IN3ETB ----
          IN3ETB.setComponentPopupMenu(BTD);
          IN3ETB.setName("IN3ETB");

          //---- OBJ_53 ----
          OBJ_53.setText("Etat");
          OBJ_53.setName("OBJ_53");

          //---- OBJ_54 ----
          OBJ_54.setText("Etablissement");
          OBJ_54.setName("OBJ_54");

          //---- OBJ_55 ----
          OBJ_55.setText("Magasin");
          OBJ_55.setName("OBJ_55");

          //---- IN3MAG ----
          IN3MAG.setComponentPopupMenu(BTD);
          IN3MAG.setName("IN3MAG");

          //---- lb_dupl2 ----
          lb_dupl2.setText("Par duplication de");
          lb_dupl2.setFont(lb_dupl2.getFont().deriveFont(lb_dupl2.getFont().getStyle() | Font.BOLD));
          lb_dupl2.setName("lb_dupl2");

          //---- OBJ_57 ----
          OBJ_57.setText("R\u00e9f\u00e9rence");
          OBJ_57.setName("OBJ_57");

          //---- IN3REF ----
          IN3REF.setComponentPopupMenu(BTD);
          IN3REF.setName("IN3REF");

          GroupLayout p_tete_duplLayout = new GroupLayout(p_tete_dupl);
          p_tete_dupl.setLayout(p_tete_duplLayout);
          p_tete_duplLayout.setHorizontalGroup(
            p_tete_duplLayout.createParallelGroup()
              .addGroup(p_tete_duplLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(lb_dupl2, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_tete_duplLayout.createParallelGroup()
                  .addGroup(p_tete_duplLayout.createSequentialGroup()
                    .addGap(30, 30, 30)
                    .addComponent(IN3IMP, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(IN3ECR, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(IN3MAG, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(IN3REF, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
          p_tete_duplLayout.setVerticalGroup(
            p_tete_duplLayout.createParallelGroup()
              .addGroup(p_tete_duplLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(lb_dupl2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(IN3IMP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_duplLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_53))
              .addGroup(p_tete_duplLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_52))
              .addComponent(IN3ECR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_duplLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_54))
              .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_duplLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_55))
              .addComponent(IN3MAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_duplLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_57))
              .addComponent(IN3REF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_dupl.add(p_tete_dupl);

        //======== p_tete_droite2 ========
        {
          p_tete_droite2.setOpaque(false);
          p_tete_droite2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite2.setPreferredSize(new Dimension(150, 0));
          p_tete_droite2.setMinimumSize(new Dimension(150, 0));
          p_tete_droite2.setName("p_tete_droite2");
          p_tete_droite2.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_dupl.add(p_tete_droite2);
      }
      p_nord.add(barre_dupl);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(670, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(25, 45, 558, 269);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(590, 45, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(590, 190, 25, 125);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 644, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_20 ----
      OBJ_20.setText("Modifier");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("Supprimer");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("Interroger");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
      BTD.addSeparator();

      //---- OBJ_27 ----
      OBJ_27.setText("Aide en ligne");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField INDECR;
  private JLabel OBJ_49;
  private XRiTextField INDIMP;
  private XRiTextField INDETB;
  private JLabel OBJ_48;
  private JLabel OBJ_50;
  private JLabel OBJ_51;
  private XRiTextField INDMAG;
  private JLabel lb_dupl;
  private JLabel OBJ_56;
  private XRiTextField INDREF;
  private JPanel p_tete_droite;
  private JMenuBar barre_dupl;
  private JPanel p_tete_dupl;
  private XRiTextField IN3ECR;
  private JLabel OBJ_52;
  private XRiTextField IN3IMP;
  private XRiTextField IN3ETB;
  private JLabel OBJ_53;
  private JLabel OBJ_54;
  private JLabel OBJ_55;
  private XRiTextField IN3MAG;
  private JLabel lb_dupl2;
  private JLabel OBJ_57;
  private XRiTextField IN3REF;
  private JPanel p_tete_droite2;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_27;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
