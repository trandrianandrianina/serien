
package ri.serien.libecranrpg.vgvx.VGVXCCFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVXCCFM_C4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXCCFM_C4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    CCCDES.setValeursSelection("OUI", "NON");
    CCCLIV.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    CCATTN.setEnabled(lexique.isPresent("CCATTN"));
    CCNOTE.setEnabled(lexique.isPresent("CCNOTE"));
    CCPTSF.setEnabled(lexique.isPresent("CCPTSF"));
    CCPTSD.setEnabled(lexique.isPresent("CCPTSD"));
    CCSAN5.setEnabled(lexique.isPresent("CCSAN5"));
    CCSAN4.setEnabled(lexique.isPresent("CCSAN4"));
    CCSAN3.setEnabled(lexique.isPresent("CCSAN3"));
    CCSAN2.setEnabled(lexique.isPresent("CCSAN2"));
    CCSAN1.setEnabled(lexique.isPresent("CCSAN1"));
    CCAEXF.setEnabled(lexique.isPresent("CCAEXF"));
    CCAEXD.setEnabled(lexique.isPresent("CCAEXD"));
    CCPLEF.setEnabled(lexique.isPresent("CCPLEF"));
    CCPLED.setEnabled(lexique.isPresent("CCPLED"));
    // CCCDES.setVisible( lexique.isPresent("CCCDES"));
    // CCCDES.setSelected(lexique.HostFieldGetData("CCCDES").equalsIgnoreCase("OUI"));
    // CCCLIV.setVisible( lexique.isPresent("CCCLIV"));
    // CCCLIV.setSelected(lexique.HostFieldGetData("CCCLIV").equalsIgnoreCase("OUI"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Critères de sélection"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (CCCDES.isSelected())
    // lexique.HostFieldPutData("CCCDES", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CCCDES", 0, "NON");
    // if (CCCLIV.isSelected())
    // lexique.HostFieldPutData("CCCLIV", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CCCLIV", 0, "NON");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_ann = new RiSousMenu();
    riSousMenu_bt_ann = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_32 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_21 = new JLabel();
    P_PnlOpts = new JPanel();
    CCCLIV = new XRiCheckBox();
    CCCDES = new XRiCheckBox();
    OBJ_39 = new JLabel();
    OBJ_41 = new JLabel();
    CCPLED = new XRiTextField();
    CCPLEF = new XRiTextField();
    CCAEXD = new XRiTextField();
    CCAEXF = new XRiTextField();
    OBJ_28 = new JLabel();
    OBJ_33 = new JLabel();
    CCSAN1 = new XRiTextField();
    CCSAN2 = new XRiTextField();
    CCSAN3 = new XRiTextField();
    CCSAN4 = new XRiTextField();
    CCSAN5 = new XRiTextField();
    CCPTSD = new XRiTextField();
    CCPTSF = new XRiTextField();
    CCNOTE = new XRiTextField();
    OBJ_30 = new JLabel();
    OBJ_35 = new JLabel();
    CCATTN = new XRiTextField();
    OBJ_43 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(730, 285));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_ann ========
          {
            riSousMenu_ann.setName("riSousMenu_ann");

            //---- riSousMenu_bt_ann ----
            riSousMenu_bt_ann.setText("Annulation");
            riSousMenu_bt_ann.setToolTipText("Annulation");
            riSousMenu_bt_ann.setName("riSousMenu_bt_ann");
            riSousMenu_ann.add(riSousMenu_bt_ann);
          }
          menus_haut.add(riSousMenu_ann);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setToolTipText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Chiffrage"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_32 ----
          OBJ_32.setText("Montant assimil\u00e9 export");
          OBJ_32.setName("OBJ_32");
          p_recup.add(OBJ_32);
          OBJ_32.setBounds(30, 114, 165, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("Plafond d'encours");
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(30, 79, 165, 20);

          //---- OBJ_37 ----
          OBJ_37.setText("Attention/blocage");
          OBJ_37.setName("OBJ_37");
          p_recup.add(OBJ_37);
          OBJ_37.setBounds(30, 149, 165, 20);

          //---- OBJ_21 ----
          OBJ_21.setText("Affaire analytique");
          OBJ_21.setName("OBJ_21");
          p_recup.add(OBJ_21);
          OBJ_21.setBounds(30, 44, 165, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- CCCLIV ----
          CCCLIV.setText("Client livr\u00e9");
          CCCLIV.setComponentPopupMenu(BTD);
          CCCLIV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CCCLIV.setName("CCCLIV");
          p_recup.add(CCCLIV);
          CCCLIV.setBounds(205, 180, 87, 20);

          //---- CCCDES ----
          CCCDES.setText("D\u00e9sactiv\u00e9");
          CCCDES.setComponentPopupMenu(BTD);
          CCCDES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CCCDES.setName("CCCDES");
          p_recup.add(CCCDES);
          CCCDES.setBounds(400, 180, 87, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("Note du client");
          OBJ_39.setName("OBJ_39");
          p_recup.add(OBJ_39);
          OBJ_39.setBounds(270, 149, 125, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("Points fid\u00e9lit\u00e9");
          OBJ_41.setName("OBJ_41");
          p_recup.add(OBJ_41);
          OBJ_41.setBounds(30, 211, 165, 20);

          //---- CCPLED ----
          CCPLED.setComponentPopupMenu(BTD);
          CCPLED.setName("CCPLED");
          p_recup.add(CCPLED);
          CCPLED.setBounds(270, 75, 66, CCPLED.getPreferredSize().height);

          //---- CCPLEF ----
          CCPLEF.setComponentPopupMenu(BTD);
          CCPLEF.setName("CCPLEF");
          p_recup.add(CCPLEF);
          CCPLEF.setBounds(400, 75, 66, CCPLEF.getPreferredSize().height);

          //---- CCAEXD ----
          CCAEXD.setComponentPopupMenu(BTD);
          CCAEXD.setName("CCAEXD");
          p_recup.add(CCAEXD);
          CCAEXD.setBounds(270, 110, 66, CCAEXD.getPreferredSize().height);

          //---- CCAEXF ----
          CCAEXF.setComponentPopupMenu(BTD);
          CCAEXF.setName("CCAEXF");
          p_recup.add(CCAEXF);
          CCAEXF.setBounds(400, 110, 66, CCAEXF.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setText("D\u00e9but");
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(205, 79, 39, 20);

          //---- OBJ_33 ----
          OBJ_33.setText("D\u00e9but");
          OBJ_33.setName("OBJ_33");
          p_recup.add(OBJ_33);
          OBJ_33.setBounds(205, 114, 39, 20);

          //---- CCSAN1 ----
          CCSAN1.setComponentPopupMenu(BTD);
          CCSAN1.setName("CCSAN1");
          p_recup.add(CCSAN1);
          CCSAN1.setBounds(205, 40, 50, CCSAN1.getPreferredSize().height);

          //---- CCSAN2 ----
          CCSAN2.setComponentPopupMenu(BTD);
          CCSAN2.setName("CCSAN2");
          p_recup.add(CCSAN2);
          CCSAN2.setBounds(270, 40, 50, CCSAN2.getPreferredSize().height);

          //---- CCSAN3 ----
          CCSAN3.setComponentPopupMenu(BTD);
          CCSAN3.setName("CCSAN3");
          p_recup.add(CCSAN3);
          CCSAN3.setBounds(335, 40, 50, CCSAN3.getPreferredSize().height);

          //---- CCSAN4 ----
          CCSAN4.setComponentPopupMenu(BTD);
          CCSAN4.setName("CCSAN4");
          p_recup.add(CCSAN4);
          CCSAN4.setBounds(400, 40, 50, CCSAN4.getPreferredSize().height);

          //---- CCSAN5 ----
          CCSAN5.setComponentPopupMenu(BTD);
          CCSAN5.setName("CCSAN5");
          p_recup.add(CCSAN5);
          CCSAN5.setBounds(465, 40, 50, CCSAN5.getPreferredSize().height);

          //---- CCPTSD ----
          CCPTSD.setComponentPopupMenu(BTD);
          CCPTSD.setName("CCPTSD");
          p_recup.add(CCPTSD);
          CCPTSD.setBounds(205, 207, 42, CCPTSD.getPreferredSize().height);

          //---- CCPTSF ----
          CCPTSF.setComponentPopupMenu(BTD);
          CCPTSF.setName("CCPTSF");
          p_recup.add(CCPTSF);
          CCPTSF.setBounds(400, 207, 42, CCPTSF.getPreferredSize().height);

          //---- CCNOTE ----
          CCNOTE.setComponentPopupMenu(BTD);
          CCNOTE.setName("CCNOTE");
          p_recup.add(CCNOTE);
          CCNOTE.setBounds(400, 145, 30, CCNOTE.getPreferredSize().height);

          //---- OBJ_30 ----
          OBJ_30.setText("Fin");
          OBJ_30.setName("OBJ_30");
          p_recup.add(OBJ_30);
          OBJ_30.setBounds(345, 79, 21, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("Fin");
          OBJ_35.setName("OBJ_35");
          p_recup.add(OBJ_35);
          OBJ_35.setBounds(345, 114, 21, 20);

          //---- CCATTN ----
          CCATTN.setComponentPopupMenu(BTD);
          CCATTN.setName("CCATTN");
          p_recup.add(CCATTN);
          CCATTN.setBounds(205, 145, 24, CCATTN.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setText("\u00e0");
          OBJ_43.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_43.setName("OBJ_43");
          p_recup.add(OBJ_43);
          OBJ_43.setBounds(261, 211, 125, 20);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(10, 10, 534, 260);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_ann;
  private RiSousMenu_bt riSousMenu_bt_ann;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_32;
  private JLabel OBJ_27;
  private JLabel OBJ_37;
  private JLabel OBJ_21;
  private JPanel P_PnlOpts;
  private XRiCheckBox CCCLIV;
  private XRiCheckBox CCCDES;
  private JLabel OBJ_39;
  private JLabel OBJ_41;
  private XRiTextField CCPLED;
  private XRiTextField CCPLEF;
  private XRiTextField CCAEXD;
  private XRiTextField CCAEXF;
  private JLabel OBJ_28;
  private JLabel OBJ_33;
  private XRiTextField CCSAN1;
  private XRiTextField CCSAN2;
  private XRiTextField CCSAN3;
  private XRiTextField CCSAN4;
  private XRiTextField CCSAN5;
  private XRiTextField CCPTSD;
  private XRiTextField CCPTSF;
  private XRiTextField CCNOTE;
  private JLabel OBJ_30;
  private JLabel OBJ_35;
  private XRiTextField CCATTN;
  private JLabel OBJ_43;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
