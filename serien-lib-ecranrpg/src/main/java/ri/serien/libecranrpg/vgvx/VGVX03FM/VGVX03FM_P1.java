
package ri.serien.libecranrpg.vgvx.VGVX03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class VGVX03FM_P1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX03FM_P1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("Type : @PRLIB@")).trim()));
    TE01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TE01@")).trim());
    TE02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TE02@")).trim());
    TE03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TE03@")).trim());
    TE04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TE04@")).trim());
    TE05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TE05@")).trim());
    TE06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TE06@")).trim());
    TE07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TE07@")).trim());
    TE08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TE08@")).trim());
    TE09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TE09@")).trim());
    TE10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TE10@")).trim());
    PX3PDA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PX3PDA@")).trim());
    PX3FRA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PX3FRA@")).trim());
    PX3PRE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PX3PRE@")).trim());
    LI01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LI01@")).trim());
    BA01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BA01@")).trim());
    COE01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COE01@")).trim());
    TKM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TKM1@")).trim());
    TY01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TY01@")).trim());
    PR01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR01@")).trim());
    MT01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT01@")).trim());
    LI02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LI02@")).trim());
    BA02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BA02@")).trim());
    COE2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COE01@")).trim());
    TKM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TKM2@")).trim());
    TY02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TY02@")).trim());
    MT02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT02@")).trim());
    PR02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR02@")).trim());
    LI03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LI03@")).trim());
    BA03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BA03@")).trim());
    COE3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COE01@")).trim());
    TKM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TKM3@")).trim());
    TY03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TY03@")).trim());
    MT03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT03@")).trim());
    PR03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR03@")).trim());
    LI04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LI04@")).trim());
    BA04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BA04@")).trim());
    COE4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COE01@")).trim());
    TKM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TKM4@")).trim());
    TY04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TY04@")).trim());
    MT04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT04@")).trim());
    PR04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR04@")).trim());
    LI05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LI05@")).trim());
    BA05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BA05@")).trim());
    COE5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COE01@")).trim());
    TKM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TKM5@")).trim());
    TY05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TY05@")).trim());
    MT05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT05@")).trim());
    PR05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR05@")).trim());
    LI06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LI06@")).trim());
    BA06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BA06@")).trim());
    COE6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COE01@")).trim());
    TKM6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TKM6@")).trim());
    TY06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TY06@")).trim());
    MT06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT06@")).trim());
    PR06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR06@")).trim());
    LI07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LI07@")).trim());
    BA07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BA07@")).trim());
    COE7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COE01@")).trim());
    TKM7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TKM7@")).trim());
    TY07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TY07@")).trim());
    MT07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT07@")).trim());
    PR07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR07@")).trim());
    LI08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LI08@")).trim());
    BA08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BA08@")).trim());
    COE8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COE01@")).trim());
    TKM8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TKM8@")).trim());
    TY08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TY08@")).trim());
    MT08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT08@")).trim());
    PR08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR08@")).trim());
    LI9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LI09@")).trim());
    B0A9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BA09@")).trim());
    COE9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COE01@")).trim());
    TKM9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TKM9@")).trim());
    TY09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TY09@")).trim());
    MT09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT09@")).trim());
    PR09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR09@")).trim());
    LI10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LI10@")).trim());
    BA10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BA10@")).trim());
    COE10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COE01@")).trim());
    TKM10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TKM10@")).trim());
    TY10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TY10@")).trim());
    MT10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT10@")).trim());
    PR10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR10@")).trim());
    MTTO1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTTO1@")).trim());
    PRTO1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PRTO1@")).trim());
    MTTO2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTTO2@")).trim());
    PRTO2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PRTO2@")).trim());
    MTTOT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTTOT@")).trim());
    PRTOT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PRTOT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Mode de calcul du prix de revient"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TE01 = new RiZoneSortie();
    TE02 = new RiZoneSortie();
    TE03 = new RiZoneSortie();
    TE04 = new RiZoneSortie();
    TE05 = new RiZoneSortie();
    TE06 = new RiZoneSortie();
    TE07 = new RiZoneSortie();
    TE08 = new RiZoneSortie();
    TE09 = new RiZoneSortie();
    TE10 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    PX3PDA = new RiZoneSortie();
    PX3FRA = new RiZoneSortie();
    PX3PRE = new RiZoneSortie();
    LI01 = new RiZoneSortie();
    WBAS1 = new RiZoneSortie();
    BA01 = new RiZoneSortie();
    COE01 = new RiZoneSortie();
    TKM1 = new RiZoneSortie();
    TY01 = new RiZoneSortie();
    PR01 = new RiZoneSortie();
    MT01 = new RiZoneSortie();
    LI02 = new RiZoneSortie();
    WBAS2 = new RiZoneSortie();
    BA02 = new RiZoneSortie();
    COE2 = new RiZoneSortie();
    TKM2 = new RiZoneSortie();
    TY02 = new RiZoneSortie();
    MT02 = new RiZoneSortie();
    PR02 = new RiZoneSortie();
    LI03 = new RiZoneSortie();
    WBAS3 = new RiZoneSortie();
    BA03 = new RiZoneSortie();
    COE3 = new RiZoneSortie();
    TKM3 = new RiZoneSortie();
    TY03 = new RiZoneSortie();
    MT03 = new RiZoneSortie();
    PR03 = new RiZoneSortie();
    LI04 = new RiZoneSortie();
    WBAS4 = new RiZoneSortie();
    BA04 = new RiZoneSortie();
    COE4 = new RiZoneSortie();
    TKM4 = new RiZoneSortie();
    TY04 = new RiZoneSortie();
    MT04 = new RiZoneSortie();
    PR04 = new RiZoneSortie();
    LI05 = new RiZoneSortie();
    WBAS5 = new RiZoneSortie();
    BA05 = new RiZoneSortie();
    COE5 = new RiZoneSortie();
    TKM5 = new RiZoneSortie();
    TY05 = new RiZoneSortie();
    MT05 = new RiZoneSortie();
    PR05 = new RiZoneSortie();
    LI06 = new RiZoneSortie();
    WBAS6 = new RiZoneSortie();
    BA06 = new RiZoneSortie();
    COE6 = new RiZoneSortie();
    TKM6 = new RiZoneSortie();
    TY06 = new RiZoneSortie();
    MT06 = new RiZoneSortie();
    PR06 = new RiZoneSortie();
    LI07 = new RiZoneSortie();
    WBAS7 = new RiZoneSortie();
    BA07 = new RiZoneSortie();
    COE7 = new RiZoneSortie();
    TKM7 = new RiZoneSortie();
    TY07 = new RiZoneSortie();
    MT07 = new RiZoneSortie();
    PR07 = new RiZoneSortie();
    LI08 = new RiZoneSortie();
    WBAS8 = new RiZoneSortie();
    BA08 = new RiZoneSortie();
    COE8 = new RiZoneSortie();
    TKM8 = new RiZoneSortie();
    TY08 = new RiZoneSortie();
    MT08 = new RiZoneSortie();
    PR08 = new RiZoneSortie();
    LI9 = new RiZoneSortie();
    WBAS9 = new RiZoneSortie();
    B0A9 = new RiZoneSortie();
    COE9 = new RiZoneSortie();
    TKM9 = new RiZoneSortie();
    TY09 = new RiZoneSortie();
    MT09 = new RiZoneSortie();
    PR09 = new RiZoneSortie();
    LI10 = new RiZoneSortie();
    WBAS10 = new RiZoneSortie();
    BA10 = new RiZoneSortie();
    COE10 = new RiZoneSortie();
    TKM10 = new RiZoneSortie();
    TY10 = new RiZoneSortie();
    MT10 = new RiZoneSortie();
    PR10 = new RiZoneSortie();
    MTTO1 = new RiZoneSortie();
    PRTO1 = new RiZoneSortie();
    MTTO2 = new RiZoneSortie();
    PRTO2 = new RiZoneSortie();
    MTTOT = new RiZoneSortie();
    PRTOT = new RiZoneSortie();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(900, 525));
    setPreferredSize(new Dimension(900, 525));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Type : @PRLIB@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- TE01 ----
          TE01.setText("@TE01@");
          TE01.setName("TE01");
          panel1.add(TE01);
          TE01.setBounds(15, 135, 40, TE01.getPreferredSize().height);

          //---- TE02 ----
          TE02.setText("@TE02@");
          TE02.setName("TE02");
          panel1.add(TE02);
          TE02.setBounds(15, 160, 40, TE02.getPreferredSize().height);

          //---- TE03 ----
          TE03.setText("@TE03@");
          TE03.setName("TE03");
          panel1.add(TE03);
          TE03.setBounds(15, 185, 40, TE03.getPreferredSize().height);

          //---- TE04 ----
          TE04.setText("@TE04@");
          TE04.setName("TE04");
          panel1.add(TE04);
          TE04.setBounds(15, 210, 40, TE04.getPreferredSize().height);

          //---- TE05 ----
          TE05.setText("@TE05@");
          TE05.setName("TE05");
          panel1.add(TE05);
          TE05.setBounds(15, 235, 40, TE05.getPreferredSize().height);

          //---- TE06 ----
          TE06.setText("@TE06@");
          TE06.setName("TE06");
          panel1.add(TE06);
          TE06.setBounds(15, 295, 40, TE06.getPreferredSize().height);

          //---- TE07 ----
          TE07.setText("@TE07@");
          TE07.setName("TE07");
          panel1.add(TE07);
          TE07.setBounds(15, 320, 40, TE07.getPreferredSize().height);

          //---- TE08 ----
          TE08.setText("@TE08@");
          TE08.setName("TE08");
          panel1.add(TE08);
          TE08.setBounds(15, 345, 40, TE08.getPreferredSize().height);

          //---- TE09 ----
          TE09.setText("@TE09@");
          TE09.setName("TE09");
          panel1.add(TE09);
          TE09.setBounds(15, 370, 40, TE09.getPreferredSize().height);

          //---- TE10 ----
          TE10.setText("@TE10@");
          TE10.setName("TE10");
          panel1.add(TE10);
          TE10.setBounds(15, 395, 40, TE10.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Prix d'achat");
          label1.setHorizontalAlignment(SwingConstants.RIGHT);
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(440, 25, 125, 30);

          //---- label2 ----
          label2.setText("Frais imput\u00e9s");
          label2.setHorizontalAlignment(SwingConstants.RIGHT);
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(440, 53, 125, 30);

          //---- label3 ----
          label3.setText("Prix d'entr\u00e9e");
          label3.setHorizontalAlignment(SwingConstants.RIGHT);
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(440, 81, 125, 30);

          //---- PX3PDA ----
          PX3PDA.setText("@PX3PDA@");
          PX3PDA.setHorizontalAlignment(SwingConstants.RIGHT);
          PX3PDA.setName("PX3PDA");
          panel1.add(PX3PDA);
          PX3PDA.setBounds(575, 28, 120, PX3PDA.getPreferredSize().height);

          //---- PX3FRA ----
          PX3FRA.setText("@PX3FRA@");
          PX3FRA.setHorizontalAlignment(SwingConstants.RIGHT);
          PX3FRA.setName("PX3FRA");
          panel1.add(PX3FRA);
          PX3FRA.setBounds(575, 56, 120, PX3FRA.getPreferredSize().height);

          //---- PX3PRE ----
          PX3PRE.setText("@PX3PRE@");
          PX3PRE.setHorizontalAlignment(SwingConstants.RIGHT);
          PX3PRE.setName("PX3PRE");
          panel1.add(PX3PRE);
          PX3PRE.setBounds(575, 84, 120, PX3PRE.getPreferredSize().height);

          //---- LI01 ----
          LI01.setText("@LI01@");
          LI01.setName("LI01");
          panel1.add(LI01);
          LI01.setBounds(60, 135, 110, LI01.getPreferredSize().height);

          //---- WBAS1 ----
          WBAS1.setText("@WBAS1");
          WBAS1.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS1.setName("WBAS1");
          panel1.add(WBAS1);
          WBAS1.setBounds(170, 135, 110, WBAS1.getPreferredSize().height);

          //---- BA01 ----
          BA01.setText("@BA01@");
          BA01.setName("BA01");
          panel1.add(BA01);
          BA01.setBounds(285, 135, 25, BA01.getPreferredSize().height);

          //---- COE01 ----
          COE01.setText("@COE01@");
          COE01.setHorizontalAlignment(SwingConstants.RIGHT);
          COE01.setName("COE01");
          panel1.add(COE01);
          COE01.setBounds(310, 135, 70, COE01.getPreferredSize().height);

          //---- TKM1 ----
          TKM1.setText("@TKM1@");
          TKM1.setName("TKM1");
          panel1.add(TKM1);
          TKM1.setBounds(385, 135, 25, TKM1.getPreferredSize().height);

          //---- TY01 ----
          TY01.setText("@TY01@");
          TY01.setName("TY01");
          panel1.add(TY01);
          TY01.setBounds(415, 135, 25, TY01.getPreferredSize().height);

          //---- PR01 ----
          PR01.setText("@PR01@");
          PR01.setHorizontalAlignment(SwingConstants.RIGHT);
          PR01.setName("PR01");
          panel1.add(PR01);
          PR01.setBounds(585, 135, 110, PR01.getPreferredSize().height);

          //---- MT01 ----
          MT01.setText("@MT01@");
          MT01.setHorizontalAlignment(SwingConstants.RIGHT);
          MT01.setName("MT01");
          panel1.add(MT01);
          MT01.setBounds(440, 135, 140, MT01.getPreferredSize().height);

          //---- LI02 ----
          LI02.setText("@LI02@");
          LI02.setName("LI02");
          panel1.add(LI02);
          LI02.setBounds(60, 160, 110, LI02.getPreferredSize().height);

          //---- WBAS2 ----
          WBAS2.setText("@WBAS2");
          WBAS2.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS2.setName("WBAS2");
          panel1.add(WBAS2);
          WBAS2.setBounds(170, 160, 110, WBAS2.getPreferredSize().height);

          //---- BA02 ----
          BA02.setText("@BA02@");
          BA02.setName("BA02");
          panel1.add(BA02);
          BA02.setBounds(285, 160, 25, BA02.getPreferredSize().height);

          //---- COE2 ----
          COE2.setText("@COE01@");
          COE2.setHorizontalAlignment(SwingConstants.RIGHT);
          COE2.setName("COE2");
          panel1.add(COE2);
          COE2.setBounds(310, 160, 70, COE2.getPreferredSize().height);

          //---- TKM2 ----
          TKM2.setText("@TKM2@");
          TKM2.setName("TKM2");
          panel1.add(TKM2);
          TKM2.setBounds(385, 160, 25, TKM2.getPreferredSize().height);

          //---- TY02 ----
          TY02.setText("@TY02@");
          TY02.setName("TY02");
          panel1.add(TY02);
          TY02.setBounds(415, 160, 25, TY02.getPreferredSize().height);

          //---- MT02 ----
          MT02.setText("@MT02@");
          MT02.setHorizontalAlignment(SwingConstants.RIGHT);
          MT02.setName("MT02");
          panel1.add(MT02);
          MT02.setBounds(440, 160, 140, MT02.getPreferredSize().height);

          //---- PR02 ----
          PR02.setText("@PR02@");
          PR02.setHorizontalAlignment(SwingConstants.RIGHT);
          PR02.setName("PR02");
          panel1.add(PR02);
          PR02.setBounds(585, 160, 110, PR02.getPreferredSize().height);

          //---- LI03 ----
          LI03.setText("@LI03@");
          LI03.setName("LI03");
          panel1.add(LI03);
          LI03.setBounds(60, 185, 110, 24);

          //---- WBAS3 ----
          WBAS3.setText("@WBAS3");
          WBAS3.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS3.setName("WBAS3");
          panel1.add(WBAS3);
          WBAS3.setBounds(170, 185, 110, 24);

          //---- BA03 ----
          BA03.setText("@BA03@");
          BA03.setName("BA03");
          panel1.add(BA03);
          BA03.setBounds(285, 185, 25, 24);

          //---- COE3 ----
          COE3.setText("@COE01@");
          COE3.setHorizontalAlignment(SwingConstants.RIGHT);
          COE3.setName("COE3");
          panel1.add(COE3);
          COE3.setBounds(310, 185, 70, 24);

          //---- TKM3 ----
          TKM3.setText("@TKM3@");
          TKM3.setName("TKM3");
          panel1.add(TKM3);
          TKM3.setBounds(385, 185, 25, 24);

          //---- TY03 ----
          TY03.setText("@TY03@");
          TY03.setName("TY03");
          panel1.add(TY03);
          TY03.setBounds(415, 185, 25, 24);

          //---- MT03 ----
          MT03.setText("@MT03@");
          MT03.setHorizontalAlignment(SwingConstants.RIGHT);
          MT03.setName("MT03");
          panel1.add(MT03);
          MT03.setBounds(440, 185, 140, 24);

          //---- PR03 ----
          PR03.setText("@PR03@");
          PR03.setHorizontalAlignment(SwingConstants.RIGHT);
          PR03.setName("PR03");
          panel1.add(PR03);
          PR03.setBounds(585, 185, 110, 24);

          //---- LI04 ----
          LI04.setText("@LI04@");
          LI04.setName("LI04");
          panel1.add(LI04);
          LI04.setBounds(60, 210, 110, LI04.getPreferredSize().height);

          //---- WBAS4 ----
          WBAS4.setText("@WBAS4");
          WBAS4.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS4.setName("WBAS4");
          panel1.add(WBAS4);
          WBAS4.setBounds(170, 210, 110, WBAS4.getPreferredSize().height);

          //---- BA04 ----
          BA04.setText("@BA04@");
          BA04.setName("BA04");
          panel1.add(BA04);
          BA04.setBounds(285, 210, 25, BA04.getPreferredSize().height);

          //---- COE4 ----
          COE4.setText("@COE01@");
          COE4.setHorizontalAlignment(SwingConstants.RIGHT);
          COE4.setName("COE4");
          panel1.add(COE4);
          COE4.setBounds(310, 210, 70, COE4.getPreferredSize().height);

          //---- TKM4 ----
          TKM4.setText("@TKM4@");
          TKM4.setName("TKM4");
          panel1.add(TKM4);
          TKM4.setBounds(385, 210, 25, TKM4.getPreferredSize().height);

          //---- TY04 ----
          TY04.setText("@TY04@");
          TY04.setName("TY04");
          panel1.add(TY04);
          TY04.setBounds(415, 210, 25, TY04.getPreferredSize().height);

          //---- MT04 ----
          MT04.setText("@MT04@");
          MT04.setHorizontalAlignment(SwingConstants.RIGHT);
          MT04.setName("MT04");
          panel1.add(MT04);
          MT04.setBounds(440, 210, 140, MT04.getPreferredSize().height);

          //---- PR04 ----
          PR04.setText("@PR04@");
          PR04.setHorizontalAlignment(SwingConstants.RIGHT);
          PR04.setName("PR04");
          panel1.add(PR04);
          PR04.setBounds(585, 210, 110, PR04.getPreferredSize().height);

          //---- LI05 ----
          LI05.setText("@LI05@");
          LI05.setName("LI05");
          panel1.add(LI05);
          LI05.setBounds(60, 235, 110, LI05.getPreferredSize().height);

          //---- WBAS5 ----
          WBAS5.setText("@WBAS5");
          WBAS5.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS5.setName("WBAS5");
          panel1.add(WBAS5);
          WBAS5.setBounds(170, 235, 110, WBAS5.getPreferredSize().height);

          //---- BA05 ----
          BA05.setText("@BA05@");
          BA05.setName("BA05");
          panel1.add(BA05);
          BA05.setBounds(285, 235, 25, BA05.getPreferredSize().height);

          //---- COE5 ----
          COE5.setText("@COE01@");
          COE5.setHorizontalAlignment(SwingConstants.RIGHT);
          COE5.setName("COE5");
          panel1.add(COE5);
          COE5.setBounds(310, 235, 70, COE5.getPreferredSize().height);

          //---- TKM5 ----
          TKM5.setText("@TKM5@");
          TKM5.setName("TKM5");
          panel1.add(TKM5);
          TKM5.setBounds(385, 235, 25, TKM5.getPreferredSize().height);

          //---- TY05 ----
          TY05.setText("@TY05@");
          TY05.setName("TY05");
          panel1.add(TY05);
          TY05.setBounds(415, 235, 25, TY05.getPreferredSize().height);

          //---- MT05 ----
          MT05.setText("@MT05@");
          MT05.setHorizontalAlignment(SwingConstants.RIGHT);
          MT05.setName("MT05");
          panel1.add(MT05);
          MT05.setBounds(440, 235, 140, MT05.getPreferredSize().height);

          //---- PR05 ----
          PR05.setText("@PR05@");
          PR05.setHorizontalAlignment(SwingConstants.RIGHT);
          PR05.setName("PR05");
          panel1.add(PR05);
          PR05.setBounds(585, 235, 110, PR05.getPreferredSize().height);

          //---- LI06 ----
          LI06.setText("@LI06@");
          LI06.setName("LI06");
          panel1.add(LI06);
          LI06.setBounds(60, 295, 110, LI06.getPreferredSize().height);

          //---- WBAS6 ----
          WBAS6.setText("@WBAS6");
          WBAS6.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS6.setName("WBAS6");
          panel1.add(WBAS6);
          WBAS6.setBounds(170, 295, 110, WBAS6.getPreferredSize().height);

          //---- BA06 ----
          BA06.setText("@BA06@");
          BA06.setName("BA06");
          panel1.add(BA06);
          BA06.setBounds(285, 295, 25, BA06.getPreferredSize().height);

          //---- COE6 ----
          COE6.setText("@COE01@");
          COE6.setHorizontalAlignment(SwingConstants.RIGHT);
          COE6.setName("COE6");
          panel1.add(COE6);
          COE6.setBounds(310, 295, 70, COE6.getPreferredSize().height);

          //---- TKM6 ----
          TKM6.setText("@TKM6@");
          TKM6.setName("TKM6");
          panel1.add(TKM6);
          TKM6.setBounds(385, 295, 25, TKM6.getPreferredSize().height);

          //---- TY06 ----
          TY06.setText("@TY06@");
          TY06.setName("TY06");
          panel1.add(TY06);
          TY06.setBounds(415, 295, 25, TY06.getPreferredSize().height);

          //---- MT06 ----
          MT06.setText("@MT06@");
          MT06.setHorizontalAlignment(SwingConstants.RIGHT);
          MT06.setName("MT06");
          panel1.add(MT06);
          MT06.setBounds(440, 295, 140, MT06.getPreferredSize().height);

          //---- PR06 ----
          PR06.setText("@PR06@");
          PR06.setHorizontalAlignment(SwingConstants.RIGHT);
          PR06.setName("PR06");
          panel1.add(PR06);
          PR06.setBounds(585, 295, 110, PR06.getPreferredSize().height);

          //---- LI07 ----
          LI07.setText("@LI07@");
          LI07.setName("LI07");
          panel1.add(LI07);
          LI07.setBounds(60, 320, 110, LI07.getPreferredSize().height);

          //---- WBAS7 ----
          WBAS7.setText("@WBAS7");
          WBAS7.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS7.setName("WBAS7");
          panel1.add(WBAS7);
          WBAS7.setBounds(170, 320, 110, WBAS7.getPreferredSize().height);

          //---- BA07 ----
          BA07.setText("@BA07@");
          BA07.setName("BA07");
          panel1.add(BA07);
          BA07.setBounds(285, 320, 25, BA07.getPreferredSize().height);

          //---- COE7 ----
          COE7.setText("@COE01@");
          COE7.setHorizontalAlignment(SwingConstants.RIGHT);
          COE7.setName("COE7");
          panel1.add(COE7);
          COE7.setBounds(310, 320, 70, COE7.getPreferredSize().height);

          //---- TKM7 ----
          TKM7.setText("@TKM7@");
          TKM7.setName("TKM7");
          panel1.add(TKM7);
          TKM7.setBounds(385, 320, 25, TKM7.getPreferredSize().height);

          //---- TY07 ----
          TY07.setText("@TY07@");
          TY07.setName("TY07");
          panel1.add(TY07);
          TY07.setBounds(415, 320, 25, TY07.getPreferredSize().height);

          //---- MT07 ----
          MT07.setText("@MT07@");
          MT07.setHorizontalAlignment(SwingConstants.RIGHT);
          MT07.setName("MT07");
          panel1.add(MT07);
          MT07.setBounds(440, 320, 140, MT07.getPreferredSize().height);

          //---- PR07 ----
          PR07.setText("@PR07@");
          PR07.setHorizontalAlignment(SwingConstants.RIGHT);
          PR07.setName("PR07");
          panel1.add(PR07);
          PR07.setBounds(585, 320, 110, PR07.getPreferredSize().height);

          //---- LI08 ----
          LI08.setText("@LI08@");
          LI08.setName("LI08");
          panel1.add(LI08);
          LI08.setBounds(60, 345, 110, LI08.getPreferredSize().height);

          //---- WBAS8 ----
          WBAS8.setText("@WBAS8");
          WBAS8.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS8.setName("WBAS8");
          panel1.add(WBAS8);
          WBAS8.setBounds(170, 345, 110, WBAS8.getPreferredSize().height);

          //---- BA08 ----
          BA08.setText("@BA08@");
          BA08.setName("BA08");
          panel1.add(BA08);
          BA08.setBounds(285, 345, 25, BA08.getPreferredSize().height);

          //---- COE8 ----
          COE8.setText("@COE01@");
          COE8.setHorizontalAlignment(SwingConstants.RIGHT);
          COE8.setName("COE8");
          panel1.add(COE8);
          COE8.setBounds(310, 345, 70, COE8.getPreferredSize().height);

          //---- TKM8 ----
          TKM8.setText("@TKM8@");
          TKM8.setName("TKM8");
          panel1.add(TKM8);
          TKM8.setBounds(385, 345, 25, TKM8.getPreferredSize().height);

          //---- TY08 ----
          TY08.setText("@TY08@");
          TY08.setName("TY08");
          panel1.add(TY08);
          TY08.setBounds(415, 345, 25, TY08.getPreferredSize().height);

          //---- MT08 ----
          MT08.setText("@MT08@");
          MT08.setHorizontalAlignment(SwingConstants.RIGHT);
          MT08.setName("MT08");
          panel1.add(MT08);
          MT08.setBounds(440, 345, 140, MT08.getPreferredSize().height);

          //---- PR08 ----
          PR08.setText("@PR08@");
          PR08.setHorizontalAlignment(SwingConstants.RIGHT);
          PR08.setName("PR08");
          panel1.add(PR08);
          PR08.setBounds(585, 345, 110, PR08.getPreferredSize().height);

          //---- LI9 ----
          LI9.setText("@LI09@");
          LI9.setName("LI9");
          panel1.add(LI9);
          LI9.setBounds(60, 370, 110, LI9.getPreferredSize().height);

          //---- WBAS9 ----
          WBAS9.setText("@WBAS9");
          WBAS9.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS9.setName("WBAS9");
          panel1.add(WBAS9);
          WBAS9.setBounds(170, 370, 110, WBAS9.getPreferredSize().height);

          //---- B0A9 ----
          B0A9.setText("@BA09@");
          B0A9.setName("B0A9");
          panel1.add(B0A9);
          B0A9.setBounds(285, 370, 25, B0A9.getPreferredSize().height);

          //---- COE9 ----
          COE9.setText("@COE01@");
          COE9.setHorizontalAlignment(SwingConstants.RIGHT);
          COE9.setName("COE9");
          panel1.add(COE9);
          COE9.setBounds(310, 370, 70, COE9.getPreferredSize().height);

          //---- TKM9 ----
          TKM9.setText("@TKM9@");
          TKM9.setName("TKM9");
          panel1.add(TKM9);
          TKM9.setBounds(385, 370, 25, TKM9.getPreferredSize().height);

          //---- TY09 ----
          TY09.setText("@TY09@");
          TY09.setName("TY09");
          panel1.add(TY09);
          TY09.setBounds(415, 370, 25, TY09.getPreferredSize().height);

          //---- MT09 ----
          MT09.setText("@MT09@");
          MT09.setHorizontalAlignment(SwingConstants.RIGHT);
          MT09.setName("MT09");
          panel1.add(MT09);
          MT09.setBounds(440, 370, 140, MT09.getPreferredSize().height);

          //---- PR09 ----
          PR09.setText("@PR09@");
          PR09.setHorizontalAlignment(SwingConstants.RIGHT);
          PR09.setName("PR09");
          panel1.add(PR09);
          PR09.setBounds(585, 370, 110, PR09.getPreferredSize().height);

          //---- LI10 ----
          LI10.setText("@LI10@");
          LI10.setName("LI10");
          panel1.add(LI10);
          LI10.setBounds(60, 395, 110, LI10.getPreferredSize().height);

          //---- WBAS10 ----
          WBAS10.setText("@WBAS10");
          WBAS10.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS10.setName("WBAS10");
          panel1.add(WBAS10);
          WBAS10.setBounds(170, 395, 110, WBAS10.getPreferredSize().height);

          //---- BA10 ----
          BA10.setText("@BA10@");
          BA10.setName("BA10");
          panel1.add(BA10);
          BA10.setBounds(285, 395, 25, BA10.getPreferredSize().height);

          //---- COE10 ----
          COE10.setText("@COE01@");
          COE10.setHorizontalAlignment(SwingConstants.RIGHT);
          COE10.setName("COE10");
          panel1.add(COE10);
          COE10.setBounds(310, 395, 70, COE10.getPreferredSize().height);

          //---- TKM10 ----
          TKM10.setText("@TKM10@");
          TKM10.setName("TKM10");
          panel1.add(TKM10);
          TKM10.setBounds(385, 395, 25, TKM10.getPreferredSize().height);

          //---- TY10 ----
          TY10.setText("@TY10@");
          TY10.setName("TY10");
          panel1.add(TY10);
          TY10.setBounds(415, 395, 25, TY10.getPreferredSize().height);

          //---- MT10 ----
          MT10.setText("@MT10@");
          MT10.setHorizontalAlignment(SwingConstants.RIGHT);
          MT10.setName("MT10");
          panel1.add(MT10);
          MT10.setBounds(440, 395, 140, MT10.getPreferredSize().height);

          //---- PR10 ----
          PR10.setText("@PR10@");
          PR10.setHorizontalAlignment(SwingConstants.RIGHT);
          PR10.setName("PR10");
          panel1.add(PR10);
          PR10.setBounds(585, 395, 110, PR10.getPreferredSize().height);

          //---- MTTO1 ----
          MTTO1.setText("@MTTO1@");
          MTTO1.setHorizontalAlignment(SwingConstants.RIGHT);
          MTTO1.setName("MTTO1");
          panel1.add(MTTO1);
          MTTO1.setBounds(440, 265, 140, MTTO1.getPreferredSize().height);

          //---- PRTO1 ----
          PRTO1.setText("@PRTO1@");
          PRTO1.setHorizontalAlignment(SwingConstants.RIGHT);
          PRTO1.setName("PRTO1");
          panel1.add(PRTO1);
          PRTO1.setBounds(585, 265, 110, PRTO1.getPreferredSize().height);

          //---- MTTO2 ----
          MTTO2.setText("@MTTO2@");
          MTTO2.setHorizontalAlignment(SwingConstants.RIGHT);
          MTTO2.setName("MTTO2");
          panel1.add(MTTO2);
          MTTO2.setBounds(440, 425, 140, MTTO2.getPreferredSize().height);

          //---- PRTO2 ----
          PRTO2.setText("@PRTO2@");
          PRTO2.setHorizontalAlignment(SwingConstants.RIGHT);
          PRTO2.setName("PRTO2");
          panel1.add(PRTO2);
          PRTO2.setBounds(585, 425, 110, PRTO2.getPreferredSize().height);

          //---- MTTOT ----
          MTTOT.setText("@MTTOT@");
          MTTOT.setHorizontalAlignment(SwingConstants.RIGHT);
          MTTOT.setName("MTTOT");
          panel1.add(MTTOT);
          MTTOT.setBounds(440, 460, 140, MTTOT.getPreferredSize().height);

          //---- PRTOT ----
          PRTOT.setText("@PRTOT@");
          PRTOT.setHorizontalAlignment(SwingConstants.RIGHT);
          PRTOT.setName("PRTOT");
          panel1.add(PRTOT);
          PRTOT.setBounds(585, 460, 110, PRTOT.getPreferredSize().height);

          //---- label4 ----
          label4.setText("Libell\u00e9");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(60, 115, 110, label4.getPreferredSize().height);

          //---- label5 ----
          label5.setText("Base");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(170, 115, 110, label5.getPreferredSize().height);

          //---- label6 ----
          label6.setText("B");
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setHorizontalAlignment(SwingConstants.CENTER);
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(285, 115, 25, label6.getPreferredSize().height);

          //---- label7 ----
          label7.setText("Coefficient/valeur");
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setHorizontalAlignment(SwingConstants.CENTER);
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(310, 115, 100, label7.getPreferredSize().height);

          //---- label8 ----
          label8.setText("U");
          label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
          label8.setHorizontalAlignment(SwingConstants.CENTER);
          label8.setName("label8");
          panel1.add(label8);
          label8.setBounds(415, 115, 25, label8.getPreferredSize().height);

          //---- label9 ----
          label9.setText("Valeur");
          label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
          label9.setHorizontalAlignment(SwingConstants.CENTER);
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(440, 115, 140, label9.getPreferredSize().height);

          //---- label10 ----
          label10.setText("Prix de revient");
          label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
          label10.setHorizontalAlignment(SwingConstants.CENTER);
          label10.setName("label10");
          panel1.add(label10);
          label10.setBounds(585, 115, 110, label10.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 710, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 503, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie TE01;
  private RiZoneSortie TE02;
  private RiZoneSortie TE03;
  private RiZoneSortie TE04;
  private RiZoneSortie TE05;
  private RiZoneSortie TE06;
  private RiZoneSortie TE07;
  private RiZoneSortie TE08;
  private RiZoneSortie TE09;
  private RiZoneSortie TE10;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private RiZoneSortie PX3PDA;
  private RiZoneSortie PX3FRA;
  private RiZoneSortie PX3PRE;
  private RiZoneSortie LI01;
  private RiZoneSortie WBAS1;
  private RiZoneSortie BA01;
  private RiZoneSortie COE01;
  private RiZoneSortie TKM1;
  private RiZoneSortie TY01;
  private RiZoneSortie PR01;
  private RiZoneSortie MT01;
  private RiZoneSortie LI02;
  private RiZoneSortie WBAS2;
  private RiZoneSortie BA02;
  private RiZoneSortie COE2;
  private RiZoneSortie TKM2;
  private RiZoneSortie TY02;
  private RiZoneSortie MT02;
  private RiZoneSortie PR02;
  private RiZoneSortie LI03;
  private RiZoneSortie WBAS3;
  private RiZoneSortie BA03;
  private RiZoneSortie COE3;
  private RiZoneSortie TKM3;
  private RiZoneSortie TY03;
  private RiZoneSortie MT03;
  private RiZoneSortie PR03;
  private RiZoneSortie LI04;
  private RiZoneSortie WBAS4;
  private RiZoneSortie BA04;
  private RiZoneSortie COE4;
  private RiZoneSortie TKM4;
  private RiZoneSortie TY04;
  private RiZoneSortie MT04;
  private RiZoneSortie PR04;
  private RiZoneSortie LI05;
  private RiZoneSortie WBAS5;
  private RiZoneSortie BA05;
  private RiZoneSortie COE5;
  private RiZoneSortie TKM5;
  private RiZoneSortie TY05;
  private RiZoneSortie MT05;
  private RiZoneSortie PR05;
  private RiZoneSortie LI06;
  private RiZoneSortie WBAS6;
  private RiZoneSortie BA06;
  private RiZoneSortie COE6;
  private RiZoneSortie TKM6;
  private RiZoneSortie TY06;
  private RiZoneSortie MT06;
  private RiZoneSortie PR06;
  private RiZoneSortie LI07;
  private RiZoneSortie WBAS7;
  private RiZoneSortie BA07;
  private RiZoneSortie COE7;
  private RiZoneSortie TKM7;
  private RiZoneSortie TY07;
  private RiZoneSortie MT07;
  private RiZoneSortie PR07;
  private RiZoneSortie LI08;
  private RiZoneSortie WBAS8;
  private RiZoneSortie BA08;
  private RiZoneSortie COE8;
  private RiZoneSortie TKM8;
  private RiZoneSortie TY08;
  private RiZoneSortie MT08;
  private RiZoneSortie PR08;
  private RiZoneSortie LI9;
  private RiZoneSortie WBAS9;
  private RiZoneSortie B0A9;
  private RiZoneSortie COE9;
  private RiZoneSortie TKM9;
  private RiZoneSortie TY09;
  private RiZoneSortie MT09;
  private RiZoneSortie PR09;
  private RiZoneSortie LI10;
  private RiZoneSortie WBAS10;
  private RiZoneSortie BA10;
  private RiZoneSortie COE10;
  private RiZoneSortie TKM10;
  private RiZoneSortie TY10;
  private RiZoneSortie MT10;
  private RiZoneSortie PR10;
  private RiZoneSortie MTTO1;
  private RiZoneSortie PRTO1;
  private RiZoneSortie MTTO2;
  private RiZoneSortie PRTO2;
  private RiZoneSortie MTTOT;
  private RiZoneSortie PRTOT;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
