
package ri.serien.libecranrpg.vgvx.VGVXCCFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVXCCFM_C2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXCCFM_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    CCMAG5.setEnabled(lexique.isPresent("CCMAG5"));
    CCMAG4.setEnabled(lexique.isPresent("CCMAG4"));
    CCMAG3.setEnabled(lexique.isPresent("CCMAG3"));
    CCMAG2.setEnabled(lexique.isPresent("CCMAG2"));
    CCMAG1.setEnabled(lexique.isPresent("CCMAG1"));
    CCMTR5.setEnabled(lexique.isPresent("CCMTR5"));
    CCMTR4.setEnabled(lexique.isPresent("CCMTR4"));
    CCMTR3.setEnabled(lexique.isPresent("CCMTR3"));
    CCMTR2.setEnabled(lexique.isPresent("CCMTR2"));
    CCMTR1.setEnabled(lexique.isPresent("CCMTR1"));
    CCMEX5.setEnabled(lexique.isPresent("CCMEX5"));
    CCMEX4.setEnabled(lexique.isPresent("CCMEX4"));
    CCMEX3.setEnabled(lexique.isPresent("CCMEX3"));
    CCMEX2.setEnabled(lexique.isPresent("CCMEX2"));
    CCMEX1.setEnabled(lexique.isPresent("CCMEX1"));
    CCZGE5.setEnabled(lexique.isPresent("CCZGE5"));
    CCZGE4.setEnabled(lexique.isPresent("CCZGE4"));
    CCZGE3.setEnabled(lexique.isPresent("CCZGE3"));
    CCZGE2.setEnabled(lexique.isPresent("CCZGE2"));
    CCZGE1.setEnabled(lexique.isPresent("CCZGE1"));
    CCMFRF.setEnabled(lexique.isPresent("CCMFRF"));
    CCMFRD.setEnabled(lexique.isPresent("CCMFRD"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Critères de sélection"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_ann = new RiSousMenu();
    riSousMenu_bt_ann = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_34 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_28 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_40 = new JLabel();
    CCMFRD = new XRiTextField();
    CCMFRF = new XRiTextField();
    OBJ_47 = new JLabel();
    CCZGE1 = new XRiTextField();
    CCZGE2 = new XRiTextField();
    CCZGE3 = new XRiTextField();
    CCZGE4 = new XRiTextField();
    CCZGE5 = new XRiTextField();
    CCMEX1 = new XRiTextField();
    CCMEX2 = new XRiTextField();
    CCMEX3 = new XRiTextField();
    CCMEX4 = new XRiTextField();
    CCMEX5 = new XRiTextField();
    CCMTR1 = new XRiTextField();
    CCMTR2 = new XRiTextField();
    CCMTR3 = new XRiTextField();
    CCMTR4 = new XRiTextField();
    CCMTR5 = new XRiTextField();
    CCMAG1 = new XRiTextField();
    CCMAG2 = new XRiTextField();
    CCMAG3 = new XRiTextField();
    CCMAG4 = new XRiTextField();
    CCMAG5 = new XRiTextField();
    OBJ_49 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(705, 250));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_ann ========
          {
            riSousMenu_ann.setName("riSousMenu_ann");

            //---- riSousMenu_bt_ann ----
            riSousMenu_bt_ann.setText("Annulation");
            riSousMenu_bt_ann.setToolTipText("Annulation");
            riSousMenu_bt_ann.setName("riSousMenu_bt_ann");
            riSousMenu_ann.add(riSousMenu_bt_ann);
          }
          menus_haut.add(riSousMenu_ann);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setToolTipText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Livraison"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_34 ----
          OBJ_34.setText("Transporteur habituel");
          OBJ_34.setName("OBJ_34");
          p_recup.add(OBJ_34);
          OBJ_34.setBounds(25, 104, 155, 20);

          //---- OBJ_22 ----
          OBJ_22.setText("Zone  g\u00e9ographique");
          OBJ_22.setName("OBJ_22");
          p_recup.add(OBJ_22);
          OBJ_22.setBounds(25, 40, 155, 20);

          //---- OBJ_46 ----
          OBJ_46.setText("Minimum pour franco");
          OBJ_46.setName("OBJ_46");
          p_recup.add(OBJ_46);
          OBJ_46.setBounds(25, 168, 155, 20);

          //---- OBJ_28 ----
          OBJ_28.setText("Mode  d'exp\u00e9dition");
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(25, 72, 155, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_40 ----
          OBJ_40.setText("Magasin");
          OBJ_40.setName("OBJ_40");
          p_recup.add(OBJ_40);
          OBJ_40.setBounds(25, 136, 155, 20);

          //---- CCMFRD ----
          CCMFRD.setComponentPopupMenu(BTD);
          CCMFRD.setName("CCMFRD");
          p_recup.add(CCMFRD);
          CCMFRD.setBounds(246, 164, 66, CCMFRD.getPreferredSize().height);

          //---- CCMFRF ----
          CCMFRF.setComponentPopupMenu(BTD);
          CCMFRF.setName("CCMFRF");
          p_recup.add(CCMFRF);
          CCMFRF.setBounds(429, 164, 66, CCMFRF.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setText("D\u00e9but");
          OBJ_47.setName("OBJ_47");
          p_recup.add(OBJ_47);
          OBJ_47.setBounds(185, 168, 60, 20);

          //---- CCZGE1 ----
          CCZGE1.setComponentPopupMenu(BTD);
          CCZGE1.setName("CCZGE1");
          p_recup.add(CCZGE1);
          CCZGE1.setBounds(185, 36, 60, CCZGE1.getPreferredSize().height);

          //---- CCZGE2 ----
          CCZGE2.setComponentPopupMenu(BTD);
          CCZGE2.setName("CCZGE2");
          p_recup.add(CCZGE2);
          CCZGE2.setBounds(246, 36, 60, CCZGE2.getPreferredSize().height);

          //---- CCZGE3 ----
          CCZGE3.setComponentPopupMenu(BTD);
          CCZGE3.setName("CCZGE3");
          p_recup.add(CCZGE3);
          CCZGE3.setBounds(307, 36, 60, CCZGE3.getPreferredSize().height);

          //---- CCZGE4 ----
          CCZGE4.setComponentPopupMenu(BTD);
          CCZGE4.setName("CCZGE4");
          p_recup.add(CCZGE4);
          CCZGE4.setBounds(368, 36, 60, CCZGE4.getPreferredSize().height);

          //---- CCZGE5 ----
          CCZGE5.setComponentPopupMenu(BTD);
          CCZGE5.setName("CCZGE5");
          p_recup.add(CCZGE5);
          CCZGE5.setBounds(429, 36, 60, CCZGE5.getPreferredSize().height);

          //---- CCMEX1 ----
          CCMEX1.setComponentPopupMenu(BTD);
          CCMEX1.setName("CCMEX1");
          p_recup.add(CCMEX1);
          CCMEX1.setBounds(185, 68, 34, CCMEX1.getPreferredSize().height);

          //---- CCMEX2 ----
          CCMEX2.setComponentPopupMenu(BTD);
          CCMEX2.setName("CCMEX2");
          p_recup.add(CCMEX2);
          CCMEX2.setBounds(246, 68, 34, CCMEX2.getPreferredSize().height);

          //---- CCMEX3 ----
          CCMEX3.setComponentPopupMenu(BTD);
          CCMEX3.setName("CCMEX3");
          p_recup.add(CCMEX3);
          CCMEX3.setBounds(307, 68, 34, CCMEX3.getPreferredSize().height);

          //---- CCMEX4 ----
          CCMEX4.setComponentPopupMenu(BTD);
          CCMEX4.setName("CCMEX4");
          p_recup.add(CCMEX4);
          CCMEX4.setBounds(368, 68, 34, CCMEX4.getPreferredSize().height);

          //---- CCMEX5 ----
          CCMEX5.setComponentPopupMenu(BTD);
          CCMEX5.setName("CCMEX5");
          p_recup.add(CCMEX5);
          CCMEX5.setBounds(429, 68, 34, CCMEX5.getPreferredSize().height);

          //---- CCMTR1 ----
          CCMTR1.setComponentPopupMenu(BTD);
          CCMTR1.setName("CCMTR1");
          p_recup.add(CCMTR1);
          CCMTR1.setBounds(185, 100, 34, CCMTR1.getPreferredSize().height);

          //---- CCMTR2 ----
          CCMTR2.setComponentPopupMenu(BTD);
          CCMTR2.setName("CCMTR2");
          p_recup.add(CCMTR2);
          CCMTR2.setBounds(246, 100, 34, CCMTR2.getPreferredSize().height);

          //---- CCMTR3 ----
          CCMTR3.setComponentPopupMenu(BTD);
          CCMTR3.setName("CCMTR3");
          p_recup.add(CCMTR3);
          CCMTR3.setBounds(307, 100, 34, CCMTR3.getPreferredSize().height);

          //---- CCMTR4 ----
          CCMTR4.setComponentPopupMenu(BTD);
          CCMTR4.setName("CCMTR4");
          p_recup.add(CCMTR4);
          CCMTR4.setBounds(368, 100, 34, CCMTR4.getPreferredSize().height);

          //---- CCMTR5 ----
          CCMTR5.setComponentPopupMenu(BTD);
          CCMTR5.setName("CCMTR5");
          p_recup.add(CCMTR5);
          CCMTR5.setBounds(429, 100, 34, CCMTR5.getPreferredSize().height);

          //---- CCMAG1 ----
          CCMAG1.setComponentPopupMenu(BTD);
          CCMAG1.setName("CCMAG1");
          p_recup.add(CCMAG1);
          CCMAG1.setBounds(185, 132, 34, CCMAG1.getPreferredSize().height);

          //---- CCMAG2 ----
          CCMAG2.setComponentPopupMenu(BTD);
          CCMAG2.setName("CCMAG2");
          p_recup.add(CCMAG2);
          CCMAG2.setBounds(246, 132, 34, CCMAG2.getPreferredSize().height);

          //---- CCMAG3 ----
          CCMAG3.setComponentPopupMenu(BTD);
          CCMAG3.setName("CCMAG3");
          p_recup.add(CCMAG3);
          CCMAG3.setBounds(307, 132, 34, CCMAG3.getPreferredSize().height);

          //---- CCMAG4 ----
          CCMAG4.setComponentPopupMenu(BTD);
          CCMAG4.setName("CCMAG4");
          p_recup.add(CCMAG4);
          CCMAG4.setBounds(368, 132, 34, CCMAG4.getPreferredSize().height);

          //---- CCMAG5 ----
          CCMAG5.setComponentPopupMenu(BTD);
          CCMAG5.setName("CCMAG5");
          p_recup.add(CCMAG5);
          CCMAG5.setBounds(429, 132, 34, CCMAG5.getPreferredSize().height);

          //---- OBJ_49 ----
          OBJ_49.setText("Fin");
          OBJ_49.setName("OBJ_49");
          p_recup.add(OBJ_49);
          OBJ_49.setBounds(368, 168, 57, 20);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(10, 10, 515, 225);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_ann;
  private RiSousMenu_bt riSousMenu_bt_ann;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_34;
  private JLabel OBJ_22;
  private JLabel OBJ_46;
  private JLabel OBJ_28;
  private JPanel P_PnlOpts;
  private JLabel OBJ_40;
  private XRiTextField CCMFRD;
  private XRiTextField CCMFRF;
  private JLabel OBJ_47;
  private XRiTextField CCZGE1;
  private XRiTextField CCZGE2;
  private XRiTextField CCZGE3;
  private XRiTextField CCZGE4;
  private XRiTextField CCZGE5;
  private XRiTextField CCMEX1;
  private XRiTextField CCMEX2;
  private XRiTextField CCMEX3;
  private XRiTextField CCMEX4;
  private XRiTextField CCMEX5;
  private XRiTextField CCMTR1;
  private XRiTextField CCMTR2;
  private XRiTextField CCMTR3;
  private XRiTextField CCMTR4;
  private XRiTextField CCMTR5;
  private XRiTextField CCMAG1;
  private XRiTextField CCMAG2;
  private XRiTextField CCMAG3;
  private XRiTextField CCMAG4;
  private XRiTextField CCMAG5;
  private JLabel OBJ_49;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
