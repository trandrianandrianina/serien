
package ri.serien.libecranrpg.vgvx.VGVXA42F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGVXA42F_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXA42F_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ZT01.setValeursSelection("1", "");
    ZT02.setValeursSelection("1", "");
    ZT03.setValeursSelection("1", "");
    ZT04.setValeursSelection("1", "");
    ZT05.setValeursSelection("1", "");
    ZT06.setValeursSelection("1", "");
    ZT07.setValeursSelection("1", "");
    ZT08.setValeursSelection("1", "");
    ZT09.setValeursSelection("1", "");
    ZT10.setValeursSelection("1", "");
    ZT11.setValeursSelection("1", "");
    ZT12.setValeursSelection("1", "");
    ZT13.setValeursSelection("1", "");
    ZT14.setValeursSelection("1", "");
    ZT15.setValeursSelection("1", "");
    ZT16.setValeursSelection("1", "");
    ZT17.setValeursSelection("1", "");
    ZT18.setValeursSelection("1", "");
    ZT19.setValeursSelection("1", "");
    ZT20.setValeursSelection("1", "");
    ZT21.setValeursSelection("1", "");
    ZT22.setValeursSelection("1", "");
    ZT23.setValeursSelection("1", "");
    ZT24.setValeursSelection("1", "");
    ZT25.setValeursSelection("1", "");
    ZT26.setValeursSelection("1", "");
    ZT27.setValeursSelection("1", "");
    ZT28.setValeursSelection("1", "");
    ZT29.setValeursSelection("1", "");
    ZT30.setValeursSelection("1", "");
    ZT31.setValeursSelection("1", "");
    ZT32.setValeursSelection("1", "");
    ZT33.setValeursSelection("1", "");
    ZT34.setValeursSelection("1", "");
    ZT35.setValeursSelection("1", "");
    ZT36.setValeursSelection("1", "");
    ZT37.setValeursSelection("1", "");
    ZT38.setValeursSelection("1", "");
    ZT39.setValeursSelection("1", "");
    ZT40.setValeursSelection("1", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WSOART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOART@")).trim());
    WSOARL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOARL@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("(@WNPQTE@ @UNSART@)")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOM@")).trim());
    OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EAFRS@")).trim());
    SONUM0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SONUM0@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EACOL@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SOSUF0@")).trim());
    ZT01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA01@")).trim());
    ZT02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA02@")).trim());
    ZT03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA03@")).trim());
    ZT04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA04@")).trim());
    ZT05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA05@")).trim());
    ZT06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA06@")).trim());
    ZT07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA07@")).trim());
    ZT08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA08@")).trim());
    ZT09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA09@")).trim());
    ZT10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA10@")).trim());
    ZT11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA11@")).trim());
    ZT12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA12@")).trim());
    ZT13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA13@")).trim());
    ZT14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA14@")).trim());
    ZT15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA15@")).trim());
    ZT16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA16@")).trim());
    ZT17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA17@")).trim());
    ZT18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA18@")).trim());
    ZT19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA19@")).trim());
    ZT20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA20@")).trim());
    ZT21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA21@")).trim());
    ZT22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA22@")).trim());
    ZT23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA23@")).trim());
    ZT24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA24@")).trim());
    ZT25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA25@")).trim());
    ZT26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA26@")).trim());
    ZT27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA27@")).trim());
    ZT28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA28@")).trim());
    ZT29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA29@")).trim());
    ZT30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA30@")).trim());
    ZT31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA31@")).trim());
    ZT32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA32@")).trim());
    ZT33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA33@")).trim());
    ZT34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA34@")).trim());
    ZT35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA35@")).trim());
    ZT36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA36@")).trim());
    ZT37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA37@")).trim());
    ZT38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA38@")).trim());
    ZT39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA39@")).trim());
    ZT40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA40@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    WETB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_36 = new JLabel();
    WNPA = new XRiTextField();
    WSOART = new RiZoneSortie();
    WSOARL = new RiZoneSortie();
    label1 = new JLabel();
    OBJ_52 = new RiZoneSortie();
    OBJ_49 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_51 = new RiZoneSortie();
    SONUM0 = new RiZoneSortie();
    OBJ_50 = new RiZoneSortie();
    OBJ_55 = new RiZoneSortie();
    ZT01 = new XRiCheckBox();
    ZT02 = new XRiCheckBox();
    ZT03 = new XRiCheckBox();
    ZT04 = new XRiCheckBox();
    ZT05 = new XRiCheckBox();
    ZT06 = new XRiCheckBox();
    ZT07 = new XRiCheckBox();
    ZT08 = new XRiCheckBox();
    ZT09 = new XRiCheckBox();
    ZT10 = new XRiCheckBox();
    ZT11 = new XRiCheckBox();
    ZT12 = new XRiCheckBox();
    ZT13 = new XRiCheckBox();
    ZT14 = new XRiCheckBox();
    ZT15 = new XRiCheckBox();
    ZT16 = new XRiCheckBox();
    ZT17 = new XRiCheckBox();
    ZT18 = new XRiCheckBox();
    ZT19 = new XRiCheckBox();
    ZT20 = new XRiCheckBox();
    ZT21 = new XRiCheckBox();
    ZT22 = new XRiCheckBox();
    ZT23 = new XRiCheckBox();
    ZT24 = new XRiCheckBox();
    ZT25 = new XRiCheckBox();
    ZT26 = new XRiCheckBox();
    ZT27 = new XRiCheckBox();
    ZT28 = new XRiCheckBox();
    ZT29 = new XRiCheckBox();
    ZT30 = new XRiCheckBox();
    ZT31 = new XRiCheckBox();
    ZT32 = new XRiCheckBox();
    ZT33 = new XRiCheckBox();
    ZT34 = new XRiCheckBox();
    ZT35 = new XRiCheckBox();
    ZT36 = new XRiCheckBox();
    ZT37 = new XRiCheckBox();
    ZT38 = new XRiCheckBox();
    ZT39 = new XRiCheckBox();
    ZT40 = new XRiCheckBox();
    separator1 = compFactory.createSeparator("A dupliquer sur");

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Palette multi-r\u00e9f\u00e9rences");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_34 ----
          OBJ_34.setText("Etablissement \u00e0 traiter");
          OBJ_34.setName("OBJ_34");
          p_tete_gauche.add(OBJ_34);
          OBJ_34.setBounds(5, 0, 151, 28);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(160, 0, 39, WETB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("On/Off saisie code barre");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(0, 0));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_36 ----
            OBJ_36.setText("Num\u00e9ro de palette");
            OBJ_36.setName("OBJ_36");
            panel1.add(OBJ_36);
            OBJ_36.setBounds(35, 45, 151, 28);

            //---- WNPA ----
            WNPA.setComponentPopupMenu(BTD);
            WNPA.setName("WNPA");
            panel1.add(WNPA);
            WNPA.setBounds(210, 45, 74, WNPA.getPreferredSize().height);

            //---- WSOART ----
            WSOART.setText("@WSOART@");
            WSOART.setName("WSOART");
            panel1.add(WSOART);
            WSOART.setBounds(210, 80, 210, WSOART.getPreferredSize().height);

            //---- WSOARL ----
            WSOARL.setText("@WSOARL@");
            WSOARL.setName("WSOARL");
            panel1.add(WSOARL);
            WSOARL.setBounds(210, 110, 310, WSOARL.getPreferredSize().height);

            //---- label1 ----
            label1.setText("(@WNPQTE@ @UNSART@)");
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(295, 45, 125, 28);

            //---- OBJ_52 ----
            OBJ_52.setText("@FRNOM@");
            OBJ_52.setName("OBJ_52");
            panel1.add(OBJ_52);
            OBJ_52.setBounds(315, 175, 231, OBJ_52.getPreferredSize().height);

            //---- OBJ_49 ----
            OBJ_49.setText("Fournisseur");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(35, 173, 151, 28);

            //---- OBJ_53 ----
            OBJ_53.setText("R\u00e9ception");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(35, 143, 151, 28);

            //---- OBJ_51 ----
            OBJ_51.setText("@EAFRS@");
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(245, 175, 56, OBJ_51.getPreferredSize().height);

            //---- SONUM0 ----
            SONUM0.setText("@SONUM0@");
            SONUM0.setName("SONUM0");
            panel1.add(SONUM0);
            SONUM0.setBounds(210, 145, 56, SONUM0.getPreferredSize().height);

            //---- OBJ_50 ----
            OBJ_50.setText("@EACOL@");
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(210, 175, 21, OBJ_50.getPreferredSize().height);

            //---- OBJ_55 ----
            OBJ_55.setText("@SOSUF0@");
            OBJ_55.setName("OBJ_55");
            panel1.add(OBJ_55);
            OBJ_55.setBounds(280, 145, 21, OBJ_55.getPreferredSize().height);

            //---- ZT01 ----
            ZT01.setText("@WNPA01@");
            ZT01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT01.setName("ZT01");
            panel1.add(ZT01);
            ZT01.setBounds(55, 230, 89, 25);

            //---- ZT02 ----
            ZT02.setText("@WNPA02@");
            ZT02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT02.setName("ZT02");
            panel1.add(ZT02);
            ZT02.setBounds(55, 258, 89, 27);

            //---- ZT03 ----
            ZT03.setText("@WNPA03@");
            ZT03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT03.setName("ZT03");
            panel1.add(ZT03);
            ZT03.setBounds(55, 286, 89, 29);

            //---- ZT04 ----
            ZT04.setText("@WNPA04@");
            ZT04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT04.setName("ZT04");
            panel1.add(ZT04);
            ZT04.setBounds(55, 314, 89, 26);

            //---- ZT05 ----
            ZT05.setText("@WNPA05@");
            ZT05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT05.setName("ZT05");
            panel1.add(ZT05);
            ZT05.setBounds(55, 342, 89, 28);

            //---- ZT06 ----
            ZT06.setText("@WNPA06@");
            ZT06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT06.setName("ZT06");
            panel1.add(ZT06);
            ZT06.setBounds(55, 370, 89, 25);

            //---- ZT07 ----
            ZT07.setText("@WNPA07@");
            ZT07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT07.setName("ZT07");
            panel1.add(ZT07);
            ZT07.setBounds(55, 398, 89, 27);

            //---- ZT08 ----
            ZT08.setText("@WNPA08@");
            ZT08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT08.setName("ZT08");
            panel1.add(ZT08);
            ZT08.setBounds(55, 426, 89, 29);

            //---- ZT09 ----
            ZT09.setText("@WNPA09@");
            ZT09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT09.setName("ZT09");
            panel1.add(ZT09);
            ZT09.setBounds(55, 454, 89, 26);

            //---- ZT10 ----
            ZT10.setText("@WNPA10@");
            ZT10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT10.setName("ZT10");
            panel1.add(ZT10);
            ZT10.setBounds(55, 482, 89, 28);

            //---- ZT11 ----
            ZT11.setText("@WNPA11@");
            ZT11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT11.setName("ZT11");
            panel1.add(ZT11);
            ZT11.setBounds(206, 230, 89, 25);

            //---- ZT12 ----
            ZT12.setText("@WNPA12@");
            ZT12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT12.setName("ZT12");
            panel1.add(ZT12);
            ZT12.setBounds(206, 258, 89, 27);

            //---- ZT13 ----
            ZT13.setText("@WNPA13@");
            ZT13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT13.setName("ZT13");
            panel1.add(ZT13);
            ZT13.setBounds(206, 286, 89, 29);

            //---- ZT14 ----
            ZT14.setText("@WNPA14@");
            ZT14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT14.setName("ZT14");
            panel1.add(ZT14);
            ZT14.setBounds(206, 314, 89, 26);

            //---- ZT15 ----
            ZT15.setText("@WNPA15@");
            ZT15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT15.setName("ZT15");
            panel1.add(ZT15);
            ZT15.setBounds(206, 342, 89, 28);

            //---- ZT16 ----
            ZT16.setText("@WNPA16@");
            ZT16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT16.setName("ZT16");
            panel1.add(ZT16);
            ZT16.setBounds(206, 370, 89, 25);

            //---- ZT17 ----
            ZT17.setText("@WNPA17@");
            ZT17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT17.setName("ZT17");
            panel1.add(ZT17);
            ZT17.setBounds(206, 398, 89, 27);

            //---- ZT18 ----
            ZT18.setText("@WNPA18@");
            ZT18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT18.setName("ZT18");
            panel1.add(ZT18);
            ZT18.setBounds(206, 426, 89, 29);

            //---- ZT19 ----
            ZT19.setText("@WNPA19@");
            ZT19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT19.setName("ZT19");
            panel1.add(ZT19);
            ZT19.setBounds(206, 454, 89, 26);

            //---- ZT20 ----
            ZT20.setText("@WNPA20@");
            ZT20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT20.setName("ZT20");
            panel1.add(ZT20);
            ZT20.setBounds(206, 482, 89, 28);

            //---- ZT21 ----
            ZT21.setText("@WNPA21@");
            ZT21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT21.setName("ZT21");
            panel1.add(ZT21);
            ZT21.setBounds(357, 230, 89, 25);

            //---- ZT22 ----
            ZT22.setText("@WNPA22@");
            ZT22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT22.setName("ZT22");
            panel1.add(ZT22);
            ZT22.setBounds(357, 258, 89, 27);

            //---- ZT23 ----
            ZT23.setText("@WNPA23@");
            ZT23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT23.setName("ZT23");
            panel1.add(ZT23);
            ZT23.setBounds(357, 286, 89, 29);

            //---- ZT24 ----
            ZT24.setText("@WNPA24@");
            ZT24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT24.setName("ZT24");
            panel1.add(ZT24);
            ZT24.setBounds(357, 314, 89, 26);

            //---- ZT25 ----
            ZT25.setText("@WNPA25@");
            ZT25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT25.setName("ZT25");
            panel1.add(ZT25);
            ZT25.setBounds(357, 342, 89, 28);

            //---- ZT26 ----
            ZT26.setText("@WNPA26@");
            ZT26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT26.setName("ZT26");
            panel1.add(ZT26);
            ZT26.setBounds(357, 370, 89, 25);

            //---- ZT27 ----
            ZT27.setText("@WNPA27@");
            ZT27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT27.setName("ZT27");
            panel1.add(ZT27);
            ZT27.setBounds(357, 398, 89, 27);

            //---- ZT28 ----
            ZT28.setText("@WNPA28@");
            ZT28.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT28.setName("ZT28");
            panel1.add(ZT28);
            ZT28.setBounds(357, 426, 89, 29);

            //---- ZT29 ----
            ZT29.setText("@WNPA29@");
            ZT29.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT29.setName("ZT29");
            panel1.add(ZT29);
            ZT29.setBounds(357, 454, 89, 26);

            //---- ZT30 ----
            ZT30.setText("@WNPA30@");
            ZT30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT30.setName("ZT30");
            panel1.add(ZT30);
            ZT30.setBounds(357, 482, 89, 28);

            //---- ZT31 ----
            ZT31.setText("@WNPA31@");
            ZT31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT31.setName("ZT31");
            panel1.add(ZT31);
            ZT31.setBounds(508, 230, 89, 25);

            //---- ZT32 ----
            ZT32.setText("@WNPA32@");
            ZT32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT32.setName("ZT32");
            panel1.add(ZT32);
            ZT32.setBounds(508, 258, 89, 27);

            //---- ZT33 ----
            ZT33.setText("@WNPA33@");
            ZT33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT33.setName("ZT33");
            panel1.add(ZT33);
            ZT33.setBounds(508, 286, 89, 29);

            //---- ZT34 ----
            ZT34.setText("@WNPA34@");
            ZT34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT34.setName("ZT34");
            panel1.add(ZT34);
            ZT34.setBounds(508, 314, 89, 26);

            //---- ZT35 ----
            ZT35.setText("@WNPA35@");
            ZT35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT35.setName("ZT35");
            panel1.add(ZT35);
            ZT35.setBounds(508, 342, 89, 28);

            //---- ZT36 ----
            ZT36.setText("@WNPA36@");
            ZT36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT36.setName("ZT36");
            panel1.add(ZT36);
            ZT36.setBounds(508, 370, 89, 25);

            //---- ZT37 ----
            ZT37.setText("@WNPA37@");
            ZT37.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT37.setName("ZT37");
            panel1.add(ZT37);
            ZT37.setBounds(508, 398, 89, 27);

            //---- ZT38 ----
            ZT38.setText("@WNPA38@");
            ZT38.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT38.setName("ZT38");
            panel1.add(ZT38);
            ZT38.setBounds(508, 426, 89, 29);

            //---- ZT39 ----
            ZT39.setText("@WNPA39@");
            ZT39.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT39.setName("ZT39");
            panel1.add(ZT39);
            ZT39.setBounds(508, 454, 89, 26);

            //---- ZT40 ----
            ZT40.setText("@WNPA40@");
            ZT40.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZT40.setName("ZT40");
            panel1.add(ZT40);
            ZT40.setBounds(508, 482, 89, 28);

            //---- separator1 ----
            separator1.setName("separator1");
            panel1.add(separator1);
            separator1.setBounds(45, 205, 555, separator1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 674, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 522, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_34;
  private XRiTextField WETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_36;
  private XRiTextField WNPA;
  private RiZoneSortie WSOART;
  private RiZoneSortie WSOARL;
  private JLabel label1;
  private RiZoneSortie OBJ_52;
  private JLabel OBJ_49;
  private JLabel OBJ_53;
  private RiZoneSortie OBJ_51;
  private RiZoneSortie SONUM0;
  private RiZoneSortie OBJ_50;
  private RiZoneSortie OBJ_55;
  private XRiCheckBox ZT01;
  private XRiCheckBox ZT02;
  private XRiCheckBox ZT03;
  private XRiCheckBox ZT04;
  private XRiCheckBox ZT05;
  private XRiCheckBox ZT06;
  private XRiCheckBox ZT07;
  private XRiCheckBox ZT08;
  private XRiCheckBox ZT09;
  private XRiCheckBox ZT10;
  private XRiCheckBox ZT11;
  private XRiCheckBox ZT12;
  private XRiCheckBox ZT13;
  private XRiCheckBox ZT14;
  private XRiCheckBox ZT15;
  private XRiCheckBox ZT16;
  private XRiCheckBox ZT17;
  private XRiCheckBox ZT18;
  private XRiCheckBox ZT19;
  private XRiCheckBox ZT20;
  private XRiCheckBox ZT21;
  private XRiCheckBox ZT22;
  private XRiCheckBox ZT23;
  private XRiCheckBox ZT24;
  private XRiCheckBox ZT25;
  private XRiCheckBox ZT26;
  private XRiCheckBox ZT27;
  private XRiCheckBox ZT28;
  private XRiCheckBox ZT29;
  private XRiCheckBox ZT30;
  private XRiCheckBox ZT31;
  private XRiCheckBox ZT32;
  private XRiCheckBox ZT33;
  private XRiCheckBox ZT34;
  private XRiCheckBox ZT35;
  private XRiCheckBox ZT36;
  private XRiCheckBox ZT37;
  private XRiCheckBox ZT38;
  private XRiCheckBox ZT39;
  private XRiCheckBox ZT40;
  private JComponent separator1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
