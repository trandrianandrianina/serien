
package ri.serien.libecranrpg.vgvx.VGVXXLBF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXXLBF_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  int tailleLignes = 0;
  int nbMaxCar = 0;
  
  public VGVXXLBF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    A1ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1ART@")).trim());
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LIB01.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB02.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB03.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB04.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB05.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB06.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB07.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB08.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB09.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB10.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB11.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB12.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB13.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB14.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    LIB15.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD2X1@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1LB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB1@")).trim());
    A1LB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB2@")).trim());
    A1LB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB3@")).trim());
    NBCL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCL1@")).trim());
    NBCLS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCLS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(riMenu_bt2);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // On retaille les lignes de saisie à la taille maximum fixée par NBCLS, fixe le nombre de caractères maximum
    // saisissables.
    // On ne le fait qu'une seule fois, et si nécessaire, c'est à dire si les lignes sont de la taille de la têtière.
    if (label21.getWidth() == LIB01.getWidth()) {
      XRiTextField[] lignes = { LIB01, LIB02, LIB03, LIB04, LIB05, LIB06, LIB07, LIB08, LIB09, LIB10, LIB11, LIB12, LIB13, LIB14, LIB15, };
      nbMaxCar = Integer.parseInt(lexique.HostFieldGetData("NBCLS"));
      tailleLignes = (Math.round(LIB01.getWidth() / 73) * nbMaxCar) + 20;
      
      for (int i = 0; i < lignes.length; i++) {
        lignes[i].setSize(tailleLignes, lignes[i].getHeight());
        lignes[i].setLongueurSaisie(nbMaxCar);
      }
    }
    
    // icones
    p_bpresentation.setCodeEtablissement(WETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    A1ART = new RiZoneSortie();
    OBJ_59 = new JLabel();
    WETB = new RiZoneSortie();
    OBJ_55 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    LIB01 = new XRiTextField();
    label21 = new JLabel();
    LIB02 = new XRiTextField();
    LIB03 = new XRiTextField();
    LIB04 = new XRiTextField();
    LIB05 = new XRiTextField();
    LIB06 = new XRiTextField();
    LIB07 = new XRiTextField();
    LIB08 = new XRiTextField();
    LIB09 = new XRiTextField();
    LIB10 = new XRiTextField();
    LIB11 = new XRiTextField();
    LIB12 = new XRiTextField();
    LIB13 = new XRiTextField();
    LIB14 = new XRiTextField();
    LIB15 = new XRiTextField();
    panel2 = new JPanel();
    A1LIB = new RiZoneSortie();
    A1LB1 = new RiZoneSortie();
    A1LB2 = new RiZoneSortie();
    A1LB3 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    NBCL1 = new RiZoneSortie();
    NBCLS = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Extensions des libell\u00e9s d'un article");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(800, 30));
          p_tete_gauche.setMinimumSize(new Dimension(800, 30));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- A1ART ----
          A1ART.setOpaque(false);
          A1ART.setText("@A1ART@");
          A1ART.setName("A1ART");
          p_tete_gauche.add(A1ART);
          A1ART.setBounds(300, 3, 210, A1ART.getPreferredSize().height);

          //---- OBJ_59 ----
          OBJ_59.setText("Code article");
          OBJ_59.setName("OBJ_59");
          p_tete_gauche.add(OBJ_59);
          OBJ_59.setBounds(210, 3, 84, 24);

          //---- WETB ----
          WETB.setOpaque(false);
          WETB.setText("@WETB@");
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(125, 3, 40, WETB.getPreferredSize().height);

          //---- OBJ_55 ----
          OBJ_55.setText("Etablissement");
          OBJ_55.setName("OBJ_55");
          p_tete_gauche.add(OBJ_55);
          OBJ_55.setBounds(5, 3, 95, 24);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Gestion des libell\u00e9s");
              riSousMenu_bt6.setToolTipText("Acc\u00e8s \u00e0 la gestion des libell\u00e9s de l'article (F2)");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("R\u00e9initialisation des libell\u00e9s");
              riSousMenu_bt7.setToolTipText("R\u00e9initialiser avec les libell\u00e9s de l'article (F5)");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 590));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(600, 590));
          p_contenu.setMaximumSize(new Dimension(706, 600));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setPreferredSize(new Dimension(574, 550));
            panel1.setMinimumSize(new Dimension(574, 557));
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(930, 85, 25, 140);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(925, 395, 25, 140);

            //---- LIB01 ----
            LIB01.setToolTipText("@LD2X1@");
            LIB01.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB01.setComponentPopupMenu(BTD);
            LIB01.setName("LIB01");
            panel1.add(LIB01);
            LIB01.setBounds(20, 30, 530, 28);

            //---- label21 ----
            label21.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
            label21.setText("Libell\u00e9..1....|....2....|....3....|....4....|....5....|....6....|....7...");
            label21.setName("label21");
            panel1.add(label21);
            label21.setBounds(30, 5, 530, 30);

            //---- LIB02 ----
            LIB02.setToolTipText("@LD2X1@");
            LIB02.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB02.setComponentPopupMenu(BTD);
            LIB02.setName("LIB02");
            panel1.add(LIB02);
            LIB02.setBounds(20, 55, 530, 28);

            //---- LIB03 ----
            LIB03.setToolTipText("@LD2X1@");
            LIB03.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB03.setComponentPopupMenu(BTD);
            LIB03.setName("LIB03");
            panel1.add(LIB03);
            LIB03.setBounds(20, 80, 530, 28);

            //---- LIB04 ----
            LIB04.setToolTipText("@LD2X1@");
            LIB04.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB04.setComponentPopupMenu(BTD);
            LIB04.setName("LIB04");
            panel1.add(LIB04);
            LIB04.setBounds(20, 105, 530, 28);

            //---- LIB05 ----
            LIB05.setToolTipText("@LD2X1@");
            LIB05.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB05.setComponentPopupMenu(BTD);
            LIB05.setName("LIB05");
            panel1.add(LIB05);
            LIB05.setBounds(20, 130, 530, 28);

            //---- LIB06 ----
            LIB06.setToolTipText("@LD2X1@");
            LIB06.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB06.setComponentPopupMenu(BTD);
            LIB06.setName("LIB06");
            panel1.add(LIB06);
            LIB06.setBounds(20, 155, 530, 28);

            //---- LIB07 ----
            LIB07.setToolTipText("@LD2X1@");
            LIB07.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB07.setComponentPopupMenu(BTD);
            LIB07.setName("LIB07");
            panel1.add(LIB07);
            LIB07.setBounds(20, 180, 530, 28);

            //---- LIB08 ----
            LIB08.setToolTipText("@LD2X1@");
            LIB08.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB08.setComponentPopupMenu(BTD);
            LIB08.setName("LIB08");
            panel1.add(LIB08);
            LIB08.setBounds(20, 205, 530, 28);

            //---- LIB09 ----
            LIB09.setToolTipText("@LD2X1@");
            LIB09.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB09.setComponentPopupMenu(BTD);
            LIB09.setName("LIB09");
            panel1.add(LIB09);
            LIB09.setBounds(20, 230, 530, 28);

            //---- LIB10 ----
            LIB10.setToolTipText("@LD2X1@");
            LIB10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB10.setComponentPopupMenu(BTD);
            LIB10.setName("LIB10");
            panel1.add(LIB10);
            LIB10.setBounds(20, 255, 530, 28);

            //---- LIB11 ----
            LIB11.setToolTipText("@LD2X1@");
            LIB11.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB11.setComponentPopupMenu(BTD);
            LIB11.setName("LIB11");
            panel1.add(LIB11);
            LIB11.setBounds(20, 280, 530, 28);

            //---- LIB12 ----
            LIB12.setToolTipText("@LD2X1@");
            LIB12.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB12.setComponentPopupMenu(BTD);
            LIB12.setName("LIB12");
            panel1.add(LIB12);
            LIB12.setBounds(20, 305, 530, 28);

            //---- LIB13 ----
            LIB13.setToolTipText("@LD2X1@");
            LIB13.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB13.setComponentPopupMenu(BTD);
            LIB13.setName("LIB13");
            panel1.add(LIB13);
            LIB13.setBounds(20, 330, 530, 28);

            //---- LIB14 ----
            LIB14.setToolTipText("@LD2X1@");
            LIB14.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB14.setComponentPopupMenu(BTD);
            LIB14.setName("LIB14");
            panel1.add(LIB14);
            LIB14.setBounds(20, 355, 530, 28);

            //---- LIB15 ----
            LIB15.setToolTipText("@LD2X1@");
            LIB15.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
            LIB15.setComponentPopupMenu(BTD);
            LIB15.setName("LIB15");
            panel1.add(LIB15);
            LIB15.setBounds(20, 380, 530, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- A1LIB ----
            A1LIB.setText("@A1LIB@");
            A1LIB.setName("A1LIB");
            panel2.add(A1LIB);
            A1LIB.setBounds(30, 10, 310, A1LIB.getPreferredSize().height);

            //---- A1LB1 ----
            A1LB1.setText("@A1LB1@");
            A1LB1.setName("A1LB1");
            panel2.add(A1LB1);
            A1LB1.setBounds(30, 35, 310, A1LB1.getPreferredSize().height);

            //---- A1LB2 ----
            A1LB2.setText("@A1LB2@");
            A1LB2.setName("A1LB2");
            panel2.add(A1LB2);
            A1LB2.setBounds(30, 60, 310, A1LB2.getPreferredSize().height);

            //---- A1LB3 ----
            A1LB3.setText("@A1LB3@");
            A1LB3.setName("A1LB3");
            panel2.add(A1LB3);
            A1LB3.setBounds(30, 85, 310, A1LB3.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Taille de la premi\u00e8re ligne");
            label1.setName("label1");
            panel2.add(label1);
            label1.setBounds(350, 10, 155, 24);

            //---- label2 ----
            label2.setText("Taille des lignes suivantes");
            label2.setName("label2");
            panel2.add(label2);
            label2.setBounds(350, 35, 155, 24);

            //---- NBCL1 ----
            NBCL1.setText("@NBCL1@");
            NBCL1.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCL1.setName("NBCL1");
            panel2.add(NBCL1);
            NBCL1.setBounds(515, 10, 28, NBCL1.getPreferredSize().height);

            //---- NBCLS ----
            NBCLS.setText("@NBCLS@");
            NBCLS.setHorizontalAlignment(SwingConstants.RIGHT);
            NBCLS.setName("NBCLS");
            panel2.add(NBCLS);
            NBCLS.setBounds(515, 35, 28, NBCLS.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 424, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie A1ART;
  private JLabel OBJ_59;
  private RiZoneSortie WETB;
  private JLabel OBJ_55;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiTextField LIB01;
  private JLabel label21;
  private XRiTextField LIB02;
  private XRiTextField LIB03;
  private XRiTextField LIB04;
  private XRiTextField LIB05;
  private XRiTextField LIB06;
  private XRiTextField LIB07;
  private XRiTextField LIB08;
  private XRiTextField LIB09;
  private XRiTextField LIB10;
  private XRiTextField LIB11;
  private XRiTextField LIB12;
  private XRiTextField LIB13;
  private XRiTextField LIB14;
  private XRiTextField LIB15;
  private JPanel panel2;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1LB1;
  private RiZoneSortie A1LB2;
  private RiZoneSortie A1LB3;
  private JLabel label1;
  private JLabel label2;
  private RiZoneSortie NBCL1;
  private RiZoneSortie NBCLS;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
