
package ri.serien.libecranrpg.vgvx.VGVX093F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class VGVX093F_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGVX093F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_retour);
    setDialog(true);
    initDiverses();
    
    setTitle("Choix unité");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WUNL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNL@")).trim());
    WLUNL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLUNL@")).trim());
    WCOEFL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOEFL@")).trim());
    WUNV1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNV1@")).trim());
    WUNL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNV@")).trim());
    WLUNLV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLUNV@")).trim());
    WUNL3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUCS@")).trim());
    WLUNCS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLUCS@")).trim());
    WCOEFC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOEFC@")).trim());
    WUNV3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNV3@")).trim());
    WUNL4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNS@")).trim());
    WLUNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLUNS@")).trim());
    WCOEFC2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOEFS@")).trim());
    WUNV4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNV4@")).trim());
    textField1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WART@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQTE@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUU@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    gererLesErreurs("19");
    
    p_stock.setVisible(lexique.isTrue("N34"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void bt_choixActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WOPT1", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_choix2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WOPT2", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_choix3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WOPT3", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_choix4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WOPT4", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    WUNL = new RiZoneSortie();
    WLUNL = new RiZoneSortie();
    lb_coeff = new JLabel();
    WCOEFL = new RiZoneSortie();
    WUNV1 = new RiZoneSortie();
    bt_choix = new SNBoutonLeger();
    WUNL2 = new RiZoneSortie();
    WLUNLV = new RiZoneSortie();
    bt_choix2 = new SNBoutonLeger();
    WUNL3 = new RiZoneSortie();
    WLUNCS = new RiZoneSortie();
    lb_coeff2 = new JLabel();
    WCOEFC = new RiZoneSortie();
    WUNV3 = new RiZoneSortie();
    bt_choix3 = new SNBoutonLeger();
    p_stock = new JPanel();
    WUNL4 = new RiZoneSortie();
    WLUNS = new RiZoneSortie();
    lb_coeff3 = new JLabel();
    WCOEFC2 = new RiZoneSortie();
    WUNV4 = new RiZoneSortie();
    bt_choix4 = new SNBoutonLeger();
    barre_tete = new JMenuBar();
    panel1 = new JPanel();
    label1 = new JLabel();
    textField1 = new RiZoneSortie();
    label2 = new JLabel();
    riZoneSortie1 = new RiZoneSortie();
    riZoneSortie3 = new RiZoneSortie();
    riZoneSortie2 = new RiZoneSortie();
    lb_unit = new JLabel();
    lb_unit2 = new JLabel();
    lb_unit3 = new JLabel();
    lb_unit4 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(780, 295));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Unit\u00e9s"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- WUNL ----
          WUNL.setText("@WUNL@");
          WUNL.setName("WUNL");
          panel2.add(WUNL);
          WUNL.setBounds(95, 45, 34, WUNL.getPreferredSize().height);

          //---- WLUNL ----
          WLUNL.setText("@WLUNL@");
          WLUNL.setName("WLUNL");
          panel2.add(WLUNL);
          WLUNL.setBounds(135, 45, 160, WLUNL.getPreferredSize().height);

          //---- lb_coeff ----
          lb_coeff.setText("Coefficient");
          lb_coeff.setName("lb_coeff");
          panel2.add(lb_coeff);
          lb_coeff.setBounds(305, 45, 70, 20);

          //---- WCOEFL ----
          WCOEFL.setText("@WCOEFL@");
          WCOEFL.setHorizontalAlignment(SwingConstants.RIGHT);
          WCOEFL.setName("WCOEFL");
          panel2.add(WCOEFL);
          WCOEFL.setBounds(370, 45, 75, WCOEFL.getPreferredSize().height);

          //---- WUNV1 ----
          WUNV1.setText("@WUNV1@");
          WUNV1.setName("WUNV1");
          panel2.add(WUNV1);
          WUNV1.setBounds(450, 45, 34, WUNV1.getPreferredSize().height);

          //---- bt_choix ----
          bt_choix.setText("choisir");
          bt_choix.setName("bt_choix");
          bt_choix.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_choixActionPerformed(e);
            }
          });
          panel2.add(bt_choix);
          bt_choix.setBounds(15, 45, 80, 25);

          //---- WUNL2 ----
          WUNL2.setText("@WUNV@");
          WUNL2.setName("WUNL2");
          panel2.add(WUNL2);
          WUNL2.setBounds(95, 80, 34, 24);

          //---- WLUNLV ----
          WLUNLV.setText("@WLUNV@");
          WLUNLV.setName("WLUNLV");
          panel2.add(WLUNLV);
          WLUNLV.setBounds(135, 80, 160, 24);

          //---- bt_choix2 ----
          bt_choix2.setText("choisir");
          bt_choix2.setName("bt_choix2");
          bt_choix2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_choix2ActionPerformed(e);
            }
          });
          panel2.add(bt_choix2);
          bt_choix2.setBounds(15, 80, 80, 25);

          //---- WUNL3 ----
          WUNL3.setText("@WUCS@");
          WUNL3.setName("WUNL3");
          panel2.add(WUNL3);
          WUNL3.setBounds(95, 115, 34, 24);

          //---- WLUNCS ----
          WLUNCS.setText("@WLUCS@");
          WLUNCS.setName("WLUNCS");
          panel2.add(WLUNCS);
          WLUNCS.setBounds(135, 115, 160, 24);

          //---- lb_coeff2 ----
          lb_coeff2.setText("Coefficient");
          lb_coeff2.setName("lb_coeff2");
          panel2.add(lb_coeff2);
          lb_coeff2.setBounds(305, 117, 70, 20);

          //---- WCOEFC ----
          WCOEFC.setText("@WCOEFC@");
          WCOEFC.setHorizontalAlignment(SwingConstants.RIGHT);
          WCOEFC.setName("WCOEFC");
          panel2.add(WCOEFC);
          WCOEFC.setBounds(370, 115, 75, 24);

          //---- WUNV3 ----
          WUNV3.setText("@WUNV3@");
          WUNV3.setName("WUNV3");
          panel2.add(WUNV3);
          WUNV3.setBounds(450, 115, 34, 24);

          //---- bt_choix3 ----
          bt_choix3.setText("choisir");
          bt_choix3.setName("bt_choix3");
          bt_choix3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_choix3ActionPerformed(e);
            }
          });
          panel2.add(bt_choix3);
          bt_choix3.setBounds(15, 115, 80, 25);

          //======== p_stock ========
          {
            p_stock.setOpaque(false);
            p_stock.setName("p_stock");
            p_stock.setLayout(null);

            //---- WUNL4 ----
            WUNL4.setText("@WUNS@");
            WUNL4.setName("WUNL4");
            p_stock.add(WUNL4);
            WUNL4.setBounds(85, 10, 34, 25);

            //---- WLUNS ----
            WLUNS.setText("@WLUNS@");
            WLUNS.setName("WLUNS");
            p_stock.add(WLUNS);
            WLUNS.setBounds(125, 10, 160, 24);

            //---- lb_coeff3 ----
            lb_coeff3.setText("Coefficient");
            lb_coeff3.setName("lb_coeff3");
            p_stock.add(lb_coeff3);
            lb_coeff3.setBounds(295, 10, 70, 20);

            //---- WCOEFC2 ----
            WCOEFC2.setText("@WCOEFS@");
            WCOEFC2.setHorizontalAlignment(SwingConstants.RIGHT);
            WCOEFC2.setName("WCOEFC2");
            p_stock.add(WCOEFC2);
            WCOEFC2.setBounds(360, 10, 75, 24);

            //---- WUNV4 ----
            WUNV4.setText("@WUNV4@");
            WUNV4.setName("WUNV4");
            p_stock.add(WUNV4);
            WUNV4.setBounds(440, 10, 34, 24);

            //---- bt_choix4 ----
            bt_choix4.setText("choisir");
            bt_choix4.setName("bt_choix4");
            bt_choix4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_choix4ActionPerformed(e);
              }
            });
            p_stock.add(bt_choix4);
            bt_choix4.setBounds(5, 10, 80, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < p_stock.getComponentCount(); i++) {
                Rectangle bounds = p_stock.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = p_stock.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              p_stock.setMinimumSize(preferredSize);
              p_stock.setPreferredSize(preferredSize);
            }
          }
          panel2.add(p_stock);
          p_stock.setBounds(10, 140, 490, 40);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(50, 50, 50)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(50, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 206, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(32, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 32));
      barre_tete.setName("barre_tete");

      //======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- label1 ----
        label1.setText("Code article");
        label1.setName("label1");
        panel1.add(label1);
        label1.setBounds(5, 4, 75, 20);

        //---- textField1 ----
        textField1.setText("@WART@");
        textField1.setOpaque(false);
        textField1.setName("textField1");
        panel1.add(textField1);
        textField1.setBounds(80, 2, 210, textField1.getPreferredSize().height);

        //---- label2 ----
        label2.setText("Quantit\u00e9 ");
        label2.setName("label2");
        panel1.add(label2);
        label2.setBounds(605, 4, 60, 20);

        //---- riZoneSortie1 ----
        riZoneSortie1.setText("@WQTE@");
        riZoneSortie1.setOpaque(false);
        riZoneSortie1.setHorizontalAlignment(SwingConstants.RIGHT);
        riZoneSortie1.setName("riZoneSortie1");
        panel1.add(riZoneSortie1);
        riZoneSortie1.setBounds(660, 2, 70, riZoneSortie1.getPreferredSize().height);

        //---- riZoneSortie3 ----
        riZoneSortie3.setText("@WUU@");
        riZoneSortie3.setOpaque(false);
        riZoneSortie3.setName("riZoneSortie3");
        panel1.add(riZoneSortie3);
        riZoneSortie3.setBounds(735, 2, 20, riZoneSortie3.getPreferredSize().height);

        //---- riZoneSortie2 ----
        riZoneSortie2.setText("@WLIB@");
        riZoneSortie2.setOpaque(false);
        riZoneSortie2.setName("riZoneSortie2");
        panel1.add(riZoneSortie2);
        riZoneSortie2.setBounds(295, 2, 290, riZoneSortie2.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(panel1);
    }
    add(barre_tete, BorderLayout.NORTH);

    //---- lb_unit ----
    lb_unit.setText("UL (C)");
    lb_unit.setName("lb_unit");

    //---- lb_unit2 ----
    lb_unit2.setText("UV (M)");
    lb_unit2.setName("lb_unit2");

    //---- lb_unit3 ----
    lb_unit3.setText("UCS (P)");
    lb_unit3.setName("lb_unit3");

    //---- lb_unit4 ----
    lb_unit4.setText("US");
    lb_unit4.setName("lb_unit4");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private RiZoneSortie WUNL;
  private RiZoneSortie WLUNL;
  private JLabel lb_coeff;
  private RiZoneSortie WCOEFL;
  private RiZoneSortie WUNV1;
  private SNBoutonLeger bt_choix;
  private RiZoneSortie WUNL2;
  private RiZoneSortie WLUNLV;
  private SNBoutonLeger bt_choix2;
  private RiZoneSortie WUNL3;
  private RiZoneSortie WLUNCS;
  private JLabel lb_coeff2;
  private RiZoneSortie WCOEFC;
  private RiZoneSortie WUNV3;
  private SNBoutonLeger bt_choix3;
  private JPanel p_stock;
  private RiZoneSortie WUNL4;
  private RiZoneSortie WLUNS;
  private JLabel lb_coeff3;
  private RiZoneSortie WCOEFC2;
  private RiZoneSortie WUNV4;
  private SNBoutonLeger bt_choix4;
  private JMenuBar barre_tete;
  private JPanel panel1;
  private JLabel label1;
  private RiZoneSortie textField1;
  private JLabel label2;
  private RiZoneSortie riZoneSortie1;
  private RiZoneSortie riZoneSortie3;
  private RiZoneSortie riZoneSortie2;
  private JLabel lb_unit;
  private JLabel lb_unit2;
  private JLabel lb_unit3;
  private JLabel lb_unit4;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
