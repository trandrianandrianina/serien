
package ri.serien.libecranrpg.vgvx.VGVXCCFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVXCCFM_C3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] CCPERF_Value = { "", "J", "S", "D", "Q", "M", };
  private String[] CCPERR_Value = { "", "J", "S", "D", "Q", "M", };
  
  public VGVXCCFM_C3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    CCPERR.setValeurs(CCPERR_Value, null);
    CCPERF.setValeurs(CCPERF_Value, null);
    CCTYPT.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // CCTYPT.setSelected(lexique.HostFieldGetData("CCTYPT").equalsIgnoreCase("1"));
    CCTYPF.setEnabled(lexique.isPresent("CCTYPF"));
    CCECH5.setEnabled(lexique.isPresent("CCECH5"));
    CCECH4.setEnabled(lexique.isPresent("CCECH4"));
    CCECH3.setEnabled(lexique.isPresent("CCECH3"));
    CCECH2.setEnabled(lexique.isPresent("CCECH2"));
    CCECH1.setEnabled(lexique.isPresent("CCECH1"));
    CCMRG5.setEnabled(lexique.isPresent("CCMRG5"));
    CCMRG4.setEnabled(lexique.isPresent("CCMRG4"));
    CCMRG3.setEnabled(lexique.isPresent("CCMRG3"));
    CCMRG2.setEnabled(lexique.isPresent("CCMRG2"));
    CCMRG1.setEnabled(lexique.isPresent("CCMRG1"));
    CCREMF.setEnabled(lexique.isPresent("CCREMF"));
    CCREMD.setEnabled(lexique.isPresent("CCREMD"));
    CCPESF.setEnabled(lexique.isPresent("CCPESF"));
    CCPESD.setEnabled(lexique.isPresent("CCPESD"));
    CCDEV3.setEnabled(lexique.isPresent("CCDEV3"));
    CCDEV2.setEnabled(lexique.isPresent("CCDEV2"));
    CCDEV1.setEnabled(lexique.isPresent("CCDEV1"));
    CCCFLF.setEnabled(lexique.isPresent("CCCFLF"));
    CCCFLD.setEnabled(lexique.isPresent("CCCFLD"));
    CCCFPF.setEnabled(lexique.isPresent("CCCFPF"));
    CCCFPD.setEnabled(lexique.isPresent("CCCFPD"));
    // CCPERF.setEnabled( lexique.isPresent("CCPERF"));
    // CCPERR.setEnabled( lexique.isPresent("CCPERR"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Critères de sélection"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (CCTYPT.isSelected())
    // lexique.HostFieldPutData("CCTYPT", 0, "1");
    // else
    // lexique.HostFieldPutData("CCTYPT", 0, " ");
    
    // lexique.HostFieldPutData("CCPERF", 0, CCPERF_Value[CCPERF.getSelectedIndex()]);
    // lexique.HostFieldPutData("CCPERR", 0, CCPERR_Value[CCPERR.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_ann = new RiSousMenu();
    riSousMenu_bt_ann = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_40 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_30 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_36 = new JLabel();
    CCCFPD = new XRiTextField();
    CCCFPF = new XRiTextField();
    OBJ_48 = new JLabel();
    OBJ_23 = new JLabel();
    CCCFLD = new XRiTextField();
    CCCFLF = new XRiTextField();
    CCDEV1 = new XRiTextField();
    CCDEV2 = new XRiTextField();
    CCDEV3 = new XRiTextField();
    CCPESD = new XRiTextField();
    CCPESF = new XRiTextField();
    CCREMD = new XRiTextField();
    CCREMF = new XRiTextField();
    CCMRG1 = new XRiTextField();
    CCMRG2 = new XRiTextField();
    CCMRG3 = new XRiTextField();
    CCMRG4 = new XRiTextField();
    CCMRG5 = new XRiTextField();
    CCECH1 = new XRiTextField();
    CCECH2 = new XRiTextField();
    CCECH3 = new XRiTextField();
    CCECH4 = new XRiTextField();
    CCECH5 = new XRiTextField();
    CCTYPF = new XRiTextField();
    OBJ_33 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_46 = new JLabel();
    separator1 = compFactory.createSeparator("P\u00e9riodicit\u00e9s");
    separator2 = compFactory.createSeparator("R\u00e8glement");
    OBJ_28 = new JLabel();
    CCPERF = new XRiComboBox();
    OBJ_29 = new JLabel();
    CCPERR = new XRiComboBox();
    CCTYPT = new XRiCheckBox();
    separator3 = compFactory.createSeparator("");
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_ann ========
          {
            riSousMenu_ann.setName("riSousMenu_ann");

            //---- riSousMenu_bt_ann ----
            riSousMenu_bt_ann.setText("Annulation");
            riSousMenu_bt_ann.setToolTipText("Annulation");
            riSousMenu_bt_ann.setName("riSousMenu_bt_ann");
            riSousMenu_ann.add(riSousMenu_bt_ann);
          }
          menus_haut.add(riSousMenu_ann);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setToolTipText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Facturation"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_40 ----
          OBJ_40.setText("Pourcentage d'escompte");
          OBJ_40.setName("OBJ_40");
          p_recup.add(OBJ_40);
          OBJ_40.setBounds(30, 169, 154, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_30 ----
          OBJ_30.setText("Client factur\u00e9");
          OBJ_30.setName("OBJ_30");
          p_recup.add(OBJ_30);
          OBJ_30.setBounds(30, 134, 79, 20);

          //---- OBJ_54 ----
          OBJ_54.setText("Ech\u00e9ance");
          OBJ_54.setName("OBJ_54");
          p_recup.add(OBJ_54);
          OBJ_54.setBounds(405, 239, 64, 20);

          //---- OBJ_44 ----
          OBJ_44.setText("Remises");
          OBJ_44.setName("OBJ_44");
          p_recup.add(OBJ_44);
          OBJ_44.setBounds(405, 170, 57, 20);

          //---- OBJ_36 ----
          OBJ_36.setText("Devise");
          OBJ_36.setName("OBJ_36");
          p_recup.add(OBJ_36);
          OBJ_36.setBounds(500, 134, 65, 20);

          //---- CCCFPD ----
          CCCFPD.setComponentPopupMenu(BTD);
          CCCFPD.setName("CCCFPD");
          p_recup.add(CCCFPD);
          CCCFPD.setBounds(195, 130, 58, CCCFPD.getPreferredSize().height);

          //---- CCCFPF ----
          CCCFPF.setComponentPopupMenu(BTD);
          CCCFPF.setName("CCCFPF");
          p_recup.add(CCCFPF);
          CCCFPF.setBounds(330, 130, 58, CCCFPF.getPreferredSize().height);

          //---- OBJ_48 ----
          OBJ_48.setText("Mode");
          OBJ_48.setName("OBJ_48");
          p_recup.add(OBJ_48);
          OBJ_48.setBounds(30, 239, 39, 20);

          //---- OBJ_23 ----
          OBJ_23.setText("Type");
          OBJ_23.setName("OBJ_23");
          p_recup.add(OBJ_23);
          OBJ_23.setBounds(30, 34, 36, 20);

          //---- CCCFLD ----
          CCCFLD.setComponentPopupMenu(BTD);
          CCCFLD.setName("CCCFLD");
          p_recup.add(CCCFLD);
          CCCFLD.setBounds(255, 130, 34, CCCFLD.getPreferredSize().height);

          //---- CCCFLF ----
          CCCFLF.setComponentPopupMenu(BTD);
          CCCFLF.setName("CCCFLF");
          p_recup.add(CCCFLF);
          CCCFLF.setBounds(390, 130, 34, CCCFLF.getPreferredSize().height);

          //---- CCDEV1 ----
          CCDEV1.setComponentPopupMenu(BTD);
          CCDEV1.setName("CCDEV1");
          p_recup.add(CCDEV1);
          CCDEV1.setBounds(570, 130, 40, CCDEV1.getPreferredSize().height);

          //---- CCDEV2 ----
          CCDEV2.setComponentPopupMenu(BTD);
          CCDEV2.setName("CCDEV2");
          p_recup.add(CCDEV2);
          CCDEV2.setBounds(625, 130, 40, CCDEV2.getPreferredSize().height);

          //---- CCDEV3 ----
          CCDEV3.setComponentPopupMenu(BTD);
          CCDEV3.setName("CCDEV3");
          p_recup.add(CCDEV3);
          CCDEV3.setBounds(675, 130, 40, CCDEV3.getPreferredSize().height);

          //---- CCPESD ----
          CCPESD.setComponentPopupMenu(BTD);
          CCPESD.setName("CCPESD");
          p_recup.add(CCPESD);
          CCPESD.setBounds(195, 165, 42, CCPESD.getPreferredSize().height);

          //---- CCPESF ----
          CCPESF.setComponentPopupMenu(BTD);
          CCPESF.setName("CCPESF");
          p_recup.add(CCPESF);
          CCPESF.setBounds(274, 165, 42, CCPESF.getPreferredSize().height);

          //---- CCREMD ----
          CCREMD.setComponentPopupMenu(BTD);
          CCREMD.setName("CCREMD");
          p_recup.add(CCREMD);
          CCREMD.setBounds(500, 165, 26, CCREMD.getPreferredSize().height);

          //---- CCREMF ----
          CCREMF.setComponentPopupMenu(BTD);
          CCREMF.setName("CCREMF");
          p_recup.add(CCREMF);
          CCREMF.setBounds(570, 165, 26, CCREMF.getPreferredSize().height);

          //---- CCMRG1 ----
          CCMRG1.setComponentPopupMenu(BTD);
          CCMRG1.setName("CCMRG1");
          p_recup.add(CCMRG1);
          CCMRG1.setBounds(120, 235, 34, CCMRG1.getPreferredSize().height);

          //---- CCMRG2 ----
          CCMRG2.setComponentPopupMenu(BTD);
          CCMRG2.setName("CCMRG2");
          p_recup.add(CCMRG2);
          CCMRG2.setBounds(156, 235, 34, CCMRG2.getPreferredSize().height);

          //---- CCMRG3 ----
          CCMRG3.setComponentPopupMenu(BTD);
          CCMRG3.setName("CCMRG3");
          p_recup.add(CCMRG3);
          CCMRG3.setBounds(192, 235, 34, CCMRG3.getPreferredSize().height);

          //---- CCMRG4 ----
          CCMRG4.setComponentPopupMenu(BTD);
          CCMRG4.setName("CCMRG4");
          p_recup.add(CCMRG4);
          CCMRG4.setBounds(228, 235, 34, CCMRG4.getPreferredSize().height);

          //---- CCMRG5 ----
          CCMRG5.setComponentPopupMenu(BTD);
          CCMRG5.setName("CCMRG5");
          p_recup.add(CCMRG5);
          CCMRG5.setBounds(264, 235, 34, CCMRG5.getPreferredSize().height);

          //---- CCECH1 ----
          CCECH1.setComponentPopupMenu(BTD);
          CCECH1.setName("CCECH1");
          p_recup.add(CCECH1);
          CCECH1.setBounds(500, 235, 34, CCECH1.getPreferredSize().height);

          //---- CCECH2 ----
          CCECH2.setComponentPopupMenu(BTD);
          CCECH2.setName("CCECH2");
          p_recup.add(CCECH2);
          CCECH2.setBounds(536, 235, 34, CCECH2.getPreferredSize().height);

          //---- CCECH3 ----
          CCECH3.setComponentPopupMenu(BTD);
          CCECH3.setName("CCECH3");
          p_recup.add(CCECH3);
          CCECH3.setBounds(572, 235, 34, CCECH3.getPreferredSize().height);

          //---- CCECH4 ----
          CCECH4.setComponentPopupMenu(BTD);
          CCECH4.setName("CCECH4");
          p_recup.add(CCECH4);
          CCECH4.setBounds(608, 235, 34, CCECH4.getPreferredSize().height);

          //---- CCECH5 ----
          CCECH5.setComponentPopupMenu(BTD);
          CCECH5.setName("CCECH5");
          p_recup.add(CCECH5);
          CCECH5.setBounds(644, 235, 34, CCECH5.getPreferredSize().height);

          //---- CCTYPF ----
          CCTYPF.setComponentPopupMenu(BTD);
          CCTYPF.setName("CCTYPF");
          p_recup.add(CCTYPF);
          CCTYPF.setBounds(120, 30, 24, CCTYPF.getPreferredSize().height);

          //---- OBJ_33 ----
          OBJ_33.setText("\u00e0");
          OBJ_33.setName("OBJ_33");
          p_recup.add(OBJ_33);
          OBJ_33.setBounds(305, 135, 12, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("\u00e0");
          OBJ_42.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_42.setName("OBJ_42");
          p_recup.add(OBJ_42);
          OBJ_42.setBounds(238, 169, 35, 20);

          //---- OBJ_46 ----
          OBJ_46.setText("\u00e0");
          OBJ_46.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_46.setName("OBJ_46");
          p_recup.add(OBJ_46);
          OBJ_46.setBounds(528, 170, 40, 20);

          //---- separator1 ----
          separator1.setName("separator1");
          p_recup.add(separator1);
          separator1.setBounds(20, 65, 700, separator1.getPreferredSize().height);

          //---- separator2 ----
          separator2.setName("separator2");
          p_recup.add(separator2);
          separator2.setBounds(20, 205, 700, separator2.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setText("Facturation");
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(30, 88, 69, 20);

          //---- CCPERF ----
          CCPERF.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Jour",
            "Semaine",
            "D\u00e9cade",
            "Quinzaine",
            "Mois"
          }));
          CCPERF.setComponentPopupMenu(BTD);
          CCPERF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CCPERF.setName("CCPERF");
          p_recup.add(CCPERF);
          CCPERF.setBounds(195, 85, 125, CCPERF.getPreferredSize().height);

          //---- OBJ_29 ----
          OBJ_29.setText("Relev\u00e9");
          OBJ_29.setName("OBJ_29");
          p_recup.add(OBJ_29);
          OBJ_29.setBounds(500, 88, 65, 20);

          //---- CCPERR ----
          CCPERR.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Jour",
            "Semaine",
            "D\u00e9cade",
            "Quinzaine",
            "Mois"
          }));
          CCPERR.setComponentPopupMenu(BTD);
          CCPERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CCPERR.setName("CCPERR");
          p_recup.add(CCPERR);
          CCPERR.setBounds(570, 85, 125, CCPERR.getPreferredSize().height);

          //---- CCTYPT ----
          CCTYPT.setText("Facture TTC");
          CCTYPT.setComponentPopupMenu(BTD);
          CCTYPT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CCTYPT.setName("CCTYPT");
          p_recup.add(CCTYPT);
          CCTYPT.setBounds(195, 34, 100, 20);

          //---- separator3 ----
          separator3.setName("separator3");
          p_recup.add(separator3);
          separator3.setBounds(20, 120, 700, 4);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(15, 15, 740, 283);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_ann;
  private RiSousMenu_bt riSousMenu_bt_ann;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_40;
  private JPanel P_PnlOpts;
  private JLabel OBJ_30;
  private JLabel OBJ_54;
  private JLabel OBJ_44;
  private JLabel OBJ_36;
  private XRiTextField CCCFPD;
  private XRiTextField CCCFPF;
  private JLabel OBJ_48;
  private JLabel OBJ_23;
  private XRiTextField CCCFLD;
  private XRiTextField CCCFLF;
  private XRiTextField CCDEV1;
  private XRiTextField CCDEV2;
  private XRiTextField CCDEV3;
  private XRiTextField CCPESD;
  private XRiTextField CCPESF;
  private XRiTextField CCREMD;
  private XRiTextField CCREMF;
  private XRiTextField CCMRG1;
  private XRiTextField CCMRG2;
  private XRiTextField CCMRG3;
  private XRiTextField CCMRG4;
  private XRiTextField CCMRG5;
  private XRiTextField CCECH1;
  private XRiTextField CCECH2;
  private XRiTextField CCECH3;
  private XRiTextField CCECH4;
  private XRiTextField CCECH5;
  private XRiTextField CCTYPF;
  private JLabel OBJ_33;
  private JLabel OBJ_42;
  private JLabel OBJ_46;
  private JComponent separator1;
  private JComponent separator2;
  private JLabel OBJ_28;
  private XRiComboBox CCPERF;
  private JLabel OBJ_29;
  private XRiComboBox CCPERR;
  private XRiCheckBox CCTYPT;
  private JComponent separator3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
