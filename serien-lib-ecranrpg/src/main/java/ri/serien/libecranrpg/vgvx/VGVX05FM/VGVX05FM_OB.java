
package ri.serien.libecranrpg.vgvx.VGVX05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX05FM_OB extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVX05FM_OB(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Titre
    setTitle("Informations complémentaires");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OB185.setEnabled(lexique.isPresent("OB185"));
    OB184.setEnabled(lexique.isPresent("OB184"));
    OB183.setEnabled(lexique.isPresent("OB183"));
    OB182.setEnabled(lexique.isPresent("OB182"));
    OB181.setEnabled(lexique.isPresent("OB181"));
    OBJ_48_OBJ_48.setVisible(lexique.isPresent("OBER4"));
    OBJ_44_OBJ_44.setVisible(lexique.isPresent("OBER3"));
    OBJ_40_OBJ_40.setVisible(lexique.isPresent("OBER2"));
    OBJ_36_OBJ_36.setVisible(lexique.isPresent("OBER1"));
    OBZP4.setVisible(lexique.isPresent("OBZP4"));
    OBZP3.setVisible(lexique.isPresent("OBZP3"));
    OBZP2.setVisible(lexique.isPresent("OBZP2"));
    OBZP1.setVisible(lexique.isPresent("OBZP1"));
    WZP34.setVisible(lexique.isPresent("WZP34"));
    WZP33.setVisible(lexique.isPresent("WZP33"));
    WZP32.setVisible(lexique.isPresent("WZP32"));
    WZP31.setVisible(lexique.isPresent("WZP31"));
    OB171.setVisible(lexique.isPresent("OB171"));
    OB161.setVisible(lexique.isPresent("OB161"));
    OB151.setVisible(lexique.isPresent("OB151"));
    OB141.setVisible(lexique.isPresent("OB141"));
    OB13.setVisible(lexique.isPresent("OB13"));
    OB12.setVisible(lexique.isPresent("OB12"));
    OB11.setVisible(lexique.isPresent("OB11"));
    OB10.setVisible(lexique.isPresent("OB10"));
    OB9.setVisible(lexique.isPresent("OB9"));
    OB8.setVisible(lexique.isPresent("OB8"));
    OB7.setVisible(lexique.isPresent("OB7"));
    OB6.setVisible(lexique.isPresent("OB6"));
    OB5.setVisible(lexique.isPresent("OB5"));
    OB4.setVisible(lexique.isPresent("OB4"));
    OB3.setVisible(lexique.isPresent("OB3"));
    OB2.setVisible(lexique.isPresent("OB2"));
    OB1.setVisible(lexique.isPresent("OB1"));
    
    if (lexique.isTrue("05")) {
      OBJ_25_OBJ_25.setText("Article lié ou commentaire:");
      OBJ_26_OBJ_26.setVisible(true);
      OB17M.setVisible(true);
    }
    else {
      OBJ_25_OBJ_25.setText("Article lié ou commentaire à générer dans le bon:");
      OBJ_26_OBJ_26.setVisible(false);
      OB17M.setVisible(false);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OB1 = new XRiTextField();
    OB2 = new XRiTextField();
    OB3 = new XRiTextField();
    OB4 = new XRiTextField();
    OB5 = new XRiTextField();
    OB6 = new XRiTextField();
    OB7 = new XRiTextField();
    OB8 = new XRiTextField();
    OB9 = new XRiTextField();
    OB10 = new XRiTextField();
    OB11 = new XRiTextField();
    OB12 = new XRiTextField();
    OB13 = new XRiTextField();
    OBJ_56_OBJ_56 = new JLabel();
    OBJ_57_OBJ_57 = new JLabel();
    OBJ_58_OBJ_58 = new JLabel();
    OBJ_59_OBJ_59 = new JLabel();
    OBJ_60_OBJ_60 = new JLabel();
    OBJ_61_OBJ_61 = new JLabel();
    OBJ_62_OBJ_62 = new JLabel();
    OBJ_63_OBJ_63 = new JLabel();
    OBJ_64_OBJ_64 = new JLabel();
    OBJ_65_OBJ_65 = new JLabel();
    OBJ_66_OBJ_66 = new JLabel();
    OBJ_67_OBJ_67 = new JLabel();
    OBJ_68_OBJ_68 = new JLabel();
    panel3 = new JPanel();
    WZP31 = new XRiTextField();
    OB141 = new XRiTextField();
    OBZP1 = new XRiTextField();
    OBJ_36_OBJ_36 = new JLabel();
    WZP32 = new XRiTextField();
    OB151 = new XRiTextField();
    OBZP2 = new XRiTextField();
    OBJ_40_OBJ_40 = new JLabel();
    WZP33 = new XRiTextField();
    WZP34 = new XRiTextField();
    OB161 = new XRiTextField();
    OBZP3 = new XRiTextField();
    OBJ_44_OBJ_44 = new JLabel();
    OB171 = new XRiTextField();
    OBZP4 = new XRiTextField();
    OBJ_48_OBJ_48 = new JLabel();
    OB181 = new XRiTextField();
    OB182 = new XRiTextField();
    OB183 = new XRiTextField();
    OB184 = new XRiTextField();
    OB185 = new XRiTextField();
    OBJ_26_OBJ_26 = new JLabel();
    OB17M = new XRiTextField();
    OBJ_25_OBJ_25 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(860, 610));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt1 ----
              riMenu_bt1.setText("@V01F@");
              riMenu_bt1.setPreferredSize(new Dimension(167, 50));
              riMenu_bt1.setMinimumSize(new Dimension(167, 50));
              riMenu_bt1.setMaximumSize(new Dimension(170, 50));
              riMenu_bt1.setFont(riMenu_bt1.getFont().deriveFont(riMenu_bt1.getFont().getSize() + 2f));
              riMenu_bt1.setName("riMenu_bt1");
              riMenu_V01F.add(riMenu_bt1);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Bloc-notes"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OB1 ----
          OB1.setName("OB1");
          panel1.add(OB1);
          OB1.setBounds(45, 34, 610, OB1.getPreferredSize().height);

          //---- OB2 ----
          OB2.setName("OB2");
          panel1.add(OB2);
          OB2.setBounds(45, 59, 610, OB2.getPreferredSize().height);

          //---- OB3 ----
          OB3.setName("OB3");
          panel1.add(OB3);
          OB3.setBounds(45, 84, 610, OB3.getPreferredSize().height);

          //---- OB4 ----
          OB4.setName("OB4");
          panel1.add(OB4);
          OB4.setBounds(45, 109, 610, OB4.getPreferredSize().height);

          //---- OB5 ----
          OB5.setName("OB5");
          panel1.add(OB5);
          OB5.setBounds(45, 134, 610, OB5.getPreferredSize().height);

          //---- OB6 ----
          OB6.setName("OB6");
          panel1.add(OB6);
          OB6.setBounds(45, 159, 610, OB6.getPreferredSize().height);

          //---- OB7 ----
          OB7.setName("OB7");
          panel1.add(OB7);
          OB7.setBounds(45, 184, 610, OB7.getPreferredSize().height);

          //---- OB8 ----
          OB8.setName("OB8");
          panel1.add(OB8);
          OB8.setBounds(45, 209, 610, OB8.getPreferredSize().height);

          //---- OB9 ----
          OB9.setName("OB9");
          panel1.add(OB9);
          OB9.setBounds(45, 234, 610, OB9.getPreferredSize().height);

          //---- OB10 ----
          OB10.setName("OB10");
          panel1.add(OB10);
          OB10.setBounds(45, 259, 610, OB10.getPreferredSize().height);

          //---- OB11 ----
          OB11.setName("OB11");
          panel1.add(OB11);
          OB11.setBounds(45, 284, 610, OB11.getPreferredSize().height);

          //---- OB12 ----
          OB12.setName("OB12");
          panel1.add(OB12);
          OB12.setBounds(45, 309, 610, OB12.getPreferredSize().height);

          //---- OB13 ----
          OB13.setName("OB13");
          panel1.add(OB13);
          OB13.setBounds(45, 334, 610, OB13.getPreferredSize().height);

          //---- OBJ_56_OBJ_56 ----
          OBJ_56_OBJ_56.setText("1");
          OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
          panel1.add(OBJ_56_OBJ_56);
          OBJ_56_OBJ_56.setBounds(25, 40, 14, 16);

          //---- OBJ_57_OBJ_57 ----
          OBJ_57_OBJ_57.setText("13");
          OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
          panel1.add(OBJ_57_OBJ_57);
          OBJ_57_OBJ_57.setBounds(25, 340, 14, 16);

          //---- OBJ_58_OBJ_58 ----
          OBJ_58_OBJ_58.setText("2");
          OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");
          panel1.add(OBJ_58_OBJ_58);
          OBJ_58_OBJ_58.setBounds(25, 65, 14, 16);

          //---- OBJ_59_OBJ_59 ----
          OBJ_59_OBJ_59.setText("3");
          OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");
          panel1.add(OBJ_59_OBJ_59);
          OBJ_59_OBJ_59.setBounds(25, 90, 14, 16);

          //---- OBJ_60_OBJ_60 ----
          OBJ_60_OBJ_60.setText("4");
          OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");
          panel1.add(OBJ_60_OBJ_60);
          OBJ_60_OBJ_60.setBounds(25, 115, 14, 16);

          //---- OBJ_61_OBJ_61 ----
          OBJ_61_OBJ_61.setText("5");
          OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");
          panel1.add(OBJ_61_OBJ_61);
          OBJ_61_OBJ_61.setBounds(25, 140, 14, 16);

          //---- OBJ_62_OBJ_62 ----
          OBJ_62_OBJ_62.setText("6");
          OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");
          panel1.add(OBJ_62_OBJ_62);
          OBJ_62_OBJ_62.setBounds(25, 165, 14, 16);

          //---- OBJ_63_OBJ_63 ----
          OBJ_63_OBJ_63.setText("7");
          OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");
          panel1.add(OBJ_63_OBJ_63);
          OBJ_63_OBJ_63.setBounds(25, 190, 14, 16);

          //---- OBJ_64_OBJ_64 ----
          OBJ_64_OBJ_64.setText("8");
          OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");
          panel1.add(OBJ_64_OBJ_64);
          OBJ_64_OBJ_64.setBounds(25, 215, 14, 16);

          //---- OBJ_65_OBJ_65 ----
          OBJ_65_OBJ_65.setText("9");
          OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");
          panel1.add(OBJ_65_OBJ_65);
          OBJ_65_OBJ_65.setBounds(25, 240, 14, 16);

          //---- OBJ_66_OBJ_66 ----
          OBJ_66_OBJ_66.setText("10");
          OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");
          panel1.add(OBJ_66_OBJ_66);
          OBJ_66_OBJ_66.setBounds(25, 265, 14, 16);

          //---- OBJ_67_OBJ_67 ----
          OBJ_67_OBJ_67.setText("11");
          OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");
          panel1.add(OBJ_67_OBJ_67);
          OBJ_67_OBJ_67.setBounds(25, 290, 14, 16);

          //---- OBJ_68_OBJ_68 ----
          OBJ_68_OBJ_68.setText("12");
          OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");
          panel1.add(OBJ_68_OBJ_68);
          OBJ_68_OBJ_68.setBounds(25, 315, 14, 16);
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 5, 680, 380);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Informations compl\u00e9mentaires"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- WZP31 ----
          WZP31.setName("WZP31");
          panel3.add(WZP31);
          WZP31.setBounds(45, 35, 262, WZP31.getPreferredSize().height);

          //---- OB141 ----
          OB141.setComponentPopupMenu(BTD);
          OB141.setName("OB141");
          panel3.add(OB141);
          OB141.setBounds(345, 35, 310, OB141.getPreferredSize().height);

          //---- OBZP1 ----
          OBZP1.setComponentPopupMenu(BTD);
          OBZP1.setName("OBZP1");
          panel3.add(OBZP1);
          OBZP1.setBounds(345, 35, 160, OBZP1.getPreferredSize().height);

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("?OB.OBJ_36_OBJ_36.text_2?");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");
          panel3.add(OBJ_36_OBJ_36);
          OBJ_36_OBJ_36.setBounds(495, 40, 157, 18);

          //---- WZP32 ----
          WZP32.setName("WZP32");
          panel3.add(WZP32);
          WZP32.setBounds(45, 60, 262, WZP32.getPreferredSize().height);

          //---- OB151 ----
          OB151.setComponentPopupMenu(BTD);
          OB151.setName("OB151");
          panel3.add(OB151);
          OB151.setBounds(345, 60, 310, OB151.getPreferredSize().height);

          //---- OBZP2 ----
          OBZP2.setComponentPopupMenu(BTD);
          OBZP2.setName("OBZP2");
          panel3.add(OBZP2);
          OBZP2.setBounds(345, 60, 160, OBZP2.getPreferredSize().height);

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("?OB.OBJ_40_OBJ_40.text_2?");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
          panel3.add(OBJ_40_OBJ_40);
          OBJ_40_OBJ_40.setBounds(495, 65, 157, 18);

          //---- WZP33 ----
          WZP33.setName("WZP33");
          panel3.add(WZP33);
          WZP33.setBounds(45, 85, 262, WZP33.getPreferredSize().height);

          //---- WZP34 ----
          WZP34.setName("WZP34");
          panel3.add(WZP34);
          WZP34.setBounds(45, 110, 262, WZP34.getPreferredSize().height);

          //---- OB161 ----
          OB161.setComponentPopupMenu(BTD);
          OB161.setName("OB161");
          panel3.add(OB161);
          OB161.setBounds(345, 85, 310, OB161.getPreferredSize().height);

          //---- OBZP3 ----
          OBZP3.setComponentPopupMenu(BTD);
          OBZP3.setName("OBZP3");
          panel3.add(OBZP3);
          OBZP3.setBounds(345, 85, 160, OBZP3.getPreferredSize().height);

          //---- OBJ_44_OBJ_44 ----
          OBJ_44_OBJ_44.setText("?OB.OBJ_44_OBJ_44.text_2?");
          OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
          panel3.add(OBJ_44_OBJ_44);
          OBJ_44_OBJ_44.setBounds(495, 90, 157, 18);

          //---- OB171 ----
          OB171.setComponentPopupMenu(BTD);
          OB171.setName("OB171");
          panel3.add(OB171);
          OB171.setBounds(345, 110, 310, OB171.getPreferredSize().height);

          //---- OBZP4 ----
          OBZP4.setComponentPopupMenu(BTD);
          OBZP4.setName("OBZP4");
          panel3.add(OBZP4);
          OBZP4.setBounds(345, 110, 160, OBZP4.getPreferredSize().height);

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("?OB.OBJ_48_OBJ_48.text_2?");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
          panel3.add(OBJ_48_OBJ_48);
          OBJ_48_OBJ_48.setBounds(495, 115, 157, 18);
        }
        p_contenu.add(panel3);
        panel3.setBounds(5, 385, 680, 160);

        //---- OB181 ----
        OB181.setComponentPopupMenu(BTD);
        OB181.setName("OB181");
        p_contenu.add(OB181);
        OB181.setBounds(50, 570, 110, OB181.getPreferredSize().height);

        //---- OB182 ----
        OB182.setComponentPopupMenu(BTD);
        OB182.setName("OB182");
        p_contenu.add(OB182);
        OB182.setBounds(173, 570, 110, OB182.getPreferredSize().height);

        //---- OB183 ----
        OB183.setComponentPopupMenu(BTD);
        OB183.setName("OB183");
        p_contenu.add(OB183);
        OB183.setBounds(296, 570, 110, OB183.getPreferredSize().height);

        //---- OB184 ----
        OB184.setComponentPopupMenu(BTD);
        OB184.setName("OB184");
        p_contenu.add(OB184);
        OB184.setBounds(419, 570, 110, OB184.getPreferredSize().height);

        //---- OB185 ----
        OB185.setComponentPopupMenu(BTD);
        OB185.setName("OB185");
        p_contenu.add(OB185);
        OB185.setBounds(542, 570, 110, OB185.getPreferredSize().height);

        //---- OBJ_26_OBJ_26 ----
        OBJ_26_OBJ_26.setText("Maintenance");
        OBJ_26_OBJ_26.setName("OBJ_26_OBJ_26");
        p_contenu.add(OBJ_26_OBJ_26);
        OBJ_26_OBJ_26.setBounds(419, 550, 80, 20);

        //---- OB17M ----
        OB17M.setComponentPopupMenu(BTD);
        OB17M.setName("OB17M");
        p_contenu.add(OB17M);
        OB17M.setBounds(542, 545, 110, OB17M.getPreferredSize().height);

        //---- OBJ_25_OBJ_25 ----
        OBJ_25_OBJ_25.setName("OBJ_25_OBJ_25");
        p_contenu.add(OBJ_25_OBJ_25);
        OBJ_25_OBJ_25.setBounds(50, 550, 320, 20);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_6 ----
      OBJ_6.setText("?OB.OBJ_6.text_2?");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD.add(OBJ_6);

      //---- OBJ_5 ----
      OBJ_5.setText("?OB.OBJ_5.text_2?");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField OB1;
  private XRiTextField OB2;
  private XRiTextField OB3;
  private XRiTextField OB4;
  private XRiTextField OB5;
  private XRiTextField OB6;
  private XRiTextField OB7;
  private XRiTextField OB8;
  private XRiTextField OB9;
  private XRiTextField OB10;
  private XRiTextField OB11;
  private XRiTextField OB12;
  private XRiTextField OB13;
  private JLabel OBJ_56_OBJ_56;
  private JLabel OBJ_57_OBJ_57;
  private JLabel OBJ_58_OBJ_58;
  private JLabel OBJ_59_OBJ_59;
  private JLabel OBJ_60_OBJ_60;
  private JLabel OBJ_61_OBJ_61;
  private JLabel OBJ_62_OBJ_62;
  private JLabel OBJ_63_OBJ_63;
  private JLabel OBJ_64_OBJ_64;
  private JLabel OBJ_65_OBJ_65;
  private JLabel OBJ_66_OBJ_66;
  private JLabel OBJ_67_OBJ_67;
  private JLabel OBJ_68_OBJ_68;
  private JPanel panel3;
  private XRiTextField WZP31;
  private XRiTextField OB141;
  private XRiTextField OBZP1;
  private JLabel OBJ_36_OBJ_36;
  private XRiTextField WZP32;
  private XRiTextField OB151;
  private XRiTextField OBZP2;
  private JLabel OBJ_40_OBJ_40;
  private XRiTextField WZP33;
  private XRiTextField WZP34;
  private XRiTextField OB161;
  private XRiTextField OBZP3;
  private JLabel OBJ_44_OBJ_44;
  private XRiTextField OB171;
  private XRiTextField OBZP4;
  private JLabel OBJ_48_OBJ_48;
  private XRiTextField OB181;
  private XRiTextField OB182;
  private XRiTextField OB183;
  private XRiTextField OB184;
  private XRiTextField OB185;
  private JLabel OBJ_26_OBJ_26;
  private XRiTextField OB17M;
  private JLabel OBJ_25_OBJ_25;
  private JPopupMenu BTD;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
