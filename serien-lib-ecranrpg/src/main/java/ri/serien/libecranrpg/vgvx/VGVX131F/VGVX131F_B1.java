
package ri.serien.libecranrpg.vgvx.VGVX131F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;

/**
 * @author Stéphane Vénéri
 */
public class VGVX131F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] TSOR_Value = { "", "V", "S", "C", };
  private String[] TENT_Value = { "", "A", "S", "C", };
  
  public VGVX131F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TSOR.setValeurs(TSOR_Value, null);
    TENT.setValeurs(TENT_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    QENTX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QENTX@")).trim());
    QSORX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QSORX@")).trim());
    QCMJX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QCMJX@")).trim());
    STKMX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@STKMX@")).trim());
    PENT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PENT@")).trim());
    PSOR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PSOR@")).trim());
    KROT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@KROT@")).trim());
    NBENT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBENT@")).trim());
    NBSOR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBSOR@")).trim());
    NBRJ.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBRJ@")).trim());
    NBRM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBRM@")).trim());
    NBGE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBGE@")).trim());
    NBGS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBGS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // TSOR.setSelectedIndex(getIndice("TSOR", TSOR_Value));
    // TENT.setSelectedIndex(getIndice("TENT", TENT_Value));
    
    NBGS.setVisible(lexique.isPresent("NBGS"));
    NBGE.setVisible(lexique.isPresent("NBGE"));
    NBRM.setEnabled(lexique.isPresent("NBRM"));
    NBRJ.setEnabled(lexique.isPresent("NBRJ"));
    NBSOR.setEnabled(lexique.isPresent("NBSOR"));
    NBENT.setEnabled(lexique.isPresent("NBENT"));
    // PERFX.setEnabled( lexique.isPresent("PERFX"));
    // PERDX.setEnabled( lexique.isPresent("PERDX"));
    KROT.setVisible(lexique.isPresent("KROT"));
    PSOR.setVisible(lexique.isPresent("PSOR"));
    PENT.setVisible(lexique.isPresent("PENT"));
    STKMX.setEnabled(lexique.isPresent("STKMX"));
    QCMJX.setEnabled(lexique.isPresent("QCMJX"));
    QSORX.setEnabled(lexique.isPresent("QSORX"));
    QENTX.setEnabled(lexique.isPresent("QENTX"));
    // TSOR.setEnabled( lexique.isPresent("TSOR"));
    // TENT.setEnabled( lexique.isPresent("TENT"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Analyse des mouvements sur période"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("TSOR", 0, TSOR_Value[TSOR.getSelectedIndex()]);
    // lexique.HostFieldPutData("TENT", 0, TENT_Value[TENT.getSelectedIndex()]);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    TENT = new XRiComboBox();
    TSOR = new XRiComboBox();
    OBJ_49 = new JLabel();
    PERDX = new XRiCalendrier();
    PERFX = new XRiCalendrier();
    OBJ_26 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_29 = new JLabel();
    panel1 = new JPanel();
    OBJ_33 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_31 = new JLabel();
    QENTX = new RiZoneSortie();
    QSORX = new RiZoneSortie();
    QCMJX = new RiZoneSortie();
    STKMX = new RiZoneSortie();
    OBJ_35 = new JLabel();
    PENT = new RiZoneSortie();
    PSOR = new RiZoneSortie();
    OBJ_50 = new JLabel();
    OBJ_53 = new JLabel();
    KROT = new RiZoneSortie();
    NBENT = new RiZoneSortie();
    NBSOR = new RiZoneSortie();
    NBRJ = new RiZoneSortie();
    NBRM = new RiZoneSortie();
    NBGE = new RiZoneSortie();
    NBGS = new RiZoneSortie();
    OBJ_52 = new JLabel();
    OBJ_54 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1040, 255));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- TENT ----
          TENT.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Achat",
            "Bordereau de stock",
            "Fabrication"
          }));
          TENT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TENT.setName("TENT");
          panel2.add(TENT);
          TENT.setBounds(315, 47, 190, TENT.getPreferredSize().height);

          //---- TSOR ----
          TSOR.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Vente",
            "Bordereau de stock",
            "Fabrication"
          }));
          TSOR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TSOR.setName("TSOR");
          panel2.add(TSOR);
          TSOR.setBounds(615, 47, 190, TSOR.getPreferredSize().height);

          //---- OBJ_49 ----
          OBJ_49.setText("Types de mouvements");
          OBJ_49.setName("OBJ_49");
          panel2.add(OBJ_49);
          OBJ_49.setBounds(25, 50, 141, 20);

          //---- PERDX ----
          PERDX.setName("PERDX");
          panel2.add(PERDX);
          PERDX.setBounds(315, 16, 105, PERDX.getPreferredSize().height);

          //---- PERFX ----
          PERFX.setName("PERFX");
          panel2.add(PERFX);
          PERFX.setBounds(615, 16, 105, PERFX.getPreferredSize().height);

          //---- OBJ_26 ----
          OBJ_26.setText("P\u00e9riode");
          OBJ_26.setName("OBJ_26");
          panel2.add(OBJ_26);
          OBJ_26.setBounds(25, 20, 55, 20);

          //---- OBJ_61 ----
          OBJ_61.setText("Entr\u00e9es");
          OBJ_61.setName("OBJ_61");
          panel2.add(OBJ_61);
          OBJ_61.setBounds(215, 50, 45, 20);

          //---- OBJ_62 ----
          OBJ_62.setText("Sorties");
          OBJ_62.setName("OBJ_62");
          panel2.add(OBJ_62);
          OBJ_62.setBounds(540, 50, 40, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("du");
          OBJ_27.setName("OBJ_27");
          panel2.add(OBJ_27);
          OBJ_27.setBounds(245, 20, 18, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("au");
          OBJ_29.setName("OBJ_29");
          panel2.add(OBJ_29);
          OBJ_29.setBounds(540, 20, 18, 20);
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 850, 90);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_33 ----
          OBJ_33.setText("Nombre de mouvements");
          OBJ_33.setName("OBJ_33");
          panel1.add(OBJ_33);
          OBJ_33.setBounds(305, 15, 140, 20);

          //---- OBJ_37 ----
          OBJ_37.setText("Nombre de mouvements");
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(305, 45, 140, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("Nombre de jours");
          OBJ_41.setName("OBJ_41");
          panel1.add(OBJ_41);
          OBJ_41.setBounds(305, 75, 140, 20);

          //---- OBJ_45 ----
          OBJ_45.setText("Nombre de mouvements");
          OBJ_45.setName("OBJ_45");
          panel1.add(OBJ_45);
          OBJ_45.setBounds(305, 105, 140, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("Conso.moyenne/jour");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(15, 75, 127, 20);

          //---- OBJ_55 ----
          OBJ_55.setText("Rotation sur p\u00e9riode");
          OBJ_55.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_55.setName("OBJ_55");
          panel1.add(OBJ_55);
          OBJ_55.setBounds(610, 105, 130, 20);

          //---- OBJ_43 ----
          OBJ_43.setText("Stock moyen");
          OBJ_43.setName("OBJ_43");
          panel1.add(OBJ_43);
          OBJ_43.setBounds(15, 105, 99, 20);

          //---- OBJ_31 ----
          OBJ_31.setText("Quantit\u00e9 entr\u00e9e");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(15, 15, 93, 20);

          //---- QENTX ----
          QENTX.setText("@QENTX@");
          QENTX.setBorder(new BevelBorder(BevelBorder.LOWERED));
          QENTX.setHorizontalAlignment(SwingConstants.RIGHT);
          QENTX.setName("QENTX");
          panel1.add(QENTX);
          QENTX.setBounds(160, 13, 93, 24);

          //---- QSORX ----
          QSORX.setHorizontalAlignment(SwingConstants.RIGHT);
          QSORX.setBorder(new BevelBorder(BevelBorder.LOWERED));
          QSORX.setText("@QSORX@");
          QSORX.setName("QSORX");
          panel1.add(QSORX);
          QSORX.setBounds(160, 43, 93, 24);

          //---- QCMJX ----
          QCMJX.setHorizontalAlignment(SwingConstants.RIGHT);
          QCMJX.setBorder(new BevelBorder(BevelBorder.LOWERED));
          QCMJX.setText("@QCMJX@");
          QCMJX.setName("QCMJX");
          panel1.add(QCMJX);
          QCMJX.setBounds(160, 73, 93, 24);

          //---- STKMX ----
          STKMX.setHorizontalAlignment(SwingConstants.RIGHT);
          STKMX.setBorder(new BevelBorder(BevelBorder.LOWERED));
          STKMX.setText("@STKMX@");
          STKMX.setName("STKMX");
          panel1.add(STKMX);
          STKMX.setBounds(160, 103, 93, 24);

          //---- OBJ_35 ----
          OBJ_35.setText("Quantit\u00e9 sortie");
          OBJ_35.setName("OBJ_35");
          panel1.add(OBJ_35);
          OBJ_35.setBounds(15, 45, 88, 20);

          //---- PENT ----
          PENT.setBorder(new BevelBorder(BevelBorder.LOWERED));
          PENT.setText("@PENT@");
          PENT.setHorizontalAlignment(SwingConstants.RIGHT);
          PENT.setName("PENT");
          panel1.add(PENT);
          PENT.setBounds(605, 13, 90, 24);

          //---- PSOR ----
          PSOR.setBorder(new BevelBorder(BevelBorder.LOWERED));
          PSOR.setText("@PSOR@");
          PSOR.setHorizontalAlignment(SwingConstants.RIGHT);
          PSOR.setName("PSOR");
          panel1.add(PSOR);
          PSOR.setBounds(605, 43, 90, 24);

          //---- OBJ_50 ----
          OBJ_50.setText("Prix moyen");
          OBJ_50.setName("OBJ_50");
          panel1.add(OBJ_50);
          OBJ_50.setBounds(530, 15, 69, 20);

          //---- OBJ_53 ----
          OBJ_53.setText("Prix moyen");
          OBJ_53.setName("OBJ_53");
          panel1.add(OBJ_53);
          OBJ_53.setBounds(530, 45, 69, 20);

          //---- KROT ----
          KROT.setBorder(new BevelBorder(BevelBorder.LOWERED));
          KROT.setText("@KROT@");
          KROT.setHorizontalAlignment(SwingConstants.RIGHT);
          KROT.setName("KROT");
          panel1.add(KROT);
          KROT.setBounds(755, 103, 58, 24);

          //---- NBENT ----
          NBENT.setBorder(new BevelBorder(BevelBorder.LOWERED));
          NBENT.setText("@NBENT@");
          NBENT.setHorizontalAlignment(SwingConstants.RIGHT);
          NBENT.setName("NBENT");
          panel1.add(NBENT);
          NBENT.setBounds(455, 13, 42, 24);

          //---- NBSOR ----
          NBSOR.setBorder(new BevelBorder(BevelBorder.LOWERED));
          NBSOR.setText("@NBSOR@");
          NBSOR.setHorizontalAlignment(SwingConstants.RIGHT);
          NBSOR.setName("NBSOR");
          panel1.add(NBSOR);
          NBSOR.setBounds(455, 43, 42, 24);

          //---- NBRJ ----
          NBRJ.setBorder(new BevelBorder(BevelBorder.LOWERED));
          NBRJ.setText("@NBRJ@");
          NBRJ.setHorizontalAlignment(SwingConstants.RIGHT);
          NBRJ.setName("NBRJ");
          panel1.add(NBRJ);
          NBRJ.setBounds(455, 73, 42, 24);

          //---- NBRM ----
          NBRM.setBorder(new BevelBorder(BevelBorder.LOWERED));
          NBRM.setText("@NBRM@");
          NBRM.setHorizontalAlignment(SwingConstants.RIGHT);
          NBRM.setName("NBRM");
          panel1.add(NBRM);
          NBRM.setBounds(455, 103, 42, 24);

          //---- NBGE ----
          NBGE.setBorder(new BevelBorder(BevelBorder.LOWERED));
          NBGE.setText("@NBGE@");
          NBGE.setHorizontalAlignment(SwingConstants.RIGHT);
          NBGE.setName("NBGE");
          panel1.add(NBGE);
          NBGE.setBounds(755, 13, 58, 24);

          //---- NBGS ----
          NBGS.setBorder(new BevelBorder(BevelBorder.LOWERED));
          NBGS.setText("@NBGS@");
          NBGS.setHorizontalAlignment(SwingConstants.RIGHT);
          NBGS.setName("NBGS");
          panel1.add(NBGS);
          NBGS.setBounds(755, 43, 58, 24);

          //---- OBJ_52 ----
          OBJ_52.setText("NbG");
          OBJ_52.setName("OBJ_52");
          panel1.add(OBJ_52);
          OBJ_52.setBounds(715, 15, 31, 20);

          //---- OBJ_54 ----
          OBJ_54.setText("NbG");
          OBJ_54.setName("OBJ_54");
          panel1.add(OBJ_54);
          OBJ_54.setBounds(715, 45, 31, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 105, 850, 140);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiComboBox TENT;
  private XRiComboBox TSOR;
  private JLabel OBJ_49;
  private XRiCalendrier PERDX;
  private XRiCalendrier PERFX;
  private JLabel OBJ_26;
  private JLabel OBJ_61;
  private JLabel OBJ_62;
  private JLabel OBJ_27;
  private JLabel OBJ_29;
  private JPanel panel1;
  private JLabel OBJ_33;
  private JLabel OBJ_37;
  private JLabel OBJ_41;
  private JLabel OBJ_45;
  private JLabel OBJ_39;
  private JLabel OBJ_55;
  private JLabel OBJ_43;
  private JLabel OBJ_31;
  private RiZoneSortie QENTX;
  private RiZoneSortie QSORX;
  private RiZoneSortie QCMJX;
  private RiZoneSortie STKMX;
  private JLabel OBJ_35;
  private RiZoneSortie PENT;
  private RiZoneSortie PSOR;
  private JLabel OBJ_50;
  private JLabel OBJ_53;
  private RiZoneSortie KROT;
  private RiZoneSortie NBENT;
  private RiZoneSortie NBSOR;
  private RiZoneSortie NBRJ;
  private RiZoneSortie NBRM;
  private RiZoneSortie NBGE;
  private RiZoneSortie NBGS;
  private JLabel OBJ_52;
  private JLabel OBJ_54;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
