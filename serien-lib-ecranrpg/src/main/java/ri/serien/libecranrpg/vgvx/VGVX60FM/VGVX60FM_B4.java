
package ri.serien.libecranrpg.vgvx.VGVX60FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX60FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGVX60FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAG@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    SUF03.setEnabled(lexique.isPresent("SUF03"));
    SUF02.setEnabled(lexique.isPresent("SUF02"));
    OBJ_48.setVisible(lexique.isPresent("ETA02"));
    OBJ_37.setVisible(lexique.isPresent("ETA02"));
    NLI03.setEnabled(lexique.isPresent("NLI03"));
    NLI02.setEnabled(lexique.isPresent("NLI02"));
    ETA03.setEnabled(lexique.isPresent("ETA03"));
    ETA02.setEnabled(lexique.isPresent("ETA02"));
    NUM03.setEnabled(lexique.isPresent("NUM03"));
    NUM02.setEnabled(lexique.isPresent("NUM02"));
    OBJ_12.setVisible(!lexique.HostFieldGetData("WMAG").trim().equalsIgnoreCase(""));
    // DLX03.setEnabled( lexique.isPresent("DLX03"));
    // DLX02.setEnabled( lexique.isPresent("DLX02"));
    OBJ_50.setEnabled(lexique.isPresent("WTP03"));
    OBJ_49.setEnabled(lexique.isPresent("WTP03"));
    OBJ_39.setEnabled(lexique.isPresent("WTP02"));
    OBJ_38.setEnabled(lexique.isPresent("WTP02"));
    OBJ_17.setEnabled(lexique.isPresent("WTP01"));
    OBJ_16.setEnabled(lexique.isPresent("WTP01"));
    QTXNA.setVisible(lexique.isPresent("QTXNA"));
    QTX03.setEnabled(lexique.isPresent("QTX03"));
    QTX02.setEnabled(lexique.isPresent("QTX02"));
    QTX01.setEnabled(lexique.isPresent("QTX01"));
    OBJ_41.setVisible(lexique.isPresent("WTP03"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Affectation ligne de commande"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 0, "4");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_38ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP02", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_39ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP02", 0, "4");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_49ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP03", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_50ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP03", 0, "4");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_16 = new SNBoutonLeger();
    OBJ_17 = new SNBoutonLeger();
    OBJ_18 = new JLabel();
    QTX01 = new XRiTextField();
    panel2 = new JPanel();
    OBJ_38 = new SNBoutonLeger();
    OBJ_39 = new SNBoutonLeger();
    OBJ_34 = new JLabel();
    QTX02 = new XRiTextField();
    NUM02 = new XRiTextField();
    SUF02 = new XRiTextField();
    NLI02 = new XRiTextField();
    OBJ_35 = new JLabel();
    OBJ_36 = new JLabel();
    DLX02 = new XRiCalendrier();
    ETA02 = new XRiTextField();
    OBJ_37 = new JLabel();
    OBJ_12 = new JLabel();
    OBJ_13 = new JLabel();
    panel3 = new JPanel();
    OBJ_49 = new SNBoutonLeger();
    OBJ_50 = new SNBoutonLeger();
    OBJ_45 = new JLabel();
    QTX03 = new XRiTextField();
    OBJ_46 = new JLabel();
    NUM03 = new XRiTextField();
    SUF03 = new XRiTextField();
    NLI03 = new XRiTextField();
    OBJ_47 = new JLabel();
    OBJ_48 = new JLabel();
    ETA03 = new XRiTextField();
    DLX03 = new XRiCalendrier();
    panel4 = new JPanel();
    OBJ_41 = new JLabel();
    QTXNA = new XRiTextField();
    OBJ_15 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_33 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(865, 420));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Affectation sur Stock Disponible"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_16 ----
          OBJ_16.setText("D\u00e9tail");
          OBJ_16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_16.setName("OBJ_16");
          OBJ_16.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_16ActionPerformed(e);
            }
          });
          panel1.add(OBJ_16);
          OBJ_16.setBounds(25, 37, 65, OBJ_16.getPreferredSize().height);

          //---- OBJ_17 ----
          OBJ_17.setText("Annulation");
          OBJ_17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_17.setName("OBJ_17");
          OBJ_17.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_17ActionPerformed(e);
            }
          });
          panel1.add(OBJ_17);
          OBJ_17.setBounds(95, 37, 100, OBJ_17.getPreferredSize().height);

          //---- OBJ_18 ----
          OBJ_18.setText("Quantit\u00e9 affect\u00e9e");
          OBJ_18.setName("OBJ_18");
          panel1.add(OBJ_18);
          OBJ_18.setBounds(212, 39, 125, 20);

          //---- QTX01 ----
          QTX01.setName("QTX01");
          panel1.add(QTX01);
          QTX01.setBounds(340, 35, 114, QTX01.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Affectation sur Attendu"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_38 ----
          OBJ_38.setText("D\u00e9tail");
          OBJ_38.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_38.setName("OBJ_38");
          OBJ_38.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_38ActionPerformed(e);
            }
          });
          panel2.add(OBJ_38);
          OBJ_38.setBounds(25, 45, 65, OBJ_38.getPreferredSize().height);

          //---- OBJ_39 ----
          OBJ_39.setText("Annulation");
          OBJ_39.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_39.setName("OBJ_39");
          OBJ_39.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_39ActionPerformed(e);
            }
          });
          panel2.add(OBJ_39);
          OBJ_39.setBounds(95, 45, 100, OBJ_39.getPreferredSize().height);

          //---- OBJ_34 ----
          OBJ_34.setText("Quantit\u00e9 affect\u00e9e");
          OBJ_34.setName("OBJ_34");
          panel2.add(OBJ_34);
          OBJ_34.setBounds(212, 25, 103, 20);

          //---- QTX02 ----
          QTX02.setName("QTX02");
          panel2.add(QTX02);
          QTX02.setBounds(210, 45, 114, QTX02.getPreferredSize().height);

          //---- NUM02 ----
          NUM02.setName("NUM02");
          panel2.add(NUM02);
          NUM02.setBounds(340, 45, 58, NUM02.getPreferredSize().height);

          //---- SUF02 ----
          SUF02.setName("SUF02");
          panel2.add(SUF02);
          SUF02.setBounds(400, 45, 18, SUF02.getPreferredSize().height);

          //---- NLI02 ----
          NLI02.setName("NLI02");
          panel2.add(NLI02);
          NLI02.setBounds(420, 45, 42, NLI02.getPreferredSize().height);

          //---- OBJ_35 ----
          OBJ_35.setText("Ligne d'achat");
          OBJ_35.setName("OBJ_35");
          panel2.add(OBJ_35);
          OBJ_35.setBounds(342, 25, 87, 20);

          //---- OBJ_36 ----
          OBJ_36.setText("Livraison");
          OBJ_36.setName("OBJ_36");
          panel2.add(OBJ_36);
          OBJ_36.setBounds(472, 25, 55, 21);

          //---- DLX02 ----
          DLX02.setName("DLX02");
          panel2.add(DLX02);
          DLX02.setBounds(470, 45, 105, DLX02.getPreferredSize().height);

          //---- ETA02 ----
          ETA02.setName("ETA02");
          panel2.add(ETA02);
          ETA02.setBounds(590, 45, 42, ETA02.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("Etat");
          OBJ_37.setName("OBJ_37");
          panel2.add(OBJ_37);
          OBJ_37.setBounds(595, 25, 35, 21);

          //---- OBJ_12 ----
          OBJ_12.setText("Magasin");
          OBJ_12.setName("OBJ_12");
          panel2.add(OBJ_12);
          OBJ_12.setBounds(265, 0, 52, 19);

          //---- OBJ_13 ----
          OBJ_13.setText("@WMAG@");
          OBJ_13.setName("OBJ_13");
          panel2.add(OBJ_13);
          OBJ_13.setBounds(360, -1, 43, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Affectation type GBA (G\u00e9n\u00e9ration bon d'achat)"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- OBJ_49 ----
          OBJ_49.setText("D\u00e9tail");
          OBJ_49.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_49.setName("OBJ_49");
          OBJ_49.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_49ActionPerformed(e);
            }
          });
          panel3.add(OBJ_49);
          OBJ_49.setBounds(25, 45, 65, OBJ_49.getPreferredSize().height);

          //---- OBJ_50 ----
          OBJ_50.setText("Annulation");
          OBJ_50.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_50.setName("OBJ_50");
          OBJ_50.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_50ActionPerformed(e);
            }
          });
          panel3.add(OBJ_50);
          OBJ_50.setBounds(95, 45, 100, OBJ_50.getPreferredSize().height);

          //---- OBJ_45 ----
          OBJ_45.setText("Quantit\u00e9 affect\u00e9e");
          OBJ_45.setName("OBJ_45");
          panel3.add(OBJ_45);
          OBJ_45.setBounds(212, 25, 103, 20);

          //---- QTX03 ----
          QTX03.setName("QTX03");
          panel3.add(QTX03);
          QTX03.setBounds(210, 45, 114, QTX03.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Ligne d'achat");
          OBJ_46.setName("OBJ_46");
          panel3.add(OBJ_46);
          OBJ_46.setBounds(342, 25, 87, 20);

          //---- NUM03 ----
          NUM03.setName("NUM03");
          panel3.add(NUM03);
          NUM03.setBounds(340, 45, 58, NUM03.getPreferredSize().height);

          //---- SUF03 ----
          SUF03.setName("SUF03");
          panel3.add(SUF03);
          SUF03.setBounds(400, 45, 18, SUF03.getPreferredSize().height);

          //---- NLI03 ----
          NLI03.setName("NLI03");
          panel3.add(NLI03);
          NLI03.setBounds(420, 45, 42, NLI03.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setText("Livraison");
          OBJ_47.setName("OBJ_47");
          panel3.add(OBJ_47);
          OBJ_47.setBounds(472, 25, 55, 20);

          //---- OBJ_48 ----
          OBJ_48.setText("Etat");
          OBJ_48.setName("OBJ_48");
          panel3.add(OBJ_48);
          OBJ_48.setBounds(595, 25, 27, 20);

          //---- ETA03 ----
          ETA03.setName("ETA03");
          panel3.add(ETA03);
          ETA03.setBounds(590, 45, 42, ETA03.getPreferredSize().height);

          //---- DLX03 ----
          DLX03.setName("DLX03");
          panel3.add(DLX03);
          DLX03.setBounds(470, 45, 105, DLX03.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }

        //======== panel4 ========
        {
          panel4.setBorder(new TitledBorder(""));
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);

          //---- OBJ_41 ----
          OBJ_41.setText("Quantit\u00e9 non affect\u00e9e");
          OBJ_41.setName("OBJ_41");
          panel4.add(OBJ_41);
          OBJ_41.setBounds(25, 30, 128, 20);

          //---- QTXNA ----
          QTXNA.setName("QTXNA");
          panel4.add(QTXNA);
          QTXNA.setBounds(210, 25, 114, QTXNA.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 665, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 665, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 665, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 665, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- OBJ_15 ----
    OBJ_15.setText("Affectation sur Stock Disponible");
    OBJ_15.setName("OBJ_15");

    //---- OBJ_44 ----
    OBJ_44.setText("Affectation type GBA (G\u00e9n\u00e9ration bon d'achat)");
    OBJ_44.setName("OBJ_44");

    //---- OBJ_33 ----
    OBJ_33.setText("Affectation sur Attendu");
    OBJ_33.setName("OBJ_33");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private SNBoutonLeger OBJ_16;
  private SNBoutonLeger OBJ_17;
  private JLabel OBJ_18;
  private XRiTextField QTX01;
  private JPanel panel2;
  private SNBoutonLeger OBJ_38;
  private SNBoutonLeger OBJ_39;
  private JLabel OBJ_34;
  private XRiTextField QTX02;
  private XRiTextField NUM02;
  private XRiTextField SUF02;
  private XRiTextField NLI02;
  private JLabel OBJ_35;
  private JLabel OBJ_36;
  private XRiCalendrier DLX02;
  private XRiTextField ETA02;
  private JLabel OBJ_37;
  private JLabel OBJ_12;
  private JLabel OBJ_13;
  private JPanel panel3;
  private SNBoutonLeger OBJ_49;
  private SNBoutonLeger OBJ_50;
  private JLabel OBJ_45;
  private XRiTextField QTX03;
  private JLabel OBJ_46;
  private XRiTextField NUM03;
  private XRiTextField SUF03;
  private XRiTextField NLI03;
  private JLabel OBJ_47;
  private JLabel OBJ_48;
  private XRiTextField ETA03;
  private XRiCalendrier DLX03;
  private JPanel panel4;
  private JLabel OBJ_41;
  private XRiTextField QTXNA;
  private JLabel OBJ_15;
  private JLabel OBJ_44;
  private JLabel OBJ_33;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
