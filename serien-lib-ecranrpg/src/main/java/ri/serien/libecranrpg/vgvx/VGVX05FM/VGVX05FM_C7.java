/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.vgvx.VGVX05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVX05FM_C7 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] A1RON_Value = { "", "1", "2", "3", "4", "5", "6", };
  private String[] A1IN16_Value = { "", "1", "2", "3", "4", };
  private String[] A1IN25_Value = { "", "1", "2", "3", };
  private String[] A1IN2_Value = { "", "1", "2", };
  private String[] A1IN8_Value = { "", "1", "2", };
  private String[] A1IN9_Value = { "", "1", "2", };
  private String[] A1IN31_Value = { "", "1", "2", };
  private String[] A1REA_Value = { "0", "1", "2", "3", "4", "5", "6", "7", "8" };
  private String[] A1IN19_Value = { "", "0", "1", "2", "3", "4", "5", "6", };
  private String[] A1IN34_Value = { "", "1", };
  private String val_A1TSP = "";
  private String val_A1IN12 = "";
  
  /**
   * Constructeur.
   */
  public VGVX05FM_C7(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    A1IN2.setValeurs(A1IN2_Value, null);
    A1IN16.setValeurs(A1IN16_Value, null);
    A1REA.setValeurs(A1REA_Value, null);
    A1IN19.setValeurs(A1IN19_Value, null);
    A1RON.setValeurs(A1RON_Value, null);
    A1IN25.setValeurs(A1IN25_Value, null);
    A1IN8.setValeurs(A1IN8_Value, null);
    A1IN9.setValeurs(A1IN9_Value, null);
    A1IN31.setValeurs(A1IN31_Value, null);
    A1IN34.setValeurs(A1IN34_Value, null);
    A1IN26.setValeursSelection("1", " ");
    A1IN15.setValeurs("1", buttonGroup2);
    A1IN15_1.setValeurs("");
    A1IN15_2.setValeurs("2");
    A1IN5.setValeursSelection("1", " ");
    A1IN10.setValeursSelection("1", " ");
    A1TNC.setValeursSelection("1", " ");
    A1IN1.setValeursSelection("1", " ");
    A1IN13.setValeursSelection("1", " ");
    A1IN27.setValeursSelection("1", " ");
    A1IN33.setValeursSelection("1", " ");
    
    setCloseKey("ENTER", "F2", "F4", "F20");
    
    // Titre
    setTitle("Informations diverses");
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LBZP21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBZP21@")).trim());
    OBJ_91_OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBZP20@")).trim());
    OBJ_45_OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNPD@")).trim());
    OBJ_49_OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNVO@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
    W1FACT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1FACT@")).trim());
    A1DA6X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1DA6X@")).trim());
    A1DA5X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1DA5X@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    boolean is_Interro = lexique.isTrue("53");
    boolean is_Creation = lexique.isTrue("51");
    
    xRiRadioButton1.setSelected(lexique.HostFieldGetData("A1TSP").trim().equalsIgnoreCase("0"));
    xRiRadioButton2.setSelected(lexique.HostFieldGetData("A1TSP").trim().equalsIgnoreCase("D"));
    xRiRadioButton3.setSelected(lexique.HostFieldGetData("A1TSP").trim().equalsIgnoreCase("Q"));
    xRiRadioButton4.setSelected(lexique.HostFieldGetData("A1TSP").trim().equalsIgnoreCase("2"));
    xRiRadioButton5.setSelected(lexique.HostFieldGetData("A1TSP").trim().equalsIgnoreCase("4"));
    xRiRadioButton6.setSelected(lexique.HostFieldGetData("A1TSP").trim().equalsIgnoreCase("V"));
    xRiRadioButton7.setSelected(lexique.HostFieldGetData("A1TSP").trim().equalsIgnoreCase("K"));
    xRiRadioButton8.setSelected(lexique.HostFieldGetData("A1TSP").trim().equalsIgnoreCase(""));
    xRiRadioButton9.setSelected(lexique.HostFieldGetData("A1TSP").trim().equalsIgnoreCase("1"));
    xRiRadioButton10.setSelected(lexique.HostFieldGetData("A1TSP").trim().equalsIgnoreCase("3"));
    xRiRadioButton11.setSelected(lexique.HostFieldGetData("A1TSP").trim().equalsIgnoreCase("T"));
    
    // Gestion onglet
    if (lexique.getValeurVariableGlobale("ongletEnCours_VGVX05FM_C7") != null) {
      OBJ_28.setSelectedIndex((Integer) lexique.getValeurVariableGlobale("ongletEnCours_VGVX05FM_C7"));
    }
    
    
    navig_valid.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    
    // Les Combo-box
    boolean coche = (xRiRadioButton1.isSelected()) || (xRiRadioButton2.isSelected()) || (xRiRadioButton3.isSelected())
        || (xRiRadioButton4.isSelected()) || (xRiRadioButton5.isSelected()) || (xRiRadioButton6.isSelected())
        || (xRiRadioButton7.isSelected());
    cb_gestion.setEnabled(!coche);
    
    // Si l'article est 'géré' ou 'non visible', il ne peut passer à 'non géré' (choix grisé) sauf en création
    if (!lexique.HostFieldGetData("A1IN15").trim().equalsIgnoreCase("1") & !is_Creation) {
      A1IN15.setEnabled(false);
    }
    
    OBJ_54_OBJ_54.setVisible(true);
    A1IN25.setVisible(true);
    A1QT1.forceVisibility();
    
    OBJ_49_OBJ_49.setVisible(lexique.isPresent("UNVO"));
    OBJ_45_OBJ_45.setVisible(lexique.isPresent("UNPD"));
    A1ZP21.setVisible(!lexique.HostFieldGetData("LBZP21").trim().equalsIgnoreCase("") & lexique.isPresent("LBZP21"));
    OBJ_154_OBJ_154.setVisible(lexique.HostFieldGetData("WDATEPAR").equalsIgnoreCase("Date de parution"));
    OBJ_153_OBJ_153.setVisible(!lexique.HostFieldGetData("WDATEPAR").trim().equalsIgnoreCase("Date de parution"));
    A1ZP20.setVisible(!lexique.HostFieldGetData("LBZP20").trim().equalsIgnoreCase("") & lexique.isPresent("LBZP20"));
    OBJ_132_OBJ_132.setVisible(lexique.isTrue("N73"));
    A1REA.setVisible(lexique.isTrue("N73"));
    
    // Gestion article spécial
    ck_special.setSelected(!lexique.HostFieldGetData("A1SPE").trim().equalsIgnoreCase("")
        && !lexique.HostFieldGetData("A1SPE").trim().equalsIgnoreCase("0"));
    ck_revient.setVisible(ck_special.isSelected());
    ck_revient.setSelected(lexique.HostFieldGetData("A1SPE").trim().equalsIgnoreCase("2"));
    
    // Gestion en série
    val_A1TSP = lexique.HostFieldGetData("A1TSP");
    val_A1IN12 = lexique.HostFieldGetData("A1IN12");
    
    if (val_A1TSP.equals("S")) {
      panel1.setVisible(false);
      if (val_A1IN12.equals("E")) {
        cb_gestion.setSelectedIndex(2);
      }
      else {
        cb_gestion.setSelectedIndex(1);
      }
    }
    else {
      cb_gestion.setSelectedIndex(0);
      panel1.setVisible(true);
    }
    
    A1RON.setEnabled(!is_Interro);
    A1IN9.setEnabled(!is_Interro);
    A1IN25.setEnabled(!is_Interro);
    A1IN2.setEnabled(!is_Interro);
    A1REA.setEnabled(!is_Interro);
    A1IN16.setEnabled(!is_Interro);
    cb_gestion.setEnabled(!is_Interro);
    ck_special.setEnabled(!is_Interro);
    ck_revient.setEnabled(!is_Interro);
    xRiRadioButton1.setEnabled(!is_Interro);
    xRiRadioButton2.setEnabled(!is_Interro);
    xRiRadioButton3.setEnabled(!is_Interro);
    xRiRadioButton4.setEnabled(!is_Interro);
    xRiRadioButton5.setEnabled(!is_Interro);
    xRiRadioButton6.setEnabled(!is_Interro);
    xRiRadioButton7.setEnabled(!is_Interro);
    xRiRadioButton8.setEnabled(!is_Interro);
    xRiRadioButton9.setEnabled(!is_Interro);
    xRiRadioButton10.setEnabled(!is_Interro);
    xRiRadioButton11.setEnabled(!is_Interro);
    
    lbDelaisLTF.setVisible(A1QT4R.isVisible());
    // Date de première livraiosn visible si 83^
    lbDatepremierlivraison.setVisible(lexique.isTrue("83"));
    A1DA5X.setVisible(lexique.isTrue("83"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Gestion onglet
    lexique.addVariableGlobale("ongletEnCours_VGVX05FM_C7", OBJ_28.getSelectedIndex());
    
    
    
    // Gestion article spécial
    if (ck_revient.isSelected()) {
      lexique.HostFieldPutData("A1SPE", 0, "2");
    }
    else if (ck_special.isSelected()) {
      lexique.HostFieldPutData("A1SPE", 0, "1");
    }
    else {
      lexique.HostFieldPutData("A1SPE", 0, "0");
    }
    
    // Gestion combo gestion
    switch (cb_gestion.getSelectedIndex()) {
      case 0:
        lexique.HostFieldPutData("A1IN12", 0, "");
        
        if (xRiRadioButton1.isSelected()) {
          lexique.HostFieldPutData("A1TSP", 0, "0");
        }
        else if (xRiRadioButton2.isSelected()) {
          lexique.HostFieldPutData("A1TSP", 0, "D");
        }
        else if (xRiRadioButton3.isSelected()) {
          lexique.HostFieldPutData("A1TSP", 0, "Q");
        }
        else if (xRiRadioButton4.isSelected()) {
          lexique.HostFieldPutData("A1TSP", 0, "2");
        }
        else if (xRiRadioButton5.isSelected()) {
          lexique.HostFieldPutData("A1TSP", 0, "4");
        }
        else if (xRiRadioButton6.isSelected()) {
          lexique.HostFieldPutData("A1TSP", 0, "V");
        }
        else if (xRiRadioButton7.isSelected()) {
          lexique.HostFieldPutData("A1TSP", 0, "K");
        }
        else if (xRiRadioButton8.isSelected()) {
          lexique.HostFieldPutData("A1TSP", 0, "");
        }
        else if (xRiRadioButton9.isSelected()) {
          lexique.HostFieldPutData("A1TSP", 0, "1");
        }
        else if (xRiRadioButton10.isSelected()) {
          lexique.HostFieldPutData("A1TSP", 0, "3");
        }
        else if (xRiRadioButton11.isSelected()) {
          lexique.HostFieldPutData("A1TSP", 0, "T");
        }
        else {
          lexique.HostFieldPutData("A1TSP", 0, "");
        }
        break;
      case 1:
        lexique.HostFieldPutData("A1TSP", 0, "S");
        lexique.HostFieldPutData("A1IN12", 0, "");
        break;
      case 2:
        lexique.HostFieldPutData("A1TSP", 0, "S");
        lexique.HostFieldPutData("A1IN12", 0, "E");
        break;
    }
    
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(14, 53);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_100ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void OBJ_125ActionPerformed(ActionEvent e) {
    
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed() {
    lexique.HostFieldPutData("WPRA", 0, "D");
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt7ActionPerformed() {
    lexique.HostFieldPutData("WPRA", 0, "B");
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt8ActionPerformed() {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void ck_specialActionPerformed(ActionEvent e) {
    ck_revient.setVisible(ck_special.isSelected());
    if (!ck_special.isSelected()) {
      ck_revient.setSelected(false);
    }
  }
  
  private void ck_revientActionPerformed(ActionEvent e) {
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void A1IN5ActionPerformed(ActionEvent e) {
    A1RST.setEnabled(!A1IN5.isSelected());
  }
  
  private void cb_gestionActionPerformed(ActionEvent e) {
    panel1.setVisible(cb_gestion.getSelectedIndex() == 0);
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_28 = new JTabbedPane();
    OBJ_29 = new JPanel();
    A1IN9 = new XRiComboBox();
    A1IN25 = new XRiComboBox();
    A1RON = new XRiComboBox();
    A1ZP20 = new XRiTextField();
    A1IN13 = new XRiCheckBox();
    A1IN1 = new XRiCheckBox();
    A1TNC = new XRiCheckBox();
    A1IN10 = new XRiCheckBox();
    OBJ_58_OBJ_58 = new JLabel();
    A1IN5 = new XRiCheckBox();
    A1ZP21 = new XRiTextField();
    OBJ_54_OBJ_54 = new JLabel();
    OBJ_59_OBJ_59 = new JLabel();
    OBJ_62_OBJ_62 = new JLabel();
    OBJ_66_OBJ_66 = new JLabel();
    OBJ_56_OBJ_56 = new JLabel();
    OBJ_64_OBJ_64 = new JLabel();
    A1NOP = new XRiTextField();
    A1RST = new XRiTextField();
    OBJ_32_OBJ_32 = new JLabel();
    LBZP21 = new JLabel();
    OBJ_91_OBJ_91 = new JLabel();
    A1QT1 = new XRiTextField();
    TTVAX = new XRiTextField();
    A1PMM = new XRiTextField();
    A1PMR = new XRiTextField();
    A1TPF = new XRiTextField();
    panel3 = new JPanel();
    OBJ_38_OBJ_38 = new JLabel();
    OBJ_43_OBJ_43 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    A1CNDX = new XRiTextField();
    A1PDSL = new XRiTextField();
    A1VOLL = new XRiTextField();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_36 = new SNBoutonDetail();
    A1UNL = new XRiTextField();
    OBJ_37_OBJ_37 = new JLabel();
    A1IN8 = new XRiComboBox();
    panel4 = new JPanel();
    A1TOP1 = new XRiTextField();
    A1TOP2 = new XRiTextField();
    A1TOP3 = new XRiTextField();
    A1TOP4 = new XRiTextField();
    A1TOP5 = new XRiTextField();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    OBJ_51_OBJ_51 = new JLabel();
    A1CNV = new XRiTextField();
    A1TVA = new XRiSpinner();
    A1IN27 = new XRiCheckBox();
    A1IN31 = new XRiComboBox();
    OBJ_58_OBJ_59 = new JLabel();
    panel7 = new JPanel();
    A1IN15 = new XRiRadioButton();
    A1IN15_1 = new XRiRadioButton();
    A1IN15_2 = new XRiRadioButton();
    A1IN33 = new XRiCheckBox();
    lbCCEREM = new JLabel();
    A1IN34 = new XRiComboBox();
    OBJ_93 = new JPanel();
    panel5 = new JPanel();
    label2 = new JLabel();
    OBJ_100 = new SNBoutonDetail();
    A1NDP = new XRiTextField();
    OBJ_102_OBJ_102 = new JLabel();
    A1OPA = new XRiTextField();
    OBJ_105_OBJ_105 = new JLabel();
    A1QT6 = new XRiTextField();
    OBJ_105_OBJ_106 = new JLabel();
    panel6 = new JPanel();
    OBJ_103_OBJ_103 = new JLabel();
    A1IN18 = new XRiTextField();
    OBJ_107_OBJ_107 = new JLabel();
    A1IN19 = new XRiComboBox();
    OBJ_129 = new JPanel();
    A1REA = new XRiComboBox();
    A1IN16 = new XRiComboBox();
    OBJ_156_OBJ_156 = new JLabel();
    OBJ_153_OBJ_153 = new JLabel();
    OBJ_154_OBJ_154 = new JLabel();
    OBJ_157_OBJ_157 = new JLabel();
    OBJ_136_OBJ_136 = new JLabel();
    OBJ_155_OBJ_155 = new JLabel();
    OBJ_134_OBJ_134 = new JLabel();
    OBJ_141_OBJ_141 = new JLabel();
    OBJ_151_OBJ_151 = new JLabel();
    A1PRDX = new XRiCalendrier();
    A1PRFX = new XRiCalendrier();
    A1P2DX = new XRiCalendrier();
    A1P2FX = new XRiCalendrier();
    OBJ_132_OBJ_132 = new JLabel();
    A1PERK = new XRiTextField();
    A1PE2K = new XRiTextField();
    OBJ_140_OBJ_140 = new JLabel();
    OBJ_137_OBJ_137 = new JLabel();
    OBJ_150_OBJ_150 = new JLabel();
    OBJ_152_OBJ_152 = new JLabel();
    A1DA1X = new XRiCalendrier();
    A1DA2X = new XRiCalendrier();
    A1DA3X = new XRiCalendrier();
    W1FACT = new RiZoneSortie();
    OBJ_157_OBJ_158 = new JLabel();
    A1DA6X = new RiZoneSortie();
    OBJ_109 = new JPanel();
    A1IN2 = new XRiComboBox();
    OBJ_111_OBJ_111 = new JLabel();
    OBJ_119_OBJ_119 = new JLabel();
    OBJ_121_OBJ_121 = new JLabel();
    OBJ_128_OBJ_128 = new JLabel();
    OBJ_125 = new SNBoutonDetail();
    A1UNP = new XRiTextField();
    A1SV1 = new XRiTextField();
    A1SV2 = new XRiTextField();
    A1IN11 = new XRiTextField();
    A1IN6 = new XRiTextField();
    A1IN7 = new XRiTextField();
    A1IN17 = new XRiTextField();
    label3 = new JLabel();
    A1IN26 = new XRiCheckBox();
    cb_gestion = new JComboBox();
    ck_special = new JCheckBox();
    ck_revient = new JCheckBox();
    panel1 = new JPanel();
    xRiRadioButton8 = new JRadioButton();
    xRiRadioButton6 = new JRadioButton();
    xRiRadioButton4 = new JRadioButton();
    xRiRadioButton11 = new JRadioButton();
    xRiRadioButton3 = new JRadioButton();
    xRiRadioButton5 = new JRadioButton();
    xRiRadioButton1 = new JRadioButton();
    xRiRadioButton9 = new JRadioButton();
    xRiRadioButton10 = new JRadioButton();
    xRiRadioButton7 = new JRadioButton();
    xRiRadioButton2 = new JRadioButton();
    A1QT4R = new XRiTextField();
    lbDelaisLTF = new JLabel();
    lbDatepremierlivraison = new JLabel();
    A1DA5X = new RiZoneSortie();
    lbDatepremierlivraison2 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_24 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    A1TSP = new XRiTextField();
    A1IN12 = new XRiTextField();
    A1SPE = new XRiTextField();
    OBJ_14 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    buttonGroup2 = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(940, 485));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 350));
            menus_haut.setPreferredSize(new Dimension(160, 350));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt1 ----
              riMenu_bt1.setText("@V01F@");
              riMenu_bt1.setPreferredSize(new Dimension(167, 50));
              riMenu_bt1.setMinimumSize(new Dimension(167, 50));
              riMenu_bt1.setMaximumSize(new Dimension(170, 50));
              riMenu_bt1.setFont(riMenu_bt1.getFont().deriveFont(riMenu_bt1.getFont().getSize() + 2f));
              riMenu_bt1.setName("riMenu_bt1");
              riMenu_V01F.add(riMenu_bt1);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Prix de la concurrence");
              riSousMenu_bt6.setToolTipText("Prix de la concurrence");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed();
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Prix vente consommateur");
              riSousMenu_bt7.setToolTipText("Prix vente consommateur");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed();
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== OBJ_28 ========
        {
          OBJ_28.setName("OBJ_28");
          
          // ======== OBJ_29 ========
          {
            OBJ_29.setOpaque(false);
            OBJ_29.setName("OBJ_29");
            OBJ_29.setLayout(null);
            
            // ---- A1IN9 ----
            A1IN9
                .setModel(new DefaultComboBoxModel(new String[] { "Pas d'exclusion", "Calcul et application", "Application seulement" }));
            A1IN9.setComponentPopupMenu(BTD);
            A1IN9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN9.setName("A1IN9");
            OBJ_29.add(A1IN9);
            A1IN9.setBounds(210, 206, 187, A1IN9.getPreferredSize().height);
            
            // ---- A1IN25 ----
            A1IN25.setModel(new DefaultComboBoxModel(new String[] { "Quantit\u00e9 maxi (alerte seule)", "Quantit\u00e9 mini forc\u00e9e",
                "Qt\u00e9 multiple forc\u00e9e par exc\u00e9s", "Nb de conditionnement mini" }));
            A1IN25.setComponentPopupMenu(BTD);
            A1IN25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN25.setName("A1IN25");
            OBJ_29.add(A1IN25);
            A1IN25.setBounds(425, 262, 204, A1IN25.getPreferredSize().height);
            
            // ---- A1RON ----
            A1RON.setModel(new DefaultComboBoxModel(new String[] { "Aucun", "0,05", "0,10", "0,25", "0,50", "1,00", "10,00" }));
            A1RON.setComponentPopupMenu(BTD);
            A1RON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1RON.setName("A1RON");
            OBJ_29.add(A1RON);
            A1RON.setBounds(616, 179, 80, A1RON.getPreferredSize().height);
            
            // ---- A1ZP20 ----
            A1ZP20.setComponentPopupMenu(BTD);
            A1ZP20.setName("A1ZP20");
            OBJ_29.add(A1ZP20);
            A1ZP20.setBounds(360, 401, 335, A1ZP20.getPreferredSize().height);
            
            // ---- A1IN13 ----
            A1IN13.setText("Taxe additionnelle");
            A1IN13.setComponentPopupMenu(BTD);
            A1IN13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN13.setName("A1IN13");
            OBJ_29.add(A1IN13);
            A1IN13.setBounds(25, 40, 190, 20);
            
            // ---- A1IN1 ----
            A1IN1.setText("Non soumis escompte");
            A1IN1.setComponentPopupMenu(BTD);
            A1IN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN1.setName("A1IN1");
            OBJ_29.add(A1IN1);
            A1IN1.setBounds(25, 61, 190, 20);
            
            // ---- A1TNC ----
            A1TNC.setText("Non commissionn\u00e9");
            A1TNC.setComponentPopupMenu(BTD);
            A1TNC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1TNC.setName("A1TNC");
            OBJ_29.add(A1TNC);
            A1TNC.setBounds(25, 82, 190, 20);
            
            // ---- A1IN10 ----
            A1IN10.setText("Exclusion des ristournes");
            A1IN10.setComponentPopupMenu(BTD);
            A1IN10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN10.setName("A1IN10");
            OBJ_29.add(A1IN10);
            A1IN10.setBounds(25, 294, 190, 20);
            
            // ---- OBJ_58_OBJ_58 ----
            OBJ_58_OBJ_58.setText("Exclusion des remises globales");
            OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");
            OBJ_29.add(OBJ_58_OBJ_58);
            OBJ_58_OBJ_58.setBounds(25, 209, 190, 20);
            
            // ---- A1IN5 ----
            A1IN5.setText("Non g\u00e9r\u00e9 en statistiques");
            A1IN5.setComponentPopupMenu(BTD);
            A1IN5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN5.setName("A1IN5");
            A1IN5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                A1IN5ActionPerformed(e);
              }
            });
            OBJ_29.add(A1IN5);
            A1IN5.setBounds(25, 105, 190, 20);
            
            // ---- A1ZP21 ----
            A1ZP21.setComponentPopupMenu(BTD);
            A1ZP21.setName("A1ZP21");
            OBJ_29.add(A1ZP21);
            A1ZP21.setBounds(100, 401, 165, A1ZP21.getPreferredSize().height);
            
            // ---- OBJ_54_OBJ_54 ----
            OBJ_54_OBJ_54.setText("Code douanier");
            OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
            OBJ_29.add(OBJ_54_OBJ_54);
            OBJ_54_OBJ_54.setBounds(425, 155, 150, 20);
            
            // ---- OBJ_59_OBJ_59 ----
            OBJ_59_OBJ_59.setText("Arrondi");
            OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");
            OBJ_29.add(OBJ_59_OBJ_59);
            OBJ_59_OBJ_59.setBounds(425, 182, 150, 20);
            
            // ---- OBJ_62_OBJ_62 ----
            OBJ_62_OBJ_62.setText("Minimum de marge");
            OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");
            OBJ_29.add(OBJ_62_OBJ_62);
            OBJ_62_OBJ_62.setBounds(425, 209, 150, 20);
            
            // ---- OBJ_66_OBJ_66 ----
            OBJ_66_OBJ_66.setText("Maximum de remise");
            OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");
            OBJ_29.add(OBJ_66_OBJ_66);
            OBJ_66_OBJ_66.setBounds(425, 237, 150, 20);
            
            // ---- OBJ_56_OBJ_56 ----
            OBJ_56_OBJ_56.setText("Regroupement statistique");
            OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
            OBJ_29.add(OBJ_56_OBJ_56);
            OBJ_56_OBJ_56.setBounds(25, 182, 180, 20);
            
            // ---- OBJ_64_OBJ_64 ----
            OBJ_64_OBJ_64.setText("Taxe parafiscale");
            OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");
            OBJ_29.add(OBJ_64_OBJ_64);
            OBJ_64_OBJ_64.setBounds(210, 294, 120, 20);
            
            // ---- A1NOP ----
            A1NOP.setComponentPopupMenu(null);
            A1NOP.setName("A1NOP");
            OBJ_29.add(A1NOP);
            A1NOP.setBounds(596, 151, 100, A1NOP.getPreferredSize().height);
            
            // ---- A1RST ----
            A1RST.setComponentPopupMenu(BTD);
            A1RST.setName("A1RST");
            OBJ_29.add(A1RST);
            A1RST.setBounds(210, 178, 140, A1RST.getPreferredSize().height);
            
            // ---- OBJ_32_OBJ_32 ----
            OBJ_32_OBJ_32.setText("Code TVA");
            OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
            OBJ_29.add(OBJ_32_OBJ_32);
            OBJ_32_OBJ_32.setBounds(25, 15, 85, 20);
            
            // ---- LBZP21 ----
            LBZP21.setText("@LBZP21@");
            LBZP21.setName("LBZP21");
            OBJ_29.add(LBZP21);
            LBZP21.setBounds(25, 405, 75, 20);
            
            // ---- OBJ_91_OBJ_91 ----
            OBJ_91_OBJ_91.setText("@LBZP20@");
            OBJ_91_OBJ_91.setName("OBJ_91_OBJ_91");
            OBJ_29.add(OBJ_91_OBJ_91);
            OBJ_91_OBJ_91.setBounds(280, 405, 80, 20);
            
            // ---- A1QT1 ----
            A1QT1.setComponentPopupMenu(BTDA);
            A1QT1.setName("A1QT1");
            OBJ_29.add(A1QT1);
            A1QT1.setBounds(630, 261, 66, A1QT1.getPreferredSize().height);
            
            // ---- TTVAX ----
            TTVAX.setName("TTVAX");
            OBJ_29.add(TTVAX);
            TTVAX.setBounds(145, 11, 60, TTVAX.getPreferredSize().height);
            
            // ---- A1PMM ----
            A1PMM.setComponentPopupMenu(null);
            A1PMM.setName("A1PMM");
            OBJ_29.add(A1PMM);
            A1PMM.setBounds(670, 205, 26, A1PMM.getPreferredSize().height);
            
            // ---- A1PMR ----
            A1PMR.setComponentPopupMenu(null);
            A1PMR.setName("A1PMR");
            OBJ_29.add(A1PMR);
            A1PMR.setBounds(670, 233, 26, A1PMR.getPreferredSize().height);
            
            // ---- A1TPF ----
            A1TPF.setComponentPopupMenu(BTDA);
            A1TPF.setName("A1TPF");
            OBJ_29.add(A1TPF);
            A1TPF.setBounds(355, 290, 20, A1TPF.getPreferredSize().height);
            
            // ======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Conditionnement vente"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);
              
              // ---- OBJ_38_OBJ_38 ----
              OBJ_38_OBJ_38.setText("Unit\u00e9");
              OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");
              panel3.add(OBJ_38_OBJ_38);
              OBJ_38_OBJ_38.setBounds(25, 27, 141, 20);
              
              // ---- OBJ_43_OBJ_43 ----
              OBJ_43_OBJ_43.setText("Poids");
              OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
              panel3.add(OBJ_43_OBJ_43);
              OBJ_43_OBJ_43.setBounds(25, 80, 141, 20);
              
              // ---- OBJ_47_OBJ_47 ----
              OBJ_47_OBJ_47.setText("Volume");
              OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
              panel3.add(OBJ_47_OBJ_47);
              OBJ_47_OBJ_47.setBounds(25, 108, 141, 20);
              
              // ---- A1CNDX ----
              A1CNDX.setToolTipText("?C7.A1CNDX.toolTipText_2?");
              A1CNDX.setComponentPopupMenu(BTDA);
              A1CNDX.setName("A1CNDX");
              panel3.add(A1CNDX);
              A1CNDX.setBounds(190, 23, 90, A1CNDX.getPreferredSize().height);
              
              // ---- A1PDSL ----
              A1PDSL.setComponentPopupMenu(null);
              A1PDSL.setName("A1PDSL");
              panel3.add(A1PDSL);
              A1PDSL.setBounds(190, 76, 74, A1PDSL.getPreferredSize().height);
              
              // ---- A1VOLL ----
              A1VOLL.setComponentPopupMenu(null);
              A1VOLL.setName("A1VOLL");
              panel3.add(A1VOLL);
              A1VOLL.setBounds(190, 104, 74, A1VOLL.getPreferredSize().height);
              
              // ---- OBJ_45_OBJ_45 ----
              OBJ_45_OBJ_45.setText("@UNPD@");
              OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");
              panel3.add(OBJ_45_OBJ_45);
              OBJ_45_OBJ_45.setBounds(270, 80, 60, 20);
              
              // ---- OBJ_49_OBJ_49 ----
              OBJ_49_OBJ_49.setText("@UNVO@");
              OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
              panel3.add(OBJ_49_OBJ_49);
              OBJ_49_OBJ_49.setBounds(270, 108, 60, 20);
              
              // ---- OBJ_36 ----
              OBJ_36.setToolTipText("D\u00e9tail des unit\u00e9s");
              OBJ_36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_36.setMargin(new Insets(0, 0, 0, 0));
              OBJ_36.setName("OBJ_36");
              OBJ_36.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_36ActionPerformed(e);
                }
              });
              panel3.add(OBJ_36);
              OBJ_36.setBounds(345, 23, 28, 28);
              
              // ---- A1UNL ----
              A1UNL.setToolTipText("?C7.A1UNL.toolTipText_2?");
              A1UNL.setComponentPopupMenu(BTD);
              A1UNL.setName("A1UNL");
              panel3.add(A1UNL);
              A1UNL.setBounds(294, 23, 30, A1UNL.getPreferredSize().height);
              
              // ---- OBJ_37_OBJ_37 ----
              OBJ_37_OBJ_37.setText("/");
              OBJ_37_OBJ_37.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
              panel3.add(OBJ_37_OBJ_37);
              OBJ_37_OBJ_37.setBounds(282, 27, 10, 20);
              
              // ---- A1IN8 ----
              A1IN8.setModel(new DefaultComboBoxModel(
                  new String[] { "Conditionnement scindable", "D\u00e9-conditionnement arrondi", "Conditionnement non scindable" }));
              A1IN8.setComponentPopupMenu(BTD);
              A1IN8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              A1IN8.setName("A1IN8");
              panel3.add(A1IN8);
              A1IN8.setBounds(25, 50, 352, A1IN8.getPreferredSize().height);
            }
            OBJ_29.add(panel3);
            panel3.setBounds(292, 5, 405, 145);
            
            // ======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("Zones personnalis\u00e9es"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);
              
              // ---- A1TOP1 ----
              A1TOP1.setComponentPopupMenu(BTD);
              A1TOP1.setName("A1TOP1");
              panel4.add(A1TOP1);
              A1TOP1.setBounds(60, 27, 30, A1TOP1.getPreferredSize().height);
              
              // ---- A1TOP2 ----
              A1TOP2.setComponentPopupMenu(BTD);
              A1TOP2.setName("A1TOP2");
              panel4.add(A1TOP2);
              A1TOP2.setBounds(140, 27, 30, A1TOP2.getPreferredSize().height);
              
              // ---- A1TOP3 ----
              A1TOP3.setComponentPopupMenu(BTD);
              A1TOP3.setName("A1TOP3");
              panel4.add(A1TOP3);
              A1TOP3.setBounds(215, 27, 30, A1TOP3.getPreferredSize().height);
              
              // ---- A1TOP4 ----
              A1TOP4.setComponentPopupMenu(BTD);
              A1TOP4.setName("A1TOP4");
              panel4.add(A1TOP4);
              A1TOP4.setBounds(290, 27, 30, A1TOP4.getPreferredSize().height);
              
              // ---- A1TOP5 ----
              A1TOP5.setComponentPopupMenu(BTD);
              A1TOP5.setName("A1TOP5");
              panel4.add(A1TOP5);
              A1TOP5.setBounds(365, 27, 30, A1TOP5.getPreferredSize().height);
              
              // ---- label4 ----
              label4.setText("@WTI1@");
              label4.setHorizontalTextPosition(SwingConstants.LEADING);
              label4.setHorizontalAlignment(SwingConstants.CENTER);
              label4.setName("label4");
              panel4.add(label4);
              label4.setBounds(25, 27, 30, 28);
              
              // ---- label5 ----
              label5.setText("@WTI2@");
              label5.setHorizontalTextPosition(SwingConstants.LEADING);
              label5.setHorizontalAlignment(SwingConstants.CENTER);
              label5.setName("label5");
              panel4.add(label5);
              label5.setBounds(100, 27, 30, 28);
              
              // ---- label6 ----
              label6.setText("@WTI3@");
              label6.setHorizontalTextPosition(SwingConstants.LEADING);
              label6.setHorizontalAlignment(SwingConstants.CENTER);
              label6.setName("label6");
              panel4.add(label6);
              label6.setBounds(175, 27, 30, 28);
              
              // ---- label7 ----
              label7.setText("@WTI4@");
              label7.setHorizontalTextPosition(SwingConstants.LEADING);
              label7.setHorizontalAlignment(SwingConstants.CENTER);
              label7.setName("label7");
              panel4.add(label7);
              label7.setBounds(250, 27, 30, 28);
              
              // ---- label8 ----
              label8.setText("@WTI5@");
              label8.setHorizontalTextPosition(SwingConstants.LEADING);
              label8.setHorizontalAlignment(SwingConstants.CENTER);
              label8.setName("label8");
              panel4.add(label8);
              label8.setBounds(330, 27, 30, 28);
            }
            OBJ_29.add(panel4);
            panel4.setBounds(20, 325, 425, 70);
            
            // ---- OBJ_51_OBJ_51 ----
            OBJ_51_OBJ_51.setText("Rattachement CNV");
            OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
            OBJ_29.add(OBJ_51_OBJ_51);
            OBJ_51_OBJ_51.setBounds(425, 294, 125, 20);
            
            // ---- A1CNV ----
            A1CNV.setComponentPopupMenu(BTDA);
            A1CNV.setName("A1CNV");
            OBJ_29.add(A1CNV);
            A1CNV.setBounds(635, 290, 60, A1CNV.getPreferredSize().height);
            
            // ---- A1TVA ----
            A1TVA.setModel(new SpinnerListModel(new String[] { " ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" }));
            A1TVA.setName("A1TVA");
            OBJ_29.add(A1TVA);
            A1TVA.setBounds(206, 11, 39, 28);
            
            // ---- A1IN27 ----
            A1IN27.setText("Non saisie directe");
            A1IN27.setComponentPopupMenu(BTD);
            A1IN27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN27.setName("A1IN27");
            OBJ_29.add(A1IN27);
            A1IN27.setBounds(25, 131, 190, 20);
            
            // ---- A1IN31 ----
            A1IN31.setModel(new DefaultComboBoxModel(
                new String[] { "Repris sans condition", "Ni repris, ni \u00e9chang\u00e9", "Repris sous conditions" }));
            A1IN31.setComponentPopupMenu(BTD);
            A1IN31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN31.setName("A1IN31");
            OBJ_29.add(A1IN31);
            A1IN31.setBounds(210, 234, 165, A1IN31.getPreferredSize().height);
            
            // ---- OBJ_58_OBJ_59 ----
            OBJ_58_OBJ_59.setText("Conditions de reprise");
            OBJ_58_OBJ_59.setName("OBJ_58_OBJ_59");
            OBJ_29.add(OBJ_58_OBJ_59);
            OBJ_58_OBJ_59.setBounds(25, 237, 190, 20);
            
            // ======== panel7 ========
            {
              panel7.setBorder(new TitledBorder(""));
              panel7.setOpaque(false);
              panel7.setName("panel7");
              panel7.setLayout(null);
              
              // ---- A1IN15 ----
              A1IN15.setText("Non g\u00e9r\u00e9 sur le Web");
              A1IN15.setName("A1IN15");
              panel7.add(A1IN15);
              A1IN15.setBounds(10, 5, 225, 18);
              
              // ---- A1IN15_1 ----
              A1IN15_1.setText("G\u00e9r\u00e9 sur le Web");
              A1IN15_1.setName("A1IN15_1");
              panel7.add(A1IN15_1);
              A1IN15_1.setBounds(10, 23, 225, 18);
              
              // ---- A1IN15_2 ----
              A1IN15_2.setText("G\u00e9r\u00e9 mais non visible sur le Web");
              A1IN15_2.setName("A1IN15_2");
              panel7.add(A1IN15_2);
              A1IN15_2.setBounds(10, 41, 225, 18);
            }
            OBJ_29.add(panel7);
            panel7.setBounds(450, 325, 245, 70);
            
            // ---- A1IN33 ----
            A1IN33.setText("Affichage m\u00e9mo obligatoire au comptoir");
            A1IN33.setPreferredSize(new Dimension(201, 20));
            A1IN33.setName("A1IN33");
            OBJ_29.add(A1IN33);
            A1IN33.setBounds(25, 155, 315, A1IN33.getPreferredSize().height);
            
            // ---- lbCCEREM ----
            lbCCEREM.setText("Edition des remises");
            lbCCEREM.setName("lbCCEREM");
            OBJ_29.add(lbCCEREM);
            lbCCEREM.setBounds(25, 261, 125, 28);
            
            // ---- A1IN34 ----
            A1IN34.setModel(new DefaultComboBoxModel(new String[] { "Edition \u00e9ventuelle", "Pas d'\u00e9dition" }));
            A1IN34.setComponentPopupMenu(BTDA);
            A1IN34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN34.setName("A1IN34");
            OBJ_29.add(A1IN34);
            A1IN34.setBounds(210, 262, 176, A1IN34.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < OBJ_29.getComponentCount(); i++) {
                Rectangle bounds = OBJ_29.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = OBJ_29.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              OBJ_29.setMinimumSize(preferredSize);
              OBJ_29.setPreferredSize(preferredSize);
            }
          }
          OBJ_28.addTab("Divers vente", OBJ_29);
          
          // ======== OBJ_93 ========
          {
            OBJ_93.setOpaque(false);
            OBJ_93.setName("OBJ_93");
            
            // ======== panel5 ========
            {
              panel5.setBorder(new TitledBorder("Article Marchandise"));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);
              
              // ---- label2 ----
              label2.setText("Nomenclature douani\u00e8re");
              label2.setName("label2");
              panel5.add(label2);
              label2.setBounds(20, 45, 145, 20);
              
              // ---- OBJ_100 ----
              OBJ_100.setToolTipText("Cliquer pour modifier la nomenclature douani\u00e8re");
              OBJ_100.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_100.setName("OBJ_100");
              OBJ_100.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_100ActionPerformed(e);
                }
              });
              panel5.add(OBJ_100);
              OBJ_100.setBounds(180, 41, 28, 28);
              
              // ---- A1NDP ----
              A1NDP.setComponentPopupMenu(BTD);
              A1NDP.setName("A1NDP");
              panel5.add(A1NDP);
              A1NDP.setBounds(211, 41, 140, A1NDP.getPreferredSize().height);
              
              // ---- OBJ_102_OBJ_102 ----
              OBJ_102_OBJ_102.setText("( NDP)");
              OBJ_102_OBJ_102.setName("OBJ_102_OBJ_102");
              panel5.add(OBJ_102_OBJ_102);
              OBJ_102_OBJ_102.setBounds(360, 45, 47, 20);
              
              // ---- A1OPA ----
              A1OPA.setComponentPopupMenu(BTD);
              A1OPA.setName("A1OPA");
              panel5.add(A1OPA);
              A1OPA.setBounds(211, 70, 34, A1OPA.getPreferredSize().height);
              
              // ---- OBJ_105_OBJ_105 ----
              OBJ_105_OBJ_105.setText("Origine pays");
              OBJ_105_OBJ_105.setName("OBJ_105_OBJ_105");
              panel5.add(OBJ_105_OBJ_105);
              OBJ_105_OBJ_105.setBounds(20, 75, 79, 20);
              
              // ---- A1QT6 ----
              A1QT6.setComponentPopupMenu(BTD);
              A1QT6.setName("A1QT6");
              panel5.add(A1QT6);
              A1QT6.setBounds(210, 100, 68, A1QT6.getPreferredSize().height);
              
              // ---- OBJ_105_OBJ_106 ----
              OBJ_105_OBJ_106.setText("Quantit\u00e9 seuil GBA");
              OBJ_105_OBJ_106.setName("OBJ_105_OBJ_106");
              panel5.add(OBJ_105_OBJ_106);
              OBJ_105_OBJ_106.setBounds(20, 104, 185, 20);
            }
            
            // ======== panel6 ========
            {
              panel6.setBorder(new TitledBorder("Article Frais"));
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);
              
              // ---- OBJ_103_OBJ_103 ----
              OBJ_103_OBJ_103.setText("El\u00e9ment de frais");
              OBJ_103_OBJ_103.setName("OBJ_103_OBJ_103");
              panel6.add(OBJ_103_OBJ_103);
              OBJ_103_OBJ_103.setBounds(20, 35, 100, 20);
              
              // ---- A1IN18 ----
              A1IN18.setComponentPopupMenu(BTD);
              A1IN18.setName("A1IN18");
              panel6.add(A1IN18);
              A1IN18.setBounds(135, 31, 20, A1IN18.getPreferredSize().height);
              
              // ---- OBJ_107_OBJ_107 ----
              OBJ_107_OBJ_107.setText("Type de r\u00e9partition");
              OBJ_107_OBJ_107.setName("OBJ_107_OBJ_107");
              panel6.add(OBJ_107_OBJ_107);
              OBJ_107_OBJ_107.setBounds(20, 65, 119, 20);
              
              // ---- A1IN19 ----
              A1IN19.setModel(new DefaultComboBoxModel(new String[] { "", "Imput\u00e9, non r\u00e9parti",
                  "Imput\u00e9, r\u00e9parti au prorata des montants", "Imput\u00e9, r\u00e9parti au prorata des quantit\u00e9s",
                  "Imput\u00e9, r\u00e9parti au prorata des poids", "Imput\u00e9, r\u00e9parti au prorata des volumes",
                  "Double r\u00e9partition : au poids et au montant", "Double r\u00e9partition : au volume et au montant" }));
              A1IN19.setComponentPopupMenu(BTD);
              A1IN19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              A1IN19.setName("A1IN19");
              panel6.add(A1IN19);
              A1IN19.setBounds(135, 62, 255, A1IN19.getPreferredSize().height);
            }
            
            GroupLayout OBJ_93Layout = new GroupLayout(OBJ_93);
            OBJ_93.setLayout(OBJ_93Layout);
            OBJ_93Layout.setHorizontalGroup(OBJ_93Layout.createParallelGroup()
                .addGroup(OBJ_93Layout.createSequentialGroup().addGap(25, 25, 25).addGroup(OBJ_93Layout.createParallelGroup()
                    .addComponent(panel5, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(OBJ_93Layout.createSequentialGroup()
                        .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE).addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap(229, Short.MAX_VALUE)));
            OBJ_93Layout.setVerticalGroup(OBJ_93Layout.createParallelGroup()
                .addGroup(OBJ_93Layout.createSequentialGroup().addGap(25, 25, 25)
                    .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE).addContainerGap()));
          }
          OBJ_28.addTab("Divers achat", OBJ_93);
          
          // ======== OBJ_129 ========
          {
            OBJ_129.setOpaque(false);
            OBJ_129.setName("OBJ_129");
            OBJ_129.setLayout(null);
            
            // ---- A1REA ----
            A1REA.setModel(new DefaultComboBoxModel(
                new String[] { "Rupture sur stock minimum", "Consommation moyenne", "R\u00e9approvisionnement manuel",
                    "Pr\u00e9visions de consommation", "Conso moyenne plafonn\u00e9e", "G\u00e9r\u00e9 (Plafond pour couverture)",
                    "Compl\u00e9ment stock maximum", "Plafond pour couverture 'Mag'", "Produit non g\u00e9r\u00e9" }));
            A1REA.setComponentPopupMenu(BTD);
            A1REA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1REA.setName("A1REA");
            OBJ_129.add(A1REA);
            A1REA.setBounds(77, 27, 263, A1REA.getPreferredSize().height);
            
            // ---- A1IN16 ----
            A1IN16.setModel(new DefaultComboBoxModel(new String[] { "Fournisseur principal", "meilleur prix", "meilleure qualit\u00e9",
                "d\u00e9lai le plus court", "magasin du fournisseur" }));
            A1IN16.setComponentPopupMenu(BTD);
            A1IN16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN16.setName("A1IN16");
            OBJ_129.add(A1IN16);
            A1IN16.setBounds(525, 27, 180, A1IN16.getPreferredSize().height);
            
            // ---- OBJ_156_OBJ_156 ----
            OBJ_156_OBJ_156.setText("Date de fin de r\u00e9approvisionnement");
            OBJ_156_OBJ_156.setName("OBJ_156_OBJ_156");
            OBJ_129.add(OBJ_156_OBJ_156);
            OBJ_156_OBJ_156.setBounds(30, 224, 215, 20);
            
            // ---- OBJ_153_OBJ_153 ----
            OBJ_153_OBJ_153.setText("Date de d\u00e9but de commercialisation");
            OBJ_153_OBJ_153.setName("OBJ_153_OBJ_153");
            OBJ_129.add(OBJ_153_OBJ_153);
            OBJ_153_OBJ_153.setBounds(30, 154, 215, 20);
            
            // ---- OBJ_154_OBJ_154 ----
            OBJ_154_OBJ_154.setText("Date de parution");
            OBJ_154_OBJ_154.setName("OBJ_154_OBJ_154");
            OBJ_129.add(OBJ_154_OBJ_154);
            OBJ_154_OBJ_154.setBounds(30, 154, 215, 20);
            
            // ---- OBJ_157_OBJ_157 ----
            OBJ_157_OBJ_157.setText("Date de fin de commercialisation");
            OBJ_157_OBJ_157.setName("OBJ_157_OBJ_157");
            OBJ_129.add(OBJ_157_OBJ_157);
            OBJ_157_OBJ_157.setBounds(30, 189, 215, 20);
            
            // ---- OBJ_136_OBJ_136 ----
            OBJ_136_OBJ_136.setText("Saisonnalit\u00e9 consommation");
            OBJ_136_OBJ_136.setName("OBJ_136_OBJ_136");
            OBJ_129.add(OBJ_136_OBJ_136);
            OBJ_136_OBJ_136.setBounds(30, 84, 171, 20);
            
            // ---- OBJ_155_OBJ_155 ----
            OBJ_155_OBJ_155.setText("Date de premi\u00e8re facture");
            OBJ_155_OBJ_155.setName("OBJ_155_OBJ_155");
            OBJ_129.add(OBJ_155_OBJ_155);
            OBJ_155_OBJ_155.setBounds(370, 154, 150, 20);
            
            // ---- OBJ_134_OBJ_134 ----
            OBJ_134_OBJ_134.setText("Recherche fournisseur");
            OBJ_134_OBJ_134.setName("OBJ_134_OBJ_134");
            OBJ_129.add(OBJ_134_OBJ_134);
            OBJ_134_OBJ_134.setBounds(370, 30, 150, 20);
            
            // ---- OBJ_141_OBJ_141 ----
            OBJ_141_OBJ_141.setText("%");
            OBJ_141_OBJ_141.setName("OBJ_141_OBJ_141");
            OBJ_129.add(OBJ_141_OBJ_141);
            OBJ_141_OBJ_141.setBounds(500, 84, 30, 20);
            
            // ---- OBJ_151_OBJ_151 ----
            OBJ_151_OBJ_151.setText("%");
            OBJ_151_OBJ_151.setName("OBJ_151_OBJ_151");
            OBJ_129.add(OBJ_151_OBJ_151);
            OBJ_151_OBJ_151.setBounds(500, 119, 30, 20);
            
            // ---- A1PRDX ----
            A1PRDX.setComponentPopupMenu(null);
            A1PRDX.setTypeSaisie(6);
            A1PRDX.setName("A1PRDX");
            OBJ_129.add(A1PRDX);
            A1PRDX.setBounds(240, 80, 90, A1PRDX.getPreferredSize().height);
            
            // ---- A1PRFX ----
            A1PRFX.setComponentPopupMenu(null);
            A1PRFX.setTypeSaisie(6);
            A1PRFX.setName("A1PRFX");
            OBJ_129.add(A1PRFX);
            A1PRFX.setBounds(370, 80, 90, A1PRFX.getPreferredSize().height);
            
            // ---- A1P2DX ----
            A1P2DX.setComponentPopupMenu(null);
            A1P2DX.setTypeSaisie(6);
            A1P2DX.setName("A1P2DX");
            OBJ_129.add(A1P2DX);
            A1P2DX.setBounds(240, 115, 90, A1P2DX.getPreferredSize().height);
            
            // ---- A1P2FX ----
            A1P2FX.setComponentPopupMenu(null);
            A1P2FX.setTypeSaisie(6);
            A1P2FX.setName("A1P2FX");
            OBJ_129.add(A1P2FX);
            A1P2FX.setBounds(370, 115, 90, A1P2FX.getPreferredSize().height);
            
            // ---- OBJ_132_OBJ_132 ----
            OBJ_132_OBJ_132.setText("Type");
            OBJ_132_OBJ_132.setName("OBJ_132_OBJ_132");
            OBJ_129.add(OBJ_132_OBJ_132);
            OBJ_132_OBJ_132.setBounds(30, 30, 36, 20);
            
            // ---- A1PERK ----
            A1PERK.setComponentPopupMenu(null);
            A1PERK.setName("A1PERK");
            OBJ_129.add(A1PERK);
            A1PERK.setBounds(525, 80, 34, A1PERK.getPreferredSize().height);
            
            // ---- A1PE2K ----
            A1PE2K.setComponentPopupMenu(null);
            A1PE2K.setName("A1PE2K");
            OBJ_129.add(A1PE2K);
            A1PE2K.setBounds(525, 115, 34, A1PE2K.getPreferredSize().height);
            
            // ---- OBJ_140_OBJ_140 ----
            OBJ_140_OBJ_140.setText("au");
            OBJ_140_OBJ_140.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_140_OBJ_140.setName("OBJ_140_OBJ_140");
            OBJ_129.add(OBJ_140_OBJ_140);
            OBJ_140_OBJ_140.setBounds(330, 84, 40, 20);
            
            // ---- OBJ_137_OBJ_137 ----
            OBJ_137_OBJ_137.setText("du");
            OBJ_137_OBJ_137.setName("OBJ_137_OBJ_137");
            OBJ_129.add(OBJ_137_OBJ_137);
            OBJ_137_OBJ_137.setBounds(210, 84, 32, 20);
            
            // ---- OBJ_150_OBJ_150 ----
            OBJ_150_OBJ_150.setText("1");
            OBJ_150_OBJ_150.setName("OBJ_150_OBJ_150");
            OBJ_129.add(OBJ_150_OBJ_150);
            OBJ_150_OBJ_150.setBounds(562, 84, 12, 20);
            
            // ---- OBJ_152_OBJ_152 ----
            OBJ_152_OBJ_152.setText("2");
            OBJ_152_OBJ_152.setName("OBJ_152_OBJ_152");
            OBJ_129.add(OBJ_152_OBJ_152);
            OBJ_152_OBJ_152.setBounds(562, 119, 12, 20);
            
            // ---- A1DA1X ----
            A1DA1X.setName("A1DA1X");
            OBJ_129.add(A1DA1X);
            A1DA1X.setBounds(240, 150, 105, A1DA1X.getPreferredSize().height);
            
            // ---- A1DA2X ----
            A1DA2X.setName("A1DA2X");
            OBJ_129.add(A1DA2X);
            A1DA2X.setBounds(240, 220, 105, A1DA2X.getPreferredSize().height);
            
            // ---- A1DA3X ----
            A1DA3X.setName("A1DA3X");
            OBJ_129.add(A1DA3X);
            A1DA3X.setBounds(240, 185, 105, A1DA3X.getPreferredSize().height);
            
            // ---- W1FACT ----
            W1FACT.setText("@W1FACT@");
            W1FACT.setHorizontalAlignment(SwingConstants.CENTER);
            W1FACT.setName("W1FACT");
            OBJ_129.add(W1FACT);
            W1FACT.setBounds(525, 152, 68, W1FACT.getPreferredSize().height);
            
            // ---- OBJ_157_OBJ_158 ----
            OBJ_157_OBJ_158.setText("Date de fin de SAV");
            OBJ_157_OBJ_158.setName("OBJ_157_OBJ_158");
            OBJ_129.add(OBJ_157_OBJ_158);
            OBJ_157_OBJ_158.setBounds(370, 194, 150, 20);
            
            // ---- A1DA6X ----
            A1DA6X.setText("@A1DA6X@");
            A1DA6X.setName("A1DA6X");
            OBJ_129.add(A1DA6X);
            A1DA6X.setBounds(525, 192, 68, A1DA6X.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < OBJ_129.getComponentCount(); i++) {
                Rectangle bounds = OBJ_129.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = OBJ_129.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              OBJ_129.setMinimumSize(preferredSize);
              OBJ_129.setPreferredSize(preferredSize);
            }
          }
          OBJ_28.addTab("R\u00e9approvisionnement", OBJ_129);
          
          // ======== OBJ_109 ========
          {
            OBJ_109.setOpaque(false);
            OBJ_109.setName("OBJ_109");
            OBJ_109.setLayout(null);
            
            // ---- A1IN2 ----
            A1IN2.setModel(new DefaultComboBoxModel(
                new String[] { "Non g\u00e9r\u00e9 par lot", "G\u00e9r\u00e9 par lots", "G\u00e9r\u00e9 par lots avec 'sous lots'" }));
            A1IN2.setComponentPopupMenu(BTD);
            A1IN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN2.setName("A1IN2");
            OBJ_109.add(A1IN2);
            A1IN2.setBounds(320, 37, 279, A1IN2.getPreferredSize().height);
            
            // ---- OBJ_111_OBJ_111 ----
            OBJ_111_OBJ_111.setText("Sp\u00e9cificit\u00e9 extraction");
            OBJ_111_OBJ_111.setName("OBJ_111_OBJ_111");
            OBJ_109.add(OBJ_111_OBJ_111);
            OBJ_111_OBJ_111.setBounds(45, 40, 135, 20);
            
            // ---- OBJ_119_OBJ_119 ----
            OBJ_119_OBJ_119.setText("Non edt. d\u00e9tail NMC");
            OBJ_119_OBJ_119.setName("OBJ_119_OBJ_119");
            OBJ_109.add(OBJ_119_OBJ_119);
            OBJ_119_OBJ_119.setBounds(45, 72, 135, 20);
            
            // ---- OBJ_121_OBJ_121 ----
            OBJ_121_OBJ_121.setText("Type extension ligne");
            OBJ_121_OBJ_121.setName("OBJ_121_OBJ_121");
            OBJ_109.add(OBJ_121_OBJ_121);
            OBJ_121_OBJ_121.setBounds(45, 104, 135, 20);
            
            // ---- OBJ_128_OBJ_128 ----
            OBJ_128_OBJ_128.setText("Unit\u00e9 pr\u00e9sentation");
            OBJ_128_OBJ_128.setName("OBJ_128_OBJ_128");
            OBJ_109.add(OBJ_128_OBJ_128);
            OBJ_128_OBJ_128.setBounds(325, 104, 135, 20);
            
            // ---- OBJ_125 ----
            OBJ_125.setToolTipText("Syst\u00e8mes variables");
            OBJ_125.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_125.setName("OBJ_125");
            OBJ_125.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_125ActionPerformed(e);
              }
            });
            OBJ_109.add(OBJ_125);
            OBJ_125.setBounds(170, 132, 26, 26);
            
            // ---- A1UNP ----
            A1UNP.setComponentPopupMenu(BTD);
            A1UNP.setName("A1UNP");
            OBJ_109.add(A1UNP);
            A1UNP.setBounds(520, 100, 30, A1UNP.getPreferredSize().height);
            
            // ---- A1SV1 ----
            A1SV1.setComponentPopupMenu(BTD);
            A1SV1.setName("A1SV1");
            OBJ_109.add(A1SV1);
            A1SV1.setBounds(200, 131, 34, A1SV1.getPreferredSize().height);
            
            // ---- A1SV2 ----
            A1SV2.setComponentPopupMenu(BTD);
            A1SV2.setName("A1SV2");
            OBJ_109.add(A1SV2);
            A1SV2.setBounds(235, 131, 34, A1SV2.getPreferredSize().height);
            
            // ---- A1IN11 ----
            A1IN11.setComponentPopupMenu(null);
            A1IN11.setName("A1IN11");
            OBJ_109.add(A1IN11);
            A1IN11.setBounds(200, 36, 20, A1IN11.getPreferredSize().height);
            
            // ---- A1IN6 ----
            A1IN6.setComponentPopupMenu(null);
            A1IN6.setName("A1IN6");
            OBJ_109.add(A1IN6);
            A1IN6.setBounds(200, 68, 20, A1IN6.getPreferredSize().height);
            
            // ---- A1IN7 ----
            A1IN7.setComponentPopupMenu(BTD);
            A1IN7.setName("A1IN7");
            OBJ_109.add(A1IN7);
            A1IN7.setBounds(200, 100, 20, A1IN7.getPreferredSize().height);
            
            // ---- A1IN17 ----
            A1IN17.setComponentPopupMenu(BTDA);
            A1IN17.setName("A1IN17");
            OBJ_109.add(A1IN17);
            A1IN17.setBounds(220, 100, 20, A1IN17.getPreferredSize().height);
            
            // ---- label3 ----
            label3.setText("Syst\u00e8mes variables");
            label3.setName("label3");
            OBJ_109.add(label3);
            label3.setBounds(45, 130, 125, 30);
            
            // ---- A1IN26 ----
            A1IN26.setText("Survente");
            A1IN26.setComponentPopupMenu(BTD);
            A1IN26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN26.setToolTipText("?C7.A1IN26.toolTipText_2?");
            A1IN26.setName("A1IN26");
            OBJ_109.add(A1IN26);
            A1IN26.setBounds(45, 198, 190, 20);
            
            // ---- cb_gestion ----
            cb_gestion.setModel(new DefaultComboBoxModel(new String[] { "Non g\u00e9r\u00e9 par num\u00e9ro de s\u00e9rie",
                "G\u00e9r\u00e9 par num\u00e9ro de s\u00e9rie", "Ensemble s\u00e9rialis\u00e9 (voir doc)" }));
            cb_gestion.setName("cb_gestion");
            cb_gestion.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                cb_gestionActionPerformed(e);
              }
            });
            OBJ_109.add(cb_gestion);
            cb_gestion.setBounds(320, 69, 279, cb_gestion.getPreferredSize().height);
            
            // ---- ck_special ----
            ck_special.setText("Article sp\u00e9cial");
            ck_special.setName("ck_special");
            ck_special.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ck_specialActionPerformed(e);
              }
            });
            OBJ_109.add(ck_special);
            ck_special.setBounds(320, 199, 130, ck_special.getPreferredSize().height);
            
            // ---- ck_revient ----
            ck_revient.setText("Prix de revient propos\u00e9");
            ck_revient.setName("ck_revient");
            ck_revient.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ck_revientActionPerformed(e);
              }
            });
            OBJ_109.add(ck_revient);
            ck_revient.setBounds(520, 199, 170, 18);
            
            // ======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setBorder(new TitledBorder(""));
              panel1.setName("panel1");
              panel1.setLayout(new GridLayout(5, 3));
              
              // ---- xRiRadioButton8 ----
              xRiRadioButton8.setText("Standard");
              xRiRadioButton8.setName("xRiRadioButton8");
              panel1.add(xRiRadioButton8);
              
              // ---- xRiRadioButton6 ----
              xRiRadioButton6.setText("En valeur");
              xRiRadioButton6.setName("xRiRadioButton6");
              panel1.add(xRiRadioButton6);
              
              // ---- xRiRadioButton4 ----
              xRiRadioButton4.setText("En surface");
              xRiRadioButton4.setName("xRiRadioButton4");
              panel1.add(xRiRadioButton4);
              
              // ---- xRiRadioButton11 ----
              xRiRadioButton11.setText("Article transport");
              xRiRadioButton11.setName("xRiRadioButton11");
              panel1.add(xRiRadioButton11);
              
              // ---- xRiRadioButton3 ----
              xRiRadioButton3.setText("En double quantit\u00e9");
              xRiRadioButton3.setName("xRiRadioButton3");
              panel1.add(xRiRadioButton3);
              
              // ---- xRiRadioButton5 ----
              xRiRadioButton5.setText("En surface et nombre");
              xRiRadioButton5.setName("xRiRadioButton5");
              panel1.add(xRiRadioButton5);
              
              // ---- xRiRadioButton1 ----
              xRiRadioButton1.setText("Article sur-conditionnement (palette)");
              xRiRadioButton1.setName("xRiRadioButton1");
              panel1.add(xRiRadioButton1);
              
              // ---- xRiRadioButton9 ----
              xRiRadioButton9.setText("En longueur");
              xRiRadioButton9.setName("xRiRadioButton9");
              panel1.add(xRiRadioButton9);
              
              // ---- xRiRadioButton10 ----
              xRiRadioButton10.setText("En volume");
              xRiRadioButton10.setName("xRiRadioButton10");
              panel1.add(xRiRadioButton10);
              
              // ---- xRiRadioButton7 ----
              xRiRadioButton7.setText("Article kit commentaire");
              xRiRadioButton7.setName("xRiRadioButton7");
              panel1.add(xRiRadioButton7);
              
              // ---- xRiRadioButton2 ----
              xRiRadioButton2.setText("D\u00e9conditionnable");
              xRiRadioButton2.setName("xRiRadioButton2");
              panel1.add(xRiRadioButton2);
            }
            OBJ_109.add(panel1);
            panel1.setBounds(15, 255, 695, 140);
            
            // ---- A1QT4R ----
            A1QT4R.setComponentPopupMenu(BTD);
            A1QT4R.setHorizontalAlignment(SwingConstants.RIGHT);
            A1QT4R.setName("A1QT4R");
            OBJ_109.add(A1QT4R);
            A1QT4R.setBounds(520, 131, 34, A1QT4R.getPreferredSize().height);
            
            // ---- lbDelaisLTF ----
            lbDelaisLTF.setText("D\u00e9lai LTF");
            lbDelaisLTF.setName("lbDelaisLTF");
            OBJ_109.add(lbDelaisLTF);
            lbDelaisLTF.setBounds(325, 135, 135, 20);
            
            // ---- lbDatepremierlivraison ----
            lbDatepremierlivraison.setText("Date de premi\u00e8re livraison");
            lbDatepremierlivraison.setName("lbDatepremierlivraison");
            OBJ_109.add(lbDatepremierlivraison);
            lbDatepremierlivraison.setBounds(325, 165, 150, 20);
            
            // ---- A1DA5X ----
            A1DA5X.setText("@A1DA5X@");
            A1DA5X.setHorizontalAlignment(SwingConstants.CENTER);
            A1DA5X.setName("A1DA5X");
            OBJ_109.add(A1DA5X);
            A1DA5X.setBounds(520, 163, 68, A1DA5X.getPreferredSize().height);
            
            // ---- lbDatepremierlivraison2 ----
            lbDatepremierlivraison2.setText("Type de saisie");
            lbDatepremierlivraison2.setName("lbDatepremierlivraison2");
            OBJ_109.add(lbDatepremierlivraison2);
            lbDatepremierlivraison2.setBounds(20, 230, 150, 20);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < OBJ_109.getComponentCount(); i++) {
                Rectangle bounds = OBJ_109.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = OBJ_109.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              OBJ_109.setMinimumSize(preferredSize);
              OBJ_109.setPreferredSize(preferredSize);
            }
          }
          OBJ_28.addTab("Divers stock/unit\u00e9s", OBJ_109);
        }
        p_contenu.add(OBJ_28);
        OBJ_28.setBounds(30, 10, 719, 468);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_24 ----
      OBJ_24.setText("Choix possibles");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);
      
      // ---- OBJ_23 ----
      OBJ_23.setText("Aide en ligne");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);
    }
    
    // ======== BTDA ========
    {
      BTDA.setName("BTDA");
      
      // ---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }
    
    // ---- A1TSP ----
    A1TSP.setComponentPopupMenu(BTD);
    A1TSP.setName("A1TSP");
    
    // ---- A1IN12 ----
    A1IN12.setComponentPopupMenu(BTD);
    A1IN12.setName("A1IN12");
    
    // ---- A1SPE ----
    A1SPE.setComponentPopupMenu(BTD);
    A1SPE.setName("A1SPE");
    
    // ---- OBJ_14 ----
    OBJ_14.setText("D\u00e9tail Attendus / Command\u00e9s");
    OBJ_14.setName("OBJ_14");
    OBJ_14.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_14ActionPerformed(e);
      }
    });
    
    // ---- OBJ_10 ----
    OBJ_10.setText("R\u00e9capitulatif des Unit\u00e9s utilis\u00e9es");
    OBJ_10.setName("OBJ_10");
    OBJ_10.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_10ActionPerformed(e);
      }
    });
    
    // ---- OBJ_15 ----
    OBJ_15.setText("Acc\u00e9s aux Observations");
    OBJ_15.setName("OBJ_15");
    OBJ_15.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_15ActionPerformed(e);
      }
    });
    
    // ======== riSousMenu8 ========
    {
      riSousMenu8.setName("riSousMenu8");
      
      // ---- riSousMenu_bt8 ----
      riSousMenu_bt8.setText("Lien photos");
      riSousMenu_bt8.setToolTipText("?C7.riSousMenu_bt8.toolTipText_2?");
      riSousMenu_bt8.setName("riSousMenu_bt8");
      riSousMenu_bt8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt8ActionPerformed();
        }
      });
      riSousMenu8.add(riSousMenu_bt8);
    }
    
    // ---- buttonGroup2 ----
    buttonGroup2.add(A1IN15);
    buttonGroup2.add(A1IN15_1);
    buttonGroup2.add(A1IN15_2);
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(xRiRadioButton8);
    buttonGroup1.add(xRiRadioButton6);
    buttonGroup1.add(xRiRadioButton4);
    buttonGroup1.add(xRiRadioButton11);
    buttonGroup1.add(xRiRadioButton3);
    buttonGroup1.add(xRiRadioButton5);
    buttonGroup1.add(xRiRadioButton1);
    buttonGroup1.add(xRiRadioButton9);
    buttonGroup1.add(xRiRadioButton10);
    buttonGroup1.add(xRiRadioButton7);
    buttonGroup1.add(xRiRadioButton2);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JTabbedPane OBJ_28;
  private JPanel OBJ_29;
  private XRiComboBox A1IN9;
  private XRiComboBox A1IN25;
  private XRiComboBox A1RON;
  private XRiTextField A1ZP20;
  private XRiCheckBox A1IN13;
  private XRiCheckBox A1IN1;
  private XRiCheckBox A1TNC;
  private XRiCheckBox A1IN10;
  private JLabel OBJ_58_OBJ_58;
  private XRiCheckBox A1IN5;
  private XRiTextField A1ZP21;
  private JLabel OBJ_54_OBJ_54;
  private JLabel OBJ_59_OBJ_59;
  private JLabel OBJ_62_OBJ_62;
  private JLabel OBJ_66_OBJ_66;
  private JLabel OBJ_56_OBJ_56;
  private JLabel OBJ_64_OBJ_64;
  private XRiTextField A1NOP;
  private XRiTextField A1RST;
  private JLabel OBJ_32_OBJ_32;
  private JLabel LBZP21;
  private JLabel OBJ_91_OBJ_91;
  private XRiTextField A1QT1;
  private XRiTextField TTVAX;
  private XRiTextField A1PMM;
  private XRiTextField A1PMR;
  private XRiTextField A1TPF;
  private JPanel panel3;
  private JLabel OBJ_38_OBJ_38;
  private JLabel OBJ_43_OBJ_43;
  private JLabel OBJ_47_OBJ_47;
  private XRiTextField A1CNDX;
  private XRiTextField A1PDSL;
  private XRiTextField A1VOLL;
  private JLabel OBJ_45_OBJ_45;
  private JLabel OBJ_49_OBJ_49;
  private SNBoutonDetail OBJ_36;
  private XRiTextField A1UNL;
  private JLabel OBJ_37_OBJ_37;
  private XRiComboBox A1IN8;
  private JPanel panel4;
  private XRiTextField A1TOP1;
  private XRiTextField A1TOP2;
  private XRiTextField A1TOP3;
  private XRiTextField A1TOP4;
  private XRiTextField A1TOP5;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel OBJ_51_OBJ_51;
  private XRiTextField A1CNV;
  private XRiSpinner A1TVA;
  private XRiCheckBox A1IN27;
  private XRiComboBox A1IN31;
  private JLabel OBJ_58_OBJ_59;
  private JPanel panel7;
  private XRiRadioButton A1IN15;
  private XRiRadioButton A1IN15_1;
  private XRiRadioButton A1IN15_2;
  private XRiCheckBox A1IN33;
  private JLabel lbCCEREM;
  private XRiComboBox A1IN34;
  private JPanel OBJ_93;
  private JPanel panel5;
  private JLabel label2;
  private SNBoutonDetail OBJ_100;
  private XRiTextField A1NDP;
  private JLabel OBJ_102_OBJ_102;
  private XRiTextField A1OPA;
  private JLabel OBJ_105_OBJ_105;
  private XRiTextField A1QT6;
  private JLabel OBJ_105_OBJ_106;
  private JPanel panel6;
  private JLabel OBJ_103_OBJ_103;
  private XRiTextField A1IN18;
  private JLabel OBJ_107_OBJ_107;
  private XRiComboBox A1IN19;
  private JPanel OBJ_129;
  private XRiComboBox A1REA;
  private XRiComboBox A1IN16;
  private JLabel OBJ_156_OBJ_156;
  private JLabel OBJ_153_OBJ_153;
  private JLabel OBJ_154_OBJ_154;
  private JLabel OBJ_157_OBJ_157;
  private JLabel OBJ_136_OBJ_136;
  private JLabel OBJ_155_OBJ_155;
  private JLabel OBJ_134_OBJ_134;
  private JLabel OBJ_141_OBJ_141;
  private JLabel OBJ_151_OBJ_151;
  private XRiCalendrier A1PRDX;
  private XRiCalendrier A1PRFX;
  private XRiCalendrier A1P2DX;
  private XRiCalendrier A1P2FX;
  private JLabel OBJ_132_OBJ_132;
  private XRiTextField A1PERK;
  private XRiTextField A1PE2K;
  private JLabel OBJ_140_OBJ_140;
  private JLabel OBJ_137_OBJ_137;
  private JLabel OBJ_150_OBJ_150;
  private JLabel OBJ_152_OBJ_152;
  private XRiCalendrier A1DA1X;
  private XRiCalendrier A1DA2X;
  private XRiCalendrier A1DA3X;
  private RiZoneSortie W1FACT;
  private JLabel OBJ_157_OBJ_158;
  private RiZoneSortie A1DA6X;
  private JPanel OBJ_109;
  private XRiComboBox A1IN2;
  private JLabel OBJ_111_OBJ_111;
  private JLabel OBJ_119_OBJ_119;
  private JLabel OBJ_121_OBJ_121;
  private JLabel OBJ_128_OBJ_128;
  private SNBoutonDetail OBJ_125;
  private XRiTextField A1UNP;
  private XRiTextField A1SV1;
  private XRiTextField A1SV2;
  private XRiTextField A1IN11;
  private XRiTextField A1IN6;
  private XRiTextField A1IN7;
  private XRiTextField A1IN17;
  private JLabel label3;
  private XRiCheckBox A1IN26;
  private JComboBox cb_gestion;
  private JCheckBox ck_special;
  private JCheckBox ck_revient;
  private JPanel panel1;
  private JRadioButton xRiRadioButton8;
  private JRadioButton xRiRadioButton6;
  private JRadioButton xRiRadioButton4;
  private JRadioButton xRiRadioButton11;
  private JRadioButton xRiRadioButton3;
  private JRadioButton xRiRadioButton5;
  private JRadioButton xRiRadioButton1;
  private JRadioButton xRiRadioButton9;
  private JRadioButton xRiRadioButton10;
  private JRadioButton xRiRadioButton7;
  private JRadioButton xRiRadioButton2;
  private XRiTextField A1QT4R;
  private JLabel lbDelaisLTF;
  private JLabel lbDatepremierlivraison;
  private RiZoneSortie A1DA5X;
  private JLabel lbDatepremierlivraison2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_23;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private XRiTextField A1TSP;
  private XRiTextField A1IN12;
  private XRiTextField A1SPE;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_15;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private ButtonGroup buttonGroup2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
