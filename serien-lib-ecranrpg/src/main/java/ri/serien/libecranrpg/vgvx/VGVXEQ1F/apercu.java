
package ri.serien.libecranrpg.vgvx.VGVXEQ1F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class apercu extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private String chemin = null;
  private ImageIcon image = null;
  
  public apercu(JPanel panel, Lexical lex, iData iD, String chm) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    chemin = chm;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    
    image = new ImageIcon(chemin);
    if (image != null) {
      label1.setIcon(image);
    }
    else {
      label1.setText("IMAGE NON DISPONIBLE (" + chemin + ")");
    }
    
    // TODO Icones
    RETOUR.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Aperçu de l'étiquette : " + chemin.substring(chemin.length() - 14, chemin.length() - 4)));
  }
  
  public void getData() {
    label1.setIcon(null);
  }
  
  public void tuer() {
    getData();
    dispose();
  }
  
  public void reveiller() {
    setData();
    setVisible(true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    panel2 = new JPanel();
    RETOUR = new JButton();
    panel1 = new JPanel();
    label1 = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(910, 610));
    setAlwaysOnTop(true);
    setResizable(false);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(90, 90, 90));
      P_Centre.setPreferredSize(new Dimension(350, 415));
      P_Centre.setMinimumSize(new Dimension(350, 415));
      P_Centre.setName("P_Centre");
      
      // ======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setName("panel2");
        panel2.setLayout(null);
        
        // ---- RETOUR ----
        RETOUR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        RETOUR.setText("Retour");
        RETOUR.setFont(RETOUR.getFont().deriveFont(RETOUR.getFont().getStyle() | Font.BOLD, RETOUR.getFont().getSize() + 3f));
        RETOUR.setForeground(Color.black);
        RETOUR.setHorizontalAlignment(SwingConstants.LEADING);
        RETOUR.setIconTextGap(10);
        RETOUR.setName("RETOUR");
        RETOUR.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_10ActionPerformed(e);
          }
        });
        panel2.add(RETOUR);
        RETOUR.setBounds(740, 535, 150, 40);
        
        // ======== panel1 ========
        {
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- label1 ----
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setHorizontalTextPosition(SwingConstants.CENTER);
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 4f));
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(0, 0, 880, 520);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        panel2.add(panel1);
        panel1.setBounds(10, 10, 880, 520);
      }
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup().addComponent(panel2, GroupLayout.Alignment.TRAILING,
          GroupLayout.DEFAULT_SIZE, 902, Short.MAX_VALUE));
      P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup().addComponent(panel2, GroupLayout.Alignment.TRAILING,
          GroupLayout.DEFAULT_SIZE, 583, Short.MAX_VALUE));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JPanel panel2;
  private JButton RETOUR;
  private JPanel panel1;
  private JLabel label1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
