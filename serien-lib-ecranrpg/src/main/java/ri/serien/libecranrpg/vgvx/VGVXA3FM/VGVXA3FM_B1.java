
package ri.serien.libecranrpg.vgvx.VGVXA3FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGVXA3FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXA3FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LSL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL1@")).trim());
    LSL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL2@")).trim());
    LSL3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL3@")).trim());
    LSL4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL4@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    panel1.setVisible(lexique.isTrue("N62"));
    panel2.setVisible(lexique.isTrue("31"));
    panel3.setVisible(lexique.isTrue("32"));
    panel4.setVisible(lexique.isTrue("33"));
    panel5.setVisible(lexique.isTrue("34"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    p_bpresentation.setCodeEtablissement(WETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_133 = new JLabel();
    WETB = new XRiTextField();
    OBJ_134 = new JLabel();
    WMAG = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_122 = new JLabel();
    UTAS = new XRiTextField();
    OBJ_124 = new JLabel();
    UUCS = new XRiTextField();
    OBJ_126 = new JLabel();
    UCAP = new XRiTextField();
    separator5 = compFactory.createSeparator("");
    separator10 = compFactory.createSeparator(" ");
    panel2 = new JPanel();
    separator1 = compFactory.createSeparator("Description du lieu de stockage", SwingConstants.CENTER);
    separator2 = compFactory.createSeparator("Valeurs possibles", SwingConstants.CENTER);
    LSL1 = new RiZoneSortie();
    VSL101 = new XRiTextField();
    VSL102 = new XRiTextField();
    VSL103 = new XRiTextField();
    VSL104 = new XRiTextField();
    VSL105 = new XRiTextField();
    VSL106 = new XRiTextField();
    VSL107 = new XRiTextField();
    VSL108 = new XRiTextField();
    VSL109 = new XRiTextField();
    VSL110 = new XRiTextField();
    VSL111 = new XRiTextField();
    VSL112 = new XRiTextField();
    VSL113 = new XRiTextField();
    VSL114 = new XRiTextField();
    VSL115 = new XRiTextField();
    VSL116 = new XRiTextField();
    VSL117 = new XRiTextField();
    VSL118 = new XRiTextField();
    VSL119 = new XRiTextField();
    VSL120 = new XRiTextField();
    OBJ_38 = new JLabel();
    panel3 = new JPanel();
    separator3 = compFactory.createSeparator("Description du lieu de stockage", SwingConstants.CENTER);
    separator4 = compFactory.createSeparator("Valeurs possibles", SwingConstants.CENTER);
    VSL201 = new XRiTextField();
    VSL202 = new XRiTextField();
    VSL203 = new XRiTextField();
    VSL204 = new XRiTextField();
    VSL205 = new XRiTextField();
    VSL206 = new XRiTextField();
    VSL207 = new XRiTextField();
    VSL208 = new XRiTextField();
    VSL209 = new XRiTextField();
    VSL210 = new XRiTextField();
    VSL211 = new XRiTextField();
    VSL212 = new XRiTextField();
    VSL213 = new XRiTextField();
    VSL214 = new XRiTextField();
    VSL215 = new XRiTextField();
    VSL216 = new XRiTextField();
    VSL217 = new XRiTextField();
    VSL218 = new XRiTextField();
    VSL219 = new XRiTextField();
    VSL220 = new XRiTextField();
    OBJ_40 = new JLabel();
    LSL2 = new RiZoneSortie();
    panel4 = new JPanel();
    separator6 = compFactory.createSeparator("Valeurs possibles", SwingConstants.CENTER);
    separator7 = compFactory.createSeparator("Description du lieu de stockage", SwingConstants.CENTER);
    VSL301 = new XRiTextField();
    VSL302 = new XRiTextField();
    VSL303 = new XRiTextField();
    VSL304 = new XRiTextField();
    VSL305 = new XRiTextField();
    VSL306 = new XRiTextField();
    VSL307 = new XRiTextField();
    VSL308 = new XRiTextField();
    VSL309 = new XRiTextField();
    VSL310 = new XRiTextField();
    VSL311 = new XRiTextField();
    VSL312 = new XRiTextField();
    VSL313 = new XRiTextField();
    VSL314 = new XRiTextField();
    VSL315 = new XRiTextField();
    VSL316 = new XRiTextField();
    VSL317 = new XRiTextField();
    VSL318 = new XRiTextField();
    VSL319 = new XRiTextField();
    VSL320 = new XRiTextField();
    OBJ_42 = new JLabel();
    LSL3 = new RiZoneSortie();
    panel5 = new JPanel();
    separator8 = compFactory.createSeparator("Valeurs possibles", SwingConstants.CENTER);
    separator9 = compFactory.createSeparator("Description du lieu de stockage", SwingConstants.CENTER);
    VSL401 = new XRiTextField();
    VSL402 = new XRiTextField();
    VSL403 = new XRiTextField();
    VSL404 = new XRiTextField();
    VSL405 = new XRiTextField();
    VSL406 = new XRiTextField();
    VSL407 = new XRiTextField();
    VSL408 = new XRiTextField();
    VSL409 = new XRiTextField();
    VSL410 = new XRiTextField();
    VSL411 = new XRiTextField();
    VSL412 = new XRiTextField();
    VSL413 = new XRiTextField();
    VSL414 = new XRiTextField();
    VSL415 = new XRiTextField();
    VSL416 = new XRiTextField();
    VSL417 = new XRiTextField();
    VSL418 = new XRiTextField();
    VSL419 = new XRiTextField();
    VSL420 = new XRiTextField();
    OBJ_44 = new JLabel();
    LSL4 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_137 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("G\u00e9n\u00e9ration automatique d'adresses de stock");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_133 ----
          OBJ_133.setText("Etablissement");
          OBJ_133.setName("OBJ_133");
          p_tete_gauche.add(OBJ_133);
          OBJ_133.setBounds(5, 0, 93, 28);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(125, 0, 40, WETB.getPreferredSize().height);

          //---- OBJ_134 ----
          OBJ_134.setText("Magasin");
          OBJ_134.setName("OBJ_134");
          p_tete_gauche.add(OBJ_134);
          OBJ_134.setBounds(195, 0, 57, 28);

          //---- WMAG ----
          WMAG.setComponentPopupMenu(BTD);
          WMAG.setName("WMAG");
          p_tete_gauche.add(WMAG);
          WMAG.setBounds(265, 0, 30, WMAG.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_122 ----
            OBJ_122.setText("Type d'adresse de stockage");
            OBJ_122.setName("OBJ_122");
            panel1.add(OBJ_122);
            OBJ_122.setBounds(85, 505, 165, 28);

            //---- UTAS ----
            UTAS.setComponentPopupMenu(BTD);
            UTAS.setName("UTAS");
            panel1.add(UTAS);
            UTAS.setBounds(250, 505, 24, UTAS.getPreferredSize().height);

            //---- OBJ_124 ----
            OBJ_124.setText("Unit\u00e9 de conditionnement");
            OBJ_124.setName("OBJ_124");
            panel1.add(OBJ_124);
            OBJ_124.setBounds(350, 505, 185, 28);

            //---- UUCS ----
            UUCS.setComponentPopupMenu(BTD);
            UUCS.setName("UUCS");
            panel1.add(UUCS);
            UUCS.setBounds(538, 505, 34, UUCS.getPreferredSize().height);

            //---- OBJ_126 ----
            OBJ_126.setText("Capacit\u00e9");
            OBJ_126.setName("OBJ_126");
            panel1.add(OBJ_126);
            OBJ_126.setBounds(632, 505, 93, 28);

            //---- UCAP ----
            UCAP.setComponentPopupMenu(BTD);
            UCAP.setName("UCAP");
            panel1.add(UCAP);
            UCAP.setBounds(726, 505, 52, UCAP.getPreferredSize().height);

            //---- separator5 ----
            separator5.setName("separator5");
            panel1.add(separator5);
            separator5.setBounds(new Rectangle(new Point(50, 490), separator5.getPreferredSize()));

            //---- separator10 ----
            separator10.setName("separator10");
            panel1.add(separator10);
            separator10.setBounds(30, 480, 820, separator10.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- separator1 ----
              separator1.setName("separator1");
              panel2.add(separator1);
              separator1.setBounds(5, 5, 225, 25);

              //---- separator2 ----
              separator2.setName("separator2");
              panel2.add(separator2);
              separator2.setBounds(270, 5, 560, 25);

              //---- LSL1 ----
              LSL1.setText("@LSL1@");
              LSL1.setName("LSL1");
              panel2.add(LSL1);
              LSL1.setBounds(60, 45, 126, 20);

              //---- VSL101 ----
              VSL101.setComponentPopupMenu(BTD);
              VSL101.setName("VSL101");
              panel2.add(VSL101);
              VSL101.setBounds(325, 40, 40, VSL101.getPreferredSize().height);

              //---- VSL102 ----
              VSL102.setComponentPopupMenu(BTD);
              VSL102.setName("VSL102");
              panel2.add(VSL102);
              VSL102.setBounds(370, 40, 40, VSL102.getPreferredSize().height);

              //---- VSL103 ----
              VSL103.setComponentPopupMenu(BTD);
              VSL103.setName("VSL103");
              panel2.add(VSL103);
              VSL103.setBounds(420, 40, 40, VSL103.getPreferredSize().height);

              //---- VSL104 ----
              VSL104.setComponentPopupMenu(BTD);
              VSL104.setName("VSL104");
              panel2.add(VSL104);
              VSL104.setBounds(465, 40, 40, VSL104.getPreferredSize().height);

              //---- VSL105 ----
              VSL105.setComponentPopupMenu(BTD);
              VSL105.setName("VSL105");
              panel2.add(VSL105);
              VSL105.setBounds(515, 40, 40, VSL105.getPreferredSize().height);

              //---- VSL106 ----
              VSL106.setComponentPopupMenu(BTD);
              VSL106.setName("VSL106");
              panel2.add(VSL106);
              VSL106.setBounds(560, 40, 40, VSL106.getPreferredSize().height);

              //---- VSL107 ----
              VSL107.setComponentPopupMenu(BTD);
              VSL107.setName("VSL107");
              panel2.add(VSL107);
              VSL107.setBounds(605, 40, 40, VSL107.getPreferredSize().height);

              //---- VSL108 ----
              VSL108.setComponentPopupMenu(BTD);
              VSL108.setName("VSL108");
              panel2.add(VSL108);
              VSL108.setBounds(655, 40, 40, VSL108.getPreferredSize().height);

              //---- VSL109 ----
              VSL109.setComponentPopupMenu(BTD);
              VSL109.setName("VSL109");
              panel2.add(VSL109);
              VSL109.setBounds(700, 40, 40, VSL109.getPreferredSize().height);

              //---- VSL110 ----
              VSL110.setComponentPopupMenu(BTD);
              VSL110.setName("VSL110");
              panel2.add(VSL110);
              VSL110.setBounds(750, 40, 40, VSL110.getPreferredSize().height);

              //---- VSL111 ----
              VSL111.setComponentPopupMenu(BTD);
              VSL111.setName("VSL111");
              panel2.add(VSL111);
              VSL111.setBounds(325, 70, 40, VSL111.getPreferredSize().height);

              //---- VSL112 ----
              VSL112.setComponentPopupMenu(BTD);
              VSL112.setName("VSL112");
              panel2.add(VSL112);
              VSL112.setBounds(370, 70, 40, VSL112.getPreferredSize().height);

              //---- VSL113 ----
              VSL113.setComponentPopupMenu(BTD);
              VSL113.setName("VSL113");
              panel2.add(VSL113);
              VSL113.setBounds(420, 70, 40, VSL113.getPreferredSize().height);

              //---- VSL114 ----
              VSL114.setComponentPopupMenu(BTD);
              VSL114.setName("VSL114");
              panel2.add(VSL114);
              VSL114.setBounds(465, 70, 40, VSL114.getPreferredSize().height);

              //---- VSL115 ----
              VSL115.setComponentPopupMenu(BTD);
              VSL115.setName("VSL115");
              panel2.add(VSL115);
              VSL115.setBounds(515, 70, 40, VSL115.getPreferredSize().height);

              //---- VSL116 ----
              VSL116.setComponentPopupMenu(BTD);
              VSL116.setName("VSL116");
              panel2.add(VSL116);
              VSL116.setBounds(560, 70, 40, VSL116.getPreferredSize().height);

              //---- VSL117 ----
              VSL117.setComponentPopupMenu(BTD);
              VSL117.setName("VSL117");
              panel2.add(VSL117);
              VSL117.setBounds(605, 70, 40, VSL117.getPreferredSize().height);

              //---- VSL118 ----
              VSL118.setComponentPopupMenu(BTD);
              VSL118.setName("VSL118");
              panel2.add(VSL118);
              VSL118.setBounds(655, 70, 40, VSL118.getPreferredSize().height);

              //---- VSL119 ----
              VSL119.setComponentPopupMenu(BTD);
              VSL119.setName("VSL119");
              panel2.add(VSL119);
              VSL119.setBounds(700, 70, 40, VSL119.getPreferredSize().height);

              //---- VSL120 ----
              VSL120.setComponentPopupMenu(BTD);
              VSL120.setName("VSL120");
              panel2.add(VSL120);
              VSL120.setBounds(750, 70, 40, VSL120.getPreferredSize().height);

              //---- OBJ_38 ----
              OBJ_38.setText("1");
              OBJ_38.setFont(OBJ_38.getFont().deriveFont(OBJ_38.getFont().getStyle() | Font.BOLD));
              OBJ_38.setName("OBJ_38");
              panel2.add(OBJ_38);
              OBJ_38.setBounds(35, 45, 20, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(25, 5, 840, 115);

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- separator3 ----
              separator3.setName("separator3");
              panel3.add(separator3);
              separator3.setBounds(5, 5, 225, 25);

              //---- separator4 ----
              separator4.setName("separator4");
              panel3.add(separator4);
              separator4.setBounds(270, 5, 560, 25);

              //---- VSL201 ----
              VSL201.setComponentPopupMenu(BTD);
              VSL201.setName("VSL201");
              panel3.add(VSL201);
              VSL201.setBounds(325, 40, 40, VSL201.getPreferredSize().height);

              //---- VSL202 ----
              VSL202.setComponentPopupMenu(BTD);
              VSL202.setName("VSL202");
              panel3.add(VSL202);
              VSL202.setBounds(370, 40, 40, VSL202.getPreferredSize().height);

              //---- VSL203 ----
              VSL203.setComponentPopupMenu(BTD);
              VSL203.setName("VSL203");
              panel3.add(VSL203);
              VSL203.setBounds(420, 40, 40, VSL203.getPreferredSize().height);

              //---- VSL204 ----
              VSL204.setComponentPopupMenu(BTD);
              VSL204.setName("VSL204");
              panel3.add(VSL204);
              VSL204.setBounds(465, 40, 40, VSL204.getPreferredSize().height);

              //---- VSL205 ----
              VSL205.setComponentPopupMenu(BTD);
              VSL205.setName("VSL205");
              panel3.add(VSL205);
              VSL205.setBounds(510, 40, 40, VSL205.getPreferredSize().height);

              //---- VSL206 ----
              VSL206.setComponentPopupMenu(BTD);
              VSL206.setName("VSL206");
              panel3.add(VSL206);
              VSL206.setBounds(560, 40, 40, VSL206.getPreferredSize().height);

              //---- VSL207 ----
              VSL207.setComponentPopupMenu(BTD);
              VSL207.setName("VSL207");
              panel3.add(VSL207);
              VSL207.setBounds(605, 40, 40, VSL207.getPreferredSize().height);

              //---- VSL208 ----
              VSL208.setComponentPopupMenu(BTD);
              VSL208.setName("VSL208");
              panel3.add(VSL208);
              VSL208.setBounds(655, 40, 40, VSL208.getPreferredSize().height);

              //---- VSL209 ----
              VSL209.setComponentPopupMenu(BTD);
              VSL209.setName("VSL209");
              panel3.add(VSL209);
              VSL209.setBounds(700, 40, 40, VSL209.getPreferredSize().height);

              //---- VSL210 ----
              VSL210.setComponentPopupMenu(BTD);
              VSL210.setName("VSL210");
              panel3.add(VSL210);
              VSL210.setBounds(745, 40, 40, VSL210.getPreferredSize().height);

              //---- VSL211 ----
              VSL211.setComponentPopupMenu(BTD);
              VSL211.setName("VSL211");
              panel3.add(VSL211);
              VSL211.setBounds(325, 70, 40, VSL211.getPreferredSize().height);

              //---- VSL212 ----
              VSL212.setComponentPopupMenu(BTD);
              VSL212.setName("VSL212");
              panel3.add(VSL212);
              VSL212.setBounds(370, 70, 40, VSL212.getPreferredSize().height);

              //---- VSL213 ----
              VSL213.setComponentPopupMenu(BTD);
              VSL213.setName("VSL213");
              panel3.add(VSL213);
              VSL213.setBounds(420, 70, 40, VSL213.getPreferredSize().height);

              //---- VSL214 ----
              VSL214.setComponentPopupMenu(BTD);
              VSL214.setName("VSL214");
              panel3.add(VSL214);
              VSL214.setBounds(465, 70, 40, VSL214.getPreferredSize().height);

              //---- VSL215 ----
              VSL215.setComponentPopupMenu(BTD);
              VSL215.setName("VSL215");
              panel3.add(VSL215);
              VSL215.setBounds(510, 70, 40, VSL215.getPreferredSize().height);

              //---- VSL216 ----
              VSL216.setComponentPopupMenu(BTD);
              VSL216.setName("VSL216");
              panel3.add(VSL216);
              VSL216.setBounds(560, 70, 40, VSL216.getPreferredSize().height);

              //---- VSL217 ----
              VSL217.setComponentPopupMenu(BTD);
              VSL217.setName("VSL217");
              panel3.add(VSL217);
              VSL217.setBounds(605, 70, 40, VSL217.getPreferredSize().height);

              //---- VSL218 ----
              VSL218.setComponentPopupMenu(BTD);
              VSL218.setName("VSL218");
              panel3.add(VSL218);
              VSL218.setBounds(655, 70, 40, VSL218.getPreferredSize().height);

              //---- VSL219 ----
              VSL219.setComponentPopupMenu(BTD);
              VSL219.setName("VSL219");
              panel3.add(VSL219);
              VSL219.setBounds(700, 70, 40, VSL219.getPreferredSize().height);

              //---- VSL220 ----
              VSL220.setComponentPopupMenu(BTD);
              VSL220.setName("VSL220");
              panel3.add(VSL220);
              VSL220.setBounds(745, 70, 40, VSL220.getPreferredSize().height);

              //---- OBJ_40 ----
              OBJ_40.setText("2");
              OBJ_40.setFont(OBJ_40.getFont().deriveFont(OBJ_40.getFont().getStyle() | Font.BOLD));
              OBJ_40.setName("OBJ_40");
              panel3.add(OBJ_40);
              OBJ_40.setBounds(35, 45, 20, 20);

              //---- LSL2 ----
              LSL2.setText("@LSL2@");
              LSL2.setName("LSL2");
              panel3.add(LSL2);
              LSL2.setBounds(60, 45, 126, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel3);
            panel3.setBounds(25, 125, 840, 115);

            //======== panel4 ========
            {
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- separator6 ----
              separator6.setName("separator6");
              panel4.add(separator6);
              separator6.setBounds(270, 5, 560, 25);

              //---- separator7 ----
              separator7.setName("separator7");
              panel4.add(separator7);
              separator7.setBounds(5, 5, 225, 25);

              //---- VSL301 ----
              VSL301.setComponentPopupMenu(BTD);
              VSL301.setName("VSL301");
              panel4.add(VSL301);
              VSL301.setBounds(325, 35, 40, VSL301.getPreferredSize().height);

              //---- VSL302 ----
              VSL302.setComponentPopupMenu(BTD);
              VSL302.setName("VSL302");
              panel4.add(VSL302);
              VSL302.setBounds(370, 35, 40, VSL302.getPreferredSize().height);

              //---- VSL303 ----
              VSL303.setComponentPopupMenu(BTD);
              VSL303.setName("VSL303");
              panel4.add(VSL303);
              VSL303.setBounds(420, 35, 40, VSL303.getPreferredSize().height);

              //---- VSL304 ----
              VSL304.setComponentPopupMenu(BTD);
              VSL304.setName("VSL304");
              panel4.add(VSL304);
              VSL304.setBounds(465, 35, 40, VSL304.getPreferredSize().height);

              //---- VSL305 ----
              VSL305.setComponentPopupMenu(BTD);
              VSL305.setName("VSL305");
              panel4.add(VSL305);
              VSL305.setBounds(510, 35, 40, VSL305.getPreferredSize().height);

              //---- VSL306 ----
              VSL306.setComponentPopupMenu(BTD);
              VSL306.setName("VSL306");
              panel4.add(VSL306);
              VSL306.setBounds(560, 35, 40, VSL306.getPreferredSize().height);

              //---- VSL307 ----
              VSL307.setComponentPopupMenu(BTD);
              VSL307.setName("VSL307");
              panel4.add(VSL307);
              VSL307.setBounds(605, 35, 40, VSL307.getPreferredSize().height);

              //---- VSL308 ----
              VSL308.setComponentPopupMenu(BTD);
              VSL308.setName("VSL308");
              panel4.add(VSL308);
              VSL308.setBounds(655, 35, 40, VSL308.getPreferredSize().height);

              //---- VSL309 ----
              VSL309.setComponentPopupMenu(BTD);
              VSL309.setName("VSL309");
              panel4.add(VSL309);
              VSL309.setBounds(700, 35, 40, VSL309.getPreferredSize().height);

              //---- VSL310 ----
              VSL310.setComponentPopupMenu(BTD);
              VSL310.setName("VSL310");
              panel4.add(VSL310);
              VSL310.setBounds(745, 35, 40, VSL310.getPreferredSize().height);

              //---- VSL311 ----
              VSL311.setComponentPopupMenu(BTD);
              VSL311.setName("VSL311");
              panel4.add(VSL311);
              VSL311.setBounds(325, 65, 40, VSL311.getPreferredSize().height);

              //---- VSL312 ----
              VSL312.setComponentPopupMenu(BTD);
              VSL312.setName("VSL312");
              panel4.add(VSL312);
              VSL312.setBounds(370, 65, 40, VSL312.getPreferredSize().height);

              //---- VSL313 ----
              VSL313.setComponentPopupMenu(BTD);
              VSL313.setName("VSL313");
              panel4.add(VSL313);
              VSL313.setBounds(420, 65, 40, VSL313.getPreferredSize().height);

              //---- VSL314 ----
              VSL314.setComponentPopupMenu(BTD);
              VSL314.setName("VSL314");
              panel4.add(VSL314);
              VSL314.setBounds(465, 65, 40, VSL314.getPreferredSize().height);

              //---- VSL315 ----
              VSL315.setComponentPopupMenu(BTD);
              VSL315.setName("VSL315");
              panel4.add(VSL315);
              VSL315.setBounds(510, 65, 40, VSL315.getPreferredSize().height);

              //---- VSL316 ----
              VSL316.setComponentPopupMenu(BTD);
              VSL316.setName("VSL316");
              panel4.add(VSL316);
              VSL316.setBounds(560, 65, 40, VSL316.getPreferredSize().height);

              //---- VSL317 ----
              VSL317.setComponentPopupMenu(BTD);
              VSL317.setName("VSL317");
              panel4.add(VSL317);
              VSL317.setBounds(605, 65, 40, VSL317.getPreferredSize().height);

              //---- VSL318 ----
              VSL318.setComponentPopupMenu(BTD);
              VSL318.setName("VSL318");
              panel4.add(VSL318);
              VSL318.setBounds(655, 65, 40, VSL318.getPreferredSize().height);

              //---- VSL319 ----
              VSL319.setComponentPopupMenu(BTD);
              VSL319.setName("VSL319");
              panel4.add(VSL319);
              VSL319.setBounds(700, 65, 40, VSL319.getPreferredSize().height);

              //---- VSL320 ----
              VSL320.setComponentPopupMenu(BTD);
              VSL320.setName("VSL320");
              panel4.add(VSL320);
              VSL320.setBounds(745, 65, 40, VSL320.getPreferredSize().height);

              //---- OBJ_42 ----
              OBJ_42.setText("3");
              OBJ_42.setFont(OBJ_42.getFont().deriveFont(OBJ_42.getFont().getStyle() | Font.BOLD));
              OBJ_42.setName("OBJ_42");
              panel4.add(OBJ_42);
              OBJ_42.setBounds(35, 40, 20, 20);

              //---- LSL3 ----
              LSL3.setText("@LSL3@");
              LSL3.setName("LSL3");
              panel4.add(LSL3);
              LSL3.setBounds(60, 40, 126, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel4);
            panel4.setBounds(25, 245, 840, 115);

            //======== panel5 ========
            {
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- separator8 ----
              separator8.setName("separator8");
              panel5.add(separator8);
              separator8.setBounds(270, 5, 560, 25);

              //---- separator9 ----
              separator9.setName("separator9");
              panel5.add(separator9);
              separator9.setBounds(5, 5, 225, 25);

              //---- VSL401 ----
              VSL401.setComponentPopupMenu(BTD);
              VSL401.setName("VSL401");
              panel5.add(VSL401);
              VSL401.setBounds(325, 40, 40, VSL401.getPreferredSize().height);

              //---- VSL402 ----
              VSL402.setComponentPopupMenu(BTD);
              VSL402.setName("VSL402");
              panel5.add(VSL402);
              VSL402.setBounds(370, 40, 40, VSL402.getPreferredSize().height);

              //---- VSL403 ----
              VSL403.setComponentPopupMenu(BTD);
              VSL403.setName("VSL403");
              panel5.add(VSL403);
              VSL403.setBounds(415, 40, 40, VSL403.getPreferredSize().height);

              //---- VSL404 ----
              VSL404.setComponentPopupMenu(BTD);
              VSL404.setName("VSL404");
              panel5.add(VSL404);
              VSL404.setBounds(465, 40, 40, VSL404.getPreferredSize().height);

              //---- VSL405 ----
              VSL405.setComponentPopupMenu(BTD);
              VSL405.setName("VSL405");
              panel5.add(VSL405);
              VSL405.setBounds(510, 40, 40, VSL405.getPreferredSize().height);

              //---- VSL406 ----
              VSL406.setComponentPopupMenu(BTD);
              VSL406.setName("VSL406");
              panel5.add(VSL406);
              VSL406.setBounds(560, 40, 40, VSL406.getPreferredSize().height);

              //---- VSL407 ----
              VSL407.setComponentPopupMenu(BTD);
              VSL407.setName("VSL407");
              panel5.add(VSL407);
              VSL407.setBounds(605, 40, 40, VSL407.getPreferredSize().height);

              //---- VSL408 ----
              VSL408.setComponentPopupMenu(BTD);
              VSL408.setName("VSL408");
              panel5.add(VSL408);
              VSL408.setBounds(650, 40, 40, VSL408.getPreferredSize().height);

              //---- VSL409 ----
              VSL409.setComponentPopupMenu(BTD);
              VSL409.setName("VSL409");
              panel5.add(VSL409);
              VSL409.setBounds(700, 40, 40, VSL409.getPreferredSize().height);

              //---- VSL410 ----
              VSL410.setComponentPopupMenu(BTD);
              VSL410.setName("VSL410");
              panel5.add(VSL410);
              VSL410.setBounds(745, 40, 40, VSL410.getPreferredSize().height);

              //---- VSL411 ----
              VSL411.setComponentPopupMenu(BTD);
              VSL411.setName("VSL411");
              panel5.add(VSL411);
              VSL411.setBounds(325, 70, 40, VSL411.getPreferredSize().height);

              //---- VSL412 ----
              VSL412.setComponentPopupMenu(BTD);
              VSL412.setName("VSL412");
              panel5.add(VSL412);
              VSL412.setBounds(370, 70, 40, VSL412.getPreferredSize().height);

              //---- VSL413 ----
              VSL413.setComponentPopupMenu(BTD);
              VSL413.setName("VSL413");
              panel5.add(VSL413);
              VSL413.setBounds(415, 70, 40, VSL413.getPreferredSize().height);

              //---- VSL414 ----
              VSL414.setComponentPopupMenu(BTD);
              VSL414.setName("VSL414");
              panel5.add(VSL414);
              VSL414.setBounds(465, 70, 40, VSL414.getPreferredSize().height);

              //---- VSL415 ----
              VSL415.setComponentPopupMenu(BTD);
              VSL415.setName("VSL415");
              panel5.add(VSL415);
              VSL415.setBounds(510, 70, 40, VSL415.getPreferredSize().height);

              //---- VSL416 ----
              VSL416.setComponentPopupMenu(BTD);
              VSL416.setName("VSL416");
              panel5.add(VSL416);
              VSL416.setBounds(560, 70, 40, VSL416.getPreferredSize().height);

              //---- VSL417 ----
              VSL417.setComponentPopupMenu(BTD);
              VSL417.setName("VSL417");
              panel5.add(VSL417);
              VSL417.setBounds(605, 70, 40, VSL417.getPreferredSize().height);

              //---- VSL418 ----
              VSL418.setComponentPopupMenu(BTD);
              VSL418.setName("VSL418");
              panel5.add(VSL418);
              VSL418.setBounds(650, 70, 40, VSL418.getPreferredSize().height);

              //---- VSL419 ----
              VSL419.setComponentPopupMenu(BTD);
              VSL419.setName("VSL419");
              panel5.add(VSL419);
              VSL419.setBounds(700, 70, 40, VSL419.getPreferredSize().height);

              //---- VSL420 ----
              VSL420.setComponentPopupMenu(BTD);
              VSL420.setName("VSL420");
              panel5.add(VSL420);
              VSL420.setBounds(745, 70, 40, VSL420.getPreferredSize().height);

              //---- OBJ_44 ----
              OBJ_44.setText("4");
              OBJ_44.setFont(OBJ_44.getFont().deriveFont(OBJ_44.getFont().getStyle() | Font.BOLD));
              OBJ_44.setName("OBJ_44");
              panel5.add(OBJ_44);
              OBJ_44.setBounds(35, 45, 20, 20);

              //---- LSL4 ----
              LSL4.setText("@LSL4@");
              LSL4.setName("LSL4");
              panel5.add(LSL4);
              LSL4.setBounds(60, 45, 126, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel5);
            panel5.setBounds(25, 365, 840, 115);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_137 ----
    OBJ_137.setText("<html>Valeurs possibles&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</html>");
    OBJ_137.setHorizontalAlignment(SwingConstants.LEFT);
    OBJ_137.setName("OBJ_137");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_133;
  private XRiTextField WETB;
  private JLabel OBJ_134;
  private XRiTextField WMAG;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_122;
  private XRiTextField UTAS;
  private JLabel OBJ_124;
  private XRiTextField UUCS;
  private JLabel OBJ_126;
  private XRiTextField UCAP;
  private JComponent separator5;
  private JComponent separator10;
  private JPanel panel2;
  private JComponent separator1;
  private JComponent separator2;
  private RiZoneSortie LSL1;
  private XRiTextField VSL101;
  private XRiTextField VSL102;
  private XRiTextField VSL103;
  private XRiTextField VSL104;
  private XRiTextField VSL105;
  private XRiTextField VSL106;
  private XRiTextField VSL107;
  private XRiTextField VSL108;
  private XRiTextField VSL109;
  private XRiTextField VSL110;
  private XRiTextField VSL111;
  private XRiTextField VSL112;
  private XRiTextField VSL113;
  private XRiTextField VSL114;
  private XRiTextField VSL115;
  private XRiTextField VSL116;
  private XRiTextField VSL117;
  private XRiTextField VSL118;
  private XRiTextField VSL119;
  private XRiTextField VSL120;
  private JLabel OBJ_38;
  private JPanel panel3;
  private JComponent separator3;
  private JComponent separator4;
  private XRiTextField VSL201;
  private XRiTextField VSL202;
  private XRiTextField VSL203;
  private XRiTextField VSL204;
  private XRiTextField VSL205;
  private XRiTextField VSL206;
  private XRiTextField VSL207;
  private XRiTextField VSL208;
  private XRiTextField VSL209;
  private XRiTextField VSL210;
  private XRiTextField VSL211;
  private XRiTextField VSL212;
  private XRiTextField VSL213;
  private XRiTextField VSL214;
  private XRiTextField VSL215;
  private XRiTextField VSL216;
  private XRiTextField VSL217;
  private XRiTextField VSL218;
  private XRiTextField VSL219;
  private XRiTextField VSL220;
  private JLabel OBJ_40;
  private RiZoneSortie LSL2;
  private JPanel panel4;
  private JComponent separator6;
  private JComponent separator7;
  private XRiTextField VSL301;
  private XRiTextField VSL302;
  private XRiTextField VSL303;
  private XRiTextField VSL304;
  private XRiTextField VSL305;
  private XRiTextField VSL306;
  private XRiTextField VSL307;
  private XRiTextField VSL308;
  private XRiTextField VSL309;
  private XRiTextField VSL310;
  private XRiTextField VSL311;
  private XRiTextField VSL312;
  private XRiTextField VSL313;
  private XRiTextField VSL314;
  private XRiTextField VSL315;
  private XRiTextField VSL316;
  private XRiTextField VSL317;
  private XRiTextField VSL318;
  private XRiTextField VSL319;
  private XRiTextField VSL320;
  private JLabel OBJ_42;
  private RiZoneSortie LSL3;
  private JPanel panel5;
  private JComponent separator8;
  private JComponent separator9;
  private XRiTextField VSL401;
  private XRiTextField VSL402;
  private XRiTextField VSL403;
  private XRiTextField VSL404;
  private XRiTextField VSL405;
  private XRiTextField VSL406;
  private XRiTextField VSL407;
  private XRiTextField VSL408;
  private XRiTextField VSL409;
  private XRiTextField VSL410;
  private XRiTextField VSL411;
  private XRiTextField VSL412;
  private XRiTextField VSL413;
  private XRiTextField VSL414;
  private XRiTextField VSL415;
  private XRiTextField VSL416;
  private XRiTextField VSL417;
  private XRiTextField VSL418;
  private XRiTextField VSL419;
  private XRiTextField VSL420;
  private JLabel OBJ_44;
  private RiZoneSortie LSL4;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JLabel OBJ_137;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
