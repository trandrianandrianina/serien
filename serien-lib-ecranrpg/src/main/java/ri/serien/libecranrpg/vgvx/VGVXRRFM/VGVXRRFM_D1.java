
package ri.serien.libecranrpg.vgvx.VGVXRRFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVXRRFM_D1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXRRFM_D1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    setTitle("Restrictions type d'extension");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LIB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB01@")).trim());
    LIB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB02@")).trim());
    LIB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB03@")).trim());
    LIB4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB04@")).trim());
    LIB5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB05@")).trim());
    LIB6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB06@")).trim());
    LIB7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB07@")).trim());
    LIB8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB08@")).trim());
    LIB9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB09@")).trim());
    LIB10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB10@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    LIB1.setVisible(!LIB1.getText().trim().equals(""));
    LIB2.setVisible(!LIB2.getText().trim().equals(""));
    LIB3.setVisible(!LIB3.getText().trim().equals(""));
    LIB4.setVisible(!LIB4.getText().trim().equals(""));
    LIB5.setVisible(!LIB5.getText().trim().equals(""));
    LIB6.setVisible(!LIB6.getText().trim().equals(""));
    LIB7.setVisible(!LIB7.getText().trim().equals(""));
    LIB8.setVisible(!LIB8.getText().trim().equals(""));
    LIB9.setVisible(!LIB9.getText().trim().equals(""));
    LIB10.setVisible(!LIB10.getText().trim().equals(""));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    LIB1 = new RiZoneSortie();
    LIB2 = new RiZoneSortie();
    LIB3 = new RiZoneSortie();
    LIB4 = new RiZoneSortie();
    LIB5 = new RiZoneSortie();
    LIB6 = new RiZoneSortie();
    LIB7 = new RiZoneSortie();
    LIB8 = new RiZoneSortie();
    LIB9 = new RiZoneSortie();
    LIB10 = new RiZoneSortie();
    CNF01 = new XRiTextField();
    CNF02 = new XRiTextField();
    CNF03 = new XRiTextField();
    CNF04 = new XRiTextField();
    CNF05 = new XRiTextField();
    CNF06 = new XRiTextField();
    CNF07 = new XRiTextField();
    CNF08 = new XRiTextField();
    CNF09 = new XRiTextField();
    CNF10 = new XRiTextField();
    panel1 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    CND01 = new XRiTextField();
    CND02 = new XRiTextField();
    CND03 = new XRiTextField();
    CND04 = new XRiTextField();
    CND05 = new XRiTextField();
    CND06 = new XRiTextField();
    CND07 = new XRiTextField();
    CND08 = new XRiTextField();
    CND09 = new XRiTextField();
    CND10 = new XRiTextField();
    CAD01 = new XRiTextField();
    CAD02 = new XRiTextField();
    CAD03 = new XRiTextField();
    CAD04 = new XRiTextField();
    CAD05 = new XRiTextField();
    CAD06 = new XRiTextField();
    CAD07 = new XRiTextField();
    CAD08 = new XRiTextField();
    CAD09 = new XRiTextField();
    CAD10 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    label18 = new JLabel();
    label19 = new JLabel();
    label20 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(920, 355));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("S\u00e9lections suppl\u00e9mentaires de"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- LIB1 ----
          LIB1.setText("@LIB01@");
          LIB1.setName("LIB1");
          panel2.add(LIB1);
          LIB1.setBounds(25, 40, 300, LIB1.getPreferredSize().height);

          //---- LIB2 ----
          LIB2.setText("@LIB02@");
          LIB2.setName("LIB2");
          panel2.add(LIB2);
          LIB2.setBounds(25, 65, 300, LIB2.getPreferredSize().height);

          //---- LIB3 ----
          LIB3.setText("@LIB03@");
          LIB3.setName("LIB3");
          panel2.add(LIB3);
          LIB3.setBounds(25, 90, 300, LIB3.getPreferredSize().height);

          //---- LIB4 ----
          LIB4.setText("@LIB04@");
          LIB4.setName("LIB4");
          panel2.add(LIB4);
          LIB4.setBounds(25, 115, 300, LIB4.getPreferredSize().height);

          //---- LIB5 ----
          LIB5.setText("@LIB05@");
          LIB5.setName("LIB5");
          panel2.add(LIB5);
          LIB5.setBounds(25, 140, 300, LIB5.getPreferredSize().height);

          //---- LIB6 ----
          LIB6.setText("@LIB06@");
          LIB6.setName("LIB6");
          panel2.add(LIB6);
          LIB6.setBounds(25, 165, 300, LIB6.getPreferredSize().height);

          //---- LIB7 ----
          LIB7.setText("@LIB07@");
          LIB7.setName("LIB7");
          panel2.add(LIB7);
          LIB7.setBounds(25, 190, 300, LIB7.getPreferredSize().height);

          //---- LIB8 ----
          LIB8.setText("@LIB08@");
          LIB8.setName("LIB8");
          panel2.add(LIB8);
          LIB8.setBounds(25, 215, 300, LIB8.getPreferredSize().height);

          //---- LIB9 ----
          LIB9.setText("@LIB09@");
          LIB9.setName("LIB9");
          panel2.add(LIB9);
          LIB9.setBounds(25, 240, 300, LIB9.getPreferredSize().height);

          //---- LIB10 ----
          LIB10.setText("@LIB10@");
          LIB10.setName("LIB10");
          panel2.add(LIB10);
          LIB10.setBounds(25, 265, 300, LIB10.getPreferredSize().height);

          //---- CNF01 ----
          CNF01.setName("CNF01");
          panel2.add(CNF01);
          CNF01.setBounds(541, 38, 100, CNF01.getPreferredSize().height);

          //---- CNF02 ----
          CNF02.setName("CNF02");
          panel2.add(CNF02);
          CNF02.setBounds(542, 63, 100, CNF02.getPreferredSize().height);

          //---- CNF03 ----
          CNF03.setName("CNF03");
          panel2.add(CNF03);
          CNF03.setBounds(542, 88, 100, CNF03.getPreferredSize().height);

          //---- CNF04 ----
          CNF04.setName("CNF04");
          panel2.add(CNF04);
          CNF04.setBounds(542, 113, 100, CNF04.getPreferredSize().height);

          //---- CNF05 ----
          CNF05.setName("CNF05");
          panel2.add(CNF05);
          CNF05.setBounds(542, 138, 100, CNF05.getPreferredSize().height);

          //---- CNF06 ----
          CNF06.setName("CNF06");
          panel2.add(CNF06);
          CNF06.setBounds(542, 163, 100, CNF06.getPreferredSize().height);

          //---- CNF07 ----
          CNF07.setName("CNF07");
          panel2.add(CNF07);
          CNF07.setBounds(542, 188, 100, CNF07.getPreferredSize().height);

          //---- CNF08 ----
          CNF08.setName("CNF08");
          panel2.add(CNF08);
          CNF08.setBounds(542, 213, 100, CNF08.getPreferredSize().height);

          //---- CNF09 ----
          CNF09.setName("CNF09");
          panel2.add(CNF09);
          CNF09.setBounds(542, 238, 100, CNF09.getPreferredSize().height);

          //---- CNF10 ----
          CNF10.setName("CNF10");
          panel2.add(CNF10);
          CNF10.setBounds(542, 263, 100, CNF10.getPreferredSize().height);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(5, 5, 25, 115);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(5, 143, 25, 115);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          panel2.add(panel1);
          panel1.setBounds(645, 33, 40, 265);

          //---- CND01 ----
          CND01.setName("CND01");
          panel2.add(CND01);
          CND01.setBounds(383, 38, 100, CND01.getPreferredSize().height);

          //---- CND02 ----
          CND02.setName("CND02");
          panel2.add(CND02);
          CND02.setBounds(383, 63, 100, CND02.getPreferredSize().height);

          //---- CND03 ----
          CND03.setName("CND03");
          panel2.add(CND03);
          CND03.setBounds(383, 88, 100, CND03.getPreferredSize().height);

          //---- CND04 ----
          CND04.setName("CND04");
          panel2.add(CND04);
          CND04.setBounds(383, 113, 100, CND04.getPreferredSize().height);

          //---- CND05 ----
          CND05.setName("CND05");
          panel2.add(CND05);
          CND05.setBounds(383, 138, 100, CND05.getPreferredSize().height);

          //---- CND06 ----
          CND06.setName("CND06");
          panel2.add(CND06);
          CND06.setBounds(383, 163, 100, CND06.getPreferredSize().height);

          //---- CND07 ----
          CND07.setName("CND07");
          panel2.add(CND07);
          CND07.setBounds(383, 188, 100, CND07.getPreferredSize().height);

          //---- CND08 ----
          CND08.setName("CND08");
          panel2.add(CND08);
          CND08.setBounds(383, 213, 100, CND08.getPreferredSize().height);

          //---- CND09 ----
          CND09.setName("CND09");
          panel2.add(CND09);
          CND09.setBounds(383, 238, 100, CND09.getPreferredSize().height);

          //---- CND10 ----
          CND10.setName("CND10");
          panel2.add(CND10);
          CND10.setBounds(383, 263, 100, CND10.getPreferredSize().height);

          //---- CAD01 ----
          CAD01.setName("CAD01");
          panel2.add(CAD01);
          CAD01.setBounds(330, 38, 210, CAD01.getPreferredSize().height);

          //---- CAD02 ----
          CAD02.setName("CAD02");
          panel2.add(CAD02);
          CAD02.setBounds(330, 63, 210, CAD02.getPreferredSize().height);

          //---- CAD03 ----
          CAD03.setName("CAD03");
          panel2.add(CAD03);
          CAD03.setBounds(330, 88, 210, CAD03.getPreferredSize().height);

          //---- CAD04 ----
          CAD04.setName("CAD04");
          panel2.add(CAD04);
          CAD04.setBounds(330, 113, 210, CAD04.getPreferredSize().height);

          //---- CAD05 ----
          CAD05.setName("CAD05");
          panel2.add(CAD05);
          CAD05.setBounds(330, 138, 210, CAD05.getPreferredSize().height);

          //---- CAD06 ----
          CAD06.setName("CAD06");
          panel2.add(CAD06);
          CAD06.setBounds(330, 163, 210, CAD06.getPreferredSize().height);

          //---- CAD07 ----
          CAD07.setName("CAD07");
          panel2.add(CAD07);
          CAD07.setBounds(330, 188, 210, CAD07.getPreferredSize().height);

          //---- CAD08 ----
          CAD08.setName("CAD08");
          panel2.add(CAD08);
          CAD08.setBounds(330, 213, 210, CAD08.getPreferredSize().height);

          //---- CAD09 ----
          CAD09.setName("CAD09");
          panel2.add(CAD09);
          CAD09.setBounds(330, 238, 210, CAD09.getPreferredSize().height);

          //---- CAD10 ----
          CAD10.setName("CAD10");
          panel2.add(CAD10);
          CAD10.setBounds(330, 263, 210, CAD10.getPreferredSize().height);

          //---- label1 ----
          label1.setText("de");
          label1.setName("label1");
          panel2.add(label1);
          label1.setBounds(345, 38, 30, 28);

          //---- label2 ----
          label2.setText("\u00e0");
          label2.setName("label2");
          panel2.add(label2);
          label2.setBounds(495, 38, 30, 28);

          //---- label3 ----
          label3.setText("de");
          label3.setName("label3");
          panel2.add(label3);
          label3.setBounds(345, 63, 30, 28);

          //---- label4 ----
          label4.setText("\u00e0");
          label4.setName("label4");
          panel2.add(label4);
          label4.setBounds(495, 63, 30, 28);

          //---- label5 ----
          label5.setText("de");
          label5.setName("label5");
          panel2.add(label5);
          label5.setBounds(345, 88, 30, 28);

          //---- label6 ----
          label6.setText("\u00e0");
          label6.setName("label6");
          panel2.add(label6);
          label6.setBounds(495, 88, 30, 28);

          //---- label7 ----
          label7.setText("de");
          label7.setName("label7");
          panel2.add(label7);
          label7.setBounds(345, 113, 30, 28);

          //---- label8 ----
          label8.setText("\u00e0");
          label8.setName("label8");
          panel2.add(label8);
          label8.setBounds(495, 113, 30, 28);

          //---- label9 ----
          label9.setText("de");
          label9.setName("label9");
          panel2.add(label9);
          label9.setBounds(345, 138, 30, 28);

          //---- label10 ----
          label10.setText("\u00e0");
          label10.setName("label10");
          panel2.add(label10);
          label10.setBounds(495, 138, 30, 28);

          //---- label11 ----
          label11.setText("de");
          label11.setName("label11");
          panel2.add(label11);
          label11.setBounds(345, 163, 30, 28);

          //---- label12 ----
          label12.setText("\u00e0");
          label12.setName("label12");
          panel2.add(label12);
          label12.setBounds(495, 163, 30, 28);

          //---- label13 ----
          label13.setText("de");
          label13.setName("label13");
          panel2.add(label13);
          label13.setBounds(345, 188, 30, 28);

          //---- label14 ----
          label14.setText("\u00e0");
          label14.setName("label14");
          panel2.add(label14);
          label14.setBounds(495, 188, 30, 28);

          //---- label15 ----
          label15.setText("de");
          label15.setName("label15");
          panel2.add(label15);
          label15.setBounds(345, 213, 30, 28);

          //---- label16 ----
          label16.setText("\u00e0");
          label16.setName("label16");
          panel2.add(label16);
          label16.setBounds(495, 213, 30, 28);

          //---- label17 ----
          label17.setText("de");
          label17.setName("label17");
          panel2.add(label17);
          label17.setBounds(345, 238, 30, 28);

          //---- label18 ----
          label18.setText("\u00e0");
          label18.setName("label18");
          panel2.add(label18);
          label18.setBounds(495, 238, 30, 28);

          //---- label19 ----
          label19.setText("de");
          label19.setName("label19");
          panel2.add(label19);
          label19.setBounds(345, 263, 30, 28);

          //---- label20 ----
          label20.setText("\u00e0");
          label20.setName("label20");
          panel2.add(label20);
          label20.setBounds(495, 263, 30, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 706, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(24, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 318, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(22, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private RiZoneSortie LIB1;
  private RiZoneSortie LIB2;
  private RiZoneSortie LIB3;
  private RiZoneSortie LIB4;
  private RiZoneSortie LIB5;
  private RiZoneSortie LIB6;
  private RiZoneSortie LIB7;
  private RiZoneSortie LIB8;
  private RiZoneSortie LIB9;
  private RiZoneSortie LIB10;
  private XRiTextField CNF01;
  private XRiTextField CNF02;
  private XRiTextField CNF03;
  private XRiTextField CNF04;
  private XRiTextField CNF05;
  private XRiTextField CNF06;
  private XRiTextField CNF07;
  private XRiTextField CNF08;
  private XRiTextField CNF09;
  private XRiTextField CNF10;
  private JPanel panel1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiTextField CND01;
  private XRiTextField CND02;
  private XRiTextField CND03;
  private XRiTextField CND04;
  private XRiTextField CND05;
  private XRiTextField CND06;
  private XRiTextField CND07;
  private XRiTextField CND08;
  private XRiTextField CND09;
  private XRiTextField CND10;
  private XRiTextField CAD01;
  private XRiTextField CAD02;
  private XRiTextField CAD03;
  private XRiTextField CAD04;
  private XRiTextField CAD05;
  private XRiTextField CAD06;
  private XRiTextField CAD07;
  private XRiTextField CAD08;
  private XRiTextField CAD09;
  private XRiTextField CAD10;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label17;
  private JLabel label18;
  private JLabel label19;
  private JLabel label20;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
