
package ri.serien.libecranrpg.vgvx.VGVX48FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX48FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX48FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    P48MAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P48MAG@")).trim());
    P48ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P48ART@")).trim());
    WSTKAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKAX@")).trim());
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIBR@")).trim());
    WTP01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL201@")).trim());
    WTP02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL202@")).trim());
    WTP03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL203@")).trim());
    WTP04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL204@")).trim());
    WTP05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL205@")).trim());
    WTP06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL206@")).trim());
    WTP07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL207@")).trim());
    WTP08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL208@")).trim());
    WTP09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL209@")).trim());
    WTP10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL210@")).trim());
    WSTKLX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKLX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    P48MAG.setEnabled(lexique.isPresent("P48MAG"));
    WSTKLX.setEnabled(lexique.isPresent("WSTKLX"));
    LOTDEB.setEnabled(lexique.isPresent("LOTDEB"));
    WSTKAX.setEnabled(lexique.isPresent("WSTKAX"));
    OBJ_36.setVisible(lexique.isPresent("A1LIBR"));
    P48ART.setEnabled(lexique.isPresent("P48ART"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Historique de vente"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void gererLesTops(String zone, String valeur) {
    lexique.HostFieldPutData(zone, 1, valeur);
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", true);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    gererLesTops(BTD.getInvoker().getName(), "1");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    gererLesTops(BTD.getInvoker().getName(), "M");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    gererLesTops(BTD.getInvoker().getName(), "C");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    gererLesTops(BTD.getInvoker().getName(), "5");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    menuBar1 = new JMenuBar();
    panel2 = new JPanel();
    OBJ_41 = new JLabel();
    P48MAG = new RiZoneSortie();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_42 = new JLabel();
    P48ART = new RiZoneSortie();
    OBJ_40 = new JLabel();
    WSTKAX = new RiZoneSortie();
    OBJ_36 = new RiZoneSortie();
    OBJ_43 = new JLabel();
    LOTDEB = new XRiTextField();
    WTP01 = new RiZoneSortie();
    STK01 = new XRiTextField();
    WTP02 = new RiZoneSortie();
    WTP03 = new RiZoneSortie();
    WTP04 = new RiZoneSortie();
    WTP05 = new RiZoneSortie();
    WTP06 = new RiZoneSortie();
    WTP07 = new RiZoneSortie();
    WTP08 = new RiZoneSortie();
    WTP09 = new RiZoneSortie();
    WTP10 = new RiZoneSortie();
    STK02 = new XRiTextField();
    STK03 = new XRiTextField();
    STK04 = new XRiTextField();
    STK05 = new XRiTextField();
    STK06 = new XRiTextField();
    STK07 = new XRiTextField();
    STK08 = new XRiTextField();
    STK09 = new XRiTextField();
    STK10 = new XRiTextField();
    label1 = new JLabel();
    WSTKLX = new RiZoneSortie();
    OBJ_37 = new JLabel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(835, 515));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== menuBar1 ========
      {
        menuBar1.setName("menuBar1");

        //======== panel2 ========
        {
          panel2.setMinimumSize(new Dimension(0, 30));
          panel2.setPreferredSize(new Dimension(0, 30));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_41 ----
          OBJ_41.setText("Magasin");
          OBJ_41.setName("OBJ_41");
          panel2.add(OBJ_41);
          OBJ_41.setBounds(5, 5, 65, 20);

          //---- P48MAG ----
          P48MAG.setComponentPopupMenu(BTD);
          P48MAG.setText("@P48MAG@");
          P48MAG.setOpaque(false);
          P48MAG.setName("P48MAG");
          panel2.add(P48MAG);
          P48MAG.setBounds(70, 3, 34, P48MAG.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        menuBar1.add(panel2);
      }
      p_principal.add(menuBar1, BorderLayout.NORTH);

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 120));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Lots non sold\u00e9s");
              riSousMenu_bt6.setToolTipText("Affichage des lots non sold\u00e9s");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setBorder(new TitledBorder(""));
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_42 ----
          OBJ_42.setText("Code article");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(25, 25, 80, 20);

          //---- P48ART ----
          P48ART.setComponentPopupMenu(BTD);
          P48ART.setText("@P48ART@");
          P48ART.setName("P48ART");
          panel1.add(P48ART);
          P48ART.setBounds(105, 23, 210, P48ART.getPreferredSize().height);

          //---- OBJ_40 ----
          OBJ_40.setText("Stock article");
          OBJ_40.setName("OBJ_40");
          panel1.add(OBJ_40);
          OBJ_40.setBounds(25, 59, 76, 20);

          //---- WSTKAX ----
          WSTKAX.setComponentPopupMenu(BTD);
          WSTKAX.setText("@WSTKAX@");
          WSTKAX.setName("WSTKAX");
          panel1.add(WSTKAX);
          WSTKAX.setBounds(105, 57, 140, WSTKAX.getPreferredSize().height);

          //---- OBJ_36 ----
          OBJ_36.setText("@A1LIBR@");
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(325, 23, 220, OBJ_36.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setText("Affich\u00e9 \u00e0 partir de");
          OBJ_43.setName("OBJ_43");
          panel1.add(OBJ_43);
          OBJ_43.setBounds(275, 59, 105, 20);

          //---- LOTDEB ----
          LOTDEB.setComponentPopupMenu(BTD);
          LOTDEB.setName("LOTDEB");
          panel1.add(LOTDEB);
          LOTDEB.setBounds(387, 55, 160, LOTDEB.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //---- WTP01 ----
        WTP01.setText("@WL201@");
        WTP01.setComponentPopupMenu(BTD);
        WTP01.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        WTP01.setName("WTP01");

        //---- STK01 ----
        STK01.setComponentPopupMenu(null);
        STK01.setName("STK01");

        //---- WTP02 ----
        WTP02.setText("@WL202@");
        WTP02.setComponentPopupMenu(BTD);
        WTP02.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        WTP02.setName("WTP02");

        //---- WTP03 ----
        WTP03.setText("@WL203@");
        WTP03.setComponentPopupMenu(BTD);
        WTP03.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        WTP03.setName("WTP03");

        //---- WTP04 ----
        WTP04.setText("@WL204@");
        WTP04.setComponentPopupMenu(BTD);
        WTP04.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        WTP04.setName("WTP04");

        //---- WTP05 ----
        WTP05.setText("@WL205@");
        WTP05.setComponentPopupMenu(BTD);
        WTP05.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        WTP05.setName("WTP05");

        //---- WTP06 ----
        WTP06.setText("@WL206@");
        WTP06.setComponentPopupMenu(BTD);
        WTP06.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        WTP06.setName("WTP06");

        //---- WTP07 ----
        WTP07.setText("@WL207@");
        WTP07.setComponentPopupMenu(BTD);
        WTP07.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        WTP07.setName("WTP07");

        //---- WTP08 ----
        WTP08.setText("@WL208@");
        WTP08.setComponentPopupMenu(BTD);
        WTP08.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        WTP08.setName("WTP08");

        //---- WTP09 ----
        WTP09.setText("@WL209@");
        WTP09.setComponentPopupMenu(BTD);
        WTP09.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        WTP09.setName("WTP09");

        //---- WTP10 ----
        WTP10.setText("@WL210@");
        WTP10.setComponentPopupMenu(BTD);
        WTP10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        WTP10.setName("WTP10");

        //---- STK02 ----
        STK02.setComponentPopupMenu(null);
        STK02.setName("STK02");

        //---- STK03 ----
        STK03.setComponentPopupMenu(null);
        STK03.setName("STK03");

        //---- STK04 ----
        STK04.setComponentPopupMenu(null);
        STK04.setName("STK04");

        //---- STK05 ----
        STK05.setComponentPopupMenu(null);
        STK05.setName("STK05");

        //---- STK06 ----
        STK06.setComponentPopupMenu(null);
        STK06.setName("STK06");

        //---- STK07 ----
        STK07.setComponentPopupMenu(null);
        STK07.setName("STK07");

        //---- STK08 ----
        STK08.setComponentPopupMenu(null);
        STK08.setName("STK08");

        //---- STK09 ----
        STK09.setComponentPopupMenu(null);
        STK09.setName("STK09");

        //---- STK10 ----
        STK10.setComponentPopupMenu(null);
        STK10.setName("STK10");

        //---- label1 ----
        label1.setText("Num\u00e9ro de lots");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
        label1.setName("label1");

        //---- WSTKLX ----
        WSTKLX.setComponentPopupMenu(BTD);
        WSTKLX.setText("@WSTKLX@");
        WSTKLX.setName("WSTKLX");

        //---- OBJ_37 ----
        OBJ_37.setText("Total stock lots");
        OBJ_37.setName("OBJ_37");

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addComponent(label1, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(WTP01, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP02, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP03, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP04, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP05, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP06, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP07, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP08, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP09, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP10, GroupLayout.PREFERRED_SIZE, 450, GroupLayout.PREFERRED_SIZE))
              .addGap(5, 5, 5)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(STK03, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addComponent(STK05, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addComponent(STK04, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addComponent(STK02, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addComponent(STK01, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addComponent(STK10, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addComponent(STK09, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addComponent(STK07, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addComponent(STK08, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addComponent(STK06, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(385, 385, 385)
              .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
              .addGap(2, 2, 2)
              .addComponent(WSTKLX, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
              .addGap(15, 15, 15)
              .addComponent(label1)
              .addGap(7, 7, 7)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(2, 2, 2)
                  .addComponent(WTP01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WTP02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WTP03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WTP04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WTP05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WTP06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WTP07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WTP08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WTP09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WTP10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(STK03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(STK05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(STK04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(STK02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(STK01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(22, 22, 22)
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(STK10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(STK09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(STK07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(STK08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(125, 125, 125)
                  .addComponent(STK06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              .addGap(9, 9, 9)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(2, 2, 2)
                  .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addComponent(WSTKLX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("D\u00e9tail");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_12 ----
      OBJ_12.setText("Mise \u00e0 jour du lot");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Cr\u00e9ation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("A partir de");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
      BTD.addSeparator();

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JMenuBar menuBar1;
  private JPanel panel2;
  private JLabel OBJ_41;
  private RiZoneSortie P48MAG;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_42;
  private RiZoneSortie P48ART;
  private JLabel OBJ_40;
  private RiZoneSortie WSTKAX;
  private RiZoneSortie OBJ_36;
  private JLabel OBJ_43;
  private XRiTextField LOTDEB;
  private RiZoneSortie WTP01;
  private XRiTextField STK01;
  private RiZoneSortie WTP02;
  private RiZoneSortie WTP03;
  private RiZoneSortie WTP04;
  private RiZoneSortie WTP05;
  private RiZoneSortie WTP06;
  private RiZoneSortie WTP07;
  private RiZoneSortie WTP08;
  private RiZoneSortie WTP09;
  private RiZoneSortie WTP10;
  private XRiTextField STK02;
  private XRiTextField STK03;
  private XRiTextField STK04;
  private XRiTextField STK05;
  private XRiTextField STK06;
  private XRiTextField STK07;
  private XRiTextField STK08;
  private XRiTextField STK09;
  private XRiTextField STK10;
  private JLabel label1;
  private RiZoneSortie WSTKLX;
  private JLabel OBJ_37;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
