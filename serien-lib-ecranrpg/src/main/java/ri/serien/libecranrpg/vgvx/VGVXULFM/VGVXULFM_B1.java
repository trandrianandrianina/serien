
package ri.serien.libecranrpg.vgvx.VGVXULFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXULFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXULFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDART@")).trim());
    INDLAN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDLAN@")).trim());
    INDIMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIMP@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("Résultat de la recherche pour l'article @A1LIB@ @A1LB2@")).trim()));
    EBL01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL01@")).trim());
    EBL02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL02@")).trim());
    EBL03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL03@")).trim());
    EBL04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL04@")).trim());
    EBL05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL05@")).trim());
    EBL06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL06@")).trim());
    EBL07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL07@")).trim());
    EBL08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL08@")).trim());
    EBL09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL09@")).trim());
    EBL10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL10@")).trim());
    EBL11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL11@")).trim());
    EBL12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL12@")).trim());
    EBL13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL13@")).trim());
    EBL14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL14@")).trim());
    EBL15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL15@")).trim());
    EBL16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL16@")).trim());
    EBL17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL17@")).trim());
    EBL18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL18@")).trim());
    EBL19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL19@")).trim());
    EBL20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBL20@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    INDART = new RiZoneSortie();
    OBJ_59 = new JLabel();
    INDLAN = new RiZoneSortie();
    OBJ_58 = new JLabel();
    OBJ_57 = new JLabel();
    INDIMP = new RiZoneSortie();
    INDETB = new RiZoneSortie();
    OBJ_55 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    label1 = new JLabel();
    ULNL01 = new XRiTextField();
    ULNL02 = new XRiTextField();
    ULNL03 = new XRiTextField();
    ULNL04 = new XRiTextField();
    ULNL05 = new XRiTextField();
    ULNL06 = new XRiTextField();
    ULNL07 = new XRiTextField();
    ULNL08 = new XRiTextField();
    ULNL09 = new XRiTextField();
    ULNL10 = new XRiTextField();
    ULNL11 = new XRiTextField();
    ULNL12 = new XRiTextField();
    ULNL13 = new XRiTextField();
    ULNL14 = new XRiTextField();
    ULNL15 = new XRiTextField();
    ULNL16 = new XRiTextField();
    ULNL17 = new XRiTextField();
    ULNL18 = new XRiTextField();
    ULNL19 = new XRiTextField();
    ULNL20 = new XRiTextField();
    EBL01 = new RiZoneSortie();
    EBL02 = new RiZoneSortie();
    EBL03 = new RiZoneSortie();
    EBL04 = new RiZoneSortie();
    EBL05 = new RiZoneSortie();
    EBL06 = new RiZoneSortie();
    EBL07 = new RiZoneSortie();
    EBL08 = new RiZoneSortie();
    EBL09 = new RiZoneSortie();
    EBL10 = new RiZoneSortie();
    EBL11 = new RiZoneSortie();
    EBL12 = new RiZoneSortie();
    EBL13 = new RiZoneSortie();
    EBL14 = new RiZoneSortie();
    EBL15 = new RiZoneSortie();
    EBL16 = new RiZoneSortie();
    EBL17 = new RiZoneSortie();
    EBL18 = new RiZoneSortie();
    EBL19 = new RiZoneSortie();
    EBL20 = new RiZoneSortie();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    label18 = new JLabel();
    label19 = new JLabel();
    label20 = new JLabel();
    BT_PGUP2 = new JButton();
    BT_PGDOWN2 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Affectation des libell\u00e9s articles");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(800, 30));
          p_tete_gauche.setMinimumSize(new Dimension(800, 30));
          p_tete_gauche.setName("p_tete_gauche");

          //---- INDART ----
          INDART.setOpaque(false);
          INDART.setText("@INDART@");
          INDART.setName("INDART");

          //---- OBJ_59 ----
          OBJ_59.setText("Code article");
          OBJ_59.setName("OBJ_59");

          //---- INDLAN ----
          INDLAN.setOpaque(false);
          INDLAN.setText("@INDLAN@");
          INDLAN.setName("INDLAN");

          //---- OBJ_58 ----
          OBJ_58.setText("Langue");
          OBJ_58.setName("OBJ_58");

          //---- OBJ_57 ----
          OBJ_57.setText("Type d'imprim\u00e9");
          OBJ_57.setName("OBJ_57");

          //---- INDIMP ----
          INDIMP.setOpaque(false);
          INDIMP.setText("@INDIMP@");
          INDIMP.setName("INDIMP");

          //---- INDETB ----
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_55 ----
          OBJ_55.setText("Etablissement");
          OBJ_55.setName("OBJ_55");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDIMP, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(INDLAN, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(80, 80, 80)
                    .addComponent(INDART, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIMP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDLAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 590));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(900, 590));
          p_contenu.setMaximumSize(new Dimension(706, 600));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("R\u00e9sultat de la recherche pour l'article @A1LIB@ @A1LB2@"));
            panel1.setOpaque(false);
            panel1.setPreferredSize(new Dimension(574, 550));
            panel1.setMinimumSize(new Dimension(574, 557));
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(930, 85, 25, 140);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(925, 395, 25, 140);

            //---- label1 ----
            label1.setText("01");
            label1.setHorizontalAlignment(SwingConstants.RIGHT);
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(10, 40, 25, 28);

            //---- ULNL01 ----
            ULNL01.setName("ULNL01");
            panel1.add(ULNL01);
            ULNL01.setBounds(45, 40, 36, ULNL01.getPreferredSize().height);

            //---- ULNL02 ----
            ULNL02.setName("ULNL02");
            panel1.add(ULNL02);
            ULNL02.setBounds(45, 65, 36, ULNL02.getPreferredSize().height);

            //---- ULNL03 ----
            ULNL03.setName("ULNL03");
            panel1.add(ULNL03);
            ULNL03.setBounds(45, 90, 36, ULNL03.getPreferredSize().height);

            //---- ULNL04 ----
            ULNL04.setName("ULNL04");
            panel1.add(ULNL04);
            ULNL04.setBounds(45, 115, 36, ULNL04.getPreferredSize().height);

            //---- ULNL05 ----
            ULNL05.setName("ULNL05");
            panel1.add(ULNL05);
            ULNL05.setBounds(45, 140, 36, ULNL05.getPreferredSize().height);

            //---- ULNL06 ----
            ULNL06.setName("ULNL06");
            panel1.add(ULNL06);
            ULNL06.setBounds(45, 165, 36, ULNL06.getPreferredSize().height);

            //---- ULNL07 ----
            ULNL07.setName("ULNL07");
            panel1.add(ULNL07);
            ULNL07.setBounds(45, 190, 36, ULNL07.getPreferredSize().height);

            //---- ULNL08 ----
            ULNL08.setName("ULNL08");
            panel1.add(ULNL08);
            ULNL08.setBounds(45, 215, 36, ULNL08.getPreferredSize().height);

            //---- ULNL09 ----
            ULNL09.setName("ULNL09");
            panel1.add(ULNL09);
            ULNL09.setBounds(45, 240, 36, ULNL09.getPreferredSize().height);

            //---- ULNL10 ----
            ULNL10.setName("ULNL10");
            panel1.add(ULNL10);
            ULNL10.setBounds(45, 265, 36, ULNL10.getPreferredSize().height);

            //---- ULNL11 ----
            ULNL11.setName("ULNL11");
            panel1.add(ULNL11);
            ULNL11.setBounds(45, 290, 36, ULNL11.getPreferredSize().height);

            //---- ULNL12 ----
            ULNL12.setName("ULNL12");
            panel1.add(ULNL12);
            ULNL12.setBounds(45, 315, 36, ULNL12.getPreferredSize().height);

            //---- ULNL13 ----
            ULNL13.setName("ULNL13");
            panel1.add(ULNL13);
            ULNL13.setBounds(45, 340, 36, ULNL13.getPreferredSize().height);

            //---- ULNL14 ----
            ULNL14.setName("ULNL14");
            panel1.add(ULNL14);
            ULNL14.setBounds(45, 365, 36, ULNL14.getPreferredSize().height);

            //---- ULNL15 ----
            ULNL15.setName("ULNL15");
            panel1.add(ULNL15);
            ULNL15.setBounds(45, 390, 36, ULNL15.getPreferredSize().height);

            //---- ULNL16 ----
            ULNL16.setName("ULNL16");
            panel1.add(ULNL16);
            ULNL16.setBounds(45, 415, 36, ULNL16.getPreferredSize().height);

            //---- ULNL17 ----
            ULNL17.setName("ULNL17");
            panel1.add(ULNL17);
            ULNL17.setBounds(45, 440, 36, ULNL17.getPreferredSize().height);

            //---- ULNL18 ----
            ULNL18.setName("ULNL18");
            panel1.add(ULNL18);
            ULNL18.setBounds(45, 465, 36, ULNL18.getPreferredSize().height);

            //---- ULNL19 ----
            ULNL19.setName("ULNL19");
            panel1.add(ULNL19);
            ULNL19.setBounds(45, 490, 36, ULNL19.getPreferredSize().height);

            //---- ULNL20 ----
            ULNL20.setName("ULNL20");
            panel1.add(ULNL20);
            ULNL20.setBounds(45, 515, 36, ULNL20.getPreferredSize().height);

            //---- EBL01 ----
            EBL01.setText("@EBL01@");
            EBL01.setName("EBL01");
            panel1.add(EBL01);
            EBL01.setBounds(85, 42, 730, EBL01.getPreferredSize().height);

            //---- EBL02 ----
            EBL02.setText("@EBL02@");
            EBL02.setName("EBL02");
            panel1.add(EBL02);
            EBL02.setBounds(85, 67, 730, EBL02.getPreferredSize().height);

            //---- EBL03 ----
            EBL03.setText("@EBL03@");
            EBL03.setName("EBL03");
            panel1.add(EBL03);
            EBL03.setBounds(85, 92, 730, EBL03.getPreferredSize().height);

            //---- EBL04 ----
            EBL04.setText("@EBL04@");
            EBL04.setName("EBL04");
            panel1.add(EBL04);
            EBL04.setBounds(85, 117, 730, EBL04.getPreferredSize().height);

            //---- EBL05 ----
            EBL05.setText("@EBL05@");
            EBL05.setName("EBL05");
            panel1.add(EBL05);
            EBL05.setBounds(85, 142, 730, EBL05.getPreferredSize().height);

            //---- EBL06 ----
            EBL06.setText("@EBL06@");
            EBL06.setName("EBL06");
            panel1.add(EBL06);
            EBL06.setBounds(85, 167, 730, EBL06.getPreferredSize().height);

            //---- EBL07 ----
            EBL07.setText("@EBL07@");
            EBL07.setName("EBL07");
            panel1.add(EBL07);
            EBL07.setBounds(85, 192, 730, EBL07.getPreferredSize().height);

            //---- EBL08 ----
            EBL08.setText("@EBL08@");
            EBL08.setName("EBL08");
            panel1.add(EBL08);
            EBL08.setBounds(85, 217, 730, EBL08.getPreferredSize().height);

            //---- EBL09 ----
            EBL09.setText("@EBL09@");
            EBL09.setName("EBL09");
            panel1.add(EBL09);
            EBL09.setBounds(85, 242, 730, EBL09.getPreferredSize().height);

            //---- EBL10 ----
            EBL10.setText("@EBL10@");
            EBL10.setName("EBL10");
            panel1.add(EBL10);
            EBL10.setBounds(85, 267, 730, EBL10.getPreferredSize().height);

            //---- EBL11 ----
            EBL11.setText("@EBL11@");
            EBL11.setName("EBL11");
            panel1.add(EBL11);
            EBL11.setBounds(85, 292, 730, EBL11.getPreferredSize().height);

            //---- EBL12 ----
            EBL12.setText("@EBL12@");
            EBL12.setName("EBL12");
            panel1.add(EBL12);
            EBL12.setBounds(85, 317, 730, EBL12.getPreferredSize().height);

            //---- EBL13 ----
            EBL13.setText("@EBL13@");
            EBL13.setName("EBL13");
            panel1.add(EBL13);
            EBL13.setBounds(85, 342, 730, EBL13.getPreferredSize().height);

            //---- EBL14 ----
            EBL14.setText("@EBL14@");
            EBL14.setName("EBL14");
            panel1.add(EBL14);
            EBL14.setBounds(85, 367, 730, EBL14.getPreferredSize().height);

            //---- EBL15 ----
            EBL15.setText("@EBL15@");
            EBL15.setName("EBL15");
            panel1.add(EBL15);
            EBL15.setBounds(85, 392, 730, EBL15.getPreferredSize().height);

            //---- EBL16 ----
            EBL16.setText("@EBL16@");
            EBL16.setName("EBL16");
            panel1.add(EBL16);
            EBL16.setBounds(85, 417, 730, EBL16.getPreferredSize().height);

            //---- EBL17 ----
            EBL17.setText("@EBL17@");
            EBL17.setName("EBL17");
            panel1.add(EBL17);
            EBL17.setBounds(85, 442, 730, EBL17.getPreferredSize().height);

            //---- EBL18 ----
            EBL18.setText("@EBL18@");
            EBL18.setName("EBL18");
            panel1.add(EBL18);
            EBL18.setBounds(85, 467, 730, EBL18.getPreferredSize().height);

            //---- EBL19 ----
            EBL19.setText("@EBL19@");
            EBL19.setName("EBL19");
            panel1.add(EBL19);
            EBL19.setBounds(85, 492, 730, EBL19.getPreferredSize().height);

            //---- EBL20 ----
            EBL20.setText("@EBL20@");
            EBL20.setName("EBL20");
            panel1.add(EBL20);
            EBL20.setBounds(85, 517, 730, EBL20.getPreferredSize().height);

            //---- label2 ----
            label2.setText("02");
            label2.setHorizontalAlignment(SwingConstants.RIGHT);
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(10, 65, 25, 28);

            //---- label3 ----
            label3.setText("03");
            label3.setHorizontalAlignment(SwingConstants.RIGHT);
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(10, 90, 25, 28);

            //---- label4 ----
            label4.setText("04");
            label4.setHorizontalAlignment(SwingConstants.RIGHT);
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(10, 115, 25, 28);

            //---- label5 ----
            label5.setText("05");
            label5.setHorizontalAlignment(SwingConstants.RIGHT);
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(10, 140, 25, 28);

            //---- label6 ----
            label6.setText("06");
            label6.setHorizontalAlignment(SwingConstants.RIGHT);
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setName("label6");
            panel1.add(label6);
            label6.setBounds(10, 165, 25, 28);

            //---- label7 ----
            label7.setText("07");
            label7.setHorizontalAlignment(SwingConstants.RIGHT);
            label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
            label7.setName("label7");
            panel1.add(label7);
            label7.setBounds(10, 190, 25, 28);

            //---- label8 ----
            label8.setText("08");
            label8.setHorizontalAlignment(SwingConstants.RIGHT);
            label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
            label8.setName("label8");
            panel1.add(label8);
            label8.setBounds(10, 215, 25, 28);

            //---- label9 ----
            label9.setText("09");
            label9.setHorizontalAlignment(SwingConstants.RIGHT);
            label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
            label9.setName("label9");
            panel1.add(label9);
            label9.setBounds(10, 240, 25, 28);

            //---- label10 ----
            label10.setText("10");
            label10.setHorizontalAlignment(SwingConstants.RIGHT);
            label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
            label10.setName("label10");
            panel1.add(label10);
            label10.setBounds(10, 265, 25, 28);

            //---- label11 ----
            label11.setText("11");
            label11.setHorizontalAlignment(SwingConstants.RIGHT);
            label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
            label11.setName("label11");
            panel1.add(label11);
            label11.setBounds(10, 290, 25, 28);

            //---- label12 ----
            label12.setText("12");
            label12.setHorizontalAlignment(SwingConstants.RIGHT);
            label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
            label12.setName("label12");
            panel1.add(label12);
            label12.setBounds(10, 315, 25, 28);

            //---- label13 ----
            label13.setText("13");
            label13.setHorizontalAlignment(SwingConstants.RIGHT);
            label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
            label13.setName("label13");
            panel1.add(label13);
            label13.setBounds(10, 340, 25, 28);

            //---- label14 ----
            label14.setText("14");
            label14.setHorizontalAlignment(SwingConstants.RIGHT);
            label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
            label14.setName("label14");
            panel1.add(label14);
            label14.setBounds(10, 365, 25, 28);

            //---- label15 ----
            label15.setText("15");
            label15.setHorizontalAlignment(SwingConstants.RIGHT);
            label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
            label15.setName("label15");
            panel1.add(label15);
            label15.setBounds(10, 390, 25, 28);

            //---- label16 ----
            label16.setText("16");
            label16.setHorizontalAlignment(SwingConstants.RIGHT);
            label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
            label16.setName("label16");
            panel1.add(label16);
            label16.setBounds(10, 415, 25, 28);

            //---- label17 ----
            label17.setText("17");
            label17.setHorizontalAlignment(SwingConstants.RIGHT);
            label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
            label17.setName("label17");
            panel1.add(label17);
            label17.setBounds(10, 440, 25, 28);

            //---- label18 ----
            label18.setText("18");
            label18.setHorizontalAlignment(SwingConstants.RIGHT);
            label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
            label18.setName("label18");
            panel1.add(label18);
            label18.setBounds(10, 465, 25, 28);

            //---- label19 ----
            label19.setText("19");
            label19.setHorizontalAlignment(SwingConstants.RIGHT);
            label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
            label19.setName("label19");
            panel1.add(label19);
            label19.setBounds(10, 490, 25, 28);

            //---- label20 ----
            label20.setText("20");
            label20.setHorizontalAlignment(SwingConstants.RIGHT);
            label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
            label20.setName("label20");
            panel1.add(label20);
            label20.setBounds(10, 515, 25, 28);

            //---- BT_PGUP2 ----
            BT_PGUP2.setText("");
            BT_PGUP2.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP2.setName("BT_PGUP2");
            panel1.add(BT_PGUP2);
            BT_PGUP2.setBounds(825, 42, 25, 218);

            //---- BT_PGDOWN2 ----
            BT_PGDOWN2.setText("");
            BT_PGDOWN2.setToolTipText("Page suivante");
            BT_PGDOWN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN2.setName("BT_PGDOWN2");
            panel1.add(BT_PGDOWN2);
            BT_PGDOWN2.setBounds(825, 321, 25, 220);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 863, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 560, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie INDART;
  private JLabel OBJ_59;
  private RiZoneSortie INDLAN;
  private JLabel OBJ_58;
  private JLabel OBJ_57;
  private RiZoneSortie INDIMP;
  private RiZoneSortie INDETB;
  private JLabel OBJ_55;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel label1;
  private XRiTextField ULNL01;
  private XRiTextField ULNL02;
  private XRiTextField ULNL03;
  private XRiTextField ULNL04;
  private XRiTextField ULNL05;
  private XRiTextField ULNL06;
  private XRiTextField ULNL07;
  private XRiTextField ULNL08;
  private XRiTextField ULNL09;
  private XRiTextField ULNL10;
  private XRiTextField ULNL11;
  private XRiTextField ULNL12;
  private XRiTextField ULNL13;
  private XRiTextField ULNL14;
  private XRiTextField ULNL15;
  private XRiTextField ULNL16;
  private XRiTextField ULNL17;
  private XRiTextField ULNL18;
  private XRiTextField ULNL19;
  private XRiTextField ULNL20;
  private RiZoneSortie EBL01;
  private RiZoneSortie EBL02;
  private RiZoneSortie EBL03;
  private RiZoneSortie EBL04;
  private RiZoneSortie EBL05;
  private RiZoneSortie EBL06;
  private RiZoneSortie EBL07;
  private RiZoneSortie EBL08;
  private RiZoneSortie EBL09;
  private RiZoneSortie EBL10;
  private RiZoneSortie EBL11;
  private RiZoneSortie EBL12;
  private RiZoneSortie EBL13;
  private RiZoneSortie EBL14;
  private RiZoneSortie EBL15;
  private RiZoneSortie EBL16;
  private RiZoneSortie EBL17;
  private RiZoneSortie EBL18;
  private RiZoneSortie EBL19;
  private RiZoneSortie EBL20;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label17;
  private JLabel label18;
  private JLabel label19;
  private JLabel label20;
  private JButton BT_PGUP2;
  private JButton BT_PGDOWN2;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
