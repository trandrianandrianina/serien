
package ri.serien.libecranrpg.vgvx.VGVX66FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.referentiel.commun.snunite.SNUnite;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX66FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVX66FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    T1RAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1RAT@")).trim());
    T1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1ETB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    FR2.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCNV@ @LIBRAT@")).trim()));
    CDNAU.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CDNAU@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("T1ETB"));
    
    snUnite.setSession(getSession());
    snUnite.charger(false);
    snUnite.setIdEtablissement(idEtablissement);
    snUnite.setSelectionParChampRPG(lexique, "WUNV");
    snUnite.setEnabled(false);
    
    p_bpresentation.setCodeEtablissement(T1ETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void helpActionPerformed(ActionEvent e) {
    // lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_presentation = new JPanel();
    p_logo = new JPanel();
    logoEtb = new JLabel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_72 = new JLabel();
    T1CNV = new XRiTextField();
    OBJ_50 = new JLabel();
    T1TRA = new XRiComboBox();
    OBJ_49 = new JLabel();
    T1RAT = new RiZoneSortie();
    OBJ_45 = new JLabel();
    T1ETB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    FR2 = new JPanel();
    pnlAutorisation = new SNPanel();
    lbNumeroAutorisation = new SNLabelChamp();
    CDNAU = new SNTexte();
    lbValidite = new SNLabelChamp();
    CDDTDX = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    CDDTFX = new XRiCalendrier();
    pnlUV = new SNPanel();
    lbUV = new SNLabelChamp();
    snUnite = new SNUnite();
    pnlVente = new SNPanelTitre();
    lbCoefficient1 = new SNLabelChamp();
    T1COE = new XRiTextField();
    lbSurPrix1 = new SNLabelUnite();
    lbOu1 = new SNLabelChamp();
    lbPrixNet1 = new SNLabelChamp();
    T1VALX = new XRiTextField();
    sNPanelTitre3 = new SNPanelTitre();
    lbDebut = new SNLabelUnite();
    lbFin = new SNLabelUnite();
    T1DTDX = new XRiCalendrier();
    T1DTFX = new XRiCalendrier();
    pnlAchat = new SNPanelTitre();
    lbCoefficient2 = new SNLabelChamp();
    T2COE = new XRiTextField();
    lbSurPrix2 = new SNLabelUnite();
    lbOu2 = new SNLabelChamp();
    lbPrixNet2 = new SNLabelChamp();
    T1VALAX = new XRiTextField();
    BTD = new JPopupMenu();
    help = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //======== p_presentation ========
      {
        p_presentation.setPreferredSize(new Dimension(1020, 55));
        p_presentation.setMinimumSize(new Dimension(950, 55));
        p_presentation.setMaximumSize(new Dimension(32767, 55));
        p_presentation.setBackground(Color.white);
        p_presentation.setAutoscrolls(true);
        p_presentation.setFocusable(false);
        p_presentation.setName("p_presentation");
        p_presentation.setLayout(new BorderLayout());

        //======== p_logo ========
        {
          p_logo.setMinimumSize(new Dimension(120, 55));
          p_logo.setPreferredSize(new Dimension(120, 55));
          p_logo.setBackground(new Color(238, 239, 241));
          p_logo.setName("p_logo");
          p_logo.setLayout(null);

          //---- logoEtb ----
          logoEtb.setBorder(null);
          logoEtb.setName("logoEtb");
          p_logo.add(logoEtb);
          logoEtb.setBounds(70, 5, 45, 45);
        }
        p_presentation.add(p_logo, BorderLayout.EAST);

        //---- p_bpresentation ----
        p_bpresentation.setText("Condition de d\u00e9rogation");
        p_bpresentation.setName("p_bpresentation");
        p_presentation.add(p_bpresentation, BorderLayout.NORTH);
      }
      p_nord.add(p_presentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(880, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_72 ----
          OBJ_72.setText("Num\u00e9ro");
          OBJ_72.setName("OBJ_72");
          p_tete_gauche.add(OBJ_72);
          OBJ_72.setBounds(160, 5, 55, 18);

          //---- T1CNV ----
          T1CNV.setComponentPopupMenu(null);
          T1CNV.setHorizontalAlignment(SwingConstants.RIGHT);
          T1CNV.setName("T1CNV");
          p_tete_gauche.add(T1CNV);
          T1CNV.setBounds(225, 0, 60, T1CNV.getPreferredSize().height);

          //---- OBJ_50 ----
          OBJ_50.setText("Type de rattachement");
          OBJ_50.setName("OBJ_50");
          p_tete_gauche.add(OBJ_50);
          OBJ_50.setBounds(300, 5, 130, 18);

          //---- T1TRA ----
          T1TRA.setModel(new DefaultComboBoxModel(new String[] {
            "Tous",
            "Article",
            "Tarif",
            "Famille",
            "Sous famille",
            "Groupe",
            "Embo\u00eetage ou g\u00e9n\u00e9ral",
            "Ensemble d'articles",
            "Num de ligne (rang)",
            "Regroupement achat",
            "Fournisseur"
          }));
          T1TRA.setComponentPopupMenu(null);
          T1TRA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T1TRA.setName("T1TRA");
          p_tete_gauche.add(T1TRA);
          T1TRA.setBounds(425, 1, 210, T1TRA.getPreferredSize().height);

          //---- OBJ_49 ----
          OBJ_49.setText("Rattachement");
          OBJ_49.setName("OBJ_49");
          p_tete_gauche.add(OBJ_49);
          OBJ_49.setBounds(640, 5, 90, 18);

          //---- T1RAT ----
          T1RAT.setOpaque(false);
          T1RAT.setText("@T1RAT@");
          T1RAT.setName("T1RAT");
          p_tete_gauche.add(T1RAT);
          T1RAT.setBounds(745, 2, 168, T1RAT.getPreferredSize().height);

          //---- OBJ_45 ----
          OBJ_45.setText("Etablissement");
          OBJ_45.setName("OBJ_45");
          p_tete_gauche.add(OBJ_45);
          OBJ_45.setBounds(5, 5, 90, 18);

          //---- T1ETB ----
          T1ETB.setOpaque(false);
          T1ETB.setText("@T1ETB@");
          T1ETB.setName("T1ETB");
          p_tete_gauche.add(T1ETB);
          T1ETB.setBounds(100, 2, 40, T1ETB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1050, 300));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1050, 300));
          p_contenu.setName("p_contenu");

          //======== FR2 ========
          {
            FR2.setBorder(new TitledBorder("@LIBCNV@ @LIBRAT@"));
            FR2.setOpaque(false);
            FR2.setName("FR2");
            FR2.setLayout(new GridBagLayout());
            ((GridBagLayout)FR2.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)FR2.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
            ((GridBagLayout)FR2.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
            ((GridBagLayout)FR2.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

            //======== pnlAutorisation ========
            {
              pnlAutorisation.setName("pnlAutorisation");
              pnlAutorisation.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlAutorisation.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
              ((GridBagLayout)pnlAutorisation.getLayout()).rowHeights = new int[] {0, 0, 0};
              ((GridBagLayout)pnlAutorisation.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
              ((GridBagLayout)pnlAutorisation.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

              //---- lbNumeroAutorisation ----
              lbNumeroAutorisation.setText("Num\u00e9ro d'autorisation");
              lbNumeroAutorisation.setName("lbNumeroAutorisation");
              pnlAutorisation.add(lbNumeroAutorisation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- CDNAU ----
              CDNAU.setText("@CDNAU@");
              CDNAU.setEnabled(false);
              CDNAU.setName("CDNAU");
              pnlAutorisation.add(CDNAU, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- lbValidite ----
              lbValidite.setText("Valide du");
              lbValidite.setMaximumSize(new Dimension(100, 30));
              lbValidite.setMinimumSize(new Dimension(100, 30));
              lbValidite.setPreferredSize(new Dimension(100, 30));
              lbValidite.setName("lbValidite");
              pnlAutorisation.add(lbValidite, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- CDDTDX ----
              CDDTDX.setComponentPopupMenu(null);
              CDDTDX.setPreferredSize(new Dimension(115, 30));
              CDDTDX.setMinimumSize(new Dimension(115, 30));
              CDDTDX.setMaximumSize(new Dimension(115, 30));
              CDDTDX.setName("CDDTDX");
              pnlAutorisation.add(CDDTDX, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbAu ----
              lbAu.setText("au");
              lbAu.setMaximumSize(new Dimension(30, 30));
              lbAu.setMinimumSize(new Dimension(30, 30));
              lbAu.setPreferredSize(new Dimension(30, 30));
              lbAu.setName("lbAu");
              pnlAutorisation.add(lbAu, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- CDDTFX ----
              CDDTFX.setComponentPopupMenu(null);
              CDDTFX.setMaximumSize(new Dimension(115, 30));
              CDDTFX.setMinimumSize(new Dimension(115, 30));
              CDDTFX.setPreferredSize(new Dimension(115, 30));
              CDDTFX.setName("CDDTFX");
              pnlAutorisation.add(CDDTFX, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            FR2.add(pnlAutorisation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //======== pnlUV ========
            {
              pnlUV.setName("pnlUV");
              pnlUV.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlUV.getLayout()).columnWidths = new int[] {0, 0, 0};
              ((GridBagLayout)pnlUV.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)pnlUV.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
              ((GridBagLayout)pnlUV.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- lbUV ----
              lbUV.setText("Unit\u00e9 de vente");
              lbUV.setName("lbUV");
              pnlUV.add(lbUV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- snUnite ----
              snUnite.setName("snUnite");
              pnlUV.add(snUnite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            FR2.add(pnlUV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //======== pnlVente ========
            {
              pnlVente.setTitre("Prix de vente calcul\u00e9");
              pnlVente.setName("pnlVente");
              pnlVente.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlVente.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0};
              ((GridBagLayout)pnlVente.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)pnlVente.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
              ((GridBagLayout)pnlVente.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- lbCoefficient1 ----
              lbCoefficient1.setText("Coefficient");
              lbCoefficient1.setMinimumSize(new Dimension(100, 30));
              lbCoefficient1.setMaximumSize(new Dimension(100, 30));
              lbCoefficient1.setPreferredSize(new Dimension(100, 30));
              lbCoefficient1.setName("lbCoefficient1");
              pnlVente.add(lbCoefficient1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- T1COE ----
              T1COE.setComponentPopupMenu(null);
              T1COE.setHorizontalAlignment(SwingConstants.RIGHT);
              T1COE.setFont(new Font("sansserif", Font.PLAIN, 14));
              T1COE.setPreferredSize(new Dimension(80, 30));
              T1COE.setMinimumSize(new Dimension(80, 30));
              T1COE.setMaximumSize(new Dimension(80, 30));
              T1COE.setName("T1COE");
              pnlVente.add(T1COE, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbSurPrix1 ----
              lbSurPrix1.setText("sur prix d'achat net");
              lbSurPrix1.setMaximumSize(new Dimension(150, 30));
              lbSurPrix1.setMinimumSize(new Dimension(150, 30));
              lbSurPrix1.setPreferredSize(new Dimension(150, 30));
              lbSurPrix1.setName("lbSurPrix1");
              pnlVente.add(lbSurPrix1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbOu1 ----
              lbOu1.setText("ou");
              lbOu1.setMaximumSize(new Dimension(30, 30));
              lbOu1.setMinimumSize(new Dimension(30, 30));
              lbOu1.setPreferredSize(new Dimension(30, 30));
              lbOu1.setHorizontalAlignment(SwingConstants.CENTER);
              lbOu1.setName("lbOu1");
              pnlVente.add(lbOu1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbPrixNet1 ----
              lbPrixNet1.setText("prix net");
              lbPrixNet1.setMaximumSize(new Dimension(100, 30));
              lbPrixNet1.setMinimumSize(new Dimension(100, 30));
              lbPrixNet1.setPreferredSize(new Dimension(100, 30));
              lbPrixNet1.setName("lbPrixNet1");
              pnlVente.add(lbPrixNet1, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- T1VALX ----
              T1VALX.setComponentPopupMenu(null);
              T1VALX.setHorizontalAlignment(SwingConstants.RIGHT);
              T1VALX.setMinimumSize(new Dimension(115, 28));
              T1VALX.setPreferredSize(new Dimension(115, 28));
              T1VALX.setMaximumSize(new Dimension(115, 28));
              T1VALX.setName("T1VALX");
              pnlVente.add(T1VALX, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            FR2.add(pnlVente, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //======== sNPanelTitre3 ========
            {
              sNPanelTitre3.setTitre("P\u00e9riode de validit\u00e9");
              sNPanelTitre3.setName("sNPanelTitre3");
              sNPanelTitre3.setLayout(new GridBagLayout());
              ((GridBagLayout)sNPanelTitre3.getLayout()).columnWidths = new int[] {0, 0, 0};
              ((GridBagLayout)sNPanelTitre3.getLayout()).rowHeights = new int[] {0, 0, 0};
              ((GridBagLayout)sNPanelTitre3.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
              ((GridBagLayout)sNPanelTitre3.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

              //---- lbDebut ----
              lbDebut.setText("D\u00e9but");
              lbDebut.setName("lbDebut");
              sNPanelTitre3.add(lbDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- lbFin ----
              lbFin.setText("Fin");
              lbFin.setName("lbFin");
              sNPanelTitre3.add(lbFin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- T1DTDX ----
              T1DTDX.setComponentPopupMenu(null);
              T1DTDX.setPreferredSize(new Dimension(115, 30));
              T1DTDX.setMinimumSize(new Dimension(115, 30));
              T1DTDX.setMaximumSize(new Dimension(115, 30));
              T1DTDX.setName("T1DTDX");
              sNPanelTitre3.add(T1DTDX, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- T1DTFX ----
              T1DTFX.setComponentPopupMenu(null);
              T1DTFX.setMaximumSize(new Dimension(115, 30));
              T1DTFX.setMinimumSize(new Dimension(115, 30));
              T1DTFX.setPreferredSize(new Dimension(115, 30));
              T1DTFX.setName("T1DTFX");
              sNPanelTitre3.add(T1DTFX, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            FR2.add(sNPanelTitre3, new GridBagConstraints(1, 1, 1, 2, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));

            //======== pnlAchat ========
            {
              pnlAchat.setTitre("Prix d'achat calcul\u00e9");
              pnlAchat.setName("pnlAchat");
              pnlAchat.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlAchat.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0};
              ((GridBagLayout)pnlAchat.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)pnlAchat.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
              ((GridBagLayout)pnlAchat.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- lbCoefficient2 ----
              lbCoefficient2.setText("Coefficient");
              lbCoefficient2.setMinimumSize(new Dimension(100, 30));
              lbCoefficient2.setMaximumSize(new Dimension(100, 30));
              lbCoefficient2.setPreferredSize(new Dimension(100, 30));
              lbCoefficient2.setName("lbCoefficient2");
              pnlAchat.add(lbCoefficient2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- T2COE ----
              T2COE.setComponentPopupMenu(null);
              T2COE.setHorizontalAlignment(SwingConstants.RIGHT);
              T2COE.setFont(new Font("sansserif", Font.PLAIN, 14));
              T2COE.setPreferredSize(new Dimension(80, 30));
              T2COE.setMinimumSize(new Dimension(80, 30));
              T2COE.setMaximumSize(new Dimension(80, 30));
              T2COE.setName("T2COE");
              pnlAchat.add(T2COE, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbSurPrix2 ----
              lbSurPrix2.setText("sur prix d'achat net");
              lbSurPrix2.setMaximumSize(new Dimension(150, 30));
              lbSurPrix2.setMinimumSize(new Dimension(150, 30));
              lbSurPrix2.setPreferredSize(new Dimension(150, 30));
              lbSurPrix2.setName("lbSurPrix2");
              pnlAchat.add(lbSurPrix2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbOu2 ----
              lbOu2.setText("ou");
              lbOu2.setMaximumSize(new Dimension(30, 30));
              lbOu2.setMinimumSize(new Dimension(30, 30));
              lbOu2.setPreferredSize(new Dimension(30, 30));
              lbOu2.setHorizontalAlignment(SwingConstants.CENTER);
              lbOu2.setName("lbOu2");
              pnlAchat.add(lbOu2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbPrixNet2 ----
              lbPrixNet2.setText("prix net");
              lbPrixNet2.setMaximumSize(new Dimension(100, 30));
              lbPrixNet2.setMinimumSize(new Dimension(100, 30));
              lbPrixNet2.setPreferredSize(new Dimension(100, 30));
              lbPrixNet2.setName("lbPrixNet2");
              pnlAchat.add(lbPrixNet2, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- T1VALAX ----
              T1VALAX.setComponentPopupMenu(null);
              T1VALAX.setHorizontalAlignment(SwingConstants.RIGHT);
              T1VALAX.setMinimumSize(new Dimension(115, 28));
              T1VALAX.setPreferredSize(new Dimension(115, 28));
              T1VALAX.setMaximumSize(new Dimension(115, 28));
              T1VALAX.setName("T1VALAX");
              pnlAchat.add(T1VALAX, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            FR2.add(pnlAchat, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(FR2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(FR2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- help ----
      help.setText("Aide en ligne");
      help.setName("help");
      help.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          helpActionPerformed(e);
        }
      });
      BTD.add(help);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private JPanel p_presentation;
  private JPanel p_logo;
  private JLabel logoEtb;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_72;
  private XRiTextField T1CNV;
  private JLabel OBJ_50;
  private XRiComboBox T1TRA;
  private JLabel OBJ_49;
  private RiZoneSortie T1RAT;
  private JLabel OBJ_45;
  private RiZoneSortie T1ETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel FR2;
  private SNPanel pnlAutorisation;
  private SNLabelChamp lbNumeroAutorisation;
  private SNTexte CDNAU;
  private SNLabelChamp lbValidite;
  private XRiCalendrier CDDTDX;
  private SNLabelChamp lbAu;
  private XRiCalendrier CDDTFX;
  private SNPanel pnlUV;
  private SNLabelChamp lbUV;
  private SNUnite snUnite;
  private SNPanelTitre pnlVente;
  private SNLabelChamp lbCoefficient1;
  private XRiTextField T1COE;
  private SNLabelUnite lbSurPrix1;
  private SNLabelChamp lbOu1;
  private SNLabelChamp lbPrixNet1;
  private XRiTextField T1VALX;
  private SNPanelTitre sNPanelTitre3;
  private SNLabelUnite lbDebut;
  private SNLabelUnite lbFin;
  private XRiCalendrier T1DTDX;
  private XRiCalendrier T1DTFX;
  private SNPanelTitre pnlAchat;
  private SNLabelChamp lbCoefficient2;
  private XRiTextField T2COE;
  private SNLabelUnite lbSurPrix2;
  private SNLabelChamp lbOu2;
  private SNLabelChamp lbPrixNet2;
  private XRiTextField T1VALAX;
  private JPopupMenu BTD;
  private JMenuItem help;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
