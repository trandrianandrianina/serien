
package ri.serien.libecranrpg.vgvx.VGVX101F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX101F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private boolean isvolume = false;
  private boolean isLongueur = false;
  private boolean isSurface = false;
  private String[] dimensions_Value = { "", "1", "2", "3" };
  private String[] dimensions_Libelle = { "m", "m", "cm", "mm" };
  
  public VGVX101F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    A1IN28.setValeurs(dimensions_Value, dimensions_Libelle);
    A1IN29.setValeurs(dimensions_Value, dimensions_Libelle);
    A1IN30.setValeurs(dimensions_Value, dimensions_Libelle);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WQTEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQTEX@")).trim());
    A1UNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNV@")).trim());
    A1LB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB3@")).trim());
    A1ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1ART@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1LB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB1@")).trim());
    A1LB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB2@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Dimensions en @UNDIM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    isvolume = lexique.isTrue("33");
    isLongueur = lexique.isTrue("31");
    isSurface = lexique.isTrue("(N31) AND (N33)");
    
    // EN VOLUME
    label1.setVisible(lexique.isPresent("WNBR"));
    label6.setVisible(lexique.isPresent("WNBR"));
    if (isvolume) {
      panel2.setBounds(65, panel2.getY(), panel2.getWidth(), panel2.getHeight());
    }
    else {
      panel2.setBounds(155, panel2.getY(), panel2.getWidth(), panel2.getHeight());
    }
    label9.setVisible(isvolume);
    
    // EN LONGUEUR
    label10.setVisible(isLongueur);
    label2.setVisible(!isLongueur);
    label4.setVisible(!isLongueur);
    label7.setVisible(!isLongueur);
    label8.setVisible(!isLongueur);
    label9.setVisible(!isLongueur);
    // WQT3X.setVisible(!isLongueur);
    
    if (isLongueur) {
      WQT1X.setBounds(110, WQT1X.getY(), WQT1X.getWidth(), WQT1X.getHeight());
    }
    else {
      WQT1X.setBounds(155, WQT1X.getY(), WQT1X.getWidth(), WQT1X.getHeight());
    }
    
    // EN SURFACE
    // WQT2X.setVisible(isSurface);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Saisie de quantité sur liste"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    label3 = new JLabel();
    WNBR = new XRiTextField();
    WQTEX = new RiZoneSortie();
    label5 = new JLabel();
    A1UNV = new RiZoneSortie();
    A1LB3 = new RiZoneSortie();
    A1ART = new RiZoneSortie();
    A1LIB = new RiZoneSortie();
    A1LB1 = new RiZoneSortie();
    A1LB2 = new RiZoneSortie();
    label6 = new JLabel();
    label7 = new JLabel();
    label9 = new JLabel();
    A1IN30 = new XRiComboBox();
    panel2 = new JPanel();
    label2 = new JLabel();
    label8 = new JLabel();
    WQT1X = new XRiTextField();
    label4 = new JLabel();
    WQT2X = new XRiTextField();
    A1IN28 = new XRiComboBox();
    A1IN29 = new XRiComboBox();
    label10 = new JLabel();
    WQT3X = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(850, 240));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Dimensions"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- label1 ----
          label1.setText("Nombre");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(25, 44, 55, 21);

          //---- label3 ----
          label3.setText("Article");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(25, 100, 55, 21);

          //---- WNBR ----
          WNBR.setHorizontalAlignment(SwingConstants.RIGHT);
          WNBR.setNextFocusableComponent(WQT1X);
          WNBR.setName("WNBR");
          panel1.add(WNBR);
          WNBR.setBounds(80, 40, 52, WNBR.getPreferredSize().height);

          //---- WQTEX ----
          WQTEX.setText("@WQTEX@");
          WQTEX.setHorizontalAlignment(SwingConstants.RIGHT);
          WQTEX.setName("WQTEX");
          panel1.add(WQTEX);
          WQTEX.setBounds(520, 42, 84, WQTEX.getPreferredSize().height);

          //---- label5 ----
          label5.setText("=");
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD, label5.getFont().getSize() + 2f));
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(490, 44, 25, 21);

          //---- A1UNV ----
          A1UNV.setText("@A1UNV@");
          A1UNV.setName("A1UNV");
          panel1.add(A1UNV);
          A1UNV.setBounds(610, 42, 34, A1UNV.getPreferredSize().height);

          //---- A1LB3 ----
          A1LB3.setText("@A1LB3@");
          A1LB3.setName("A1LB3");
          panel1.add(A1LB3);
          A1LB3.setBounds(310, 173, 314, A1LB3.getPreferredSize().height);

          //---- A1ART ----
          A1ART.setText("@A1ART@");
          A1ART.setName("A1ART");
          panel1.add(A1ART);
          A1ART.setBounds(80, 98, 214, A1ART.getPreferredSize().height);

          //---- A1LIB ----
          A1LIB.setText("@A1LIB@");
          A1LIB.setName("A1LIB");
          panel1.add(A1LIB);
          A1LIB.setBounds(310, 98, 314, A1LIB.getPreferredSize().height);

          //---- A1LB1 ----
          A1LB1.setText("@A1LB1@");
          A1LB1.setName("A1LB1");
          panel1.add(A1LB1);
          A1LB1.setBounds(310, 123, 314, A1LB1.getPreferredSize().height);

          //---- A1LB2 ----
          A1LB2.setText("@A1LB2@");
          A1LB2.setName("A1LB2");
          panel1.add(A1LB2);
          A1LB2.setBounds(310, 148, 314, A1LB2.getPreferredSize().height);

          //---- label6 ----
          label6.setText("X");
          label6.setHorizontalAlignment(SwingConstants.CENTER);
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(136, 44, 25, 21);

          //---- label7 ----
          label7.setText(")");
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getSize() + 5f));
          label7.setHorizontalAlignment(SwingConstants.CENTER);
          label7.setVerticalAlignment(SwingConstants.TOP);
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(480, 40, 10, 28);

          //---- label9 ----
          label9.setText("X");
          label9.setHorizontalAlignment(SwingConstants.CENTER);
          label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(385, 45, 20, 21);

          //---- A1IN30 ----
          A1IN30.setName("A1IN30");
          panel1.add(A1IN30);
          A1IN30.setBounds(412, 65, 60, A1IN30.getPreferredSize().height);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- label2 ----
            label2.setText("Dimensions en @UNDIM@");
            label2.setName("label2");
            panel2.add(label2);
            label2.setBounds(5, 35, 140, 28);

            //---- label8 ----
            label8.setText("(");
            label8.setFont(label8.getFont().deriveFont(label8.getFont().getSize() + 5f));
            label8.setHorizontalAlignment(SwingConstants.CENTER);
            label8.setVerticalAlignment(SwingConstants.TOP);
            label8.setName("label8");
            panel2.add(label8);
            label8.setBounds(145, 10, 10, 28);

            //---- WQT1X ----
            WQT1X.setHorizontalAlignment(SwingConstants.RIGHT);
            WQT1X.setNextFocusableComponent(WQT2X);
            WQT1X.setName("WQT1X");
            panel2.add(WQT1X);
            WQT1X.setBounds(155, 10, 74, WQT1X.getPreferredSize().height);

            //---- label4 ----
            label4.setText("X");
            label4.setHorizontalAlignment(SwingConstants.CENTER);
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");
            panel2.add(label4);
            label4.setBounds(230, 15, 20, 21);

            //---- WQT2X ----
            WQT2X.setHorizontalAlignment(SwingConstants.RIGHT);
            WQT2X.setNextFocusableComponent(WQT3X);
            WQT2X.setName("WQT2X");
            panel2.add(WQT2X);
            WQT2X.setBounds(250, 10, 74, WQT2X.getPreferredSize().height);

            //---- A1IN28 ----
            A1IN28.setName("A1IN28");
            panel2.add(A1IN28);
            A1IN28.setBounds(162, 35, 60, A1IN28.getPreferredSize().height);

            //---- A1IN29 ----
            A1IN29.setName("A1IN29");
            panel2.add(A1IN29);
            A1IN29.setBounds(257, 35, 60, A1IN29.getPreferredSize().height);

            //---- label10 ----
            label10.setText("longueur");
            label10.setName("label10");
            panel2.add(label10);
            label10.setBounds(20, 10, 90, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(155, 30, 325, 65);

          //---- WQT3X ----
          WQT3X.setHorizontalAlignment(SwingConstants.RIGHT);
          WQT3X.setNextFocusableComponent(WNBR);
          WQT3X.setName("WQT3X");
          panel1.add(WQT3X);
          WQT3X.setBounds(405, 40, 74, WQT3X.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 660, 220);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label1;
  private JLabel label3;
  private XRiTextField WNBR;
  private RiZoneSortie WQTEX;
  private JLabel label5;
  private RiZoneSortie A1UNV;
  private RiZoneSortie A1LB3;
  private RiZoneSortie A1ART;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1LB1;
  private RiZoneSortie A1LB2;
  private JLabel label6;
  private JLabel label7;
  private JLabel label9;
  private XRiComboBox A1IN30;
  private JPanel panel2;
  private JLabel label2;
  private JLabel label8;
  private XRiTextField WQT1X;
  private JLabel label4;
  private XRiTextField WQT2X;
  private XRiComboBox A1IN28;
  private XRiComboBox A1IN29;
  private JLabel label10;
  private XRiTextField WQT3X;
  // JFormDesigner - End of variables declaration  //GEN-END:variables


}
