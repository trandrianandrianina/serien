
package ri.serien.libecranrpg.vgvx.VGVXLMFM;
// Nom Fichier: b_VGVXLMFM_FMTB3_214.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXLMFM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXLMFM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDLET.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDLET@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAG@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SUIT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - LETTRES TYPE"));
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    p_nord = new JPanel();
    p_presentation = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_37 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_38 = new JLabel();
    INDLET = new RiZoneSortie();
    OBJ_42 = new JLabel();
    LMOBJ = new XRiTextField();
    OBJ_43 = new JLabel();
    LMREF = new XRiTextField();
    p_tete_droite = new JPanel();
    OBJ_44 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_45 = new JLabel();
    V06F1 = new XRiTextField();
    LML01 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    LML2 = new XRiTextField();
    label3 = new JLabel();
    LML3 = new XRiTextField();
    label4 = new JLabel();
    LML4 = new XRiTextField();
    label5 = new JLabel();
    LML5 = new XRiTextField();
    label6 = new JLabel();
    LML6 = new XRiTextField();
    label7 = new JLabel();
    LML7 = new XRiTextField();
    label8 = new JLabel();
    LML8 = new XRiTextField();
    label9 = new JLabel();
    LML9 = new XRiTextField();
    label10 = new JLabel();
    LML10 = new XRiTextField();
    label11 = new JLabel();
    LML11 = new XRiTextField();
    label12 = new JLabel();
    LML12 = new XRiTextField();
    label13 = new JLabel();
    LML13 = new XRiTextField();
    label14 = new JLabel();
    LML14 = new XRiTextField();
    label15 = new JLabel();
    LML15 = new XRiTextField();
    label16 = new JLabel();
    LML16 = new XRiTextField();
    label17 = new JLabel();
    LML17 = new XRiTextField();
    label18 = new JLabel();
    LML18 = new XRiTextField();
    label19 = new JLabel();
    LML19 = new XRiTextField();
    label20 = new JLabel();
    LML20 = new XRiTextField();
    label21 = new JLabel();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //======== p_presentation ========
      {
        p_presentation.setPreferredSize(new Dimension(1020, 55));
        p_presentation.setMinimumSize(new Dimension(950, 55));
        p_presentation.setMaximumSize(new Dimension(32767, 55));
        p_presentation.setBackground(Color.white);
        p_presentation.setAutoscrolls(true);
        p_presentation.setFocusable(false);
        p_presentation.setName("p_presentation");
        p_presentation.setLayout(new BorderLayout());

        //---- p_bpresentation ----
        p_bpresentation.setText("Lettres type");
        p_bpresentation.setName("p_bpresentation");
        p_presentation.add(p_bpresentation, BorderLayout.CENTER);
      }
      p_nord.add(p_presentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_37 ----
          OBJ_37.setText("Etablissement");
          OBJ_37.setName("OBJ_37");
          p_tete_gauche.add(OBJ_37);
          OBJ_37.setBounds(5, 5, 100, 20);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(120, 3, 40, INDETB.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setText("Code lettre");
          OBJ_38.setName("OBJ_38");
          p_tete_gauche.add(OBJ_38);
          OBJ_38.setBounds(180, 5, 111, 20);

          //---- INDLET ----
          INDLET.setComponentPopupMenu(BTD);
          INDLET.setText("@INDLET@");
          INDLET.setOpaque(false);
          INDLET.setName("INDLET");
          p_tete_gauche.add(INDLET);
          INDLET.setBounds(295, 3, 110, INDLET.getPreferredSize().height);

          //---- OBJ_42 ----
          OBJ_42.setText("Objet");
          OBJ_42.setName("OBJ_42");
          p_tete_gauche.add(OBJ_42);
          OBJ_42.setBounds(415, 5, 40, 20);

          //---- LMOBJ ----
          LMOBJ.setComponentPopupMenu(BTD);
          LMOBJ.setName("LMOBJ");
          p_tete_gauche.add(LMOBJ);
          LMOBJ.setBounds(455, 1, 160, LMOBJ.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setText("R\u00e9f\u00e9rence");
          OBJ_43.setName("OBJ_43");
          p_tete_gauche.add(OBJ_43);
          OBJ_43.setBounds(630, 5, 65, 20);

          //---- LMREF ----
          LMREF.setComponentPopupMenu(BTD);
          LMREF.setName("LMREF");
          p_tete_gauche.add(LMREF);
          LMREF.setBounds(695, 1, 160, LMREF.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_44 ----
          OBJ_44.setText("@WPAG@");
          OBJ_44.setFont(OBJ_44.getFont().deriveFont(OBJ_44.getFont().getStyle() | Font.BOLD, OBJ_44.getFont().getSize() + 3f));
          OBJ_44.setName("OBJ_44");
          p_tete_droite.add(OBJ_44);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_45 ----
            OBJ_45.setText("@SUIT@");
            OBJ_45.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(790, 529, 120, 30);

            //---- V06F1 ----
            V06F1.setComponentPopupMenu(BTD);
            V06F1.setName("V06F1");
            panel1.add(V06F1);
            V06F1.setBounds(915, 530, 20, V06F1.getPreferredSize().height);

            //---- LML01 ----
            LML01.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML01.setName("LML01");
            panel1.add(LML01);
            LML01.setBounds(25, 25, 910, LML01.getPreferredSize().height);

            //---- label1 ----
            label1.setText("1");
            label1.setHorizontalAlignment(SwingConstants.RIGHT);
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(5, 25, 15, 28);

            //---- label2 ----
            label2.setText("2");
            label2.setHorizontalAlignment(SwingConstants.RIGHT);
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(5, 50, 15, 28);

            //---- LML2 ----
            LML2.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML2.setName("LML2");
            panel1.add(LML2);
            LML2.setBounds(25, 50, 910, LML2.getPreferredSize().height);

            //---- label3 ----
            label3.setText("3");
            label3.setHorizontalAlignment(SwingConstants.RIGHT);
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(5, 75, 15, 28);

            //---- LML3 ----
            LML3.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML3.setName("LML3");
            panel1.add(LML3);
            LML3.setBounds(25, 75, 910, LML3.getPreferredSize().height);

            //---- label4 ----
            label4.setText("4");
            label4.setHorizontalAlignment(SwingConstants.RIGHT);
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(5, 100, 15, 28);

            //---- LML4 ----
            LML4.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML4.setName("LML4");
            panel1.add(LML4);
            LML4.setBounds(25, 100, 910, LML4.getPreferredSize().height);

            //---- label5 ----
            label5.setText("5");
            label5.setHorizontalAlignment(SwingConstants.RIGHT);
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(5, 125, 15, 28);

            //---- LML5 ----
            LML5.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML5.setName("LML5");
            panel1.add(LML5);
            LML5.setBounds(25, 125, 910, LML5.getPreferredSize().height);

            //---- label6 ----
            label6.setText("6");
            label6.setHorizontalAlignment(SwingConstants.RIGHT);
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setName("label6");
            panel1.add(label6);
            label6.setBounds(5, 150, 15, 28);

            //---- LML6 ----
            LML6.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML6.setName("LML6");
            panel1.add(LML6);
            LML6.setBounds(25, 150, 910, LML6.getPreferredSize().height);

            //---- label7 ----
            label7.setText("7");
            label7.setHorizontalAlignment(SwingConstants.RIGHT);
            label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
            label7.setName("label7");
            panel1.add(label7);
            label7.setBounds(5, 175, 15, 28);

            //---- LML7 ----
            LML7.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML7.setName("LML7");
            panel1.add(LML7);
            LML7.setBounds(25, 175, 910, LML7.getPreferredSize().height);

            //---- label8 ----
            label8.setText("8");
            label8.setHorizontalAlignment(SwingConstants.RIGHT);
            label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
            label8.setName("label8");
            panel1.add(label8);
            label8.setBounds(5, 200, 15, 28);

            //---- LML8 ----
            LML8.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML8.setName("LML8");
            panel1.add(LML8);
            LML8.setBounds(25, 200, 910, LML8.getPreferredSize().height);

            //---- label9 ----
            label9.setText("9");
            label9.setHorizontalAlignment(SwingConstants.RIGHT);
            label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
            label9.setName("label9");
            panel1.add(label9);
            label9.setBounds(5, 225, 15, 28);

            //---- LML9 ----
            LML9.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML9.setName("LML9");
            panel1.add(LML9);
            LML9.setBounds(25, 225, 910, LML9.getPreferredSize().height);

            //---- label10 ----
            label10.setText("10");
            label10.setHorizontalAlignment(SwingConstants.RIGHT);
            label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
            label10.setName("label10");
            panel1.add(label10);
            label10.setBounds(5, 250, 15, 28);

            //---- LML10 ----
            LML10.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML10.setName("LML10");
            panel1.add(LML10);
            LML10.setBounds(25, 250, 910, LML10.getPreferredSize().height);

            //---- label11 ----
            label11.setText("11");
            label11.setHorizontalAlignment(SwingConstants.RIGHT);
            label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
            label11.setName("label11");
            panel1.add(label11);
            label11.setBounds(5, 275, 15, 28);

            //---- LML11 ----
            LML11.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML11.setName("LML11");
            panel1.add(LML11);
            LML11.setBounds(25, 275, 910, LML11.getPreferredSize().height);

            //---- label12 ----
            label12.setText("12");
            label12.setHorizontalAlignment(SwingConstants.RIGHT);
            label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
            label12.setName("label12");
            panel1.add(label12);
            label12.setBounds(5, 300, 15, 28);

            //---- LML12 ----
            LML12.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML12.setName("LML12");
            panel1.add(LML12);
            LML12.setBounds(25, 300, 910, LML12.getPreferredSize().height);

            //---- label13 ----
            label13.setText("13");
            label13.setHorizontalAlignment(SwingConstants.RIGHT);
            label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
            label13.setName("label13");
            panel1.add(label13);
            label13.setBounds(5, 325, 15, 28);

            //---- LML13 ----
            LML13.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML13.setName("LML13");
            panel1.add(LML13);
            LML13.setBounds(25, 325, 910, LML13.getPreferredSize().height);

            //---- label14 ----
            label14.setText("14");
            label14.setHorizontalAlignment(SwingConstants.RIGHT);
            label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
            label14.setName("label14");
            panel1.add(label14);
            label14.setBounds(5, 350, 15, 28);

            //---- LML14 ----
            LML14.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML14.setName("LML14");
            panel1.add(LML14);
            LML14.setBounds(25, 350, 910, LML14.getPreferredSize().height);

            //---- label15 ----
            label15.setText("15");
            label15.setHorizontalAlignment(SwingConstants.RIGHT);
            label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
            label15.setName("label15");
            panel1.add(label15);
            label15.setBounds(5, 375, 15, 28);

            //---- LML15 ----
            LML15.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML15.setName("LML15");
            panel1.add(LML15);
            LML15.setBounds(25, 375, 910, LML15.getPreferredSize().height);

            //---- label16 ----
            label16.setText("16");
            label16.setHorizontalAlignment(SwingConstants.RIGHT);
            label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
            label16.setName("label16");
            panel1.add(label16);
            label16.setBounds(5, 400, 15, 28);

            //---- LML16 ----
            LML16.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML16.setName("LML16");
            panel1.add(LML16);
            LML16.setBounds(25, 400, 910, LML16.getPreferredSize().height);

            //---- label17 ----
            label17.setText("17");
            label17.setHorizontalAlignment(SwingConstants.RIGHT);
            label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
            label17.setName("label17");
            panel1.add(label17);
            label17.setBounds(5, 425, 15, 28);

            //---- LML17 ----
            LML17.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML17.setName("LML17");
            panel1.add(LML17);
            LML17.setBounds(25, 425, 910, LML17.getPreferredSize().height);

            //---- label18 ----
            label18.setText("18");
            label18.setHorizontalAlignment(SwingConstants.RIGHT);
            label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
            label18.setName("label18");
            panel1.add(label18);
            label18.setBounds(5, 450, 15, 28);

            //---- LML18 ----
            LML18.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML18.setName("LML18");
            panel1.add(LML18);
            LML18.setBounds(25, 450, 910, LML18.getPreferredSize().height);

            //---- label19 ----
            label19.setText("19");
            label19.setHorizontalAlignment(SwingConstants.RIGHT);
            label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
            label19.setName("label19");
            panel1.add(label19);
            label19.setBounds(5, 475, 15, 28);

            //---- LML19 ----
            LML19.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML19.setName("LML19");
            panel1.add(LML19);
            LML19.setBounds(25, 475, 910, LML19.getPreferredSize().height);

            //---- label20 ----
            label20.setText("20");
            label20.setHorizontalAlignment(SwingConstants.RIGHT);
            label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
            label20.setName("label20");
            panel1.add(label20);
            label20.setBounds(5, 500, 15, 28);

            //---- LML20 ----
            LML20.setFont(new Font("Courier New", Font.PLAIN, 12));
            LML20.setName("LML20");
            panel1.add(LML20);
            LML20.setBounds(25, 500, 910, LML20.getPreferredSize().height);

            //---- label21 ----
            label21.setText("0---:----1----:----2----:----3----:----4----:----5----:----6----:----7----:----8----:----9----:----0----:----1----:----2----:---");
            label21.setFont(new Font("Courier New", label21.getFont().getStyle() | Font.BOLD, label21.getFont().getSize()));
            label21.setName("label21");
            panel1.add(label21);
            label21.setBounds(30, 5, 905, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JPanel p_nord;
  private JPanel p_presentation;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_37;
  private RiZoneSortie INDETB;
  private JLabel OBJ_38;
  private RiZoneSortie INDLET;
  private JLabel OBJ_42;
  private XRiTextField LMOBJ;
  private JLabel OBJ_43;
  private XRiTextField LMREF;
  private JPanel p_tete_droite;
  private JLabel OBJ_44;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_45;
  private XRiTextField V06F1;
  private XRiTextField LML01;
  private JLabel label1;
  private JLabel label2;
  private XRiTextField LML2;
  private JLabel label3;
  private XRiTextField LML3;
  private JLabel label4;
  private XRiTextField LML4;
  private JLabel label5;
  private XRiTextField LML5;
  private JLabel label6;
  private XRiTextField LML6;
  private JLabel label7;
  private XRiTextField LML7;
  private JLabel label8;
  private XRiTextField LML8;
  private JLabel label9;
  private XRiTextField LML9;
  private JLabel label10;
  private XRiTextField LML10;
  private JLabel label11;
  private XRiTextField LML11;
  private JLabel label12;
  private XRiTextField LML12;
  private JLabel label13;
  private XRiTextField LML13;
  private JLabel label14;
  private XRiTextField LML14;
  private JLabel label15;
  private XRiTextField LML15;
  private JLabel label16;
  private XRiTextField LML16;
  private JLabel label17;
  private XRiTextField LML17;
  private JLabel label18;
  private XRiTextField LML18;
  private JLabel label19;
  private XRiTextField LML19;
  private JLabel label20;
  private XRiTextField LML20;
  private JLabel label21;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
