
package ri.serien.libecranrpg.vgvx.VGVX38F2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX38F2_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX38F2_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SOLDE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    CSMAG.setEnabled(lexique.isPresent("CSMAG"));
    CSQTE.setEnabled(lexique.isPresent("CSQTE"));
    CSQTS.setEnabled(lexique.isPresent("CSQTS"));
    CSQT0.setEnabled(lexique.isPresent("CSQT0"));
    OBJ_35.setVisible(lexique.isPresent("SOLDE"));
    CSART.setEnabled(lexique.isPresent("CSART"));
    OBJ_29.setVisible(lexique.isPresent("MALIB"));
    OBJ_28.setVisible(lexique.isPresent("A1LIB"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt21ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_41ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="ENTER"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F16"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F16", true);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F18"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F18", true);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvx382"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_28 = new RiZoneSortie();
    OBJ_29 = new RiZoneSortie();
    CSART = new XRiTextField();
    P_PnlOpts = new JPanel();
    OBJ_30 = new JLabel();
    OBJ_35 = new RiZoneSortie();
    CSQT0 = new XRiTextField();
    CSQTS = new XRiTextField();
    CSQTE = new XRiTextField();
    OBJ_38 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_26 = new JLabel();
    CSMAG = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(810, 330));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("D\u00e9tails"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_28 ----
          OBJ_28.setText("@A1LIB@");
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(350, 47, 232, OBJ_28.getPreferredSize().height);

          //---- OBJ_29 ----
          OBJ_29.setText("@MALIB@");
          OBJ_29.setName("OBJ_29");
          p_recup.add(OBJ_29);
          OBJ_29.setBounds(175, 77, 232, OBJ_29.getPreferredSize().height);

          //---- CSART ----
          CSART.setComponentPopupMenu(BTD);
          CSART.setName("CSART");
          p_recup.add(CSART);
          CSART.setBounds(135, 45, 210, CSART.getPreferredSize().height);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_30 ----
          OBJ_30.setText("Quantit\u00e9 initiale");
          OBJ_30.setName("OBJ_30");
          p_recup.add(OBJ_30);
          OBJ_30.setBounds(30, 149, 108, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("@SOLDE@");
          OBJ_35.setName("OBJ_35");
          p_recup.add(OBJ_35);
          OBJ_35.setBounds(316, 177, 91, OBJ_35.getPreferredSize().height);

          //---- CSQT0 ----
          CSQT0.setComponentPopupMenu(BTD);
          CSQT0.setName("CSQT0");
          p_recup.add(CSQT0);
          CSQT0.setBounds(135, 145, 90, CSQT0.getPreferredSize().height);

          //---- CSQTS ----
          CSQTS.setComponentPopupMenu(BTD);
          CSQTS.setName("CSQTS");
          p_recup.add(CSQTS);
          CSQTS.setBounds(135, 175, 90, CSQTS.getPreferredSize().height);

          //---- CSQTE ----
          CSQTE.setComponentPopupMenu(BTD);
          CSQTE.setName("CSQTE");
          p_recup.add(CSQTE);
          CSQTE.setBounds(135, 205, 90, CSQTE.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setText("Magasin");
          OBJ_38.setName("OBJ_38");
          p_recup.add(OBJ_38);
          OBJ_38.setBounds(30, 79, 108, 20);

          //---- OBJ_36 ----
          OBJ_36.setText("Retours");
          OBJ_36.setName("OBJ_36");
          p_recup.add(OBJ_36);
          OBJ_36.setBounds(30, 209, 108, 20);

          //---- OBJ_32 ----
          OBJ_32.setText("Entr\u00e9es");
          OBJ_32.setName("OBJ_32");
          p_recup.add(OBJ_32);
          OBJ_32.setBounds(30, 179, 108, 20);

          //---- OBJ_34 ----
          OBJ_34.setText("Solde");
          OBJ_34.setName("OBJ_34");
          p_recup.add(OBJ_34);
          OBJ_34.setBounds(260, 179, 50, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("Article");
          OBJ_26.setName("OBJ_26");
          p_recup.add(OBJ_26);
          OBJ_26.setBounds(30, 49, 108, 20);

          //---- CSMAG ----
          CSMAG.setComponentPopupMenu(BTD);
          CSMAG.setName("CSMAG");
          p_recup.add(CSMAG);
          CSMAG.setBounds(135, 75, 34, CSMAG.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 610, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie OBJ_28;
  private RiZoneSortie OBJ_29;
  private XRiTextField CSART;
  private JPanel P_PnlOpts;
  private JLabel OBJ_30;
  private RiZoneSortie OBJ_35;
  private XRiTextField CSQT0;
  private XRiTextField CSQTS;
  private XRiTextField CSQTE;
  private JLabel OBJ_38;
  private JLabel OBJ_36;
  private JLabel OBJ_32;
  private JLabel OBJ_34;
  private JLabel OBJ_26;
  private XRiTextField CSMAG;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
