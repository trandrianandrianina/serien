
package ri.serien.libecranrpg.vgvx.VGVXBNFM;
// MON PACKAGE

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVXBNFM_O2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXBNFM_O2(ArrayList<?> param) {
    super(param);
    initComponents();
    
    setVersionLook(2);
    
    setDialog(true);
    // Ajout
    initDiverses();
    
    setDefaultButton(bouton_valider);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    PBNLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PBNLIB@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SVPA@")).trim());
  }
  
  @Override
  public void setData() {
    // TODO set Data
    
    super.setData();
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    OB1 = new XRiTextField();
    OB2 = new XRiTextField();
    OB3 = new XRiTextField();
    OB4 = new XRiTextField();
    OB5 = new XRiTextField();
    OB6 = new XRiTextField();
    OB7 = new XRiTextField();
    OB8 = new XRiTextField();
    separator1 = compFactory.createSeparator("Bloc-notes");
    PBNLIB = new JLabel();
    OB9 = new XRiTextField();
    OB10 = new XRiTextField();
    OB11 = new XRiTextField();
    OB12 = new XRiTextField();
    OBJ_29 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(715, 410));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- OB1 ----
        OB1.setName("OB1");
        p_contenu.add(OB1);
        OB1.setBounds(20, 70, 500, OB1.getPreferredSize().height);

        //---- OB2 ----
        OB2.setName("OB2");
        p_contenu.add(OB2);
        OB2.setBounds(20, 95, 500, OB2.getPreferredSize().height);

        //---- OB3 ----
        OB3.setName("OB3");
        p_contenu.add(OB3);
        OB3.setBounds(20, 120, 500, OB3.getPreferredSize().height);

        //---- OB4 ----
        OB4.setName("OB4");
        p_contenu.add(OB4);
        OB4.setBounds(20, 145, 500, OB4.getPreferredSize().height);

        //---- OB5 ----
        OB5.setName("OB5");
        p_contenu.add(OB5);
        OB5.setBounds(20, 170, 500, OB5.getPreferredSize().height);

        //---- OB6 ----
        OB6.setName("OB6");
        p_contenu.add(OB6);
        OB6.setBounds(20, 195, 500, OB6.getPreferredSize().height);

        //---- OB7 ----
        OB7.setName("OB7");
        p_contenu.add(OB7);
        OB7.setBounds(20, 220, 500, OB7.getPreferredSize().height);

        //---- OB8 ----
        OB8.setName("OB8");
        p_contenu.add(OB8);
        OB8.setBounds(20, 245, 500, OB8.getPreferredSize().height);

        //---- separator1 ----
        separator1.setName("separator1");
        p_contenu.add(separator1);
        separator1.setBounds(10, 10, 520, separator1.getPreferredSize().height);

        //---- PBNLIB ----
        PBNLIB.setText("@PBNLIB@");
        PBNLIB.setName("PBNLIB");
        p_contenu.add(PBNLIB);
        PBNLIB.setBounds(20, 30, 500, 20);

        //---- OB9 ----
        OB9.setName("OB9");
        p_contenu.add(OB9);
        OB9.setBounds(20, 270, 500, OB9.getPreferredSize().height);

        //---- OB10 ----
        OB10.setName("OB10");
        p_contenu.add(OB10);
        OB10.setBounds(20, 295, 500, OB10.getPreferredSize().height);

        //---- OB11 ----
        OB11.setName("OB11");
        p_contenu.add(OB11);
        OB11.setBounds(20, 320, 500, OB11.getPreferredSize().height);

        //---- OB12 ----
        OB12.setName("OB12");
        p_contenu.add(OB12);
        OB12.setBounds(20, 345, 500, OB12.getPreferredSize().height);

        //---- OBJ_29 ----
        OBJ_29.setText("@SVPA@");
        OBJ_29.setName("OBJ_29");
        p_contenu.add(OBJ_29);
        OBJ_29.setBounds(20, 375, 250, 25);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private XRiTextField OB1;
  private XRiTextField OB2;
  private XRiTextField OB3;
  private XRiTextField OB4;
  private XRiTextField OB5;
  private XRiTextField OB6;
  private XRiTextField OB7;
  private XRiTextField OB8;
  private JComponent separator1;
  private JLabel PBNLIB;
  private XRiTextField OB9;
  private XRiTextField OB10;
  private XRiTextField OB11;
  private XRiTextField OB12;
  private JLabel OBJ_29;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
