
package ri.serien.libecranrpg.vgvx.VGVX13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVX13FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _HD1_Title = { "Date", "Quantité", "Prix unitaire", "Libellé", };
  private String[][] _HD1_Data = { { "HD1", "HQ1", "HP1", "HL1", }, { "HD2", "HQ2", "HP2", "HL2", }, { "HD3", "HQ3", "HP3", "HL3", },
      { "HD4", "HQ4", "HP4", "HL4", }, { "HD5", "HQ5", "HP5", "HL5", }, };
  private int[] _HD1_Width = { 60, 95, 96, 180, };
  // private String[] _LIST_Top=null;
  
  public VGVX13FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    HD1.setAspectTable(null, _HD1_Title, _HD1_Data, _HD1_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1ART@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAGA@")).trim());
    OBJ_15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FOUR@")).trim());
    OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBORI@")).trim());
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEB@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H1MAG2@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBTYP@")).trim());
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H1DATX@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H1ORD@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H1OPE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top);
    
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    H1TI1.setVisible(lexique.isPresent("H1TI1"));
    OBJ_28.setVisible(lexique.isPresent("H1OPE"));
    H1SUF0.setVisible(lexique.isPresent("H1SUF0"));
    OBJ_33.setVisible(lexique.isPresent("H1MAG2"));
    H1TI3.setVisible(lexique.isPresent("H1TI3"));
    OBJ_26.setVisible(lexique.isPresent("H1ORD"));
    H1NLI0.setVisible(lexique.isPresent("H1NLI0"));
    OBJ_31.setVisible(lexique.isPresent("WEB"));
    H1NUM0.setVisible(lexique.isPresent("H1NUM0"));
    H1TI2.setVisible(lexique.isPresent("H1TI2"));
    H1TI22.setVisible(lexique.isPresent("H1TI22"));
    OBJ_41.setVisible(lexique.isPresent("LBORI"));
    OBJ_24.setVisible(lexique.isPresent("H1DATX"));
    OBJ_29.setVisible(lexique.isPresent("LBTYP"));
    OBJ_30.setVisible(lexique.isPresent("L1ART"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    // setTitle(???);
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvx13"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    HD1 = new XRiTable();
    OBJ_30 = new RiZoneSortie();
    OBJ_32 = new JLabel();
    OBJ_15 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_42 = new JLabel();
    H1TI22 = new XRiTextField();
    H1TI2 = new XRiTextField();
    H1NUM0 = new XRiTextField();
    OBJ_31 = new JLabel();
    H1NLI0 = new XRiTextField();
    H1TI3 = new XRiTextField();
    OBJ_33 = new JLabel();
    OBJ_43 = new JLabel();
    H1SUF0 = new XRiTextField();
    H1TI1 = new XRiTextField();
    barre_tete = new JMenuBar();
    panel1 = new JPanel();
    OBJ_23 = new JLabel();
    OBJ_29 = new RiZoneSortie();
    OBJ_24 = new RiZoneSortie();
    OBJ_25 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_26 = new RiZoneSortie();
    OBJ_28 = new RiZoneSortie();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 235));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setComponentPopupMenu(BTD);
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- HD1 ----
          HD1.setName("HD1");
          SCROLLPANE_LIST.setViewportView(HD1);
        }
        p_contenu.add(SCROLLPANE_LIST);
        SCROLLPANE_LIST.setBounds(30, 80, 550, 110);

        //---- OBJ_30 ----
        OBJ_30.setText("@L1ART@");
        OBJ_30.setName("OBJ_30");
        p_contenu.add(OBJ_30);
        OBJ_30.setBounds(15, 10, 345, 20);

        //---- OBJ_32 ----
        OBJ_32.setText("@MAGA@");
        OBJ_32.setName("OBJ_32");
        p_contenu.add(OBJ_32);
        OBJ_32.setBounds(450, 12, 111, 16);

        //---- OBJ_15 ----
        OBJ_15.setText("@FOUR@");
        OBJ_15.setName("OBJ_15");
        p_contenu.add(OBJ_15);
        OBJ_15.setBounds(375, 50, 91, 16);

        //---- OBJ_41 ----
        OBJ_41.setText("@LBORI@");
        OBJ_41.setName("OBJ_41");
        p_contenu.add(OBJ_41);
        OBJ_41.setBounds(30, 50, 75, 16);

        //---- OBJ_42 ----
        OBJ_42.setText("R\u00e9f\u00e9rence");
        OBJ_42.setName("OBJ_42");
        p_contenu.add(OBJ_42);
        OBJ_42.setBounds(120, 50, 75, 16);

        //---- H1TI22 ----
        H1TI22.setComponentPopupMenu(BTD);
        H1TI22.setName("H1TI22");
        p_contenu.add(H1TI22);
        H1TI22.setBounds(485, 45, 58, H1TI22.getPreferredSize().height);

        //---- H1TI2 ----
        H1TI2.setComponentPopupMenu(BTD);
        H1TI2.setName("H1TI2");
        p_contenu.add(H1TI2);
        H1TI2.setBounds(505, 45, 58, H1TI2.getPreferredSize().height);

        //---- H1NUM0 ----
        H1NUM0.setComponentPopupMenu(BTD);
        H1NUM0.setName("H1NUM0");
        p_contenu.add(H1NUM0);
        H1NUM0.setBounds(205, 45, 58, H1NUM0.getPreferredSize().height);

        //---- OBJ_31 ----
        OBJ_31.setText("@WEB@");
        OBJ_31.setName("OBJ_31");
        p_contenu.add(OBJ_31);
        OBJ_31.setBounds(405, 10, 33, 20);

        //---- H1NLI0 ----
        H1NLI0.setComponentPopupMenu(BTD);
        H1NLI0.setName("H1NLI0");
        p_contenu.add(H1NLI0);
        H1NLI0.setBounds(325, 45, 34, H1NLI0.getPreferredSize().height);

        //---- H1TI3 ----
        H1TI3.setComponentPopupMenu(BTD);
        H1TI3.setName("H1TI3");
        p_contenu.add(H1TI3);
        H1TI3.setBounds(545, 45, 34, H1TI3.getPreferredSize().height);

        //---- OBJ_33 ----
        OBJ_33.setText("@H1MAG2@");
        OBJ_33.setName("OBJ_33");
        p_contenu.add(OBJ_33);
        OBJ_33.setBounds(580, 12, 21, 16);

        //---- OBJ_43 ----
        OBJ_43.setText("sur");
        OBJ_43.setName("OBJ_43");
        p_contenu.add(OBJ_43);
        OBJ_43.setBounds(265, 50, 21, 16);

        //---- H1SUF0 ----
        H1SUF0.setComponentPopupMenu(BTD);
        H1SUF0.setName("H1SUF0");
        p_contenu.add(H1SUF0);
        H1SUF0.setBounds(295, 45, 18, H1SUF0.getPreferredSize().height);

        //---- H1TI1 ----
        H1TI1.setComponentPopupMenu(BTD);
        H1TI1.setName("H1TI1");
        p_contenu.add(H1TI1);
        H1TI1.setBounds(485, 45, 18, H1TI1.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- OBJ_23 ----
        OBJ_23.setText("Modification mouvement");
        OBJ_23.setName("OBJ_23");
        panel1.add(OBJ_23);
        OBJ_23.setBounds(15, 4, 145, 20);

        //---- OBJ_29 ----
        OBJ_29.setText("@LBTYP@");
        OBJ_29.setOpaque(false);
        OBJ_29.setName("OBJ_29");
        panel1.add(OBJ_29);
        OBJ_29.setBounds(469, 2, 117, OBJ_29.getPreferredSize().height);

        //---- OBJ_24 ----
        OBJ_24.setText("@H1DATX@");
        OBJ_24.setOpaque(false);
        OBJ_24.setName("OBJ_24");
        panel1.add(OBJ_24);
        OBJ_24.setBounds(165, 2, 73, OBJ_24.getPreferredSize().height);

        //---- OBJ_25 ----
        OBJ_25.setText("N\u00b0 d'ordre");
        OBJ_25.setName("OBJ_25");
        panel1.add(OBJ_25);
        OBJ_25.setBounds(263, 6, 64, OBJ_25.getPreferredSize().height);

        //---- OBJ_27 ----
        OBJ_27.setText("Type");
        OBJ_27.setName("OBJ_27");
        panel1.add(OBJ_27);
        OBJ_27.setBounds(414, 6, 36, OBJ_27.getPreferredSize().height);

        //---- OBJ_26 ----
        OBJ_26.setText("@H1ORD@");
        OBJ_26.setOpaque(false);
        OBJ_26.setName("OBJ_26");
        panel1.add(OBJ_26);
        OBJ_26.setBounds(334, 2, 34, OBJ_26.getPreferredSize().height);

        //---- OBJ_28 ----
        OBJ_28.setText("@H1OPE@");
        OBJ_28.setOpaque(false);
        OBJ_28.setName("OBJ_28");
        panel1.add(OBJ_28);
        OBJ_28.setBounds(449, 2, 18, OBJ_28.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(panel1);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Annuler");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Exploitation");
      OBJ_7.setName("OBJ_7");
      OBJ_4.add(OBJ_7);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);

      //---- OBJ_10 ----
      OBJ_10.setText("Invite");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable HD1;
  private RiZoneSortie OBJ_30;
  private JLabel OBJ_32;
  private JLabel OBJ_15;
  private JLabel OBJ_41;
  private JLabel OBJ_42;
  private XRiTextField H1TI22;
  private XRiTextField H1TI2;
  private XRiTextField H1NUM0;
  private JLabel OBJ_31;
  private XRiTextField H1NLI0;
  private XRiTextField H1TI3;
  private JLabel OBJ_33;
  private JLabel OBJ_43;
  private XRiTextField H1SUF0;
  private XRiTextField H1TI1;
  private JMenuBar barre_tete;
  private JPanel panel1;
  private JLabel OBJ_23;
  private RiZoneSortie OBJ_29;
  private RiZoneSortie OBJ_24;
  private JLabel OBJ_25;
  private JLabel OBJ_27;
  private RiZoneSortie OBJ_26;
  private RiZoneSortie OBJ_28;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
