
package ri.serien.libecranrpg.vgvx.VGVX14FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX14FX_AA extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX14FX_AA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VDNOM@")).trim());
    NUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNUM@")).trim());
    E1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ETB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLBOPE@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDMINX@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDMAXX@")).trim());
    E1OPE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1OPE@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLBMAG@")).trim());
    OBJ_72.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLBMA2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_73.setVisible(lexique.isPresent("TRSTI1"));
    OBJ_63.setVisible(lexique.isPresent("EMTTI1"));
    OBJ_53.setVisible(lexique.isPresent("WDMAXX"));
    OBJ_47.setVisible(lexique.isPresent("WDMINX"));
    OBJ_70.setVisible(lexique.isPresent("MAG2"));
    OBJ_52.setVisible(lexique.isPresent("WDATX"));
    OBJ_43.setVisible(lexique.isPresent("VDNOM"));
    OBJ_17.setVisible(lexique.isPresent("WLBOPE"));
    OBJ_72.setVisible(lexique.isPresent("WLBMA2"));
    OBJ_62.setVisible(lexique.isPresent("WLBMAG"));
    
    p_bpresentation.setCodeEtablissement(E1ETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(E1ETB.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_43 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_35 = new JLabel();
    NUM = new RiZoneSortie();
    E1ETB = new RiZoneSortie();
    E1UTI = new XRiTextField();
    OBJ_34 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_17 = new RiZoneSortie();
    OBJ_19 = new JLabel();
    WDATX = new XRiCalendrier();
    OBJ_52 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_54 = new JLabel();
    E1OPE = new RiZoneSortie();
    OBJ_57 = new JLabel();
    OBJ_79 = new JXTitledSeparator();
    OBJ_62 = new RiZoneSortie();
    OBJ_72 = new RiZoneSortie();
    OBJ_85 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_70 = new JLabel();
    E20NAT = new XRiTextField();
    E1MAG1 = new XRiTextField();
    E1MAG2 = new XRiTextField();
    EMTTI1 = new XRiTextField();
    TRSTI1 = new XRiTextField();
    OBJ_49 = new JLabel();
    SECTE = new XRiTextField();
    ACTE = new XRiTextField();
    SAN = new XRiTextField();
    ACT = new XRiTextField();
    OBJ_63 = new JLabel();
    OBJ_73 = new JLabel();
    EMTTI2 = new XRiTextField();
    TRSTI2 = new XRiTextField();
    OBJ_68 = new JLabel();
    OBJ_83 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Bordereau de stock");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(800, 32));
          p_tete_gauche.setMinimumSize(new Dimension(800, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_43 ----
          OBJ_43.setText("@VDNOM@");
          OBJ_43.setName("OBJ_43");

          //---- OBJ_41 ----
          OBJ_41.setText("Magasinier");
          OBJ_41.setName("OBJ_41");

          //---- OBJ_35 ----
          OBJ_35.setText("N\u00b0 de bordereau");
          OBJ_35.setName("OBJ_35");

          //---- NUM ----
          NUM.setComponentPopupMenu(null);
          NUM.setOpaque(false);
          NUM.setText("@WNUM@");
          NUM.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM.setName("NUM");

          //---- E1ETB ----
          E1ETB.setComponentPopupMenu(null);
          E1ETB.setOpaque(false);
          E1ETB.setText("@E1ETB@");
          E1ETB.setName("E1ETB");

          //---- E1UTI ----
          E1UTI.setComponentPopupMenu(BTD);
          E1UTI.setName("E1UTI");

          //---- OBJ_34 ----
          OBJ_34.setText("Etablissement");
          OBJ_34.setName("OBJ_34");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(NUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(78, 78, 78)
                    .addComponent(E1UTI, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(NUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(E1UTI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 250));
            menus_haut.setPreferredSize(new Dimension(160, 250));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(750, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setBorder(new TitledBorder(""));
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_17 ----
            OBJ_17.setText("@WLBOPE@");
            OBJ_17.setComponentPopupMenu(null);
            OBJ_17.setName("OBJ_17");
            panel1.add(OBJ_17);
            OBJ_17.setBounds(185, 37, 222, OBJ_17.getPreferredSize().height);

            //---- OBJ_19 ----
            OBJ_19.setText("Code op\u00e9ration");
            OBJ_19.setName("OBJ_19");
            panel1.add(OBJ_19);
            OBJ_19.setBounds(30, 39, 115, 20);

            //---- WDATX ----
            WDATX.setComponentPopupMenu(null);
            WDATX.setName("WDATX");
            panel1.add(WDATX);
            WDATX.setBounds(500, 35, 105, WDATX.getPreferredSize().height);

            //---- OBJ_52 ----
            OBJ_52.setText("En date du");
            OBJ_52.setName("OBJ_52");
            panel1.add(OBJ_52);
            OBJ_52.setBounds(430, 39, 70, 20);

            //---- OBJ_47 ----
            OBJ_47.setText("@WDMINX@");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(500, 15, 65, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("@WDMAXX@");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(500, 65, 65, 20);

            //---- OBJ_48 ----
            OBJ_48.setText("Mini");
            OBJ_48.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(565, 15, 39, 20);

            //---- OBJ_54 ----
            OBJ_54.setText("Max");
            OBJ_54.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_54.setName("OBJ_54");
            panel1.add(OBJ_54);
            OBJ_54.setBounds(565, 65, 39, 20);

            //---- E1OPE ----
            E1OPE.setComponentPopupMenu(null);
            E1OPE.setText("@E1OPE@");
            E1OPE.setName("E1OPE");
            panel1.add(E1OPE);
            E1OPE.setBounds(145, 37, 24, E1OPE.getPreferredSize().height);

            //---- OBJ_57 ----
            OBJ_57.setText("Magasin \u00e9metteur");
            OBJ_57.setName("OBJ_57");
            panel1.add(OBJ_57);
            OBJ_57.setBounds(30, 73, 115, 22);

            //---- OBJ_79 ----
            OBJ_79.setTitle("Imputation");
            OBJ_79.setName("OBJ_79");
            panel1.add(OBJ_79);
            OBJ_79.setBounds(30, 238, 570, 22);

            //---- OBJ_62 ----
            OBJ_62.setText("@WLBMAG@");
            OBJ_62.setName("OBJ_62");
            panel1.add(OBJ_62);
            OBJ_62.setBounds(185, 72, 290, OBJ_62.getPreferredSize().height);

            //---- OBJ_72 ----
            OBJ_72.setText("@WLBMA2@");
            OBJ_72.setName("OBJ_72");
            panel1.add(OBJ_72);
            OBJ_72.setBounds(185, 167, 290, OBJ_72.getPreferredSize().height);

            //---- OBJ_85 ----
            OBJ_85.setText("(\u00e0 R\u00e9percuter/lignes)");
            OBJ_85.setName("OBJ_85");
            panel1.add(OBJ_85);
            OBJ_85.setBounds(435, 274, 151, 20);

            //---- OBJ_66 ----
            OBJ_66.setText("Section / affaire");
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(30, 109, 115, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("Section / Affaire");
            OBJ_81.setName("OBJ_81");
            panel1.add(OBJ_81);
            OBJ_81.setBounds(30, 274, 110, 20);

            //---- OBJ_70 ----
            OBJ_70.setText("Magasin r\u00e9cepteur");
            OBJ_70.setName("OBJ_70");
            panel1.add(OBJ_70);
            OBJ_70.setBounds(30, 169, 115, 20);

            //---- E20NAT ----
            E20NAT.setComponentPopupMenu(BTD);
            E20NAT.setName("E20NAT");
            panel1.add(E20NAT);
            E20NAT.setBounds(365, 270, 60, E20NAT.getPreferredSize().height);

            //---- E1MAG1 ----
            E1MAG1.setComponentPopupMenu(BTD);
            E1MAG1.setName("E1MAG1");
            panel1.add(E1MAG1);
            E1MAG1.setBounds(145, 70, 34, E1MAG1.getPreferredSize().height);

            //---- E1MAG2 ----
            E1MAG2.setComponentPopupMenu(BTD);
            E1MAG2.setName("E1MAG2");
            panel1.add(E1MAG2);
            E1MAG2.setBounds(145, 165, 34, E1MAG2.getPreferredSize().height);

            //---- EMTTI1 ----
            EMTTI1.setComponentPopupMenu(BTD);
            EMTTI1.setName("EMTTI1");
            panel1.add(EMTTI1);
            EMTTI1.setBounds(365, 105, 58, EMTTI1.getPreferredSize().height);

            //---- TRSTI1 ----
            TRSTI1.setComponentPopupMenu(BTD);
            TRSTI1.setName("TRSTI1");
            panel1.add(TRSTI1);
            TRSTI1.setBounds(365, 200, 58, TRSTI1.getPreferredSize().height);

            //---- OBJ_49 ----
            OBJ_49.setText("Nature");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(315, 274, 50, 20);

            //---- SECTE ----
            SECTE.setName("SECTE");
            panel1.add(SECTE);
            SECTE.setBounds(145, 105, 50, SECTE.getPreferredSize().height);

            //---- ACTE ----
            ACTE.setComponentPopupMenu(BTD);
            ACTE.setName("ACTE");
            panel1.add(ACTE);
            ACTE.setBounds(225, 105, 50, ACTE.getPreferredSize().height);

            //---- SAN ----
            SAN.setComponentPopupMenu(BTD);
            SAN.setName("SAN");
            panel1.add(SAN);
            SAN.setBounds(145, 270, 50, SAN.getPreferredSize().height);

            //---- ACT ----
            ACT.setComponentPopupMenu(BTD);
            ACT.setName("ACT");
            panel1.add(ACT);
            ACT.setBounds(225, 270, 50, ACT.getPreferredSize().height);

            //---- OBJ_63 ----
            OBJ_63.setText("Tiers");
            OBJ_63.setName("OBJ_63");
            panel1.add(OBJ_63);
            OBJ_63.setBounds(320, 109, 45, 20);

            //---- OBJ_73 ----
            OBJ_73.setText("Tiers");
            OBJ_73.setName("OBJ_73");
            panel1.add(OBJ_73);
            OBJ_73.setBounds(315, 204, 45, 20);

            //---- EMTTI2 ----
            EMTTI2.setComponentPopupMenu(BTD);
            EMTTI2.setName("EMTTI2");
            panel1.add(EMTTI2);
            EMTTI2.setBounds(425, 105, 34, EMTTI2.getPreferredSize().height);

            //---- TRSTI2 ----
            TRSTI2.setComponentPopupMenu(BTD);
            TRSTI2.setName("TRSTI2");
            panel1.add(TRSTI2);
            TRSTI2.setBounds(425, 200, 34, TRSTI2.getPreferredSize().height);

            //---- OBJ_68 ----
            OBJ_68.setText("/");
            OBJ_68.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_68.setName("OBJ_68");
            panel1.add(OBJ_68);
            OBJ_68.setBounds(200, 110, 20, 20);

            //---- OBJ_83 ----
            OBJ_83.setText("/");
            OBJ_83.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_83.setName("OBJ_83");
            panel1.add(OBJ_83);
            OBJ_83.setBounds(200, 274, 20, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 645, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(57, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_43;
  private JLabel OBJ_41;
  private JLabel OBJ_35;
  private RiZoneSortie NUM;
  private RiZoneSortie E1ETB;
  private XRiTextField E1UTI;
  private JLabel OBJ_34;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie OBJ_17;
  private JLabel OBJ_19;
  private XRiCalendrier WDATX;
  private JLabel OBJ_52;
  private JLabel OBJ_47;
  private JLabel OBJ_53;
  private JLabel OBJ_48;
  private JLabel OBJ_54;
  private RiZoneSortie E1OPE;
  private JLabel OBJ_57;
  private JXTitledSeparator OBJ_79;
  private RiZoneSortie OBJ_62;
  private RiZoneSortie OBJ_72;
  private JLabel OBJ_85;
  private JLabel OBJ_66;
  private JLabel OBJ_81;
  private JLabel OBJ_70;
  private XRiTextField E20NAT;
  private XRiTextField E1MAG1;
  private XRiTextField E1MAG2;
  private XRiTextField EMTTI1;
  private XRiTextField TRSTI1;
  private JLabel OBJ_49;
  private XRiTextField SECTE;
  private XRiTextField ACTE;
  private XRiTextField SAN;
  private XRiTextField ACT;
  private JLabel OBJ_63;
  private JLabel OBJ_73;
  private XRiTextField EMTTI2;
  private XRiTextField TRSTI2;
  private JLabel OBJ_68;
  private JLabel OBJ_83;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
