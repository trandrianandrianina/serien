
package ri.serien.libecranrpg.vgvx.VGVX20FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX20FM_S3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX20FM_S3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    NUM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM1@")).trim());
    NUM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM2@")).trim());
    NUM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM3@")).trim());
    NUM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM4@")).trim());
    NUM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM5@")).trim());
    NUM6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM6@")).trim());
    NUM7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM7@")).trim());
    NUM8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM8@")).trim());
    NUM9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM9@")).trim());
    NUM10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM10@")).trim());
    ENS11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENS11@")).trim());
    ENS21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENS21@")).trim());
    ENS31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENS31@")).trim());
    ENS41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENS41@")).trim());
    ENS51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENS51@")).trim());
    ENS61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENS61@")).trim());
    ENS71.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENS71@")).trim());
    ENS81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENS81@")).trim());
    ENS91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENS91@")).trim());
    ENS101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENS101@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    A1ARTR.setVisible(!lexique.isTrue("96"));
    A1ART.setVisible(lexique.isTrue("96"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Numéros de série"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    panel3 = new JPanel();
    OBJ_17 = new JLabel();
    A1ART = new XRiTextField();
    A1ARTR = new XRiTextField();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    NUM1 = new JLabel();
    NUM2 = new JLabel();
    NUM3 = new JLabel();
    NUM4 = new JLabel();
    NUM5 = new JLabel();
    NUM6 = new JLabel();
    NUM7 = new JLabel();
    NUM8 = new JLabel();
    NUM9 = new JLabel();
    NUM10 = new JLabel();
    ENS12 = new XRiTextField();
    ENS22 = new XRiTextField();
    ENS32 = new XRiTextField();
    ENS42 = new XRiTextField();
    ENS52 = new XRiTextField();
    ENS62 = new XRiTextField();
    ENS72 = new XRiTextField();
    ENS82 = new XRiTextField();
    ENS92 = new XRiTextField();
    ENS102 = new XRiTextField();
    ENS11 = new RiZoneSortie();
    ENS21 = new RiZoneSortie();
    ENS31 = new RiZoneSortie();
    ENS41 = new RiZoneSortie();
    ENS51 = new RiZoneSortie();
    ENS61 = new RiZoneSortie();
    ENS71 = new RiZoneSortie();
    ENS81 = new RiZoneSortie();
    ENS91 = new RiZoneSortie();
    ENS101 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(800, 400));
    setPreferredSize(new Dimension(800, 400));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Saisie des \u00e9l\u00e9ments");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setForeground(Color.black);
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //======== panel3 ========
          {
            panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_17 ----
            OBJ_17.setText("Saisie de num\u00e9ros de s\u00e9rie pour l'article");
            OBJ_17.setName("OBJ_17");
            panel3.add(OBJ_17);
            OBJ_17.setBounds(15, 14, 235, 20);

            //---- A1ART ----
            A1ART.setName("A1ART");
            panel3.add(A1ART);
            A1ART.setBounds(315, 10, 210, A1ART.getPreferredSize().height);

            //---- A1ARTR ----
            A1ARTR.setName("A1ARTR");
            panel3.add(A1ARTR);
            A1ARTR.setBounds(415, 10, 110, A1ARTR.getPreferredSize().height);
          }
          panel2.add(panel3);
          panel3.setBounds(20, 20, 565, 45);

          //---- BT_PGUP ----
          BT_PGUP.setToolTipText("?S6.BT_PGUP.toolTipText_2?");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel2.add(BT_PGUP);
          BT_PGUP.setBounds(560, 90, 25, 115);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setToolTipText("?S6.BT_PGDOWN.toolTipText_2?");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel2.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(560, 230, 25, 115);

          //---- NUM1 ----
          NUM1.setText("@NUM1@");
          NUM1.setName("NUM1");
          panel2.add(NUM1);
          NUM1.setBounds(20, 92, 40, 25);

          //---- NUM2 ----
          NUM2.setText("@NUM2@");
          NUM2.setName("NUM2");
          panel2.add(NUM2);
          NUM2.setBounds(20, 117, 40, 25);

          //---- NUM3 ----
          NUM3.setText("@NUM3@");
          NUM3.setName("NUM3");
          panel2.add(NUM3);
          NUM3.setBounds(20, 142, 40, 25);

          //---- NUM4 ----
          NUM4.setText("@NUM4@");
          NUM4.setName("NUM4");
          panel2.add(NUM4);
          NUM4.setBounds(20, 167, 40, 25);

          //---- NUM5 ----
          NUM5.setText("@NUM5@");
          NUM5.setName("NUM5");
          panel2.add(NUM5);
          NUM5.setBounds(20, 192, 40, 25);

          //---- NUM6 ----
          NUM6.setText("@NUM6@");
          NUM6.setName("NUM6");
          panel2.add(NUM6);
          NUM6.setBounds(20, 217, 40, 25);

          //---- NUM7 ----
          NUM7.setText("@NUM7@");
          NUM7.setName("NUM7");
          panel2.add(NUM7);
          NUM7.setBounds(20, 242, 40, 25);

          //---- NUM8 ----
          NUM8.setText("@NUM8@");
          NUM8.setName("NUM8");
          panel2.add(NUM8);
          NUM8.setBounds(20, 267, 40, 25);

          //---- NUM9 ----
          NUM9.setText("@NUM9@");
          NUM9.setName("NUM9");
          panel2.add(NUM9);
          NUM9.setBounds(20, 292, 40, 25);

          //---- NUM10 ----
          NUM10.setText("@NUM10@");
          NUM10.setName("NUM10");
          panel2.add(NUM10);
          NUM10.setBounds(20, 317, 40, 25);

          //---- ENS12 ----
          ENS12.setName("ENS12");
          panel2.add(ENS12);
          ENS12.setBounds(170, 90, 380, ENS12.getPreferredSize().height);

          //---- ENS22 ----
          ENS22.setName("ENS22");
          panel2.add(ENS22);
          ENS22.setBounds(170, 115, 380, ENS22.getPreferredSize().height);

          //---- ENS32 ----
          ENS32.setName("ENS32");
          panel2.add(ENS32);
          ENS32.setBounds(170, 140, 380, ENS32.getPreferredSize().height);

          //---- ENS42 ----
          ENS42.setName("ENS42");
          panel2.add(ENS42);
          ENS42.setBounds(170, 165, 380, ENS42.getPreferredSize().height);

          //---- ENS52 ----
          ENS52.setName("ENS52");
          panel2.add(ENS52);
          ENS52.setBounds(170, 190, 380, ENS52.getPreferredSize().height);

          //---- ENS62 ----
          ENS62.setName("ENS62");
          panel2.add(ENS62);
          ENS62.setBounds(170, 215, 380, ENS62.getPreferredSize().height);

          //---- ENS72 ----
          ENS72.setName("ENS72");
          panel2.add(ENS72);
          ENS72.setBounds(170, 240, 380, ENS72.getPreferredSize().height);

          //---- ENS82 ----
          ENS82.setName("ENS82");
          panel2.add(ENS82);
          ENS82.setBounds(170, 265, 380, ENS82.getPreferredSize().height);

          //---- ENS92 ----
          ENS92.setName("ENS92");
          panel2.add(ENS92);
          ENS92.setBounds(170, 290, 380, ENS92.getPreferredSize().height);

          //---- ENS102 ----
          ENS102.setName("ENS102");
          panel2.add(ENS102);
          ENS102.setBounds(170, 315, 380, ENS102.getPreferredSize().height);

          //---- ENS11 ----
          ENS11.setText("@ENS11@");
          ENS11.setName("ENS11");
          panel2.add(ENS11);
          ENS11.setBounds(new Rectangle(new Point(65, 92), ENS11.getPreferredSize()));

          //---- ENS21 ----
          ENS21.setText("@ENS21@");
          ENS21.setName("ENS21");
          panel2.add(ENS21);
          ENS21.setBounds(new Rectangle(new Point(65, 117), ENS21.getPreferredSize()));

          //---- ENS31 ----
          ENS31.setText("@ENS31@");
          ENS31.setName("ENS31");
          panel2.add(ENS31);
          ENS31.setBounds(new Rectangle(new Point(65, 142), ENS31.getPreferredSize()));

          //---- ENS41 ----
          ENS41.setText("@ENS41@");
          ENS41.setName("ENS41");
          panel2.add(ENS41);
          ENS41.setBounds(new Rectangle(new Point(65, 167), ENS41.getPreferredSize()));

          //---- ENS51 ----
          ENS51.setText("@ENS51@");
          ENS51.setName("ENS51");
          panel2.add(ENS51);
          ENS51.setBounds(new Rectangle(new Point(65, 192), ENS51.getPreferredSize()));

          //---- ENS61 ----
          ENS61.setText("@ENS61@");
          ENS61.setName("ENS61");
          panel2.add(ENS61);
          ENS61.setBounds(new Rectangle(new Point(65, 217), ENS61.getPreferredSize()));

          //---- ENS71 ----
          ENS71.setText("@ENS71@");
          ENS71.setName("ENS71");
          panel2.add(ENS71);
          ENS71.setBounds(new Rectangle(new Point(65, 242), ENS71.getPreferredSize()));

          //---- ENS81 ----
          ENS81.setText("@ENS81@");
          ENS81.setName("ENS81");
          panel2.add(ENS81);
          ENS81.setBounds(new Rectangle(new Point(65, 267), ENS81.getPreferredSize()));

          //---- ENS91 ----
          ENS91.setText("@ENS91@");
          ENS91.setName("ENS91");
          panel2.add(ENS91);
          ENS91.setBounds(new Rectangle(new Point(65, 292), ENS91.getPreferredSize()));

          //---- ENS101 ----
          ENS101.setText("@ENS101@");
          ENS101.setName("ENS101");
          panel2.add(ENS101);
          ENS101.setBounds(new Rectangle(new Point(65, 317), ENS101.getPreferredSize()));

          //---- label1 ----
          label1.setText("Marques");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");
          panel2.add(label1);
          label1.setBounds(65, 70, 100, 25);

          //---- label2 ----
          label2.setText("Num\u00e9ros de s\u00e9rie");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel2.add(label2);
          label2.setBounds(170, 70, 380, 25);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 598, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel2;
  private JPanel panel3;
  private JLabel OBJ_17;
  private XRiTextField A1ART;
  private XRiTextField A1ARTR;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel NUM1;
  private JLabel NUM2;
  private JLabel NUM3;
  private JLabel NUM4;
  private JLabel NUM5;
  private JLabel NUM6;
  private JLabel NUM7;
  private JLabel NUM8;
  private JLabel NUM9;
  private JLabel NUM10;
  private XRiTextField ENS12;
  private XRiTextField ENS22;
  private XRiTextField ENS32;
  private XRiTextField ENS42;
  private XRiTextField ENS52;
  private XRiTextField ENS62;
  private XRiTextField ENS72;
  private XRiTextField ENS82;
  private XRiTextField ENS92;
  private XRiTextField ENS102;
  private RiZoneSortie ENS11;
  private RiZoneSortie ENS21;
  private RiZoneSortie ENS31;
  private RiZoneSortie ENS41;
  private RiZoneSortie ENS51;
  private RiZoneSortie ENS61;
  private RiZoneSortie ENS71;
  private RiZoneSortie ENS81;
  private RiZoneSortie ENS91;
  private RiZoneSortie ENS101;
  private JLabel label1;
  private JLabel label2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
