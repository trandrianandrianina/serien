
package ri.serien.libecranrpg.vgvx.VGVX36FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class VGVX36FM_01 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX36FM_01(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider_oui);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WART.setValeursSelection("X", " ");
    WCNA.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB@")).trim());
    OBJ_13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUN@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_8.setVisible(lexique.isTrue("11"));
    WART.setVisible(lexique.isTrue("11"));
    // WART.setSelected(lexique.HostFieldGetData("WART").equalsIgnoreCase("X"));
    WCNA.setVisible(lexique.isTrue("11"));
    // WCNA.setSelected(lexique.HostFieldGetData("WCNA").equalsIgnoreCase("X"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Confirmation des modifications"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WART.isSelected())
    // lexique.HostFieldPutData("WART", 0, "X");
    // else
    // lexique.HostFieldPutData("WART", 0, " ");
    // if (WCNA.isSelected())
    // lexique.HostFieldPutData("WCNA", 0, "X");
    // else
    // lexique.HostFieldPutData("WCNA", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WREP", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_valider2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WREP", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider_oui = new RiMenu_bt();
    navig_valid2 = new RiMenu();
    bouton_valider_non = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    OBJ_8 = new JLabel();
    WCNA = new XRiCheckBox();
    OBJ_14 = new RiZoneSortie();
    OBJ_12 = new JLabel();
    WART = new XRiCheckBox();
    OBJ_13 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    separator2 = compFactory.createSeparator("Modification du nombre de d\u00e9cimales");
    OBJ_11 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 290));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider_oui ----
            bouton_valider_oui.setText("OUI");
            bouton_valider_oui.setToolTipText("Valider");
            bouton_valider_oui.setMargin(new Insets(0, 0, 0, 0));
            bouton_valider_oui.setName("bouton_valider_oui");
            bouton_valider_oui.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider_oui);
          }
          menus_bas.add(navig_valid);

          //======== navig_valid2 ========
          {
            navig_valid2.setName("navig_valid2");

            //---- bouton_valider_non ----
            bouton_valider_non.setText("NON");
            bouton_valider_non.setToolTipText("Valider");
            bouton_valider_non.setMargin(new Insets(0, 0, 0, 0));
            bouton_valider_non.setName("bouton_valider_non");
            bouton_valider_non.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_valider2ActionPerformed(e);
              }
            });
            navig_valid2.add(bouton_valider_non);
          }
          menus_bas.add(navig_valid2);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- OBJ_8 ----
        OBJ_8.setText("utilisations futures");
        OBJ_8.setName("OBJ_8");
        p_contenu.add(OBJ_8);
        OBJ_8.setBounds(45, 210, 205, 20);

        //---- WCNA ----
        WCNA.setText("Condition d'achat");
        WCNA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        WCNA.setName("WCNA");
        p_contenu.add(WCNA);
        WCNA.setBounds(255, 240, 129, 20);

        //---- OBJ_14 ----
        OBJ_14.setText("@WLIB@");
        OBJ_14.setName("OBJ_14");
        p_contenu.add(OBJ_14);
        OBJ_14.setBounds(255, 80, 205, OBJ_14.getPreferredSize().height);

        //---- OBJ_12 ----
        OBJ_12.setText("de l'unit\u00e9");
        OBJ_12.setName("OBJ_12");
        p_contenu.add(OBJ_12);
        OBJ_12.setBounds(120, 82, 75, 20);

        //---- WART ----
        WART.setText("Article");
        WART.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        WART.setName("WART");
        p_contenu.add(WART);
        WART.setBounds(255, 210, 63, 20);

        //---- OBJ_13 ----
        OBJ_13.setText("@WUN@");
        OBJ_13.setName("OBJ_13");
        p_contenu.add(OBJ_13);
        OBJ_13.setBounds(215, 80, 32, OBJ_13.getPreferredSize().height);

        //---- label1 ----
        label1.setText("Si vous avez des articles ou des conditions d'achat qui ont cette unit\u00e9,");
        label1.setHorizontalAlignment(SwingConstants.CENTER);
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 3f));
        label1.setName("label1");
        p_contenu.add(label1);
        label1.setBounds(20, 125, 555, 25);

        //---- label2 ----
        label2.setText("voulez-vous mettre \u00e0 jour vos fichiers pour les utilisations futures ?");
        label2.setHorizontalAlignment(SwingConstants.CENTER);
        label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() & ~Font.BOLD, label2.getFont().getSize() + 3f));
        label2.setName("label2");
        p_contenu.add(label2);
        label2.setBounds(20, 155, 555, 25);

        //---- separator2 ----
        separator2.setName("separator2");
        p_contenu.add(separator2);
        separator2.setBounds(15, 15, 590, separator2.getPreferredSize().height);

        //---- OBJ_11 ----
        OBJ_11.setText("Vous venez de modifier le nombre de d\u00e9cimales");
        OBJ_11.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_11.setName("OBJ_11");
        p_contenu.add(OBJ_11);
        OBJ_11.setBounds(20, 45, 555, 20);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD.add(OBJ_6);

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider_oui;
  private RiMenu navig_valid2;
  private RiMenu_bt bouton_valider_non;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JLabel OBJ_8;
  private XRiCheckBox WCNA;
  private RiZoneSortie OBJ_14;
  private JLabel OBJ_12;
  private XRiCheckBox WART;
  private RiZoneSortie OBJ_13;
  private JLabel label1;
  private JLabel label2;
  private JComponent separator2;
  private JLabel OBJ_11;
  private JPopupMenu BTD;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
