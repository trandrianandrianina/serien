
package ri.serien.libecranrpg.vgvx.VGVXEQ5F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXEQ5F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXEQ5F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    WETB.setVisible(lexique.isPresent("WETB"));
    OBJ_49.setVisible(lexique.isPresent("DGNOM"));
    
    // TODO Icones
    BT_ChgSoc.setIcon(lexique.chargerImage("images/changer.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WETB");
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_48 = new JXTitledSeparator();
    WETB = new XRiTextField();
    BT_ChgSoc = new SNBoutonRecherche();
    OBJ_49 = new RiZoneSortie();
    OBJ_51 = new JXTitledSeparator();
    OBJ_55 = new JLabel();
    ZP01A = new XRiTextField();
    OBJ_56 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_59 = new JLabel();
    ZP02A = new XRiTextField();
    ZP03A = new XRiTextField();
    ZP04A = new XRiTextField();
    ZP01C = new XRiTextField();
    ZP02C = new XRiTextField();
    ZP03C = new XRiTextField();
    ZP04C = new XRiTextField();
    ZP01F = new XRiTextField();
    ZP02F = new XRiTextField();
    ZP03F = new XRiTextField();
    ZP04F = new XRiTextField();
    ZP01P = new XRiTextField();
    ZP02P = new XRiTextField();
    ZP03P = new XRiTextField();
    ZP04P = new XRiTextField();
    OBJ_60 = new JLabel();
    ZP05A = new XRiTextField();
    ZP05C = new XRiTextField();
    ZP05F = new XRiTextField();
    ZP05P = new XRiTextField();
    OBJ_61 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_64 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Initialisation");
              riSousMenu_bt6.setToolTipText("Initialisation depuis le catalogue standard");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(720, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- OBJ_48 ----
            OBJ_48.setTitle("Etablissement s\u00e9lectionn\u00e9");
            OBJ_48.setName("OBJ_48");

            //---- WETB ----
            WETB.setComponentPopupMenu(BTD);
            WETB.setName("WETB");

            //---- BT_ChgSoc ----
            BT_ChgSoc.setText("");
            BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
            BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_ChgSoc.setName("BT_ChgSoc");
            BT_ChgSoc.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_ChgSocActionPerformed(e);
                BT_ChgSocActionPerformed(e);
              }
            });

            //---- OBJ_49 ----
            OBJ_49.setText("@DGNOM@");
            OBJ_49.setName("OBJ_49");

            //---- OBJ_51 ----
            OBJ_51.setTitle("Choix des ZP longues \u00e0 extraire par type pour les \u00e9tiquettes");
            OBJ_51.setName("OBJ_51");

            //---- OBJ_55 ----
            OBJ_55.setText("Type");
            OBJ_55.setName("OBJ_55");

            //---- ZP01A ----
            ZP01A.setComponentPopupMenu(BTD);
            ZP01A.setName("ZP01A");

            //---- OBJ_56 ----
            OBJ_56.setText("Article");
            OBJ_56.setName("OBJ_56");

            //---- OBJ_57 ----
            OBJ_57.setText("Client");
            OBJ_57.setName("OBJ_57");

            //---- OBJ_58 ----
            OBJ_58.setText("Fournisseur");
            OBJ_58.setName("OBJ_58");

            //---- OBJ_59 ----
            OBJ_59.setText("Prospect");
            OBJ_59.setName("OBJ_59");

            //---- ZP02A ----
            ZP02A.setComponentPopupMenu(BTD);
            ZP02A.setName("ZP02A");

            //---- ZP03A ----
            ZP03A.setComponentPopupMenu(BTD);
            ZP03A.setName("ZP03A");

            //---- ZP04A ----
            ZP04A.setComponentPopupMenu(BTD);
            ZP04A.setName("ZP04A");

            //---- ZP01C ----
            ZP01C.setComponentPopupMenu(BTD);
            ZP01C.setName("ZP01C");

            //---- ZP02C ----
            ZP02C.setComponentPopupMenu(BTD);
            ZP02C.setName("ZP02C");

            //---- ZP03C ----
            ZP03C.setComponentPopupMenu(BTD);
            ZP03C.setName("ZP03C");

            //---- ZP04C ----
            ZP04C.setComponentPopupMenu(BTD);
            ZP04C.setName("ZP04C");

            //---- ZP01F ----
            ZP01F.setComponentPopupMenu(BTD);
            ZP01F.setName("ZP01F");

            //---- ZP02F ----
            ZP02F.setComponentPopupMenu(BTD);
            ZP02F.setName("ZP02F");

            //---- ZP03F ----
            ZP03F.setComponentPopupMenu(BTD);
            ZP03F.setName("ZP03F");

            //---- ZP04F ----
            ZP04F.setComponentPopupMenu(BTD);
            ZP04F.setName("ZP04F");

            //---- ZP01P ----
            ZP01P.setComponentPopupMenu(BTD);
            ZP01P.setName("ZP01P");

            //---- ZP02P ----
            ZP02P.setComponentPopupMenu(BTD);
            ZP02P.setName("ZP02P");

            //---- ZP03P ----
            ZP03P.setComponentPopupMenu(BTD);
            ZP03P.setName("ZP03P");

            //---- ZP04P ----
            ZP04P.setComponentPopupMenu(BTD);
            ZP04P.setName("ZP04P");

            //---- OBJ_60 ----
            OBJ_60.setText("ZP01");
            OBJ_60.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_60.setFont(OBJ_60.getFont().deriveFont(OBJ_60.getFont().getStyle() | Font.BOLD));
            OBJ_60.setName("OBJ_60");

            //---- ZP05A ----
            ZP05A.setComponentPopupMenu(BTD);
            ZP05A.setName("ZP05A");

            //---- ZP05C ----
            ZP05C.setComponentPopupMenu(BTD);
            ZP05C.setName("ZP05C");

            //---- ZP05F ----
            ZP05F.setComponentPopupMenu(BTD);
            ZP05F.setName("ZP05F");

            //---- ZP05P ----
            ZP05P.setComponentPopupMenu(BTD);
            ZP05P.setName("ZP05P");

            //---- OBJ_61 ----
            OBJ_61.setText("ZP02");
            OBJ_61.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_61.setFont(OBJ_61.getFont().deriveFont(OBJ_61.getFont().getStyle() | Font.BOLD));
            OBJ_61.setName("OBJ_61");

            //---- OBJ_62 ----
            OBJ_62.setText("ZP03");
            OBJ_62.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_62.setFont(OBJ_62.getFont().deriveFont(OBJ_62.getFont().getStyle() | Font.BOLD));
            OBJ_62.setName("OBJ_62");

            //---- OBJ_63 ----
            OBJ_63.setText("ZP04");
            OBJ_63.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_63.setFont(OBJ_63.getFont().deriveFont(OBJ_63.getFont().getStyle() | Font.BOLD));
            OBJ_63.setName("OBJ_63");

            //---- OBJ_64 ----
            OBJ_64.setText("ZP05");
            OBJ_64.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_64.setFont(OBJ_64.getFont().deriveFont(OBJ_64.getFont().getStyle() | Font.BOLD));
            OBJ_64.setName("OBJ_64");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(90, 90, 90)
                  .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                  .addGap(48, 48, 48)
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 426, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(90, 90, 90)
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                  .addGap(37, 37, 37)
                  .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(90, 90, 90)
                  .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                  .addGap(45, 45, 45)
                  .addComponent(ZP01A, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP02A, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP03A, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP04A, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP05A, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(90, 90, 90)
                  .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                  .addGap(45, 45, 45)
                  .addComponent(ZP01C, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP02C, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP03C, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP04C, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP05C, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(90, 90, 90)
                  .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                  .addGap(45, 45, 45)
                  .addComponent(ZP01F, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP02F, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP03F, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP04F, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP05F, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(90, 90, 90)
                  .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                  .addGap(45, 45, 45)
                  .addComponent(ZP01P, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP02P, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP03P, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP04P, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(32, 32, 32)
                  .addComponent(ZP05P, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(21, 21, 21)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)))
                  .addGap(36, 36, 36)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(21, 21, 21)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(10, 10, 10)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(ZP01A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(ZP02A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(ZP03A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(ZP04A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ZP05A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(10, 10, 10)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ZP01C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZP02C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZP03C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZP04C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(ZP05C, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(7, 7, 7)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(ZP01F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(ZP02F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(ZP03F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(ZP04F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ZP05F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(10, 10, 10)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(ZP01P, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZP02P, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZP03P, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZP04P, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(ZP05P, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledSeparator OBJ_48;
  private XRiTextField WETB;
  private SNBoutonRecherche BT_ChgSoc;
  private RiZoneSortie OBJ_49;
  private JXTitledSeparator OBJ_51;
  private JLabel OBJ_55;
  private XRiTextField ZP01A;
  private JLabel OBJ_56;
  private JLabel OBJ_57;
  private JLabel OBJ_58;
  private JLabel OBJ_59;
  private XRiTextField ZP02A;
  private XRiTextField ZP03A;
  private XRiTextField ZP04A;
  private XRiTextField ZP01C;
  private XRiTextField ZP02C;
  private XRiTextField ZP03C;
  private XRiTextField ZP04C;
  private XRiTextField ZP01F;
  private XRiTextField ZP02F;
  private XRiTextField ZP03F;
  private XRiTextField ZP04F;
  private XRiTextField ZP01P;
  private XRiTextField ZP02P;
  private XRiTextField ZP03P;
  private XRiTextField ZP04P;
  private JLabel OBJ_60;
  private XRiTextField ZP05A;
  private XRiTextField ZP05C;
  private XRiTextField ZP05F;
  private XRiTextField ZP05P;
  private JLabel OBJ_61;
  private JLabel OBJ_62;
  private JLabel OBJ_63;
  private JLabel OBJ_64;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
