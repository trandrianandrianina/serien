
package ri.serien.libecranrpg.vgvx.VGVX21FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class CONF_PANEL extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  
  public CONF_PANEL(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
    OBJ_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V05F@")).trim());
    OBJ_14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    ERR.setText(lexique.TranslationTable("Erreur(s) : " + interpreteurD.analyseExpression("@V03F@")).trim());
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    if (lexique.HostFieldGetData("V03F").trim().equals("")) {
      p_DemandeConf.setVisible(true);
      p_Erreur.setVisible(false);
    }
    else {
      p_Erreur.setVisible(true);
      p_DemandeConf.setVisible(false);
    }
    // TODO Icones
    // RETOUR.setIcon(lexique.getImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Options avancées"));
  }
  
  public void getData() {
  }
  
  public void tuer() {
    dispose();
  }
  
  public void reveiller() {
    mettreAJourVariableLibelle();
    setVisible(true);
  }
  
  private void OKActionPerformed() {
    lexique.HostFieldPutData("V06F", 0, "OUI");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    setVisible(false);
    tuer();
  }
  
  private void ANNActionPerformed() {
    lexique.HostFieldPutData("V06F", 0, "NON");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    setVisible(false);
    tuer();
  }
  
  private void ExitActionPerformed() {
    lexique.HostScreenSendKey(lexique.getPanel(), "F3");
    setVisible(false);
    tuer();
  }
  
  private void ERRActionPerformed() {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    p_DemandeConf = new JPanel();
    OBJ_5 = new JLabel();
    OBJ_14 = new JLabel();
    OK = new JButton();
    ANN = new JButton();
    p_Erreur = new JPanel();
    OK2 = new JButton();
    ANN2 = new JButton();
    ERR = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(340, 200));
    setAlwaysOnTop(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(90, 90, 90));
      P_Centre.setPreferredSize(new Dimension(340, 200));
      P_Centre.setMinimumSize(new Dimension(350, 415));
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);
      
      // ======== p_DemandeConf ========
      {
        p_DemandeConf.setOpaque(false);
        p_DemandeConf.setPreferredSize(new Dimension(340, 140));
        p_DemandeConf.setBackground(new Color(90, 90, 90));
        p_DemandeConf.setName("p_DemandeConf");
        p_DemandeConf.setLayout(null);
        
        // ---- OBJ_5 ----
        OBJ_5.setText("@V05F@");
        OBJ_5.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_5.setRequestFocusEnabled(false);
        OBJ_5.setFont(new Font("sansserif", Font.PLAIN, 16));
        OBJ_5.setForeground(Color.white);
        OBJ_5.setName("OBJ_5");
        p_DemandeConf.add(OBJ_5);
        OBJ_5.setBounds(0, 30, 340, 25);
        
        // ---- OBJ_14 ----
        OBJ_14.setText("@V03F@");
        OBJ_14.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_14.setRequestFocusEnabled(false);
        OBJ_14.setForeground(Color.white);
        OBJ_14.setFont(new Font("sansserif", Font.PLAIN, 15));
        OBJ_14.setName("OBJ_14");
        p_DemandeConf.add(OBJ_14);
        OBJ_14.setBounds(0, 5, 340, 25);
        
        // ---- OK ----
        OK.setText("Valider");
        OK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OK.setMargin(new Insets(0, -15, 0, 0));
        OK.setPreferredSize(new Dimension(110, 19));
        OK.setMinimumSize(new Dimension(110, 1));
        OK.setFont(OK.getFont().deriveFont(OK.getFont().getStyle() | Font.BOLD, OK.getFont().getSize() + 2f));
        OK.setIconTextGap(25);
        OK.setName("OK");
        OK.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OKActionPerformed();
          }
        });
        p_DemandeConf.add(OK);
        OK.setBounds(90, 60, 150, 40);
        
        // ---- ANN ----
        ANN.setText(" Retour");
        ANN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ANN.setMargin(new Insets(0, -15, 0, 0));
        ANN.setPreferredSize(new Dimension(120, 19));
        ANN.setMinimumSize(new Dimension(120, 1));
        ANN.setFont(new Font("sansserif", Font.BOLD, 14));
        ANN.setIconTextGap(25);
        ANN.setName("ANN");
        ANN.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            ANNActionPerformed();
          }
        });
        p_DemandeConf.add(ANN);
        ANN.setBounds(90, 100, 150, 40);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_DemandeConf.getComponentCount(); i++) {
            Rectangle bounds = p_DemandeConf.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_DemandeConf.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_DemandeConf.setMinimumSize(preferredSize);
          p_DemandeConf.setPreferredSize(preferredSize);
        }
      }
      P_Centre.add(p_DemandeConf);
      p_DemandeConf.setBounds(0, 0, p_DemandeConf.getPreferredSize().width, 145);
      
      // ======== p_Erreur ========
      {
        p_Erreur.setPreferredSize(new Dimension(340, 140));
        p_Erreur.setBackground(new Color(106, 23, 21));
        p_Erreur.setName("p_Erreur");
        p_Erreur.setLayout(null);
        
        // ---- OK2 ----
        OK2.setText("Valider");
        OK2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OK2.setPreferredSize(new Dimension(110, 19));
        OK2.setMinimumSize(new Dimension(110, 1));
        OK2.setFont(OK2.getFont().deriveFont(OK2.getFont().getStyle() | Font.BOLD, OK2.getFont().getSize() + 2f));
        OK2.setIconTextGap(5);
        OK2.setIcon(null);
        OK2.setName("OK2");
        OK2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OKActionPerformed();
          }
        });
        p_Erreur.add(OK2);
        OK2.setBounds(70, 60, 205, 40);
        
        // ---- ANN2 ----
        ANN2.setText("Corriger");
        ANN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ANN2.setPreferredSize(new Dimension(120, 19));
        ANN2.setMinimumSize(new Dimension(120, 1));
        ANN2.setFont(new Font("sansserif", Font.BOLD, 14));
        ANN2.setIconTextGap(5);
        ANN2.setName("ANN2");
        ANN2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            ANNActionPerformed();
          }
        });
        p_Erreur.add(ANN2);
        ANN2.setBounds(70, 100, 205, 40);
        
        // ---- ERR ----
        ERR.setText("Erreur(s) : @V03F@");
        ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ERR.setPreferredSize(new Dimension(120, 19));
        ERR.setMinimumSize(new Dimension(120, 1));
        ERR.setFont(new Font("sansserif", Font.BOLD, 14));
        ERR.setIconTextGap(0);
        ERR.setToolTipText("Cliquez pour le d\u00e9tail des erreurs");
        ERR.setBackground(new Color(106, 23, 21));
        ERR.setForeground(Color.white);
        ERR.setName("ERR");
        ERR.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            ERRActionPerformed();
          }
        });
        p_Erreur.add(ERR);
        ERR.setBounds(0, 5, 340, 40);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_Erreur.getComponentCount(); i++) {
            Rectangle bounds = p_Erreur.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_Erreur.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_Erreur.setMinimumSize(preferredSize);
          p_Erreur.setPreferredSize(preferredSize);
        }
      }
      P_Centre.add(p_Erreur);
      p_Erreur.setBounds(0, 0, p_Erreur.getPreferredSize().width, 190);
      
      { // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JPanel p_DemandeConf;
  private JLabel OBJ_5;
  private JLabel OBJ_14;
  private JButton OK;
  private JButton ANN;
  private JPanel p_Erreur;
  private JButton OK2;
  private JButton ANN2;
  private JButton ERR;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
