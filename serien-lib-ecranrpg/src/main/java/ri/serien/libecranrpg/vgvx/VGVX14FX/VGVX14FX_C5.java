
package ri.serien.libecranrpg.vgvx.VGVX14FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX14FX_C5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX14FX_C5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    LSTOP2.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADEV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    WCOD.setVisible(lexique.isPresent("WCOD"));
    A1UNV.setEnabled(lexique.isPresent("A1UNV"));
    CAUNA.setEnabled(lexique.isPresent("CAUNA"));
    OBJ_48.setVisible(lexique.isPresent("CADEV"));
    WNLI.setVisible(lexique.isPresent("WNLI"));
    OBJ_41.setVisible(lexique.isPresent("WARTT"));
    OBJ_32.setVisible(lexique.isPresent("WNLI"));
    LSCO1.setEnabled(lexique.isPresent("LSCO1"));
    // LSDTFX.setEnabled( lexique.isPresent("LSDTFX"));
    // LSDTDX.setEnabled( lexique.isPresent("LSDTDX"));
    LSP02X.setEnabled(lexique.isPresent("LSP02X"));
    LSP01X.setEnabled(lexique.isPresent("LSP01X"));
    PV01A.setEnabled(lexique.isPresent("PV01A"));
    PCALX.setEnabled(lexique.isPresent("PCALX"));
    PRSX.setEnabled(lexique.isPresent("PRSX"));
    // LSTOP2.setEnabled( lexique.isPresent("LSTOP2"));
    // LSTOP2.setSelected(lexique.HostFieldGetData("LSTOP2").equalsIgnoreCase("1"));
    WARTT.setVisible(lexique.isPresent("WARTT"));
    LSOBS.setEnabled(lexique.isPresent("LSOBS"));
    OBJ_47.setVisible(lexique.isPresent("A1LIB"));
    OBJ_15.setVisible(lexique.isPresent("A1LIB"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (LSTOP2.isSelected())
    // lexique.HostFieldPutData("LSTOP2", 0, "1");
    // else
    // lexique.HostFieldPutData("LSTOP2", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_15 = new RiZoneSortie();
    OBJ_47 = new JLabel();
    LSOBS = new XRiTextField();
    WARTT = new XRiTextField();
    LSTOP2 = new XRiCheckBox();
    OBJ_30 = new JLabel();
    PRSX = new XRiTextField();
    PCALX = new XRiTextField();
    PV01A = new XRiTextField();
    OBJ_24 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_29 = new JLabel();
    OBJ_18 = new JLabel();
    OBJ_46 = new JLabel();
    LSP01X = new XRiTextField();
    LSP02X = new XRiTextField();
    OBJ_16 = new JLabel();
    LSDTDX = new XRiCalendrier();
    LSDTFX = new XRiCalendrier();
    LSCO1 = new XRiTextField();
    OBJ_32 = new JLabel();
    OBJ_41 = new JLabel();
    WNLI = new XRiTextField();
    OBJ_48 = new JLabel();
    CAUNA = new XRiTextField();
    A1UNV = new XRiTextField();
    WCOD = new XRiTextField();
    OBJ_17 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_42 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(995, 295));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_15 ----
          OBJ_15.setText("@A1LIB@");
          OBJ_15.setName("OBJ_15");
          p_recup.add(OBJ_15);
          OBJ_15.setBounds(308, 47, 440, OBJ_15.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setText("@A1LIB@");
          OBJ_47.setName("OBJ_47");
          p_recup.add(OBJ_47);
          OBJ_47.setBounds(308, 46, 231, 20);

          //---- LSOBS ----
          LSOBS.setComponentPopupMenu(BTD);
          LSOBS.setName("LSOBS");
          p_recup.add(LSOBS);
          LSOBS.setBounds(440, 80, 310, LSOBS.getPreferredSize().height);

          //---- WARTT ----
          WARTT.setComponentPopupMenu(BTD);
          WARTT.setName("WARTT");
          p_recup.add(WARTT);
          WARTT.setBounds(90, 45, 210, WARTT.getPreferredSize().height);

          //---- LSTOP2 ----
          LSTOP2.setText("Pas de mise \u00e0 jour");
          LSTOP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LSTOP2.setName("LSTOP2");
          p_recup.add(LSTOP2);
          LSTOP2.setBounds(280, 174, 200, 20);

          //---- OBJ_30 ----
          OBJ_30.setText("Prix de vente actuel");
          OBJ_30.setName("OBJ_30");
          p_recup.add(OBJ_30);
          OBJ_30.setBounds(510, 174, 125, 20);

          //---- PRSX ----
          PRSX.setComponentPopupMenu(BTD);
          PRSX.setName("PRSX");
          p_recup.add(PRSX);
          PRSX.setBounds(380, 115, 114, PRSX.getPreferredSize().height);

          //---- PCALX ----
          PCALX.setComponentPopupMenu(BTD);
          PCALX.setName("PCALX");
          p_recup.add(PCALX);
          PCALX.setBounds(636, 115, 114, PCALX.getPreferredSize().height);

          //---- PV01A ----
          PV01A.setComponentPopupMenu(BTD);
          PV01A.setName("PV01A");
          p_recup.add(PV01A);
          PV01A.setBounds(636, 170, 114, PV01A.getPreferredSize().height);

          //---- OBJ_24 ----
          OBJ_24.setText("PRV Standard");
          OBJ_24.setName("OBJ_24");
          p_recup.add(OBJ_24);
          OBJ_24.setBounds(280, 119, 100, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_29 ----
          OBJ_29.setText("Prix de vente");
          OBJ_29.setName("OBJ_29");
          p_recup.add(OBJ_29);
          OBJ_29.setBounds(20, 174, 85, 20);

          //---- OBJ_18 ----
          OBJ_18.setText("Prix n\u00e9goci\u00e9");
          OBJ_18.setName("OBJ_18");
          p_recup.add(OBJ_18);
          OBJ_18.setBounds(20, 119, 85, 20);

          //---- OBJ_46 ----
          OBJ_46.setText("Observation");
          OBJ_46.setName("OBJ_46");
          p_recup.add(OBJ_46);
          OBJ_46.setBounds(360, 84, 80, 20);

          //---- LSP01X ----
          LSP01X.setComponentPopupMenu(BTD);
          LSP01X.setName("LSP01X");
          p_recup.add(LSP01X);
          LSP01X.setBounds(105, 115, 114, LSP01X.getPreferredSize().height);

          //---- LSP02X ----
          LSP02X.setComponentPopupMenu(BTD);
          LSP02X.setName("LSP02X");
          p_recup.add(LSP02X);
          LSP02X.setBounds(105, 170, 114, LSP02X.getPreferredSize().height);

          //---- OBJ_16 ----
          OBJ_16.setText("Validit\u00e9 de");
          OBJ_16.setName("OBJ_16");
          p_recup.add(OBJ_16);
          OBJ_16.setBounds(20, 84, 70, 20);

          //---- LSDTDX ----
          LSDTDX.setComponentPopupMenu(BTD);
          LSDTDX.setName("LSDTDX");
          p_recup.add(LSDTDX);
          LSDTDX.setBounds(105, 80, 105, LSDTDX.getPreferredSize().height);

          //---- LSDTFX ----
          LSDTFX.setComponentPopupMenu(BTD);
          LSDTFX.setName("LSDTFX");
          p_recup.add(LSDTFX);
          LSDTFX.setBounds(240, 80, 105, LSDTFX.getPreferredSize().height);

          //---- LSCO1 ----
          LSCO1.setComponentPopupMenu(BTD);
          LSCO1.setName("LSCO1");
          p_recup.add(LSCO1);
          LSCO1.setBounds(535, 115, 58, LSCO1.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("C    N\u00b0Li");
          OBJ_32.setName("OBJ_32");
          p_recup.add(OBJ_32);
          OBJ_32.setBounds(25, 25, 45, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("Article");
          OBJ_41.setName("OBJ_41");
          p_recup.add(OBJ_41);
          OBJ_41.setBounds(95, 25, 45, 20);

          //---- WNLI ----
          WNLI.setComponentPopupMenu(BTD);
          WNLI.setName("WNLI");
          p_recup.add(WNLI);
          WNLI.setBounds(40, 45, 50, WNLI.getPreferredSize().height);

          //---- OBJ_48 ----
          OBJ_48.setText("@CADEV@");
          OBJ_48.setName("OBJ_48");
          p_recup.add(OBJ_48);
          OBJ_48.setBounds(145, 150, 34, OBJ_48.getPreferredSize().height);

          //---- CAUNA ----
          CAUNA.setComponentPopupMenu(BTD);
          CAUNA.setName("CAUNA");
          p_recup.add(CAUNA);
          CAUNA.setBounds(220, 115, 30, CAUNA.getPreferredSize().height);

          //---- A1UNV ----
          A1UNV.setComponentPopupMenu(BTD);
          A1UNV.setName("A1UNV");
          p_recup.add(A1UNV);
          A1UNV.setBounds(220, 170, 30, A1UNV.getPreferredSize().height);

          //---- WCOD ----
          WCOD.setComponentPopupMenu(BTD);
          WCOD.setName("WCOD");
          p_recup.add(WCOD);
          WCOD.setBounds(20, 45, 20, WCOD.getPreferredSize().height);

          //---- OBJ_17 ----
          OBJ_17.setText("\u00e0");
          OBJ_17.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_17.setName("OBJ_17");
          p_recup.add(OBJ_17);
          OBJ_17.setBounds(215, 84, 20, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("x");
          OBJ_26.setName("OBJ_26");
          p_recup.add(OBJ_26);
          OBJ_26.setBounds(510, 120, 9, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("=");
          OBJ_42.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_42.setName("OBJ_42");
          p_recup.add(OBJ_42);
          OBJ_42.setBounds(605, 120, 20, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 775, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(25, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 239, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(26, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Choix possibles");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie OBJ_15;
  private JLabel OBJ_47;
  private XRiTextField LSOBS;
  private XRiTextField WARTT;
  private XRiCheckBox LSTOP2;
  private JLabel OBJ_30;
  private XRiTextField PRSX;
  private XRiTextField PCALX;
  private XRiTextField PV01A;
  private JLabel OBJ_24;
  private JPanel P_PnlOpts;
  private JLabel OBJ_29;
  private JLabel OBJ_18;
  private JLabel OBJ_46;
  private XRiTextField LSP01X;
  private XRiTextField LSP02X;
  private JLabel OBJ_16;
  private XRiCalendrier LSDTDX;
  private XRiCalendrier LSDTFX;
  private XRiTextField LSCO1;
  private JLabel OBJ_32;
  private JLabel OBJ_41;
  private XRiTextField WNLI;
  private JLabel OBJ_48;
  private XRiTextField CAUNA;
  private XRiTextField A1UNV;
  private XRiTextField WCOD;
  private JLabel OBJ_17;
  private JLabel OBJ_26;
  private JLabel OBJ_42;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
