
package ri.serien.libecranrpg.vgvx.VGVXSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXSEFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] A_Value = { "", "0", "1", "2", "3", };
  private String[] C_Value = { "", "0", "1", "2" };
  private String[] D_Value = { "", "0", "1", "2" };
  private String[] E_Value = { "", "0", "1", "2", "3", "9" };
  private String[] onze_Value = { "", "0", "1", "9" };
  
  public VGVXSEFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SET018.setValeurs(C_Value, null);
    SET141.setValeurs(C_Value, null);
    SET019.setValeurs(A_Value, null);
    SET015.setValeurs(A_Value, null);
    SET154.setValeurs(D_Value, null);
    SET006.setValeurs(A_Value, null);
    SET005.setValeurs(A_Value, null);
    SET004.setValeurs(A_Value, null);
    SET003.setValeurs(A_Value, null);
    SET002.setValeurs(A_Value, null);
    SET001.setValeurs(E_Value, null);
    SET011.setValeurs(onze_Value, null);
    SETDR.setValeursSelection("1", "");
    SET035.setValeursSelection("0", "1");
    SET022.setValeursSelection("0", "1");
    SET021.setValeursSelection("0", "1");
    SET020.setValeursSelection("0", "1");
    SET017.setValeursSelection("0", "1");
    SET016.setValeursSelection("0", "1");
    SET014.setValeursSelection("0", "1");
    SET013.setValeursSelection("0", "1");
    SET012.setValeursSelection("0", "1");
    SET010.setValeursSelection("0", "1");
    SET009.setValeursSelection("0", "1");
    SET008.setValeursSelection("0", "1");
    SET007.setValeursSelection("0", "1");
    SET168.setValeursSelection("0", "1");
    // 4 Valeurs possibles : Non autorisé à effectuer un différé, Autorisé à effectuer un différé avec demande d'autorisation, Autorisé à
    // effectuer un différé sans demande d'autorisation, Autorisé à effectuer un différé et à autoriser des demandes
    SET171.setValeurs(A_Value, null);
    // 1=Droits direction 0=Pas de droits direction
    SET172.setValeursSelection("1", "0");
    SET173.setValeursSelection("0", "1");
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(
        lexique.TranslationTable(interpreteurD.analyseExpression("Sécurité de la gestion des ventes et des achats (@LOCUSR@)")).trim());
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_97.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Gestion des tops mals initialisés : si on a une case à cocher, on lit la zone correspondante. Si blanc DECOCHE
    // comme avec la valeur 1.
    // SEUL LE 0 COCHE (autorisé).
    for (int i = 0; i < getAllComponents(this).size(); i++) {
      if (getAllComponents(this).get(i) instanceof XRiCheckBox) {
        if (lexique.HostFieldGetData(getAllComponents(this).get(i).getName()).trim().equals("")) {
          ((XRiCheckBox) getAllComponents(this).get(i)).setSelected(false);
        }
      }
    }
    
    riSousMenu6.setEnabled(lexique.isTrue("51"));
    
    V06F1.setVisible(lexique.isTrue("N53"));
    V06F.setVisible(!V06F1.isVisible());
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (V06F1.isVisible()) {
      V06F1.setText("F");
    }
    else {
      lexique.HostFieldPutData("V06F1", 0, "F");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  public static List<Component> getAllComponents(final Container c) {
    Component[] comps = c.getComponents();
    List<Component> compList = new ArrayList<Component>();
    for (Component comp : comps) {
      compList.add(comp);
      if (comp instanceof Container) {
        compList.addAll(getAllComponents((Container) comp));
      }
    }
    return compList;
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    INDUSR = new RiZoneSortie();
    OBJ_35 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_40 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_97 = new JLabel();
    OBJ_95 = new JLabel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    pnlContenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    pnlHaut = new SNPanel();
    OBJ_86 = new JLabel();
    SERPUS = new XRiTextField();
    SET155 = new XRiTextField();
    SEGRP = new XRiTextField();
    OBJ_87 = new JLabel();
    OBJ_88 = new JLabel();
    SETDR = new XRiCheckBox();
    SET172 = new XRiCheckBox();
    pnlGauche = new SNPanel();
    OBJ_90 = new JLabel();
    OBJ_96 = new JLabel();
    OBJ_102 = new JLabel();
    OBJ_110 = new JLabel();
    OBJ_116 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_134 = new JLabel();
    OBJ_144 = new JLabel();
    OBJ_150 = new JLabel();
    OBJ_156 = new JLabel();
    OBJ_163 = new JLabel();
    OBJ_80 = new JLabel();
    SET001 = new XRiComboBox();
    lab002 = new JLabel();
    lab003 = new JLabel();
    lab004 = new JLabel();
    lab005 = new JLabel();
    lab006 = new JLabel();
    SET007 = new XRiCheckBox();
    SET008 = new XRiCheckBox();
    SET009 = new XRiCheckBox();
    SET010 = new XRiCheckBox();
    SET012 = new XRiCheckBox();
    label15 = new JLabel();
    SET002 = new XRiComboBox();
    SET003 = new XRiComboBox();
    SET004 = new XRiComboBox();
    SET005 = new XRiComboBox();
    SET006 = new XRiComboBox();
    SET154 = new XRiComboBox();
    SET011 = new XRiComboBox();
    lab7 = new JLabel();
    SET171 = new XRiComboBox();
    OBJ_129 = new JLabel();
    pnlDroite = new SNPanel();
    OBJ_83 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_105 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_119 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_131 = new JLabel();
    OBJ_137 = new JLabel();
    OBJ_147 = new JLabel();
    OBJ_153 = new JLabel();
    OBJ_159 = new JLabel();
    SET013 = new XRiCheckBox();
    label8 = new JLabel();
    SET014 = new XRiCheckBox();
    SET016 = new XRiCheckBox();
    SET017 = new XRiCheckBox();
    SET020 = new XRiCheckBox();
    SET021 = new XRiCheckBox();
    SET022 = new XRiCheckBox();
    SET035 = new XRiCheckBox();
    OBJ_120 = new JLabel();
    OBJ_160 = new JLabel();
    SET015 = new XRiComboBox();
    SET019 = new XRiComboBox();
    label16 = new JLabel();
    SET141 = new XRiComboBox();
    SET018 = new XRiComboBox();
    SET168 = new XRiCheckBox();
    OBJ_154 = new JLabel();
    SET173 = new XRiCheckBox();
    OBJ_155 = new JLabel();
    pnlBas = new SNPanel();
    V06F1 = new XRiTextField();
    V06F = new XRiTextField();
    OBJ_69 = new SNBoutonLeger();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 de la gestion des ventes et des achats (@LOCUSR@)");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_34 ----
          OBJ_34.setText("Utilisateur");
          OBJ_34.setName("OBJ_34");
          
          // ---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setText("@INDUSR@");
          INDUSR.setOpaque(false);
          INDUSR.setName("INDUSR");
          
          // ---- OBJ_35 ----
          OBJ_35.setText("Etablissement");
          OBJ_35.setName("OBJ_35");
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");
          
          // ---- OBJ_40 ----
          OBJ_40.setText("@DGNOM@");
          OBJ_40.setOpaque(false);
          OBJ_40.setName("OBJ_40");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(75, 75, 75).addComponent(INDUSR,
                          GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                      .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
                  .addGap(65, 65, 65).addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE).addGap(3, 3, 3)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(9, 9, 9)
                  .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 339, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- OBJ_97 ----
          OBJ_97.setText("@WPAGE@");
          OBJ_97.setName("OBJ_97");
          p_tete_droite.add(OBJ_97);
          
          // ---- OBJ_95 ----
          OBJ_95.setText("Page");
          OBJ_95.setName("OBJ_95");
          p_tete_droite.add(OBJ_95);
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);
    
    // ======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());
      
      // ======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Tous droits sur tout");
              riSousMenu_bt6.setToolTipText("Tous droits sur tout");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed(e));
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Valider les modifications");
              riSousMenu_bt7.setToolTipText("Valider les modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(e -> riSousMenu_bt7ActionPerformed(e));
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(1000, 600));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);
        
        // ======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Objet de la s\u00e9curit\u00e9 (GVM)");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);
          
          // ======== pnlHaut ========
          {
            pnlHaut.setBorder(new TitledBorder(""));
            pnlHaut.setOpaque(false);
            pnlHaut.setName("pnlHaut");
            pnlHaut.setLayout(null);
            
            // ---- OBJ_86 ----
            OBJ_86.setText("Code repr\u00e9sentant");
            OBJ_86.setName("OBJ_86");
            pnlHaut.add(OBJ_86);
            OBJ_86.setBounds(25, 10, 125, 20);
            
            // ---- SERPUS ----
            SERPUS.setComponentPopupMenu(BTD);
            SERPUS.setName("SERPUS");
            pnlHaut.add(SERPUS);
            SERPUS.setBounds(155, 6, 30, SERPUS.getPreferredSize().height);
            
            // ---- SET155 ----
            SET155.setComponentPopupMenu(BTD);
            SET155.setName("SET155");
            pnlHaut.add(SET155);
            SET155.setBounds(295, 6, 20, SET155.getPreferredSize().height);
            
            // ---- SEGRP ----
            SEGRP.setComponentPopupMenu(BTD);
            SEGRP.setName("SEGRP");
            pnlHaut.add(SEGRP);
            SEGRP.setBounds(805, 6, 130, SEGRP.getPreferredSize().height);
            
            // ---- OBJ_87 ----
            OBJ_87.setText("S\u00e9curit\u00e9");
            OBJ_87.setName("OBJ_87");
            pnlHaut.add(OBJ_87);
            OBJ_87.setBounds(235, 10, 60, 20);
            
            // ---- OBJ_88 ----
            OBJ_88.setText("Profil de groupe");
            OBJ_88.setName("OBJ_88");
            pnlHaut.add(OBJ_88);
            OBJ_88.setBounds(700, 10, 100, 20);
            
            // ---- SETDR ----
            SETDR.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SETDR.setComponentPopupMenu(BTD);
            SETDR.setText("Tous les droits");
            SETDR.setName("SETDR");
            pnlHaut.add(SETDR);
            SETDR.setBounds(360, 11, 145, SETDR.getPreferredSize().height);
            
            // ---- SET172 ----
            SET172.setToolTipText("coch\u00e9 = droits direction, d\u00e9coch\u00e9 = pas de droits direction");
            SET172.setComponentPopupMenu(BTD);
            SET172.setText("Droits direction");
            SET172.setName("SET172");
            pnlHaut.add(SET172);
            SET172.setBounds(530, 11, 130, SET172.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < pnlHaut.getComponentCount(); i++) {
                Rectangle bounds = pnlHaut.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlHaut.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlHaut.setMinimumSize(preferredSize);
              pnlHaut.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel1ContentContainer.add(pnlHaut);
          pnlHaut.setBounds(10, 5, 950, 45);
          
          // ======== pnlGauche ========
          {
            pnlGauche.setBorder(new TitledBorder(""));
            pnlGauche.setOpaque(false);
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(null);
            
            // ---- OBJ_90 ----
            OBJ_90.setText("02");
            OBJ_90.setFont(OBJ_90.getFont().deriveFont(OBJ_90.getFont().getStyle() | Font.BOLD));
            OBJ_90.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_90.setName("OBJ_90");
            pnlGauche.add(OBJ_90);
            OBJ_90.setBounds(20, 48, 21, 22);
            
            // ---- OBJ_96 ----
            OBJ_96.setText("03");
            OBJ_96.setFont(OBJ_96.getFont().deriveFont(OBJ_96.getFont().getStyle() | Font.BOLD));
            OBJ_96.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_96.setName("OBJ_96");
            pnlGauche.add(OBJ_96);
            OBJ_96.setBounds(20, 81, 21, 22);
            
            // ---- OBJ_102 ----
            OBJ_102.setText("04");
            OBJ_102.setFont(OBJ_102.getFont().deriveFont(OBJ_102.getFont().getStyle() | Font.BOLD));
            OBJ_102.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_102.setName("OBJ_102");
            pnlGauche.add(OBJ_102);
            OBJ_102.setBounds(20, 114, 21, 22);
            
            // ---- OBJ_110 ----
            OBJ_110.setText("05");
            OBJ_110.setFont(OBJ_110.getFont().deriveFont(OBJ_110.getFont().getStyle() | Font.BOLD));
            OBJ_110.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_110.setName("OBJ_110");
            pnlGauche.add(OBJ_110);
            OBJ_110.setBounds(20, 147, 21, 22);
            
            // ---- OBJ_116 ----
            OBJ_116.setText("06");
            OBJ_116.setFont(OBJ_116.getFont().deriveFont(OBJ_116.getFont().getStyle() | Font.BOLD));
            OBJ_116.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_116.setName("OBJ_116");
            pnlGauche.add(OBJ_116);
            OBJ_116.setBounds(20, 180, 21, 22);
            
            // ---- OBJ_122 ----
            OBJ_122.setText("07");
            OBJ_122.setFont(OBJ_122.getFont().deriveFont(OBJ_122.getFont().getStyle() | Font.BOLD));
            OBJ_122.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_122.setName("OBJ_122");
            pnlGauche.add(OBJ_122);
            OBJ_122.setBounds(20, 209, 21, 22);
            
            // ---- OBJ_128 ----
            OBJ_128.setText("08");
            OBJ_128.setFont(OBJ_128.getFont().deriveFont(OBJ_128.getFont().getStyle() | Font.BOLD));
            OBJ_128.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_128.setName("OBJ_128");
            pnlGauche.add(OBJ_128);
            OBJ_128.setBounds(20, 234, 21, 22);
            
            // ---- OBJ_134 ----
            OBJ_134.setText("09");
            OBJ_134.setFont(OBJ_134.getFont().deriveFont(OBJ_134.getFont().getStyle() | Font.BOLD));
            OBJ_134.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_134.setName("OBJ_134");
            pnlGauche.add(OBJ_134);
            OBJ_134.setBounds(20, 259, 21, 22);
            
            // ---- OBJ_144 ----
            OBJ_144.setText("10");
            OBJ_144.setFont(OBJ_144.getFont().deriveFont(OBJ_144.getFont().getStyle() | Font.BOLD));
            OBJ_144.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_144.setName("OBJ_144");
            pnlGauche.add(OBJ_144);
            OBJ_144.setBounds(20, 284, 21, 22);
            
            // ---- OBJ_150 ----
            OBJ_150.setText("11");
            OBJ_150.setFont(OBJ_150.getFont().deriveFont(OBJ_150.getFont().getStyle() | Font.BOLD));
            OBJ_150.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_150.setName("OBJ_150");
            pnlGauche.add(OBJ_150);
            OBJ_150.setBounds(20, 313, 21, 22);
            
            // ---- OBJ_156 ----
            OBJ_156.setText("12");
            OBJ_156.setFont(OBJ_156.getFont().deriveFont(OBJ_156.getFont().getStyle() | Font.BOLD));
            OBJ_156.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_156.setName("OBJ_156");
            pnlGauche.add(OBJ_156);
            OBJ_156.setBounds(20, 342, 21, 22);
            
            // ---- OBJ_163 ----
            OBJ_163.setText("154");
            OBJ_163.setFont(OBJ_163.getFont().deriveFont(OBJ_163.getFont().getStyle() | Font.BOLD));
            OBJ_163.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_163.setName("OBJ_163");
            pnlGauche.add(OBJ_163);
            OBJ_163.setBounds(10, 369, 29, 26);
            
            // ---- OBJ_80 ----
            OBJ_80.setText("01");
            OBJ_80.setFont(OBJ_80.getFont().deriveFont(OBJ_80.getFont().getStyle() | Font.BOLD));
            OBJ_80.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_80.setName("OBJ_80");
            pnlGauche.add(OBJ_80);
            OBJ_80.setBounds(20, 15, 21, 22);
            
            // ---- SET001 ----
            SET001.setComponentPopupMenu(BTD);
            SET001.setModel(new DefaultComboBoxModel<>(new String[] { " ", "Tous les droits", "consultation et modification",
                "consultation seulement", "Aucun droit", "Superviseur" }));
            SET001.setName("SET001");
            pnlGauche.add(SET001);
            SET001.setBounds(new Rectangle(new Point(50, 13), SET001.getPreferredSize()));
            
            // ---- lab002 ----
            lab002.setComponentPopupMenu(BTD);
            lab002.setText("Gestion des clients");
            lab002.setName("lab002");
            pnlGauche.add(lab002);
            lab002.setBounds(235, 48, 230, 22);
            
            // ---- lab003 ----
            lab003.setComponentPopupMenu(BTD);
            lab003.setText("Gestion des articles");
            lab003.setName("lab003");
            pnlGauche.add(lab003);
            lab003.setBounds(235, 81, 230, 22);
            
            // ---- lab004 ----
            lab004.setComponentPopupMenu(BTD);
            lab004.setText("Gestion des tarifs en cours");
            lab004.setName("lab004");
            pnlGauche.add(lab004);
            lab004.setBounds(235, 114, 230, 22);
            
            // ---- lab005 ----
            lab005.setComponentPopupMenu(BTD);
            lab005.setText("Gestion des tarifs en pr\u00e9paration");
            lab005.setName("lab005");
            pnlGauche.add(lab005);
            lab005.setBounds(235, 147, 230, 22);
            
            // ---- lab006 ----
            lab006.setComponentPopupMenu(BTD);
            lab006.setText("Gestion des conditions de ventes");
            lab006.setName("lab006");
            pnlGauche.add(lab006);
            lab006.setBounds(235, 180, 230, 22);
            
            // ---- SET007 ----
            SET007.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET007.setComponentPopupMenu(BTD);
            SET007.setText("Vue achats sur fiche article");
            SET007.setName("SET007");
            pnlGauche.add(SET007);
            SET007.setBounds(50, 211, 405, SET007.getPreferredSize().height);
            
            // ---- SET008 ----
            SET008.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET008.setComponentPopupMenu(BTD);
            SET008.setText("Listes des articles");
            SET008.setName("SET008");
            pnlGauche.add(SET008);
            SET008.setBounds(50, 236, 405, SET008.getPreferredSize().height);
            
            // ---- SET009 ----
            SET009.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET009.setComponentPopupMenu(BTD);
            SET009.setText("Listes des tarifs");
            SET009.setName("SET009");
            pnlGauche.add(SET009);
            SET009.setBounds(50, 261, 405, SET009.getPreferredSize().height);
            
            // ---- SET010 ----
            SET010.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET010.setComponentPopupMenu(BTD);
            SET010.setText("Listes des conditions de ventes");
            SET010.setName("SET010");
            pnlGauche.add(SET010);
            SET010.setBounds(50, 286, 405, SET010.getPreferredSize().height);
            
            // ---- SET012 ----
            SET012.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET012.setComponentPopupMenu(BTD);
            SET012.setText("Edition du journal fiscal");
            SET012.setName("SET012");
            pnlGauche.add(SET012);
            SET012.setBounds(50, 344, 405, SET012.getPreferredSize().height);
            
            // ---- label15 ----
            label15.setText("Personnalisation du syst\u00e8me");
            label15.setName("label15");
            pnlGauche.add(label15);
            label15.setBounds(235, 15, 230, 22);
            
            // ---- SET002 ----
            SET002.setComponentPopupMenu(BTD);
            SET002.setModel(new DefaultComboBoxModel<>(
                new String[] { " ", "Tous les droits", "consultation et modification", "consultation seulement", "Aucun droit" }));
            SET002.setName("SET002");
            pnlGauche.add(SET002);
            SET002.setBounds(new Rectangle(new Point(50, 46), SET002.getPreferredSize()));
            
            // ---- SET003 ----
            SET003.setComponentPopupMenu(BTD);
            SET003.setModel(new DefaultComboBoxModel<>(
                new String[] { " ", "Tous les droits", "consultation et modification", "consultation seulement", "Aucun droit" }));
            SET003.setName("SET003");
            pnlGauche.add(SET003);
            SET003.setBounds(new Rectangle(new Point(50, 79), SET003.getPreferredSize()));
            
            // ---- SET004 ----
            SET004.setComponentPopupMenu(BTD);
            SET004.setModel(new DefaultComboBoxModel<>(
                new String[] { " ", "Tous les droits", "consultation et modification", "consultation seulement", "Aucun droit" }));
            SET004.setName("SET004");
            pnlGauche.add(SET004);
            SET004.setBounds(new Rectangle(new Point(50, 112), SET004.getPreferredSize()));
            
            // ---- SET005 ----
            SET005.setComponentPopupMenu(BTD);
            SET005.setModel(new DefaultComboBoxModel<>(
                new String[] { " ", "Tous les droits", "consultation et modification", "consultation seulement", "Aucun droit" }));
            SET005.setName("SET005");
            pnlGauche.add(SET005);
            SET005.setBounds(new Rectangle(new Point(50, 145), SET005.getPreferredSize()));
            
            // ---- SET006 ----
            SET006.setComponentPopupMenu(BTD);
            SET006.setModel(new DefaultComboBoxModel<>(
                new String[] { " ", "Tous les droits", "consultation et modification", "consultation seulement", "Aucun droit" }));
            SET006.setName("SET006");
            pnlGauche.add(SET006);
            SET006.setBounds(new Rectangle(new Point(50, 178), SET006.getPreferredSize()));
            
            // ---- SET154 ----
            SET154.setComponentPopupMenu(BTD);
            SET154.setModel(new DefaultComboBoxModel<>(new String[] { " ", "Vente en dessous du prix minimum autoris\u00e9",
                "Vente et achat interdit en dessous du prix minimum",
                "Vente interdit mais achat autoris\u00e9 en dessous du prix minimum" }));
            SET154.setToolTipText("Vente en dessous du prix minimum");
            SET154.setName("SET154");
            pnlGauche.add(SET154);
            SET154.setBounds(50, 369, 405, SET154.getPreferredSize().height);
            
            // ---- SET011 ----
            SET011.setComponentPopupMenu(BTD);
            SET011
                .setModel(new DefaultComboBoxModel<>(new String[] { " ", "Edition interdite", "Edition autoris\u00e9e", "Superviseur" }));
            SET011.setName("SET011");
            pnlGauche.add(SET011);
            SET011.setBounds(50, 311, 180, SET011.getPreferredSize().height);
            
            // ---- lab7 ----
            lab7.setComponentPopupMenu(BTD);
            lab7.setText("Listes des clients");
            lab7.setName("lab7");
            pnlGauche.add(lab7);
            lab7.setBounds(235, 313, 230, 22);
            
            // ---- SET171 ----
            SET171.setToolTipText("Autoriser les diff\u00e9r\u00e9s de r\u00e8glements");
            SET171.setComponentPopupMenu(BTD);
            SET171.setModel(new DefaultComboBoxModel<>(new String[] { " ", "Non autoris\u00e9 \u00e0 effectuer un diff\u00e9r\u00e9",
                "Autoris\u00e9 \u00e0 effectuer un diff\u00e9r\u00e9 avec demande d'autorisation",
                "Autoris\u00e9 \u00e0 effectuer un diff\u00e9r\u00e9 sans demande d'autorisation",
                "Autoris\u00e9 \u00e0 effectuer un diff\u00e9r\u00e9 et \u00e0 autoriser des demandes" }));
            SET171.setName("SET171");
            pnlGauche.add(SET171);
            SET171.setBounds(50, 402, 405, SET171.getPreferredSize().height);
            
            // ---- OBJ_129 ----
            OBJ_129.setText("171");
            OBJ_129.setFont(OBJ_129.getFont().deriveFont(OBJ_129.getFont().getStyle() | Font.BOLD));
            OBJ_129.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_129.setName("OBJ_129");
            pnlGauche.add(OBJ_129);
            OBJ_129.setBounds(20, 404, 21, 22);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < pnlGauche.getComponentCount(); i++) {
                Rectangle bounds = pnlGauche.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlGauche.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlGauche.setMinimumSize(preferredSize);
              pnlGauche.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel1ContentContainer.add(pnlGauche);
          pnlGauche.setBounds(10, 55, 470, 450);
          
          // ======== pnlDroite ========
          {
            pnlDroite.setBorder(new TitledBorder(""));
            pnlDroite.setOpaque(false);
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(null);
            
            // ---- OBJ_83 ----
            OBJ_83.setText("13");
            OBJ_83.setFont(OBJ_83.getFont().deriveFont(OBJ_83.getFont().getStyle() | Font.BOLD));
            OBJ_83.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_83.setName("OBJ_83");
            pnlDroite.add(OBJ_83);
            OBJ_83.setBounds(20, 15, 21, 22);
            
            // ---- OBJ_93 ----
            OBJ_93.setText("14");
            OBJ_93.setFont(OBJ_93.getFont().deriveFont(OBJ_93.getFont().getStyle() | Font.BOLD));
            OBJ_93.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_93.setName("OBJ_93");
            pnlDroite.add(OBJ_93);
            OBJ_93.setBounds(20, 44, 21, 22);
            
            // ---- OBJ_99 ----
            OBJ_99.setText("15");
            OBJ_99.setFont(OBJ_99.getFont().deriveFont(OBJ_99.getFont().getStyle() | Font.BOLD));
            OBJ_99.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_99.setName("OBJ_99");
            pnlDroite.add(OBJ_99);
            OBJ_99.setBounds(20, 73, 21, 22);
            
            // ---- OBJ_105 ----
            OBJ_105.setText("16");
            OBJ_105.setFont(OBJ_105.getFont().deriveFont(OBJ_105.getFont().getStyle() | Font.BOLD));
            OBJ_105.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_105.setName("OBJ_105");
            pnlDroite.add(OBJ_105);
            OBJ_105.setBounds(20, 102, 21, 22);
            
            // ---- OBJ_113 ----
            OBJ_113.setText("17");
            OBJ_113.setFont(OBJ_113.getFont().deriveFont(OBJ_113.getFont().getStyle() | Font.BOLD));
            OBJ_113.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_113.setName("OBJ_113");
            pnlDroite.add(OBJ_113);
            OBJ_113.setBounds(20, 131, 21, 22);
            
            // ---- OBJ_119 ----
            OBJ_119.setText("18");
            OBJ_119.setFont(OBJ_119.getFont().deriveFont(OBJ_119.getFont().getStyle() | Font.BOLD));
            OBJ_119.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_119.setName("OBJ_119");
            pnlDroite.add(OBJ_119);
            OBJ_119.setBounds(20, 160, 21, 22);
            
            // ---- OBJ_125 ----
            OBJ_125.setText("19");
            OBJ_125.setFont(OBJ_125.getFont().deriveFont(OBJ_125.getFont().getStyle() | Font.BOLD));
            OBJ_125.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_125.setName("OBJ_125");
            pnlDroite.add(OBJ_125);
            OBJ_125.setBounds(20, 189, 21, 22);
            
            // ---- OBJ_131 ----
            OBJ_131.setText("20");
            OBJ_131.setFont(OBJ_131.getFont().deriveFont(OBJ_131.getFont().getStyle() | Font.BOLD));
            OBJ_131.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_131.setName("OBJ_131");
            pnlDroite.add(OBJ_131);
            OBJ_131.setBounds(20, 218, 21, 22);
            
            // ---- OBJ_137 ----
            OBJ_137.setText("21");
            OBJ_137.setFont(OBJ_137.getFont().deriveFont(OBJ_137.getFont().getStyle() | Font.BOLD));
            OBJ_137.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_137.setName("OBJ_137");
            pnlDroite.add(OBJ_137);
            OBJ_137.setBounds(20, 247, 21, 22);
            
            // ---- OBJ_147 ----
            OBJ_147.setText("22");
            OBJ_147.setFont(OBJ_147.getFont().deriveFont(OBJ_147.getFont().getStyle() | Font.BOLD));
            OBJ_147.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_147.setName("OBJ_147");
            pnlDroite.add(OBJ_147);
            OBJ_147.setBounds(20, 276, 21, 22);
            
            // ---- OBJ_153 ----
            OBJ_153.setText("35");
            OBJ_153.setFont(OBJ_153.getFont().deriveFont(OBJ_153.getFont().getStyle() | Font.BOLD));
            OBJ_153.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_153.setName("OBJ_153");
            pnlDroite.add(OBJ_153);
            OBJ_153.setBounds(20, 305, 21, 22);
            
            // ---- OBJ_159 ----
            OBJ_159.setText("141");
            OBJ_159.setFont(OBJ_159.getFont().deriveFont(OBJ_159.getFont().getStyle() | Font.BOLD));
            OBJ_159.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_159.setName("OBJ_159");
            pnlDroite.add(OBJ_159);
            OBJ_159.setBounds(10, 334, 29, 26);
            
            // ---- SET013 ----
            SET013.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET013.setComponentPopupMenu(BTD);
            SET013.setText("Comptabilisation des ventes");
            SET013.setName("SET013");
            pnlDroite.add(SET013);
            SET013.setBounds(50, 17, 405, SET013.getPreferredSize().height);
            
            // ---- label8 ----
            label8.setText("Repr\u00e9sentants et commissionnements");
            label8.setName("label8");
            pnlDroite.add(label8);
            label8.setBounds(235, 73, 235, 22);
            
            // ---- SET014 ----
            SET014.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET014.setComponentPopupMenu(BTD);
            SET014.setText("Arr\u00eat\u00e9 mensuel GVM/GAM");
            SET014.setName("SET014");
            pnlDroite.add(SET014);
            SET014.setBounds(50, 46, 405, SET014.getPreferredSize().height);
            
            // ---- SET016 ----
            SET016.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET016.setComponentPopupMenu(BTD);
            SET016.setText("Edition des statistiques");
            SET016.setName("SET016");
            pnlDroite.add(SET016);
            SET016.setBounds(50, 104, 405, SET016.getPreferredSize().height);
            
            // ---- SET017 ----
            SET017.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET017.setComponentPopupMenu(BTD);
            SET017.setText("Tableau de bord GVM");
            SET017.setName("SET017");
            pnlDroite.add(SET017);
            SET017.setBounds(50, 133, 405, SET017.getPreferredSize().height);
            
            // ---- SET020 ----
            SET020.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET020.setComponentPopupMenu(BTD);
            SET020.setText("Purge achats, ventes et stocks");
            SET020.setName("SET020");
            pnlDroite.add(SET020);
            SET020.setBounds(50, 220, 405, SET020.getPreferredSize().height);
            
            // ---- SET021 ----
            SET021.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET021.setComponentPopupMenu(BTD);
            SET021.setText("Marge sur tableau de bord GVM");
            SET021.setName("SET021");
            pnlDroite.add(SET021);
            SET021.setBounds(50, 249, 405, SET021.getPreferredSize().height);
            
            // ---- SET022 ----
            SET022.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET022.setComponentPopupMenu(BTD);
            SET022.setText("G\u00e9n\u00e9rateur d'\u00e9tats");
            SET022.setName("SET022");
            pnlDroite.add(SET022);
            SET022.setBounds(50, 278, 405, SET022.getPreferredSize().height);
            
            // ---- SET035 ----
            SET035.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET035.setComponentPopupMenu(BTD);
            SET035.setText("Facturation diff\u00e9r\u00e9e");
            SET035.setName("SET035");
            pnlDroite.add(SET035);
            SET035.setBounds(50, 307, 405, SET035.getPreferredSize().height);
            
            // ---- OBJ_120 ----
            OBJ_120.setText("Encours client");
            OBJ_120.setName("OBJ_120");
            pnlDroite.add(OBJ_120);
            OBJ_120.setBounds(235, 160, 235, 22);
            
            // ---- OBJ_160 ----
            OBJ_160.setText("Gestion du bloc-notes client");
            OBJ_160.setName("OBJ_160");
            pnlDroite.add(OBJ_160);
            OBJ_160.setBounds(235, 336, 235, 22);
            
            // ---- SET015 ----
            SET015.setComponentPopupMenu(BTD);
            SET015.setModel(new DefaultComboBoxModel<>(
                new String[] { " ", "Tous les droits", "consultation et modification", "consultation seulement", "Aucun droit" }));
            SET015.setName("SET015");
            pnlDroite.add(SET015);
            SET015.setBounds(new Rectangle(new Point(50, 71), SET015.getPreferredSize()));
            
            // ---- SET019 ----
            SET019.setComponentPopupMenu(BTD);
            SET019.setModel(new DefaultComboBoxModel<>(
                new String[] { " ", "Tous les droits", "consultation et modification", "consultation seulement", "Aucun droit" }));
            SET019.setName("SET019");
            pnlDroite.add(SET019);
            SET019.setBounds(new Rectangle(new Point(50, 187), SET019.getPreferredSize()));
            
            // ---- label16 ----
            label16.setText("Conditions de r\u00e9mun\u00e9rations");
            label16.setName("label16");
            pnlDroite.add(label16);
            label16.setBounds(235, 189, 235, 22);
            
            // ---- SET141 ----
            SET141.setComponentPopupMenu(BTD);
            SET141.setModel(new DefaultComboBoxModel<>(new String[] { " ", "Tous les droits", "Consultation seulement", "Aucun droit" }));
            SET141.setName("SET141");
            pnlDroite.add(SET141);
            SET141.setBounds(50, 334, 180, SET141.getPreferredSize().height);
            
            // ---- SET018 ----
            SET018.setComponentPopupMenu(BTD);
            SET018.setModel(new DefaultComboBoxModel<>(new String[] { " ", "Tous les droits", "consultation seulement", "Aucun droit" }));
            SET018.setName("SET018");
            pnlDroite.add(SET018);
            SET018.setBounds(50, 158, 180, SET018.getPreferredSize().height);
            
            // ---- SET168 ----
            SET168.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET168.setComponentPopupMenu(BTD);
            SET168.setText("Visualisation et \u00e9dition des statistiques sur magasin unique");
            SET168.setName("SET168");
            pnlDroite.add(SET168);
            SET168.setBounds(50, 369, 405, SET168.getPreferredSize().height);
            
            // ---- OBJ_154 ----
            OBJ_154.setText("168");
            OBJ_154.setFont(OBJ_154.getFont().deriveFont(OBJ_154.getFont().getStyle() | Font.BOLD));
            OBJ_154.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_154.setName("OBJ_154");
            pnlDroite.add(OBJ_154);
            OBJ_154.setBounds(10, 367, 29, 22);
            
            // ---- SET173 ----
            SET173.setToolTipText("coch\u00e9 = autoris\u00e9, d\u00e9coch\u00e9 = interdit");
            SET173.setComponentPopupMenu(BTD);
            SET173.setText("Gestion de la veille concurrentielle");
            SET173.setName("SET173");
            pnlDroite.add(SET173);
            SET173.setBounds(50, 402, 405, SET173.getPreferredSize().height);
            
            // ---- OBJ_155 ----
            OBJ_155.setText("173");
            OBJ_155.setFont(OBJ_155.getFont().deriveFont(OBJ_155.getFont().getStyle() | Font.BOLD));
            OBJ_155.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_155.setName("OBJ_155");
            pnlDroite.add(OBJ_155);
            OBJ_155.setBounds(10, 400, 29, 22);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < pnlDroite.getComponentCount(); i++) {
                Rectangle bounds = pnlDroite.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlDroite.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlDroite.setMinimumSize(preferredSize);
              pnlDroite.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel1ContentContainer.add(pnlDroite);
          pnlDroite.setBounds(490, 55, 470, 450);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(15, 10, 974, 550);
        
        // ======== pnlBas ========
        {
          pnlBas.setOpaque(false);
          pnlBas.setName("pnlBas");
          pnlBas.setLayout(null);
          
          // ---- V06F1 ----
          V06F1.setComponentPopupMenu(BTD);
          V06F1.setName("V06F1");
          pnlBas.add(V06F1);
          V06F1.setBounds(220, 0, 25, V06F1.getPreferredSize().height);
          
          // ---- V06F ----
          V06F.setComponentPopupMenu(BTD);
          V06F.setName("V06F");
          pnlBas.add(V06F);
          V06F.setBounds(220, 0, 25, V06F.getPreferredSize().height);
          
          // ---- OBJ_69 ----
          OBJ_69.setText("Aller \u00e0 la page");
          OBJ_69.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_69.setName("OBJ_69");
          OBJ_69.addActionListener(e -> OBJ_69ActionPerformed(e));
          pnlBas.add(OBJ_69);
          OBJ_69.setBounds(new Rectangle(new Point(75, 0), OBJ_69.getPreferredSize()));
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < pnlBas.getComponentCount(); i++) {
              Rectangle bounds = pnlBas.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlBas.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlBas.setMinimumSize(preferredSize);
            pnlBas.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(pnlBas);
        pnlBas.setBounds(732, 560, 267, 34);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(e -> OBJ_16ActionPerformed(e));
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_34;
  private RiZoneSortie INDUSR;
  private JLabel OBJ_35;
  private RiZoneSortie INDETB;
  private RiZoneSortie OBJ_40;
  private JPanel p_tete_droite;
  private JLabel OBJ_97;
  private JLabel OBJ_95;
  private SNPanelFond pnlSud;
  private JPanel pnlMenus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel pnlContenu;
  private JXTitledPanel xTitledPanel1;
  private SNPanel pnlHaut;
  private JLabel OBJ_86;
  private XRiTextField SERPUS;
  private XRiTextField SET155;
  private XRiTextField SEGRP;
  private JLabel OBJ_87;
  private JLabel OBJ_88;
  private XRiCheckBox SETDR;
  private XRiCheckBox SET172;
  private SNPanel pnlGauche;
  private JLabel OBJ_90;
  private JLabel OBJ_96;
  private JLabel OBJ_102;
  private JLabel OBJ_110;
  private JLabel OBJ_116;
  private JLabel OBJ_122;
  private JLabel OBJ_128;
  private JLabel OBJ_134;
  private JLabel OBJ_144;
  private JLabel OBJ_150;
  private JLabel OBJ_156;
  private JLabel OBJ_163;
  private JLabel OBJ_80;
  private XRiComboBox SET001;
  private JLabel lab002;
  private JLabel lab003;
  private JLabel lab004;
  private JLabel lab005;
  private JLabel lab006;
  private XRiCheckBox SET007;
  private XRiCheckBox SET008;
  private XRiCheckBox SET009;
  private XRiCheckBox SET010;
  private XRiCheckBox SET012;
  private JLabel label15;
  private XRiComboBox SET002;
  private XRiComboBox SET003;
  private XRiComboBox SET004;
  private XRiComboBox SET005;
  private XRiComboBox SET006;
  private XRiComboBox SET154;
  private XRiComboBox SET011;
  private JLabel lab7;
  private XRiComboBox SET171;
  private JLabel OBJ_129;
  private SNPanel pnlDroite;
  private JLabel OBJ_83;
  private JLabel OBJ_93;
  private JLabel OBJ_99;
  private JLabel OBJ_105;
  private JLabel OBJ_113;
  private JLabel OBJ_119;
  private JLabel OBJ_125;
  private JLabel OBJ_131;
  private JLabel OBJ_137;
  private JLabel OBJ_147;
  private JLabel OBJ_153;
  private JLabel OBJ_159;
  private XRiCheckBox SET013;
  private JLabel label8;
  private XRiCheckBox SET014;
  private XRiCheckBox SET016;
  private XRiCheckBox SET017;
  private XRiCheckBox SET020;
  private XRiCheckBox SET021;
  private XRiCheckBox SET022;
  private XRiCheckBox SET035;
  private JLabel OBJ_120;
  private JLabel OBJ_160;
  private XRiComboBox SET015;
  private XRiComboBox SET019;
  private JLabel label16;
  private XRiComboBox SET141;
  private XRiComboBox SET018;
  private XRiCheckBox SET168;
  private JLabel OBJ_154;
  private XRiCheckBox SET173;
  private JLabel OBJ_155;
  private SNPanel pnlBas;
  private XRiTextField V06F1;
  private XRiTextField V06F;
  private SNBoutonLeger OBJ_69;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
