
package ri.serien.libecranrpg.vgvx.VGVXAIFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXAIFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] AITYP_Value = { "", "A", "B", "I", "S", "1", "2", "P", };
  private String[] AIIN3_Value = { "", "1", "2", "3", "4", };
  
  public VGVXAIFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    AITYP.setValeurs(AITYP_Value, null);
    AIIN3.setValeurs(AIIN3_Value, null);
    AIIN4.setValeursSelection("1", " ");
    AIIN2.setValeursSelection("1", " ");
    AIIN1.setValeursSelection("1", " ");
    AIIN5.setValeursSelection("1", " ");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDECR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDECR@")).trim());
    INDIMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIMP@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDMAG@")).trim());
    INDREF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDREF@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_77.setVisible(lexique.isPresent("AIPRTF"));
    
    // icones
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    bt_NewSim.setIcon(lexique.chargerImage("images/inject.gif", true));
    p_bpresentation.setCodeEtablissement(INDETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void AIIN4ActionPerformed(ActionEvent e) {
    bt_NewSim.setVisible(AIIN4.isSelected());
  }
  
  private void AIIN3ActionPerformed(ActionEvent e) {
    AIIN4.setSelected(AIIN3.getSelectedIndex() != 0);
    AIIN4.setVisible(AIIN4.isSelected());
    bt_NewSim.setVisible(AIIN4.isSelected());
  }
  
  private void bt_NewSimActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    INDECR = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDIMP = new RiZoneSortie();
    INDETB = new RiZoneSortie();
    OBJ_43 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_46 = new JLabel();
    INDMAG = new RiZoneSortie();
    OBJ_50 = new JLabel();
    INDREF = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    AIIN3 = new XRiComboBox();
    AITYP = new XRiComboBox();
    AIIN1 = new XRiCheckBox();
    AIIN2 = new XRiCheckBox();
    OBJ_80 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_74 = new JLabel();
    AIPRT = new XRiTextField();
    AIOQE = new XRiTextField();
    AIPRTF = new XRiTextField();
    AIPGS = new XRiTextField();
    AISPL = new XRiTextField();
    OBJ_78 = new JLabel();
    OBJ_73 = new JLabel();
    AIBAC = new XRiTextField();
    AITIR = new XRiTextField();
    OBJ_62 = new JLabel();
    AIFDP = new XRiTextField();
    OBJ_79 = new JLabel();
    bt_NewSim = new JButton();
    AIIN4 = new XRiCheckBox();
    AIIN5 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Affectation d'impressions");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(880, 32));
          p_tete_gauche.setMinimumSize(new Dimension(880, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
          
          // ---- INDECR ----
          INDECR.setComponentPopupMenu(BTD);
          INDECR.setText("@INDECR@");
          INDECR.setOpaque(false);
          INDECR.setName("INDECR");
          p_tete_gauche.add(INDECR);
          INDECR.setBounds(195, 1, 110, INDECR.getPreferredSize().height);
          
          // ---- OBJ_44 ----
          OBJ_44.setText("Profil");
          OBJ_44.setName("OBJ_44");
          p_tete_gauche.add(OBJ_44);
          OBJ_44.setBounds(120, 1, 75, 24);
          
          // ---- INDIMP ----
          INDIMP.setComponentPopupMenu(BTD);
          INDIMP.setText("@INDIMP@");
          INDIMP.setOpaque(false);
          INDIMP.setName("INDIMP");
          p_tete_gauche.add(INDIMP);
          INDIMP.setBounds(35, 1, 60, INDIMP.getPreferredSize().height);
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(425, 1, 40, INDETB.getPreferredSize().height);
          
          // ---- OBJ_43 ----
          OBJ_43.setText("Etat");
          OBJ_43.setName("OBJ_43");
          p_tete_gauche.add(OBJ_43);
          OBJ_43.setBounds(5, 1, 31, 24);
          
          // ---- OBJ_45 ----
          OBJ_45.setText("Etablissement");
          OBJ_45.setName("OBJ_45");
          p_tete_gauche.add(OBJ_45);
          OBJ_45.setBounds(335, 1, 90, 24);
          
          // ---- OBJ_46 ----
          OBJ_46.setText("Magasin");
          OBJ_46.setName("OBJ_46");
          p_tete_gauche.add(OBJ_46);
          OBJ_46.setBounds(495, 1, 55, 24);
          
          // ---- INDMAG ----
          INDMAG.setComponentPopupMenu(BTD);
          INDMAG.setText("@INDMAG@");
          INDMAG.setOpaque(false);
          INDMAG.setName("INDMAG");
          p_tete_gauche.add(INDMAG);
          INDMAG.setBounds(555, 1, 34, INDMAG.getPreferredSize().height);
          
          // ---- OBJ_50 ----
          OBJ_50.setText("R\u00e9f\u00e9rence");
          OBJ_50.setName("OBJ_50");
          p_tete_gauche.add(OBJ_50);
          OBJ_50.setBounds(607, 1, 67, 24);
          
          // ---- INDREF ----
          INDREF.setComponentPopupMenu(BTD);
          INDREF.setText("@INDREF@");
          INDREF.setOpaque(false);
          INDREF.setName("INDREF");
          p_tete_gauche.add(INDREF);
          INDREF.setBounds(677, 1, 110, INDREF.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Extentions");
              riSousMenu_bt6.setToolTipText("Extensions des affectations d'impression");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(570, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- AIIN3 ----
            AIIN3.setModel(new DefaultComboBoxModel(new String[] { "Pas d'\u00e9dition PDF", "PDF", "PDF sans message adresse",
                "PDF sans message adresse ni TVA", "PDF sans message adresse ni TVA (double monnaie)" }));
            AIIN3.setComponentPopupMenu(BTD);
            AIIN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AIIN3.setName("AIIN3");
            AIIN3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                AIIN3ActionPerformed(e);
              }
            });
            panel1.add(AIIN3);
            AIIN3.setBounds(185, 310, 290, AIIN3.getPreferredSize().height);
            
            // ---- AITYP ----
            AITYP.setModel(new DefaultComboBoxModel(new String[] { "", "Alcool", "Papier blanc", "A l'italienne", "Sp\u00e9cifique",
                "A4 Vertical", "A4", "Prog. Sp\u00e9cifique" }));
            AITYP.setComponentPopupMenu(BTD);
            AITYP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AITYP.setName("AITYP");
            panel1.add(AITYP);
            AITYP.setBounds(185, 206, 135, AITYP.getPreferredSize().height);
            
            // ---- AIIN1 ----
            AIIN1.setText("Edition \u00e0 suspendre");
            AIIN1.setComponentPopupMenu(BTD);
            AIIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AIIN1.setName("AIIN1");
            panel1.add(AIIN1);
            AIIN1.setBounds(185, 345, 245, 20);
            
            // ---- AIIN2 ----
            AIIN2.setText("Edition \u00e0 sauvegarder");
            AIIN2.setComponentPopupMenu(BTD);
            AIIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            AIIN2.setName("AIIN2");
            panel1.add(AIIN2);
            AIIN2.setBounds(185, 375, 245, 20);
            
            // ---- OBJ_80 ----
            OBJ_80.setText("Programme sp\u00e9cifique");
            OBJ_80.setName("OBJ_80");
            panel1.add(OBJ_80);
            OBJ_80.setBounds(35, 244, 141, 20);
            
            // ---- OBJ_77 ----
            OBJ_77.setText("Fichier d'impression");
            OBJ_77.setName("OBJ_77");
            panel1.add(OBJ_77);
            OBJ_77.setBounds(365, 174, 123, 20);
            
            // ---- OBJ_75 ----
            OBJ_75.setText("Tiroir alimentation");
            OBJ_75.setName("OBJ_75");
            panel1.add(OBJ_75);
            OBJ_75.setBounds(35, 139, 141, 20);
            
            // ---- OBJ_76 ----
            OBJ_76.setText("Fond de page");
            OBJ_76.setName("OBJ_76");
            panel1.add(OBJ_76);
            OBJ_76.setBounds(35, 174, 141, 20);
            
            // ---- OBJ_81 ----
            OBJ_81.setText("Nom du spool");
            OBJ_81.setName("OBJ_81");
            panel1.add(OBJ_81);
            OBJ_81.setBounds(35, 279, 141, 20);
            
            // ---- OBJ_82 ----
            OBJ_82.setText("Edition PDF");
            OBJ_82.setName("OBJ_82");
            panel1.add(OBJ_82);
            OBJ_82.setBounds(35, 313, 141, 20);
            
            // ---- OBJ_74 ----
            OBJ_74.setText("Bac de r\u00e9ception");
            OBJ_74.setName("OBJ_74");
            panel1.add(OBJ_74);
            OBJ_74.setBounds(35, 104, 141, 20);
            
            // ---- AIPRT ----
            AIPRT.setComponentPopupMenu(BTD);
            AIPRT.setName("AIPRT");
            panel1.add(AIPRT);
            AIPRT.setBounds(185, 30, 110, AIPRT.getPreferredSize().height);
            
            // ---- AIOQE ----
            AIOQE.setComponentPopupMenu(BTD);
            AIOQE.setName("AIOQE");
            panel1.add(AIOQE);
            AIOQE.setBounds(185, 65, 110, AIOQE.getPreferredSize().height);
            
            // ---- AIPRTF ----
            AIPRTF.setComponentPopupMenu(BTD);
            AIPRTF.setName("AIPRTF");
            panel1.add(AIPRTF);
            AIPRTF.setBounds(365, 205, 110, AIPRTF.getPreferredSize().height);
            
            // ---- AIPGS ----
            AIPGS.setComponentPopupMenu(BTD);
            AIPGS.setName("AIPGS");
            panel1.add(AIPGS);
            AIPGS.setBounds(185, 240, 110, AIPGS.getPreferredSize().height);
            
            // ---- AISPL ----
            AISPL.setComponentPopupMenu(BTD);
            AISPL.setName("AISPL");
            panel1.add(AISPL);
            AISPL.setBounds(185, 275, 110, AISPL.getPreferredSize().height);
            
            // ---- OBJ_78 ----
            OBJ_78.setText("Type de papier");
            OBJ_78.setName("OBJ_78");
            panel1.add(OBJ_78);
            OBJ_78.setBounds(35, 209, 141, 20);
            
            // ---- OBJ_73 ----
            OBJ_73.setText("File d'attente");
            OBJ_73.setName("OBJ_73");
            panel1.add(OBJ_73);
            OBJ_73.setBounds(35, 69, 141, 20);
            
            // ---- AIBAC ----
            AIBAC.setComponentPopupMenu(BTD);
            AIBAC.setName("AIBAC");
            panel1.add(AIBAC);
            AIBAC.setBounds(185, 100, 110, AIBAC.getPreferredSize().height);
            
            // ---- AITIR ----
            AITIR.setComponentPopupMenu(BTD);
            AITIR.setName("AITIR");
            panel1.add(AITIR);
            AITIR.setBounds(185, 135, 110, AITIR.getPreferredSize().height);
            
            // ---- OBJ_62 ----
            OBJ_62.setText("Imprimante");
            OBJ_62.setName("OBJ_62");
            panel1.add(OBJ_62);
            OBJ_62.setBounds(35, 34, 141, 20);
            
            // ---- AIFDP ----
            AIFDP.setName("AIFDP");
            panel1.add(AIFDP);
            AIFDP.setBounds(185, 170, 90, AIFDP.getPreferredSize().height);
            
            // ---- OBJ_79 ----
            OBJ_79.setText("\"P\" ->");
            OBJ_79.setName("OBJ_79");
            panel1.add(OBJ_79);
            OBJ_79.setBounds(330, 209, 33, 20);
            
            // ---- bt_NewSim ----
            bt_NewSim.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            bt_NewSim.setName("bt_NewSim");
            bt_NewSim.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_NewSimActionPerformed(e);
              }
            });
            panel1.add(bt_NewSim);
            bt_NewSim.setBounds(485, 410, 48, 48);
            
            // ---- AIIN4 ----
            AIIN4.setText("Edition NewSim");
            AIIN4.setName("AIIN4");
            AIIN4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                AIIN4ActionPerformed(e);
              }
            });
            panel1.add(AIIN4);
            AIIN4.setBounds(185, 405, 245, AIIN4.getPreferredSize().height);
            
            // ---- AIIN5 ----
            AIIN5.setText("Envoyer le mail directement");
            AIIN5.setName("AIIN5");
            AIIN5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                AIIN4ActionPerformed(e);
              }
            });
            panel1.add(AIIN5);
            AIIN5.setBounds(185, 435, 245, AIIN5.getPreferredSize().height);
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 544, Short.MAX_VALUE).addContainerGap()));
          p_contenuLayout.setVerticalGroup(
              p_contenuLayout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                  .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE).addContainerGap()));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie INDECR;
  private JLabel OBJ_44;
  private RiZoneSortie INDIMP;
  private RiZoneSortie INDETB;
  private JLabel OBJ_43;
  private JLabel OBJ_45;
  private JLabel OBJ_46;
  private RiZoneSortie INDMAG;
  private JLabel OBJ_50;
  private RiZoneSortie INDREF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiComboBox AIIN3;
  private XRiComboBox AITYP;
  private XRiCheckBox AIIN1;
  private XRiCheckBox AIIN2;
  private JLabel OBJ_80;
  private JLabel OBJ_77;
  private JLabel OBJ_75;
  private JLabel OBJ_76;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private JLabel OBJ_74;
  private XRiTextField AIPRT;
  private XRiTextField AIOQE;
  private XRiTextField AIPRTF;
  private XRiTextField AIPGS;
  private XRiTextField AISPL;
  private JLabel OBJ_78;
  private JLabel OBJ_73;
  private XRiTextField AIBAC;
  private XRiTextField AITIR;
  private JLabel OBJ_62;
  private XRiTextField AIFDP;
  private JLabel OBJ_79;
  private JButton bt_NewSim;
  private XRiCheckBox AIIN4;
  private XRiCheckBox AIIN5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
