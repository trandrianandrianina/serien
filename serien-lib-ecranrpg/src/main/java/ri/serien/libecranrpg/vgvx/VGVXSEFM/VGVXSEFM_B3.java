
package ri.serien.libecranrpg.vgvx.VGVXSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXSEFM_B3 extends SNPanelEcranRPG implements ioFrame {
   
  private String[] A_Value = { "", "0", "1", "2", "3", "9" };
  private String[] D_Value = { "", "0", "1", "2", "3", "4" };
  private String[] G_Value = { "0", "1", "2" };
  
  public VGVXSEFM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SET052.setValeurs(D_Value, null);
    SET051.setValeurs(A_Value, null);
    SET049.setValeurs(A_Value, null);
    SET048.setValeurs(A_Value, null);
    SET047.setValeurs(A_Value, null);
    SET046.setValeurs(A_Value, null);
    SET045.setValeurs(A_Value, null);
    SET044.setValeurs(A_Value, null);
    SET043.setValeurs(A_Value, null);
    SET054.setValeursSelection("0", "1");
    SET053.setValeursSelection("0", "1");
    SET059.setValeursSelection("0", "1");
    SET058.setValeursSelection("0", "1");
    SET176.setValeurs(G_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Sécurité de la gestion des ventes et des achats  (@LOCUSR@)")).trim());
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_96.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    riSousMenu6.setEnabled(lexique.isTrue("51"));
    
    
    // V06F.setVisible(!lexique.HostFieldGetData("V01F").trim().equalsIgnoreCase("MODIFIC."));
    V06F1.setVisible(lexique.isTrue("N53"));
    V06F.setVisible(!V06F1.isVisible());
    
    // Gestion des tops mals initialisés : si on a une case à cocher, on lit la zone correspondante. Si blanc décoche
    // comme avec la valeur 1.
    // SEUL LE 0 COCHE (autorisé).
    for (int i = 0; i < getAllComponents(this).size(); i++) {
      if (getAllComponents(this).get(i) instanceof XRiCheckBox) {
        if (lexique.HostFieldGetData(getAllComponents(this).get(i).getName()).trim().equals("")) {
          ((XRiCheckBox) getAllComponents(this).get(i)).setSelected(false);
        }
      }
    }
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (V06F1.isVisible()) {
      V06F1.setText("F");
    }
    else {
      lexique.HostFieldPutData("V06F1", 0, "F");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  public static List<Component> getAllComponents(final Container c) {
    Component[] comps = c.getComponents();
    List<Component> compList = new ArrayList<Component>();
    for (Component comp : comps) {
      compList.add(comp);
      if (comp instanceof Container) {
        compList.addAll(getAllComponents((Container) comp));
      }
    }
    return compList;
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    INDUSR = new RiZoneSortie();
    OBJ_36 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_40 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_96 = new JLabel();
    OBJ_95 = new JLabel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    p_menus2 = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    pnlBas = new SNPanel();
    V06F1 = new XRiTextField();
    V06F = new XRiTextField();
    OBJ_69 = new SNBoutonLeger();
    xTitledPanel1 = new JXTitledPanel();
    pnlGauche = new SNPanel();
    OBJ_90 = new JLabel();
    OBJ_121 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_186 = new JLabel();
    OBJ_187 = new JLabel();
    OBJ_190 = new JLabel();
    OBJ_194 = new JLabel();
    OBJ_195 = new JLabel();
    OBJ_196 = new JLabel();
    OBJ_197 = new JLabel();
    OBJ_198 = new JLabel();
    OBJ_199 = new JLabel();
    OBJ_81 = new JLabel();
    SET058 = new XRiCheckBox();
    OBJ_200 = new JLabel();
    SET044 = new XRiComboBox();
    OBJ_120 = new JLabel();
    SET045 = new XRiComboBox();
    OBJ_123 = new JLabel();
    SET046 = new XRiComboBox();
    OBJ_124 = new JLabel();
    SET047 = new XRiComboBox();
    OBJ_125 = new JLabel();
    SET048 = new XRiComboBox();
    OBJ_126 = new JLabel();
    SET049 = new XRiComboBox();
    OBJ_127 = new JLabel();
    SET051 = new XRiComboBox();
    OBJ_128 = new JLabel();
    SET059 = new XRiCheckBox();
    SET052 = new XRiComboBox();
    OBJ_201 = new JLabel();
    SET053 = new XRiCheckBox();
    SET054 = new XRiCheckBox();
    SET043 = new XRiComboBox();
    pnlDroite = new SNPanel();
    OBJ_85 = new JLabel();
    SET167 = new XRiTextField();
    OBJ_155 = new JLabel();
    OBJ_99 = new JLabel();
    SET176 = new XRiComboBox();
    OBJ_202 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 de la gestion des ventes et des achats  (@LOCUSR@)");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_34 ----
          OBJ_34.setText("Utilisateur");
          OBJ_34.setName("OBJ_34");

          //---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setText("@INDUSR@");
          INDUSR.setOpaque(false);
          INDUSR.setName("INDUSR");

          //---- OBJ_36 ----
          OBJ_36.setText("Etablissement");
          OBJ_36.setName("OBJ_36");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_40 ----
          OBJ_40.setText("@DGNOM@");
          OBJ_40.setOpaque(false);
          OBJ_40.setName("OBJ_40");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(75, 75, 75)
                    .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)))
                .addGap(65, 65, 65)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 339, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_96 ----
          OBJ_96.setText("@WPAGE@");
          OBJ_96.setName("OBJ_96");
          p_tete_droite.add(OBJ_96);

          //---- OBJ_95 ----
          OBJ_95.setText("Page");
          OBJ_95.setName("OBJ_95");
          p_tete_droite.add(OBJ_95);
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());

      //======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());

        //======== p_menus2 ========
        {
          p_menus2.setPreferredSize(new Dimension(170, 0));
          p_menus2.setMinimumSize(new Dimension(170, 0));
          p_menus2.setBackground(new Color(238, 239, 241));
          p_menus2.setBorder(LineBorder.createGrayLineBorder());
          p_menus2.setName("p_menus2");
          p_menus2.setLayout(new BorderLayout());

          //======== menus_bas ========
          {
            menus_bas.setOpaque(false);
            menus_bas.setBackground(new Color(238, 239, 241));
            menus_bas.setName("menus_bas");
            menus_bas.setLayout(new VerticalLayout());

            //======== navig_erreurs ========
            {
              navig_erreurs.setName("navig_erreurs");

              //---- bouton_erreurs ----
              bouton_erreurs.setText("Erreurs");
              bouton_erreurs.setToolTipText("Erreurs");
              bouton_erreurs.setName("bouton_erreurs");
              navig_erreurs.add(bouton_erreurs);
            }
            menus_bas.add(navig_erreurs);

            //======== navig_valid ========
            {
              navig_valid.setName("navig_valid");

              //---- bouton_valider ----
              bouton_valider.setText("Valider");
              bouton_valider.setToolTipText("Valider");
              bouton_valider.setName("bouton_valider");
              bouton_valider.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bouton_validerActionPerformed(e);
                }
              });
              navig_valid.add(bouton_valider);
            }
            menus_bas.add(navig_valid);

            //======== navig_retour ========
            {
              navig_retour.setName("navig_retour");

              //---- bouton_retour ----
              bouton_retour.setText("Retour");
              bouton_retour.setToolTipText("Retour");
              bouton_retour.setName("bouton_retour");
              bouton_retour.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bouton_retourActionPerformed(e);
                }
              });
              navig_retour.add(bouton_retour);
            }
            menus_bas.add(navig_retour);
          }
          p_menus2.add(menus_bas, BorderLayout.SOUTH);

          //======== scroll_droite ========
          {
            scroll_droite.setBackground(new Color(238, 239, 241));
            scroll_droite.setPreferredSize(new Dimension(16, 520));
            scroll_droite.setBorder(null);
            scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            scroll_droite.setName("scroll_droite");

            //======== menus_haut ========
            {
              menus_haut.setMinimumSize(new Dimension(160, 520));
              menus_haut.setPreferredSize(new Dimension(160, 520));
              menus_haut.setBackground(new Color(238, 239, 241));
              menus_haut.setAutoscrolls(true);
              menus_haut.setName("menus_haut");
              menus_haut.setLayout(new VerticalLayout());

              //======== riMenu_V01F ========
              {
                riMenu_V01F.setMinimumSize(new Dimension(104, 50));
                riMenu_V01F.setPreferredSize(new Dimension(170, 50));
                riMenu_V01F.setMaximumSize(new Dimension(104, 50));
                riMenu_V01F.setName("riMenu_V01F");

                //---- riMenu_bt_V01F ----
                riMenu_bt_V01F.setText("@V01F@");
                riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
                riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
                riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
                riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
                riMenu_bt_V01F.setName("riMenu_bt_V01F");
                riMenu_V01F.add(riMenu_bt_V01F);
              }
              menus_haut.add(riMenu_V01F);

              //======== riSousMenu_consult ========
              {
                riSousMenu_consult.setName("riSousMenu_consult");

                //---- riSousMenu_bt_consult ----
                riSousMenu_bt_consult.setText("Consultation");
                riSousMenu_bt_consult.setToolTipText("Consultation");
                riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
                riSousMenu_consult.add(riSousMenu_bt_consult);
              }
              menus_haut.add(riSousMenu_consult);

              //======== riSousMenu_modif ========
              {
                riSousMenu_modif.setName("riSousMenu_modif");

                //---- riSousMenu_bt_modif ----
                riSousMenu_bt_modif.setText("Modification");
                riSousMenu_bt_modif.setToolTipText("Modification");
                riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
                riSousMenu_modif.add(riSousMenu_bt_modif);
              }
              menus_haut.add(riSousMenu_modif);

              //======== riSousMenu_crea ========
              {
                riSousMenu_crea.setName("riSousMenu_crea");

                //---- riSousMenu_bt_crea ----
                riSousMenu_bt_crea.setText("Cr\u00e9ation");
                riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
                riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
                riSousMenu_crea.add(riSousMenu_bt_crea);
              }
              menus_haut.add(riSousMenu_crea);

              //======== riSousMenu_suppr ========
              {
                riSousMenu_suppr.setName("riSousMenu_suppr");

                //---- riSousMenu_bt_suppr ----
                riSousMenu_bt_suppr.setText("Annulation");
                riSousMenu_bt_suppr.setToolTipText("Annulation");
                riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
                riSousMenu_suppr.add(riSousMenu_bt_suppr);
              }
              menus_haut.add(riSousMenu_suppr);

              //======== riSousMenuF_dupli ========
              {
                riSousMenuF_dupli.setName("riSousMenuF_dupli");

                //---- riSousMenu_bt_dupli ----
                riSousMenu_bt_dupli.setText("Duplication");
                riSousMenu_bt_dupli.setToolTipText("Duplication");
                riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
                riSousMenuF_dupli.add(riSousMenu_bt_dupli);
              }
              menus_haut.add(riSousMenuF_dupli);

              //======== riSousMenu_rappel ========
              {
                riSousMenu_rappel.setName("riSousMenu_rappel");

                //---- riSousMenu_bt_rappel ----
                riSousMenu_bt_rappel.setText("Rappel");
                riSousMenu_bt_rappel.setToolTipText("Rappel");
                riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
                riSousMenu_rappel.add(riSousMenu_bt_rappel);
              }
              menus_haut.add(riSousMenu_rappel);

              //======== riSousMenu_reac ========
              {
                riSousMenu_reac.setName("riSousMenu_reac");

                //---- riSousMenu_bt_reac ----
                riSousMenu_bt_reac.setText("R\u00e9activation");
                riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
                riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
                riSousMenu_reac.add(riSousMenu_bt_reac);
              }
              menus_haut.add(riSousMenu_reac);

              //======== riSousMenu_destr ========
              {
                riSousMenu_destr.setName("riSousMenu_destr");

                //---- riSousMenu_bt_destr ----
                riSousMenu_bt_destr.setText("Suppression");
                riSousMenu_bt_destr.setToolTipText("Suppression");
                riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
                riSousMenu_destr.add(riSousMenu_bt_destr);
              }
              menus_haut.add(riSousMenu_destr);

              //======== riMenu2 ========
              {
                riMenu2.setName("riMenu2");

                //---- riMenu_bt2 ----
                riMenu_bt2.setText("Options");
                riMenu_bt2.setName("riMenu_bt2");
                riMenu2.add(riMenu_bt2);
              }
              menus_haut.add(riMenu2);

              //======== riSousMenu6 ========
              {
                riSousMenu6.setName("riSousMenu6");

                //---- riSousMenu_bt6 ----
                riSousMenu_bt6.setText("Tous droits sur tout");
                riSousMenu_bt6.setToolTipText("Tous droits sur tout");
                riSousMenu_bt6.setName("riSousMenu_bt6");
                riSousMenu_bt6.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riSousMenu_bt6ActionPerformed(e);
                  }
                });
                riSousMenu6.add(riSousMenu_bt6);
              }
              menus_haut.add(riSousMenu6);

              //======== riSousMenu7 ========
              {
                riSousMenu7.setName("riSousMenu7");

                //---- riSousMenu_bt7 ----
                riSousMenu_bt7.setText("Valider les modifications");
                riSousMenu_bt7.setToolTipText("Valider les modifications");
                riSousMenu_bt7.setName("riSousMenu_bt7");
                riSousMenu_bt7.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riSousMenu_bt7ActionPerformed(e);
                  }
                });
                riSousMenu7.add(riSousMenu_bt7);
              }
              menus_haut.add(riSousMenu7);
            }
            scroll_droite.setViewportView(menus_haut);
          }
          p_menus2.add(scroll_droite, BorderLayout.NORTH);
        }
        pnlMenus.add(p_menus2, BorderLayout.CENTER);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);

      //======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(1000, 600));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);

        //======== pnlBas ========
        {
          pnlBas.setOpaque(false);
          pnlBas.setName("pnlBas");
          pnlBas.setLayout(null);

          //---- V06F1 ----
          V06F1.setComponentPopupMenu(BTD);
          V06F1.setName("V06F1");
          pnlBas.add(V06F1);
          V06F1.setBounds(220, 0, 25, V06F1.getPreferredSize().height);

          //---- V06F ----
          V06F.setComponentPopupMenu(BTD);
          V06F.setName("V06F");
          pnlBas.add(V06F);
          V06F.setBounds(220, 0, 25, V06F.getPreferredSize().height);

          //---- OBJ_69 ----
          OBJ_69.setText("Aller \u00e0 la page");
          OBJ_69.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_69.setName("OBJ_69");
          OBJ_69.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_69ActionPerformed(e);
            }
          });
          pnlBas.add(OBJ_69);
          OBJ_69.setBounds(new Rectangle(new Point(75, 0), OBJ_69.getPreferredSize()));

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < pnlBas.getComponentCount(); i++) {
              Rectangle bounds = pnlBas.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlBas.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlBas.setMinimumSize(preferredSize);
            pnlBas.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(pnlBas);
        pnlBas.setBounds(732, 560, 267, 34);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Objet de la s\u00e9curit\u00e9 (GVM et GAM)");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //======== pnlGauche ========
          {
            pnlGauche.setBorder(new TitledBorder(""));
            pnlGauche.setOpaque(false);
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(null);

            //---- OBJ_90 ----
            OBJ_90.setText("44");
            OBJ_90.setFont(OBJ_90.getFont().deriveFont(OBJ_90.getFont().getStyle() | Font.BOLD));
            OBJ_90.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_90.setName("OBJ_90");
            pnlGauche.add(OBJ_90);
            OBJ_90.setBounds(22, 51, 21, 26);

            //---- OBJ_121 ----
            OBJ_121.setText("45");
            OBJ_121.setFont(OBJ_121.getFont().deriveFont(OBJ_121.getFont().getStyle() | Font.BOLD));
            OBJ_121.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_121.setName("OBJ_121");
            pnlGauche.add(OBJ_121);
            OBJ_121.setBounds(22, 82, 21, 26);

            //---- OBJ_122 ----
            OBJ_122.setText("46");
            OBJ_122.setFont(OBJ_122.getFont().deriveFont(OBJ_122.getFont().getStyle() | Font.BOLD));
            OBJ_122.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_122.setName("OBJ_122");
            pnlGauche.add(OBJ_122);
            OBJ_122.setBounds(22, 113, 21, 26);

            //---- OBJ_186 ----
            OBJ_186.setText("47");
            OBJ_186.setFont(OBJ_186.getFont().deriveFont(OBJ_186.getFont().getStyle() | Font.BOLD));
            OBJ_186.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_186.setName("OBJ_186");
            pnlGauche.add(OBJ_186);
            OBJ_186.setBounds(22, 144, 21, 26);

            //---- OBJ_187 ----
            OBJ_187.setText("48");
            OBJ_187.setFont(OBJ_187.getFont().deriveFont(OBJ_187.getFont().getStyle() | Font.BOLD));
            OBJ_187.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_187.setName("OBJ_187");
            pnlGauche.add(OBJ_187);
            OBJ_187.setBounds(22, 175, 21, 26);

            //---- OBJ_190 ----
            OBJ_190.setText("49");
            OBJ_190.setFont(OBJ_190.getFont().deriveFont(OBJ_190.getFont().getStyle() | Font.BOLD));
            OBJ_190.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_190.setName("OBJ_190");
            pnlGauche.add(OBJ_190);
            OBJ_190.setBounds(22, 206, 21, 26);

            //---- OBJ_194 ----
            OBJ_194.setText("51");
            OBJ_194.setFont(OBJ_194.getFont().deriveFont(OBJ_194.getFont().getStyle() | Font.BOLD));
            OBJ_194.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_194.setName("OBJ_194");
            pnlGauche.add(OBJ_194);
            OBJ_194.setBounds(22, 237, 21, 26);

            //---- OBJ_195 ----
            OBJ_195.setText("58");
            OBJ_195.setFont(OBJ_195.getFont().deriveFont(OBJ_195.getFont().getStyle() | Font.BOLD));
            OBJ_195.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_195.setName("OBJ_195");
            pnlGauche.add(OBJ_195);
            OBJ_195.setBounds(22, 268, 21, 26);

            //---- OBJ_196 ----
            OBJ_196.setText("59");
            OBJ_196.setFont(OBJ_196.getFont().deriveFont(OBJ_196.getFont().getStyle() | Font.BOLD));
            OBJ_196.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_196.setName("OBJ_196");
            pnlGauche.add(OBJ_196);
            OBJ_196.setBounds(22, 299, 21, 26);

            //---- OBJ_197 ----
            OBJ_197.setText("52");
            OBJ_197.setFont(OBJ_197.getFont().deriveFont(OBJ_197.getFont().getStyle() | Font.BOLD));
            OBJ_197.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_197.setName("OBJ_197");
            pnlGauche.add(OBJ_197);
            OBJ_197.setBounds(22, 330, 21, 26);

            //---- OBJ_198 ----
            OBJ_198.setText("53");
            OBJ_198.setFont(OBJ_198.getFont().deriveFont(OBJ_198.getFont().getStyle() | Font.BOLD));
            OBJ_198.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_198.setName("OBJ_198");
            pnlGauche.add(OBJ_198);
            OBJ_198.setBounds(22, 361, 21, 26);

            //---- OBJ_199 ----
            OBJ_199.setText("54");
            OBJ_199.setFont(OBJ_199.getFont().deriveFont(OBJ_199.getFont().getStyle() | Font.BOLD));
            OBJ_199.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_199.setName("OBJ_199");
            pnlGauche.add(OBJ_199);
            OBJ_199.setBounds(22, 392, 21, 26);

            //---- OBJ_81 ----
            OBJ_81.setText("43");
            OBJ_81.setFont(OBJ_81.getFont().deriveFont(OBJ_81.getFont().getStyle() | Font.BOLD));
            OBJ_81.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_81.setName("OBJ_81");
            pnlGauche.add(OBJ_81);
            OBJ_81.setBounds(20, 20, 21, 26);

            //---- SET058 ----
            SET058.setToolTipText("Valeur entre 0 et 9");
            SET058.setComponentPopupMenu(BTD);
            SET058.setText("Modification prix sur transfert  de stock");
            SET058.setName("SET058");
            pnlGauche.add(SET058);
            SET058.setBounds(52, 268, 405, 26);

            //---- OBJ_200 ----
            OBJ_200.setText("Gestion de mouvements de stock");
            OBJ_200.setName("OBJ_200");
            pnlGauche.add(OBJ_200);
            OBJ_200.setBounds(240, 20, 205, 26);

            //---- SET044 ----
            SET044.setComponentPopupMenu(BTD);
            SET044.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit",
              "Superviseur"
            }));
            SET044.setName("SET044");
            pnlGauche.add(SET044);
            SET044.setBounds(new Rectangle(new Point(52, 51), SET044.getPreferredSize()));

            //---- OBJ_120 ----
            OBJ_120.setText("Bordereaux d'entr\u00e9e en stock");
            OBJ_120.setName("OBJ_120");
            pnlGauche.add(OBJ_120);
            OBJ_120.setBounds(242, 51, 235, 26);

            //---- SET045 ----
            SET045.setComponentPopupMenu(BTD);
            SET045.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit",
              "Superviseur"
            }));
            SET045.setName("SET045");
            pnlGauche.add(SET045);
            SET045.setBounds(new Rectangle(new Point(52, 82), SET045.getPreferredSize()));

            //---- OBJ_123 ----
            OBJ_123.setText("Bordereaux de sortie de stock");
            OBJ_123.setName("OBJ_123");
            pnlGauche.add(OBJ_123);
            OBJ_123.setBounds(242, 82, 235, 26);

            //---- SET046 ----
            SET046.setComponentPopupMenu(BTD);
            SET046.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit",
              "Superviseur"
            }));
            SET046.setName("SET046");
            pnlGauche.add(SET046);
            SET046.setBounds(new Rectangle(new Point(52, 113), SET046.getPreferredSize()));

            //---- OBJ_124 ----
            OBJ_124.setText("Bordereaux de transfert de stock");
            OBJ_124.setName("OBJ_124");
            pnlGauche.add(OBJ_124);
            OBJ_124.setBounds(242, 113, 235, 26);

            //---- SET047 ----
            SET047.setComponentPopupMenu(BTD);
            SET047.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit",
              "Superviseur"
            }));
            SET047.setName("SET047");
            pnlGauche.add(SET047);
            SET047.setBounds(new Rectangle(new Point(52, 144), SET047.getPreferredSize()));

            //---- OBJ_125 ----
            OBJ_125.setText("Bordereaux d'Inventaire");
            OBJ_125.setName("OBJ_125");
            pnlGauche.add(OBJ_125);
            OBJ_125.setBounds(242, 144, 235, 26);

            //---- SET048 ----
            SET048.setComponentPopupMenu(BTD);
            SET048.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit",
              "Superviseur"
            }));
            SET048.setName("SET048");
            pnlGauche.add(SET048);
            SET048.setBounds(new Rectangle(new Point(52, 175), SET048.getPreferredSize()));

            //---- OBJ_126 ----
            OBJ_126.setText("Bordereaux d'op\u00e9rations diverses");
            OBJ_126.setName("OBJ_126");
            pnlGauche.add(OBJ_126);
            OBJ_126.setBounds(242, 175, 235, 26);

            //---- SET049 ----
            SET049.setComponentPopupMenu(BTD);
            SET049.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit",
              "Superviseur"
            }));
            SET049.setName("SET049");
            pnlGauche.add(SET049);
            SET049.setBounds(new Rectangle(new Point(52, 206), SET049.getPreferredSize()));

            //---- OBJ_127 ----
            OBJ_127.setText("Bordereaux de stocks mini/maxi");
            OBJ_127.setName("OBJ_127");
            pnlGauche.add(OBJ_127);
            OBJ_127.setBounds(242, 206, 235, 26);

            //---- SET051 ----
            SET051.setComponentPopupMenu(BTD);
            SET051.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit",
              "Superviseur"
            }));
            SET051.setName("SET051");
            pnlGauche.add(SET051);
            SET051.setBounds(new Rectangle(new Point(52, 237), SET051.getPreferredSize()));

            //---- OBJ_128 ----
            OBJ_128.setText("Bordereaux de d\u00e9pr\u00e9ciations");
            OBJ_128.setName("OBJ_128");
            pnlGauche.add(OBJ_128);
            OBJ_128.setBounds(242, 237, 235, 26);

            //---- SET059 ----
            SET059.setToolTipText("Valeur entre 0 et 9");
            SET059.setComponentPopupMenu(BTD);
            SET059.setText("Modification de prix sur inventaire");
            SET059.setName("SET059");
            pnlGauche.add(SET059);
            SET059.setBounds(52, 299, 405, 26);

            //---- SET052 ----
            SET052.setComponentPopupMenu(BTD);
            SET052.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "Droits HOM, REC et FAC",
              "Droits HOM et REC",
              "Droits FAC uniquement",
              "Droits ATT seulement"
            }));
            SET052.setName("SET052");
            pnlGauche.add(SET052);
            SET052.setBounds(52, 330, 210, SET052.getPreferredSize().height);

            //---- OBJ_201 ----
            OBJ_201.setText("Adresses de stockage");
            OBJ_201.setName("OBJ_201");
            pnlGauche.add(OBJ_201);
            OBJ_201.setBounds(267, 330, 205, 26);

            //---- SET053 ----
            SET053.setToolTipText("Valeur entre 0 et 9");
            SET053.setComponentPopupMenu(BTD);
            SET053.setText("Etats de stocks");
            SET053.setName("SET053");
            pnlGauche.add(SET053);
            SET053.setBounds(52, 361, 405, 26);

            //---- SET054 ----
            SET054.setToolTipText("Valeur entre 0 et 9");
            SET054.setComponentPopupMenu(BTD);
            SET054.setText("Chiffrages de stocks");
            SET054.setName("SET054");
            pnlGauche.add(SET054);
            SET054.setBounds(52, 392, 405, 26);

            //---- SET043 ----
            SET043.setComponentPopupMenu(BTD);
            SET043.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit",
              "Superviseur"
            }));
            SET043.setName("SET043");
            pnlGauche.add(SET043);
            SET043.setBounds(52, 20, 180, SET043.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < pnlGauche.getComponentCount(); i++) {
                Rectangle bounds = pnlGauche.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlGauche.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlGauche.setMinimumSize(preferredSize);
              pnlGauche.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel1ContentContainer.add(pnlGauche);
          pnlGauche.setBounds(10, 55, 470, 450);

          //======== pnlDroite ========
          {
            pnlDroite.setBorder(new TitledBorder(""));
            pnlDroite.setOpaque(false);
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(null);

            //---- OBJ_85 ----
            OBJ_85.setText("167");
            OBJ_85.setFont(OBJ_85.getFont().deriveFont(OBJ_85.getFont().getStyle() | Font.BOLD));
            OBJ_85.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_85.setName("OBJ_85");
            pnlDroite.add(OBJ_85);
            OBJ_85.setBounds(10, 20, 31, 28);

            //---- SET167 ----
            SET167.setComponentPopupMenu(BTD);
            SET167.setName("SET167");
            pnlDroite.add(SET167);
            SET167.setBounds(52, 20, 20, SET167.getPreferredSize().height);

            //---- OBJ_155 ----
            OBJ_155.setText("Modification date traitement");
            OBJ_155.setName("OBJ_155");
            pnlDroite.add(OBJ_155);
            OBJ_155.setBounds(84, 20, 385, 28);

            //---- OBJ_99 ----
            OBJ_99.setText("176");
            OBJ_99.setFont(OBJ_99.getFont().deriveFont(OBJ_99.getFont().getStyle() | Font.BOLD));
            OBJ_99.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_99.setName("OBJ_99");
            pnlDroite.add(OBJ_99);
            OBJ_99.setBounds(10, 52, 31, 23);

            //---- SET176 ----
            SET176.setComponentPopupMenu(BTD);
            SET176.setModel(new DefaultComboBoxModel(new String[] {
              "Magasin principal",
              "Magasin associ\u00e9 \u00e0 l'utilisateur",
              "Cumul de tous les magasins"
            }));
            SET176.setName("SET176");
            pnlDroite.add(SET176);
            SET176.setBounds(52, 50, 210, SET176.getPreferredSize().height);

            //---- OBJ_202 ----
            OBJ_202.setText("Affichage des stocks en mobilit\u00e9");
            OBJ_202.setName("OBJ_202");
            pnlDroite.add(OBJ_202);
            OBJ_202.setBounds(270, 50, 195, 26);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < pnlDroite.getComponentCount(); i++) {
                Rectangle bounds = pnlDroite.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlDroite.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlDroite.setMinimumSize(preferredSize);
              pnlDroite.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel1ContentContainer.add(pnlDroite);
          pnlDroite.setBounds(490, 55, 470, 450);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(15, 10, 974, 550);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel pnlNord;
    private SNBandeauTitre p_bpresentation;
    private JMenuBar barre_tete;
    private JPanel p_tete_gauche;
    private JLabel OBJ_34;
    private RiZoneSortie INDUSR;
    private JLabel OBJ_36;
    private RiZoneSortie INDETB;
    private RiZoneSortie OBJ_40;
    private JPanel p_tete_droite;
    private JLabel OBJ_96;
    private JLabel OBJ_95;
    private SNPanelFond pnlSud;
    private JPanel pnlMenus;
    private JPanel p_menus2;
    private JPanel menus_bas;
    private RiMenu navig_erreurs;
    private RiMenu_bt bouton_erreurs;
    private RiMenu navig_valid;
    private RiMenu_bt bouton_valider;
    private RiMenu navig_retour;
    private RiMenu_bt bouton_retour;
    private JScrollPane scroll_droite;
    private JPanel menus_haut;
    private RiMenu riMenu_V01F;
    private RiMenu_bt riMenu_bt_V01F;
    private RiSousMenu riSousMenu_consult;
    private RiSousMenu_bt riSousMenu_bt_consult;
    private RiSousMenu riSousMenu_modif;
    private RiSousMenu_bt riSousMenu_bt_modif;
    private RiSousMenu riSousMenu_crea;
    private RiSousMenu_bt riSousMenu_bt_crea;
    private RiSousMenu riSousMenu_suppr;
    private RiSousMenu_bt riSousMenu_bt_suppr;
    private RiSousMenu riSousMenuF_dupli;
    private RiSousMenu_bt riSousMenu_bt_dupli;
    private RiSousMenu riSousMenu_rappel;
    private RiSousMenu_bt riSousMenu_bt_rappel;
    private RiSousMenu riSousMenu_reac;
    private RiSousMenu_bt riSousMenu_bt_reac;
    private RiSousMenu riSousMenu_destr;
    private RiSousMenu_bt riSousMenu_bt_destr;
    private RiMenu riMenu2;
    private RiMenu_bt riMenu_bt2;
    private RiSousMenu riSousMenu6;
    private RiSousMenu_bt riSousMenu_bt6;
    private RiSousMenu riSousMenu7;
    private RiSousMenu_bt riSousMenu_bt7;
    private SNPanelContenu pnlContenu;
    private SNPanel pnlBas;
    private XRiTextField V06F1;
    private XRiTextField V06F;
    private SNBoutonLeger OBJ_69;
    private JXTitledPanel xTitledPanel1;
    private SNPanel pnlGauche;
    private JLabel OBJ_90;
    private JLabel OBJ_121;
    private JLabel OBJ_122;
    private JLabel OBJ_186;
    private JLabel OBJ_187;
    private JLabel OBJ_190;
    private JLabel OBJ_194;
    private JLabel OBJ_195;
    private JLabel OBJ_196;
    private JLabel OBJ_197;
    private JLabel OBJ_198;
    private JLabel OBJ_199;
    private JLabel OBJ_81;
    private XRiCheckBox SET058;
    private JLabel OBJ_200;
    private XRiComboBox SET044;
    private JLabel OBJ_120;
    private XRiComboBox SET045;
    private JLabel OBJ_123;
    private XRiComboBox SET046;
    private JLabel OBJ_124;
    private XRiComboBox SET047;
    private JLabel OBJ_125;
    private XRiComboBox SET048;
    private JLabel OBJ_126;
    private XRiComboBox SET049;
    private JLabel OBJ_127;
    private XRiComboBox SET051;
    private JLabel OBJ_128;
    private XRiCheckBox SET059;
    private XRiComboBox SET052;
    private JLabel OBJ_201;
    private XRiCheckBox SET053;
    private XRiCheckBox SET054;
    private XRiComboBox SET043;
    private SNPanel pnlDroite;
    private JLabel OBJ_85;
    private XRiTextField SET167;
    private JLabel OBJ_155;
    private JLabel OBJ_99;
    private XRiComboBox SET176;
    private JLabel OBJ_202;
    private JPopupMenu BTD;
    private JMenuItem OBJ_16;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
