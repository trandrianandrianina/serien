
package ri.serien.libecranrpg.vgvx.VGVXABFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVXABFM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", };
  private String[] _WTP01_Title = { "Message", };
  private String[][] _WTP01_Data =
      { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", }, { "LD09", }, { "LD10", }, };
  private int[] _WTP01_Width = { 545, };
  
  public VGVXABFM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    REPONS.setValeurs("0", REPONS_GRP);
    REPONS_REFUS.setValeurs("1");
    REPONS_ATT.setValeurs("2");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // OBJ_18.setEnabled( lexique.isPresent("REPONS"));
    // OBJ_18.setSelected(lexique.HostFieldGetData("REPONS").equalsIgnoreCase("1"));
    // OBJ_19.setEnabled( lexique.isPresent("REPONS"));
    // OBJ_19.setSelected(lexique.HostFieldGetData("REPONS").equalsIgnoreCase("2"));
    // OBJ_13.setEnabled( lexique.isPresent("REPONS"));
    // OBJ_13.setSelected(lexique.HostFieldGetData("REPONS").equalsIgnoreCase("0"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (OBJ_18.isSelected())
    // lexique.HostFieldPutData("REPONS", 0, "1");
    // if (OBJ_19.isSelected())
    // lexique.HostFieldPutData("REPONS", 0, "2");
    // if (OBJ_13.isSelected())
    // lexique.HostFieldPutData("REPONS", 0, "0");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "E", "Enter");
    WTP01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "A", "Enter");
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvxab"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    REPONS = new XRiRadioButton();
    REPONS_ATT = new XRiRadioButton();
    REPONS_REFUS = new XRiRadioButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    REPONS_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- WTP01 ----
            WTP01.setComponentPopupMenu(BTD);
            WTP01.setName("WTP01");
            WTP01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(WTP01);
          }
          p_recup.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(20, 25, 562, 190);

          //---- REPONS ----
          REPONS.setText("Autorisation");
          REPONS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPONS.setName("REPONS");
          p_recup.add(REPONS);
          REPONS.setBounds(25, 230, 105, 20);

          //---- REPONS_ATT ----
          REPONS_ATT.setText("Attente");
          REPONS_ATT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPONS_ATT.setName("REPONS_ATT");
          p_recup.add(REPONS_ATT);
          REPONS_ATT.setBounds(200, 230, 74, 20);

          //---- REPONS_REFUS ----
          REPONS_REFUS.setText("Refus");
          REPONS_REFUS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPONS_REFUS.setName("REPONS_REFUS");
          p_recup.add(REPONS_REFUS);
          REPONS_REFUS.setBounds(130, 230, 70, 20);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          p_recup.add(BT_PGUP);
          BT_PGUP.setBounds(585, 25, 25, 88);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          p_recup.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(585, 125, 25, 88);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_recup.getComponentCount(); i++) {
              Rectangle bounds = p_recup.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_recup.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_recup.setMinimumSize(preferredSize);
            p_recup.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 635, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_5 ----
      OBJ_5.setText("Erreur n\u00e9cessitant autorisation");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Autorisation accord\u00e9e");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD.add(OBJ_6);
      BTD.addSeparator();

      //---- OBJ_8 ----
      OBJ_8.setText("Choix possibles");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      BTD.add(OBJ_8);

      //---- OBJ_7 ----
      OBJ_7.setText("Aide en ligne");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      BTD.add(OBJ_7);
    }

    //---- REPONS_GRP ----
    REPONS_GRP.add(REPONS);
    REPONS_GRP.add(REPONS_ATT);
    REPONS_GRP.add(REPONS_REFUS);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private XRiRadioButton REPONS;
  private XRiRadioButton REPONS_ATT;
  private XRiRadioButton REPONS_REFUS;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_7;
  private ButtonGroup REPONS_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
