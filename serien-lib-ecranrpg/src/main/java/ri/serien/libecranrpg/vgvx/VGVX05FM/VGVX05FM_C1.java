
package ri.serien.libecranrpg.vgvx.VGVX05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVX05FM_C1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] A1TDUO_Value = { "", "J", "S", "M", "1", "2", "3" };
  private String[] dimensions_Value = { "", "2", "3" };
  private String[] dimensions_Libelle = { "m", "cm", "mm" };
  
  /**
   * Constructeur.
   */
  public VGVX05FM_C1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    A1TDUO.setValeurs(A1TDUO_Value, null);
    A1IN15.setValeursSelection("1", " ");
    A1IN23.setValeursSelection("1", " ");
    A1STK.setValeursSelection("1", " ");
    A1IN28.setValeurs(dimensions_Value, dimensions_Libelle);
    A1IN29.setValeurs(dimensions_Value, dimensions_Libelle);
    A1IN30.setValeurs(dimensions_Value, dimensions_Libelle);
    A1IN32.setValeursSelection("1", " ");
    
    // Titre
    setTitle("Stocks");
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNPD@")).trim());
    OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNPDN@")).trim());
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNVO@")).trim());
    A1UNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNV@")).trim());
    OBJ_104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBUSC@")).trim());
    OBJ_105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBUCR@")).trim());
    ULBUCS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBUCS@")).trim());
    WA1UNL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WA1UNL@")).trim());
    A1UNS2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    OBJ_135.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNPD2@")).trim());
    OBJ_137.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNVO2@")).trim());
    A1UNVC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNV@")).trim());
    ULBUNL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBUNL@")).trim());
    OBJ_144.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@&M000529@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Gestion onglet
    if (lexique.getValeurVariableGlobale("ongletEnCours_VGVX05FM_C1") != null) {
      OBJ_31.setSelectedIndex((Integer) lexique.getValeurVariableGlobale("ongletEnCours_VGVX05FM_C1"));
    }
    
    navig_valid.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    OBJ_57.setVisible(A1MAG1.isVisible());
    
    // On n'affiche pas les zones pour dire combien d'unités de ventes par unités de stock si on a la même unité
    panel1.setVisible(!lexique.HostFieldGetData("A1UNS").trim().equalsIgnoreCase(lexique.HostFieldGetData("A1UNV")));
    OBJ_143_OBJ_144.setVisible(!lexique.HostFieldGetData("LIBKSV").trim().isEmpty());
    A1KSVX.setVisible(!lexique.HostFieldGetData("LIBKSV").trim().isEmpty());
    A1UNV.setVisible(!lexique.HostFieldGetData("LIBKSV").trim().isEmpty());
    labelStock.setVisible(lexique.HostFieldGetData("A1STK").trim().equals("1"));
    A1STK.setVisible(lexique.getMode() == Lexical.MODE_MODIFICATION);
    // Protection si autre chose que 1 dans A1IN32
    A1IN32.setSelected(!lexique.HostFieldGetData("A1IN32").trim().isEmpty());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Gestion onglet
    lexique.addVariableGlobale("ongletEnCours_VGVX05FM_C1", OBJ_31.getSelectedIndex());
  }
  
  private void riSousMenu_bt6ActionPerformed() {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt7ActionPerformed() {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt8ActionPerformed() {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(4, 70);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void miChoixPossibleActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void miAideEnLigneActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "enter");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_31 = new JTabbedPane();
    STK2 = new JPanel();
    A1UNS = new XRiTextField();
    ULBUNS = new XRiTextField();
    A1PDS = new XRiTextField();
    A1COL = new XRiTextField();
    A1PDN = new XRiTextField();
    A1VOL = new XRiTextField();
    A1ABC = new XRiTextField();
    A1MAG1 = new XRiTextField();
    ULBABC = new XRiTextField();
    A1IN23 = new XRiCheckBox();
    OBJ_44 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_46 = new RiZoneSortie();
    OBJ_48 = new RiZoneSortie();
    OBJ_50 = new JLabel();
    OBJ_54 = new RiZoneSortie();
    OBJ_56 = new JLabel();
    OBJ_57 = new JLabel();
    panel9 = new JPanel();
    OBJ_61 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_67 = new JLabel();
    A1DUO1 = new XRiTextField();
    A1DUO2 = new XRiTextField();
    A1DUO3 = new XRiTextField();
    A1TDUO = new XRiComboBox();
    A1STK = new XRiCheckBox();
    panel1 = new JPanel();
    OBJ_143_OBJ_144 = new JLabel();
    A1KSVX = new XRiTextField();
    A1UNV = new RiZoneSortie();
    A1REF1 = new XRiTextField();
    OBJ_58 = new JLabel();
    A1IN32 = new XRiCheckBox();
    CND2 = new JPanel();
    panel6 = new JPanel();
    panel7 = new JPanel();
    OBJ_104 = new RiZoneSortie();
    OBJ_111 = new JLabel();
    X1USC = new XRiTextField();
    X1KSC = new XRiTextField();
    panel10 = new JPanel();
    OBJ_105 = new RiZoneSortie();
    OBJ_113 = new JLabel();
    X1UCR = new XRiTextField();
    X1KCR = new XRiTextField();
    OBJ_109 = new JLabel();
    OBJ_117 = new JLabel();
    X1PPC = new XRiTextField();
    X1PPL = new XRiTextField();
    X1PPP = new XRiTextField();
    WCLPL = new XRiTextField();
    OBJ_115 = new JLabel();
    OBJ_120 = new JLabel();
    OBJ_121 = new JLabel();
    OBJ_119 = new JLabel();
    X1CNS = new XRiTextField();
    panel2 = new JPanel();
    ULBUCS = new RiZoneSortie();
    A1UCS = new XRiTextField();
    A1KCS = new XRiTextField();
    WA1UNL = new RiZoneSortie();
    WQTUNL = new XRiTextField();
    label1 = new JLabel();
    A1UNS2 = new RiZoneSortie();
    panel11 = new JPanel();
    OBJ_123 = new JLabel();
    OBJ_126 = new JLabel();
    A1CNDX = new XRiTextField();
    A1UNL = new XRiTextField();
    A1PDSL = new XRiTextField();
    A1VOLL = new XRiTextField();
    OBJ_135 = new JLabel();
    OBJ_137 = new JLabel();
    OBJ_124 = new JLabel();
    A1UNVC = new RiZoneSortie();
    ULBUNL = new RiZoneSortie();
    TPO2 = new JPanel();
    A1KTS = new XRiTextField();
    A1UNT = new XRiTextField();
    ULBUNT = new XRiTextField();
    A1RGC = new XRiTextField();
    A1IN15 = new XRiCheckBox();
    OBJ_147 = new JLabel();
    OBJ_142 = new JLabel();
    OBJ_144 = new RiZoneSortie();
    OBJ_143 = new JLabel();
    panel3 = new JPanel();
    A1LNG = new XRiTextField();
    A1IN28 = new XRiComboBox();
    OBJ_154 = new JLabel();
    A1LRG = new XRiTextField();
    A1IN29 = new XRiComboBox();
    OBJ_156 = new JLabel();
    A1HTR = new XRiTextField();
    A1IN30 = new XRiComboBox();
    OBJ_157 = new JLabel();
    WVOL = new XRiTextField();
    labelStock = new JLabel();
    BTD = new JPopupMenu();
    miChoixPossible = new JMenuItem();
    miAideEnLigne = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(900, 500));
    setPreferredSize(new Dimension(870, 460));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setMinimumSize(new Dimension(850, 700));
      p_principal.setPreferredSize(new Dimension(850, 700));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 350));
            menus_haut.setPreferredSize(new Dimension(160, 350));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt1 ----
              riMenu_bt1.setText("@V01F@");
              riMenu_bt1.setPreferredSize(new Dimension(167, 50));
              riMenu_bt1.setMinimumSize(new Dimension(167, 50));
              riMenu_bt1.setMaximumSize(new Dimension(170, 50));
              riMenu_bt1.setFont(riMenu_bt1.getFont().deriveFont(riMenu_bt1.getFont().getSize() + 2f));
              riMenu_bt1.setName("riMenu_bt1");
              riMenu_V01F.add(riMenu_bt1);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Stocks par magasin");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed();
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Synth\u00e8se des unit\u00e9s");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed();
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Stock autres \u00e9tablissem.");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed();
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Gestion stocks minimums");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(600, 520));
        p_contenu.setMinimumSize(new Dimension(600, 520));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== OBJ_31 ========
        {
          OBJ_31.setPreferredSize(new Dimension(660, 450));
          OBJ_31.setName("OBJ_31");
          
          // ======== STK2 ========
          {
            STK2.setMinimumSize(new Dimension(500, 300));
            STK2.setOpaque(false);
            STK2.setName("STK2");
            STK2.setLayout(null);
            
            // ---- A1UNS ----
            A1UNS.setName("A1UNS");
            STK2.add(A1UNS);
            A1UNS.setBounds(130, 16, 34, A1UNS.getPreferredSize().height);
            
            // ---- ULBUNS ----
            ULBUNS.setName("ULBUNS");
            STK2.add(ULBUNS);
            ULBUNS.setBounds(170, 16, 112, ULBUNS.getPreferredSize().height);
            
            // ---- A1PDS ----
            A1PDS.setName("A1PDS");
            STK2.add(A1PDS);
            A1PDS.setBounds(130, 60, 74, A1PDS.getPreferredSize().height);
            
            // ---- A1COL ----
            A1COL.setName("A1COL");
            STK2.add(A1COL);
            A1COL.setBounds(590, 60, 34, A1COL.getPreferredSize().height);
            
            // ---- A1PDN ----
            A1PDN.setName("A1PDN");
            STK2.add(A1PDN);
            A1PDN.setBounds(130, 92, 74, A1PDN.getPreferredSize().height);
            
            // ---- A1VOL ----
            A1VOL.setName("A1VOL");
            STK2.add(A1VOL);
            A1VOL.setBounds(130, 124, 74, A1VOL.getPreferredSize().height);
            
            // ---- A1ABC ----
            A1ABC.setComponentPopupMenu(BTD);
            A1ABC.setName("A1ABC");
            STK2.add(A1ABC);
            A1ABC.setBounds(130, 165, 24, A1ABC.getPreferredSize().height);
            
            // ---- A1MAG1 ----
            A1MAG1.setComponentPopupMenu(BTD);
            A1MAG1.setName("A1MAG1");
            STK2.add(A1MAG1);
            A1MAG1.setBounds(130, 229, 34, A1MAG1.getPreferredSize().height);
            
            // ---- ULBABC ----
            ULBABC.setComponentPopupMenu(BTD);
            ULBABC.setName("ULBABC");
            STK2.add(ULBABC);
            ULBABC.setBounds(170, 165, 310, ULBABC.getPreferredSize().height);
            
            // ---- A1IN23 ----
            A1IN23.setText("Gestion d\u00e9positaire");
            A1IN23.setComponentPopupMenu(BTD);
            A1IN23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN23.setName("A1IN23");
            STK2.add(A1IN23);
            A1IN23.setBounds(25, 360, 142, 20);
            
            // ---- OBJ_44 ----
            OBJ_44.setText("Nombre de colis");
            OBJ_44.setHorizontalAlignment(SwingConstants.LEFT);
            OBJ_44.setName("OBJ_44");
            STK2.add(OBJ_44);
            OBJ_44.setBounds(475, 64, 105, 20);
            
            // ---- OBJ_53 ----
            OBJ_53.setText("Classe article");
            OBJ_53.setHorizontalAlignment(SwingConstants.LEFT);
            OBJ_53.setName("OBJ_53");
            STK2.add(OBJ_53);
            OBJ_53.setBounds(25, 169, 100, 20);
            
            // ---- OBJ_35 ----
            OBJ_35.setText("Unit\u00e9 de stock");
            OBJ_35.setName("OBJ_35");
            STK2.add(OBJ_35);
            OBJ_35.setBounds(25, 20, 97, 20);
            
            // ---- OBJ_41 ----
            OBJ_41.setText("Poids Brut");
            OBJ_41.setName("OBJ_41");
            STK2.add(OBJ_41);
            OBJ_41.setBounds(25, 64, 97, 20);
            
            // ---- OBJ_46 ----
            OBJ_46.setText("@UNPD@");
            OBJ_46.setName("OBJ_46");
            STK2.add(OBJ_46);
            OBJ_46.setBounds(210, 62, 90, OBJ_46.getPreferredSize().height);
            
            // ---- OBJ_48 ----
            OBJ_48.setText("@UNPDN@");
            OBJ_48.setName("OBJ_48");
            STK2.add(OBJ_48);
            OBJ_48.setBounds(210, 94, 90, OBJ_48.getPreferredSize().height);
            
            // ---- OBJ_50 ----
            OBJ_50.setText("Volume");
            OBJ_50.setName("OBJ_50");
            STK2.add(OBJ_50);
            OBJ_50.setBounds(25, 128, 97, 20);
            
            // ---- OBJ_54 ----
            OBJ_54.setText("@UNVO@");
            OBJ_54.setName("OBJ_54");
            STK2.add(OBJ_54);
            OBJ_54.setBounds(210, 126, 90, OBJ_54.getPreferredSize().height);
            
            // ---- OBJ_56 ----
            OBJ_56.setText("Poids Net");
            OBJ_56.setName("OBJ_56");
            STK2.add(OBJ_56);
            OBJ_56.setBounds(25, 96, 80, 20);
            
            // ---- OBJ_57 ----
            OBJ_57.setText("Magasin");
            OBJ_57.setName("OBJ_57");
            STK2.add(OBJ_57);
            OBJ_57.setBounds(25, 233, 73, 20);
            
            // ======== panel9 ========
            {
              panel9.setBorder(new TitledBorder("DLC ou DLUO"));
              panel9.setOpaque(false);
              panel9.setName("panel9");
              panel9.setLayout(null);
              
              // ---- OBJ_61 ----
              OBJ_61.setText("R\u00e9ception");
              OBJ_61.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_61.setName("OBJ_61");
              panel9.add(OBJ_61);
              OBJ_61.setBounds(395, 33, 65, 20);
              
              // ---- OBJ_65 ----
              OBJ_65.setText("Standard");
              OBJ_65.setName("OBJ_65");
              panel9.add(OBJ_65);
              OBJ_65.setBounds(275, 33, 58, 20);
              
              // ---- OBJ_66 ----
              OBJ_66.setText("D\u00e9lais en");
              OBJ_66.setName("OBJ_66");
              panel9.add(OBJ_66);
              OBJ_66.setBounds(20, 33, 85, 20);
              
              // ---- OBJ_67 ----
              OBJ_67.setText("Vente");
              OBJ_67.setName("OBJ_67");
              panel9.add(OBJ_67);
              OBJ_67.setBounds(550, 33, 38, 20);
              
              // ---- A1DUO1 ----
              A1DUO1.setName("A1DUO1");
              panel9.add(A1DUO1);
              A1DUO1.setBounds(335, 29, 28, A1DUO1.getPreferredSize().height);
              
              // ---- A1DUO2 ----
              A1DUO2.setName("A1DUO2");
              panel9.add(A1DUO2);
              A1DUO2.setBounds(465, 29, 28, A1DUO2.getPreferredSize().height);
              
              // ---- A1DUO3 ----
              A1DUO3.setName("A1DUO3");
              panel9.add(A1DUO3);
              A1DUO3.setBounds(595, 29, 28, A1DUO3.getPreferredSize().height);
              
              // ---- A1TDUO ----
              A1TDUO.setModel(new DefaultComboBoxModel(
                  new String[] { " ", "jours", "semaines", "mois", "+ 100 jours", "+ 200 jours", "+ 300 jours" }));
              A1TDUO.setComponentPopupMenu(BTD);
              A1TDUO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              A1TDUO.setName("A1TDUO");
              panel9.add(A1TDUO);
              A1TDUO.setBounds(105, 30, 120, A1TDUO.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel9.getComponentCount(); i++) {
                  Rectangle bounds = panel9.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel9.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel9.setMinimumSize(preferredSize);
                panel9.setPreferredSize(preferredSize);
              }
            }
            STK2.add(panel9);
            panel9.setBounds(15, 275, 640, 75);
            
            // ---- A1STK ----
            A1STK.setText("Article non g\u00e9r\u00e9 en stock");
            A1STK.setName("A1STK");
            STK2.add(A1STK);
            A1STK.setBounds(475, 15, 175, 30);
            
            // ======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);
              
              // ---- OBJ_143_OBJ_144 ----
              OBJ_143_OBJ_144.setText("de");
              OBJ_143_OBJ_144.setName("OBJ_143_OBJ_144");
              panel1.add(OBJ_143_OBJ_144);
              OBJ_143_OBJ_144.setBounds(5, 7, 30, 20);
              
              // ---- A1KSVX ----
              A1KSVX.setName("A1KSVX");
              panel1.add(A1KSVX);
              A1KSVX.setBounds(45, 3, 74, A1KSVX.getPreferredSize().height);
              
              // ---- A1UNV ----
              A1UNV.setComponentPopupMenu(BTD);
              A1UNV.setText("@A1UNV@");
              A1UNV.setToolTipText("?C1.A1UNV.toolTipText_3?");
              A1UNV.setName("A1UNV");
              panel1.add(A1UNV);
              A1UNV.setBounds(125, 5, 34, A1UNV.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            STK2.add(panel1);
            panel1.setBounds(285, 13, 175, 35);
            
            // ---- A1REF1 ----
            A1REF1.setName("A1REF1");
            STK2.add(A1REF1);
            A1REF1.setBounds(130, 197, 160, A1REF1.getPreferredSize().height);
            
            // ---- OBJ_58 ----
            OBJ_58.setText("Marque");
            OBJ_58.setName("OBJ_58");
            STK2.add(OBJ_58);
            OBJ_58.setBounds(25, 201, 73, 20);
            
            // ---- A1IN32 ----
            A1IN32.setText("Direct usine");
            A1IN32.setName("A1IN32");
            STK2.add(A1IN32);
            A1IN32.setBounds(475, 91, 175, 30);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < STK2.getComponentCount(); i++) {
                Rectangle bounds = STK2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = STK2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              STK2.setMinimumSize(preferredSize);
              STK2.setPreferredSize(preferredSize);
            }
          }
          OBJ_31.addTab("Stocks", STK2);
          
          // ======== CND2 ========
          {
            CND2.setOpaque(false);
            CND2.setName("CND2");
            CND2.setLayout(null);
            
            // ======== panel6 ========
            {
              panel6.setBorder(new TitledBorder("Conditionnement de stock"));
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);
              
              // ======== panel7 ========
              {
                panel7.setBorder(new TitledBorder("Sous conditionement de stock"));
                panel7.setOpaque(false);
                panel7.setName("panel7");
                panel7.setLayout(null);
                
                // ---- OBJ_104 ----
                OBJ_104.setText("@ULBUSC@");
                OBJ_104.setName("OBJ_104");
                panel7.add(OBJ_104);
                OBJ_104.setBounds(60, 32, 142, OBJ_104.getPreferredSize().height);
                
                // ---- OBJ_111 ----
                OBJ_111.setText("Nb US/USC");
                OBJ_111.setName("OBJ_111");
                panel7.add(OBJ_111);
                OBJ_111.setBounds(20, 65, 77, 19);
                
                // ---- X1USC ----
                X1USC.setComponentPopupMenu(BTD);
                X1USC.setName("X1USC");
                panel7.add(X1USC);
                X1USC.setBounds(20, 30, 34, X1USC.getPreferredSize().height);
                
                // ---- X1KSC ----
                X1KSC.setName("X1KSC");
                panel7.add(X1KSC);
                X1KSC.setBounds(121, 60, 82, X1KSC.getPreferredSize().height);
              }
              panel6.add(panel7);
              panel7.setBounds(15, 100, 230, 105);
              
              // ======== panel10 ========
              {
                panel10.setBorder(new TitledBorder("Unit\u00e9 de r\u00e9approvisionnement transtockeur"));
                panel10.setOpaque(false);
                panel10.setName("panel10");
                panel10.setLayout(null);
                
                // ---- OBJ_105 ----
                OBJ_105.setText("@ULBUCR@");
                OBJ_105.setName("OBJ_105");
                panel10.add(OBJ_105);
                OBJ_105.setBounds(60, 32, 142, OBJ_105.getPreferredSize().height);
                
                // ---- OBJ_113 ----
                OBJ_113.setText("Nb US/UCR");
                OBJ_113.setName("OBJ_113");
                panel10.add(OBJ_113);
                OBJ_113.setBounds(20, 65, 77, 19);
                
                // ---- X1UCR ----
                X1UCR.setComponentPopupMenu(BTD);
                X1UCR.setName("X1UCR");
                panel10.add(X1UCR);
                X1UCR.setBounds(20, 30, 34, X1UCR.getPreferredSize().height);
                
                // ---- X1KCR ----
                X1KCR.setName("X1KCR");
                panel10.add(X1KCR);
                X1KCR.setBounds(121, 60, 82, X1KCR.getPreferredSize().height);
              }
              panel6.add(panel10);
              panel10.setBounds(255, 100, 295, 105);
              
              // ---- OBJ_109 ----
              OBJ_109.setText("Plan de palettisation");
              OBJ_109.setName("OBJ_109");
              panel6.add(OBJ_109);
              OBJ_109.setBounds(20, 215, 124, 20);
              
              // ---- OBJ_117 ----
              OBJ_117.setText("CL/PL");
              OBJ_117.setName("OBJ_117");
              panel6.add(OBJ_117);
              OBJ_117.setBounds(454, 215, 77, 19);
              
              // ---- X1PPC ----
              X1PPC.setName("X1PPC");
              panel6.add(X1PPC);
              X1PPC.setBounds(195, 210, 26, X1PPC.getPreferredSize().height);
              
              // ---- X1PPL ----
              X1PPL.setName("X1PPL");
              panel6.add(X1PPL);
              X1PPL.setBounds(260, 210, 26, X1PPL.getPreferredSize().height);
              
              // ---- X1PPP ----
              X1PPP.setName("X1PPP");
              panel6.add(X1PPP);
              X1PPP.setBounds(325, 210, 26, X1PPP.getPreferredSize().height);
              
              // ---- WCLPL ----
              WCLPL.setName("WCLPL");
              panel6.add(WCLPL);
              WCLPL.setBounds(390, 210, 49, WCLPL.getPreferredSize().height);
              
              // ---- OBJ_115 ----
              OBJ_115.setText("x");
              OBJ_115.setName("OBJ_115");
              panel6.add(OBJ_115);
              OBJ_115.setBounds(236, 215, 9, 20);
              
              // ---- OBJ_120 ----
              OBJ_120.setText("x");
              OBJ_120.setName("OBJ_120");
              panel6.add(OBJ_120);
              OBJ_120.setBounds(301, 215, 9, 20);
              
              // ---- OBJ_121 ----
              OBJ_121.setText("=");
              OBJ_121.setName("OBJ_121");
              panel6.add(OBJ_121);
              OBJ_121.setBounds(366, 215, 9, 20);
              
              // ---- OBJ_119 ----
              OBJ_119.setText("Code condition de stockage");
              OBJ_119.setName("OBJ_119");
              panel6.add(OBJ_119);
              OBJ_119.setBounds(20, 249, 166, 20);
              
              // ---- X1CNS ----
              X1CNS.setName("X1CNS");
              panel6.add(X1CNS);
              X1CNS.setBounds(195, 245, 60, X1CNS.getPreferredSize().height);
              
              // ======== panel2 ========
              {
                panel2.setBorder(new TitledBorder("Nombre d'unit\u00e9s de stock par unit\u00e9 de conditionnement de stockage"));
                panel2.setOpaque(false);
                panel2.setName("panel2");
                panel2.setLayout(null);
                
                // ---- ULBUCS ----
                ULBUCS.setText("@ULBUCS@");
                ULBUCS.setName("ULBUCS");
                panel2.add(ULBUCS);
                ULBUCS.setBounds(20, 30, 110, ULBUCS.getPreferredSize().height);
                
                // ---- A1UCS ----
                A1UCS.setComponentPopupMenu(BTD);
                A1UCS.setName("A1UCS");
                panel2.add(A1UCS);
                A1UCS.setBounds(135, 28, 34, A1UCS.getPreferredSize().height);
                
                // ---- A1KCS ----
                A1KCS.setName("A1KCS");
                panel2.add(A1KCS);
                A1KCS.setBounds(171, 28, 82, A1KCS.getPreferredSize().height);
                
                // ---- WA1UNL ----
                WA1UNL.setText("@WA1UNL@");
                WA1UNL.setName("WA1UNL");
                panel2.add(WA1UNL);
                WA1UNL.setBounds(415, 30, 28, WA1UNL.getPreferredSize().height);
                
                // ---- WQTUNL ----
                WQTUNL.setName("WQTUNL");
                panel2.add(WQTUNL);
                WQTUNL.setBounds(450, 28, 59, WQTUNL.getPreferredSize().height);
                
                // ---- label1 ----
                label1.setText("conditionn\u00e9 en");
                label1.setName("label1");
                panel2.add(label1);
                label1.setBounds(305, 30, 110, 25);
                
                // ---- A1UNS2 ----
                A1UNS2.setComponentPopupMenu(BTD);
                A1UNS2.setText("@A1UNS@");
                A1UNS2.setToolTipText("?C1.A1UNS2.toolTipText_3?");
                A1UNS2.setName("A1UNS2");
                panel2.add(A1UNS2);
                A1UNS2.setBounds(255, 30, 34, A1UNS2.getPreferredSize().height);
                
                { // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < panel2.getComponentCount(); i++) {
                    Rectangle bounds = panel2.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel2.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel2.setMinimumSize(preferredSize);
                  panel2.setPreferredSize(preferredSize);
                }
              }
              panel6.add(panel2);
              panel2.setBounds(15, 30, 535, 75);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel6.getComponentCount(); i++) {
                  Rectangle bounds = panel6.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel6.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel6.setMinimumSize(preferredSize);
                panel6.setPreferredSize(preferredSize);
              }
            }
            CND2.add(panel6);
            panel6.setBounds(5, 110, 645, 290);
            
            // ======== panel11 ========
            {
              panel11.setBorder(new TitledBorder("Conditionnement de vente"));
              panel11.setOpaque(false);
              panel11.setName("panel11");
              panel11.setLayout(null);
              
              // ---- OBJ_123 ----
              OBJ_123.setText("Poids");
              OBJ_123.setName("OBJ_123");
              panel11.add(OBJ_123);
              OBJ_123.setBounds(405, 24, 45, 20);
              
              // ---- OBJ_126 ----
              OBJ_126.setText("Volume");
              OBJ_126.setName("OBJ_126");
              panel11.add(OBJ_126);
              OBJ_126.setBounds(405, 54, 50, 20);
              
              // ---- A1CNDX ----
              A1CNDX.setName("A1CNDX");
              panel11.add(A1CNDX);
              A1CNDX.setBounds(235, 35, 75, A1CNDX.getPreferredSize().height);
              
              // ---- A1UNL ----
              A1UNL.setComponentPopupMenu(BTD);
              A1UNL.setName("A1UNL");
              panel11.add(A1UNL);
              A1UNL.setBounds(10, 35, 30, A1UNL.getPreferredSize().height);
              
              // ---- A1PDSL ----
              A1PDSL.setName("A1PDSL");
              panel11.add(A1PDSL);
              A1PDSL.setBounds(455, 20, 74, A1PDSL.getPreferredSize().height);
              
              // ---- A1VOLL ----
              A1VOLL.setName("A1VOLL");
              panel11.add(A1VOLL);
              A1VOLL.setBounds(455, 50, 74, A1VOLL.getPreferredSize().height);
              
              // ---- OBJ_135 ----
              OBJ_135.setText("@UNPD2@");
              OBJ_135.setName("OBJ_135");
              panel11.add(OBJ_135);
              OBJ_135.setBounds(535, 24, 23, 20);
              
              // ---- OBJ_137 ----
              OBJ_137.setText("@UNVO2@");
              OBJ_137.setName("OBJ_137");
              panel11.add(OBJ_137);
              OBJ_137.setBounds(535, 54, 23, 20);
              
              // ---- OBJ_124 ----
              OBJ_124.setText("de");
              OBJ_124.setName("OBJ_124");
              panel11.add(OBJ_124);
              OBJ_124.setBounds(210, 39, 25, 20);
              
              // ---- A1UNVC ----
              A1UNVC.setComponentPopupMenu(BTD);
              A1UNVC.setText("@A1UNV@");
              A1UNVC.setName("A1UNVC");
              panel11.add(A1UNVC);
              A1UNVC.setBounds(315, 37, 30, A1UNVC.getPreferredSize().height);
              
              // ---- ULBUNL ----
              ULBUNL.setText("@ULBUNL@");
              ULBUNL.setName("ULBUNL");
              panel11.add(ULBUNL);
              ULBUNL.setBounds(45, 37, 150, ULBUNL.getPreferredSize().height);
            }
            CND2.add(panel11);
            panel11.setBounds(5, 10, 645, 90);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < CND2.getComponentCount(); i++) {
                Rectangle bounds = CND2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = CND2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              CND2.setMinimumSize(preferredSize);
              CND2.setPreferredSize(preferredSize);
            }
          }
          OBJ_31.addTab("Conditionnement", CND2);
          
          // ======== TPO2 ========
          {
            TPO2.setOpaque(false);
            TPO2.setName("TPO2");
            TPO2.setLayout(null);
            
            // ---- A1KTS ----
            A1KTS.setName("A1KTS");
            TPO2.add(A1KTS);
            A1KTS.setBounds(300, 68, 85, A1KTS.getPreferredSize().height);
            
            // ---- A1UNT ----
            A1UNT.setComponentPopupMenu(BTD);
            A1UNT.setName("A1UNT");
            TPO2.add(A1UNT);
            A1UNT.setBounds(300, 30, 34, A1UNT.getPreferredSize().height);
            
            // ---- ULBUNT ----
            ULBUNT.setName("ULBUNT");
            TPO2.add(ULBUNT);
            ULBUNT.setBounds(340, 30, 175, ULBUNT.getPreferredSize().height);
            
            // ---- A1RGC ----
            A1RGC.setComponentPopupMenu(BTD);
            A1RGC.setName("A1RGC");
            TPO2.add(A1RGC);
            A1RGC.setBounds(300, 106, 60, A1RGC.getPreferredSize().height);
            
            // ---- A1IN15 ----
            A1IN15.setText("Non regroupement sur unit\u00e9 de transport");
            A1IN15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            A1IN15.setName("A1IN15");
            TPO2.add(A1IN15);
            A1IN15.setBounds(30, 144, 264, 20);
            
            // ---- OBJ_147 ----
            OBJ_147.setText("Regroupement colisage");
            OBJ_147.setName("OBJ_147");
            TPO2.add(OBJ_147);
            OBJ_147.setBounds(30, 110, 165, 20);
            
            // ---- OBJ_142 ----
            OBJ_142.setText("Nombre d'unit\u00e9s de stock par unit\u00e9 de transport");
            OBJ_142.setName("OBJ_142");
            TPO2.add(OBJ_142);
            OBJ_142.setBounds(30, 72, 265, 20);
            
            // ---- OBJ_144 ----
            OBJ_144.setText("@&M000529@");
            OBJ_144.setName("OBJ_144");
            TPO2.add(OBJ_144);
            OBJ_144.setBounds(390, 70, 30, OBJ_144.getPreferredSize().height);
            
            // ---- OBJ_143 ----
            OBJ_143.setText("Unit\u00e9 de transport");
            OBJ_143.setName("OBJ_143");
            TPO2.add(OBJ_143);
            OBJ_143.setBounds(30, 34, 265, 20);
            
            // ======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Dimensions (longueur x largeur x hauteur)"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);
              
              // ---- A1LNG ----
              A1LNG.setName("A1LNG");
              panel3.add(A1LNG);
              A1LNG.setBounds(20, 35, 50, A1LNG.getPreferredSize().height);
              
              // ---- A1IN28 ----
              A1IN28.setName("A1IN28");
              panel3.add(A1IN28);
              A1IN28.setBounds(70, 35, 60, A1IN28.getPreferredSize().height);
              
              // ---- OBJ_154 ----
              OBJ_154.setText("x");
              OBJ_154.setName("OBJ_154");
              panel3.add(OBJ_154);
              OBJ_154.setBounds(146, 40, 9, 20);
              
              // ---- A1LRG ----
              A1LRG.setName("A1LRG");
              panel3.add(A1LRG);
              A1LRG.setBounds(171, 35, 50, A1LRG.getPreferredSize().height);
              
              // ---- A1IN29 ----
              A1IN29.setName("A1IN29");
              panel3.add(A1IN29);
              A1IN29.setBounds(220, 35, 60, A1IN29.getPreferredSize().height);
              
              // ---- OBJ_156 ----
              OBJ_156.setText("x");
              OBJ_156.setName("OBJ_156");
              panel3.add(OBJ_156);
              OBJ_156.setBounds(296, 40, 9, 20);
              
              // ---- A1HTR ----
              A1HTR.setName("A1HTR");
              panel3.add(A1HTR);
              A1HTR.setBounds(321, 35, 50, A1HTR.getPreferredSize().height);
              
              // ---- A1IN30 ----
              A1IN30.setName("A1IN30");
              panel3.add(A1IN30);
              A1IN30.setBounds(370, 35, 60, A1IN30.getPreferredSize().height);
              
              // ---- OBJ_157 ----
              OBJ_157.setText("=");
              OBJ_157.setName("OBJ_157");
              panel3.add(OBJ_157);
              OBJ_157.setBounds(447, 40, 9, 20);
              
              // ---- WVOL ----
              WVOL.setName("WVOL");
              panel3.add(WVOL);
              WVOL.setBounds(473, 35, 82, WVOL.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            TPO2.add(panel3);
            panel3.setBounds(30, 180, 595, 80);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < TPO2.getComponentCount(); i++) {
                Rectangle bounds = TPO2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = TPO2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              TPO2.setMinimumSize(preferredSize);
              TPO2.setPreferredSize(preferredSize);
            }
          }
          OBJ_31.addTab("Transports", TPO2);
        }
        p_contenu.add(OBJ_31);
        OBJ_31.setBounds(20, 15, 660, 430);
        
        // ---- labelStock ----
        labelStock.setText("Article non g\u00e9r\u00e9 en stock");
        labelStock.setFont(labelStock.getFont().deriveFont(labelStock.getFont().getSize() + 3f));
        labelStock.setForeground(new Color(204, 0, 0));
        labelStock.setHorizontalAlignment(SwingConstants.RIGHT);
        labelStock.setName("labelStock");
        p_contenu.add(labelStock);
        labelStock.setBounds(470, 5, 210, 25);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- miChoixPossible ----
      miChoixPossible.setText("Choix possible");
      miChoixPossible.setName("miChoixPossible");
      miChoixPossible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossibleActionPerformed(e);
        }
      });
      BTD.add(miChoixPossible);
      
      // ---- miAideEnLigne ----
      miAideEnLigne.setText("Aide en ligne");
      miAideEnLigne.setName("miAideEnLigne");
      miAideEnLigne.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAideEnLigneActionPerformed(e);
        }
      });
      BTD.add(miAideEnLigne);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private JPanel p_contenu;
  private JTabbedPane OBJ_31;
  private JPanel STK2;
  private XRiTextField A1UNS;
  private XRiTextField ULBUNS;
  private XRiTextField A1PDS;
  private XRiTextField A1COL;
  private XRiTextField A1PDN;
  private XRiTextField A1VOL;
  private XRiTextField A1ABC;
  private XRiTextField A1MAG1;
  private XRiTextField ULBABC;
  private XRiCheckBox A1IN23;
  private JLabel OBJ_44;
  private JLabel OBJ_53;
  private JLabel OBJ_35;
  private JLabel OBJ_41;
  private RiZoneSortie OBJ_46;
  private RiZoneSortie OBJ_48;
  private JLabel OBJ_50;
  private RiZoneSortie OBJ_54;
  private JLabel OBJ_56;
  private JLabel OBJ_57;
  private JPanel panel9;
  private JLabel OBJ_61;
  private JLabel OBJ_65;
  private JLabel OBJ_66;
  private JLabel OBJ_67;
  private XRiTextField A1DUO1;
  private XRiTextField A1DUO2;
  private XRiTextField A1DUO3;
  private XRiComboBox A1TDUO;
  private XRiCheckBox A1STK;
  private JPanel panel1;
  private JLabel OBJ_143_OBJ_144;
  private XRiTextField A1KSVX;
  private RiZoneSortie A1UNV;
  private XRiTextField A1REF1;
  private JLabel OBJ_58;
  private XRiCheckBox A1IN32;
  private JPanel CND2;
  private JPanel panel6;
  private JPanel panel7;
  private RiZoneSortie OBJ_104;
  private JLabel OBJ_111;
  private XRiTextField X1USC;
  private XRiTextField X1KSC;
  private JPanel panel10;
  private RiZoneSortie OBJ_105;
  private JLabel OBJ_113;
  private XRiTextField X1UCR;
  private XRiTextField X1KCR;
  private JLabel OBJ_109;
  private JLabel OBJ_117;
  private XRiTextField X1PPC;
  private XRiTextField X1PPL;
  private XRiTextField X1PPP;
  private XRiTextField WCLPL;
  private JLabel OBJ_115;
  private JLabel OBJ_120;
  private JLabel OBJ_121;
  private JLabel OBJ_119;
  private XRiTextField X1CNS;
  private JPanel panel2;
  private RiZoneSortie ULBUCS;
  private XRiTextField A1UCS;
  private XRiTextField A1KCS;
  private RiZoneSortie WA1UNL;
  private XRiTextField WQTUNL;
  private JLabel label1;
  private RiZoneSortie A1UNS2;
  private JPanel panel11;
  private JLabel OBJ_123;
  private JLabel OBJ_126;
  private XRiTextField A1CNDX;
  private XRiTextField A1UNL;
  private XRiTextField A1PDSL;
  private XRiTextField A1VOLL;
  private JLabel OBJ_135;
  private JLabel OBJ_137;
  private JLabel OBJ_124;
  private RiZoneSortie A1UNVC;
  private RiZoneSortie ULBUNL;
  private JPanel TPO2;
  private XRiTextField A1KTS;
  private XRiTextField A1UNT;
  private XRiTextField ULBUNT;
  private XRiTextField A1RGC;
  private XRiCheckBox A1IN15;
  private JLabel OBJ_147;
  private JLabel OBJ_142;
  private RiZoneSortie OBJ_144;
  private JLabel OBJ_143;
  private JPanel panel3;
  private XRiTextField A1LNG;
  private XRiComboBox A1IN28;
  private JLabel OBJ_154;
  private XRiTextField A1LRG;
  private XRiComboBox A1IN29;
  private JLabel OBJ_156;
  private XRiTextField A1HTR;
  private XRiComboBox A1IN30;
  private JLabel OBJ_157;
  private XRiTextField WVOL;
  private JLabel labelStock;
  private JPopupMenu BTD;
  private JMenuItem miChoixPossible;
  private JMenuItem miAideEnLigne;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
