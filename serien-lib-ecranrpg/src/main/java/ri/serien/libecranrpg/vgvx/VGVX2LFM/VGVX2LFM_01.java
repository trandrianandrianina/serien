
package ri.serien.libecranrpg.vgvx.VGVX2LFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGVX2LFM_01 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX2LFM_01(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    ZL01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZL01@")).trim());
    ZL02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZL02@")).trim());
    ZL03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZL03@")).trim());
    ZL04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZL04@")).trim());
    ZL05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZL05@")).trim());
    TIZ01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZ01@")).trim());
    TIZ02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZ02@")).trim());
    TIZ03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZ03@")).trim());
    TIZ04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZ04@")).trim());
    TIZ05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZ05@")).trim());
    SLLOT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLLOT@")).trim());
    SLLOTF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLLOTF@")).trim());
    SLART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLART@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    SLMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLMAG@")).trim());
    WSTK.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTK@")).trim());
    WDIS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDIS@")).trim());
    SLCCT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLCCT@")).trim());
    WSLCND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSLCND@")).trim());
    SLUNL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLUNL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    panel2.setVisible(lexique.isTrue("17"));
    OBJ_52.setVisible(lexique.isTrue("90"));
    WSLCND.setVisible(lexique.isTrue("90"));
    OBJ_53.setVisible(lexique.isTrue("90"));
    SLUNL.setVisible(lexique.isTrue("90"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Modification des informations d'un lot"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OB1 = new XRiTextField();
    OB2 = new XRiTextField();
    OB3 = new XRiTextField();
    OB4 = new XRiTextField();
    OB5 = new XRiTextField();
    OB6 = new XRiTextField();
    OB7 = new XRiTextField();
    OB8 = new XRiTextField();
    ZL01 = new RiZoneSortie();
    ZL02 = new RiZoneSortie();
    ZL03 = new RiZoneSortie();
    ZL04 = new RiZoneSortie();
    ZL05 = new RiZoneSortie();
    TIZ01 = new RiZoneSortie();
    TIZ02 = new RiZoneSortie();
    TIZ03 = new RiZoneSortie();
    TIZ04 = new RiZoneSortie();
    TIZ05 = new RiZoneSortie();
    WTP1 = new XRiTextField();
    WTP2 = new XRiTextField();
    WTP3 = new XRiTextField();
    WTP4 = new XRiTextField();
    WTP5 = new XRiTextField();
    panel2 = new JPanel();
    OBJ_50 = new JLabel();
    OBJ_51 = new JLabel();
    WXART1 = new XRiTextField();
    WXART2 = new XRiTextField();
    WXQTE2 = new XRiTextField();
    panel3 = new JPanel();
    OBJ_41 = new JLabel();
    SLLOT = new RiZoneSortie();
    SLLOTF = new XRiTextField();
    OBJ_42 = new JLabel();
    SLART = new RiZoneSortie();
    A1LIB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    SLMAG = new RiZoneSortie();
    OBJ_45 = new JLabel();
    WSTK = new RiZoneSortie();
    OBJ_46 = new JLabel();
    WDIS = new RiZoneSortie();
    OBJ_47 = new JLabel();
    SLCCT = new RiZoneSortie();
    OBJ_48 = new JLabel();
    WDTEX = new XRiCalendrier();
    OBJ_49 = new JLabel();
    WLIMX = new XRiCalendrier();
    WSLCND = new RiZoneSortie();
    OBJ_52 = new JLabel();
    SLUNL = new RiZoneSortie();
    OBJ_53 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(890, 630));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(720, 620));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Informations du lot"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OB1 ----
          OB1.setComponentPopupMenu(BTD);
          OB1.setName("OB1");
          panel1.add(OB1);
          OB1.setBounds(40, 225, 610, OB1.getPreferredSize().height);

          //---- OB2 ----
          OB2.setComponentPopupMenu(BTD);
          OB2.setName("OB2");
          panel1.add(OB2);
          OB2.setBounds(40, 250, 610, OB2.getPreferredSize().height);

          //---- OB3 ----
          OB3.setComponentPopupMenu(BTD);
          OB3.setName("OB3");
          panel1.add(OB3);
          OB3.setBounds(40, 275, 610, OB3.getPreferredSize().height);

          //---- OB4 ----
          OB4.setComponentPopupMenu(BTD);
          OB4.setName("OB4");
          panel1.add(OB4);
          OB4.setBounds(40, 300, 610, OB4.getPreferredSize().height);

          //---- OB5 ----
          OB5.setComponentPopupMenu(BTD);
          OB5.setName("OB5");
          panel1.add(OB5);
          OB5.setBounds(40, 325, 610, OB5.getPreferredSize().height);

          //---- OB6 ----
          OB6.setComponentPopupMenu(BTD);
          OB6.setName("OB6");
          panel1.add(OB6);
          OB6.setBounds(40, 350, 610, OB6.getPreferredSize().height);

          //---- OB7 ----
          OB7.setComponentPopupMenu(BTD);
          OB7.setName("OB7");
          panel1.add(OB7);
          OB7.setBounds(40, 375, 610, OB7.getPreferredSize().height);

          //---- OB8 ----
          OB8.setComponentPopupMenu(BTD);
          OB8.setName("OB8");
          panel1.add(OB8);
          OB8.setBounds(40, 400, 610, OB8.getPreferredSize().height);

          //---- ZL01 ----
          ZL01.setText("@ZL01@");
          ZL01.setName("ZL01");
          panel1.add(ZL01);
          ZL01.setBounds(40, 435, 260, ZL01.getPreferredSize().height);

          //---- ZL02 ----
          ZL02.setText("@ZL02@");
          ZL02.setName("ZL02");
          panel1.add(ZL02);
          ZL02.setBounds(40, 465, 260, ZL02.getPreferredSize().height);

          //---- ZL03 ----
          ZL03.setText("@ZL03@");
          ZL03.setName("ZL03");
          panel1.add(ZL03);
          ZL03.setBounds(40, 495, 260, ZL03.getPreferredSize().height);

          //---- ZL04 ----
          ZL04.setText("@ZL04@");
          ZL04.setName("ZL04");
          panel1.add(ZL04);
          ZL04.setBounds(40, 525, 260, ZL04.getPreferredSize().height);

          //---- ZL05 ----
          ZL05.setText("@ZL05@");
          ZL05.setName("ZL05");
          panel1.add(ZL05);
          ZL05.setBounds(40, 555, 260, ZL05.getPreferredSize().height);

          //---- TIZ01 ----
          TIZ01.setText("@TIZ01@");
          TIZ01.setName("TIZ01");
          panel1.add(TIZ01);
          TIZ01.setBounds(315, 435, 30, TIZ01.getPreferredSize().height);

          //---- TIZ02 ----
          TIZ02.setText("@TIZ02@");
          TIZ02.setName("TIZ02");
          panel1.add(TIZ02);
          TIZ02.setBounds(315, 465, 30, TIZ02.getPreferredSize().height);

          //---- TIZ03 ----
          TIZ03.setText("@TIZ03@");
          TIZ03.setName("TIZ03");
          panel1.add(TIZ03);
          TIZ03.setBounds(315, 495, 30, TIZ03.getPreferredSize().height);

          //---- TIZ04 ----
          TIZ04.setText("@TIZ04@");
          TIZ04.setName("TIZ04");
          panel1.add(TIZ04);
          TIZ04.setBounds(315, 525, 30, TIZ04.getPreferredSize().height);

          //---- TIZ05 ----
          TIZ05.setText("@TIZ05@");
          TIZ05.setName("TIZ05");
          panel1.add(TIZ05);
          TIZ05.setBounds(315, 555, 30, TIZ05.getPreferredSize().height);

          //---- WTP1 ----
          WTP1.setComponentPopupMenu(BTD);
          WTP1.setName("WTP1");
          panel1.add(WTP1);
          WTP1.setBounds(350, 435, 30, WTP1.getPreferredSize().height);

          //---- WTP2 ----
          WTP2.setComponentPopupMenu(BTD);
          WTP2.setName("WTP2");
          panel1.add(WTP2);
          WTP2.setBounds(350, 465, 30, WTP2.getPreferredSize().height);

          //---- WTP3 ----
          WTP3.setComponentPopupMenu(BTD);
          WTP3.setName("WTP3");
          panel1.add(WTP3);
          WTP3.setBounds(350, 495, 30, WTP3.getPreferredSize().height);

          //---- WTP4 ----
          WTP4.setComponentPopupMenu(BTD);
          WTP4.setName("WTP4");
          panel1.add(WTP4);
          WTP4.setBounds(350, 525, 30, WTP4.getPreferredSize().height);

          //---- WTP5 ----
          WTP5.setComponentPopupMenu(BTD);
          WTP5.setName("WTP5");
          panel1.add(WTP5);
          WTP5.setBounds(350, 555, 30, WTP5.getPreferredSize().height);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Support palettes"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_50 ----
            OBJ_50.setText("Principal");
            OBJ_50.setName("OBJ_50");
            panel2.add(OBJ_50);
            OBJ_50.setBounds(15, 50, 75, 28);

            //---- OBJ_51 ----
            OBJ_51.setText("Secondaire");
            OBJ_51.setName("OBJ_51");
            panel2.add(OBJ_51);
            OBJ_51.setBounds(15, 90, 75, 28);

            //---- WXART1 ----
            WXART1.setComponentPopupMenu(BTD);
            WXART1.setName("WXART1");
            panel2.add(WXART1);
            WXART1.setBounds(90, 50, 110, WXART1.getPreferredSize().height);

            //---- WXART2 ----
            WXART2.setComponentPopupMenu(BTD);
            WXART2.setName("WXART2");
            panel2.add(WXART2);
            WXART2.setBounds(90, 90, 110, WXART2.getPreferredSize().height);

            //---- WXQTE2 ----
            WXQTE2.setComponentPopupMenu(BTD);
            WXQTE2.setName("WXQTE2");
            panel2.add(WXQTE2);
            WXQTE2.setBounds(205, 90, 40, WXQTE2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(390, 435, 260, 145);

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Lot"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_41 ----
            OBJ_41.setText("Lot");
            OBJ_41.setName("OBJ_41");
            panel3.add(OBJ_41);
            OBJ_41.setBounds(25, 33, 60, 28);

            //---- SLLOT ----
            SLLOT.setText("@SLLOT@");
            SLLOT.setName("SLLOT");
            panel3.add(SLLOT);
            SLLOT.setBounds(85, 35, 260, SLLOT.getPreferredSize().height);

            //---- SLLOTF ----
            SLLOTF.setText("@SLLOTF@");
            SLLOTF.setName("SLLOTF");
            panel3.add(SLLOTF);
            SLLOTF.setBounds(355, 33, 260, SLLOTF.getPreferredSize().height);

            //---- OBJ_42 ----
            OBJ_42.setText("Article");
            OBJ_42.setName("OBJ_42");
            panel3.add(OBJ_42);
            OBJ_42.setBounds(25, 63, 60, 28);

            //---- SLART ----
            SLART.setText("@SLART@");
            SLART.setName("SLART");
            panel3.add(SLART);
            SLART.setBounds(85, 65, 210, SLART.getPreferredSize().height);

            //---- A1LIB ----
            A1LIB.setText("@A1LIB@");
            A1LIB.setName("A1LIB");
            panel3.add(A1LIB);
            A1LIB.setBounds(305, 65, 310, A1LIB.getPreferredSize().height);

            //---- OBJ_44 ----
            OBJ_44.setText("Magasin");
            OBJ_44.setName("OBJ_44");
            panel3.add(OBJ_44);
            OBJ_44.setBounds(25, 93, 60, 28);

            //---- SLMAG ----
            SLMAG.setText("@SLMAG@");
            SLMAG.setName("SLMAG");
            panel3.add(SLMAG);
            SLMAG.setBounds(85, 95, 30, SLMAG.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setText("Stock");
            OBJ_45.setName("OBJ_45");
            panel3.add(OBJ_45);
            OBJ_45.setBounds(220, 93, 60, 28);

            //---- WSTK ----
            WSTK.setText("@WSTK@");
            WSTK.setHorizontalAlignment(SwingConstants.RIGHT);
            WSTK.setName("WSTK");
            panel3.add(WSTK);
            WSTK.setBounds(305, 95, 100, WSTK.getPreferredSize().height);

            //---- OBJ_46 ----
            OBJ_46.setText("Disponible");
            OBJ_46.setName("OBJ_46");
            panel3.add(OBJ_46);
            OBJ_46.setBounds(435, 93, 75, 28);

            //---- WDIS ----
            WDIS.setText("@WDIS@");
            WDIS.setHorizontalAlignment(SwingConstants.RIGHT);
            WDIS.setName("WDIS");
            panel3.add(WDIS);
            WDIS.setBounds(new Rectangle(new Point(515, 95), WDIS.getPreferredSize()));

            //---- OBJ_47 ----
            OBJ_47.setText("Dossier");
            OBJ_47.setName("OBJ_47");
            panel3.add(OBJ_47);
            OBJ_47.setBounds(25, 123, 60, 28);

            //---- SLCCT ----
            SLCCT.setText("@SLCCT@");
            SLCCT.setName("SLCCT");
            panel3.add(SLCCT);
            SLCCT.setBounds(85, 125, 110, SLCCT.getPreferredSize().height);

            //---- OBJ_48 ----
            OBJ_48.setText("Date d'entr\u00e9e");
            OBJ_48.setName("OBJ_48");
            panel3.add(OBJ_48);
            OBJ_48.setBounds(220, 123, 85, 28);

            //---- WDTEX ----
            WDTEX.setName("WDTEX");
            panel3.add(WDTEX);
            WDTEX.setBounds(305, 123, 105, WDTEX.getPreferredSize().height);

            //---- OBJ_49 ----
            OBJ_49.setText("Date limite");
            OBJ_49.setName("OBJ_49");
            panel3.add(OBJ_49);
            OBJ_49.setBounds(435, 123, 75, 28);

            //---- WLIMX ----
            WLIMX.setName("WLIMX");
            panel3.add(WLIMX);
            WLIMX.setBounds(515, 123, 105, WLIMX.getPreferredSize().height);

            //---- WSLCND ----
            WSLCND.setText("@WSLCND@");
            WSLCND.setHorizontalAlignment(SwingConstants.RIGHT);
            WSLCND.setName("WSLCND");
            panel3.add(WSLCND);
            WSLCND.setBounds(220, 155, 60, WSLCND.getPreferredSize().height);

            //---- OBJ_52 ----
            OBJ_52.setText("Conditionnement du lot par");
            OBJ_52.setName("OBJ_52");
            panel3.add(OBJ_52);
            OBJ_52.setBounds(25, 153, 190, 28);

            //---- SLUNL ----
            SLUNL.setText("@SLUNL@");
            SLUNL.setName("SLUNL");
            panel3.add(SLUNL);
            SLUNL.setBounds(375, 155, 30, SLUNL.getPreferredSize().height);

            //---- OBJ_53 ----
            OBJ_53.setText("/ code SKU");
            OBJ_53.setName("OBJ_53");
            panel3.add(OBJ_53);
            OBJ_53.setBounds(305, 153, 70, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel3);
          panel3.setBounds(25, 25, 645, 195);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 696, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField OB1;
  private XRiTextField OB2;
  private XRiTextField OB3;
  private XRiTextField OB4;
  private XRiTextField OB5;
  private XRiTextField OB6;
  private XRiTextField OB7;
  private XRiTextField OB8;
  private RiZoneSortie ZL01;
  private RiZoneSortie ZL02;
  private RiZoneSortie ZL03;
  private RiZoneSortie ZL04;
  private RiZoneSortie ZL05;
  private RiZoneSortie TIZ01;
  private RiZoneSortie TIZ02;
  private RiZoneSortie TIZ03;
  private RiZoneSortie TIZ04;
  private RiZoneSortie TIZ05;
  private XRiTextField WTP1;
  private XRiTextField WTP2;
  private XRiTextField WTP3;
  private XRiTextField WTP4;
  private XRiTextField WTP5;
  private JPanel panel2;
  private JLabel OBJ_50;
  private JLabel OBJ_51;
  private XRiTextField WXART1;
  private XRiTextField WXART2;
  private XRiTextField WXQTE2;
  private JPanel panel3;
  private JLabel OBJ_41;
  private RiZoneSortie SLLOT;
  private XRiTextField SLLOTF;
  private JLabel OBJ_42;
  private RiZoneSortie SLART;
  private RiZoneSortie A1LIB;
  private JLabel OBJ_44;
  private RiZoneSortie SLMAG;
  private JLabel OBJ_45;
  private RiZoneSortie WSTK;
  private JLabel OBJ_46;
  private RiZoneSortie WDIS;
  private JLabel OBJ_47;
  private RiZoneSortie SLCCT;
  private JLabel OBJ_48;
  private XRiCalendrier WDTEX;
  private JLabel OBJ_49;
  private XRiCalendrier WLIMX;
  private RiZoneSortie WSLCND;
  private JLabel OBJ_52;
  private RiZoneSortie SLUNL;
  private JLabel OBJ_53;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
