
package ri.serien.libecranrpg.vgvx.VGVX21FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX21FM_L3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX21FM_L3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    NUM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM1@")).trim());
    NUM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM2@")).trim());
    NUM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM3@")).trim());
    NUM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM4@")).trim());
    NUM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM5@")).trim());
    NUM6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM6@")).trim());
    NUM7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM7@")).trim());
    NUM8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM8@")).trim());
    NUM9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM9@")).trim());
    NUM10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM10@")).trim());
    QTSX1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX1@")).trim());
    QTSX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX2@")).trim());
    QTSX3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX3@")).trim());
    QTSX4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX4@")).trim());
    QTSX5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX5@")).trim());
    QTSX6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX6@")).trim());
    QTSX7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX7@")).trim());
    QTSX8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX8@")).trim());
    QTSX9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX9@")).trim());
    QTSX10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX10@")).trim());
    P21ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P21ART@+")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    WDISX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    WDISUN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISUN@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLDALA@")).trim());
    A1CNDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1CNDX@")).trim());
    OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1QT2@")).trim());
    WCLPL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCLPL@")).trim());
    X1PPC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@X1PPC@")).trim());
    X1PPL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@X1PPL@")).trim());
    X1PPP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@X1PPP@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNL@")).trim());
    OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNL@")).trim());
    OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1QT3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    riSousMenu15.setVisible(!lexique.HostFieldGetData("WCF15").trim().equalsIgnoreCase(""));
    OBJ_13.setVisible(!lexique.HostFieldGetData("WCF13").trim().equalsIgnoreCase(""));
    OBJ_14.setVisible(!lexique.HostFieldGetData("WCF14").trim().equalsIgnoreCase(""));
    
    OBJ_35.setVisible(!lexique.HostFieldGetData("WCLPL").trim().equalsIgnoreCase("") & lexique.isPresent("WCLPL"));
    OBJ_37.setVisible(!lexique.HostFieldGetData("WCLPL").trim().equalsIgnoreCase("") & lexique.isPresent("WCLPL"));
    OBJ_28.setVisible(lexique.isPresent("A1CNDX"));
    X1PPP.setEnabled(lexique.isPresent("X1PPP"));
    X1PPL.setEnabled(lexique.isPresent("X1PPL"));
    X1PPC.setEnabled(lexique.isPresent("X1PPC"));
    WDISUN.setEnabled(lexique.isPresent("WDISUN"));
    OBJ_38.setVisible(lexique.isPresent("A1QT2"));
    WCLPL.setVisible(!lexique.HostFieldGetData("WCLPL").trim().equalsIgnoreCase("") & lexique.isPresent("WCLPL"));
    OBJ_41.setVisible(lexique.isPresent("A1QT3"));
    OBJ_39.setVisible(lexique.isPresent("A1QT2"));
    OBJ_40.setVisible(lexique.isPresent("A1QT3"));
    P21QPX.setVisible(!lexique.HostFieldGetData("HD02").trim().equalsIgnoreCase("ngueur*Bb") & lexique.isPresent("P21QPX"));
    P21QTX.setVisible(!lexique.HostFieldGetData("HD02").trim().equalsIgnoreCase("ngueur*Bb"));
    A1CNDX.setVisible(lexique.isPresent("A1CNDX"));
    WDISX.setEnabled(lexique.isPresent("WDISX"));
    OBJ_26.setVisible(lexique.isPresent("A1CNDX"));
    P21ART.setVisible(lexique.isPresent("P21ART"));
    OBJ_42.setVisible(lexique.isPresent("WLDALA"));
    
    // entête
    label2.setText(lexique.HostFieldGetData("HLD01").substring(4, 27));
    label4.setText(lexique.HostFieldGetData("HLD01").substring(29, 39));
    label5.setText(lexique.HostFieldGetData("HLD01").substring(39, 49));
    label7.setText(lexique.HostFieldGetData("HLD01").substring(51, 62));
    label8.setText(lexique.HostFieldGetData("HLD01").substring(62, 70));
    label11.setText(lexique.HostFieldGetData("HLD01").substring(76, lexique.HostFieldGetData("HLD01").length()));
    
    // Gestion du focus avec les touches flèches
    final XRiTextField[] ENL1table = { ENL101, ENL102, ENL103, ENL104, ENL105, ENL106, ENL107, ENL108, ENL109, ENL110 };
    final XRiTextField[] ENL2table = { ENL201, ENL202, ENL203, ENL204, ENL205, ENL206, ENL207, ENL208, ENL209, ENL210 };
    final XRiTextField[] QTEXtable = { QTEX1, QTEX2, QTEX3, QTEX4, QTEX5, QTEX6, QTEX7, QTEX8, QTEX9, QTEX10 };
    final XRiTextField[] DPEXtable = { DPEX1, DPEX2, DPEX3, DPEX4, DPEX5, DPEX6, DPEX7, DPEX8, DPEX9, DPEX10 };
    final XRiTextField[] D1EXtable = { D1EX1, D1EX2, D1EX3, D1EX4, D1EX5, D1EX6, D1EX7, D1EX8, D1EX9, D1EX10 };
    final XRiTextField[] TP1table = { TP11, TP12, TP13, TP14, TP15, TP16, TP17, TP18, TP19, TP110 };
    final XRiTextField[] TP2table = { TP21, TP22, TP23, TP24, TP25, TP26, TP27, TP28, TP29, TP210 };
    final XRiTextField[] TP3table = { TP11, TP12, TP13, TP14, TP15, TP16, TP17, TP18, TP19, TP110 };
    final XRiTextField[] TP4table = { TP21, TP22, TP23, TP24, TP25, TP26, TP27, TP28, TP29, TP210 };
    final XRiTextField[] TP5table = { TP21, TP22, TP23, TP24, TP25, TP26, TP27, TP28, TP29, TP210 };
    final XRiTextField[] DOStable = { DOS01, DOS02, DOS03, DOS04, DOS05, DOS06, DOS07, DOS08, DOS09, DOS10 };
    final XRiTextField[] LD3Xtable = { LD3X1, LD3X2, LD3X3, LD3X4, LD3X5, LD3X6, LD3X7, LD3X8, LD3X9, LD3X10 };
    
    for (int i = 0; i < ENL1table.length; i++) {
      final int indice = i;
      
      ENL1table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              ENL2table[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                ENL1table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                ENL1table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      ENL2table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              QTEXtable[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              ENL1table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                ENL2table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                ENL2table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      QTEXtable[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              DPEXtable[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              ENL2table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                QTEXtable[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                QTEXtable[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      DPEXtable[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              D1EXtable[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              QTEXtable[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                DPEXtable[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                DPEXtable[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      D1EXtable[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              TP1table[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              DPEXtable[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                D1EXtable[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                D1EXtable[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      TP1table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              TP2table[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              D1EXtable[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                TP1table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                TP1table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      TP2table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              TP3table[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              TP1table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                TP2table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                TP2table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      TP3table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              TP4table[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              TP2table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                TP3table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                TP3table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      TP4table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              TP5table[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              TP3table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                TP4table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                TP4table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      TP5table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              DOStable[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              TP4table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                TP5table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                TP5table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      DOStable[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              LD3Xtable[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              TP5table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                DOStable[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                DOStable[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      LD3Xtable[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              
              break;
            case KeyEvent.VK_LEFT:
              DOStable[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                LD3Xtable[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                LD3Xtable[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
    }
    
    setTitle(interpreteurD.analyseExpression("Saisie de numéros de lots pour l'article @P21ART@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    NUM1 = new RiZoneSortie();
    ENL101 = new XRiTextField();
    ENL201 = new XRiTextField();
    NUM2 = new RiZoneSortie();
    ENL102 = new XRiTextField();
    ENL202 = new XRiTextField();
    NUM3 = new RiZoneSortie();
    ENL103 = new XRiTextField();
    ENL203 = new XRiTextField();
    NUM4 = new RiZoneSortie();
    ENL104 = new XRiTextField();
    ENL204 = new XRiTextField();
    NUM5 = new RiZoneSortie();
    ENL105 = new XRiTextField();
    ENL205 = new XRiTextField();
    NUM6 = new RiZoneSortie();
    ENL106 = new XRiTextField();
    ENL206 = new XRiTextField();
    NUM7 = new RiZoneSortie();
    ENL107 = new XRiTextField();
    ENL207 = new XRiTextField();
    NUM8 = new RiZoneSortie();
    ENL108 = new XRiTextField();
    ENL208 = new XRiTextField();
    NUM9 = new RiZoneSortie();
    ENL109 = new XRiTextField();
    ENL209 = new XRiTextField();
    NUM10 = new RiZoneSortie();
    ENL110 = new XRiTextField();
    ENL210 = new XRiTextField();
    QTSX1 = new RiZoneSortie();
    QTSX2 = new RiZoneSortie();
    QTSX3 = new RiZoneSortie();
    QTSX4 = new RiZoneSortie();
    QTSX5 = new RiZoneSortie();
    QTSX6 = new RiZoneSortie();
    QTSX7 = new RiZoneSortie();
    QTSX8 = new RiZoneSortie();
    QTSX9 = new RiZoneSortie();
    QTSX10 = new RiZoneSortie();
    QTEX1 = new XRiTextField();
    QTEX2 = new XRiTextField();
    QTEX3 = new XRiTextField();
    QTEX4 = new XRiTextField();
    QTEX5 = new XRiTextField();
    QTEX6 = new XRiTextField();
    QTEX7 = new XRiTextField();
    QTEX8 = new XRiTextField();
    QTEX9 = new XRiTextField();
    QTEX10 = new XRiTextField();
    DPEX1 = new XRiTextField();
    DPEX2 = new XRiTextField();
    DPEX3 = new XRiTextField();
    DPEX4 = new XRiTextField();
    DPEX5 = new XRiTextField();
    DPEX6 = new XRiTextField();
    DPEX7 = new XRiTextField();
    DPEX8 = new XRiTextField();
    DPEX9 = new XRiTextField();
    DPEX10 = new XRiTextField();
    TP11 = new XRiTextField();
    TP12 = new XRiTextField();
    TP13 = new XRiTextField();
    TP14 = new XRiTextField();
    TP15 = new XRiTextField();
    TP16 = new XRiTextField();
    TP17 = new XRiTextField();
    TP18 = new XRiTextField();
    TP19 = new XRiTextField();
    TP110 = new XRiTextField();
    TP21 = new XRiTextField();
    TP22 = new XRiTextField();
    TP23 = new XRiTextField();
    TP24 = new XRiTextField();
    TP25 = new XRiTextField();
    TP26 = new XRiTextField();
    TP27 = new XRiTextField();
    TP28 = new XRiTextField();
    TP29 = new XRiTextField();
    TP210 = new XRiTextField();
    TP31 = new XRiTextField();
    TP32 = new XRiTextField();
    TP33 = new XRiTextField();
    TP34 = new XRiTextField();
    TP35 = new XRiTextField();
    TP36 = new XRiTextField();
    TP37 = new XRiTextField();
    TP38 = new XRiTextField();
    TP39 = new XRiTextField();
    TP310 = new XRiTextField();
    TP41 = new XRiTextField();
    TP42 = new XRiTextField();
    TP43 = new XRiTextField();
    TP44 = new XRiTextField();
    TP45 = new XRiTextField();
    TP46 = new XRiTextField();
    TP47 = new XRiTextField();
    TP48 = new XRiTextField();
    TP49 = new XRiTextField();
    TP410 = new XRiTextField();
    TP51 = new XRiTextField();
    TP52 = new XRiTextField();
    TP53 = new XRiTextField();
    TP54 = new XRiTextField();
    TP55 = new XRiTextField();
    TP56 = new XRiTextField();
    TP57 = new XRiTextField();
    TP58 = new XRiTextField();
    TP59 = new XRiTextField();
    TP510 = new XRiTextField();
    DOS01 = new XRiTextField();
    DOS02 = new XRiTextField();
    DOS03 = new XRiTextField();
    DOS04 = new XRiTextField();
    DOS05 = new XRiTextField();
    DOS06 = new XRiTextField();
    DOS07 = new XRiTextField();
    DOS08 = new XRiTextField();
    DOS09 = new XRiTextField();
    DOS10 = new XRiTextField();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    P21QPX = new XRiTextField();
    panel1 = new JPanel();
    P21ART = new RiZoneSortie();
    OBJ_19 = new JLabel();
    A1LIB = new RiZoneSortie();
    OBJ_23 = new JLabel();
    WDISX = new RiZoneSortie();
    WDISUN = new RiZoneSortie();
    OBJ_42 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_26 = new JLabel();
    A1CNDX = new RiZoneSortie();
    OBJ_40 = new JLabel();
    OBJ_39 = new RiZoneSortie();
    WCLPL = new RiZoneSortie();
    OBJ_38 = new JLabel();
    X1PPC = new RiZoneSortie();
    X1PPL = new RiZoneSortie();
    X1PPP = new RiZoneSortie();
    OBJ_28 = new RiZoneSortie();
    OBJ_37 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_41 = new RiZoneSortie();
    label9 = new JLabel();
    LD3X1 = new XRiTextField();
    D1EX1 = new XRiTextField();
    P21QTX = new XRiTextField();
    LD3X2 = new XRiTextField();
    LD3X3 = new XRiTextField();
    LD3X4 = new XRiTextField();
    LD3X5 = new XRiTextField();
    LD3X6 = new XRiTextField();
    LD3X7 = new XRiTextField();
    LD3X8 = new XRiTextField();
    LD3X9 = new XRiTextField();
    LD3X10 = new XRiTextField();
    D1EX2 = new XRiTextField();
    D1EX3 = new XRiTextField();
    D1EX4 = new XRiTextField();
    D1EX5 = new XRiTextField();
    D1EX6 = new XRiTextField();
    D1EX7 = new XRiTextField();
    D1EX8 = new XRiTextField();
    D1EX9 = new XRiTextField();
    D1EX10 = new XRiTextField();
    label2 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label11 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1400, 485));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Autre vue");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Detail conditionnement");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");

            //---- riMenu_bt3 ----
            riMenu_bt3.setText("Outils");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);

          //======== riSousMenu14 ========
          {
            riSousMenu14.setName("riSousMenu14");

            //---- riSousMenu_bt14 ----
            riSousMenu_bt14.setText("Bloc notes");
            riSousMenu_bt14.setName("riSousMenu_bt14");
            riSousMenu_bt14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt14ActionPerformed(e);
              }
            });
            riSousMenu14.add(riSousMenu_bt14);
          }
          menus_haut.add(riSousMenu14);

          //======== riSousMenu15 ========
          {
            riSousMenu15.setName("riSousMenu15");

            //---- riSousMenu_bt15 ----
            riSousMenu_bt15.setText("Fabrication");
            riSousMenu_bt15.setToolTipText("Fabrication");
            riSousMenu_bt15.setName("riSousMenu_bt15");
            riSousMenu_bt15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt15ActionPerformed(e);
              }
            });
            riSousMenu15.add(riSousMenu_bt15);
          }
          menus_haut.add(riSousMenu15);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- NUM1 ----
        NUM1.setText("@NUM1@");
        NUM1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        NUM1.setName("NUM1");
        p_contenu.add(NUM1);
        NUM1.setBounds(15, 182, 40, 24);

        //---- ENL101 ----
        ENL101.setName("ENL101");
        p_contenu.add(ENL101);
        ENL101.setBounds(60, 180, 220, 28);

        //---- ENL201 ----
        ENL201.setName("ENL201");
        p_contenu.add(ENL201);
        ENL201.setBounds(285, 180, 34, 28);

        //---- NUM2 ----
        NUM2.setText("@NUM2@");
        NUM2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        NUM2.setName("NUM2");
        p_contenu.add(NUM2);
        NUM2.setBounds(15, 207, 40, 24);

        //---- ENL102 ----
        ENL102.setName("ENL102");
        p_contenu.add(ENL102);
        ENL102.setBounds(60, 205, 220, 28);

        //---- ENL202 ----
        ENL202.setName("ENL202");
        p_contenu.add(ENL202);
        ENL202.setBounds(285, 205, 34, 28);

        //---- NUM3 ----
        NUM3.setText("@NUM3@");
        NUM3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        NUM3.setName("NUM3");
        p_contenu.add(NUM3);
        NUM3.setBounds(15, 232, 40, 24);

        //---- ENL103 ----
        ENL103.setName("ENL103");
        p_contenu.add(ENL103);
        ENL103.setBounds(60, 230, 220, 28);

        //---- ENL203 ----
        ENL203.setName("ENL203");
        p_contenu.add(ENL203);
        ENL203.setBounds(285, 230, 34, 28);

        //---- NUM4 ----
        NUM4.setText("@NUM4@");
        NUM4.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        NUM4.setName("NUM4");
        p_contenu.add(NUM4);
        NUM4.setBounds(15, 257, 40, 24);

        //---- ENL104 ----
        ENL104.setName("ENL104");
        p_contenu.add(ENL104);
        ENL104.setBounds(60, 255, 220, 28);

        //---- ENL204 ----
        ENL204.setName("ENL204");
        p_contenu.add(ENL204);
        ENL204.setBounds(285, 255, 34, 28);

        //---- NUM5 ----
        NUM5.setText("@NUM5@");
        NUM5.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        NUM5.setName("NUM5");
        p_contenu.add(NUM5);
        NUM5.setBounds(15, 282, 40, 24);

        //---- ENL105 ----
        ENL105.setName("ENL105");
        p_contenu.add(ENL105);
        ENL105.setBounds(60, 280, 220, 28);

        //---- ENL205 ----
        ENL205.setName("ENL205");
        p_contenu.add(ENL205);
        ENL205.setBounds(285, 280, 34, 28);

        //---- NUM6 ----
        NUM6.setText("@NUM6@");
        NUM6.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        NUM6.setName("NUM6");
        p_contenu.add(NUM6);
        NUM6.setBounds(15, 307, 40, 24);

        //---- ENL106 ----
        ENL106.setName("ENL106");
        p_contenu.add(ENL106);
        ENL106.setBounds(60, 305, 220, 28);

        //---- ENL206 ----
        ENL206.setName("ENL206");
        p_contenu.add(ENL206);
        ENL206.setBounds(285, 305, 34, 28);

        //---- NUM7 ----
        NUM7.setText("@NUM7@");
        NUM7.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        NUM7.setName("NUM7");
        p_contenu.add(NUM7);
        NUM7.setBounds(15, 332, 40, 24);

        //---- ENL107 ----
        ENL107.setName("ENL107");
        p_contenu.add(ENL107);
        ENL107.setBounds(60, 330, 220, 28);

        //---- ENL207 ----
        ENL207.setName("ENL207");
        p_contenu.add(ENL207);
        ENL207.setBounds(285, 330, 34, 28);

        //---- NUM8 ----
        NUM8.setText("@NUM8@");
        NUM8.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        NUM8.setName("NUM8");
        p_contenu.add(NUM8);
        NUM8.setBounds(15, 357, 40, 24);

        //---- ENL108 ----
        ENL108.setName("ENL108");
        p_contenu.add(ENL108);
        ENL108.setBounds(60, 355, 220, 28);

        //---- ENL208 ----
        ENL208.setName("ENL208");
        p_contenu.add(ENL208);
        ENL208.setBounds(285, 355, 34, 28);

        //---- NUM9 ----
        NUM9.setText("@NUM9@");
        NUM9.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        NUM9.setName("NUM9");
        p_contenu.add(NUM9);
        NUM9.setBounds(15, 382, 40, 24);

        //---- ENL109 ----
        ENL109.setName("ENL109");
        p_contenu.add(ENL109);
        ENL109.setBounds(60, 380, 220, 28);

        //---- ENL209 ----
        ENL209.setName("ENL209");
        p_contenu.add(ENL209);
        ENL209.setBounds(285, 380, 34, 28);

        //---- NUM10 ----
        NUM10.setText("@NUM10@");
        NUM10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        NUM10.setName("NUM10");
        p_contenu.add(NUM10);
        NUM10.setBounds(15, 407, 40, 24);

        //---- ENL110 ----
        ENL110.setName("ENL110");
        p_contenu.add(ENL110);
        ENL110.setBounds(60, 405, 220, 28);

        //---- ENL210 ----
        ENL210.setName("ENL210");
        p_contenu.add(ENL210);
        ENL210.setBounds(285, 405, 34, 28);

        //---- QTSX1 ----
        QTSX1.setText("@QTSX1@");
        QTSX1.setHorizontalAlignment(SwingConstants.RIGHT);
        QTSX1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTSX1.setName("QTSX1");
        p_contenu.add(QTSX1);
        QTSX1.setBounds(325, 182, 75, 24);

        //---- QTSX2 ----
        QTSX2.setText("@QTSX2@");
        QTSX2.setHorizontalAlignment(SwingConstants.RIGHT);
        QTSX2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTSX2.setName("QTSX2");
        p_contenu.add(QTSX2);
        QTSX2.setBounds(325, 207, 75, 24);

        //---- QTSX3 ----
        QTSX3.setText("@QTSX3@");
        QTSX3.setHorizontalAlignment(SwingConstants.RIGHT);
        QTSX3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTSX3.setName("QTSX3");
        p_contenu.add(QTSX3);
        QTSX3.setBounds(325, 232, 75, 24);

        //---- QTSX4 ----
        QTSX4.setText("@QTSX4@");
        QTSX4.setHorizontalAlignment(SwingConstants.RIGHT);
        QTSX4.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTSX4.setName("QTSX4");
        p_contenu.add(QTSX4);
        QTSX4.setBounds(325, 257, 75, 24);

        //---- QTSX5 ----
        QTSX5.setText("@QTSX5@");
        QTSX5.setHorizontalAlignment(SwingConstants.RIGHT);
        QTSX5.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTSX5.setName("QTSX5");
        p_contenu.add(QTSX5);
        QTSX5.setBounds(325, 282, 75, 24);

        //---- QTSX6 ----
        QTSX6.setText("@QTSX6@");
        QTSX6.setHorizontalAlignment(SwingConstants.RIGHT);
        QTSX6.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTSX6.setName("QTSX6");
        p_contenu.add(QTSX6);
        QTSX6.setBounds(325, 307, 75, 24);

        //---- QTSX7 ----
        QTSX7.setText("@QTSX7@");
        QTSX7.setHorizontalAlignment(SwingConstants.RIGHT);
        QTSX7.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTSX7.setName("QTSX7");
        p_contenu.add(QTSX7);
        QTSX7.setBounds(325, 332, 75, 24);

        //---- QTSX8 ----
        QTSX8.setText("@QTSX8@");
        QTSX8.setHorizontalAlignment(SwingConstants.RIGHT);
        QTSX8.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTSX8.setName("QTSX8");
        p_contenu.add(QTSX8);
        QTSX8.setBounds(325, 357, 75, 24);

        //---- QTSX9 ----
        QTSX9.setText("@QTSX9@");
        QTSX9.setHorizontalAlignment(SwingConstants.RIGHT);
        QTSX9.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTSX9.setName("QTSX9");
        p_contenu.add(QTSX9);
        QTSX9.setBounds(325, 382, 75, 24);

        //---- QTSX10 ----
        QTSX10.setText("@QTSX10@");
        QTSX10.setHorizontalAlignment(SwingConstants.RIGHT);
        QTSX10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTSX10.setName("QTSX10");
        p_contenu.add(QTSX10);
        QTSX10.setBounds(325, 407, 75, 24);

        //---- QTEX1 ----
        QTEX1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTEX1.setHorizontalAlignment(SwingConstants.RIGHT);
        QTEX1.setName("QTEX1");
        p_contenu.add(QTEX1);
        QTEX1.setBounds(405, 180, 80, 29);

        //---- QTEX2 ----
        QTEX2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTEX2.setHorizontalAlignment(SwingConstants.RIGHT);
        QTEX2.setName("QTEX2");
        p_contenu.add(QTEX2);
        QTEX2.setBounds(405, 205, 80, 28);

        //---- QTEX3 ----
        QTEX3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTEX3.setHorizontalAlignment(SwingConstants.RIGHT);
        QTEX3.setName("QTEX3");
        p_contenu.add(QTEX3);
        QTEX3.setBounds(405, 230, 80, 28);

        //---- QTEX4 ----
        QTEX4.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTEX4.setHorizontalAlignment(SwingConstants.RIGHT);
        QTEX4.setName("QTEX4");
        p_contenu.add(QTEX4);
        QTEX4.setBounds(405, 255, 80, 28);

        //---- QTEX5 ----
        QTEX5.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTEX5.setHorizontalAlignment(SwingConstants.RIGHT);
        QTEX5.setName("QTEX5");
        p_contenu.add(QTEX5);
        QTEX5.setBounds(405, 280, 80, 28);

        //---- QTEX6 ----
        QTEX6.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTEX6.setHorizontalAlignment(SwingConstants.RIGHT);
        QTEX6.setName("QTEX6");
        p_contenu.add(QTEX6);
        QTEX6.setBounds(405, 305, 80, 28);

        //---- QTEX7 ----
        QTEX7.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTEX7.setHorizontalAlignment(SwingConstants.RIGHT);
        QTEX7.setName("QTEX7");
        p_contenu.add(QTEX7);
        QTEX7.setBounds(405, 330, 80, 28);

        //---- QTEX8 ----
        QTEX8.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTEX8.setHorizontalAlignment(SwingConstants.RIGHT);
        QTEX8.setName("QTEX8");
        p_contenu.add(QTEX8);
        QTEX8.setBounds(405, 355, 80, 28);

        //---- QTEX9 ----
        QTEX9.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTEX9.setHorizontalAlignment(SwingConstants.RIGHT);
        QTEX9.setName("QTEX9");
        p_contenu.add(QTEX9);
        QTEX9.setBounds(405, 380, 80, 28);

        //---- QTEX10 ----
        QTEX10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        QTEX10.setHorizontalAlignment(SwingConstants.RIGHT);
        QTEX10.setName("QTEX10");
        p_contenu.add(QTEX10);
        QTEX10.setBounds(405, 405, 80, 28);

        //---- DPEX1 ----
        DPEX1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        DPEX1.setName("DPEX1");
        p_contenu.add(DPEX1);
        DPEX1.setBounds(485, 180, 70, 29);

        //---- DPEX2 ----
        DPEX2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        DPEX2.setName("DPEX2");
        p_contenu.add(DPEX2);
        DPEX2.setBounds(485, 205, 70, 29);

        //---- DPEX3 ----
        DPEX3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        DPEX3.setName("DPEX3");
        p_contenu.add(DPEX3);
        DPEX3.setBounds(485, 230, 70, 29);

        //---- DPEX4 ----
        DPEX4.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        DPEX4.setName("DPEX4");
        p_contenu.add(DPEX4);
        DPEX4.setBounds(485, 255, 70, 29);

        //---- DPEX5 ----
        DPEX5.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        DPEX5.setName("DPEX5");
        p_contenu.add(DPEX5);
        DPEX5.setBounds(485, 280, 70, 29);

        //---- DPEX6 ----
        DPEX6.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        DPEX6.setName("DPEX6");
        p_contenu.add(DPEX6);
        DPEX6.setBounds(485, 305, 70, 29);

        //---- DPEX7 ----
        DPEX7.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        DPEX7.setName("DPEX7");
        p_contenu.add(DPEX7);
        DPEX7.setBounds(485, 330, 70, 29);

        //---- DPEX8 ----
        DPEX8.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        DPEX8.setName("DPEX8");
        p_contenu.add(DPEX8);
        DPEX8.setBounds(485, 355, 70, 29);

        //---- DPEX9 ----
        DPEX9.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        DPEX9.setName("DPEX9");
        p_contenu.add(DPEX9);
        DPEX9.setBounds(485, 380, 70, 29);

        //---- DPEX10 ----
        DPEX10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        DPEX10.setName("DPEX10");
        p_contenu.add(DPEX10);
        DPEX10.setBounds(485, 405, 70, 29);

        //---- TP11 ----
        TP11.setName("TP11");
        p_contenu.add(TP11);
        TP11.setBounds(629, 180, 34, TP11.getPreferredSize().height);

        //---- TP12 ----
        TP12.setName("TP12");
        p_contenu.add(TP12);
        TP12.setBounds(629, 205, 34, 28);

        //---- TP13 ----
        TP13.setName("TP13");
        p_contenu.add(TP13);
        TP13.setBounds(629, 230, 34, 28);

        //---- TP14 ----
        TP14.setName("TP14");
        p_contenu.add(TP14);
        TP14.setBounds(629, 255, 34, 28);

        //---- TP15 ----
        TP15.setName("TP15");
        p_contenu.add(TP15);
        TP15.setBounds(629, 280, 34, 28);

        //---- TP16 ----
        TP16.setName("TP16");
        p_contenu.add(TP16);
        TP16.setBounds(629, 305, 34, 28);

        //---- TP17 ----
        TP17.setName("TP17");
        p_contenu.add(TP17);
        TP17.setBounds(629, 330, 34, 28);

        //---- TP18 ----
        TP18.setName("TP18");
        p_contenu.add(TP18);
        TP18.setBounds(629, 355, 34, 28);

        //---- TP19 ----
        TP19.setName("TP19");
        p_contenu.add(TP19);
        TP19.setBounds(629, 380, 34, 28);

        //---- TP110 ----
        TP110.setName("TP110");
        p_contenu.add(TP110);
        TP110.setBounds(629, 405, 34, 28);

        //---- TP21 ----
        TP21.setName("TP21");
        p_contenu.add(TP21);
        TP21.setBounds(665, 180, 34, 28);

        //---- TP22 ----
        TP22.setName("TP22");
        p_contenu.add(TP22);
        TP22.setBounds(665, 205, 34, 28);

        //---- TP23 ----
        TP23.setName("TP23");
        p_contenu.add(TP23);
        TP23.setBounds(665, 230, 34, 28);

        //---- TP24 ----
        TP24.setName("TP24");
        p_contenu.add(TP24);
        TP24.setBounds(665, 255, 34, 28);

        //---- TP25 ----
        TP25.setName("TP25");
        p_contenu.add(TP25);
        TP25.setBounds(665, 280, 34, 28);

        //---- TP26 ----
        TP26.setName("TP26");
        p_contenu.add(TP26);
        TP26.setBounds(665, 305, 34, 28);

        //---- TP27 ----
        TP27.setName("TP27");
        p_contenu.add(TP27);
        TP27.setBounds(665, 330, 34, 28);

        //---- TP28 ----
        TP28.setName("TP28");
        p_contenu.add(TP28);
        TP28.setBounds(665, 355, 34, 28);

        //---- TP29 ----
        TP29.setName("TP29");
        p_contenu.add(TP29);
        TP29.setBounds(665, 380, 34, 28);

        //---- TP210 ----
        TP210.setName("TP210");
        p_contenu.add(TP210);
        TP210.setBounds(665, 405, 34, 28);

        //---- TP31 ----
        TP31.setName("TP31");
        p_contenu.add(TP31);
        TP31.setBounds(700, 180, 34, 28);

        //---- TP32 ----
        TP32.setName("TP32");
        p_contenu.add(TP32);
        TP32.setBounds(700, 205, 34, 28);

        //---- TP33 ----
        TP33.setName("TP33");
        p_contenu.add(TP33);
        TP33.setBounds(700, 230, 34, 28);

        //---- TP34 ----
        TP34.setName("TP34");
        p_contenu.add(TP34);
        TP34.setBounds(700, 255, 34, 28);

        //---- TP35 ----
        TP35.setName("TP35");
        p_contenu.add(TP35);
        TP35.setBounds(700, 280, 34, 28);

        //---- TP36 ----
        TP36.setName("TP36");
        p_contenu.add(TP36);
        TP36.setBounds(700, 305, 34, 28);

        //---- TP37 ----
        TP37.setName("TP37");
        p_contenu.add(TP37);
        TP37.setBounds(700, 330, 34, 28);

        //---- TP38 ----
        TP38.setName("TP38");
        p_contenu.add(TP38);
        TP38.setBounds(700, 355, 34, 28);

        //---- TP39 ----
        TP39.setName("TP39");
        p_contenu.add(TP39);
        TP39.setBounds(700, 380, 34, 28);

        //---- TP310 ----
        TP310.setName("TP310");
        p_contenu.add(TP310);
        TP310.setBounds(700, 405, 34, 28);

        //---- TP41 ----
        TP41.setName("TP41");
        p_contenu.add(TP41);
        TP41.setBounds(735, 180, 34, 28);

        //---- TP42 ----
        TP42.setName("TP42");
        p_contenu.add(TP42);
        TP42.setBounds(735, 205, 34, 28);

        //---- TP43 ----
        TP43.setName("TP43");
        p_contenu.add(TP43);
        TP43.setBounds(735, 230, 34, 28);

        //---- TP44 ----
        TP44.setName("TP44");
        p_contenu.add(TP44);
        TP44.setBounds(735, 255, 34, 28);

        //---- TP45 ----
        TP45.setName("TP45");
        p_contenu.add(TP45);
        TP45.setBounds(735, 280, 34, 28);

        //---- TP46 ----
        TP46.setName("TP46");
        p_contenu.add(TP46);
        TP46.setBounds(735, 305, 34, 28);

        //---- TP47 ----
        TP47.setName("TP47");
        p_contenu.add(TP47);
        TP47.setBounds(735, 330, 34, 28);

        //---- TP48 ----
        TP48.setName("TP48");
        p_contenu.add(TP48);
        TP48.setBounds(735, 355, 34, 28);

        //---- TP49 ----
        TP49.setName("TP49");
        p_contenu.add(TP49);
        TP49.setBounds(735, 380, 34, 28);

        //---- TP410 ----
        TP410.setName("TP410");
        p_contenu.add(TP410);
        TP410.setBounds(735, 405, 34, 28);

        //---- TP51 ----
        TP51.setName("TP51");
        p_contenu.add(TP51);
        TP51.setBounds(770, 180, 34, 28);

        //---- TP52 ----
        TP52.setName("TP52");
        p_contenu.add(TP52);
        TP52.setBounds(770, 205, 34, 28);

        //---- TP53 ----
        TP53.setName("TP53");
        p_contenu.add(TP53);
        TP53.setBounds(770, 230, 34, 28);

        //---- TP54 ----
        TP54.setName("TP54");
        p_contenu.add(TP54);
        TP54.setBounds(770, 255, 34, 28);

        //---- TP55 ----
        TP55.setName("TP55");
        p_contenu.add(TP55);
        TP55.setBounds(770, 280, 34, 28);

        //---- TP56 ----
        TP56.setName("TP56");
        p_contenu.add(TP56);
        TP56.setBounds(770, 305, 34, 28);

        //---- TP57 ----
        TP57.setName("TP57");
        p_contenu.add(TP57);
        TP57.setBounds(770, 330, 34, 28);

        //---- TP58 ----
        TP58.setName("TP58");
        p_contenu.add(TP58);
        TP58.setBounds(770, 355, 34, 28);

        //---- TP59 ----
        TP59.setName("TP59");
        p_contenu.add(TP59);
        TP59.setBounds(770, 380, 34, 28);

        //---- TP510 ----
        TP510.setName("TP510");
        p_contenu.add(TP510);
        TP510.setBounds(770, 405, 34, 28);

        //---- DOS01 ----
        DOS01.setName("DOS01");
        p_contenu.add(DOS01);
        DOS01.setBounds(805, 180, 110, DOS01.getPreferredSize().height);

        //---- DOS02 ----
        DOS02.setName("DOS02");
        p_contenu.add(DOS02);
        DOS02.setBounds(805, 205, 110, 28);

        //---- DOS03 ----
        DOS03.setName("DOS03");
        p_contenu.add(DOS03);
        DOS03.setBounds(805, 230, 110, 28);

        //---- DOS04 ----
        DOS04.setName("DOS04");
        p_contenu.add(DOS04);
        DOS04.setBounds(805, 255, 110, 28);

        //---- DOS05 ----
        DOS05.setName("DOS05");
        p_contenu.add(DOS05);
        DOS05.setBounds(805, 280, 110, 28);

        //---- DOS06 ----
        DOS06.setName("DOS06");
        p_contenu.add(DOS06);
        DOS06.setBounds(805, 305, 110, 28);

        //---- DOS07 ----
        DOS07.setName("DOS07");
        p_contenu.add(DOS07);
        DOS07.setBounds(805, 330, 110, 28);

        //---- DOS08 ----
        DOS08.setName("DOS08");
        p_contenu.add(DOS08);
        DOS08.setBounds(805, 355, 110, 28);

        //---- DOS09 ----
        DOS09.setName("DOS09");
        p_contenu.add(DOS09);
        DOS09.setBounds(805, 380, 110, 28);

        //---- DOS10 ----
        DOS10.setName("DOS10");
        p_contenu.add(DOS10);
        DOS10.setBounds(805, 405, 110, 28);

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");
        p_contenu.add(BT_PGUP);
        BT_PGUP.setBounds(1195, 180, 25, 115);

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setToolTipText("Page suivante");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");
        p_contenu.add(BT_PGDOWN);
        BT_PGDOWN.setBounds(1195, 315, 25, 115);

        //---- P21QPX ----
        P21QPX.setHorizontalAlignment(SwingConstants.RIGHT);
        P21QPX.setName("P21QPX");
        p_contenu.add(P21QPX);
        P21QPX.setBounds(325, 440, 75, P21QPX.getPreferredSize().height);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- P21ART ----
          P21ART.setText("@P21ART@+");
          P21ART.setName("P21ART");
          panel1.add(P21ART);
          P21ART.setBounds(255, 20, 210, P21ART.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setText("Saisie de num\u00e9ros de lots pour l'article");
          OBJ_19.setName("OBJ_19");
          panel1.add(OBJ_19);
          OBJ_19.setBounds(25, 20, 230, 25);

          //---- A1LIB ----
          A1LIB.setText("@A1LIB@");
          A1LIB.setName("A1LIB");
          panel1.add(A1LIB);
          A1LIB.setBounds(475, 20, 259, A1LIB.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("Disponible");
          OBJ_23.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(385, 55, 80, 20);

          //---- WDISX ----
          WDISX.setText("@WDISX@");
          WDISX.setName("WDISX");
          panel1.add(WDISX);
          WDISX.setBounds(475, 50, 84, WDISX.getPreferredSize().height);

          //---- WDISUN ----
          WDISUN.setText("@WDISUN@");
          WDISUN.setName("WDISUN");
          panel1.add(WDISUN);
          WDISUN.setBounds(565, 50, 27, WDISUN.getPreferredSize().height);

          //---- OBJ_42 ----
          OBJ_42.setText("@WLDALA@");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(15, 110, 835, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("Plan palettisation");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(25, 82, 123, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("Conditionnement");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(25, 55, 115, 20);

          //---- A1CNDX ----
          A1CNDX.setText("@A1CNDX@");
          A1CNDX.setHorizontalAlignment(SwingConstants.RIGHT);
          A1CNDX.setName("A1CNDX");
          panel1.add(A1CNDX);
          A1CNDX.setBounds(165, 50, 84, A1CNDX.getPreferredSize().height);

          //---- OBJ_40 ----
          OBJ_40.setText("Longueur");
          OBJ_40.setName("OBJ_40");
          panel1.add(OBJ_40);
          OBJ_40.setBounds(565, 82, 70, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("@A1QT2@");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(475, 80, 63, OBJ_39.getPreferredSize().height);

          //---- WCLPL ----
          WCLPL.setText("@WCLPL@");
          WCLPL.setName("WCLPL");
          panel1.add(WCLPL);
          WCLPL.setBounds(315, 80, 45, WCLPL.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setText("Laize");
          OBJ_38.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_38.setName("OBJ_38");
          panel1.add(OBJ_38);
          OBJ_38.setBounds(420, 82, 45, 20);

          //---- X1PPC ----
          X1PPC.setText("@X1PPC@");
          X1PPC.setName("X1PPC");
          panel1.add(X1PPC);
          X1PPC.setBounds(165, 80, 26, X1PPC.getPreferredSize().height);

          //---- X1PPL ----
          X1PPL.setText("@X1PPL@");
          X1PPL.setName("X1PPL");
          panel1.add(X1PPL);
          X1PPL.setBounds(215, 80, 26, X1PPL.getPreferredSize().height);

          //---- X1PPP ----
          X1PPP.setText("@X1PPP@");
          X1PPP.setName("X1PPP");
          panel1.add(X1PPP);
          X1PPP.setBounds(263, 80, 26, X1PPP.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setText("@A1UNL@");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(255, 50, 34, OBJ_28.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("@A1UNL@");
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(370, 82, 20, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("=");
          OBJ_35.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_35.setName("OBJ_35");
          panel1.add(OBJ_35);
          OBJ_35.setBounds(290, 82, 25, 20);

          //---- OBJ_31 ----
          OBJ_31.setText("x");
          OBJ_31.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(190, 82, 20, 20);

          //---- OBJ_33 ----
          OBJ_33.setText("x");
          OBJ_33.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_33.setName("OBJ_33");
          panel1.add(OBJ_33);
          OBJ_33.setBounds(245, 82, 15, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("@A1QT3@");
          OBJ_41.setName("OBJ_41");
          panel1.add(OBJ_41);
          OBJ_41.setBounds(630, 80, 49, OBJ_41.getPreferredSize().height);

          //---- label9 ----
          label9.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label9.setText("label1");
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(585, 140, 50, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 1210, 145);

        //---- LD3X1 ----
        LD3X1.setName("LD3X1");
        p_contenu.add(LD3X1);
        LD3X1.setBounds(915, 180, 274, LD3X1.getPreferredSize().height);

        //---- D1EX1 ----
        D1EX1.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        D1EX1.setName("D1EX1");
        p_contenu.add(D1EX1);
        D1EX1.setBounds(557, 180, 70, D1EX1.getPreferredSize().height);

        //---- P21QTX ----
        P21QTX.setHorizontalAlignment(SwingConstants.RIGHT);
        P21QTX.setName("P21QTX");
        p_contenu.add(P21QTX);
        P21QTX.setBounds(405, 440, 80, P21QTX.getPreferredSize().height);

        //---- LD3X2 ----
        LD3X2.setName("LD3X2");
        p_contenu.add(LD3X2);
        LD3X2.setBounds(915, 205, 274, LD3X2.getPreferredSize().height);

        //---- LD3X3 ----
        LD3X3.setName("LD3X3");
        p_contenu.add(LD3X3);
        LD3X3.setBounds(915, 230, 274, LD3X3.getPreferredSize().height);

        //---- LD3X4 ----
        LD3X4.setName("LD3X4");
        p_contenu.add(LD3X4);
        LD3X4.setBounds(915, 255, 274, LD3X4.getPreferredSize().height);

        //---- LD3X5 ----
        LD3X5.setName("LD3X5");
        p_contenu.add(LD3X5);
        LD3X5.setBounds(915, 280, 274, LD3X5.getPreferredSize().height);

        //---- LD3X6 ----
        LD3X6.setName("LD3X6");
        p_contenu.add(LD3X6);
        LD3X6.setBounds(915, 305, 274, LD3X6.getPreferredSize().height);

        //---- LD3X7 ----
        LD3X7.setName("LD3X7");
        p_contenu.add(LD3X7);
        LD3X7.setBounds(915, 330, 274, LD3X7.getPreferredSize().height);

        //---- LD3X8 ----
        LD3X8.setName("LD3X8");
        p_contenu.add(LD3X8);
        LD3X8.setBounds(915, 355, 274, LD3X8.getPreferredSize().height);

        //---- LD3X9 ----
        LD3X9.setName("LD3X9");
        p_contenu.add(LD3X9);
        LD3X9.setBounds(915, 380, 274, LD3X9.getPreferredSize().height);

        //---- LD3X10 ----
        LD3X10.setName("LD3X10");
        p_contenu.add(LD3X10);
        LD3X10.setBounds(915, 405, 274, LD3X10.getPreferredSize().height);

        //---- D1EX2 ----
        D1EX2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        D1EX2.setName("D1EX2");
        p_contenu.add(D1EX2);
        D1EX2.setBounds(557, 205, 70, D1EX2.getPreferredSize().height);

        //---- D1EX3 ----
        D1EX3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        D1EX3.setName("D1EX3");
        p_contenu.add(D1EX3);
        D1EX3.setBounds(557, 230, 70, D1EX3.getPreferredSize().height);

        //---- D1EX4 ----
        D1EX4.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        D1EX4.setName("D1EX4");
        p_contenu.add(D1EX4);
        D1EX4.setBounds(557, 255, 70, D1EX4.getPreferredSize().height);

        //---- D1EX5 ----
        D1EX5.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        D1EX5.setName("D1EX5");
        p_contenu.add(D1EX5);
        D1EX5.setBounds(557, 280, 70, D1EX5.getPreferredSize().height);

        //---- D1EX6 ----
        D1EX6.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        D1EX6.setName("D1EX6");
        p_contenu.add(D1EX6);
        D1EX6.setBounds(557, 305, 70, D1EX6.getPreferredSize().height);

        //---- D1EX7 ----
        D1EX7.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        D1EX7.setName("D1EX7");
        p_contenu.add(D1EX7);
        D1EX7.setBounds(557, 330, 70, D1EX7.getPreferredSize().height);

        //---- D1EX8 ----
        D1EX8.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        D1EX8.setName("D1EX8");
        p_contenu.add(D1EX8);
        D1EX8.setBounds(557, 355, 70, D1EX8.getPreferredSize().height);

        //---- D1EX9 ----
        D1EX9.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        D1EX9.setName("D1EX9");
        p_contenu.add(D1EX9);
        D1EX9.setBounds(557, 380, 70, D1EX9.getPreferredSize().height);

        //---- D1EX10 ----
        D1EX10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        D1EX10.setName("D1EX10");
        p_contenu.add(D1EX10);
        D1EX10.setBounds(557, 405, 70, D1EX10.getPreferredSize().height);

        //---- label2 ----
        label2.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
        label2.setText("label1");
        label2.setName("label2");
        p_contenu.add(label2);
        label2.setBounds(65, 160, 215, 20);

        //---- label4 ----
        label4.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
        label4.setText("label1");
        label4.setName("label4");
        p_contenu.add(label4);
        label4.setBounds(325, 160, 75, 20);

        //---- label5 ----
        label5.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
        label5.setText("label1");
        label5.setName("label5");
        p_contenu.add(label5);
        label5.setBounds(405, 160, 75, 20);

        //---- label7 ----
        label7.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
        label7.setText("label1");
        label7.setName("label7");
        p_contenu.add(label7);
        label7.setBounds(480, 160, 80, 20);

        //---- label8 ----
        label8.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
        label8.setText("label1");
        label8.setName("label8");
        p_contenu.add(label8);
        label8.setBounds(560, 160, 80, 20);

        //---- label11 ----
        label11.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
        label11.setText("label1");
        label11.setName("label11");
        p_contenu.add(label11);
        label11.setBounds(770, 160, 415, 20);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("D\u00e9coupe");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Annulation de d\u00e9coupe");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
      BTD.addSeparator();

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_contenu;
  private RiZoneSortie NUM1;
  private XRiTextField ENL101;
  private XRiTextField ENL201;
  private RiZoneSortie NUM2;
  private XRiTextField ENL102;
  private XRiTextField ENL202;
  private RiZoneSortie NUM3;
  private XRiTextField ENL103;
  private XRiTextField ENL203;
  private RiZoneSortie NUM4;
  private XRiTextField ENL104;
  private XRiTextField ENL204;
  private RiZoneSortie NUM5;
  private XRiTextField ENL105;
  private XRiTextField ENL205;
  private RiZoneSortie NUM6;
  private XRiTextField ENL106;
  private XRiTextField ENL206;
  private RiZoneSortie NUM7;
  private XRiTextField ENL107;
  private XRiTextField ENL207;
  private RiZoneSortie NUM8;
  private XRiTextField ENL108;
  private XRiTextField ENL208;
  private RiZoneSortie NUM9;
  private XRiTextField ENL109;
  private XRiTextField ENL209;
  private RiZoneSortie NUM10;
  private XRiTextField ENL110;
  private XRiTextField ENL210;
  private RiZoneSortie QTSX1;
  private RiZoneSortie QTSX2;
  private RiZoneSortie QTSX3;
  private RiZoneSortie QTSX4;
  private RiZoneSortie QTSX5;
  private RiZoneSortie QTSX6;
  private RiZoneSortie QTSX7;
  private RiZoneSortie QTSX8;
  private RiZoneSortie QTSX9;
  private RiZoneSortie QTSX10;
  private XRiTextField QTEX1;
  private XRiTextField QTEX2;
  private XRiTextField QTEX3;
  private XRiTextField QTEX4;
  private XRiTextField QTEX5;
  private XRiTextField QTEX6;
  private XRiTextField QTEX7;
  private XRiTextField QTEX8;
  private XRiTextField QTEX9;
  private XRiTextField QTEX10;
  private XRiTextField DPEX1;
  private XRiTextField DPEX2;
  private XRiTextField DPEX3;
  private XRiTextField DPEX4;
  private XRiTextField DPEX5;
  private XRiTextField DPEX6;
  private XRiTextField DPEX7;
  private XRiTextField DPEX8;
  private XRiTextField DPEX9;
  private XRiTextField DPEX10;
  private XRiTextField TP11;
  private XRiTextField TP12;
  private XRiTextField TP13;
  private XRiTextField TP14;
  private XRiTextField TP15;
  private XRiTextField TP16;
  private XRiTextField TP17;
  private XRiTextField TP18;
  private XRiTextField TP19;
  private XRiTextField TP110;
  private XRiTextField TP21;
  private XRiTextField TP22;
  private XRiTextField TP23;
  private XRiTextField TP24;
  private XRiTextField TP25;
  private XRiTextField TP26;
  private XRiTextField TP27;
  private XRiTextField TP28;
  private XRiTextField TP29;
  private XRiTextField TP210;
  private XRiTextField TP31;
  private XRiTextField TP32;
  private XRiTextField TP33;
  private XRiTextField TP34;
  private XRiTextField TP35;
  private XRiTextField TP36;
  private XRiTextField TP37;
  private XRiTextField TP38;
  private XRiTextField TP39;
  private XRiTextField TP310;
  private XRiTextField TP41;
  private XRiTextField TP42;
  private XRiTextField TP43;
  private XRiTextField TP44;
  private XRiTextField TP45;
  private XRiTextField TP46;
  private XRiTextField TP47;
  private XRiTextField TP48;
  private XRiTextField TP49;
  private XRiTextField TP410;
  private XRiTextField TP51;
  private XRiTextField TP52;
  private XRiTextField TP53;
  private XRiTextField TP54;
  private XRiTextField TP55;
  private XRiTextField TP56;
  private XRiTextField TP57;
  private XRiTextField TP58;
  private XRiTextField TP59;
  private XRiTextField TP510;
  private XRiTextField DOS01;
  private XRiTextField DOS02;
  private XRiTextField DOS03;
  private XRiTextField DOS04;
  private XRiTextField DOS05;
  private XRiTextField DOS06;
  private XRiTextField DOS07;
  private XRiTextField DOS08;
  private XRiTextField DOS09;
  private XRiTextField DOS10;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiTextField P21QPX;
  private JPanel panel1;
  private RiZoneSortie P21ART;
  private JLabel OBJ_19;
  private RiZoneSortie A1LIB;
  private JLabel OBJ_23;
  private RiZoneSortie WDISX;
  private RiZoneSortie WDISUN;
  private JLabel OBJ_42;
  private JLabel OBJ_29;
  private JLabel OBJ_26;
  private RiZoneSortie A1CNDX;
  private JLabel OBJ_40;
  private RiZoneSortie OBJ_39;
  private RiZoneSortie WCLPL;
  private JLabel OBJ_38;
  private RiZoneSortie X1PPC;
  private RiZoneSortie X1PPL;
  private RiZoneSortie X1PPP;
  private RiZoneSortie OBJ_28;
  private JLabel OBJ_37;
  private JLabel OBJ_35;
  private JLabel OBJ_31;
  private JLabel OBJ_33;
  private RiZoneSortie OBJ_41;
  private JLabel label9;
  private XRiTextField LD3X1;
  private XRiTextField D1EX1;
  private XRiTextField P21QTX;
  private XRiTextField LD3X2;
  private XRiTextField LD3X3;
  private XRiTextField LD3X4;
  private XRiTextField LD3X5;
  private XRiTextField LD3X6;
  private XRiTextField LD3X7;
  private XRiTextField LD3X8;
  private XRiTextField LD3X9;
  private XRiTextField LD3X10;
  private XRiTextField D1EX2;
  private XRiTextField D1EX3;
  private XRiTextField D1EX4;
  private XRiTextField D1EX5;
  private XRiTextField D1EX6;
  private XRiTextField D1EX7;
  private XRiTextField D1EX8;
  private XRiTextField D1EX9;
  private XRiTextField D1EX10;
  private JLabel label2;
  private JLabel label4;
  private JLabel label5;
  private JLabel label7;
  private JLabel label8;
  private JLabel label11;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
