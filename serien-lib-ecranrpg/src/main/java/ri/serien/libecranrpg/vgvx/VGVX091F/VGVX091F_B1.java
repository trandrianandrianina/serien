
package ri.serien.libecranrpg.vgvx.VGVX091F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.imagearticle.ImageArticle;
import ri.serien.libcommun.gescom.commun.imagearticle.ListeImageArticle;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.image.OutilImage;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.metier.referentiel.article.snphotoarticle.DialogueDiaporamaPhoto;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX091F_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private ListeImageArticle listeImageArticle = new ListeImageArticle();
  private ImageArticle logoArticle = null;
  private ImageIcon logoArticleAffichage = null;
  private String cheminIcon = null;
  
  public VGVX091F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    label13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNLIB@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB1@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB2@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB3@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDTDIS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // riBoutonInfo1.gererAffichageV03F(lexique.isTrue("19"),lexique.getConvertInfosFiche(lexique.HostFieldGetData("V03F").trim()));
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    // if(riSousMenu_bt_export instanceof RiSousMenu_bt)
    // lexique.setNomFichierTableur( lexique.HostFieldGetData("TITPG1").trim() + " " +
    // lexique.HostFieldGetData("TITPG2").trim());
    
    
    
    K09QT1.setVisible(lexique.isTrue("60") && lexique.isTrue(""));
    OBJ_21.setVisible(lexique.isPresent("K09QT1"));
    
    // TODO Icones
    // VAL.setIcon(lexique.getImage("images/OK.png", true));
    // OBJ_32.setIcon(lexique.getImage("images/retour.png", true));
    
    // Vignette photo ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    cheminIcon = (lexique.HostFieldGetData("RACINE").trim() + lexique.HostFieldGetData("CHEM1").trim()
        + lexique.HostFieldGetData("A1ART").trim() + "&").replaceAll("/", "&");
    // listeImageArticle = new ListeImageArticle().charger(ManagerSessionClient.getInstance().getEnvUser(), cheminIcon);
    
    int i = 0;
    boolean trouve = false;
    
    if (listeImageArticle != null) {
      while ((trouve == false) && (i < listeImageArticle.size())) {
        logoArticle = listeImageArticle.get(i);
        if (logoArticle != null) {
          logoArticleAffichage = OutilImage.reduireIcone(logoArticle.getIconeImage(), photo.getWidth(), photo.getHeight());
          trouve = true;
        }
        i++;
      }
    }
    
    // On affiche la photo si trouvée
    if (logoArticleAffichage != null) {
      photo.setIcon(logoArticleAffichage);
      photo.setToolTipText("<html><img src=\""
          + ManagerSessionClient.getInstance().getUrlImage(logoArticle.getId().getCheminDAccesImageArticle().toString(), false) + "\">");
    }
    else {
      photo.setIcon(lexique.chargerImage("images/pas_photo.png", true));
    }
    
    photo.setEnabled(true);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Saisie complémentaire"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void photoActionPerformed(ActionEvent e) {
    DialogueDiaporamaPhoto.afficher(ManagerSessionClient.getInstance().getEnvUser(), cheminIcon);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    JPopupMenu pop = lexique.ViewerDoc(cheminIcon);
    if (pop != null) {
      pop.show(photo, 0, photo.getHeight());
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_23 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_25 = new JLabel();
    K10PVB = new XRiTextField();
    K10PVN = new XRiTextField();
    OBJ_19 = new JLabel();
    WQTEX = new XRiTextField();
    K09QT1 = new XRiTextField();
    OBJ_21 = new JLabel();
    K10RE1 = new XRiTextField();
    K10RE2 = new XRiTextField();
    K10RE3 = new XRiTextField();
    label1 = new JLabel();
    label4 = new JLabel();
    label9 = new JLabel();
    WSTK8 = new XRiTextField();
    label10 = new JLabel();
    OBJ_20 = new JLabel();
    WCNDX = new XRiTextField();
    A1UNV = new XRiTextField();
    WUNIT = new XRiTextField();
    label13 = new RiZoneSortie();
    A1UNVS = new XRiTextField();
    panel3 = new JPanel();
    label3 = new RiZoneSortie();
    label5 = new RiZoneSortie();
    label6 = new RiZoneSortie();
    label7 = new RiZoneSortie();
    A1FAM = new XRiTextField();
    photo = new JButton();
    A1ART = new XRiTextField();
    label2 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    popupMenu1 = new JPopupMenu();
    menuItem1 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(905, 330));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");
            
            // ---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);
          
          // ======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");
            
            // ---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);
          
          // ======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");
            
            // ---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);
          
          // ======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");
            
            // ---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);
          
          // ======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");
            
            // ---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
          
          // ======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");
            
            // ---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);
          
          // ======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");
            
            // ---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);
          
          // ======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");
            
            // ---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);
          
          // ======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");
            
            // ---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- OBJ_23 ----
          OBJ_23.setText("Prix de base");
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(20, 84, 96, 20);
          
          // ---- OBJ_29 ----
          OBJ_29.setText("Ou  prix net");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(20, 114, 85, 20);
          
          // ---- OBJ_25 ----
          OBJ_25.setText("Remises");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(345, 84, 65, 20);
          
          // ---- K10PVB ----
          K10PVB.setComponentPopupMenu(BTD);
          K10PVB.setHorizontalAlignment(SwingConstants.RIGHT);
          K10PVB.setName("K10PVB");
          panel1.add(K10PVB);
          K10PVB.setBounds(125, 80, 90, K10PVB.getPreferredSize().height);
          
          // ---- K10PVN ----
          K10PVN.setComponentPopupMenu(BTD);
          K10PVN.setName("K10PVN");
          panel1.add(K10PVN);
          K10PVN.setBounds(125, 110, 90, K10PVN.getPreferredSize().height);
          
          // ---- OBJ_19 ----
          OBJ_19.setText("Quantit\u00e9");
          OBJ_19.setName("OBJ_19");
          panel1.add(OBJ_19);
          OBJ_19.setBounds(20, 24, 70, 20);
          
          // ---- WQTEX ----
          WQTEX.setComponentPopupMenu(BTD);
          WQTEX.setHorizontalAlignment(SwingConstants.RIGHT);
          WQTEX.setName("WQTEX");
          panel1.add(WQTEX);
          WQTEX.setBounds(125, 20, 90, WQTEX.getPreferredSize().height);
          
          // ---- K09QT1 ----
          K09QT1.setComponentPopupMenu(BTD);
          K09QT1.setName("K09QT1");
          panel1.add(K09QT1);
          K09QT1.setBounds(410, 20, 82, K09QT1.getPreferredSize().height);
          
          // ---- OBJ_21 ----
          OBJ_21.setText("Pi\u00e8ces");
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(360, 24, 50, 20);
          
          // ---- K10RE1 ----
          K10RE1.setComponentPopupMenu(BTD);
          K10RE1.setName("K10RE1");
          panel1.add(K10RE1);
          K10RE1.setBounds(410, 80, 50, K10RE1.getPreferredSize().height);
          
          // ---- K10RE2 ----
          K10RE2.setComponentPopupMenu(BTD);
          K10RE2.setName("K10RE2");
          panel1.add(K10RE2);
          K10RE2.setBounds(460, 80, 50, K10RE2.getPreferredSize().height);
          
          // ---- K10RE3 ----
          K10RE3.setComponentPopupMenu(BTD);
          K10RE3.setName("K10RE3");
          panel1.add(K10RE3);
          K10RE3.setBounds(510, 80, 50, K10RE3.getPreferredSize().height);
          
          // ---- label1 ----
          label1.setText("Article");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(20, 144, 85, 20);
          
          // ---- label4 ----
          label4.setText("Famille");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(20, 174, 60, 20);
          
          // ---- label9 ----
          label9.setText("En stock");
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(20, 204, 75, 20);
          
          // ---- WSTK8 ----
          WSTK8.setHorizontalAlignment(SwingConstants.RIGHT);
          WSTK8.setName("WSTK8");
          panel1.add(WSTK8);
          WSTK8.setBounds(125, 200, 90, WSTK8.getPreferredSize().height);
          
          // ---- label10 ----
          label10.setText("Disponible le");
          label10.setName("label10");
          panel1.add(label10);
          label10.setBounds(20, 234, 90, 20);
          
          // ---- OBJ_20 ----
          OBJ_20.setText("Conditionnement");
          OBJ_20.setName("OBJ_20");
          panel1.add(OBJ_20);
          OBJ_20.setBounds(20, 54, 105, 20);
          
          // ---- WCNDX ----
          WCNDX.setComponentPopupMenu(BTD);
          WCNDX.setHorizontalAlignment(SwingConstants.RIGHT);
          WCNDX.setName("WCNDX");
          panel1.add(WCNDX);
          WCNDX.setBounds(125, 50, 90, 28);
          
          // ---- A1UNV ----
          A1UNV.setComponentPopupMenu(BTD);
          A1UNV.setName("A1UNV");
          panel1.add(A1UNV);
          A1UNV.setBounds(215, 50, 20, 28);
          
          // ---- WUNIT ----
          WUNIT.setComponentPopupMenu(BTD);
          WUNIT.setName("WUNIT");
          panel1.add(WUNIT);
          WUNIT.setBounds(215, 20, 20, 28);
          
          // ---- label13 ----
          label13.setText("@UNLIB@");
          label13.setName("label13");
          panel1.add(label13);
          label13.setBounds(240, 24, 105, 20);
          
          // ---- A1UNVS ----
          A1UNVS.setComponentPopupMenu(BTD);
          A1UNVS.setName("A1UNVS");
          panel1.add(A1UNVS);
          A1UNVS.setBounds(215, 200, 20, A1UNVS.getPreferredSize().height);
          
          // ======== panel3 ========
          {
            panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);
            
            // ---- label3 ----
            label3.setText("@A1LIB@");
            label3.setName("label3");
            panel3.add(label3);
            label3.setBounds(15, 10, 315, label3.getPreferredSize().height);
            
            // ---- label5 ----
            label5.setText("@A1LB1@");
            label5.setName("label5");
            panel3.add(label5);
            label5.setBounds(15, 40, 315, label5.getPreferredSize().height);
            
            // ---- label6 ----
            label6.setText("@A1LB2@");
            label6.setName("label6");
            panel3.add(label6);
            label6.setBounds(15, 70, 315, label6.getPreferredSize().height);
            
            // ---- label7 ----
            label7.setText("@A1LB3@");
            label7.setName("label7");
            panel3.add(label7);
            label7.setBounds(15, 100, 315, label7.getPreferredSize().height);
          }
          panel1.add(panel3);
          panel3.setBounds(325, 130, 345, 135);
          
          // ---- A1FAM ----
          A1FAM.setComponentPopupMenu(BTD);
          A1FAM.setName("A1FAM");
          panel1.add(A1FAM);
          A1FAM.setBounds(125, 170, 40, A1FAM.getPreferredSize().height);
          
          // ---- photo ----
          photo.setForeground(Color.darkGray);
          photo.setBorder(null);
          photo.setComponentPopupMenu(popupMenu1);
          photo.setContentAreaFilled(false);
          photo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          photo.setName("photo");
          photo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              photoActionPerformed(e);
            }
          });
          panel1.add(photo);
          photo.setBounds(585, 25, 85, 85);
          
          // ---- A1ART ----
          A1ART.setName("A1ART");
          panel1.add(A1ART);
          A1ART.setBounds(125, 140, 172, A1ART.getPreferredSize().height);
          
          // ---- label2 ----
          label2.setComponentPopupMenu(BTD);
          label2.setBorder(new BevelBorder(BevelBorder.LOWERED));
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setHorizontalTextPosition(SwingConstants.CENTER);
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setText("@WDTDIS@");
          label2.setAlignmentX(2.0F);
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(127, 231, 70, label2.getPreferredSize().height);
        }
        p_contenu.add(panel1);
        panel1.setBounds(25, 20, 690, 290);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    
    // ======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");
      
      // ---- menuItem1 ----
      menuItem1.setText("voir tous les documents");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem1);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_23;
  private JLabel OBJ_29;
  private JLabel OBJ_25;
  private XRiTextField K10PVB;
  private XRiTextField K10PVN;
  private JLabel OBJ_19;
  private XRiTextField WQTEX;
  private XRiTextField K09QT1;
  private JLabel OBJ_21;
  private XRiTextField K10RE1;
  private XRiTextField K10RE2;
  private XRiTextField K10RE3;
  private JLabel label1;
  private JLabel label4;
  private JLabel label9;
  private XRiTextField WSTK8;
  private JLabel label10;
  private JLabel OBJ_20;
  private XRiTextField WCNDX;
  private XRiTextField A1UNV;
  private XRiTextField WUNIT;
  private RiZoneSortie label13;
  private XRiTextField A1UNVS;
  private JPanel panel3;
  private RiZoneSortie label3;
  private RiZoneSortie label5;
  private RiZoneSortie label6;
  private RiZoneSortie label7;
  private XRiTextField A1FAM;
  private JButton photo;
  private XRiTextField A1ART;
  private RiZoneSortie label2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
