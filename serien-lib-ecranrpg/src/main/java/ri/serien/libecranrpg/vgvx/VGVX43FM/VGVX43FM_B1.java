
package ri.serien.libecranrpg.vgvx.VGVX43FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX43FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX43FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL01@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL02@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL03@")).trim());
    OBJ_93.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL04@")).trim());
    OBJ_95.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL05@")).trim());
    OBJ_97.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL06@")).trim());
    OBJ_99.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL07@")).trim());
    OBJ_102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL08@")).trim());
    OBJ_104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL09@")).trim());
    OBJ_106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL10@")).trim());
    OBJ_108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL11@")).trim());
    OBJ_110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL12@")).trim());
    OBJ_112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL13@")).trim());
    OBJ_114.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL14@")).trim());
    OBJ_116.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL15@")).trim());
    OBJ_126.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL16@")).trim());
    OBJ_127.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL17@")).trim());
    OBJ_128.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL18@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP2@")).trim());
    OBJ_92.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP3@")).trim());
    OBJ_94.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP4@")).trim());
    OBJ_96.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP5@")).trim());
    OBJ_98.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP6@")).trim());
    OBJ_100.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP7@")).trim());
    OBJ_103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP8@")).trim());
    OBJ_105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP9@")).trim());
    OBJ_107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP10@")).trim());
    OBJ_109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP11@")).trim());
    OBJ_111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP12@")).trim());
    OBJ_113.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP13@")).trim());
    OBJ_115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP14@")).trim());
    OBJ_117.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP15@")).trim());
    OBJ_119.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP16@")).trim());
    OBJ_121.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP17@")).trim());
    OBJ_123.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP18@")).trim());
    OBJ_101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T01@")).trim());
    OBJ_118.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T16@")).trim());
    OBJ_120.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T17@")).trim());
    OBJ_122.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T18@")).trim());
    EBART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBART@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_122.setVisible(!lexique.HostFieldGetData("TP18").trim().equalsIgnoreCase("B"));
    OBJ_120.setVisible(!lexique.HostFieldGetData("TP17").trim().equalsIgnoreCase("B"));
    OBJ_118.setVisible(!lexique.HostFieldGetData("TP16").trim().equalsIgnoreCase("B"));
    OBJ_101.setVisible(!lexique.HostFieldGetData("TP01").trim().equalsIgnoreCase("B"));
    OBJ_123.setVisible(EBZP18.isVisible());
    OBJ_121.setVisible(EBZP17.isVisible());
    OBJ_119.setVisible(EBZP16.isVisible());
    OBJ_117.setVisible(EBZP15.isVisible());
    OBJ_115.setVisible(EBZP14.isVisible());
    OBJ_113.setVisible(EBZP13.isVisible());
    OBJ_111.setVisible(EBZP12.isVisible());
    OBJ_109.setVisible(EBZP11.isVisible());
    OBJ_107.setVisible(EBZP10.isVisible());
    OBJ_105.setVisible(EBZP9.isVisible());
    OBJ_103.setVisible(EBZP8.isVisible());
    OBJ_100.setVisible(EBZP7.isVisible());
    OBJ_98.setVisible(EBZP6.isVisible());
    OBJ_96.setVisible(EBZP5.isVisible());
    OBJ_94.setVisible(EBZP4.isVisible());
    OBJ_92.setVisible(EBZP3.isVisible());
    OBJ_90.setVisible(EBZP2.isVisible());
    OBJ_88.setVisible(EBZP1.isVisible());
    OBJ_128.setVisible(EBZP18.isVisible());
    OBJ_127.setVisible(EBZP17.isVisible());
    OBJ_126.setVisible(EBZP16.isVisible());
    OBJ_116.setVisible(EBZP15.isVisible());
    OBJ_114.setVisible(EBZP14.isVisible());
    OBJ_112.setVisible(EBZP13.isVisible());
    OBJ_110.setVisible(EBZP12.isVisible());
    OBJ_108.setVisible(EBZP11.isVisible());
    OBJ_106.setVisible(EBZP10.isVisible());
    OBJ_104.setVisible(EBZP9.isVisible());
    OBJ_102.setVisible(EBZP8.isVisible());
    OBJ_99.setVisible(EBZP7.isVisible());
    OBJ_97.setVisible(EBZP6.isVisible());
    OBJ_95.setVisible(EBZP5.isVisible());
    OBJ_93.setVisible(EBZP4.isVisible());
    OBJ_91.setVisible(EBZP3.isVisible());
    OBJ_89.setVisible(EBZP2.isVisible());
    OBJ_87.setVisible(EBZP1.isVisible());
    OBJ_27.setVisible(lexique.isPresent("A1LIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Extension fiche article"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvx43"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_22 = new JPanel();
    EBZP1 = new XRiTextField();
    EBZP2 = new XRiTextField();
    EBZP3 = new XRiTextField();
    EBZP4 = new XRiTextField();
    EBZP5 = new XRiTextField();
    EBZP6 = new XRiTextField();
    EBZP7 = new XRiTextField();
    EBZP8 = new XRiTextField();
    EBZP9 = new XRiTextField();
    EBZP10 = new XRiTextField();
    EBZP11 = new XRiTextField();
    EBZP12 = new XRiTextField();
    EBZP13 = new XRiTextField();
    EBZP14 = new XRiTextField();
    EBZP15 = new XRiTextField();
    EBZP16 = new XRiTextField();
    EBZP17 = new XRiTextField();
    EBZP18 = new XRiTextField();
    OBJ_87 = new RiZoneSortie();
    OBJ_89 = new RiZoneSortie();
    OBJ_91 = new RiZoneSortie();
    OBJ_93 = new RiZoneSortie();
    OBJ_95 = new RiZoneSortie();
    OBJ_97 = new RiZoneSortie();
    OBJ_99 = new RiZoneSortie();
    OBJ_102 = new RiZoneSortie();
    OBJ_104 = new RiZoneSortie();
    OBJ_106 = new RiZoneSortie();
    OBJ_108 = new RiZoneSortie();
    OBJ_110 = new RiZoneSortie();
    OBJ_112 = new RiZoneSortie();
    OBJ_114 = new RiZoneSortie();
    OBJ_116 = new RiZoneSortie();
    OBJ_126 = new RiZoneSortie();
    OBJ_127 = new RiZoneSortie();
    OBJ_128 = new RiZoneSortie();
    OBJ_88 = new RiZoneSortie();
    OBJ_90 = new RiZoneSortie();
    OBJ_92 = new RiZoneSortie();
    OBJ_94 = new RiZoneSortie();
    OBJ_96 = new RiZoneSortie();
    OBJ_98 = new RiZoneSortie();
    OBJ_100 = new RiZoneSortie();
    OBJ_103 = new RiZoneSortie();
    OBJ_105 = new RiZoneSortie();
    OBJ_107 = new RiZoneSortie();
    OBJ_109 = new RiZoneSortie();
    OBJ_111 = new RiZoneSortie();
    OBJ_113 = new RiZoneSortie();
    OBJ_115 = new RiZoneSortie();
    OBJ_117 = new RiZoneSortie();
    OBJ_119 = new RiZoneSortie();
    OBJ_121 = new RiZoneSortie();
    OBJ_123 = new RiZoneSortie();
    OBJ_101 = new JLabel();
    OBJ_118 = new JLabel();
    OBJ_120 = new JLabel();
    OBJ_122 = new JLabel();
    barre_tete = new JMenuBar();
    panel2 = new JPanel();
    OBJ_86 = new JLabel();
    EBART = new RiZoneSortie();
    OBJ_27 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(905, 555));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 330));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Recherche multi-crit\u00e8res");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== OBJ_22 ========
        {
          OBJ_22.setOpaque(false);
          OBJ_22.setName("OBJ_22");
          OBJ_22.setLayout(null);

          //---- EBZP1 ----
          EBZP1.setComponentPopupMenu(BTD);
          EBZP1.setName("EBZP1");
          OBJ_22.add(EBZP1);
          EBZP1.setBounds(224, 0, 300, EBZP1.getPreferredSize().height);

          //---- EBZP2 ----
          EBZP2.setComponentPopupMenu(BTD);
          EBZP2.setName("EBZP2");
          OBJ_22.add(EBZP2);
          EBZP2.setBounds(224, 27, 300, EBZP2.getPreferredSize().height);

          //---- EBZP3 ----
          EBZP3.setComponentPopupMenu(BTD);
          EBZP3.setName("EBZP3");
          OBJ_22.add(EBZP3);
          EBZP3.setBounds(224, 54, 300, EBZP3.getPreferredSize().height);

          //---- EBZP4 ----
          EBZP4.setComponentPopupMenu(BTD);
          EBZP4.setName("EBZP4");
          OBJ_22.add(EBZP4);
          EBZP4.setBounds(224, 81, 300, EBZP4.getPreferredSize().height);

          //---- EBZP5 ----
          EBZP5.setComponentPopupMenu(BTD);
          EBZP5.setName("EBZP5");
          OBJ_22.add(EBZP5);
          EBZP5.setBounds(224, 108, 300, EBZP5.getPreferredSize().height);

          //---- EBZP6 ----
          EBZP6.setComponentPopupMenu(BTD);
          EBZP6.setName("EBZP6");
          OBJ_22.add(EBZP6);
          EBZP6.setBounds(224, 135, 300, EBZP6.getPreferredSize().height);

          //---- EBZP7 ----
          EBZP7.setComponentPopupMenu(BTD);
          EBZP7.setName("EBZP7");
          OBJ_22.add(EBZP7);
          EBZP7.setBounds(224, 162, 300, EBZP7.getPreferredSize().height);

          //---- EBZP8 ----
          EBZP8.setComponentPopupMenu(BTD);
          EBZP8.setName("EBZP8");
          OBJ_22.add(EBZP8);
          EBZP8.setBounds(224, 189, 300, EBZP8.getPreferredSize().height);

          //---- EBZP9 ----
          EBZP9.setComponentPopupMenu(BTD);
          EBZP9.setName("EBZP9");
          OBJ_22.add(EBZP9);
          EBZP9.setBounds(224, 216, 300, EBZP9.getPreferredSize().height);

          //---- EBZP10 ----
          EBZP10.setComponentPopupMenu(BTD);
          EBZP10.setName("EBZP10");
          OBJ_22.add(EBZP10);
          EBZP10.setBounds(224, 243, 300, EBZP10.getPreferredSize().height);

          //---- EBZP11 ----
          EBZP11.setComponentPopupMenu(BTD);
          EBZP11.setName("EBZP11");
          OBJ_22.add(EBZP11);
          EBZP11.setBounds(224, 270, 300, EBZP11.getPreferredSize().height);

          //---- EBZP12 ----
          EBZP12.setComponentPopupMenu(BTD);
          EBZP12.setName("EBZP12");
          OBJ_22.add(EBZP12);
          EBZP12.setBounds(224, 297, 300, EBZP12.getPreferredSize().height);

          //---- EBZP13 ----
          EBZP13.setComponentPopupMenu(BTD);
          EBZP13.setName("EBZP13");
          OBJ_22.add(EBZP13);
          EBZP13.setBounds(224, 324, 300, EBZP13.getPreferredSize().height);

          //---- EBZP14 ----
          EBZP14.setComponentPopupMenu(BTD);
          EBZP14.setName("EBZP14");
          OBJ_22.add(EBZP14);
          EBZP14.setBounds(224, 351, 300, EBZP14.getPreferredSize().height);

          //---- EBZP15 ----
          EBZP15.setComponentPopupMenu(BTD);
          EBZP15.setName("EBZP15");
          OBJ_22.add(EBZP15);
          EBZP15.setBounds(224, 378, 300, EBZP15.getPreferredSize().height);

          //---- EBZP16 ----
          EBZP16.setComponentPopupMenu(BTD);
          EBZP16.setName("EBZP16");
          OBJ_22.add(EBZP16);
          EBZP16.setBounds(224, 405, 300, EBZP16.getPreferredSize().height);

          //---- EBZP17 ----
          EBZP17.setComponentPopupMenu(BTD);
          EBZP17.setName("EBZP17");
          OBJ_22.add(EBZP17);
          EBZP17.setBounds(224, 432, 300, EBZP17.getPreferredSize().height);

          //---- EBZP18 ----
          EBZP18.setComponentPopupMenu(BTD);
          EBZP18.setName("EBZP18");
          OBJ_22.add(EBZP18);
          EBZP18.setBounds(224, 459, 300, EBZP18.getPreferredSize().height);

          //---- OBJ_87 ----
          OBJ_87.setText("@WZPL01@");
          OBJ_87.setName("OBJ_87");
          OBJ_22.add(OBJ_87);
          OBJ_87.setBounds(10, 2, 209, OBJ_87.getPreferredSize().height);

          //---- OBJ_89 ----
          OBJ_89.setText("@WZPL02@");
          OBJ_89.setName("OBJ_89");
          OBJ_22.add(OBJ_89);
          OBJ_89.setBounds(10, 29, 209, OBJ_89.getPreferredSize().height);

          //---- OBJ_91 ----
          OBJ_91.setText("@WZPL03@");
          OBJ_91.setName("OBJ_91");
          OBJ_22.add(OBJ_91);
          OBJ_91.setBounds(10, 56, 209, OBJ_91.getPreferredSize().height);

          //---- OBJ_93 ----
          OBJ_93.setText("@WZPL04@");
          OBJ_93.setName("OBJ_93");
          OBJ_22.add(OBJ_93);
          OBJ_93.setBounds(10, 83, 209, OBJ_93.getPreferredSize().height);

          //---- OBJ_95 ----
          OBJ_95.setText("@WZPL05@");
          OBJ_95.setName("OBJ_95");
          OBJ_22.add(OBJ_95);
          OBJ_95.setBounds(10, 110, 209, OBJ_95.getPreferredSize().height);

          //---- OBJ_97 ----
          OBJ_97.setText("@WZPL06@");
          OBJ_97.setName("OBJ_97");
          OBJ_22.add(OBJ_97);
          OBJ_97.setBounds(10, 137, 209, OBJ_97.getPreferredSize().height);

          //---- OBJ_99 ----
          OBJ_99.setText("@WZPL07@");
          OBJ_99.setName("OBJ_99");
          OBJ_22.add(OBJ_99);
          OBJ_99.setBounds(10, 164, 209, OBJ_99.getPreferredSize().height);

          //---- OBJ_102 ----
          OBJ_102.setText("@WZPL08@");
          OBJ_102.setName("OBJ_102");
          OBJ_22.add(OBJ_102);
          OBJ_102.setBounds(10, 191, 209, OBJ_102.getPreferredSize().height);

          //---- OBJ_104 ----
          OBJ_104.setText("@WZPL09@");
          OBJ_104.setName("OBJ_104");
          OBJ_22.add(OBJ_104);
          OBJ_104.setBounds(10, 218, 209, OBJ_104.getPreferredSize().height);

          //---- OBJ_106 ----
          OBJ_106.setText("@WZPL10@");
          OBJ_106.setName("OBJ_106");
          OBJ_22.add(OBJ_106);
          OBJ_106.setBounds(10, 245, 209, OBJ_106.getPreferredSize().height);

          //---- OBJ_108 ----
          OBJ_108.setText("@WZPL11@");
          OBJ_108.setName("OBJ_108");
          OBJ_22.add(OBJ_108);
          OBJ_108.setBounds(10, 272, 209, OBJ_108.getPreferredSize().height);

          //---- OBJ_110 ----
          OBJ_110.setText("@WZPL12@");
          OBJ_110.setName("OBJ_110");
          OBJ_22.add(OBJ_110);
          OBJ_110.setBounds(10, 299, 209, OBJ_110.getPreferredSize().height);

          //---- OBJ_112 ----
          OBJ_112.setText("@WZPL13@");
          OBJ_112.setName("OBJ_112");
          OBJ_22.add(OBJ_112);
          OBJ_112.setBounds(10, 326, 209, OBJ_112.getPreferredSize().height);

          //---- OBJ_114 ----
          OBJ_114.setText("@WZPL14@");
          OBJ_114.setName("OBJ_114");
          OBJ_22.add(OBJ_114);
          OBJ_114.setBounds(10, 353, 209, OBJ_114.getPreferredSize().height);

          //---- OBJ_116 ----
          OBJ_116.setText("@WZPL15@");
          OBJ_116.setName("OBJ_116");
          OBJ_22.add(OBJ_116);
          OBJ_116.setBounds(10, 380, 209, OBJ_116.getPreferredSize().height);

          //---- OBJ_126 ----
          OBJ_126.setText("@WZPL16@");
          OBJ_126.setName("OBJ_126");
          OBJ_22.add(OBJ_126);
          OBJ_126.setBounds(10, 407, 209, OBJ_126.getPreferredSize().height);

          //---- OBJ_127 ----
          OBJ_127.setText("@WZPL17@");
          OBJ_127.setName("OBJ_127");
          OBJ_22.add(OBJ_127);
          OBJ_127.setBounds(10, 434, 209, OBJ_127.getPreferredSize().height);

          //---- OBJ_128 ----
          OBJ_128.setText("@WZPL18@");
          OBJ_128.setName("OBJ_128");
          OBJ_22.add(OBJ_128);
          OBJ_128.setBounds(10, 461, 209, OBJ_128.getPreferredSize().height);

          //---- OBJ_88 ----
          OBJ_88.setText("@ERZP1@");
          OBJ_88.setForeground(new Color(255, 0, 51));
          OBJ_88.setName("OBJ_88");
          OBJ_22.add(OBJ_88);
          OBJ_88.setBounds(529, 2, 185, OBJ_88.getPreferredSize().height);

          //---- OBJ_90 ----
          OBJ_90.setText("@ERZP2@");
          OBJ_90.setForeground(new Color(255, 0, 51));
          OBJ_90.setName("OBJ_90");
          OBJ_22.add(OBJ_90);
          OBJ_90.setBounds(529, 29, 185, OBJ_90.getPreferredSize().height);

          //---- OBJ_92 ----
          OBJ_92.setText("@ERZP3@");
          OBJ_92.setForeground(new Color(255, 0, 51));
          OBJ_92.setName("OBJ_92");
          OBJ_22.add(OBJ_92);
          OBJ_92.setBounds(529, 56, 185, OBJ_92.getPreferredSize().height);

          //---- OBJ_94 ----
          OBJ_94.setText("@ERZP4@");
          OBJ_94.setForeground(new Color(255, 0, 51));
          OBJ_94.setName("OBJ_94");
          OBJ_22.add(OBJ_94);
          OBJ_94.setBounds(529, 83, 185, OBJ_94.getPreferredSize().height);

          //---- OBJ_96 ----
          OBJ_96.setText("@ERZP5@");
          OBJ_96.setForeground(new Color(255, 0, 51));
          OBJ_96.setName("OBJ_96");
          OBJ_22.add(OBJ_96);
          OBJ_96.setBounds(529, 110, 185, OBJ_96.getPreferredSize().height);

          //---- OBJ_98 ----
          OBJ_98.setText("@ERZP6@");
          OBJ_98.setForeground(new Color(255, 0, 51));
          OBJ_98.setName("OBJ_98");
          OBJ_22.add(OBJ_98);
          OBJ_98.setBounds(529, 137, 185, OBJ_98.getPreferredSize().height);

          //---- OBJ_100 ----
          OBJ_100.setText("@ERZP7@");
          OBJ_100.setForeground(new Color(255, 0, 51));
          OBJ_100.setName("OBJ_100");
          OBJ_22.add(OBJ_100);
          OBJ_100.setBounds(529, 164, 185, OBJ_100.getPreferredSize().height);

          //---- OBJ_103 ----
          OBJ_103.setText("@ERZP8@");
          OBJ_103.setForeground(new Color(255, 0, 51));
          OBJ_103.setName("OBJ_103");
          OBJ_22.add(OBJ_103);
          OBJ_103.setBounds(529, 191, 185, OBJ_103.getPreferredSize().height);

          //---- OBJ_105 ----
          OBJ_105.setText("@ERZP9@");
          OBJ_105.setForeground(new Color(255, 0, 51));
          OBJ_105.setName("OBJ_105");
          OBJ_22.add(OBJ_105);
          OBJ_105.setBounds(529, 218, 185, OBJ_105.getPreferredSize().height);

          //---- OBJ_107 ----
          OBJ_107.setText("@ERZP10@");
          OBJ_107.setForeground(new Color(255, 0, 51));
          OBJ_107.setName("OBJ_107");
          OBJ_22.add(OBJ_107);
          OBJ_107.setBounds(529, 245, 185, OBJ_107.getPreferredSize().height);

          //---- OBJ_109 ----
          OBJ_109.setText("@ERZP11@");
          OBJ_109.setForeground(new Color(255, 0, 51));
          OBJ_109.setName("OBJ_109");
          OBJ_22.add(OBJ_109);
          OBJ_109.setBounds(529, 272, 185, OBJ_109.getPreferredSize().height);

          //---- OBJ_111 ----
          OBJ_111.setText("@ERZP12@");
          OBJ_111.setForeground(new Color(255, 0, 51));
          OBJ_111.setName("OBJ_111");
          OBJ_22.add(OBJ_111);
          OBJ_111.setBounds(529, 299, 185, OBJ_111.getPreferredSize().height);

          //---- OBJ_113 ----
          OBJ_113.setText("@ERZP13@");
          OBJ_113.setForeground(new Color(255, 0, 51));
          OBJ_113.setName("OBJ_113");
          OBJ_22.add(OBJ_113);
          OBJ_113.setBounds(529, 326, 185, OBJ_113.getPreferredSize().height);

          //---- OBJ_115 ----
          OBJ_115.setText("@ERZP14@");
          OBJ_115.setForeground(new Color(255, 0, 51));
          OBJ_115.setName("OBJ_115");
          OBJ_22.add(OBJ_115);
          OBJ_115.setBounds(529, 353, 185, OBJ_115.getPreferredSize().height);

          //---- OBJ_117 ----
          OBJ_117.setText("@ERZP15@");
          OBJ_117.setForeground(new Color(255, 0, 51));
          OBJ_117.setName("OBJ_117");
          OBJ_22.add(OBJ_117);
          OBJ_117.setBounds(529, 380, 185, OBJ_117.getPreferredSize().height);

          //---- OBJ_119 ----
          OBJ_119.setText("@ERZP16@");
          OBJ_119.setForeground(new Color(255, 0, 51));
          OBJ_119.setName("OBJ_119");
          OBJ_22.add(OBJ_119);
          OBJ_119.setBounds(529, 407, 185, OBJ_119.getPreferredSize().height);

          //---- OBJ_121 ----
          OBJ_121.setText("@ERZP17@");
          OBJ_121.setForeground(new Color(255, 0, 51));
          OBJ_121.setName("OBJ_121");
          OBJ_22.add(OBJ_121);
          OBJ_121.setBounds(529, 434, 185, OBJ_121.getPreferredSize().height);

          //---- OBJ_123 ----
          OBJ_123.setText("@ERZP18@");
          OBJ_123.setForeground(new Color(255, 0, 51));
          OBJ_123.setName("OBJ_123");
          OBJ_22.add(OBJ_123);
          OBJ_123.setBounds(529, 461, 185, OBJ_123.getPreferredSize().height);

          //---- OBJ_101 ----
          OBJ_101.setText("@T01@");
          OBJ_101.setName("OBJ_101");
          OBJ_22.add(OBJ_101);
          OBJ_101.setBounds(7, 176, 1, OBJ_101.getPreferredSize().height);

          //---- OBJ_118 ----
          OBJ_118.setText("@T16@");
          OBJ_118.setName("OBJ_118");
          OBJ_22.add(OBJ_118);
          OBJ_118.setBounds(8, 382, 1, OBJ_118.getPreferredSize().height);

          //---- OBJ_120 ----
          OBJ_120.setText("@T17@");
          OBJ_120.setName("OBJ_120");
          OBJ_22.add(OBJ_120);
          OBJ_120.setBounds(8, 408, 1, OBJ_120.getPreferredSize().height);

          //---- OBJ_122 ----
          OBJ_122.setText("@T18@");
          OBJ_122.setName("OBJ_122");
          OBJ_22.add(OBJ_122);
          OBJ_122.setBounds(8, 433, 1, OBJ_122.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_22.getComponentCount(); i++) {
              Rectangle bounds = OBJ_22.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_22.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_22.setMinimumSize(preferredSize);
            OBJ_22.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(OBJ_22);
        OBJ_22.setBounds(10, 20, 720, 495);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //======== panel2 ========
      {
        panel2.setMinimumSize(new Dimension(740, 35));
        panel2.setBorder(null);
        panel2.setOpaque(false);
        panel2.setBackground(new Color(214, 217, 223));
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- OBJ_86 ----
        OBJ_86.setText("Article");
        OBJ_86.setName("OBJ_86");
        panel2.add(OBJ_86);
        OBJ_86.setBounds(15, 4, 48, 20);

        //---- EBART ----
        EBART.setText("@EBART@");
        EBART.setFont(EBART.getFont().deriveFont(EBART.getFont().getStyle() | Font.BOLD));
        EBART.setBackground(new Color(214, 217, 223));
        EBART.setOpaque(false);
        EBART.setName("EBART");
        panel2.add(EBART);
        EBART.setBounds(70, 4, 155, 20);

        //---- OBJ_27 ----
        OBJ_27.setText("@A1LIB@");
        OBJ_27.setBackground(new Color(214, 217, 223));
        OBJ_27.setOpaque(false);
        OBJ_27.setName("OBJ_27");
        panel2.add(OBJ_27);
        OBJ_27.setBounds(230, 4, 223, 20);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(panel2);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel OBJ_22;
  private XRiTextField EBZP1;
  private XRiTextField EBZP2;
  private XRiTextField EBZP3;
  private XRiTextField EBZP4;
  private XRiTextField EBZP5;
  private XRiTextField EBZP6;
  private XRiTextField EBZP7;
  private XRiTextField EBZP8;
  private XRiTextField EBZP9;
  private XRiTextField EBZP10;
  private XRiTextField EBZP11;
  private XRiTextField EBZP12;
  private XRiTextField EBZP13;
  private XRiTextField EBZP14;
  private XRiTextField EBZP15;
  private XRiTextField EBZP16;
  private XRiTextField EBZP17;
  private XRiTextField EBZP18;
  private RiZoneSortie OBJ_87;
  private RiZoneSortie OBJ_89;
  private RiZoneSortie OBJ_91;
  private RiZoneSortie OBJ_93;
  private RiZoneSortie OBJ_95;
  private RiZoneSortie OBJ_97;
  private RiZoneSortie OBJ_99;
  private RiZoneSortie OBJ_102;
  private RiZoneSortie OBJ_104;
  private RiZoneSortie OBJ_106;
  private RiZoneSortie OBJ_108;
  private RiZoneSortie OBJ_110;
  private RiZoneSortie OBJ_112;
  private RiZoneSortie OBJ_114;
  private RiZoneSortie OBJ_116;
  private RiZoneSortie OBJ_126;
  private RiZoneSortie OBJ_127;
  private RiZoneSortie OBJ_128;
  private RiZoneSortie OBJ_88;
  private RiZoneSortie OBJ_90;
  private RiZoneSortie OBJ_92;
  private RiZoneSortie OBJ_94;
  private RiZoneSortie OBJ_96;
  private RiZoneSortie OBJ_98;
  private RiZoneSortie OBJ_100;
  private RiZoneSortie OBJ_103;
  private RiZoneSortie OBJ_105;
  private RiZoneSortie OBJ_107;
  private RiZoneSortie OBJ_109;
  private RiZoneSortie OBJ_111;
  private RiZoneSortie OBJ_113;
  private RiZoneSortie OBJ_115;
  private RiZoneSortie OBJ_117;
  private RiZoneSortie OBJ_119;
  private RiZoneSortie OBJ_121;
  private RiZoneSortie OBJ_123;
  private JLabel OBJ_101;
  private JLabel OBJ_118;
  private JLabel OBJ_120;
  private JLabel OBJ_122;
  private JMenuBar barre_tete;
  private JPanel panel2;
  private JLabel OBJ_86;
  private RiZoneSortie EBART;
  private RiZoneSortie OBJ_27;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
