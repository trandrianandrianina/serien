
package ri.serien.libecranrpg.vgvx.VGVX32FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVX32FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WL501_Title = { "HLD01", };
  private String[][] _WL501_Data = { { "WL501", }, };
  private int[] _WL501_Width = { 66, };
  
  public VGVX32FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WL501.setAspectTable(null, _WL501_Title, _WL501_Data, _WL501_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    VALX4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VALX4@")).trim());
    QTX4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTX4@")).trim());
    PRX4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PRX4@")).trim());
    QTX1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTX1@")).trim());
    PRX1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PRX1@")).trim());
    VALX1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VALX1@")).trim());
    QTX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTX2@")).trim());
    PRX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PRX2@")).trim());
    VALX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VALX2@")).trim());
    QTX3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTX3@")).trim());
    VALX3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VALX3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    VALX3.setVisible(lexique.isPresent("VALX3"));
    QTX3.setVisible(lexique.isPresent("QTX3"));
    VALX2.setVisible(lexique.isPresent("VALX2"));
    PRX2.setVisible(lexique.isPresent("PRX2"));
    QTX2.setVisible(lexique.isPresent("QTX2"));
    VALX1.setVisible(lexique.isPresent("VALX1"));
    PRX1.setVisible(lexique.isPresent("PRX1"));
    QTX1.setVisible(lexique.isPresent("QTX1"));
    PRX4.setVisible(lexique.isPresent("PRX4"));
    QTX4.setVisible(lexique.isPresent("QTX4"));
    VALX4.setVisible(lexique.isPresent("VALX4"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("P.U.M.P."));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WL501 = new XRiTable();
    OBJ_27 = new JXTitledSeparator();
    OBJ_48 = new JXTitledSeparator();
    VALX4 = new RiZoneSortie();
    QTX4 = new RiZoneSortie();
    PRX4 = new RiZoneSortie();
    P_PnlOpts = new JPanel();
    OBJ_29 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_31 = new JLabel();
    QTX1 = new RiZoneSortie();
    PRX1 = new RiZoneSortie();
    VALX1 = new RiZoneSortie();
    QTX2 = new RiZoneSortie();
    PRX2 = new RiZoneSortie();
    VALX2 = new RiZoneSortie();
    QTX3 = new RiZoneSortie();
    VALX3 = new RiZoneSortie();
    OBJ_32 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_42 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(775, 430));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setBorder(new TitledBorder("Justificatif P.U.M.P"));
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- WL501 ----
            WL501.setName("WL501");
            SCROLLPANE_LIST.setViewportView(WL501);
          }
          p_recup.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(29, 48, 506, 47);

          //---- OBJ_27 ----
          OBJ_27.setTitle("Calcul");
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(35, 115, 500, 20);

          //---- OBJ_48 ----
          OBJ_48.setTitle("Nouveau P.U.M.P");
          OBJ_48.setName("OBJ_48");
          p_recup.add(OBJ_48);
          OBJ_48.setBounds(35, 290, 500, 20);

          //---- VALX4 ----
          VALX4.setToolTipText("valeur totale");
          VALX4.setText("@VALX4@");
          VALX4.setHorizontalAlignment(SwingConstants.RIGHT);
          VALX4.setName("VALX4");
          p_recup.add(VALX4);
          VALX4.setBounds(35, 324, 120, VALX4.getPreferredSize().height);

          //---- QTX4 ----
          QTX4.setToolTipText("quantit\u00e9 totale");
          QTX4.setText("@QTX4@");
          QTX4.setHorizontalAlignment(SwingConstants.RIGHT);
          QTX4.setName("QTX4");
          p_recup.add(QTX4);
          QTX4.setBounds(205, 324, 120, QTX4.getPreferredSize().height);

          //---- PRX4 ----
          PRX4.setText("@PRX4@");
          PRX4.setHorizontalAlignment(SwingConstants.RIGHT);
          PRX4.setName("PRX4");
          p_recup.add(PRX4);
          PRX4.setBounds(365, 324, 120, PRX4.getPreferredSize().height);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_29 ----
          OBJ_29.setText("Quantit\u00e9");
          OBJ_29.setName("OBJ_29");
          p_recup.add(OBJ_29);
          OBJ_29.setBounds(130, 144, 90, 20);

          //---- OBJ_30 ----
          OBJ_30.setText("Prix");
          OBJ_30.setName("OBJ_30");
          p_recup.add(OBJ_30);
          OBJ_30.setBounds(275, 144, 90, 20);

          //---- OBJ_31 ----
          OBJ_31.setText("Valeur");
          OBJ_31.setName("OBJ_31");
          p_recup.add(OBJ_31);
          OBJ_31.setBounds(420, 144, 90, 20);

          //---- QTX1 ----
          QTX1.setText("@QTX1@");
          QTX1.setHorizontalAlignment(SwingConstants.RIGHT);
          QTX1.setName("QTX1");
          p_recup.add(QTX1);
          QTX1.setBounds(125, 165, 120, QTX1.getPreferredSize().height);

          //---- PRX1 ----
          PRX1.setText("@PRX1@");
          PRX1.setHorizontalAlignment(SwingConstants.RIGHT);
          PRX1.setName("PRX1");
          p_recup.add(PRX1);
          PRX1.setBounds(270, 165, 120, PRX1.getPreferredSize().height);

          //---- VALX1 ----
          VALX1.setText("@VALX1@");
          VALX1.setHorizontalAlignment(SwingConstants.RIGHT);
          VALX1.setName("VALX1");
          p_recup.add(VALX1);
          VALX1.setBounds(416, 165, 120, VALX1.getPreferredSize().height);

          //---- QTX2 ----
          QTX2.setText("@QTX2@");
          QTX2.setHorizontalAlignment(SwingConstants.RIGHT);
          QTX2.setName("QTX2");
          p_recup.add(QTX2);
          QTX2.setBounds(125, 195, 120, QTX2.getPreferredSize().height);

          //---- PRX2 ----
          PRX2.setText("@PRX2@");
          PRX2.setHorizontalAlignment(SwingConstants.RIGHT);
          PRX2.setName("PRX2");
          p_recup.add(PRX2);
          PRX2.setBounds(270, 195, 120, PRX2.getPreferredSize().height);

          //---- VALX2 ----
          VALX2.setText("@VALX2@");
          VALX2.setHorizontalAlignment(SwingConstants.RIGHT);
          VALX2.setName("VALX2");
          p_recup.add(VALX2);
          VALX2.setBounds(416, 195, 120, VALX2.getPreferredSize().height);

          //---- QTX3 ----
          QTX3.setToolTipText("quantit\u00e9 totale");
          QTX3.setText("@QTX3@");
          QTX3.setHorizontalAlignment(SwingConstants.RIGHT);
          QTX3.setName("QTX3");
          p_recup.add(QTX3);
          QTX3.setBounds(125, 225, 120, QTX3.getPreferredSize().height);

          //---- VALX3 ----
          VALX3.setToolTipText("valeur totale");
          VALX3.setText("@VALX3@");
          VALX3.setHorizontalAlignment(SwingConstants.RIGHT);
          VALX3.setName("VALX3");
          p_recup.add(VALX3);
          VALX3.setBounds(416, 225, 120, VALX3.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("Ancien stock");
          OBJ_32.setName("OBJ_32");
          p_recup.add(OBJ_32);
          OBJ_32.setBounds(35, 169, 91, 20);

          //---- OBJ_44 ----
          OBJ_44.setText("Stock actuel");
          OBJ_44.setName("OBJ_44");
          p_recup.add(OBJ_44);
          OBJ_44.setBounds(35, 229, 91, 20);

          //---- OBJ_38 ----
          OBJ_38.setText("Mouvement");
          OBJ_38.setName("OBJ_38");
          p_recup.add(OBJ_38);
          OBJ_38.setBounds(35, 199, 91, 20);

          //---- OBJ_52 ----
          OBJ_52.setText("=");
          OBJ_52.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_52.setName("OBJ_52");
          p_recup.add(OBJ_52);
          OBJ_52.setBounds(335, 324, 23, 24);

          //---- OBJ_54 ----
          OBJ_54.setText("/");
          OBJ_54.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_54.setName("OBJ_54");
          p_recup.add(OBJ_54);
          OBJ_54.setBounds(165, 324, 28, 24);

          //---- OBJ_34 ----
          OBJ_34.setText("x");
          OBJ_34.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_34.setName("OBJ_34");
          p_recup.add(OBJ_34);
          OBJ_34.setBounds(250, 169, 17, 20);

          //---- OBJ_36 ----
          OBJ_36.setText("=");
          OBJ_36.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_36.setName("OBJ_36");
          p_recup.add(OBJ_36);
          OBJ_36.setBounds(392, 168, 18, 20);

          //---- OBJ_40 ----
          OBJ_40.setText("x");
          OBJ_40.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_40.setName("OBJ_40");
          p_recup.add(OBJ_40);
          OBJ_40.setBounds(250, 199, 17, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("=");
          OBJ_42.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_42.setName("OBJ_42");
          p_recup.add(OBJ_42);
          OBJ_42.setBounds(392, 199, 18, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 565, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WL501;
  private JXTitledSeparator OBJ_27;
  private JXTitledSeparator OBJ_48;
  private RiZoneSortie VALX4;
  private RiZoneSortie QTX4;
  private RiZoneSortie PRX4;
  private JPanel P_PnlOpts;
  private JLabel OBJ_29;
  private JLabel OBJ_30;
  private JLabel OBJ_31;
  private RiZoneSortie QTX1;
  private RiZoneSortie PRX1;
  private RiZoneSortie VALX1;
  private RiZoneSortie QTX2;
  private RiZoneSortie PRX2;
  private RiZoneSortie VALX2;
  private RiZoneSortie QTX3;
  private RiZoneSortie VALX3;
  private JLabel OBJ_32;
  private JLabel OBJ_44;
  private JLabel OBJ_38;
  private JLabel OBJ_52;
  private JLabel OBJ_54;
  private JLabel OBJ_34;
  private JLabel OBJ_36;
  private JLabel OBJ_40;
  private JLabel OBJ_42;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
