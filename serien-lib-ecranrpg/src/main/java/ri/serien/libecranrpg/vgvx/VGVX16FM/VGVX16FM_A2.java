
package ri.serien.libecranrpg.vgvx.VGVX16FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX16FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WTP01_Top =
      { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11", "WTP12", "WTP13", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, };
  private int[] _WTP01_Width = { 57, };
  private Color[][] _LIST_Text_Color = new Color[13][1];
  private Color[][] _LIST_Fond_Color = null;
  
  public VGVX16FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, _LIST_Text_Color, _LIST_Fond_Color, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTIT@")).trim());
    OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGARTL@")).trim());
    OBJ_95.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGPGA@")).trim());
    PGQDEV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGQDEV@")).trim());
    PGQPOS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGQPOS@")).trim());
    PGSTK.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGSTK@")).trim());
    PGATT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGATT@")).trim());
    PGRES.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGRES@")).trim());
    PGPOS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGPOS@")).trim());
    PGAFF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGAFF@")).trim());
    PGDIV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGDIV@")).trim());
    OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGMAG@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGMAGL@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGUNS@")).trim());
    OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGUNSL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_85.setVisible(lexique.isTrue("N50"));
    OBJ_86.setVisible(OBJ_85.isVisible());
    OBJ_88.setVisible(OBJ_85.isVisible());
    // PGETB.setVisible( lexique.isPresent("PGETB"));
    OBJ_109.setVisible(!lexique.HostFieldGetData("PGQDEV").trim().equalsIgnoreCase(""));
    OBJ_128.setVisible(lexique.isPresent("PGDIV"));
    PGQPOS.setVisible(!lexique.HostFieldGetData("PGQPOS").trim().equalsIgnoreCase(""));
    PGQDEV.setVisible(!lexique.HostFieldGetData("PGQDEV").trim().equalsIgnoreCase(""));
    PGDIV.setVisible(lexique.isPresent("PGDIV"));
    PGPOS.setVisible(lexique.isPresent("PGPOS"));
    PGRES.setVisible(lexique.isPresent("PGRES"));
    PGATT.setVisible(lexique.isPresent("PGATT"));
    PGSTK.setVisible(lexique.isPresent("PGSTK"));
    // PGART.setVisible( lexique.isPresent("PGART"));
    OBJ_111.setVisible(!lexique.HostFieldGetData("PGQPOS").trim().equalsIgnoreCase(""));
    OBJ_88.setVisible(lexique.isPresent("PGMAGL"));
    OBJ_63.setVisible(lexique.isPresent("PGARTL"));
    label1.setVisible(lexique.isTrue("50"));
    
    // couleurs pour valeurs négatives
    if (lexique.HostFieldGetData("PGDIV").contains("-")) {
      PGDIV.setForeground(ri.serien.libcommun.outils.Constantes.COULEUR_ERREURS);
    }
    
    // Couleurs de la liste (suivant indicateurs de 1 à 13)
    for (int i = 0; i < 13; i++) {
      if (lexique.isTrue(String.valueOf(((i + 1) < 10 ? "0" + (i + 1) : (i + 1))))) {
        _LIST_Text_Color[i][0] = ri.serien.libcommun.outils.Constantes.COULEUR_LISTE_COMMENTAIRE;
      }
      else {
        _LIST_Text_Color[i][0] = Color.BLACK;
      }
    }
    
    btlien1.setVisible(lexique.HostFieldGetData("LD01").substring(42, 43).contains("l"));
    btlien2.setVisible(lexique.HostFieldGetData("LD02").substring(42, 43).contains("l"));
    btlien3.setVisible(lexique.HostFieldGetData("LD03").substring(42, 43).contains("l"));
    btlien4.setVisible(lexique.HostFieldGetData("LD04").substring(42, 43).contains("l"));
    btlien5.setVisible(lexique.HostFieldGetData("LD05").substring(42, 43).contains("l"));
    btlien6.setVisible(lexique.HostFieldGetData("LD06").substring(42, 43).contains("l"));
    btlien7.setVisible(lexique.HostFieldGetData("LD07").substring(42, 43).contains("l"));
    btlien8.setVisible(lexique.HostFieldGetData("LD08").substring(42, 43).contains("l"));
    btlien9.setVisible(lexique.HostFieldGetData("LD09").substring(42, 43).contains("l"));
    btlien10.setVisible(lexique.HostFieldGetData("LD10").substring(42, 43).contains("l"));
    btlien11.setVisible(lexique.HostFieldGetData("LD11").substring(42, 43).contains("l"));
    btlien12.setVisible(lexique.HostFieldGetData("LD12").substring(42, 43).contains("l"));
    btlien13.setVisible(lexique.HostFieldGetData("LD13").substring(42, 43).contains("l"));
    
    // TODO Icones
    l_plus.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    l_moins.setIcon(lexique.chargerImage("images/moins_petit.png", true));
    l_egal.setIcon(lexique.chargerImage("images/egal_petit.png", true));
    
    p_bpresentation.setCodeEtablissement(PGETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(PGETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("L");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void btlien1ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien2ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP02", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien3ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP03", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien4ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP04", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien5ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP05", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien6ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP06", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien7ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP07", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien8ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP08", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien9ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP09", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
    // WTP01.setValeurTop("L");
    // lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void btlien10ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP10", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien11ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP11", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien12ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP12", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btlien13ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP13", 0, "L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_66 = new JLabel();
    PGETB = new XRiTextField();
    OBJ_67 = new JLabel();
    PGART = new XRiTextField();
    OBJ_63 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_95 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_94 = new JLabel();
    DREPX = new XRiCalendrier();
    btlien1 = new SNBoutonDetail();
    btlien2 = new SNBoutonDetail();
    btlien3 = new SNBoutonDetail();
    btlien4 = new SNBoutonDetail();
    btlien5 = new SNBoutonDetail();
    btlien6 = new SNBoutonDetail();
    btlien7 = new SNBoutonDetail();
    btlien8 = new SNBoutonDetail();
    btlien9 = new SNBoutonDetail();
    btlien10 = new SNBoutonDetail();
    btlien11 = new SNBoutonDetail();
    btlien12 = new SNBoutonDetail();
    btlien13 = new SNBoutonDetail();
    OBJ_111 = new JLabel();
    PGQDEV = new RiZoneSortie();
    PGQPOS = new RiZoneSortie();
    OBJ_109 = new JLabel();
    panel2 = new JPanel();
    PGSTK = new RiZoneSortie();
    PGATT = new RiZoneSortie();
    PGRES = new RiZoneSortie();
    PGPOS = new RiZoneSortie();
    PGAFF = new RiZoneSortie();
    PGDIV = new RiZoneSortie();
    OBJ_117 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_114 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_116 = new JLabel();
    l_plus = new JLabel();
    l_egal = new JLabel();
    l_moins = new JLabel();
    panel3 = new JPanel();
    OBJ_85 = new JLabel();
    OBJ_86 = new RiZoneSortie();
    OBJ_88 = new RiZoneSortie();
    label1 = new JLabel();
    panel4 = new JPanel();
    OBJ_91 = new JLabel();
    OBJ_89 = new RiZoneSortie();
    OBJ_90 = new RiZoneSortie();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    BTD2 = new JPopupMenu();
    CHOISIR2 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_26 = new JMenuItem();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(1200, 700));
    setPreferredSize(new Dimension(1200, 700));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@WTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_66 ----
          OBJ_66.setText("Etablissement");
          OBJ_66.setName("OBJ_66");

          //---- PGETB ----
          PGETB.setName("PGETB");

          //---- OBJ_67 ----
          OBJ_67.setText("Article");
          OBJ_67.setName("OBJ_67");

          //---- PGART ----
          PGART.setName("PGART");

          //---- OBJ_63 ----
          OBJ_63.setText("@PGARTL@");
          OBJ_63.setOpaque(false);
          OBJ_63.setName("OBJ_63");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_66)
                .addGap(10, 10, 10)
                .addComponent(PGETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(PGART, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 233, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(OBJ_66))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(PGETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(OBJ_67))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(PGART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_95 ----
          OBJ_95.setText("@PGPGA@");
          OBJ_95.setFont(OBJ_95.getFont().deriveFont(OBJ_95.getFont().getStyle() | Font.BOLD));
          OBJ_95.setName("OBJ_95");
          p_tete_droite.add(OBJ_95);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setMinimumSize(new Dimension(1400, 575));
      p_sud.setPreferredSize(new Dimension(1400, 575));
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Quantit\u00e9 \u00e0 terme");
              riSousMenu_bt6.setToolTipText("Quantit\u00e9 \u00e0 terme");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Simulation");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Simulation commandes");
              riSousMenu_bt7.setToolTipText("Ajout des commandes de positionnement");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Simulation devis");
              riSousMenu_bt8.setToolTipText("Ajout des devis en cours non sign\u00e9s");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1020, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1000, 550));
          p_contenu.setMaximumSize(new Dimension(1000, 550));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD2);
              WTP01.setMinimumSize(new Dimension(15, 208));
              WTP01.setName("WTP01");
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(25, 45, 925, 240);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(960, 45, 25, 110);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(960, 175, 25, 110);

            //---- OBJ_94 ----
            OBJ_94.setText("Date de r\u00e9ception th\u00e9orique");
            OBJ_94.setName("OBJ_94");
            panel1.add(OBJ_94);
            OBJ_94.setBounds(680, 15, 165, 18);

            //---- DREPX ----
            DREPX.setName("DREPX");
            panel1.add(DREPX);
            DREPX.setBounds(845, 10, 105, DREPX.getPreferredSize().height);

            //---- btlien1 ----
            btlien1.setText("");
            btlien1.setToolTipText("Liens/commandes");
            btlien1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien1.setName("btlien1");
            btlien1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien1ActionPerformed(e);
              }
            });
            panel1.add(btlien1);
            btlien1.setBounds(5, 70, 20, 15);

            //---- btlien2 ----
            btlien2.setText("");
            btlien2.setToolTipText("Liens/commandes");
            btlien2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien2.setName("btlien2");
            btlien2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien2ActionPerformed(e);
              }
            });
            panel1.add(btlien2);
            btlien2.setBounds(5, 86, 20, 15);

            //---- btlien3 ----
            btlien3.setText("");
            btlien3.setToolTipText("Liens/commandes");
            btlien3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien3.setName("btlien3");
            btlien3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien3ActionPerformed(e);
              }
            });
            panel1.add(btlien3);
            btlien3.setBounds(5, 102, 20, 15);

            //---- btlien4 ----
            btlien4.setText("");
            btlien4.setToolTipText("Liens/commandes");
            btlien4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien4.setName("btlien4");
            btlien4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien4ActionPerformed(e);
              }
            });
            panel1.add(btlien4);
            btlien4.setBounds(5, 118, 20, 15);

            //---- btlien5 ----
            btlien5.setText("");
            btlien5.setToolTipText("Liens/commandes");
            btlien5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien5.setName("btlien5");
            btlien5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien5ActionPerformed(e);
              }
            });
            panel1.add(btlien5);
            btlien5.setBounds(5, 134, 20, 15);

            //---- btlien6 ----
            btlien6.setText("");
            btlien6.setToolTipText("Liens/commandes");
            btlien6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien6.setName("btlien6");
            btlien6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien6ActionPerformed(e);
              }
            });
            panel1.add(btlien6);
            btlien6.setBounds(5, 150, 20, 15);

            //---- btlien7 ----
            btlien7.setText("");
            btlien7.setToolTipText("Liens/commandes");
            btlien7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien7.setName("btlien7");
            btlien7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien7ActionPerformed(e);
              }
            });
            panel1.add(btlien7);
            btlien7.setBounds(5, 166, 20, 15);

            //---- btlien8 ----
            btlien8.setText("");
            btlien8.setToolTipText("Liens/commandes");
            btlien8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien8.setName("btlien8");
            btlien8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien8ActionPerformed(e);
              }
            });
            panel1.add(btlien8);
            btlien8.setBounds(5, 182, 20, 15);

            //---- btlien9 ----
            btlien9.setText("");
            btlien9.setToolTipText("Liens/commandes");
            btlien9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien9.setName("btlien9");
            btlien9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien9ActionPerformed(e);
              }
            });
            panel1.add(btlien9);
            btlien9.setBounds(5, 198, 20, 15);

            //---- btlien10 ----
            btlien10.setText("");
            btlien10.setToolTipText("Liens/commandes");
            btlien10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien10.setName("btlien10");
            btlien10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien10ActionPerformed(e);
              }
            });
            panel1.add(btlien10);
            btlien10.setBounds(5, 214, 20, 15);

            //---- btlien11 ----
            btlien11.setText("");
            btlien11.setToolTipText("Liens/commandes");
            btlien11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien11.setName("btlien11");
            btlien11.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien11ActionPerformed(e);
              }
            });
            panel1.add(btlien11);
            btlien11.setBounds(5, 230, 20, 15);

            //---- btlien12 ----
            btlien12.setText("");
            btlien12.setToolTipText("Liens/commandes");
            btlien12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien12.setName("btlien12");
            btlien12.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien12ActionPerformed(e);
              }
            });
            panel1.add(btlien12);
            btlien12.setBounds(5, 246, 20, 15);

            //---- btlien13 ----
            btlien13.setText("");
            btlien13.setToolTipText("Liens/commandes");
            btlien13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btlien13.setName("btlien13");
            btlien13.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btlien13ActionPerformed(e);
              }
            });
            panel1.add(btlien13);
            btlien13.setBounds(5, 262, 20, 15);

            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //---- OBJ_111 ----
          OBJ_111.setText("Commandes de positionnement");
          OBJ_111.setName("OBJ_111");

          //---- PGQDEV ----
          PGQDEV.setHorizontalAlignment(SwingConstants.RIGHT);
          PGQDEV.setText("@PGQDEV@");
          PGQDEV.setName("PGQDEV");

          //---- PGQPOS ----
          PGQPOS.setHorizontalAlignment(SwingConstants.RIGHT);
          PGQPOS.setText("@PGQPOS@");
          PGQPOS.setName("PGQPOS");

          //---- OBJ_109 ----
          OBJ_109.setText("Devis");
          OBJ_109.setName("OBJ_109");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Position"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- PGSTK ----
            PGSTK.setHorizontalAlignment(SwingConstants.RIGHT);
            PGSTK.setText("@PGSTK@");
            PGSTK.setName("PGSTK");
            panel2.add(PGSTK);
            PGSTK.setBounds(35, 50, 106, PGSTK.getPreferredSize().height);

            //---- PGATT ----
            PGATT.setHorizontalAlignment(SwingConstants.RIGHT);
            PGATT.setText("@PGATT@");
            PGATT.setName("PGATT");
            panel2.add(PGATT);
            PGATT.setBounds(195, 50, 90, PGATT.getPreferredSize().height);

            //---- PGRES ----
            PGRES.setHorizontalAlignment(SwingConstants.RIGHT);
            PGRES.setText("@PGRES@");
            PGRES.setName("PGRES");
            panel2.add(PGRES);
            PGRES.setBounds(340, 50, 90, PGRES.getPreferredSize().height);

            //---- PGPOS ----
            PGPOS.setHorizontalAlignment(SwingConstants.RIGHT);
            PGPOS.setText("@PGPOS@");
            PGPOS.setFont(PGPOS.getFont().deriveFont(PGPOS.getFont().getStyle() | Font.BOLD));
            PGPOS.setName("PGPOS");
            panel2.add(PGPOS);
            PGPOS.setBounds(485, 50, 90, PGPOS.getPreferredSize().height);

            //---- PGAFF ----
            PGAFF.setHorizontalAlignment(SwingConstants.RIGHT);
            PGAFF.setText("@PGAFF@");
            PGAFF.setName("PGAFF");
            panel2.add(PGAFF);
            PGAFF.setBounds(650, 50, 106, PGAFF.getPreferredSize().height);

            //---- PGDIV ----
            PGDIV.setHorizontalAlignment(SwingConstants.RIGHT);
            PGDIV.setFont(PGDIV.getFont().deriveFont(PGDIV.getFont().getStyle() | Font.BOLD));
            PGDIV.setText("@PGDIV@");
            PGDIV.setName("PGDIV");
            panel2.add(PGDIV);
            PGDIV.setBounds(830, 50, 106, PGDIV.getPreferredSize().height);

            //---- OBJ_117 ----
            OBJ_117.setText("R\u00e9serv\u00e9");
            OBJ_117.setName("OBJ_117");
            panel2.add(OBJ_117);
            OBJ_117.setBounds(650, 25, 82, 25);

            //---- OBJ_128 ----
            OBJ_128.setText("Net");
            OBJ_128.setBackground(Color.black);
            OBJ_128.setFont(OBJ_128.getFont().deriveFont(OBJ_128.getFont().getStyle() | Font.BOLD));
            OBJ_128.setName("OBJ_128");
            panel2.add(OBJ_128);
            OBJ_128.setBounds(830, 25, 74, 25);

            //---- OBJ_115 ----
            OBJ_115.setText("Command\u00e9");
            OBJ_115.setName("OBJ_115");
            panel2.add(OBJ_115);
            OBJ_115.setBounds(340, 25, 73, 25);

            //---- OBJ_114 ----
            OBJ_114.setText("Attendu");
            OBJ_114.setName("OBJ_114");
            panel2.add(OBJ_114);
            OBJ_114.setBounds(195, 25, 67, 25);

            //---- OBJ_113 ----
            OBJ_113.setText("Physique");
            OBJ_113.setName("OBJ_113");
            panel2.add(OBJ_113);
            OBJ_113.setBounds(35, 25, 55, 25);

            //---- OBJ_116 ----
            OBJ_116.setText("Th\u00e9orique");
            OBJ_116.setFont(OBJ_116.getFont().deriveFont(OBJ_116.getFont().getStyle() | Font.BOLD));
            OBJ_116.setName("OBJ_116");
            panel2.add(OBJ_116);
            OBJ_116.setBounds(490, 25, 87, 25);

            //---- l_plus ----
            l_plus.setName("l_plus");
            panel2.add(l_plus);
            l_plus.setBounds(160, 54, 16, 16);

            //---- l_egal ----
            l_egal.setName("l_egal");
            panel2.add(l_egal);
            l_egal.setBounds(450, 54, 16, 16);

            //---- l_moins ----
            l_moins.setName("l_moins");
            panel2.add(l_moins);
            l_moins.setBounds(305, 54, 16, 16);

            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");

            //---- OBJ_85 ----
            OBJ_85.setText("Magasin");
            OBJ_85.setName("OBJ_85");

            //---- OBJ_86 ----
            OBJ_86.setText("@PGMAG@");
            OBJ_86.setName("OBJ_86");

            //---- OBJ_88 ----
            OBJ_88.setText("@PGMAGL@");
            OBJ_88.setName("OBJ_88");

            //---- label1 ----
            label1.setText("(Tous magasins confondus)");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");

            GroupLayout panel3Layout = new GroupLayout(panel3);
            panel3.setLayout(panel3Layout);
            panel3Layout.setHorizontalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(60, 60, 60)
                      .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(OBJ_88, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)))
                  .addGap(659, 659, 659))
            );
            panel3Layout.setVerticalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(label1))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(OBJ_88, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OBJ_91 ----
            OBJ_91.setText("Unit\u00e9 stock");
            OBJ_91.setName("OBJ_91");
            panel4.add(OBJ_91);
            OBJ_91.setBounds(205, 10, 78, 18);

            //---- OBJ_89 ----
            OBJ_89.setText("@PGUNS@");
            OBJ_89.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_89.setName("OBJ_89");
            panel4.add(OBJ_89);
            OBJ_89.setBounds(295, 7, 35, 24);

            //---- OBJ_90 ----
            OBJ_90.setText("@PGUNSL@");
            OBJ_90.setName("OBJ_90");
            panel4.add(OBJ_90);
            OBJ_90.setBounds(335, 7, 151, 24);

            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 998, Short.MAX_VALUE)
                .addContainerGap())
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 504, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 499, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(9, 9, 9)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 997, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(39, 39, 39)
                    .addComponent(OBJ_109, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
                    .addGap(17, 17, 17)
                    .addComponent(PGQDEV, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
                    .addGap(445, 445, 445)
                    .addComponent(OBJ_111, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(PGQPOS, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_109, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(PGQDEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_111, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(PGQPOS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Affichage \u00e0 partir de...");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_22 ----
      OBJ_22.setText("Modification du bon");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("Affichage du bon");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_24 ----
      OBJ_24.setText("Affectation sur achat");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- CHOISIR2 ----
      CHOISIR2.setText("Affichage \u00e0 partir de...");
      CHOISIR2.setName("CHOISIR2");
      CHOISIR2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD2.add(CHOISIR2);

      //---- OBJ_25 ----
      OBJ_25.setText("Modification du bon");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_25);

      //---- OBJ_26 ----
      OBJ_26.setText("Affichage du bon");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_26);
    }

    //======== riSousMenu9 ========
    {
      riSousMenu9.setName("riSousMenu9");

      //---- riSousMenu_bt9 ----
      riSousMenu_bt9.setText("Sim. OF pr\u00e9visionels");
      riSousMenu_bt9.setName("riSousMenu_bt9");
      riSousMenu_bt9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt9ActionPerformed(e);
        }
      });
      riSousMenu9.add(riSousMenu_bt9);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_66;
  private XRiTextField PGETB;
  private JLabel OBJ_67;
  private XRiTextField PGART;
  private RiZoneSortie OBJ_63;
  private JPanel p_tete_droite;
  private JLabel OBJ_95;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_94;
  private XRiCalendrier DREPX;
  private SNBoutonDetail btlien1;
  private SNBoutonDetail btlien2;
  private SNBoutonDetail btlien3;
  private SNBoutonDetail btlien4;
  private SNBoutonDetail btlien5;
  private SNBoutonDetail btlien6;
  private SNBoutonDetail btlien7;
  private SNBoutonDetail btlien8;
  private SNBoutonDetail btlien9;
  private SNBoutonDetail btlien10;
  private SNBoutonDetail btlien11;
  private SNBoutonDetail btlien12;
  private SNBoutonDetail btlien13;
  private JLabel OBJ_111;
  private RiZoneSortie PGQDEV;
  private RiZoneSortie PGQPOS;
  private JLabel OBJ_109;
  private JPanel panel2;
  private RiZoneSortie PGSTK;
  private RiZoneSortie PGATT;
  private RiZoneSortie PGRES;
  private RiZoneSortie PGPOS;
  private RiZoneSortie PGAFF;
  private RiZoneSortie PGDIV;
  private JLabel OBJ_117;
  private JLabel OBJ_128;
  private JLabel OBJ_115;
  private JLabel OBJ_114;
  private JLabel OBJ_113;
  private JLabel OBJ_116;
  private JLabel l_plus;
  private JLabel l_egal;
  private JLabel l_moins;
  private JPanel panel3;
  private JLabel OBJ_85;
  private RiZoneSortie OBJ_86;
  private RiZoneSortie OBJ_88;
  private JLabel label1;
  private JPanel panel4;
  private JLabel OBJ_91;
  private RiZoneSortie OBJ_89;
  private RiZoneSortie OBJ_90;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JPopupMenu BTD2;
  private JMenuItem CHOISIR2;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_26;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
