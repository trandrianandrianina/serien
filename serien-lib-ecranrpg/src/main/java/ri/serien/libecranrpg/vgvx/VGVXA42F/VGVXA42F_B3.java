
package ri.serien.libecranrpg.vgvx.VGVXA42F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGVXA42F_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXA42F_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WNPA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA@")).trim());
    WSOART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOART@")).trim());
    WSOARL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOARL@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("(@WNPQTE@ @UNSART@)")).trim());
    LB013.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB013@")).trim());
    LB023.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB023@")).trim());
    LB043.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB043@")).trim());
    LB033.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB033@")).trim());
    LB053.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB053@")).trim());
    LB063.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB063@")).trim());
    LB073.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB073@")).trim());
    LB093.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB093@")).trim());
    LB083.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB083@")).trim());
    LB103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB103@")).trim());
    UNV01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNV01@")).trim());
    UNV02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNV03@")).trim());
    UNV03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNV03@")).trim());
    UNV04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNV04@")).trim());
    UNV05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNV05@")).trim());
    UNV06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNV06@")).trim());
    UNV07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNV07@")).trim());
    UNV08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNV08@")).trim());
    UNV09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNV09@")).trim());
    UNV10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNV10@")).trim());
    TOTPRA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTPRA@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MES53@")).trim());
    TOTCOL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTCOL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    WTB01.setVisible(!lexique.HostFieldGetData("LB013").trim().equals(""));
    WTB02.setVisible(!lexique.HostFieldGetData("LB023").trim().equals(""));
    WTB03.setVisible(!lexique.HostFieldGetData("LB033").trim().equals(""));
    WTB04.setVisible(!lexique.HostFieldGetData("LB043").trim().equals(""));
    WTB05.setVisible(!lexique.HostFieldGetData("LB053").trim().equals(""));
    WTB06.setVisible(!lexique.HostFieldGetData("LB063").trim().equals(""));
    WTB07.setVisible(!lexique.HostFieldGetData("LB073").trim().equals(""));
    WTB08.setVisible(!lexique.HostFieldGetData("LB083").trim().equals(""));
    WTB09.setVisible(!lexique.HostFieldGetData("LB093").trim().equals(""));
    WTB10.setVisible(!lexique.HostFieldGetData("LB103").trim().equals(""));
    LB013.setVisible(!lexique.HostFieldGetData("LB013").trim().equals(""));
    LB023.setVisible(!lexique.HostFieldGetData("LB023").trim().equals(""));
    LB033.setVisible(!lexique.HostFieldGetData("LB033").trim().equals(""));
    LB043.setVisible(!lexique.HostFieldGetData("LB043").trim().equals(""));
    LB053.setVisible(!lexique.HostFieldGetData("LB053").trim().equals(""));
    LB063.setVisible(!lexique.HostFieldGetData("LB063").trim().equals(""));
    LB073.setVisible(!lexique.HostFieldGetData("LB073").trim().equals(""));
    LB083.setVisible(!lexique.HostFieldGetData("LB083").trim().equals(""));
    LB093.setVisible(!lexique.HostFieldGetData("LB093").trim().equals(""));
    LB103.setVisible(!lexique.HostFieldGetData("LB103").trim().equals(""));
    UNV01.setVisible(!lexique.HostFieldGetData("LB013").trim().equals(""));
    UNV02.setVisible(!lexique.HostFieldGetData("LB023").trim().equals(""));
    UNV03.setVisible(!lexique.HostFieldGetData("LB033").trim().equals(""));
    UNV04.setVisible(!lexique.HostFieldGetData("LB043").trim().equals(""));
    UNV05.setVisible(!lexique.HostFieldGetData("LB053").trim().equals(""));
    UNV06.setVisible(!lexique.HostFieldGetData("LB063").trim().equals(""));
    UNV07.setVisible(!lexique.HostFieldGetData("LB073").trim().equals(""));
    UNV08.setVisible(!lexique.HostFieldGetData("LB083").trim().equals(""));
    UNV09.setVisible(!lexique.HostFieldGetData("LB093").trim().equals(""));
    UNV10.setVisible(!lexique.HostFieldGetData("LB103").trim().equals(""));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@(TITLE)@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, "P");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riBoutonDetailListe1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WTB01");
    BTD.show(WTB01, 5, 5);
  }
  
  private void riBoutonDetailListe2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WTB02");
    BTD.show(WTB02, 5, 5);
  }
  
  private void riBoutonDetailListe3ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WTB03");
    BTD.show(WTB03, 5, 5);
  }
  
  private void riBoutonDetailListe4ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WTB04");
    BTD.show(WTB04, 5, 5);
  }
  
  private void riBoutonDetailListe5ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WTB05");
    BTD.show(WTB05, 5, 5);
  }
  
  private void riBoutonDetailListe6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WTB06");
    BTD.show(WTB06, 5, 5);
  }
  
  private void riBoutonDetailListe7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WTB07");
    BTD.show(WTB07, 5, 5);
  }
  
  private void riBoutonDetailListe8ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WTB08");
    BTD.show(WTB08, 5, 5);
  }
  
  private void riBoutonDetailListe9ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WTB09");
    BTD.show(WTB09, 5, 5);
  }
  
  private void riBoutonDetailListe10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WTB10");
    BTD.show(WTB10, 5, 5);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_14 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    WETB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_36 = new JLabel();
    WNPA = new RiZoneSortie();
    WSOART = new RiZoneSortie();
    WSOARL = new RiZoneSortie();
    label1 = new JLabel();
    LB013 = new RiZoneSortie();
    LB023 = new RiZoneSortie();
    LB043 = new RiZoneSortie();
    LB033 = new RiZoneSortie();
    LB053 = new RiZoneSortie();
    LB063 = new RiZoneSortie();
    LB073 = new RiZoneSortie();
    LB093 = new RiZoneSortie();
    LB083 = new RiZoneSortie();
    LB103 = new RiZoneSortie();
    PRA01 = new XRiTextField();
    PRA02 = new XRiTextField();
    PRA03 = new XRiTextField();
    PRA04 = new XRiTextField();
    PRA05 = new XRiTextField();
    PRA06 = new XRiTextField();
    PRA07 = new XRiTextField();
    PRA08 = new XRiTextField();
    PRA09 = new XRiTextField();
    PRA10 = new XRiTextField();
    UNV01 = new RiZoneSortie();
    UNV02 = new RiZoneSortie();
    UNV03 = new RiZoneSortie();
    UNV04 = new RiZoneSortie();
    UNV05 = new RiZoneSortie();
    UNV06 = new RiZoneSortie();
    UNV07 = new RiZoneSortie();
    UNV08 = new RiZoneSortie();
    UNV09 = new RiZoneSortie();
    UNV10 = new RiZoneSortie();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    TOTPRA = new RiZoneSortie();
    OBJ_56 = new JLabel();
    OBJ_55 = new JLabel();
    TOTCOL = new RiZoneSortie();
    WTB01 = new SNBoutonDetail();
    WTB02 = new SNBoutonDetail();
    WTB03 = new SNBoutonDetail();
    WTB04 = new SNBoutonDetail();
    WTB05 = new SNBoutonDetail();
    WTB06 = new SNBoutonDetail();
    WTB07 = new SNBoutonDetail();
    WTB08 = new SNBoutonDetail();
    WTB09 = new SNBoutonDetail();
    WTB10 = new SNBoutonDetail();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_14 ----
      OBJ_14.setText("Validation tarif");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Palette multi-r\u00e9f\u00e9rences");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_34 ----
          OBJ_34.setText("Etablissement \u00e0 traiter");
          OBJ_34.setName("OBJ_34");
          p_tete_gauche.add(OBJ_34);
          OBJ_34.setBounds(5, 0, 151, 28);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setText("@WETB@");
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(160, 0, 39, WETB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Duplication de palette");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("On/Off saisie code barre");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Saisie d'un prix d'achat");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("D\u00e9tail de la palette");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setPreferredSize(new Dimension(720, 575));
        p_centrage.setMinimumSize(new Dimension(0, 0));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(870, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_36 ----
            OBJ_36.setText("Num\u00e9ro de palette");
            OBJ_36.setName("OBJ_36");
            panel1.add(OBJ_36);
            OBJ_36.setBounds(35, 45, 151, 28);

            //---- WNPA ----
            WNPA.setComponentPopupMenu(BTD);
            WNPA.setText("@WNPA@");
            WNPA.setName("WNPA");
            panel1.add(WNPA);
            WNPA.setBounds(210, 45, 74, WNPA.getPreferredSize().height);

            //---- WSOART ----
            WSOART.setText("@WSOART@");
            WSOART.setName("WSOART");
            panel1.add(WSOART);
            WSOART.setBounds(210, 80, 210, WSOART.getPreferredSize().height);

            //---- WSOARL ----
            WSOARL.setText("@WSOARL@");
            WSOARL.setName("WSOARL");
            panel1.add(WSOARL);
            WSOARL.setBounds(210, 110, 310, WSOARL.getPreferredSize().height);

            //---- label1 ----
            label1.setText("(@WNPQTE@ @UNSART@)");
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(295, 45, 125, 28);

            //---- LB013 ----
            LB013.setText("@LB013@");
            LB013.setComponentPopupMenu(null);
            LB013.setName("LB013");
            panel1.add(LB013);
            LB013.setBounds(35, 180, 600, LB013.getPreferredSize().height);

            //---- LB023 ----
            LB023.setText("@LB023@");
            LB023.setComponentPopupMenu(null);
            LB023.setName("LB023");
            panel1.add(LB023);
            LB023.setBounds(35, 205, 600, LB023.getPreferredSize().height);

            //---- LB043 ----
            LB043.setText("@LB043@");
            LB043.setComponentPopupMenu(null);
            LB043.setName("LB043");
            panel1.add(LB043);
            LB043.setBounds(35, 255, 600, LB043.getPreferredSize().height);

            //---- LB033 ----
            LB033.setText("@LB033@");
            LB033.setComponentPopupMenu(null);
            LB033.setName("LB033");
            panel1.add(LB033);
            LB033.setBounds(35, 230, 600, LB033.getPreferredSize().height);

            //---- LB053 ----
            LB053.setText("@LB053@");
            LB053.setComponentPopupMenu(null);
            LB053.setName("LB053");
            panel1.add(LB053);
            LB053.setBounds(35, 280, 600, LB053.getPreferredSize().height);

            //---- LB063 ----
            LB063.setText("@LB063@");
            LB063.setComponentPopupMenu(null);
            LB063.setName("LB063");
            panel1.add(LB063);
            LB063.setBounds(35, 305, 600, LB063.getPreferredSize().height);

            //---- LB073 ----
            LB073.setText("@LB073@");
            LB073.setComponentPopupMenu(null);
            LB073.setName("LB073");
            panel1.add(LB073);
            LB073.setBounds(35, 330, 600, LB073.getPreferredSize().height);

            //---- LB093 ----
            LB093.setText("@LB093@");
            LB093.setComponentPopupMenu(null);
            LB093.setName("LB093");
            panel1.add(LB093);
            LB093.setBounds(35, 380, 600, LB093.getPreferredSize().height);

            //---- LB083 ----
            LB083.setText("@LB083@");
            LB083.setComponentPopupMenu(null);
            LB083.setName("LB083");
            panel1.add(LB083);
            LB083.setBounds(35, 355, 600, LB083.getPreferredSize().height);

            //---- LB103 ----
            LB103.setText("@LB103@");
            LB103.setComponentPopupMenu(null);
            LB103.setName("LB103");
            panel1.add(LB103);
            LB103.setBounds(35, 405, 600, LB103.getPreferredSize().height);

            //---- PRA01 ----
            PRA01.setComponentPopupMenu(null);
            PRA01.setName("PRA01");
            panel1.add(PRA01);
            PRA01.setBounds(640, 178, 110, PRA01.getPreferredSize().height);

            //---- PRA02 ----
            PRA02.setComponentPopupMenu(null);
            PRA02.setName("PRA02");
            panel1.add(PRA02);
            PRA02.setBounds(640, 203, 110, PRA02.getPreferredSize().height);

            //---- PRA03 ----
            PRA03.setComponentPopupMenu(null);
            PRA03.setName("PRA03");
            panel1.add(PRA03);
            PRA03.setBounds(640, 228, 110, PRA03.getPreferredSize().height);

            //---- PRA04 ----
            PRA04.setComponentPopupMenu(null);
            PRA04.setName("PRA04");
            panel1.add(PRA04);
            PRA04.setBounds(640, 253, 110, PRA04.getPreferredSize().height);

            //---- PRA05 ----
            PRA05.setComponentPopupMenu(null);
            PRA05.setName("PRA05");
            panel1.add(PRA05);
            PRA05.setBounds(640, 278, 110, PRA05.getPreferredSize().height);

            //---- PRA06 ----
            PRA06.setComponentPopupMenu(null);
            PRA06.setName("PRA06");
            panel1.add(PRA06);
            PRA06.setBounds(640, 303, 110, PRA06.getPreferredSize().height);

            //---- PRA07 ----
            PRA07.setComponentPopupMenu(null);
            PRA07.setName("PRA07");
            panel1.add(PRA07);
            PRA07.setBounds(640, 328, 110, PRA07.getPreferredSize().height);

            //---- PRA08 ----
            PRA08.setComponentPopupMenu(null);
            PRA08.setName("PRA08");
            panel1.add(PRA08);
            PRA08.setBounds(640, 353, 110, PRA08.getPreferredSize().height);

            //---- PRA09 ----
            PRA09.setComponentPopupMenu(null);
            PRA09.setName("PRA09");
            panel1.add(PRA09);
            PRA09.setBounds(640, 378, 110, PRA09.getPreferredSize().height);

            //---- PRA10 ----
            PRA10.setComponentPopupMenu(null);
            PRA10.setName("PRA10");
            panel1.add(PRA10);
            PRA10.setBounds(640, 403, 110, PRA10.getPreferredSize().height);

            //---- UNV01 ----
            UNV01.setText("@UNV01@");
            UNV01.setComponentPopupMenu(null);
            UNV01.setName("UNV01");
            panel1.add(UNV01);
            UNV01.setBounds(755, 180, 40, UNV01.getPreferredSize().height);

            //---- UNV02 ----
            UNV02.setText("@UNV03@");
            UNV02.setComponentPopupMenu(null);
            UNV02.setName("UNV02");
            panel1.add(UNV02);
            UNV02.setBounds(755, 205, 40, UNV02.getPreferredSize().height);

            //---- UNV03 ----
            UNV03.setText("@UNV03@");
            UNV03.setComponentPopupMenu(null);
            UNV03.setName("UNV03");
            panel1.add(UNV03);
            UNV03.setBounds(755, 230, 40, UNV03.getPreferredSize().height);

            //---- UNV04 ----
            UNV04.setText("@UNV04@");
            UNV04.setComponentPopupMenu(null);
            UNV04.setName("UNV04");
            panel1.add(UNV04);
            UNV04.setBounds(755, 255, 40, UNV04.getPreferredSize().height);

            //---- UNV05 ----
            UNV05.setText("@UNV05@");
            UNV05.setComponentPopupMenu(null);
            UNV05.setName("UNV05");
            panel1.add(UNV05);
            UNV05.setBounds(755, 280, 40, UNV05.getPreferredSize().height);

            //---- UNV06 ----
            UNV06.setText("@UNV06@");
            UNV06.setComponentPopupMenu(null);
            UNV06.setName("UNV06");
            panel1.add(UNV06);
            UNV06.setBounds(755, 305, 40, UNV06.getPreferredSize().height);

            //---- UNV07 ----
            UNV07.setText("@UNV07@");
            UNV07.setComponentPopupMenu(null);
            UNV07.setName("UNV07");
            panel1.add(UNV07);
            UNV07.setBounds(755, 330, 40, UNV07.getPreferredSize().height);

            //---- UNV08 ----
            UNV08.setText("@UNV08@");
            UNV08.setComponentPopupMenu(null);
            UNV08.setName("UNV08");
            panel1.add(UNV08);
            UNV08.setBounds(755, 355, 40, UNV08.getPreferredSize().height);

            //---- UNV09 ----
            UNV09.setText("@UNV09@");
            UNV09.setComponentPopupMenu(null);
            UNV09.setName("UNV09");
            panel1.add(UNV09);
            UNV09.setBounds(755, 380, 40, UNV09.getPreferredSize().height);

            //---- UNV10 ----
            UNV10.setText("@UNV10@");
            UNV10.setComponentPopupMenu(null);
            UNV10.setName("UNV10");
            panel1.add(UNV10);
            UNV10.setBounds(755, 405, 40, UNV10.getPreferredSize().height);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(805, 180, 25, 120);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(805, 308, 25, 121);

            //---- TOTPRA ----
            TOTPRA.setComponentPopupMenu(BTD);
            TOTPRA.setText("@TOTPRA@");
            TOTPRA.setHorizontalAlignment(SwingConstants.RIGHT);
            TOTPRA.setName("TOTPRA");
            panel1.add(TOTPRA);
            TOTPRA.setBounds(640, 440, 110, TOTPRA.getPreferredSize().height);

            //---- OBJ_56 ----
            OBJ_56.setText("@MES53@");
            OBJ_56.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_56.setName("OBJ_56");
            panel1.add(OBJ_56);
            OBJ_56.setBounds(400, 440, 228, 28);

            //---- OBJ_55 ----
            OBJ_55.setText("Total colis");
            OBJ_55.setName("OBJ_55");
            panel1.add(OBJ_55);
            OBJ_55.setBounds(35, 440, 65, 28);

            //---- TOTCOL ----
            TOTCOL.setComponentPopupMenu(BTD);
            TOTCOL.setText("@TOTCOL@");
            TOTCOL.setHorizontalAlignment(SwingConstants.RIGHT);
            TOTCOL.setName("TOTCOL");
            panel1.add(TOTCOL);
            TOTCOL.setBounds(110, 440, 49, TOTCOL.getPreferredSize().height);

            //---- WTB01 ----
            WTB01.setComponentPopupMenu(BTD);
            WTB01.setName("WTB01");
            WTB01.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe1ActionPerformed(e);
              }
            });
            panel1.add(WTB01);
            WTB01.setBounds(10, 180, 20, 24);

            //---- WTB02 ----
            WTB02.setComponentPopupMenu(BTD);
            WTB02.setName("WTB02");
            WTB02.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe2ActionPerformed(e);
              }
            });
            panel1.add(WTB02);
            WTB02.setBounds(10, 205, 20, 24);

            //---- WTB03 ----
            WTB03.setComponentPopupMenu(BTD);
            WTB03.setName("WTB03");
            WTB03.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe3ActionPerformed(e);
              }
            });
            panel1.add(WTB03);
            WTB03.setBounds(10, 230, 20, 24);

            //---- WTB04 ----
            WTB04.setComponentPopupMenu(BTD);
            WTB04.setName("WTB04");
            WTB04.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe4ActionPerformed(e);
              }
            });
            panel1.add(WTB04);
            WTB04.setBounds(10, 255, 20, 24);

            //---- WTB05 ----
            WTB05.setComponentPopupMenu(BTD);
            WTB05.setName("WTB05");
            WTB05.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe5ActionPerformed(e);
              }
            });
            panel1.add(WTB05);
            WTB05.setBounds(10, 280, 20, 24);

            //---- WTB06 ----
            WTB06.setComponentPopupMenu(BTD);
            WTB06.setName("WTB06");
            WTB06.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe6ActionPerformed(e);
              }
            });
            panel1.add(WTB06);
            WTB06.setBounds(10, 305, 20, 24);

            //---- WTB07 ----
            WTB07.setComponentPopupMenu(BTD);
            WTB07.setName("WTB07");
            WTB07.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe7ActionPerformed(e);
              }
            });
            panel1.add(WTB07);
            WTB07.setBounds(10, 330, 20, 24);

            //---- WTB08 ----
            WTB08.setComponentPopupMenu(BTD);
            WTB08.setName("WTB08");
            WTB08.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe8ActionPerformed(e);
              }
            });
            panel1.add(WTB08);
            WTB08.setBounds(10, 355, 20, 24);

            //---- WTB09 ----
            WTB09.setComponentPopupMenu(BTD);
            WTB09.setName("WTB09");
            WTB09.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe9ActionPerformed(e);
              }
            });
            panel1.add(WTB09);
            WTB09.setBounds(10, 380, 20, 24);

            //---- WTB10 ----
            WTB10.setComponentPopupMenu(BTD);
            WTB10.setName("WTB10");
            WTB10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetailListe10ActionPerformed(e);
              }
            });
            panel1.add(WTB10);
            WTB10.setBounds(10, 405, 20, 24);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 844, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 522, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_14;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_34;
  private RiZoneSortie WETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_36;
  private RiZoneSortie WNPA;
  private RiZoneSortie WSOART;
  private RiZoneSortie WSOARL;
  private JLabel label1;
  private RiZoneSortie LB013;
  private RiZoneSortie LB023;
  private RiZoneSortie LB043;
  private RiZoneSortie LB033;
  private RiZoneSortie LB053;
  private RiZoneSortie LB063;
  private RiZoneSortie LB073;
  private RiZoneSortie LB093;
  private RiZoneSortie LB083;
  private RiZoneSortie LB103;
  private XRiTextField PRA01;
  private XRiTextField PRA02;
  private XRiTextField PRA03;
  private XRiTextField PRA04;
  private XRiTextField PRA05;
  private XRiTextField PRA06;
  private XRiTextField PRA07;
  private XRiTextField PRA08;
  private XRiTextField PRA09;
  private XRiTextField PRA10;
  private RiZoneSortie UNV01;
  private RiZoneSortie UNV02;
  private RiZoneSortie UNV03;
  private RiZoneSortie UNV04;
  private RiZoneSortie UNV05;
  private RiZoneSortie UNV06;
  private RiZoneSortie UNV07;
  private RiZoneSortie UNV08;
  private RiZoneSortie UNV09;
  private RiZoneSortie UNV10;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private RiZoneSortie TOTPRA;
  private JLabel OBJ_56;
  private JLabel OBJ_55;
  private RiZoneSortie TOTCOL;
  private SNBoutonDetail WTB01;
  private SNBoutonDetail WTB02;
  private SNBoutonDetail WTB03;
  private SNBoutonDetail WTB04;
  private SNBoutonDetail WTB05;
  private SNBoutonDetail WTB06;
  private SNBoutonDetail WTB07;
  private SNBoutonDetail WTB08;
  private SNBoutonDetail WTB09;
  private SNBoutonDetail WTB10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
