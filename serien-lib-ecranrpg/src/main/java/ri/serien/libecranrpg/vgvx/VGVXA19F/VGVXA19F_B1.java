
package ri.serien.libecranrpg.vgvx.VGVXA19F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGVXA19F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXA19F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LSL1R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL1R@")).trim());
    LSL2R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL2R@")).trim());
    LSL3R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL3R@")).trim());
    LSL4R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL4R@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITRE@"));
    
    

    p_bpresentation.setCodeEtablissement(XDETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    XDETB = new XRiTextField();
    OBJ_57 = new JLabel();
    OBJ_67 = new JLabel();
    XDMAG = new XRiTextField();
    XDAD1 = new XRiTextField();
    XDAD2 = new XRiTextField();
    XDAD3 = new XRiTextField();
    XDAD4 = new XRiTextField();
    LSL1R = new JLabel();
    LSL2R = new JLabel();
    LSL3R = new JLabel();
    LSL4R = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_35 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_42 = new JLabel();
    XDUC11 = new XRiTextField();
    XDUC12 = new XRiTextField();
    XDUC13 = new XRiTextField();
    XDUC14 = new XRiTextField();
    XDUC15 = new XRiTextField();
    XDUC16 = new XRiTextField();
    XDUC17 = new XRiTextField();
    XDUC18 = new XRiTextField();
    XDUC21 = new XRiTextField();
    XDUC22 = new XRiTextField();
    XDUC23 = new XRiTextField();
    XDUC24 = new XRiTextField();
    XDUC25 = new XRiTextField();
    XDUC26 = new XRiTextField();
    XDUC27 = new XRiTextField();
    XDUC28 = new XRiTextField();
    XDUC31 = new XRiTextField();
    XDUC32 = new XRiTextField();
    XDUC33 = new XRiTextField();
    XDUC34 = new XRiTextField();
    XDUC35 = new XRiTextField();
    XDUC36 = new XRiTextField();
    XDUC37 = new XRiTextField();
    XDUC38 = new XRiTextField();
    XDUC41 = new XRiTextField();
    XDUC42 = new XRiTextField();
    XDUC43 = new XRiTextField();
    XDUC44 = new XRiTextField();
    XDUC45 = new XRiTextField();
    XDUC46 = new XRiTextField();
    XDUC47 = new XRiTextField();
    XDUC48 = new XRiTextField();
    XDUC51 = new XRiTextField();
    XDUC52 = new XRiTextField();
    XDUC53 = new XRiTextField();
    XDUC54 = new XRiTextField();
    XDUC55 = new XRiTextField();
    XDUC56 = new XRiTextField();
    XDUC57 = new XRiTextField();
    XDUC58 = new XRiTextField();
    XDUC61 = new XRiTextField();
    XDUC62 = new XRiTextField();
    XDUC63 = new XRiTextField();
    XDUC64 = new XRiTextField();
    XDUC65 = new XRiTextField();
    XDUC66 = new XRiTextField();
    XDUC67 = new XRiTextField();
    XDUC68 = new XRiTextField();
    XDUC71 = new XRiTextField();
    XDUC72 = new XRiTextField();
    XDUC73 = new XRiTextField();
    XDUC74 = new XRiTextField();
    XDUC75 = new XRiTextField();
    XDUC76 = new XRiTextField();
    XDUC77 = new XRiTextField();
    XDUC78 = new XRiTextField();
    XDUC81 = new XRiTextField();
    XDUC82 = new XRiTextField();
    XDUC83 = new XRiTextField();
    XDUC84 = new XRiTextField();
    XDUC85 = new XRiTextField();
    XDUC86 = new XRiTextField();
    XDUC87 = new XRiTextField();
    XDUC88 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Combinaisons d'adresses");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- XDETB ----
          XDETB.setComponentPopupMenu(BTD);
          XDETB.setName("XDETB");
          p_tete_gauche.add(XDETB);
          XDETB.setBounds(120, 0, 40, XDETB.getPreferredSize().height);

          //---- OBJ_57 ----
          OBJ_57.setText("Etablissement");
          OBJ_57.setName("OBJ_57");
          p_tete_gauche.add(OBJ_57);
          OBJ_57.setBounds(5, 0, 90, 28);

          //---- OBJ_67 ----
          OBJ_67.setText("Magasin");
          OBJ_67.setName("OBJ_67");
          p_tete_gauche.add(OBJ_67);
          OBJ_67.setBounds(175, 0, 55, 28);

          //---- XDMAG ----
          XDMAG.setComponentPopupMenu(BTD);
          XDMAG.setName("XDMAG");
          p_tete_gauche.add(XDMAG);
          XDMAG.setBounds(235, 0, 34, XDMAG.getPreferredSize().height);

          //---- XDAD1 ----
          XDAD1.setComponentPopupMenu(BTD);
          XDAD1.setName("XDAD1");
          p_tete_gauche.add(XDAD1);
          XDAD1.setBounds(330, 0, 40, XDAD1.getPreferredSize().height);

          //---- XDAD2 ----
          XDAD2.setComponentPopupMenu(BTD);
          XDAD2.setName("XDAD2");
          p_tete_gauche.add(XDAD2);
          XDAD2.setBounds(430, 0, 40, XDAD2.getPreferredSize().height);

          //---- XDAD3 ----
          XDAD3.setComponentPopupMenu(BTD);
          XDAD3.setName("XDAD3");
          p_tete_gauche.add(XDAD3);
          XDAD3.setBounds(530, 0, 40, XDAD3.getPreferredSize().height);

          //---- XDAD4 ----
          XDAD4.setComponentPopupMenu(BTD);
          XDAD4.setName("XDAD4");
          p_tete_gauche.add(XDAD4);
          XDAD4.setBounds(630, 0, 40, XDAD4.getPreferredSize().height);

          //---- LSL1R ----
          LSL1R.setText("@LSL1R@");
          LSL1R.setName("LSL1R");
          p_tete_gauche.add(LSL1R);
          LSL1R.setBounds(290, 0, 40, 28);

          //---- LSL2R ----
          LSL2R.setText("@LSL2R@");
          LSL2R.setName("LSL2R");
          p_tete_gauche.add(LSL2R);
          LSL2R.setBounds(390, 0, 40, 28);

          //---- LSL3R ----
          LSL3R.setText("@LSL3R@");
          LSL3R.setName("LSL3R");
          p_tete_gauche.add(LSL3R);
          LSL3R.setBounds(490, 0, 40, 28);

          //---- LSL4R ----
          LSL4R.setText("@LSL4R@");
          LSL4R.setName("LSL4R");
          p_tete_gauche.add(LSL4R);
          LSL4R.setBounds(590, 0, 40, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(530, 430));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_35 ----
            OBJ_35.setText("Combinaison 1");
            OBJ_35.setFont(OBJ_35.getFont().deriveFont(OBJ_35.getFont().getStyle() | Font.BOLD));
            OBJ_35.setName("OBJ_35");
            panel1.add(OBJ_35);
            OBJ_35.setBounds(35, 26, 110, 28);

            //---- OBJ_36 ----
            OBJ_36.setText("Combinaison 2");
            OBJ_36.setFont(OBJ_36.getFont().deriveFont(OBJ_36.getFont().getStyle() | Font.BOLD));
            OBJ_36.setName("OBJ_36");
            panel1.add(OBJ_36);
            OBJ_36.setBounds(35, 71, 110, 28);

            //---- OBJ_37 ----
            OBJ_37.setText("Combinaison 3");
            OBJ_37.setFont(OBJ_37.getFont().deriveFont(OBJ_37.getFont().getStyle() | Font.BOLD));
            OBJ_37.setName("OBJ_37");
            panel1.add(OBJ_37);
            OBJ_37.setBounds(35, 116, 110, 28);

            //---- OBJ_38 ----
            OBJ_38.setText("Combinaison 4");
            OBJ_38.setFont(OBJ_38.getFont().deriveFont(OBJ_38.getFont().getStyle() | Font.BOLD));
            OBJ_38.setName("OBJ_38");
            panel1.add(OBJ_38);
            OBJ_38.setBounds(35, 161, 110, 28);

            //---- OBJ_39 ----
            OBJ_39.setText("Combinaison 5");
            OBJ_39.setFont(OBJ_39.getFont().deriveFont(OBJ_39.getFont().getStyle() | Font.BOLD));
            OBJ_39.setName("OBJ_39");
            panel1.add(OBJ_39);
            OBJ_39.setBounds(35, 206, 110, 28);

            //---- OBJ_40 ----
            OBJ_40.setText("Combinaison 6");
            OBJ_40.setFont(OBJ_40.getFont().deriveFont(OBJ_40.getFont().getStyle() | Font.BOLD));
            OBJ_40.setName("OBJ_40");
            panel1.add(OBJ_40);
            OBJ_40.setBounds(35, 251, 110, 28);

            //---- OBJ_41 ----
            OBJ_41.setText("Combinaison 7");
            OBJ_41.setFont(OBJ_41.getFont().deriveFont(OBJ_41.getFont().getStyle() | Font.BOLD));
            OBJ_41.setName("OBJ_41");
            panel1.add(OBJ_41);
            OBJ_41.setBounds(35, 296, 110, 28);

            //---- OBJ_42 ----
            OBJ_42.setText("Combinaison 8");
            OBJ_42.setFont(OBJ_42.getFont().deriveFont(OBJ_42.getFont().getStyle() | Font.BOLD));
            OBJ_42.setName("OBJ_42");
            panel1.add(OBJ_42);
            OBJ_42.setBounds(35, 341, 110, 28);

            //---- XDUC11 ----
            XDUC11.setComponentPopupMenu(BTD);
            XDUC11.setName("XDUC11");
            panel1.add(XDUC11);
            XDUC11.setBounds(175, 26, 30, XDUC11.getPreferredSize().height);

            //---- XDUC12 ----
            XDUC12.setComponentPopupMenu(BTD);
            XDUC12.setName("XDUC12");
            panel1.add(XDUC12);
            XDUC12.setBounds(211, 26, 30, XDUC12.getPreferredSize().height);

            //---- XDUC13 ----
            XDUC13.setComponentPopupMenu(BTD);
            XDUC13.setName("XDUC13");
            panel1.add(XDUC13);
            XDUC13.setBounds(247, 26, 30, XDUC13.getPreferredSize().height);

            //---- XDUC14 ----
            XDUC14.setComponentPopupMenu(BTD);
            XDUC14.setName("XDUC14");
            panel1.add(XDUC14);
            XDUC14.setBounds(283, 26, 30, XDUC14.getPreferredSize().height);

            //---- XDUC15 ----
            XDUC15.setComponentPopupMenu(BTD);
            XDUC15.setName("XDUC15");
            panel1.add(XDUC15);
            XDUC15.setBounds(319, 26, 30, XDUC15.getPreferredSize().height);

            //---- XDUC16 ----
            XDUC16.setComponentPopupMenu(BTD);
            XDUC16.setName("XDUC16");
            panel1.add(XDUC16);
            XDUC16.setBounds(355, 26, 30, XDUC16.getPreferredSize().height);

            //---- XDUC17 ----
            XDUC17.setComponentPopupMenu(BTD);
            XDUC17.setName("XDUC17");
            panel1.add(XDUC17);
            XDUC17.setBounds(391, 26, 30, XDUC17.getPreferredSize().height);

            //---- XDUC18 ----
            XDUC18.setComponentPopupMenu(BTD);
            XDUC18.setName("XDUC18");
            panel1.add(XDUC18);
            XDUC18.setBounds(427, 26, 30, XDUC18.getPreferredSize().height);

            //---- XDUC21 ----
            XDUC21.setComponentPopupMenu(BTD);
            XDUC21.setName("XDUC21");
            panel1.add(XDUC21);
            XDUC21.setBounds(175, 71, 30, XDUC21.getPreferredSize().height);

            //---- XDUC22 ----
            XDUC22.setComponentPopupMenu(BTD);
            XDUC22.setName("XDUC22");
            panel1.add(XDUC22);
            XDUC22.setBounds(211, 71, 30, XDUC22.getPreferredSize().height);

            //---- XDUC23 ----
            XDUC23.setComponentPopupMenu(BTD);
            XDUC23.setName("XDUC23");
            panel1.add(XDUC23);
            XDUC23.setBounds(247, 71, 30, XDUC23.getPreferredSize().height);

            //---- XDUC24 ----
            XDUC24.setComponentPopupMenu(BTD);
            XDUC24.setName("XDUC24");
            panel1.add(XDUC24);
            XDUC24.setBounds(283, 71, 30, XDUC24.getPreferredSize().height);

            //---- XDUC25 ----
            XDUC25.setComponentPopupMenu(BTD);
            XDUC25.setName("XDUC25");
            panel1.add(XDUC25);
            XDUC25.setBounds(319, 71, 30, XDUC25.getPreferredSize().height);

            //---- XDUC26 ----
            XDUC26.setComponentPopupMenu(BTD);
            XDUC26.setName("XDUC26");
            panel1.add(XDUC26);
            XDUC26.setBounds(355, 71, 30, XDUC26.getPreferredSize().height);

            //---- XDUC27 ----
            XDUC27.setComponentPopupMenu(BTD);
            XDUC27.setName("XDUC27");
            panel1.add(XDUC27);
            XDUC27.setBounds(391, 71, 30, XDUC27.getPreferredSize().height);

            //---- XDUC28 ----
            XDUC28.setComponentPopupMenu(BTD);
            XDUC28.setName("XDUC28");
            panel1.add(XDUC28);
            XDUC28.setBounds(427, 71, 30, XDUC28.getPreferredSize().height);

            //---- XDUC31 ----
            XDUC31.setComponentPopupMenu(BTD);
            XDUC31.setName("XDUC31");
            panel1.add(XDUC31);
            XDUC31.setBounds(175, 116, 30, XDUC31.getPreferredSize().height);

            //---- XDUC32 ----
            XDUC32.setComponentPopupMenu(BTD);
            XDUC32.setName("XDUC32");
            panel1.add(XDUC32);
            XDUC32.setBounds(211, 116, 30, XDUC32.getPreferredSize().height);

            //---- XDUC33 ----
            XDUC33.setComponentPopupMenu(BTD);
            XDUC33.setName("XDUC33");
            panel1.add(XDUC33);
            XDUC33.setBounds(247, 116, 30, XDUC33.getPreferredSize().height);

            //---- XDUC34 ----
            XDUC34.setComponentPopupMenu(BTD);
            XDUC34.setName("XDUC34");
            panel1.add(XDUC34);
            XDUC34.setBounds(283, 116, 30, XDUC34.getPreferredSize().height);

            //---- XDUC35 ----
            XDUC35.setComponentPopupMenu(BTD);
            XDUC35.setName("XDUC35");
            panel1.add(XDUC35);
            XDUC35.setBounds(319, 116, 30, XDUC35.getPreferredSize().height);

            //---- XDUC36 ----
            XDUC36.setComponentPopupMenu(BTD);
            XDUC36.setName("XDUC36");
            panel1.add(XDUC36);
            XDUC36.setBounds(355, 116, 30, XDUC36.getPreferredSize().height);

            //---- XDUC37 ----
            XDUC37.setComponentPopupMenu(BTD);
            XDUC37.setName("XDUC37");
            panel1.add(XDUC37);
            XDUC37.setBounds(391, 116, 30, XDUC37.getPreferredSize().height);

            //---- XDUC38 ----
            XDUC38.setComponentPopupMenu(BTD);
            XDUC38.setName("XDUC38");
            panel1.add(XDUC38);
            XDUC38.setBounds(427, 116, 30, XDUC38.getPreferredSize().height);

            //---- XDUC41 ----
            XDUC41.setComponentPopupMenu(BTD);
            XDUC41.setName("XDUC41");
            panel1.add(XDUC41);
            XDUC41.setBounds(175, 161, 30, XDUC41.getPreferredSize().height);

            //---- XDUC42 ----
            XDUC42.setComponentPopupMenu(BTD);
            XDUC42.setName("XDUC42");
            panel1.add(XDUC42);
            XDUC42.setBounds(211, 161, 30, XDUC42.getPreferredSize().height);

            //---- XDUC43 ----
            XDUC43.setComponentPopupMenu(BTD);
            XDUC43.setName("XDUC43");
            panel1.add(XDUC43);
            XDUC43.setBounds(247, 161, 30, XDUC43.getPreferredSize().height);

            //---- XDUC44 ----
            XDUC44.setComponentPopupMenu(BTD);
            XDUC44.setName("XDUC44");
            panel1.add(XDUC44);
            XDUC44.setBounds(283, 161, 30, XDUC44.getPreferredSize().height);

            //---- XDUC45 ----
            XDUC45.setComponentPopupMenu(BTD);
            XDUC45.setName("XDUC45");
            panel1.add(XDUC45);
            XDUC45.setBounds(319, 161, 30, XDUC45.getPreferredSize().height);

            //---- XDUC46 ----
            XDUC46.setComponentPopupMenu(BTD);
            XDUC46.setName("XDUC46");
            panel1.add(XDUC46);
            XDUC46.setBounds(355, 161, 30, XDUC46.getPreferredSize().height);

            //---- XDUC47 ----
            XDUC47.setComponentPopupMenu(BTD);
            XDUC47.setName("XDUC47");
            panel1.add(XDUC47);
            XDUC47.setBounds(391, 161, 30, XDUC47.getPreferredSize().height);

            //---- XDUC48 ----
            XDUC48.setComponentPopupMenu(BTD);
            XDUC48.setName("XDUC48");
            panel1.add(XDUC48);
            XDUC48.setBounds(427, 161, 30, XDUC48.getPreferredSize().height);

            //---- XDUC51 ----
            XDUC51.setComponentPopupMenu(BTD);
            XDUC51.setName("XDUC51");
            panel1.add(XDUC51);
            XDUC51.setBounds(175, 206, 30, XDUC51.getPreferredSize().height);

            //---- XDUC52 ----
            XDUC52.setComponentPopupMenu(BTD);
            XDUC52.setName("XDUC52");
            panel1.add(XDUC52);
            XDUC52.setBounds(211, 206, 30, XDUC52.getPreferredSize().height);

            //---- XDUC53 ----
            XDUC53.setComponentPopupMenu(BTD);
            XDUC53.setName("XDUC53");
            panel1.add(XDUC53);
            XDUC53.setBounds(247, 206, 30, XDUC53.getPreferredSize().height);

            //---- XDUC54 ----
            XDUC54.setComponentPopupMenu(BTD);
            XDUC54.setName("XDUC54");
            panel1.add(XDUC54);
            XDUC54.setBounds(283, 206, 30, XDUC54.getPreferredSize().height);

            //---- XDUC55 ----
            XDUC55.setComponentPopupMenu(BTD);
            XDUC55.setName("XDUC55");
            panel1.add(XDUC55);
            XDUC55.setBounds(319, 206, 30, XDUC55.getPreferredSize().height);

            //---- XDUC56 ----
            XDUC56.setComponentPopupMenu(BTD);
            XDUC56.setName("XDUC56");
            panel1.add(XDUC56);
            XDUC56.setBounds(355, 206, 30, XDUC56.getPreferredSize().height);

            //---- XDUC57 ----
            XDUC57.setComponentPopupMenu(BTD);
            XDUC57.setName("XDUC57");
            panel1.add(XDUC57);
            XDUC57.setBounds(391, 206, 30, XDUC57.getPreferredSize().height);

            //---- XDUC58 ----
            XDUC58.setComponentPopupMenu(BTD);
            XDUC58.setName("XDUC58");
            panel1.add(XDUC58);
            XDUC58.setBounds(427, 206, 30, XDUC58.getPreferredSize().height);

            //---- XDUC61 ----
            XDUC61.setComponentPopupMenu(BTD);
            XDUC61.setName("XDUC61");
            panel1.add(XDUC61);
            XDUC61.setBounds(175, 251, 30, XDUC61.getPreferredSize().height);

            //---- XDUC62 ----
            XDUC62.setComponentPopupMenu(BTD);
            XDUC62.setName("XDUC62");
            panel1.add(XDUC62);
            XDUC62.setBounds(211, 251, 30, XDUC62.getPreferredSize().height);

            //---- XDUC63 ----
            XDUC63.setComponentPopupMenu(BTD);
            XDUC63.setName("XDUC63");
            panel1.add(XDUC63);
            XDUC63.setBounds(247, 251, 30, XDUC63.getPreferredSize().height);

            //---- XDUC64 ----
            XDUC64.setComponentPopupMenu(BTD);
            XDUC64.setName("XDUC64");
            panel1.add(XDUC64);
            XDUC64.setBounds(283, 251, 30, XDUC64.getPreferredSize().height);

            //---- XDUC65 ----
            XDUC65.setComponentPopupMenu(BTD);
            XDUC65.setName("XDUC65");
            panel1.add(XDUC65);
            XDUC65.setBounds(319, 251, 30, XDUC65.getPreferredSize().height);

            //---- XDUC66 ----
            XDUC66.setComponentPopupMenu(BTD);
            XDUC66.setName("XDUC66");
            panel1.add(XDUC66);
            XDUC66.setBounds(355, 251, 30, XDUC66.getPreferredSize().height);

            //---- XDUC67 ----
            XDUC67.setComponentPopupMenu(BTD);
            XDUC67.setName("XDUC67");
            panel1.add(XDUC67);
            XDUC67.setBounds(391, 251, 30, XDUC67.getPreferredSize().height);

            //---- XDUC68 ----
            XDUC68.setComponentPopupMenu(BTD);
            XDUC68.setName("XDUC68");
            panel1.add(XDUC68);
            XDUC68.setBounds(427, 251, 30, XDUC68.getPreferredSize().height);

            //---- XDUC71 ----
            XDUC71.setComponentPopupMenu(BTD);
            XDUC71.setName("XDUC71");
            panel1.add(XDUC71);
            XDUC71.setBounds(175, 296, 30, XDUC71.getPreferredSize().height);

            //---- XDUC72 ----
            XDUC72.setComponentPopupMenu(BTD);
            XDUC72.setName("XDUC72");
            panel1.add(XDUC72);
            XDUC72.setBounds(211, 296, 30, XDUC72.getPreferredSize().height);

            //---- XDUC73 ----
            XDUC73.setComponentPopupMenu(BTD);
            XDUC73.setName("XDUC73");
            panel1.add(XDUC73);
            XDUC73.setBounds(247, 296, 30, XDUC73.getPreferredSize().height);

            //---- XDUC74 ----
            XDUC74.setComponentPopupMenu(BTD);
            XDUC74.setName("XDUC74");
            panel1.add(XDUC74);
            XDUC74.setBounds(283, 296, 30, XDUC74.getPreferredSize().height);

            //---- XDUC75 ----
            XDUC75.setComponentPopupMenu(BTD);
            XDUC75.setName("XDUC75");
            panel1.add(XDUC75);
            XDUC75.setBounds(319, 296, 30, XDUC75.getPreferredSize().height);

            //---- XDUC76 ----
            XDUC76.setComponentPopupMenu(BTD);
            XDUC76.setName("XDUC76");
            panel1.add(XDUC76);
            XDUC76.setBounds(355, 296, 30, XDUC76.getPreferredSize().height);

            //---- XDUC77 ----
            XDUC77.setComponentPopupMenu(BTD);
            XDUC77.setName("XDUC77");
            panel1.add(XDUC77);
            XDUC77.setBounds(391, 296, 30, XDUC77.getPreferredSize().height);

            //---- XDUC78 ----
            XDUC78.setComponentPopupMenu(BTD);
            XDUC78.setName("XDUC78");
            panel1.add(XDUC78);
            XDUC78.setBounds(427, 296, 30, XDUC78.getPreferredSize().height);

            //---- XDUC81 ----
            XDUC81.setComponentPopupMenu(BTD);
            XDUC81.setName("XDUC81");
            panel1.add(XDUC81);
            XDUC81.setBounds(175, 341, 30, XDUC81.getPreferredSize().height);

            //---- XDUC82 ----
            XDUC82.setComponentPopupMenu(BTD);
            XDUC82.setName("XDUC82");
            panel1.add(XDUC82);
            XDUC82.setBounds(211, 341, 30, XDUC82.getPreferredSize().height);

            //---- XDUC83 ----
            XDUC83.setComponentPopupMenu(BTD);
            XDUC83.setName("XDUC83");
            panel1.add(XDUC83);
            XDUC83.setBounds(247, 341, 30, XDUC83.getPreferredSize().height);

            //---- XDUC84 ----
            XDUC84.setComponentPopupMenu(BTD);
            XDUC84.setName("XDUC84");
            panel1.add(XDUC84);
            XDUC84.setBounds(283, 341, 30, XDUC84.getPreferredSize().height);

            //---- XDUC85 ----
            XDUC85.setComponentPopupMenu(BTD);
            XDUC85.setName("XDUC85");
            panel1.add(XDUC85);
            XDUC85.setBounds(319, 341, 30, XDUC85.getPreferredSize().height);

            //---- XDUC86 ----
            XDUC86.setComponentPopupMenu(BTD);
            XDUC86.setName("XDUC86");
            panel1.add(XDUC86);
            XDUC86.setBounds(355, 341, 30, XDUC86.getPreferredSize().height);

            //---- XDUC87 ----
            XDUC87.setComponentPopupMenu(BTD);
            XDUC87.setName("XDUC87");
            panel1.add(XDUC87);
            XDUC87.setBounds(391, 341, 30, XDUC87.getPreferredSize().height);

            //---- XDUC88 ----
            XDUC88.setComponentPopupMenu(BTD);
            XDUC88.setName("XDUC88");
            panel1.add(XDUC88);
            XDUC88.setBounds(427, 341, 30, XDUC88.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 504, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField XDETB;
  private JLabel OBJ_57;
  private JLabel OBJ_67;
  private XRiTextField XDMAG;
  private XRiTextField XDAD1;
  private XRiTextField XDAD2;
  private XRiTextField XDAD3;
  private XRiTextField XDAD4;
  private JLabel LSL1R;
  private JLabel LSL2R;
  private JLabel LSL3R;
  private JLabel LSL4R;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_35;
  private JLabel OBJ_36;
  private JLabel OBJ_37;
  private JLabel OBJ_38;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private JLabel OBJ_41;
  private JLabel OBJ_42;
  private XRiTextField XDUC11;
  private XRiTextField XDUC12;
  private XRiTextField XDUC13;
  private XRiTextField XDUC14;
  private XRiTextField XDUC15;
  private XRiTextField XDUC16;
  private XRiTextField XDUC17;
  private XRiTextField XDUC18;
  private XRiTextField XDUC21;
  private XRiTextField XDUC22;
  private XRiTextField XDUC23;
  private XRiTextField XDUC24;
  private XRiTextField XDUC25;
  private XRiTextField XDUC26;
  private XRiTextField XDUC27;
  private XRiTextField XDUC28;
  private XRiTextField XDUC31;
  private XRiTextField XDUC32;
  private XRiTextField XDUC33;
  private XRiTextField XDUC34;
  private XRiTextField XDUC35;
  private XRiTextField XDUC36;
  private XRiTextField XDUC37;
  private XRiTextField XDUC38;
  private XRiTextField XDUC41;
  private XRiTextField XDUC42;
  private XRiTextField XDUC43;
  private XRiTextField XDUC44;
  private XRiTextField XDUC45;
  private XRiTextField XDUC46;
  private XRiTextField XDUC47;
  private XRiTextField XDUC48;
  private XRiTextField XDUC51;
  private XRiTextField XDUC52;
  private XRiTextField XDUC53;
  private XRiTextField XDUC54;
  private XRiTextField XDUC55;
  private XRiTextField XDUC56;
  private XRiTextField XDUC57;
  private XRiTextField XDUC58;
  private XRiTextField XDUC61;
  private XRiTextField XDUC62;
  private XRiTextField XDUC63;
  private XRiTextField XDUC64;
  private XRiTextField XDUC65;
  private XRiTextField XDUC66;
  private XRiTextField XDUC67;
  private XRiTextField XDUC68;
  private XRiTextField XDUC71;
  private XRiTextField XDUC72;
  private XRiTextField XDUC73;
  private XRiTextField XDUC74;
  private XRiTextField XDUC75;
  private XRiTextField XDUC76;
  private XRiTextField XDUC77;
  private XRiTextField XDUC78;
  private XRiTextField XDUC81;
  private XRiTextField XDUC82;
  private XRiTextField XDUC83;
  private XRiTextField XDUC84;
  private XRiTextField XDUC85;
  private XRiTextField XDUC86;
  private XRiTextField XDUC87;
  private XRiTextField XDUC88;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
