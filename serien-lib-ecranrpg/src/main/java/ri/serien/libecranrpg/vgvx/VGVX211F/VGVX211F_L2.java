
package ri.serien.libecranrpg.vgvx.VGVX211F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX211F_L2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX211F_L2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    QTFABX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTFABX@")).trim());
    A1UNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    WDISLX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISLX@")).trim());
    WQPDI0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQPDI0@")).trim());
    A1UNL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNL@")).trim());
    SLCNDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SLCNDX@")).trim());
    QTEX1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTEX1@")).trim());
    QTEX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTEX2@")).trim());
    QTEX3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTEX3@")).trim());
    QTEX4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTEX4@")).trim());
    QTEX5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTEX5@")).trim());
    QTEX6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTEX6@")).trim());
    QTEX7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTEX7@")).trim());
    QTEX8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTEX8@")).trim());
    W21QPX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W21QPX@")).trim());
    W21QTX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W21QTX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    gererLesErreurs("19");
    
    
    if (lexique.isTrue("(91)AND(92)")) {
      label1.setText("Annulation de découpe");
      panel3.setVisible(false);
      panel4.setVisible(false);
      panel5.setVisible(true);
    }
    if (lexique.isTrue("(91)AND(N92)")) {
      label1.setText("Opération de découpe");
      panel3.setVisible(false);
      panel4.setVisible(true);
      panel5.setVisible(false);
    }
    if (lexique.isTrue("(N91)AND(92)")) {
      label1.setText("Ordre de fabrication");
      panel3.setVisible(true);
      panel4.setVisible(false);
      panel5.setVisible(false);
    }
    
    label4.setText(lexique.HostFieldGetData("HLD01").substring(0, 10));
    label5.setText(lexique.HostFieldGetData("HLD01").substring(10, 20));
    label6.setText(lexique.HostFieldGetData("HLD01").substring(20, 30));
    
    // TODO Icones
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label2 = new JLabel();
    P21ART = new XRiTextField();
    A1LIB = new XRiTextField();
    panel3 = new JPanel();
    label3 = new JLabel();
    QTFABX = new RiZoneSortie();
    A1UNS = new RiZoneSortie();
    panel4 = new JPanel();
    label7 = new JLabel();
    WDISLX = new RiZoneSortie();
    panel5 = new JPanel();
    label8 = new JLabel();
    WQPDI0 = new RiZoneSortie();
    A1UNL = new RiZoneSortie();
    SLCNDX = new RiZoneSortie();
    label1 = new JLabel();
    panel2 = new JPanel();
    QTPX1 = new XRiTextField();
    QTPX2 = new XRiTextField();
    QTPX3 = new XRiTextField();
    QTPX4 = new XRiTextField();
    QTPX5 = new XRiTextField();
    QTPX6 = new XRiTextField();
    QTPX7 = new XRiTextField();
    QTPX8 = new XRiTextField();
    EKUX1 = new XRiTextField();
    EKUX2 = new XRiTextField();
    EKUX3 = new XRiTextField();
    EKUX4 = new XRiTextField();
    EKUX5 = new XRiTextField();
    EKUX6 = new XRiTextField();
    EKUX7 = new XRiTextField();
    EKUX8 = new XRiTextField();
    QTEX1 = new RiZoneSortie();
    QTEX2 = new RiZoneSortie();
    QTEX3 = new RiZoneSortie();
    QTEX4 = new RiZoneSortie();
    QTEX5 = new RiZoneSortie();
    QTEX6 = new RiZoneSortie();
    QTEX7 = new RiZoneSortie();
    QTEX8 = new RiZoneSortie();
    W21QPX = new RiZoneSortie();
    W21QTX = new RiZoneSortie();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(650, 180));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- label2 ----
          label2.setText("Article");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(20, 25, 70, 21);

          //---- P21ART ----
          P21ART.setName("P21ART");
          panel1.add(P21ART);
          P21ART.setBounds(105, 21, 214, P21ART.getPreferredSize().height);

          //---- A1LIB ----
          A1LIB.setName("A1LIB");
          panel1.add(A1LIB);
          A1LIB.setBounds(105, 50, 314, A1LIB.getPreferredSize().height);

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- label3 ----
            label3.setText("Quantit\u00e9 fabricable");
            label3.setName("label3");
            panel3.add(label3);
            label3.setBounds(10, 15, 120, 21);

            //---- QTFABX ----
            QTFABX.setText("@QTFABX@");
            QTFABX.setName("QTFABX");
            panel3.add(QTFABX);
            QTFABX.setBounds(170, 13, 114, QTFABX.getPreferredSize().height);

            //---- A1UNS ----
            A1UNS.setText("@A1UNS@");
            A1UNS.setName("A1UNS");
            panel3.add(A1UNS);
            A1UNS.setBounds(295, 13, 34, A1UNS.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel3);
          panel3.setBounds(10, 85, 425, 50);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- label7 ----
            label7.setText("Disponible");
            label7.setName("label7");
            panel4.add(label7);
            label7.setBounds(10, 15, 120, 21);

            //---- WDISLX ----
            WDISLX.setText("@WDISLX@");
            WDISLX.setName("WDISLX");
            panel4.add(WDISLX);
            WDISLX.setBounds(170, 13, 164, WDISLX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel4);
          panel4.setBounds(10, 85, 425, 50);

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- label8 ----
            label8.setText("Quantit\u00e9 \u00e0 r\u00e9int\u00e9grer");
            label8.setName("label8");
            panel5.add(label8);
            label8.setBounds(10, 15, 130, 21);

            //---- WQPDI0 ----
            WQPDI0.setText("@WQPDI0@");
            WQPDI0.setName("WQPDI0");
            panel5.add(WQPDI0);
            WQPDI0.setBounds(170, 13, 36, WQPDI0.getPreferredSize().height);

            //---- A1UNL ----
            A1UNL.setText("@A1UNL@");
            A1UNL.setName("A1UNL");
            panel5.add(A1UNL);
            A1UNL.setBounds(218, 13, 34, A1UNL.getPreferredSize().height);

            //---- SLCNDX ----
            SLCNDX.setText("@SLCNDX@");
            SLCNDX.setName("SLCNDX");
            panel5.add(SLCNDX);
            SLCNDX.setBounds(264, 13, 94, SLCNDX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel5);
          panel5.setBounds(10, 85, 425, 50);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 25, 460, 145);

        //---- label1 ----
        label1.setText("text");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
        label1.setName("label1");
        p_contenu.add(label1);
        label1.setBounds(20, 5, 445, label1.getPreferredSize().height);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- QTPX1 ----
          QTPX1.setHorizontalAlignment(SwingConstants.RIGHT);
          QTPX1.setName("QTPX1");
          panel2.add(QTPX1);
          QTPX1.setBounds(45, 40, 104, QTPX1.getPreferredSize().height);

          //---- QTPX2 ----
          QTPX2.setHorizontalAlignment(SwingConstants.RIGHT);
          QTPX2.setName("QTPX2");
          panel2.add(QTPX2);
          QTPX2.setBounds(45, 70, 104, QTPX2.getPreferredSize().height);

          //---- QTPX3 ----
          QTPX3.setHorizontalAlignment(SwingConstants.RIGHT);
          QTPX3.setName("QTPX3");
          panel2.add(QTPX3);
          QTPX3.setBounds(45, 100, 104, QTPX3.getPreferredSize().height);

          //---- QTPX4 ----
          QTPX4.setHorizontalAlignment(SwingConstants.RIGHT);
          QTPX4.setName("QTPX4");
          panel2.add(QTPX4);
          QTPX4.setBounds(45, 130, 104, QTPX4.getPreferredSize().height);

          //---- QTPX5 ----
          QTPX5.setHorizontalAlignment(SwingConstants.RIGHT);
          QTPX5.setName("QTPX5");
          panel2.add(QTPX5);
          QTPX5.setBounds(45, 160, 104, QTPX5.getPreferredSize().height);

          //---- QTPX6 ----
          QTPX6.setHorizontalAlignment(SwingConstants.RIGHT);
          QTPX6.setName("QTPX6");
          panel2.add(QTPX6);
          QTPX6.setBounds(45, 190, 104, QTPX6.getPreferredSize().height);

          //---- QTPX7 ----
          QTPX7.setHorizontalAlignment(SwingConstants.RIGHT);
          QTPX7.setName("QTPX7");
          panel2.add(QTPX7);
          QTPX7.setBounds(45, 220, 104, QTPX7.getPreferredSize().height);

          //---- QTPX8 ----
          QTPX8.setHorizontalAlignment(SwingConstants.RIGHT);
          QTPX8.setName("QTPX8");
          panel2.add(QTPX8);
          QTPX8.setBounds(45, 250, 104, QTPX8.getPreferredSize().height);

          //---- EKUX1 ----
          EKUX1.setHorizontalAlignment(SwingConstants.RIGHT);
          EKUX1.setName("EKUX1");
          panel2.add(EKUX1);
          EKUX1.setBounds(170, 40, 104, EKUX1.getPreferredSize().height);

          //---- EKUX2 ----
          EKUX2.setHorizontalAlignment(SwingConstants.RIGHT);
          EKUX2.setName("EKUX2");
          panel2.add(EKUX2);
          EKUX2.setBounds(170, 70, 104, EKUX2.getPreferredSize().height);

          //---- EKUX3 ----
          EKUX3.setHorizontalAlignment(SwingConstants.RIGHT);
          EKUX3.setName("EKUX3");
          panel2.add(EKUX3);
          EKUX3.setBounds(170, 100, 104, EKUX3.getPreferredSize().height);

          //---- EKUX4 ----
          EKUX4.setHorizontalAlignment(SwingConstants.RIGHT);
          EKUX4.setName("EKUX4");
          panel2.add(EKUX4);
          EKUX4.setBounds(170, 130, 104, EKUX4.getPreferredSize().height);

          //---- EKUX5 ----
          EKUX5.setHorizontalAlignment(SwingConstants.RIGHT);
          EKUX5.setName("EKUX5");
          panel2.add(EKUX5);
          EKUX5.setBounds(170, 160, 104, EKUX5.getPreferredSize().height);

          //---- EKUX6 ----
          EKUX6.setHorizontalAlignment(SwingConstants.RIGHT);
          EKUX6.setName("EKUX6");
          panel2.add(EKUX6);
          EKUX6.setBounds(170, 190, 104, EKUX6.getPreferredSize().height);

          //---- EKUX7 ----
          EKUX7.setHorizontalAlignment(SwingConstants.RIGHT);
          EKUX7.setName("EKUX7");
          panel2.add(EKUX7);
          EKUX7.setBounds(170, 220, 104, EKUX7.getPreferredSize().height);

          //---- EKUX8 ----
          EKUX8.setHorizontalAlignment(SwingConstants.RIGHT);
          EKUX8.setName("EKUX8");
          panel2.add(EKUX8);
          EKUX8.setBounds(170, 250, 104, EKUX8.getPreferredSize().height);

          //---- QTEX1 ----
          QTEX1.setText("@QTEX1@");
          QTEX1.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX1.setName("QTEX1");
          panel2.add(QTEX1);
          QTEX1.setBounds(295, 40, 104, QTEX1.getPreferredSize().height);

          //---- QTEX2 ----
          QTEX2.setText("@QTEX2@");
          QTEX2.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX2.setName("QTEX2");
          panel2.add(QTEX2);
          QTEX2.setBounds(295, 70, 104, QTEX2.getPreferredSize().height);

          //---- QTEX3 ----
          QTEX3.setText("@QTEX3@");
          QTEX3.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX3.setName("QTEX3");
          panel2.add(QTEX3);
          QTEX3.setBounds(295, 100, 104, QTEX3.getPreferredSize().height);

          //---- QTEX4 ----
          QTEX4.setText("@QTEX4@");
          QTEX4.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX4.setName("QTEX4");
          panel2.add(QTEX4);
          QTEX4.setBounds(295, 130, 104, QTEX4.getPreferredSize().height);

          //---- QTEX5 ----
          QTEX5.setText("@QTEX5@");
          QTEX5.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX5.setName("QTEX5");
          panel2.add(QTEX5);
          QTEX5.setBounds(295, 160, 104, QTEX5.getPreferredSize().height);

          //---- QTEX6 ----
          QTEX6.setText("@QTEX6@");
          QTEX6.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX6.setName("QTEX6");
          panel2.add(QTEX6);
          QTEX6.setBounds(295, 190, 104, QTEX6.getPreferredSize().height);

          //---- QTEX7 ----
          QTEX7.setText("@QTEX7@");
          QTEX7.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX7.setName("QTEX7");
          panel2.add(QTEX7);
          QTEX7.setBounds(295, 220, 104, QTEX7.getPreferredSize().height);

          //---- QTEX8 ----
          QTEX8.setText("@QTEX8@");
          QTEX8.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX8.setName("QTEX8");
          panel2.add(QTEX8);
          QTEX8.setBounds(295, 250, 104, QTEX8.getPreferredSize().height);

          //---- W21QPX ----
          W21QPX.setText("@W21QPX@");
          W21QPX.setFont(W21QPX.getFont().deriveFont(W21QPX.getFont().getStyle() | Font.BOLD));
          W21QPX.setHorizontalAlignment(SwingConstants.RIGHT);
          W21QPX.setName("W21QPX");
          panel2.add(W21QPX);
          W21QPX.setBounds(45, 290, 104, W21QPX.getPreferredSize().height);

          //---- W21QTX ----
          W21QTX.setText("@W21QTX@");
          W21QTX.setFont(W21QTX.getFont().deriveFont(W21QTX.getFont().getStyle() | Font.BOLD));
          W21QTX.setHorizontalAlignment(SwingConstants.RIGHT);
          W21QTX.setName("W21QTX");
          panel2.add(W21QTX);
          W21QTX.setBounds(295, 290, 104, W21QTX.getPreferredSize().height);

          //---- label4 ----
          label4.setText("text");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setName("label4");
          panel2.add(label4);
          label4.setBounds(45, 15, 104, 20);

          //---- label5 ----
          label5.setText("text");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setName("label5");
          panel2.add(label5);
          label5.setBounds(175, 15, 104, 20);

          //---- label6 ----
          label6.setText("text");
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setName("label6");
          panel2.add(label6);
          label6.setBounds(295, 15, 104, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 180, 460, 330);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label2;
  private XRiTextField P21ART;
  private XRiTextField A1LIB;
  private JPanel panel3;
  private JLabel label3;
  private RiZoneSortie QTFABX;
  private RiZoneSortie A1UNS;
  private JPanel panel4;
  private JLabel label7;
  private RiZoneSortie WDISLX;
  private JPanel panel5;
  private JLabel label8;
  private RiZoneSortie WQPDI0;
  private RiZoneSortie A1UNL;
  private RiZoneSortie SLCNDX;
  private JLabel label1;
  private JPanel panel2;
  private XRiTextField QTPX1;
  private XRiTextField QTPX2;
  private XRiTextField QTPX3;
  private XRiTextField QTPX4;
  private XRiTextField QTPX5;
  private XRiTextField QTPX6;
  private XRiTextField QTPX7;
  private XRiTextField QTPX8;
  private XRiTextField EKUX1;
  private XRiTextField EKUX2;
  private XRiTextField EKUX3;
  private XRiTextField EKUX4;
  private XRiTextField EKUX5;
  private XRiTextField EKUX6;
  private XRiTextField EKUX7;
  private XRiTextField EKUX8;
  private RiZoneSortie QTEX1;
  private RiZoneSortie QTEX2;
  private RiZoneSortie QTEX3;
  private RiZoneSortie QTEX4;
  private RiZoneSortie QTEX5;
  private RiZoneSortie QTEX6;
  private RiZoneSortie QTEX7;
  private RiZoneSortie QTEX8;
  private RiZoneSortie W21QPX;
  private RiZoneSortie W21QTX;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
