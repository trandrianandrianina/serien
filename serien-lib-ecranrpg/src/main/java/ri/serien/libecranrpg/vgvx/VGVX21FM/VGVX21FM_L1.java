
package ri.serien.libecranrpg.vgvx.VGVX21FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX21FM_L1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX21FM_L1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    A1CNDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1CNDX@")).trim());
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNL@")).trim());
    WDISX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    WDISUN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISUN@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLDALA@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1QT2@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1QT3@")).trim());
    WCLPL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCLPL@")).trim());
    X1PPC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@X1PPC@")).trim());
    X1PPL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@X1PPL@")).trim());
    X1PPP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@X1PPP@")).trim());
    OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNL@")).trim());
    NUM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM1@")).trim());
    QTSX1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX1@")).trim());
    NUM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM2@")).trim());
    QTSX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX2@")).trim());
    NUM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM3@")).trim());
    QTSX3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX3@")).trim());
    NUM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM4@")).trim());
    QTSX4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX4@")).trim());
    NUM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM5@")).trim());
    QTSX5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX5@")).trim());
    NUM6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM6@")).trim());
    QTSX6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX6@")).trim());
    NUM7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM7@")).trim());
    QTSX7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX7@")).trim());
    NUM8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM8@")).trim());
    QTSX8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX8@")).trim());
    NUM9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM9@")).trim());
    QTSX9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX9@")).trim());
    NUM10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM10@")).trim());
    QTSX10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX10@")).trim());
    lb_a_virer.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Saisie de numéros de lots pour l'article @P21ART@ @A1LIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    riSousMenu15.setVisible(!lexique.HostFieldGetData("WCF15").trim().equalsIgnoreCase(""));
    OBJ_13.setVisible(!lexique.HostFieldGetData("WCF13").trim().equalsIgnoreCase(""));
    OBJ_14.setVisible(!lexique.HostFieldGetData("WCF14").trim().equalsIgnoreCase(""));
    
    OBJ_43.setVisible(!lexique.HostFieldGetData("WCLPL").trim().equalsIgnoreCase("") & lexique.isPresent("WCLPL"));
    OBJ_45.setVisible(!lexique.HostFieldGetData("WCLPL").trim().equalsIgnoreCase("") & lexique.isPresent("WCLPL"));
    OBJ_36.setVisible(lexique.isPresent("A1CNDX"));
    X1PPP.setEnabled(lexique.isPresent("X1PPP"));
    X1PPL.setEnabled(lexique.isPresent("X1PPL"));
    X1PPC.setEnabled(lexique.isPresent("X1PPC"));
    WDISUN.setEnabled(lexique.isPresent("WDISUN"));
    OBJ_46.setVisible(lexique.isPresent("A1QT2"));
    WCLPL.setVisible(!lexique.HostFieldGetData("WCLPL").trim().equalsIgnoreCase(""));
    WCLPL.setEnabled(lexique.isPresent("WCLPL"));
    OBJ_49.setVisible(lexique.isPresent("A1QT3"));
    OBJ_47.setVisible(lexique.isPresent("A1QT2"));
    OBJ_48.setVisible(lexique.isPresent("A1QT3"));
    // P21QPX.setVisible(!lexique.HostFieldGetData("HD02").trim().equalsIgnoreCase("ngueur*Bb") &
    // lexique.isPresent("P21QPX"));
    // P21QTX_doublon_33.setVisible(lexique.HostFieldGetData("HD02").equalsIgnoreCase("ngueur*Bb") &
    // lexique.isPresent("P21QTX"));
    // P21QTX.setVisible(!lexique.HostFieldGetData("HD02").trim().equalsIgnoreCase("ngueur*Bb") &
    // lexique.isPresent("P21QTX"));
    A1CNDX.setVisible(lexique.isPresent("A1CNDX"));
    WDISX.setEnabled(lexique.isPresent("WDISX"));
    OBJ_34.setVisible(lexique.isPresent("A1CNDX"));
    // P21ART.setVisible( lexique.isPresent("P21ART"));
    OBJ_50.setVisible(lexique.isPresent("WLDALA"));
    // OBJ_20.setVisible( lexique.isPresent("CF20"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Saisie de numéros de lots pour l'article @P21ART@"));
    
    // Gestion du focus avec les touches flèches
    final XRiTextField[] ENL1table = { ENL101, ENL102, ENL103, ENL104, ENL105, ENL106, ENL107, ENL108, ENL109, ENL110 };
    final XRiTextField[] ENL2table = { ENL201, ENL202, ENL203, ENL204, ENL205, ENL206, ENL207, ENL208, ENL209, ENL210 };
    final XRiTextField[] QTEXtable = { QTEX1, QTEX2, QTEX3, QTEX4, QTEX5, QTEX6, QTEX7, QTEX8, QTEX9, QTEX10 };
    final XRiTextField[] ZPLtable = { ZPL01, ZPL02, ZPL03, ZPL04, ZPL05, ZPL06, ZPL07, ZPL08, ZPL09, ZPL10 };
    
    for (int i = 0; i < ENL1table.length; i++) {
      final int indice = i;
      
      ENL1table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              ENL2table[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                ENL1table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                ENL1table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      ENL2table[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              QTEXtable[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              ENL1table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                ENL2table[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                ENL2table[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      QTEXtable[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              ZPLtable[indice].requestFocus();
              break;
            case KeyEvent.VK_LEFT:
              ENL2table[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                QTEXtable[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                QTEXtable[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
      ZPLtable[i].addKeyListener(new KeyListener() {
        @Override
        public void keyPressed(KeyEvent e) {
          switch (e.getKeyCode()) {
            case KeyEvent.VK_RIGHT:
              
              break;
            case KeyEvent.VK_LEFT:
              QTEXtable[indice].requestFocus();
              break;
            case KeyEvent.VK_UP:
              if (indice > 0) {
                ZPLtable[indice - 1].requestFocus();
              }
              break;
            case KeyEvent.VK_DOWN:
              if (indice < ENL1table.length - 1) {
                ZPLtable[indice + 1].requestFocus();
              }
              break;
          }
        }
        
        public void keyReleased(KeyEvent e) {
        }
        
        public void keyTyped(KeyEvent e) {
        }
        
      });
      
    }
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    // riMenu_bt4.setIcon(lexique.getImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_34 = new JLabel();
    A1CNDX = new RiZoneSortie();
    OBJ_36 = new RiZoneSortie();
    OBJ_31 = new JLabel();
    WDISX = new RiZoneSortie();
    WDISUN = new RiZoneSortie();
    OBJ_50 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_47 = new RiZoneSortie();
    OBJ_49 = new RiZoneSortie();
    WCLPL = new RiZoneSortie();
    OBJ_46 = new JLabel();
    X1PPC = new RiZoneSortie();
    X1PPL = new RiZoneSortie();
    X1PPP = new RiZoneSortie();
    OBJ_45 = new RiZoneSortie();
    OBJ_43 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_41 = new JLabel();
    panel2 = new JPanel();
    NUM1 = new RiZoneSortie();
    ENL101 = new XRiTextField();
    ENL201 = new XRiTextField();
    QTSX1 = new RiZoneSortie();
    QTEX1 = new XRiTextField();
    ES01 = new XRiTextField();
    ZPL01 = new XRiTextField();
    NUM2 = new RiZoneSortie();
    ENL102 = new XRiTextField();
    ENL202 = new XRiTextField();
    QTSX2 = new RiZoneSortie();
    QTEX2 = new XRiTextField();
    ES2 = new XRiTextField();
    ZPL02 = new XRiTextField();
    NUM3 = new RiZoneSortie();
    ENL103 = new XRiTextField();
    ENL203 = new XRiTextField();
    QTSX3 = new RiZoneSortie();
    QTEX3 = new XRiTextField();
    ES3 = new XRiTextField();
    ZPL03 = new XRiTextField();
    NUM4 = new RiZoneSortie();
    ENL104 = new XRiTextField();
    ENL204 = new XRiTextField();
    QTSX4 = new RiZoneSortie();
    QTEX4 = new XRiTextField();
    ES4 = new XRiTextField();
    ZPL04 = new XRiTextField();
    NUM5 = new RiZoneSortie();
    ENL105 = new XRiTextField();
    ENL205 = new XRiTextField();
    QTSX5 = new RiZoneSortie();
    QTEX5 = new XRiTextField();
    ES5 = new XRiTextField();
    ZPL05 = new XRiTextField();
    NUM6 = new RiZoneSortie();
    ENL106 = new XRiTextField();
    ENL206 = new XRiTextField();
    QTSX6 = new RiZoneSortie();
    QTEX6 = new XRiTextField();
    ES6 = new XRiTextField();
    ZPL06 = new XRiTextField();
    NUM7 = new RiZoneSortie();
    ENL107 = new XRiTextField();
    ENL207 = new XRiTextField();
    QTSX7 = new RiZoneSortie();
    QTEX7 = new XRiTextField();
    ES7 = new XRiTextField();
    ZPL07 = new XRiTextField();
    NUM8 = new RiZoneSortie();
    ENL108 = new XRiTextField();
    ENL208 = new XRiTextField();
    QTSX8 = new RiZoneSortie();
    QTEX8 = new XRiTextField();
    ES8 = new XRiTextField();
    ZPL08 = new XRiTextField();
    NUM9 = new RiZoneSortie();
    ENL109 = new XRiTextField();
    ENL209 = new XRiTextField();
    QTSX9 = new RiZoneSortie();
    QTEX9 = new XRiTextField();
    ES9 = new XRiTextField();
    ZPL09 = new XRiTextField();
    NUM10 = new RiZoneSortie();
    ENL110 = new XRiTextField();
    ENL210 = new XRiTextField();
    QTSX10 = new RiZoneSortie();
    QTEX10 = new XRiTextField();
    ES10 = new XRiTextField();
    ZPL10 = new XRiTextField();
    P21QPX = new XRiTextField();
    P21QTX = new XRiTextField();
    P21QTX_doublon_33 = new XRiTextField();
    label21 = new JLabel();
    label22 = new JLabel();
    label23 = new JLabel();
    label24 = new JLabel();
    barre_tete = new JMenuBar();
    lb_a_virer = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(915, 510));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Autre vue");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Detail conditionnement");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");

            //---- riMenu_bt3 ----
            riMenu_bt3.setText("Outils");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);

          //======== riSousMenu14 ========
          {
            riSousMenu14.setName("riSousMenu14");

            //---- riSousMenu_bt14 ----
            riSousMenu_bt14.setText("Bloc notes");
            riSousMenu_bt14.setName("riSousMenu_bt14");
            riSousMenu_bt14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt14ActionPerformed(e);
              }
            });
            riSousMenu14.add(riSousMenu_bt14);
          }
          menus_haut.add(riSousMenu14);

          //======== riSousMenu15 ========
          {
            riSousMenu15.setName("riSousMenu15");

            //---- riSousMenu_bt15 ----
            riSousMenu_bt15.setText("Fabrication");
            riSousMenu_bt15.setToolTipText("Fabrication");
            riSousMenu_bt15.setName("riSousMenu_bt15");
            riSousMenu_bt15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt15ActionPerformed(e);
              }
            });
            riSousMenu15.add(riSousMenu_bt15);
          }
          menus_haut.add(riSousMenu15);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_34 ----
          OBJ_34.setText("Conditionnement");
          OBJ_34.setName("OBJ_34");
          panel1.add(OBJ_34);
          OBJ_34.setBounds(20, 21, 115, 24);

          //---- A1CNDX ----
          A1CNDX.setHorizontalAlignment(SwingConstants.RIGHT);
          A1CNDX.setText("@A1CNDX@");
          A1CNDX.setName("A1CNDX");
          panel1.add(A1CNDX);
          A1CNDX.setBounds(145, 21, 84, 24);

          //---- OBJ_36 ----
          OBJ_36.setText("@A1UNL@");
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(235, 21, 34, 24);

          //---- OBJ_31 ----
          OBJ_31.setText("Disponible");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(435, 21, 86, 24);

          //---- WDISX ----
          WDISX.setText("@WDISX@");
          WDISX.setName("WDISX");
          panel1.add(WDISX);
          WDISX.setBounds(510, 21, 110, 24);

          //---- WDISUN ----
          WDISUN.setText("@WDISUN@");
          WDISUN.setName("WDISUN");
          panel1.add(WDISUN);
          WDISUN.setBounds(645, 21, 34, 24);

          //---- OBJ_50 ----
          OBJ_50.setText("@WLDALA@");
          OBJ_50.setName("OBJ_50");
          panel1.add(OBJ_50);
          OBJ_50.setBounds(20, 85, 680, 20);

          //---- OBJ_37 ----
          OBJ_37.setText("Plan palettisation");
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(20, 53, 123, 24);

          //---- OBJ_48 ----
          OBJ_48.setText("Longueur");
          OBJ_48.setName("OBJ_48");
          panel1.add(OBJ_48);
          OBJ_48.setBounds(560, 53, 70, 24);

          //---- OBJ_47 ----
          OBJ_47.setText("@A1QT2@");
          OBJ_47.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_47.setName("OBJ_47");
          panel1.add(OBJ_47);
          OBJ_47.setBounds(480, 53, 68, 24);

          //---- OBJ_49 ----
          OBJ_49.setText("@A1QT3@");
          OBJ_49.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_49.setName("OBJ_49");
          panel1.add(OBJ_49);
          OBJ_49.setBounds(627, 53, 52, 24);

          //---- WCLPL ----
          WCLPL.setText("@WCLPL@");
          WCLPL.setName("WCLPL");
          panel1.add(WCLPL);
          WCLPL.setBounds(320, 53, 52, WCLPL.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Laize");
          OBJ_46.setName("OBJ_46");
          panel1.add(OBJ_46);
          OBJ_46.setBounds(435, 53, 40, 24);

          //---- X1PPC ----
          X1PPC.setText("@X1PPC@");
          X1PPC.setName("X1PPC");
          panel1.add(X1PPC);
          X1PPC.setBounds(145, 53, 28, X1PPC.getPreferredSize().height);

          //---- X1PPL ----
          X1PPL.setText("@X1PPL@");
          X1PPL.setName("X1PPL");
          panel1.add(X1PPL);
          X1PPL.setBounds(200, 53, 28, X1PPL.getPreferredSize().height);

          //---- X1PPP ----
          X1PPP.setText("@X1PPP@");
          X1PPP.setName("X1PPP");
          panel1.add(X1PPP);
          X1PPP.setBounds(260, 53, 28, X1PPP.getPreferredSize().height);

          //---- OBJ_45 ----
          OBJ_45.setText("@A1UNL@");
          OBJ_45.setName("OBJ_45");
          panel1.add(OBJ_45);
          OBJ_45.setBounds(380, 53, 34, 24);

          //---- OBJ_43 ----
          OBJ_43.setText("=");
          OBJ_43.setName("OBJ_43");
          panel1.add(OBJ_43);
          OBJ_43.setBounds(300, 53, 10, 24);

          //---- OBJ_39 ----
          OBJ_39.setText("x");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(180, 53, 9, 24);

          //---- OBJ_41 ----
          OBJ_41.setText("x");
          OBJ_41.setName("OBJ_41");
          panel1.add(OBJ_41);
          OBJ_41.setBounds(240, 53, 9, 24);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 720, 125);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- NUM1 ----
          NUM1.setText("@NUM1@");
          NUM1.setName("NUM1");
          panel2.add(NUM1);
          NUM1.setBounds(40, 35, 40, 24);

          //---- ENL101 ----
          ENL101.setName("ENL101");
          panel2.add(ENL101);
          ENL101.setBounds(85, 33, 240, ENL101.getPreferredSize().height);

          //---- ENL201 ----
          ENL201.setName("ENL201");
          panel2.add(ENL201);
          ENL201.setBounds(325, 33, 34, ENL201.getPreferredSize().height);

          //---- QTSX1 ----
          QTSX1.setText("@QTSX1@");
          QTSX1.setName("QTSX1");
          panel2.add(QTSX1);
          QTSX1.setBounds(360, 35, 100, 24);

          //---- QTEX1 ----
          QTEX1.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX1.setName("QTEX1");
          panel2.add(QTEX1);
          QTEX1.setBounds(465, 33, 100, QTEX1.getPreferredSize().height);

          //---- ES01 ----
          ES01.setName("ES01");
          panel2.add(ES01);
          ES01.setBounds(565, 33, 24, ES01.getPreferredSize().height);

          //---- ZPL01 ----
          ZPL01.setHorizontalAlignment(SwingConstants.RIGHT);
          ZPL01.setName("ZPL01");
          panel2.add(ZPL01);
          ZPL01.setBounds(590, 33, 110, ZPL01.getPreferredSize().height);

          //---- NUM2 ----
          NUM2.setText("@NUM2@");
          NUM2.setName("NUM2");
          panel2.add(NUM2);
          NUM2.setBounds(40, 61, 40, 24);

          //---- ENL102 ----
          ENL102.setName("ENL102");
          panel2.add(ENL102);
          ENL102.setBounds(85, 59, 240, 28);

          //---- ENL202 ----
          ENL202.setName("ENL202");
          panel2.add(ENL202);
          ENL202.setBounds(325, 59, 34, 28);

          //---- QTSX2 ----
          QTSX2.setText("@QTSX2@");
          QTSX2.setName("QTSX2");
          panel2.add(QTSX2);
          QTSX2.setBounds(360, 61, 100, 24);

          //---- QTEX2 ----
          QTEX2.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX2.setName("QTEX2");
          panel2.add(QTEX2);
          QTEX2.setBounds(465, 59, 100, 28);

          //---- ES2 ----
          ES2.setName("ES2");
          panel2.add(ES2);
          ES2.setBounds(565, 59, 24, 28);

          //---- ZPL02 ----
          ZPL02.setHorizontalAlignment(SwingConstants.RIGHT);
          ZPL02.setName("ZPL02");
          panel2.add(ZPL02);
          ZPL02.setBounds(590, 59, 110, 28);

          //---- NUM3 ----
          NUM3.setText("@NUM3@");
          NUM3.setName("NUM3");
          panel2.add(NUM3);
          NUM3.setBounds(40, 87, 40, 24);

          //---- ENL103 ----
          ENL103.setName("ENL103");
          panel2.add(ENL103);
          ENL103.setBounds(85, 85, 240, 28);

          //---- ENL203 ----
          ENL203.setName("ENL203");
          panel2.add(ENL203);
          ENL203.setBounds(325, 85, 34, 28);

          //---- QTSX3 ----
          QTSX3.setText("@QTSX3@");
          QTSX3.setName("QTSX3");
          panel2.add(QTSX3);
          QTSX3.setBounds(360, 87, 100, 24);

          //---- QTEX3 ----
          QTEX3.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX3.setName("QTEX3");
          panel2.add(QTEX3);
          QTEX3.setBounds(465, 85, 100, 28);

          //---- ES3 ----
          ES3.setName("ES3");
          panel2.add(ES3);
          ES3.setBounds(565, 85, 24, 28);

          //---- ZPL03 ----
          ZPL03.setHorizontalAlignment(SwingConstants.RIGHT);
          ZPL03.setName("ZPL03");
          panel2.add(ZPL03);
          ZPL03.setBounds(590, 85, 110, 28);

          //---- NUM4 ----
          NUM4.setText("@NUM4@");
          NUM4.setName("NUM4");
          panel2.add(NUM4);
          NUM4.setBounds(40, 113, 40, 24);

          //---- ENL104 ----
          ENL104.setName("ENL104");
          panel2.add(ENL104);
          ENL104.setBounds(85, 111, 240, 28);

          //---- ENL204 ----
          ENL204.setName("ENL204");
          panel2.add(ENL204);
          ENL204.setBounds(325, 111, 34, 28);

          //---- QTSX4 ----
          QTSX4.setText("@QTSX4@");
          QTSX4.setName("QTSX4");
          panel2.add(QTSX4);
          QTSX4.setBounds(360, 113, 100, 24);

          //---- QTEX4 ----
          QTEX4.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX4.setName("QTEX4");
          panel2.add(QTEX4);
          QTEX4.setBounds(465, 111, 100, 28);

          //---- ES4 ----
          ES4.setName("ES4");
          panel2.add(ES4);
          ES4.setBounds(565, 111, 24, 28);

          //---- ZPL04 ----
          ZPL04.setHorizontalAlignment(SwingConstants.RIGHT);
          ZPL04.setName("ZPL04");
          panel2.add(ZPL04);
          ZPL04.setBounds(590, 111, 110, 28);

          //---- NUM5 ----
          NUM5.setText("@NUM5@");
          NUM5.setName("NUM5");
          panel2.add(NUM5);
          NUM5.setBounds(40, 139, 40, 24);

          //---- ENL105 ----
          ENL105.setName("ENL105");
          panel2.add(ENL105);
          ENL105.setBounds(85, 137, 240, 28);

          //---- ENL205 ----
          ENL205.setName("ENL205");
          panel2.add(ENL205);
          ENL205.setBounds(325, 137, 34, 28);

          //---- QTSX5 ----
          QTSX5.setText("@QTSX5@");
          QTSX5.setName("QTSX5");
          panel2.add(QTSX5);
          QTSX5.setBounds(360, 139, 100, 24);

          //---- QTEX5 ----
          QTEX5.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX5.setName("QTEX5");
          panel2.add(QTEX5);
          QTEX5.setBounds(465, 137, 100, 28);

          //---- ES5 ----
          ES5.setName("ES5");
          panel2.add(ES5);
          ES5.setBounds(565, 137, 24, 28);

          //---- ZPL05 ----
          ZPL05.setHorizontalAlignment(SwingConstants.RIGHT);
          ZPL05.setName("ZPL05");
          panel2.add(ZPL05);
          ZPL05.setBounds(590, 137, 110, 28);

          //---- NUM6 ----
          NUM6.setText("@NUM6@");
          NUM6.setName("NUM6");
          panel2.add(NUM6);
          NUM6.setBounds(40, 165, 40, 24);

          //---- ENL106 ----
          ENL106.setName("ENL106");
          panel2.add(ENL106);
          ENL106.setBounds(85, 163, 240, 28);

          //---- ENL206 ----
          ENL206.setName("ENL206");
          panel2.add(ENL206);
          ENL206.setBounds(325, 163, 34, 28);

          //---- QTSX6 ----
          QTSX6.setText("@QTSX6@");
          QTSX6.setName("QTSX6");
          panel2.add(QTSX6);
          QTSX6.setBounds(360, 165, 100, 24);

          //---- QTEX6 ----
          QTEX6.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX6.setName("QTEX6");
          panel2.add(QTEX6);
          QTEX6.setBounds(465, 163, 100, 28);

          //---- ES6 ----
          ES6.setName("ES6");
          panel2.add(ES6);
          ES6.setBounds(565, 163, 24, 28);

          //---- ZPL06 ----
          ZPL06.setHorizontalAlignment(SwingConstants.RIGHT);
          ZPL06.setName("ZPL06");
          panel2.add(ZPL06);
          ZPL06.setBounds(590, 163, 110, 28);

          //---- NUM7 ----
          NUM7.setText("@NUM7@");
          NUM7.setName("NUM7");
          panel2.add(NUM7);
          NUM7.setBounds(40, 191, 40, 24);

          //---- ENL107 ----
          ENL107.setName("ENL107");
          panel2.add(ENL107);
          ENL107.setBounds(85, 189, 240, 28);

          //---- ENL207 ----
          ENL207.setName("ENL207");
          panel2.add(ENL207);
          ENL207.setBounds(325, 189, 34, 28);

          //---- QTSX7 ----
          QTSX7.setText("@QTSX7@");
          QTSX7.setName("QTSX7");
          panel2.add(QTSX7);
          QTSX7.setBounds(360, 191, 100, 24);

          //---- QTEX7 ----
          QTEX7.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX7.setName("QTEX7");
          panel2.add(QTEX7);
          QTEX7.setBounds(465, 189, 100, 28);

          //---- ES7 ----
          ES7.setName("ES7");
          panel2.add(ES7);
          ES7.setBounds(565, 189, 24, 28);

          //---- ZPL07 ----
          ZPL07.setHorizontalAlignment(SwingConstants.RIGHT);
          ZPL07.setName("ZPL07");
          panel2.add(ZPL07);
          ZPL07.setBounds(590, 189, 110, 28);

          //---- NUM8 ----
          NUM8.setText("@NUM8@");
          NUM8.setName("NUM8");
          panel2.add(NUM8);
          NUM8.setBounds(40, 217, 40, 24);

          //---- ENL108 ----
          ENL108.setName("ENL108");
          panel2.add(ENL108);
          ENL108.setBounds(85, 215, 240, 28);

          //---- ENL208 ----
          ENL208.setName("ENL208");
          panel2.add(ENL208);
          ENL208.setBounds(325, 215, 34, 28);

          //---- QTSX8 ----
          QTSX8.setText("@QTSX8@");
          QTSX8.setName("QTSX8");
          panel2.add(QTSX8);
          QTSX8.setBounds(360, 217, 100, 24);

          //---- QTEX8 ----
          QTEX8.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX8.setName("QTEX8");
          panel2.add(QTEX8);
          QTEX8.setBounds(465, 215, 100, 28);

          //---- ES8 ----
          ES8.setName("ES8");
          panel2.add(ES8);
          ES8.setBounds(565, 215, 24, 28);

          //---- ZPL08 ----
          ZPL08.setHorizontalAlignment(SwingConstants.RIGHT);
          ZPL08.setName("ZPL08");
          panel2.add(ZPL08);
          ZPL08.setBounds(590, 215, 110, 28);

          //---- NUM9 ----
          NUM9.setText("@NUM9@");
          NUM9.setName("NUM9");
          panel2.add(NUM9);
          NUM9.setBounds(40, 243, 40, 24);

          //---- ENL109 ----
          ENL109.setName("ENL109");
          panel2.add(ENL109);
          ENL109.setBounds(85, 241, 240, 28);

          //---- ENL209 ----
          ENL209.setName("ENL209");
          panel2.add(ENL209);
          ENL209.setBounds(325, 241, 34, 28);

          //---- QTSX9 ----
          QTSX9.setText("@QTSX9@");
          QTSX9.setName("QTSX9");
          panel2.add(QTSX9);
          QTSX9.setBounds(360, 243, 100, 24);

          //---- QTEX9 ----
          QTEX9.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX9.setName("QTEX9");
          panel2.add(QTEX9);
          QTEX9.setBounds(465, 241, 100, 28);

          //---- ES9 ----
          ES9.setName("ES9");
          panel2.add(ES9);
          ES9.setBounds(565, 241, 24, 28);

          //---- ZPL09 ----
          ZPL09.setHorizontalAlignment(SwingConstants.RIGHT);
          ZPL09.setName("ZPL09");
          panel2.add(ZPL09);
          ZPL09.setBounds(590, 241, 110, 28);

          //---- NUM10 ----
          NUM10.setText("@NUM10@");
          NUM10.setName("NUM10");
          panel2.add(NUM10);
          NUM10.setBounds(40, 269, 40, 24);

          //---- ENL110 ----
          ENL110.setName("ENL110");
          panel2.add(ENL110);
          ENL110.setBounds(85, 267, 240, 28);

          //---- ENL210 ----
          ENL210.setName("ENL210");
          panel2.add(ENL210);
          ENL210.setBounds(325, 267, 34, 28);

          //---- QTSX10 ----
          QTSX10.setText("@QTSX10@");
          QTSX10.setName("QTSX10");
          panel2.add(QTSX10);
          QTSX10.setBounds(360, 269, 100, 24);

          //---- QTEX10 ----
          QTEX10.setHorizontalAlignment(SwingConstants.RIGHT);
          QTEX10.setName("QTEX10");
          panel2.add(QTEX10);
          QTEX10.setBounds(465, 267, 100, 28);

          //---- ES10 ----
          ES10.setName("ES10");
          panel2.add(ES10);
          ES10.setBounds(565, 267, 24, 28);

          //---- ZPL10 ----
          ZPL10.setHorizontalAlignment(SwingConstants.RIGHT);
          ZPL10.setName("ZPL10");
          panel2.add(ZPL10);
          ZPL10.setBounds(590, 267, 110, 28);

          //---- P21QPX ----
          P21QPX.setHorizontalAlignment(SwingConstants.RIGHT);
          P21QPX.setName("P21QPX");
          panel2.add(P21QPX);
          P21QPX.setBounds(465, 295, 100, P21QPX.getPreferredSize().height);

          //---- P21QTX ----
          P21QTX.setName("P21QTX");
          panel2.add(P21QTX);
          P21QTX.setBounds(565, 295, 24, P21QTX.getPreferredSize().height);

          //---- P21QTX_doublon_33 ----
          P21QTX_doublon_33.setHorizontalAlignment(SwingConstants.RIGHT);
          P21QTX_doublon_33.setName("P21QTX_doublon_33");
          panel2.add(P21QTX_doublon_33);
          P21QTX_doublon_33.setBounds(590, 295, 110, P21QTX_doublon_33.getPreferredSize().height);

          //---- label21 ----
          label21.setText("Num\u00e9ro de lot");
          label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD));
          label21.setName("label21");
          panel2.add(label21);
          label21.setBounds(85, 15, 240, 20);

          //---- label22 ----
          label22.setText("Unit\u00e9");
          label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD));
          label22.setName("label22");
          panel2.add(label22);
          label22.setBounds(360, 15, 100, 20);

          //---- label23 ----
          label23.setText("Quantit\u00e9");
          label23.setFont(label23.getFont().deriveFont(label23.getFont().getStyle() | Font.BOLD));
          label23.setName("label23");
          panel2.add(label23);
          label23.setBounds(465, 15, 100, 20);

          //---- label24 ----
          label24.setText("Suppl. lot");
          label24.setFont(label24.getFont().deriveFont(label24.getFont().getStyle() | Font.BOLD));
          label24.setName("label24");
          panel2.add(label24);
          label24.setBounds(590, 15, 110, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 140, 720, 330);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //---- lb_a_virer ----
      lb_a_virer.setText("Saisie de num\u00e9ros de lots pour l'article @P21ART@ @A1LIB@");
      lb_a_virer.setForeground(SystemColor.windowText);
      lb_a_virer.setName("lb_a_virer");
      barre_tete.add(lb_a_virer);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("D\u00e9coupe");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Annulation de d\u00e9coupe");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
      BTD.addSeparator();

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_34;
  private RiZoneSortie A1CNDX;
  private RiZoneSortie OBJ_36;
  private JLabel OBJ_31;
  private RiZoneSortie WDISX;
  private RiZoneSortie WDISUN;
  private JLabel OBJ_50;
  private JLabel OBJ_37;
  private JLabel OBJ_48;
  private RiZoneSortie OBJ_47;
  private RiZoneSortie OBJ_49;
  private RiZoneSortie WCLPL;
  private JLabel OBJ_46;
  private RiZoneSortie X1PPC;
  private RiZoneSortie X1PPL;
  private RiZoneSortie X1PPP;
  private RiZoneSortie OBJ_45;
  private JLabel OBJ_43;
  private JLabel OBJ_39;
  private JLabel OBJ_41;
  private JPanel panel2;
  private RiZoneSortie NUM1;
  private XRiTextField ENL101;
  private XRiTextField ENL201;
  private RiZoneSortie QTSX1;
  private XRiTextField QTEX1;
  private XRiTextField ES01;
  private XRiTextField ZPL01;
  private RiZoneSortie NUM2;
  private XRiTextField ENL102;
  private XRiTextField ENL202;
  private RiZoneSortie QTSX2;
  private XRiTextField QTEX2;
  private XRiTextField ES2;
  private XRiTextField ZPL02;
  private RiZoneSortie NUM3;
  private XRiTextField ENL103;
  private XRiTextField ENL203;
  private RiZoneSortie QTSX3;
  private XRiTextField QTEX3;
  private XRiTextField ES3;
  private XRiTextField ZPL03;
  private RiZoneSortie NUM4;
  private XRiTextField ENL104;
  private XRiTextField ENL204;
  private RiZoneSortie QTSX4;
  private XRiTextField QTEX4;
  private XRiTextField ES4;
  private XRiTextField ZPL04;
  private RiZoneSortie NUM5;
  private XRiTextField ENL105;
  private XRiTextField ENL205;
  private RiZoneSortie QTSX5;
  private XRiTextField QTEX5;
  private XRiTextField ES5;
  private XRiTextField ZPL05;
  private RiZoneSortie NUM6;
  private XRiTextField ENL106;
  private XRiTextField ENL206;
  private RiZoneSortie QTSX6;
  private XRiTextField QTEX6;
  private XRiTextField ES6;
  private XRiTextField ZPL06;
  private RiZoneSortie NUM7;
  private XRiTextField ENL107;
  private XRiTextField ENL207;
  private RiZoneSortie QTSX7;
  private XRiTextField QTEX7;
  private XRiTextField ES7;
  private XRiTextField ZPL07;
  private RiZoneSortie NUM8;
  private XRiTextField ENL108;
  private XRiTextField ENL208;
  private RiZoneSortie QTSX8;
  private XRiTextField QTEX8;
  private XRiTextField ES8;
  private XRiTextField ZPL08;
  private RiZoneSortie NUM9;
  private XRiTextField ENL109;
  private XRiTextField ENL209;
  private RiZoneSortie QTSX9;
  private XRiTextField QTEX9;
  private XRiTextField ES9;
  private XRiTextField ZPL09;
  private RiZoneSortie NUM10;
  private XRiTextField ENL110;
  private XRiTextField ENL210;
  private RiZoneSortie QTSX10;
  private XRiTextField QTEX10;
  private XRiTextField ES10;
  private XRiTextField ZPL10;
  private XRiTextField P21QPX;
  private XRiTextField P21QTX;
  private XRiTextField P21QTX_doublon_33;
  private JLabel label21;
  private JLabel label22;
  private JLabel label23;
  private JLabel label24;
  private JMenuBar barre_tete;
  private JLabel lb_a_virer;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
