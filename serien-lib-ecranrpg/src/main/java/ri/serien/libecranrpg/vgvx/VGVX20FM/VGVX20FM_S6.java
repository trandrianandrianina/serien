
package ri.serien.libecranrpg.vgvx.VGVX20FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX20FM_S6 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX20FM_S6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    setModal(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    NUM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM1@")).trim());
    NUM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM2@")).trim());
    NUM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM3@")).trim());
    NUM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM4@")).trim());
    NUM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM5@")).trim());
    NUM6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM6@")).trim());
    NUM7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM7@")).trim());
    NUM8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM8@")).trim());
    NUM9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM9@")).trim());
    NUM10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM10@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    A1ARTR.setVisible(!lexique.isTrue("96"));
    A1ART.setVisible(lexique.isTrue("96"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Numéros de série"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel3 = new JPanel();
    OBJ_17 = new JLabel();
    A1ART = new XRiTextField();
    A1ARTR = new XRiTextField();
    NUM1 = new JLabel();
    NUM2 = new JLabel();
    NUM3 = new JLabel();
    NUM4 = new JLabel();
    NUM5 = new JLabel();
    NUM6 = new JLabel();
    NUM7 = new JLabel();
    NUM8 = new JLabel();
    NUM9 = new JLabel();
    NUM10 = new JLabel();
    ENX1 = new XRiTextField();
    ENX2 = new XRiTextField();
    ENX3 = new XRiTextField();
    ENX4 = new XRiTextField();
    ENX5 = new XRiTextField();
    ENX6 = new XRiTextField();
    ENX7 = new XRiTextField();
    ENX8 = new XRiTextField();
    ENX9 = new XRiTextField();
    ENX10 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(740, 370));
    setPreferredSize(new Dimension(740, 370));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setForeground(Color.black);
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- BT_PGUP ----
          BT_PGUP.setToolTipText("?S6.BT_PGUP.toolTipText_2?");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel2.add(BT_PGUP);
          BT_PGUP.setBounds(490, 65, 25, 115);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setToolTipText("?S6.BT_PGDOWN.toolTipText_2?");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel2.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(490, 203, 25, 115);

          //======== panel3 ========
          {
            panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_17 ----
            OBJ_17.setText("Saisie d'un num\u00e9ro de s\u00e9rie pour l'article");
            OBJ_17.setName("OBJ_17");
            panel3.add(OBJ_17);
            OBJ_17.setBounds(15, 14, 226, 20);

            //---- A1ART ----
            A1ART.setName("A1ART");
            panel3.add(A1ART);
            A1ART.setBounds(250, 10, 210, A1ART.getPreferredSize().height);

            //---- A1ARTR ----
            A1ARTR.setName("A1ARTR");
            panel3.add(A1ARTR);
            A1ARTR.setBounds(255, 10, 110, A1ARTR.getPreferredSize().height);
          }
          panel2.add(panel3);
          panel3.setBounds(20, 10, 495, 45);

          //---- NUM1 ----
          NUM1.setText("@NUM1@");
          NUM1.setName("NUM1");
          panel2.add(NUM1);
          NUM1.setBounds(35, 67, 55, 25);

          //---- NUM2 ----
          NUM2.setText("@NUM2@");
          NUM2.setName("NUM2");
          panel2.add(NUM2);
          NUM2.setBounds(35, 92, 55, 25);

          //---- NUM3 ----
          NUM3.setText("@NUM3@");
          NUM3.setName("NUM3");
          panel2.add(NUM3);
          NUM3.setBounds(35, 117, 55, 25);

          //---- NUM4 ----
          NUM4.setText("@NUM4@");
          NUM4.setName("NUM4");
          panel2.add(NUM4);
          NUM4.setBounds(35, 142, 55, 25);

          //---- NUM5 ----
          NUM5.setText("@NUM5@");
          NUM5.setName("NUM5");
          panel2.add(NUM5);
          NUM5.setBounds(35, 167, 55, 25);

          //---- NUM6 ----
          NUM6.setText("@NUM6@");
          NUM6.setName("NUM6");
          panel2.add(NUM6);
          NUM6.setBounds(35, 192, 55, 25);

          //---- NUM7 ----
          NUM7.setText("@NUM7@");
          NUM7.setName("NUM7");
          panel2.add(NUM7);
          NUM7.setBounds(35, 217, 55, 25);

          //---- NUM8 ----
          NUM8.setText("@NUM8@");
          NUM8.setName("NUM8");
          panel2.add(NUM8);
          NUM8.setBounds(35, 242, 55, 25);

          //---- NUM9 ----
          NUM9.setText("@NUM9@");
          NUM9.setName("NUM9");
          panel2.add(NUM9);
          NUM9.setBounds(35, 267, 55, 25);

          //---- NUM10 ----
          NUM10.setText("@NUM10@");
          NUM10.setName("NUM10");
          panel2.add(NUM10);
          NUM10.setBounds(35, 292, 55, 25);

          //---- ENX1 ----
          ENX1.setName("ENX1");
          panel2.add(ENX1);
          ENX1.setBounds(100, 65, 380, ENX1.getPreferredSize().height);

          //---- ENX2 ----
          ENX2.setName("ENX2");
          panel2.add(ENX2);
          ENX2.setBounds(100, 90, 380, ENX2.getPreferredSize().height);

          //---- ENX3 ----
          ENX3.setName("ENX3");
          panel2.add(ENX3);
          ENX3.setBounds(100, 115, 380, ENX3.getPreferredSize().height);

          //---- ENX4 ----
          ENX4.setName("ENX4");
          panel2.add(ENX4);
          ENX4.setBounds(100, 140, 380, ENX4.getPreferredSize().height);

          //---- ENX5 ----
          ENX5.setName("ENX5");
          panel2.add(ENX5);
          ENX5.setBounds(100, 165, 380, ENX5.getPreferredSize().height);

          //---- ENX6 ----
          ENX6.setName("ENX6");
          panel2.add(ENX6);
          ENX6.setBounds(100, 190, 380, ENX6.getPreferredSize().height);

          //---- ENX7 ----
          ENX7.setName("ENX7");
          panel2.add(ENX7);
          ENX7.setBounds(100, 215, 380, ENX7.getPreferredSize().height);

          //---- ENX8 ----
          ENX8.setName("ENX8");
          panel2.add(ENX8);
          ENX8.setBounds(100, 240, 380, ENX8.getPreferredSize().height);

          //---- ENX9 ----
          ENX9.setName("ENX9");
          panel2.add(ENX9);
          ENX9.setBounds(100, 265, 380, ENX9.getPreferredSize().height);

          //---- ENX10 ----
          ENX10.setName("ENX10");
          panel2.add(ENX10);
          ENX10.setBounds(100, 290, 380, ENX10.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel3;
  private JLabel OBJ_17;
  private XRiTextField A1ART;
  private XRiTextField A1ARTR;
  private JLabel NUM1;
  private JLabel NUM2;
  private JLabel NUM3;
  private JLabel NUM4;
  private JLabel NUM5;
  private JLabel NUM6;
  private JLabel NUM7;
  private JLabel NUM8;
  private JLabel NUM9;
  private JLabel NUM10;
  private XRiTextField ENX1;
  private XRiTextField ENX2;
  private XRiTextField ENX3;
  private XRiTextField ENX4;
  private XRiTextField ENX5;
  private XRiTextField ENX6;
  private XRiTextField ENX7;
  private XRiTextField ENX8;
  private XRiTextField ENX9;
  private XRiTextField ENX10;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables


}
