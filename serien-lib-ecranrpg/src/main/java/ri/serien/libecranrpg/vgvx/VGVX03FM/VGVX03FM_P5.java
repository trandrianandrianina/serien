
package ri.serien.libecranrpg.vgvx.VGVX03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX03FM_P5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX03FM_P5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("Type : @PRLIB@")).trim()));
    PX3PDA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PX3PDA@")).trim());
    PR01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR01@")).trim());
    MT01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT01R@")).trim());
    MT02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT02R@")).trim());
    PR02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR02@")).trim());
    MT03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT03R@")).trim());
    PR03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR03@")).trim());
    MT04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT04R@")).trim());
    PR04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR04@")).trim());
    MT05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT05R@")).trim());
    PR05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR05@")).trim());
    MT06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT06R@")).trim());
    PR06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR06@")).trim());
    MT07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT07R@")).trim());
    PR07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR07@")).trim());
    MT08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT08R@")).trim());
    PR08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR08@")).trim());
    MT09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT09R@")).trim());
    PR09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR09@")).trim());
    MT10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT10R@")).trim());
    PR10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PR10@")).trim());
    MTTOT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTTOT@")).trim());
    PRTOT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PRTOT@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Exprimés en @UNLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Mode de calcul du prix de revient"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    EF01 = new XRiTextField();
    EF02 = new XRiTextField();
    EF03 = new XRiTextField();
    EF04 = new XRiTextField();
    EF05 = new XRiTextField();
    EF06 = new XRiTextField();
    EF07 = new XRiTextField();
    EF08 = new XRiTextField();
    EF09 = new XRiTextField();
    EF10 = new XRiTextField();
    label1 = new JLabel();
    PX3PDA = new RiZoneSortie();
    LI01 = new XRiTextField();
    WBAS1 = new RiZoneSortie();
    BA01 = new XRiTextField();
    PSK01 = new XRiTextField();
    PSV01 = new XRiTextField();
    TY01 = new XRiTextField();
    PR01 = new RiZoneSortie();
    MT01 = new RiZoneSortie();
    LI02 = new XRiTextField();
    WBAS2 = new RiZoneSortie();
    BA02 = new XRiTextField();
    PSK02 = new XRiTextField();
    PSV02 = new XRiTextField();
    TY02 = new XRiTextField();
    MT02 = new RiZoneSortie();
    PR02 = new RiZoneSortie();
    LI03 = new XRiTextField();
    WBAS3 = new RiZoneSortie();
    BA03 = new XRiTextField();
    PSK03 = new XRiTextField();
    PSV03 = new XRiTextField();
    TY03 = new XRiTextField();
    MT03 = new RiZoneSortie();
    PR03 = new RiZoneSortie();
    LI04 = new XRiTextField();
    WBAS4 = new RiZoneSortie();
    BA04 = new XRiTextField();
    PSK04 = new XRiTextField();
    PSV04 = new XRiTextField();
    TY04 = new XRiTextField();
    MT04 = new RiZoneSortie();
    PR04 = new RiZoneSortie();
    LI05 = new XRiTextField();
    WBAS5 = new RiZoneSortie();
    BA05 = new XRiTextField();
    PSK05 = new XRiTextField();
    PSV05 = new XRiTextField();
    TY05 = new XRiTextField();
    MT05 = new RiZoneSortie();
    PR05 = new RiZoneSortie();
    LI06 = new XRiTextField();
    WBAS6 = new RiZoneSortie();
    BA06 = new XRiTextField();
    PSK06 = new XRiTextField();
    PSV06 = new XRiTextField();
    TY06 = new XRiTextField();
    MT06 = new RiZoneSortie();
    PR06 = new RiZoneSortie();
    LI07 = new XRiTextField();
    WBAS7 = new RiZoneSortie();
    BA07 = new XRiTextField();
    PSK07 = new XRiTextField();
    PSV07 = new XRiTextField();
    TY07 = new XRiTextField();
    MT07 = new RiZoneSortie();
    PR07 = new RiZoneSortie();
    LI08 = new XRiTextField();
    WBAS8 = new RiZoneSortie();
    BA08 = new XRiTextField();
    PSK08 = new XRiTextField();
    PSV08 = new XRiTextField();
    TY08 = new XRiTextField();
    MT08 = new RiZoneSortie();
    PR08 = new RiZoneSortie();
    LI9 = new XRiTextField();
    WBAS9 = new RiZoneSortie();
    B0A9 = new XRiTextField();
    PSK09 = new XRiTextField();
    PSV09 = new XRiTextField();
    TY09 = new XRiTextField();
    MT09 = new RiZoneSortie();
    PR09 = new RiZoneSortie();
    LI10 = new XRiTextField();
    WBAS10 = new RiZoneSortie();
    BA10 = new XRiTextField();
    PSK10 = new XRiTextField();
    PSV10 = new XRiTextField();
    TY10 = new XRiTextField();
    MT10 = new RiZoneSortie();
    PR10 = new RiZoneSortie();
    MTTOT = new RiZoneSortie();
    PRTOT = new RiZoneSortie();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label2 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(900, 450));
    setPreferredSize(new Dimension(900, 450));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Type : @PRLIB@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- EF01 ----
          EF01.setName("EF01");
          panel1.add(EF01);
          EF01.setBounds(30, 95, 25, EF01.getPreferredSize().height);

          //---- EF02 ----
          EF02.setName("EF02");
          panel1.add(EF02);
          EF02.setBounds(30, 120, 25, EF02.getPreferredSize().height);

          //---- EF03 ----
          EF03.setName("EF03");
          panel1.add(EF03);
          EF03.setBounds(30, 145, 25, EF03.getPreferredSize().height);

          //---- EF04 ----
          EF04.setName("EF04");
          panel1.add(EF04);
          EF04.setBounds(30, 170, 25, EF04.getPreferredSize().height);

          //---- EF05 ----
          EF05.setName("EF05");
          panel1.add(EF05);
          EF05.setBounds(30, 195, 25, EF05.getPreferredSize().height);

          //---- EF06 ----
          EF06.setName("EF06");
          panel1.add(EF06);
          EF06.setBounds(30, 220, 25, EF06.getPreferredSize().height);

          //---- EF07 ----
          EF07.setName("EF07");
          panel1.add(EF07);
          EF07.setBounds(30, 245, 25, EF07.getPreferredSize().height);

          //---- EF08 ----
          EF08.setName("EF08");
          panel1.add(EF08);
          EF08.setBounds(30, 270, 25, EF08.getPreferredSize().height);

          //---- EF09 ----
          EF09.setName("EF09");
          panel1.add(EF09);
          EF09.setBounds(30, 295, 25, EF09.getPreferredSize().height);

          //---- EF10 ----
          EF10.setName("EF10");
          panel1.add(EF10);
          EF10.setBounds(30, 320, 25, EF10.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Prix d'achat");
          label1.setHorizontalAlignment(SwingConstants.RIGHT);
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(505, 25, 125, 30);

          //---- PX3PDA ----
          PX3PDA.setText("@PX3PDA@");
          PX3PDA.setHorizontalAlignment(SwingConstants.RIGHT);
          PX3PDA.setName("PX3PDA");
          panel1.add(PX3PDA);
          PX3PDA.setBounds(640, 25, 120, PX3PDA.getPreferredSize().height);

          //---- LI01 ----
          LI01.setName("LI01");
          panel1.add(LI01);
          LI01.setBounds(60, 95, 110, LI01.getPreferredSize().height);

          //---- WBAS1 ----
          WBAS1.setText("@WBAS1");
          WBAS1.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS1.setName("WBAS1");
          panel1.add(WBAS1);
          WBAS1.setBounds(170, 95, 110, WBAS1.getPreferredSize().height);

          //---- BA01 ----
          BA01.setName("BA01");
          panel1.add(BA01);
          BA01.setBounds(285, 95, 25, BA01.getPreferredSize().height);

          //---- PSK01 ----
          PSK01.setName("PSK01");
          panel1.add(PSK01);
          PSK01.setBounds(310, 95, 70, PSK01.getPreferredSize().height);

          //---- PSV01 ----
          PSV01.setName("PSV01");
          panel1.add(PSV01);
          PSV01.setBounds(385, 95, 90, PSV01.getPreferredSize().height);

          //---- TY01 ----
          TY01.setName("TY01");
          panel1.add(TY01);
          TY01.setBounds(480, 95, 25, TY01.getPreferredSize().height);

          //---- PR01 ----
          PR01.setText("@PR01@");
          PR01.setHorizontalAlignment(SwingConstants.RIGHT);
          PR01.setName("PR01");
          panel1.add(PR01);
          PR01.setBounds(650, 95, 110, PR01.getPreferredSize().height);

          //---- MT01 ----
          MT01.setText("@MT01R@");
          MT01.setHorizontalAlignment(SwingConstants.RIGHT);
          MT01.setName("MT01");
          panel1.add(MT01);
          MT01.setBounds(505, 95, 140, MT01.getPreferredSize().height);

          //---- LI02 ----
          LI02.setName("LI02");
          panel1.add(LI02);
          LI02.setBounds(60, 120, 110, 28);

          //---- WBAS2 ----
          WBAS2.setText("@WBAS2");
          WBAS2.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS2.setName("WBAS2");
          panel1.add(WBAS2);
          WBAS2.setBounds(170, 120, 110, WBAS2.getPreferredSize().height);

          //---- BA02 ----
          BA02.setName("BA02");
          panel1.add(BA02);
          BA02.setBounds(285, 120, 25, BA02.getPreferredSize().height);

          //---- PSK02 ----
          PSK02.setName("PSK02");
          panel1.add(PSK02);
          PSK02.setBounds(310, 120, 70, PSK02.getPreferredSize().height);

          //---- PSV02 ----
          PSV02.setName("PSV02");
          panel1.add(PSV02);
          PSV02.setBounds(385, 120, 90, PSV02.getPreferredSize().height);

          //---- TY02 ----
          TY02.setName("TY02");
          panel1.add(TY02);
          TY02.setBounds(480, 120, 25, TY02.getPreferredSize().height);

          //---- MT02 ----
          MT02.setText("@MT02R@");
          MT02.setHorizontalAlignment(SwingConstants.RIGHT);
          MT02.setName("MT02");
          panel1.add(MT02);
          MT02.setBounds(505, 120, 140, MT02.getPreferredSize().height);

          //---- PR02 ----
          PR02.setText("@PR02@");
          PR02.setHorizontalAlignment(SwingConstants.RIGHT);
          PR02.setName("PR02");
          panel1.add(PR02);
          PR02.setBounds(650, 120, 110, PR02.getPreferredSize().height);

          //---- LI03 ----
          LI03.setName("LI03");
          panel1.add(LI03);
          LI03.setBounds(60, 145, 110, 28);

          //---- WBAS3 ----
          WBAS3.setText("@WBAS3");
          WBAS3.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS3.setName("WBAS3");
          panel1.add(WBAS3);
          WBAS3.setBounds(170, 145, 110, 24);

          //---- BA03 ----
          BA03.setName("BA03");
          panel1.add(BA03);
          BA03.setBounds(285, 145, 25, 28);

          //---- PSK03 ----
          PSK03.setName("PSK03");
          panel1.add(PSK03);
          PSK03.setBounds(310, 145, 70, 28);

          //---- PSV03 ----
          PSV03.setName("PSV03");
          panel1.add(PSV03);
          PSV03.setBounds(385, 145, 90, 28);

          //---- TY03 ----
          TY03.setName("TY03");
          panel1.add(TY03);
          TY03.setBounds(480, 145, 25, 28);

          //---- MT03 ----
          MT03.setText("@MT03R@");
          MT03.setHorizontalAlignment(SwingConstants.RIGHT);
          MT03.setName("MT03");
          panel1.add(MT03);
          MT03.setBounds(505, 145, 140, 24);

          //---- PR03 ----
          PR03.setText("@PR03@");
          PR03.setHorizontalAlignment(SwingConstants.RIGHT);
          PR03.setName("PR03");
          panel1.add(PR03);
          PR03.setBounds(650, 145, 110, 24);

          //---- LI04 ----
          LI04.setName("LI04");
          panel1.add(LI04);
          LI04.setBounds(60, 170, 110, 28);

          //---- WBAS4 ----
          WBAS4.setText("@WBAS4");
          WBAS4.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS4.setName("WBAS4");
          panel1.add(WBAS4);
          WBAS4.setBounds(170, 170, 110, WBAS4.getPreferredSize().height);

          //---- BA04 ----
          BA04.setName("BA04");
          panel1.add(BA04);
          BA04.setBounds(285, 170, 25, BA04.getPreferredSize().height);

          //---- PSK04 ----
          PSK04.setName("PSK04");
          panel1.add(PSK04);
          PSK04.setBounds(310, 170, 70, PSK04.getPreferredSize().height);

          //---- PSV04 ----
          PSV04.setName("PSV04");
          panel1.add(PSV04);
          PSV04.setBounds(385, 170, 90, PSV04.getPreferredSize().height);

          //---- TY04 ----
          TY04.setName("TY04");
          panel1.add(TY04);
          TY04.setBounds(480, 170, 25, TY04.getPreferredSize().height);

          //---- MT04 ----
          MT04.setText("@MT04R@");
          MT04.setHorizontalAlignment(SwingConstants.RIGHT);
          MT04.setName("MT04");
          panel1.add(MT04);
          MT04.setBounds(505, 170, 140, MT04.getPreferredSize().height);

          //---- PR04 ----
          PR04.setText("@PR04@");
          PR04.setHorizontalAlignment(SwingConstants.RIGHT);
          PR04.setName("PR04");
          panel1.add(PR04);
          PR04.setBounds(650, 170, 110, PR04.getPreferredSize().height);

          //---- LI05 ----
          LI05.setName("LI05");
          panel1.add(LI05);
          LI05.setBounds(60, 195, 110, LI05.getPreferredSize().height);

          //---- WBAS5 ----
          WBAS5.setText("@WBAS5");
          WBAS5.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS5.setName("WBAS5");
          panel1.add(WBAS5);
          WBAS5.setBounds(170, 195, 110, WBAS5.getPreferredSize().height);

          //---- BA05 ----
          BA05.setName("BA05");
          panel1.add(BA05);
          BA05.setBounds(285, 195, 25, BA05.getPreferredSize().height);

          //---- PSK05 ----
          PSK05.setName("PSK05");
          panel1.add(PSK05);
          PSK05.setBounds(310, 195, 70, PSK05.getPreferredSize().height);

          //---- PSV05 ----
          PSV05.setName("PSV05");
          panel1.add(PSV05);
          PSV05.setBounds(385, 195, 90, PSV05.getPreferredSize().height);

          //---- TY05 ----
          TY05.setName("TY05");
          panel1.add(TY05);
          TY05.setBounds(480, 195, 25, TY05.getPreferredSize().height);

          //---- MT05 ----
          MT05.setText("@MT05R@");
          MT05.setHorizontalAlignment(SwingConstants.RIGHT);
          MT05.setName("MT05");
          panel1.add(MT05);
          MT05.setBounds(505, 195, 140, MT05.getPreferredSize().height);

          //---- PR05 ----
          PR05.setText("@PR05@");
          PR05.setHorizontalAlignment(SwingConstants.RIGHT);
          PR05.setName("PR05");
          panel1.add(PR05);
          PR05.setBounds(650, 195, 110, PR05.getPreferredSize().height);

          //---- LI06 ----
          LI06.setName("LI06");
          panel1.add(LI06);
          LI06.setBounds(60, 220, 110, LI06.getPreferredSize().height);

          //---- WBAS6 ----
          WBAS6.setText("@WBAS6");
          WBAS6.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS6.setName("WBAS6");
          panel1.add(WBAS6);
          WBAS6.setBounds(170, 220, 110, WBAS6.getPreferredSize().height);

          //---- BA06 ----
          BA06.setName("BA06");
          panel1.add(BA06);
          BA06.setBounds(285, 220, 25, BA06.getPreferredSize().height);

          //---- PSK06 ----
          PSK06.setName("PSK06");
          panel1.add(PSK06);
          PSK06.setBounds(310, 220, 70, PSK06.getPreferredSize().height);

          //---- PSV06 ----
          PSV06.setName("PSV06");
          panel1.add(PSV06);
          PSV06.setBounds(385, 220, 90, PSV06.getPreferredSize().height);

          //---- TY06 ----
          TY06.setName("TY06");
          panel1.add(TY06);
          TY06.setBounds(480, 220, 25, TY06.getPreferredSize().height);

          //---- MT06 ----
          MT06.setText("@MT06R@");
          MT06.setHorizontalAlignment(SwingConstants.RIGHT);
          MT06.setName("MT06");
          panel1.add(MT06);
          MT06.setBounds(505, 220, 140, MT06.getPreferredSize().height);

          //---- PR06 ----
          PR06.setText("@PR06@");
          PR06.setHorizontalAlignment(SwingConstants.RIGHT);
          PR06.setName("PR06");
          panel1.add(PR06);
          PR06.setBounds(650, 220, 110, PR06.getPreferredSize().height);

          //---- LI07 ----
          LI07.setName("LI07");
          panel1.add(LI07);
          LI07.setBounds(60, 245, 110, LI07.getPreferredSize().height);

          //---- WBAS7 ----
          WBAS7.setText("@WBAS7");
          WBAS7.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS7.setName("WBAS7");
          panel1.add(WBAS7);
          WBAS7.setBounds(170, 245, 110, WBAS7.getPreferredSize().height);

          //---- BA07 ----
          BA07.setName("BA07");
          panel1.add(BA07);
          BA07.setBounds(285, 245, 25, BA07.getPreferredSize().height);

          //---- PSK07 ----
          PSK07.setName("PSK07");
          panel1.add(PSK07);
          PSK07.setBounds(310, 245, 70, PSK07.getPreferredSize().height);

          //---- PSV07 ----
          PSV07.setName("PSV07");
          panel1.add(PSV07);
          PSV07.setBounds(385, 245, 90, PSV07.getPreferredSize().height);

          //---- TY07 ----
          TY07.setName("TY07");
          panel1.add(TY07);
          TY07.setBounds(480, 245, 25, TY07.getPreferredSize().height);

          //---- MT07 ----
          MT07.setText("@MT07R@");
          MT07.setHorizontalAlignment(SwingConstants.RIGHT);
          MT07.setName("MT07");
          panel1.add(MT07);
          MT07.setBounds(505, 245, 140, MT07.getPreferredSize().height);

          //---- PR07 ----
          PR07.setText("@PR07@");
          PR07.setHorizontalAlignment(SwingConstants.RIGHT);
          PR07.setName("PR07");
          panel1.add(PR07);
          PR07.setBounds(650, 245, 110, PR07.getPreferredSize().height);

          //---- LI08 ----
          LI08.setName("LI08");
          panel1.add(LI08);
          LI08.setBounds(60, 270, 110, LI08.getPreferredSize().height);

          //---- WBAS8 ----
          WBAS8.setText("@WBAS8");
          WBAS8.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS8.setName("WBAS8");
          panel1.add(WBAS8);
          WBAS8.setBounds(170, 270, 110, WBAS8.getPreferredSize().height);

          //---- BA08 ----
          BA08.setName("BA08");
          panel1.add(BA08);
          BA08.setBounds(285, 270, 25, BA08.getPreferredSize().height);

          //---- PSK08 ----
          PSK08.setName("PSK08");
          panel1.add(PSK08);
          PSK08.setBounds(310, 270, 70, PSK08.getPreferredSize().height);

          //---- PSV08 ----
          PSV08.setName("PSV08");
          panel1.add(PSV08);
          PSV08.setBounds(385, 270, 90, PSV08.getPreferredSize().height);

          //---- TY08 ----
          TY08.setName("TY08");
          panel1.add(TY08);
          TY08.setBounds(480, 270, 25, TY08.getPreferredSize().height);

          //---- MT08 ----
          MT08.setText("@MT08R@");
          MT08.setHorizontalAlignment(SwingConstants.RIGHT);
          MT08.setName("MT08");
          panel1.add(MT08);
          MT08.setBounds(505, 270, 140, MT08.getPreferredSize().height);

          //---- PR08 ----
          PR08.setText("@PR08@");
          PR08.setHorizontalAlignment(SwingConstants.RIGHT);
          PR08.setName("PR08");
          panel1.add(PR08);
          PR08.setBounds(650, 270, 110, PR08.getPreferredSize().height);

          //---- LI9 ----
          LI9.setName("LI9");
          panel1.add(LI9);
          LI9.setBounds(60, 295, 110, LI9.getPreferredSize().height);

          //---- WBAS9 ----
          WBAS9.setText("@WBAS9");
          WBAS9.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS9.setName("WBAS9");
          panel1.add(WBAS9);
          WBAS9.setBounds(170, 295, 110, WBAS9.getPreferredSize().height);

          //---- B0A9 ----
          B0A9.setName("B0A9");
          panel1.add(B0A9);
          B0A9.setBounds(285, 295, 25, B0A9.getPreferredSize().height);

          //---- PSK09 ----
          PSK09.setName("PSK09");
          panel1.add(PSK09);
          PSK09.setBounds(310, 295, 70, PSK09.getPreferredSize().height);

          //---- PSV09 ----
          PSV09.setName("PSV09");
          panel1.add(PSV09);
          PSV09.setBounds(385, 295, 90, PSV09.getPreferredSize().height);

          //---- TY09 ----
          TY09.setName("TY09");
          panel1.add(TY09);
          TY09.setBounds(480, 295, 25, TY09.getPreferredSize().height);

          //---- MT09 ----
          MT09.setText("@MT09R@");
          MT09.setHorizontalAlignment(SwingConstants.RIGHT);
          MT09.setName("MT09");
          panel1.add(MT09);
          MT09.setBounds(505, 295, 140, MT09.getPreferredSize().height);

          //---- PR09 ----
          PR09.setText("@PR09@");
          PR09.setHorizontalAlignment(SwingConstants.RIGHT);
          PR09.setName("PR09");
          panel1.add(PR09);
          PR09.setBounds(650, 295, 110, PR09.getPreferredSize().height);

          //---- LI10 ----
          LI10.setName("LI10");
          panel1.add(LI10);
          LI10.setBounds(60, 320, 110, LI10.getPreferredSize().height);

          //---- WBAS10 ----
          WBAS10.setText("@WBAS10");
          WBAS10.setHorizontalAlignment(SwingConstants.RIGHT);
          WBAS10.setName("WBAS10");
          panel1.add(WBAS10);
          WBAS10.setBounds(170, 320, 110, WBAS10.getPreferredSize().height);

          //---- BA10 ----
          BA10.setName("BA10");
          panel1.add(BA10);
          BA10.setBounds(285, 320, 25, BA10.getPreferredSize().height);

          //---- PSK10 ----
          PSK10.setName("PSK10");
          panel1.add(PSK10);
          PSK10.setBounds(310, 320, 70, PSK10.getPreferredSize().height);

          //---- PSV10 ----
          PSV10.setName("PSV10");
          panel1.add(PSV10);
          PSV10.setBounds(385, 320, 90, PSV10.getPreferredSize().height);

          //---- TY10 ----
          TY10.setName("TY10");
          panel1.add(TY10);
          TY10.setBounds(480, 320, 25, TY10.getPreferredSize().height);

          //---- MT10 ----
          MT10.setText("@MT10R@");
          MT10.setHorizontalAlignment(SwingConstants.RIGHT);
          MT10.setName("MT10");
          panel1.add(MT10);
          MT10.setBounds(505, 320, 140, MT10.getPreferredSize().height);

          //---- PR10 ----
          PR10.setText("@PR10@");
          PR10.setHorizontalAlignment(SwingConstants.RIGHT);
          PR10.setName("PR10");
          panel1.add(PR10);
          PR10.setBounds(650, 320, 110, PR10.getPreferredSize().height);

          //---- MTTOT ----
          MTTOT.setText("@MTTOT@");
          MTTOT.setHorizontalAlignment(SwingConstants.RIGHT);
          MTTOT.setName("MTTOT");
          panel1.add(MTTOT);
          MTTOT.setBounds(505, 360, 140, MTTOT.getPreferredSize().height);

          //---- PRTOT ----
          PRTOT.setText("@PRTOT@");
          PRTOT.setHorizontalAlignment(SwingConstants.RIGHT);
          PRTOT.setName("PRTOT");
          panel1.add(PRTOT);
          PRTOT.setBounds(650, 360, 110, PRTOT.getPreferredSize().height);

          //---- label4 ----
          label4.setText("Libell\u00e9");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(60, 75, 110, label4.getPreferredSize().height);

          //---- label5 ----
          label5.setText("Base");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(170, 75, 110, label5.getPreferredSize().height);

          //---- label6 ----
          label6.setText("B");
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setHorizontalAlignment(SwingConstants.CENTER);
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(285, 75, 25, label6.getPreferredSize().height);

          //---- label7 ----
          label7.setText("Coefficient");
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setHorizontalAlignment(SwingConstants.CENTER);
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(310, 75, 70, label7.getPreferredSize().height);

          //---- label8 ----
          label8.setText("U");
          label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
          label8.setHorizontalAlignment(SwingConstants.CENTER);
          label8.setName("label8");
          panel1.add(label8);
          label8.setBounds(480, 75, 25, label8.getPreferredSize().height);

          //---- label9 ----
          label9.setText("Valeur");
          label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
          label9.setHorizontalAlignment(SwingConstants.CENTER);
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(505, 75, 140, label9.getPreferredSize().height);

          //---- label10 ----
          label10.setText("Prix de revient");
          label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
          label10.setHorizontalAlignment(SwingConstants.CENTER);
          label10.setName("label10");
          panel1.add(label10);
          label10.setBounds(650, 75, 110, label10.getPreferredSize().height);

          //---- label11 ----
          label11.setText("Valeur");
          label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
          label11.setHorizontalAlignment(SwingConstants.CENTER);
          label11.setName("label11");
          panel1.add(label11);
          label11.setBounds(385, 75, 90, label11.getPreferredSize().height);

          //---- label2 ----
          label2.setText("Exprim\u00e9s en @UNLIB@");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(30, 357, 365, 30);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 810, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 408, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField EF01;
  private XRiTextField EF02;
  private XRiTextField EF03;
  private XRiTextField EF04;
  private XRiTextField EF05;
  private XRiTextField EF06;
  private XRiTextField EF07;
  private XRiTextField EF08;
  private XRiTextField EF09;
  private XRiTextField EF10;
  private JLabel label1;
  private RiZoneSortie PX3PDA;
  private XRiTextField LI01;
  private RiZoneSortie WBAS1;
  private XRiTextField BA01;
  private XRiTextField PSK01;
  private XRiTextField PSV01;
  private XRiTextField TY01;
  private RiZoneSortie PR01;
  private RiZoneSortie MT01;
  private XRiTextField LI02;
  private RiZoneSortie WBAS2;
  private XRiTextField BA02;
  private XRiTextField PSK02;
  private XRiTextField PSV02;
  private XRiTextField TY02;
  private RiZoneSortie MT02;
  private RiZoneSortie PR02;
  private XRiTextField LI03;
  private RiZoneSortie WBAS3;
  private XRiTextField BA03;
  private XRiTextField PSK03;
  private XRiTextField PSV03;
  private XRiTextField TY03;
  private RiZoneSortie MT03;
  private RiZoneSortie PR03;
  private XRiTextField LI04;
  private RiZoneSortie WBAS4;
  private XRiTextField BA04;
  private XRiTextField PSK04;
  private XRiTextField PSV04;
  private XRiTextField TY04;
  private RiZoneSortie MT04;
  private RiZoneSortie PR04;
  private XRiTextField LI05;
  private RiZoneSortie WBAS5;
  private XRiTextField BA05;
  private XRiTextField PSK05;
  private XRiTextField PSV05;
  private XRiTextField TY05;
  private RiZoneSortie MT05;
  private RiZoneSortie PR05;
  private XRiTextField LI06;
  private RiZoneSortie WBAS6;
  private XRiTextField BA06;
  private XRiTextField PSK06;
  private XRiTextField PSV06;
  private XRiTextField TY06;
  private RiZoneSortie MT06;
  private RiZoneSortie PR06;
  private XRiTextField LI07;
  private RiZoneSortie WBAS7;
  private XRiTextField BA07;
  private XRiTextField PSK07;
  private XRiTextField PSV07;
  private XRiTextField TY07;
  private RiZoneSortie MT07;
  private RiZoneSortie PR07;
  private XRiTextField LI08;
  private RiZoneSortie WBAS8;
  private XRiTextField BA08;
  private XRiTextField PSK08;
  private XRiTextField PSV08;
  private XRiTextField TY08;
  private RiZoneSortie MT08;
  private RiZoneSortie PR08;
  private XRiTextField LI9;
  private RiZoneSortie WBAS9;
  private XRiTextField B0A9;
  private XRiTextField PSK09;
  private XRiTextField PSV09;
  private XRiTextField TY09;
  private RiZoneSortie MT09;
  private RiZoneSortie PR09;
  private XRiTextField LI10;
  private RiZoneSortie WBAS10;
  private XRiTextField BA10;
  private XRiTextField PSK10;
  private XRiTextField PSV10;
  private XRiTextField TY10;
  private RiZoneSortie MT10;
  private RiZoneSortie PR10;
  private RiZoneSortie MTTOT;
  private RiZoneSortie PRTOT;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label2;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
