
package ri.serien.libecranrpg.vgvx.VGVX23FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX23FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private final static String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  
  public VGVX23FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    xriBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    xriBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    xriBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    WRESX.setVisible(lexique.isPresent("WRESX"));
    WSTKX.setVisible(lexique.isPresent("WSTKX"));
    WDISX.setVisible(lexique.isPresent("WDISX"));
    xriBarreBouton.rafraichir(lexique);
    String affUnite = lexique.HostFieldGetData("P23FLI").trim();
    
    if (affUnite.equals("1")) {
      lbUnite.setText("Affichage en unités de stock");
    }
    else if (affUnite.equals("2")) {
      lbUnite.setText("Affichage en unités de conditionnement");
    }
    else if (affUnite.equals("3")) {
      lbUnite.setText("Affichage en mètres lineaires");
    }
    else {
      lbUnite.setText("");
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Adresses de stockage"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    xriBarreBouton.isTraiterClickBouton(pSNBouton);
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void miChoixActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void miAideActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    miChoix = new JMenuItem();
    miAide = new JMenuItem();
    xriBarreBouton = new XRiBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlPositonStock = new SNPanelTitre();
    lbReserve = new SNLabelUnite();
    WSTKX = new XRiTextField();
    lbMoins = new SNLabelUnite();
    WRESX = new XRiTextField();
    WDISX = new XRiTextField();
    lbEgale = new SNLabelUnite();
    lbUnite = new SNLabelUnite();
    lbEnStocks = new SNLabelUnite();
    lbDisponible = new SNLabelUnite();
    pnlAdresse = new SNPanelTitre();
    LSL1 = new XRiTextField();
    LSL2 = new XRiTextField();
    LSL3 = new XRiTextField();
    LSL4 = new XRiTextField();
    AD11 = new XRiTextField();
    AD21 = new XRiTextField();
    AD31 = new XRiTextField();
    AD41 = new XRiTextField();
    AD12 = new XRiTextField();
    AD22 = new XRiTextField();
    AD32 = new XRiTextField();
    AD42 = new XRiTextField();
    AD13 = new XRiTextField();
    AD23 = new XRiTextField();
    AD33 = new XRiTextField();
    AD43 = new XRiTextField();
    AD14 = new XRiTextField();
    BT_PGUP = new JButton();
    AD24 = new XRiTextField();
    AD34 = new XRiTextField();
    AD44 = new XRiTextField();
    AD15 = new XRiTextField();
    AD25 = new XRiTextField();
    AD35 = new XRiTextField();
    AD45 = new XRiTextField();
    BT_PGDOWN = new JButton();
    AD16 = new XRiTextField();
    AD26 = new XRiTextField();
    AD36 = new XRiTextField();
    AD46 = new XRiTextField();
    AD17 = new XRiTextField();
    AD27 = new XRiTextField();
    AD37 = new XRiTextField();
    AD47 = new XRiTextField();
    AD18 = new XRiTextField();
    AD28 = new XRiTextField();
    AD38 = new XRiTextField();
    AD48 = new XRiTextField();
    AD19 = new XRiTextField();
    AD29 = new XRiTextField();
    AD39 = new XRiTextField();
    AD49 = new XRiTextField();
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- miChoix ----
      miChoix.setText("Choix possibles");
      miChoix.setName("miChoix");
      miChoix.addActionListener(e -> miChoixActionPerformed(e));
      BTD.add(miChoix);
      
      // ---- miAide ----
      miAide.setText("Aide en ligne");
      miAide.setName("miAide");
      miAide.addActionListener(e -> miAideActionPerformed(e));
      BTD.add(miAide);
    }
    
    // ======== this ========
    setPreferredSize(new Dimension(600, 630));
    setMinimumSize(new Dimension(600, 630));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- xriBarreBouton ----
    xriBarreBouton.setName("xriBarreBouton");
    add(xriBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlPositonStock ========
      {
        pnlPositonStock.setBorder(new TitledBorder("Position stock lot sur magasin"));
        pnlPositonStock.setOpaque(false);
        pnlPositonStock.setFont(new Font("sansserif", Font.PLAIN, 14));
        pnlPositonStock.setName("pnlPositonStock");
        pnlPositonStock.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPositonStock.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlPositonStock.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlPositonStock.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlPositonStock.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbReserve ----
        lbReserve.setText("R\u00e9serv\u00e9");
        lbReserve.setMaximumSize(new Dimension(110, 30));
        lbReserve.setMinimumSize(new Dimension(110, 30));
        lbReserve.setPreferredSize(new Dimension(110, 30));
        lbReserve.setName("lbReserve");
        pnlPositonStock.add(lbReserve, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WSTKX ----
        WSTKX.setMaximumSize(new Dimension(155, 30));
        WSTKX.setMinimumSize(new Dimension(155, 30));
        WSTKX.setPreferredSize(new Dimension(155, 30));
        WSTKX.setName("WSTKX");
        pnlPositonStock.add(WSTKX, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbMoins ----
        lbMoins.setText("-");
        lbMoins.setMaximumSize(new Dimension(25, 30));
        lbMoins.setMinimumSize(new Dimension(25, 30));
        lbMoins.setPreferredSize(new Dimension(25, 30));
        lbMoins.setName("lbMoins");
        pnlPositonStock.add(lbMoins, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WRESX ----
        WRESX.setMaximumSize(new Dimension(155, 30));
        WRESX.setMinimumSize(new Dimension(155, 30));
        WRESX.setPreferredSize(new Dimension(155, 30));
        WRESX.setName("WRESX");
        pnlPositonStock.add(WRESX, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WDISX ----
        WDISX.setMaximumSize(new Dimension(155, 30));
        WDISX.setMinimumSize(new Dimension(155, 30));
        WDISX.setPreferredSize(new Dimension(155, 30));
        WDISX.setName("WDISX");
        pnlPositonStock.add(WDISX, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- lbEgale ----
        lbEgale.setText("=");
        lbEgale.setMaximumSize(new Dimension(25, 30));
        lbEgale.setMinimumSize(new Dimension(25, 30));
        lbEgale.setPreferredSize(new Dimension(25, 30));
        lbEgale.setName("lbEgale");
        pnlPositonStock.add(lbEgale, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbUnite ----
        lbUnite.setText("text");
        lbUnite.setMaximumSize(new Dimension(450, 30));
        lbUnite.setMinimumSize(new Dimension(450, 30));
        lbUnite.setPreferredSize(new Dimension(450, 30));
        lbUnite.setName("lbUnite");
        pnlPositonStock.add(lbUnite, new GridBagConstraints(0, 0, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbEnStocks ----
        lbEnStocks.setText("En stock");
        lbEnStocks.setMaximumSize(new Dimension(110, 30));
        lbEnStocks.setMinimumSize(new Dimension(110, 30));
        lbEnStocks.setPreferredSize(new Dimension(110, 30));
        lbEnStocks.setName("lbEnStocks");
        pnlPositonStock.add(lbEnStocks, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDisponible ----
        lbDisponible.setText("Disponible");
        lbDisponible.setMaximumSize(new Dimension(110, 30));
        lbDisponible.setMinimumSize(new Dimension(110, 30));
        lbDisponible.setPreferredSize(new Dimension(110, 30));
        lbDisponible.setName("lbDisponible");
        pnlPositonStock.add(lbDisponible, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlPositonStock,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlAdresse ========
      {
        pnlAdresse.setTitre("Adresses de stockage");
        pnlAdresse.setName("pnlAdresse");
        pnlAdresse.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlAdresse.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlAdresse.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlAdresse.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlAdresse.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- LSL1 ----
        LSL1.setMaximumSize(new Dimension(95, 30));
        LSL1.setMinimumSize(new Dimension(95, 30));
        LSL1.setPreferredSize(new Dimension(95, 30));
        LSL1.setName("LSL1");
        pnlAdresse.add(LSL1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- LSL2 ----
        LSL2.setMaximumSize(new Dimension(95, 30));
        LSL2.setMinimumSize(new Dimension(95, 30));
        LSL2.setPreferredSize(new Dimension(95, 30));
        LSL2.setName("LSL2");
        pnlAdresse.add(LSL2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- LSL3 ----
        LSL3.setMaximumSize(new Dimension(95, 30));
        LSL3.setMinimumSize(new Dimension(95, 30));
        LSL3.setPreferredSize(new Dimension(95, 30));
        LSL3.setName("LSL3");
        pnlAdresse.add(LSL3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- LSL4 ----
        LSL4.setMaximumSize(new Dimension(95, 30));
        LSL4.setMinimumSize(new Dimension(95, 30));
        LSL4.setPreferredSize(new Dimension(95, 30));
        LSL4.setName("LSL4");
        pnlAdresse.add(LSL4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD11 ----
        AD11.setMaximumSize(new Dimension(48, 30));
        AD11.setMinimumSize(new Dimension(48, 30));
        AD11.setPreferredSize(new Dimension(48, 30));
        AD11.setName("AD11");
        pnlAdresse.add(AD11, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD21 ----
        AD21.setMaximumSize(new Dimension(48, 30));
        AD21.setMinimumSize(new Dimension(48, 30));
        AD21.setPreferredSize(new Dimension(48, 30));
        AD21.setName("AD21");
        pnlAdresse.add(AD21, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD31 ----
        AD31.setMaximumSize(new Dimension(48, 30));
        AD31.setMinimumSize(new Dimension(48, 30));
        AD31.setPreferredSize(new Dimension(48, 30));
        AD31.setName("AD31");
        pnlAdresse.add(AD31, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD41 ----
        AD41.setPreferredSize(new Dimension(48, 30));
        AD41.setMaximumSize(new Dimension(48, 30));
        AD41.setMinimumSize(new Dimension(48, 30));
        AD41.setName("AD41");
        pnlAdresse.add(AD41, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD12 ----
        AD12.setMaximumSize(new Dimension(48, 30));
        AD12.setMinimumSize(new Dimension(48, 30));
        AD12.setPreferredSize(new Dimension(48, 30));
        AD12.setName("AD12");
        pnlAdresse.add(AD12, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD22 ----
        AD22.setMaximumSize(new Dimension(48, 30));
        AD22.setMinimumSize(new Dimension(48, 30));
        AD22.setPreferredSize(new Dimension(48, 30));
        AD22.setName("AD22");
        pnlAdresse.add(AD22, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD32 ----
        AD32.setMaximumSize(new Dimension(48, 30));
        AD32.setMinimumSize(new Dimension(48, 30));
        AD32.setPreferredSize(new Dimension(48, 30));
        AD32.setName("AD32");
        pnlAdresse.add(AD32, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD42 ----
        AD42.setPreferredSize(new Dimension(48, 30));
        AD42.setMaximumSize(new Dimension(48, 30));
        AD42.setMinimumSize(new Dimension(48, 30));
        AD42.setName("AD42");
        pnlAdresse.add(AD42, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD13 ----
        AD13.setMaximumSize(new Dimension(48, 30));
        AD13.setMinimumSize(new Dimension(48, 30));
        AD13.setPreferredSize(new Dimension(48, 30));
        AD13.setName("AD13");
        pnlAdresse.add(AD13, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD23 ----
        AD23.setMaximumSize(new Dimension(48, 30));
        AD23.setMinimumSize(new Dimension(48, 30));
        AD23.setPreferredSize(new Dimension(48, 30));
        AD23.setName("AD23");
        pnlAdresse.add(AD23, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD33 ----
        AD33.setMaximumSize(new Dimension(48, 30));
        AD33.setMinimumSize(new Dimension(48, 30));
        AD33.setPreferredSize(new Dimension(48, 30));
        AD33.setName("AD33");
        pnlAdresse.add(AD33, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD43 ----
        AD43.setPreferredSize(new Dimension(48, 30));
        AD43.setMaximumSize(new Dimension(48, 30));
        AD43.setMinimumSize(new Dimension(48, 30));
        AD43.setName("AD43");
        pnlAdresse.add(AD43, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD14 ----
        AD14.setMaximumSize(new Dimension(48, 30));
        AD14.setMinimumSize(new Dimension(48, 30));
        AD14.setPreferredSize(new Dimension(48, 30));
        AD14.setName("AD14");
        pnlAdresse.add(AD14, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setMaximumSize(new Dimension(25, 100));
        BT_PGUP.setMinimumSize(new Dimension(25, 100));
        BT_PGUP.setPreferredSize(new Dimension(25, 100));
        BT_PGUP.setName("BT_PGUP");
        pnlAdresse.add(BT_PGUP, new GridBagConstraints(4, 1, 1, 4, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- AD24 ----
        AD24.setMaximumSize(new Dimension(48, 30));
        AD24.setMinimumSize(new Dimension(48, 30));
        AD24.setPreferredSize(new Dimension(48, 30));
        AD24.setName("AD24");
        pnlAdresse.add(AD24, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD34 ----
        AD34.setMaximumSize(new Dimension(48, 30));
        AD34.setMinimumSize(new Dimension(48, 30));
        AD34.setPreferredSize(new Dimension(48, 30));
        AD34.setName("AD34");
        pnlAdresse.add(AD34, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD44 ----
        AD44.setMaximumSize(new Dimension(48, 30));
        AD44.setMinimumSize(new Dimension(48, 30));
        AD44.setPreferredSize(new Dimension(48, 30));
        AD44.setName("AD44");
        pnlAdresse.add(AD44, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD15 ----
        AD15.setPreferredSize(new Dimension(48, 30));
        AD15.setMaximumSize(new Dimension(48, 30));
        AD15.setMinimumSize(new Dimension(48, 30));
        AD15.setName("AD15");
        pnlAdresse.add(AD15, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD25 ----
        AD25.setMaximumSize(new Dimension(48, 30));
        AD25.setMinimumSize(new Dimension(48, 30));
        AD25.setPreferredSize(new Dimension(48, 30));
        AD25.setName("AD25");
        pnlAdresse.add(AD25, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD35 ----
        AD35.setMaximumSize(new Dimension(48, 30));
        AD35.setMinimumSize(new Dimension(48, 30));
        AD35.setPreferredSize(new Dimension(48, 30));
        AD35.setName("AD35");
        pnlAdresse.add(AD35, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD45 ----
        AD45.setMaximumSize(new Dimension(48, 30));
        AD45.setMinimumSize(new Dimension(48, 30));
        AD45.setPreferredSize(new Dimension(48, 30));
        AD45.setName("AD45");
        pnlAdresse.add(AD45, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setToolTipText("Page suivante");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setMaximumSize(new Dimension(25, 100));
        BT_PGDOWN.setMinimumSize(new Dimension(25, 100));
        BT_PGDOWN.setPreferredSize(new Dimension(25, 100));
        BT_PGDOWN.setName("BT_PGDOWN");
        pnlAdresse.add(BT_PGDOWN, new GridBagConstraints(4, 5, 1, 4, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- AD16 ----
        AD16.setPreferredSize(new Dimension(48, 30));
        AD16.setMaximumSize(new Dimension(48, 30));
        AD16.setMinimumSize(new Dimension(48, 30));
        AD16.setName("AD16");
        pnlAdresse.add(AD16, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD26 ----
        AD26.setMaximumSize(new Dimension(48, 30));
        AD26.setMinimumSize(new Dimension(48, 30));
        AD26.setPreferredSize(new Dimension(48, 30));
        AD26.setName("AD26");
        pnlAdresse.add(AD26, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD36 ----
        AD36.setMaximumSize(new Dimension(48, 30));
        AD36.setMinimumSize(new Dimension(48, 30));
        AD36.setPreferredSize(new Dimension(48, 30));
        AD36.setName("AD36");
        pnlAdresse.add(AD36, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD46 ----
        AD46.setMaximumSize(new Dimension(48, 30));
        AD46.setMinimumSize(new Dimension(48, 30));
        AD46.setPreferredSize(new Dimension(48, 30));
        AD46.setName("AD46");
        pnlAdresse.add(AD46, new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD17 ----
        AD17.setPreferredSize(new Dimension(48, 30));
        AD17.setMaximumSize(new Dimension(48, 30));
        AD17.setMinimumSize(new Dimension(48, 30));
        AD17.setName("AD17");
        pnlAdresse.add(AD17, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD27 ----
        AD27.setMaximumSize(new Dimension(48, 30));
        AD27.setMinimumSize(new Dimension(48, 30));
        AD27.setPreferredSize(new Dimension(48, 30));
        AD27.setName("AD27");
        pnlAdresse.add(AD27, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD37 ----
        AD37.setMaximumSize(new Dimension(48, 30));
        AD37.setMinimumSize(new Dimension(48, 30));
        AD37.setPreferredSize(new Dimension(48, 30));
        AD37.setName("AD37");
        pnlAdresse.add(AD37, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD47 ----
        AD47.setMaximumSize(new Dimension(48, 30));
        AD47.setMinimumSize(new Dimension(48, 30));
        AD47.setPreferredSize(new Dimension(48, 30));
        AD47.setName("AD47");
        pnlAdresse.add(AD47, new GridBagConstraints(3, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD18 ----
        AD18.setPreferredSize(new Dimension(48, 30));
        AD18.setMaximumSize(new Dimension(48, 30));
        AD18.setMinimumSize(new Dimension(48, 30));
        AD18.setName("AD18");
        pnlAdresse.add(AD18, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD28 ----
        AD28.setMaximumSize(new Dimension(48, 30));
        AD28.setMinimumSize(new Dimension(48, 30));
        AD28.setPreferredSize(new Dimension(48, 30));
        AD28.setName("AD28");
        pnlAdresse.add(AD28, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD38 ----
        AD38.setMaximumSize(new Dimension(48, 30));
        AD38.setMinimumSize(new Dimension(48, 30));
        AD38.setPreferredSize(new Dimension(48, 30));
        AD38.setName("AD38");
        pnlAdresse.add(AD38, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD48 ----
        AD48.setMaximumSize(new Dimension(48, 30));
        AD48.setMinimumSize(new Dimension(48, 30));
        AD48.setPreferredSize(new Dimension(48, 30));
        AD48.setName("AD48");
        pnlAdresse.add(AD48, new GridBagConstraints(3, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- AD19 ----
        AD19.setPreferredSize(new Dimension(48, 30));
        AD19.setMaximumSize(new Dimension(48, 30));
        AD19.setMinimumSize(new Dimension(48, 30));
        AD19.setName("AD19");
        pnlAdresse.add(AD19, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- AD29 ----
        AD29.setPreferredSize(new Dimension(48, 30));
        AD29.setMaximumSize(new Dimension(48, 30));
        AD29.setMinimumSize(new Dimension(48, 30));
        AD29.setName("AD29");
        pnlAdresse.add(AD29, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- AD39 ----
        AD39.setPreferredSize(new Dimension(48, 30));
        AD39.setMaximumSize(new Dimension(48, 30));
        AD39.setMinimumSize(new Dimension(48, 30));
        AD39.setName("AD39");
        pnlAdresse.add(AD39, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- AD49 ----
        AD49.setPreferredSize(new Dimension(48, 30));
        AD49.setMaximumSize(new Dimension(48, 30));
        AD49.setMinimumSize(new Dimension(48, 30));
        AD49.setName("AD49");
        pnlAdresse.add(AD49, new GridBagConstraints(3, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlContenu.add(pnlAdresse,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem miChoix;
  private JMenuItem miAide;
  private XRiBarreBouton xriBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlPositonStock;
  private SNLabelUnite lbReserve;
  private XRiTextField WSTKX;
  private SNLabelUnite lbMoins;
  private XRiTextField WRESX;
  private XRiTextField WDISX;
  private SNLabelUnite lbEgale;
  private SNLabelUnite lbUnite;
  private SNLabelUnite lbEnStocks;
  private SNLabelUnite lbDisponible;
  private SNPanelTitre pnlAdresse;
  private XRiTextField LSL1;
  private XRiTextField LSL2;
  private XRiTextField LSL3;
  private XRiTextField LSL4;
  private XRiTextField AD11;
  private XRiTextField AD21;
  private XRiTextField AD31;
  private XRiTextField AD41;
  private XRiTextField AD12;
  private XRiTextField AD22;
  private XRiTextField AD32;
  private XRiTextField AD42;
  private XRiTextField AD13;
  private XRiTextField AD23;
  private XRiTextField AD33;
  private XRiTextField AD43;
  private XRiTextField AD14;
  private JButton BT_PGUP;
  private XRiTextField AD24;
  private XRiTextField AD34;
  private XRiTextField AD44;
  private XRiTextField AD15;
  private XRiTextField AD25;
  private XRiTextField AD35;
  private XRiTextField AD45;
  private JButton BT_PGDOWN;
  private XRiTextField AD16;
  private XRiTextField AD26;
  private XRiTextField AD36;
  private XRiTextField AD46;
  private XRiTextField AD17;
  private XRiTextField AD27;
  private XRiTextField AD37;
  private XRiTextField AD47;
  private XRiTextField AD18;
  private XRiTextField AD28;
  private XRiTextField AD38;
  private XRiTextField AD48;
  private XRiTextField AD19;
  private XRiTextField AD29;
  private XRiTextField AD39;
  private XRiTextField AD49;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
