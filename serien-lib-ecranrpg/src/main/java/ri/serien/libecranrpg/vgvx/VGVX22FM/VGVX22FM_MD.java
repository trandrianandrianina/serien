
package ri.serien.libecranrpg.vgvx.VGVX22FM;
// Nom Fichier: pop_VGVX22FM_FMTMD_FMTF1_136.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VGVX22FM_MD extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX22FM_MD(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    // setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix fonctions"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="R"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "R", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="C"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "C", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="M"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "M", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="I"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "I", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="A"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "A", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="D"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "D", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    P_PnlOpts = new JPanel();
    OBJ_7 = new JButton();
    OBJ_8 = new JButton();
    OBJ_9 = new JButton();
    OBJ_10 = new JButton();
    OBJ_11 = new JButton();
    OBJ_12 = new JButton();
    OBJ_6 = new JLabel();

    //======== this ========
    setPreferredSize(new Dimension(205, 235));
    setName("this");
    setLayout(null);

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setBorder(new TitledBorder("Fonctions"));
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);

      //---- OBJ_7 ----
      OBJ_7.setText("Retour");
      OBJ_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_7);
      OBJ_7.setBounds(20, 30, 150, 30);

      //---- OBJ_8 ----
      OBJ_8.setText("Cr\u00e9ation");
      OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_8);
      OBJ_8.setBounds(20, 60, 150, 30);

      //---- OBJ_9 ----
      OBJ_9.setText("Modification");
      OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_9);
      OBJ_9.setBounds(20, 90, 150, 30);

      //---- OBJ_10 ----
      OBJ_10.setText("Interrogation");
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_10);
      OBJ_10.setBounds(20, 120, 150, 30);

      //---- OBJ_11 ----
      OBJ_11.setText("Annulation");
      OBJ_11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_11);
      OBJ_11.setBounds(20, 150, 150, 30);

      //---- OBJ_12 ----
      OBJ_12.setText("Duplication");
      OBJ_12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_12);
      OBJ_12.setBounds(20, 180, 150, 30);
    }
    add(P_PnlOpts);
    P_PnlOpts.setBounds(5, 5, 190, 225);

    setPreferredSize(new Dimension(205, 235));

    //---- OBJ_6 ----
    OBJ_6.setText("Fonctions");
    OBJ_6.setName("OBJ_6");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel P_PnlOpts;
  private JButton OBJ_7;
  private JButton OBJ_8;
  private JButton OBJ_9;
  private JButton OBJ_10;
  private JButton OBJ_11;
  private JButton OBJ_12;
  private JLabel OBJ_6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
