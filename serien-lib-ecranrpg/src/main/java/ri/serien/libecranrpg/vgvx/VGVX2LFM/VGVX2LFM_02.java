
package ri.serien.libecranrpg.vgvx.VGVX2LFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGVX2LFM_02 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX2LFM_02(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    ZL01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZL01@")).trim());
    ZL02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZL02@")).trim());
    ZL03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZL03@")).trim());
    ZL04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZL04@")).trim());
    ZL05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZL05@")).trim());
    TIZ01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZ01@")).trim());
    TIZ02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZ02@")).trim());
    TIZ03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZ03@")).trim());
    TIZ04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZ04@")).trim());
    TIZ05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZ05@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Modification des informations d'un lot"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    ZL01 = new RiZoneSortie();
    ZL02 = new RiZoneSortie();
    ZL03 = new RiZoneSortie();
    ZL04 = new RiZoneSortie();
    ZL05 = new RiZoneSortie();
    TIZ01 = new RiZoneSortie();
    TIZ02 = new RiZoneSortie();
    TIZ03 = new RiZoneSortie();
    TIZ04 = new RiZoneSortie();
    TIZ05 = new RiZoneSortie();
    WTP11 = new XRiTextField();
    WTP21 = new XRiTextField();
    WTP31 = new XRiTextField();
    WTP41 = new XRiTextField();
    WTP51 = new XRiTextField();
    WTP12 = new XRiTextField();
    WTP22 = new XRiTextField();
    WTP32 = new XRiTextField();
    WTP42 = new XRiTextField();
    WTP52 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(665, 255));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Modification des zones personnalis\u00e9es"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- ZL01 ----
          ZL01.setText("@ZL01@");
          ZL01.setName("ZL01");
          panel1.add(ZL01);
          ZL01.setBounds(25, 50, 260, ZL01.getPreferredSize().height);

          //---- ZL02 ----
          ZL02.setText("@ZL02@");
          ZL02.setName("ZL02");
          panel1.add(ZL02);
          ZL02.setBounds(25, 80, 260, ZL02.getPreferredSize().height);

          //---- ZL03 ----
          ZL03.setText("@ZL03@");
          ZL03.setName("ZL03");
          panel1.add(ZL03);
          ZL03.setBounds(25, 110, 260, ZL03.getPreferredSize().height);

          //---- ZL04 ----
          ZL04.setText("@ZL04@");
          ZL04.setName("ZL04");
          panel1.add(ZL04);
          ZL04.setBounds(25, 140, 260, ZL04.getPreferredSize().height);

          //---- ZL05 ----
          ZL05.setText("@ZL05@");
          ZL05.setName("ZL05");
          panel1.add(ZL05);
          ZL05.setBounds(25, 170, 260, ZL05.getPreferredSize().height);

          //---- TIZ01 ----
          TIZ01.setText("@TIZ01@");
          TIZ01.setName("TIZ01");
          panel1.add(TIZ01);
          TIZ01.setBounds(300, 50, 30, TIZ01.getPreferredSize().height);

          //---- TIZ02 ----
          TIZ02.setText("@TIZ02@");
          TIZ02.setName("TIZ02");
          panel1.add(TIZ02);
          TIZ02.setBounds(300, 80, 30, TIZ02.getPreferredSize().height);

          //---- TIZ03 ----
          TIZ03.setText("@TIZ03@");
          TIZ03.setName("TIZ03");
          panel1.add(TIZ03);
          TIZ03.setBounds(300, 110, 30, TIZ03.getPreferredSize().height);

          //---- TIZ04 ----
          TIZ04.setText("@TIZ04@");
          TIZ04.setName("TIZ04");
          panel1.add(TIZ04);
          TIZ04.setBounds(300, 140, 30, TIZ04.getPreferredSize().height);

          //---- TIZ05 ----
          TIZ05.setText("@TIZ05@");
          TIZ05.setName("TIZ05");
          panel1.add(TIZ05);
          TIZ05.setBounds(300, 170, 30, TIZ05.getPreferredSize().height);

          //---- WTP11 ----
          WTP11.setComponentPopupMenu(BTD);
          WTP11.setName("WTP11");
          panel1.add(WTP11);
          WTP11.setBounds(350, 48, 30, WTP11.getPreferredSize().height);

          //---- WTP21 ----
          WTP21.setComponentPopupMenu(BTD);
          WTP21.setName("WTP21");
          panel1.add(WTP21);
          WTP21.setBounds(350, 78, 30, WTP21.getPreferredSize().height);

          //---- WTP31 ----
          WTP31.setComponentPopupMenu(BTD);
          WTP31.setName("WTP31");
          panel1.add(WTP31);
          WTP31.setBounds(350, 108, 30, WTP31.getPreferredSize().height);

          //---- WTP41 ----
          WTP41.setComponentPopupMenu(BTD);
          WTP41.setName("WTP41");
          panel1.add(WTP41);
          WTP41.setBounds(350, 138, 30, WTP41.getPreferredSize().height);

          //---- WTP51 ----
          WTP51.setComponentPopupMenu(BTD);
          WTP51.setName("WTP51");
          panel1.add(WTP51);
          WTP51.setBounds(350, 168, 30, WTP51.getPreferredSize().height);

          //---- WTP12 ----
          WTP12.setComponentPopupMenu(BTD);
          WTP12.setName("WTP12");
          panel1.add(WTP12);
          WTP12.setBounds(405, 48, 30, WTP12.getPreferredSize().height);

          //---- WTP22 ----
          WTP22.setComponentPopupMenu(BTD);
          WTP22.setName("WTP22");
          panel1.add(WTP22);
          WTP22.setBounds(405, 78, 30, WTP22.getPreferredSize().height);

          //---- WTP32 ----
          WTP32.setComponentPopupMenu(BTD);
          WTP32.setName("WTP32");
          panel1.add(WTP32);
          WTP32.setBounds(405, 108, 30, WTP32.getPreferredSize().height);

          //---- WTP42 ----
          WTP42.setComponentPopupMenu(BTD);
          WTP42.setName("WTP42");
          panel1.add(WTP42);
          WTP42.setBounds(405, 138, 30, WTP42.getPreferredSize().height);

          //---- WTP52 ----
          WTP52.setComponentPopupMenu(BTD);
          WTP52.setName("WTP52");
          panel1.add(WTP52);
          WTP52.setBounds(405, 168, 30, WTP52.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Avant");
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(345, 25, 40, 25);

          //---- label2 ----
          label2.setText("Apr\u00e8s");
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(400, 25, 40, 25);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 471, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie ZL01;
  private RiZoneSortie ZL02;
  private RiZoneSortie ZL03;
  private RiZoneSortie ZL04;
  private RiZoneSortie ZL05;
  private RiZoneSortie TIZ01;
  private RiZoneSortie TIZ02;
  private RiZoneSortie TIZ03;
  private RiZoneSortie TIZ04;
  private RiZoneSortie TIZ05;
  private XRiTextField WTP11;
  private XRiTextField WTP21;
  private XRiTextField WTP31;
  private XRiTextField WTP41;
  private XRiTextField WTP51;
  private XRiTextField WTP12;
  private XRiTextField WTP22;
  private XRiTextField WTP32;
  private XRiTextField WTP42;
  private XRiTextField WTP52;
  private JLabel label1;
  private JLabel label2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
