
package ri.serien.libecranrpg.vgvx.VGVX66FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVX66FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] T1TCD_Value = { "", "N", "R", "K", };
  private String[] T1TRA_Value = { "", "A", "T", "F", "S", "G", "*", "R", "L", "1", "2", "3", };
  private String[] WTAR_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "98", "99" };
  private String[] T2TCD_Value = { "", "M", "Q" };
  
  public VGVX66FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    // Ajout
    T1TCD.setValeurs(T1TCD_Value, null);
    T1TRA.setValeurs(T1TRA_Value, null);
    WTAR1.setValeurs(WTAR_Value, null);
    T2TCD.setValeurs(T2TCD_Value, null);
    rbTF.setValeurs("", "T1IN1");
    rbTD.setValeurs("D", "T1IN1");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    T1RAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1RAT@")).trim());
    T1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1ETB@")).trim());
    CDNAU.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CDNAU@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    boolean isDerogSimplifiee = lexique.isTrue("21");
    boolean isModifiable = (lexique.getMode() != Lexical.MODE_CONSULTATION);
    
    // En dérogation simplifiée
    rbT1REM4.setVisible(!isDerogSimplifiee);
    T1REM4.setVisible(!isDerogSimplifiee);
    rbT1REM5.setVisible(!isDerogSimplifiee);
    T1REM5.setVisible(!isDerogSimplifiee);
    rbWCOE5.setVisible(!isDerogSimplifiee);
    WCOE5.setVisible(!isDerogSimplifiee);
    lbPourcentagePV.setVisible(!isDerogSimplifiee);
    lbPourcentagePC.setVisible(!isDerogSimplifiee);
    // separator2.setVisible(!isDerogSimplifiee);
    // rbT1VALAX.setVisible(!isDerogSimplifiee);
    lbT1VALAX.setVisible(isDerogSimplifiee);
    rbT1VALAX.setSelected(isDerogSimplifiee);
    T2VALX.setVisible(!isDerogSimplifiee);
    rbT2VALX.setVisible(!isDerogSimplifiee);
    lbT2TCD.setVisible(!isDerogSimplifiee);
    T2TCD.setVisible(!isDerogSimplifiee);
    
    T1VALX.setEnabled(lexique.HostFieldGetData("T1TCD").trim().equals("N") && isModifiable);
    T1REM1.setEnabled(lexique.HostFieldGetData("T1TCD").trim().equals("R") && isModifiable);
    T1REM2.setEnabled(lexique.HostFieldGetData("T1TCD").trim().equals("R") && isModifiable);
    T1REM3.setEnabled(lexique.HostFieldGetData("T1TCD").trim().equals("R") && isModifiable);
    T1COE.setEnabled(lexique.HostFieldGetData("T1TCD").trim().equals("K") && isModifiable);
    WTAR1.setEnabled(
        (lexique.HostFieldGetData("T1TCD").trim().equals("R") || lexique.HostFieldGetData("T1TCD").trim().equals("K")) && isModifiable);
    
    rbT1VALAX.setEnabled(isModifiable);
    T1VALAX.setEnabled(rbT1VALAX.isSelected() && isModifiable);
    rbT1REM4.setEnabled(isModifiable);
    T1REM4.setEnabled(rbT1REM4.isSelected() && isModifiable);
    rbT1REM5.setEnabled(isModifiable);
    T1REM5.setEnabled(rbT1REM5.isSelected() && isModifiable);
    rbWCOE5.setEnabled(isModifiable);
    WCOE5.setEnabled(rbWCOE5.isSelected() && isModifiable);
    rbTF.setEnabled(rbT1REM5.isSelected() && isModifiable);
    rbTD.setEnabled(rbT1REM5.isSelected() && isModifiable);
    pnlRemiseCompensatoire.setVisible(!isDerogSimplifiee);
    
    p_bpresentation.setCodeEtablissement(T1ETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void helpActionPerformed(ActionEvent e) {
    // lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void T1TCDItemStateChanged(ItemEvent e) {
    T1VALX.setEnabled(T1TCD.getSelectedIndex() == 1);
    T1REM1.setEnabled(T1TCD.getSelectedIndex() == 2);
    T1REM2.setEnabled(T1TCD.getSelectedIndex() == 2);
    T1REM3.setEnabled(T1TCD.getSelectedIndex() == 2);
    T1COE.setEnabled(T1TCD.getSelectedIndex() == 3);
    WTAR1.setEnabled(T1TCD.getSelectedIndex() == 2 || T1TCD.getSelectedIndex() == 3);
  }
  
  private void rbOptionsActionPerformed(ActionEvent e) {
    T1VALAX.setEnabled(rbT1VALAX.isSelected());
    T1REM4.setEnabled(rbT1REM4.isSelected());
    T1REM5.setEnabled(rbT1REM5.isSelected());
    WCOE5.setEnabled(rbWCOE5.isSelected());
    rbTF.setEnabled(rbT1REM5.isSelected());
    rbTD.setEnabled(rbT1REM5.isSelected());
  }
  
  private void initComponents() {
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_presentation = new JPanel();
    p_logo = new JPanel();
    logoEtb = new JLabel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_72 = new JLabel();
    T1CNV = new XRiTextField();
    OBJ_50 = new JLabel();
    T1TRA = new XRiComboBox();
    OBJ_49 = new JLabel();
    T1RAT = new RiZoneSortie();
    OBJ_45 = new JLabel();
    T1ETB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new JPanel();
    p_contenu = new JPanel();
    lbA1UNV = new JLabel();
    lbCDNAU = new JLabel();
    CDNAU = new RiZoneSortie();
    A1UNV = new XRiTextField();
    separator1 = new SNLabelTitre();
    lbT1DTDX = new JLabel();
    lbT1DTFX = new JLabel();
    spPeriodes = new SNLabelTitre();
    T1TCD = new XRiComboBox();
    T1VALX = new XRiTextField();
    lbT1REM = new JLabel();
    T1REM1 = new XRiTextField();
    T1REM2 = new XRiTextField();
    T1REM3 = new XRiTextField();
    lbT1VALX = new JLabel();
    OBJ_84 = new JLabel();
    lbT1COE = new JLabel();
    lbWTAR1 = new JLabel();
    T1COE = new XRiTextField();
    WTAR1 = new XRiComboBox();
    T1DTDX = new XRiCalendrier();
    T1DTFX = new XRiCalendrier();
    rbT2VALX = new JLabel();
    T2VALX = new XRiTextField();
    lbT2TCD = new JLabel();
    T2TCD = new XRiComboBox();
    separator2 = new SNLabelTitre();
    rbT1VALAX = new JRadioButton();
    T1VALAX = new XRiTextField();
    rbT1REM4 = new JRadioButton();
    T1REM4 = new XRiTextField();
    rbT1REM5 = new JRadioButton();
    rbWCOE5 = new JRadioButton();
    WCOE5 = new XRiTextField();
    lbPourcentagePV = new JLabel();
    pnlRemiseCompensatoire = new SNPanel();
    T1REM5 = new XRiTextField();
    lbPourcentagePC = new JLabel();
    rbTF = new XRiRadioButton();
    rbTD = new XRiRadioButton();
    BTD = new JPopupMenu();
    help = new JMenuItem();
    lbT1VALAX = new JLabel();
    buttonGroup1 = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ======== p_presentation ========
      {
        p_presentation.setPreferredSize(new Dimension(1020, 55));
        p_presentation.setMinimumSize(new Dimension(950, 55));
        p_presentation.setMaximumSize(new Dimension(32767, 55));
        p_presentation.setBackground(Color.white);
        p_presentation.setAutoscrolls(true);
        p_presentation.setFocusable(false);
        p_presentation.setName("p_presentation");
        p_presentation.setLayout(new BorderLayout());
        
        // ======== p_logo ========
        {
          p_logo.setMinimumSize(new Dimension(120, 55));
          p_logo.setPreferredSize(new Dimension(120, 55));
          p_logo.setBackground(new Color(238, 239, 241));
          p_logo.setName("p_logo");
          p_logo.setLayout(null);
          
          // ---- logoEtb ----
          logoEtb.setBorder(null);
          logoEtb.setName("logoEtb");
          p_logo.add(logoEtb);
          logoEtb.setBounds(70, 5, 45, 45);
        }
        p_presentation.add(p_logo, BorderLayout.EAST);
        
        // ---- p_bpresentation ----
        p_bpresentation.setText("Condition de d\u00e9rogation");
        p_bpresentation.setName("p_bpresentation");
        p_presentation.add(p_bpresentation, BorderLayout.NORTH);
      }
      p_nord.add(p_presentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(880, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
          
          // ---- OBJ_72 ----
          OBJ_72.setText("Num\u00e9ro");
          OBJ_72.setName("OBJ_72");
          p_tete_gauche.add(OBJ_72);
          OBJ_72.setBounds(160, 5, 55, 18);
          
          // ---- T1CNV ----
          T1CNV.setComponentPopupMenu(null);
          T1CNV.setHorizontalAlignment(SwingConstants.RIGHT);
          T1CNV.setName("T1CNV");
          p_tete_gauche.add(T1CNV);
          T1CNV.setBounds(225, 0, 60, T1CNV.getPreferredSize().height);
          
          // ---- OBJ_50 ----
          OBJ_50.setText("Type de rattachement");
          OBJ_50.setName("OBJ_50");
          p_tete_gauche.add(OBJ_50);
          OBJ_50.setBounds(300, 5, 130, 18);
          
          // ---- T1TRA ----
          T1TRA.setModel(new DefaultComboBoxModel(
              new String[] { "Tous", "Article", "Tarif", "Famille", "Sous famille", "Groupe", "Embo\u00eetage ou g\u00e9n\u00e9ral",
                  "Ensemble d'articles", "Num de ligne (rang)", "Regroupement achat", "Fournisseur" }));
          T1TRA.setComponentPopupMenu(null);
          T1TRA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T1TRA.setName("T1TRA");
          p_tete_gauche.add(T1TRA);
          T1TRA.setBounds(425, 1, 210, T1TRA.getPreferredSize().height);
          
          // ---- OBJ_49 ----
          OBJ_49.setText("Rattachement");
          OBJ_49.setName("OBJ_49");
          p_tete_gauche.add(OBJ_49);
          OBJ_49.setBounds(640, 5, 90, 18);
          
          // ---- T1RAT ----
          T1RAT.setOpaque(false);
          T1RAT.setText("@T1RAT@");
          T1RAT.setName("T1RAT");
          p_tete_gauche.add(T1RAT);
          T1RAT.setBounds(745, 2, 168, T1RAT.getPreferredSize().height);
          
          // ---- OBJ_45 ----
          OBJ_45.setText("Etablissement");
          OBJ_45.setName("OBJ_45");
          p_tete_gauche.add(OBJ_45);
          OBJ_45.setBounds(5, 5, 90, 18);
          
          // ---- T1ETB ----
          T1ETB.setOpaque(false);
          T1ETB.setText("@T1ETB@");
          T1ETB.setName("T1ETB");
          p_tete_gauche.add(T1ETB);
          T1ETB.setBounds(100, 2, 40, T1ETB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.CENTER);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(239, 239, 222));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        ((GridBagLayout) p_centrage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) p_centrage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) p_centrage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) p_centrage.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(960, 250));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setBorder(new TitledBorder(""));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(new GridBagLayout());
          ((GridBagLayout) p_contenu.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 123, 0, 78, 65, 0, 0, 0 };
          ((GridBagLayout) p_contenu.getLayout()).rowHeights = new int[] { 45, 55, 0, 40, 0, 0, 0, 55, 40, 40, 40, 40, 0, 0 };
          ((GridBagLayout) p_contenu.getLayout()).columnWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) p_contenu.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
          
          // ---- lbA1UNV ----
          lbA1UNV.setText("Unit\u00e9 de vente");
          lbA1UNV.setName("lbA1UNV");
          p_contenu.add(lbA1UNV, new GridBagConstraints(9, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbCDNAU ----
          lbCDNAU.setComponentPopupMenu(null);
          lbCDNAU.setText("Num\u00e9ro d'autorisation");
          lbCDNAU.setFont(lbCDNAU.getFont().deriveFont(lbCDNAU.getFont().getStyle() | Font.BOLD));
          lbCDNAU.setName("lbCDNAU");
          p_contenu.add(lbCDNAU, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- CDNAU ----
          CDNAU.setText("@CDNAU@");
          CDNAU.setMinimumSize(new Dimension(210, 24));
          CDNAU.setPreferredSize(new Dimension(210, 24));
          CDNAU.setName("CDNAU");
          p_contenu.add(CDNAU, new GridBagConstraints(2, 0, 4, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1UNV ----
          A1UNV.setPreferredSize(new Dimension(30, 28));
          A1UNV.setMinimumSize(new Dimension(30, 28));
          A1UNV.setName("A1UNV");
          p_contenu.add(A1UNV, new GridBagConstraints(11, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- separator1 ----
          separator1.setText("Prix de vente d\u00e9rog\u00e9");
          separator1.setName("separator1");
          p_contenu.add(separator1, new GridBagConstraints(0, 1, 13, 1, 0.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 10, 0), 0, 0));
          
          // ---- lbT1DTDX ----
          lbT1DTDX.setComponentPopupMenu(null);
          lbT1DTDX.setText("D\u00e9but");
          lbT1DTDX.setFont(lbT1DTDX.getFont().deriveFont(lbT1DTDX.getFont().getStyle() | Font.BOLD));
          lbT1DTDX.setName("lbT1DTDX");
          p_contenu.add(lbT1DTDX, new GridBagConstraints(8, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbT1DTFX ----
          lbT1DTFX.setComponentPopupMenu(null);
          lbT1DTFX.setText("Fin");
          lbT1DTFX.setFont(lbT1DTFX.getFont().deriveFont(lbT1DTFX.getFont().getStyle() | Font.BOLD));
          lbT1DTFX.setName("lbT1DTFX");
          p_contenu.add(lbT1DTFX, new GridBagConstraints(10, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- spPeriodes ----
          spPeriodes.setText("P\u00e9riode de validit\u00e9");
          spPeriodes.setName("spPeriodes");
          p_contenu.add(spPeriodes, new GridBagConstraints(8, 3, 4, 1, 0.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- T1TCD ----
          T1TCD.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type ", "Prix net", "Remise en %", "Coefficient" }));
          T1TCD.setComponentPopupMenu(null);
          T1TCD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T1TCD.setMaximumSize(new Dimension(137, 26));
          T1TCD.setName("T1TCD");
          T1TCD.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              T1TCDItemStateChanged(e);
            }
          });
          p_contenu.add(T1TCD, new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 10, 5), 0, 0));
          
          // ---- T1VALX ----
          T1VALX.setComponentPopupMenu(null);
          T1VALX.setHorizontalAlignment(SwingConstants.RIGHT);
          T1VALX.setMinimumSize(new Dimension(100, 28));
          T1VALX.setPreferredSize(new Dimension(100, 28));
          T1VALX.setName("T1VALX");
          p_contenu.add(T1VALX, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbT1REM ----
          lbT1REM.setComponentPopupMenu(null);
          lbT1REM.setText("Remises");
          lbT1REM.setFont(lbT1REM.getFont().deriveFont(lbT1REM.getFont().getStyle() | Font.BOLD));
          lbT1REM.setName("lbT1REM");
          p_contenu.add(lbT1REM, new GridBagConstraints(2, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- T1REM1 ----
          T1REM1.setComponentPopupMenu(null);
          T1REM1.setHorizontalAlignment(SwingConstants.RIGHT);
          T1REM1.setPreferredSize(new Dimension(50, 28));
          T1REM1.setMinimumSize(new Dimension(50, 28));
          T1REM1.setName("T1REM1");
          p_contenu.add(T1REM1, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- T1REM2 ----
          T1REM2.setComponentPopupMenu(null);
          T1REM2.setHorizontalAlignment(SwingConstants.RIGHT);
          T1REM2.setMinimumSize(new Dimension(50, 28));
          T1REM2.setPreferredSize(new Dimension(50, 28));
          T1REM2.setName("T1REM2");
          p_contenu.add(T1REM2, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- T1REM3 ----
          T1REM3.setComponentPopupMenu(null);
          T1REM3.setHorizontalAlignment(SwingConstants.RIGHT);
          T1REM3.setMinimumSize(new Dimension(50, 28));
          T1REM3.setPreferredSize(new Dimension(50, 28));
          T1REM3.setName("T1REM3");
          p_contenu.add(T1REM3, new GridBagConstraints(4, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbT1VALX ----
          lbT1VALX.setComponentPopupMenu(null);
          lbT1VALX.setText("Prix net");
          lbT1VALX.setFont(lbT1VALX.getFont().deriveFont(lbT1VALX.getFont().getStyle() | Font.BOLD));
          lbT1VALX.setName("lbT1VALX");
          p_contenu.add(lbT1VALX, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- OBJ_84 ----
          OBJ_84.setComponentPopupMenu(null);
          OBJ_84.setText("Type");
          OBJ_84.setFont(OBJ_84.getFont().deriveFont(OBJ_84.getFont().getStyle() | Font.BOLD));
          OBJ_84.setName("OBJ_84");
          p_contenu.add(OBJ_84, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbT1COE ----
          lbT1COE.setComponentPopupMenu(null);
          lbT1COE.setText("Coefficient");
          lbT1COE.setFont(lbT1COE.getFont().deriveFont(lbT1COE.getFont().getStyle() | Font.BOLD));
          lbT1COE.setName("lbT1COE");
          p_contenu.add(lbT1COE, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbWTAR1 ----
          lbWTAR1.setComponentPopupMenu(null);
          lbWTAR1.setText("Sur tarif");
          lbWTAR1.setFont(lbWTAR1.getFont().deriveFont(lbWTAR1.getFont().getStyle() | Font.BOLD));
          lbWTAR1.setName("lbWTAR1");
          p_contenu.add(lbWTAR1, new GridBagConstraints(6, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- T1COE ----
          T1COE.setComponentPopupMenu(null);
          T1COE.setHorizontalAlignment(SwingConstants.RIGHT);
          T1COE.setPreferredSize(new Dimension(66, 28));
          T1COE.setMinimumSize(new Dimension(66, 28));
          T1COE.setName("T1COE");
          p_contenu.add(T1COE, new GridBagConstraints(5, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WTAR1 ----
          WTAR1.setComponentPopupMenu(null);
          WTAR1.setPreferredSize(new Dimension(170, 28));
          WTAR1.setMinimumSize(new Dimension(170, 28));
          WTAR1.setName("WTAR1");
          p_contenu.add(WTAR1, new GridBagConstraints(6, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- T1DTDX ----
          T1DTDX.setComponentPopupMenu(null);
          T1DTDX.setMinimumSize(new Dimension(105, 28));
          T1DTDX.setMaximumSize(new Dimension(105, 28));
          T1DTDX.setPreferredSize(new Dimension(105, 28));
          T1DTDX.setName("T1DTDX");
          p_contenu.add(T1DTDX, new GridBagConstraints(8, 5, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- T1DTFX ----
          T1DTFX.setComponentPopupMenu(null);
          T1DTFX.setMinimumSize(new Dimension(105, 28));
          T1DTFX.setMaximumSize(new Dimension(105, 28));
          T1DTFX.setPreferredSize(new Dimension(105, 28));
          T1DTFX.setName("T1DTFX");
          p_contenu.add(T1DTFX, new GridBagConstraints(10, 5, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- rbT2VALX ----
          rbT2VALX.setText("Limite");
          rbT2VALX.setName("rbT2VALX");
          p_contenu.add(rbT2VALX, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- T2VALX ----
          T2VALX.setPreferredSize(new Dimension(90, 28));
          T2VALX.setMinimumSize(new Dimension(90, 28));
          T2VALX.setName("T2VALX");
          p_contenu.add(T2VALX, new GridBagConstraints(2, 6, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbT2TCD ----
          lbT2TCD.setText("Limite sur");
          lbT2TCD.setName("lbT2TCD");
          p_contenu.add(lbT2TCD, new GridBagConstraints(4, 6, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- T2TCD ----
          T2TCD.setModel(new DefaultComboBoxModel(new String[] { " ", "Montant", "Quantit\u00e9" }));
          T2TCD.setMinimumSize(new Dimension(135, 28));
          T2TCD.setPreferredSize(new Dimension(135, 28));
          T2TCD.setName("T2TCD");
          p_contenu.add(T2TCD, new GridBagConstraints(5, 6, 4, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- separator2 ----
          separator2.setText("Prix d'achat d\u00e9rog\u00e9");
          separator2.setName("separator2");
          p_contenu.add(separator2, new GridBagConstraints(0, 7, 13, 1, 0.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 10, 0), 0, 0));
          
          // ---- rbT1VALAX ----
          rbT1VALAX.setComponentPopupMenu(null);
          rbT1VALAX.setText("Prix d'achat net");
          rbT1VALAX.setFont(rbT1VALAX.getFont().deriveFont(rbT1VALAX.getFont().getStyle() | Font.BOLD));
          rbT1VALAX.setName("rbT1VALAX");
          rbT1VALAX.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              rbOptionsActionPerformed(e);
            }
          });
          p_contenu.add(rbT1VALAX, new GridBagConstraints(1, 8, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- T1VALAX ----
          T1VALAX.setComponentPopupMenu(null);
          T1VALAX.setHorizontalAlignment(SwingConstants.RIGHT);
          T1VALAX.setMinimumSize(new Dimension(100, 28));
          T1VALAX.setPreferredSize(new Dimension(100, 28));
          T1VALAX.setName("T1VALAX");
          p_contenu.add(T1VALAX, new GridBagConstraints(3, 8, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- rbT1REM4 ----
          rbT1REM4.setComponentPopupMenu(null);
          rbT1REM4.setText("Marge refactur\u00e9e");
          rbT1REM4.setFont(rbT1REM4.getFont().deriveFont(rbT1REM4.getFont().getStyle() | Font.BOLD));
          rbT1REM4.setName("rbT1REM4");
          rbT1REM4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              rbOptionsActionPerformed(e);
            }
          });
          p_contenu.add(rbT1REM4, new GridBagConstraints(1, 9, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- T1REM4 ----
          T1REM4.setComponentPopupMenu(null);
          T1REM4.setHorizontalAlignment(SwingConstants.RIGHT);
          T1REM4.setMinimumSize(new Dimension(50, 28));
          T1REM4.setPreferredSize(new Dimension(50, 28));
          T1REM4.setName("T1REM4");
          p_contenu.add(T1REM4, new GridBagConstraints(3, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- rbT1REM5 ----
          rbT1REM5.setComponentPopupMenu(null);
          rbT1REM5.setText("Remise compensatoire");
          rbT1REM5.setFont(rbT1REM5.getFont().deriveFont(rbT1REM5.getFont().getStyle() | Font.BOLD));
          rbT1REM5.setName("rbT1REM5");
          rbT1REM5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              rbOptionsActionPerformed(e);
            }
          });
          p_contenu.add(rbT1REM5, new GridBagConstraints(1, 10, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- rbWCOE5 ----
          rbWCOE5.setComponentPopupMenu(null);
          rbWCOE5.setText("coefficient");
          rbWCOE5.setFont(rbWCOE5.getFont().deriveFont(rbWCOE5.getFont().getStyle() | Font.BOLD));
          rbWCOE5.setName("rbWCOE5");
          rbWCOE5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              rbOptionsActionPerformed(e);
            }
          });
          p_contenu.add(rbWCOE5, new GridBagConstraints(1, 11, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WCOE5 ----
          WCOE5.setComponentPopupMenu(null);
          WCOE5.setHorizontalAlignment(SwingConstants.RIGHT);
          WCOE5.setPreferredSize(new Dimension(66, 28));
          WCOE5.setMinimumSize(new Dimension(66, 28));
          WCOE5.setName("WCOE5");
          p_contenu.add(WCOE5, new GridBagConstraints(3, 11, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPourcentagePV ----
          lbPourcentagePV.setText(" en % sur le prix de vente");
          lbPourcentagePV.setName("lbPourcentagePV");
          p_contenu.add(lbPourcentagePV, new GridBagConstraints(4, 9, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlRemiseCompensatoire ========
          {
            pnlRemiseCompensatoire.setName("pnlRemiseCompensatoire");
            pnlRemiseCompensatoire.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRemiseCompensatoire.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlRemiseCompensatoire.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlRemiseCompensatoire.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlRemiseCompensatoire.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- T1REM5 ----
            T1REM5.setComponentPopupMenu(null);
            T1REM5.setHorizontalAlignment(SwingConstants.RIGHT);
            T1REM5.setMinimumSize(new Dimension(50, 28));
            T1REM5.setPreferredSize(new Dimension(50, 28));
            T1REM5.setName("T1REM5");
            pnlRemiseCompensatoire.add(T1REM5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPourcentagePC ----
            lbPourcentagePC.setText(" en % sur le prix d'achat brut HT");
            lbPourcentagePC.setMaximumSize(new Dimension(200, 30));
            lbPourcentagePC.setMinimumSize(new Dimension(200, 30));
            lbPourcentagePC.setPreferredSize(new Dimension(200, 30));
            lbPourcentagePC.setName("lbPourcentagePC");
            pnlRemiseCompensatoire.add(lbPourcentagePC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- rbTF ----
            rbTF.setText("sur le tarif fournisseur");
            rbTF.setName("rbTF");
            pnlRemiseCompensatoire.add(rbTF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- rbTD ----
            rbTD.setText("sur le tarif distributeur");
            rbTD.setName("rbTD");
            pnlRemiseCompensatoire.add(rbTD, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          p_contenu.add(pnlRemiseCompensatoire, new GridBagConstraints(3, 10, 10, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(10, 10, 10, 10), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- help ----
      help.setText("Aide en ligne");
      help.setName("help");
      help.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          helpActionPerformed(e);
        }
      });
      BTD.add(help);
    }
    
    // ---- lbT1VALAX ----
    lbT1VALAX.setComponentPopupMenu(null);
    lbT1VALAX.setText("Prix d'achat d\u00e9rogatoire");
    lbT1VALAX.setFont(lbT1VALAX.getFont().deriveFont(lbT1VALAX.getFont().getStyle() | Font.BOLD));
    lbT1VALAX.setName("lbT1VALAX");
    
    // ---- buttonGroup1 ----
    buttonGroup1.add(rbT1VALAX);
    buttonGroup1.add(rbT1REM4);
    buttonGroup1.add(rbT1REM5);
    buttonGroup1.add(rbWCOE5);
    
    // ---- buttonGroup2 ----
    ButtonGroup buttonGroup2 = new ButtonGroup();
    buttonGroup2.add(rbTF);
    buttonGroup2.add(rbTD);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private JPanel p_presentation;
  private JPanel p_logo;
  private JLabel logoEtb;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_72;
  private XRiTextField T1CNV;
  private JLabel OBJ_50;
  private XRiComboBox T1TRA;
  private JLabel OBJ_49;
  private RiZoneSortie T1RAT;
  private JLabel OBJ_45;
  private RiZoneSortie T1ETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_centrage;
  private JPanel p_contenu;
  private JLabel lbA1UNV;
  private JLabel lbCDNAU;
  private RiZoneSortie CDNAU;
  private XRiTextField A1UNV;
  private SNLabelTitre separator1;
  private JLabel lbT1DTDX;
  private JLabel lbT1DTFX;
  private SNLabelTitre spPeriodes;
  private XRiComboBox T1TCD;
  private XRiTextField T1VALX;
  private JLabel lbT1REM;
  private XRiTextField T1REM1;
  private XRiTextField T1REM2;
  private XRiTextField T1REM3;
  private JLabel lbT1VALX;
  private JLabel OBJ_84;
  private JLabel lbT1COE;
  private JLabel lbWTAR1;
  private XRiTextField T1COE;
  private XRiComboBox WTAR1;
  private XRiCalendrier T1DTDX;
  private XRiCalendrier T1DTFX;
  private JLabel rbT2VALX;
  private XRiTextField T2VALX;
  private JLabel lbT2TCD;
  private XRiComboBox T2TCD;
  private SNLabelTitre separator2;
  private JRadioButton rbT1VALAX;
  private XRiTextField T1VALAX;
  private JRadioButton rbT1REM4;
  private XRiTextField T1REM4;
  private JRadioButton rbT1REM5;
  private JRadioButton rbWCOE5;
  private XRiTextField WCOE5;
  private JLabel lbPourcentagePV;
  private SNPanel pnlRemiseCompensatoire;
  private XRiTextField T1REM5;
  private JLabel lbPourcentagePC;
  private XRiRadioButton rbTF;
  private XRiRadioButton rbTD;
  private JPopupMenu BTD;
  private JMenuItem help;
  private JLabel lbT1VALAX;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
