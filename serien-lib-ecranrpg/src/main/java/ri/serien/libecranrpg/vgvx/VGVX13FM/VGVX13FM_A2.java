//$$david$$ ££16/12/10££ -> new look

package ri.serien.libecranrpg.vgvx.VGVX13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX13FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  // VGVX13FM_REST d_04 = null;
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", "WTP16", "WTP17", "WTP18", "WTP19", "WTP20", "WTP21", "WTP22", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, { "LD16", }, { "LD17", },
      { "LD18", }, { "LD19", }, { "LD20", }, { "LD21", }, { "LD22", }, };
  private int[] _WTP01_Width = { 400, };
  
  public ODialog dialog_REST = null;
  
  public VGVX13FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    H2ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H2ART@")).trim());
    H2ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H2ETB@")).trim());
    panel4.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@ @A1LB1@ @A1LB2@ @A1LB3@")).trim()));
    OBJ_127.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNLIB@")).trim());
    WAFFX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WAFFX@")).trim());
    OBJ_126.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    OBJ_96.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1CL2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_129.setVisible(lexique.isPresent("WAFFX"));
    WAFFX.setVisible(lexique.isPresent("WAFFX"));
    
    OBJ_97.setVisible(lexique.HostFieldGetData("FLIPST").equalsIgnoreCase("2"));
    OBJ_100.setVisible(lexique.HostFieldGetData("FLIPST").equalsIgnoreCase("3"));
    OBJ_98.setVisible(lexique.HostFieldGetData("FLIPST").equalsIgnoreCase("3"));
    OBJ_99.setVisible(lexique.HostFieldGetData("FLIPST").equalsIgnoreCase("4"));
    OBJ_77.setVisible(WASB.isVisible());
    
    // double quantité
    if (lexique.isTrue("N20")) {
      OBJ_75.setText("En unité de stock");
    }
    else {
      OBJ_75.setText("Stock en double quantité");
    }
    
    riSousMenu12.setEnabled(lexique.HostFieldGetData("A1TSP").equalsIgnoreCase("Q"));
    
    // restrictions vendeur
    if (lexique.HostFieldGetData("INDF13").equalsIgnoreCase("")) {
      OBJ_78.setText("Avec restrictions magasin");
    }
    else {
      OBJ_78.setText("Sans restrictions magasin");
    }
    
    // Gestion des lots
    menuItem1.setVisible(lexique.isTrue("96"));
    
    // POP-UP ou ECRAN PLEIN +++++
    transfomerEnPopUp(lexique.HostFieldGetData("U13L1").equalsIgnoreCase("1"));
    
    // TODO Icones
    l_plus.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    l_moins.setIcon(lexique.chargerImage("images/moins_petit.png", true));
    l_egal.setIcon(lexique.chargerImage("images/egal_petit.png", true));
    p_bpresentation.setCodeEtablissement(H2ETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(H2ETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("H1MAG", 0, H1MAG.getText());
  }
  
  public void transfomerEnPopUp(boolean isPop) {
    if (isPop) {
      this.setPreferredSize(new Dimension(890, 600));
      setTitle("POSITION ARTICLE");
    }
    else {
      this.setPreferredSize(new Dimension(1000, 600));
      setTitle("");
    }
    // Voir vraiment l'utilité de la chose
    p_contenu.setBackground(SNCharteGraphique.COULEUR_FOND);
    
    p_nord.setVisible(!isPop);
    OBJ_96.setVisible(!isPop);
    // label1.setVisible(isPop);
    OBJ_125.setVisible(!isPop);
    OBJ_126.setVisible(!isPop);
    OBJ_127.setVisible(!isPop);
    setDialog(isPop);
    validate();
    repaint();
    // setModal(isPop);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    H2MAG.setText("**");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17", false);
  }
  
  private void riSousMenu_bt21ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    // WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "7", "Enter");
    WTP01.setValeurTop("7");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "8", "Enter");
    WTP01.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "H", "Enter");
    WTP01.setValeurTop("H");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "T", "Enter");
    WTP01.setValeurTop("T");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "R", "Enter");
    WTP01.setValeurTop("R");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "ENTER", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void riSousMenu_bt23ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void riSousMenu_bt24ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    if (dialog_REST == null) {
      dialog_REST = new ODialog((Window) getTopLevelAncestor(), new VGVX13FM_REST(this));
    }
    dialog_REST.affichePopupPerso();
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "6", "Enter");
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_74 = new JLabel();
    OBJ_77 = new JLabel();
    H2ART = new RiZoneSortie();
    H2ETB = new RiZoneSortie();
    OBJ_73 = new JLabel();
    OBJ_76 = new JLabel();
    H2MAG = new XRiTextField();
    WASB = new XRiTextField();
    p_tete_droite = new JPanel();
    OBJ_75 = new JLabel();
    label2 = new JLabel();
    riBoutonDetail1 = new SNBoutonDetail();
    OBJ_78 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu24 = new RiSousMenu();
    riSousMenu_bt24 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu4 = new RiSousMenu();
    riSousMenu_bt4 = new RiSousMenu_bt();
    riSousMenu5 = new RiSousMenu();
    riSousMenu_bt5 = new RiSousMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    riSousMenu23 = new RiSousMenu();
    riSousMenu_bt23 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    OBJ_97 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_100 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel2 = new JPanel();
    OBJ_120 = new JLabel();
    OBJ_127 = new RiZoneSortie();
    WPOSX = new XRiTextField();
    WSTKX = new XRiTextField();
    WATTX = new XRiTextField();
    WRESX = new XRiTextField();
    WAFFX = new RiZoneSortie();
    OBJ_129 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_126 = new RiZoneSortie();
    l_plus = new JLabel();
    l_moins = new JLabel();
    l_egal = new JLabel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    menuItem1 = new JMenuItem();
    OBJ_36 = new JMenuItem();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();
    OBJ_96 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Position article");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_74 ----
          OBJ_74.setText("Article");
          OBJ_74.setName("OBJ_74");
          p_tete_gauche.add(OBJ_74);
          OBJ_74.setBounds(155, 6, 48, 18);

          //---- OBJ_77 ----
          OBJ_77.setText("Substitution");
          OBJ_77.setName("OBJ_77");
          p_tete_gauche.add(OBJ_77);
          OBJ_77.setBounds(540, 6, 75, 18);

          //---- H2ART ----
          H2ART.setToolTipText("Code article");
          H2ART.setComponentPopupMenu(BTD);
          H2ART.setText("@H2ART@");
          H2ART.setOpaque(false);
          H2ART.setName("H2ART");
          p_tete_gauche.add(H2ART);
          H2ART.setBounds(200, 3, 210, H2ART.getPreferredSize().height);

          //---- H2ETB ----
          H2ETB.setToolTipText("Code \u00e9tablissement");
          H2ETB.setComponentPopupMenu(BTD);
          H2ETB.setText("@H2ETB@");
          H2ETB.setOpaque(false);
          H2ETB.setName("H2ETB");
          p_tete_gauche.add(H2ETB);
          H2ETB.setBounds(95, 3, 40, H2ETB.getPreferredSize().height);

          //---- OBJ_73 ----
          OBJ_73.setText("Etablissement");
          OBJ_73.setName("OBJ_73");
          p_tete_gauche.add(OBJ_73);
          OBJ_73.setBounds(5, 6, 90, 18);

          //---- OBJ_76 ----
          OBJ_76.setText("Magasin");
          OBJ_76.setName("OBJ_76");
          p_tete_gauche.add(OBJ_76);
          OBJ_76.setBounds(420, 6, 60, 18);

          //---- H2MAG ----
          H2MAG.setToolTipText("Magasin");
          H2MAG.setComponentPopupMenu(BTD);
          H2MAG.setName("H2MAG");
          p_tete_gauche.add(H2MAG);
          H2MAG.setBounds(480, 1, 34, H2MAG.getPreferredSize().height);

          //---- WASB ----
          WASB.setToolTipText("Type");
          WASB.setComponentPopupMenu(BTD);
          WASB.setName("WASB");
          p_tete_gauche.add(WASB);
          WASB.setBounds(620, 1, 210, WASB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_75 ----
          OBJ_75.setText("en double quantit\u00e9");
          OBJ_75.setFont(OBJ_75.getFont().deriveFont(OBJ_75.getFont().getStyle() | Font.BOLD));
          OBJ_75.setName("OBJ_75");
          p_tete_droite.add(OBJ_75);

          //---- label2 ----
          label2.setText("/");
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          p_tete_droite.add(label2);

          //---- riBoutonDetail1 ----
          riBoutonDetail1.setToolTipText("Saisir les crit\u00e8res de restriction");
          riBoutonDetail1.setName("riBoutonDetail1");
          riBoutonDetail1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetail1ActionPerformed(e);
            }
          });
          p_tete_droite.add(riBoutonDetail1);

          //---- OBJ_78 ----
          OBJ_78.setText("restrictions");
          OBJ_78.setFont(OBJ_78.getFont().deriveFont(OBJ_78.getFont().getStyle() | Font.BOLD));
          OBJ_78.setName("OBJ_78");
          p_tete_droite.add(OBJ_78);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Recherche article");
              riSousMenu_bt14.setToolTipText("Recherche article");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Recherche multi-crit\u00e8res");
              riSousMenu_bt7.setToolTipText("Recherche multi-crit\u00e8res");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Mouvements par p\u00e9riode");
              riSousMenu_bt6.setToolTipText("Analyse des mouvements sur une p\u00e9riode");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Histo. conso.regroup\u00e9e");
              riSousMenu_bt8.setToolTipText("Historique consommation regroup\u00e9e");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Autres \u00e9tablissements");
              riSousMenu_bt9.setToolTipText("Visualisation stocks sur d'autres \u00e9tablissements");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Autres vues");
              riSousMenu_bt10.setToolTipText("Autres vues");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Stock en double quantit\u00e9");
              riSousMenu_bt12.setToolTipText("R\u00e9capitulatif stock en double quantit\u00e9");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("D\u00e9tail tous magasins");
              riSousMenu_bt13.setToolTipText("D\u00e9tail tous magasins");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);

            //======== riSousMenu24 ========
            {
              riSousMenu24.setName("riSousMenu24");

              //---- riSousMenu_bt24 ----
              riSousMenu_bt24.setText("Conditionnement/m\u00e8tres L");
              riSousMenu_bt24.setToolTipText("On/Off Qt\u00e9s en U,Stock/ U. conditionnement/M\u00e8tres lineaires");
              riSousMenu_bt24.setName("riSousMenu_bt24");
              riSousMenu_bt24.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt24ActionPerformed(e);
                }
              });
              riSousMenu24.add(riSousMenu_bt24);
            }
            menus_haut.add(riSousMenu24);

            //======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");

              //---- riMenu_bt4 ----
              riMenu_bt4.setText("Affichage");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);

            //======== riSousMenu4 ========
            {
              riSousMenu4.setName("riSousMenu4");

              //---- riSousMenu_bt4 ----
              riSousMenu_bt4.setText("Attendu fournisseurs");
              riSousMenu_bt4.setToolTipText("Affichage du d\u00e9tail des  articles attendus de la part des fournisseurs");
              riSousMenu_bt4.setName("riSousMenu_bt4");
              riSousMenu_bt4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt4ActionPerformed(e);
                }
              });
              riSousMenu4.add(riSousMenu_bt4);
            }
            menus_haut.add(riSousMenu4);

            //======== riSousMenu5 ========
            {
              riSousMenu5.setName("riSousMenu5");

              //---- riSousMenu_bt5 ----
              riSousMenu_bt5.setText("Command\u00e9 clients");
              riSousMenu_bt5.setToolTipText("Affichage du d\u00e9tail des articles command\u00e9s par les clients");
              riSousMenu_bt5.setSelectedIcon(null);
              riSousMenu_bt5.setName("riSousMenu_bt5");
              riSousMenu_bt5.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt5ActionPerformed(e);
                }
              });
              riSousMenu5.add(riSousMenu_bt5);
            }
            menus_haut.add(riSousMenu5);

            //======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");

              //---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("Attendu + command\u00e9");
              riSousMenu_bt3.setToolTipText("Affichage d\u00e9tail Attendu et Command\u00e9");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt3ActionPerformed(e);
                }
              });
              riSousMenu3.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu3);

            //======== riSousMenu20 ========
            {
              riSousMenu20.setName("riSousMenu20");

              //---- riSousMenu_bt20 ----
              riSousMenu_bt20.setText("Fiche article");
              riSousMenu_bt20.setToolTipText("Affichage de la fiche Article");
              riSousMenu_bt20.setName("riSousMenu_bt20");
              riSousMenu_bt20.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt20ActionPerformed(e);
                }
              });
              riSousMenu20.add(riSousMenu_bt20);
            }
            menus_haut.add(riSousMenu20);

            //======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");

              //---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Historique achats - PUMP");
              riSousMenu_bt2.setToolTipText("Affichage Historique Achats ou PUMP");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt2ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);

            //======== riSousMenu23 ========
            {
              riSousMenu23.setName("riSousMenu23");

              //---- riSousMenu_bt23 ----
              riSousMenu_bt23.setText("Autres unit\u00e9s de stocks");
              riSousMenu_bt23.setToolTipText("Affichage autres unit\u00e9s de stocks");
              riSousMenu_bt23.setName("riSousMenu_bt23");
              riSousMenu_bt23.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt23ActionPerformed(e);
                }
              });
              riSousMenu23.add(riSousMenu_bt23);
            }
            menus_haut.add(riSousMenu23);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(715, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(715, 600));
          p_contenu.setName("p_contenu");

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder("@A1LIB@ @A1LB1@ @A1LB2@ @A1LB3@"));
            panel4.setOpaque(false);
            panel4.setMinimumSize(new Dimension(680, 450));
            panel4.setPreferredSize(new Dimension(680, 450));
            panel4.setName("panel4");
            panel4.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel4.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(40, 50, 579, 380);

            //---- OBJ_97 ----
            OBJ_97.setText("En unite de conditionnement");
            OBJ_97.setComponentPopupMenu(BTD);
            OBJ_97.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_97.setName("OBJ_97");
            panel4.add(OBJ_97);
            OBJ_97.setBounds(475, 25, 170, 25);

            //---- OBJ_99 ----
            OBJ_99.setText("En nombre de palette");
            OBJ_99.setComponentPopupMenu(BTD);
            OBJ_99.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_99.setName("OBJ_99");
            panel4.add(OBJ_99);
            OBJ_99.setBounds(514, 25, 131, 25);

            //---- OBJ_98 ----
            OBJ_98.setText("En m\u00e8tres lineaires");
            OBJ_98.setComponentPopupMenu(BTD);
            OBJ_98.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_98.setName("OBJ_98");
            panel4.add(OBJ_98);
            OBJ_98.setBounds(528, 25, 117, 25);

            //---- OBJ_100 ----
            OBJ_100.setText("En m\u00e8tres lineaires");
            OBJ_100.setComponentPopupMenu(BTD);
            OBJ_100.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_100.setName("OBJ_100");
            panel4.add(OBJ_100);
            OBJ_100.setBounds(528, 25, 117, 25);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel4.add(BT_PGUP);
            BT_PGUP.setBounds(625, 50, 25, 155);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel4.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(625, 275, 25, 155);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_120 ----
              OBJ_120.setText("Total article");
              OBJ_120.setName("OBJ_120");
              panel2.add(OBJ_120);
              OBJ_120.setBounds(15, 10, 100, 20);

              //---- OBJ_127 ----
              OBJ_127.setText("@UNLIB@");
              OBJ_127.setName("OBJ_127");
              panel2.add(OBJ_127);
              OBJ_127.setBounds(460, 80, 130, OBJ_127.getPreferredSize().height);

              //---- WPOSX ----
              WPOSX.setComponentPopupMenu(BTD);
              WPOSX.setHorizontalAlignment(SwingConstants.RIGHT);
              WPOSX.setName("WPOSX");
              panel2.add(WPOSX);
              WPOSX.setBounds(490, 5, 100, WPOSX.getPreferredSize().height);

              //---- WSTKX ----
              WSTKX.setHorizontalAlignment(SwingConstants.RIGHT);
              WSTKX.setName("WSTKX");
              panel2.add(WSTKX);
              WSTKX.setBounds(175, 5, 87, WSTKX.getPreferredSize().height);

              //---- WATTX ----
              WATTX.setHorizontalAlignment(SwingConstants.RIGHT);
              WATTX.setName("WATTX");
              panel2.add(WATTX);
              WATTX.setBounds(280, 5, 87, WATTX.getPreferredSize().height);

              //---- WRESX ----
              WRESX.setHorizontalAlignment(SwingConstants.RIGHT);
              WRESX.setName("WRESX");
              panel2.add(WRESX);
              WRESX.setBounds(385, 5, 87, WRESX.getPreferredSize().height);

              //---- WAFFX ----
              WAFFX.setText("@WAFFX@");
              WAFFX.setHorizontalAlignment(SwingConstants.RIGHT);
              WAFFX.setName("WAFFX");
              panel2.add(WAFFX);
              WAFFX.setBounds(490, 40, 100, WAFFX.getPreferredSize().height);

              //---- OBJ_129 ----
              OBJ_129.setText("dont quantit\u00e9 r\u00e9serv\u00e9e");
              OBJ_129.setHorizontalTextPosition(SwingConstants.LEADING);
              OBJ_129.setName("OBJ_129");
              panel2.add(OBJ_129);
              OBJ_129.setBounds(360, 42, 128, 20);

              //---- OBJ_125 ----
              OBJ_125.setText("Unit\u00e9");
              OBJ_125.setName("OBJ_125");
              panel2.add(OBJ_125);
              OBJ_125.setBounds(360, 82, 65, 20);

              //---- OBJ_126 ----
              OBJ_126.setText("@A1UNS@");
              OBJ_126.setName("OBJ_126");
              panel2.add(OBJ_126);
              OBJ_126.setBounds(425, 80, 30, OBJ_126.getPreferredSize().height);

              //---- l_plus ----
              l_plus.setName("l_plus");
              panel2.add(l_plus);
              l_plus.setBounds(263, 5, 16, 28);

              //---- l_moins ----
              l_moins.setName("l_moins");
              panel2.add(l_moins);
              l_moins.setBounds(368, 5, 16, 28);

              //---- l_egal ----
              l_egal.setName("l_egal");
              panel2.add(l_egal);
              l_egal.setBounds(473, 5, 16, 28);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel4.add(panel2);
            panel2.setBounds(25, 440, 630, 115);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel4, GroupLayout.DEFAULT_SIZE, 689, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel4, GroupLayout.DEFAULT_SIZE, 572, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_26 ----
      OBJ_26.setText("Adresse de stockage");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);

      //---- OBJ_27 ----
      OBJ_27.setText("D\u00e9tail de livraison");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);

      //---- OBJ_28 ----
      OBJ_28.setText("Historique de consommation");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed(e);
        }
      });
      BTD.add(OBJ_28);

      //---- OBJ_29 ----
      OBJ_29.setText("Gestion syst\u00e8mes de variables");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD.add(OBJ_29);

      //---- OBJ_30 ----
      OBJ_30.setText("Recalcul compteurs");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed(e);
        }
      });
      BTD.add(OBJ_30);

      //---- menuItem1 ----
      menuItem1.setText("Affichage des lots");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD.add(menuItem1);

      //---- OBJ_36 ----
      OBJ_36.setText("Aide en ligne");
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      BTD.add(OBJ_36);
    }

    //======== riSousMenu18 ========
    {
      riSousMenu18.setName("riSousMenu18");

      //---- riSousMenu_bt18 ----
      riSousMenu_bt18.setText("Affichage des lots");
      riSousMenu_bt18.setToolTipText("Affichage des lots");
      riSousMenu_bt18.setName("riSousMenu_bt18");
      riSousMenu_bt18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt18ActionPerformed(e);
        }
      });
      riSousMenu18.add(riSousMenu_bt18);
    }

    //======== riSousMenu11 ========
    {
      riSousMenu11.setName("riSousMenu11");

      //---- riSousMenu_bt11 ----
      riSousMenu_bt11.setText("Stock \u00e9diteur");
      riSousMenu_bt11.setToolTipText("Interrogation Stock Editeur");
      riSousMenu_bt11.setName("riSousMenu_bt11");
      riSousMenu_bt11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt11ActionPerformed(e);
        }
      });
      riSousMenu11.add(riSousMenu_bt11);
    }

    //======== riSousMenu21 ========
    {
      riSousMenu21.setName("riSousMenu21");

      //---- riSousMenu_bt21 ----
      riSousMenu_bt21.setText("Mouvements inventaires");
      riSousMenu_bt21.setToolTipText("Affichage R\u00e9capitulatif Mouvements entre Inventaires");
      riSousMenu_bt21.setName("riSousMenu_bt21");
      riSousMenu_bt21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt21ActionPerformed(e);
        }
      });
      riSousMenu21.add(riSousMenu_bt21);
    }

    //---- OBJ_96 ----
    OBJ_96.setText("@A1CL2@");
    OBJ_96.setName("OBJ_96");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_74;
  private JLabel OBJ_77;
  private RiZoneSortie H2ART;
  private RiZoneSortie H2ETB;
  private JLabel OBJ_73;
  private JLabel OBJ_76;
  private XRiTextField H2MAG;
  private XRiTextField WASB;
  private JPanel p_tete_droite;
  private JLabel OBJ_75;
  private JLabel label2;
  private SNBoutonDetail riBoutonDetail1;
  private JLabel OBJ_78;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu24;
  private RiSousMenu_bt riSousMenu_bt24;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu4;
  private RiSousMenu_bt riSousMenu_bt4;
  private RiSousMenu riSousMenu5;
  private RiSousMenu_bt riSousMenu_bt5;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt3;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private RiSousMenu riSousMenu23;
  private RiSousMenu_bt riSousMenu_bt23;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel4;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JLabel OBJ_97;
  private JLabel OBJ_99;
  private JLabel OBJ_98;
  private JLabel OBJ_100;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel2;
  private JLabel OBJ_120;
  private RiZoneSortie OBJ_127;
  private XRiTextField WPOSX;
  private XRiTextField WSTKX;
  private XRiTextField WATTX;
  private XRiTextField WRESX;
  private RiZoneSortie WAFFX;
  private JLabel OBJ_129;
  private JLabel OBJ_125;
  private RiZoneSortie OBJ_126;
  private JLabel l_plus;
  private JLabel l_moins;
  private JLabel l_egal;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_28;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_30;
  private JMenuItem menuItem1;
  private JMenuItem OBJ_36;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  private JLabel OBJ_96;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
