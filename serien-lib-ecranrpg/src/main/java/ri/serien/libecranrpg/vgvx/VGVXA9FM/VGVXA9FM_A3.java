
package ri.serien.libecranrpg.vgvx.VGVXA9FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGVXA9FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] ARGMVT_Value = { "E", "S", "T", };
  
  public VGVXA9FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX1.setValeurs("1", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX3.setValeurs("3", "RB");
    ARGMVT.setValeurs(ARGMVT_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FICHE PALETTE"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    ARGMVT = new XRiComboBox();
    TIDX2 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    ARGART = new XRiTextField();
    TIDX1 = new XRiRadioButton();
    OBJ_40 = new JLabel();
    OBJ_43 = new JLabel();
    ARG1 = new XRiTextField();
    ARG34 = new XRiTextField();
    ARG21 = new XRiTextField();
    ARG22 = new XRiTextField();
    ARG23 = new XRiTextField();
    ARG24 = new XRiTextField();
    ARG31 = new XRiTextField();
    ARG32 = new XRiTextField();
    ARG33 = new XRiTextField();
    ARG20 = new XRiTextField();
    ARG30 = new XRiTextField();
    OBJ_37 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    RB = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(700, 310));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("On/Off missions en cours");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- ARGMVT ----
          ARGMVT.setModel(new DefaultComboBoxModel(new String[] {
            "   ",
            "R\u00e9ception",
            "Vente",
            "R\u00e9approvisionnement"
          }));
          ARGMVT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARGMVT.setName("ARGMVT");
          panel1.add(ARGMVT);
          ARGMVT.setBounds(240, 190, 139, ARGMVT.getPreferredSize().height);

          //---- TIDX2 ----
          TIDX2.setText("R\u00e9f\u00e9rence adresse de d\u00e9but");
          TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2.setName("TIDX2");
          panel1.add(TIDX2);
          TIDX2.setBounds(30, 100, 195, 28);

          //---- TIDX3 ----
          TIDX3.setText("R\u00e9f\u00e9rence adresse de fin");
          TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX3.setName("TIDX3");
          panel1.add(TIDX3);
          TIDX3.setBounds(30, 145, 195, 28);

          //---- ARGART ----
          ARGART.setComponentPopupMenu(BTD);
          ARGART.setName("ARGART");
          panel1.add(ARGART);
          ARGART.setBounds(240, 233, 210, ARGART.getPreferredSize().height);

          //---- TIDX1 ----
          TIDX1.setText("Num\u00e9ro de palette");
          TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX1.setName("TIDX1");
          panel1.add(TIDX1);
          TIDX1.setBounds(30, 55, 150, 28);

          //---- OBJ_40 ----
          OBJ_40.setText("Type de mouvement");
          OBJ_40.setName("OBJ_40");
          panel1.add(OBJ_40);
          OBJ_40.setBounds(35, 189, 127, 28);

          //---- OBJ_43 ----
          OBJ_43.setText("R\u00e9f\u00e9rence article");
          OBJ_43.setName("OBJ_43");
          panel1.add(OBJ_43);
          OBJ_43.setBounds(35, 233, 123, 28);

          //---- ARG1 ----
          ARG1.setComponentPopupMenu(BTD);
          ARG1.setName("ARG1");
          panel1.add(ARG1);
          ARG1.setBounds(240, 55, 74, ARG1.getPreferredSize().height);

          //---- ARG34 ----
          ARG34.setComponentPopupMenu(BTD);
          ARG34.setName("ARG34");
          panel1.add(ARG34);
          ARG34.setBounds(414, 145, 40, ARG34.getPreferredSize().height);

          //---- ARG21 ----
          ARG21.setComponentPopupMenu(BTD);
          ARG21.setName("ARG21");
          panel1.add(ARG21);
          ARG21.setBounds(275, 100, 40, ARG21.getPreferredSize().height);

          //---- ARG22 ----
          ARG22.setComponentPopupMenu(BTD);
          ARG22.setName("ARG22");
          panel1.add(ARG22);
          ARG22.setBounds(320, 100, 40, ARG22.getPreferredSize().height);

          //---- ARG23 ----
          ARG23.setComponentPopupMenu(BTD);
          ARG23.setName("ARG23");
          panel1.add(ARG23);
          ARG23.setBounds(370, 100, 40, ARG23.getPreferredSize().height);

          //---- ARG24 ----
          ARG24.setComponentPopupMenu(BTD);
          ARG24.setName("ARG24");
          panel1.add(ARG24);
          ARG24.setBounds(414, 100, 40, ARG24.getPreferredSize().height);

          //---- ARG31 ----
          ARG31.setComponentPopupMenu(BTD);
          ARG31.setName("ARG31");
          panel1.add(ARG31);
          ARG31.setBounds(275, 145, 40, ARG31.getPreferredSize().height);

          //---- ARG32 ----
          ARG32.setComponentPopupMenu(BTD);
          ARG32.setName("ARG32");
          panel1.add(ARG32);
          ARG32.setBounds(320, 145, 40, ARG32.getPreferredSize().height);

          //---- ARG33 ----
          ARG33.setComponentPopupMenu(BTD);
          ARG33.setName("ARG33");
          panel1.add(ARG33);
          ARG33.setBounds(370, 145, 40, ARG33.getPreferredSize().height);

          //---- ARG20 ----
          ARG20.setComponentPopupMenu(BTD);
          ARG20.setName("ARG20");
          panel1.add(ARG20);
          ARG20.setBounds(240, 100, 30, ARG20.getPreferredSize().height);

          //---- ARG30 ----
          ARG30.setComponentPopupMenu(BTD);
          ARG30.setName("ARG30");
          panel1.add(ARG30);
          ARG30.setBounds(240, 145, 30, ARG30.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("Mission en cours");
          OBJ_37.setFont(OBJ_37.getFont().deriveFont(OBJ_37.getFont().getStyle() | Font.BOLD, OBJ_37.getFont().getSize() + 3f));
          OBJ_37.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(365, 25, 131, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 510, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- RB ----
    RB.add(TIDX2);
    RB.add(TIDX3);
    RB.add(TIDX1);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiComboBox ARGMVT;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX3;
  private XRiTextField ARGART;
  private XRiRadioButton TIDX1;
  private JLabel OBJ_40;
  private JLabel OBJ_43;
  private XRiTextField ARG1;
  private XRiTextField ARG34;
  private XRiTextField ARG21;
  private XRiTextField ARG22;
  private XRiTextField ARG23;
  private XRiTextField ARG24;
  private XRiTextField ARG31;
  private XRiTextField ARG32;
  private XRiTextField ARG33;
  private XRiTextField ARG20;
  private XRiTextField ARG30;
  private JLabel OBJ_37;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  private ButtonGroup RB;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
