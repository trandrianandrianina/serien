
package ri.serien.libecranrpg.vgvx.VGVXCCFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXCCFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] CCPERR_Value = { "", "J", "S", "D", "Q", "M", };
  private String[] CCPERF_Value = { "", "J", "S", "D", "Q", "M", };
  
  public VGVXCCFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    CCPERR.setValeurs(CCPERR_Value, null);
    CCPERF.setValeurs(CCPERF_Value, null);
    CCCDES.setValeursSelection("OUI", "NON");
    CCCLIV.setValeursSelection("OUI", "NON");
    CCTYPT.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT1@")).trim());
    OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT2@")).trim());
    OBJ_65.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT3@")).trim());
    OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT4@")).trim());
    OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    IDEN.setRightDecoration(bt_identification);
    LIVR.setRightDecoration(bt_livraison);
    FACT.setRightDecoration(bt_facturation);
    COMP.setRightDecoration(bt_comptabilite);
    DATE.setRightDecoration(bt_dates);
    STAT.setRightDecoration(bt_statistiques);
    VPC.setRightDecoration(bt_vpc);
    HISTO.setRightDecoration(bt_historique);
    
    
    OBJ_69.setVisible(lexique.isPresent("TCI7"));
    FLD001.setVisible(lexique.isPresent("FLD001"));
    CVNBEF.setVisible(lexique.isPresent("CVNBEF"));
    CVNBED.setVisible(lexique.isPresent("CVNBED"));
    CCEXEF.setVisible(lexique.isPresent("CCEXEF"));
    CCEXED.setVisible(lexique.isPresent("CCEXED"));
    CCTYPF.setVisible(lexique.isPresent("CCTYPF"));
    CVZP5.setVisible(lexique.isPresent("CVZP5"));
    CVZP4.setVisible(lexique.isPresent("CVZP4"));
    CVZP3.setVisible(lexique.isPresent("CVZP3"));
    CVZP2.setVisible(lexique.isPresent("CVZP2"));
    CVZP1.setVisible(lexique.isPresent("CVZP1"));
    CCMOIF.setVisible(lexique.isPresent("CCMOIF"));
    CCMOID.setVisible(lexique.isPresent("CCMOID"));
    CCREMF.setVisible(lexique.isPresent("CCREMF"));
    CCREMD.setVisible(lexique.isPresent("CCREMD"));
    CCMEX5.setEnabled(lexique.isPresent("CCMEX5"));
    CCMEX4.setEnabled(lexique.isPresent("CCMEX4"));
    CCMEX3.setEnabled(lexique.isPresent("CCMEX3"));
    CCMEX2.setEnabled(lexique.isPresent("CCMEX2"));
    CCMEX1.setEnabled(lexique.isPresent("CCMEX1"));
    CCREP5.setEnabled(lexique.isPresent("CCREP5"));
    CCREP4.setEnabled(lexique.isPresent("CCREP4"));
    CCREP3.setEnabled(lexique.isPresent("CCREP3"));
    CCREP2.setEnabled(lexique.isPresent("CCREP2"));
    CCREP1.setEnabled(lexique.isPresent("CCREP1"));
    OBJ_67.setVisible(lexique.isPresent("TIT5"));
    OBJ_66.setVisible(lexique.isPresent("TIT4"));
    OBJ_65.setVisible(lexique.isPresent("TIT3"));
    OBJ_64.setVisible(lexique.isPresent("TIT2"));
    OBJ_63.setVisible(lexique.isPresent("TIT1"));
    CCSAN1.setVisible(lexique.isPresent("CCSAN1"));
    CCDEV3.setVisible(lexique.isPresent("CCDEV3"));
    CCDEV2.setVisible(lexique.isPresent("CCDEV2"));
    CCDEV1.setVisible(lexique.isPresent("CCDEV1"));
    CCCAT5.setEnabled(lexique.isPresent("CCCAT5"));
    CCCAT4.setEnabled(lexique.isPresent("CCCAT4"));
    CCCAT3.setEnabled(lexique.isPresent("CCCAT3"));
    CCCAT2.setEnabled(lexique.isPresent("CCCAT2"));
    CCCAT1.setEnabled(lexique.isPresent("CCCAT1"));
    CCZGE5.setEnabled(lexique.isPresent("CCZGE5"));
    CCZGE4.setEnabled(lexique.isPresent("CCZGE4"));
    CCZGE3.setEnabled(lexique.isPresent("CCZGE3"));
    CCZGE2.setEnabled(lexique.isPresent("CCZGE2"));
    CCZGE1.setEnabled(lexique.isPresent("CCZGE1"));
    CCCDPF.setEnabled(lexique.isPresent("CCCDPF"));
    CCCDPD.setEnabled(lexique.isPresent("CCCDPD"));
    OBJ_68.setVisible(lexique.isPresent("TCI7"));
    // CCVEFX.setVisible( lexique.isPresent("CCVEFX"));
    // CCVEDX.setVisible( lexique.isPresent("CCVEDX"));
    // CCVIFX.setVisible( lexique.isPresent("CCVIFX"));
    // CCVIDX.setVisible( lexique.isPresent("CCVIDX"));
    CCPLEF.setVisible(lexique.isPresent("CCPLEF"));
    CCPLED.setVisible(lexique.isPresent("CCPLED"));
    // CCCDES.setVisible( lexique.isPresent("CCCDES"));
    // CCCDES.setSelected(lexique.HostFieldGetData("CCCDES").equalsIgnoreCase("OUI"));
    // CCCLIV.setVisible( lexique.isPresent("CCCLIV"));
    // CCCLIV.setSelected(lexique.HostFieldGetData("CCCLIV").equalsIgnoreCase("OUI"));
    CCCAFF.setVisible(lexique.isPresent("CCCAFF"));
    CCCAFD.setVisible(lexique.isPresent("CCCAFD"));
    // CCTYPT.setVisible( lexique.isPresent("CCTYPT"));
    // CCTYPT.setSelected(lexique.HostFieldGetData("CCTYPT").equalsIgnoreCase("1"));
    OBJ_70.setVisible(lexique.isPresent("TCI8"));
    // CCPERR.setEnabled( lexique.isPresent("CCPERR"));
    // CCPERF.setEnabled( lexique.isPresent("CCPERF"));
    CCOBS.setEnabled(lexique.isPresent("CCOBS"));
    CCOBJ.setEnabled(lexique.isPresent("CCOBJ"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Critères de sélection"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (CCCDES.isSelected())
    // lexique.HostFieldPutData("CCCDES", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CCCDES", 0, "NON");
    // if (CCCLIV.isSelected())
    // lexique.HostFieldPutData("CCCLIV", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CCCLIV", 0, "NON");
    // if (CCTYPT.isSelected())
    // lexique.HostFieldPutData("CCTYPT", 0, "1");
    // else
    // lexique.HostFieldPutData("CCTYPT", 0, " ");
    // lexique.HostFieldPutData("CCPERR", 0, CCPERR_Value[CCPERR.getSelectedIndex()]);
    // lexique.HostFieldPutData("CCPERF", 0, CCPERF_Value[CCPERF.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_identificationActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI1").trim().equals("")) {
      lexique.HostFieldPutData("TCI1", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI1"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_livraisonActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI2").trim().equals("")) {
      lexique.HostFieldPutData("TCI2", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI2"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_facturationActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI3").trim().equals("")) {
      lexique.HostFieldPutData("TCI3", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI3"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_comptabiliteActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI4").trim().equals("")) {
      lexique.HostFieldPutData("TCI4", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI4"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_datesActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI5").trim().equals("")) {
      lexique.HostFieldPutData("TCI5", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI5"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_statistiquesActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI6").trim().equals("")) {
      lexique.HostFieldPutData("TCI6", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI6"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_vpcActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI7").trim().equals("")) {
      lexique.HostFieldPutData("TCI7", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI7"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_historiqueActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI8").trim().equals("")) {
      lexique.HostFieldPutData("TCI8", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI8"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_154 = new JLabel();
    INDNUM = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_ann = new RiSousMenu();
    riSousMenu_bt_ann = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_75 = new JLabel();
    CCOBJ = new XRiTextField();
    OBJ_130 = new JLabel();
    CCOBS = new XRiTextField();
    IDEN = new JXTitledPanel();
    OBJ_43 = new JLabel();
    CCCDPD = new XRiTextField();
    OBJ_44 = new JLabel();
    CCCDPF = new XRiTextField();
    OBJ_45 = new JLabel();
    CCCAT1 = new XRiTextField();
    CCCAT2 = new XRiTextField();
    CCCAT3 = new XRiTextField();
    CCCAT4 = new XRiTextField();
    CCCAT5 = new XRiTextField();
    OBJ_46 = new JLabel();
    CCREP1 = new XRiTextField();
    CCREP2 = new XRiTextField();
    CCREP3 = new XRiTextField();
    CCREP4 = new XRiTextField();
    CCREP5 = new XRiTextField();
    FACT = new JXTitledPanel();
    OBJ_49 = new JLabel();
    CCTYPF = new XRiTextField();
    CCTYPT = new XRiCheckBox();
    OBJ_171 = new JLabel();
    CCPERF = new XRiComboBox();
    OBJ_172 = new JLabel();
    CCPERR = new XRiComboBox();
    OBJ_50 = new JLabel();
    CCREMD = new XRiTextField();
    OBJ_51 = new JLabel();
    CCREMF = new XRiTextField();
    OBJ_52 = new JLabel();
    CCDEV1 = new XRiTextField();
    CCDEV2 = new XRiTextField();
    CCDEV3 = new XRiTextField();
    DATE = new JXTitledPanel();
    OBJ_58 = new JLabel();
    CCVIDX = new XRiCalendrier();
    CCVIFX = new XRiCalendrier();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    CCVEDX = new XRiCalendrier();
    CCVEFX = new XRiCalendrier();
    OBJ_61 = new JLabel();
    VPC = new JXTitledPanel();
    OBJ_62 = new JLabel();
    OBJ_63 = new JLabel();
    CVZP1 = new XRiTextField();
    OBJ_64 = new JLabel();
    CVZP2 = new XRiTextField();
    OBJ_65 = new JLabel();
    CVZP3 = new XRiTextField();
    OBJ_66 = new JLabel();
    CVZP4 = new XRiTextField();
    OBJ_67 = new JLabel();
    CVZP5 = new XRiTextField();
    OBJ_68 = new JLabel();
    CVNBED = new XRiTextField();
    OBJ_69 = new JLabel();
    CVNBEF = new XRiTextField();
    LIVR = new JXTitledPanel();
    OBJ_47 = new JLabel();
    CCZGE1 = new XRiTextField();
    CCZGE2 = new XRiTextField();
    CCZGE3 = new XRiTextField();
    CCZGE4 = new XRiTextField();
    CCZGE5 = new XRiTextField();
    OBJ_48 = new JLabel();
    CCMEX1 = new XRiTextField();
    CCMEX2 = new XRiTextField();
    CCMEX3 = new XRiTextField();
    CCMEX4 = new XRiTextField();
    CCMEX5 = new XRiTextField();
    COMP = new JXTitledPanel();
    OBJ_55 = new JLabel();
    CCSAN1 = new XRiTextField();
    OBJ_56 = new JLabel();
    CCPLED = new XRiTextField();
    OBJ_57 = new JLabel();
    CCPLEF = new XRiTextField();
    CCCLIV = new XRiCheckBox();
    CCCDES = new XRiCheckBox();
    STAT = new JXTitledPanel();
    OBJ_161 = new JLabel();
    OBJ_162 = new JLabel();
    CCMOID = new XRiTextField();
    OBJ_166 = new JLabel();
    CCEXED = new XRiTextField();
    OBJ_163 = new JLabel();
    CCMOIF = new XRiTextField();
    OBJ_167 = new JLabel();
    CCEXEF = new XRiTextField();
    OBJ_164 = new JLabel();
    CCCAFD = new XRiTextField();
    OBJ_165 = new JLabel();
    CCCAFF = new XRiTextField();
    HISTO = new JXTitledPanel();
    OBJ_70 = new JLabel();
    FLD001 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    bt_identification = new SNBoutonDetail();
    bt_livraison = new SNBoutonDetail();
    bt_facturation = new SNBoutonDetail();
    bt_comptabilite = new SNBoutonDetail();
    bt_dates = new SNBoutonDetail();
    bt_statistiques = new SNBoutonDetail();
    bt_vpc = new SNBoutonDetail();
    bt_historique = new SNBoutonDetail();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Crit\u00e8res de s\u00e9lection");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_154 ----
          OBJ_154.setText("Num\u00e9ro");
          OBJ_154.setName("OBJ_154");
          p_tete_gauche.add(OBJ_154);
          OBJ_154.setBounds(5, 5, 51, 18);

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");
          p_tete_gauche.add(INDNUM);
          INDNUM.setBounds(80, 0, 34, INDNUM.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_ann ========
            {
              riSousMenu_ann.setName("riSousMenu_ann");

              //---- riSousMenu_bt_ann ----
              riSousMenu_bt_ann.setText("Annulation");
              riSousMenu_bt_ann.setToolTipText("Annulation");
              riSousMenu_bt_ann.setName("riSousMenu_bt_ann");
              riSousMenu_ann.add(riSousMenu_bt_ann);
            }
            menus_haut.add(riSousMenu_ann);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- OBJ_75 ----
            OBJ_75.setText("Objet s\u00e9lection");
            OBJ_75.setName("OBJ_75");

            //---- CCOBJ ----
            CCOBJ.setComponentPopupMenu(BTD);
            CCOBJ.setName("CCOBJ");

            //---- OBJ_130 ----
            OBJ_130.setText("Observation");
            OBJ_130.setName("OBJ_130");

            //---- CCOBS ----
            CCOBS.setComponentPopupMenu(BTD);
            CCOBS.setName("CCOBS");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(105, 105, 105)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                      .addGap(57, 57, 57)
                      .addComponent(CCOBJ, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_130, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(74, 74, 74)
                      .addComponent(CCOBS, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_75, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCOBJ, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_130, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCOBS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //======== IDEN ========
          {
            IDEN.setTitle("Identification");
            IDEN.setBorder(new DropShadowBorder());
            IDEN.setName("IDEN");
            Container IDENContentContainer = IDEN.getContentContainer();

            //---- OBJ_43 ----
            OBJ_43.setText("Code postal");
            OBJ_43.setName("OBJ_43");

            //---- CCCDPD ----
            CCCDPD.setComponentPopupMenu(BTD);
            CCCDPD.setName("CCCDPD");

            //---- OBJ_44 ----
            OBJ_44.setText("\u00e0");
            OBJ_44.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_44.setName("OBJ_44");

            //---- CCCDPF ----
            CCCDPF.setComponentPopupMenu(BTD);
            CCCDPF.setName("CCCDPF");

            //---- OBJ_45 ----
            OBJ_45.setText("Cat\u00e9gorie");
            OBJ_45.setName("OBJ_45");

            //---- CCCAT1 ----
            CCCAT1.setComponentPopupMenu(BTD);
            CCCAT1.setName("CCCAT1");

            //---- CCCAT2 ----
            CCCAT2.setComponentPopupMenu(BTD);
            CCCAT2.setName("CCCAT2");

            //---- CCCAT3 ----
            CCCAT3.setComponentPopupMenu(BTD);
            CCCAT3.setName("CCCAT3");

            //---- CCCAT4 ----
            CCCAT4.setComponentPopupMenu(BTD);
            CCCAT4.setName("CCCAT4");

            //---- CCCAT5 ----
            CCCAT5.setComponentPopupMenu(BTD);
            CCCAT5.setName("CCCAT5");

            //---- OBJ_46 ----
            OBJ_46.setText("Repr\u00e9sentant");
            OBJ_46.setName("OBJ_46");

            //---- CCREP1 ----
            CCREP1.setComponentPopupMenu(BTD);
            CCREP1.setName("CCREP1");

            //---- CCREP2 ----
            CCREP2.setComponentPopupMenu(BTD);
            CCREP2.setName("CCREP2");

            //---- CCREP3 ----
            CCREP3.setComponentPopupMenu(BTD);
            CCREP3.setName("CCREP3");

            //---- CCREP4 ----
            CCREP4.setComponentPopupMenu(BTD);
            CCREP4.setName("CCREP4");

            //---- CCREP5 ----
            CCREP5.setComponentPopupMenu(BTD);
            CCREP5.setName("CCREP5");

            GroupLayout IDENContentContainerLayout = new GroupLayout(IDENContentContainer);
            IDENContentContainer.setLayout(IDENContentContainerLayout);
            IDENContentContainerLayout.setHorizontalGroup(
              IDENContentContainerLayout.createParallelGroup()
                .addGroup(IDENContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(IDENContentContainerLayout.createParallelGroup()
                    .addGroup(IDENContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(19, 19, 19)
                      .addComponent(CCCDPD, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                      .addGap(2, 2, 2)
                      .addComponent(CCCDPF, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                    .addGroup(IDENContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
                      .addGap(32, 32, 32)
                      .addComponent(CCCAT1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(CCCAT2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(CCCAT3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(CCCAT4, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(CCCAT5, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                    .addGroup(IDENContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                      .addGap(11, 11, 11)
                      .addComponent(CCREP1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(17, 17, 17)
                      .addComponent(CCREP2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(17, 17, 17)
                      .addComponent(CCREP3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(17, 17, 17)
                      .addComponent(CCREP4, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(17, 17, 17)
                      .addComponent(CCREP5, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
            );
            IDENContentContainerLayout.setVerticalGroup(
              IDENContentContainerLayout.createParallelGroup()
                .addGroup(IDENContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(IDENContentContainerLayout.createParallelGroup()
                    .addGroup(IDENContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCCDPD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(IDENContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCCDPF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(IDENContentContainerLayout.createParallelGroup()
                    .addGroup(IDENContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCCAT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCCAT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCCAT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCCAT4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCCAT5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(IDENContentContainerLayout.createParallelGroup()
                    .addGroup(IDENContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCREP1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCREP2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCREP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCREP4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCREP5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //======== FACT ========
          {
            FACT.setTitle("Facturation et r\u00e8glement");
            FACT.setBorder(new DropShadowBorder());
            FACT.setName("FACT");
            Container FACTContentContainer = FACT.getContentContainer();

            //---- OBJ_49 ----
            OBJ_49.setText("Type");
            OBJ_49.setName("OBJ_49");

            //---- CCTYPF ----
            CCTYPF.setComponentPopupMenu(BTD);
            CCTYPF.setName("CCTYPF");

            //---- CCTYPT ----
            CCTYPT.setText("Facture TTC");
            CCTYPT.setComponentPopupMenu(BTD);
            CCTYPT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCTYPT.setName("CCTYPT");

            //---- OBJ_171 ----
            OBJ_171.setText("Facturation");
            OBJ_171.setName("OBJ_171");

            //---- CCPERF ----
            CCPERF.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Jour",
              "Semaine",
              "D\u00e9cade",
              "Quinzaine",
              "Mois"
            }));
            CCPERF.setComponentPopupMenu(BTD);
            CCPERF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCPERF.setName("CCPERF");

            //---- OBJ_172 ----
            OBJ_172.setText("Relev\u00e9");
            OBJ_172.setName("OBJ_172");

            //---- CCPERR ----
            CCPERR.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Jour",
              "Semaine",
              "D\u00e9cade",
              "Quinzaine",
              "Mois"
            }));
            CCPERR.setComponentPopupMenu(BTD);
            CCPERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCPERR.setName("CCPERR");

            //---- OBJ_50 ----
            OBJ_50.setText("Remises");
            OBJ_50.setName("OBJ_50");

            //---- CCREMD ----
            CCREMD.setComponentPopupMenu(BTD);
            CCREMD.setName("CCREMD");

            //---- OBJ_51 ----
            OBJ_51.setText("\u00e0");
            OBJ_51.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_51.setName("OBJ_51");

            //---- CCREMF ----
            CCREMF.setComponentPopupMenu(BTD);
            CCREMF.setName("CCREMF");

            //---- OBJ_52 ----
            OBJ_52.setText("Devise");
            OBJ_52.setName("OBJ_52");

            //---- CCDEV1 ----
            CCDEV1.setComponentPopupMenu(BTD);
            CCDEV1.setName("CCDEV1");

            //---- CCDEV2 ----
            CCDEV2.setComponentPopupMenu(BTD);
            CCDEV2.setName("CCDEV2");

            //---- CCDEV3 ----
            CCDEV3.setComponentPopupMenu(BTD);
            CCDEV3.setName("CCDEV3");

            GroupLayout FACTContentContainerLayout = new GroupLayout(FACTContentContainer);
            FACTContentContainer.setLayout(FACTContentContainerLayout);
            FACTContentContainerLayout.setHorizontalGroup(
              FACTContentContainerLayout.createParallelGroup()
                .addGroup(FACTContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(FACTContentContainerLayout.createParallelGroup()
                    .addGroup(FACTContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                      .addGap(59, 59, 59)
                      .addComponent(CCTYPF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(CCTYPT, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(OBJ_171, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(CCPERF, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE))
                    .addGroup(FACTContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE)
                      .addGap(38, 38, 38)
                      .addComponent(CCREMD, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(18, 18, 18)
                      .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                      .addGap(19, 19, 19)
                      .addComponent(CCREMF, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(41, 41, 41)
                      .addComponent(OBJ_172, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addComponent(CCPERR, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE))
                    .addGroup(FACTContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                      .addGap(49, 49, 49)
                      .addComponent(CCDEV1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(CCDEV2, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(CCDEV3, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))))
            );
            FACTContentContainerLayout.setVerticalGroup(
              FACTContentContainerLayout.createParallelGroup()
                .addGroup(FACTContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(FACTContentContainerLayout.createParallelGroup()
                    .addGroup(FACTContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCTYPF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(FACTContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(CCTYPT, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(FACTContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_171, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addGroup(FACTContentContainerLayout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(CCPERF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(2, 2, 2)
                  .addGroup(FACTContentContainerLayout.createParallelGroup()
                    .addGroup(FACTContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCREMD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(FACTContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCREMF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(FACTContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_172, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addGroup(FACTContentContainerLayout.createSequentialGroup()
                      .addGap(1, 1, 1)
                      .addComponent(CCPERR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(2, 2, 2)
                  .addGroup(FACTContentContainerLayout.createParallelGroup()
                    .addGroup(FACTContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCDEV1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCDEV2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCDEV3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //======== DATE ========
          {
            DATE.setTitle("Dates");
            DATE.setBorder(new DropShadowBorder());
            DATE.setName("DATE");
            Container DATEContentContainer = DATE.getContentContainer();

            //---- OBJ_58 ----
            OBJ_58.setText("Derni\u00e8re visite");
            OBJ_58.setName("OBJ_58");

            //---- CCVIDX ----
            CCVIDX.setComponentPopupMenu(BTD);
            CCVIDX.setName("CCVIDX");

            //---- CCVIFX ----
            CCVIFX.setComponentPopupMenu(BTD);
            CCVIFX.setName("CCVIFX");

            //---- OBJ_59 ----
            OBJ_59.setText("\u00e0");
            OBJ_59.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_59.setName("OBJ_59");

            //---- OBJ_60 ----
            OBJ_60.setText("Derni\u00e8re vente");
            OBJ_60.setName("OBJ_60");

            //---- CCVEDX ----
            CCVEDX.setComponentPopupMenu(BTD);
            CCVEDX.setName("CCVEDX");

            //---- CCVEFX ----
            CCVEFX.setComponentPopupMenu(BTD);
            CCVEFX.setName("CCVEFX");

            //---- OBJ_61 ----
            OBJ_61.setText("\u00e0");
            OBJ_61.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_61.setName("OBJ_61");

            GroupLayout DATEContentContainerLayout = new GroupLayout(DATEContentContainer);
            DATEContentContainer.setLayout(DATEContentContainerLayout);
            DATEContentContainerLayout.setHorizontalGroup(
              DATEContentContainerLayout.createParallelGroup()
                .addGroup(DATEContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(DATEContentContainerLayout.createParallelGroup()
                    .addGroup(DATEContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(CCVIDX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(CCVIFX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
                    .addGroup(DATEContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                      .addGap(7, 7, 7)
                      .addComponent(CCVEDX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(CCVEFX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))))
            );
            DATEContentContainerLayout.setVerticalGroup(
              DATEContentContainerLayout.createParallelGroup()
                .addGroup(DATEContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(DATEContentContainerLayout.createParallelGroup()
                    .addComponent(CCVIDX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCVIFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(DATEContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(DATEContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(2, 2, 2)
                  .addGroup(DATEContentContainerLayout.createParallelGroup()
                    .addComponent(CCVEDX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCVEFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(DATEContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(DATEContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))))
            );
          }

          //======== VPC ========
          {
            VPC.setTitle("Vente par correspondance");
            VPC.setBorder(new DropShadowBorder());
            VPC.setName("VPC");
            Container VPCContentContainer = VPC.getContentContainer();

            //---- OBJ_62 ----
            OBJ_62.setText("Zones personnalis\u00e9es");
            OBJ_62.setName("OBJ_62");

            //---- OBJ_63 ----
            OBJ_63.setText("@TIT1@");
            OBJ_63.setName("OBJ_63");

            //---- CVZP1 ----
            CVZP1.setComponentPopupMenu(BTD);
            CVZP1.setName("CVZP1");

            //---- OBJ_64 ----
            OBJ_64.setText("@TIT2@");
            OBJ_64.setName("OBJ_64");

            //---- CVZP2 ----
            CVZP2.setComponentPopupMenu(BTD);
            CVZP2.setName("CVZP2");

            //---- OBJ_65 ----
            OBJ_65.setText("@TIT3@");
            OBJ_65.setName("OBJ_65");

            //---- CVZP3 ----
            CVZP3.setComponentPopupMenu(BTD);
            CVZP3.setName("CVZP3");

            //---- OBJ_66 ----
            OBJ_66.setText("@TIT4@");
            OBJ_66.setName("OBJ_66");

            //---- CVZP4 ----
            CVZP4.setComponentPopupMenu(BTD);
            CVZP4.setName("CVZP4");

            //---- OBJ_67 ----
            OBJ_67.setText("@TIT5@");
            OBJ_67.setName("OBJ_67");

            //---- CVZP5 ----
            CVZP5.setComponentPopupMenu(BTD);
            CVZP5.setName("CVZP5");

            //---- OBJ_68 ----
            OBJ_68.setText("Enfants");
            OBJ_68.setName("OBJ_68");

            //---- CVNBED ----
            CVNBED.setComponentPopupMenu(BTD);
            CVNBED.setName("CVNBED");

            //---- OBJ_69 ----
            OBJ_69.setText("\u00e0");
            OBJ_69.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_69.setName("OBJ_69");

            //---- CVNBEF ----
            CVNBEF.setComponentPopupMenu(BTD);
            CVNBEF.setName("CVNBEF");

            GroupLayout VPCContentContainerLayout = new GroupLayout(VPCContentContainer);
            VPCContentContainer.setLayout(VPCContentContainerLayout);
            VPCContentContainerLayout.setHorizontalGroup(
              VPCContentContainerLayout.createParallelGroup()
                .addGroup(VPCContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(VPCContentContainerLayout.createParallelGroup()
                    .addGroup(VPCContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
                      .addGap(187, 187, 187)
                      .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
                    .addGroup(VPCContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(CVZP1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addGroup(VPCContentContainerLayout.createParallelGroup()
                        .addGroup(VPCContentContainerLayout.createSequentialGroup()
                          .addGap(20, 20, 20)
                          .addComponent(CVZP2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                        .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addGroup(VPCContentContainerLayout.createParallelGroup()
                        .addGroup(VPCContentContainerLayout.createSequentialGroup()
                          .addGap(20, 20, 20)
                          .addComponent(CVZP3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                        .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addGroup(VPCContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                        .addGroup(VPCContentContainerLayout.createSequentialGroup()
                          .addGap(20, 20, 20)
                          .addComponent(CVZP4, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(CVZP5, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(30, 30, 30)
                      .addComponent(CVNBED, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(CVNBEF, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
            );
            VPCContentContainerLayout.setVerticalGroup(
              VPCContentContainerLayout.createParallelGroup()
                .addGroup(VPCContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(VPCContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(VPCContentContainerLayout.createParallelGroup()
                    .addComponent(CVZP1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVZP2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVZP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVZP4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVZP5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVNBED, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CVNBEF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(VPCContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(VPCContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))))
            );
          }

          //======== LIVR ========
          {
            LIVR.setTitle("Livraison");
            LIVR.setBorder(new DropShadowBorder());
            LIVR.setName("LIVR");
            Container LIVRContentContainer = LIVR.getContentContainer();

            //---- OBJ_47 ----
            OBJ_47.setText("Zone");
            OBJ_47.setToolTipText("Zone g\u00e9ographique");
            OBJ_47.setName("OBJ_47");

            //---- CCZGE1 ----
            CCZGE1.setToolTipText("Zone g\u00e9ographique");
            CCZGE1.setComponentPopupMenu(BTD);
            CCZGE1.setName("CCZGE1");

            //---- CCZGE2 ----
            CCZGE2.setToolTipText("Zone g\u00e9ographique");
            CCZGE2.setComponentPopupMenu(BTD);
            CCZGE2.setName("CCZGE2");

            //---- CCZGE3 ----
            CCZGE3.setToolTipText("Zone g\u00e9ographique");
            CCZGE3.setComponentPopupMenu(BTD);
            CCZGE3.setName("CCZGE3");

            //---- CCZGE4 ----
            CCZGE4.setToolTipText("Zone g\u00e9ographique");
            CCZGE4.setComponentPopupMenu(BTD);
            CCZGE4.setName("CCZGE4");

            //---- CCZGE5 ----
            CCZGE5.setToolTipText("Zone g\u00e9ographique");
            CCZGE5.setComponentPopupMenu(BTD);
            CCZGE5.setName("CCZGE5");

            //---- OBJ_48 ----
            OBJ_48.setText("Mode d'exp\u00e9dition");
            OBJ_48.setName("OBJ_48");

            //---- CCMEX1 ----
            CCMEX1.setComponentPopupMenu(BTD);
            CCMEX1.setName("CCMEX1");

            //---- CCMEX2 ----
            CCMEX2.setComponentPopupMenu(BTD);
            CCMEX2.setName("CCMEX2");

            //---- CCMEX3 ----
            CCMEX3.setComponentPopupMenu(BTD);
            CCMEX3.setName("CCMEX3");

            //---- CCMEX4 ----
            CCMEX4.setComponentPopupMenu(BTD);
            CCMEX4.setName("CCMEX4");

            //---- CCMEX5 ----
            CCMEX5.setComponentPopupMenu(BTD);
            CCMEX5.setName("CCMEX5");

            GroupLayout LIVRContentContainerLayout = new GroupLayout(LIVRContentContainer);
            LIVRContentContainer.setLayout(LIVRContentContainerLayout);
            LIVRContentContainerLayout.setHorizontalGroup(
              LIVRContentContainerLayout.createParallelGroup()
                .addGroup(LIVRContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(LIVRContentContainerLayout.createParallelGroup()
                    .addGroup(LIVRContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                      .addGap(76, 76, 76)
                      .addGroup(LIVRContentContainerLayout.createParallelGroup()
                        .addGroup(LIVRContentContainerLayout.createSequentialGroup()
                          .addGap(118, 118, 118)
                          .addComponent(CCZGE3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                        .addComponent(CCZGE1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addGroup(LIVRContentContainerLayout.createSequentialGroup()
                          .addGap(177, 177, 177)
                          .addComponent(CCZGE4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                        .addGroup(LIVRContentContainerLayout.createSequentialGroup()
                          .addGap(59, 59, 59)
                          .addComponent(CCZGE2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                        .addGroup(LIVRContentContainerLayout.createSequentialGroup()
                          .addGap(236, 236, 236)
                          .addComponent(CCZGE5, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(LIVRContentContainerLayout.createSequentialGroup()
                      .addGroup(LIVRContentContainerLayout.createParallelGroup()
                        .addGroup(LIVRContentContainerLayout.createSequentialGroup()
                          .addGap(110, 110, 110)
                          .addComponent(CCMEX1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                        .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE))
                      .addGap(29, 29, 29)
                      .addComponent(CCMEX2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(29, 29, 29)
                      .addComponent(CCMEX3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(29, 29, 29)
                      .addComponent(CCMEX4, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(29, 29, 29)
                      .addComponent(CCMEX5, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
            );
            LIVRContentContainerLayout.setVerticalGroup(
              LIVRContentContainerLayout.createParallelGroup()
                .addGroup(LIVRContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(LIVRContentContainerLayout.createParallelGroup()
                    .addGroup(LIVRContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCZGE3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCZGE1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCZGE4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCZGE2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCZGE5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(LIVRContentContainerLayout.createParallelGroup()
                    .addComponent(CCMEX1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(LIVRContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCMEX2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCMEX3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCMEX4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCMEX5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //======== COMP ========
          {
            COMP.setTitle("Comptabilit\u00e9 et divers");
            COMP.setBorder(new DropShadowBorder());
            COMP.setName("COMP");
            Container COMPContentContainer = COMP.getContentContainer();

            //---- OBJ_55 ----
            OBJ_55.setText("Affaire analytique");
            OBJ_55.setName("OBJ_55");

            //---- CCSAN1 ----
            CCSAN1.setComponentPopupMenu(BTD);
            CCSAN1.setName("CCSAN1");

            //---- OBJ_56 ----
            OBJ_56.setText("Plafond encours");
            OBJ_56.setName("OBJ_56");

            //---- CCPLED ----
            CCPLED.setComponentPopupMenu(BTD);
            CCPLED.setName("CCPLED");

            //---- OBJ_57 ----
            OBJ_57.setText("\u00e0");
            OBJ_57.setForeground(Color.black);
            OBJ_57.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_57.setName("OBJ_57");

            //---- CCPLEF ----
            CCPLEF.setComponentPopupMenu(BTD);
            CCPLEF.setName("CCPLEF");

            //---- CCCLIV ----
            CCCLIV.setText("Client livr\u00e9");
            CCCLIV.setComponentPopupMenu(BTD);
            CCCLIV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCCLIV.setName("CCCLIV");

            //---- CCCDES ----
            CCCDES.setText("Client d\u00e9sactiv\u00e9");
            CCCDES.setComponentPopupMenu(BTD);
            CCCDES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCCDES.setName("CCCDES");

            GroupLayout COMPContentContainerLayout = new GroupLayout(COMPContentContainer);
            COMPContentContainer.setLayout(COMPContentContainerLayout);
            COMPContentContainerLayout.setHorizontalGroup(
              COMPContentContainerLayout.createParallelGroup()
                .addGroup(COMPContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(COMPContentContainerLayout.createParallelGroup()
                    .addGroup(COMPContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(CCSAN1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                    .addGroup(COMPContentContainerLayout.createSequentialGroup()
                      .addComponent(CCCLIV, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                      .addGap(93, 93, 93)
                      .addComponent(CCCDES, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE))
                    .addGroup(COMPContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(CCPLED, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                      .addGap(4, 4, 4)
                      .addComponent(CCPLEF, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE))))
            );
            COMPContentContainerLayout.setVerticalGroup(
              COMPContentContainerLayout.createParallelGroup()
                .addGroup(COMPContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(COMPContentContainerLayout.createParallelGroup()
                    .addGroup(COMPContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCSAN1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(6, 6, 6)
                  .addGroup(COMPContentContainerLayout.createParallelGroup()
                    .addComponent(CCCLIV, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCCDES, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(6, 6, 6)
                  .addGroup(COMPContentContainerLayout.createParallelGroup()
                    .addComponent(CCPLED, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCPLEF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(COMPContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(COMPContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))))
            );
          }

          //======== STAT ========
          {
            STAT.setTitle("Statistiques");
            STAT.setBorder(new DropShadowBorder());
            STAT.setName("STAT");
            Container STATContentContainer = STAT.getContentContainer();

            //---- OBJ_161 ----
            OBJ_161.setText("Mois/exercice");
            OBJ_161.setName("OBJ_161");

            //---- OBJ_162 ----
            OBJ_162.setText("D\u00e9but");
            OBJ_162.setName("OBJ_162");

            //---- CCMOID ----
            CCMOID.setComponentPopupMenu(BTD);
            CCMOID.setName("CCMOID");

            //---- OBJ_166 ----
            OBJ_166.setText("/");
            OBJ_166.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_166.setName("OBJ_166");

            //---- CCEXED ----
            CCEXED.setComponentPopupMenu(BTD);
            CCEXED.setName("CCEXED");

            //---- OBJ_163 ----
            OBJ_163.setText("Fin");
            OBJ_163.setName("OBJ_163");

            //---- CCMOIF ----
            CCMOIF.setComponentPopupMenu(BTD);
            CCMOIF.setName("CCMOIF");

            //---- OBJ_167 ----
            OBJ_167.setText("/");
            OBJ_167.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_167.setName("OBJ_167");

            //---- CCEXEF ----
            CCEXEF.setComponentPopupMenu(BTD);
            CCEXEF.setName("CCEXEF");

            //---- OBJ_164 ----
            OBJ_164.setText("Chiffre d'affaire");
            OBJ_164.setName("OBJ_164");

            //---- CCCAFD ----
            CCCAFD.setComponentPopupMenu(BTD);
            CCCAFD.setName("CCCAFD");

            //---- OBJ_165 ----
            OBJ_165.setText("\u00e0");
            OBJ_165.setName("OBJ_165");

            //---- CCCAFF ----
            CCCAFF.setComponentPopupMenu(BTD);
            CCCAFF.setName("CCCAFF");

            GroupLayout STATContentContainerLayout = new GroupLayout(STATContentContainer);
            STATContentContainer.setLayout(STATContentContainerLayout);
            STATContentContainerLayout.setHorizontalGroup(
              STATContentContainerLayout.createParallelGroup()
                .addGroup(STATContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(STATContentContainerLayout.createParallelGroup()
                    .addGroup(STATContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_161, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
                      .addGap(23, 23, 23)
                      .addComponent(OBJ_162, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                      .addGap(11, 11, 11)
                      .addGroup(STATContentContainerLayout.createParallelGroup()
                        .addGroup(STATContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(OBJ_166, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE))
                        .addComponent(CCMOID, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                      .addGap(3, 3, 3)
                      .addComponent(CCEXED, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(60, 60, 60)
                      .addComponent(OBJ_163, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                      .addGap(29, 29, 29)
                      .addGroup(STATContentContainerLayout.createParallelGroup()
                        .addComponent(CCMOIF, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                        .addGroup(STATContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(OBJ_167, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)))
                      .addComponent(CCEXEF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(STATContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_164, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
                      .addGap(19, 19, 19)
                      .addComponent(CCCAFD, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(OBJ_165, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                      .addGap(38, 38, 38)
                      .addComponent(CCCAFF, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))))
            );
            STATContentContainerLayout.setVerticalGroup(
              STATContentContainerLayout.createParallelGroup()
                .addGroup(STATContentContainerLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(STATContentContainerLayout.createParallelGroup()
                    .addComponent(CCMOID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCEXED, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCMOIF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCEXEF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(STATContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(STATContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_161, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_162, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_166, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_163, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_167, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(2, 2, 2)
                  .addGroup(STATContentContainerLayout.createParallelGroup()
                    .addGroup(STATContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_164, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CCCAFD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_165, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CCCAFF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //======== HISTO ========
          {
            HISTO.setTitle("Historique");
            HISTO.setBorder(new DropShadowBorder());
            HISTO.setName("HISTO");
            Container HISTOContentContainer = HISTO.getContentContainer();

            //---- OBJ_70 ----
            OBJ_70.setText("S\u00e9lection historique proposition de vente et lien client/article");
            OBJ_70.setName("OBJ_70");

            //---- FLD001 ----
            FLD001.setComponentPopupMenu(BTD);
            FLD001.setName("FLD001");

            GroupLayout HISTOContentContainerLayout = new GroupLayout(HISTOContentContainer);
            HISTOContentContainer.setLayout(HISTOContentContainerLayout);
            HISTOContentContainerLayout.setHorizontalGroup(
              HISTOContentContainerLayout.createParallelGroup()
                .addGroup(HISTOContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 357, GroupLayout.PREFERRED_SIZE))
                .addGroup(HISTOContentContainerLayout.createSequentialGroup()
                  .addGap(365, 365, 365)
                  .addComponent(FLD001, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            );
            HISTOContentContainerLayout.setVerticalGroup(
              HISTOContentContainerLayout.createParallelGroup()
                .addGroup(HISTOContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGroup(HISTOContentContainerLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(FLD001, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(IDEN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(14, 14, 14)
                    .addComponent(LIVR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(FACT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(14, 14, 14)
                    .addComponent(COMP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(DATE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(14, 14, 14)
                    .addComponent(STAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(VPC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(14, 14, 14)
                    .addComponent(HISTO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(IDEN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(LIVR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(FACT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(COMP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(DATE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(STAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(VPC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(HISTO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }

    //---- bt_identification ----
    bt_identification.setPreferredSize(new Dimension(18, 18));
    bt_identification.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_identification.setIconTextGap(5);
    bt_identification.setToolTipText("Identification");
    bt_identification.setName("bt_identification");
    bt_identification.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_identificationActionPerformed(e);
      }
    });

    //---- bt_livraison ----
    bt_livraison.setPreferredSize(new Dimension(18, 18));
    bt_livraison.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_livraison.setIconTextGap(5);
    bt_livraison.setToolTipText("Livraison");
    bt_livraison.setName("bt_livraison");
    bt_livraison.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_livraisonActionPerformed(e);
      }
    });

    //---- bt_facturation ----
    bt_facturation.setPreferredSize(new Dimension(18, 18));
    bt_facturation.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_facturation.setIconTextGap(5);
    bt_facturation.setToolTipText("Facturation et r\u00e8glement");
    bt_facturation.setName("bt_facturation");
    bt_facturation.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_facturationActionPerformed(e);
      }
    });

    //---- bt_comptabilite ----
    bt_comptabilite.setPreferredSize(new Dimension(18, 18));
    bt_comptabilite.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_comptabilite.setIconTextGap(5);
    bt_comptabilite.setToolTipText("Comptabilit\u00e9 et divers");
    bt_comptabilite.setName("bt_comptabilite");
    bt_comptabilite.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_comptabiliteActionPerformed(e);
      }
    });

    //---- bt_dates ----
    bt_dates.setPreferredSize(new Dimension(18, 18));
    bt_dates.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_dates.setIconTextGap(5);
    bt_dates.setToolTipText("Dates");
    bt_dates.setName("bt_dates");
    bt_dates.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_datesActionPerformed(e);
      }
    });

    //---- bt_statistiques ----
    bt_statistiques.setPreferredSize(new Dimension(18, 18));
    bt_statistiques.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_statistiques.setIconTextGap(5);
    bt_statistiques.setToolTipText("Statistiques");
    bt_statistiques.setName("bt_statistiques");
    bt_statistiques.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_statistiquesActionPerformed(e);
      }
    });

    //---- bt_vpc ----
    bt_vpc.setPreferredSize(new Dimension(18, 18));
    bt_vpc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_vpc.setIconTextGap(5);
    bt_vpc.setToolTipText("Vente par correspondance");
    bt_vpc.setName("bt_vpc");
    bt_vpc.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_vpcActionPerformed(e);
      }
    });

    //---- bt_historique ----
    bt_historique.setPreferredSize(new Dimension(18, 18));
    bt_historique.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_historique.setIconTextGap(5);
    bt_historique.setToolTipText("Historique");
    bt_historique.setName("bt_historique");
    bt_historique.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_historiqueActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_154;
  private XRiTextField INDNUM;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_ann;
  private RiSousMenu_bt riSousMenu_bt_ann;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_75;
  private XRiTextField CCOBJ;
  private JLabel OBJ_130;
  private XRiTextField CCOBS;
  private JXTitledPanel IDEN;
  private JLabel OBJ_43;
  private XRiTextField CCCDPD;
  private JLabel OBJ_44;
  private XRiTextField CCCDPF;
  private JLabel OBJ_45;
  private XRiTextField CCCAT1;
  private XRiTextField CCCAT2;
  private XRiTextField CCCAT3;
  private XRiTextField CCCAT4;
  private XRiTextField CCCAT5;
  private JLabel OBJ_46;
  private XRiTextField CCREP1;
  private XRiTextField CCREP2;
  private XRiTextField CCREP3;
  private XRiTextField CCREP4;
  private XRiTextField CCREP5;
  private JXTitledPanel FACT;
  private JLabel OBJ_49;
  private XRiTextField CCTYPF;
  private XRiCheckBox CCTYPT;
  private JLabel OBJ_171;
  private XRiComboBox CCPERF;
  private JLabel OBJ_172;
  private XRiComboBox CCPERR;
  private JLabel OBJ_50;
  private XRiTextField CCREMD;
  private JLabel OBJ_51;
  private XRiTextField CCREMF;
  private JLabel OBJ_52;
  private XRiTextField CCDEV1;
  private XRiTextField CCDEV2;
  private XRiTextField CCDEV3;
  private JXTitledPanel DATE;
  private JLabel OBJ_58;
  private XRiCalendrier CCVIDX;
  private XRiCalendrier CCVIFX;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private XRiCalendrier CCVEDX;
  private XRiCalendrier CCVEFX;
  private JLabel OBJ_61;
  private JXTitledPanel VPC;
  private JLabel OBJ_62;
  private JLabel OBJ_63;
  private XRiTextField CVZP1;
  private JLabel OBJ_64;
  private XRiTextField CVZP2;
  private JLabel OBJ_65;
  private XRiTextField CVZP3;
  private JLabel OBJ_66;
  private XRiTextField CVZP4;
  private JLabel OBJ_67;
  private XRiTextField CVZP5;
  private JLabel OBJ_68;
  private XRiTextField CVNBED;
  private JLabel OBJ_69;
  private XRiTextField CVNBEF;
  private JXTitledPanel LIVR;
  private JLabel OBJ_47;
  private XRiTextField CCZGE1;
  private XRiTextField CCZGE2;
  private XRiTextField CCZGE3;
  private XRiTextField CCZGE4;
  private XRiTextField CCZGE5;
  private JLabel OBJ_48;
  private XRiTextField CCMEX1;
  private XRiTextField CCMEX2;
  private XRiTextField CCMEX3;
  private XRiTextField CCMEX4;
  private XRiTextField CCMEX5;
  private JXTitledPanel COMP;
  private JLabel OBJ_55;
  private XRiTextField CCSAN1;
  private JLabel OBJ_56;
  private XRiTextField CCPLED;
  private JLabel OBJ_57;
  private XRiTextField CCPLEF;
  private XRiCheckBox CCCLIV;
  private XRiCheckBox CCCDES;
  private JXTitledPanel STAT;
  private JLabel OBJ_161;
  private JLabel OBJ_162;
  private XRiTextField CCMOID;
  private JLabel OBJ_166;
  private XRiTextField CCEXED;
  private JLabel OBJ_163;
  private XRiTextField CCMOIF;
  private JLabel OBJ_167;
  private XRiTextField CCEXEF;
  private JLabel OBJ_164;
  private XRiTextField CCCAFD;
  private JLabel OBJ_165;
  private XRiTextField CCCAFF;
  private JXTitledPanel HISTO;
  private JLabel OBJ_70;
  private XRiTextField FLD001;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private SNBoutonDetail bt_identification;
  private SNBoutonDetail bt_livraison;
  private SNBoutonDetail bt_facturation;
  private SNBoutonDetail bt_comptabilite;
  private SNBoutonDetail bt_dates;
  private SNBoutonDetail bt_statistiques;
  private SNBoutonDetail bt_vpc;
  private SNBoutonDetail bt_historique;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
