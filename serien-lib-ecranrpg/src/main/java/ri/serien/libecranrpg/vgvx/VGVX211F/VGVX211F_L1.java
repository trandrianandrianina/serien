
package ri.serien.libecranrpg.vgvx.VGVX211F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVX211F_L1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTB01_Top = { "WTB01", "WTB02", "WTB03", "WTB04", "WTB05", "WTB06", "WTB07", "WTB08", "WTB09", "WTB10", "WTB11",
      "WTB12", "WTB13", "WTB14", "WTB15", "WTB16", "WTB17", "WTB18", "WTB19", "WTB20" };
  private String[] _WTB01_Title = { "Choix du bordereau à annuler", };
  private String[][] _WTB01_Data = { { "WLD01", }, { "WLD02", }, { "WLD03", }, { "WLD04", }, { "WLD05", }, { "WLD06", }, { "WLD07", },
      { "WLD08", }, { "WLD09", }, { "WLD10", }, { "WLD11", }, { "WLD12", }, { "WLD13", }, { "WLD14", }, { "WLD15", }, { "WLD16", },
      { "WLD17", }, { "WLD18", }, { "WLD19", }, { "WLD20", }, };
  private int[] _WTB01_Width = { 240, };
  
  public VGVX211F_L1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    initDiverses();
    WTB01.setAspectTable(_WTB01_Top, _WTB01_Title, _WTB01_Data, _WTB01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    gererLesErreurs("19");
    
    
    if (lexique.isTrue("(91)AND(92)")) {
      label1.setText("Annulation de découpe");
    }
    if (lexique.isTrue("(91)AND(N92)")) {
      label1.setText("Opération de découpe");
    }
    if (lexique.isTrue("(N91)AND(92)")) {
      label1.setText("Ordre de fabrication");
    }
    
    // TODO Icones
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void WTB01MouseClicked(MouseEvent e) {
    if (WTB01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label2 = new JLabel();
    P21ART = new XRiTextField();
    A1LIB = new XRiTextField();
    scrollPane1 = new JScrollPane();
    WTB01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    label1 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(650, 180));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- label2 ----
          label2.setText("Article");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(20, 25, 70, 21);

          //---- P21ART ----
          P21ART.setName("P21ART");
          panel1.add(P21ART);
          P21ART.setBounds(105, 21, 214, P21ART.getPreferredSize().height);

          //---- A1LIB ----
          A1LIB.setName("A1LIB");
          panel1.add(A1LIB);
          A1LIB.setBounds(105, 50, 314, A1LIB.getPreferredSize().height);

          //======== scrollPane1 ========
          {
            scrollPane1.setName("scrollPane1");

            //---- WTB01 ----
            WTB01.setModel(new DefaultTableModel(
              new Object[][] {
                {"@WLD01@"},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
              },
              new String[] {
                "Choix du bordereau \u00e0 annuler"
              }
            ));
            {
              TableColumnModel cm = WTB01.getColumnModel();
              cm.getColumn(0).setMinWidth(85);
            }
            WTB01.setName("WTB01");
            WTB01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTB01MouseClicked(e);
              }
            });
            scrollPane1.setViewportView(WTB01);
          }
          panel1.add(scrollPane1);
          scrollPane1.setBounds(105, 110, 285, 355);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setName("BT_PGUP");
          panel1.add(BT_PGUP);
          BT_PGUP.setBounds(394, 110, 25, 155);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setName("BT_PGDOWN");
          panel1.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(394, 310, 25, 155);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 25, 460, 495);

        //---- label1 ----
        label1.setText("text");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
        label1.setName("label1");
        p_contenu.add(label1);
        label1.setBounds(20, 5, 445, label1.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label2;
  private XRiTextField P21ART;
  private XRiTextField A1LIB;
  private JScrollPane scrollPane1;
  private XRiTable WTB01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel label1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
