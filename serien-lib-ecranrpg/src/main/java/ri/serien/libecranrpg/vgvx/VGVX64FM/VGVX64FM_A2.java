
package ri.serien.libecranrpg.vgvx.VGVX64FM;
// Nom Fichier: i_VGVX64FM_FMTA2_587.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVX64FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LIST_Top=null;
  private String[] _LD01_Title = { "HLD01", };
  private String[][] _LD01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _LD01_Width = { 654, };
  // private String[][] _LIST_Title_Data_Brut=null;
  
  public VGVX64FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    
    // Ajout
    initDiverses();
    LD01.setAspectTable(null, _LD01_Title, _LD01_Data, _LD01_Width, false, null, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MARG@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@(TITLE)@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, _LIST_Title_Data_Brut, _LIST_Top);
    
    
    
    FAMFIN.setVisible(lexique.isPresent("FAMFIN"));
    FAMDEB.setVisible(lexique.isPresent("FAMDEB"));
    V06F.setEnabled(lexique.isPresent("V06F"));
    // DATFIN.setVisible( lexique.isPresent("DATFIN"));
    // DATDEB.setVisible( lexique.isPresent("DATDEB"));
    FRSFIN.setVisible(lexique.isPresent("FRSFIN"));
    FRSDEB.setVisible(lexique.isPresent("FRSDEB"));
    OBJ_69.setEnabled(lexique.isPresent("V06F"));
    OBJ_68.setEnabled(lexique.isPresent("V06F"));
    ARTFIN.setVisible(lexique.isPresent("ARTFIN"));
    ARTDEB.setVisible(lexique.isPresent("ARTDEB"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    panel3.setVisible(lexique.isPresent("ARTDEB"));
    panel5.setVisible(lexique.isPresent("FAMDEB"));
    panel4.setVisible(lexique.isPresent("FRSDEB"));
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    // xH_Titre.setIcon(ManagerSessionClient.getInstance().getLogoImage(#ETB#.getText()));
    OBJ_69.setIcon(lexique.chargerImage("images/pfin20.png", true));
    OBJ_68.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvx64"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    // lexique.HostFieldPutData( "V06F", 0, "D" );
    V06F.setText("D");
    // Script original (à supprimer)
    // Touche="ENTER"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    // lexique.HostFieldPutData( "V06F", 0, "F" );
    V06F.setText("F");
    // Script original (à supprimer)
    // Touche="ENTER"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_72ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="ENTER"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    l_V01F = new JLabel();
    DEMETB = new XRiTextField();
    OBJ_40 = new JLabel();
    bt_Fonctions = new JButton();
    OBJ_52 = new JLabel();
    DATDEB = new XRiCalendrier();
    OBJ_54 = new JLabel();
    DATFIN = new XRiCalendrier();
    P_Centre = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    LD01 = new XRiTable();
    OBJ_56 = new JLabel();
    BT_PGDOWN = new JButton();
    BT_PGUP = new JButton();
    OBJ_68 = new JButton();
    OBJ_69 = new JButton();
    panel1 = new JPanel();
    OBJ_71 = new JLabel();
    V06F = new XRiTextField();
    OBJ_72 = new JButton();
    panel3 = new JPanel();
    label1 = new JLabel();
    ARTDEB = new XRiTextField();
    OBJ_43 = new JLabel();
    ARTFIN = new XRiTextField();
    panel4 = new JPanel();
    label2 = new JLabel();
    OBJ_45 = new JLabel();
    FRSDEB = new XRiTextField();
    FRSFIN = new XRiTextField();
    panel5 = new JPanel();
    label3 = new JLabel();
    OBJ_46 = new JLabel();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    OBJ_66 = new JPanel();
    OBJ_65 = new JPanel();
    OBJ_44 = new JLabel();
    OBJ_35 = new JPanel();
    OBJ_63 = new JPanel();
    OBJ_64 = new JPanel();
    P_PnlOpts = new JPanel();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      CMD.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 35));
        P_Infos.setPreferredSize(new Dimension(1000, 35));
        P_Infos.setName("P_Infos");

        //---- l_V01F ----
        l_V01F.setFont(new Font("sansserif", Font.BOLD, 12));
        l_V01F.setName("l_V01F");

        //---- DEMETB ----
        DEMETB.setName("DEMETB");

        //---- OBJ_40 ----
        OBJ_40.setName("OBJ_40");

        //---- bt_Fonctions ----
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- OBJ_52 ----
        OBJ_52.setName("OBJ_52");

        //---- DATDEB ----
        DATDEB.setName("DATDEB");

        //---- OBJ_54 ----
        OBJ_54.setName("OBJ_54");

        //---- DATFIN ----
        DATFIN.setName("DATFIN");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(19, 19, 19)
              .addComponent(l_V01F, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
              .addGap(9, 9, 9)
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addGap(155, 155, 155)
              .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
              .addGap(21, 21, 21)
              .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
              .addGap(20, 20, 20)
              .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 13, GroupLayout.PREFERRED_SIZE)
              .addGap(12, 12, 12)
              .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 113, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(7, 7, 7)
              .addComponent(l_V01F, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_40))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(DEMETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_52))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_54))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(bt_Fonctions)))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //======== panel2 ========
      {
        panel2.setName("panel2");
        panel2.setLayout(null);

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- LD01 ----
          LD01.setFont(new Font("Courier New", Font.PLAIN, 12));
          LD01.setComponentPopupMenu(BTD);
          LD01.setName("LD01");
          SCROLLPANE_LIST.setViewportView(LD01);
        }
        panel2.add(SCROLLPANE_LIST);
        SCROLLPANE_LIST.setBounds(15, 55, 995, 270);

        //---- OBJ_56 ----
        OBJ_56.setName("OBJ_56");
        panel2.add(OBJ_56);
        OBJ_56.setBounds(630, 30, 69, 18);

        //---- BT_PGDOWN ----
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");
        BT_PGDOWN.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_PGDOWNActionPerformed(e);
          }
        });
        panel2.add(BT_PGDOWN);
        BT_PGDOWN.setBounds(1015, 195, 25, 95);

        //---- BT_PGUP ----
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");
        BT_PGUP.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_PGUPActionPerformed(e);
          }
        });
        panel2.add(BT_PGUP);
        BT_PGUP.setBounds(1015, 90, 25, 95);

        //---- OBJ_68 ----
        OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_68.setName("OBJ_68");
        OBJ_68.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_68ActionPerformed(e);
          }
        });
        panel2.add(OBJ_68);
        OBJ_68.setBounds(1015, 55, 25, 30);

        //---- OBJ_69 ----
        OBJ_69.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_69.setName("OBJ_69");
        OBJ_69.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_69ActionPerformed(e);
          }
        });
        panel2.add(OBJ_69);
        OBJ_69.setBounds(1015, 295, 25, 30);

        //======== panel1 ========
        {
          panel1.setBorder(null);
          panel1.setName("panel1");

          //---- OBJ_71 ----
          OBJ_71.setName("OBJ_71");

          //---- V06F ----
          V06F.setName("V06F");

          //---- OBJ_72 ----
          OBJ_72.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_72.setName("OBJ_72");
          OBJ_72.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_72ActionPerformed(e);
            }
          });

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(V06F, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(V06F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
          );
        }
        panel2.add(panel1);
        panel1.setBounds(695, 330, 330, 40);

        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      P_Centre.add(panel2);
      panel2.setBounds(20, 70, 1060, 390);

      //======== panel3 ========
      {
        panel3.setName("panel3");
        panel3.setLayout(null);

        //---- label1 ----
        label1.setName("label1");
        panel3.add(label1);
        label1.setBounds(28, 9, 80, 21);

        //---- ARTDEB ----
        ARTDEB.setName("ARTDEB");
        panel3.add(ARTDEB);
        ARTDEB.setBounds(120, 5, 210, ARTDEB.getPreferredSize().height);

        //---- OBJ_43 ----
        OBJ_43.setName("OBJ_43");
        panel3.add(OBJ_43);
        OBJ_43.setBounds(348, 9, 96, 21);

        //---- ARTFIN ----
        ARTFIN.setName("ARTFIN");
        panel3.add(ARTFIN);
        ARTFIN.setBounds(453, 5, 210, ARTFIN.getPreferredSize().height);

        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel3.getComponentCount(); i++) {
            Rectangle bounds = panel3.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel3.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel3.setMinimumSize(preferredSize);
          panel3.setPreferredSize(preferredSize);
        }
      }
      P_Centre.add(panel3);
      panel3.setBounds(20, 15, 710, 40);

      //======== panel4 ========
      {
        panel4.setName("panel4");
        panel4.setLayout(null);

        //---- label2 ----
        label2.setName("label2");
        panel4.add(label2);
        label2.setBounds(28, 9, 125, 21);

        //---- OBJ_45 ----
        OBJ_45.setName("OBJ_45");
        panel4.add(OBJ_45);
        OBJ_45.setBounds(348, 9, 115, 21);

        //---- FRSDEB ----
        FRSDEB.setName("FRSDEB");
        panel4.add(FRSDEB);
        FRSDEB.setBounds(200, 5, 80, FRSDEB.getPreferredSize().height);

        //---- FRSFIN ----
        FRSFIN.setName("FRSFIN");
        panel4.add(FRSFIN);
        FRSFIN.setBounds(493, 5, 80, FRSFIN.getPreferredSize().height);

        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel4.getComponentCount(); i++) {
            Rectangle bounds = panel4.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel4.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel4.setMinimumSize(preferredSize);
          panel4.setPreferredSize(preferredSize);
        }
      }
      P_Centre.add(panel4);
      panel4.setBounds(20, 15, 665, 40);

      //======== panel5 ========
      {
        panel5.setName("panel5");
        panel5.setLayout(null);

        //---- label3 ----
        label3.setName("label3");
        panel5.add(label3);
        label3.setBounds(28, 9, 125, 21);

        //---- OBJ_46 ----
        OBJ_46.setName("OBJ_46");
        panel5.add(OBJ_46);
        OBJ_46.setBounds(348, 9, 115, 21);

        //---- FAMDEB ----
        FAMDEB.setName("FAMDEB");
        panel5.add(FAMDEB);
        FAMDEB.setBounds(205, 5, 40, FAMDEB.getPreferredSize().height);

        //---- FAMFIN ----
        FAMFIN.setName("FAMFIN");
        panel5.add(FAMFIN);
        FAMFIN.setBounds(493, 5, 40, FAMFIN.getPreferredSize().height);

        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel5.getComponentCount(); i++) {
            Rectangle bounds = panel5.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel5.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel5.setMinimumSize(preferredSize);
          panel5.setPreferredSize(preferredSize);
        }
      }
      P_Centre.add(panel5);
      panel5.setBounds(20, 15, 665, 40);

      { // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //======== OBJ_66 ========
    {
      OBJ_66.setName("OBJ_66");
      OBJ_66.setLayout(null);

      OBJ_66.setPreferredSize(new Dimension(74, 55));
    }

    //======== OBJ_65 ========
    {
      OBJ_65.setName("OBJ_65");
      OBJ_65.setLayout(null);

      OBJ_65.setPreferredSize(new Dimension(382, 55));
    }

    //---- OBJ_44 ----
    OBJ_44.setName("OBJ_44");

    //======== OBJ_35 ========
    {
      OBJ_35.setName("OBJ_35");
      OBJ_35.setLayout(null);

      OBJ_35.setPreferredSize(new Dimension(4, 93));
    }

    //======== OBJ_63 ========
    {
      OBJ_63.setName("OBJ_63");
      OBJ_63.setLayout(null);

      OBJ_63.setPreferredSize(new Dimension(132, 4));
    }

    //======== OBJ_64 ========
    {
      OBJ_64.setName("OBJ_64");
      OBJ_64.setLayout(null);

      OBJ_64.setPreferredSize(new Dimension(4, 55));
    }

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);

      P_PnlOpts.setPreferredSize(new Dimension(55, 516));
    }

    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JLabel l_V01F;
  private XRiTextField DEMETB;
  private JLabel OBJ_40;
  private JButton bt_Fonctions;
  private JLabel OBJ_52;
  private XRiCalendrier DATDEB;
  private JLabel OBJ_54;
  private XRiCalendrier DATFIN;
  private JPanel P_Centre;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LD01;
  private JLabel OBJ_56;
  private JButton BT_PGDOWN;
  private JButton BT_PGUP;
  private JButton OBJ_68;
  private JButton OBJ_69;
  private JPanel panel1;
  private JLabel OBJ_71;
  private XRiTextField V06F;
  private JButton OBJ_72;
  private JPanel panel3;
  private JLabel label1;
  private XRiTextField ARTDEB;
  private JLabel OBJ_43;
  private XRiTextField ARTFIN;
  private JPanel panel4;
  private JLabel label2;
  private JLabel OBJ_45;
  private XRiTextField FRSDEB;
  private XRiTextField FRSFIN;
  private JPanel panel5;
  private JLabel label3;
  private JLabel OBJ_46;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private JPanel OBJ_66;
  private JPanel OBJ_65;
  private JLabel OBJ_44;
  private JPanel OBJ_35;
  private JPanel OBJ_63;
  private JPanel OBJ_64;
  private JPanel P_PnlOpts;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
