
package ri.serien.libecranrpg.vgvx.VGVX30FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class VGVX30FM_G01 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_LINE3D);
  private RiGraphe graphe2 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  
  public VGVX30FM_G01(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    // Valeur
    // label2.setText(lexique.HostFieldGetData("MG01"));
    
    // GRAPHE
    
    String[] lignes = new String[10];
    String[] libelle1 = new String[10];
    String[] donnee1 = new String[10];
    String[] donnee2 = new String[10];
    
    // Chargement des lignes du tableau en tables
    for (int i = 0; i < lignes.length; i++) {
      lignes[i] = lexique.HostFieldGetData("WL2" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)));
      lignes[i] = lignes[i].replaceAll(",", ".");
      
    }
    
    // Chargement des données
    // libellés
    
    for (int i = 0; i < lignes.length; i++) {
      libelle1[i] = lignes[i].substring(0, 8);
    }
    
    // données
    for (int i = 0; i < lignes.length; i++) {
      
      if (lignes[i].trim().equalsIgnoreCase("")) {
        donnee1[i] = "0.0";
        donnee2[i] = "0.0";
      }
      else {
        donnee1[i] = lignes[i].substring(25, 37).trim();
        donnee2[i] = lignes[i].substring(13, 23).trim();
      }
      
    }
    
    // Préparation des données
    Object[][] data = new Object[libelle1.length][2];
    Object[][] data2 = new Object[libelle1.length][2];
    for (int i = 0; i < libelle1.length; i++) {
      data[i][0] = libelle1[i];
      data2[i][0] = libelle1[i];
      data[i][1] = Double.parseDouble(donnee1[i]);
      data2[i][1] = Double.parseDouble(donnee2[i]);
    }
    
    graphe.setDonnee(data, "", false);
    graphe.getGraphe("Evolution du prix unitaire", false);
    
    graphe2.setDonnee(data2, "", false);
    graphe2.getGraphe("Evolution des quantités", false);
    
    l_graphe.setIcon(graphe.getPicture(l_graphe.getWidth(), l_graphe.getHeight()));
    l_graphe2.setIcon(graphe2.getPicture(l_graphe2.getWidth(), l_graphe2.getHeight()));
    
    
    
    // TODO Icones
    button1.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques"));
  }
  
  public void getData() {
    
  }
  
  public JDialog getDialog() {
    return this;
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    getData();
    setVisible(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    P_Centre = new JPanel();
    l_graphe = new JLabel();
    l_graphe2 = new JLabel();
    separator1 = compFactory.createSeparator("Historique d'achat");
    button1 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1100, 690));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);
      
      // ---- l_graphe ----
      l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe.setComponentPopupMenu(null);
      l_graphe.setBackground(new Color(214, 217, 223));
      l_graphe.setName("l_graphe");
      P_Centre.add(l_graphe);
      l_graphe.setBounds(15, 30, 1065, 285);
      
      // ---- l_graphe2 ----
      l_graphe2.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe2.setComponentPopupMenu(null);
      l_graphe2.setBackground(new Color(214, 217, 223));
      l_graphe2.setName("l_graphe2");
      P_Centre.add(l_graphe2);
      l_graphe2.setBounds(15, 320, 1065, 289);
      
      // ---- separator1 ----
      separator1.setName("separator1");
      P_Centre.add(separator1);
      separator1.setBounds(12, 11, 1068, separator1.getPreferredSize().height);
      
      // ---- button1 ----
      button1.setText("Retour");
      button1.setFont(button1.getFont().deriveFont(button1.getFont().getStyle() | Font.BOLD, button1.getFont().getSize() + 2f));
      button1.setName("button1");
      button1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          button1ActionPerformed(e);
        }
      });
      P_Centre.add(button1);
      button1.setBounds(960, 615, 120, 35);
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JLabel l_graphe;
  private JLabel l_graphe2;
  private JComponent separator1;
  private JButton button1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
