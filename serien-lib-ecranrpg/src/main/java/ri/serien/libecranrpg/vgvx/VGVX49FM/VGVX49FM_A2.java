
package ri.serien.libecranrpg.vgvx.VGVX49FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX49FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX49FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    WTP01.setValeursSelection("4", " ");
    WTP02.setValeursSelection("4", " ");
    WTP03.setValeursSelection("4", " ");
    WTP04.setValeursSelection("4", " ");
    WTP05.setValeursSelection("4", " ");
    WTP06.setValeursSelection("4", " ");
    WTP07.setValeursSelection("4", " ");
    WTP08.setValeursSelection("4", " ");
    WTP09.setValeursSelection("4", " ");
    WTP10.setValeursSelection("4", " ");
    WTP11.setValeursSelection("4", " ");
    WTP12.setValeursSelection("4", " ");
    WTP13.setValeursSelection("4", " ");
    WTP14.setValeursSelection("4", " ");
    WTP15.setValeursSelection("4", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    MAL01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL01@")).trim());
    MAL201.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL201@")).trim());
    MAL02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL02@")).trim());
    MAL03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL03@")).trim());
    MAL04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL04@")).trim());
    MAL05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL05@")).trim());
    MAL06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL06@")).trim());
    MAL07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL07@")).trim());
    MAL08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL08@")).trim());
    MAL09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL09@")).trim());
    MAL10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL10@")).trim());
    MAL11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL11@")).trim());
    MAL12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL12@")).trim());
    MAL13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL13@")).trim());
    MAL14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL14@")).trim());
    MAL15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL15@")).trim());
    MAL202.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL202@")).trim());
    MAL203.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL203@")).trim());
    MAL204.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL204@")).trim());
    MAL205.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL205@")).trim());
    MAL206.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL206@")).trim());
    MAL207.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL207@")).trim());
    MAL208.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL208@")).trim());
    MAL209.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL209@")).trim());
    MAL210.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL210@")).trim());
    MAL211.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL211@")).trim());
    MAL212.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL212@")).trim());
    MAL213.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL213@")).trim());
    MAL214.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL214@")).trim());
    MAL215.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAL215@")).trim());
    EBART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("Saisie transferts autorisés"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvx43"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledSeparator2 = new JXTitledSeparator();
    MAG01 = new XRiTextField();
    MAL01 = new RiZoneSortie();
    MAG201 = new XRiTextField();
    MAL201 = new RiZoneSortie();
    WTP01 = new XRiCheckBox();
    MAG02 = new XRiTextField();
    MAG03 = new XRiTextField();
    MAG04 = new XRiTextField();
    MAG05 = new XRiTextField();
    MAG06 = new XRiTextField();
    MAG07 = new XRiTextField();
    MAG08 = new XRiTextField();
    MAG09 = new XRiTextField();
    MAG10 = new XRiTextField();
    MAG11 = new XRiTextField();
    MAG12 = new XRiTextField();
    MAG13 = new XRiTextField();
    MAG14 = new XRiTextField();
    MAG15 = new XRiTextField();
    MAL02 = new RiZoneSortie();
    MAL03 = new RiZoneSortie();
    MAL04 = new RiZoneSortie();
    MAL05 = new RiZoneSortie();
    MAL06 = new RiZoneSortie();
    MAL07 = new RiZoneSortie();
    MAL08 = new RiZoneSortie();
    MAL09 = new RiZoneSortie();
    MAL10 = new RiZoneSortie();
    MAL11 = new RiZoneSortie();
    MAL12 = new RiZoneSortie();
    MAL13 = new RiZoneSortie();
    MAL14 = new RiZoneSortie();
    MAL15 = new RiZoneSortie();
    MAG202 = new XRiTextField();
    MAL202 = new RiZoneSortie();
    MAG203 = new XRiTextField();
    MAG204 = new XRiTextField();
    MAG205 = new XRiTextField();
    MAG206 = new XRiTextField();
    MAG207 = new XRiTextField();
    MAG208 = new XRiTextField();
    MAG209 = new XRiTextField();
    MAG210 = new XRiTextField();
    MAG211 = new XRiTextField();
    MAG212 = new XRiTextField();
    MAG213 = new XRiTextField();
    MAG214 = new XRiTextField();
    MAG215 = new XRiTextField();
    MAL203 = new RiZoneSortie();
    MAL204 = new RiZoneSortie();
    MAL205 = new RiZoneSortie();
    MAL206 = new RiZoneSortie();
    MAL207 = new RiZoneSortie();
    MAL208 = new RiZoneSortie();
    MAL209 = new RiZoneSortie();
    MAL210 = new RiZoneSortie();
    MAL211 = new RiZoneSortie();
    MAL212 = new RiZoneSortie();
    MAL213 = new RiZoneSortie();
    MAL214 = new RiZoneSortie();
    MAL215 = new RiZoneSortie();
    WTP02 = new XRiCheckBox();
    WTP03 = new XRiCheckBox();
    WTP04 = new XRiCheckBox();
    WTP05 = new XRiCheckBox();
    WTP06 = new XRiCheckBox();
    WTP07 = new XRiCheckBox();
    WTP08 = new XRiCheckBox();
    WTP09 = new XRiCheckBox();
    WTP10 = new XRiCheckBox();
    WTP11 = new XRiCheckBox();
    WTP12 = new XRiCheckBox();
    WTP13 = new XRiCheckBox();
    WTP14 = new XRiCheckBox();
    WTP15 = new XRiCheckBox();
    barre_tete = new JMenuBar();
    panel2 = new JPanel();
    label1 = new JLabel();
    EBART = new RiZoneSortie();
    label2 = new JLabel();
    WMAG = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(905, 555));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 330));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //---- xTitledSeparator1 ----
        xTitledSeparator1.setTitle("Magasin \u00e9metteur");
        xTitledSeparator1.setName("xTitledSeparator1");

        //---- xTitledSeparator2 ----
        xTitledSeparator2.setTitle("Magasin r\u00e9cepteur");
        xTitledSeparator2.setName("xTitledSeparator2");

        //---- MAG01 ----
        MAG01.setName("MAG01");

        //---- MAL01 ----
        MAL01.setText("@MAL01@");
        MAL01.setName("MAL01");

        //---- MAG201 ----
        MAG201.setName("MAG201");

        //---- MAL201 ----
        MAL201.setText("@MAL201@");
        MAL201.setName("MAL201");

        //---- WTP01 ----
        WTP01.setText("Supprimer");
        WTP01.setName("WTP01");

        //---- MAG02 ----
        MAG02.setName("MAG02");

        //---- MAG03 ----
        MAG03.setName("MAG03");

        //---- MAG04 ----
        MAG04.setName("MAG04");

        //---- MAG05 ----
        MAG05.setName("MAG05");

        //---- MAG06 ----
        MAG06.setName("MAG06");

        //---- MAG07 ----
        MAG07.setName("MAG07");

        //---- MAG08 ----
        MAG08.setName("MAG08");

        //---- MAG09 ----
        MAG09.setName("MAG09");

        //---- MAG10 ----
        MAG10.setName("MAG10");

        //---- MAG11 ----
        MAG11.setName("MAG11");

        //---- MAG12 ----
        MAG12.setName("MAG12");

        //---- MAG13 ----
        MAG13.setName("MAG13");

        //---- MAG14 ----
        MAG14.setName("MAG14");

        //---- MAG15 ----
        MAG15.setName("MAG15");

        //---- MAL02 ----
        MAL02.setText("@MAL02@");
        MAL02.setName("MAL02");

        //---- MAL03 ----
        MAL03.setText("@MAL03@");
        MAL03.setName("MAL03");

        //---- MAL04 ----
        MAL04.setText("@MAL04@");
        MAL04.setName("MAL04");

        //---- MAL05 ----
        MAL05.setText("@MAL05@");
        MAL05.setName("MAL05");

        //---- MAL06 ----
        MAL06.setText("@MAL06@");
        MAL06.setName("MAL06");

        //---- MAL07 ----
        MAL07.setText("@MAL07@");
        MAL07.setName("MAL07");

        //---- MAL08 ----
        MAL08.setText("@MAL08@");
        MAL08.setName("MAL08");

        //---- MAL09 ----
        MAL09.setText("@MAL09@");
        MAL09.setName("MAL09");

        //---- MAL10 ----
        MAL10.setText("@MAL10@");
        MAL10.setName("MAL10");

        //---- MAL11 ----
        MAL11.setText("@MAL11@");
        MAL11.setName("MAL11");

        //---- MAL12 ----
        MAL12.setText("@MAL12@");
        MAL12.setName("MAL12");

        //---- MAL13 ----
        MAL13.setText("@MAL13@");
        MAL13.setName("MAL13");

        //---- MAL14 ----
        MAL14.setText("@MAL14@");
        MAL14.setName("MAL14");

        //---- MAL15 ----
        MAL15.setText("@MAL15@");
        MAL15.setName("MAL15");

        //---- MAG202 ----
        MAG202.setName("MAG202");

        //---- MAL202 ----
        MAL202.setText("@MAL202@");
        MAL202.setName("MAL202");

        //---- MAG203 ----
        MAG203.setName("MAG203");

        //---- MAG204 ----
        MAG204.setName("MAG204");

        //---- MAG205 ----
        MAG205.setName("MAG205");

        //---- MAG206 ----
        MAG206.setName("MAG206");

        //---- MAG207 ----
        MAG207.setName("MAG207");

        //---- MAG208 ----
        MAG208.setName("MAG208");

        //---- MAG209 ----
        MAG209.setName("MAG209");

        //---- MAG210 ----
        MAG210.setName("MAG210");

        //---- MAG211 ----
        MAG211.setName("MAG211");

        //---- MAG212 ----
        MAG212.setName("MAG212");

        //---- MAG213 ----
        MAG213.setName("MAG213");

        //---- MAG214 ----
        MAG214.setName("MAG214");

        //---- MAG215 ----
        MAG215.setName("MAG215");

        //---- MAL203 ----
        MAL203.setText("@MAL203@");
        MAL203.setName("MAL203");

        //---- MAL204 ----
        MAL204.setText("@MAL204@");
        MAL204.setName("MAL204");

        //---- MAL205 ----
        MAL205.setText("@MAL205@");
        MAL205.setName("MAL205");

        //---- MAL206 ----
        MAL206.setText("@MAL206@");
        MAL206.setName("MAL206");

        //---- MAL207 ----
        MAL207.setText("@MAL207@");
        MAL207.setName("MAL207");

        //---- MAL208 ----
        MAL208.setText("@MAL208@");
        MAL208.setName("MAL208");

        //---- MAL209 ----
        MAL209.setText("@MAL209@");
        MAL209.setName("MAL209");

        //---- MAL210 ----
        MAL210.setText("@MAL210@");
        MAL210.setName("MAL210");

        //---- MAL211 ----
        MAL211.setText("@MAL211@");
        MAL211.setName("MAL211");

        //---- MAL212 ----
        MAL212.setText("@MAL212@");
        MAL212.setName("MAL212");

        //---- MAL213 ----
        MAL213.setText("@MAL213@");
        MAL213.setName("MAL213");

        //---- MAL214 ----
        MAL214.setText("@MAL214@");
        MAL214.setName("MAL214");

        //---- MAL215 ----
        MAL215.setText("@MAL215@");
        MAL215.setName("MAL215");

        //---- WTP02 ----
        WTP02.setText("Supprimer");
        WTP02.setName("WTP02");

        //---- WTP03 ----
        WTP03.setText("Supprimer");
        WTP03.setName("WTP03");

        //---- WTP04 ----
        WTP04.setText("Supprimer");
        WTP04.setName("WTP04");

        //---- WTP05 ----
        WTP05.setText("Supprimer");
        WTP05.setName("WTP05");

        //---- WTP06 ----
        WTP06.setText("Supprimer");
        WTP06.setName("WTP06");

        //---- WTP07 ----
        WTP07.setText("Supprimer");
        WTP07.setName("WTP07");

        //---- WTP08 ----
        WTP08.setText("Supprimer");
        WTP08.setName("WTP08");

        //---- WTP09 ----
        WTP09.setText("Supprimer");
        WTP09.setName("WTP09");

        //---- WTP10 ----
        WTP10.setText("Supprimer");
        WTP10.setName("WTP10");

        //---- WTP11 ----
        WTP11.setText("Supprimer");
        WTP11.setName("WTP11");

        //---- WTP12 ----
        WTP12.setText("Supprimer");
        WTP12.setName("WTP12");

        //---- WTP13 ----
        WTP13.setText("Supprimer");
        WTP13.setName("WTP13");

        //---- WTP14 ----
        WTP14.setText("Supprimer");
        WTP14.setName("WTP14");

        //---- WTP15 ----
        WTP15.setText("Supprimer");
        WTP15.setName("WTP15");

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE)
              .addGap(25, 25, 25)
              .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(MAG07, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG01, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG08, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG04, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG03, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG05, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG02, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG10, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG09, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG06, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG13, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG15, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG14, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG12, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG11, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
              .addGap(11, 11, 11)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(MAL01, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL02, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL03, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL04, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL05, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL06, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL07, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL08, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL09, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL10, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL11, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL12, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL13, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL14, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL15, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE))
              .addGap(35, 35, 35)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(MAG202, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG201, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG208, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG212, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG205, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG207, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG206, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG215, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG214, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG213, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG211, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG209, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG204, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG210, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAG203, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
              .addGap(11, 11, 11)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(MAL201, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL202, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL203, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL204, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL205, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL206, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL207, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL208, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL209, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL210, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL211, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL212, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL213, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL214, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                .addComponent(MAL215, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE))
              .addGap(15, 15, 15)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(WTP01, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP02, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP03, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP04, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP05, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP06, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP07, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP08, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP09, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP10, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP11, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP12, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP13, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP14, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WTP15, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(45, 45, 45)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(xTitledSeparator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(24, 24, 24)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(150, 150, 150)
                      .addComponent(MAG07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(MAG01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(175, 175, 175)
                      .addComponent(MAG08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(MAG04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(MAG03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(MAG05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(MAG02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(225, 225, 225)
                      .addComponent(MAG10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(200, 200, 200)
                      .addComponent(MAG09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(125, 125, 125)
                      .addComponent(MAG06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(22, 22, 22)
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(MAG13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(MAG15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(MAG14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(MAG12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(250, 250, 250)
                  .addComponent(MAG11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(2, 2, 2)
                  .addComponent(MAL01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(MAG202, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(MAG201, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(22, 22, 22)
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(MAG208, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(200, 200, 200)
                      .addComponent(MAG212, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(MAG205, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(MAG207, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(MAG206, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(275, 275, 275)
                      .addComponent(MAG215, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(250, 250, 250)
                      .addComponent(MAG214, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(225, 225, 225)
                      .addComponent(MAG213, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(175, 175, 175)
                      .addComponent(MAG211, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(125, 125, 125)
                      .addComponent(MAG209, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(MAG204, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(150, 150, 150)
                      .addComponent(MAG210, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(50, 50, 50)
                  .addComponent(MAG203, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(2, 2, 2)
                  .addComponent(MAL201, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL202, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL203, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL204, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL205, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL206, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL207, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL208, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL209, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL210, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL211, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL212, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL213, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL214, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(MAL215, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(2, 2, 2)
                  .addComponent(WTP01, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP02, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP03, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP04, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP05, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP06, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP07, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP08, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP09, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP10, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP11, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP12, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP13, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP14, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addGap(0, 0, 0)
                  .addComponent(WTP15, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 35));
      barre_tete.setMinimumSize(new Dimension(752, 40));
      barre_tete.setName("barre_tete");

      //======== panel2 ========
      {
        panel2.setMinimumSize(new Dimension(740, 35));
        panel2.setBorder(null);
        panel2.setOpaque(false);
        panel2.setBackground(new Color(214, 217, 223));
        panel2.setName("panel2");

        //---- label1 ----
        label1.setText("Etablissement");
        label1.setName("label1");

        //---- EBART ----
        EBART.setText("@WETB@");
        EBART.setFont(EBART.getFont().deriveFont(EBART.getFont().getStyle() | Font.BOLD));
        EBART.setBackground(new Color(214, 217, 223));
        EBART.setOpaque(false);
        EBART.setHorizontalAlignment(SwingConstants.CENTER);
        EBART.setName("EBART");

        //---- label2 ----
        label2.setText("Magasin");
        label2.setName("label2");

        //---- WMAG ----
        WMAG.setName("WMAG");

        GroupLayout panel2Layout = new GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
          panel2Layout.createParallelGroup()
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addGroup(panel2Layout.createParallelGroup()
                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(95, 95, 95)
                  .addComponent(EBART, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
              .addGap(35, 35, 35)
              .addGroup(panel2Layout.createParallelGroup()
                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(60, 60, 60)
                  .addComponent(WMAG, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))))
        );
        panel2Layout.setVerticalGroup(
          panel2Layout.createParallelGroup()
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(4, 4, 4)
              .addGroup(panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(2, 2, 2)
                  .addComponent(label1, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                .addComponent(EBART, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addGroup(panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addComponent(label2, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                .addComponent(WMAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
        );
      }
      barre_tete.add(panel2);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator xTitledSeparator2;
  private XRiTextField MAG01;
  private RiZoneSortie MAL01;
  private XRiTextField MAG201;
  private RiZoneSortie MAL201;
  private XRiCheckBox WTP01;
  private XRiTextField MAG02;
  private XRiTextField MAG03;
  private XRiTextField MAG04;
  private XRiTextField MAG05;
  private XRiTextField MAG06;
  private XRiTextField MAG07;
  private XRiTextField MAG08;
  private XRiTextField MAG09;
  private XRiTextField MAG10;
  private XRiTextField MAG11;
  private XRiTextField MAG12;
  private XRiTextField MAG13;
  private XRiTextField MAG14;
  private XRiTextField MAG15;
  private RiZoneSortie MAL02;
  private RiZoneSortie MAL03;
  private RiZoneSortie MAL04;
  private RiZoneSortie MAL05;
  private RiZoneSortie MAL06;
  private RiZoneSortie MAL07;
  private RiZoneSortie MAL08;
  private RiZoneSortie MAL09;
  private RiZoneSortie MAL10;
  private RiZoneSortie MAL11;
  private RiZoneSortie MAL12;
  private RiZoneSortie MAL13;
  private RiZoneSortie MAL14;
  private RiZoneSortie MAL15;
  private XRiTextField MAG202;
  private RiZoneSortie MAL202;
  private XRiTextField MAG203;
  private XRiTextField MAG204;
  private XRiTextField MAG205;
  private XRiTextField MAG206;
  private XRiTextField MAG207;
  private XRiTextField MAG208;
  private XRiTextField MAG209;
  private XRiTextField MAG210;
  private XRiTextField MAG211;
  private XRiTextField MAG212;
  private XRiTextField MAG213;
  private XRiTextField MAG214;
  private XRiTextField MAG215;
  private RiZoneSortie MAL203;
  private RiZoneSortie MAL204;
  private RiZoneSortie MAL205;
  private RiZoneSortie MAL206;
  private RiZoneSortie MAL207;
  private RiZoneSortie MAL208;
  private RiZoneSortie MAL209;
  private RiZoneSortie MAL210;
  private RiZoneSortie MAL211;
  private RiZoneSortie MAL212;
  private RiZoneSortie MAL213;
  private RiZoneSortie MAL214;
  private RiZoneSortie MAL215;
  private XRiCheckBox WTP02;
  private XRiCheckBox WTP03;
  private XRiCheckBox WTP04;
  private XRiCheckBox WTP05;
  private XRiCheckBox WTP06;
  private XRiCheckBox WTP07;
  private XRiCheckBox WTP08;
  private XRiCheckBox WTP09;
  private XRiCheckBox WTP10;
  private XRiCheckBox WTP11;
  private XRiCheckBox WTP12;
  private XRiCheckBox WTP13;
  private XRiCheckBox WTP14;
  private XRiCheckBox WTP15;
  private JMenuBar barre_tete;
  private JPanel panel2;
  private JLabel label1;
  private RiZoneSortie EBART;
  private JLabel label2;
  private XRiTextField WMAG;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
