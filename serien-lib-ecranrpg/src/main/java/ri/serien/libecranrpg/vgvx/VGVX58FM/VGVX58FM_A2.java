
package ri.serien.libecranrpg.vgvx.VGVX58FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX58FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", "WTP16", "WTP17", "WTP18", };
  private String[] _WTP01_Title = {
      "Date Rec Mode expédition    N°Bon   Fournisseur                  Famille                       Coeff.     Calculé  Forcé  Sélect.", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, { "LD16", }, { "LD17", }, { "LD18", }, };
  private int[] _WTP01_Width = { 773, };
  private Color[][] _LIST_Text_Color = new Color[_WTP01_Top.length][1];
  
  public VGVX58FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    DESALL.setValeursSelection("X", " ");
    VALALL.setValeursSelection("X", " ");
    TTDAT.setValeursSelection("**", "  ");
    TTGFA.setValeursSelection("**", "  ");
    TTFRS.setValeursSelection("**", "  ");
    TTEXP.setValeursSelection("**", "  ");
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, _LIST_Text_Color, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    DEMETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEMETB@")).trim());
    LIBETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBETB@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBFAM@")).trim());
    OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBFRS@")).trim());
    OBJ_66.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBEXP@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CFMYXP@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CFSAXP@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENXP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top, null, _LIST_Text_Color, null, null);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    
    
    // VALALL.setSelected(lexique.HostFieldGetData("VALALL").equalsIgnoreCase("X"));
    // TTDAT.setSelected(lexique.HostFieldGetData("TTDAT").equalsIgnoreCase("**"));
    // TTGFA.setSelected(lexique.HostFieldGetData("TTGFA").equalsIgnoreCase("**"));
    // DESALL.setSelected(lexique.HostFieldGetData("DESALL").equalsIgnoreCase("X"));
    // TTFRS.setSelected(lexique.HostFieldGetData("TTFRS").equalsIgnoreCase("**"));
    // TTEXP.setSelected(lexique.HostFieldGetData("TTEXP").equalsIgnoreCase("**"));
    
    OBJ_47.setVisible(lexique.isTrue("14"));
    OBJ_56.setVisible(lexique.isTrue("14"));
    
    // Couleurs de la liste (suivant présence du texte "Sélect." sur la
    // ligne)
    for (int i = 0; i < _WTP01_Top.length; i++) {
      
      if (lexique.HostFieldGetData("LD" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).contains("Sélect.")) {
        _LIST_Text_Color[i][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
      }
      else {
        _LIST_Text_Color[i][0] = Color.BLACK;
      }
      
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - HISTORIQUE DE COEFFICIENT D'APPROCHE"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (VALALL.isSelected()) {
    // lexique.HostFieldPutData("VALALL", 0, "X");
    // } else {
    // lexique.HostFieldPutData("VALALL", 0, " ");
    // }
    // if (DESALL.isSelected()) {
    // lexique.HostFieldPutData("DESALL", 0, "X");
    // } else {
    // lexique.HostFieldPutData("DESALL", 0, " ");
    // }
    
    // if (TTDAT.isSelected())
    // lexique.HostFieldPutData("TTDAT", 0, "**");
    // else
    // lexique.HostFieldPutData("TTDAT", 0, " ");
    // if (TTGFA.isSelected())
    // lexique.HostFieldPutData("TTGFA", 0, "**");
    // else
    // lexique.HostFieldPutData("TTGFA", 0, " ");
    // if (TTFRS.isSelected())
    // lexique.HostFieldPutData("TTFRS", 0, "**");
    // else
    // lexique.HostFieldPutData("TTFRS", 0, " ");
    // if (TTEXP.isSelected())
    // lexique.HostFieldPutData("TTEXP", 0, "**");
    // else
    // lexique.HostFieldPutData("TTEXP", 0, " ");
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void VALALLActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("DESALL", 0, " ");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void DESALLActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("VALALL", 0, " ");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "ENTER", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_33 = new JLabel();
    DEMETB = new RiZoneSortie();
    LIBETB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_53 = new RiZoneSortie();
    OBJ_63 = new RiZoneSortie();
    OBJ_66 = new RiZoneSortie();
    TTEXP = new XRiCheckBox();
    OBJ_64 = new JLabel();
    TTFRS = new XRiCheckBox();
    OBJ_57 = new JLabel();
    TTGFA = new XRiCheckBox();
    TTDAT = new XRiCheckBox();
    OBJ_50 = new JLabel();
    OBJ_61 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    FRSDEB = new XRiTextField();
    GFADEB = new XRiTextField();
    EXPDEB = new XRiTextField();
    OBJ_59 = new JLabel();
    panel4 = new JPanel();
    VALALL = new XRiCheckBox();
    DESALL = new XRiCheckBox();
    panel3 = new JPanel();
    OBJ_45 = new JLabel();
    OBJ_54 = new JLabel();
    COEFMY = new XRiTextField();
    COEFSA = new XRiTextField();
    OBJ_47 = new RiZoneSortie();
    OBJ_56 = new RiZoneSortie();
    OBJ_43 = new JLabel();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_14 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Historique de coefficient d'approche");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_33 ----
          OBJ_33.setText("Societe");
          OBJ_33.setName("OBJ_33");
          p_tete_gauche.add(OBJ_33);
          OBJ_33.setBounds(5, 4, 90, 20);

          //---- DEMETB ----
          DEMETB.setComponentPopupMenu(BTD);
          DEMETB.setText("@DEMETB@");
          DEMETB.setOpaque(false);
          DEMETB.setName("DEMETB");
          p_tete_gauche.add(DEMETB);
          DEMETB.setBounds(100, 2, 40, DEMETB.getPreferredSize().height);

          //---- LIBETB ----
          LIBETB.setComponentPopupMenu(BTD);
          LIBETB.setText("@LIBETB@");
          LIBETB.setOpaque(false);
          LIBETB.setName("LIBETB");
          p_tete_gauche.add(LIBETB);
          LIBETB.setBounds(150, 2, 235, LIBETB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 6400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_53 ----
            OBJ_53.setText("@LIBFAM@");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(305, 120, 231, OBJ_53.getPreferredSize().height);

            //---- OBJ_63 ----
            OBJ_63.setText("@LIBFRS@");
            OBJ_63.setName("OBJ_63");
            panel1.add(OBJ_63);
            OBJ_63.setBounds(305, 185, 231, OBJ_63.getPreferredSize().height);

            //---- OBJ_66 ----
            OBJ_66.setText("@LIBEXP@");
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(305, 215, 231, OBJ_66.getPreferredSize().height);

            //---- TTEXP ----
            TTEXP.setText("Tous les modes d'exp\u00e9dition");
            TTEXP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TTEXP.setName("TTEXP");
            panel1.add(TTEXP);
            TTEXP.setBounds(605, 217, 195, 20);

            //---- OBJ_64 ----
            OBJ_64.setText("Mode exp\u00e9dition");
            OBJ_64.setName("OBJ_64");
            panel1.add(OBJ_64);
            OBJ_64.setBounds(15, 217, 150, 20);

            //---- TTFRS ----
            TTFRS.setText("Tous les fournisseurs");
            TTFRS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TTFRS.setName("TTFRS");
            panel1.add(TTFRS);
            TTFRS.setBounds(605, 187, 195, 20);

            //---- OBJ_57 ----
            OBJ_57.setText("Date reception");
            OBJ_57.setName("OBJ_57");
            panel1.add(OBJ_57);
            OBJ_57.setBounds(15, 155, 150, 20);

            //---- TTGFA ----
            TTGFA.setText("Toutes les familles");
            TTGFA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TTGFA.setName("TTGFA");
            panel1.add(TTGFA);
            TTGFA.setBounds(605, 122, 195, 20);

            //---- TTDAT ----
            TTDAT.setText("Toutes les dates");
            TTDAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TTDAT.setName("TTDAT");
            panel1.add(TTDAT);
            TTDAT.setBounds(605, 155, 195, 20);

            //---- OBJ_50 ----
            OBJ_50.setText("Code famille");
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(15, 124, 150, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("Fournisseur");
            OBJ_61.setName("OBJ_61");
            panel1.add(OBJ_61);
            OBJ_61.setBounds(15, 186, 150, 20);

            //---- DATDEB ----
            DATDEB.setComponentPopupMenu(BTD);
            DATDEB.setName("DATDEB");
            panel1.add(DATDEB);
            DATDEB.setBounds(170, 151, 105, DATDEB.getPreferredSize().height);

            //---- DATFIN ----
            DATFIN.setComponentPopupMenu(BTD);
            DATFIN.setName("DATFIN");
            panel1.add(DATFIN);
            DATFIN.setBounds(305, 151, 105, DATFIN.getPreferredSize().height);

            //---- FRSDEB ----
            FRSDEB.setComponentPopupMenu(BTD);
            FRSDEB.setName("FRSDEB");
            panel1.add(FRSDEB);
            FRSDEB.setBounds(170, 182, 66, FRSDEB.getPreferredSize().height);

            //---- GFADEB ----
            GFADEB.setComponentPopupMenu(BTD);
            GFADEB.setName("GFADEB");
            panel1.add(GFADEB);
            GFADEB.setBounds(170, 120, 40, GFADEB.getPreferredSize().height);

            //---- EXPDEB ----
            EXPDEB.setComponentPopupMenu(BTD);
            EXPDEB.setName("EXPDEB");
            panel1.add(EXPDEB);
            EXPDEB.setBounds(170, 213, 30, EXPDEB.getPreferredSize().height);

            //---- OBJ_59 ----
            OBJ_59.setText("au");
            OBJ_59.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_59.setName("OBJ_59");
            panel1.add(OBJ_59);
            OBJ_59.setBounds(275, 155, 28, 20);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("S\u00e9lection"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- VALALL ----
              VALALL.setText("Choix global");
              VALALL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              VALALL.setName("VALALL");
              VALALL.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  VALALLActionPerformed(e);
                }
              });
              panel4.add(VALALL);
              VALALL.setBounds(55, 55, 145, 20);

              //---- DESALL ----
              DESALL.setText("D\u00e9s\u00e9lection globale");
              DESALL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              DESALL.setName("DESALL");
              DESALL.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  DESALLActionPerformed(e);
                }
              });
              panel4.add(DESALL);
              DESALL.setBounds(220, 55, 165, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel4);
            panel4.setBounds(570, 10, 415, 110);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Coefficient d'approche moyen"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_45 ----
              OBJ_45.setText("Calcul\u00e9");
              OBJ_45.setName("OBJ_45");
              panel3.add(OBJ_45);
              OBJ_45.setBounds(25, 37, 130, 20);

              //---- OBJ_54 ----
              OBJ_54.setText("Forc\u00e9");
              OBJ_54.setName("OBJ_54");
              panel3.add(OBJ_54);
              OBJ_54.setBounds(25, 69, 130, 20);

              //---- COEFMY ----
              COEFMY.setComponentPopupMenu(BTD);
              COEFMY.setName("COEFMY");
              panel3.add(COEFMY);
              COEFMY.setBounds(160, 35, 56, COEFMY.getPreferredSize().height);

              //---- COEFSA ----
              COEFSA.setComponentPopupMenu(BTD);
              COEFSA.setName("COEFSA");
              panel3.add(COEFSA);
              COEFSA.setBounds(160, 65, 58, COEFSA.getPreferredSize().height);

              //---- OBJ_47 ----
              OBJ_47.setText("@CFMYXP@");
              OBJ_47.setName("OBJ_47");
              panel3.add(OBJ_47);
              OBJ_47.setBounds(405, 35, 91, OBJ_47.getPreferredSize().height);

              //---- OBJ_56 ----
              OBJ_56.setText("@CFSAXP@");
              OBJ_56.setName("OBJ_56");
              panel3.add(OBJ_56);
              OBJ_56.setBounds(405, 67, 91, OBJ_56.getPreferredSize().height);

              //---- OBJ_43 ----
              OBJ_43.setText("@ENXP@");
              OBJ_43.setName("OBJ_43");
              panel3.add(OBJ_43);
              OBJ_43.setBounds(295, 37, 91, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel3);
            panel3.setBounds(10, 10, 530, 110);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(0, 1, 1000, panel1.getPreferredSize().height);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD2);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel2.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(5, 0, 980, 318);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            BT_PGUP.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_PGUPActionPerformed(e);
              }
            });
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(990, 0, 25, 145);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            BT_PGDOWN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_PGDOWNActionPerformed(e);
              }
            });
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(990, 173, 25, 145);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(0, 249, 1020, 325);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 3, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_14 ----
      OBJ_14.setText("Choisir");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_14);
    }
    // //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_33;
  private RiZoneSortie DEMETB;
  private RiZoneSortie LIBETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie OBJ_53;
  private RiZoneSortie OBJ_63;
  private RiZoneSortie OBJ_66;
  private XRiCheckBox TTEXP;
  private JLabel OBJ_64;
  private XRiCheckBox TTFRS;
  private JLabel OBJ_57;
  private XRiCheckBox TTGFA;
  private XRiCheckBox TTDAT;
  private JLabel OBJ_50;
  private JLabel OBJ_61;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private XRiTextField FRSDEB;
  private XRiTextField GFADEB;
  private XRiTextField EXPDEB;
  private JLabel OBJ_59;
  private JPanel panel4;
  private XRiCheckBox VALALL;
  private XRiCheckBox DESALL;
  private JPanel panel3;
  private JLabel OBJ_45;
  private JLabel OBJ_54;
  private XRiTextField COEFMY;
  private XRiTextField COEFSA;
  private RiZoneSortie OBJ_47;
  private RiZoneSortie OBJ_56;
  private JLabel OBJ_43;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_14;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
