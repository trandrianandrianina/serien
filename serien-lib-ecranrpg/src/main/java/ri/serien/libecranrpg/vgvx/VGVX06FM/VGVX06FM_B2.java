
package ri.serien.libecranrpg.vgvx.VGVX06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.conditionvente.snconditionvente.SNConditionVenteEtClient;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVX06FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVX06FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WART@")).trim());
    WETB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    CLNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    CLCLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCLI@")).trim());
    CLLIV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLLIV@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1LB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB1@")).trim());
    A1LB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB2@")).trim());
    A1LB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB3@")).trim());
    A1CL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1CL1@")).trim());
    A1CL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1CL2@")).trim());
    lbClassement1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TMC1@")).trim());
    lbClassement2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TMC2@")).trim());
    FRNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOM@")).trim());
    WPRX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPRX@")).trim());
    A1UNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNV@")).trim());
    WCOE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOE@")).trim());
    WPVB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPVB@")).trim());
    UREM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UREM1@")).trim());
    UREM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UREM2@")).trim());
    UREM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UREM3@")).trim());
    WPRX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPRX2@")).trim());
    WPVF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPVF@")).trim());
    WSTKX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKX@")).trim());
    WRESX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRESX@")).trim());
    A1UNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    WDISX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    WATTX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTX@")).trim());
    TGLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TGLIBR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    lbQuantite.setVisible(WQTEX.isVisible());
    lbTypeGratuit.setVisible(lexique.isTrue("69"));
    TGLIBR.setVisible(lexique.isTrue("69"));
    
    if (lexique.isTrue("N24")) {
      pnlRemises.setVisible(false);
    }
    else {
      pnlRemises.setVisible(true);
    }
    
    flash.setForeground(Color.BLACK);
    flash.setToolTipText("");
    
    String heure = "";
    if (lexique.HostFieldGetData("WUHEU").length() > 0) {
      heure = " à " + lexique.HostFieldGetData("WUHEU").substring(0, 2) + " heure " + lexique.HostFieldGetData("WUHEU").substring(2);
      flash.setToolTipText("Négocié par " + lexique.HostFieldGetData("WUSER") + " le " + lexique.HostFieldGetData("WUDAT") + heure);
      flash.setForeground(Color.BLUE);
    }
    lbBase.setVisible(WPVB.isVisible());
    pnlPrix.setVisible(!lexique.HostFieldGetData("WPVF").trim().equalsIgnoreCase(""));
    lbPrixNegocie.setVisible(pnlPrix.isVisible());
    
    String nombre = "";
    float disponible = 0;
    
    if (!lexique.HostFieldGetData("WDISX").trim().equals("")) {
      nombre = lexique.HostFieldGetData("WDISX").trim();
      if (nombre.endsWith("-")) {
        nombre = ("-" + nombre.replace('-', ' ')).trim();
      }
      nombre = nombre.replace(',', '.');
      
      disponible = Float.parseFloat(nombre);
    }
    
    WDISX.setVisible(disponible > 0);
    lbDispo.setVisible(WDISX.isVisible());
    lbPasDispo.setVisible(!lbDispo.isVisible());
    
    lbAttendu.setVisible(!lexique.HostFieldGetData("WATTX").trim().equals(""));
    WATTX.setVisible(!lexique.HostFieldGetData("WATTX").trim().equals(""));
    
    WCOE.setVisible(!lexique.HostFieldGetData("WCOE").trim().equals(""));
    lbCoeff.setVisible(WCOE.isVisible());
    
    UREM1.setVisible(!WCOE.isVisible());
    UREM2.setVisible(!WCOE.isVisible());
    UREM3.setVisible(!WCOE.isVisible());
    lbRemises.setVisible(!WCOE.isVisible());
    
    lbPrixNet.setVisible(lexique.isTrue("N66"));
    WPRX.setVisible(lexique.isTrue("N66"));
    lbPrixTTC.setVisible(lexique.isTrue("66"));
    WPRX2.setVisible(lexique.isTrue("66"));
    
    bouton_valider.setVisible(lexique.isTrue("N66"));
    bouton_valider2.setVisible(lexique.isTrue("66"));
    bouton_retour.setVisible(lexique.isTrue("N66"));
    bouton_retour2.setVisible(lexique.isTrue("66"));
    navig_retour2.setVisible(lexique.isTrue("66"));
    
    pnlRemises.setVisible(lexique.isTrue("N69"));
    
    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    // Identifiant établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("WETB"));
    
    // Condition vente
    snConditionVenteEtClient.setSession(getSession());
    snConditionVenteEtClient.setIdEtablissement(idEtablissement);
    snConditionVenteEtClient.setSelectionParChampRPG(lexique, "WCNV");
    
    // Magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
    
    // ICONES ++++
    p_bpresentation.setCodeEtablissement(WETB2.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB2.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    bouton_retour2.setIcon(lexique.chargerImage("images/loupe.png", true));
    bouton_valider2.setIcon(lexique.chargerImage("images/OK_p.png", true));
    bouton_retour4.setIcon(lexique.chargerImage("images/retour_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snConditionVenteEtClient.renseignerChampRPG(lexique, "WCNV");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void flashActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_valider2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18");
  }
  
  private void bouton_retour2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WART = new RiZoneSortie();
    OBJ_61 = new JLabel();
    WETB2 = new RiZoneSortie();
    OBJ_63 = new JLabel();
    OBJ_27 = new JLabel();
    CLNOM = new RiZoneSortie();
    CLCLI = new RiZoneSortie();
    CLLIV = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new SNPanelFond();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    bouton_valider2 = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    bouton_retour2 = new RiMenu_bt();
    navig_retour2 = new RiMenu();
    bouton_retour4 = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    pnlArticle = new SNPanelTitre();
    A1LIB = new SNTexte();
    lbClassement1 = new SNLabelChamp();
    A1CL1 = new SNTexte();
    A1LB1 = new SNTexte();
    lbClassement2 = new SNLabelChamp();
    A1CL2 = new SNTexte();
    A1LB2 = new SNTexte();
    A1LB3 = new SNTexte();
    lbFournisseur = new SNLabelChamp();
    FRNOM = new SNTexte();
    sNPanelTitre1 = new SNPanelTitre();
    lbCondition = new SNLabelChamp();
    snConditionVenteEtClient = new SNConditionVenteEtClient();
    flash = new SNBoutonDetail();
    lbUV = new SNLabelChamp();
    pnlQuantite = new SNPanel();
    A1UNV = new SNTexte();
    lbQuantite = new SNLabelChamp();
    WQTEX = new XRiTextField();
    lbTypeGratuit = new SNLabelChamp();
    TGLIBR = new SNTexte();
    lbBase = new SNLabelChamp();
    WPVB = new XRiTextField();
    lbCoeff = new SNLabelChamp();
    pnlRemises = new SNPanel();
    WCOE = new XRiTextField();
    lbRemises = new SNLabelChamp();
    UREM1 = new XRiTextField();
    UREM2 = new XRiTextField();
    UREM3 = new XRiTextField();
    lbPrixNet = new SNLabelChamp();
    WPRX = new XRiTextField();
    lbPrixNegocie = new SNLabelChamp();
    pnlPrix = new SNPanel();
    WPVF = new XRiTextField();
    lbPrixTTC = new SNLabelChamp();
    WPRX2 = new XRiTextField();
    sNPanelTitre2 = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbQuantiteStock = new SNLabelChamp();
    pnlStocks = new SNPanel();
    WSTKX = new XRiTextField();
    A1UNS = new SNTexte();
    OBJ_133 = new JLabel();
    lbQuantiteReserve = new SNLabelChamp();
    WRESX = new XRiTextField();
    OBJ_131 = new JLabel();
    lbDispo = new SNLabelChamp();
    WDISX = new XRiTextField();
    lbPasDispo = new SNLabelUnite();
    lbAttendu = new SNLabelChamp();
    WATTX = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(970, 560));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Interrogation de condition article");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- WART ----
          WART.setOpaque(false);
          WART.setText("@WART@");
          WART.setName("WART");
          
          // ---- OBJ_61 ----
          OBJ_61.setText("Etablissement");
          OBJ_61.setName("OBJ_61");
          
          // ---- WETB2 ----
          WETB2.setOpaque(false);
          WETB2.setText("@WETB@");
          WETB2.setName("WETB2");
          
          // ---- OBJ_63 ----
          OBJ_63.setText("Article");
          OBJ_63.setName("OBJ_63");
          
          // ---- OBJ_27 ----
          OBJ_27.setText("Client");
          OBJ_27.setName("OBJ_27");
          
          // ---- CLNOM ----
          CLNOM.setOpaque(false);
          CLNOM.setText("@CLNOM@");
          CLNOM.setName("CLNOM");
          
          // ---- CLCLI ----
          CLCLI.setOpaque(false);
          CLCLI.setText("@CLCLI@");
          CLCLI.setHorizontalAlignment(SwingConstants.RIGHT);
          CLCLI.setHorizontalTextPosition(SwingConstants.RIGHT);
          CLCLI.setName("CLCLI");
          
          // ---- CLLIV ----
          CLLIV.setOpaque(false);
          CLLIV.setText("@CLLIV@");
          CLLIV.setHorizontalAlignment(SwingConstants.RIGHT);
          CLLIV.setHorizontalTextPosition(SwingConstants.RIGHT);
          CLLIV.setName("CLLIV");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(90, 90, 90).addComponent(WETB2,
                          GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                  .addGap(18, 18, 18).addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(WART, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(CLCLI, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(CLLIV, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(CLNOM, GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE).addContainerGap()));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(3, 3, 3).addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(WETB2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CLCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CLLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(CLNOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider (Entr\u00e9e)");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
            
            // ---- bouton_valider2 ----
            bouton_valider2.setText("Valider prix");
            bouton_valider2.setActionCommand("Valider prix");
            bouton_valider2.setName("bouton_valider2");
            bouton_valider2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_valider2ActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider2);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour  (F12, F3 pour sortir)");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
            
            // ---- bouton_retour2 ----
            bouton_retour2.setText("Autre article");
            bouton_retour2.setName("bouton_retour2");
            bouton_retour2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retour2ActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour2);
          }
          menus_bas.add(navig_retour);
          
          // ======== navig_retour2 ========
          {
            navig_retour2.setName("navig_retour2");
            
            // ---- bouton_retour4 ----
            bouton_retour4.setText("Autre client");
            bouton_retour4.setToolTipText("Autre client");
            bouton_retour4.setName("bouton_retour4");
            bouton_retour4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour2.add(bouton_retour4);
          }
          menus_bas.add(navig_retour2);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Options article");
              riSousMenu_bt6.setToolTipText("Options article (F2)");
              riSousMenu_bt6.setRolloverIcon(null);
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Prix flash");
              riSousMenu_bt1.setToolTipText("Prix flash (F9)");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");
              
              // ---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Conditions vente client");
              riSousMenu_bt16.setToolTipText("Conditions de vente du client  (Shift + F7)");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);
            
            // ======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");
              
              // ---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Options client");
              riSousMenu_bt12.setToolTipText("Options client (F5)");
              riSousMenu_bt12.setRolloverIcon(null);
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Attendu/command\u00e9");
              riSousMenu_bt7.setToolTipText("Attendu/command\u00e9 (Shift + F9)");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");
              
              // ---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Affichage \u00e9tendu");
              riSousMenu_bt13.setToolTipText("Affichage \u00e9tendu  (F6)");
              riSousMenu_bt13.setRolloverIcon(null);
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Saisie de bon");
              riSousMenu_bt9.setToolTipText("Saisie de bon  (F10)");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Recherche de bon");
              riSousMenu_bt10.setToolTipText("Recherche de bon  (F7)");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Historique de conso.");
              riSousMenu_bt11.setToolTipText("Historique de consommation  (Shift + F8)");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Infos achat");
              riSousMenu_bt15.setToolTipText("Informations achat  (Shift + F11)");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc notes article");
              riSousMenu_bt14.setToolTipText("Bloc notes article   (Shift + F10)");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlArticle ========
        {
          pnlArticle.setTitre("Article");
          pnlArticle.setName("pnlArticle");
          pnlArticle.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlArticle.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlArticle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlArticle.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlArticle.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- A1LIB ----
          A1LIB.setMinimumSize(new Dimension(300, 30));
          A1LIB.setPreferredSize(new Dimension(300, 30));
          A1LIB.setText("@A1LIB@");
          A1LIB.setEnabled(false);
          A1LIB.setName("A1LIB");
          pnlArticle.add(A1LIB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbClassement1 ----
          lbClassement1.setText("@TMC1@");
          lbClassement1.setName("lbClassement1");
          pnlArticle.add(lbClassement1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1CL1 ----
          A1CL1.setText("@A1CL1@");
          A1CL1.setMinimumSize(new Dimension(200, 30));
          A1CL1.setPreferredSize(new Dimension(200, 30));
          A1CL1.setEnabled(false);
          A1CL1.setName("A1CL1");
          pnlArticle.add(A1CL1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- A1LB1 ----
          A1LB1.setMinimumSize(new Dimension(300, 30));
          A1LB1.setPreferredSize(new Dimension(300, 30));
          A1LB1.setText("@A1LB1@");
          A1LB1.setEnabled(false);
          A1LB1.setName("A1LB1");
          pnlArticle.add(A1LB1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbClassement2 ----
          lbClassement2.setText("@TMC2@");
          lbClassement2.setName("lbClassement2");
          pnlArticle.add(lbClassement2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1CL2 ----
          A1CL2.setText("@A1CL2@");
          A1CL2.setMinimumSize(new Dimension(200, 30));
          A1CL2.setPreferredSize(new Dimension(200, 30));
          A1CL2.setEnabled(false);
          A1CL2.setName("A1CL2");
          pnlArticle.add(A1CL2, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- A1LB2 ----
          A1LB2.setMinimumSize(new Dimension(300, 30));
          A1LB2.setPreferredSize(new Dimension(300, 30));
          A1LB2.setText("@A1LB2@");
          A1LB2.setEnabled(false);
          A1LB2.setName("A1LB2");
          pnlArticle.add(A1LB2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1LB3 ----
          A1LB3.setMinimumSize(new Dimension(300, 30));
          A1LB3.setPreferredSize(new Dimension(300, 30));
          A1LB3.setText("@A1LB3@");
          A1LB3.setEnabled(false);
          A1LB3.setName("A1LB3");
          pnlArticle.add(A1LB3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbFournisseur ----
          lbFournisseur.setText("Fournisseur");
          lbFournisseur.setName("lbFournisseur");
          pnlArticle.add(lbFournisseur, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- FRNOM ----
          FRNOM.setText("@FRNOM@");
          FRNOM.setEnabled(false);
          FRNOM.setName("FRNOM");
          pnlArticle.add(FRNOM, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== sNPanelTitre1 ========
        {
          sNPanelTitre1.setTitre("Condition de vente");
          sNPanelTitre1.setName("sNPanelTitre1");
          sNPanelTitre1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanelTitre1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanelTitre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbCondition ----
          lbCondition.setText("Condition");
          lbCondition.setName("lbCondition");
          sNPanelTitre1.add(lbCondition, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snConditionVenteEtClient ----
          snConditionVenteEtClient.setName("snConditionVenteEtClient");
          sNPanelTitre1.add(snConditionVenteEtClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- flash ----
          flash.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          flash.setHorizontalAlignment(SwingConstants.LEADING);
          flash.setHorizontalTextPosition(SwingConstants.RIGHT);
          flash.setText("Prix flash");
          flash.setMargin(new Insets(10, 10, 10, 10));
          flash.setPreferredSize(new Dimension(150, 30));
          flash.setMinimumSize(new Dimension(150, 30));
          flash.setMaximumSize(new Dimension(150, 30));
          flash.setFont(new Font("sansserif", Font.PLAIN, 14));
          flash.setName("flash");
          flash.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              flashActionPerformed(e);
            }
          });
          sNPanelTitre1.add(flash, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbUV ----
          lbUV.setText("Unit\u00e9 de vente (UV)");
          lbUV.setName("lbUV");
          sNPanelTitre1.add(lbUV, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlQuantite ========
          {
            pnlQuantite.setName("pnlQuantite");
            pnlQuantite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlQuantite.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlQuantite.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlQuantite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlQuantite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- A1UNV ----
            A1UNV.setMinimumSize(new Dimension(50, 30));
            A1UNV.setPreferredSize(new Dimension(50, 30));
            A1UNV.setText("@A1UNV@");
            A1UNV.setEnabled(false);
            A1UNV.setName("A1UNV");
            pnlQuantite.add(A1UNV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbQuantite ----
            lbQuantite.setText("Quantit\u00e9");
            lbQuantite.setName("lbQuantite");
            pnlQuantite.add(lbQuantite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WQTEX ----
            WQTEX.setComponentPopupMenu(null);
            WQTEX.setHorizontalAlignment(SwingConstants.RIGHT);
            WQTEX.setMinimumSize(new Dimension(100, 30));
            WQTEX.setPreferredSize(new Dimension(100, 30));
            WQTEX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WQTEX.setName("WQTEX");
            pnlQuantite.add(WQTEX, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          sNPanelTitre1.add(pnlQuantite, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbTypeGratuit ----
          lbTypeGratuit.setText("Type de gratuit");
          lbTypeGratuit.setName("lbTypeGratuit");
          sNPanelTitre1.add(lbTypeGratuit, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- TGLIBR ----
          TGLIBR.setMinimumSize(new Dimension(300, 30));
          TGLIBR.setPreferredSize(new Dimension(300, 30));
          TGLIBR.setText("@TGLIBR@");
          TGLIBR.setEnabled(false);
          TGLIBR.setName("TGLIBR");
          sNPanelTitre1.add(TGLIBR, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbBase ----
          lbBase.setText("Prix de base");
          lbBase.setName("lbBase");
          sNPanelTitre1.add(lbBase, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WPVB ----
          WPVB.setComponentPopupMenu(null);
          WPVB.setHorizontalAlignment(SwingConstants.RIGHT);
          WPVB.setMinimumSize(new Dimension(100, 30));
          WPVB.setPreferredSize(new Dimension(100, 30));
          WPVB.setText("@WPVB@");
          WPVB.setFont(new Font("sansserif", Font.PLAIN, 14));
          WPVB.setEnabled(false);
          WPVB.setName("WPVB");
          sNPanelTitre1.add(WPVB, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbCoeff ----
          lbCoeff.setText("Coefficient");
          lbCoeff.setName("lbCoeff");
          sNPanelTitre1.add(lbCoeff, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlRemises ========
          {
            pnlRemises.setName("pnlRemises");
            pnlRemises.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRemises.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlRemises.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlRemises.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlRemises.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- WCOE ----
            WCOE.setComponentPopupMenu(null);
            WCOE.setHorizontalAlignment(SwingConstants.RIGHT);
            WCOE.setMinimumSize(new Dimension(100, 30));
            WCOE.setPreferredSize(new Dimension(100, 30));
            WCOE.setText("@WCOE@");
            WCOE.setFont(new Font("sansserif", Font.PLAIN, 14));
            WCOE.setName("WCOE");
            pnlRemises.add(WCOE, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbRemises ----
            lbRemises.setText("Remises");
            lbRemises.setName("lbRemises");
            pnlRemises.add(lbRemises, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- UREM1 ----
            UREM1.setComponentPopupMenu(null);
            UREM1.setHorizontalAlignment(SwingConstants.RIGHT);
            UREM1.setMinimumSize(new Dimension(60, 30));
            UREM1.setPreferredSize(new Dimension(60, 30));
            UREM1.setText("@UREM1@");
            UREM1.setFont(new Font("sansserif", Font.PLAIN, 14));
            UREM1.setName("UREM1");
            pnlRemises.add(UREM1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- UREM2 ----
            UREM2.setComponentPopupMenu(null);
            UREM2.setHorizontalAlignment(SwingConstants.RIGHT);
            UREM2.setMinimumSize(new Dimension(60, 30));
            UREM2.setPreferredSize(new Dimension(60, 30));
            UREM2.setText("@UREM2@");
            UREM2.setFont(new Font("sansserif", Font.PLAIN, 14));
            UREM2.setName("UREM2");
            pnlRemises.add(UREM2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- UREM3 ----
            UREM3.setComponentPopupMenu(null);
            UREM3.setHorizontalAlignment(SwingConstants.RIGHT);
            UREM3.setMinimumSize(new Dimension(60, 30));
            UREM3.setPreferredSize(new Dimension(60, 30));
            UREM3.setText("@UREM3@");
            UREM3.setFont(new Font("sansserif", Font.PLAIN, 14));
            UREM3.setName("UREM3");
            pnlRemises.add(UREM3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPrixNet ----
            lbPrixNet.setText("Prix net");
            lbPrixNet.setName("lbPrixNet");
            pnlRemises.add(lbPrixNet, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WPRX ----
            WPRX.setComponentPopupMenu(null);
            WPRX.setHorizontalAlignment(SwingConstants.RIGHT);
            WPRX.setMinimumSize(new Dimension(100, 30));
            WPRX.setPreferredSize(new Dimension(100, 30));
            WPRX.setText("@WPRX@");
            WPRX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WPRX.setName("WPRX");
            pnlRemises.add(WPRX, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanelTitre1.add(pnlRemises, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPrixNegocie ----
          lbPrixNegocie.setText("Prix n\u00e9goci\u00e9");
          lbPrixNegocie.setName("lbPrixNegocie");
          sNPanelTitre1.add(lbPrixNegocie, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlPrix ========
          {
            pnlPrix.setName("pnlPrix");
            pnlPrix.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPrix.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlPrix.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPrix.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPrix.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- WPVF ----
            WPVF.setComponentPopupMenu(null);
            WPVF.setHorizontalAlignment(SwingConstants.RIGHT);
            WPVF.setMinimumSize(new Dimension(100, 30));
            WPVF.setPreferredSize(new Dimension(100, 30));
            WPVF.setText("@WPVF@");
            WPVF.setFont(new Font("sansserif", Font.PLAIN, 14));
            WPVF.setName("WPVF");
            pnlPrix.add(WPVF, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPrixTTC ----
            lbPrixTTC.setText("Prix public TTC");
            lbPrixTTC.setName("lbPrixTTC");
            pnlPrix.add(lbPrixTTC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WPRX2 ----
            WPRX2.setComponentPopupMenu(null);
            WPRX2.setHorizontalAlignment(SwingConstants.RIGHT);
            WPRX2.setMinimumSize(new Dimension(100, 30));
            WPRX2.setPreferredSize(new Dimension(100, 30));
            WPRX2.setText("@WPRX2@");
            WPRX2.setFont(new Font("sansserif", Font.PLAIN, 14));
            WPRX2.setName("WPRX2");
            pnlPrix.add(WPRX2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          sNPanelTitre1.add(pnlPrix, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlContenu.add(sNPanelTitre1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== sNPanelTitre2 ========
        {
          sNPanelTitre2.setTitre("Position stock globale sur magasin");
          sNPanelTitre2.setName("sNPanelTitre2");
          sNPanelTitre2.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanelTitre2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre2.getLayout()).columnWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanelTitre2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          sNPanelTitre2.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin ----
          snMagasin.setName("snMagasin");
          sNPanelTitre2.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbQuantiteStock ----
          lbQuantiteStock.setText("Quantit\u00e9 en stock");
          lbQuantiteStock.setName("lbQuantiteStock");
          sNPanelTitre2.add(lbQuantiteStock, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlStocks ========
          {
            pnlStocks.setName("pnlStocks");
            pnlStocks.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlStocks.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlStocks.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlStocks.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlStocks.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- WSTKX ----
            WSTKX.setComponentPopupMenu(null);
            WSTKX.setHorizontalAlignment(SwingConstants.RIGHT);
            WSTKX.setMinimumSize(new Dimension(100, 30));
            WSTKX.setPreferredSize(new Dimension(100, 30));
            WSTKX.setText("@WSTKX@");
            WSTKX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WSTKX.setEnabled(false);
            WSTKX.setName("WSTKX");
            pnlStocks.add(WSTKX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- A1UNS ----
            A1UNS.setMinimumSize(new Dimension(50, 30));
            A1UNS.setPreferredSize(new Dimension(50, 30));
            A1UNS.setText("@A1UNS@");
            A1UNS.setEnabled(false);
            A1UNS.setName("A1UNS");
            pnlStocks.add(A1UNS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- OBJ_133 ----
            OBJ_133.setText("-");
            OBJ_133.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_133.setFont(new Font("sansserif", Font.BOLD, 28));
            OBJ_133.setMaximumSize(new Dimension(30, 30));
            OBJ_133.setMinimumSize(new Dimension(30, 30));
            OBJ_133.setPreferredSize(new Dimension(30, 30));
            OBJ_133.setName("OBJ_133");
            pnlStocks.add(OBJ_133, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbQuantiteReserve ----
            lbQuantiteReserve.setText("Reserv\u00e9 client");
            lbQuantiteReserve.setMaximumSize(new Dimension(100, 30));
            lbQuantiteReserve.setMinimumSize(new Dimension(100, 30));
            lbQuantiteReserve.setPreferredSize(new Dimension(100, 30));
            lbQuantiteReserve.setName("lbQuantiteReserve");
            pnlStocks.add(lbQuantiteReserve, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WRESX ----
            WRESX.setComponentPopupMenu(null);
            WRESX.setHorizontalAlignment(SwingConstants.RIGHT);
            WRESX.setMinimumSize(new Dimension(100, 30));
            WRESX.setPreferredSize(new Dimension(100, 30));
            WRESX.setText("@WRESX@");
            WRESX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WRESX.setEnabled(false);
            WRESX.setName("WRESX");
            pnlStocks.add(WRESX, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- OBJ_131 ----
            OBJ_131.setText("=");
            OBJ_131.setFont(new Font("sansserif", Font.BOLD, 28));
            OBJ_131.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_131.setMinimumSize(new Dimension(30, 30));
            OBJ_131.setMaximumSize(new Dimension(30, 30));
            OBJ_131.setPreferredSize(new Dimension(30, 30));
            OBJ_131.setName("OBJ_131");
            pnlStocks.add(OBJ_131, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbDispo ----
            lbDispo.setText("Disponible");
            lbDispo.setMinimumSize(new Dimension(100, 30));
            lbDispo.setMaximumSize(new Dimension(100, 30));
            lbDispo.setPreferredSize(new Dimension(100, 30));
            lbDispo.setName("lbDispo");
            pnlStocks.add(lbDispo, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WDISX ----
            WDISX.setComponentPopupMenu(null);
            WDISX.setHorizontalAlignment(SwingConstants.RIGHT);
            WDISX.setMinimumSize(new Dimension(100, 30));
            WDISX.setPreferredSize(new Dimension(100, 30));
            WDISX.setText("@WDISX@");
            WDISX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDISX.setEnabled(false);
            WDISX.setName("WDISX");
            pnlStocks.add(WDISX, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPasDispo ----
            lbPasDispo.setText("Pas de disponible");
            lbPasDispo.setFont(new Font("sansserif", Font.BOLD, 14));
            lbPasDispo.setForeground(Color.red);
            lbPasDispo.setHorizontalAlignment(SwingConstants.RIGHT);
            lbPasDispo.setMaximumSize(new Dimension(150, 30));
            lbPasDispo.setMinimumSize(new Dimension(150, 30));
            lbPasDispo.setPreferredSize(new Dimension(150, 30));
            lbPasDispo.setName("lbPasDispo");
            pnlStocks.add(lbPasDispo, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanelTitre2.add(pnlStocks, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbAttendu ----
          lbAttendu.setText("Attendu");
          lbAttendu.setName("lbAttendu");
          sNPanelTitre2.add(lbAttendu, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WATTX ----
          WATTX.setComponentPopupMenu(null);
          WATTX.setHorizontalAlignment(SwingConstants.RIGHT);
          WATTX.setMinimumSize(new Dimension(100, 30));
          WATTX.setPreferredSize(new Dimension(100, 30));
          WATTX.setText("@WATTX@");
          WATTX.setFont(new Font("sansserif", Font.PLAIN, 14));
          WATTX.setEnabled(false);
          WATTX.setName("WATTX");
          sNPanelTitre2.add(WATTX, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlContenu.add(sNPanelTitre2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie WART;
  private JLabel OBJ_61;
  private RiZoneSortie WETB2;
  private JLabel OBJ_63;
  private JLabel OBJ_27;
  private RiZoneSortie CLNOM;
  private RiZoneSortie CLCLI;
  private RiZoneSortie CLLIV;
  private JPanel p_tete_droite;
  private SNPanelFond p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu_bt bouton_valider2;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private RiMenu_bt bouton_retour2;
  private RiMenu navig_retour2;
  private RiMenu_bt bouton_retour4;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlArticle;
  private SNTexte A1LIB;
  private SNLabelChamp lbClassement1;
  private SNTexte A1CL1;
  private SNTexte A1LB1;
  private SNLabelChamp lbClassement2;
  private SNTexte A1CL2;
  private SNTexte A1LB2;
  private SNTexte A1LB3;
  private SNLabelChamp lbFournisseur;
  private SNTexte FRNOM;
  private SNPanelTitre sNPanelTitre1;
  private SNLabelChamp lbCondition;
  private SNConditionVenteEtClient snConditionVenteEtClient;
  private SNBoutonDetail flash;
  private SNLabelChamp lbUV;
  private SNPanel pnlQuantite;
  private SNTexte A1UNV;
  private SNLabelChamp lbQuantite;
  private XRiTextField WQTEX;
  private SNLabelChamp lbTypeGratuit;
  private SNTexte TGLIBR;
  private SNLabelChamp lbBase;
  private XRiTextField WPVB;
  private SNLabelChamp lbCoeff;
  private SNPanel pnlRemises;
  private XRiTextField WCOE;
  private SNLabelChamp lbRemises;
  private XRiTextField UREM1;
  private XRiTextField UREM2;
  private XRiTextField UREM3;
  private SNLabelChamp lbPrixNet;
  private XRiTextField WPRX;
  private SNLabelChamp lbPrixNegocie;
  private SNPanel pnlPrix;
  private XRiTextField WPVF;
  private SNLabelChamp lbPrixTTC;
  private XRiTextField WPRX2;
  private SNPanelTitre sNPanelTitre2;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbQuantiteStock;
  private SNPanel pnlStocks;
  private XRiTextField WSTKX;
  private SNTexte A1UNS;
  private JLabel OBJ_133;
  private SNLabelChamp lbQuantiteReserve;
  private XRiTextField WRESX;
  private JLabel OBJ_131;
  private SNLabelChamp lbDispo;
  private XRiTextField WDISX;
  private SNLabelUnite lbPasDispo;
  private SNLabelChamp lbAttendu;
  private XRiTextField WATTX;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
