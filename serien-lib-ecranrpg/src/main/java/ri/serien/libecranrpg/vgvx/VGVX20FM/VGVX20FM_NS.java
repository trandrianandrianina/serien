
package ri.serien.libecranrpg.vgvx.VGVX20FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGVX20FM_NS extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX20FM_NS(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    A1ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1ART@")).trim());
    A1ARTR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1ARTR@")).trim());
    NUM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM1@")).trim());
    NUM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM2@")).trim());
    NUM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM3@")).trim());
    NUM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM4@")).trim());
    NUM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM5@")).trim());
    NUM6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM6@")).trim());
    NUM7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM7@")).trim());
    NUM8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM8@")).trim());
    NUM9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM9@")).trim());
    NUM10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM10@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    btErr1.setVisible(lexique.isTrue("31"));
    btErr2.setVisible(lexique.isTrue("32"));
    btErr3.setVisible(lexique.isTrue("33"));
    btErr4.setVisible(lexique.isTrue("34"));
    btErr5.setVisible(lexique.isTrue("35"));
    btErr6.setVisible(lexique.isTrue("36"));
    btErr7.setVisible(lexique.isTrue("37"));
    btErr8.setVisible(lexique.isTrue("38"));
    btErr9.setVisible(lexique.isTrue("39"));
    btErr10.setVisible(lexique.isTrue("40"));
    
    // TODO Icones
    btErr1.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    btErr2.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    btErr3.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    btErr4.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    btErr5.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    btErr6.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    btErr7.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    btErr8.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    btErr9.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    btErr10.setIcon(lexique.chargerImage("images/erreurs_N.png", true));
    
    // V07F
    
    // Titre
    // setTitle(???);
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    BT_PGDOWN = new JButton();
    BT_PGUP = new JButton();
    panel2 = new JPanel();
    OBJ_18 = new JLabel();
    A1ART = new RiZoneSortie();
    A1ARTR = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    NUM1 = new RiZoneSortie();
    ENS1 = new XRiTextField();
    ENS1M = new XRiTextField();
    NUM2 = new RiZoneSortie();
    ENS2 = new XRiTextField();
    ENS2M = new XRiTextField();
    NUM3 = new RiZoneSortie();
    ENS3 = new XRiTextField();
    ENS3M = new XRiTextField();
    NUM4 = new RiZoneSortie();
    ENS4 = new XRiTextField();
    ENS4M = new XRiTextField();
    NUM5 = new RiZoneSortie();
    ENS5 = new XRiTextField();
    ENS5M = new XRiTextField();
    NUM6 = new RiZoneSortie();
    ENS6 = new XRiTextField();
    ENS6M = new XRiTextField();
    NUM7 = new RiZoneSortie();
    ENS7 = new XRiTextField();
    ENS7M = new XRiTextField();
    NUM8 = new RiZoneSortie();
    ENS8 = new XRiTextField();
    ENS8M = new XRiTextField();
    NUM9 = new RiZoneSortie();
    ENS9 = new XRiTextField();
    ENS9M = new XRiTextField();
    NUM10 = new RiZoneSortie();
    ENS10 = new XRiTextField();
    ENS10M = new XRiTextField();
    btErr1 = new JLabel();
    btErr2 = new JLabel();
    btErr3 = new JLabel();
    btErr4 = new JLabel();
    btErr5 = new JLabel();
    btErr6 = new JLabel();
    btErr7 = new JLabel();
    btErr8 = new JLabel();
    btErr9 = new JLabel();
    btErr10 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(780, 390));
    setPreferredSize(new Dimension(780, 390));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel1.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(545, 230, 25, 120);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel1.add(BT_PGUP);
          BT_PGUP.setBounds(545, 95, 25, 120);

          //======== panel2 ========
          {
            panel2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_18 ----
            OBJ_18.setText("Saisie num\u00e9ros de s\u00e9rie pour l'article");
            OBJ_18.setName("OBJ_18");
            panel2.add(OBJ_18);
            OBJ_18.setBounds(10, 8, 226, 24);

            //---- A1ART ----
            A1ART.setText("@A1ART@");
            A1ART.setName("A1ART");
            panel2.add(A1ART);
            A1ART.setBounds(240, 8, 210, A1ART.getPreferredSize().height);

            //---- A1ARTR ----
            A1ARTR.setText("@A1ARTR@");
            A1ARTR.setName("A1ARTR");
            panel2.add(A1ARTR);
            A1ARTR.setBounds(240, 8, 110, A1ARTR.getPreferredSize().height);
          }
          panel1.add(panel2);
          panel2.setBounds(20, 15, 550, 40);

          //---- label1 ----
          label1.setText("Num\u00e9ro");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(45, 70, 60, 25);

          //---- label2 ----
          label2.setText("Num\u00e9ro de s\u00e9rie");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(118, 70, 214, 25);

          //---- label3 ----
          label3.setText("Marques");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(440, 70, 85, 25);

          //---- NUM1 ----
          NUM1.setText("@NUM1@");
          NUM1.setName("NUM1");
          panel1.add(NUM1);
          NUM1.setBounds(45, 97, 60, NUM1.getPreferredSize().height);

          //---- ENS1 ----
          ENS1.setName("ENS1");
          panel1.add(ENS1);
          ENS1.setBounds(118, 95, 310, ENS1.getPreferredSize().height);

          //---- ENS1M ----
          ENS1M.setName("ENS1M");
          panel1.add(ENS1M);
          ENS1M.setBounds(440, 95, 85, ENS1M.getPreferredSize().height);

          //---- NUM2 ----
          NUM2.setText("@NUM2@");
          NUM2.setName("NUM2");
          panel1.add(NUM2);
          NUM2.setBounds(45, 122, 60, NUM2.getPreferredSize().height);

          //---- ENS2 ----
          ENS2.setName("ENS2");
          panel1.add(ENS2);
          ENS2.setBounds(118, 120, 310, ENS2.getPreferredSize().height);

          //---- ENS2M ----
          ENS2M.setName("ENS2M");
          panel1.add(ENS2M);
          ENS2M.setBounds(440, 120, 85, ENS2M.getPreferredSize().height);

          //---- NUM3 ----
          NUM3.setText("@NUM3@");
          NUM3.setName("NUM3");
          panel1.add(NUM3);
          NUM3.setBounds(45, 147, 60, NUM3.getPreferredSize().height);

          //---- ENS3 ----
          ENS3.setName("ENS3");
          panel1.add(ENS3);
          ENS3.setBounds(118, 145, 310, ENS3.getPreferredSize().height);

          //---- ENS3M ----
          ENS3M.setName("ENS3M");
          panel1.add(ENS3M);
          ENS3M.setBounds(440, 145, 85, ENS3M.getPreferredSize().height);

          //---- NUM4 ----
          NUM4.setText("@NUM4@");
          NUM4.setName("NUM4");
          panel1.add(NUM4);
          NUM4.setBounds(45, 172, 60, NUM4.getPreferredSize().height);

          //---- ENS4 ----
          ENS4.setName("ENS4");
          panel1.add(ENS4);
          ENS4.setBounds(118, 170, 310, ENS4.getPreferredSize().height);

          //---- ENS4M ----
          ENS4M.setName("ENS4M");
          panel1.add(ENS4M);
          ENS4M.setBounds(440, 170, 85, ENS4M.getPreferredSize().height);

          //---- NUM5 ----
          NUM5.setText("@NUM5@");
          NUM5.setName("NUM5");
          panel1.add(NUM5);
          NUM5.setBounds(45, 197, 60, NUM5.getPreferredSize().height);

          //---- ENS5 ----
          ENS5.setName("ENS5");
          panel1.add(ENS5);
          ENS5.setBounds(118, 195, 310, ENS5.getPreferredSize().height);

          //---- ENS5M ----
          ENS5M.setName("ENS5M");
          panel1.add(ENS5M);
          ENS5M.setBounds(440, 195, 85, ENS5M.getPreferredSize().height);

          //---- NUM6 ----
          NUM6.setText("@NUM6@");
          NUM6.setName("NUM6");
          panel1.add(NUM6);
          NUM6.setBounds(45, 222, 60, NUM6.getPreferredSize().height);

          //---- ENS6 ----
          ENS6.setName("ENS6");
          panel1.add(ENS6);
          ENS6.setBounds(118, 220, 310, ENS6.getPreferredSize().height);

          //---- ENS6M ----
          ENS6M.setName("ENS6M");
          panel1.add(ENS6M);
          ENS6M.setBounds(440, 220, 85, ENS6M.getPreferredSize().height);

          //---- NUM7 ----
          NUM7.setText("@NUM7@");
          NUM7.setName("NUM7");
          panel1.add(NUM7);
          NUM7.setBounds(45, 247, 60, NUM7.getPreferredSize().height);

          //---- ENS7 ----
          ENS7.setName("ENS7");
          panel1.add(ENS7);
          ENS7.setBounds(118, 245, 310, ENS7.getPreferredSize().height);

          //---- ENS7M ----
          ENS7M.setName("ENS7M");
          panel1.add(ENS7M);
          ENS7M.setBounds(440, 245, 85, ENS7M.getPreferredSize().height);

          //---- NUM8 ----
          NUM8.setText("@NUM8@");
          NUM8.setName("NUM8");
          panel1.add(NUM8);
          NUM8.setBounds(45, 272, 60, NUM8.getPreferredSize().height);

          //---- ENS8 ----
          ENS8.setName("ENS8");
          panel1.add(ENS8);
          ENS8.setBounds(118, 270, 310, ENS8.getPreferredSize().height);

          //---- ENS8M ----
          ENS8M.setName("ENS8M");
          panel1.add(ENS8M);
          ENS8M.setBounds(440, 270, 85, ENS8M.getPreferredSize().height);

          //---- NUM9 ----
          NUM9.setText("@NUM9@");
          NUM9.setName("NUM9");
          panel1.add(NUM9);
          NUM9.setBounds(45, 297, 60, NUM9.getPreferredSize().height);

          //---- ENS9 ----
          ENS9.setName("ENS9");
          panel1.add(ENS9);
          ENS9.setBounds(118, 295, 310, ENS9.getPreferredSize().height);

          //---- ENS9M ----
          ENS9M.setName("ENS9M");
          panel1.add(ENS9M);
          ENS9M.setBounds(440, 295, 85, ENS9M.getPreferredSize().height);

          //---- NUM10 ----
          NUM10.setText("@NUM10@");
          NUM10.setName("NUM10");
          panel1.add(NUM10);
          NUM10.setBounds(45, 322, 60, NUM10.getPreferredSize().height);

          //---- ENS10 ----
          ENS10.setName("ENS10");
          panel1.add(ENS10);
          ENS10.setBounds(118, 320, 310, ENS10.getPreferredSize().height);

          //---- ENS10M ----
          ENS10M.setName("ENS10M");
          panel1.add(ENS10M);
          ENS10M.setBounds(440, 320, 85, ENS10M.getPreferredSize().height);

          //---- btErr1 ----
          btErr1.setText("text");
          btErr1.setName("btErr1");
          panel1.add(btErr1);
          btErr1.setBounds(15, 97, 30, 24);

          //---- btErr2 ----
          btErr2.setText("text");
          btErr2.setName("btErr2");
          panel1.add(btErr2);
          btErr2.setBounds(15, 122, 30, 24);

          //---- btErr3 ----
          btErr3.setText("text");
          btErr3.setName("btErr3");
          panel1.add(btErr3);
          btErr3.setBounds(15, 147, 30, 24);

          //---- btErr4 ----
          btErr4.setText("text");
          btErr4.setName("btErr4");
          panel1.add(btErr4);
          btErr4.setBounds(15, 172, 30, 24);

          //---- btErr5 ----
          btErr5.setText("text");
          btErr5.setName("btErr5");
          panel1.add(btErr5);
          btErr5.setBounds(15, 197, 30, 24);

          //---- btErr6 ----
          btErr6.setText("text");
          btErr6.setName("btErr6");
          panel1.add(btErr6);
          btErr6.setBounds(15, 222, 30, 24);

          //---- btErr7 ----
          btErr7.setText("text");
          btErr7.setName("btErr7");
          panel1.add(btErr7);
          btErr7.setBounds(15, 247, 30, 24);

          //---- btErr8 ----
          btErr8.setText("text");
          btErr8.setName("btErr8");
          panel1.add(btErr8);
          btErr8.setBounds(15, 272, 30, 24);

          //---- btErr9 ----
          btErr9.setText("text");
          btErr9.setName("btErr9");
          panel1.add(btErr9);
          btErr9.setBounds(15, 297, 30, 24);

          //---- btErr10 ----
          btErr10.setText("text");
          btErr10.setName("btErr10");
          panel1.add(btErr10);
          btErr10.setBounds(15, 322, 30, 24);
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 590, 370);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JButton BT_PGDOWN;
  private JButton BT_PGUP;
  private JPanel panel2;
  private JLabel OBJ_18;
  private RiZoneSortie A1ART;
  private RiZoneSortie A1ARTR;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private RiZoneSortie NUM1;
  private XRiTextField ENS1;
  private XRiTextField ENS1M;
  private RiZoneSortie NUM2;
  private XRiTextField ENS2;
  private XRiTextField ENS2M;
  private RiZoneSortie NUM3;
  private XRiTextField ENS3;
  private XRiTextField ENS3M;
  private RiZoneSortie NUM4;
  private XRiTextField ENS4;
  private XRiTextField ENS4M;
  private RiZoneSortie NUM5;
  private XRiTextField ENS5;
  private XRiTextField ENS5M;
  private RiZoneSortie NUM6;
  private XRiTextField ENS6;
  private XRiTextField ENS6M;
  private RiZoneSortie NUM7;
  private XRiTextField ENS7;
  private XRiTextField ENS7M;
  private RiZoneSortie NUM8;
  private XRiTextField ENS8;
  private XRiTextField ENS8M;
  private RiZoneSortie NUM9;
  private XRiTextField ENS9;
  private XRiTextField ENS9M;
  private RiZoneSortie NUM10;
  private XRiTextField ENS10;
  private XRiTextField ENS10M;
  private JLabel btErr1;
  private JLabel btErr2;
  private JLabel btErr3;
  private JLabel btErr4;
  private JLabel btErr5;
  private JLabel btErr6;
  private JLabel btErr7;
  private JLabel btErr8;
  private JLabel btErr9;
  private JLabel btErr10;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
