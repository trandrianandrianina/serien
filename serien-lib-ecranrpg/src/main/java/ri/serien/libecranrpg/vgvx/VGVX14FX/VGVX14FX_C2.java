
package ri.serien.libecranrpg.vgvx.VGVX14FX;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX14FX_C2 extends SNPanelEcranRPG implements ioFrame {
  
  // Boutons
  private static final String BOUTON_STOCK = "Accéder aux stocks";
  private static final String BOUTON_LOTS = "Gérer les lots";
  
  public VGVX14FX_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WSER.setValeursSelection("L", " ");
    WSGN.setValeurs("+", WSGN_GRP);
    WSGN_MOINS.setValeurs("-");
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_STOCK, 's', false);
    snBarreBouton.ajouterBouton(BOUTON_LOTS, 'l', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbUnite.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1UNS@")).trim());
    lbUTITP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UTITP@")).trim());
    WLBMA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLBMA2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    if (!lexique.isTrue("76")) {
      WSER.setText("Lots");
      WSER.setValeursSelection("L", " ");
    }
    else {
      WSER.setText("Série");
      WSER.setValeursSelection("S", " ");
    }
    
    WSER.setVisible(!lexique.HostFieldGetData("A1IN2").trim().equalsIgnoreCase(""));
    
    WSGN_MOINS.setEnabled(WSGN.isEnabled());
    
    checkBox1.setSelected(lexique.HostFieldGetData("L1MDP").trim().equals("1"));
    checkBox1.setVisible(lexique.isTrue("(N22) AND (N26) AND (N72)"));
    checkBox2.setVisible(checkBox1.isVisible() & lexique.isTrue("(33)"));
    checkBox2.setSelected(lexique.HostFieldGetData("L1MDP").trim().equals("2"));
    
    L1CNDR.setVisible(lexique.isTrue("(N23) AND (N26) AND (N72)"));
    L1QTPX.setVisible(lexique.isTrue("(N08) AND (23)"));
    if (L1CNDR.isVisible()) {
      lbCoeff.setText("Coefficient");
    }
    else if (L1QTPX.isVisible()) {
      lbCoeff.setText("Pièces");
    }
    else {
      lbCoeff.setText("");
    }
    
    WLBMA2.setVisible(lexique.isTrue("(23) AND (26)"));
    L1QTEX.setVisible(lexique.isTrue("(N26) AND (N72)"));
    lbQuantite.setVisible(L1QTEX.isVisible());
    lbUnite.setVisible(L1QTEX.isVisible());
    lbArticleSubstitution.setVisible(WASB.isVisible());
    lbDateInventaire.setVisible(DATINV.isVisible());
    lbUTITP.setVisible(L1PHTX.isVisible());
    pnlSens.setVisible(lexique.isPresent("WSGN"));
    
    lbDisponible.setVisible(lexique.isPresent("WDISX"));
    lbAttendu.setVisible(lexique.isPresent("WATTX"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("STOCKS"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
    if (checkBox1.isSelected()) {
      lexique.HostFieldPutData("L1MDP", 0, "1");
    }
    else if (checkBox2.isSelected()) {
      lexique.HostFieldPutData("L1MDP", 0, "2");
    }
    else {
      lexique.HostFieldPutData("L1MDP", 0, " ");
    }
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_STOCK)) {
        lexique.HostCursorPut(15, 2);
        lexique.HostScreenSendKey(this, "F4");
      }
      else if (pSNBouton.isBouton(BOUTON_LOTS)) {
        lexique.HostFieldPutData("WSER", 0, "L");
        lexique.HostScreenSendKey(this, "ENTER");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void checkBox1ActionPerformed(ActionEvent e) {
    if (checkBox1.isSelected()) {
      checkBox2.setSelected(false);
    }
  }
  
  private void checkBox2ActionPerformed(ActionEvent e) {
    if (checkBox2.isSelected()) {
      checkBox1.setSelected(false);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlDate = new SNPanel();
    lbDateStock = new SNLabelChamp();
    WDATX = new XRiCalendrier();
    lbDateInventaire = new SNLabelChamp();
    DATINV = new XRiCalendrier();
    sNPanelTitre1 = new SNPanelTitre();
    lbStock = new SNLabelUnite();
    lbCommande = new SNLabelUnite();
    lbReserve = new SNLabelUnite();
    lbDisponible = new SNLabelUnite();
    lbAttendu = new SNLabelUnite();
    WSTKX = new XRiTextField();
    lbMoins = new SNLabelChamp();
    WRESX = new XRiTextField();
    WAFFX = new XRiTextField();
    lbEgal = new SNLabelChamp();
    WDISX = new XRiTextField();
    WATTX = new XRiTextField();
    sNLabelChamp1 = new SNLabelChamp();
    X15PM1 = new XRiTextField();
    pnlLigne = new SNPanelTitre();
    lbWCOD = new SNLabelChamp();
    WCOD = new XRiTextField();
    lbWNLI = new SNLabelChamp();
    WNLI = new XRiTextField();
    lbArticle = new SNLabelChamp();
    WARTT = new XRiTextField();
    A1LIB = new XRiTextField();
    WSER = new XRiCheckBox();
    lbDate = new SNLabelChamp();
    L1DATX = new XRiCalendrier();
    pnlSaisieLigne = new SNPanelTitre();
    pnlSaisie = new SNPanel();
    lbQuantite = new SNLabelChamp();
    L1QTEX = new XRiTextField();
    lbUnite = new SNLabelUnite();
    lbCoeff = new SNLabelChamp();
    L1CNDR = new XRiTextField();
    L1QTPX = new XRiTextField();
    pnlSens = new SNPanel();
    lbSens = new SNLabelChamp();
    WSGN = new XRiRadioButton();
    WSGN_MOINS = new XRiRadioButton();
    pnlCommentaire = new SNPanel();
    lbCommentaire = new SNLabelChamp();
    L1OBS = new XRiTextField();
    pnlSubstitution = new SNPanel();
    lbUTITP = new SNLabelChamp();
    L1PHTX = new XRiTextField();
    lbArticleSubstitution = new SNLabelChamp();
    WASB = new XRiTextField();
    WLBMA2 = new SNLabelUnite();
    pnlOptions = new SNPanel();
    checkBox1 = new JCheckBox();
    checkBox2 = new JCheckBox();
    WSGN_GRP = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1090, 515));
    setPreferredSize(new Dimension(1090, 515));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlDate ========
      {
        pnlDate.setName("pnlDate");
        pnlDate.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDate.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlDate.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlDate.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlDate.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbDateStock ----
        lbDateStock.setText("Position en stock au");
        lbDateStock.setName("lbDateStock");
        pnlDate.add(lbDateStock, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WDATX ----
        WDATX.setPreferredSize(new Dimension(110, 30));
        WDATX.setMinimumSize(new Dimension(110, 30));
        WDATX.setMaximumSize(new Dimension(110, 30));
        WDATX.setFont(new Font("sansserif", Font.PLAIN, 14));
        WDATX.setName("WDATX");
        pnlDate.add(WDATX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbDateInventaire ----
        lbDateInventaire.setText("Dernier inventaire le");
        lbDateInventaire.setPreferredSize(new Dimension(200, 30));
        lbDateInventaire.setMinimumSize(new Dimension(200, 30));
        lbDateInventaire.setMaximumSize(new Dimension(200, 30));
        lbDateInventaire.setName("lbDateInventaire");
        pnlDate.add(lbDateInventaire, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- DATINV ----
        DATINV.setPreferredSize(new Dimension(110, 30));
        DATINV.setMinimumSize(new Dimension(110, 30));
        DATINV.setMaximumSize(new Dimension(110, 30));
        DATINV.setFont(new Font("sansserif", Font.PLAIN, 14));
        DATINV.setName("DATINV");
        pnlDate.add(DATINV, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDate,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== sNPanelTitre1 ========
      {
        sNPanelTitre1.setTitre("Stocks");
        sNPanelTitre1.setName("sNPanelTitre1");
        sNPanelTitre1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelTitre1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) sNPanelTitre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbStock ----
        lbStock.setText("En stock");
        lbStock.setName("lbStock");
        sNPanelTitre1.add(lbStock, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbCommande ----
        lbCommande.setText("Command\u00e9");
        lbCommande.setName("lbCommande");
        sNPanelTitre1.add(lbCommande, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbReserve ----
        lbReserve.setText("(dont r\u00e9serv\u00e9)");
        lbReserve.setName("lbReserve");
        sNPanelTitre1.add(lbReserve, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDisponible ----
        lbDisponible.setText("Disponible");
        lbDisponible.setName("lbDisponible");
        sNPanelTitre1.add(lbDisponible, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbAttendu ----
        lbAttendu.setText("Attendu");
        lbAttendu.setName("lbAttendu");
        sNPanelTitre1.add(lbAttendu, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WSTKX ----
        WSTKX.setPreferredSize(new Dimension(120, 30));
        WSTKX.setMinimumSize(new Dimension(120, 30));
        WSTKX.setHorizontalAlignment(SwingConstants.TRAILING);
        WSTKX.setFont(new Font("sansserif", Font.PLAIN, 14));
        WSTKX.setName("WSTKX");
        sNPanelTitre1.add(WSTKX, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbMoins ----
        lbMoins.setText("-");
        lbMoins.setMinimumSize(new Dimension(30, 30));
        lbMoins.setMaximumSize(new Dimension(30, 30));
        lbMoins.setPreferredSize(new Dimension(30, 30));
        lbMoins.setHorizontalAlignment(SwingConstants.CENTER);
        lbMoins.setName("lbMoins");
        sNPanelTitre1.add(lbMoins, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WRESX ----
        WRESX.setPreferredSize(new Dimension(120, 30));
        WRESX.setMinimumSize(new Dimension(120, 30));
        WRESX.setHorizontalAlignment(SwingConstants.TRAILING);
        WRESX.setFont(new Font("sansserif", Font.PLAIN, 14));
        WRESX.setName("WRESX");
        sNPanelTitre1.add(WRESX, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WAFFX ----
        WAFFX.setPreferredSize(new Dimension(120, 30));
        WAFFX.setMinimumSize(new Dimension(120, 30));
        WAFFX.setHorizontalAlignment(SwingConstants.TRAILING);
        WAFFX.setFont(new Font("sansserif", Font.PLAIN, 14));
        WAFFX.setName("WAFFX");
        sNPanelTitre1.add(WAFFX, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbEgal ----
        lbEgal.setText("=");
        lbEgal.setMinimumSize(new Dimension(30, 30));
        lbEgal.setMaximumSize(new Dimension(30, 30));
        lbEgal.setPreferredSize(new Dimension(30, 30));
        lbEgal.setHorizontalAlignment(SwingConstants.CENTER);
        lbEgal.setName("lbEgal");
        sNPanelTitre1.add(lbEgal, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WDISX ----
        WDISX.setPreferredSize(new Dimension(120, 30));
        WDISX.setMinimumSize(new Dimension(120, 30));
        WDISX.setHorizontalAlignment(SwingConstants.TRAILING);
        WDISX.setFont(new Font("sansserif", Font.PLAIN, 14));
        WDISX.setName("WDISX");
        sNPanelTitre1.add(WDISX, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WATTX ----
        WATTX.setPreferredSize(new Dimension(120, 30));
        WATTX.setMinimumSize(new Dimension(120, 30));
        WATTX.setHorizontalAlignment(SwingConstants.TRAILING);
        WATTX.setFont(new Font("sansserif", Font.PLAIN, 14));
        WATTX.setName("WATTX");
        sNPanelTitre1.add(WATTX, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- sNLabelChamp1 ----
        sNLabelChamp1.setText("PUMP");
        sNLabelChamp1.setMaximumSize(new Dimension(50, 30));
        sNLabelChamp1.setMinimumSize(new Dimension(50, 30));
        sNLabelChamp1.setPreferredSize(new Dimension(50, 30));
        sNLabelChamp1.setName("sNLabelChamp1");
        sNPanelTitre1.add(sNLabelChamp1, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- X15PM1 ----
        X15PM1.setPreferredSize(new Dimension(120, 30));
        X15PM1.setMinimumSize(new Dimension(120, 30));
        X15PM1.setHorizontalAlignment(SwingConstants.TRAILING);
        X15PM1.setFont(new Font("sansserif", Font.PLAIN, 14));
        X15PM1.setName("X15PM1");
        sNPanelTitre1.add(X15PM1, new GridBagConstraints(8, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(sNPanelTitre1,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlLigne ========
      {
        pnlLigne.setTitre("Ligne");
        pnlLigne.setName("pnlLigne");
        pnlLigne.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlLigne.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlLigne.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlLigne.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlLigne.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbWCOD ----
        lbWCOD.setText("Code ");
        lbWCOD.setMaximumSize(new Dimension(40, 30));
        lbWCOD.setMinimumSize(new Dimension(40, 30));
        lbWCOD.setPreferredSize(new Dimension(40, 30));
        lbWCOD.setName("lbWCOD");
        pnlLigne.add(lbWCOD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WCOD ----
        WCOD.setMaximumSize(new Dimension(24, 28));
        WCOD.setMinimumSize(new Dimension(24, 28));
        WCOD.setPreferredSize(new Dimension(24, 28));
        WCOD.setFont(new Font("sansserif", Font.PLAIN, 14));
        WCOD.setName("WCOD");
        pnlLigne.add(WCOD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbWNLI ----
        lbWNLI.setText("Ligne");
        lbWNLI.setRequestFocusEnabled(false);
        lbWNLI.setPreferredSize(new Dimension(60, 30));
        lbWNLI.setMinimumSize(new Dimension(60, 30));
        lbWNLI.setMaximumSize(new Dimension(60, 30));
        lbWNLI.setName("lbWNLI");
        pnlLigne.add(lbWNLI, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WNLI ----
        WNLI.setMaximumSize(new Dimension(44, 28));
        WNLI.setMinimumSize(new Dimension(44, 28));
        WNLI.setPreferredSize(new Dimension(44, 28));
        WNLI.setRequestFocusEnabled(false);
        WNLI.setFont(new Font("sansserif", Font.PLAIN, 14));
        WNLI.setName("WNLI");
        pnlLigne.add(WNLI, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbArticle ----
        lbArticle.setText("Article");
        lbArticle.setRequestFocusEnabled(false);
        lbArticle.setPreferredSize(new Dimension(60, 30));
        lbArticle.setMinimumSize(new Dimension(60, 30));
        lbArticle.setMaximumSize(new Dimension(60, 30));
        lbArticle.setName("lbArticle");
        pnlLigne.add(lbArticle, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WARTT ----
        WARTT.setMaximumSize(new Dimension(210, 30));
        WARTT.setMinimumSize(new Dimension(210, 30));
        WARTT.setPreferredSize(new Dimension(210, 30));
        WARTT.setRequestFocusEnabled(false);
        WARTT.setFont(new Font("sansserif", Font.PLAIN, 14));
        WARTT.setName("WARTT");
        pnlLigne.add(WARTT, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- A1LIB ----
        A1LIB.setMaximumSize(new Dimension(400, 30));
        A1LIB.setMinimumSize(new Dimension(350, 30));
        A1LIB.setPreferredSize(new Dimension(350, 30));
        A1LIB.setRequestFocusEnabled(false);
        A1LIB.setFont(new Font("sansserif", Font.PLAIN, 14));
        A1LIB.setName("A1LIB");
        pnlLigne.add(A1LIB, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WSER ----
        WSER.setText("WSER");
        WSER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        WSER.setName("WSER");
        pnlLigne.add(WSER, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbDate ----
        lbDate.setText("Date");
        lbDate.setRequestFocusEnabled(false);
        lbDate.setPreferredSize(new Dimension(60, 30));
        lbDate.setMinimumSize(new Dimension(60, 30));
        lbDate.setMaximumSize(new Dimension(60, 30));
        lbDate.setName("lbDate");
        pnlLigne.add(lbDate, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- L1DATX ----
        L1DATX.setComponentPopupMenu(null);
        L1DATX.setPreferredSize(new Dimension(110, 30));
        L1DATX.setMinimumSize(new Dimension(110, 30));
        L1DATX.setMaximumSize(new Dimension(110, 30));
        L1DATX.setFont(new Font("sansserif", Font.PLAIN, 14));
        L1DATX.setName("L1DATX");
        pnlLigne.add(L1DATX, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlLigne,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlSaisieLigne ========
      {
        pnlSaisieLigne.setTitre("Saisie");
        pnlSaisieLigne.setName("pnlSaisieLigne");
        pnlSaisieLigne.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlSaisieLigne.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlSaisieLigne.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlSaisieLigne.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlSaisieLigne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlSaisie ========
        {
          pnlSaisie.setName("pnlSaisie");
          pnlSaisie.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlSaisie.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlSaisie.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlSaisie.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlSaisie.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbQuantite ----
          lbQuantite.setText("Quantit\u00e9");
          lbQuantite.setName("lbQuantite");
          pnlSaisie.add(lbQuantite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- L1QTEX ----
          L1QTEX.setPreferredSize(new Dimension(120, 30));
          L1QTEX.setMinimumSize(new Dimension(120, 30));
          L1QTEX.setFont(new Font("sansserif", Font.PLAIN, 14));
          L1QTEX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1QTEX.setName("L1QTEX");
          pnlSaisie.add(L1QTEX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbUnite ----
          lbUnite.setText("@L1UNS@");
          lbUnite.setName("lbUnite");
          pnlSaisie.add(lbUnite, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbCoeff ----
          lbCoeff.setText("Coefficient");
          lbCoeff.setName("lbCoeff");
          pnlSaisie.add(lbCoeff, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- L1CNDR ----
          L1CNDR.setPreferredSize(new Dimension(70, 30));
          L1CNDR.setMinimumSize(new Dimension(70, 30));
          L1CNDR.setFont(new Font("sansserif", Font.PLAIN, 14));
          L1CNDR.setHorizontalAlignment(SwingConstants.TRAILING);
          L1CNDR.setName("L1CNDR");
          pnlSaisie.add(L1CNDR, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- L1QTPX ----
          L1QTPX.setMinimumSize(new Dimension(90, 30));
          L1QTPX.setPreferredSize(new Dimension(90, 30));
          L1QTPX.setFont(new Font("sansserif", Font.PLAIN, 14));
          L1QTPX.setHorizontalAlignment(SwingConstants.TRAILING);
          L1QTPX.setName("L1QTPX");
          pnlSaisie.add(L1QTPX, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlSens ========
          {
            pnlSens.setName("pnlSens");
            pnlSens.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlSens.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlSens.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlSens.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlSens.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbSens ----
            lbSens.setText("Sens");
            lbSens.setName("lbSens");
            pnlSens.add(lbSens, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WSGN ----
            WSGN.setText("+");
            WSGN.setToolTipText("Signe op\u00e9ration");
            WSGN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WSGN.setFont(new Font("sansserif", Font.PLAIN, 14));
            WSGN.setName("WSGN");
            pnlSens.add(WSGN, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WSGN_MOINS ----
            WSGN_MOINS.setText("-");
            WSGN_MOINS.setToolTipText("Signe op\u00e9ration");
            WSGN_MOINS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WSGN_MOINS.setFont(new Font("sansserif", Font.PLAIN, 14));
            WSGN_MOINS.setName("WSGN_MOINS");
            pnlSens.add(WSGN_MOINS, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlSaisie.add(pnlSens, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSaisieLigne.add(pnlSaisie, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlCommentaire ========
        {
          pnlCommentaire.setName("pnlCommentaire");
          pnlCommentaire.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCommentaire.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCommentaire.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlCommentaire.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlCommentaire.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbCommentaire ----
          lbCommentaire.setText("Commentaire");
          lbCommentaire.setName("lbCommentaire");
          pnlCommentaire.add(lbCommentaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- L1OBS ----
          L1OBS.setComponentPopupMenu(null);
          L1OBS.setToolTipText("Libell\u00e9 visible dans les mouvements de stock");
          L1OBS.setPreferredSize(new Dimension(630, 30));
          L1OBS.setMinimumSize(new Dimension(630, 30));
          L1OBS.setFont(new Font("sansserif", Font.PLAIN, 14));
          L1OBS.setName("L1OBS");
          pnlCommentaire.add(L1OBS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSaisieLigne.add(pnlCommentaire, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlSubstitution ========
        {
          pnlSubstitution.setName("pnlSubstitution");
          pnlSubstitution.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlSubstitution.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlSubstitution.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlSubstitution.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlSubstitution.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbUTITP ----
          lbUTITP.setText("@UTITP@");
          lbUTITP.setName("lbUTITP");
          pnlSubstitution.add(lbUTITP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- L1PHTX ----
          L1PHTX.setFont(new Font("sansserif", Font.PLAIN, 14));
          L1PHTX.setPreferredSize(new Dimension(120, 31));
          L1PHTX.setMinimumSize(new Dimension(120, 31));
          L1PHTX.setHorizontalAlignment(SwingConstants.TRAILING);
          L1PHTX.setName("L1PHTX");
          pnlSubstitution.add(L1PHTX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbArticleSubstitution ----
          lbArticleSubstitution.setText("Article de substitution");
          lbArticleSubstitution.setName("lbArticleSubstitution");
          pnlSubstitution.add(lbArticleSubstitution, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WASB ----
          WASB.setMaximumSize(new Dimension(210, 30));
          WASB.setMinimumSize(new Dimension(210, 30));
          WASB.setPreferredSize(new Dimension(210, 30));
          WASB.setRequestFocusEnabled(false);
          WASB.setFont(new Font("sansserif", Font.PLAIN, 14));
          WASB.setName("WASB");
          pnlSubstitution.add(WASB, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WLBMA2 ----
          WLBMA2.setText("@WLBMA2@");
          WLBMA2.setMinimumSize(new Dimension(350, 30));
          WLBMA2.setPreferredSize(new Dimension(350, 30));
          WLBMA2.setName("WLBMA2");
          pnlSubstitution.add(WLBMA2, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlSaisieLigne.add(pnlSubstitution, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlOptions ========
        {
          pnlOptions.setName("pnlOptions");
          pnlOptions.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlOptions.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlOptions.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlOptions.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlOptions.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- checkBox1 ----
          checkBox1.setText("Modificateur de PUMP");
          checkBox1.setFont(new Font("sansserif", Font.PLAIN, 14));
          checkBox1.setPreferredSize(new Dimension(200, 30));
          checkBox1.setMinimumSize(new Dimension(200, 30));
          checkBox1.setName("checkBox1");
          checkBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              checkBox1ActionPerformed(e);
            }
          });
          pnlOptions.add(checkBox1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- checkBox2 ----
          checkBox2.setText("PUMP impos\u00e9");
          checkBox2.setFont(new Font("sansserif", Font.PLAIN, 14));
          checkBox2.setPreferredSize(new Dimension(200, 30));
          checkBox2.setMinimumSize(new Dimension(200, 30));
          checkBox2.setName("checkBox2");
          checkBox2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              checkBox2ActionPerformed(e);
            }
          });
          pnlOptions.add(checkBox2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlSaisieLigne.add(pnlOptions, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlContenu.add(pnlSaisieLigne,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- WSGN_GRP ----
    WSGN_GRP.add(WSGN);
    WSGN_GRP.add(WSGN_MOINS);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlDate;
  private SNLabelChamp lbDateStock;
  private XRiCalendrier WDATX;
  private SNLabelChamp lbDateInventaire;
  private XRiCalendrier DATINV;
  private SNPanelTitre sNPanelTitre1;
  private SNLabelUnite lbStock;
  private SNLabelUnite lbCommande;
  private SNLabelUnite lbReserve;
  private SNLabelUnite lbDisponible;
  private SNLabelUnite lbAttendu;
  private XRiTextField WSTKX;
  private SNLabelChamp lbMoins;
  private XRiTextField WRESX;
  private XRiTextField WAFFX;
  private SNLabelChamp lbEgal;
  private XRiTextField WDISX;
  private XRiTextField WATTX;
  private SNLabelChamp sNLabelChamp1;
  private XRiTextField X15PM1;
  private SNPanelTitre pnlLigne;
  private SNLabelChamp lbWCOD;
  private XRiTextField WCOD;
  private SNLabelChamp lbWNLI;
  private XRiTextField WNLI;
  private SNLabelChamp lbArticle;
  private XRiTextField WARTT;
  private XRiTextField A1LIB;
  private XRiCheckBox WSER;
  private SNLabelChamp lbDate;
  private XRiCalendrier L1DATX;
  private SNPanelTitre pnlSaisieLigne;
  private SNPanel pnlSaisie;
  private SNLabelChamp lbQuantite;
  private XRiTextField L1QTEX;
  private SNLabelUnite lbUnite;
  private SNLabelChamp lbCoeff;
  private XRiTextField L1CNDR;
  private XRiTextField L1QTPX;
  private SNPanel pnlSens;
  private SNLabelChamp lbSens;
  private XRiRadioButton WSGN;
  private XRiRadioButton WSGN_MOINS;
  private SNPanel pnlCommentaire;
  private SNLabelChamp lbCommentaire;
  private XRiTextField L1OBS;
  private SNPanel pnlSubstitution;
  private SNLabelChamp lbUTITP;
  private XRiTextField L1PHTX;
  private SNLabelChamp lbArticleSubstitution;
  private XRiTextField WASB;
  private SNLabelUnite WLBMA2;
  private SNPanel pnlOptions;
  private JCheckBox checkBox1;
  private JCheckBox checkBox2;
  private ButtonGroup WSGN_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
