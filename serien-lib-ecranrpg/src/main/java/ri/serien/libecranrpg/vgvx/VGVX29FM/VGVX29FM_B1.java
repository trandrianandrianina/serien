
package ri.serien.libecranrpg.vgvx.VGVX29FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVX29FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WL501_Title = { "HLD01", };
  private String[][] _WL501_Data = { { "WL501", }, };
  private int[] _WL501_Width = { 15, };
  
  public VGVX29FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WL501.setAspectTable(null, _WL501_Title, _WL501_Data, _WL501_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    A1ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1ART@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    E1RCC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1RCC@")).trim());
    OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MODLIB@")).trim());
    OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETALIB@")).trim());
    E1NCC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NCC@")).trim());
    E1NFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NFA@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLFP@")).trim());
    E1FACX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1FACX@")).trim());
    DLDATX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLDATX@")).trim());
    E1SAN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1SAN@")).trim());
    E1ACT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ACT@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLFS@")).trim());
    DLMOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLMOD@")).trim());
    DLETA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLETA@")).trim());
    DLTOU.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLTOU@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    DLTOU.setVisible(lexique.isPresent("DLTOU"));
    DLETA.setVisible(lexique.isPresent("DLETA"));
    DLMOD.setVisible(lexique.isPresent("DLMOD"));
    E1ACT.setVisible(lexique.isPresent("E1ACT"));
    E1SAN.setVisible(lexique.isPresent("E1SAN"));
    DLDATX.setVisible(lexique.isPresent("DLDATX"));
    E1FACX.setVisible(lexique.isPresent("E1FACX"));
    E1NFA.setVisible(lexique.isPresent("E1NFA"));
    E1NCC.setVisible(lexique.isPresent("E1NCC"));
    OBJ_51.setVisible(lexique.isPresent("ETALIB"));
    OBJ_48.setVisible(lexique.isPresent("MODLIB"));
    E1RCC.setVisible(lexique.isPresent("E1RCC"));
    OBJ_27.setVisible(lexique.isPresent("MALIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail livraison"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WL501 = new XRiTable();
    OBJ_27 = new JLabel();
    A1ART = new RiZoneSortie();
    A1LIB = new RiZoneSortie();
    panel1 = new JPanel();
    OBJ_34 = new RiZoneSortie();
    E1RCC = new RiZoneSortie();
    OBJ_48 = new RiZoneSortie();
    OBJ_51 = new RiZoneSortie();
    E1NCC = new RiZoneSortie();
    OBJ_49 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_31 = new JLabel();
    E1NFA = new RiZoneSortie();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_32 = new RiZoneSortie();
    E1FACX = new RiZoneSortie();
    DLDATX = new RiZoneSortie();
    OBJ_44 = new JLabel();
    E1SAN = new RiZoneSortie();
    E1ACT = new RiZoneSortie();
    OBJ_54 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_33 = new RiZoneSortie();
    DLMOD = new RiZoneSortie();
    DLETA = new RiZoneSortie();
    DLTOU = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- WL501 ----
          WL501.setName("WL501");
          SCROLLPANE_LIST.setViewportView(WL501);
        }

        //---- OBJ_27 ----
        OBJ_27.setText("@MALIB@");
        OBJ_27.setName("OBJ_27");

        //---- A1ART ----
        A1ART.setText("@A1ART@");
        A1ART.setName("A1ART");

        //---- A1LIB ----
        A1LIB.setText("@A1LIB@");
        A1LIB.setName("A1LIB");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("D\u00e9tail"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_34 ----
          OBJ_34.setText("@CLNOM@");
          OBJ_34.setName("OBJ_34");
          panel1.add(OBJ_34);
          OBJ_34.setBounds(240, 40, 310, OBJ_34.getPreferredSize().height);

          //---- E1RCC ----
          E1RCC.setText("@E1RCC@");
          E1RCC.setName("E1RCC");
          panel1.add(E1RCC);
          E1RCC.setBounds(240, 70, 196, E1RCC.getPreferredSize().height);

          //---- OBJ_48 ----
          OBJ_48.setText("@MODLIB@");
          OBJ_48.setName("OBJ_48");
          panel1.add(OBJ_48);
          OBJ_48.setBounds(155, 130, 154, OBJ_48.getPreferredSize().height);

          //---- OBJ_51 ----
          OBJ_51.setText("@ETALIB@");
          OBJ_51.setName("OBJ_51");
          panel1.add(OBJ_51);
          OBJ_51.setBounds(155, 160, 154, OBJ_51.getPreferredSize().height);

          //---- E1NCC ----
          E1NCC.setText("@E1NCC@");
          E1NCC.setName("E1NCC");
          panel1.add(E1NCC);
          E1NCC.setBounds(125, 70, 109, E1NCC.getPreferredSize().height);

          //---- OBJ_49 ----
          OBJ_49.setText("Etat livraison");
          OBJ_49.setName("OBJ_49");
          panel1.add(OBJ_49);
          OBJ_49.setBounds(25, 164, 100, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("Facturation le");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(25, 104, 100, 20);

          //---- OBJ_46 ----
          OBJ_46.setText("Mode livraison");
          OBJ_46.setName("OBJ_46");
          panel1.add(OBJ_46);
          OBJ_46.setBounds(25, 134, 100, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("R\u00e9f\u00e9rence");
          OBJ_35.setName("OBJ_35");
          panel1.add(OBJ_35);
          OBJ_35.setBounds(25, 70, 95, 20);

          //---- OBJ_31 ----
          OBJ_31.setText("Client");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(25, 42, 88, 20);

          //---- E1NFA ----
          E1NFA.setText("@E1NFA@");
          E1NFA.setName("E1NFA");
          panel1.add(E1NFA);
          E1NFA.setBounds(300, 100, 60, E1NFA.getPreferredSize().height);

          //---- OBJ_39 ----
          OBJ_39.setText("Section");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(445, 72, 55, 20);

          //---- OBJ_40 ----
          OBJ_40.setText("Affaire");
          OBJ_40.setName("OBJ_40");
          panel1.add(OBJ_40);
          OBJ_40.setBounds(445, 104, 58, 20);

          //---- OBJ_32 ----
          OBJ_32.setText("@E1CLFP@");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(125, 40, 60, OBJ_32.getPreferredSize().height);

          //---- E1FACX ----
          E1FACX.setText("@E1FACX@");
          E1FACX.setName("E1FACX");
          panel1.add(E1FACX);
          E1FACX.setBounds(125, 100, 75, E1FACX.getPreferredSize().height);

          //---- DLDATX ----
          DLDATX.setText("@DLDATX@");
          DLDATX.setName("DLDATX");
          panel1.add(DLDATX);
          DLDATX.setBounds(361, 160, 75, DLDATX.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Num\u00e9ro");
          OBJ_44.setName("OBJ_44");
          panel1.add(OBJ_44);
          OBJ_44.setBounds(242, 102, 65, 20);

          //---- E1SAN ----
          E1SAN.setText("@E1SAN@");
          E1SAN.setName("E1SAN");
          panel1.add(E1SAN);
          E1SAN.setBounds(500, 70, 50, E1SAN.getPreferredSize().height);

          //---- E1ACT ----
          E1ACT.setText("@E1ACT@");
          E1ACT.setName("E1ACT");
          panel1.add(E1ACT);
          E1ACT.setBounds(500, 100, 50, E1ACT.getPreferredSize().height);

          //---- OBJ_54 ----
          OBJ_54.setText("Date");
          OBJ_54.setName("OBJ_54");
          panel1.add(OBJ_54);
          OBJ_54.setBounds(320, 162, 40, 20);

          //---- OBJ_52 ----
          OBJ_52.setText("Indice");
          OBJ_52.setName("OBJ_52");
          panel1.add(OBJ_52);
          OBJ_52.setBounds(480, 162, 50, 20);

          //---- OBJ_33 ----
          OBJ_33.setText("@E1CLFS@");
          OBJ_33.setName("OBJ_33");
          panel1.add(OBJ_33);
          OBJ_33.setBounds(195, 40, 36, OBJ_33.getPreferredSize().height);

          //---- DLMOD ----
          DLMOD.setText("@DLMOD@");
          DLMOD.setName("DLMOD");
          panel1.add(DLMOD);
          DLMOD.setBounds(125, 130, 20, DLMOD.getPreferredSize().height);

          //---- DLETA ----
          DLETA.setText("@DLETA@");
          DLETA.setName("DLETA");
          panel1.add(DLETA);
          DLETA.setBounds(125, 160, 20, DLETA.getPreferredSize().height);

          //---- DLTOU ----
          DLTOU.setText("@DLTOU@");
          DLTOU.setName("DLTOU");
          panel1.add(DLTOU);
          DLTOU.setBounds(530, 160, 20, DLTOU.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(A1ART, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(A1LIB, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 610, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 610, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(A1ART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(A1LIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(16, 16, 16)
              .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
              .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
              .addGap(25, 25, 25)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WL501;
  private JLabel OBJ_27;
  private RiZoneSortie A1ART;
  private RiZoneSortie A1LIB;
  private JPanel panel1;
  private RiZoneSortie OBJ_34;
  private RiZoneSortie E1RCC;
  private RiZoneSortie OBJ_48;
  private RiZoneSortie OBJ_51;
  private RiZoneSortie E1NCC;
  private JLabel OBJ_49;
  private JLabel OBJ_42;
  private JLabel OBJ_46;
  private JLabel OBJ_35;
  private JLabel OBJ_31;
  private RiZoneSortie E1NFA;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private RiZoneSortie OBJ_32;
  private RiZoneSortie E1FACX;
  private RiZoneSortie DLDATX;
  private JLabel OBJ_44;
  private RiZoneSortie E1SAN;
  private RiZoneSortie E1ACT;
  private JLabel OBJ_54;
  private JLabel OBJ_52;
  private RiZoneSortie OBJ_33;
  private RiZoneSortie DLMOD;
  private RiZoneSortie DLETA;
  private RiZoneSortie DLTOU;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
