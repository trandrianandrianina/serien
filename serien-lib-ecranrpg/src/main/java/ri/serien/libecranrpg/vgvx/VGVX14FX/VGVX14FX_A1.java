
package ri.serien.libecranrpg.vgvx.VGVX14FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX14FX_A1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private static final String BOUTON_SAISIE_CODE_BARRE = "Saisir par code barres";
  private static final String BOUTON_TRANSFERT_AUTORISE = "Autoriser transfert";
  private static final String BOUTON_DATE_JOUR = "Changer date du jour";
  
  public VGVX14FX_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_SAISIE_CODE_BARRE, 'b', true);
    snBarreBouton.ajouterBouton(BOUTON_TRANSFERT_AUTORISE, 't', true);
    snBarreBouton.ajouterBouton(BOUTON_DATE_JOUR, 'j', true);
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    boolean isInventaire = lexique.isTrue("21");
    
    radioButton2.setSelected(false);
    radioButton3.setSelected(false);
    radioButton4.setSelected(false);
    radioButton5.setSelected(false);
    radioButton6.setSelected(false);
    radioButton7.setSelected(false);
    radioButton8.setSelected(false);
    
    // Code établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "E1ETB");
    snEtablissement.setEnabled(false);
    
    // Magasins
    snMagasinEmetteur.setSession(getSession());
    snMagasinEmetteur.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinEmetteur.charger(false);
    snMagasinEmetteur.setSelectionParChampRPG(lexique, "E1MAG");
    
    snMagasinRecepteur.setSession(getSession());
    snMagasinRecepteur.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinRecepteur.charger(false);
    snMagasinRecepteur.setSelectionParChampRPG(lexique, "E1MAG2");
    lbMagasinEmetteur.setVisible(snMagasinEmetteur.isVisible());
    snMagasinRecepteur.setVisible(radioButton3.isSelected());
    lbMagasinRecepteur.setVisible(snMagasinRecepteur.isVisible());
    
    // date
    WDATEX.setEnabled(false);
    
    String typeOperation = lexique.HostFieldGetData("E1OPE").trim();
    
    if (typeOperation.equals("S")) {
      radioButton2.setSelected(true);
    }
    else if (typeOperation.equalsIgnoreCase("T")) {
      radioButton3.setSelected(true);
    }
    else if (typeOperation.equalsIgnoreCase("E")) {
      radioButton4.setSelected(true);
    }
    else if (typeOperation.equalsIgnoreCase("D")) {
      radioButton5.setSelected(true);
    }
    else if (typeOperation.equalsIgnoreCase("I")) {
      radioButton6.setSelected(true);
    }
    else if (typeOperation.equalsIgnoreCase("M")) {
      radioButton7.setSelected(true);
    }
    else if (typeOperation.equalsIgnoreCase("X")) {
      radioButton8.setSelected(true);
    }
    else {
      buttonGroup1.clearSelection();
    }
    
    pnlTypeBordereau.setVisible(!isInventaire);
    if (isInventaire) {
      bpPresentation.setText("Saisie d'inventaire");
    }
    else {
      bpPresentation.setText("Bordereau de stocks");
    }
    
    traiterVisibiliteBouton();
    
    bpPresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    snEtablissement.renseignerChampRPG(lexique, "E1ETB");
    snMagasinEmetteur.renseignerChampRPG(lexique, "E1MAG");
    snMagasinRecepteur.renseignerChampRPG(lexique, "E1MAG2");
  }
  
  /**
   * Traiter la visibilité des boutons
   */
  private void traiterVisibiliteBouton() {
    boolean actif = false;
    if (pnlTypeBordereau.isVisible()) {
      if (radioButton2.isSelected() || radioButton3.isSelected() || radioButton4.isSelected() || radioButton5.isSelected()
          || radioButton6.isSelected() || radioButton7.isSelected() || radioButton8.isSelected()) {
        actif = true;
      }
    }
    else {
      actif = true;
    }
    snBarreBouton.activerBouton(EnumBouton.CONTINUER, actif);
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_SAISIE_CODE_BARRE)) {
        lexique.HostScreenSendKey(this, "F5");
      }
      else if (pSNBouton.isBouton(BOUTON_TRANSFERT_AUTORISE)) {
        lexique.HostScreenSendKey(this, "F23");
      }
      else if (pSNBouton.isBouton(BOUTON_DATE_JOUR)) {
        lexique.HostScreenSendKey(this, "F11");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void E1OPEActionPerformed(ActionEvent e) {
    
    if (radioButton2.isSelected()) {
      lexique.HostFieldPutData("E1OPE", 1, "S");
    }
    else if (radioButton3.isSelected()) {
      lexique.HostFieldPutData("E1OPE", 1, "T");
    }
    else if (radioButton4.isSelected()) {
      lexique.HostFieldPutData("E1OPE", 1, "E");
    }
    else if (radioButton5.isSelected()) {
      lexique.HostFieldPutData("E1OPE", 1, "D");
    }
    else if (radioButton6.isSelected()) {
      lexique.HostFieldPutData("E1OPE", 1, "I");
    }
    else if (radioButton7.isSelected()) {
      lexique.HostFieldPutData("E1OPE", 1, "M");
    }
    else if (radioButton8.isSelected()) {
      lexique.HostFieldPutData("E1OPE", 1, "X");
    }
    else {
      lexique.HostFieldPutData("E1OPE", 1, "");
    }
    
    traiterVisibiliteBouton();
  }
  
  private void radioButton3ItemStateChanged(ItemEvent e) {
    snMagasinRecepteur.setVisible(radioButton3.isSelected());
    lbMagasinRecepteur.setVisible(snMagasinRecepteur.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBandeau = new JPanel();
    bpPresentation = new SNBandeauTitre();
    pnlPrincipal = new SNPanel();
    pnlContenu = new SNPanelContenu();
    pnlInformations = new SNPanel();
    pnlGauche = new SNPanel();
    lbNumero = new SNLabelChamp();
    WNUM = new XRiTextField();
    lbDateTraitement2 = new SNLabelChamp();
    WDATEX = new XRiCalendrier();
    pnlDroite = new SNPanel();
    lbE1ETB = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlMagasin = new SNPanelTitre();
    snMagasinEmetteur = new SNMagasin();
    lbMagasinEmetteur = new SNLabelChamp();
    lbMagasinRecepteur = new SNLabelChamp();
    snMagasinRecepteur = new SNMagasin();
    pnlTypeBordereau = new SNPanelTitre();
    radioButton2 = new XRiRadioButton();
    radioButton8 = new XRiRadioButton();
    radioButton7 = new XRiRadioButton();
    radioButton6 = new XRiRadioButton();
    radioButton5 = new XRiRadioButton();
    radioButton4 = new XRiRadioButton();
    radioButton3 = new XRiRadioButton();
    snBarreBouton = new SNBarreBouton();
    buttonGroup1 = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlBandeau ========
    {
      pnlBandeau.setName("pnlBandeau");
      pnlBandeau.setLayout(new VerticalLayout());

      //---- bpPresentation ----
      bpPresentation.setText("Cr\u00e9ation de bordereau de stocks");
      bpPresentation.setName("bpPresentation");
      pnlBandeau.add(bpPresentation);
    }
    add(pnlBandeau, BorderLayout.NORTH);

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setOpaque(false);
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      //======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //======== pnlInformations ========
        {
          pnlInformations.setName("pnlInformations");
          pnlInformations.setLayout(new GridLayout(1, 2, 5, 0));

          //======== pnlGauche ========
          {
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbNumero ----
            lbNumero.setText("Num\u00e9ro");
            lbNumero.setMaximumSize(new Dimension(200, 30));
            lbNumero.setMinimumSize(new Dimension(200, 30));
            lbNumero.setPreferredSize(new Dimension(200, 30));
            lbNumero.setInheritsPopupMenu(false);
            lbNumero.setName("lbNumero");
            pnlGauche.add(lbNumero, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- WNUM ----
            WNUM.setComponentPopupMenu(null);
            WNUM.setMinimumSize(new Dimension(70, 30));
            WNUM.setMaximumSize(new Dimension(70, 30));
            WNUM.setPreferredSize(new Dimension(70, 30));
            WNUM.setFont(new Font("sansserif", Font.PLAIN, 14));
            WNUM.setName("WNUM");
            pnlGauche.add(WNUM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbDateTraitement2 ----
            lbDateTraitement2.setText("Date de traitement");
            lbDateTraitement2.setMaximumSize(new Dimension(200, 30));
            lbDateTraitement2.setMinimumSize(new Dimension(200, 30));
            lbDateTraitement2.setPreferredSize(new Dimension(200, 30));
            lbDateTraitement2.setInheritsPopupMenu(false);
            lbDateTraitement2.setName("lbDateTraitement2");
            pnlGauche.add(lbDateTraitement2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- WDATEX ----
            WDATEX.setComponentPopupMenu(null);
            WDATEX.setMaximumSize(new Dimension(110, 30));
            WDATEX.setMinimumSize(new Dimension(110, 30));
            WDATEX.setPreferredSize(new Dimension(110, 30));
            WDATEX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDATEX.setName("WDATEX");
            pnlGauche.add(WDATEX, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInformations.add(pnlGauche);

          //======== pnlDroite ========
          {
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- lbE1ETB ----
            lbE1ETB.setText("Etablissement");
            lbE1ETB.setMaximumSize(new Dimension(200, 30));
            lbE1ETB.setMinimumSize(new Dimension(200, 30));
            lbE1ETB.setPreferredSize(new Dimension(200, 30));
            lbE1ETB.setInheritsPopupMenu(false);
            lbE1ETB.setName("lbE1ETB");
            pnlDroite.add(lbE1ETB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snEtablissement ----
            snEtablissement.setName("snEtablissement");
            pnlDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInformations.add(pnlDroite);
        }
        pnlContenu.add(pnlInformations, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlMagasin ========
        {
          pnlMagasin.setForeground(Color.black);
          pnlMagasin.setTitre("Magasin");
          pnlMagasin.setName("pnlMagasin");
          pnlMagasin.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlMagasin.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlMagasin.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlMagasin.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlMagasin.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- snMagasinEmetteur ----
          snMagasinEmetteur.setName("snMagasinEmetteur");
          pnlMagasin.add(snMagasinEmetteur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbMagasinEmetteur ----
          lbMagasinEmetteur.setText("Magasin \u00e9metteur");
          lbMagasinEmetteur.setPreferredSize(new Dimension(180, 30));
          lbMagasinEmetteur.setMaximumSize(new Dimension(180, 30));
          lbMagasinEmetteur.setMinimumSize(new Dimension(180, 30));
          lbMagasinEmetteur.setName("lbMagasinEmetteur");
          pnlMagasin.add(lbMagasinEmetteur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbMagasinRecepteur ----
          lbMagasinRecepteur.setText("Magasin r\u00e9cepteur");
          lbMagasinRecepteur.setMaximumSize(new Dimension(200, 30));
          lbMagasinRecepteur.setMinimumSize(new Dimension(200, 30));
          lbMagasinRecepteur.setPreferredSize(new Dimension(200, 30));
          lbMagasinRecepteur.setName("lbMagasinRecepteur");
          pnlMagasin.add(lbMagasinRecepteur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snMagasinRecepteur ----
          snMagasinRecepteur.setName("snMagasinRecepteur");
          pnlMagasin.add(snMagasinRecepteur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlTypeBordereau ========
        {
          pnlTypeBordereau.setTitre("Type de bordereau");
          pnlTypeBordereau.setName("pnlTypeBordereau");
          pnlTypeBordereau.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlTypeBordereau.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlTypeBordereau.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlTypeBordereau.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlTypeBordereau.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- radioButton2 ----
          radioButton2.setText("Sortie de stock");
          radioButton2.setFont(new Font("sansserif", Font.PLAIN, 14));
          radioButton2.setMaximumSize(new Dimension(130, 30));
          radioButton2.setMinimumSize(new Dimension(130, 30));
          radioButton2.setPreferredSize(new Dimension(130, 30));
          radioButton2.setName("radioButton2");
          radioButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              E1OPEActionPerformed(e);
            }
          });
          pnlTypeBordereau.add(radioButton2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- radioButton8 ----
          radioButton8.setText("Stock maximum");
          radioButton8.setFont(new Font("sansserif", Font.PLAIN, 14));
          radioButton8.setPreferredSize(new Dimension(130, 30));
          radioButton8.setMinimumSize(new Dimension(130, 30));
          radioButton8.setMaximumSize(new Dimension(130, 30));
          radioButton8.setName("radioButton8");
          radioButton8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              E1OPEActionPerformed(e);
            }
          });
          pnlTypeBordereau.add(radioButton8, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- radioButton7 ----
          radioButton7.setText("Stock minimum");
          radioButton7.setFont(new Font("sansserif", Font.PLAIN, 14));
          radioButton7.setPreferredSize(new Dimension(130, 30));
          radioButton7.setMinimumSize(new Dimension(130, 30));
          radioButton7.setMaximumSize(new Dimension(130, 30));
          radioButton7.setName("radioButton7");
          radioButton7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              E1OPEActionPerformed(e);
            }
          });
          pnlTypeBordereau.add(radioButton7, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- radioButton6 ----
          radioButton6.setText("Inventaire (saisi)");
          radioButton6.setFont(new Font("sansserif", Font.PLAIN, 14));
          radioButton6.setPreferredSize(new Dimension(130, 30));
          radioButton6.setMinimumSize(new Dimension(130, 30));
          radioButton6.setMaximumSize(new Dimension(130, 30));
          radioButton6.setName("radioButton6");
          radioButton6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              E1OPEActionPerformed(e);
            }
          });
          pnlTypeBordereau.add(radioButton6, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- radioButton5 ----
          radioButton5.setText("Divers (mouvements)");
          radioButton5.setFont(new Font("sansserif", Font.PLAIN, 14));
          radioButton5.setPreferredSize(new Dimension(130, 30));
          radioButton5.setMinimumSize(new Dimension(130, 30));
          radioButton5.setMaximumSize(new Dimension(130, 30));
          radioButton5.setName("radioButton5");
          radioButton5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              E1OPEActionPerformed(e);
            }
          });
          pnlTypeBordereau.add(radioButton5, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- radioButton4 ----
          radioButton4.setText("Entr\u00e9e de stock");
          radioButton4.setFont(new Font("sansserif", Font.PLAIN, 14));
          radioButton4.setPreferredSize(new Dimension(130, 30));
          radioButton4.setMinimumSize(new Dimension(130, 30));
          radioButton4.setMaximumSize(new Dimension(130, 30));
          radioButton4.setName("radioButton4");
          radioButton4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              E1OPEActionPerformed(e);
            }
          });
          pnlTypeBordereau.add(radioButton4, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- radioButton3 ----
          radioButton3.setText("Transferts");
          radioButton3.setFont(new Font("sansserif", Font.PLAIN, 14));
          radioButton3.setPreferredSize(new Dimension(130, 30));
          radioButton3.setMinimumSize(new Dimension(130, 30));
          radioButton3.setMaximumSize(new Dimension(130, 30));
          radioButton3.setName("radioButton3");
          radioButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              E1OPEActionPerformed(e);
            }
          });
          radioButton3.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              radioButton3ItemStateChanged(e);
            }
          });
          pnlTypeBordereau.add(radioButton3, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlContenu.add(pnlTypeBordereau, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);

      //---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    add(pnlPrincipal, BorderLayout.CENTER);

    //---- buttonGroup1 ----
    buttonGroup1.add(radioButton2);
    buttonGroup1.add(radioButton8);
    buttonGroup1.add(radioButton7);
    buttonGroup1.add(radioButton6);
    buttonGroup1.add(radioButton5);
    buttonGroup1.add(radioButton4);
    buttonGroup1.add(radioButton3);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlBandeau;
  private SNBandeauTitre bpPresentation;
  private SNPanel pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlInformations;
  private SNPanel pnlGauche;
  private SNLabelChamp lbNumero;
  private XRiTextField WNUM;
  private SNLabelChamp lbDateTraitement2;
  private XRiCalendrier WDATEX;
  private SNPanel pnlDroite;
  private SNLabelChamp lbE1ETB;
  private SNEtablissement snEtablissement;
  private SNPanelTitre pnlMagasin;
  private SNMagasin snMagasinEmetteur;
  private SNLabelChamp lbMagasinEmetteur;
  private SNLabelChamp lbMagasinRecepteur;
  private SNMagasin snMagasinRecepteur;
  private SNPanelTitre pnlTypeBordereau;
  private XRiRadioButton radioButton2;
  private XRiRadioButton radioButton8;
  private XRiRadioButton radioButton7;
  private XRiRadioButton radioButton6;
  private XRiRadioButton radioButton5;
  private XRiRadioButton radioButton4;
  private XRiRadioButton radioButton3;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
