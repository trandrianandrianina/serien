
package ri.serien.libecranrpg.vgvx.VGVX054F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX054F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  boolean isLibelleLong = false;
  
  public VGVX054F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    setCloseKey("ENTER", "F12");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FALIB@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOM@")).trim());
    A1UNV2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19", lexique.HostFieldGetData("LIBERR"));
    
    
    
    // DESIGNATION 4 zones ou 2 zones
    isLibelleLong = (lexique.HostFieldGetData("PST39").equalsIgnoreCase("4"));
    WLIB1.setVisible(!isLibelleLong);
    WLIB2.setVisible(!isLibelleLong);
    WLIB3.setVisible(!isLibelleLong);
    WLIB4.setVisible(!isLibelleLong);
    A1LB12.setVisible(isLibelleLong);
    A1LB34.setVisible(isLibelleLong);
    
    riSousMenu6.setVisible(!lexique.HostFieldGetData("FRNOM").trim().equals(""));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WPACX");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_28 = new RiZoneSortie();
    OBJ_30 = new RiZoneSortie();
    WLIB1 = new XRiTextField();
    WLIB2 = new XRiTextField();
    WLIB3 = new XRiTextField();
    WLIB4 = new XRiTextField();
    WARTT = new XRiTextField();
    OBJ_29 = new JLabel();
    OBJ_16 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_27 = new JLabel();
    WFAM = new XRiTextField();
    OBJ_32 = new JLabel();
    OBJ_33 = new JLabel();
    WPVNX = new XRiTextField();
    WPRVX = new XRiTextField();
    OBJ_34 = new JLabel();
    WFRNOM = new XRiTextField();
    OBJ_35 = new JLabel();
    OBJ_36 = new JLabel();
    WPACX = new XRiTextField();
    OBJ_37 = new JLabel();
    A1UNL = new XRiTextField();
    A1CND = new XRiTextField();
    A1LB12 = new XRiTextField();
    A1LB34 = new XRiTextField();
    A1UNV = new XRiTextField();
    OBJ_38 = new JLabel();
    OBJ_39 = new JLabel();
    A1UNV2 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1055, 355));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Acc\u00e8s \u00e0 la CNA");
            riSousMenu_bt6.setToolTipText("Acc\u00e8s \u00e0 la CNA");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Cr\u00e9ation d'un article \"divers\""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_28 ----
          OBJ_28.setText("@FALIB@");
          OBJ_28.setName("OBJ_28");
          panel2.add(OBJ_28);
          OBJ_28.setBounds(180, 84, 268, OBJ_28.getPreferredSize().height);

          //---- OBJ_30 ----
          OBJ_30.setText("@FRNOM@");
          OBJ_30.setName("OBJ_30");
          panel2.add(OBJ_30);
          OBJ_30.setBounds(135, 210, 230, OBJ_30.getPreferredSize().height);

          //---- WLIB1 ----
          WLIB1.setComponentPopupMenu(BTD);
          WLIB1.setName("WLIB1");
          panel2.add(WLIB1);
          WLIB1.setBounds(135, 129, 320, WLIB1.getPreferredSize().height);

          //---- WLIB2 ----
          WLIB2.setComponentPopupMenu(BTD);
          WLIB2.setName("WLIB2");
          panel2.add(WLIB2);
          WLIB2.setBounds(455, 129, 320, WLIB2.getPreferredSize().height);

          //---- WLIB3 ----
          WLIB3.setComponentPopupMenu(BTD);
          WLIB3.setName("WLIB3");
          panel2.add(WLIB3);
          WLIB3.setBounds(135, 160, 320, WLIB3.getPreferredSize().height);

          //---- WLIB4 ----
          WLIB4.setComponentPopupMenu(BTD);
          WLIB4.setName("WLIB4");
          panel2.add(WLIB4);
          WLIB4.setBounds(455, 160, 320, WLIB4.getPreferredSize().height);

          //---- WARTT ----
          WARTT.setComponentPopupMenu(BTD);
          WARTT.setName("WARTT");
          panel2.add(WARTT);
          WARTT.setBounds(135, 35, 210, WARTT.getPreferredSize().height);

          //---- OBJ_29 ----
          OBJ_29.setText("Fournisseur");
          OBJ_29.setName("OBJ_29");
          panel2.add(OBJ_29);
          OBJ_29.setBounds(25, 208, 90, 28);

          //---- OBJ_16 ----
          OBJ_16.setText("Article");
          OBJ_16.setName("OBJ_16");
          panel2.add(OBJ_16);
          OBJ_16.setBounds(25, 35, 90, 28);

          //---- OBJ_31 ----
          OBJ_31.setText("D\u00e9signations");
          OBJ_31.setName("OBJ_31");
          panel2.add(OBJ_31);
          OBJ_31.setBounds(25, 130, 90, 28);

          //---- OBJ_27 ----
          OBJ_27.setText("Famille");
          OBJ_27.setName("OBJ_27");
          panel2.add(OBJ_27);
          OBJ_27.setBounds(25, 82, 90, 28);

          //---- WFAM ----
          WFAM.setComponentPopupMenu(BTD);
          WFAM.setName("WFAM");
          panel2.add(WFAM);
          WFAM.setBounds(135, 82, 40, WFAM.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("Prix de vente");
          OBJ_32.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_32.setName("OBJ_32");
          panel2.add(OBJ_32);
          OBJ_32.setBounds(275, 280, 90, 28);

          //---- OBJ_33 ----
          OBJ_33.setText("Prix de revient");
          OBJ_33.setName("OBJ_33");
          panel2.add(OBJ_33);
          OBJ_33.setBounds(545, 280, 90, 28);

          //---- WPVNX ----
          WPVNX.setHorizontalAlignment(SwingConstants.RIGHT);
          WPVNX.setName("WPVNX");
          panel2.add(WPVNX);
          WPVNX.setBounds(385, 280, 108, WPVNX.getPreferredSize().height);

          //---- WPRVX ----
          WPRVX.setHorizontalAlignment(SwingConstants.RIGHT);
          WPRVX.setName("WPRVX");
          panel2.add(WPRVX);
          WPRVX.setBounds(667, 280, 108, WPRVX.getPreferredSize().height);

          //---- OBJ_34 ----
          OBJ_34.setText("Unit\u00e9 de vente");
          OBJ_34.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_34.setName("OBJ_34");
          panel2.add(OBJ_34);
          OBJ_34.setBounds(25, 243, 90, 28);

          //---- WFRNOM ----
          WFRNOM.setName("WFRNOM");
          panel2.add(WFRNOM);
          WFRNOM.setBounds(545, 208, 230, WFRNOM.getPreferredSize().height);

          //---- OBJ_35 ----
          OBJ_35.setText("Recherche d'un fournisseur");
          OBJ_35.setName("OBJ_35");
          panel2.add(OBJ_35);
          OBJ_35.setBounds(385, 208, 165, 28);

          //---- OBJ_36 ----
          OBJ_36.setText("Prix d'achat");
          OBJ_36.setName("OBJ_36");
          panel2.add(OBJ_36);
          OBJ_36.setBounds(25, 280, 90, 28);

          //---- WPACX ----
          WPACX.setHorizontalAlignment(SwingConstants.RIGHT);
          WPACX.setName("WPACX");
          panel2.add(WPACX);
          WPACX.setBounds(135, 280, 108, WPACX.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("Unit\u00e9 de conditionnement");
          OBJ_37.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_37.setName("OBJ_37");
          panel2.add(OBJ_37);
          OBJ_37.setBounds(210, 243, 155, 28);

          //---- A1UNL ----
          A1UNL.setComponentPopupMenu(BTD);
          A1UNL.setName("A1UNL");
          panel2.add(A1UNL);
          A1UNL.setBounds(385, 243, 34, A1UNL.getPreferredSize().height);

          //---- A1CND ----
          A1CND.setComponentPopupMenu(BTD);
          A1CND.setToolTipText("Nombre d'unit\u00e9s de vente par conditionnement");
          A1CND.setHorizontalAlignment(SwingConstants.RIGHT);
          A1CND.setName("A1CND");
          panel2.add(A1CND);
          A1CND.setBounds(674, 243, 101, A1CND.getPreferredSize().height);

          //---- A1LB12 ----
          A1LB12.setFont(A1LB12.getFont().deriveFont(A1LB12.getFont().getStyle() | Font.BOLD));
          A1LB12.setComponentPopupMenu(BTD);
          A1LB12.setName("A1LB12");
          panel2.add(A1LB12);
          A1LB12.setBounds(135, 129, 640, A1LB12.getPreferredSize().height);

          //---- A1LB34 ----
          A1LB34.setFont(A1LB34.getFont().deriveFont(A1LB34.getFont().getStyle() | Font.BOLD));
          A1LB34.setComponentPopupMenu(BTD);
          A1LB34.setName("A1LB34");
          panel2.add(A1LB34);
          A1LB34.setBounds(135, 160, 640, A1LB34.getPreferredSize().height);

          //---- A1UNV ----
          A1UNV.setComponentPopupMenu(BTD);
          A1UNV.setName("A1UNV");
          panel2.add(A1UNV);
          A1UNV.setBounds(135, 243, 34, A1UNV.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setText("Coefficient");
          OBJ_38.setToolTipText("Nombre d'unit\u00e9s de vente par conditionnement");
          OBJ_38.setName("OBJ_38");
          panel2.add(OBJ_38);
          OBJ_38.setBounds(545, 243, 90, 28);

          //---- OBJ_39 ----
          OBJ_39.setText("par");
          OBJ_39.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_39.setName("OBJ_39");
          panel2.add(OBJ_39);
          OBJ_39.setBounds(780, 280, 30, 28);

          //---- A1UNV2 ----
          A1UNV2.setComponentPopupMenu(BTD);
          A1UNV2.setText("@A1UNV@");
          A1UNV2.setName("A1UNV2");
          panel2.add(A1UNV2);
          A1UNV2.setBounds(810, 280, 34, 28);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 861, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel2;
  private RiZoneSortie OBJ_28;
  private RiZoneSortie OBJ_30;
  private XRiTextField WLIB1;
  private XRiTextField WLIB2;
  private XRiTextField WLIB3;
  private XRiTextField WLIB4;
  private XRiTextField WARTT;
  private JLabel OBJ_29;
  private JLabel OBJ_16;
  private JLabel OBJ_31;
  private JLabel OBJ_27;
  private XRiTextField WFAM;
  private JLabel OBJ_32;
  private JLabel OBJ_33;
  private XRiTextField WPVNX;
  private XRiTextField WPRVX;
  private JLabel OBJ_34;
  private XRiTextField WFRNOM;
  private JLabel OBJ_35;
  private JLabel OBJ_36;
  private XRiTextField WPACX;
  private JLabel OBJ_37;
  private XRiTextField A1UNL;
  private XRiTextField A1CND;
  private XRiTextField A1LB12;
  private XRiTextField A1LB34;
  private XRiTextField A1UNV;
  private JLabel OBJ_38;
  private JLabel OBJ_39;
  private RiZoneSortie A1UNV2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
