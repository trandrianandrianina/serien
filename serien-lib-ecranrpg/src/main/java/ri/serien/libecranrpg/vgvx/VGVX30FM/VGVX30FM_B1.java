//$$david$$ ££16/12/10££ -> new look

package ri.serien.libecranrpg.vgvx.VGVX30FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class VGVX30FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX30FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    FRNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOM@")).trim());
    AHLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AHLIB@")).trim());
    EAHOMX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EAHOMX@")).trim());
    EARECX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EARECX@")).trim());
    EAFACX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EAFACX@")).trim());
    NUMFAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUMFAC@")).trim());
    LASAN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LASAN@")).trim());
    LAACT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAACT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle("Historique d'achat");
    
    riSousMenu1.setVisible(true);
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    FRNOM = new RiZoneSortie();
    AHLIB = new RiZoneSortie();
    OBJ_56 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_48 = new JLabel();
    EAHOMX = new RiZoneSortie();
    EARECX = new RiZoneSortie();
    EAFACX = new RiZoneSortie();
    NUMFAC = new RiZoneSortie();
    OBJ_45 = new JLabel();
    OBJ_49 = new JLabel();
    LASAN = new RiZoneSortie();
    LAACT = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 240));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Acc\u00e8s au bon ");
              riSousMenu_bt1.setToolTipText("Affichage \u00e9tendu");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("D\u00e9tail"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- FRNOM ----
          FRNOM.setText("@FRNOM@");
          FRNOM.setName("FRNOM");
          panel2.add(FRNOM);
          FRNOM.setBounds(145, 40, 310, FRNOM.getPreferredSize().height);

          //---- AHLIB ----
          AHLIB.setText("@AHLIB@");
          AHLIB.setName("AHLIB");
          panel2.add(AHLIB);
          AHLIB.setBounds(145, 70, 310, AHLIB.getPreferredSize().height);

          //---- OBJ_56 ----
          OBJ_56.setText("Num\u00e9ro de facture");
          OBJ_56.setName("OBJ_56");
          panel2.add(OBJ_56);
          OBJ_56.setBounds(275, 163, 114, 18);

          //---- OBJ_44 ----
          OBJ_44.setText("Fournisseur");
          OBJ_44.setName("OBJ_44");
          panel2.add(OBJ_44);
          OBJ_44.setBounds(35, 43, 90, 18);

          //---- OBJ_51 ----
          OBJ_51.setText("Commande");
          OBJ_51.setName("OBJ_51");
          panel2.add(OBJ_51);
          OBJ_51.setBounds(35, 103, 90, 18);

          //---- OBJ_55 ----
          OBJ_55.setText("Facturation");
          OBJ_55.setName("OBJ_55");
          panel2.add(OBJ_55);
          OBJ_55.setBounds(35, 163, 90, 18);

          //---- OBJ_53 ----
          OBJ_53.setText("R\u00e9ception");
          OBJ_53.setName("OBJ_53");
          panel2.add(OBJ_53);
          OBJ_53.setBounds(35, 133, 90, 18);

          //---- OBJ_48 ----
          OBJ_48.setText("Acheteur");
          OBJ_48.setName("OBJ_48");
          panel2.add(OBJ_48);
          OBJ_48.setBounds(35, 73, 90, 18);

          //---- EAHOMX ----
          EAHOMX.setText("@EAHOMX@");
          EAHOMX.setName("EAHOMX");
          panel2.add(EAHOMX);
          EAHOMX.setBounds(145, 100, 75, EAHOMX.getPreferredSize().height);

          //---- EARECX ----
          EARECX.setText("@EARECX@");
          EARECX.setName("EARECX");
          panel2.add(EARECX);
          EARECX.setBounds(145, 130, 75, EARECX.getPreferredSize().height);

          //---- EAFACX ----
          EAFACX.setText("@EAFACX@");
          EAFACX.setName("EAFACX");
          panel2.add(EAFACX);
          EAFACX.setBounds(145, 160, 75, EAFACX.getPreferredSize().height);

          //---- NUMFAC ----
          NUMFAC.setText("@NUMFAC@");
          NUMFAC.setHorizontalAlignment(SwingConstants.RIGHT);
          NUMFAC.setName("NUMFAC");
          panel2.add(NUMFAC);
          NUMFAC.setBounds(395, 160, 60, NUMFAC.getPreferredSize().height);

          //---- OBJ_45 ----
          OBJ_45.setText("Section");
          OBJ_45.setName("OBJ_45");
          panel2.add(OBJ_45);
          OBJ_45.setBounds(475, 43, 48, 18);

          //---- OBJ_49 ----
          OBJ_49.setText("Affaire");
          OBJ_49.setName("OBJ_49");
          panel2.add(OBJ_49);
          OBJ_49.setBounds(475, 73, 42, 18);

          //---- LASAN ----
          LASAN.setText("@LASAN@");
          LASAN.setName("LASAN");
          panel2.add(LASAN);
          LASAN.setBounds(530, 40, 50, LASAN.getPreferredSize().height);

          //---- LAACT ----
          LAACT.setText("@LAACT@");
          LAACT.setName("LAACT");
          panel2.add(LAACT);
          LAACT.setBounds(530, 70, 50, LAACT.getPreferredSize().height);
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 610, 210);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private JPanel p_contenu;
  private JPanel panel2;
  private RiZoneSortie FRNOM;
  private RiZoneSortie AHLIB;
  private JLabel OBJ_56;
  private JLabel OBJ_44;
  private JLabel OBJ_51;
  private JLabel OBJ_55;
  private JLabel OBJ_53;
  private JLabel OBJ_48;
  private RiZoneSortie EAHOMX;
  private RiZoneSortie EARECX;
  private RiZoneSortie EAFACX;
  private RiZoneSortie NUMFAC;
  private JLabel OBJ_45;
  private JLabel OBJ_49;
  private RiZoneSortie LASAN;
  private RiZoneSortie LAACT;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
