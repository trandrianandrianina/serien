
package ri.serien.libecranrpg.vgvx.VGVX41FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX41FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] LXSGN_Value = { " ", "-", };
  private String[] LOOPE_Value = { "", "E", "S", "T", "D", "I", };
  
  public VGVX41FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    LXSGN.setValeurs(LXSGN_Value, null);
    LOOPE.setValeurs(LOOPE_Value, null);
    LOMDP.setValeursSelection("1", " ");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_recup.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("Mouvement sur article @LOART@ (@A1LIB@)")).trim()));
    LOMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOMAG@")).trim());
    A1UNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_36.setVisible(A1UNS.isVisible());
    
    LOMAG2.setVisible(LOOPE.getSelectedIndex() == 3);
    OBJ_19.setVisible(LOOPE.getSelectedIndex() == 3);
    separator1.setVisible(LOOPE.getSelectedIndex() == 3);
    OBJ_35.setVisible((LOOPE.getSelectedIndex() != 0) && (LOOPE.getSelectedIndex() != 5));
    LXSGN.setVisible((LOOPE.getSelectedIndex() != 0) && (LOOPE.getSelectedIndex() != 5));
    LOMDP.setVisible((LOOPE.getSelectedIndex() != 0) && (LOOPE.getSelectedIndex() != 2) && (LOOPE.getSelectedIndex() != 4));
    
    LOOPE.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent event) {
        OBJ_19.setVisible(LOOPE.getSelectedIndex() == 3);
        LOMAG2.setVisible(LOOPE.getSelectedIndex() == 3);
        separator1.setVisible(LOOPE.getSelectedIndex() == 3);
        OBJ_35.setVisible((LOOPE.getSelectedIndex() != 0) && (LOOPE.getSelectedIndex() != 5));
        LXSGN.setVisible((LOOPE.getSelectedIndex() != 0) && (LOOPE.getSelectedIndex() != 5));
        LOMDP.setVisible((LOOPE.getSelectedIndex() != 0) && (LOOPE.getSelectedIndex() != 2) && (LOOPE.getSelectedIndex() != 4));
      }
    });
    
    OBJ_37.setVisible(lexique.isTrue("96"));
    OBJ_39.setVisible(lexique.isTrue("96"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Saisie d'un mouvement de stock"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    LOOPE = new XRiComboBox();
    LXSGN = new XRiComboBox();
    LOOBS = new XRiTextField();
    OBJ_19 = new JLabel();
    OBJ_20 = new JLabel();
    LOMDP = new XRiCheckBox();
    OBJ_38 = new JLabel();
    OBJ_35 = new JLabel();
    P_PnlOpts = new JPanel();
    LOPHTX = new XRiTextField();
    LOQTEX = new XRiTextField();
    OBJ_21 = new JLabel();
    OBJ_34 = new JLabel();
    LODATX = new XRiCalendrier();
    OBJ_43 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_22 = new JLabel();
    LOMAG = new RiZoneSortie();
    LOMAG2 = new XRiTextField();
    A1UNS = new RiZoneSortie();
    LWAFF = new XRiTextField();
    OBJ_37 = new JLabel();
    LWAFF2 = new XRiTextField();
    OBJ_39 = new JLabel();
    separator1 = compFactory.createSeparator("Transfert de stock affaires / hors affaires");
    separator2 = compFactory.createSeparator(" ");
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(970, 290));
    setPreferredSize(new Dimension(970, 290));
    setMaximumSize(new Dimension(970, 290));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Mouvement sur article @LOART@ (@A1LIB@)"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- LOOPE ----
          LOOPE.setModel(new DefaultComboBoxModel(new String[] {
            "aucune",
            "Entr\u00e9es",
            "Sorties",
            "Transferts",
            "Divers",
            "Inventaire"
          }));
          LOOPE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LOOPE.setName("LOOPE");
          p_recup.add(LOOPE);
          LOOPE.setBounds(380, 56, 105, LOOPE.getPreferredSize().height);

          //---- LXSGN ----
          LXSGN.setModel(new DefaultComboBoxModel(new String[] {
            "+",
            "-"
          }));
          LXSGN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LXSGN.setFont(LXSGN.getFont().deriveFont(LXSGN.getFont().getStyle() | Font.BOLD, LXSGN.getFont().getSize() + 3f));
          LXSGN.setEnabled(false);
          LXSGN.setName("LXSGN");
          p_recup.add(LXSGN);
          LXSGN.setBounds(133, 144, 45, LXSGN.getPreferredSize().height);

          //---- LOOBS ----
          LOOBS.setComponentPopupMenu(null);
          LOOBS.setName("LOOBS");
          p_recup.add(LOOBS);
          LOOBS.setBounds(133, 55, 240, LOOBS.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setText("Code du magasin r\u00e9cepteur");
          OBJ_19.setName("OBJ_19");
          p_recup.add(OBJ_19);
          OBJ_19.setBounds(20, 215, 164, 28);

          //---- OBJ_20 ----
          OBJ_20.setText("Magasin \u00e9metteur");
          OBJ_20.setName("OBJ_20");
          p_recup.add(OBJ_20);
          OBJ_20.setBounds(495, 30, 110, 28);

          //---- LOMDP ----
          LOMDP.setText("Mise \u00e0 jour PUMP");
          LOMDP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LOMDP.setName("LOMDP");
          p_recup.add(LOMDP);
          LOMDP.setBounds(600, 145, 140, 28);

          //---- OBJ_38 ----
          OBJ_38.setText("Prix unitaire HT");
          OBJ_38.setName("OBJ_38");
          p_recup.add(OBJ_38);
          OBJ_38.setBounds(380, 115, 90, 28);

          //---- OBJ_35 ----
          OBJ_35.setText("Sens de l'op\u00e9ration");
          OBJ_35.setName("OBJ_35");
          p_recup.add(OBJ_35);
          OBJ_35.setBounds(133, 115, 140, 28);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- LOPHTX ----
          LOPHTX.setComponentPopupMenu(null);
          LOPHTX.setName("LOPHTX");
          p_recup.add(LOPHTX);
          LOPHTX.setBounds(380, 145, 130, LOPHTX.getPreferredSize().height);

          //---- LOQTEX ----
          LOQTEX.setComponentPopupMenu(null);
          LOQTEX.setName("LOQTEX");
          p_recup.add(LOQTEX);
          LOQTEX.setBounds(20, 145, 105, LOQTEX.getPreferredSize().height);

          //---- OBJ_21 ----
          OBJ_21.setText("Op\u00e9ration");
          OBJ_21.setName("OBJ_21");
          p_recup.add(OBJ_21);
          OBJ_21.setBounds(380, 30, 90, 28);

          //---- OBJ_34 ----
          OBJ_34.setText("Quantit\u00e9");
          OBJ_34.setName("OBJ_34");
          p_recup.add(OBJ_34);
          OBJ_34.setBounds(20, 115, 90, 28);

          //---- LODATX ----
          LODATX.setComponentPopupMenu(BTD);
          LODATX.setName("LODATX");
          p_recup.add(LODATX);
          LODATX.setBounds(20, 55, 105, LODATX.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setText("Libell\u00e9");
          OBJ_43.setName("OBJ_43");
          p_recup.add(OBJ_43);
          OBJ_43.setBounds(135, 30, 85, 28);

          //---- OBJ_36 ----
          OBJ_36.setText("Unit\u00e9");
          OBJ_36.setName("OBJ_36");
          p_recup.add(OBJ_36);
          OBJ_36.setBounds(280, 115, 45, 28);

          //---- OBJ_22 ----
          OBJ_22.setText("Date");
          OBJ_22.setName("OBJ_22");
          p_recup.add(OBJ_22);
          OBJ_22.setBounds(20, 30, 40, 28);

          //---- LOMAG ----
          LOMAG.setComponentPopupMenu(BTD);
          LOMAG.setText("@LOMAG@");
          LOMAG.setName("LOMAG");
          p_recup.add(LOMAG);
          LOMAG.setBounds(494, 57, 34, LOMAG.getPreferredSize().height);

          //---- LOMAG2 ----
          LOMAG2.setComponentPopupMenu(BTD);
          LOMAG2.setName("LOMAG2");
          p_recup.add(LOMAG2);
          LOMAG2.setBounds(195, 215, 34, LOMAG2.getPreferredSize().height);

          //---- A1UNS ----
          A1UNS.setText("@A1UNS@");
          A1UNS.setName("A1UNS");
          p_recup.add(A1UNS);
          A1UNS.setBounds(280, 147, 34, A1UNS.getPreferredSize().height);

          //---- LWAFF ----
          LWAFF.setComponentPopupMenu(BTD);
          LWAFF.setName("LWAFF");
          p_recup.add(LWAFF);
          LWAFF.setBounds(380, 215, 160, LWAFF.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("de l'affaire");
          OBJ_37.setName("OBJ_37");
          p_recup.add(OBJ_37);
          OBJ_37.setBounds(600, 30, 65, 28);

          //---- LWAFF2 ----
          LWAFF2.setComponentPopupMenu(BTD);
          LWAFF2.setName("LWAFF2");
          p_recup.add(LWAFF2);
          LWAFF2.setBounds(600, 55, 160, LWAFF2.getPreferredSize().height);

          //---- OBJ_39 ----
          OBJ_39.setText("vers l'affaire");
          OBJ_39.setName("OBJ_39");
          p_recup.add(OBJ_39);
          OBJ_39.setBounds(280, 215, 95, 28);

          //---- separator1 ----
          separator1.setName("separator1");
          p_recup.add(separator1);
          separator1.setBounds(15, 185, 745, separator1.getPreferredSize().height);

          //---- separator2 ----
          separator2.setName("separator2");
          p_recup.add(separator2);
          separator2.setBounds(15, 100, 745, separator2.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 776, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 264, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiComboBox LOOPE;
  private XRiComboBox LXSGN;
  private XRiTextField LOOBS;
  private JLabel OBJ_19;
  private JLabel OBJ_20;
  private XRiCheckBox LOMDP;
  private JLabel OBJ_38;
  private JLabel OBJ_35;
  private JPanel P_PnlOpts;
  private XRiTextField LOPHTX;
  private XRiTextField LOQTEX;
  private JLabel OBJ_21;
  private JLabel OBJ_34;
  private XRiCalendrier LODATX;
  private JLabel OBJ_43;
  private JLabel OBJ_36;
  private JLabel OBJ_22;
  private RiZoneSortie LOMAG;
  private XRiTextField LOMAG2;
  private RiZoneSortie A1UNS;
  private XRiTextField LWAFF;
  private JLabel OBJ_37;
  private XRiTextField LWAFF2;
  private JLabel OBJ_39;
  private JComponent separator1;
  private JComponent separator2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
