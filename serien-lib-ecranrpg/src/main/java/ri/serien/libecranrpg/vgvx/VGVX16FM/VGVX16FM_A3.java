
package ri.serien.libecranrpg.vgvx.VGVX16FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX16FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] a = new String[13];
  private String[] b = new String[13];
  private String[] c = new String[13];
  private String[] d = new String[13];
  private String[] e = new String[13];
  private String[] f = new String[13];
  private String[] g = new String[13];
  
  public VGVX16FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGARTL@")).trim());
    OBJ_95.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGPGA@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGMAG@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGMAGL@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGUNS@")).trim());
    OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PGUNSL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    PGQPOS.setVisible(!lexique.HostFieldGetData("PGQPOS").trim().equalsIgnoreCase(""));
    PGQDEV.setVisible(!lexique.HostFieldGetData("PGQDEV").trim().equalsIgnoreCase(""));
    OBJ_109.setVisible(lexique.isPresent("PGQDEV"));
    OBJ_111.setVisible(lexique.isPresent("PGQPOS"));
    OBJ_128.setVisible(lexique.isPresent("PGQPOS"));
    
    if (lexique.HostFieldGetData("PGDIV").contains("-")) {
      PGDIV.setForeground(ri.serien.libcommun.outils.Constantes.COULEUR_ERREURS);
    }
    
    for (int i = 0; i < 13; i++) {
      String ligne = lexique.HostFieldGetData("L3" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)));
      a[i] = ligne.substring(0, 10);
      b[i] = ligne.substring(10, 22);
      c[i] = ligne.substring(22, 29);
      d[i] = ligne.substring(29, 34);
      e[i] = ligne.substring(34, 43);
      f[i] = ligne.substring(43, 63);
      g[i] = ligne.substring(63, 67);
    }
    
    RiZoneSortie[] A = { A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13 };
    RiZoneSortie[] B = { B1, B2, B3, B4, B5, B6, B7, B8, B9, B10, B11, B12, B13 };
    RiZoneSortie[] C = { C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11, C12, C13 };
    RiZoneSortie[] D = { D1, D2, D3, D4, D5, D6, D7, D8, D9, D10, D11, D12, D13 };
    RiZoneSortie[] E = { E1, E2, E3, E4, E5, E6, E7, E8, E9, E10, E11, E12, E13 };
    RiZoneSortie[] F = { F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12, F13 };
    RiZoneSortie[] G = { G1, G2, G3, G4, G5, G6, G7, G8, G9, G10, G11, G12, G13 };
    
    for (int i = 0; i < a.length; i++) {
      A[i].setText(a[i]);
      B[i].setText(b[i]);
      C[i].setText(c[i]);
      D[i].setText(d[i]);
      E[i].setText(e[i]);
      F[i].setText(f[i]);
      G[i].setText(g[i]);
    }
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  Affichage attendu/réservé"));
    
    

    
    p_bpresentation.setCodeEtablissement(PGETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(PGETB.getText()));
    l_plus.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    l_moins.setIcon(lexique.chargerImage("images/moins_petit.png", true));
    l_egal.setIcon(lexique.chargerImage("images/egal_petit.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_66 = new JLabel();
    PGETB = new XRiTextField();
    OBJ_67 = new JLabel();
    PGART = new XRiTextField();
    OBJ_63 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_95 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    PGSTK = new XRiTextField();
    PGATT = new XRiTextField();
    PGRES = new XRiTextField();
    PGPOS = new XRiTextField();
    PGAFF = new XRiTextField();
    PGDIV = new XRiTextField();
    OBJ_117 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_114 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_116 = new JLabel();
    l_plus = new JLabel();
    l_moins = new JLabel();
    l_egal = new JLabel();
    PGQDEV = new XRiTextField();
    OBJ_109 = new JLabel();
    OBJ_111 = new JLabel();
    PGQPOS = new XRiTextField();
    panel1 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    WTQ01 = new XRiTextField();
    WTQ02 = new XRiTextField();
    WTQ03 = new XRiTextField();
    WTQ04 = new XRiTextField();
    WTQ05 = new XRiTextField();
    WTQ06 = new XRiTextField();
    WTQ07 = new XRiTextField();
    WTQ08 = new XRiTextField();
    WTQ09 = new XRiTextField();
    WTQ10 = new XRiTextField();
    WTQ11 = new XRiTextField();
    WTQ12 = new XRiTextField();
    WTQ13 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    A1 = new RiZoneSortie();
    B1 = new RiZoneSortie();
    C1 = new RiZoneSortie();
    D1 = new RiZoneSortie();
    E1 = new RiZoneSortie();
    F1 = new RiZoneSortie();
    G1 = new RiZoneSortie();
    A2 = new RiZoneSortie();
    B2 = new RiZoneSortie();
    C2 = new RiZoneSortie();
    D2 = new RiZoneSortie();
    E2 = new RiZoneSortie();
    F2 = new RiZoneSortie();
    G2 = new RiZoneSortie();
    A3 = new RiZoneSortie();
    B3 = new RiZoneSortie();
    C3 = new RiZoneSortie();
    D3 = new RiZoneSortie();
    E3 = new RiZoneSortie();
    F3 = new RiZoneSortie();
    G3 = new RiZoneSortie();
    A4 = new RiZoneSortie();
    B4 = new RiZoneSortie();
    C4 = new RiZoneSortie();
    D4 = new RiZoneSortie();
    E4 = new RiZoneSortie();
    F4 = new RiZoneSortie();
    G4 = new RiZoneSortie();
    A5 = new RiZoneSortie();
    B5 = new RiZoneSortie();
    C5 = new RiZoneSortie();
    D5 = new RiZoneSortie();
    E5 = new RiZoneSortie();
    F5 = new RiZoneSortie();
    G5 = new RiZoneSortie();
    A6 = new RiZoneSortie();
    B6 = new RiZoneSortie();
    C6 = new RiZoneSortie();
    D6 = new RiZoneSortie();
    E6 = new RiZoneSortie();
    F6 = new RiZoneSortie();
    G6 = new RiZoneSortie();
    A7 = new RiZoneSortie();
    B7 = new RiZoneSortie();
    C7 = new RiZoneSortie();
    D7 = new RiZoneSortie();
    E7 = new RiZoneSortie();
    F7 = new RiZoneSortie();
    G7 = new RiZoneSortie();
    A8 = new RiZoneSortie();
    B8 = new RiZoneSortie();
    C8 = new RiZoneSortie();
    D8 = new RiZoneSortie();
    E8 = new RiZoneSortie();
    F8 = new RiZoneSortie();
    G8 = new RiZoneSortie();
    A9 = new RiZoneSortie();
    B9 = new RiZoneSortie();
    C9 = new RiZoneSortie();
    D9 = new RiZoneSortie();
    E9 = new RiZoneSortie();
    F9 = new RiZoneSortie();
    G9 = new RiZoneSortie();
    A10 = new RiZoneSortie();
    B10 = new RiZoneSortie();
    C10 = new RiZoneSortie();
    D10 = new RiZoneSortie();
    E10 = new RiZoneSortie();
    F10 = new RiZoneSortie();
    G10 = new RiZoneSortie();
    A11 = new RiZoneSortie();
    B11 = new RiZoneSortie();
    C11 = new RiZoneSortie();
    D11 = new RiZoneSortie();
    E11 = new RiZoneSortie();
    F11 = new RiZoneSortie();
    G11 = new RiZoneSortie();
    A12 = new RiZoneSortie();
    B12 = new RiZoneSortie();
    C12 = new RiZoneSortie();
    D12 = new RiZoneSortie();
    E12 = new RiZoneSortie();
    F12 = new RiZoneSortie();
    G12 = new RiZoneSortie();
    A13 = new RiZoneSortie();
    B13 = new RiZoneSortie();
    C13 = new RiZoneSortie();
    D13 = new RiZoneSortie();
    E13 = new RiZoneSortie();
    F13 = new RiZoneSortie();
    G13 = new RiZoneSortie();
    OBJ_85 = new JLabel();
    OBJ_86 = new RiZoneSortie();
    OBJ_88 = new RiZoneSortie();
    DREPX = new XRiCalendrier();
    OBJ_94 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_89 = new RiZoneSortie();
    OBJ_90 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Attendus / command\u00e9s");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(880, 30));
          p_tete_gauche.setMinimumSize(new Dimension(880, 30));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_66 ----
          OBJ_66.setText("Etablissement");
          OBJ_66.setName("OBJ_66");

          //---- PGETB ----
          PGETB.setName("PGETB");

          //---- OBJ_67 ----
          OBJ_67.setText("Article");
          OBJ_67.setName("OBJ_67");

          //---- PGART ----
          PGART.setName("PGART");

          //---- OBJ_63 ----
          OBJ_63.setText("@PGARTL@");
          OBJ_63.setOpaque(false);
          OBJ_63.setName("OBJ_63");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(PGETB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(PGART, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 233, GroupLayout.PREFERRED_SIZE)
                .addGap(268, 268, 268))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_66, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(PGETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(PGART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(100, 0));
          p_tete_droite.setMinimumSize(new Dimension(100, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_95 ----
          OBJ_95.setText("@PGPGA@");
          OBJ_95.setFont(OBJ_95.getFont().deriveFont(OBJ_95.getFont().getStyle() | Font.BOLD));
          OBJ_95.setName("OBJ_95");
          p_tete_droite.add(OBJ_95);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Position"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- PGSTK ----
            PGSTK.setHorizontalAlignment(SwingConstants.RIGHT);
            PGSTK.setName("PGSTK");
            panel2.add(PGSTK);
            PGSTK.setBounds(15, 45, 106, PGSTK.getPreferredSize().height);

            //---- PGATT ----
            PGATT.setHorizontalAlignment(SwingConstants.RIGHT);
            PGATT.setName("PGATT");
            panel2.add(PGATT);
            PGATT.setBounds(180, 45, 90, PGATT.getPreferredSize().height);

            //---- PGRES ----
            PGRES.setHorizontalAlignment(SwingConstants.RIGHT);
            PGRES.setName("PGRES");
            panel2.add(PGRES);
            PGRES.setBounds(330, 45, 90, PGRES.getPreferredSize().height);

            //---- PGPOS ----
            PGPOS.setForeground(Color.black);
            PGPOS.setFont(PGPOS.getFont().deriveFont(PGPOS.getFont().getStyle() | Font.BOLD));
            PGPOS.setName("PGPOS");
            panel2.add(PGPOS);
            PGPOS.setBounds(475, 45, 90, PGPOS.getPreferredSize().height);

            //---- PGAFF ----
            PGAFF.setHorizontalAlignment(SwingConstants.RIGHT);
            PGAFF.setName("PGAFF");
            panel2.add(PGAFF);
            PGAFF.setBounds(595, 45, 106, PGAFF.getPreferredSize().height);

            //---- PGDIV ----
            PGDIV.setFont(PGDIV.getFont().deriveFont(PGDIV.getFont().getStyle() & ~Font.BOLD));
            PGDIV.setForeground(Color.black);
            PGDIV.setName("PGDIV");
            panel2.add(PGDIV);
            PGDIV.setBounds(730, 45, 106, PGDIV.getPreferredSize().height);

            //---- OBJ_117 ----
            OBJ_117.setText("R\u00e9serv\u00e9");
            OBJ_117.setName("OBJ_117");
            panel2.add(OBJ_117);
            OBJ_117.setBounds(595, 25, 82, 23);

            //---- OBJ_128 ----
            OBJ_128.setText("Disponible");
            OBJ_128.setForeground(Color.black);
            OBJ_128.setFont(OBJ_128.getFont().deriveFont(OBJ_128.getFont().getStyle() | Font.BOLD));
            OBJ_128.setName("OBJ_128");
            panel2.add(OBJ_128);
            OBJ_128.setBounds(730, 25, 74, 23);

            //---- OBJ_115 ----
            OBJ_115.setText("Command\u00e9");
            OBJ_115.setName("OBJ_115");
            panel2.add(OBJ_115);
            OBJ_115.setBounds(330, 25, 73, 23);

            //---- OBJ_114 ----
            OBJ_114.setText("Attendu");
            OBJ_114.setName("OBJ_114");
            panel2.add(OBJ_114);
            OBJ_114.setBounds(180, 25, 67, 23);

            //---- OBJ_113 ----
            OBJ_113.setText("Stock");
            OBJ_113.setName("OBJ_113");
            panel2.add(OBJ_113);
            OBJ_113.setBounds(15, 25, 55, 23);

            //---- OBJ_116 ----
            OBJ_116.setText("Position");
            OBJ_116.setForeground(Color.black);
            OBJ_116.setFont(OBJ_116.getFont().deriveFont(OBJ_116.getFont().getStyle() | Font.BOLD));
            OBJ_116.setName("OBJ_116");
            panel2.add(OBJ_116);
            OBJ_116.setBounds(475, 25, 80, 23);

            //---- l_plus ----
            l_plus.setName("l_plus");
            panel2.add(l_plus);
            l_plus.setBounds(145, 55, 16, 16);

            //---- l_moins ----
            l_moins.setName("l_moins");
            panel2.add(l_moins);
            l_moins.setBounds(290, 55, 16, 16);

            //---- l_egal ----
            l_egal.setName("l_egal");
            panel2.add(l_egal);
            l_egal.setBounds(440, 55, 16, 16);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(13, 447, 879, 93);

          //---- PGQDEV ----
          PGQDEV.setHorizontalAlignment(SwingConstants.RIGHT);
          PGQDEV.setName("PGQDEV");
          p_contenu.add(PGQDEV);
          PGQDEV.setBounds(70, 420, 106, PGQDEV.getPreferredSize().height);

          //---- OBJ_109 ----
          OBJ_109.setText("Devis");
          OBJ_109.setName("OBJ_109");
          p_contenu.add(OBJ_109);
          OBJ_109.setBounds(15, 424, 47, 20);

          //---- OBJ_111 ----
          OBJ_111.setText("Commandes de positionnement");
          OBJ_111.setName("OBJ_111");
          p_contenu.add(OBJ_111);
          OBJ_111.setBounds(585, 424, 185, 20);

          //---- PGQPOS ----
          PGQPOS.setHorizontalAlignment(SwingConstants.RIGHT);
          PGQPOS.setName("PGQPOS");
          p_contenu.add(PGQPOS);
          PGQPOS.setBounds(786, 420, 106, PGQPOS.getPreferredSize().height);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(830, 30, 25, 145);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(830, 210, 25, 146);

            //---- WTQ01 ----
            WTQ01.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ01.setName("WTQ01");
            panel1.add(WTQ01);
            WTQ01.setBounds(15, 28, 90, WTQ01.getPreferredSize().height);

            //---- WTQ02 ----
            WTQ02.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ02.setName("WTQ02");
            panel1.add(WTQ02);
            WTQ02.setBounds(15, 53, 90, WTQ02.getPreferredSize().height);

            //---- WTQ03 ----
            WTQ03.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ03.setName("WTQ03");
            panel1.add(WTQ03);
            WTQ03.setBounds(15, 78, 90, WTQ03.getPreferredSize().height);

            //---- WTQ04 ----
            WTQ04.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ04.setName("WTQ04");
            panel1.add(WTQ04);
            WTQ04.setBounds(15, 103, 90, WTQ04.getPreferredSize().height);

            //---- WTQ05 ----
            WTQ05.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ05.setName("WTQ05");
            panel1.add(WTQ05);
            WTQ05.setBounds(15, 128, 90, WTQ05.getPreferredSize().height);

            //---- WTQ06 ----
            WTQ06.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ06.setName("WTQ06");
            panel1.add(WTQ06);
            WTQ06.setBounds(15, 153, 90, WTQ06.getPreferredSize().height);

            //---- WTQ07 ----
            WTQ07.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ07.setName("WTQ07");
            panel1.add(WTQ07);
            WTQ07.setBounds(15, 178, 90, WTQ07.getPreferredSize().height);

            //---- WTQ08 ----
            WTQ08.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ08.setName("WTQ08");
            panel1.add(WTQ08);
            WTQ08.setBounds(15, 203, 90, WTQ08.getPreferredSize().height);

            //---- WTQ09 ----
            WTQ09.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ09.setName("WTQ09");
            panel1.add(WTQ09);
            WTQ09.setBounds(15, 228, 90, WTQ09.getPreferredSize().height);

            //---- WTQ10 ----
            WTQ10.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ10.setName("WTQ10");
            panel1.add(WTQ10);
            WTQ10.setBounds(15, 253, 90, WTQ10.getPreferredSize().height);

            //---- WTQ11 ----
            WTQ11.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ11.setName("WTQ11");
            panel1.add(WTQ11);
            WTQ11.setBounds(15, 278, 90, WTQ11.getPreferredSize().height);

            //---- WTQ12 ----
            WTQ12.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ12.setName("WTQ12");
            panel1.add(WTQ12);
            WTQ12.setBounds(15, 303, 90, WTQ12.getPreferredSize().height);

            //---- WTQ13 ----
            WTQ13.setHorizontalAlignment(SwingConstants.RIGHT);
            WTQ13.setName("WTQ13");
            panel1.add(WTQ13);
            WTQ13.setBounds(15, 328, 90, WTQ13.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Qt\u00e9 affectations");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(10, 5, 100, 26);

            //---- label2 ----
            label2.setText("Qt\u00e9 attendue");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setHorizontalAlignment(SwingConstants.CENTER);
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(123, 5, 85, 26);

            //---- label3 ----
            label3.setText("Qt\u00e9 dispo");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setHorizontalAlignment(SwingConstants.CENTER);
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(235, 5, 70, 26);

            //---- label4 ----
            label4.setText("n\u00b0 bon");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setHorizontalAlignment(SwingConstants.CENTER);
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(330, 5, 50, 26);

            //---- label5 ----
            label5.setText("n\u00b0 ligne");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setHorizontalAlignment(SwingConstants.CENTER);
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(393, 5, 55, 26);

            //---- label6 ----
            label6.setText("Liv. pr\u00e9vue");
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setHorizontalAlignment(SwingConstants.CENTER);
            label6.setName("label6");
            panel1.add(label6);
            label6.setBounds(468, 5, 75, 26);

            //---- label7 ----
            label7.setText("Fournisseur");
            label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
            label7.setHorizontalAlignment(SwingConstants.CENTER);
            label7.setName("label7");
            panel1.add(label7);
            label7.setBounds(623, 5, 85, 26);

            //---- A1 ----
            A1.setText("text");
            A1.setHorizontalTextPosition(SwingConstants.RIGHT);
            A1.setHorizontalAlignment(SwingConstants.RIGHT);
            A1.setName("A1");
            panel1.add(A1);
            A1.setBounds(new Rectangle(new Point(115, 30), A1.getPreferredSize()));

            //---- B1 ----
            B1.setText("text");
            B1.setHorizontalTextPosition(SwingConstants.RIGHT);
            B1.setHorizontalAlignment(SwingConstants.RIGHT);
            B1.setName("B1");
            panel1.add(B1);
            B1.setBounds(new Rectangle(new Point(220, 30), B1.getPreferredSize()));

            //---- C1 ----
            C1.setText("text");
            C1.setName("C1");
            panel1.add(C1);
            C1.setBounds(325, 30, 60, C1.getPreferredSize().height);

            //---- D1 ----
            D1.setText("text");
            D1.setHorizontalAlignment(SwingConstants.RIGHT);
            D1.setName("D1");
            panel1.add(D1);
            D1.setBounds(390, 30, 60, D1.getPreferredSize().height);

            //---- E1 ----
            E1.setText("text");
            E1.setName("E1");
            panel1.add(E1);
            E1.setBounds(new Rectangle(new Point(455, 30), E1.getPreferredSize()));

            //---- F1 ----
            F1.setText("text");
            F1.setName("F1");
            panel1.add(F1);
            F1.setBounds(560, 30, 210, F1.getPreferredSize().height);

            //---- G1 ----
            G1.setText("text");
            G1.setName("G1");
            panel1.add(G1);
            G1.setBounds(775, 30, 34, G1.getPreferredSize().height);

            //---- A2 ----
            A2.setText("text");
            A2.setHorizontalTextPosition(SwingConstants.RIGHT);
            A2.setHorizontalAlignment(SwingConstants.RIGHT);
            A2.setName("A2");
            panel1.add(A2);
            A2.setBounds(new Rectangle(new Point(115, 55), A2.getPreferredSize()));

            //---- B2 ----
            B2.setText("text");
            B2.setHorizontalTextPosition(SwingConstants.RIGHT);
            B2.setHorizontalAlignment(SwingConstants.RIGHT);
            B2.setName("B2");
            panel1.add(B2);
            B2.setBounds(new Rectangle(new Point(220, 55), B2.getPreferredSize()));

            //---- C2 ----
            C2.setText("text");
            C2.setName("C2");
            panel1.add(C2);
            C2.setBounds(325, 55, 60, C2.getPreferredSize().height);

            //---- D2 ----
            D2.setText("text");
            D2.setHorizontalAlignment(SwingConstants.RIGHT);
            D2.setName("D2");
            panel1.add(D2);
            D2.setBounds(390, 55, 60, D2.getPreferredSize().height);

            //---- E2 ----
            E2.setText("text");
            E2.setName("E2");
            panel1.add(E2);
            E2.setBounds(new Rectangle(new Point(455, 55), E2.getPreferredSize()));

            //---- F2 ----
            F2.setText("text");
            F2.setName("F2");
            panel1.add(F2);
            F2.setBounds(560, 55, 210, F2.getPreferredSize().height);

            //---- G2 ----
            G2.setText("text");
            G2.setName("G2");
            panel1.add(G2);
            G2.setBounds(775, 55, 34, G2.getPreferredSize().height);

            //---- A3 ----
            A3.setText("text");
            A3.setHorizontalTextPosition(SwingConstants.RIGHT);
            A3.setHorizontalAlignment(SwingConstants.RIGHT);
            A3.setName("A3");
            panel1.add(A3);
            A3.setBounds(new Rectangle(new Point(115, 80), A3.getPreferredSize()));

            //---- B3 ----
            B3.setText("text");
            B3.setHorizontalTextPosition(SwingConstants.RIGHT);
            B3.setHorizontalAlignment(SwingConstants.RIGHT);
            B3.setName("B3");
            panel1.add(B3);
            B3.setBounds(new Rectangle(new Point(220, 80), B3.getPreferredSize()));

            //---- C3 ----
            C3.setText("text");
            C3.setName("C3");
            panel1.add(C3);
            C3.setBounds(325, 80, 60, C3.getPreferredSize().height);

            //---- D3 ----
            D3.setText("text");
            D3.setHorizontalAlignment(SwingConstants.RIGHT);
            D3.setName("D3");
            panel1.add(D3);
            D3.setBounds(390, 80, 60, D3.getPreferredSize().height);

            //---- E3 ----
            E3.setText("text");
            E3.setName("E3");
            panel1.add(E3);
            E3.setBounds(new Rectangle(new Point(455, 80), E3.getPreferredSize()));

            //---- F3 ----
            F3.setText("text");
            F3.setName("F3");
            panel1.add(F3);
            F3.setBounds(560, 80, 210, F3.getPreferredSize().height);

            //---- G3 ----
            G3.setText("text");
            G3.setName("G3");
            panel1.add(G3);
            G3.setBounds(775, 80, 34, G3.getPreferredSize().height);

            //---- A4 ----
            A4.setText("text");
            A4.setHorizontalTextPosition(SwingConstants.RIGHT);
            A4.setHorizontalAlignment(SwingConstants.RIGHT);
            A4.setName("A4");
            panel1.add(A4);
            A4.setBounds(new Rectangle(new Point(115, 105), A4.getPreferredSize()));

            //---- B4 ----
            B4.setText("text");
            B4.setHorizontalTextPosition(SwingConstants.RIGHT);
            B4.setHorizontalAlignment(SwingConstants.RIGHT);
            B4.setName("B4");
            panel1.add(B4);
            B4.setBounds(new Rectangle(new Point(220, 105), B4.getPreferredSize()));

            //---- C4 ----
            C4.setText("text");
            C4.setName("C4");
            panel1.add(C4);
            C4.setBounds(325, 105, 60, C4.getPreferredSize().height);

            //---- D4 ----
            D4.setText("text");
            D4.setHorizontalAlignment(SwingConstants.RIGHT);
            D4.setName("D4");
            panel1.add(D4);
            D4.setBounds(390, 105, 60, D4.getPreferredSize().height);

            //---- E4 ----
            E4.setText("text");
            E4.setName("E4");
            panel1.add(E4);
            E4.setBounds(new Rectangle(new Point(455, 105), E4.getPreferredSize()));

            //---- F4 ----
            F4.setText("text");
            F4.setName("F4");
            panel1.add(F4);
            F4.setBounds(560, 105, 210, F4.getPreferredSize().height);

            //---- G4 ----
            G4.setText("text");
            G4.setName("G4");
            panel1.add(G4);
            G4.setBounds(775, 105, 34, G4.getPreferredSize().height);

            //---- A5 ----
            A5.setText("text");
            A5.setHorizontalTextPosition(SwingConstants.RIGHT);
            A5.setHorizontalAlignment(SwingConstants.RIGHT);
            A5.setName("A5");
            panel1.add(A5);
            A5.setBounds(new Rectangle(new Point(115, 130), A5.getPreferredSize()));

            //---- B5 ----
            B5.setText("text");
            B5.setHorizontalTextPosition(SwingConstants.RIGHT);
            B5.setHorizontalAlignment(SwingConstants.RIGHT);
            B5.setName("B5");
            panel1.add(B5);
            B5.setBounds(new Rectangle(new Point(220, 130), B5.getPreferredSize()));

            //---- C5 ----
            C5.setText("text");
            C5.setName("C5");
            panel1.add(C5);
            C5.setBounds(325, 130, 60, C5.getPreferredSize().height);

            //---- D5 ----
            D5.setText("text");
            D5.setHorizontalAlignment(SwingConstants.RIGHT);
            D5.setName("D5");
            panel1.add(D5);
            D5.setBounds(390, 130, 60, D5.getPreferredSize().height);

            //---- E5 ----
            E5.setText("text");
            E5.setName("E5");
            panel1.add(E5);
            E5.setBounds(new Rectangle(new Point(455, 130), E5.getPreferredSize()));

            //---- F5 ----
            F5.setText("text");
            F5.setName("F5");
            panel1.add(F5);
            F5.setBounds(560, 130, 210, F5.getPreferredSize().height);

            //---- G5 ----
            G5.setText("text");
            G5.setName("G5");
            panel1.add(G5);
            G5.setBounds(775, 130, 34, G5.getPreferredSize().height);

            //---- A6 ----
            A6.setText("text");
            A6.setHorizontalTextPosition(SwingConstants.RIGHT);
            A6.setHorizontalAlignment(SwingConstants.RIGHT);
            A6.setName("A6");
            panel1.add(A6);
            A6.setBounds(new Rectangle(new Point(115, 155), A6.getPreferredSize()));

            //---- B6 ----
            B6.setText("text");
            B6.setHorizontalTextPosition(SwingConstants.RIGHT);
            B6.setHorizontalAlignment(SwingConstants.RIGHT);
            B6.setName("B6");
            panel1.add(B6);
            B6.setBounds(new Rectangle(new Point(220, 155), B6.getPreferredSize()));

            //---- C6 ----
            C6.setText("text");
            C6.setName("C6");
            panel1.add(C6);
            C6.setBounds(325, 155, 60, C6.getPreferredSize().height);

            //---- D6 ----
            D6.setText("text");
            D6.setHorizontalAlignment(SwingConstants.RIGHT);
            D6.setName("D6");
            panel1.add(D6);
            D6.setBounds(390, 155, 60, D6.getPreferredSize().height);

            //---- E6 ----
            E6.setText("text");
            E6.setName("E6");
            panel1.add(E6);
            E6.setBounds(new Rectangle(new Point(455, 155), E6.getPreferredSize()));

            //---- F6 ----
            F6.setText("text");
            F6.setName("F6");
            panel1.add(F6);
            F6.setBounds(560, 155, 210, F6.getPreferredSize().height);

            //---- G6 ----
            G6.setText("text");
            G6.setName("G6");
            panel1.add(G6);
            G6.setBounds(775, 155, 34, G6.getPreferredSize().height);

            //---- A7 ----
            A7.setText("text");
            A7.setHorizontalTextPosition(SwingConstants.RIGHT);
            A7.setHorizontalAlignment(SwingConstants.RIGHT);
            A7.setName("A7");
            panel1.add(A7);
            A7.setBounds(new Rectangle(new Point(115, 180), A7.getPreferredSize()));

            //---- B7 ----
            B7.setText("text");
            B7.setHorizontalTextPosition(SwingConstants.RIGHT);
            B7.setHorizontalAlignment(SwingConstants.RIGHT);
            B7.setName("B7");
            panel1.add(B7);
            B7.setBounds(new Rectangle(new Point(220, 180), B7.getPreferredSize()));

            //---- C7 ----
            C7.setText("text");
            C7.setName("C7");
            panel1.add(C7);
            C7.setBounds(325, 180, 60, C7.getPreferredSize().height);

            //---- D7 ----
            D7.setText("text");
            D7.setHorizontalAlignment(SwingConstants.RIGHT);
            D7.setName("D7");
            panel1.add(D7);
            D7.setBounds(390, 180, 60, D7.getPreferredSize().height);

            //---- E7 ----
            E7.setText("text");
            E7.setName("E7");
            panel1.add(E7);
            E7.setBounds(new Rectangle(new Point(455, 180), E7.getPreferredSize()));

            //---- F7 ----
            F7.setText("text");
            F7.setName("F7");
            panel1.add(F7);
            F7.setBounds(560, 180, 210, F7.getPreferredSize().height);

            //---- G7 ----
            G7.setText("text");
            G7.setName("G7");
            panel1.add(G7);
            G7.setBounds(775, 180, 34, G7.getPreferredSize().height);

            //---- A8 ----
            A8.setText("text");
            A8.setHorizontalTextPosition(SwingConstants.RIGHT);
            A8.setHorizontalAlignment(SwingConstants.RIGHT);
            A8.setName("A8");
            panel1.add(A8);
            A8.setBounds(new Rectangle(new Point(115, 205), A8.getPreferredSize()));

            //---- B8 ----
            B8.setText("text");
            B8.setHorizontalTextPosition(SwingConstants.RIGHT);
            B8.setHorizontalAlignment(SwingConstants.RIGHT);
            B8.setName("B8");
            panel1.add(B8);
            B8.setBounds(new Rectangle(new Point(220, 205), B8.getPreferredSize()));

            //---- C8 ----
            C8.setText("text");
            C8.setName("C8");
            panel1.add(C8);
            C8.setBounds(325, 205, 60, C8.getPreferredSize().height);

            //---- D8 ----
            D8.setText("text");
            D8.setHorizontalAlignment(SwingConstants.RIGHT);
            D8.setName("D8");
            panel1.add(D8);
            D8.setBounds(390, 205, 60, D8.getPreferredSize().height);

            //---- E8 ----
            E8.setText("text");
            E8.setName("E8");
            panel1.add(E8);
            E8.setBounds(new Rectangle(new Point(455, 205), E8.getPreferredSize()));

            //---- F8 ----
            F8.setText("text");
            F8.setName("F8");
            panel1.add(F8);
            F8.setBounds(560, 205, 210, F8.getPreferredSize().height);

            //---- G8 ----
            G8.setText("text");
            G8.setName("G8");
            panel1.add(G8);
            G8.setBounds(775, 205, 34, G8.getPreferredSize().height);

            //---- A9 ----
            A9.setText("text");
            A9.setHorizontalTextPosition(SwingConstants.RIGHT);
            A9.setHorizontalAlignment(SwingConstants.RIGHT);
            A9.setName("A9");
            panel1.add(A9);
            A9.setBounds(new Rectangle(new Point(115, 230), A9.getPreferredSize()));

            //---- B9 ----
            B9.setText("text");
            B9.setHorizontalTextPosition(SwingConstants.RIGHT);
            B9.setHorizontalAlignment(SwingConstants.RIGHT);
            B9.setName("B9");
            panel1.add(B9);
            B9.setBounds(new Rectangle(new Point(220, 230), B9.getPreferredSize()));

            //---- C9 ----
            C9.setText("text");
            C9.setName("C9");
            panel1.add(C9);
            C9.setBounds(325, 230, 60, C9.getPreferredSize().height);

            //---- D9 ----
            D9.setText("text");
            D9.setHorizontalAlignment(SwingConstants.RIGHT);
            D9.setName("D9");
            panel1.add(D9);
            D9.setBounds(390, 230, 60, D9.getPreferredSize().height);

            //---- E9 ----
            E9.setText("text");
            E9.setName("E9");
            panel1.add(E9);
            E9.setBounds(new Rectangle(new Point(455, 230), E9.getPreferredSize()));

            //---- F9 ----
            F9.setText("text");
            F9.setName("F9");
            panel1.add(F9);
            F9.setBounds(560, 230, 210, F9.getPreferredSize().height);

            //---- G9 ----
            G9.setText("text");
            G9.setName("G9");
            panel1.add(G9);
            G9.setBounds(775, 230, 34, G9.getPreferredSize().height);

            //---- A10 ----
            A10.setText("text");
            A10.setHorizontalTextPosition(SwingConstants.RIGHT);
            A10.setHorizontalAlignment(SwingConstants.RIGHT);
            A10.setName("A10");
            panel1.add(A10);
            A10.setBounds(new Rectangle(new Point(115, 255), A10.getPreferredSize()));

            //---- B10 ----
            B10.setText("text");
            B10.setHorizontalTextPosition(SwingConstants.RIGHT);
            B10.setHorizontalAlignment(SwingConstants.RIGHT);
            B10.setName("B10");
            panel1.add(B10);
            B10.setBounds(new Rectangle(new Point(220, 255), B10.getPreferredSize()));

            //---- C10 ----
            C10.setText("text");
            C10.setName("C10");
            panel1.add(C10);
            C10.setBounds(325, 255, 60, C10.getPreferredSize().height);

            //---- D10 ----
            D10.setText("text");
            D10.setHorizontalAlignment(SwingConstants.RIGHT);
            D10.setName("D10");
            panel1.add(D10);
            D10.setBounds(390, 255, 60, D10.getPreferredSize().height);

            //---- E10 ----
            E10.setText("text");
            E10.setName("E10");
            panel1.add(E10);
            E10.setBounds(new Rectangle(new Point(455, 255), E10.getPreferredSize()));

            //---- F10 ----
            F10.setText("text");
            F10.setName("F10");
            panel1.add(F10);
            F10.setBounds(560, 255, 210, F10.getPreferredSize().height);

            //---- G10 ----
            G10.setText("text");
            G10.setName("G10");
            panel1.add(G10);
            G10.setBounds(775, 255, 34, G10.getPreferredSize().height);

            //---- A11 ----
            A11.setText("text");
            A11.setHorizontalTextPosition(SwingConstants.RIGHT);
            A11.setHorizontalAlignment(SwingConstants.RIGHT);
            A11.setName("A11");
            panel1.add(A11);
            A11.setBounds(new Rectangle(new Point(115, 280), A11.getPreferredSize()));

            //---- B11 ----
            B11.setText("text");
            B11.setHorizontalTextPosition(SwingConstants.RIGHT);
            B11.setHorizontalAlignment(SwingConstants.RIGHT);
            B11.setName("B11");
            panel1.add(B11);
            B11.setBounds(new Rectangle(new Point(220, 280), B11.getPreferredSize()));

            //---- C11 ----
            C11.setText("text");
            C11.setName("C11");
            panel1.add(C11);
            C11.setBounds(325, 280, 60, C11.getPreferredSize().height);

            //---- D11 ----
            D11.setText("text");
            D11.setHorizontalAlignment(SwingConstants.RIGHT);
            D11.setName("D11");
            panel1.add(D11);
            D11.setBounds(390, 280, 60, D11.getPreferredSize().height);

            //---- E11 ----
            E11.setText("text");
            E11.setName("E11");
            panel1.add(E11);
            E11.setBounds(new Rectangle(new Point(455, 280), E11.getPreferredSize()));

            //---- F11 ----
            F11.setText("text");
            F11.setName("F11");
            panel1.add(F11);
            F11.setBounds(560, 280, 210, F11.getPreferredSize().height);

            //---- G11 ----
            G11.setText("text");
            G11.setName("G11");
            panel1.add(G11);
            G11.setBounds(775, 280, 34, G11.getPreferredSize().height);

            //---- A12 ----
            A12.setText("text");
            A12.setHorizontalTextPosition(SwingConstants.RIGHT);
            A12.setHorizontalAlignment(SwingConstants.RIGHT);
            A12.setName("A12");
            panel1.add(A12);
            A12.setBounds(new Rectangle(new Point(115, 305), A12.getPreferredSize()));

            //---- B12 ----
            B12.setText("text");
            B12.setHorizontalTextPosition(SwingConstants.RIGHT);
            B12.setHorizontalAlignment(SwingConstants.RIGHT);
            B12.setName("B12");
            panel1.add(B12);
            B12.setBounds(new Rectangle(new Point(220, 305), B12.getPreferredSize()));

            //---- C12 ----
            C12.setText("text");
            C12.setName("C12");
            panel1.add(C12);
            C12.setBounds(325, 305, 60, C12.getPreferredSize().height);

            //---- D12 ----
            D12.setText("text");
            D12.setHorizontalAlignment(SwingConstants.RIGHT);
            D12.setName("D12");
            panel1.add(D12);
            D12.setBounds(390, 305, 60, D12.getPreferredSize().height);

            //---- E12 ----
            E12.setText("text");
            E12.setName("E12");
            panel1.add(E12);
            E12.setBounds(new Rectangle(new Point(455, 305), E12.getPreferredSize()));

            //---- F12 ----
            F12.setText("text");
            F12.setName("F12");
            panel1.add(F12);
            F12.setBounds(560, 305, 210, F12.getPreferredSize().height);

            //---- G12 ----
            G12.setText("text");
            G12.setName("G12");
            panel1.add(G12);
            G12.setBounds(775, 305, 34, G12.getPreferredSize().height);

            //---- A13 ----
            A13.setText("text");
            A13.setHorizontalTextPosition(SwingConstants.RIGHT);
            A13.setHorizontalAlignment(SwingConstants.RIGHT);
            A13.setName("A13");
            panel1.add(A13);
            A13.setBounds(new Rectangle(new Point(115, 330), A13.getPreferredSize()));

            //---- B13 ----
            B13.setText("text");
            B13.setHorizontalTextPosition(SwingConstants.RIGHT);
            B13.setHorizontalAlignment(SwingConstants.RIGHT);
            B13.setName("B13");
            panel1.add(B13);
            B13.setBounds(new Rectangle(new Point(220, 330), B13.getPreferredSize()));

            //---- C13 ----
            C13.setText("text");
            C13.setName("C13");
            panel1.add(C13);
            C13.setBounds(325, 330, 60, C13.getPreferredSize().height);

            //---- D13 ----
            D13.setText("text");
            D13.setHorizontalAlignment(SwingConstants.RIGHT);
            D13.setName("D13");
            panel1.add(D13);
            D13.setBounds(390, 330, 60, D13.getPreferredSize().height);

            //---- E13 ----
            E13.setText("text");
            E13.setName("E13");
            panel1.add(E13);
            E13.setBounds(new Rectangle(new Point(455, 330), E13.getPreferredSize()));

            //---- F13 ----
            F13.setText("text");
            F13.setName("F13");
            panel1.add(F13);
            F13.setBounds(560, 330, 210, F13.getPreferredSize().height);

            //---- G13 ----
            G13.setText("text");
            G13.setName("G13");
            panel1.add(G13);
            G13.setBounds(775, 330, 34, G13.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(13, 45, 879, 375);

          //---- OBJ_85 ----
          OBJ_85.setText("Magasin");
          OBJ_85.setName("OBJ_85");
          p_contenu.add(OBJ_85);
          OBJ_85.setBounds(13, 14, 55, 24);

          //---- OBJ_86 ----
          OBJ_86.setText("@PGMAG@");
          OBJ_86.setName("OBJ_86");
          p_contenu.add(OBJ_86);
          OBJ_86.setBounds(78, 14, 34, OBJ_86.getPreferredSize().height);

          //---- OBJ_88 ----
          OBJ_88.setText("@PGMAGL@");
          OBJ_88.setName("OBJ_88");
          p_contenu.add(OBJ_88);
          OBJ_88.setBounds(115, 14, 232, OBJ_88.getPreferredSize().height);

          //---- DREPX ----
          DREPX.setName("DREPX");
          p_contenu.add(DREPX);
          DREPX.setBounds(790, 12, 105, DREPX.getPreferredSize().height);

          //---- OBJ_94 ----
          OBJ_94.setText("Date de r\u00e9ception th\u00e9orique");
          OBJ_94.setName("OBJ_94");
          p_contenu.add(OBJ_94);
          OBJ_94.setBounds(625, 14, 165, 24);

          //---- OBJ_91 ----
          OBJ_91.setText("Unit\u00e9 stock");
          OBJ_91.setName("OBJ_91");
          p_contenu.add(OBJ_91);
          OBJ_91.setBounds(360, 14, 78, 24);

          //---- OBJ_89 ----
          OBJ_89.setText("@PGUNS@");
          OBJ_89.setName("OBJ_89");
          p_contenu.add(OBJ_89);
          OBJ_89.setBounds(430, 14, 24, OBJ_89.getPreferredSize().height);

          //---- OBJ_90 ----
          OBJ_90.setText("@PGUNSL@");
          OBJ_90.setName("OBJ_90");
          p_contenu.add(OBJ_90);
          OBJ_90.setBounds(460, 14, 151, OBJ_90.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_66;
  private XRiTextField PGETB;
  private JLabel OBJ_67;
  private XRiTextField PGART;
  private RiZoneSortie OBJ_63;
  private JPanel p_tete_droite;
  private JLabel OBJ_95;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField PGSTK;
  private XRiTextField PGATT;
  private XRiTextField PGRES;
  private XRiTextField PGPOS;
  private XRiTextField PGAFF;
  private XRiTextField PGDIV;
  private JLabel OBJ_117;
  private JLabel OBJ_128;
  private JLabel OBJ_115;
  private JLabel OBJ_114;
  private JLabel OBJ_113;
  private JLabel OBJ_116;
  private JLabel l_plus;
  private JLabel l_moins;
  private JLabel l_egal;
  private XRiTextField PGQDEV;
  private JLabel OBJ_109;
  private JLabel OBJ_111;
  private XRiTextField PGQPOS;
  private JPanel panel1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiTextField WTQ01;
  private XRiTextField WTQ02;
  private XRiTextField WTQ03;
  private XRiTextField WTQ04;
  private XRiTextField WTQ05;
  private XRiTextField WTQ06;
  private XRiTextField WTQ07;
  private XRiTextField WTQ08;
  private XRiTextField WTQ09;
  private XRiTextField WTQ10;
  private XRiTextField WTQ11;
  private XRiTextField WTQ12;
  private XRiTextField WTQ13;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private RiZoneSortie A1;
  private RiZoneSortie B1;
  private RiZoneSortie C1;
  private RiZoneSortie D1;
  private RiZoneSortie E1;
  private RiZoneSortie F1;
  private RiZoneSortie G1;
  private RiZoneSortie A2;
  private RiZoneSortie B2;
  private RiZoneSortie C2;
  private RiZoneSortie D2;
  private RiZoneSortie E2;
  private RiZoneSortie F2;
  private RiZoneSortie G2;
  private RiZoneSortie A3;
  private RiZoneSortie B3;
  private RiZoneSortie C3;
  private RiZoneSortie D3;
  private RiZoneSortie E3;
  private RiZoneSortie F3;
  private RiZoneSortie G3;
  private RiZoneSortie A4;
  private RiZoneSortie B4;
  private RiZoneSortie C4;
  private RiZoneSortie D4;
  private RiZoneSortie E4;
  private RiZoneSortie F4;
  private RiZoneSortie G4;
  private RiZoneSortie A5;
  private RiZoneSortie B5;
  private RiZoneSortie C5;
  private RiZoneSortie D5;
  private RiZoneSortie E5;
  private RiZoneSortie F5;
  private RiZoneSortie G5;
  private RiZoneSortie A6;
  private RiZoneSortie B6;
  private RiZoneSortie C6;
  private RiZoneSortie D6;
  private RiZoneSortie E6;
  private RiZoneSortie F6;
  private RiZoneSortie G6;
  private RiZoneSortie A7;
  private RiZoneSortie B7;
  private RiZoneSortie C7;
  private RiZoneSortie D7;
  private RiZoneSortie E7;
  private RiZoneSortie F7;
  private RiZoneSortie G7;
  private RiZoneSortie A8;
  private RiZoneSortie B8;
  private RiZoneSortie C8;
  private RiZoneSortie D8;
  private RiZoneSortie E8;
  private RiZoneSortie F8;
  private RiZoneSortie G8;
  private RiZoneSortie A9;
  private RiZoneSortie B9;
  private RiZoneSortie C9;
  private RiZoneSortie D9;
  private RiZoneSortie E9;
  private RiZoneSortie F9;
  private RiZoneSortie G9;
  private RiZoneSortie A10;
  private RiZoneSortie B10;
  private RiZoneSortie C10;
  private RiZoneSortie D10;
  private RiZoneSortie E10;
  private RiZoneSortie F10;
  private RiZoneSortie G10;
  private RiZoneSortie A11;
  private RiZoneSortie B11;
  private RiZoneSortie C11;
  private RiZoneSortie D11;
  private RiZoneSortie E11;
  private RiZoneSortie F11;
  private RiZoneSortie G11;
  private RiZoneSortie A12;
  private RiZoneSortie B12;
  private RiZoneSortie C12;
  private RiZoneSortie D12;
  private RiZoneSortie E12;
  private RiZoneSortie F12;
  private RiZoneSortie G12;
  private RiZoneSortie A13;
  private RiZoneSortie B13;
  private RiZoneSortie C13;
  private RiZoneSortie D13;
  private RiZoneSortie E13;
  private RiZoneSortie F13;
  private RiZoneSortie G13;
  private JLabel OBJ_85;
  private RiZoneSortie OBJ_86;
  private RiZoneSortie OBJ_88;
  private XRiCalendrier DREPX;
  private JLabel OBJ_94;
  private JLabel OBJ_91;
  private RiZoneSortie OBJ_89;
  private RiZoneSortie OBJ_90;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
