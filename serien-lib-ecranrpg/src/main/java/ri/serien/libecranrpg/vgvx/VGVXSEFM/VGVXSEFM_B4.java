
package ri.serien.libecranrpg.vgvx.VGVXSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXSEFM_B4 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] A_Value = { "", "0", "1", "2", "3", };
  private String[] B_Value = { "", "0", "1", };
  private String[] D_Value = { "", "0", "1", "2", "3", "4" };
  private String[] E_Value = { "", "0", "1", "2", "3", "9" };
  private String[] F_Value = { "", "0", "1", "2", "3", "4", "9" };
  private String[] G_Value = { "0", "1", "2" };
  
  public VGVXSEFM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SET081.setValeurs(F_Value, null);
    SET093.setValeurs(D_Value, null);
    SET146.setValeurs(B_Value, null);
    SET179.setValeurs(G_Value, null);
    SET073.setValeurs(E_Value, null);
    SET072.setValeurs(A_Value, null);
    SET071.setValeurs(A_Value, null);
    SET092.setValeursSelection("0", "1");
    SET166.setValeursSelection("0", "1");
    SET156.setValeursSelection("0", "1");
    SET090.setValeursSelection("0", "1");
    SET091.setValeursSelection("0", "1");
    SET082.setValeursSelection("0", "1");
    SET089.setValeursSelection("0", "1");
    SET088.setValeursSelection("0", "1");
    SET087.setValeursSelection("0", "1");
    SET086.setValeursSelection("0", "1");
    SET085.setValeursSelection("0", "1");
    SET084.setValeursSelection("0", "1");
    SET083.setValeursSelection("0", "1");
    SET080.setValeursSelection("0", "1");
    SET094.setValeursSelection("0", "1");
    SET095.setValeursSelection("0", "1");
    SET142.setValeursSelection("0", "1");
    SET079.setValeursSelection("0", "1");
    SET078.setValeursSelection("0", "1");
    SET077.setValeursSelection("0", "1");
    SET076.setValeursSelection("0", "1");
    SET075.setValeursSelection("0", "1");
    SET074.setValeursSelection("0", "1");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Sécurité de la gestion des ventes et des achats  (@LOCUSR@)")).trim());
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    // Gestion des tops mals initialisés : si on a une case à cocher, on lit la zone correspondante. Si blanc décoche
    // comme avec la valeur 1.
    // SEUL LE 0 COCHE (autorisé).
    for (int i = 0; i < getAllComponents(this).size(); i++) {
      if (getAllComponents(this).get(i) instanceof XRiCheckBox) {
        if (lexique.HostFieldGetData(getAllComponents(this).get(i).getName()).trim().equals("")) {
          ((XRiCheckBox) getAllComponents(this).get(i)).setSelected(false);
        }
      }
    }
    
    
    V06F1.setVisible(lexique.isTrue("N53"));
    V06F.setVisible(!V06F1.isVisible());
    
    riSousMenu6.setEnabled(lexique.isTrue("51"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (V06F1.isVisible()) {
      V06F1.setText("F");
    }
    else {
      lexique.HostFieldPutData("V06F1", 0, "F");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  public static List<Component> getAllComponents(final Container c) {
    Component[] comps = c.getComponents();
    List<Component> compList = new ArrayList<Component>();
    for (Component comp : comps) {
      compList.add(comp);
      if (comp instanceof Container) {
        compList.addAll(getAllComponents((Container) comp));
      }
    }
    return compList;
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    INDUSR = new RiZoneSortie();
    OBJ_36 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_40 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_115 = new JLabel();
    OBJ_108 = new JLabel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    scroll_droite2 = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    xTitledPanel1 = new JXTitledPanel();
    pnlHaut = new SNPanel();
    OBJ_49 = new JLabel();
    OBJ_58 = new JLabel();
    SEAHUS = new XRiTextField();
    OBJ_55 = new JLabel();
    SEMAGA = new XRiTextField();
    OBJ_53 = new JLabel();
    SET145 = new XRiTextField();
    OBJ_57 = new JLabel();
    OBJ_59 = new JLabel();
    pnlGauche = new SNPanel();
    OBJ_90 = new JLabel();
    OBJ_123 = new JLabel();
    OBJ_124 = new JLabel();
    OBJ_186 = new JLabel();
    OBJ_189 = new JLabel();
    OBJ_193 = new JLabel();
    OBJ_294 = new JLabel();
    OBJ_195 = new JLabel();
    OBJ_196 = new JLabel();
    OBJ_197 = new JLabel();
    OBJ_198 = new JLabel();
    OBJ_199 = new JLabel();
    OBJ_81 = new JLabel();
    SET074 = new XRiCheckBox();
    SET075 = new XRiCheckBox();
    SET076 = new XRiCheckBox();
    SET077 = new XRiCheckBox();
    SET078 = new XRiCheckBox();
    SET079 = new XRiCheckBox();
    OBJ_213 = new JLabel();
    OBJ_200 = new JLabel();
    OBJ_214 = new JLabel();
    OBJ_219 = new JLabel();
    OBJ_220 = new JLabel();
    SET142 = new XRiCheckBox();
    SET095 = new XRiCheckBox();
    SET094 = new XRiCheckBox();
    SET080 = new XRiCheckBox();
    SET071 = new XRiComboBox();
    SET072 = new XRiComboBox();
    SET073 = new XRiComboBox();
    SET146 = new XRiComboBox();
    OBJ_218 = new JLabel();
    SET179 = new XRiComboBox();
    pnlDroite = new SNPanel();
    OBJ_89 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_201 = new JLabel();
    OBJ_202 = new JLabel();
    OBJ_203 = new JLabel();
    OBJ_204 = new JLabel();
    OBJ_205 = new JLabel();
    OBJ_206 = new JLabel();
    OBJ_207 = new JLabel();
    OBJ_208 = new JLabel();
    OBJ_209 = new JLabel();
    OBJ_210 = new JLabel();
    SET083 = new XRiCheckBox();
    SET084 = new XRiCheckBox();
    SET085 = new XRiCheckBox();
    SET086 = new XRiCheckBox();
    SET087 = new XRiCheckBox();
    SET088 = new XRiCheckBox();
    SET089 = new XRiCheckBox();
    OBJ_215 = new JLabel();
    OBJ_216 = new JLabel();
    OBJ_217 = new JLabel();
    SET082 = new XRiCheckBox();
    SET091 = new XRiCheckBox();
    SET090 = new XRiCheckBox();
    SET093 = new XRiComboBox();
    OBJ_211 = new JLabel();
    OBJ_212 = new JLabel();
    SET081 = new XRiComboBox();
    SET156 = new XRiCheckBox();
    SET166 = new XRiCheckBox();
    SET092 = new XRiCheckBox();
    pnlBas = new SNPanel();
    V06F1 = new XRiTextField();
    V06F = new XRiTextField();
    OBJ_74 = new SNBoutonLeger();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 de la gestion des ventes et des achats  (@LOCUSR@)");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_34 ----
          OBJ_34.setText("Utilisateur");
          OBJ_34.setName("OBJ_34");

          //---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setText("@INDUSR@");
          INDUSR.setOpaque(false);
          INDUSR.setName("INDUSR");

          //---- OBJ_36 ----
          OBJ_36.setText("Etablissement");
          OBJ_36.setName("OBJ_36");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_40 ----
          OBJ_40.setText("@DGNOM@");
          OBJ_40.setOpaque(false);
          OBJ_40.setName("OBJ_40");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(75, 75, 75)
                    .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
                .addGap(65, 65, 65)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 339, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_115 ----
          OBJ_115.setText("@WPAGE@");
          OBJ_115.setName("OBJ_115");
          p_tete_droite.add(OBJ_115);

          //---- OBJ_108 ----
          OBJ_108.setText("Page");
          OBJ_108.setName("OBJ_108");
          p_tete_droite.add(OBJ_108);
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());

      //======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== scroll_droite2 ========
          {
            scroll_droite2.setBackground(new Color(238, 239, 241));
            scroll_droite2.setPreferredSize(new Dimension(16, 520));
            scroll_droite2.setBorder(null);
            scroll_droite2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            scroll_droite2.setName("scroll_droite2");

            //======== menus_haut ========
            {
              menus_haut.setMinimumSize(new Dimension(160, 520));
              menus_haut.setPreferredSize(new Dimension(160, 520));
              menus_haut.setBackground(new Color(238, 239, 241));
              menus_haut.setAutoscrolls(true);
              menus_haut.setName("menus_haut");
              menus_haut.setLayout(new VerticalLayout());

              //======== riMenu_V01F ========
              {
                riMenu_V01F.setMinimumSize(new Dimension(104, 50));
                riMenu_V01F.setPreferredSize(new Dimension(170, 50));
                riMenu_V01F.setMaximumSize(new Dimension(104, 50));
                riMenu_V01F.setName("riMenu_V01F");

                //---- riMenu_bt_V01F ----
                riMenu_bt_V01F.setText("@V01F@");
                riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
                riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
                riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
                riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
                riMenu_bt_V01F.setName("riMenu_bt_V01F");
                riMenu_V01F.add(riMenu_bt_V01F);
              }
              menus_haut.add(riMenu_V01F);

              //======== riSousMenu_consult ========
              {
                riSousMenu_consult.setName("riSousMenu_consult");

                //---- riSousMenu_bt_consult ----
                riSousMenu_bt_consult.setText("Consultation");
                riSousMenu_bt_consult.setToolTipText("Consultation");
                riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
                riSousMenu_consult.add(riSousMenu_bt_consult);
              }
              menus_haut.add(riSousMenu_consult);

              //======== riSousMenu_modif ========
              {
                riSousMenu_modif.setName("riSousMenu_modif");

                //---- riSousMenu_bt_modif ----
                riSousMenu_bt_modif.setText("Modification");
                riSousMenu_bt_modif.setToolTipText("Modification");
                riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
                riSousMenu_modif.add(riSousMenu_bt_modif);
              }
              menus_haut.add(riSousMenu_modif);

              //======== riSousMenu_crea ========
              {
                riSousMenu_crea.setName("riSousMenu_crea");

                //---- riSousMenu_bt_crea ----
                riSousMenu_bt_crea.setText("Cr\u00e9ation");
                riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
                riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
                riSousMenu_crea.add(riSousMenu_bt_crea);
              }
              menus_haut.add(riSousMenu_crea);

              //======== riSousMenu_suppr ========
              {
                riSousMenu_suppr.setName("riSousMenu_suppr");

                //---- riSousMenu_bt_suppr ----
                riSousMenu_bt_suppr.setText("Annulation");
                riSousMenu_bt_suppr.setToolTipText("Annulation");
                riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
                riSousMenu_suppr.add(riSousMenu_bt_suppr);
              }
              menus_haut.add(riSousMenu_suppr);

              //======== riSousMenuF_dupli ========
              {
                riSousMenuF_dupli.setName("riSousMenuF_dupli");

                //---- riSousMenu_bt_dupli ----
                riSousMenu_bt_dupli.setText("Duplication");
                riSousMenu_bt_dupli.setToolTipText("Duplication");
                riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
                riSousMenuF_dupli.add(riSousMenu_bt_dupli);
              }
              menus_haut.add(riSousMenuF_dupli);

              //======== riSousMenu_rappel ========
              {
                riSousMenu_rappel.setName("riSousMenu_rappel");

                //---- riSousMenu_bt_rappel ----
                riSousMenu_bt_rappel.setText("Rappel");
                riSousMenu_bt_rappel.setToolTipText("Rappel");
                riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
                riSousMenu_rappel.add(riSousMenu_bt_rappel);
              }
              menus_haut.add(riSousMenu_rappel);

              //======== riSousMenu_reac ========
              {
                riSousMenu_reac.setName("riSousMenu_reac");

                //---- riSousMenu_bt_reac ----
                riSousMenu_bt_reac.setText("R\u00e9activation");
                riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
                riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
                riSousMenu_reac.add(riSousMenu_bt_reac);
              }
              menus_haut.add(riSousMenu_reac);

              //======== riSousMenu_destr ========
              {
                riSousMenu_destr.setName("riSousMenu_destr");

                //---- riSousMenu_bt_destr ----
                riSousMenu_bt_destr.setText("Suppression");
                riSousMenu_bt_destr.setToolTipText("Suppression");
                riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
                riSousMenu_destr.add(riSousMenu_bt_destr);
              }
              menus_haut.add(riSousMenu_destr);

              //======== riMenu2 ========
              {
                riMenu2.setName("riMenu2");

                //---- riMenu_bt2 ----
                riMenu_bt2.setText("Options");
                riMenu_bt2.setName("riMenu_bt2");
                riMenu2.add(riMenu_bt2);
              }
              menus_haut.add(riMenu2);

              //======== riSousMenu6 ========
              {
                riSousMenu6.setName("riSousMenu6");

                //---- riSousMenu_bt6 ----
                riSousMenu_bt6.setText("Tous droits sur tout");
                riSousMenu_bt6.setToolTipText("Tous droits sur tout");
                riSousMenu_bt6.setName("riSousMenu_bt6");
                riSousMenu_bt6.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riSousMenu_bt6ActionPerformed(e);
                  }
                });
                riSousMenu6.add(riSousMenu_bt6);
              }
              menus_haut.add(riSousMenu6);

              //======== riSousMenu7 ========
              {
                riSousMenu7.setName("riSousMenu7");

                //---- riSousMenu_bt7 ----
                riSousMenu_bt7.setText("Valider les modifications");
                riSousMenu_bt7.setToolTipText("Valider les modifications");
                riSousMenu_bt7.setName("riSousMenu_bt7");
                riSousMenu_bt7.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riSousMenu_bt7ActionPerformed(e);
                  }
                });
                riSousMenu7.add(riSousMenu_bt7);
              }
              menus_haut.add(riSousMenu7);
            }
            scroll_droite2.setViewportView(menus_haut);
          }
          scroll_droite.setViewportView(scroll_droite2);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);

      //======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(1000, 600));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Objet de la s\u00e9curit\u00e9 (GAM)");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //======== pnlHaut ========
          {
            pnlHaut.setBorder(new TitledBorder(""));
            pnlHaut.setOpaque(false);
            pnlHaut.setName("pnlHaut");
            pnlHaut.setLayout(null);

            //---- OBJ_49 ----
            OBJ_49.setText("R\u00e9percution sur bon");
            OBJ_49.setName("OBJ_49");
            pnlHaut.add(OBJ_49);
            OBJ_49.setBounds(105, 5, 148, 19);

            //---- OBJ_58 ----
            OBJ_58.setText("Protection");
            OBJ_58.setName("OBJ_58");
            pnlHaut.add(OBJ_58);
            OBJ_58.setBounds(140, 20, 75, 16);

            //---- SEAHUS ----
            SEAHUS.setName("SEAHUS");
            pnlHaut.add(SEAHUS);
            SEAHUS.setBounds(575, 10, 40, SEAHUS.getPreferredSize().height);

            //---- OBJ_55 ----
            OBJ_55.setText("Ach");
            OBJ_55.setName("OBJ_55");
            pnlHaut.add(OBJ_55);
            OBJ_55.setBounds(540, 14, 27, 20);

            //---- SEMAGA ----
            SEMAGA.setName("SEMAGA");
            pnlHaut.add(SEMAGA);
            SEMAGA.setBounds(500, 10, 30, SEMAGA.getPreferredSize().height);

            //---- OBJ_53 ----
            OBJ_53.setText("Ma");
            OBJ_53.setName("OBJ_53");
            pnlHaut.add(OBJ_53);
            OBJ_53.setBounds(465, 14, 22, 20);

            //---- SET145 ----
            SET145.setComponentPopupMenu(BTD);
            SET145.setName("SET145");
            pnlHaut.add(SET145);
            SET145.setBounds(335, 10, 20, SET145.getPreferredSize().height);

            //---- OBJ_57 ----
            OBJ_57.setText("145");
            OBJ_57.setName("OBJ_57");
            pnlHaut.add(OBJ_57);
            OBJ_57.setBounds(105, 20, 21, 16);

            //---- OBJ_59 ----
            OBJ_59.setText("b");
            OBJ_59.setName("OBJ_59");
            pnlHaut.add(OBJ_59);
            OBJ_59.setBounds(375, 16, 12, 16);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < pnlHaut.getComponentCount(); i++) {
                Rectangle bounds = pnlHaut.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlHaut.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlHaut.setMinimumSize(preferredSize);
              pnlHaut.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel1ContentContainer.add(pnlHaut);
          pnlHaut.setBounds(10, 5, 950, 45);

          //======== pnlGauche ========
          {
            pnlGauche.setBorder(new TitledBorder(""));
            pnlGauche.setOpaque(false);
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(null);

            //---- OBJ_90 ----
            OBJ_90.setText("72");
            OBJ_90.setFont(OBJ_90.getFont().deriveFont(OBJ_90.getFont().getStyle() | Font.BOLD));
            OBJ_90.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_90.setName("OBJ_90");
            pnlGauche.add(OBJ_90);
            OBJ_90.setBounds(10, 39, 21, 16);

            //---- OBJ_123 ----
            OBJ_123.setText("73");
            OBJ_123.setFont(OBJ_123.getFont().deriveFont(OBJ_123.getFont().getStyle() | Font.BOLD));
            OBJ_123.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_123.setName("OBJ_123");
            pnlGauche.add(OBJ_123);
            OBJ_123.setBounds(10, 68, 21, 16);

            //---- OBJ_124 ----
            OBJ_124.setText("74");
            OBJ_124.setFont(OBJ_124.getFont().deriveFont(OBJ_124.getFont().getStyle() | Font.BOLD));
            OBJ_124.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_124.setName("OBJ_124");
            pnlGauche.add(OBJ_124);
            OBJ_124.setBounds(10, 97, 21, 16);

            //---- OBJ_186 ----
            OBJ_186.setText("75");
            OBJ_186.setFont(OBJ_186.getFont().deriveFont(OBJ_186.getFont().getStyle() | Font.BOLD));
            OBJ_186.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_186.setName("OBJ_186");
            pnlGauche.add(OBJ_186);
            OBJ_186.setBounds(10, 126, 21, 16);

            //---- OBJ_189 ----
            OBJ_189.setText("76");
            OBJ_189.setFont(OBJ_189.getFont().deriveFont(OBJ_189.getFont().getStyle() | Font.BOLD));
            OBJ_189.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_189.setName("OBJ_189");
            pnlGauche.add(OBJ_189);
            OBJ_189.setBounds(10, 155, 21, 16);

            //---- OBJ_193 ----
            OBJ_193.setText("77");
            OBJ_193.setFont(OBJ_193.getFont().deriveFont(OBJ_193.getFont().getStyle() | Font.BOLD));
            OBJ_193.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_193.setName("OBJ_193");
            pnlGauche.add(OBJ_193);
            OBJ_193.setBounds(10, 184, 21, 16);

            //---- OBJ_294 ----
            OBJ_294.setText("78");
            OBJ_294.setFont(OBJ_294.getFont().deriveFont(OBJ_294.getFont().getStyle() | Font.BOLD));
            OBJ_294.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_294.setName("OBJ_294");
            pnlGauche.add(OBJ_294);
            OBJ_294.setBounds(10, 213, 21, 16);

            //---- OBJ_195 ----
            OBJ_195.setText("79");
            OBJ_195.setFont(OBJ_195.getFont().deriveFont(OBJ_195.getFont().getStyle() | Font.BOLD));
            OBJ_195.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_195.setName("OBJ_195");
            pnlGauche.add(OBJ_195);
            OBJ_195.setBounds(10, 242, 21, 16);

            //---- OBJ_196 ----
            OBJ_196.setText("80");
            OBJ_196.setFont(OBJ_196.getFont().deriveFont(OBJ_196.getFont().getStyle() | Font.BOLD));
            OBJ_196.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_196.setName("OBJ_196");
            pnlGauche.add(OBJ_196);
            OBJ_196.setBounds(10, 271, 21, 16);

            //---- OBJ_197 ----
            OBJ_197.setText("94");
            OBJ_197.setFont(OBJ_197.getFont().deriveFont(OBJ_197.getFont().getStyle() | Font.BOLD));
            OBJ_197.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_197.setName("OBJ_197");
            pnlGauche.add(OBJ_197);
            OBJ_197.setBounds(10, 300, 21, 16);

            //---- OBJ_198 ----
            OBJ_198.setText("95");
            OBJ_198.setFont(OBJ_198.getFont().deriveFont(OBJ_198.getFont().getStyle() | Font.BOLD));
            OBJ_198.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_198.setName("OBJ_198");
            pnlGauche.add(OBJ_198);
            OBJ_198.setBounds(10, 329, 21, 16);

            //---- OBJ_199 ----
            OBJ_199.setText("142");
            OBJ_199.setFont(OBJ_199.getFont().deriveFont(OBJ_199.getFont().getStyle() | Font.BOLD));
            OBJ_199.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_199.setName("OBJ_199");
            pnlGauche.add(OBJ_199);
            OBJ_199.setBounds(10, 358, 21, 16);

            //---- OBJ_81 ----
            OBJ_81.setText("71");
            OBJ_81.setFont(OBJ_81.getFont().deriveFont(OBJ_81.getFont().getStyle() | Font.BOLD));
            OBJ_81.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_81.setName("OBJ_81");
            pnlGauche.add(OBJ_81);
            OBJ_81.setBounds(10, 10, 21, 16);

            //---- SET074 ----
            SET074.setToolTipText("Valeur entre 0 et 9");
            SET074.setComponentPopupMenu(BTD);
            SET074.setText("Listes des articles");
            SET074.setName("SET074");
            pnlGauche.add(SET074);
            SET074.setBounds(40, 96, 405, SET074.getPreferredSize().height);

            //---- SET075 ----
            SET075.setToolTipText("Valeur entre 0 et 9");
            SET075.setComponentPopupMenu(BTD);
            SET075.setText("Listes des fournisseurs");
            SET075.setName("SET075");
            pnlGauche.add(SET075);
            SET075.setBounds(40, 125, 405, SET075.getPreferredSize().height);

            //---- SET076 ----
            SET076.setToolTipText("Valeur entre 0 et 9");
            SET076.setComponentPopupMenu(BTD);
            SET076.setText("Listes des conditions d'achat");
            SET076.setName("SET076");
            pnlGauche.add(SET076);
            SET076.setBounds(40, 154, 405, SET076.getPreferredSize().height);

            //---- SET077 ----
            SET077.setToolTipText("Valeur entre 0 et 9");
            SET077.setComponentPopupMenu(BTD);
            SET077.setText("Edition du journal fiscal");
            SET077.setName("SET077");
            pnlGauche.add(SET077);
            SET077.setBounds(40, 183, 405, SET077.getPreferredSize().height);

            //---- SET078 ----
            SET078.setToolTipText("Valeur entre 0 et 9");
            SET078.setComponentPopupMenu(BTD);
            SET078.setText("Edition des statistiques");
            SET078.setName("SET078");
            pnlGauche.add(SET078);
            SET078.setBounds(40, 212, 405, SET078.getPreferredSize().height);

            //---- SET079 ----
            SET079.setToolTipText("Valeur entre 0 et 9");
            SET079.setComponentPopupMenu(BTD);
            SET079.setText("Tableau de bord GAM");
            SET079.setName("SET079");
            pnlGauche.add(SET079);
            SET079.setBounds(40, 241, 405, SET079.getPreferredSize().height);

            //---- OBJ_213 ----
            OBJ_213.setText("146");
            OBJ_213.setFont(OBJ_213.getFont().deriveFont(OBJ_213.getFont().getStyle() | Font.BOLD));
            OBJ_213.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_213.setName("OBJ_213");
            pnlGauche.add(OBJ_213);
            OBJ_213.setBounds(5, 381, 26, 26);

            //---- OBJ_200 ----
            OBJ_200.setText("Personnalisation du syst\u00e8me");
            OBJ_200.setName("OBJ_200");
            pnlGauche.add(OBJ_200);
            OBJ_200.setBounds(255, 4, 205, 26);

            //---- OBJ_214 ----
            OBJ_214.setText("Gestion des fournisseurs");
            OBJ_214.setName("OBJ_214");
            pnlGauche.add(OBJ_214);
            OBJ_214.setBounds(255, 33, 205, 26);

            //---- OBJ_219 ----
            OBJ_219.setText("Gestion des conditions d'achat");
            OBJ_219.setName("OBJ_219");
            pnlGauche.add(OBJ_219);
            OBJ_219.setBounds(255, 62, 205, 26);

            //---- OBJ_220 ----
            OBJ_220.setText("Extraction obligatoire sur r\u00e9ception");
            OBJ_220.setName("OBJ_220");
            pnlGauche.add(OBJ_220);
            OBJ_220.setBounds(255, 381, 205, 26);

            //---- SET142 ----
            SET142.setToolTipText("Valeur entre 0 et 9");
            SET142.setComponentPopupMenu(BTD);
            SET142.setText("Mise \u00e0 jour du prix de revient");
            SET142.setName("SET142");
            pnlGauche.add(SET142);
            SET142.setBounds(40, 357, 405, SET142.getPreferredSize().height);

            //---- SET095 ----
            SET095.setToolTipText("Valeur entre 0 et 9");
            SET095.setComponentPopupMenu(BTD);
            SET095.setText("Mise \u00e0 jour des conditions d'achat");
            SET095.setName("SET095");
            pnlGauche.add(SET095);
            SET095.setBounds(40, 328, 405, SET095.getPreferredSize().height);

            //---- SET094 ----
            SET094.setToolTipText("Valeur entre 0 et 9");
            SET094.setComponentPopupMenu(BTD);
            SET094.setText("Analytique achats/ventes");
            SET094.setName("SET094");
            pnlGauche.add(SET094);
            SET094.setBounds(40, 299, 405, SET094.getPreferredSize().height);

            //---- SET080 ----
            SET080.setToolTipText("Valeur entre 0 et 9");
            SET080.setComponentPopupMenu(BTD);
            SET080.setText("Engagements des achats");
            SET080.setName("SET080");
            pnlGauche.add(SET080);
            SET080.setBounds(40, 270, 405, SET080.getPreferredSize().height);

            //---- SET071 ----
            SET071.setComponentPopupMenu(BTD);
            SET071.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit"
            }));
            SET071.setName("SET071");
            pnlGauche.add(SET071);
            SET071.setBounds(40, 4, 180, SET071.getPreferredSize().height);

            //---- SET072 ----
            SET072.setComponentPopupMenu(BTD);
            SET072.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit"
            }));
            SET072.setName("SET072");
            pnlGauche.add(SET072);
            SET072.setBounds(40, 33, 180, SET072.getPreferredSize().height);

            //---- SET073 ----
            SET073.setComponentPopupMenu(BTD);
            SET073.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit",
              "Superviseur"
            }));
            SET073.setName("SET073");
            pnlGauche.add(SET073);
            SET073.setBounds(40, 62, 180, SET073.getPreferredSize().height);

            //---- SET146 ----
            SET146.setComponentPopupMenu(BTD);
            SET146.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Extraction facultative",
              "Extraction impos\u00e9e"
            }));
            SET146.setName("SET146");
            pnlGauche.add(SET146);
            SET146.setBounds(40, 381, 180, SET146.getPreferredSize().height);

            //---- OBJ_218 ----
            OBJ_218.setText("179");
            OBJ_218.setFont(OBJ_218.getFont().deriveFont(OBJ_218.getFont().getStyle() | Font.BOLD));
            OBJ_218.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_218.setName("OBJ_218");
            pnlGauche.add(OBJ_218);
            OBJ_218.setBounds(5, 410, 26, 26);

            //---- SET179 ----
            SET179.setComponentPopupMenu(BTD);
            SET179.setModel(new DefaultComboBoxModel(new String[] {
              "Pas de n\u00e9gociation achat depuis le comptoir",
              "Acc\u00e9s n\u00e9gociation achat comptoir sans les frais d'exploitation",
              "Acc\u00e9s n\u00e9gociation achat comptoir avec les frais d'exploitation"
            }));
            SET179.setName("SET179");
            pnlGauche.add(SET179);
            SET179.setBounds(40, 410, 410, SET179.getPreferredSize().height);
          }
          xTitledPanel1ContentContainer.add(pnlGauche);
          pnlGauche.setBounds(10, 55, 470, 450);

          //======== pnlDroite ========
          {
            pnlDroite.setBorder(new TitledBorder(""));
            pnlDroite.setOpaque(false);
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(null);

            //---- OBJ_89 ----
            OBJ_89.setText("81");
            OBJ_89.setFont(OBJ_89.getFont().deriveFont(OBJ_89.getFont().getStyle() | Font.BOLD));
            OBJ_89.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_89.setName("OBJ_89");
            pnlDroite.add(OBJ_89);
            OBJ_89.setBounds(10, 10, 21, 16);

            //---- OBJ_99 ----
            OBJ_99.setText("82");
            OBJ_99.setFont(OBJ_99.getFont().deriveFont(OBJ_99.getFont().getStyle() | Font.BOLD));
            OBJ_99.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_99.setName("OBJ_99");
            pnlDroite.add(OBJ_99);
            OBJ_99.setBounds(10, 39, 21, 16);

            //---- OBJ_201 ----
            OBJ_201.setText("83");
            OBJ_201.setFont(OBJ_201.getFont().deriveFont(OBJ_201.getFont().getStyle() | Font.BOLD));
            OBJ_201.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_201.setName("OBJ_201");
            pnlDroite.add(OBJ_201);
            OBJ_201.setBounds(10, 68, 21, 16);

            //---- OBJ_202 ----
            OBJ_202.setText("84");
            OBJ_202.setFont(OBJ_202.getFont().deriveFont(OBJ_202.getFont().getStyle() | Font.BOLD));
            OBJ_202.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_202.setName("OBJ_202");
            pnlDroite.add(OBJ_202);
            OBJ_202.setBounds(10, 97, 21, 16);

            //---- OBJ_203 ----
            OBJ_203.setText("85");
            OBJ_203.setFont(OBJ_203.getFont().deriveFont(OBJ_203.getFont().getStyle() | Font.BOLD));
            OBJ_203.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_203.setName("OBJ_203");
            pnlDroite.add(OBJ_203);
            OBJ_203.setBounds(10, 126, 21, 16);

            //---- OBJ_204 ----
            OBJ_204.setText("86");
            OBJ_204.setFont(OBJ_204.getFont().deriveFont(OBJ_204.getFont().getStyle() | Font.BOLD));
            OBJ_204.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_204.setName("OBJ_204");
            pnlDroite.add(OBJ_204);
            OBJ_204.setBounds(10, 155, 21, 16);

            //---- OBJ_205 ----
            OBJ_205.setText("87");
            OBJ_205.setFont(OBJ_205.getFont().deriveFont(OBJ_205.getFont().getStyle() | Font.BOLD));
            OBJ_205.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_205.setName("OBJ_205");
            pnlDroite.add(OBJ_205);
            OBJ_205.setBounds(10, 184, 21, 16);

            //---- OBJ_206 ----
            OBJ_206.setText("88");
            OBJ_206.setFont(OBJ_206.getFont().deriveFont(OBJ_206.getFont().getStyle() | Font.BOLD));
            OBJ_206.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_206.setName("OBJ_206");
            pnlDroite.add(OBJ_206);
            OBJ_206.setBounds(10, 213, 21, 16);

            //---- OBJ_207 ----
            OBJ_207.setText("89");
            OBJ_207.setFont(OBJ_207.getFont().deriveFont(OBJ_207.getFont().getStyle() | Font.BOLD));
            OBJ_207.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_207.setName("OBJ_207");
            pnlDroite.add(OBJ_207);
            OBJ_207.setBounds(10, 242, 21, 16);

            //---- OBJ_208 ----
            OBJ_208.setText("90");
            OBJ_208.setFont(OBJ_208.getFont().deriveFont(OBJ_208.getFont().getStyle() | Font.BOLD));
            OBJ_208.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_208.setName("OBJ_208");
            pnlDroite.add(OBJ_208);
            OBJ_208.setBounds(10, 271, 21, 16);

            //---- OBJ_209 ----
            OBJ_209.setText("91");
            OBJ_209.setFont(OBJ_209.getFont().deriveFont(OBJ_209.getFont().getStyle() | Font.BOLD));
            OBJ_209.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_209.setName("OBJ_209");
            pnlDroite.add(OBJ_209);
            OBJ_209.setBounds(10, 300, 21, 16);

            //---- OBJ_210 ----
            OBJ_210.setText("92");
            OBJ_210.setFont(OBJ_210.getFont().deriveFont(OBJ_210.getFont().getStyle() | Font.BOLD));
            OBJ_210.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_210.setName("OBJ_210");
            pnlDroite.add(OBJ_210);
            OBJ_210.setBounds(10, 327, 21, 21);

            //---- SET083 ----
            SET083.setToolTipText("Valeur entre 0 et 9");
            SET083.setComponentPopupMenu(BTD);
            SET083.setText("Cr\u00e9ation d'avoir");
            SET083.setName("SET083");
            pnlDroite.add(SET083);
            SET083.setBounds(40, 67, 405, SET083.getPreferredSize().height);

            //---- SET084 ----
            SET084.setToolTipText("Valeur entre 0 et 9");
            SET084.setComponentPopupMenu(BTD);
            SET084.setText("Option REC et FAC directes");
            SET084.setName("SET084");
            pnlDroite.add(SET084);
            SET084.setBounds(40, 96, 405, SET084.getPreferredSize().height);

            //---- SET085 ----
            SET085.setToolTipText("Valeur entre 0 et 9");
            SET085.setComponentPopupMenu(BTD);
            SET085.setText("Rapprochement");
            SET085.setName("SET085");
            pnlDroite.add(SET085);
            SET085.setBounds(40, 125, 405, SET085.getPreferredSize().height);

            //---- SET086 ----
            SET086.setToolTipText("Valeur entre 0 et 9");
            SET086.setComponentPopupMenu(BTD);
            SET086.setText("Editions de bons");
            SET086.setName("SET086");
            pnlDroite.add(SET086);
            SET086.setBounds(40, 154, 405, SET086.getPreferredSize().height);

            //---- SET087 ----
            SET087.setToolTipText("Valeur entre 0 et 9");
            SET087.setComponentPopupMenu(BTD);
            SET087.setText("Etats de bons");
            SET087.setName("SET087");
            pnlDroite.add(SET087);
            SET087.setBounds(40, 183, 405, SET087.getPreferredSize().height);

            //---- SET088 ----
            SET088.setToolTipText("Valeur entre 0 et 9");
            SET088.setComponentPopupMenu(BTD);
            SET088.setText("Editions de billets \u00e0 ordre");
            SET088.setName("SET088");
            pnlDroite.add(SET088);
            SET088.setBounds(40, 212, 405, SET088.getPreferredSize().height);

            //---- SET089 ----
            SET089.setToolTipText("Valeur entre 0 et 9");
            SET089.setComponentPopupMenu(BTD);
            SET089.setText("Comptabilisation des achats");
            SET089.setName("SET089");
            pnlDroite.add(SET089);
            SET089.setBounds(40, 241, 405, SET089.getPreferredSize().height);

            //---- OBJ_215 ----
            OBJ_215.setText("93");
            OBJ_215.setFont(OBJ_215.getFont().deriveFont(OBJ_215.getFont().getStyle() | Font.BOLD));
            OBJ_215.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_215.setName("OBJ_215");
            pnlDroite.add(OBJ_215);
            OBJ_215.setBounds(10, 355, 21, 21);

            //---- OBJ_216 ----
            OBJ_216.setText("156");
            OBJ_216.setFont(OBJ_216.getFont().deriveFont(OBJ_216.getFont().getStyle() | Font.BOLD));
            OBJ_216.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_216.setName("OBJ_216");
            pnlDroite.add(OBJ_216);
            OBJ_216.setBounds(10, 385, 21, 21);

            //---- OBJ_217 ----
            OBJ_217.setText("166");
            OBJ_217.setFont(OBJ_217.getFont().deriveFont(OBJ_217.getFont().getStyle() | Font.BOLD));
            OBJ_217.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_217.setName("OBJ_217");
            pnlDroite.add(OBJ_217);
            OBJ_217.setBounds(10, 414, 21, 21);

            //---- SET082 ----
            SET082.setToolTipText("Valeur entre 0 et 9");
            SET082.setComponentPopupMenu(BTD);
            SET082.setText("Modification prix ou condition");
            SET082.setName("SET082");
            pnlDroite.add(SET082);
            SET082.setBounds(40, 38, 405, SET082.getPreferredSize().height);

            //---- SET091 ----
            SET091.setToolTipText("Valeur entre 0 et 9");
            SET091.setComponentPopupMenu(BTD);
            SET091.setText("G\u00e9n\u00e9ration automatique de bons");
            SET091.setName("SET091");
            pnlDroite.add(SET091);
            SET091.setBounds(40, 299, 405, SET091.getPreferredSize().height);

            //---- SET090 ----
            SET090.setToolTipText("Valeur entre 0 et 9");
            SET090.setComponentPopupMenu(BTD);
            SET090.setText("Relev\u00e9 des achats mensuels");
            SET090.setName("SET090");
            pnlDroite.add(SET090);
            SET090.setBounds(40, 270, 405, SET090.getPreferredSize().height);

            //---- SET093 ----
            SET093.setComponentPopupMenu(BTD);
            SET093.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Ecart maximum  < 10 000",
              "< 1000",
              "< 100",
              "< 10",
              "< 1"
            }));
            SET093.setName("SET093");
            pnlDroite.add(SET093);
            SET093.setBounds(40, 352, 210, SET093.getPreferredSize().height);

            //---- OBJ_211 ----
            OBJ_211.setText("Ecarts sur rapprochement");
            OBJ_211.setName("OBJ_211");
            pnlDroite.add(OBJ_211);
            OBJ_211.setBounds(255, 352, 205, 26);

            //---- OBJ_212 ----
            OBJ_212.setText("Gestion des bons et factures");
            OBJ_212.setName("OBJ_212");
            pnlDroite.add(OBJ_212);
            OBJ_212.setBounds(255, 4, 205, 26);

            //---- SET081 ----
            SET081.setComponentPopupMenu(BTD);
            SET081.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "Droits VAL, REC et FAC",
              "Droits VAL et REC",
              "Droits VAL uniquement",
              "Droits ATT seulement",
              "Superviseur"
            }));
            SET081.setName("SET081");
            pnlDroite.add(SET081);
            SET081.setBounds(40, 4, 210, SET081.getPreferredSize().height);

            //---- SET156 ----
            SET156.setToolTipText("Valeur entre 0 et 9");
            SET156.setComponentPopupMenu(BTD);
            SET156.setText("Modification de la date r\u00e9ception");
            SET156.setName("SET156");
            pnlDroite.add(SET156);
            SET156.setBounds(40, 386, 405, SET156.getPreferredSize().height);

            //---- SET166 ----
            SET166.setToolTipText("Valeur entre 0 et 9");
            SET166.setComponentPopupMenu(BTD);
            SET166.setText("Purge des r\u00e9appros");
            SET166.setName("SET166");
            pnlDroite.add(SET166);
            SET166.setBounds(40, 415, 405, SET166.getPreferredSize().height);

            //---- SET092 ----
            SET092.setToolTipText("Valeur entre 0 et 9");
            SET092.setComponentPopupMenu(BTD);
            SET092.setText("Gestion des r\u00e9appros");
            SET092.setName("SET092");
            pnlDroite.add(SET092);
            SET092.setBounds(40, 328, 405, 18);
          }
          xTitledPanel1ContentContainer.add(pnlDroite);
          pnlDroite.setBounds(490, 55, 470, 450);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(15, 10, 974, 550);

        //======== pnlBas ========
        {
          pnlBas.setOpaque(false);
          pnlBas.setName("pnlBas");
          pnlBas.setLayout(null);

          //---- V06F1 ----
          V06F1.setComponentPopupMenu(BTD);
          V06F1.setName("V06F1");
          pnlBas.add(V06F1);
          V06F1.setBounds(220, 0, 25, V06F1.getPreferredSize().height);

          //---- V06F ----
          V06F.setComponentPopupMenu(BTD);
          V06F.setName("V06F");
          pnlBas.add(V06F);
          V06F.setBounds(220, 0, 25, V06F.getPreferredSize().height);

          //---- OBJ_74 ----
          OBJ_74.setText("Aller \u00e0 la page");
          OBJ_74.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_74.setName("OBJ_74");
          OBJ_74.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_69ActionPerformed(e);
            }
          });
          pnlBas.add(OBJ_74);
          OBJ_74.setBounds(new Rectangle(new Point(75, 0), OBJ_74.getPreferredSize()));

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < pnlBas.getComponentCount(); i++) {
              Rectangle bounds = pnlBas.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlBas.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlBas.setMinimumSize(preferredSize);
            pnlBas.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(pnlBas);
        pnlBas.setBounds(732, 560, 267, 34);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_34;
  private RiZoneSortie INDUSR;
  private JLabel OBJ_36;
  private RiZoneSortie INDETB;
  private RiZoneSortie OBJ_40;
  private JPanel p_tete_droite;
  private JLabel OBJ_115;
  private JLabel OBJ_108;
  private SNPanelFond pnlSud;
  private JPanel pnlMenus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JScrollPane scroll_droite2;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelContenu pnlContenu;
  private JXTitledPanel xTitledPanel1;
  private SNPanel pnlHaut;
  private JLabel OBJ_49;
  private JLabel OBJ_58;
  private XRiTextField SEAHUS;
  private JLabel OBJ_55;
  private XRiTextField SEMAGA;
  private JLabel OBJ_53;
  private XRiTextField SET145;
  private JLabel OBJ_57;
  private JLabel OBJ_59;
  private SNPanel pnlGauche;
  private JLabel OBJ_90;
  private JLabel OBJ_123;
  private JLabel OBJ_124;
  private JLabel OBJ_186;
  private JLabel OBJ_189;
  private JLabel OBJ_193;
  private JLabel OBJ_294;
  private JLabel OBJ_195;
  private JLabel OBJ_196;
  private JLabel OBJ_197;
  private JLabel OBJ_198;
  private JLabel OBJ_199;
  private JLabel OBJ_81;
  private XRiCheckBox SET074;
  private XRiCheckBox SET075;
  private XRiCheckBox SET076;
  private XRiCheckBox SET077;
  private XRiCheckBox SET078;
  private XRiCheckBox SET079;
  private JLabel OBJ_213;
  private JLabel OBJ_200;
  private JLabel OBJ_214;
  private JLabel OBJ_219;
  private JLabel OBJ_220;
  private XRiCheckBox SET142;
  private XRiCheckBox SET095;
  private XRiCheckBox SET094;
  private XRiCheckBox SET080;
  private XRiComboBox SET071;
  private XRiComboBox SET072;
  private XRiComboBox SET073;
  private XRiComboBox SET146;
  private JLabel OBJ_218;
  private XRiComboBox SET179;
  private SNPanel pnlDroite;
  private JLabel OBJ_89;
  private JLabel OBJ_99;
  private JLabel OBJ_201;
  private JLabel OBJ_202;
  private JLabel OBJ_203;
  private JLabel OBJ_204;
  private JLabel OBJ_205;
  private JLabel OBJ_206;
  private JLabel OBJ_207;
  private JLabel OBJ_208;
  private JLabel OBJ_209;
  private JLabel OBJ_210;
  private XRiCheckBox SET083;
  private XRiCheckBox SET084;
  private XRiCheckBox SET085;
  private XRiCheckBox SET086;
  private XRiCheckBox SET087;
  private XRiCheckBox SET088;
  private XRiCheckBox SET089;
  private JLabel OBJ_215;
  private JLabel OBJ_216;
  private JLabel OBJ_217;
  private XRiCheckBox SET082;
  private XRiCheckBox SET091;
  private XRiCheckBox SET090;
  private XRiComboBox SET093;
  private JLabel OBJ_211;
  private JLabel OBJ_212;
  private XRiComboBox SET081;
  private XRiCheckBox SET156;
  private XRiCheckBox SET166;
  private XRiCheckBox SET092;
  private SNPanel pnlBas;
  private XRiTextField V06F1;
  private XRiTextField V06F;
  private SNBoutonLeger OBJ_74;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
