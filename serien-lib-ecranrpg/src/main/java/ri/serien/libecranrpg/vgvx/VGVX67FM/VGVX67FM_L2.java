
package ri.serien.libecranrpg.vgvx.VGVX67FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX67FM_L2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX67FM_L2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_recup.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("Saisie des affaires pour l'article @P67ART@ (@A1LIB@)")).trim()));
    NUM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM1@")).trim());
    NUM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM2@")).trim());
    NUM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM3@")).trim());
    NUM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM4@")).trim());
    NUM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM5@")).trim());
    NUM6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM6@")).trim());
    NUM7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM7@")).trim());
    NUM8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM8@")).trim());
    NUM9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM9@")).trim());
    NUM10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM10@")).trim());
    QTSX1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX1@")).trim());
    QTSX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX2@")).trim());
    QTSX3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX3@")).trim());
    QTSX4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX4@")).trim());
    QTSX5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX5@")).trim());
    QTSX6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX6@")).trim());
    QTSX7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX7@")).trim());
    QTSX8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX8@")).trim());
    QTSX9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX9@")).trim());
    QTSX10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTSX10@")).trim());
    P67QTX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P67QTX@")).trim());
    P67QPX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P67QPX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    label5.setVisible(lexique.isTrue("42"));
    panel2.setVisible(lexique.isTrue("11"));
    panel1.setVisible(lexique.isTrue("N22"));
    err1.setVisible(lexique.isTrue("31"));
    err2.setVisible(lexique.isTrue("32"));
    err3.setVisible(lexique.isTrue("33"));
    err4.setVisible(lexique.isTrue("34"));
    err5.setVisible(lexique.isTrue("35"));
    err6.setVisible(lexique.isTrue("36"));
    err7.setVisible(lexique.isTrue("37"));
    err8.setVisible(lexique.isTrue("38"));
    err9.setVisible(lexique.isTrue("39"));
    err10.setVisible(lexique.isTrue("40"));
    P67QTX.setVisible(lexique.isTrue("42"));
    P67QPX.setVisible(lexique.isTrue("42"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Saisie des affaires pour un article"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    P_PnlOpts = new JPanel();
    NUM1 = new RiZoneSortie();
    NUM2 = new RiZoneSortie();
    NUM3 = new RiZoneSortie();
    NUM4 = new RiZoneSortie();
    NUM5 = new RiZoneSortie();
    NUM6 = new RiZoneSortie();
    NUM7 = new RiZoneSortie();
    NUM8 = new RiZoneSortie();
    NUM9 = new RiZoneSortie();
    NUM10 = new RiZoneSortie();
    EAF101 = new XRiTextField();
    QTSX1 = new RiZoneSortie();
    QTEX1 = new XRiTextField();
    QTPX1 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    EAF102 = new XRiTextField();
    QTSX2 = new RiZoneSortie();
    QTEX2 = new XRiTextField();
    QTPX2 = new XRiTextField();
    EAF103 = new XRiTextField();
    QTSX3 = new RiZoneSortie();
    QTEX3 = new XRiTextField();
    QTPX3 = new XRiTextField();
    EAF104 = new XRiTextField();
    QTSX4 = new RiZoneSortie();
    QTEX4 = new XRiTextField();
    QTPX4 = new XRiTextField();
    EAF105 = new XRiTextField();
    QTSX5 = new RiZoneSortie();
    QTEX5 = new XRiTextField();
    QTPX5 = new XRiTextField();
    EAF106 = new XRiTextField();
    QTSX6 = new RiZoneSortie();
    QTEX6 = new XRiTextField();
    QTPX6 = new XRiTextField();
    EAF107 = new XRiTextField();
    QTSX7 = new RiZoneSortie();
    QTEX7 = new XRiTextField();
    QTPX7 = new XRiTextField();
    EAF108 = new XRiTextField();
    QTSX8 = new RiZoneSortie();
    QTEX8 = new XRiTextField();
    QTPX8 = new XRiTextField();
    EAF109 = new XRiTextField();
    QTSX9 = new RiZoneSortie();
    QTEX9 = new XRiTextField();
    QTPX9 = new XRiTextField();
    EAF110 = new XRiTextField();
    QTSX10 = new RiZoneSortie();
    QTEX10 = new XRiTextField();
    QTPX10 = new XRiTextField();
    panel1 = new JPanel();
    PHT0X = new XRiTextField();
    PHT1X = new XRiTextField();
    OBJ_38 = new JLabel();
    OBJ_39 = new JLabel();
    panel2 = new JPanel();
    WAFF2 = new XRiTextField();
    err1 = new JLabel();
    err2 = new JLabel();
    err3 = new JLabel();
    err4 = new JLabel();
    err5 = new JLabel();
    err6 = new JLabel();
    err7 = new JLabel();
    err8 = new JLabel();
    err9 = new JLabel();
    err10 = new JLabel();
    P67QTX = new RiZoneSortie();
    P67QPX = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(915, 365));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Saisie des affaires pour l'article @P67ART@ (@A1LIB@)"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- NUM1 ----
          NUM1.setText("@NUM1@");
          NUM1.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM1.setName("NUM1");
          p_recup.add(NUM1);
          NUM1.setBounds(25, 50, 36, NUM1.getPreferredSize().height);

          //---- NUM2 ----
          NUM2.setText("@NUM2@");
          NUM2.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM2.setName("NUM2");
          p_recup.add(NUM2);
          NUM2.setBounds(25, 75, 36, NUM2.getPreferredSize().height);

          //---- NUM3 ----
          NUM3.setText("@NUM3@");
          NUM3.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM3.setName("NUM3");
          p_recup.add(NUM3);
          NUM3.setBounds(25, 100, 36, NUM3.getPreferredSize().height);

          //---- NUM4 ----
          NUM4.setText("@NUM4@");
          NUM4.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM4.setName("NUM4");
          p_recup.add(NUM4);
          NUM4.setBounds(25, 125, 36, NUM4.getPreferredSize().height);

          //---- NUM5 ----
          NUM5.setText("@NUM5@");
          NUM5.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM5.setName("NUM5");
          p_recup.add(NUM5);
          NUM5.setBounds(25, 150, 36, NUM5.getPreferredSize().height);

          //---- NUM6 ----
          NUM6.setText("@NUM6@");
          NUM6.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM6.setName("NUM6");
          p_recup.add(NUM6);
          NUM6.setBounds(25, 175, 36, NUM6.getPreferredSize().height);

          //---- NUM7 ----
          NUM7.setText("@NUM7@");
          NUM7.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM7.setName("NUM7");
          p_recup.add(NUM7);
          NUM7.setBounds(25, 200, 36, NUM7.getPreferredSize().height);

          //---- NUM8 ----
          NUM8.setText("@NUM8@");
          NUM8.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM8.setName("NUM8");
          p_recup.add(NUM8);
          NUM8.setBounds(25, 225, 36, NUM8.getPreferredSize().height);

          //---- NUM9 ----
          NUM9.setText("@NUM9@");
          NUM9.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM9.setName("NUM9");
          p_recup.add(NUM9);
          NUM9.setBounds(25, 250, 36, NUM9.getPreferredSize().height);

          //---- NUM10 ----
          NUM10.setText("@NUM10@");
          NUM10.setHorizontalAlignment(SwingConstants.RIGHT);
          NUM10.setName("NUM10");
          p_recup.add(NUM10);
          NUM10.setBounds(25, 275, 36, NUM10.getPreferredSize().height);

          //---- EAF101 ----
          EAF101.setName("EAF101");
          p_recup.add(EAF101);
          EAF101.setBounds(65, 48, 160, EAF101.getPreferredSize().height);

          //---- QTSX1 ----
          QTSX1.setText("@QTSX1@");
          QTSX1.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX1.setName("QTSX1");
          p_recup.add(QTSX1);
          QTSX1.setBounds(229, 50, 84, QTSX1.getPreferredSize().height);

          //---- QTEX1 ----
          QTEX1.setName("QTEX1");
          p_recup.add(QTEX1);
          QTEX1.setBounds(317, 48, 84, QTEX1.getPreferredSize().height);

          //---- QTPX1 ----
          QTPX1.setName("QTPX1");
          p_recup.add(QTPX1);
          QTPX1.setBounds(405, 48, 84, QTPX1.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Num.");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");
          p_recup.add(label1);
          label1.setBounds(25, 25, 36, 26);

          //---- label2 ----
          label2.setText("Affaire");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          p_recup.add(label2);
          label2.setBounds(65, 25, 160, 26);

          //---- label3 ----
          label3.setText("Stock");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          p_recup.add(label3);
          label3.setBounds(229, 25, 84, 26);

          //---- label4 ----
          label4.setText("Quantit\u00e9");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setName("label4");
          p_recup.add(label4);
          label4.setBounds(317, 25, 84, 26);

          //---- label5 ----
          label5.setText("Pi\u00e8ces");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setName("label5");
          p_recup.add(label5);
          label5.setBounds(405, 25, 84, 26);

          //---- EAF102 ----
          EAF102.setName("EAF102");
          p_recup.add(EAF102);
          EAF102.setBounds(65, 73, 160, EAF102.getPreferredSize().height);

          //---- QTSX2 ----
          QTSX2.setText("@QTSX2@");
          QTSX2.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX2.setName("QTSX2");
          p_recup.add(QTSX2);
          QTSX2.setBounds(229, 75, 84, QTSX2.getPreferredSize().height);

          //---- QTEX2 ----
          QTEX2.setName("QTEX2");
          p_recup.add(QTEX2);
          QTEX2.setBounds(317, 73, 84, QTEX2.getPreferredSize().height);

          //---- QTPX2 ----
          QTPX2.setName("QTPX2");
          p_recup.add(QTPX2);
          QTPX2.setBounds(405, 73, 84, QTPX2.getPreferredSize().height);

          //---- EAF103 ----
          EAF103.setName("EAF103");
          p_recup.add(EAF103);
          EAF103.setBounds(65, 98, 160, EAF103.getPreferredSize().height);

          //---- QTSX3 ----
          QTSX3.setText("@QTSX3@");
          QTSX3.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX3.setName("QTSX3");
          p_recup.add(QTSX3);
          QTSX3.setBounds(229, 100, 84, QTSX3.getPreferredSize().height);

          //---- QTEX3 ----
          QTEX3.setName("QTEX3");
          p_recup.add(QTEX3);
          QTEX3.setBounds(317, 98, 84, QTEX3.getPreferredSize().height);

          //---- QTPX3 ----
          QTPX3.setName("QTPX3");
          p_recup.add(QTPX3);
          QTPX3.setBounds(405, 98, 84, QTPX3.getPreferredSize().height);

          //---- EAF104 ----
          EAF104.setName("EAF104");
          p_recup.add(EAF104);
          EAF104.setBounds(65, 123, 160, EAF104.getPreferredSize().height);

          //---- QTSX4 ----
          QTSX4.setText("@QTSX4@");
          QTSX4.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX4.setName("QTSX4");
          p_recup.add(QTSX4);
          QTSX4.setBounds(229, 125, 84, QTSX4.getPreferredSize().height);

          //---- QTEX4 ----
          QTEX4.setName("QTEX4");
          p_recup.add(QTEX4);
          QTEX4.setBounds(317, 123, 84, QTEX4.getPreferredSize().height);

          //---- QTPX4 ----
          QTPX4.setName("QTPX4");
          p_recup.add(QTPX4);
          QTPX4.setBounds(405, 123, 84, QTPX4.getPreferredSize().height);

          //---- EAF105 ----
          EAF105.setName("EAF105");
          p_recup.add(EAF105);
          EAF105.setBounds(65, 148, 160, EAF105.getPreferredSize().height);

          //---- QTSX5 ----
          QTSX5.setText("@QTSX5@");
          QTSX5.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX5.setName("QTSX5");
          p_recup.add(QTSX5);
          QTSX5.setBounds(229, 150, 84, QTSX5.getPreferredSize().height);

          //---- QTEX5 ----
          QTEX5.setName("QTEX5");
          p_recup.add(QTEX5);
          QTEX5.setBounds(317, 148, 84, QTEX5.getPreferredSize().height);

          //---- QTPX5 ----
          QTPX5.setName("QTPX5");
          p_recup.add(QTPX5);
          QTPX5.setBounds(405, 148, 84, QTPX5.getPreferredSize().height);

          //---- EAF106 ----
          EAF106.setName("EAF106");
          p_recup.add(EAF106);
          EAF106.setBounds(65, 173, 160, EAF106.getPreferredSize().height);

          //---- QTSX6 ----
          QTSX6.setText("@QTSX6@");
          QTSX6.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX6.setName("QTSX6");
          p_recup.add(QTSX6);
          QTSX6.setBounds(229, 175, 84, QTSX6.getPreferredSize().height);

          //---- QTEX6 ----
          QTEX6.setName("QTEX6");
          p_recup.add(QTEX6);
          QTEX6.setBounds(317, 173, 84, QTEX6.getPreferredSize().height);

          //---- QTPX6 ----
          QTPX6.setName("QTPX6");
          p_recup.add(QTPX6);
          QTPX6.setBounds(405, 173, 84, QTPX6.getPreferredSize().height);

          //---- EAF107 ----
          EAF107.setName("EAF107");
          p_recup.add(EAF107);
          EAF107.setBounds(65, 198, 160, EAF107.getPreferredSize().height);

          //---- QTSX7 ----
          QTSX7.setText("@QTSX7@");
          QTSX7.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX7.setName("QTSX7");
          p_recup.add(QTSX7);
          QTSX7.setBounds(229, 200, 84, QTSX7.getPreferredSize().height);

          //---- QTEX7 ----
          QTEX7.setName("QTEX7");
          p_recup.add(QTEX7);
          QTEX7.setBounds(317, 198, 84, QTEX7.getPreferredSize().height);

          //---- QTPX7 ----
          QTPX7.setName("QTPX7");
          p_recup.add(QTPX7);
          QTPX7.setBounds(405, 198, 84, QTPX7.getPreferredSize().height);

          //---- EAF108 ----
          EAF108.setName("EAF108");
          p_recup.add(EAF108);
          EAF108.setBounds(65, 223, 160, EAF108.getPreferredSize().height);

          //---- QTSX8 ----
          QTSX8.setText("@QTSX8@");
          QTSX8.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX8.setName("QTSX8");
          p_recup.add(QTSX8);
          QTSX8.setBounds(229, 225, 84, QTSX8.getPreferredSize().height);

          //---- QTEX8 ----
          QTEX8.setName("QTEX8");
          p_recup.add(QTEX8);
          QTEX8.setBounds(317, 223, 84, QTEX8.getPreferredSize().height);

          //---- QTPX8 ----
          QTPX8.setName("QTPX8");
          p_recup.add(QTPX8);
          QTPX8.setBounds(405, 223, 84, QTPX8.getPreferredSize().height);

          //---- EAF109 ----
          EAF109.setName("EAF109");
          p_recup.add(EAF109);
          EAF109.setBounds(65, 248, 160, EAF109.getPreferredSize().height);

          //---- QTSX9 ----
          QTSX9.setText("@QTSX9@");
          QTSX9.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX9.setName("QTSX9");
          p_recup.add(QTSX9);
          QTSX9.setBounds(229, 250, 84, QTSX9.getPreferredSize().height);

          //---- QTEX9 ----
          QTEX9.setName("QTEX9");
          p_recup.add(QTEX9);
          QTEX9.setBounds(317, 248, 84, QTEX9.getPreferredSize().height);

          //---- QTPX9 ----
          QTPX9.setName("QTPX9");
          p_recup.add(QTPX9);
          QTPX9.setBounds(405, 248, 84, QTPX9.getPreferredSize().height);

          //---- EAF110 ----
          EAF110.setName("EAF110");
          p_recup.add(EAF110);
          EAF110.setBounds(65, 273, 160, EAF110.getPreferredSize().height);

          //---- QTSX10 ----
          QTSX10.setText("@QTSX10@");
          QTSX10.setHorizontalAlignment(SwingConstants.RIGHT);
          QTSX10.setName("QTSX10");
          p_recup.add(QTSX10);
          QTSX10.setBounds(229, 275, 84, QTSX10.getPreferredSize().height);

          //---- QTEX10 ----
          QTEX10.setName("QTEX10");
          p_recup.add(QTEX10);
          QTEX10.setBounds(317, 273, 84, QTEX10.getPreferredSize().height);

          //---- QTPX10 ----
          QTPX10.setName("QTPX10");
          p_recup.add(QTPX10);
          QTPX10.setBounds(405, 273, 84, QTPX10.getPreferredSize().height);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Prix Unitaire Moyen Pond\u00e9r\u00e9"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- PHT0X ----
            PHT0X.setComponentPopupMenu(null);
            PHT0X.setName("PHT0X");
            panel1.add(PHT0X);
            PHT0X.setBounds(20, 70, 130, PHT0X.getPreferredSize().height);

            //---- PHT1X ----
            PHT1X.setComponentPopupMenu(null);
            PHT1X.setName("PHT1X");
            panel1.add(PHT1X);
            PHT1X.setBounds(20, 120, 130, PHT1X.getPreferredSize().height);

            //---- OBJ_38 ----
            OBJ_38.setText("Hors affaires");
            OBJ_38.setName("OBJ_38");
            panel1.add(OBJ_38);
            OBJ_38.setBounds(20, 45, 160, 28);

            //---- OBJ_39 ----
            OBJ_39.setText("Affaires");
            OBJ_39.setName("OBJ_39");
            panel1.add(OBJ_39);
            OBJ_39.setBounds(20, 95, 160, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_recup.add(panel1);
          panel1.setBounds(505, 125, 200, 175);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Transfert vers affaire"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- WAFF2 ----
            WAFF2.setComponentPopupMenu(null);
            WAFF2.setName("WAFF2");
            panel2.add(WAFF2);
            WAFF2.setBounds(20, 30, 160, WAFF2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_recup.add(panel2);
          panel2.setBounds(505, 45, 200, 75);

          //---- err1 ----
          err1.setText(">");
          err1.setFont(err1.getFont().deriveFont(err1.getFont().getStyle() | Font.BOLD));
          err1.setForeground(Color.red);
          err1.setHorizontalAlignment(SwingConstants.CENTER);
          err1.setName("err1");
          p_recup.add(err1);
          err1.setBounds(5, 49, 20, 26);

          //---- err2 ----
          err2.setText(">");
          err2.setFont(err2.getFont().deriveFont(err2.getFont().getStyle() | Font.BOLD));
          err2.setForeground(Color.red);
          err2.setHorizontalAlignment(SwingConstants.CENTER);
          err2.setName("err2");
          p_recup.add(err2);
          err2.setBounds(5, 74, 20, 26);

          //---- err3 ----
          err3.setText(">");
          err3.setFont(err3.getFont().deriveFont(err3.getFont().getStyle() | Font.BOLD));
          err3.setForeground(Color.red);
          err3.setHorizontalAlignment(SwingConstants.CENTER);
          err3.setName("err3");
          p_recup.add(err3);
          err3.setBounds(5, 99, 20, 26);

          //---- err4 ----
          err4.setText(">");
          err4.setFont(err4.getFont().deriveFont(err4.getFont().getStyle() | Font.BOLD));
          err4.setForeground(Color.red);
          err4.setHorizontalAlignment(SwingConstants.CENTER);
          err4.setName("err4");
          p_recup.add(err4);
          err4.setBounds(5, 124, 20, 26);

          //---- err5 ----
          err5.setText(">");
          err5.setFont(err5.getFont().deriveFont(err5.getFont().getStyle() | Font.BOLD));
          err5.setForeground(Color.red);
          err5.setHorizontalAlignment(SwingConstants.CENTER);
          err5.setName("err5");
          p_recup.add(err5);
          err5.setBounds(5, 149, 20, 26);

          //---- err6 ----
          err6.setText(">");
          err6.setFont(err6.getFont().deriveFont(err6.getFont().getStyle() | Font.BOLD));
          err6.setForeground(Color.red);
          err6.setHorizontalAlignment(SwingConstants.CENTER);
          err6.setName("err6");
          p_recup.add(err6);
          err6.setBounds(5, 174, 20, 26);

          //---- err7 ----
          err7.setText(">");
          err7.setFont(err7.getFont().deriveFont(err7.getFont().getStyle() | Font.BOLD));
          err7.setForeground(Color.red);
          err7.setHorizontalAlignment(SwingConstants.CENTER);
          err7.setName("err7");
          p_recup.add(err7);
          err7.setBounds(5, 199, 20, 26);

          //---- err8 ----
          err8.setText(">");
          err8.setFont(err8.getFont().deriveFont(err8.getFont().getStyle() | Font.BOLD));
          err8.setForeground(Color.red);
          err8.setHorizontalAlignment(SwingConstants.CENTER);
          err8.setName("err8");
          p_recup.add(err8);
          err8.setBounds(5, 224, 20, 26);

          //---- err9 ----
          err9.setText(">");
          err9.setFont(err9.getFont().deriveFont(err9.getFont().getStyle() | Font.BOLD));
          err9.setForeground(Color.red);
          err9.setHorizontalAlignment(SwingConstants.CENTER);
          err9.setName("err9");
          p_recup.add(err9);
          err9.setBounds(5, 249, 20, 26);

          //---- err10 ----
          err10.setText(">");
          err10.setFont(err10.getFont().deriveFont(err10.getFont().getStyle() | Font.BOLD));
          err10.setForeground(Color.red);
          err10.setHorizontalAlignment(SwingConstants.CENTER);
          err10.setName("err10");
          p_recup.add(err10);
          err10.setBounds(5, 274, 20, 26);

          //---- P67QTX ----
          P67QTX.setText("@P67QTX@");
          P67QTX.setHorizontalAlignment(SwingConstants.RIGHT);
          P67QTX.setName("P67QTX");
          p_recup.add(P67QTX);
          P67QTX.setBounds(317, 300, 84, P67QTX.getPreferredSize().height);

          //---- P67QPX ----
          P67QPX.setText("@P67QPX@");
          P67QPX.setHorizontalAlignment(SwingConstants.RIGHT);
          P67QPX.setName("P67QPX");
          p_recup.add(P67QPX);
          P67QPX.setBounds(405, 300, 84, P67QPX.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 721, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JPanel P_PnlOpts;
  private RiZoneSortie NUM1;
  private RiZoneSortie NUM2;
  private RiZoneSortie NUM3;
  private RiZoneSortie NUM4;
  private RiZoneSortie NUM5;
  private RiZoneSortie NUM6;
  private RiZoneSortie NUM7;
  private RiZoneSortie NUM8;
  private RiZoneSortie NUM9;
  private RiZoneSortie NUM10;
  private XRiTextField EAF101;
  private RiZoneSortie QTSX1;
  private XRiTextField QTEX1;
  private XRiTextField QTPX1;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private XRiTextField EAF102;
  private RiZoneSortie QTSX2;
  private XRiTextField QTEX2;
  private XRiTextField QTPX2;
  private XRiTextField EAF103;
  private RiZoneSortie QTSX3;
  private XRiTextField QTEX3;
  private XRiTextField QTPX3;
  private XRiTextField EAF104;
  private RiZoneSortie QTSX4;
  private XRiTextField QTEX4;
  private XRiTextField QTPX4;
  private XRiTextField EAF105;
  private RiZoneSortie QTSX5;
  private XRiTextField QTEX5;
  private XRiTextField QTPX5;
  private XRiTextField EAF106;
  private RiZoneSortie QTSX6;
  private XRiTextField QTEX6;
  private XRiTextField QTPX6;
  private XRiTextField EAF107;
  private RiZoneSortie QTSX7;
  private XRiTextField QTEX7;
  private XRiTextField QTPX7;
  private XRiTextField EAF108;
  private RiZoneSortie QTSX8;
  private XRiTextField QTEX8;
  private XRiTextField QTPX8;
  private XRiTextField EAF109;
  private RiZoneSortie QTSX9;
  private XRiTextField QTEX9;
  private XRiTextField QTPX9;
  private XRiTextField EAF110;
  private RiZoneSortie QTSX10;
  private XRiTextField QTEX10;
  private XRiTextField QTPX10;
  private JPanel panel1;
  private XRiTextField PHT0X;
  private XRiTextField PHT1X;
  private JLabel OBJ_38;
  private JLabel OBJ_39;
  private JPanel panel2;
  private XRiTextField WAFF2;
  private JLabel err1;
  private JLabel err2;
  private JLabel err3;
  private JLabel err4;
  private JLabel err5;
  private JLabel err6;
  private JLabel err7;
  private JLabel err8;
  private JLabel err9;
  private JLabel err10;
  private RiZoneSortie P67QTX;
  private RiZoneSortie P67QPX;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
