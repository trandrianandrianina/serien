
package ri.serien.libecranrpg.vgvx.VGVX60FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX60FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGVX60FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    L1ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1ART@")).trim());
    L1NUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1NUM@")).trim());
    L1NLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1NLI@")).trim());
    E1MAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1MAG@")).trim());
    L1SUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1SUF@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WQTVX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQTVX@")).trim());
    WQTSX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQTSX@")).trim());
    L1KSV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1KSV@")).trim());
    L1UNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1UNV@")).trim());
    L1UNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1UNS@")).trim());
    E1HOMX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1HOMX@")).trim());
    WDLPX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDLPX@")).trim());
    L1SER.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1SER@")).trim());
    QTX02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTX02@")).trim());
    NUM02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM02@")).trim());
    NLI02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NLI02@")).trim());
    SUF02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SUF02@")).trim());
    ETA02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETA02@")).trim());
    DLX02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLX02@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAG@")).trim());
    QTX03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTX03@")).trim());
    NUM03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUM03@")).trim());
    SUF03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SUF03@")).trim());
    NLI03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NLI03@")).trim());
    ETA03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETA03@")).trim());
    DLX03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DLX03@")).trim());
    OBJ_94.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AFFECT@")).trim());
    OBJ_95.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AFFECT2@")).trim());
    OBJ_101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BOR_OF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    SUF03.setVisible(lexique.isPresent("SUF03"));
    SUF02.setEnabled(lexique.isPresent("SUF02"));
    L1SER.setVisible(lexique.isPresent("L1SER"));
    WTP05.setVisible(lexique.isPresent("WTP05"));
    OBJ_105.setVisible(lexique.isPresent("WTP03"));
    OBJ_104.setVisible(lexique.isPresent("WTP04"));
    OBJ_83.setVisible(lexique.isPresent("ETA02"));
    L1UNS.setEnabled(lexique.isPresent("L1UNS"));
    L1UNV.setEnabled(lexique.isPresent("L1UNV"));
    OBJ_134.setVisible(lexique.isPresent("WDATTX"));
    OBJ_76.setVisible(lexique.isPresent("L1SER"));
    NLI03.setVisible(lexique.isPresent("NLI03"));
    NLI02.setEnabled(lexique.isPresent("NLI02"));
    ETA03.setVisible(lexique.isPresent("ETA03"));
    ETA04.setVisible(lexique.isPresent("ETA04"));
    ETA02.setEnabled(lexique.isPresent("ETA02"));
    OBJ_102.setVisible(lexique.isPresent("WTP04"));
    NUM04.setVisible(lexique.isPresent("WTP04"));
    NUM03.setVisible(lexique.isPresent("NUM03"));
    NUM02.setEnabled(lexique.isPresent("NUM02"));
    OBJ_74.setVisible(lexique.isPresent("WTP05"));
    OBJ_61.setVisible(!lexique.HostFieldGetData("WMAG").trim().equalsIgnoreCase(""));
    OBJ_103.setVisible(lexique.isPresent("WTP03"));
    WDLPX.setEnabled(lexique.isPresent("WDLPX"));
    E1HOMX.setEnabled(lexique.isPresent("E1HOMX"));
    DLX03.setVisible(lexique.isPresent("DLX03"));
    // DLX04.setVisible( lexique.isPresent("DLX04"));
    DLX02.setEnabled(lexique.isPresent("DLX02"));
    // DAX01.setEnabled( lexique.isPresent("DAX01"));
    // OBJ_133.setVisible( lexique.isPresent("WATTX3"));
    // OBJ_132.setVisible( lexique.isPresent("WATTX"));
    // WDATTX.setVisible( lexique.isPresent("WDATTX"));
    OBJ_99.setVisible(lexique.isPresent("WTP03"));
    OBJ_98.setVisible(lexique.isPresent("WTP04"));
    OBJ_115.setVisible(lexique.HostFieldGetData("OF").equalsIgnoreCase("+") & lexique.isPresent("WTP04"));
    L1KSV.setEnabled(lexique.isPresent("L1KSV"));
    OBJ_121.setVisible(lexique.isPresent("WTP04"));
    OBJ_109.setVisible(lexique.isPresent("WTP03"));
    OBJ_108.setVisible(lexique.isPresent("WTP03"));
    OBJ_107.setVisible(lexique.isPresent("WTP04"));
    OBJ_106.setVisible(lexique.isPresent("WTP04"));
    OBJ_85.setEnabled(lexique.isPresent("WTP02"));
    OBJ_84.setEnabled(lexique.isPresent("WTP02"));
    OBJ_69.setEnabled(lexique.isPresent("WTP01"));
    OBJ_68.setEnabled(lexique.isPresent("WTP01"));
    // OBJ_128.setVisible( lexique.isPresent("WATTX2"));
    // OBJ_130.setVisible( lexique.isPresent("WDISX"));
    // OBJ_129.setVisible( lexique.isPresent("WAFFX"));
    // OBJ_126.setVisible( lexique.isPresent("WQRTX"));
    // OBJ_125.setVisible( lexique.isPresent("WRESX2"));
    OBJ_100.setVisible(lexique.isPresent("WTP03"));
    OBJ_101.setVisible(lexique.isPresent("WTP04"));
    // OBJ_131.setVisible( lexique.isPresent("WCNRX"));
    // OBJ_127.setVisible( lexique.isPresent("WDISX3"));
    // OBJ_124.setVisible( lexique.isPresent("WRESX"));
    QTX03.setVisible(lexique.isPresent("QTX03"));
    QTX04.setVisible(lexique.isPresent("WTP04"));
    QTX02.setEnabled(lexique.isPresent("QTX02"));
    QTX01.setEnabled(lexique.isPresent("QTX01"));
    WQTSX.setEnabled(lexique.isPresent("WQTSX"));
    WQTVX.setEnabled(lexique.isPresent("WQTVX"));
    WATTX3.setVisible(lexique.isPresent("WATTX3"));
    WATTX.setVisible(lexique.isPresent("WATTX"));
    WDISX.setVisible(lexique.isPresent("WDISX"));
    WCNRX.setVisible(lexique.isPresent("WCNRX"));
    WDISX3.setVisible(lexique.isPresent("WDISX3"));
    WAFFX.setVisible(lexique.isPresent("WAFFX"));
    WATTX2.setVisible(lexique.isPresent("WATTX2"));
    WRESX.setVisible(lexique.isPresent("WRESX"));
    WRESX2.setVisible(lexique.isPresent("WRESX2"));
    WQRTX.setVisible(lexique.isPresent("WQRTX"));
    WSTKX.setEnabled(lexique.isPresent("WSTKX"));
    // OBJ_38.setVisible( lexique.isPresent("V01F"));
    OBJ_95.setVisible(lexique.isPresent("WTP04"));
    OBJ_94.setVisible(lexique.isPresent("WTP03"));
    WDISX2.setText(lexique.HostFieldGetData("WDISX2"));
    
    panel3.setVisible(!lexique.isTrue("27") && !lexique.isTrue("26"));
    panel4.setVisible(lexique.isTrue("27"));
    panel2.setVisible(lexique.isTrue("26"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 0, "4");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_84ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP02", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_85ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP02", 0, "4");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_109ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP03", 0, "4");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_108ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP03", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_106ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP04", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_107ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP04", 0, "4");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_121ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP04", 0, "+");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    A1LIB = new RiZoneSortie();
    L1ART = new RiZoneSortie();
    OBJ_32 = new JLabel();
    L1NUM = new RiZoneSortie();
    OBJ_35 = new JLabel();
    L1NLI = new RiZoneSortie();
    OBJ_34 = new JLabel();
    OBJ_37 = new JLabel();
    E1MAG = new RiZoneSortie();
    L1SUF = new RiZoneSortie();
    OBJ_33 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_47 = new JLabel();
    OBJ_48 = new JLabel();
    WQTVX = new RiZoneSortie();
    OBJ_49 = new JLabel();
    OBJ_55 = new JLabel();
    WQTSX = new RiZoneSortie();
    L1KSV = new RiZoneSortie();
    OBJ_54 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_59 = new JLabel();
    L1UNV = new RiZoneSortie();
    L1UNS = new RiZoneSortie();
    E1HOMX = new RiZoneSortie();
    WDLPX = new RiZoneSortie();
    xtp1 = new JXTitledPanel();
    OBJ_68 = new SNBoutonLeger();
    OBJ_69 = new SNBoutonLeger();
    OBJ_70 = new JLabel();
    QTX01 = new XRiTextField();
    OBJ_72 = new JLabel();
    OBJ_74 = new JLabel();
    WTP05 = new XRiTextField();
    OBJ_76 = new JLabel();
    L1SER = new RiZoneSortie();
    DAX01 = new XRiCalendrier();
    xtp2 = new JXTitledPanel();
    OBJ_84 = new SNBoutonLeger();
    OBJ_85 = new SNBoutonLeger();
    OBJ_80 = new JLabel();
    QTX02 = new RiZoneSortie();
    NUM02 = new RiZoneSortie();
    OBJ_81 = new JLabel();
    NLI02 = new RiZoneSortie();
    OBJ_82 = new JLabel();
    SUF02 = new RiZoneSortie();
    ETA02 = new RiZoneSortie();
    OBJ_83 = new JLabel();
    DLX02 = new RiZoneSortie();
    OBJ_61 = new JLabel();
    OBJ_62 = new JLabel();
    xtp3 = new JXTitledPanel();
    OBJ_109 = new SNBoutonLeger();
    OBJ_108 = new SNBoutonLeger();
    OBJ_98 = new JLabel();
    QTX03 = new RiZoneSortie();
    NUM03 = new RiZoneSortie();
    SUF03 = new RiZoneSortie();
    NLI03 = new RiZoneSortie();
    OBJ_103 = new JLabel();
    ETA03 = new RiZoneSortie();
    OBJ_104 = new JLabel();
    DLX03 = new RiZoneSortie();
    OBJ_100 = new JLabel();
    panel3 = new JPanel();
    OBJ_124 = new JLabel();
    OBJ_136 = new JLabel();
    WRESX = new XRiTextField();
    OBJ_129 = new JLabel();
    WAFFX = new XRiTextField();
    OBJ_145 = new JLabel();
    OBJ_131 = new JLabel();
    WDISX = new XRiTextField();
    OBJ_132 = new JLabel();
    WATTX = new XRiTextField();
    panel5 = new JPanel();
    OBJ_123 = new JLabel();
    WSTKX = new XRiTextField();
    panel4 = new JPanel();
    OBJ_137 = new JLabel();
    OBJ_126 = new JLabel();
    WQRTX = new XRiTextField();
    OBJ_146 = new JLabel();
    OBJ_127 = new JLabel();
    WDISX3 = new XRiTextField();
    OBJ_130 = new JLabel();
    WCNRX = new XRiTextField();
    WATTX3 = new XRiTextField();
    OBJ_133 = new JLabel();
    panel2 = new JPanel();
    OBJ_125 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_134 = new JLabel();
    OBJ_140 = new JLabel();
    OBJ_141 = new JLabel();
    OBJ_138 = new JLabel();
    WRESX2 = new XRiTextField();
    WATTX2 = new XRiTextField();
    WDISX2 = new XRiTextField();
    OBJ_135 = new JLabel();
    WDATTX = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_66 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_95 = new JLabel();
    xTitledPanel3 = new JXTitledPanel();
    OBJ_106 = new JButton();
    OBJ_107 = new JButton();
    OBJ_99 = new JLabel();
    QTX04 = new XRiTextField();
    NUM04 = new XRiTextField();
    OBJ_102 = new JLabel();
    ETA04 = new XRiTextField();
    OBJ_105 = new JLabel();
    OBJ_121 = new JButton();
    OBJ_115 = new JLabel();
    DLX04 = new XRiCalendrier();
    OBJ_101 = new JLabel();
    OBJ_7 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1050, 640));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Affectation ligne de vente");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(899, 32));
          p_tete_gauche.setMinimumSize(new Dimension(899, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- A1LIB ----
          A1LIB.setOpaque(false);
          A1LIB.setText("@A1LIB@");
          A1LIB.setName("A1LIB");

          //---- L1ART ----
          L1ART.setOpaque(false);
          L1ART.setText("@L1ART@");
          L1ART.setName("L1ART");

          //---- OBJ_32 ----
          OBJ_32.setText("Num\u00e9ro");
          OBJ_32.setName("OBJ_32");

          //---- L1NUM ----
          L1NUM.setOpaque(false);
          L1NUM.setText("@L1NUM@");
          L1NUM.setName("L1NUM");

          //---- OBJ_35 ----
          OBJ_35.setText("Article");
          OBJ_35.setName("OBJ_35");

          //---- L1NLI ----
          L1NLI.setOpaque(false);
          L1NLI.setText("@L1NLI@");
          L1NLI.setName("L1NLI");

          //---- OBJ_34 ----
          OBJ_34.setText("Ligne");
          OBJ_34.setName("OBJ_34");

          //---- OBJ_37 ----
          OBJ_37.setText("Magasin");
          OBJ_37.setName("OBJ_37");

          //---- E1MAG ----
          E1MAG.setOpaque(false);
          E1MAG.setText("@E1MAG@");
          E1MAG.setName("E1MAG");

          //---- L1SUF ----
          L1SUF.setOpaque(false);
          L1SUF.setText("@L1SUF@");
          L1SUF.setName("L1SUF");

          //---- OBJ_33 ----
          OBJ_33.setText("S");
          OBJ_33.setName("OBJ_33");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(L1NUM, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(L1SUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(L1NLI, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(40, 40, 40)
                    .addComponent(A1LIB, GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(L1ART, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(E1MAG, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(L1NUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(L1SUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(L1NLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(A1LIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(L1ART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(E1MAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(50, 0));
          p_tete_droite.setMinimumSize(new Dimension(50, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_47 ----
            OBJ_47.setText("Quantit\u00e9");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(50, 5, 65, 25);

            //---- OBJ_48 ----
            OBJ_48.setText("Unit\u00e9");
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(175, 5, 45, 25);

            //---- WQTVX ----
            WQTVX.setText("@WQTVX@");
            WQTVX.setHorizontalAlignment(SwingConstants.RIGHT);
            WQTVX.setName("WQTVX");
            panel1.add(WQTVX);
            WQTVX.setBounds(50, 32, 120, WQTVX.getPreferredSize().height);

            //---- OBJ_49 ----
            OBJ_49.setText("UV");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(10, 34, 22, 20);

            //---- OBJ_55 ----
            OBJ_55.setText("US");
            OBJ_55.setName("OBJ_55");
            panel1.add(OBJ_55);
            OBJ_55.setBounds(10, 64, 22, 20);

            //---- WQTSX ----
            WQTSX.setText("@WQTSX@");
            WQTSX.setHorizontalAlignment(SwingConstants.RIGHT);
            WQTSX.setName("WQTSX");
            panel1.add(WQTSX);
            WQTSX.setBounds(50, 62, 120, WQTSX.getPreferredSize().height);

            //---- L1KSV ----
            L1KSV.setText("@L1KSV@");
            L1KSV.setName("L1KSV");
            panel1.add(L1KSV);
            L1KSV.setBounds(220, 62, 74, L1KSV.getPreferredSize().height);

            //---- OBJ_54 ----
            OBJ_54.setText("NbUV/1US");
            OBJ_54.setName("OBJ_54");
            panel1.add(OBJ_54);
            OBJ_54.setBounds(223, 32, 73, 24);

            //---- OBJ_52 ----
            OBJ_52.setText("Date commande");
            OBJ_52.setName("OBJ_52");
            panel1.add(OBJ_52);
            OBJ_52.setBounds(325, 34, 115, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("Livraison pr\u00e9vue");
            OBJ_59.setName("OBJ_59");
            panel1.add(OBJ_59);
            OBJ_59.setBounds(325, 64, 115, 20);

            //---- L1UNV ----
            L1UNV.setText("@L1UNV@");
            L1UNV.setName("L1UNV");
            panel1.add(L1UNV);
            L1UNV.setBounds(175, 32, 30, L1UNV.getPreferredSize().height);

            //---- L1UNS ----
            L1UNS.setText("@L1UNS@");
            L1UNS.setName("L1UNS");
            panel1.add(L1UNS);
            L1UNS.setBounds(175, 62, 30, L1UNS.getPreferredSize().height);

            //---- E1HOMX ----
            E1HOMX.setText("@E1HOMX@");
            E1HOMX.setHorizontalAlignment(SwingConstants.CENTER);
            E1HOMX.setName("E1HOMX");
            panel1.add(E1HOMX);
            E1HOMX.setBounds(440, 32, 70, E1HOMX.getPreferredSize().height);

            //---- WDLPX ----
            WDLPX.setText("@WDLPX@");
            WDLPX.setHorizontalAlignment(SwingConstants.CENTER);
            WDLPX.setName("WDLPX");
            panel1.add(WDLPX);
            WDLPX.setBounds(440, 62, 70, WDLPX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== xtp1 ========
          {
            xtp1.setTitle("Affectation sur stock disponible");
            xtp1.setBorder(new DropShadowBorder());
            xtp1.setName("xtp1");
            Container xtp1ContentContainer = xtp1.getContentContainer();
            xtp1ContentContainer.setLayout(null);

            //---- OBJ_68 ----
            OBJ_68.setText("D\u00e9tail");
            OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_68.setName("OBJ_68");
            OBJ_68.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_68ActionPerformed(e);
              }
            });
            xtp1ContentContainer.add(OBJ_68);
            OBJ_68.setBounds(5, 22, 64, OBJ_68.getPreferredSize().height);

            //---- OBJ_69 ----
            OBJ_69.setText("Annulation");
            OBJ_69.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_69.setName("OBJ_69");
            OBJ_69.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_69ActionPerformed(e);
              }
            });
            xtp1ContentContainer.add(OBJ_69);
            OBJ_69.setBounds(75, 22, 100, OBJ_69.getPreferredSize().height);

            //---- OBJ_70 ----
            OBJ_70.setText("Quantit\u00e9 affect\u00e9e");
            OBJ_70.setName("OBJ_70");
            xtp1ContentContainer.add(OBJ_70);
            OBJ_70.setBounds(185, 24, 100, 20);

            //---- QTX01 ----
            QTX01.setComponentPopupMenu(BTD);
            QTX01.setName("QTX01");
            xtp1ContentContainer.add(QTX01);
            QTX01.setBounds(285, 20, 100, QTX01.getPreferredSize().height);

            //---- OBJ_72 ----
            OBJ_72.setText("D\u00e9but");
            OBJ_72.setName("OBJ_72");
            xtp1ContentContainer.add(OBJ_72);
            OBJ_72.setBounds(395, 24, 42, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("N\u00b0 s\u00e9rie");
            OBJ_74.setName("OBJ_74");
            xtp1ContentContainer.add(OBJ_74);
            OBJ_74.setBounds(555, 24, 55, 20);

            //---- WTP05 ----
            WTP05.setComponentPopupMenu(BTD);
            WTP05.setName("WTP05");
            xtp1ContentContainer.add(WTP05);
            WTP05.setBounds(610, 20, 20, WTP05.getPreferredSize().height);

            //---- OBJ_76 ----
            OBJ_76.setText("Etat");
            OBJ_76.setName("OBJ_76");
            xtp1ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(640, 24, 35, 20);

            //---- L1SER ----
            L1SER.setText("@L1SER@");
            L1SER.setName("L1SER");
            xtp1ContentContainer.add(L1SER);
            L1SER.setBounds(675, 20, 20, L1SER.getPreferredSize().height);

            //---- DAX01 ----
            DAX01.setName("DAX01");
            xtp1ContentContainer.add(DAX01);
            DAX01.setBounds(440, 20, 105, DAX01.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xtp1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xtp1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xtp1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xtp1ContentContainer.setMinimumSize(preferredSize);
              xtp1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xtp2 ========
          {
            xtp2.setBorder(new DropShadowBorder());
            xtp2.setTitle("Affectation sur attendu");
            xtp2.setName("xtp2");
            Container xtp2ContentContainer = xtp2.getContentContainer();
            xtp2ContentContainer.setLayout(null);

            //---- OBJ_84 ----
            OBJ_84.setText("D\u00e9tail");
            OBJ_84.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_84.setName("OBJ_84");
            OBJ_84.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_84ActionPerformed(e);
              }
            });
            xtp2ContentContainer.add(OBJ_84);
            OBJ_84.setBounds(5, 22, 64, OBJ_84.getPreferredSize().height);

            //---- OBJ_85 ----
            OBJ_85.setText("Annulation");
            OBJ_85.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_85.setName("OBJ_85");
            OBJ_85.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_85ActionPerformed(e);
              }
            });
            xtp2ContentContainer.add(OBJ_85);
            OBJ_85.setBounds(75, 22, 100, OBJ_85.getPreferredSize().height);

            //---- OBJ_80 ----
            OBJ_80.setText("Quantit\u00e9 affect\u00e9e");
            OBJ_80.setName("OBJ_80");
            xtp2ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(185, 24, 100, 20);

            //---- QTX02 ----
            QTX02.setText("@QTX02@");
            QTX02.setHorizontalAlignment(SwingConstants.RIGHT);
            QTX02.setName("QTX02");
            xtp2ContentContainer.add(QTX02);
            QTX02.setBounds(285, 22, 100, QTX02.getPreferredSize().height);

            //---- NUM02 ----
            NUM02.setText("@NUM02@");
            NUM02.setName("NUM02");
            xtp2ContentContainer.add(NUM02);
            NUM02.setBounds(420, 22, 58, NUM02.getPreferredSize().height);

            //---- OBJ_81 ----
            OBJ_81.setText("Ligne d'achat");
            OBJ_81.setName("OBJ_81");
            xtp2ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(425, 0, 87, 24);

            //---- NLI02 ----
            NLI02.setText("@NLI02@");
            NLI02.setName("NLI02");
            xtp2ContentContainer.add(NLI02);
            NLI02.setBounds(500, 22, 42, NLI02.getPreferredSize().height);

            //---- OBJ_82 ----
            OBJ_82.setText("Livraison");
            OBJ_82.setName("OBJ_82");
            xtp2ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(555, 0, 60, 24);

            //---- SUF02 ----
            SUF02.setText("@SUF02@");
            SUF02.setName("SUF02");
            xtp2ContentContainer.add(SUF02);
            SUF02.setBounds(480, 22, 18, SUF02.getPreferredSize().height);

            //---- ETA02 ----
            ETA02.setText("@ETA02@");
            ETA02.setName("ETA02");
            xtp2ContentContainer.add(ETA02);
            ETA02.setBounds(690, 22, 42, ETA02.getPreferredSize().height);

            //---- OBJ_83 ----
            OBJ_83.setText("Etat");
            OBJ_83.setName("OBJ_83");
            xtp2ContentContainer.add(OBJ_83);
            OBJ_83.setBounds(690, 0, 40, 24);

            //---- DLX02 ----
            DLX02.setText("@DLX02@");
            DLX02.setHorizontalAlignment(SwingConstants.CENTER);
            DLX02.setName("DLX02");
            xtp2ContentContainer.add(DLX02);
            DLX02.setBounds(555, 22, 70, DLX02.getPreferredSize().height);

            //---- OBJ_61 ----
            OBJ_61.setText("Magasin");
            OBJ_61.setName("OBJ_61");
            xtp2ContentContainer.add(OBJ_61);
            OBJ_61.setBounds(15, 0, 52, 24);

            //---- OBJ_62 ----
            OBJ_62.setText("@WMAG@");
            OBJ_62.setName("OBJ_62");
            xtp2ContentContainer.add(OBJ_62);
            OBJ_62.setBounds(85, 0, 43, 24);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xtp2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xtp2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xtp2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xtp2ContentContainer.setMinimumSize(preferredSize);
              xtp2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xtp3 ========
          {
            xtp3.setTitle("Affectation type GBA (G\u00e9n\u00e9ration bon d'achat)");
            xtp3.setBorder(new DropShadowBorder());
            xtp3.setName("xtp3");
            Container xtp3ContentContainer = xtp3.getContentContainer();
            xtp3ContentContainer.setLayout(null);

            //---- OBJ_109 ----
            OBJ_109.setText("Annulation");
            OBJ_109.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_109.setName("OBJ_109");
            OBJ_109.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_109ActionPerformed(e);
              }
            });
            xtp3ContentContainer.add(OBJ_109);
            OBJ_109.setBounds(75, 25, 100, OBJ_109.getPreferredSize().height);

            //---- OBJ_108 ----
            OBJ_108.setText("D\u00e9tail");
            OBJ_108.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_108.setName("OBJ_108");
            OBJ_108.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_108ActionPerformed(e);
              }
            });
            xtp3ContentContainer.add(OBJ_108);
            OBJ_108.setBounds(5, 25, 64, OBJ_108.getPreferredSize().height);

            //---- OBJ_98 ----
            OBJ_98.setText("Quantit\u00e9 affect\u00e9e");
            OBJ_98.setName("OBJ_98");
            xtp3ContentContainer.add(OBJ_98);
            OBJ_98.setBounds(185, 27, 100, 20);

            //---- QTX03 ----
            QTX03.setText("@QTX03@");
            QTX03.setHorizontalAlignment(SwingConstants.RIGHT);
            QTX03.setName("QTX03");
            xtp3ContentContainer.add(QTX03);
            QTX03.setBounds(285, 25, 100, QTX03.getPreferredSize().height);

            //---- NUM03 ----
            NUM03.setText("@NUM03@");
            NUM03.setName("NUM03");
            xtp3ContentContainer.add(NUM03);
            NUM03.setBounds(420, 25, 58, NUM03.getPreferredSize().height);

            //---- SUF03 ----
            SUF03.setText("@SUF03@");
            SUF03.setName("SUF03");
            xtp3ContentContainer.add(SUF03);
            SUF03.setBounds(480, 25, 18, SUF03.getPreferredSize().height);

            //---- NLI03 ----
            NLI03.setText("@NLI03@");
            NLI03.setName("NLI03");
            xtp3ContentContainer.add(NLI03);
            NLI03.setBounds(500, 25, 42, NLI03.getPreferredSize().height);

            //---- OBJ_103 ----
            OBJ_103.setText("Livraison");
            OBJ_103.setName("OBJ_103");
            xtp3ContentContainer.add(OBJ_103);
            OBJ_103.setBounds(555, 5, 55, 18);

            //---- ETA03 ----
            ETA03.setText("@ETA03@");
            ETA03.setName("ETA03");
            xtp3ContentContainer.add(ETA03);
            ETA03.setBounds(690, 25, 42, ETA03.getPreferredSize().height);

            //---- OBJ_104 ----
            OBJ_104.setText("Etat");
            OBJ_104.setName("OBJ_104");
            xtp3ContentContainer.add(OBJ_104);
            OBJ_104.setBounds(690, 5, 27, OBJ_104.getPreferredSize().height);

            //---- DLX03 ----
            DLX03.setText("@DLX03@");
            DLX03.setHorizontalAlignment(SwingConstants.CENTER);
            DLX03.setName("DLX03");
            xtp3ContentContainer.add(DLX03);
            DLX03.setBounds(555, 25, 70, DLX03.getPreferredSize().height);

            //---- OBJ_100 ----
            OBJ_100.setText("Ligne d'achat");
            OBJ_100.setName("OBJ_100");
            xtp3ContentContainer.add(OBJ_100);
            OBJ_100.setBounds(425, 5, 88, 24);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xtp3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xtp3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xtp3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xtp3ContentContainer.setMinimumSize(preferredSize);
              xtp3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_124 ----
            OBJ_124.setText("Command\u00e9");
            OBJ_124.setName("OBJ_124");
            panel3.add(OBJ_124);
            OBJ_124.setBounds(45, 10, 85, 20);

            //---- OBJ_136 ----
            OBJ_136.setText("-");
            OBJ_136.setName("OBJ_136");
            panel3.add(OBJ_136);
            OBJ_136.setBounds(15, 35, 12, 20);

            //---- WRESX ----
            WRESX.setName("WRESX");
            panel3.add(WRESX);
            WRESX.setBounds(45, 31, 120, WRESX.getPreferredSize().height);

            //---- OBJ_129 ----
            OBJ_129.setText("Dont r\u00e9serv\u00e9");
            OBJ_129.setName("OBJ_129");
            panel3.add(OBJ_129);
            OBJ_129.setBounds(195, 10, 79, 20);

            //---- WAFFX ----
            WAFFX.setName("WAFFX");
            panel3.add(WAFFX);
            WAFFX.setBounds(195, 31, 120, WAFFX.getPreferredSize().height);

            //---- OBJ_145 ----
            OBJ_145.setText("=");
            OBJ_145.setName("OBJ_145");
            panel3.add(OBJ_145);
            OBJ_145.setBounds(330, 35, 13, 20);

            //---- OBJ_131 ----
            OBJ_131.setText("Disponible");
            OBJ_131.setName("OBJ_131");
            panel3.add(OBJ_131);
            OBJ_131.setBounds(355, 10, 85, 20);

            //---- WDISX ----
            WDISX.setName("WDISX");
            panel3.add(WDISX);
            WDISX.setBounds(355, 31, 120, WDISX.getPreferredSize().height);

            //---- OBJ_132 ----
            OBJ_132.setText("Attendu");
            OBJ_132.setName("OBJ_132");
            panel3.add(OBJ_132);
            OBJ_132.setBounds(505, 10, 57, 20);

            //---- WATTX ----
            WATTX.setName("WATTX");
            panel3.add(WATTX);
            WATTX.setBounds(505, 31, 120, WATTX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- OBJ_123 ----
            OBJ_123.setText("En Stock");
            OBJ_123.setName("OBJ_123");
            panel5.add(OBJ_123);
            OBJ_123.setBounds(15, 10, 57, 20);

            //---- WSTKX ----
            WSTKX.setName("WSTKX");
            panel5.add(WSTKX);
            WSTKX.setBounds(15, 31, 120, WSTKX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OBJ_137 ----
            OBJ_137.setText("-");
            OBJ_137.setName("OBJ_137");
            panel4.add(OBJ_137);
            OBJ_137.setBounds(15, 35, 12, 20);

            //---- OBJ_126 ----
            OBJ_126.setText("R\u00e9serv\u00e9");
            OBJ_126.setName("OBJ_126");
            panel4.add(OBJ_126);
            OBJ_126.setBounds(45, 10, 79, 20);

            //---- WQRTX ----
            WQRTX.setName("WQRTX");
            panel4.add(WQRTX);
            WQRTX.setBounds(45, 31, 120, WQRTX.getPreferredSize().height);

            //---- OBJ_146 ----
            OBJ_146.setText("=");
            OBJ_146.setName("OBJ_146");
            panel4.add(OBJ_146);
            OBJ_146.setBounds(175, 35, 13, 20);

            //---- OBJ_127 ----
            OBJ_127.setText("Disponible");
            OBJ_127.setName("OBJ_127");
            panel4.add(OBJ_127);
            OBJ_127.setBounds(195, 10, 85, 20);

            //---- WDISX3 ----
            WDISX3.setName("WDISX3");
            panel4.add(WDISX3);
            WDISX3.setBounds(195, 31, 120, WDISX3.getPreferredSize().height);

            //---- OBJ_130 ----
            OBJ_130.setText("Command\u00e9");
            OBJ_130.setName("OBJ_130");
            panel4.add(OBJ_130);
            OBJ_130.setBounds(355, 10, 79, 20);

            //---- WCNRX ----
            WCNRX.setName("WCNRX");
            panel4.add(WCNRX);
            WCNRX.setBounds(355, 31, 120, WCNRX.getPreferredSize().height);

            //---- WATTX3 ----
            WATTX3.setName("WATTX3");
            panel4.add(WATTX3);
            WATTX3.setBounds(505, 31, 120, WATTX3.getPreferredSize().height);

            //---- OBJ_133 ----
            OBJ_133.setText("Attendu");
            OBJ_133.setName("OBJ_133");
            panel4.add(OBJ_133);
            OBJ_133.setBounds(505, 10, 57, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_125 ----
            OBJ_125.setText("Command\u00e9");
            OBJ_125.setName("OBJ_125");
            panel2.add(OBJ_125);
            OBJ_125.setBounds(45, 10, 79, 20);

            //---- OBJ_128 ----
            OBJ_128.setText("Attendu");
            OBJ_128.setName("OBJ_128");
            panel2.add(OBJ_128);
            OBJ_128.setBounds(195, 10, 73, 20);

            //---- OBJ_134 ----
            OBJ_134.setText("au");
            OBJ_134.setName("OBJ_134");
            panel2.add(OBJ_134);
            OBJ_134.setBounds(505, 10, 24, 20);

            //---- OBJ_140 ----
            OBJ_140.setText("=");
            OBJ_140.setName("OBJ_140");
            panel2.add(OBJ_140);
            OBJ_140.setBounds(325, 35, 13, 20);

            //---- OBJ_141 ----
            OBJ_141.setText("+");
            OBJ_141.setName("OBJ_141");
            panel2.add(OBJ_141);
            OBJ_141.setBounds(175, 35, 13, 20);

            //---- OBJ_138 ----
            OBJ_138.setText("-");
            OBJ_138.setName("OBJ_138");
            panel2.add(OBJ_138);
            OBJ_138.setBounds(15, 35, 12, 20);

            //---- WRESX2 ----
            WRESX2.setName("WRESX2");
            panel2.add(WRESX2);
            WRESX2.setBounds(45, 31, 120, WRESX2.getPreferredSize().height);

            //---- WATTX2 ----
            WATTX2.setName("WATTX2");
            panel2.add(WATTX2);
            WATTX2.setBounds(195, 31, 120, WATTX2.getPreferredSize().height);

            //---- WDISX2 ----
            WDISX2.setName("WDISX2");
            panel2.add(WDISX2);
            WDISX2.setBounds(355, 31, 120, WDISX2.getPreferredSize().height);

            //---- OBJ_135 ----
            OBJ_135.setText("Disponible");
            OBJ_135.setName("OBJ_135");
            panel2.add(OBJ_135);
            OBJ_135.setBounds(355, 10, 85, 20);

            //---- WDATTX ----
            WDATTX.setName("WDATTX");
            panel2.add(WDATTX);
            WDATTX.setBounds(505, 31, 115, WDATTX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(19, 19, 19)
                    .addComponent(xtp1, GroupLayout.PREFERRED_SIZE, 805, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(19, 19, 19)
                    .addComponent(xtp2, GroupLayout.PREFERRED_SIZE, 805, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(19, 19, 19)
                    .addComponent(xtp3, GroupLayout.PREFERRED_SIZE, 805, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(9, 9, 9)
                    .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 635, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 635, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 635, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(21, 21, 21)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 727, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(24, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xtp1, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(xtp2, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xtp3, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_66 ----
    OBJ_66.setText("Affectation sur Stock Disponible");
    OBJ_66.setName("OBJ_66");

    //---- OBJ_78 ----
    OBJ_78.setText("Affectation sur Attendu");
    OBJ_78.setName("OBJ_78");

    //---- OBJ_94 ----
    OBJ_94.setText("@AFFECT@");
    OBJ_94.setName("OBJ_94");

    //---- OBJ_95 ----
    OBJ_95.setText("@AFFECT2@");
    OBJ_95.setName("OBJ_95");

    //======== xTitledPanel3 ========
    {
      xTitledPanel3.setTitle("Affectation sur Composition (GNM)");
      xTitledPanel3.setBorder(new DropShadowBorder());
      xTitledPanel3.setName("xTitledPanel3");
      Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
      xTitledPanel3ContentContainer.setLayout(null);

      //---- OBJ_106 ----
      OBJ_106.setText("D\u00e9tail");
      OBJ_106.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_106.setName("OBJ_106");
      OBJ_106.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_106ActionPerformed(e);
        }
      });
      xTitledPanel3ContentContainer.add(OBJ_106);
      OBJ_106.setBounds(0, 20, 64, OBJ_106.getPreferredSize().height);

      //---- OBJ_107 ----
      OBJ_107.setText("Annulation");
      OBJ_107.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_107.setName("OBJ_107");
      OBJ_107.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_107ActionPerformed(e);
        }
      });
      xTitledPanel3ContentContainer.add(OBJ_107);
      OBJ_107.setBounds(60, 20, 90, OBJ_107.getPreferredSize().height);

      //---- OBJ_99 ----
      OBJ_99.setText("Quantit\u00e9 affect\u00e9e");
      OBJ_99.setName("OBJ_99");
      xTitledPanel3ContentContainer.add(OBJ_99);
      OBJ_99.setBounds(155, 34, 100, 20);

      //---- QTX04 ----
      QTX04.setName("QTX04");
      xTitledPanel3ContentContainer.add(QTX04);
      QTX04.setBounds(260, 30, 114, QTX04.getPreferredSize().height);

      //---- NUM04 ----
      NUM04.setName("NUM04");
      xTitledPanel3ContentContainer.add(NUM04);
      NUM04.setBounds(410, 30, 58, NUM04.getPreferredSize().height);

      //---- OBJ_102 ----
      OBJ_102.setText("Livraison");
      OBJ_102.setName("OBJ_102");
      xTitledPanel3ContentContainer.add(OBJ_102);
      OBJ_102.setBounds(560, 5, 55, OBJ_102.getPreferredSize().height);

      //---- ETA04 ----
      ETA04.setName("ETA04");
      xTitledPanel3ContentContainer.add(ETA04);
      ETA04.setBounds(690, 30, 42, ETA04.getPreferredSize().height);

      //---- OBJ_105 ----
      OBJ_105.setText("Etat");
      OBJ_105.setName("OBJ_105");
      xTitledPanel3ContentContainer.add(OBJ_105);
      OBJ_105.setBounds(690, 5, 27, OBJ_105.getPreferredSize().height);

      //---- OBJ_121 ----
      OBJ_121.setText("Ajout O.F.");
      OBJ_121.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_121.setName("OBJ_121");
      OBJ_121.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_121ActionPerformed(e);
        }
      });
      xTitledPanel3ContentContainer.add(OBJ_121);
      OBJ_121.setBounds(30, 50, 90, OBJ_121.getPreferredSize().height);

      //---- OBJ_115 ----
      OBJ_115.setText("Autres O.F.");
      OBJ_115.setName("OBJ_115");
      xTitledPanel3ContentContainer.add(OBJ_115);
      OBJ_115.setBounds(480, 34, 64, 20);

      //---- DLX04 ----
      DLX04.setName("DLX04");
      xTitledPanel3ContentContainer.add(DLX04);
      DLX04.setBounds(555, 30, 105, DLX04.getPreferredSize().height);

      //---- OBJ_101 ----
      OBJ_101.setText("@BOR_OF@");
      OBJ_101.setName("OBJ_101");
      xTitledPanel3ContentContainer.add(OBJ_101);
      OBJ_101.setBounds(410, 5, 105, OBJ_101.getPreferredSize().height);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
          Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = xTitledPanel3ContentContainer.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
        xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
      }
    }

    //---- OBJ_7 ----
    OBJ_7.setText("Affectation attendu sur stock");
    OBJ_7.setName("OBJ_7");
    OBJ_7.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_7ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie A1LIB;
  private RiZoneSortie L1ART;
  private JLabel OBJ_32;
  private RiZoneSortie L1NUM;
  private JLabel OBJ_35;
  private RiZoneSortie L1NLI;
  private JLabel OBJ_34;
  private JLabel OBJ_37;
  private RiZoneSortie E1MAG;
  private RiZoneSortie L1SUF;
  private JLabel OBJ_33;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_47;
  private JLabel OBJ_48;
  private RiZoneSortie WQTVX;
  private JLabel OBJ_49;
  private JLabel OBJ_55;
  private RiZoneSortie WQTSX;
  private RiZoneSortie L1KSV;
  private JLabel OBJ_54;
  private JLabel OBJ_52;
  private JLabel OBJ_59;
  private RiZoneSortie L1UNV;
  private RiZoneSortie L1UNS;
  private RiZoneSortie E1HOMX;
  private RiZoneSortie WDLPX;
  private JXTitledPanel xtp1;
  private SNBoutonLeger OBJ_68;
  private SNBoutonLeger OBJ_69;
  private JLabel OBJ_70;
  private XRiTextField QTX01;
  private JLabel OBJ_72;
  private JLabel OBJ_74;
  private XRiTextField WTP05;
  private JLabel OBJ_76;
  private RiZoneSortie L1SER;
  private XRiCalendrier DAX01;
  private JXTitledPanel xtp2;
  private SNBoutonLeger OBJ_84;
  private SNBoutonLeger OBJ_85;
  private JLabel OBJ_80;
  private RiZoneSortie QTX02;
  private RiZoneSortie NUM02;
  private JLabel OBJ_81;
  private RiZoneSortie NLI02;
  private JLabel OBJ_82;
  private RiZoneSortie SUF02;
  private RiZoneSortie ETA02;
  private JLabel OBJ_83;
  private RiZoneSortie DLX02;
  private JLabel OBJ_61;
  private JLabel OBJ_62;
  private JXTitledPanel xtp3;
  private SNBoutonLeger OBJ_109;
  private SNBoutonLeger OBJ_108;
  private JLabel OBJ_98;
  private RiZoneSortie QTX03;
  private RiZoneSortie NUM03;
  private RiZoneSortie SUF03;
  private RiZoneSortie NLI03;
  private JLabel OBJ_103;
  private RiZoneSortie ETA03;
  private JLabel OBJ_104;
  private RiZoneSortie DLX03;
  private JLabel OBJ_100;
  private JPanel panel3;
  private JLabel OBJ_124;
  private JLabel OBJ_136;
  private XRiTextField WRESX;
  private JLabel OBJ_129;
  private XRiTextField WAFFX;
  private JLabel OBJ_145;
  private JLabel OBJ_131;
  private XRiTextField WDISX;
  private JLabel OBJ_132;
  private XRiTextField WATTX;
  private JPanel panel5;
  private JLabel OBJ_123;
  private XRiTextField WSTKX;
  private JPanel panel4;
  private JLabel OBJ_137;
  private JLabel OBJ_126;
  private XRiTextField WQRTX;
  private JLabel OBJ_146;
  private JLabel OBJ_127;
  private XRiTextField WDISX3;
  private JLabel OBJ_130;
  private XRiTextField WCNRX;
  private XRiTextField WATTX3;
  private JLabel OBJ_133;
  private JPanel panel2;
  private JLabel OBJ_125;
  private JLabel OBJ_128;
  private JLabel OBJ_134;
  private JLabel OBJ_140;
  private JLabel OBJ_141;
  private JLabel OBJ_138;
  private XRiTextField WRESX2;
  private XRiTextField WATTX2;
  private XRiTextField WDISX2;
  private JLabel OBJ_135;
  private XRiCalendrier WDATTX;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JLabel OBJ_66;
  private JLabel OBJ_78;
  private JLabel OBJ_94;
  private JLabel OBJ_95;
  private JXTitledPanel xTitledPanel3;
  private JButton OBJ_106;
  private JButton OBJ_107;
  private JLabel OBJ_99;
  private XRiTextField QTX04;
  private XRiTextField NUM04;
  private JLabel OBJ_102;
  private XRiTextField ETA04;
  private JLabel OBJ_105;
  private JButton OBJ_121;
  private JLabel OBJ_115;
  private XRiCalendrier DLX04;
  private JLabel OBJ_101;
  private JMenuItem OBJ_7;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
