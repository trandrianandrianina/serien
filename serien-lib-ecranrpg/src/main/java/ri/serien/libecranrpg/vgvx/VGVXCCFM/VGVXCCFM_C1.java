
package ri.serien.libecranrpg.vgvx.VGVXCCFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVXCCFM_C1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXCCFM_C1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    CCREP5.setEnabled(lexique.isPresent("CCREP5"));
    CCREP4.setEnabled(lexique.isPresent("CCREP4"));
    CCREP3.setEnabled(lexique.isPresent("CCREP3"));
    CCREP2.setEnabled(lexique.isPresent("CCREP2"));
    CCREP1.setEnabled(lexique.isPresent("CCREP1"));
    CCZP5.setEnabled(lexique.isPresent("CCZP5"));
    CCZP4.setEnabled(lexique.isPresent("CCZP4"));
    CCZP3.setEnabled(lexique.isPresent("CCZP3"));
    CCZP2.setEnabled(lexique.isPresent("CCZP2"));
    CCZP1.setEnabled(lexique.isPresent("CCZP1"));
    CCCAT6.setEnabled(lexique.isPresent("CCCAT6"));
    CCCAT5.setEnabled(lexique.isPresent("CCCAT5"));
    CCCAT4.setEnabled(lexique.isPresent("CCCAT4"));
    CCCAT3.setEnabled(lexique.isPresent("CCCAT3"));
    CCCAT2.setEnabled(lexique.isPresent("CCCAT2"));
    CCCAT1.setEnabled(lexique.isPresent("CCCAT1"));
    CCCDPF.setEnabled(lexique.isPresent("CCCDPF"));
    CCCDPD.setEnabled(lexique.isPresent("CCCDPD"));
    CXAPE5.setEnabled(lexique.isPresent("CXAPE5"));
    CXAPE4.setEnabled(lexique.isPresent("CXAPE4"));
    CXAPE3.setEnabled(lexique.isPresent("CXAPE3"));
    CXAPE2.setEnabled(lexique.isPresent("CXAPE2"));
    CXAPE1.setEnabled(lexique.isPresent("CXAPE1"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Critères de sélection"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_ann = new RiSousMenu();
    riSousMenu_bt_ann = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_47 = new JLabel();
    OBJ_48 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_46 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_18 = new JLabel();
    CXAPE1 = new XRiTextField();
    CXAPE2 = new XRiTextField();
    CXAPE3 = new XRiTextField();
    CXAPE4 = new XRiTextField();
    CXAPE5 = new XRiTextField();
    OBJ_19 = new JLabel();
    CCCDPD = new XRiTextField();
    CCCDPF = new XRiTextField();
    CCCAT1 = new XRiTextField();
    CCCAT2 = new XRiTextField();
    CCCAT3 = new XRiTextField();
    CCCAT4 = new XRiTextField();
    CCCAT5 = new XRiTextField();
    CCCAT6 = new XRiTextField();
    CCZP1 = new XRiTextField();
    CCZP2 = new XRiTextField();
    CCZP3 = new XRiTextField();
    CCZP4 = new XRiTextField();
    CCZP5 = new XRiTextField();
    CCREP1 = new XRiTextField();
    CCREP2 = new XRiTextField();
    CCREP3 = new XRiTextField();
    CCREP4 = new XRiTextField();
    CCREP5 = new XRiTextField();
    OBJ_44 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(725, 255));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_ann ========
          {
            riSousMenu_ann.setName("riSousMenu_ann");

            //---- riSousMenu_bt_ann ----
            riSousMenu_bt_ann.setText("Annulation");
            riSousMenu_bt_ann.setToolTipText("Annulation");
            riSousMenu_bt_ann.setName("riSousMenu_bt_ann");
            riSousMenu_ann.add(riSousMenu_bt_ann);
          }
          menus_haut.add(riSousMenu_ann);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setToolTipText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Identification"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_47 ----
          OBJ_47.setText("Zone personnalis\u00e9e");
          OBJ_47.setName("OBJ_47");
          p_recup.add(OBJ_47);
          OBJ_47.setBounds(30, 138, 160, 20);

          //---- OBJ_48 ----
          OBJ_48.setText("Repr\u00e9sentant");
          OBJ_48.setName("OBJ_48");
          p_recup.add(OBJ_48);
          OBJ_48.setBounds(30, 169, 160, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- OBJ_46 ----
          OBJ_46.setText("Code APE");
          OBJ_46.setName("OBJ_46");
          p_recup.add(OBJ_46);
          OBJ_46.setBounds(30, 107, 160, 20);

          //---- OBJ_45 ----
          OBJ_45.setText("Cat\u00e9gorie");
          OBJ_45.setName("OBJ_45");
          p_recup.add(OBJ_45);
          OBJ_45.setBounds(30, 76, 160, 20);

          //---- OBJ_18 ----
          OBJ_18.setText("Code postal");
          OBJ_18.setName("OBJ_18");
          p_recup.add(OBJ_18);
          OBJ_18.setBounds(30, 45, 160, 20);

          //---- CXAPE1 ----
          CXAPE1.setComponentPopupMenu(BTD);
          CXAPE1.setName("CXAPE1");
          p_recup.add(CXAPE1);
          CXAPE1.setBounds(200, 103, 60, CXAPE1.getPreferredSize().height);

          //---- CXAPE2 ----
          CXAPE2.setComponentPopupMenu(BTD);
          CXAPE2.setName("CXAPE2");
          p_recup.add(CXAPE2);
          CXAPE2.setBounds(260, 103, 60, CXAPE2.getPreferredSize().height);

          //---- CXAPE3 ----
          CXAPE3.setComponentPopupMenu(BTD);
          CXAPE3.setName("CXAPE3");
          p_recup.add(CXAPE3);
          CXAPE3.setBounds(320, 103, 60, CXAPE3.getPreferredSize().height);

          //---- CXAPE4 ----
          CXAPE4.setComponentPopupMenu(BTD);
          CXAPE4.setName("CXAPE4");
          p_recup.add(CXAPE4);
          CXAPE4.setBounds(380, 103, 60, CXAPE4.getPreferredSize().height);

          //---- CXAPE5 ----
          CXAPE5.setComponentPopupMenu(BTD);
          CXAPE5.setName("CXAPE5");
          p_recup.add(CXAPE5);
          CXAPE5.setBounds(440, 103, 60, CXAPE5.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setText("D\u00e9but");
          OBJ_19.setName("OBJ_19");
          p_recup.add(OBJ_19);
          OBJ_19.setBounds(200, 45, 50, 20);

          //---- CCCDPD ----
          CCCDPD.setComponentPopupMenu(BTD);
          CCCDPD.setName("CCCDPD");
          p_recup.add(CCCDPD);
          CCCDPD.setBounds(252, 41, 50, CCCDPD.getPreferredSize().height);

          //---- CCCDPF ----
          CCCDPF.setComponentPopupMenu(BTD);
          CCCDPF.setName("CCCDPF");
          p_recup.add(CCCDPF);
          CCCDPF.setBounds(408, 41, 50, CCCDPF.getPreferredSize().height);

          //---- CCCAT1 ----
          CCCAT1.setComponentPopupMenu(BTD);
          CCCAT1.setName("CCCAT1");
          p_recup.add(CCCAT1);
          CCCAT1.setBounds(200, 72, 40, CCCAT1.getPreferredSize().height);

          //---- CCCAT2 ----
          CCCAT2.setComponentPopupMenu(BTD);
          CCCAT2.setName("CCCAT2");
          p_recup.add(CCCAT2);
          CCCAT2.setBounds(252, 72, 40, CCCAT2.getPreferredSize().height);

          //---- CCCAT3 ----
          CCCAT3.setComponentPopupMenu(BTD);
          CCCAT3.setName("CCCAT3");
          p_recup.add(CCCAT3);
          CCCAT3.setBounds(304, 72, 40, CCCAT3.getPreferredSize().height);

          //---- CCCAT4 ----
          CCCAT4.setComponentPopupMenu(BTD);
          CCCAT4.setName("CCCAT4");
          p_recup.add(CCCAT4);
          CCCAT4.setBounds(356, 72, 40, CCCAT4.getPreferredSize().height);

          //---- CCCAT5 ----
          CCCAT5.setComponentPopupMenu(BTD);
          CCCAT5.setName("CCCAT5");
          p_recup.add(CCCAT5);
          CCCAT5.setBounds(408, 72, 40, CCCAT5.getPreferredSize().height);

          //---- CCCAT6 ----
          CCCAT6.setComponentPopupMenu(BTD);
          CCCAT6.setName("CCCAT6");
          p_recup.add(CCCAT6);
          CCCAT6.setBounds(460, 72, 40, CCCAT6.getPreferredSize().height);

          //---- CCZP1 ----
          CCZP1.setComponentPopupMenu(BTD);
          CCZP1.setName("CCZP1");
          p_recup.add(CCZP1);
          CCZP1.setBounds(200, 134, 30, CCZP1.getPreferredSize().height);

          //---- CCZP2 ----
          CCZP2.setComponentPopupMenu(BTD);
          CCZP2.setName("CCZP2");
          p_recup.add(CCZP2);
          CCZP2.setBounds(260, 134, 30, CCZP2.getPreferredSize().height);

          //---- CCZP3 ----
          CCZP3.setComponentPopupMenu(BTD);
          CCZP3.setName("CCZP3");
          p_recup.add(CCZP3);
          CCZP3.setBounds(320, 134, 30, CCZP3.getPreferredSize().height);

          //---- CCZP4 ----
          CCZP4.setComponentPopupMenu(BTD);
          CCZP4.setName("CCZP4");
          p_recup.add(CCZP4);
          CCZP4.setBounds(380, 134, 30, CCZP4.getPreferredSize().height);

          //---- CCZP5 ----
          CCZP5.setComponentPopupMenu(BTD);
          CCZP5.setName("CCZP5");
          p_recup.add(CCZP5);
          CCZP5.setBounds(440, 134, 30, CCZP5.getPreferredSize().height);

          //---- CCREP1 ----
          CCREP1.setComponentPopupMenu(BTD);
          CCREP1.setName("CCREP1");
          p_recup.add(CCREP1);
          CCREP1.setBounds(200, 165, 30, CCREP1.getPreferredSize().height);

          //---- CCREP2 ----
          CCREP2.setComponentPopupMenu(BTD);
          CCREP2.setName("CCREP2");
          p_recup.add(CCREP2);
          CCREP2.setBounds(260, 165, 30, CCREP2.getPreferredSize().height);

          //---- CCREP3 ----
          CCREP3.setComponentPopupMenu(BTD);
          CCREP3.setName("CCREP3");
          p_recup.add(CCREP3);
          CCREP3.setBounds(320, 165, 30, CCREP3.getPreferredSize().height);

          //---- CCREP4 ----
          CCREP4.setComponentPopupMenu(BTD);
          CCREP4.setName("CCREP4");
          p_recup.add(CCREP4);
          CCREP4.setBounds(380, 165, 30, CCREP4.getPreferredSize().height);

          //---- CCREP5 ----
          CCREP5.setComponentPopupMenu(BTD);
          CCREP5.setName("CCREP5");
          p_recup.add(CCREP5);
          CCREP5.setBounds(440, 165, 30, CCREP5.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Fin");
          OBJ_44.setName("OBJ_44");
          p_recup.add(OBJ_44);
          OBJ_44.setBounds(356, 45, 51, 20);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(10, 10, 535, 235);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_ann;
  private RiSousMenu_bt riSousMenu_bt_ann;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_47;
  private JLabel OBJ_48;
  private JPanel P_PnlOpts;
  private JLabel OBJ_46;
  private JLabel OBJ_45;
  private JLabel OBJ_18;
  private XRiTextField CXAPE1;
  private XRiTextField CXAPE2;
  private XRiTextField CXAPE3;
  private XRiTextField CXAPE4;
  private XRiTextField CXAPE5;
  private JLabel OBJ_19;
  private XRiTextField CCCDPD;
  private XRiTextField CCCDPF;
  private XRiTextField CCCAT1;
  private XRiTextField CCCAT2;
  private XRiTextField CCCAT3;
  private XRiTextField CCCAT4;
  private XRiTextField CCCAT5;
  private XRiTextField CCCAT6;
  private XRiTextField CCZP1;
  private XRiTextField CCZP2;
  private XRiTextField CCZP3;
  private XRiTextField CCZP4;
  private XRiTextField CCZP5;
  private XRiTextField CCREP1;
  private XRiTextField CCREP2;
  private XRiTextField CCREP3;
  private XRiTextField CCREP4;
  private XRiTextField CCREP5;
  private JLabel OBJ_44;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
