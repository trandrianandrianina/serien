
package ri.serien.libecranrpg.vgvx.VGVX05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVX05FM_BX extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private int nbrLigneMax = 0;
  private int nbrColonneMax = 0;
  private String dernierVisible = null;
  private boolean visible = true;
  
  /**
   * Constructeur.
   */
  public VGVX05FM_BX(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    
    AVA.setIcon(lexique.chargerImage("images/avant.gif", true));
    RET.setIcon(lexique.chargerImage("images/arriere.gif", true));
    OBJ_34.setIcon(lexique.chargerImage("images/plann.gif", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    L01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L01@")).trim());
    L02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L02@")).trim());
    L03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L03@")).trim());
    L04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L04@")).trim());
    L05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L05@")).trim());
    L06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L06@")).trim());
    L07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L07@")).trim());
    L08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L08@")).trim());
    L10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L10@")).trim());
    L11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L11@")).trim());
    L12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L12@")).trim());
    L13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L13@")).trim());
    L14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L14@")).trim());
    L15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L15@")).trim());
    L09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L09@")).trim());
    T01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T01@")).trim());
    T02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T02@")).trim());
    T03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T03@")).trim());
    T04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T04@")).trim());
    T05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T05@")).trim());
    T06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T06@")).trim());
    T07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T07@")).trim());
    T08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T08@")).trim());
    T09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T09@")).trim());
    T10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T10@")).trim());
    T11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T11@")).trim());
    T12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T12@")).trim());
    T13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T13@")).trim());
    T14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T14@")).trim());
    T15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T15@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@INDART@ - @A1LIB@")).trim()));
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    navig_valid.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    riMenu2.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    riSousMenu6.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    riSousMenu_bt6.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    BTD.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    
    // Traitement des JTextArea
    miseEnForme(V01, "01");
    miseEnForme(V02, "02");
    miseEnForme(V03, "03");
    miseEnForme(V04, "04");
    miseEnForme(V05, "05");
    miseEnForme(V06, "06");
    miseEnForme(V07, "07");
    miseEnForme(V08, "08");
    miseEnForme(V09, "09");
    miseEnForme(V10, "10");
    miseEnForme(V11, "11");
    miseEnForme(V12, "12");
    miseEnForme(V13, "13");
    miseEnForme(V14, "14");
    miseEnForme(V15, "15");
    
    // Gestion visibilités
    SNBoutonDetail[] boutons = { bt01, bt02, bt03, bt04, bt05, bt06, bt07, bt08, bt09, bt10, bt11, bt12, bt13, bt14, bt15 };
    RiZoneSortie[] entetes = { T01, T02, T03, T04, T05, T06, T07, T08, T09, T10, T11, T12, T13, T14, T15 };
    XRiTextField[] valeurs = { V01, V02, V03, V04, V05, V06, V07, V08, V09, V10, V11, V12, V13, V14, V15 };
    RiZoneSortie[] libelles = { L01, L02, L03, L04, L05, L06, L07, L08, L09, L10, L11, L12, L13, L14, L15 };
    
    for (int i = 0; i < entetes.length; i++) {
      visible = !lexique.HostFieldGetData(entetes[i].getName()).trim().equals("");
      boutons[i].setVisible(visible && (lexique.getMode() != Lexical.MODE_CONSULTATION));
      entetes[i].setVisible(visible);
      valeurs[i].setVisible(visible);
      libelles[i].setVisible(visible);
      if (visible) {
        dernierVisible = entetes[i].getName();
      }
    }
    
    // Placement bouton
    // VAL.setBounds(819, (nbrLigneMax+190), 56, 40);
    
    // Taille de la popup
    this.setPreferredSize(new Dimension((1050), (nbrLigneMax + 50)));
    
    // Panel décors
    panel1.setBounds(5, 5, (875), (nbrLigneMax + 40));
    
    this.repaint();
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@DFTIT1@ @DFTIT2@"));
  }
  
  public int calculLongueur(String longueurDeBase, String typeDeZone) {
    if (longueurDeBase.trim().isEmpty()) {
      longueurDeBase = "0";
    }
    if (typeDeZone == "N") {
      return ((Integer.parseInt(longueurDeBase) - 1) * 8) + 20;
    }
    else if (typeDeZone == "D") {
      return 118;
    }
    else {
      return ((Integer.parseInt(longueurDeBase) - 1) * 10) + 20;
    }
  }
  
  public void miseEnForme(XRiTextField zoneSGM, String numeroZone) {
    int longueur = 0;
    int hauteur = V01.getHeight();
    int ligne = 0;
    int colonne = 0;
    if (lexique.isPresent("T" + numeroZone)) {
      ligne = Integer.parseInt(numeroZone) * 28;
      colonne = 240;
      longueur = calculLongueur(lexique.HostFieldGetData("LG" + numeroZone), lexique.HostFieldGetData("TP" + numeroZone));
      // formeZone = formeZone_value[Integer.parseInt(lexique.HostFieldGetData("XFD"+numeroZone))-1];
      if (lexique.HostFieldGetData("C" + numeroZone).equalsIgnoreCase("")) {
        zoneSGM.setBackground(Color.white);
        zoneSGM.setFocusable(true);
      }
      if (lexique.HostFieldGetData("C" + numeroZone).equalsIgnoreCase("2")) {
        zoneSGM.setBackground(new Color(224, 227, 233));
        zoneSGM.setFocusable(false);
      }
      if (lexique.HostFieldGetData("C" + numeroZone).equalsIgnoreCase("3")) {
        zoneSGM.setBackground(new Color(214, 217, 223));
        zoneSGM.setFocusable(false);
      }
      if (lexique.HostFieldGetData("C" + numeroZone).equalsIgnoreCase("4")) {
        zoneSGM.setForeground(Color.blue);
        zoneSGM.setBackground(new Color(214, 217, 223));
        zoneSGM.setFocusable(false);
      }
      if (lexique.HostFieldGetData("C" + numeroZone).equalsIgnoreCase("5")) {
        zoneSGM.setForeground(Color.red);
        zoneSGM.setBackground(new Color(214, 217, 223));
        zoneSGM.setFocusable(false);
      }
      zoneSGM.setBounds(colonne, ligne, longueur, hauteur);
      
      // zoneSGM.setBorder(formeZone);
      zoneSGM.setVisible(true);
      
      // stockage taille max
      if ((colonne + longueur) > nbrColonneMax) {
        nbrColonneMax = colonne + longueur;
      }
      if (ligne > nbrLigneMax) {
        nbrLigneMax = ligne;
      }
    }
    else {
      zoneSGM.setVisible(false);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void RETActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void AVAActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    if (Integer.parseInt(dernierVisible.substring(1, 3)) < 10) {
      dernierVisible = dernierVisible.substring(0, 1) + "0" + (Integer.parseInt(dernierVisible.substring(1, 3)) + 1);
    }
    else if (Integer.parseInt(dernierVisible.substring(1, 3)) < 15) {
      dernierVisible = dernierVisible.substring(0, 1) + (Integer.parseInt(dernierVisible.substring(1, 3)) + 1);
    }
    lexique.HostCursorPut(dernierVisible);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt01ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T01");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt02ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T02");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt03ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T03");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt04ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T04");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt05ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T05");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt06ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T06");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt07ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T07");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt08ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T08");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt09ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T09");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T10");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T11");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T12");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T13");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T14");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("T15");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    V01 = new XRiTextField();
    V02 = new XRiTextField();
    V03 = new XRiTextField();
    V04 = new XRiTextField();
    V05 = new XRiTextField();
    V06 = new XRiTextField();
    V07 = new XRiTextField();
    V08 = new XRiTextField();
    V10 = new XRiTextField();
    V11 = new XRiTextField();
    V12 = new XRiTextField();
    V13 = new XRiTextField();
    V14 = new XRiTextField();
    V15 = new XRiTextField();
    V09 = new XRiTextField();
    L01 = new RiZoneSortie();
    L02 = new RiZoneSortie();
    L03 = new RiZoneSortie();
    L04 = new RiZoneSortie();
    L05 = new RiZoneSortie();
    L06 = new RiZoneSortie();
    L07 = new RiZoneSortie();
    L08 = new RiZoneSortie();
    L10 = new RiZoneSortie();
    L11 = new RiZoneSortie();
    L12 = new RiZoneSortie();
    L13 = new RiZoneSortie();
    L14 = new RiZoneSortie();
    L15 = new RiZoneSortie();
    L09 = new RiZoneSortie();
    T01 = new RiZoneSortie();
    T02 = new RiZoneSortie();
    T03 = new RiZoneSortie();
    T04 = new RiZoneSortie();
    T05 = new RiZoneSortie();
    T06 = new RiZoneSortie();
    T07 = new RiZoneSortie();
    T08 = new RiZoneSortie();
    T09 = new RiZoneSortie();
    T10 = new RiZoneSortie();
    T11 = new RiZoneSortie();
    T12 = new RiZoneSortie();
    T13 = new RiZoneSortie();
    T14 = new RiZoneSortie();
    T15 = new RiZoneSortie();
    panel1 = new JPanel();
    bt01 = new SNBoutonDetail();
    bt02 = new SNBoutonDetail();
    bt03 = new SNBoutonDetail();
    bt04 = new SNBoutonDetail();
    bt05 = new SNBoutonDetail();
    bt06 = new SNBoutonDetail();
    bt07 = new SNBoutonDetail();
    bt08 = new SNBoutonDetail();
    bt09 = new SNBoutonDetail();
    bt10 = new SNBoutonDetail();
    bt11 = new SNBoutonDetail();
    bt12 = new SNBoutonDetail();
    bt13 = new SNBoutonDetail();
    bt14 = new SNBoutonDetail();
    bt15 = new SNBoutonDetail();
    panel2 = new JPanel();
    PHO = new JPanel();
    RET = new JButton();
    AVA = new JButton();
    OBJ_34 = new JButton();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("?BX.riSousMenu_bt_consult.toolTipText_2?");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("?BX.riSousMenu_bt_modif.toolTipText_2?");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("?BX.riSousMenu_bt_crea.toolTipText_2?");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("?BX.riSousMenu_bt_suppr.toolTipText_2?");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("?BX.riSousMenu_bt_dupli.toolTipText_2?");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("?BX.riSousMenu_bt_rappel.toolTipText_2?");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("?BX.riSousMenu_bt_reac.toolTipText_2?");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("?BX.riSousMenu_bt_destr.toolTipText_2?");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Ajouter une ligne");
              riSousMenu_bt6.setToolTipText("?BX.riSousMenu_bt6.toolTipText_2?");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- V01 ----
        V01.setComponentPopupMenu(BTD);
        V01.setName("V01");
        p_contenu.add(V01);
        V01.setBounds(245, 95, 222, V01.getPreferredSize().height);

        //---- V02 ----
        V02.setComponentPopupMenu(BTD);
        V02.setName("V02");
        p_contenu.add(V02);
        V02.setBounds(245, 123, 222, V02.getPreferredSize().height);

        //---- V03 ----
        V03.setComponentPopupMenu(BTD);
        V03.setName("V03");
        p_contenu.add(V03);
        V03.setBounds(245, 151, 222, V03.getPreferredSize().height);

        //---- V04 ----
        V04.setComponentPopupMenu(BTD);
        V04.setName("V04");
        p_contenu.add(V04);
        V04.setBounds(245, 179, 222, V04.getPreferredSize().height);

        //---- V05 ----
        V05.setComponentPopupMenu(BTD);
        V05.setName("V05");
        p_contenu.add(V05);
        V05.setBounds(245, 207, 222, V05.getPreferredSize().height);

        //---- V06 ----
        V06.setComponentPopupMenu(BTD);
        V06.setName("V06");
        p_contenu.add(V06);
        V06.setBounds(245, 235, 222, V06.getPreferredSize().height);

        //---- V07 ----
        V07.setComponentPopupMenu(BTD);
        V07.setName("V07");
        p_contenu.add(V07);
        V07.setBounds(245, 263, 222, V07.getPreferredSize().height);

        //---- V08 ----
        V08.setComponentPopupMenu(BTD);
        V08.setName("V08");
        p_contenu.add(V08);
        V08.setBounds(245, 291, 222, V08.getPreferredSize().height);

        //---- V10 ----
        V10.setComponentPopupMenu(BTD);
        V10.setName("V10");
        p_contenu.add(V10);
        V10.setBounds(245, 347, 222, V10.getPreferredSize().height);

        //---- V11 ----
        V11.setComponentPopupMenu(BTD);
        V11.setName("V11");
        p_contenu.add(V11);
        V11.setBounds(245, 375, 222, V11.getPreferredSize().height);

        //---- V12 ----
        V12.setComponentPopupMenu(BTD);
        V12.setName("V12");
        p_contenu.add(V12);
        V12.setBounds(245, 403, 222, V12.getPreferredSize().height);

        //---- V13 ----
        V13.setComponentPopupMenu(BTD);
        V13.setName("V13");
        p_contenu.add(V13);
        V13.setBounds(245, 431, 222, V13.getPreferredSize().height);

        //---- V14 ----
        V14.setComponentPopupMenu(BTD);
        V14.setName("V14");
        p_contenu.add(V14);
        V14.setBounds(245, 459, 222, V14.getPreferredSize().height);

        //---- V15 ----
        V15.setComponentPopupMenu(BTD);
        V15.setName("V15");
        p_contenu.add(V15);
        V15.setBounds(245, 487, 222, V15.getPreferredSize().height);

        //---- V09 ----
        V09.setComponentPopupMenu(BTD);
        V09.setName("V09");
        p_contenu.add(V09);
        V09.setBounds(245, 319, 222, V09.getPreferredSize().height);

        //---- L01 ----
        L01.setText("@L01@");
        L01.setName("L01");
        p_contenu.add(L01);
        L01.setBounds(560, 28, 300, L01.getPreferredSize().height);

        //---- L02 ----
        L02.setText("@L02@");
        L02.setName("L02");
        p_contenu.add(L02);
        L02.setBounds(560, 56, 300, L02.getPreferredSize().height);

        //---- L03 ----
        L03.setText("@L03@");
        L03.setName("L03");
        p_contenu.add(L03);
        L03.setBounds(560, 84, 300, L03.getPreferredSize().height);

        //---- L04 ----
        L04.setText("@L04@");
        L04.setName("L04");
        p_contenu.add(L04);
        L04.setBounds(560, 112, 300, L04.getPreferredSize().height);

        //---- L05 ----
        L05.setText("@L05@");
        L05.setName("L05");
        p_contenu.add(L05);
        L05.setBounds(560, 140, 300, L05.getPreferredSize().height);

        //---- L06 ----
        L06.setText("@L06@");
        L06.setName("L06");
        p_contenu.add(L06);
        L06.setBounds(560, 168, 300, L06.getPreferredSize().height);

        //---- L07 ----
        L07.setText("@L07@");
        L07.setName("L07");
        p_contenu.add(L07);
        L07.setBounds(560, 196, 300, L07.getPreferredSize().height);

        //---- L08 ----
        L08.setText("@L08@");
        L08.setName("L08");
        p_contenu.add(L08);
        L08.setBounds(560, 224, 300, L08.getPreferredSize().height);

        //---- L10 ----
        L10.setText("@L10@");
        L10.setName("L10");
        p_contenu.add(L10);
        L10.setBounds(560, 280, 300, L10.getPreferredSize().height);

        //---- L11 ----
        L11.setText("@L11@");
        L11.setName("L11");
        p_contenu.add(L11);
        L11.setBounds(560, 308, 300, L11.getPreferredSize().height);

        //---- L12 ----
        L12.setText("@L12@");
        L12.setName("L12");
        p_contenu.add(L12);
        L12.setBounds(560, 336, 300, L12.getPreferredSize().height);

        //---- L13 ----
        L13.setText("@L13@");
        L13.setName("L13");
        p_contenu.add(L13);
        L13.setBounds(560, 364, 300, L13.getPreferredSize().height);

        //---- L14 ----
        L14.setText("@L14@");
        L14.setName("L14");
        p_contenu.add(L14);
        L14.setBounds(560, 392, 300, L14.getPreferredSize().height);

        //---- L15 ----
        L15.setText("@L15@");
        L15.setName("L15");
        p_contenu.add(L15);
        L15.setBounds(560, 420, 300, L15.getPreferredSize().height);

        //---- L09 ----
        L09.setText("@L09@");
        L09.setName("L09");
        p_contenu.add(L09);
        L09.setBounds(560, 252, 300, L09.getPreferredSize().height);

        //---- T01 ----
        T01.setText("@T01@");
        T01.setComponentPopupMenu(BTD);
        T01.setName("T01");
        p_contenu.add(T01);
        T01.setBounds(15, 27, 210, 27);

        //---- T02 ----
        T02.setText("@T02@");
        T02.setComponentPopupMenu(BTD);
        T02.setName("T02");
        p_contenu.add(T02);
        T02.setBounds(15, 56, 210, 24);

        //---- T03 ----
        T03.setText("@T03@");
        T03.setComponentPopupMenu(BTD);
        T03.setName("T03");
        p_contenu.add(T03);
        T03.setBounds(15, 82, 210, 26);

        //---- T04 ----
        T04.setText("@T04@");
        T04.setComponentPopupMenu(BTD);
        T04.setName("T04");
        p_contenu.add(T04);
        T04.setBounds(15, 110, 210, 28);

        //---- T05 ----
        T05.setText("@T05@");
        T05.setComponentPopupMenu(BTD);
        T05.setName("T05");
        p_contenu.add(T05);
        T05.setBounds(15, 140, 210, 25);

        //---- T06 ----
        T06.setText("@T06@");
        T06.setComponentPopupMenu(BTD);
        T06.setName("T06");
        p_contenu.add(T06);
        T06.setBounds(15, 167, 210, 27);

        //---- T07 ----
        T07.setText("@T07@");
        T07.setComponentPopupMenu(BTD);
        T07.setName("T07");
        p_contenu.add(T07);
        T07.setBounds(15, 196, 210, 24);

        //---- T08 ----
        T08.setText("@T08@");
        T08.setComponentPopupMenu(BTD);
        T08.setName("T08");
        p_contenu.add(T08);
        T08.setBounds(15, 222, 210, 26);

        //---- T09 ----
        T09.setText("@T09@");
        T09.setComponentPopupMenu(BTD);
        T09.setName("T09");
        p_contenu.add(T09);
        T09.setBounds(15, 250, 210, 28);

        //---- T10 ----
        T10.setText("@T10@");
        T10.setComponentPopupMenu(BTD);
        T10.setName("T10");
        p_contenu.add(T10);
        T10.setBounds(15, 280, 210, 25);

        //---- T11 ----
        T11.setText("@T11@");
        T11.setComponentPopupMenu(BTD);
        T11.setName("T11");
        p_contenu.add(T11);
        T11.setBounds(15, 307, 210, 27);

        //---- T12 ----
        T12.setText("@T12@");
        T12.setComponentPopupMenu(BTD);
        T12.setName("T12");
        p_contenu.add(T12);
        T12.setBounds(15, 336, 210, 24);

        //---- T13 ----
        T13.setText("@T13@");
        T13.setComponentPopupMenu(BTD);
        T13.setName("T13");
        p_contenu.add(T13);
        T13.setBounds(15, 362, 210, 26);

        //---- T14 ----
        T14.setText("@T14@");
        T14.setComponentPopupMenu(BTD);
        T14.setName("T14");
        p_contenu.add(T14);
        T14.setBounds(15, 390, 210, 28);

        //---- T15 ----
        T15.setText("@T15@");
        T15.setComponentPopupMenu(BTD);
        T15.setName("T15");
        p_contenu.add(T15);
        T15.setBounds(15, 420, 210, 25);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("@INDART@ - @A1LIB@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(20, 445, 280, 115);

        //---- bt01 ----
        bt01.setToolTipText("Modifier ce param\u00e8tre");
        bt01.setName("bt01");
        bt01.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt01ActionPerformed(e);
          }
        });
        p_contenu.add(bt01);
        bt01.setBounds(225, 27, bt01.getPreferredSize().width, 27);

        //---- bt02 ----
        bt02.setToolTipText("Modifier ce param\u00e8tre");
        bt02.setName("bt02");
        bt02.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt02ActionPerformed(e);
          }
        });
        p_contenu.add(bt02);
        bt02.setBounds(225, 55, bt02.getPreferredSize().width, 27);

        //---- bt03 ----
        bt03.setToolTipText("Modifier ce param\u00e8tre");
        bt03.setName("bt03");
        bt03.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt03ActionPerformed(e);
          }
        });
        p_contenu.add(bt03);
        bt03.setBounds(225, 82, bt03.getPreferredSize().width, 27);

        //---- bt04 ----
        bt04.setToolTipText("Modifier ce param\u00e8tre");
        bt04.setName("bt04");
        bt04.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt04ActionPerformed(e);
          }
        });
        p_contenu.add(bt04);
        bt04.setBounds(225, 111, bt04.getPreferredSize().width, 27);

        //---- bt05 ----
        bt05.setToolTipText("Modifier ce param\u00e8tre");
        bt05.setName("bt05");
        bt05.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt05ActionPerformed(e);
          }
        });
        p_contenu.add(bt05);
        bt05.setBounds(225, 139, bt05.getPreferredSize().width, 27);

        //---- bt06 ----
        bt06.setToolTipText("Modifier ce param\u00e8tre");
        bt06.setName("bt06");
        bt06.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt06ActionPerformed(e);
          }
        });
        p_contenu.add(bt06);
        bt06.setBounds(225, 167, bt06.getPreferredSize().width, 27);

        //---- bt07 ----
        bt07.setToolTipText("Modifier ce param\u00e8tre");
        bt07.setName("bt07");
        bt07.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt07ActionPerformed(e);
          }
        });
        p_contenu.add(bt07);
        bt07.setBounds(225, 195, bt07.getPreferredSize().width, 27);

        //---- bt08 ----
        bt08.setToolTipText("Modifier ce param\u00e8tre");
        bt08.setName("bt08");
        bt08.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt08ActionPerformed(e);
          }
        });
        p_contenu.add(bt08);
        bt08.setBounds(225, 222, bt08.getPreferredSize().width, 27);

        //---- bt09 ----
        bt09.setToolTipText("Modifier ce param\u00e8tre");
        bt09.setName("bt09");
        bt09.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt09ActionPerformed(e);
          }
        });
        p_contenu.add(bt09);
        bt09.setBounds(225, 251, bt09.getPreferredSize().width, 27);

        //---- bt10 ----
        bt10.setToolTipText("Modifier ce param\u00e8tre");
        bt10.setName("bt10");
        bt10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt10ActionPerformed(e);
          }
        });
        p_contenu.add(bt10);
        bt10.setBounds(225, 279, bt10.getPreferredSize().width, 27);

        //---- bt11 ----
        bt11.setToolTipText("Modifier ce param\u00e8tre");
        bt11.setName("bt11");
        bt11.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt11ActionPerformed(e);
          }
        });
        p_contenu.add(bt11);
        bt11.setBounds(225, 307, bt11.getPreferredSize().width, 27);

        //---- bt12 ----
        bt12.setToolTipText("Modifier ce param\u00e8tre");
        bt12.setName("bt12");
        bt12.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt12ActionPerformed(e);
          }
        });
        p_contenu.add(bt12);
        bt12.setBounds(225, 335, bt12.getPreferredSize().width, 27);

        //---- bt13 ----
        bt13.setToolTipText("Modifier ce param\u00e8tre");
        bt13.setName("bt13");
        bt13.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt13ActionPerformed(e);
          }
        });
        p_contenu.add(bt13);
        bt13.setBounds(225, 362, bt13.getPreferredSize().width, 27);

        //---- bt14 ----
        bt14.setToolTipText("Modifier ce param\u00e8tre");
        bt14.setName("bt14");
        bt14.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt14ActionPerformed(e);
          }
        });
        p_contenu.add(bt14);
        bt14.setBounds(225, 391, bt14.getPreferredSize().width, 27);

        //---- bt15 ----
        bt15.setToolTipText("Modifier ce param\u00e8tre");
        bt15.setName("bt15");
        bt15.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt15ActionPerformed(e);
          }
        });
        p_contenu.add(bt15);
        bt15.setBounds(225, 419, bt15.getPreferredSize().width, 27);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== panel2 ========
    {
      panel2.setBorder(new TitledBorder("Photo de l'article"));
      panel2.setOpaque(false);
      panel2.setName("panel2");
      panel2.setLayout(null);

      //======== PHO ========
      {
        PHO.setName("PHO");
        PHO.setLayout(null);
      }
      panel2.add(PHO);
      PHO.setBounds(15, 25, 390, 225);

      //---- RET ----
      RET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      RET.setToolTipText("Photographie pr\u00e9c\u00e9dente");
      RET.setName("RET");
      RET.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          RETActionPerformed(e);
        }
      });
      panel2.add(RET);
      RET.setBounds(310, 265, 25, 18);

      //---- AVA ----
      AVA.setToolTipText("Photographie suivante");
      AVA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      AVA.setName("AVA");
      AVA.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          AVAActionPerformed(e);
        }
      });
      panel2.add(AVA);
      AVA.setBounds(340, 265, 25, 18);

      //---- OBJ_34 ----
      OBJ_34.setToolTipText("Agrandir cette photographie");
      OBJ_34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_34.setName("OBJ_34");
      panel2.add(OBJ_34);
      OBJ_34.setBounds(380, 255, 31, 26);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel2.getComponentCount(); i++) {
          Rectangle bounds = panel2.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel2.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel2.setMinimumSize(preferredSize);
        panel2.setPreferredSize(preferredSize);
      }
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private XRiTextField V01;
  private XRiTextField V02;
  private XRiTextField V03;
  private XRiTextField V04;
  private XRiTextField V05;
  private XRiTextField V06;
  private XRiTextField V07;
  private XRiTextField V08;
  private XRiTextField V10;
  private XRiTextField V11;
  private XRiTextField V12;
  private XRiTextField V13;
  private XRiTextField V14;
  private XRiTextField V15;
  private XRiTextField V09;
  private RiZoneSortie L01;
  private RiZoneSortie L02;
  private RiZoneSortie L03;
  private RiZoneSortie L04;
  private RiZoneSortie L05;
  private RiZoneSortie L06;
  private RiZoneSortie L07;
  private RiZoneSortie L08;
  private RiZoneSortie L10;
  private RiZoneSortie L11;
  private RiZoneSortie L12;
  private RiZoneSortie L13;
  private RiZoneSortie L14;
  private RiZoneSortie L15;
  private RiZoneSortie L09;
  private RiZoneSortie T01;
  private RiZoneSortie T02;
  private RiZoneSortie T03;
  private RiZoneSortie T04;
  private RiZoneSortie T05;
  private RiZoneSortie T06;
  private RiZoneSortie T07;
  private RiZoneSortie T08;
  private RiZoneSortie T09;
  private RiZoneSortie T10;
  private RiZoneSortie T11;
  private RiZoneSortie T12;
  private RiZoneSortie T13;
  private RiZoneSortie T14;
  private RiZoneSortie T15;
  private JPanel panel1;
  private SNBoutonDetail bt01;
  private SNBoutonDetail bt02;
  private SNBoutonDetail bt03;
  private SNBoutonDetail bt04;
  private SNBoutonDetail bt05;
  private SNBoutonDetail bt06;
  private SNBoutonDetail bt07;
  private SNBoutonDetail bt08;
  private SNBoutonDetail bt09;
  private SNBoutonDetail bt10;
  private SNBoutonDetail bt11;
  private SNBoutonDetail bt12;
  private SNBoutonDetail bt13;
  private SNBoutonDetail bt14;
  private SNBoutonDetail bt15;
  private JPanel panel2;
  private JPanel PHO;
  private JButton RET;
  private JButton AVA;
  private JButton OBJ_34;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
