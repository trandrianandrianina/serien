
package ri.serien.libecranrpg.vgvx.VGVX14FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX14FX_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] E1OPE_Value = { "", "E", "S", "T", "D", "I", "M", "X", };
  private String[] E1MAG2_Value = { "%P", "%R", "%E", "VP", "VR", "VE" };
  
  public VGVX14FX_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    E1OPE.setValeurs(E1OPE_Value, null);
    E1MAG2.setValeurs(E1MAG2_Value, null);
    
    snBarreBouton.ajouterBouton(EnumBouton.LIGNES, true);
    snBarreBouton.ajouterBouton(EnumBouton.RETOURNER_RECHERCHE, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Entête de bordereau de stock: @TITB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Code établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "E1ETB");
    snEtablissement.setEnabled(false);
    
    // Magasinier
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(snEtablissement.getIdSelection());
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "E1UTI");
    
    // Magasins
    pnlMagasinEmetteur.setVisible(lexique.isTrue("(N26) AND (N72)"));
    snMagasinEmetteur.setSession(getSession());
    snMagasinEmetteur.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinEmetteur.charger(false);
    snMagasinEmetteur.setSelectionParChampRPG(lexique, "E1MAG");
    // if (lexique.isTrue("N50")) {
    // snMagasinEmetteur.setEnabled(false);
    // }
    snMagasinEmetteur.setVisible(lexique.isTrue("(N26) AND (N72)"));
    lbMagasinEmetteur.setVisible(snMagasinEmetteur.isVisible());
    
    snMagasinRecepteur.setSession(getSession());
    snMagasinRecepteur.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinRecepteur.charger(false);
    snMagasinRecepteur.setSelectionParChampRPG(lexique, "E1MAG2");
    if (lexique.isTrue("N50")) {
      snMagasinRecepteur.setEnabled(false);
    }
    snMagasinRecepteur.setVisible(lexique.isTrue("(34)"));
    lbMagasinRecepteur.setVisible(snMagasinRecepteur.isVisible());
    
    snMagasinIntermediaire.setSession(getSession());
    snMagasinIntermediaire.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinIntermediaire.charger(false);
    snMagasinIntermediaire.setSelectionParChampRPG(lexique, "MAINT");
    snMagasinIntermediaire.setEnabled(false);
    snMagasinIntermediaire.setVisible(lexique.isTrue("(34)"));
    lbMagasinIntermédiare.setVisible(snMagasinIntermediaire.isVisible());
    
    pnlTransfert.setVisible(lexique.isTrue("(34)"));
    
    pnlDepreciation.setVisible(lexique.isTrue("(26)"));
    lbTypeDepreciation.setVisible(lexique.isTrue("(26)"));
    E1MAG2.setVisible(lexique.isTrue("(26)"));
    
    // Client
    pnlClientDepositaire.setVisible(lexique.isTrue("(N26) AND (N72) AND (71)"));
    snClientDepositaire.setSession(getSession());
    snClientDepositaire.setIdEtablissement(snEtablissement.getIdSelection());
    snClientDepositaire.charger(false);
    snClientDepositaire.setSelectionParChampRPG(lexique, "EMTTI1", "EMTTI2");
    snClientDepositaire.setVisible(lexique.isTrue("(N26) AND (N72) AND (71)"));
    lbClientDepositaire.setVisible(snClientDepositaire.isVisible());
    
    snClientTransfertDepositaire.setSession(getSession());
    snClientTransfertDepositaire.setIdEtablissement(snEtablissement.getIdSelection());
    snClientTransfertDepositaire.charger(false);
    snClientTransfertDepositaire.setSelectionParChampRPG(lexique, "TRSTI1", "TRSTI2");
    snClientTransfertDepositaire.setVisible(lexique.isTrue("(73) AND (34)"));
    lbClientTransfertDepositaire.setVisible(snClientTransfertDepositaire.isVisible());
    
    // Commentaire, Vendeur, Equipe
    
    if (lexique.isTrue("(N61) AND (N69)")) {
      lbObservation.setText("Commentaire");
    }
    if (lexique.isTrue("(61)")) {
      lbObservation.setText("Type de divers");
    }
    if (lexique.isTrue("(69)")) {
      lbObservation.setText("Equipe");
    }
    
    WOBS.setVisible(lexique.isTrue("(N61) AND (N69)"));
    WOBSD1.setVisible(lexique.isTrue("(61) OR (69)"));
    WOBSD3.setVisible(lexique.isTrue("(61) OR (69)"));
    WTDLIB.setVisible(lexique.isTrue("(61) AND (N69)"));
    


    
    
    
    lbDateTraitement.setVisible(WDTRTX.isVisible());
    // Inventaire
    lbDateComptage.setVisible(WDRMX.isVisible());
    lbComptageà.setVisible(WHRE.isVisible());
    lbHeureComptage.setVisible(WHRE.isVisible());
    lbMinuteComptage.setVisible(WMIN.isVisible());
    
    // date
    WDMINX.setVisible(lexique.isTrue("(N26) AND (19) AND (17) AND (N72)"));
    lbDateMini.setVisible(WDMINX.isVisible());
    WDMINX.setEnabled(false);
    WDMAXX.setVisible(lexique.isTrue("(N26) AND (19) AND (17) AND (N72)"));
    lbDateMaxi.setVisible(WDMAXX.isVisible());
    WDMAXX.setEnabled(false);
    
    if (lexique.HostFieldGetData("E1OPE").trim().equals("I")) {
      p_bpresentation.setText("Entête de bordereau d'inventaire");
    }
    else if (lexique.HostFieldGetData("E1OPE").trim().equals("R")) {
      p_bpresentation.setText("Entête de bordereau de dépréciation");
    }
    else {
      p_bpresentation.setText("Entête de bordereau de stock");
    }
    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    snEtablissement.renseignerChampRPG(lexique, "E1ETB");
    snMagasinEmetteur.renseignerChampRPG(lexique, "E1MAG");
    snMagasinRecepteur.renseignerChampRPG(lexique, "E1MAG2");
    snMagasinIntermediaire.renseignerChampRPG(lexique, "MAINT");
    snClientDepositaire.renseignerChampRPG(lexique, "EMTTI1", "EMTTI2");
    snClientTransfertDepositaire.renseignerChampRPG(lexique, "TRSTI1", "TRSTI2");
    
    if (lexique.isTrue("26")) {
      int idnex = E1MAG2.getSelectedIndex();
      String valeur = E1MAG2_Value[idnex];
      lexique.HostFieldPutData("E1MAG2", 0, valeur);
    }
    
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.LIGNES)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.RETOURNER_RECHERCHE)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    pnlPrincipal = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    sNPanelContenu1 = new SNPanelContenu();
    pnlInformations = new SNPanel();
    pnlGauche = new SNPanel();
    lbNumero = new SNLabelChamp();
    WNUM = new XRiTextField();
    pnlDroite = new SNPanel();
    lbE1ETB = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbMagasinier = new SNLabelChamp();
    snVendeur = new SNVendeur();
    pnlMagasinEmetteur = new SNPanel();
    lbMagasinEmetteur = new SNLabelChamp();
    snMagasinEmetteur = new SNMagasin();
    pnlOperation = new SNPanel();
    lbCodeOperation = new SNLabelChamp();
    E1OPE = new XRiComboBox();
    lbDateTraitement = new SNLabelChamp();
    WDTRTX = new XRiCalendrier();
    lbDateMini = new SNLabelChamp();
    WDMINX = new XRiCalendrier();
    lbDateMaxi = new SNLabelUnite();
    WDMAXX = new XRiCalendrier();
    pnlDateComptage = new SNPanel();
    lbDateComptage = new SNLabelChamp();
    WDRMX = new XRiCalendrier();
    lbComptageà = new SNLabelChamp();
    WHRE = new XRiTextField();
    lbHeureComptage = new SNLabelUnite();
    WMIN = new XRiTextField();
    lbMinuteComptage = new SNLabelUnite();
    pnlDepreciation = new SNPanel();
    lbTypeDepreciation = new SNLabelChamp();
    E1MAG2 = new XRiComboBox();
    pnlObservations = new SNPanel();
    lbObservation = new SNLabelChamp();
    WOBS = new XRiTextField();
    WOBSD1 = new XRiTextField();
    WOBSD3 = new XRiTextField();
    WTDLIB = new XRiTextField();
    pnlTransfert = new SNPanel();
    lbMagasinRecepteur = new SNLabelChamp();
    snMagasinRecepteur = new SNMagasin();
    lbMagasinIntermédiare = new SNLabelChamp();
    snMagasinIntermediaire = new SNMagasin();
    pnlClientDepositaire = new SNPanel();
    lbClientDepositaire = new SNLabelChamp();
    snClientDepositaire = new SNClient();
    lbClientTransfertDepositaire = new SNLabelChamp();
    snClientTransfertDepositaire = new SNClient();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1100, 460));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Ent\u00eate de bordereau de stock: @TITB@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setOpaque(false);
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      //---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);

      //======== sNPanelContenu1 ========
      {
        sNPanelContenu1.setName("sNPanelContenu1");
        sNPanelContenu1.setLayout(new GridBagLayout());
        ((GridBagLayout)sNPanelContenu1.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)sNPanelContenu1.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
        ((GridBagLayout)sNPanelContenu1.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)sNPanelContenu1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //======== pnlInformations ========
        {
          pnlInformations.setName("pnlInformations");
          pnlInformations.setLayout(new GridLayout(1, 2, 5, 0));

          //======== pnlGauche ========
          {
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbNumero ----
            lbNumero.setText("Num\u00e9ro");
            lbNumero.setMaximumSize(new Dimension(200, 30));
            lbNumero.setMinimumSize(new Dimension(200, 30));
            lbNumero.setPreferredSize(new Dimension(200, 30));
            lbNumero.setInheritsPopupMenu(false);
            lbNumero.setName("lbNumero");
            pnlGauche.add(lbNumero, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- WNUM ----
            WNUM.setComponentPopupMenu(null);
            WNUM.setMinimumSize(new Dimension(70, 30));
            WNUM.setMaximumSize(new Dimension(70, 30));
            WNUM.setPreferredSize(new Dimension(70, 30));
            WNUM.setFont(new Font("sansserif", Font.PLAIN, 14));
            WNUM.setName("WNUM");
            pnlGauche.add(WNUM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlInformations.add(pnlGauche);

          //======== pnlDroite ========
          {
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //---- lbE1ETB ----
            lbE1ETB.setText("Etablissement");
            lbE1ETB.setMaximumSize(new Dimension(200, 30));
            lbE1ETB.setMinimumSize(new Dimension(200, 30));
            lbE1ETB.setPreferredSize(new Dimension(200, 30));
            lbE1ETB.setInheritsPopupMenu(false);
            lbE1ETB.setName("lbE1ETB");
            pnlDroite.add(lbE1ETB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snEtablissement ----
            snEtablissement.setName("snEtablissement");
            pnlDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbMagasinier ----
            lbMagasinier.setText("Magasinier");
            lbMagasinier.setMaximumSize(new Dimension(200, 30));
            lbMagasinier.setMinimumSize(new Dimension(200, 30));
            lbMagasinier.setPreferredSize(new Dimension(200, 30));
            lbMagasinier.setInheritsPopupMenu(false);
            lbMagasinier.setName("lbMagasinier");
            pnlDroite.add(lbMagasinier, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snVendeur ----
            snVendeur.setName("snVendeur");
            pnlDroite.add(snVendeur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInformations.add(pnlDroite);
        }
        sNPanelContenu1.add(pnlInformations, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlMagasinEmetteur ========
        {
          pnlMagasinEmetteur.setName("pnlMagasinEmetteur");
          pnlMagasinEmetteur.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlMagasinEmetteur.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlMagasinEmetteur.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlMagasinEmetteur.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlMagasinEmetteur.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbMagasinEmetteur ----
          lbMagasinEmetteur.setText("Magasin \u00e9metteur");
          lbMagasinEmetteur.setPreferredSize(new Dimension(200, 30));
          lbMagasinEmetteur.setMaximumSize(new Dimension(200, 30));
          lbMagasinEmetteur.setMinimumSize(new Dimension(200, 30));
          lbMagasinEmetteur.setName("lbMagasinEmetteur");
          pnlMagasinEmetteur.add(lbMagasinEmetteur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snMagasinEmetteur ----
          snMagasinEmetteur.setPreferredSize(new Dimension(400, 30));
          snMagasinEmetteur.setMinimumSize(new Dimension(400, 30));
          snMagasinEmetteur.setMaximumSize(new Dimension(500, 30));
          snMagasinEmetteur.setName("snMagasinEmetteur");
          pnlMagasinEmetteur.add(snMagasinEmetteur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelContenu1.add(pnlMagasinEmetteur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlOperation ========
        {
          pnlOperation.setName("pnlOperation");
          pnlOperation.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlOperation.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlOperation.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlOperation.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlOperation.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbCodeOperation ----
          lbCodeOperation.setText("Op\u00e9ration");
          lbCodeOperation.setMaximumSize(new Dimension(200, 30));
          lbCodeOperation.setMinimumSize(new Dimension(200, 30));
          lbCodeOperation.setPreferredSize(new Dimension(200, 30));
          lbCodeOperation.setName("lbCodeOperation");
          pnlOperation.add(lbCodeOperation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- E1OPE ----
          E1OPE.setModel(new DefaultComboBoxModel(new String[] {
            "Tout type",
            "Entr\u00e9e en stock",
            "Sortie de stock",
            "Transfert",
            "Mouvements divers",
            "Inventaire homologable",
            "Stock mini",
            "Stock maxi"
          }));
          E1OPE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          E1OPE.setBackground(Color.white);
          E1OPE.setFont(new Font("sansserif", Font.PLAIN, 14));
          E1OPE.setPreferredSize(new Dimension(400, 30));
          E1OPE.setMinimumSize(new Dimension(400, 30));
          E1OPE.setMaximumSize(new Dimension(500, 30));
          E1OPE.setName("E1OPE");
          pnlOperation.add(E1OPE, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbDateTraitement ----
          lbDateTraitement.setText("en date du");
          lbDateTraitement.setMaximumSize(new Dimension(80, 30));
          lbDateTraitement.setMinimumSize(new Dimension(80, 30));
          lbDateTraitement.setPreferredSize(new Dimension(80, 30));
          lbDateTraitement.setName("lbDateTraitement");
          pnlOperation.add(lbDateTraitement, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WDTRTX ----
          WDTRTX.setMaximumSize(new Dimension(110, 30));
          WDTRTX.setMinimumSize(new Dimension(110, 30));
          WDTRTX.setPreferredSize(new Dimension(110, 30));
          WDTRTX.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDTRTX.setName("WDTRTX");
          pnlOperation.add(WDTRTX, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbDateMini ----
          lbDateMini.setText("Autoris\u00e9e du");
          lbDateMini.setPreferredSize(new Dimension(100, 30));
          lbDateMini.setMinimumSize(new Dimension(100, 30));
          lbDateMini.setInheritsPopupMenu(false);
          lbDateMini.setMaximumSize(new Dimension(100, 30));
          lbDateMini.setName("lbDateMini");
          pnlOperation.add(lbDateMini, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WDMINX ----
          WDMINX.setMaximumSize(new Dimension(110, 30));
          WDMINX.setMinimumSize(new Dimension(110, 30));
          WDMINX.setPreferredSize(new Dimension(110, 30));
          WDMINX.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDMINX.setName("WDMINX");
          pnlOperation.add(WDMINX, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbDateMaxi ----
          lbDateMaxi.setText("au");
          lbDateMaxi.setMaximumSize(new Dimension(30, 30));
          lbDateMaxi.setMinimumSize(new Dimension(30, 30));
          lbDateMaxi.setPreferredSize(new Dimension(30, 30));
          lbDateMaxi.setInheritsPopupMenu(false);
          lbDateMaxi.setHorizontalAlignment(SwingConstants.CENTER);
          lbDateMaxi.setName("lbDateMaxi");
          pnlOperation.add(lbDateMaxi, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WDMAXX ----
          WDMAXX.setMaximumSize(new Dimension(110, 30));
          WDMAXX.setMinimumSize(new Dimension(110, 30));
          WDMAXX.setPreferredSize(new Dimension(110, 30));
          WDMAXX.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDMAXX.setName("WDMAXX");
          pnlOperation.add(WDMAXX, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelContenu1.add(pnlOperation, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlDateComptage ========
        {
          pnlDateComptage.setName("pnlDateComptage");
          pnlDateComptage.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDateComptage.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlDateComptage.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlDateComptage.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlDateComptage.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbDateComptage ----
          lbDateComptage.setText("Date de comptage");
          lbDateComptage.setMaximumSize(new Dimension(200, 30));
          lbDateComptage.setMinimumSize(new Dimension(200, 30));
          lbDateComptage.setPreferredSize(new Dimension(200, 30));
          lbDateComptage.setName("lbDateComptage");
          pnlDateComptage.add(lbDateComptage, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WDRMX ----
          WDRMX.setMaximumSize(new Dimension(110, 30));
          WDRMX.setMinimumSize(new Dimension(110, 30));
          WDRMX.setPreferredSize(new Dimension(110, 30));
          WDRMX.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDRMX.setName("WDRMX");
          pnlDateComptage.add(WDRMX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbComptageà ----
          lbComptageà.setText("\u00e0");
          lbComptageà.setMaximumSize(new Dimension(25, 30));
          lbComptageà.setMinimumSize(new Dimension(25, 30));
          lbComptageà.setPreferredSize(new Dimension(25, 30));
          lbComptageà.setHorizontalAlignment(SwingConstants.CENTER);
          lbComptageà.setName("lbComptage\u00e0");
          pnlDateComptage.add(lbComptageà, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WHRE ----
          WHRE.setMaximumSize(new Dimension(28, 28));
          WHRE.setMinimumSize(new Dimension(28, 28));
          WHRE.setPreferredSize(new Dimension(28, 28));
          WHRE.setFont(new Font("sansserif", Font.PLAIN, 14));
          WHRE.setName("WHRE");
          pnlDateComptage.add(WHRE, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbHeureComptage ----
          lbHeureComptage.setText("heure");
          lbHeureComptage.setPreferredSize(new Dimension(60, 30));
          lbHeureComptage.setMaximumSize(new Dimension(30, 30));
          lbHeureComptage.setMinimumSize(new Dimension(60, 30));
          lbHeureComptage.setHorizontalAlignment(SwingConstants.LEFT);
          lbHeureComptage.setName("lbHeureComptage");
          pnlDateComptage.add(lbHeureComptage, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WMIN ----
          WMIN.setMaximumSize(new Dimension(28, 28));
          WMIN.setMinimumSize(new Dimension(28, 28));
          WMIN.setPreferredSize(new Dimension(28, 28));
          WMIN.setFont(new Font("sansserif", Font.PLAIN, 14));
          WMIN.setName("WMIN");
          pnlDateComptage.add(WMIN, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbMinuteComptage ----
          lbMinuteComptage.setText("minutes");
          lbMinuteComptage.setMaximumSize(new Dimension(60, 30));
          lbMinuteComptage.setMinimumSize(new Dimension(60, 30));
          lbMinuteComptage.setPreferredSize(new Dimension(60, 30));
          lbMinuteComptage.setName("lbMinuteComptage");
          pnlDateComptage.add(lbMinuteComptage, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelContenu1.add(pnlDateComptage, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlDepreciation ========
        {
          pnlDepreciation.setName("pnlDepreciation");
          pnlDepreciation.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDepreciation.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlDepreciation.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlDepreciation.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlDepreciation.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbTypeDepreciation ----
          lbTypeDepreciation.setText("Type de d\u00e9pr\u00e9ciation");
          lbTypeDepreciation.setMaximumSize(new Dimension(200, 30));
          lbTypeDepreciation.setMinimumSize(new Dimension(200, 30));
          lbTypeDepreciation.setPreferredSize(new Dimension(200, 30));
          lbTypeDepreciation.setName("lbTypeDepreciation");
          pnlDepreciation.add(lbTypeDepreciation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- E1MAG2 ----
          E1MAG2.setModel(new DefaultComboBoxModel(new String[] {
            "Pourcentage sur le PUMP",
            "Pourcentage sur le prix de revient",
            "Pourcentage sur le prix de derni\u00e8re entr\u00e9e",
            "Valeur sur le PUMP",
            "Valeur sur le prix de revient",
            "Valeur sur le prix de derni\u00e8re entr\u00e9e"
          }));
          E1MAG2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          E1MAG2.setBackground(Color.white);
          E1MAG2.setFont(new Font("sansserif", Font.PLAIN, 14));
          E1MAG2.setPreferredSize(new Dimension(400, 30));
          E1MAG2.setMinimumSize(new Dimension(400, 30));
          E1MAG2.setMaximumSize(new Dimension(500, 30));
          E1MAG2.setName("E1MAG2");
          pnlDepreciation.add(E1MAG2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelContenu1.add(pnlDepreciation, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlObservations ========
        {
          pnlObservations.setName("pnlObservations");
          pnlObservations.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlObservations.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlObservations.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlObservations.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlObservations.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbObservation ----
          lbObservation.setMaximumSize(new Dimension(200, 30));
          lbObservation.setMinimumSize(new Dimension(200, 30));
          lbObservation.setPreferredSize(new Dimension(200, 30));
          lbObservation.setText("lbOservation");
          lbObservation.setName("lbObservation");
          pnlObservations.add(lbObservation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WOBS ----
          WOBS.setToolTipText("r\u00e9percut\u00e9 sur les lignes donc visible dans les mouvements de stock  ");
          WOBS.setMaximumSize(new Dimension(250, 28));
          WOBS.setMinimumSize(new Dimension(250, 28));
          WOBS.setPreferredSize(new Dimension(250, 30));
          WOBS.setFont(new Font("sansserif", Font.PLAIN, 14));
          WOBS.setName("WOBS");
          pnlObservations.add(WOBS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WOBSD1 ----
          WOBSD1.setMaximumSize(new Dimension(40, 28));
          WOBSD1.setMinimumSize(new Dimension(40, 28));
          WOBSD1.setPreferredSize(new Dimension(40, 28));
          WOBSD1.setFont(new Font("sansserif", Font.PLAIN, 14));
          WOBSD1.setName("WOBSD1");
          pnlObservations.add(WOBSD1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WOBSD3 ----
          WOBSD3.setMaximumSize(new Dimension(210, 28));
          WOBSD3.setMinimumSize(new Dimension(210, 28));
          WOBSD3.setPreferredSize(new Dimension(210, 28));
          WOBSD3.setFont(new Font("sansserif", Font.PLAIN, 14));
          WOBSD3.setName("WOBSD3");
          pnlObservations.add(WOBSD3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WTDLIB ----
          WTDLIB.setMaximumSize(new Dimension(310, 28));
          WTDLIB.setMinimumSize(new Dimension(310, 28));
          WTDLIB.setPreferredSize(new Dimension(310, 28));
          WTDLIB.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTDLIB.setName("WTDLIB");
          pnlObservations.add(WTDLIB, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelContenu1.add(pnlObservations, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlTransfert ========
        {
          pnlTransfert.setName("pnlTransfert");
          pnlTransfert.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlTransfert.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlTransfert.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlTransfert.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlTransfert.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbMagasinRecepteur ----
          lbMagasinRecepteur.setText(" Magasin r\u00e9cepteur");
          lbMagasinRecepteur.setMaximumSize(new Dimension(200, 30));
          lbMagasinRecepteur.setMinimumSize(new Dimension(200, 30));
          lbMagasinRecepteur.setPreferredSize(new Dimension(200, 30));
          lbMagasinRecepteur.setName("lbMagasinRecepteur");
          pnlTransfert.add(lbMagasinRecepteur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snMagasinRecepteur ----
          snMagasinRecepteur.setPreferredSize(new Dimension(400, 30));
          snMagasinRecepteur.setMinimumSize(new Dimension(400, 30));
          snMagasinRecepteur.setMaximumSize(new Dimension(500, 30));
          snMagasinRecepteur.setName("snMagasinRecepteur");
          pnlTransfert.add(snMagasinRecepteur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbMagasinIntermédiare ----
          lbMagasinIntermédiare.setText(" Magasin interm\u00e9diare");
          lbMagasinIntermédiare.setMaximumSize(new Dimension(200, 30));
          lbMagasinIntermédiare.setMinimumSize(new Dimension(200, 30));
          lbMagasinIntermédiare.setPreferredSize(new Dimension(200, 30));
          lbMagasinIntermédiare.setName("lbMagasinInterm\u00e9diare");
          pnlTransfert.add(lbMagasinIntermédiare, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snMagasinIntermediaire ----
          snMagasinIntermediaire.setPreferredSize(new Dimension(400, 30));
          snMagasinIntermediaire.setMinimumSize(new Dimension(400, 30));
          snMagasinIntermediaire.setMaximumSize(new Dimension(500, 30));
          snMagasinIntermediaire.setName("snMagasinIntermediaire");
          pnlTransfert.add(snMagasinIntermediaire, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelContenu1.add(pnlTransfert, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlClientDepositaire ========
        {
          pnlClientDepositaire.setName("pnlClientDepositaire");
          pnlClientDepositaire.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlClientDepositaire.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlClientDepositaire.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlClientDepositaire.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlClientDepositaire.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbClientDepositaire ----
          lbClientDepositaire.setText("Client");
          lbClientDepositaire.setMaximumSize(new Dimension(200, 30));
          lbClientDepositaire.setMinimumSize(new Dimension(200, 30));
          lbClientDepositaire.setPreferredSize(new Dimension(200, 30));
          lbClientDepositaire.setName("lbClientDepositaire");
          pnlClientDepositaire.add(lbClientDepositaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snClientDepositaire ----
          snClientDepositaire.setName("snClientDepositaire");
          pnlClientDepositaire.add(snClientDepositaire, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbClientTransfertDepositaire ----
          lbClientTransfertDepositaire.setText("Client");
          lbClientTransfertDepositaire.setName("lbClientTransfertDepositaire");
          pnlClientDepositaire.add(lbClientTransfertDepositaire, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snClientTransfertDepositaire ----
          snClientTransfertDepositaire.setName("snClientTransfertDepositaire");
          pnlClientDepositaire.add(snClientTransfertDepositaire, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelContenu1.add(pnlClientDepositaire, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(sNPanelContenu1, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNPanel pnlPrincipal;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelContenu1;
  private SNPanel pnlInformations;
  private SNPanel pnlGauche;
  private SNLabelChamp lbNumero;
  private XRiTextField WNUM;
  private SNPanel pnlDroite;
  private SNLabelChamp lbE1ETB;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbMagasinier;
  private SNVendeur snVendeur;
  private SNPanel pnlMagasinEmetteur;
  private SNLabelChamp lbMagasinEmetteur;
  private SNMagasin snMagasinEmetteur;
  private SNPanel pnlOperation;
  private SNLabelChamp lbCodeOperation;
  private XRiComboBox E1OPE;
  private SNLabelChamp lbDateTraitement;
  private XRiCalendrier WDTRTX;
  private SNLabelChamp lbDateMini;
  private XRiCalendrier WDMINX;
  private SNLabelUnite lbDateMaxi;
  private XRiCalendrier WDMAXX;
  private SNPanel pnlDateComptage;
  private SNLabelChamp lbDateComptage;
  private XRiCalendrier WDRMX;
  private SNLabelChamp lbComptageà;
  private XRiTextField WHRE;
  private SNLabelUnite lbHeureComptage;
  private XRiTextField WMIN;
  private SNLabelUnite lbMinuteComptage;
  private SNPanel pnlDepreciation;
  private SNLabelChamp lbTypeDepreciation;
  private XRiComboBox E1MAG2;
  private SNPanel pnlObservations;
  private SNLabelChamp lbObservation;
  private XRiTextField WOBS;
  private XRiTextField WOBSD1;
  private XRiTextField WOBSD3;
  private XRiTextField WTDLIB;
  private SNPanel pnlTransfert;
  private SNLabelChamp lbMagasinRecepteur;
  private SNMagasin snMagasinRecepteur;
  private SNLabelChamp lbMagasinIntermédiare;
  private SNMagasin snMagasinIntermediaire;
  private SNPanel pnlClientDepositaire;
  private SNLabelChamp lbClientDepositaire;
  private SNClient snClientDepositaire;
  private SNLabelChamp lbClientTransfertDepositaire;
  private SNClient snClientTransfertDepositaire;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
