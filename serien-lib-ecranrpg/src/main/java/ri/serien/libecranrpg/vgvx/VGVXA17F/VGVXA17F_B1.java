
package ri.serien.libecranrpg.vgvx.VGVXA17F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGVXA17F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXA17F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LSL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL1@")).trim());
    LSL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL2@")).trim());
    LSL3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL3@")).trim());
    LSL4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSL4@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    String titres = lexique.HostFieldGetData("HLD02");
    LSL1.setText(titres.substring(0, 7));
    LSL2.setText(titres.substring(8, 15));
    LSL3.setText(titres.substring(16, 23));
    LSL4.setText(titres.substring(24, 31));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  CONDITIONS DE STOCKAGE"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_31 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_41 = new JLabel();
    LSL1 = new JLabel();
    LSL2 = new JLabel();
    LSL3 = new JLabel();
    LSL4 = new JLabel();
    C0AD1 = new XRiTextField();
    C0AD2 = new XRiTextField();
    C0AD3 = new XRiTextField();
    C0AD4 = new XRiTextField();
    C0AF1 = new XRiTextField();
    C0AF2 = new XRiTextField();
    C0AF3 = new XRiTextField();
    C0AF4 = new XRiTextField();
    C0RD1 = new XRiTextField();
    C0RD2 = new XRiTextField();
    C0RD3 = new XRiTextField();
    C0RD4 = new XRiTextField();
    C0RF1 = new XRiTextField();
    C0RF2 = new XRiTextField();
    C0RF3 = new XRiTextField();
    C0RF4 = new XRiTextField();
    OBJ_42 = new JLabel();
    OBJ_47 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(800, 300));
    setPreferredSize(new Dimension(800, 300));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Conditions de stockage"));
          panel1.setPreferredSize(new Dimension(800, 300));
          panel1.setMinimumSize(new Dimension(800, 300));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_31 ----
          OBJ_31.setText("Adresse de d\u00e9but");
          OBJ_31.setFont(OBJ_31.getFont().deriveFont(OBJ_31.getFont().getStyle() | Font.BOLD));
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(30, 70, 130, 28);

          //---- OBJ_36 ----
          OBJ_36.setText("Adresse de fin");
          OBJ_36.setFont(OBJ_36.getFont().deriveFont(OBJ_36.getFont().getStyle() | Font.BOLD));
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(30, 120, 130, 28);

          //---- OBJ_41 ----
          OBJ_41.setText("Restrictions");
          OBJ_41.setFont(OBJ_41.getFont().deriveFont(OBJ_41.getFont().getStyle() | Font.BOLD));
          OBJ_41.setName("OBJ_41");
          panel1.add(OBJ_41);
          OBJ_41.setBounds(30, 165, 90, 28);

          //---- LSL1 ----
          LSL1.setText("@LSL1@");
          LSL1.setFont(LSL1.getFont().deriveFont(LSL1.getFont().getStyle() | Font.BOLD));
          LSL1.setHorizontalAlignment(SwingConstants.CENTER);
          LSL1.setName("LSL1");
          panel1.add(LSL1);
          LSL1.setBounds(151, 45, 58, 25);

          //---- LSL2 ----
          LSL2.setText("@LSL2@");
          LSL2.setFont(LSL2.getFont().deriveFont(LSL2.getFont().getStyle() | Font.BOLD));
          LSL2.setHorizontalAlignment(SwingConstants.CENTER);
          LSL2.setName("LSL2");
          panel1.add(LSL2);
          LSL2.setBounds(246, 45, 58, 25);

          //---- LSL3 ----
          LSL3.setText("@LSL3@");
          LSL3.setFont(LSL3.getFont().deriveFont(LSL3.getFont().getStyle() | Font.BOLD));
          LSL3.setHorizontalAlignment(SwingConstants.CENTER);
          LSL3.setName("LSL3");
          panel1.add(LSL3);
          LSL3.setBounds(341, 45, 58, 25);

          //---- LSL4 ----
          LSL4.setText("@LSL4@");
          LSL4.setFont(LSL4.getFont().deriveFont(LSL4.getFont().getStyle() | Font.BOLD));
          LSL4.setHorizontalAlignment(SwingConstants.CENTER);
          LSL4.setName("LSL4");
          panel1.add(LSL4);
          LSL4.setBounds(436, 45, 58, 25);

          //---- C0AD1 ----
          C0AD1.setComponentPopupMenu(BTD);
          C0AD1.setName("C0AD1");
          panel1.add(C0AD1);
          C0AD1.setBounds(160, 70, 40, C0AD1.getPreferredSize().height);

          //---- C0AD2 ----
          C0AD2.setComponentPopupMenu(BTD);
          C0AD2.setName("C0AD2");
          panel1.add(C0AD2);
          C0AD2.setBounds(255, 70, 40, 28);

          //---- C0AD3 ----
          C0AD3.setComponentPopupMenu(BTD);
          C0AD3.setName("C0AD3");
          panel1.add(C0AD3);
          C0AD3.setBounds(350, 70, 40, 28);

          //---- C0AD4 ----
          C0AD4.setComponentPopupMenu(BTD);
          C0AD4.setName("C0AD4");
          panel1.add(C0AD4);
          C0AD4.setBounds(445, 70, 40, 28);

          //---- C0AF1 ----
          C0AF1.setComponentPopupMenu(BTD);
          C0AF1.setName("C0AF1");
          panel1.add(C0AF1);
          C0AF1.setBounds(160, 120, 40, C0AF1.getPreferredSize().height);

          //---- C0AF2 ----
          C0AF2.setComponentPopupMenu(BTD);
          C0AF2.setName("C0AF2");
          panel1.add(C0AF2);
          C0AF2.setBounds(255, 120, 40, 28);

          //---- C0AF3 ----
          C0AF3.setComponentPopupMenu(BTD);
          C0AF3.setName("C0AF3");
          panel1.add(C0AF3);
          C0AF3.setBounds(350, 120, 40, 28);

          //---- C0AF4 ----
          C0AF4.setComponentPopupMenu(BTD);
          C0AF4.setName("C0AF4");
          panel1.add(C0AF4);
          C0AF4.setBounds(445, 120, 40, 28);

          //---- C0RD1 ----
          C0RD1.setComponentPopupMenu(BTD);
          C0RD1.setName("C0RD1");
          panel1.add(C0RD1);
          C0RD1.setBounds(160, 165, 40, C0RD1.getPreferredSize().height);

          //---- C0RD2 ----
          C0RD2.setComponentPopupMenu(BTD);
          C0RD2.setName("C0RD2");
          panel1.add(C0RD2);
          C0RD2.setBounds(255, 165, 40, 28);

          //---- C0RD3 ----
          C0RD3.setComponentPopupMenu(BTD);
          C0RD3.setName("C0RD3");
          panel1.add(C0RD3);
          C0RD3.setBounds(350, 165, 40, 28);

          //---- C0RD4 ----
          C0RD4.setComponentPopupMenu(BTD);
          C0RD4.setName("C0RD4");
          panel1.add(C0RD4);
          C0RD4.setBounds(445, 165, 40, 28);

          //---- C0RF1 ----
          C0RF1.setComponentPopupMenu(BTD);
          C0RF1.setName("C0RF1");
          panel1.add(C0RF1);
          C0RF1.setBounds(160, 215, 40, C0RF1.getPreferredSize().height);

          //---- C0RF2 ----
          C0RF2.setComponentPopupMenu(BTD);
          C0RF2.setName("C0RF2");
          panel1.add(C0RF2);
          C0RF2.setBounds(255, 215, 40, 28);

          //---- C0RF3 ----
          C0RF3.setComponentPopupMenu(BTD);
          C0RF3.setName("C0RF3");
          panel1.add(C0RF3);
          C0RF3.setBounds(350, 215, 40, 28);

          //---- C0RF4 ----
          C0RF4.setComponentPopupMenu(BTD);
          C0RF4.setName("C0RF4");
          panel1.add(C0RF4);
          C0RF4.setBounds(445, 215, 40, 28);

          //---- OBJ_42 ----
          OBJ_42.setText("de");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(130, 165, 30, 28);

          //---- OBJ_47 ----
          OBJ_47.setText("\u00e0");
          OBJ_47.setName("OBJ_47");
          panel1.add(OBJ_47);
          OBJ_47.setBounds(130, 215, 30, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 610, 280);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_31;
  private JLabel OBJ_36;
  private JLabel OBJ_41;
  private JLabel LSL1;
  private JLabel LSL2;
  private JLabel LSL3;
  private JLabel LSL4;
  private XRiTextField C0AD1;
  private XRiTextField C0AD2;
  private XRiTextField C0AD3;
  private XRiTextField C0AD4;
  private XRiTextField C0AF1;
  private XRiTextField C0AF2;
  private XRiTextField C0AF3;
  private XRiTextField C0AF4;
  private XRiTextField C0RD1;
  private XRiTextField C0RD2;
  private XRiTextField C0RD3;
  private XRiTextField C0RD4;
  private XRiTextField C0RF1;
  private XRiTextField C0RF2;
  private XRiTextField C0RF3;
  private XRiTextField C0RF4;
  private JLabel OBJ_42;
  private JLabel OBJ_47;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
