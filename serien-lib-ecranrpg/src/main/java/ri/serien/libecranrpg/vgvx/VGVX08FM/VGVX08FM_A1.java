
package ri.serien.libecranrpg.vgvx.VGVX08FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX08FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX08FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX1.setValeurs("1", "RB");
    TIDX9.setValeurs("9", "RB");
    TIDX11.setValeurs("11", "RB");
    TIDX10.setValeurs("10", "RB");
    TIDX8.setValeurs("8", "RB");
    TIDX7.setValeurs("7", "RB");
    TIDX6.setValeurs("6", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX12.setValeurs("12", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX2.setValeurs("2", "RB");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    ERRRECH.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERRRECH@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P8RES@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_71.setVisible(!lexique.HostFieldGetData("ERRRECH").trim().equalsIgnoreCase(""));
    OBJ_23.setVisible(lexique.isPresent("P8RES"));
    ERRRECH.setVisible(!lexique.HostFieldGetData("ERRRECH").trim().equalsIgnoreCase(""));
    OBJ_70.setVisible(!lexique.HostFieldGetData("ERRRECH").trim().equalsIgnoreCase(""));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("RECHERCHE ARTICLE"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    

    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(7, 62);
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(10, 76);
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
 
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BT4.getInvoker().getName());
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BT4.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_70 = new XRiTextField();
    ERRRECH = new JLabel();
    TIDX2 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    TIDX12 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    TIDX10 = new XRiRadioButton();
    TIDX11 = new XRiRadioButton();
    TIDX9 = new XRiRadioButton();
    OBJ_64 = new JLabel();
    TIDX1 = new XRiRadioButton();
    ARG1 = new XRiTextField();
    ARG2 = new XRiTextField();
    ARG3 = new XRiTextField();
    ARG6 = new XRiTextField();
    ARG8 = new XRiTextField();
    ARG10 = new XRiTextField();
    ARG11 = new XRiTextField();
    ARG9 = new XRiTextField();
    OBJ_42 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_71 = new JLabel();
    ARG12 = new XRiTextField();
    ARG5 = new XRiTextField();
    ARG7 = new XRiTextField();
    ARG4 = new XRiTextField();
    ARGZP1 = new XRiTextField();
    ARGZP2 = new XRiTextField();
    ARGZP3 = new XRiTextField();
    ARGZP4 = new XRiTextField();
    ARGZP5 = new XRiTextField();
    TIDX1_CHK = new JCheckBox();
    TIDX2_CHK = new JCheckBox();
    TIDX2_CHK2 = new JCheckBox();
    TIDX3_CHK = new JCheckBox();
    TIDX4_CHK = new JCheckBox();
    TIDX5_CHK = new JCheckBox();
    TIDX6_CHK = new JCheckBox();
    TIDX7_CHK = new JCheckBox();
    TIDX8_CHK = new JCheckBox();
    TIDX10_CHK = new JCheckBox();
    TIDX11_CHK = new JCheckBox();
    TIDX9_CHK = new JCheckBox();
    WEXT = new XRiTextField();
    ARGGP = new XRiTextField();
    OBJ_23 = new JLabel();
    RB = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    BT4 = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_15 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(690, 515));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Autres crit\u00e8res");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Restriction de recherche");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Scan libell\u00e9s");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Disponibles ou attendus");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Articles d\u00e9sactiv\u00e9s");
              riSousMenu_bt10.setToolTipText("Mise en/hors fonction affichage des articles d\u00e9sactiv\u00e9s");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_70 ----
          OBJ_70.setName("OBJ_70");
          panel2.add(OBJ_70);
          OBJ_70.setBounds(30, 445, 364, OBJ_70.getPreferredSize().height);

          //---- ERRRECH ----
          ERRRECH.setText("@ERRRECH@");
          ERRRECH.setName("ERRRECH");
          panel2.add(ERRRECH);
          ERRRECH.setBounds(105, 451, 289, 16);

          //---- TIDX2 ----
          TIDX2.setText("Recherche alphab\u00e9tique");
          TIDX2.setComponentPopupMenu(BTD);
          TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2.setName("TIDX2");
          panel2.add(TIDX2);
          TIDX2.setBounds(30, 103, 214, 20);

          //---- TIDX3 ----
          TIDX3.setText("Mot de classement");
          TIDX3.setComponentPopupMenu(BTD);
          TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX3.setName("TIDX3");
          panel2.add(TIDX3);
          TIDX3.setBounds(30, 131, 214, 20);

          //---- TIDX4 ----
          TIDX4.setText("Groupe, Famille");
          TIDX4.setComponentPopupMenu(BTD);
          TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX4.setName("TIDX4");
          panel2.add(TIDX4);
          TIDX4.setBounds(30, 159, 214, 20);

          //---- TIDX12 ----
          TIDX12.setText("Sous-Famille");
          TIDX12.setComponentPopupMenu(BTD);
          TIDX12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX12.setName("TIDX12");
          panel2.add(TIDX12);
          TIDX12.setBounds(30, 187, 214, 20);

          //---- TIDX5 ----
          TIDX5.setText("R\u00e9f\u00e9rence tarif");
          TIDX5.setComponentPopupMenu(BTD);
          TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX5.setName("TIDX5");
          panel2.add(TIDX5);
          TIDX5.setBounds(30, 215, 214, 20);

          //---- TIDX6 ----
          TIDX6.setText("Mot de classement fournisseur");
          TIDX6.setComponentPopupMenu(BTD);
          TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX6.setName("TIDX6");
          panel2.add(TIDX6);
          TIDX6.setBounds(30, 243, 214, 20);

          //---- TIDX7 ----
          TIDX7.setText("Num\u00e9ro fournisseur");
          TIDX7.setComponentPopupMenu(BTD);
          TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX7.setName("TIDX7");
          panel2.add(TIDX7);
          TIDX7.setBounds(30, 271, 214, 20);

          //---- TIDX8 ----
          TIDX8.setText("R\u00e9f\u00e9rence fournisseur");
          TIDX8.setComponentPopupMenu(BTD);
          TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX8.setName("TIDX8");
          panel2.add(TIDX8);
          TIDX8.setBounds(30, 299, 214, 20);

          //---- TIDX10 ----
          TIDX10.setText("R\u00e9f\u00e9rence fabricant");
          TIDX10.setComponentPopupMenu(BTD);
          TIDX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX10.setName("TIDX10");
          panel2.add(TIDX10);
          TIDX10.setBounds(30, 327, 214, 20);

          //---- TIDX11 ----
          TIDX11.setText("Code \u00e0 barres fournisseur");
          TIDX11.setComponentPopupMenu(BTD);
          TIDX11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX11.setName("TIDX11");
          panel2.add(TIDX11);
          TIDX11.setBounds(30, 355, 214, 20);

          //---- TIDX9 ----
          TIDX9.setText("Code \u00e0 barres interne");
          TIDX9.setComponentPopupMenu(BTD);
          TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX9.setName("TIDX9");
          panel2.add(TIDX9);
          TIDX9.setBounds(30, 383, 214, 20);

          //---- OBJ_64 ----
          OBJ_64.setText("Zones personnalis\u00e9es");
          OBJ_64.setName("OBJ_64");
          panel2.add(OBJ_64);
          OBJ_64.setBounds(30, 415, 214, 20);

          //---- TIDX1 ----
          TIDX1.setText("Code article");
          TIDX1.setComponentPopupMenu(BTD);
          TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX1.setName("TIDX1");
          panel2.add(TIDX1);
          TIDX1.setBounds(30, 75, 214, 20);

          //---- ARG1 ----
          ARG1.setComponentPopupMenu(BTD);
          ARG1.setName("ARG1");
          panel2.add(ARG1);
          ARG1.setBounds(265, 71, 160, ARG1.getPreferredSize().height);

          //---- ARG2 ----
          ARG2.setComponentPopupMenu(BT4);
          ARG2.setName("ARG2");
          panel2.add(ARG2);
          ARG2.setBounds(265, 99, 160, ARG2.getPreferredSize().height);

          //---- ARG3 ----
          ARG3.setComponentPopupMenu(BT4);
          ARG3.setName("ARG3");
          panel2.add(ARG3);
          ARG3.setBounds(265, 127, 160, ARG3.getPreferredSize().height);

          //---- ARG6 ----
          ARG6.setComponentPopupMenu(BTD);
          ARG6.setName("ARG6");
          panel2.add(ARG6);
          ARG6.setBounds(265, 239, 160, ARG6.getPreferredSize().height);

          //---- ARG8 ----
          ARG8.setComponentPopupMenu(BTD);
          ARG8.setName("ARG8");
          panel2.add(ARG8);
          ARG8.setBounds(265, 295, 160, ARG8.getPreferredSize().height);

          //---- ARG10 ----
          ARG10.setComponentPopupMenu(BTD);
          ARG10.setName("ARG10");
          panel2.add(ARG10);
          ARG10.setBounds(265, 323, 160, ARG10.getPreferredSize().height);

          //---- ARG11 ----
          ARG11.setComponentPopupMenu(BTD);
          ARG11.setName("ARG11");
          panel2.add(ARG11);
          ARG11.setBounds(265, 351, 160, ARG11.getPreferredSize().height);

          //---- ARG9 ----
          ARG9.setComponentPopupMenu(BTD);
          ARG9.setName("ARG9");
          panel2.add(ARG9);
          ARG9.setBounds(265, 379, 140, ARG9.getPreferredSize().height);

          //---- OBJ_42 ----
          OBJ_42.setText("Extension");
          OBJ_42.setName("OBJ_42");
          panel2.add(OBJ_42);
          OBJ_42.setBounds(330, 159, 65, 20);

          //---- OBJ_45 ----
          OBJ_45.setText("Gestion");
          OBJ_45.setToolTipText("Gestionnaire produit");
          OBJ_45.setName("OBJ_45");
          panel2.add(OBJ_45);
          OBJ_45.setBounds(330, 187, 65, 20);

          //---- OBJ_71 ----
          OBJ_71.setText("Recherche");
          OBJ_71.setName("OBJ_71");
          panel2.add(OBJ_71);
          OBJ_71.setBounds(30, 451, 70, 16);

          //---- ARG12 ----
          ARG12.setComponentPopupMenu(BTD);
          ARG12.setName("ARG12");
          panel2.add(ARG12);
          ARG12.setBounds(265, 183, 60, ARG12.getPreferredSize().height);

          //---- ARG5 ----
          ARG5.setComponentPopupMenu(BT4);
          ARG5.setName("ARG5");
          panel2.add(ARG5);
          ARG5.setBounds(265, 211, 60, ARG5.getPreferredSize().height);

          //---- ARG7 ----
          ARG7.setComponentPopupMenu(BT4);
          ARG7.setName("ARG7");
          panel2.add(ARG7);
          ARG7.setBounds(265, 267, 80, ARG7.getPreferredSize().height);

          //---- ARG4 ----
          ARG4.setComponentPopupMenu(BT4);
          ARG4.setName("ARG4");
          panel2.add(ARG4);
          ARG4.setBounds(265, 155, 40, ARG4.getPreferredSize().height);

          //---- ARGZP1 ----
          ARGZP1.setComponentPopupMenu(BTD);
          ARGZP1.setName("ARGZP1");
          panel2.add(ARGZP1);
          ARGZP1.setBounds(265, 410, 30, ARGZP1.getPreferredSize().height);

          //---- ARGZP2 ----
          ARGZP2.setComponentPopupMenu(BTD);
          ARGZP2.setName("ARGZP2");
          panel2.add(ARGZP2);
          ARGZP2.setBounds(300, 411, 30, ARGZP2.getPreferredSize().height);

          //---- ARGZP3 ----
          ARGZP3.setComponentPopupMenu(BTD);
          ARGZP3.setName("ARGZP3");
          panel2.add(ARGZP3);
          ARGZP3.setBounds(335, 411, 30, ARGZP3.getPreferredSize().height);

          //---- ARGZP4 ----
          ARGZP4.setComponentPopupMenu(BTD);
          ARGZP4.setName("ARGZP4");
          panel2.add(ARGZP4);
          ARGZP4.setBounds(370, 411, 30, ARGZP4.getPreferredSize().height);

          //---- ARGZP5 ----
          ARGZP5.setComponentPopupMenu(BTD);
          ARGZP5.setName("ARGZP5");
          panel2.add(ARGZP5);
          ARGZP5.setBounds(405, 411, 30, ARGZP5.getPreferredSize().height);

          //---- TIDX1_CHK ----
          TIDX1_CHK.setText("");
          TIDX1_CHK.setToolTipText("Scan");
          TIDX1_CHK.setComponentPopupMenu(BTD);
          TIDX1_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX1_CHK.setName("TIDX1_CHK");
          panel2.add(TIDX1_CHK);
          TIDX1_CHK.setBounds(425, 75, 16, 20);

          //---- TIDX2_CHK ----
          TIDX2_CHK.setText("");
          TIDX2_CHK.setToolTipText("Scan");
          TIDX2_CHK.setComponentPopupMenu(BTD);
          TIDX2_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2_CHK.setName("TIDX2_CHK");
          panel2.add(TIDX2_CHK);
          TIDX2_CHK.setBounds(425, 103, 16, 20);

          //---- TIDX2_CHK2 ----
          TIDX2_CHK2.setText("");
          TIDX2_CHK2.setToolTipText("Orthographes voisines");
          TIDX2_CHK2.setComponentPopupMenu(BTD);
          TIDX2_CHK2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2_CHK2.setName("TIDX2_CHK2");
          panel2.add(TIDX2_CHK2);
          TIDX2_CHK2.setBounds(455, 103, 16, 20);

          //---- TIDX3_CHK ----
          TIDX3_CHK.setText("");
          TIDX3_CHK.setToolTipText("Scan");
          TIDX3_CHK.setComponentPopupMenu(BTD);
          TIDX3_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX3_CHK.setName("TIDX3_CHK");
          panel2.add(TIDX3_CHK);
          TIDX3_CHK.setBounds(425, 131, 16, 20);

          //---- TIDX4_CHK ----
          TIDX4_CHK.setText("");
          TIDX4_CHK.setToolTipText("Scan");
          TIDX4_CHK.setComponentPopupMenu(BTD);
          TIDX4_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX4_CHK.setName("TIDX4_CHK");
          panel2.add(TIDX4_CHK);
          TIDX4_CHK.setBounds(425, 159, 16, 20);

          //---- TIDX5_CHK ----
          TIDX5_CHK.setText("");
          TIDX5_CHK.setToolTipText("Scan");
          TIDX5_CHK.setComponentPopupMenu(BTD);
          TIDX5_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX5_CHK.setName("TIDX5_CHK");
          panel2.add(TIDX5_CHK);
          TIDX5_CHK.setBounds(425, 215, 16, 20);

          //---- TIDX6_CHK ----
          TIDX6_CHK.setText("");
          TIDX6_CHK.setToolTipText("Scan");
          TIDX6_CHK.setComponentPopupMenu(BTD);
          TIDX6_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX6_CHK.setName("TIDX6_CHK");
          panel2.add(TIDX6_CHK);
          TIDX6_CHK.setBounds(425, 243, 16, 20);

          //---- TIDX7_CHK ----
          TIDX7_CHK.setText("");
          TIDX7_CHK.setToolTipText("Scan");
          TIDX7_CHK.setComponentPopupMenu(BTD);
          TIDX7_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX7_CHK.setName("TIDX7_CHK");
          panel2.add(TIDX7_CHK);
          TIDX7_CHK.setBounds(425, 271, 16, 20);

          //---- TIDX8_CHK ----
          TIDX8_CHK.setText("");
          TIDX8_CHK.setToolTipText("Scan");
          TIDX8_CHK.setComponentPopupMenu(BTD);
          TIDX8_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX8_CHK.setName("TIDX8_CHK");
          panel2.add(TIDX8_CHK);
          TIDX8_CHK.setBounds(425, 299, 16, 20);

          //---- TIDX10_CHK ----
          TIDX10_CHK.setText("");
          TIDX10_CHK.setToolTipText("Scan");
          TIDX10_CHK.setComponentPopupMenu(BTD);
          TIDX10_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX10_CHK.setName("TIDX10_CHK");
          panel2.add(TIDX10_CHK);
          TIDX10_CHK.setBounds(425, 327, 16, 20);

          //---- TIDX11_CHK ----
          TIDX11_CHK.setText("");
          TIDX11_CHK.setToolTipText("Scan");
          TIDX11_CHK.setComponentPopupMenu(BTD);
          TIDX11_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX11_CHK.setName("TIDX11_CHK");
          panel2.add(TIDX11_CHK);
          TIDX11_CHK.setBounds(425, 355, 16, 20);

          //---- TIDX9_CHK ----
          TIDX9_CHK.setText("");
          TIDX9_CHK.setToolTipText("Scan");
          TIDX9_CHK.setComponentPopupMenu(BTD);
          TIDX9_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX9_CHK.setName("TIDX9_CHK");
          panel2.add(TIDX9_CHK);
          TIDX9_CHK.setBounds(425, 383, 16, 20);

          //---- WEXT ----
          WEXT.setComponentPopupMenu(BT4);
          WEXT.setName("WEXT");
          panel2.add(WEXT);
          WEXT.setBounds(405, 155, 20, WEXT.getPreferredSize().height);

          //---- ARGGP ----
          ARGGP.setComponentPopupMenu(BTD);
          ARGGP.setName("ARGGP");
          panel2.add(ARGGP);
          ARGGP.setBounds(405, 183, 20, ARGGP.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("@P8RES@");
          OBJ_23.setName("OBJ_23");
          panel2.add(OBJ_23);
          OBJ_23.setBounds(240, 25, 135, 20);

          //---- RB ----
          RB.setComponentPopupMenu(BTD);
          RB.setName("RB");
          panel2.add(RB);
          RB.setBounds(380, 21, 26, RB.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 495, 490);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }

    //======== BT4 ========
    {
      BT4.setName("BT4");

      //---- OBJ_16 ----
      OBJ_16.setText("Choix possibles");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BT4.add(OBJ_16);

      //---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BT4.add(OBJ_15);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX12);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDX8);
    RB_GRP.add(TIDX10);
    RB_GRP.add(TIDX11);
    RB_GRP.add(TIDX9);
    RB_GRP.add(TIDX1);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField OBJ_70;
  private JLabel ERRRECH;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX12;
  private XRiRadioButton TIDX5;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX7;
  private XRiRadioButton TIDX8;
  private XRiRadioButton TIDX10;
  private XRiRadioButton TIDX11;
  private XRiRadioButton TIDX9;
  private JLabel OBJ_64;
  private XRiRadioButton TIDX1;
  private XRiTextField ARG1;
  private XRiTextField ARG2;
  private XRiTextField ARG3;
  private XRiTextField ARG6;
  private XRiTextField ARG8;
  private XRiTextField ARG10;
  private XRiTextField ARG11;
  private XRiTextField ARG9;
  private JLabel OBJ_42;
  private JLabel OBJ_45;
  private JLabel OBJ_71;
  private XRiTextField ARG12;
  private XRiTextField ARG5;
  private XRiTextField ARG7;
  private XRiTextField ARG4;
  private XRiTextField ARGZP1;
  private XRiTextField ARGZP2;
  private XRiTextField ARGZP3;
  private XRiTextField ARGZP4;
  private XRiTextField ARGZP5;
  private JCheckBox TIDX1_CHK;
  private JCheckBox TIDX2_CHK;
  private JCheckBox TIDX2_CHK2;
  private JCheckBox TIDX3_CHK;
  private JCheckBox TIDX4_CHK;
  private JCheckBox TIDX5_CHK;
  private JCheckBox TIDX6_CHK;
  private JCheckBox TIDX7_CHK;
  private JCheckBox TIDX8_CHK;
  private JCheckBox TIDX10_CHK;
  private JCheckBox TIDX11_CHK;
  private JCheckBox TIDX9_CHK;
  private XRiTextField WEXT;
  private XRiTextField ARGGP;
  private JLabel OBJ_23;
  private XRiTextField RB;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JPopupMenu BT4;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_15;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
