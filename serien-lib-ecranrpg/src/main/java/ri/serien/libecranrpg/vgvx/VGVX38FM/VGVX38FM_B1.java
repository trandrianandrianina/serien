
package ri.serien.libecranrpg.vgvx.VGVX38FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX38FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX38FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    


    
    
   
    OBJ_28.setVisible(!lexique.HostFieldGetData("MALIB").trim().equalsIgnoreCase(""));
    OBJ_27.setVisible(!lexique.HostFieldGetData("A1LIB").trim().equalsIgnoreCase(""));
    lbMotif.setVisible(MOTIF.isVisible());
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Emballages repris"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_27 = new RiZoneSortie();
    OBJ_28 = new RiZoneSortie();
    CSART = new XRiTextField();
    OBJ_37 = new JLabel();
    OBJ_25 = new JLabel();
    CSMAG = new XRiTextField();
    panel2 = new JPanel();
    OBJ_29 = new JLabel();
    SOLDE = new XRiTextField();
    CSQT0 = new XRiTextField();
    CSQTS = new XRiTextField();
    CSQTE = new XRiTextField();
    OBJ_35 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_33 = new JLabel();
    MOTIF = new XRiTextField();
    lbMotif = new JLabel();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(840, 300));
    setOpaque(false);
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setMinimumSize(new Dimension(800, 250));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("D\u00e9tails"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_27 ----
          OBJ_27.setText("@A1LIB@");
          OBJ_27.setName("OBJ_27");
          panel1.add(OBJ_27);
          OBJ_27.setBounds(350, 37, 232, OBJ_27.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setText("@MALIB@");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(350, 67, 232, OBJ_28.getPreferredSize().height);

          //---- CSART ----
          CSART.setComponentPopupMenu(BTD);
          CSART.setName("CSART");
          panel1.add(CSART);
          CSART.setBounds(130, 35, 210, CSART.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("Magasin");
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(25, 69, 66, 20);

          //---- OBJ_25 ----
          OBJ_25.setText("Article");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(25, 35, 49, 20);

          //---- CSMAG ----
          CSMAG.setComponentPopupMenu(BTD);
          CSMAG.setName("CSMAG");
          panel1.add(CSMAG);
          CSMAG.setBounds(130, 65, 34, CSMAG.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_29 ----
          OBJ_29.setText("Quantit\u00e9 initiale");
          OBJ_29.setName("OBJ_29");
          panel2.add(OBJ_29);
          OBJ_29.setBounds(25, 44, 109, 20);

          //---- SOLDE ----
          SOLDE.setName("SOLDE");
          panel2.add(SOLDE);
          SOLDE.setBounds(350, 70, 91, SOLDE.getPreferredSize().height);

          //---- CSQT0 ----
          CSQT0.setComponentPopupMenu(BTD);
          CSQT0.setName("CSQT0");
          panel2.add(CSQT0);
          CSQT0.setBounds(130, 40, 90, CSQT0.getPreferredSize().height);

          //---- CSQTS ----
          CSQTS.setComponentPopupMenu(BTD);
          CSQTS.setName("CSQTS");
          panel2.add(CSQTS);
          CSQTS.setBounds(130, 70, 90, CSQTS.getPreferredSize().height);

          //---- CSQTE ----
          CSQTE.setComponentPopupMenu(BTD);
          CSQTE.setName("CSQTE");
          panel2.add(CSQTE);
          CSQTE.setBounds(130, 100, 90, CSQTE.getPreferredSize().height);

          //---- OBJ_35 ----
          OBJ_35.setText("Retours");
          OBJ_35.setName("OBJ_35");
          panel2.add(OBJ_35);
          OBJ_35.setBounds(25, 104, 109, 20);

          //---- OBJ_31 ----
          OBJ_31.setText("Sorties");
          OBJ_31.setName("OBJ_31");
          panel2.add(OBJ_31);
          OBJ_31.setBounds(25, 74, 109, 20);

          //---- OBJ_33 ----
          OBJ_33.setText("Solde");
          OBJ_33.setName("OBJ_33");
          panel2.add(OBJ_33);
          OBJ_33.setBounds(295, 74, 55, 20);

          //---- MOTIF ----
          MOTIF.setComponentPopupMenu(BTD);
          MOTIF.setName("MOTIF");
          panel2.add(MOTIF);
          MOTIF.setBounds(130, 10, 310, MOTIF.getPreferredSize().height);

          //---- lbMotif ----
          lbMotif.setText("Motif");
          lbMotif.setName("lbMotif");
          panel2.add(lbMotif);
          lbMotif.setBounds(25, 14, 109, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE))
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
              .addGap(15, 15, 15)
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie OBJ_27;
  private RiZoneSortie OBJ_28;
  private XRiTextField CSART;
  private JLabel OBJ_37;
  private JLabel OBJ_25;
  private XRiTextField CSMAG;
  private JPanel panel2;
  private JLabel OBJ_29;
  private XRiTextField SOLDE;
  private XRiTextField CSQT0;
  private XRiTextField CSQTS;
  private XRiTextField CSQTE;
  private JLabel OBJ_35;
  private JLabel OBJ_31;
  private JLabel OBJ_33;
  private XRiTextField MOTIF;
  private JLabel lbMotif;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
