
package ri.serien.libecranrpg.vgvx.VGVX05FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class OPTION_PANEL extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  
  public OPTION_PANEL(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    this.setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    // TODO Icones
    RETOUR.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    this.setTitle(interpreteurD.analyseExpression("Options avancées"));
  }
  
  public void getData() {
    
  }
  
  public void tuer() {
    this.dispose();
  }
  
  public void reveiller() {
    this.setVisible(true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    this.setVisible(false);
  }
  
  private void button1ActionPerformed() {
    lexique.HostFieldPutData("V06FO", 0, "P");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    this.setVisible(false);
  }
  
  private void button2ActionPerformed() {
    lexique.HostFieldPutData("V06FO", 0, "H");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    this.setVisible(false);
  }
  
  private void button5ActionPerformed() {
    lexique.HostFieldPutData("V06FO", 0, "C");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    this.setVisible(false);
  }
  
  private void button6ActionPerformed() {
    lexique.HostFieldPutData("V06FO", 0, "A");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    this.setVisible(false);
  }
  
  private void button7ActionPerformed() {
    lexique.HostFieldPutData("V06FO", 0, "W");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    this.setVisible(false);
  }
  
  private void button8ActionPerformed() {
    lexique.HostFieldPutData("V06FO", 0, "V");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    this.setVisible(false);
  }
  
  private void button9ActionPerformed() {
    lexique.HostFieldPutData("V06FO", 0, "Q");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    this.setVisible(false);
  }
  
  private void button10ActionPerformed() {
    lexique.HostFieldPutData("V06FO", 0, "S");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    this.setVisible(false);
  }
  
  private void button15ActionPerformed() {
    lexique.HostFieldPutData("V06FO", 0, "R");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    this.setVisible(false);
  }
  
  private void button16ActionPerformed() {
    lexique.HostFieldPutData("V06FO", 0, "0");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    this.setVisible(false);
  }
  
  private void button17ActionPerformed() {
    lexique.HostFieldPutData("V06FO", 0, "É");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
    this.setVisible(false);
  }
  
  private void button18ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "D");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
  }
  
  private void button20ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "°");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
  }
  
  private void button19ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "B");
    lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    panel2 = new JPanel();
    button1 = new JButton();
    button2 = new JButton();
    button7 = new JButton();
    button8 = new JButton();
    button9 = new JButton();
    button10 = new JButton();
    button15 = new JButton();
    button17 = new JButton();
    RETOUR = new JButton();
    button16 = new JButton();
    button5 = new JButton();
    button6 = new JButton();
    button18 = new JButton();
    button19 = new JButton();
    button20 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(350, 550));
    setAlwaysOnTop(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(90, 90, 90));
      P_Centre.setPreferredSize(new Dimension(350, 415));
      P_Centre.setMinimumSize(new Dimension(350, 415));
      P_Centre.setName("P_Centre");
      
      // ======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setName("panel2");
        panel2.setLayout(null);
        
        // ---- button1 ----
        button1.setText("Acc\u00e8s aux devis ");
        button1.setForeground(Color.black);
        button1.setName("button1");
        button1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button1ActionPerformed();
          }
        });
        panel2.add(button1);
        button1.setBounds(20, 10, 300, 26);
        
        // ---- button2 ----
        button2.setText("Acc\u00e8s aux commandes");
        button2.setForeground(Color.black);
        button2.setName("button2");
        button2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button2ActionPerformed();
          }
        });
        panel2.add(button2);
        button2.setBounds(20, 38, 300, 26);
        
        // ---- button7 ----
        button7.setText("Position article \u00e0 venir");
        button7.setForeground(Color.black);
        button7.setName("button7");
        button7.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button7ActionPerformed();
          }
        });
        panel2.add(button7);
        button7.setBounds(20, 66, 300, 26);
        
        // ---- button8 ----
        button8.setText("Historique des pr\u00eats");
        button8.setForeground(Color.black);
        button8.setName("button8");
        button8.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button8ActionPerformed();
          }
        });
        panel2.add(button8);
        button8.setBounds(20, 206, 300, 26);
        
        // ---- button9 ----
        button9.setText("Acc\u00e8s aux quotas");
        button9.setForeground(Color.black);
        button9.setName("button9");
        button9.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button9ActionPerformed();
          }
        });
        panel2.add(button9);
        button9.setBounds(20, 234, 300, 26);
        
        // ---- button10 ----
        button10.setText("Traitement sp\u00e9cifique");
        button10.setForeground(Color.black);
        button10.setName("button10");
        button10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button10ActionPerformed();
          }
        });
        panel2.add(button10);
        button10.setBounds(20, 290, 300, 26);
        
        // ---- button15 ----
        button15.setText("Pr\u00e9visions de consommation");
        button15.setForeground(Color.black);
        button15.setName("button15");
        button15.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button15ActionPerformed();
          }
        });
        panel2.add(button15);
        button15.setBounds(20, 122, 300, 26);
        
        // ---- button17 ----
        button17.setText("Eco-taxes");
        button17.setForeground(Color.black);
        button17.setName("button17");
        button17.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button17ActionPerformed();
          }
        });
        panel2.add(button17);
        button17.setBounds(20, 262, 300, 26);
        
        // ---- RETOUR ----
        RETOUR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        RETOUR.setText("Retour");
        RETOUR.setFont(RETOUR.getFont().deriveFont(RETOUR.getFont().getStyle() | Font.BOLD, RETOUR.getFont().getSize() + 3f));
        RETOUR.setForeground(Color.black);
        RETOUR.setHorizontalAlignment(SwingConstants.LEADING);
        RETOUR.setIconTextGap(10);
        RETOUR.setName("RETOUR");
        RETOUR.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_10ActionPerformed(e);
          }
        });
        panel2.add(RETOUR);
        RETOUR.setBounds(95, 450, 150, 40);
        
        // ---- button16 ----
        button16.setText("Fiche r\u00e9approvisionnement");
        button16.setForeground(Color.black);
        button16.setName("button16");
        button16.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button16ActionPerformed();
          }
        });
        panel2.add(button16);
        button16.setBounds(20, 94, 300, 26);
        
        // ---- button5 ----
        button5.setText("Statistiques crois\u00e9es Article/Clients");
        button5.setForeground(Color.black);
        button5.setName("button5");
        button5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button5ActionPerformed();
          }
        });
        panel2.add(button5);
        button5.setBounds(20, 150, 300, 26);
        
        // ---- button6 ----
        button6.setText("Statistiques sans choix de p\u00e9riode");
        button6.setForeground(Color.black);
        button6.setName("button6");
        button6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button6ActionPerformed();
          }
        });
        panel2.add(button6);
        button6.setBounds(20, 178, 300, 26);
        
        // ---- button18 ----
        button18.setText("Prix vente consommateur");
        button18.setForeground(Color.black);
        button18.setName("button18");
        button18.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button18ActionPerformed(e);
            button18ActionPerformed(e);
          }
        });
        panel2.add(button18);
        button18.setBounds(20, 320, 300, 26);
        
        // ---- button19 ----
        button19.setText("Prix de la concurrence");
        button19.setForeground(Color.black);
        button19.setName("button19");
        button19.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button19ActionPerformed(e);
          }
        });
        panel2.add(button19);
        button19.setBounds(20, 350, 300, 26);
        
        // ---- button20 ----
        button20.setText("Historique des modifications");
        button20.setForeground(Color.black);
        button20.setName("button20");
        button20.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button20ActionPerformed(e);
          }
        });
        panel2.add(button20);
        button20.setBounds(20, 380, 300, 26);
      }
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup().addComponent(panel2, GroupLayout.Alignment.TRAILING,
          GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE));
      P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup().addComponent(panel2, GroupLayout.Alignment.TRAILING,
          GroupLayout.DEFAULT_SIZE, 501, Short.MAX_VALUE));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JPanel panel2;
  private JButton button1;
  private JButton button2;
  private JButton button7;
  private JButton button8;
  private JButton button9;
  private JButton button10;
  private JButton button15;
  private JButton button17;
  private JButton RETOUR;
  private JButton button16;
  private JButton button5;
  private JButton button6;
  private JButton button18;
  private JButton button19;
  private JButton button20;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
