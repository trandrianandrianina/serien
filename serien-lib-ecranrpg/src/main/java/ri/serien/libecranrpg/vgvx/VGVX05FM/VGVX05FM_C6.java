
package ri.serien.libecranrpg.vgvx.VGVX05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVX05FM_C6 extends SNPanelEcranRPG implements ioFrame {
  private String[] A1TPR_Value = { "", "1", "2", "3", };
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public VGVX05FM_C6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    setDialog(true);
    
    // Ajout
    initDiverses();
    A1TPR.setValeurs(A1TPR_Value, null);
    A1CDP.setValeurs("P", A1CDP_GRP);
    A1CDP_REV.setValeurs("R");
    A1CDP_DENTRE.setValeurs("E");
    
    // Titre
    setTitle("Chiffrage");
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WPMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMP@")).trim());
    COEMAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COEMAR@")).trim());
    WPRI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPRI@")).trim());
    R18PDE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@R18PDE@")).trim());
    OBJ_53_OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBPFB@")).trim());
    R18DDE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@R18DDE@")).trim());
    OBJ_74_OBJ_74.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@F4PFA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    boolean creation = (lexique.getMode() == Lexical.MODE_CREATION);
    boolean modification = (lexique.getMode() == Lexical.MODE_MODIFICATION);
    
    OBJ_74_OBJ_74.setVisible(lexique.isPresent("F4PFA"));
    OBJ_53_OBJ_53.setVisible(!lexique.HostFieldGetData("NMC").trim().equalsIgnoreCase("NMC"));
    A1CDP.setEnabled(!creation);
    A1CDP_REV.setEnabled(!creation);
    A1CDP_DENTRE.setEnabled(!creation);
    
    navig_valid.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    
    pr_rev.setEnabled(modification);
    pr_moy.setEnabled(modification);
    pr_moy2.setEnabled(modification);
    pr_moy3.setEnabled(modification);
    OBJ_34.setEnabled(modification);
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes auto-génrées par JFormdesigner
  // ---------------------------------------------------------------------------------------------------------------------------------------
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel7 = new JPanel();
    A1PRV = new XRiTextField();
    A1CPR = new XRiTextField();
    WPMP = new RiZoneSortie();
    COEMAR = new RiZoneSortie();
    WPRI = new RiZoneSortie();
    R18PDE = new RiZoneSortie();
    A1PFB = new XRiTextField();
    A1TPR = new XRiComboBox();
    OBJ_43_OBJ_43 = new JLabel();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_34 = new SNBoutonDetail();
    OBJ_49_OBJ_49 = new JLabel();
    label1 = new JLabel();
    pr_rev = new SNBoutonDetail();
    pr_moy = new SNBoutonDetail();
    label2 = new JLabel();
    pr_moy2 = new SNBoutonDetail();
    pr_moy3 = new SNBoutonDetail();
    label3 = new JLabel();
    label4 = new JLabel();
    R18DDE = new RiZoneSortie();
    OBJ_52_OBJ_52 = new JLabel();
    A1PRS = new XRiTextField();
    A1CMA = new XRiTextField();
    OBJ_38_OBJ_38 = new JLabel();
    panel8 = new JPanel();
    A1MDP = new XRiTextField();
    A1PDP = new XRiTextField();
    A1CDP = new XRiRadioButton();
    A1CDP_REV = new XRiRadioButton();
    A1CDP_DENTRE = new XRiRadioButton();
    OBJ_60_OBJ_60 = new JLabel();
    OBJ_61_OBJ_61 = new JLabel();
    OBJ_63_OBJ_63 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_22 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    FCT1 = new JPopupMenu();
    OBJ_25 = new JMenuItem();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_74_OBJ_74 = new JLabel();
    OBJ_24 = new JMenuItem();
    A1PRI = new XRiTextField();
    label5 = new JLabel();
    A1CDP_GRP = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(890, 420));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 255));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt1 ----
              riMenu_bt1.setText("@V01F@");
              riMenu_bt1.setPreferredSize(new Dimension(167, 50));
              riMenu_bt1.setMinimumSize(new Dimension(167, 50));
              riMenu_bt1.setMaximumSize(new Dimension(170, 50));
              riMenu_bt1.setFont(riMenu_bt1.getFont().deriveFont(riMenu_bt1.getFont().getSize() + 2f));
              riMenu_bt1.setName("riMenu_bt1");
              riMenu_V01F.add(riMenu_bt1);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        
        // ======== panel7 ========
        {
          panel7.setBorder(new TitledBorder("Prix par Unit\u00e9 de Stock"));
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);
          
          // ---- A1PRV ----
          A1PRV.setComponentPopupMenu(FCT1);
          A1PRV.setName("A1PRV");
          A1PRV.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
              A1PRVKeyTyped(e);
            }
          });
          panel7.add(A1PRV);
          A1PRV.setBounds(170, 35, 95, A1PRV.getPreferredSize().height);
          
          // ---- A1CPR ----
          A1CPR.setComponentPopupMenu(BTD);
          A1CPR.setToolTipText("?C6.A1CPR.toolTipText_2?");
          A1CPR.setName("A1CPR");
          panel7.add(A1CPR);
          A1CPR.setBounds(595, 35, 40, A1CPR.getPreferredSize().height);
          
          // ---- WPMP ----
          WPMP.setComponentPopupMenu(FCT1);
          WPMP.setText("@WPMP@");
          WPMP.setHorizontalAlignment(SwingConstants.RIGHT);
          WPMP.setName("WPMP");
          panel7.add(WPMP);
          WPMP.setBounds(171, 105, 93, WPMP.getPreferredSize().height);
          
          // ---- COEMAR ----
          COEMAR.setComponentPopupMenu(FCT1);
          COEMAR.setText("@COEMAR@");
          COEMAR.setHorizontalAlignment(SwingConstants.RIGHT);
          COEMAR.setName("COEMAR");
          panel7.add(COEMAR);
          COEMAR.setBounds(575, 105, 60, COEMAR.getPreferredSize().height);
          
          // ---- WPRI ----
          WPRI.setComponentPopupMenu(FCT1);
          WPRI.setText("@WPRI@");
          WPRI.setHorizontalAlignment(SwingConstants.RIGHT);
          WPRI.setName("WPRI");
          panel7.add(WPRI);
          WPRI.setBounds(171, 140, 93, WPRI.getPreferredSize().height);
          
          // ---- R18PDE ----
          R18PDE.setComponentPopupMenu(FCT1);
          R18PDE.setText("@R18PDE@");
          R18PDE.setHorizontalAlignment(SwingConstants.RIGHT);
          R18PDE.setName("R18PDE");
          panel7.add(R18PDE);
          R18PDE.setBounds(171, 175, 93, R18PDE.getPreferredSize().height);
          
          // ---- A1PFB ----
          A1PFB.setComponentPopupMenu(FCT1);
          A1PFB.setName("A1PFB");
          panel7.add(A1PFB);
          A1PFB.setBounds(170, 210, 95, A1PFB.getPreferredSize().height);
          
          // ---- A1TPR ----
          A1TPR.setModel(new DefaultComboBoxModel<>(new String[] { "Saisissable et modifi\u00e9 \u00e0 chaque entr\u00e9e en stock",
              "Saisi et jamais calcul\u00e9", "Calcul\u00e9 \u00e0 chaque entr\u00e9e avec mode de calcul param\u00e9tr\u00e9",
              "Saisi, jamais calcul\u00e9 & PUMP non modifi\u00e9 par syst\u00e8me" }));
          A1TPR.setComponentPopupMenu(BTD);
          A1TPR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1TPR.setName("A1TPR");
          panel7.add(A1TPR);
          A1TPR.setBounds(275, 36, 312, A1TPR.getPreferredSize().height);
          
          // ---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("Prix de vente sur P.U.M.P.");
          OBJ_43_OBJ_43.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
          panel7.add(OBJ_43_OBJ_43);
          OBJ_43_OBJ_43.setBounds(395, 108, 150, 18);
          
          // ---- OBJ_53_OBJ_53 ----
          OBJ_53_OBJ_53.setText("@LIBPFB@");
          OBJ_53_OBJ_53.setComponentPopupMenu(BTD);
          OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");
          panel7.add(OBJ_53_OBJ_53);
          OBJ_53_OBJ_53.setBounds(20, 214, 140, 21);
          
          // ---- OBJ_34 ----
          OBJ_34.setToolTipText("Mode de calcul du prix de revient");
          OBJ_34.setComponentPopupMenu(BTD);
          OBJ_34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_34.setName("OBJ_34");
          OBJ_34.addActionListener(e -> OBJ_34ActionPerformed(e));
          panel7.add(OBJ_34);
          OBJ_34.setBounds(635, 36, 33, 26);
          
          // ---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("Date de derni\u00e8re entr\u00e9e");
          OBJ_49_OBJ_49.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
          panel7.add(OBJ_49_OBJ_49);
          OBJ_49_OBJ_49.setBounds(390, 177, 155, 20);
          
          // ---- label1 ----
          label1.setText("Prix de revient");
          label1.setName("label1");
          panel7.add(label1);
          label1.setBounds(20, 39, 120, 20);
          
          // ---- pr_rev ----
          pr_rev.setToolTipText("Historique des achats");
          pr_rev.setName("pr_rev");
          pr_rev.addActionListener(e -> pr_revActionPerformed(e));
          panel7.add(pr_rev);
          pr_rev.setBounds(140, 33, 32, 32);
          
          // ---- pr_moy ----
          pr_moy.setToolTipText("Justificatif du prix moyen pond\u00e9r\u00e9");
          pr_moy.setName("pr_moy");
          pr_moy.addActionListener(e -> pr_moyActionPerformed(e));
          panel7.add(pr_moy);
          pr_moy.setBounds(140, 101, 32, 32);
          
          // ---- label2 ----
          label2.setText("Prix moyen pond\u00e9r\u00e9");
          label2.setName("label2");
          panel7.add(label2);
          label2.setBounds(20, 107, 120, 20);
          
          // ---- pr_moy2 ----
          pr_moy2.setToolTipText("Justificatif du prix d'inventaire");
          pr_moy2.setName("pr_moy2");
          pr_moy2.addActionListener(e -> pr_moy2ActionPerformed(e));
          panel7.add(pr_moy2);
          pr_moy2.setBounds(140, 136, 32, 32);
          
          // ---- pr_moy3 ----
          pr_moy3.setToolTipText("Historique des achats");
          pr_moy3.setName("pr_moy3");
          pr_moy3.addActionListener(e -> pr_moy3ActionPerformed(e));
          panel7.add(pr_moy3);
          pr_moy3.setBounds(140, 171, 32, 32);
          
          // ---- label3 ----
          label3.setText("Prix d'inventaire");
          label3.setName("label3");
          panel7.add(label3);
          label3.setBounds(20, 142, 120, 20);
          
          // ---- label4 ----
          label4.setText("Co\u00fbt derni\u00e8re entr\u00e9e");
          label4.setName("label4");
          panel7.add(label4);
          label4.setBounds(20, 177, 125, 20);
          
          // ---- R18DDE ----
          R18DDE.setText("@R18DDE@");
          R18DDE.setHorizontalAlignment(SwingConstants.CENTER);
          R18DDE.setName("R18DDE");
          panel7.add(R18DDE);
          R18DDE.setBounds(560, 175, 75, R18DDE.getPreferredSize().height);
          
          // ---- OBJ_52_OBJ_52 ----
          OBJ_52_OBJ_52.setText("Prix de revient standard");
          OBJ_52_OBJ_52.setHorizontalAlignment(SwingConstants.LEFT);
          OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");
          panel7.add(OBJ_52_OBJ_52);
          OBJ_52_OBJ_52.setBounds(20, 74, 150, 20);
          
          // ---- A1PRS ----
          A1PRS.setComponentPopupMenu(FCT1);
          A1PRS.setName("A1PRS");
          panel7.add(A1PRS);
          A1PRS.setBounds(170, 70, 95, A1PRS.getPreferredSize().height);
          
          // ---- A1CMA ----
          A1CMA.setComponentPopupMenu(null);
          A1CMA.setName("A1CMA");
          panel7.add(A1CMA);
          A1CMA.setBounds(579, 210, 58, A1CMA.getPreferredSize().height);
          
          // ---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("Coefficient de vente");
          OBJ_38_OBJ_38.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");
          panel7.add(OBJ_38_OBJ_38);
          OBJ_38_OBJ_38.setBounds(425, 214, 120, 20);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel7.getComponentCount(); i++) {
              Rectangle bounds = panel7.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel7.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel7.setMinimumSize(preferredSize);
            panel7.setPreferredSize(preferredSize);
          }
        }
        
        // ======== panel8 ========
        {
          panel8.setBorder(new TitledBorder("D\u00e9pr\u00e9ciation de stock"));
          panel8.setOpaque(false);
          panel8.setName("panel8");
          panel8.setLayout(null);
          
          // ---- A1MDP ----
          A1MDP.setComponentPopupMenu(null);
          A1MDP.setName("A1MDP");
          panel8.add(A1MDP);
          A1MDP.setBounds(170, 61, 80, A1MDP.getPreferredSize().height);
          
          // ---- A1PDP ----
          A1PDP.setComponentPopupMenu(null);
          A1PDP.setName("A1PDP");
          panel8.add(A1PDP);
          A1PDP.setBounds(380, 61, 50, A1PDP.getPreferredSize().height);
          
          // ---- A1CDP ----
          A1CDP.setText("P.U.M.P.");
          A1CDP.setComponentPopupMenu(BTD);
          A1CDP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1CDP.setName("A1CDP");
          panel8.add(A1CDP);
          A1CDP.setBounds(520, 35, 155, 20);
          
          // ---- A1CDP_REV ----
          A1CDP_REV.setText("Prix de revient");
          A1CDP_REV.setComponentPopupMenu(BTD);
          A1CDP_REV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1CDP_REV.setName("A1CDP_REV");
          panel8.add(A1CDP_REV);
          A1CDP_REV.setBounds(520, 65, 155, 20);
          
          // ---- A1CDP_DENTRE ----
          A1CDP_DENTRE.setText("Prix derni\u00e8re entr\u00e9e");
          A1CDP_DENTRE.setComponentPopupMenu(BTD);
          A1CDP_DENTRE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          A1CDP_DENTRE.setName("A1CDP_DENTRE");
          panel8.add(A1CDP_DENTRE);
          A1CDP_DENTRE.setBounds(520, 95, 155, 20);
          
          // ---- OBJ_60_OBJ_60 ----
          OBJ_60_OBJ_60.setText("Valeur de d\u00e9preciation");
          OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");
          panel8.add(OBJ_60_OBJ_60);
          OBJ_60_OBJ_60.setBounds(20, 65, 145, 20);
          
          // ---- OBJ_61_OBJ_61 ----
          OBJ_61_OBJ_61.setText("ou pourcentage");
          OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");
          panel8.add(OBJ_61_OBJ_61);
          OBJ_61_OBJ_61.setBounds(278, 65, 107, 20);
          
          // ---- OBJ_63_OBJ_63 ----
          OBJ_63_OBJ_63.setText("sur prix");
          OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");
          panel8.add(OBJ_63_OBJ_63);
          OBJ_63_OBJ_63.setBounds(455, 65, 65, 20);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel8.getComponentCount(); i++) {
              Rectangle bounds = panel8.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel8.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel8.setMinimumSize(preferredSize);
            panel8.setPreferredSize(preferredSize);
          }
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup().addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup()
                    .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 688, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel8, GroupLayout.PREFERRED_SIZE, 688, GroupLayout.PREFERRED_SIZE))));
        p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup().addGap(5, 5, 5)
                .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                .addComponent(panel8, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_22 ----
      OBJ_22.setText("?C6.OBJ_22.text_2?");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(e -> OBJ_22ActionPerformed(e));
      BTD.add(OBJ_22);
      
      // ---- OBJ_21 ----
      OBJ_21.setText("?C6.OBJ_21.text_2?");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(e -> OBJ_21ActionPerformed(e));
      BTD.add(OBJ_21);
    }
    
    // ======== FCT1 ========
    {
      FCT1.setName("FCT1");
      
      // ---- OBJ_25 ----
      OBJ_25.setText("?C6.OBJ_25.text_2?");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(e -> OBJ_25ActionPerformed(e));
      FCT1.add(OBJ_25);
    }
    
    // ======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");
      
      // ---- OBJ_5 ----
      OBJ_5.setText("?C6.OBJ_5.text_2?");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(e -> OBJ_5ActionPerformed(e));
      OBJ_4.add(OBJ_5);
      
      // ---- OBJ_7 ----
      OBJ_7.setText("?C6.OBJ_7.text_2?");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(e -> OBJ_7ActionPerformed(e));
      OBJ_4.add(OBJ_7);
      
      // ---- OBJ_8 ----
      OBJ_8.setText("?C6.OBJ_8.text_2?");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(e -> OBJ_8ActionPerformed(e));
      OBJ_4.add(OBJ_8);
      
      // ---- OBJ_9 ----
      OBJ_9.setText("?C6.OBJ_9.text_2?");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(e -> OBJ_9ActionPerformed(e));
      OBJ_4.add(OBJ_9);
      
      // ---- OBJ_10 ----
      OBJ_10.setText("?C6.OBJ_10.text_2?");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(e -> OBJ_10ActionPerformed(e));
      OBJ_4.add(OBJ_10);
      
      // ---- OBJ_11 ----
      OBJ_11.setText("?C6.OBJ_11.text_2?");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(e -> OBJ_11ActionPerformed(e));
      OBJ_4.add(OBJ_11);
      
      // ---- OBJ_12 ----
      OBJ_12.setText("?C6.OBJ_12.text_2?");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(e -> OBJ_12ActionPerformed(e));
      OBJ_4.add(OBJ_12);
      
      // ---- OBJ_13 ----
      OBJ_13.setText("?C6.OBJ_13.text_2?");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(e -> OBJ_13ActionPerformed(e));
      OBJ_4.add(OBJ_13);
      OBJ_4.addSeparator();
      
      // ---- OBJ_19 ----
      OBJ_19.setText("?C6.OBJ_19.text_2?");
      OBJ_19.setName("OBJ_19");
      OBJ_4.add(OBJ_19);
    }
    
    // ---- OBJ_74_OBJ_74 ----
    OBJ_74_OBJ_74.setText("@F4PFA@");
    OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");
    
    // ---- OBJ_24 ----
    OBJ_24.setText("?C6.OBJ_24.text_2?");
    OBJ_24.setName("OBJ_24");
    OBJ_24.addActionListener(e -> OBJ_24ActionPerformed(e));
    
    // ---- A1PRI ----
    A1PRI.setComponentPopupMenu(FCT1);
    A1PRI.setName("A1PRI");
    
    // ---- label5 ----
    label5.setText("?C6.label5.text_2?");
    label5.setName("label5");
    
    // ---- A1CDP_GRP ----
    A1CDP_GRP.add(A1CDP);
    A1CDP_GRP.add(A1CDP_REV);
    A1CDP_GRP.add(A1CDP_DENTRE);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(9, 52);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void pr_revActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("A1PRV");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void pr_moyActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WPMP");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void pr_moy2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WPRI");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void pr_moy3ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("R18PDE");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(FCT1.getInvoker().getName());
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F6"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F10"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F21"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F22"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F22");
  }
  
  /**
   * Valider le chiffrage.
   * @param e L'évènement clic.
   */
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * Retourner à l'écran précédent.
   * @param e L'évènement clic.
   */
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  /**
   * Capturer l'évènement lorsque l'utilisateur tape des caractères.
   * @param e L'évènement.
   */
  private void A1PRVKeyTyped(KeyEvent e) {
    try {
      char caractereTaper = e.getKeyChar();
      int longueurMaxPartieEntiere = 7;
      int longueurMaxPartieDecimale = 2;
      // Vérifier si le montant tapé par l'utilisateur matche avec le format défini
      if (!A1PRV.verifierFormatMontant(longueurMaxPartieEntiere, longueurMaxPartieDecimale, caractereTaper)) {
        e.consume();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel7;
  private XRiTextField A1PRV;
  private XRiTextField A1CPR;
  private RiZoneSortie WPMP;
  private RiZoneSortie COEMAR;
  private RiZoneSortie WPRI;
  private RiZoneSortie R18PDE;
  private XRiTextField A1PFB;
  private XRiComboBox A1TPR;
  private JLabel OBJ_43_OBJ_43;
  private JLabel OBJ_53_OBJ_53;
  private SNBoutonDetail OBJ_34;
  private JLabel OBJ_49_OBJ_49;
  private JLabel label1;
  private SNBoutonDetail pr_rev;
  private SNBoutonDetail pr_moy;
  private JLabel label2;
  private SNBoutonDetail pr_moy2;
  private SNBoutonDetail pr_moy3;
  private JLabel label3;
  private JLabel label4;
  private RiZoneSortie R18DDE;
  private JLabel OBJ_52_OBJ_52;
  private XRiTextField A1PRS;
  private XRiTextField A1CMA;
  private JLabel OBJ_38_OBJ_38;
  private JPanel panel8;
  private XRiTextField A1MDP;
  private XRiTextField A1PDP;
  private XRiRadioButton A1CDP;
  private XRiRadioButton A1CDP_REV;
  private XRiRadioButton A1CDP_DENTRE;
  private JLabel OBJ_60_OBJ_60;
  private JLabel OBJ_61_OBJ_61;
  private JLabel OBJ_63_OBJ_63;
  private JPopupMenu BTD;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_21;
  private JPopupMenu FCT1;
  private JMenuItem OBJ_25;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_19;
  private JLabel OBJ_74_OBJ_74;
  private JMenuItem OBJ_24;
  private XRiTextField A1PRI;
  private JLabel label5;
  private ButtonGroup A1CDP_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
