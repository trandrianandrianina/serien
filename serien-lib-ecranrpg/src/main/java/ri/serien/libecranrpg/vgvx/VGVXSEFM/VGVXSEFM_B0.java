
package ri.serien.libecranrpg.vgvx.VGVXSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXSEFM_B0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVXSEFM_B0(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SEFUAA.setValeursSelection("1", " ");
    SEFUFR.setValeursSelection("1", " ");
    SEFUCL.setValeursSelection("1", " ");
    SEFUAV.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Sécurité de la gestion des ventes et des achats (@LOCUSR@)")).trim());
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_70.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
  }
  
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    SEETP.setEnabled(lexique.isPresent("SEETP"));
    SECWF.setEnabled(lexique.isPresent("SECWF"));
    INDUSR.setVisible(lexique.isPresent("INDUSR"));
    
    // SEFUAV.setSelected(lexique.HostFieldGetData("SEFUAV").equalsIgnoreCase("1"));
    // SEFUCL.setSelected(lexique.HostFieldGetData("SEFUCL").equalsIgnoreCase("1"));
    // SEFUFR.setSelected(lexique.HostFieldGetData("SEFUFR").equalsIgnoreCase("1"));
    // SEFUAA.setSelected(lexique.HostFieldGetData("SEFUAA").equalsIgnoreCase("1"));
    
    

    
    
    
  }
  
  public void getData() {
    super.getData();
    
    
    // if (SEFUAV.isSelected())
    // lexique.HostFieldPutData("SEFUAV", 0, "1");
    // else
    // lexique.HostFieldPutData("SEFUAV", 0, " ");
    // if (SEFUCL.isSelected())
    // lexique.HostFieldPutData("SEFUCL", 0, "1");
    // else
    // lexique.HostFieldPutData("SEFUCL", 0, " ");
    // if (SEFUFR.isSelected())
    // lexique.HostFieldPutData("SEFUFR", 0, "1");
    // else
    // lexique.HostFieldPutData("SEFUFR", 0, " ");
    // if (SEFUAA.isSelected())
    // lexique.HostFieldPutData("SEFUAA", 0, "1");
    // else
    // lexique.HostFieldPutData("SEFUAA", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    INDUSR = new RiZoneSortie();
    p_tete_droite = new JPanel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    pnlEtablissement = new JPanel();
    OBJ_70 = new RiZoneSortie();
    OBJ_64 = new JLabel();
    OBJ_69 = new JLabel();
    SECWF = new XRiTextField();
    SEETP = new XRiTextField();
    pnlFormat = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    SEFPAV = new XRiTextField();
    SEFUAV = new XRiCheckBox();
    SEFPCL = new XRiTextField();
    SEFUCL = new XRiCheckBox();
    SEFPFR = new XRiTextField();
    SEFUFR = new XRiCheckBox();
    SEFPAA = new XRiTextField();
    SEFUAA = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 de la gestion des ventes et des achats (@LOCUSR@)");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_34 ----
          OBJ_34.setText("Utilisateur");
          OBJ_34.setName("OBJ_34");
          p_tete_gauche.add(OBJ_34);
          OBJ_34.setBounds(5, 2, 78, 20);

          //---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setText("@INDUSR@");
          INDUSR.setOpaque(false);
          INDUSR.setName("INDUSR");
          p_tete_gauche.add(INDUSR);
          INDUSR.setBounds(80, 0, 110, INDUSR.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());

      //======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);

      //======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(720, 400));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setOpaque(false);
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(null);

          //---- OBJ_70 ----
          OBJ_70.setText("@DGNOM@");
          OBJ_70.setName("OBJ_70");
          pnlEtablissement.add(OBJ_70);
          OBJ_70.setBounds(255, 30, 322, OBJ_70.getPreferredSize().height);

          //---- OBJ_64 ----
          OBJ_64.setText("Mot de passe WorkFlow");
          OBJ_64.setName("OBJ_64");
          pnlEtablissement.add(OBJ_64);
          OBJ_64.setBounds(30, 69, 160, 20);

          //---- OBJ_69 ----
          OBJ_69.setText("Etablissement principal");
          OBJ_69.setName("OBJ_69");
          pnlEtablissement.add(OBJ_69);
          OBJ_69.setBounds(30, 32, 160, 20);

          //---- SECWF ----
          SECWF.setComponentPopupMenu(BTD);
          SECWF.setName("SECWF");
          pnlEtablissement.add(SECWF);
          SECWF.setBounds(205, 65, 110, SECWF.getPreferredSize().height);

          //---- SEETP ----
          SEETP.setComponentPopupMenu(BTD);
          SEETP.setName("SEETP");
          pnlEtablissement.add(SEETP);
          SEETP.setBounds(205, 28, 40, SEETP.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < pnlEtablissement.getComponentCount(); i++) {
              Rectangle bounds = pnlEtablissement.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlEtablissement.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlEtablissement.setMinimumSize(preferredSize);
            pnlEtablissement.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(pnlEtablissement);
        pnlEtablissement.setBounds(25, 5, 674, 100);

        //======== pnlFormat ========
        {
          pnlFormat.setBorder(new TitledBorder("Formats personnalis\u00e9s"));
          pnlFormat.setOpaque(false);
          pnlFormat.setName("pnlFormat");
          pnlFormat.setLayout(null);

          //---- label1 ----
          label1.setText("Article");
          label1.setName("label1");
          pnlFormat.add(label1);
          label1.setBounds(30, 44, 125, 20);

          //---- label2 ----
          label2.setText("Client");
          label2.setName("label2");
          pnlFormat.add(label2);
          label2.setBounds(30, 79, 125, 20);

          //---- label3 ----
          label3.setText("Fournisseur");
          label3.setName("label3");
          pnlFormat.add(label3);
          label3.setBounds(30, 114, 125, 20);

          //---- label4 ----
          label4.setText("Ventes");
          label4.setName("label4");
          pnlFormat.add(label4);
          label4.setBounds(30, 149, 125, 20);

          //---- SEFPAV ----
          SEFPAV.setName("SEFPAV");
          pnlFormat.add(SEFPAV);
          SEFPAV.setBounds(205, 40, 65, SEFPAV.getPreferredSize().height);

          //---- SEFUAV ----
          SEFUAV.setText("protection");
          SEFUAV.setName("SEFUAV");
          pnlFormat.add(SEFUAV);
          SEFUAV.setBounds(290, 45, 100, SEFUAV.getPreferredSize().height);

          //---- SEFPCL ----
          SEFPCL.setName("SEFPCL");
          pnlFormat.add(SEFPCL);
          SEFPCL.setBounds(205, 75, 65, SEFPCL.getPreferredSize().height);

          //---- SEFUCL ----
          SEFUCL.setText("protection");
          SEFUCL.setName("SEFUCL");
          pnlFormat.add(SEFUCL);
          SEFUCL.setBounds(290, 80, 100, SEFUCL.getPreferredSize().height);

          //---- SEFPFR ----
          SEFPFR.setName("SEFPFR");
          pnlFormat.add(SEFPFR);
          SEFPFR.setBounds(205, 110, 65, SEFPFR.getPreferredSize().height);

          //---- SEFUFR ----
          SEFUFR.setText("protection");
          SEFUFR.setName("SEFUFR");
          pnlFormat.add(SEFUFR);
          SEFUFR.setBounds(290, 115, 100, SEFUFR.getPreferredSize().height);

          //---- SEFPAA ----
          SEFPAA.setName("SEFPAA");
          pnlFormat.add(SEFPAA);
          SEFPAA.setBounds(205, 145, 65, SEFPAA.getPreferredSize().height);

          //---- SEFUAA ----
          SEFUAA.setText("protection achats");
          SEFUAA.setName("SEFUAA");
          pnlFormat.add(SEFUAA);
          SEFUAA.setBounds(290, 150, 175, SEFUAA.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < pnlFormat.getComponentCount(); i++) {
              Rectangle bounds = pnlFormat.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlFormat.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlFormat.setMinimumSize(preferredSize);
            pnlFormat.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(pnlFormat);
        pnlFormat.setBounds(25, 145, 674, 210);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel pnlNord;
    private SNBandeauTitre p_bpresentation;
    private JMenuBar barre_tete;
    private JPanel p_tete_gauche;
    private JLabel OBJ_34;
    private RiZoneSortie INDUSR;
    private JPanel p_tete_droite;
    private SNPanelFond pnlSud;
    private JPanel pnlMenus;
    private JPanel menus_bas;
    private RiMenu navig_erreurs;
    private RiMenu_bt bouton_erreurs;
    private RiMenu navig_valid;
    private RiMenu_bt bouton_valider;
    private RiMenu navig_retour;
    private RiMenu_bt bouton_retour;
    private JScrollPane scroll_droite;
    private JPanel menus_haut;
    private RiMenu riMenu_V01F;
    private RiMenu_bt riMenu_bt_V01F;
    private RiSousMenu riSousMenu_consult;
    private RiSousMenu_bt riSousMenu_bt_consult;
    private RiSousMenu riSousMenu_modif;
    private RiSousMenu_bt riSousMenu_bt_modif;
    private RiSousMenu riSousMenu_crea;
    private RiSousMenu_bt riSousMenu_bt_crea;
    private RiSousMenu riSousMenu_suppr;
    private RiSousMenu_bt riSousMenu_bt_suppr;
    private RiSousMenu riSousMenuF_dupli;
    private RiSousMenu_bt riSousMenu_bt_dupli;
    private RiSousMenu riSousMenu_rappel;
    private RiSousMenu_bt riSousMenu_bt_rappel;
    private RiSousMenu riSousMenu_reac;
    private RiSousMenu_bt riSousMenu_bt_reac;
    private RiSousMenu riSousMenu_destr;
    private RiSousMenu_bt riSousMenu_bt_destr;
    private SNPanelContenu pnlContenu;
    private JPanel pnlEtablissement;
    private RiZoneSortie OBJ_70;
    private JLabel OBJ_64;
    private JLabel OBJ_69;
    private XRiTextField SECWF;
    private XRiTextField SEETP;
    private JPanel pnlFormat;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private XRiTextField SEFPAV;
    private XRiCheckBox SEFUAV;
    private XRiTextField SEFPCL;
    private XRiCheckBox SEFUCL;
    private XRiTextField SEFPFR;
    private XRiCheckBox SEFUFR;
    private XRiTextField SEFPAA;
    private XRiCheckBox SEFUAA;
    private JPopupMenu BTD;
    private JMenuItem OBJ_15;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
