
package ri.serien.libecranrpg.vgvx.VGVX35FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVX35FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WL501_Title = { "HLD01", };
  private String[][] _WL501_Data = { { "WL501", }, };
  private int[] _WL501_Width = { 75, };
  
  public VGVX35FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WL501.setAspectTable(null, _WL501_Title, _WL501_Data, _WL501_Width, true, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WH1TI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WH1TI@")).trim());
    CLNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    E1REP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1REP@")).trim());
    RPLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLIB@")).trim());
    E1NCC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NCC@")).trim());
    E1RCC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1RCC@")).trim());
    E1HOMX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1HOMX@")).trim());
    E1EXPX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1EXPX@")).trim());
    E1SAN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1SAN@")).trim());
    E1FACX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1FACX@")).trim());
    E1ACT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ACT@")).trim());
    E1NFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NFA@")).trim());
    L1REM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1REM1@")).trim());
    L1REM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1REM2@")).trim());
    L1REM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1REM3@")).trim());
    L1REM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1REM4@")).trim());
    L1REM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1REM5@")).trim());
    L1REM6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1REM6@")).trim());
    A1ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1ART@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Historique de vente"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WH1TI = new RiZoneSortie();
    CLNOM = new RiZoneSortie();
    E1REP = new RiZoneSortie();
    RPLIB = new RiZoneSortie();
    E1NCC = new RiZoneSortie();
    E1RCC = new RiZoneSortie();
    E1HOMX = new RiZoneSortie();
    E1EXPX = new RiZoneSortie();
    E1SAN = new RiZoneSortie();
    E1FACX = new RiZoneSortie();
    E1ACT = new RiZoneSortie();
    E1NFA = new RiZoneSortie();
    OBJ_33 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_42 = new JLabel();
    L1REM1 = new RiZoneSortie();
    L1REM2 = new RiZoneSortie();
    L1REM3 = new RiZoneSortie();
    L1REM4 = new RiZoneSortie();
    L1REM5 = new RiZoneSortie();
    L1REM6 = new RiZoneSortie();
    OBJ_46 = new JLabel();
    OBJ_30 = new JLabel();
    A1ART = new RiZoneSortie();
    A1LIB = new RiZoneSortie();
    scrollPane1 = new JScrollPane();
    WL501 = new XRiTable();
    label1 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(870, 420));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(630, 375));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("D\u00e9tail"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WH1TI ----
          WH1TI.setText("@WH1TI@");
          WH1TI.setName("WH1TI");
          panel1.add(WH1TI);
          WH1TI.setBounds(130, 47, 110, WH1TI.getPreferredSize().height);

          //---- CLNOM ----
          CLNOM.setText("@CLNOM@");
          CLNOM.setName("CLNOM");
          panel1.add(CLNOM);
          CLNOM.setBounds(243, 47, 310, CLNOM.getPreferredSize().height);

          //---- E1REP ----
          E1REP.setText("@E1REP@");
          E1REP.setName("E1REP");
          panel1.add(E1REP);
          E1REP.setBounds(130, 77, 34, E1REP.getPreferredSize().height);

          //---- RPLIB ----
          RPLIB.setText("@RPLIB@");
          RPLIB.setName("RPLIB");
          panel1.add(RPLIB);
          RPLIB.setBounds(167, 77, 320, RPLIB.getPreferredSize().height);

          //---- E1NCC ----
          E1NCC.setText("@E1NCC@");
          E1NCC.setName("E1NCC");
          panel1.add(E1NCC);
          E1NCC.setBounds(130, 107, 90, E1NCC.getPreferredSize().height);

          //---- E1RCC ----
          E1RCC.setText("@E1RCC@");
          E1RCC.setName("E1RCC");
          panel1.add(E1RCC);
          E1RCC.setBounds(223, 107, 264, E1RCC.getPreferredSize().height);

          //---- E1HOMX ----
          E1HOMX.setText("@E1HOMX@");
          E1HOMX.setName("E1HOMX");
          panel1.add(E1HOMX);
          E1HOMX.setBounds(130, 137, 90, E1HOMX.getPreferredSize().height);

          //---- E1EXPX ----
          E1EXPX.setText("@E1EXPX@");
          E1EXPX.setName("E1EXPX");
          panel1.add(E1EXPX);
          E1EXPX.setBounds(130, 167, 90, E1EXPX.getPreferredSize().height);

          //---- E1SAN ----
          E1SAN.setText("@E1SAN@");
          E1SAN.setName("E1SAN");
          panel1.add(E1SAN);
          E1SAN.setBounds(325, 137, 50, E1SAN.getPreferredSize().height);

          //---- E1FACX ----
          E1FACX.setText("@E1FACX@");
          E1FACX.setName("E1FACX");
          panel1.add(E1FACX);
          E1FACX.setBounds(130, 197, 90, E1FACX.getPreferredSize().height);

          //---- E1ACT ----
          E1ACT.setText("@E1ACT@");
          E1ACT.setName("E1ACT");
          panel1.add(E1ACT);
          E1ACT.setBounds(325, 167, 50, E1ACT.getPreferredSize().height);

          //---- E1NFA ----
          E1NFA.setText("@E1NFA@");
          E1NFA.setHorizontalAlignment(SwingConstants.RIGHT);
          E1NFA.setName("E1NFA");
          panel1.add(E1NFA);
          E1NFA.setBounds(130, 227, 66, E1NFA.getPreferredSize().height);

          //---- OBJ_33 ----
          OBJ_33.setText("Repr\u00e9sentant");
          OBJ_33.setName("OBJ_33");
          panel1.add(OBJ_33);
          OBJ_33.setBounds(28, 80, 97, 18);

          //---- OBJ_40 ----
          OBJ_40.setText("Commande");
          OBJ_40.setName("OBJ_40");
          panel1.add(OBJ_40);
          OBJ_40.setBounds(28, 140, 87, 18);

          //---- OBJ_48 ----
          OBJ_48.setText("Facturation");
          OBJ_48.setName("OBJ_48");
          panel1.add(OBJ_48);
          OBJ_48.setBounds(28, 200, 87, 18);

          //---- OBJ_36 ----
          OBJ_36.setText("R\u00e9f\u00e9rence");
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(28, 110, 82, 18);

          //---- OBJ_49 ----
          OBJ_49.setText("Num\u00e9ro facture");
          OBJ_49.setName("OBJ_49");
          panel1.add(OBJ_49);
          OBJ_49.setBounds(28, 230, 97, 18);

          //---- OBJ_44 ----
          OBJ_44.setText("Livraison");
          OBJ_44.setName("OBJ_44");
          panel1.add(OBJ_44);
          OBJ_44.setBounds(28, 170, 87, 18);

          //---- OBJ_51 ----
          OBJ_51.setText("Remises");
          OBJ_51.setName("OBJ_51");
          panel1.add(OBJ_51);
          OBJ_51.setBounds(260, 200, 65, 18);

          //---- OBJ_42 ----
          OBJ_42.setText("Section");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(260, 140, 65, 18);

          //---- L1REM1 ----
          L1REM1.setText("@L1REM1@");
          L1REM1.setName("L1REM1");
          panel1.add(L1REM1);
          L1REM1.setBounds(325, 197, 50, L1REM1.getPreferredSize().height);

          //---- L1REM2 ----
          L1REM2.setText("@L1REM2@");
          L1REM2.setName("L1REM2");
          panel1.add(L1REM2);
          L1REM2.setBounds(381, 197, 50, L1REM2.getPreferredSize().height);

          //---- L1REM3 ----
          L1REM3.setText("@L1REM3@");
          L1REM3.setName("L1REM3");
          panel1.add(L1REM3);
          L1REM3.setBounds(437, 197, 50, L1REM3.getPreferredSize().height);

          //---- L1REM4 ----
          L1REM4.setText("@L1REM4@");
          L1REM4.setName("L1REM4");
          panel1.add(L1REM4);
          L1REM4.setBounds(325, 227, 50, L1REM4.getPreferredSize().height);

          //---- L1REM5 ----
          L1REM5.setText("@L1REM5@");
          L1REM5.setName("L1REM5");
          panel1.add(L1REM5);
          L1REM5.setBounds(381, 227, 50, L1REM5.getPreferredSize().height);

          //---- L1REM6 ----
          L1REM6.setText("@L1REM6@");
          L1REM6.setName("L1REM6");
          panel1.add(L1REM6);
          L1REM6.setBounds(437, 227, 50, L1REM6.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Affaire");
          OBJ_46.setName("OBJ_46");
          panel1.add(OBJ_46);
          OBJ_46.setBounds(260, 170, 55, 18);

          //---- OBJ_30 ----
          OBJ_30.setText("Client");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(28, 50, 62, 18);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(25, 115, 650, 285);

        //---- A1ART ----
        A1ART.setText("@A1ART@");
        A1ART.setName("A1ART");
        p_contenu.add(A1ART);
        A1ART.setBounds(125, 25, 215, A1ART.getPreferredSize().height);

        //---- A1LIB ----
        A1LIB.setText("@A1LIB@");
        A1LIB.setName("A1LIB");
        p_contenu.add(A1LIB);
        A1LIB.setBounds(350, 25, 320, A1LIB.getPreferredSize().height);

        //======== scrollPane1 ========
        {
          scrollPane1.setName("scrollPane1");

          //---- WL501 ----
          WL501.setName("WL501");
          scrollPane1.setViewportView(WL501);
        }
        p_contenu.add(scrollPane1);
        scrollPane1.setBounds(25, 55, 650, 50);

        //---- label1 ----
        label1.setText("Article");
        label1.setName("label1");
        p_contenu.add(label1);
        label1.setBounds(50, 27, 75, 20);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie WH1TI;
  private RiZoneSortie CLNOM;
  private RiZoneSortie E1REP;
  private RiZoneSortie RPLIB;
  private RiZoneSortie E1NCC;
  private RiZoneSortie E1RCC;
  private RiZoneSortie E1HOMX;
  private RiZoneSortie E1EXPX;
  private RiZoneSortie E1SAN;
  private RiZoneSortie E1FACX;
  private RiZoneSortie E1ACT;
  private RiZoneSortie E1NFA;
  private JLabel OBJ_33;
  private JLabel OBJ_40;
  private JLabel OBJ_48;
  private JLabel OBJ_36;
  private JLabel OBJ_49;
  private JLabel OBJ_44;
  private JLabel OBJ_51;
  private JLabel OBJ_42;
  private RiZoneSortie L1REM1;
  private RiZoneSortie L1REM2;
  private RiZoneSortie L1REM3;
  private RiZoneSortie L1REM4;
  private RiZoneSortie L1REM5;
  private RiZoneSortie L1REM6;
  private JLabel OBJ_46;
  private JLabel OBJ_30;
  private RiZoneSortie A1ART;
  private RiZoneSortie A1LIB;
  private JScrollPane scrollPane1;
  private XRiTable WL501;
  private JLabel label1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
