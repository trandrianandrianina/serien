
package ri.serien.libecranrpg.vgvx.VGVX18FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX18FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private VGVX18FM_G01 g01 = null;
  
  public VGVX18FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Position article à venir @PG18LB@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNLIB@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIBR@")).trim()));
    M01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@M01@")).trim());
    M02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@M02@")).trim());
    M03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@M03@")).trim());
    M04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@M04@")).trim());
    M05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@M05@")).trim());
    M06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@M06@")).trim());
    M07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@M07@")).trim());
    M08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@M08@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    
    OBJ_227.setVisible(lexique.isTrue("95"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCJRP/+1/@ - POSITION ARTICLE A VENIR"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (g01 != null) {
      g01.reveiller();
    }
    else {
      g01 = new VGVX18FM_G01(this, lexique, interpreteurD);
    }
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_224ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(7, 2);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_225ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(11, 2);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_226ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 2);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_228ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 2);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_223ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(5, 2);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_39 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_41 = new JLabel();
    INDMAG = new XRiTextField();
    OBJ_42 = new JLabel();
    INDART = new XRiTextField();
    OBJ_45 = new JLabel();
    A1UNS = new XRiTextField();
    OBJ_46 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_227 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_224 = new SNBoutonDetail();
    OBJ_225 = new SNBoutonDetail();
    OBJ_226 = new SNBoutonDetail();
    OBJ_228 = new SNBoutonDetail();
    OBJ_223 = new SNBoutonDetail();
    M01 = new RiZoneSortie();
    LD011 = new XRiTextField();
    LD021 = new XRiTextField();
    LD031 = new XRiTextField();
    LD041 = new XRiTextField();
    LD051 = new XRiTextField();
    LD061 = new XRiTextField();
    LD071 = new XRiTextField();
    LD081 = new XRiTextField();
    LD091 = new XRiTextField();
    LD101 = new XRiTextField();
    LD111 = new XRiTextField();
    LD121 = new XRiTextField();
    LD131 = new XRiTextField();
    LD141 = new XRiTextField();
    LD151 = new XRiTextField();
    LD161 = new XRiTextField();
    LD171 = new XRiTextField();
    M02 = new RiZoneSortie();
    LD012 = new XRiTextField();
    LD022 = new XRiTextField();
    LD032 = new XRiTextField();
    LD042 = new XRiTextField();
    LD052 = new XRiTextField();
    LD062 = new XRiTextField();
    LD072 = new XRiTextField();
    LD082 = new XRiTextField();
    LD092 = new XRiTextField();
    LD102 = new XRiTextField();
    LD112 = new XRiTextField();
    LD122 = new XRiTextField();
    LD132 = new XRiTextField();
    LD142 = new XRiTextField();
    LD152 = new XRiTextField();
    LD162 = new XRiTextField();
    LD172 = new XRiTextField();
    M03 = new RiZoneSortie();
    LD013 = new XRiTextField();
    LD023 = new XRiTextField();
    LD033 = new XRiTextField();
    LD043 = new XRiTextField();
    LD053 = new XRiTextField();
    LD063 = new XRiTextField();
    LD073 = new XRiTextField();
    LD083 = new XRiTextField();
    LD093 = new XRiTextField();
    LD103 = new XRiTextField();
    LD113 = new XRiTextField();
    LD123 = new XRiTextField();
    LD133 = new XRiTextField();
    LD143 = new XRiTextField();
    LD153 = new XRiTextField();
    LD163 = new XRiTextField();
    LD173 = new XRiTextField();
    M04 = new RiZoneSortie();
    LD014 = new XRiTextField();
    LD024 = new XRiTextField();
    LD034 = new XRiTextField();
    LD044 = new XRiTextField();
    LD054 = new XRiTextField();
    LD064 = new XRiTextField();
    LD074 = new XRiTextField();
    LD084 = new XRiTextField();
    LD094 = new XRiTextField();
    LD104 = new XRiTextField();
    LD114 = new XRiTextField();
    LD124 = new XRiTextField();
    LD134 = new XRiTextField();
    LD144 = new XRiTextField();
    LD154 = new XRiTextField();
    LD164 = new XRiTextField();
    LD174 = new XRiTextField();
    M05 = new RiZoneSortie();
    LD015 = new XRiTextField();
    LD025 = new XRiTextField();
    LD035 = new XRiTextField();
    LD045 = new XRiTextField();
    LD055 = new XRiTextField();
    LD065 = new XRiTextField();
    LD075 = new XRiTextField();
    LD085 = new XRiTextField();
    LD095 = new XRiTextField();
    LD105 = new XRiTextField();
    LD115 = new XRiTextField();
    LD125 = new XRiTextField();
    LD135 = new XRiTextField();
    LD145 = new XRiTextField();
    LD155 = new XRiTextField();
    LD165 = new XRiTextField();
    LD175 = new XRiTextField();
    M06 = new RiZoneSortie();
    LD016 = new XRiTextField();
    LD026 = new XRiTextField();
    LD036 = new XRiTextField();
    LD046 = new XRiTextField();
    LD056 = new XRiTextField();
    LD066 = new XRiTextField();
    LD076 = new XRiTextField();
    LD086 = new XRiTextField();
    LD096 = new XRiTextField();
    LD106 = new XRiTextField();
    LD116 = new XRiTextField();
    LD126 = new XRiTextField();
    LD136 = new XRiTextField();
    LD146 = new XRiTextField();
    LD156 = new XRiTextField();
    LD166 = new XRiTextField();
    LD176 = new XRiTextField();
    M07 = new RiZoneSortie();
    LD017 = new XRiTextField();
    LD027 = new XRiTextField();
    LD037 = new XRiTextField();
    LD047 = new XRiTextField();
    LD057 = new XRiTextField();
    LD067 = new XRiTextField();
    LD077 = new XRiTextField();
    LD087 = new XRiTextField();
    LD097 = new XRiTextField();
    LD107 = new XRiTextField();
    LD117 = new XRiTextField();
    LD127 = new XRiTextField();
    LD137 = new XRiTextField();
    LD147 = new XRiTextField();
    LD157 = new XRiTextField();
    LD167 = new XRiTextField();
    LD177 = new XRiTextField();
    M08 = new RiZoneSortie();
    LD018 = new XRiTextField();
    LD028 = new XRiTextField();
    LD038 = new XRiTextField();
    LD048 = new XRiTextField();
    LD058 = new XRiTextField();
    LD068 = new XRiTextField();
    LD078 = new XRiTextField();
    LD088 = new XRiTextField();
    LD098 = new XRiTextField();
    LD108 = new XRiTextField();
    LD118 = new XRiTextField();
    LD128 = new XRiTextField();
    LD138 = new XRiTextField();
    LD148 = new XRiTextField();
    LD158 = new XRiTextField();
    LD168 = new XRiTextField();
    LD178 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label9 = new JLabel();
    label8 = new JLabel();
    label7 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Position article \u00e0 venir @PG18LB@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(850, 40));
          p_tete_gauche.setMinimumSize(new Dimension(850, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_39 ----
          OBJ_39.setText("Etablissement");
          OBJ_39.setName("OBJ_39");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_41 ----
          OBJ_41.setText("Magasin");
          OBJ_41.setName("OBJ_41");

          //---- INDMAG ----
          INDMAG.setComponentPopupMenu(BTD);
          INDMAG.setName("INDMAG");

          //---- OBJ_42 ----
          OBJ_42.setText("Article");
          OBJ_42.setName("OBJ_42");

          //---- INDART ----
          INDART.setComponentPopupMenu(BTD);
          INDART.setName("INDART");

          //---- OBJ_45 ----
          OBJ_45.setText("Unit\u00e9 de stock");
          OBJ_45.setName("OBJ_45");

          //---- A1UNS ----
          A1UNS.setName("A1UNS");

          //---- OBJ_46 ----
          OBJ_46.setText("@UNLIB@");
          OBJ_46.setOpaque(false);
          OBJ_46.setName("OBJ_46");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDMAG, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDART, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(A1UNS, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(OBJ_39))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(OBJ_41))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDMAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(OBJ_42))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(A1UNS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_227 ----
          OBJ_227.setText("Ordres de fabrication pr\u00e9visionnels pr\u00e9sents");
          OBJ_227.setName("OBJ_227");
          p_tete_droite.add(OBJ_227);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("D\u00e9filement de mois");
              riSousMenu_bt6.setToolTipText("D\u00e9filement de mois (de 2 en 2)");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Graphes statistiques");
              riSousMenu_bt7.setToolTipText("Graphe statistique");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Ordres de fabrication");
              riSousMenu_bt8.setToolTipText("Ordres de fabrication pr\u00e9visionnels (ON)");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("@A1LIBR@"));
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- OBJ_224 ----
            OBJ_224.setText("");
            OBJ_224.setToolTipText("Pr\u00e9visions de consommation");
            OBJ_224.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_224.setContentAreaFilled(true);
            OBJ_224.setBackground(new Color(239, 239, 222));
            OBJ_224.setName("OBJ_224");
            OBJ_224.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_224ActionPerformed(e);
              }
            });

            //---- OBJ_225 ----
            OBJ_225.setText("");
            OBJ_225.setToolTipText("Command\u00e9");
            OBJ_225.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_225.setContentAreaFilled(true);
            OBJ_225.setBackground(new Color(239, 239, 222));
            OBJ_225.setName("OBJ_225");
            OBJ_225.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_225ActionPerformed(e);
              }
            });

            //---- OBJ_226 ----
            OBJ_226.setText("");
            OBJ_226.setToolTipText("Attendu");
            OBJ_226.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_226.setContentAreaFilled(true);
            OBJ_226.setBackground(new Color(239, 239, 222));
            OBJ_226.setName("OBJ_226");
            OBJ_226.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_226ActionPerformed(e);
              }
            });

            //---- OBJ_228 ----
            OBJ_228.setText("");
            OBJ_228.setToolTipText("Attendu/Command\u00e9");
            OBJ_228.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_228.setContentAreaFilled(true);
            OBJ_228.setBackground(new Color(239, 239, 222));
            OBJ_228.setName("OBJ_228");
            OBJ_228.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_228ActionPerformed(e);
              }
            });

            //---- OBJ_223 ----
            OBJ_223.setText("");
            OBJ_223.setToolTipText("Stocks");
            OBJ_223.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_223.setContentAreaFilled(true);
            OBJ_223.setBackground(new Color(239, 239, 222));
            OBJ_223.setName("OBJ_223");
            OBJ_223.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_223ActionPerformed(e);
              }
            });

            //---- M01 ----
            M01.setHorizontalAlignment(SwingConstants.CENTER);
            M01.setFont(M01.getFont().deriveFont(M01.getFont().getStyle() | Font.BOLD));
            M01.setText("@M01@");
            M01.setName("M01");

            //---- LD011 ----
            LD011.setFont(LD011.getFont().deriveFont(LD011.getFont().getStyle() | Font.BOLD));
            LD011.setName("LD011");

            //---- LD021 ----
            LD021.setName("LD021");

            //---- LD031 ----
            LD031.setName("LD031");

            //---- LD041 ----
            LD041.setName("LD041");

            //---- LD051 ----
            LD051.setName("LD051");

            //---- LD061 ----
            LD061.setFont(LD061.getFont().deriveFont(LD061.getFont().getStyle() | Font.BOLD));
            LD061.setName("LD061");

            //---- LD071 ----
            LD071.setName("LD071");

            //---- LD081 ----
            LD081.setName("LD081");

            //---- LD091 ----
            LD091.setName("LD091");

            //---- LD101 ----
            LD101.setFont(LD101.getFont().deriveFont(LD101.getFont().getStyle() | Font.BOLD));
            LD101.setName("LD101");

            //---- LD111 ----
            LD111.setName("LD111");

            //---- LD121 ----
            LD121.setName("LD121");

            //---- LD131 ----
            LD131.setName("LD131");

            //---- LD141 ----
            LD141.setFont(LD141.getFont().deriveFont(LD141.getFont().getStyle() | Font.BOLD));
            LD141.setName("LD141");

            //---- LD151 ----
            LD151.setName("LD151");

            //---- LD161 ----
            LD161.setName("LD161");

            //---- LD171 ----
            LD171.setFont(LD171.getFont().deriveFont(LD171.getFont().getStyle() | Font.BOLD));
            LD171.setName("LD171");

            //---- M02 ----
            M02.setHorizontalAlignment(SwingConstants.CENTER);
            M02.setFont(M02.getFont().deriveFont(M02.getFont().getStyle() | Font.BOLD));
            M02.setText("@M02@");
            M02.setName("M02");

            //---- LD012 ----
            LD012.setFont(LD012.getFont().deriveFont(LD012.getFont().getStyle() | Font.BOLD));
            LD012.setName("LD012");

            //---- LD022 ----
            LD022.setName("LD022");

            //---- LD032 ----
            LD032.setName("LD032");

            //---- LD042 ----
            LD042.setName("LD042");

            //---- LD052 ----
            LD052.setName("LD052");

            //---- LD062 ----
            LD062.setFont(LD062.getFont().deriveFont(LD062.getFont().getStyle() | Font.BOLD));
            LD062.setName("LD062");

            //---- LD072 ----
            LD072.setName("LD072");

            //---- LD082 ----
            LD082.setName("LD082");

            //---- LD092 ----
            LD092.setName("LD092");

            //---- LD102 ----
            LD102.setFont(LD102.getFont().deriveFont(LD102.getFont().getStyle() | Font.BOLD));
            LD102.setName("LD102");

            //---- LD112 ----
            LD112.setName("LD112");

            //---- LD122 ----
            LD122.setName("LD122");

            //---- LD132 ----
            LD132.setName("LD132");

            //---- LD142 ----
            LD142.setFont(LD142.getFont().deriveFont(LD142.getFont().getStyle() | Font.BOLD));
            LD142.setName("LD142");

            //---- LD152 ----
            LD152.setName("LD152");

            //---- LD162 ----
            LD162.setName("LD162");

            //---- LD172 ----
            LD172.setFont(LD172.getFont().deriveFont(LD172.getFont().getStyle() | Font.BOLD));
            LD172.setName("LD172");

            //---- M03 ----
            M03.setHorizontalAlignment(SwingConstants.CENTER);
            M03.setFont(M03.getFont().deriveFont(M03.getFont().getStyle() | Font.BOLD));
            M03.setText("@M03@");
            M03.setName("M03");

            //---- LD013 ----
            LD013.setFont(LD013.getFont().deriveFont(LD013.getFont().getStyle() | Font.BOLD));
            LD013.setName("LD013");

            //---- LD023 ----
            LD023.setName("LD023");

            //---- LD033 ----
            LD033.setName("LD033");

            //---- LD043 ----
            LD043.setName("LD043");

            //---- LD053 ----
            LD053.setName("LD053");

            //---- LD063 ----
            LD063.setFont(LD063.getFont().deriveFont(LD063.getFont().getStyle() | Font.BOLD));
            LD063.setName("LD063");

            //---- LD073 ----
            LD073.setName("LD073");

            //---- LD083 ----
            LD083.setName("LD083");

            //---- LD093 ----
            LD093.setName("LD093");

            //---- LD103 ----
            LD103.setFont(LD103.getFont().deriveFont(LD103.getFont().getStyle() | Font.BOLD));
            LD103.setName("LD103");

            //---- LD113 ----
            LD113.setName("LD113");

            //---- LD123 ----
            LD123.setName("LD123");

            //---- LD133 ----
            LD133.setName("LD133");

            //---- LD143 ----
            LD143.setFont(LD143.getFont().deriveFont(LD143.getFont().getStyle() | Font.BOLD));
            LD143.setName("LD143");

            //---- LD153 ----
            LD153.setName("LD153");

            //---- LD163 ----
            LD163.setName("LD163");

            //---- LD173 ----
            LD173.setFont(LD173.getFont().deriveFont(LD173.getFont().getStyle() | Font.BOLD));
            LD173.setName("LD173");

            //---- M04 ----
            M04.setHorizontalAlignment(SwingConstants.CENTER);
            M04.setFont(M04.getFont().deriveFont(M04.getFont().getStyle() | Font.BOLD));
            M04.setText("@M04@");
            M04.setName("M04");

            //---- LD014 ----
            LD014.setFont(LD014.getFont().deriveFont(LD014.getFont().getStyle() | Font.BOLD));
            LD014.setName("LD014");

            //---- LD024 ----
            LD024.setName("LD024");

            //---- LD034 ----
            LD034.setName("LD034");

            //---- LD044 ----
            LD044.setName("LD044");

            //---- LD054 ----
            LD054.setName("LD054");

            //---- LD064 ----
            LD064.setFont(LD064.getFont().deriveFont(LD064.getFont().getStyle() | Font.BOLD));
            LD064.setName("LD064");

            //---- LD074 ----
            LD074.setName("LD074");

            //---- LD084 ----
            LD084.setName("LD084");

            //---- LD094 ----
            LD094.setName("LD094");

            //---- LD104 ----
            LD104.setFont(LD104.getFont().deriveFont(LD104.getFont().getStyle() | Font.BOLD));
            LD104.setName("LD104");

            //---- LD114 ----
            LD114.setName("LD114");

            //---- LD124 ----
            LD124.setName("LD124");

            //---- LD134 ----
            LD134.setName("LD134");

            //---- LD144 ----
            LD144.setFont(LD144.getFont().deriveFont(LD144.getFont().getStyle() | Font.BOLD));
            LD144.setName("LD144");

            //---- LD154 ----
            LD154.setName("LD154");

            //---- LD164 ----
            LD164.setName("LD164");

            //---- LD174 ----
            LD174.setFont(LD174.getFont().deriveFont(LD174.getFont().getStyle() | Font.BOLD));
            LD174.setName("LD174");

            //---- M05 ----
            M05.setHorizontalAlignment(SwingConstants.CENTER);
            M05.setFont(M05.getFont().deriveFont(M05.getFont().getStyle() | Font.BOLD));
            M05.setText("@M05@");
            M05.setName("M05");

            //---- LD015 ----
            LD015.setFont(LD015.getFont().deriveFont(LD015.getFont().getStyle() | Font.BOLD));
            LD015.setName("LD015");

            //---- LD025 ----
            LD025.setName("LD025");

            //---- LD035 ----
            LD035.setName("LD035");

            //---- LD045 ----
            LD045.setName("LD045");

            //---- LD055 ----
            LD055.setName("LD055");

            //---- LD065 ----
            LD065.setFont(LD065.getFont().deriveFont(LD065.getFont().getStyle() | Font.BOLD));
            LD065.setName("LD065");

            //---- LD075 ----
            LD075.setName("LD075");

            //---- LD085 ----
            LD085.setName("LD085");

            //---- LD095 ----
            LD095.setName("LD095");

            //---- LD105 ----
            LD105.setFont(LD105.getFont().deriveFont(LD105.getFont().getStyle() | Font.BOLD));
            LD105.setName("LD105");

            //---- LD115 ----
            LD115.setName("LD115");

            //---- LD125 ----
            LD125.setName("LD125");

            //---- LD135 ----
            LD135.setName("LD135");

            //---- LD145 ----
            LD145.setFont(LD145.getFont().deriveFont(LD145.getFont().getStyle() | Font.BOLD));
            LD145.setName("LD145");

            //---- LD155 ----
            LD155.setName("LD155");

            //---- LD165 ----
            LD165.setName("LD165");

            //---- LD175 ----
            LD175.setFont(LD175.getFont().deriveFont(LD175.getFont().getStyle() | Font.BOLD));
            LD175.setName("LD175");

            //---- M06 ----
            M06.setHorizontalAlignment(SwingConstants.CENTER);
            M06.setFont(M06.getFont().deriveFont(M06.getFont().getStyle() | Font.BOLD));
            M06.setText("@M06@");
            M06.setName("M06");

            //---- LD016 ----
            LD016.setFont(LD016.getFont().deriveFont(LD016.getFont().getStyle() | Font.BOLD));
            LD016.setName("LD016");

            //---- LD026 ----
            LD026.setName("LD026");

            //---- LD036 ----
            LD036.setName("LD036");

            //---- LD046 ----
            LD046.setName("LD046");

            //---- LD056 ----
            LD056.setName("LD056");

            //---- LD066 ----
            LD066.setFont(LD066.getFont().deriveFont(LD066.getFont().getStyle() | Font.BOLD));
            LD066.setName("LD066");

            //---- LD076 ----
            LD076.setName("LD076");

            //---- LD086 ----
            LD086.setName("LD086");

            //---- LD096 ----
            LD096.setName("LD096");

            //---- LD106 ----
            LD106.setFont(LD106.getFont().deriveFont(LD106.getFont().getStyle() | Font.BOLD));
            LD106.setName("LD106");

            //---- LD116 ----
            LD116.setName("LD116");

            //---- LD126 ----
            LD126.setName("LD126");

            //---- LD136 ----
            LD136.setName("LD136");

            //---- LD146 ----
            LD146.setFont(LD146.getFont().deriveFont(LD146.getFont().getStyle() | Font.BOLD));
            LD146.setName("LD146");

            //---- LD156 ----
            LD156.setName("LD156");

            //---- LD166 ----
            LD166.setName("LD166");

            //---- LD176 ----
            LD176.setFont(LD176.getFont().deriveFont(LD176.getFont().getStyle() | Font.BOLD));
            LD176.setName("LD176");

            //---- M07 ----
            M07.setHorizontalAlignment(SwingConstants.CENTER);
            M07.setFont(M07.getFont().deriveFont(M07.getFont().getStyle() | Font.BOLD));
            M07.setText("@M07@");
            M07.setName("M07");

            //---- LD017 ----
            LD017.setFont(LD017.getFont().deriveFont(LD017.getFont().getStyle() | Font.BOLD));
            LD017.setName("LD017");

            //---- LD027 ----
            LD027.setName("LD027");

            //---- LD037 ----
            LD037.setName("LD037");

            //---- LD047 ----
            LD047.setName("LD047");

            //---- LD057 ----
            LD057.setName("LD057");

            //---- LD067 ----
            LD067.setFont(LD067.getFont().deriveFont(LD067.getFont().getStyle() | Font.BOLD));
            LD067.setName("LD067");

            //---- LD077 ----
            LD077.setName("LD077");

            //---- LD087 ----
            LD087.setName("LD087");

            //---- LD097 ----
            LD097.setName("LD097");

            //---- LD107 ----
            LD107.setFont(LD107.getFont().deriveFont(LD107.getFont().getStyle() | Font.BOLD));
            LD107.setName("LD107");

            //---- LD117 ----
            LD117.setName("LD117");

            //---- LD127 ----
            LD127.setName("LD127");

            //---- LD137 ----
            LD137.setName("LD137");

            //---- LD147 ----
            LD147.setFont(LD147.getFont().deriveFont(LD147.getFont().getStyle() | Font.BOLD));
            LD147.setName("LD147");

            //---- LD157 ----
            LD157.setName("LD157");

            //---- LD167 ----
            LD167.setName("LD167");

            //---- LD177 ----
            LD177.setFont(LD177.getFont().deriveFont(LD177.getFont().getStyle() | Font.BOLD));
            LD177.setName("LD177");

            //---- M08 ----
            M08.setHorizontalAlignment(SwingConstants.CENTER);
            M08.setFont(M08.getFont().deriveFont(M08.getFont().getStyle() | Font.BOLD));
            M08.setText("@M08@");
            M08.setName("M08");

            //---- LD018 ----
            LD018.setFont(LD018.getFont().deriveFont(LD018.getFont().getStyle() | Font.BOLD));
            LD018.setName("LD018");

            //---- LD028 ----
            LD028.setName("LD028");

            //---- LD038 ----
            LD038.setName("LD038");

            //---- LD048 ----
            LD048.setName("LD048");

            //---- LD058 ----
            LD058.setName("LD058");

            //---- LD068 ----
            LD068.setFont(LD068.getFont().deriveFont(LD068.getFont().getStyle() | Font.BOLD));
            LD068.setName("LD068");

            //---- LD078 ----
            LD078.setName("LD078");

            //---- LD088 ----
            LD088.setName("LD088");

            //---- LD098 ----
            LD098.setName("LD098");

            //---- LD108 ----
            LD108.setFont(LD108.getFont().deriveFont(LD108.getFont().getStyle() | Font.BOLD));
            LD108.setName("LD108");

            //---- LD118 ----
            LD118.setName("LD118");

            //---- LD128 ----
            LD128.setName("LD128");

            //---- LD138 ----
            LD138.setName("LD138");

            //---- LD148 ----
            LD148.setFont(LD148.getFont().deriveFont(LD148.getFont().getStyle() | Font.BOLD));
            LD148.setName("LD148");

            //---- LD158 ----
            LD158.setName("LD158");

            //---- LD168 ----
            LD168.setName("LD168");

            //---- LD178 ----
            LD178.setFont(LD178.getFont().deriveFont(LD178.getFont().getStyle() | Font.BOLD));
            LD178.setName("LD178");

            //---- label1 ----
            label1.setText("Stock disponible");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 2f));
            label1.setName("label1");

            //---- label2 ----
            label2.setText("Total des pr\u00e9visions");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD, label2.getFont().getSize() + 2f));
            label2.setName("label2");

            //---- label3 ----
            label3.setText("Total des commandes");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD, label3.getFont().getSize() + 2f));
            label3.setName("label3");

            //---- label4 ----
            label4.setText("Total attendu");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD, label4.getFont().getSize() + 2f));
            label4.setName("label4");

            //---- label5 ----
            label5.setText("BESOINS TOTAUX");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD, label5.getFont().getSize() + 2f));
            label5.setName("label5");

            //---- label6 ----
            label6.setText("Pr\u00e9vision mensuelle");
            label6.setName("label6");

            //---- label9 ----
            label9.setText(" pr\u00e9vision d\u00e9cade 3");
            label9.setName("label9");

            //---- label8 ----
            label8.setText(" pr\u00e9vision d\u00e9cade 2");
            label8.setName("label8");

            //---- label7 ----
            label7.setText("ou pr\u00e9vision d\u00e9cade 1");
            label7.setName("label7");

            //---- label10 ----
            label10.setText("r\u00e9serv\u00e9 d\u00e9cade 1");
            label10.setName("label10");

            //---- label11 ----
            label11.setText("r\u00e9serv\u00e9 d\u00e9cade 2");
            label11.setName("label11");

            //---- label12 ----
            label12.setText("r\u00e9serv\u00e9 d\u00e9cade 3");
            label12.setName("label12");

            //---- label13 ----
            label13.setText("attendu d\u00e9cade 1");
            label13.setName("label13");

            //---- label14 ----
            label14.setText("attendu d\u00e9cade 2");
            label14.setName("label14");

            //---- label15 ----
            label15.setText("attendu d\u00e9cade 3");
            label15.setName("label15");

            //---- label16 ----
            label16.setText("besoins fin d\u00e9cade 1");
            label16.setName("label16");

            //---- label17 ----
            label17.setText("besoins fin d\u00e9cade 2");
            label17.setName("label17");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(244, 244, 244)
                  .addComponent(M01, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGap(13, 13, 13)
                  .addComponent(M02, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGap(13, 13, 13)
                  .addComponent(M03, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGap(13, 13, 13)
                  .addComponent(M04, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGap(13, 13, 13)
                  .addComponent(M05, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGap(13, 13, 13)
                  .addComponent(M06, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGap(13, 13, 13)
                  .addComponent(M07, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGap(13, 13, 13)
                  .addComponent(M08, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(34, 34, 34)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(OBJ_223, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_224, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_225, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(label1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(label6, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
                    .addComponent(label7, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(label8, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(label9, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(label2, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(label10, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(label11, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(15, 15, 15)
                      .addComponent(label12, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(label3, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD011, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD021, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD031, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD041, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD051, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD061, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD071, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD081, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD091, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD101, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD012, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD022, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD032, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD042, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD052, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD062, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD072, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD082, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD092, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD102, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD013, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD023, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD033, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD043, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD053, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD063, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD073, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD083, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD093, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD103, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD014, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD024, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD034, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD044, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD054, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD064, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD074, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD084, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD094, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD104, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD015, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD025, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD035, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD045, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD055, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD065, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD075, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD085, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD095, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD105, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD016, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD026, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD036, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD046, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD056, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD066, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD076, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD086, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD096, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD106, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD017, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD027, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD037, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD047, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD057, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD067, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD077, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD087, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD097, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD107, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD018, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD028, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD038, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD048, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD058, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD068, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD078, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD088, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD098, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD108, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(34, 34, 34)
                  .addComponent(OBJ_226, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(label4, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(label13, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label14, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label15, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD111, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD121, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD131, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD141, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD112, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD122, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD132, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD142, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD113, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD123, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD133, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD143, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD114, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD124, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD134, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD144, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD115, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD125, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD135, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD145, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD116, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD126, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD136, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD146, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD117, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD127, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD137, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD147, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD118, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD128, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD138, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD148, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(34, 34, 34)
                  .addComponent(OBJ_228, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(label5, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(10, 10, 10)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(label16, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label17, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD151, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD161, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD171, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD152, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD162, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD172, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD153, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD163, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD173, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD154, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD164, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD174, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD155, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD165, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD175, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD156, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD166, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD176, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD157, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD167, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD177, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE))
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(LD158, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD168, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addComponent(LD178, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(M01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(M02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(M03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(M04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(M05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(M06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(M07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(M08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(4, 4, 4)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(OBJ_223, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_224, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(OBJ_225, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(label1)
                      .addGap(10, 10, 10)
                      .addComponent(label6)
                      .addGap(12, 12, 12)
                      .addComponent(label7)
                      .addGap(12, 12, 12)
                      .addComponent(label8)
                      .addGap(12, 12, 12)
                      .addComponent(label9)
                      .addGap(11, 11, 11)
                      .addComponent(label2)
                      .addGap(10, 10, 10)
                      .addComponent(label10)
                      .addGap(12, 12, 12)
                      .addComponent(label11)
                      .addGap(12, 12, 12)
                      .addComponent(label12)
                      .addGap(11, 11, 11)
                      .addComponent(label3))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD011, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD021, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD031, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD041, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD051, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD061, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD071, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD081, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD091, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD101, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD012, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD022, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD032, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD042, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD052, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD062, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD072, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD082, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD092, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD102, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD013, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD023, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD033, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD043, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD053, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD063, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD073, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD083, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD093, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD103, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD014, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD024, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD034, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD044, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD054, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD064, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD074, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD084, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD094, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD104, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD015, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD025, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD035, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD045, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD055, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD065, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD075, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD085, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD095, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD105, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD016, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD026, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD036, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD046, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD056, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD066, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD076, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD086, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD096, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD106, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD017, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD027, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD037, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD047, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD057, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD067, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD077, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD087, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD097, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD107, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD018, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD028, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD038, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD048, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD058, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD068, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD078, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD088, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD098, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD108, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_226, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label13)
                      .addGap(12, 12, 12)
                      .addComponent(label14)
                      .addGap(12, 12, 12)
                      .addComponent(label15)
                      .addGap(11, 11, 11)
                      .addComponent(label4))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD111, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD121, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD131, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD141, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD112, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD122, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD132, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD142, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD113, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD123, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD133, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD143, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD114, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD124, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD134, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD144, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD115, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD125, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD135, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD145, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD116, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD126, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD136, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD146, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD117, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD127, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD137, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD147, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD118, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD128, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD138, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD148, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(OBJ_228, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label16)
                      .addGap(12, 12, 12)
                      .addComponent(label17)
                      .addGap(11, 11, 11)
                      .addComponent(label5))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD151, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD161, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD171, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD152, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD162, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD172, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD153, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD163, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD173, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD154, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD164, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD174, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD155, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD165, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD175, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD156, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD166, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD176, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD157, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD167, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD177, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(LD158, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD168, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(LD178, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(29, Short.MAX_VALUE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_39;
  private XRiTextField INDETB;
  private JLabel OBJ_41;
  private XRiTextField INDMAG;
  private JLabel OBJ_42;
  private XRiTextField INDART;
  private JLabel OBJ_45;
  private XRiTextField A1UNS;
  private RiZoneSortie OBJ_46;
  private JPanel p_tete_droite;
  private JLabel OBJ_227;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private SNBoutonDetail OBJ_224;
  private SNBoutonDetail OBJ_225;
  private SNBoutonDetail OBJ_226;
  private SNBoutonDetail OBJ_228;
  private SNBoutonDetail OBJ_223;
  private RiZoneSortie M01;
  private XRiTextField LD011;
  private XRiTextField LD021;
  private XRiTextField LD031;
  private XRiTextField LD041;
  private XRiTextField LD051;
  private XRiTextField LD061;
  private XRiTextField LD071;
  private XRiTextField LD081;
  private XRiTextField LD091;
  private XRiTextField LD101;
  private XRiTextField LD111;
  private XRiTextField LD121;
  private XRiTextField LD131;
  private XRiTextField LD141;
  private XRiTextField LD151;
  private XRiTextField LD161;
  private XRiTextField LD171;
  private RiZoneSortie M02;
  private XRiTextField LD012;
  private XRiTextField LD022;
  private XRiTextField LD032;
  private XRiTextField LD042;
  private XRiTextField LD052;
  private XRiTextField LD062;
  private XRiTextField LD072;
  private XRiTextField LD082;
  private XRiTextField LD092;
  private XRiTextField LD102;
  private XRiTextField LD112;
  private XRiTextField LD122;
  private XRiTextField LD132;
  private XRiTextField LD142;
  private XRiTextField LD152;
  private XRiTextField LD162;
  private XRiTextField LD172;
  private RiZoneSortie M03;
  private XRiTextField LD013;
  private XRiTextField LD023;
  private XRiTextField LD033;
  private XRiTextField LD043;
  private XRiTextField LD053;
  private XRiTextField LD063;
  private XRiTextField LD073;
  private XRiTextField LD083;
  private XRiTextField LD093;
  private XRiTextField LD103;
  private XRiTextField LD113;
  private XRiTextField LD123;
  private XRiTextField LD133;
  private XRiTextField LD143;
  private XRiTextField LD153;
  private XRiTextField LD163;
  private XRiTextField LD173;
  private RiZoneSortie M04;
  private XRiTextField LD014;
  private XRiTextField LD024;
  private XRiTextField LD034;
  private XRiTextField LD044;
  private XRiTextField LD054;
  private XRiTextField LD064;
  private XRiTextField LD074;
  private XRiTextField LD084;
  private XRiTextField LD094;
  private XRiTextField LD104;
  private XRiTextField LD114;
  private XRiTextField LD124;
  private XRiTextField LD134;
  private XRiTextField LD144;
  private XRiTextField LD154;
  private XRiTextField LD164;
  private XRiTextField LD174;
  private RiZoneSortie M05;
  private XRiTextField LD015;
  private XRiTextField LD025;
  private XRiTextField LD035;
  private XRiTextField LD045;
  private XRiTextField LD055;
  private XRiTextField LD065;
  private XRiTextField LD075;
  private XRiTextField LD085;
  private XRiTextField LD095;
  private XRiTextField LD105;
  private XRiTextField LD115;
  private XRiTextField LD125;
  private XRiTextField LD135;
  private XRiTextField LD145;
  private XRiTextField LD155;
  private XRiTextField LD165;
  private XRiTextField LD175;
  private RiZoneSortie M06;
  private XRiTextField LD016;
  private XRiTextField LD026;
  private XRiTextField LD036;
  private XRiTextField LD046;
  private XRiTextField LD056;
  private XRiTextField LD066;
  private XRiTextField LD076;
  private XRiTextField LD086;
  private XRiTextField LD096;
  private XRiTextField LD106;
  private XRiTextField LD116;
  private XRiTextField LD126;
  private XRiTextField LD136;
  private XRiTextField LD146;
  private XRiTextField LD156;
  private XRiTextField LD166;
  private XRiTextField LD176;
  private RiZoneSortie M07;
  private XRiTextField LD017;
  private XRiTextField LD027;
  private XRiTextField LD037;
  private XRiTextField LD047;
  private XRiTextField LD057;
  private XRiTextField LD067;
  private XRiTextField LD077;
  private XRiTextField LD087;
  private XRiTextField LD097;
  private XRiTextField LD107;
  private XRiTextField LD117;
  private XRiTextField LD127;
  private XRiTextField LD137;
  private XRiTextField LD147;
  private XRiTextField LD157;
  private XRiTextField LD167;
  private XRiTextField LD177;
  private RiZoneSortie M08;
  private XRiTextField LD018;
  private XRiTextField LD028;
  private XRiTextField LD038;
  private XRiTextField LD048;
  private XRiTextField LD058;
  private XRiTextField LD068;
  private XRiTextField LD078;
  private XRiTextField LD088;
  private XRiTextField LD098;
  private XRiTextField LD108;
  private XRiTextField LD118;
  private XRiTextField LD128;
  private XRiTextField LD138;
  private XRiTextField LD148;
  private XRiTextField LD158;
  private XRiTextField LD168;
  private XRiTextField LD178;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label9;
  private JLabel label8;
  private JLabel label7;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label17;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
