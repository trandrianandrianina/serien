
package ri.serien.libecranrpg.vgvx.VGVX40FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVX40FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVX40FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    ELART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELART@")).trim());
    CLNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    CLTEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLTEL@")).trim());
    DATBON.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DATBON@")).trim());
    E1FAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1FAC@")).trim());
    NUMBON.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUMBON@")).trim());
    CLCLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCLI@")).trim());
    NUMDEV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUMDEV@")).trim());
    CLLIV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLLIV@")).trim());
    SUFBON.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SUFBON@")).trim());
    WZP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP1@")).trim());
    ERZP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    WZP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP2@")).trim());
    WZP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP3@")).trim());
    WZP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP4@")).trim());
    WZP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP5@")).trim());
    WZP6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP6@")).trim());
    WZP7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP7@")).trim());
    WZP8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP8@")).trim());
    ERZP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    ERZP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    ERZP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    ERZP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    ERZP6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    ERZP7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    ERZP8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    SUFBON.setVisible(lexique.isPresent("SUFBON"));
    CLLIV.setVisible(lexique.isPresent("CLLIV"));
    NUMDEV.setVisible(lexique.isPresent("NUMDEV"));
    CLCLI.setVisible(lexique.isPresent("CLCLI"));
    NUMBON.setVisible(lexique.isPresent("NUMBON"));
    E1FAC.setVisible(lexique.isPresent("E1FAC"));
    ELART.setVisible(lexique.isPresent("ELART"));
    
    WZP1.setVisible(!lexique.HostFieldGetData("WZP1").trim().equals(""));
    WZP2.setVisible(!lexique.HostFieldGetData("WZP2").trim().equals(""));
    WZP3.setVisible(!lexique.HostFieldGetData("WZP3").trim().equals(""));
    WZP4.setVisible(!lexique.HostFieldGetData("WZP4").trim().equals(""));
    WZP5.setVisible(!lexique.HostFieldGetData("WZP5").trim().equals(""));
    WZP6.setVisible(!lexique.HostFieldGetData("WZP6").trim().equals(""));
    WZP7.setVisible(!lexique.HostFieldGetData("WZP7").trim().equals(""));
    WZP8.setVisible(!lexique.HostFieldGetData("WZP8").trim().equals(""));
    
    ELZP1.setVisible(WZP1.isVisible());
    ELZP2.setVisible(WZP2.isVisible());
    ELZP3.setVisible(WZP3.isVisible());
    ELZP4.setVisible(WZP4.isVisible());
    ELZP5.setVisible(WZP5.isVisible());
    ELZP6.setVisible(WZP6.isVisible());
    ELZP7.setVisible(WZP7.isVisible());
    ELZP8.setVisible(WZP8.isVisible());
    
    ERZP1.setVisible(WZP1.isVisible());
    ERZP2.setVisible(WZP2.isVisible());
    ERZP3.setVisible(WZP3.isVisible());
    ERZP4.setVisible(WZP4.isVisible());
    ERZP5.setVisible(WZP5.isVisible());
    ERZP6.setVisible(WZP6.isVisible());
    ERZP7.setVisible(WZP7.isVisible());
    ERZP8.setVisible(WZP8.isVisible());
    
    panel2.setVisible(WZP1.isVisible() || WZP2.isVisible() || WZP3.isVisible() || WZP4.isVisible() || WZP5.isVisible() || WZP6.isVisible()
        || WZP7.isVisible() || WZP8.isVisible());
    OBJ_96.setVisible(!panel2.isVisible());
    
    panel3.setVisible(lexique.isTrue("81"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Extension ligne commerciale"));
    
    

    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("TCI1", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void riBoutonDetail2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("TCI2", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void riBoutonDetail3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("TCI3", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_96 = new JLabel();
    panel1 = new JPanel();
    A1LIB = new RiZoneSortie();
    ELART = new RiZoneSortie();
    label1 = new JLabel();
    riBoutonDetail1 = new SNBoutonDetail();
    panel3 = new JPanel();
    CLNOM = new RiZoneSortie();
    CLTEL = new RiZoneSortie();
    DATBON = new RiZoneSortie();
    E1FAC = new RiZoneSortie();
    NUMBON = new RiZoneSortie();
    OBJ_63 = new JLabel();
    CLCLI = new RiZoneSortie();
    NUMDEV = new RiZoneSortie();
    OBJ_61 = new JLabel();
    CLLIV = new RiZoneSortie();
    OBJ_59 = new JLabel();
    SUFBON = new RiZoneSortie();
    label2 = new JLabel();
    label3 = new JLabel();
    riBoutonDetail2 = new SNBoutonDetail();
    riBoutonDetail3 = new SNBoutonDetail();
    label4 = new JLabel();
    panel2 = new JPanel();
    WZP1 = new JLabel();
    ELZP1 = new XRiTextField();
    ERZP1 = new RiZoneSortie();
    ELZP2 = new XRiTextField();
    ELZP3 = new XRiTextField();
    ELZP4 = new XRiTextField();
    ELZP5 = new XRiTextField();
    ELZP6 = new XRiTextField();
    ELZP7 = new XRiTextField();
    ELZP8 = new XRiTextField();
    WZP2 = new JLabel();
    WZP3 = new JLabel();
    WZP4 = new JLabel();
    WZP5 = new JLabel();
    WZP6 = new JLabel();
    WZP7 = new JLabel();
    WZP8 = new JLabel();
    ERZP2 = new RiZoneSortie();
    ERZP3 = new RiZoneSortie();
    ERZP4 = new RiZoneSortie();
    ERZP5 = new RiZoneSortie();
    ERZP6 = new RiZoneSortie();
    ERZP7 = new RiZoneSortie();
    ERZP8 = new RiZoneSortie();
    barre_tete = new JMenuBar();
    OBJ_53 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(845, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Bloc-notes");
            riSousMenu_bt6.setToolTipText("Bloc-notes");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //---- OBJ_96 ----
        OBJ_96.setText("Aucune zone personnalis\u00e9e de ligne commerciale n'a \u00e9t\u00e9 sp\u00e9cifi\u00e9e pour cet \u00e9tablissement");
        OBJ_96.setFont(OBJ_96.getFont().deriveFont(OBJ_96.getFont().getStyle() | Font.BOLD));
        OBJ_96.setHorizontalAlignment(SwingConstants.CENTER);
        OBJ_96.setName("OBJ_96");

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- A1LIB ----
          A1LIB.setText("@A1LIB@");
          A1LIB.setName("A1LIB");
          panel1.add(A1LIB);
          A1LIB.setBounds(315, 10, 300, A1LIB.getPreferredSize().height);

          //---- ELART ----
          ELART.setText("@ELART@");
          ELART.setName("ELART");
          panel1.add(ELART);
          ELART.setBounds(100, 10, 210, ELART.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Article");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(45, 12, 55, 20);

          //---- riBoutonDetail1 ----
          riBoutonDetail1.setName("riBoutonDetail1");
          riBoutonDetail1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetail1ActionPerformed(e);
            }
          });
          panel1.add(riBoutonDetail1);
          riBoutonDetail1.setBounds(new Rectangle(new Point(15, 13), riBoutonDetail1.getPreferredSize()));

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- CLNOM ----
            CLNOM.setText("@CLNOM@");
            CLNOM.setName("CLNOM");
            panel3.add(CLNOM);
            CLNOM.setBounds(210, 35, 390, CLNOM.getPreferredSize().height);

            //---- CLTEL ----
            CLTEL.setText("@CLTEL@");
            CLTEL.setName("CLTEL");
            panel3.add(CLTEL);
            CLTEL.setBounds(210, 65, 110, CLTEL.getPreferredSize().height);

            //---- DATBON ----
            DATBON.setText("@DATBON@");
            DATBON.setHorizontalAlignment(SwingConstants.CENTER);
            DATBON.setName("DATBON");
            panel3.add(DATBON);
            DATBON.setBounds(210, 5, 70, DATBON.getPreferredSize().height);

            //---- E1FAC ----
            E1FAC.setText("@E1FAC@");
            E1FAC.setName("E1FAC");
            panel3.add(E1FAC);
            E1FAC.setBounds(540, 5, 60, E1FAC.getPreferredSize().height);

            //---- NUMBON ----
            NUMBON.setText("@NUMBON@");
            NUMBON.setName("NUMBON");
            panel3.add(NUMBON);
            NUMBON.setBounds(85, 5, 60, NUMBON.getPreferredSize().height);

            //---- OBJ_63 ----
            OBJ_63.setText("Facture");
            OBJ_63.setName("OBJ_63");
            panel3.add(OBJ_63);
            OBJ_63.setBounds(485, 7, 55, 20);

            //---- CLCLI ----
            CLCLI.setText("@CLCLI@");
            CLCLI.setName("CLCLI");
            panel3.add(CLCLI);
            CLCLI.setBounds(85, 35, 60, CLCLI.getPreferredSize().height);

            //---- NUMDEV ----
            NUMDEV.setText("@NUMDEV@");
            NUMDEV.setName("NUMDEV");
            panel3.add(NUMDEV);
            NUMDEV.setBounds(350, 5, 60, NUMDEV.getPreferredSize().height);

            //---- OBJ_61 ----
            OBJ_61.setText("Devis");
            OBJ_61.setName("OBJ_61");
            panel3.add(OBJ_61);
            OBJ_61.setBounds(305, 7, 45, 20);

            //---- CLLIV ----
            CLLIV.setText("@CLLIV@");
            CLLIV.setName("CLLIV");
            panel3.add(CLLIV);
            CLLIV.setBounds(150, 35, 34, CLLIV.getPreferredSize().height);

            //---- OBJ_59 ----
            OBJ_59.setText("du");
            OBJ_59.setName("OBJ_59");
            panel3.add(OBJ_59);
            OBJ_59.setBounds(180, 7, 25, 20);

            //---- SUFBON ----
            SUFBON.setText("@SUFBON@");
            SUFBON.setName("SUFBON");
            panel3.add(SUFBON);
            SUFBON.setBounds(150, 5, 20, SUFBON.getPreferredSize().height);

            //---- label2 ----
            label2.setText("Vente");
            label2.setName("label2");
            panel3.add(label2);
            label2.setBounds(30, 7, 55, 20);

            //---- label3 ----
            label3.setText("Client");
            label3.setName("label3");
            panel3.add(label3);
            label3.setBounds(30, 37, 55, 20);

            //---- riBoutonDetail2 ----
            riBoutonDetail2.setName("riBoutonDetail2");
            riBoutonDetail2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetail2ActionPerformed(e);
              }
            });
            panel3.add(riBoutonDetail2);
            riBoutonDetail2.setBounds(0, 8, 18, 18);

            //---- riBoutonDetail3 ----
            riBoutonDetail3.setName("riBoutonDetail3");
            riBoutonDetail3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetail3ActionPerformed(e);
              }
            });
            panel3.add(riBoutonDetail3);
            riBoutonDetail3.setBounds(0, 38, 18, 18);

            //---- label4 ----
            label4.setText("T\u00e9l\u00e9phone");
            label4.setName("label4");
            panel3.add(label4);
            label4.setBounds(130, 67, 75, 21);
          }
          panel1.add(panel3);
          panel3.setBounds(15, 35, 635, 95);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Zones personnalis\u00e9es"));
          panel2.setOpaque(false);
          panel2.setName("panel2");

          //---- WZP1 ----
          WZP1.setText("@WZP1@");
          WZP1.setName("WZP1");

          //---- ELZP1 ----
          ELZP1.setComponentPopupMenu(BTD);
          ELZP1.setName("ELZP1");

          //---- ERZP1 ----
          ERZP1.setComponentPopupMenu(BTD);
          ERZP1.setText("@ERZP1@");
          ERZP1.setName("ERZP1");

          //---- ELZP2 ----
          ELZP2.setComponentPopupMenu(BTD);
          ELZP2.setName("ELZP2");

          //---- ELZP3 ----
          ELZP3.setComponentPopupMenu(BTD);
          ELZP3.setName("ELZP3");

          //---- ELZP4 ----
          ELZP4.setComponentPopupMenu(BTD);
          ELZP4.setName("ELZP4");

          //---- ELZP5 ----
          ELZP5.setComponentPopupMenu(BTD);
          ELZP5.setName("ELZP5");

          //---- ELZP6 ----
          ELZP6.setComponentPopupMenu(BTD);
          ELZP6.setName("ELZP6");

          //---- ELZP7 ----
          ELZP7.setComponentPopupMenu(BTD);
          ELZP7.setName("ELZP7");

          //---- ELZP8 ----
          ELZP8.setComponentPopupMenu(BTD);
          ELZP8.setName("ELZP8");

          //---- WZP2 ----
          WZP2.setText("@WZP2@");
          WZP2.setName("WZP2");

          //---- WZP3 ----
          WZP3.setText("@WZP3@");
          WZP3.setName("WZP3");

          //---- WZP4 ----
          WZP4.setText("@WZP4@");
          WZP4.setName("WZP4");

          //---- WZP5 ----
          WZP5.setText("@WZP5@");
          WZP5.setName("WZP5");

          //---- WZP6 ----
          WZP6.setText("@WZP6@");
          WZP6.setName("WZP6");

          //---- WZP7 ----
          WZP7.setText("@WZP7@");
          WZP7.setName("WZP7");

          //---- WZP8 ----
          WZP8.setText("@WZP8@");
          WZP8.setName("WZP8");

          //---- ERZP2 ----
          ERZP2.setComponentPopupMenu(BTD);
          ERZP2.setText("@ERZP1@");
          ERZP2.setName("ERZP2");

          //---- ERZP3 ----
          ERZP3.setComponentPopupMenu(BTD);
          ERZP3.setText("@ERZP1@");
          ERZP3.setName("ERZP3");

          //---- ERZP4 ----
          ERZP4.setComponentPopupMenu(BTD);
          ERZP4.setText("@ERZP1@");
          ERZP4.setName("ERZP4");

          //---- ERZP5 ----
          ERZP5.setComponentPopupMenu(BTD);
          ERZP5.setText("@ERZP1@");
          ERZP5.setName("ERZP5");

          //---- ERZP6 ----
          ERZP6.setComponentPopupMenu(BTD);
          ERZP6.setText("@ERZP1@");
          ERZP6.setName("ERZP6");

          //---- ERZP7 ----
          ERZP7.setComponentPopupMenu(BTD);
          ERZP7.setText("@ERZP1@");
          ERZP7.setName("ERZP7");

          //---- ERZP8 ----
          ERZP8.setComponentPopupMenu(BTD);
          ERZP8.setText("@ERZP1@");
          ERZP8.setName("ERZP8");

          GroupLayout panel2Layout = new GroupLayout(panel2);
          panel2.setLayout(panel2Layout);
          panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WZP1, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(ELZP1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)))
                    .addGap(5, 5, 5)
                    .addComponent(ERZP1, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(ELZP2, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WZP2, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
                    .addGap(5, 5, 5)
                    .addComponent(ERZP2, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(ELZP3, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WZP3, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
                    .addGap(5, 5, 5)
                    .addComponent(ERZP3, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(ELZP4, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WZP4, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
                    .addGap(5, 5, 5)
                    .addComponent(ERZP4, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WZP5, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(ELZP5, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)))
                    .addGap(5, 5, 5)
                    .addComponent(ERZP5, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WZP6, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(ELZP6, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)))
                    .addGap(5, 5, 5)
                    .addComponent(ERZP6, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WZP7, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(ELZP7, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)))
                    .addGap(5, 5, 5)
                    .addComponent(ERZP7, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(ELZP8, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WZP8, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
                    .addGap(5, 5, 5)
                    .addComponent(ERZP8, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))))
          );
          panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(WZP1, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ELZP1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(ERZP1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(panel2Layout.createParallelGroup()
                  .addComponent(ELZP2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(WZP2, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(ERZP2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(panel2Layout.createParallelGroup()
                  .addComponent(ELZP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(WZP3, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(ERZP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(panel2Layout.createParallelGroup()
                  .addComponent(ELZP4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(WZP4, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(ERZP4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(WZP5, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ELZP5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(ERZP5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(WZP6, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ELZP6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(ERZP6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(WZP7, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ELZP7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(ERZP7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(panel2Layout.createParallelGroup()
                  .addComponent(ELZP8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(WZP8, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(ERZP8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 660, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(50, 50, 50)
                  .addComponent(OBJ_96, GroupLayout.PREFERRED_SIZE, 545, GroupLayout.PREFERRED_SIZE))))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(45, 45, 45)
                  .addComponent(OBJ_96, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //---- OBJ_53 ----
      OBJ_53.setText("@LIBZP@");
      OBJ_53.setOpaque(false);
      OBJ_53.setPreferredSize(new Dimension(300, 24));
      OBJ_53.setMinimumSize(new Dimension(300, 20));
      OBJ_53.setMaximumSize(new Dimension(300, 20));
      OBJ_53.setName("OBJ_53");
      barre_tete.add(OBJ_53);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JLabel OBJ_96;
  private JPanel panel1;
  private RiZoneSortie A1LIB;
  private RiZoneSortie ELART;
  private JLabel label1;
  private SNBoutonDetail riBoutonDetail1;
  private JPanel panel3;
  private RiZoneSortie CLNOM;
  private RiZoneSortie CLTEL;
  private RiZoneSortie DATBON;
  private RiZoneSortie E1FAC;
  private RiZoneSortie NUMBON;
  private JLabel OBJ_63;
  private RiZoneSortie CLCLI;
  private RiZoneSortie NUMDEV;
  private JLabel OBJ_61;
  private RiZoneSortie CLLIV;
  private JLabel OBJ_59;
  private RiZoneSortie SUFBON;
  private JLabel label2;
  private JLabel label3;
  private SNBoutonDetail riBoutonDetail2;
  private SNBoutonDetail riBoutonDetail3;
  private JLabel label4;
  private JPanel panel2;
  private JLabel WZP1;
  private XRiTextField ELZP1;
  private RiZoneSortie ERZP1;
  private XRiTextField ELZP2;
  private XRiTextField ELZP3;
  private XRiTextField ELZP4;
  private XRiTextField ELZP5;
  private XRiTextField ELZP6;
  private XRiTextField ELZP7;
  private XRiTextField ELZP8;
  private JLabel WZP2;
  private JLabel WZP3;
  private JLabel WZP4;
  private JLabel WZP5;
  private JLabel WZP6;
  private JLabel WZP7;
  private JLabel WZP8;
  private RiZoneSortie ERZP2;
  private RiZoneSortie ERZP3;
  private RiZoneSortie ERZP4;
  private RiZoneSortie ERZP5;
  private RiZoneSortie ERZP6;
  private RiZoneSortie ERZP7;
  private RiZoneSortie ERZP8;
  private JMenuBar barre_tete;
  private RiZoneSortie OBJ_53;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
