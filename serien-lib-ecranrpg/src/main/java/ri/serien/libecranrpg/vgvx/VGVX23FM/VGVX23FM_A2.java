
package ri.serien.libecranrpg.vgvx.VGVX23FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonInformation;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX23FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVX23FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WART@")).trim());
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    A1LIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIBR@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LD01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD01@")).trim());
    LD02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD02@")).trim());
    LD03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD03@")).trim());
    LD04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD04@")).trim());
    LD05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD05@")).trim());
    LD06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD06@")).trim());
    LD07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD07@")).trim());
    LD08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD08@")).trim());
    LD09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD09@")).trim());
    LD10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD10@")).trim());
    LD11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD11@")).trim());
    LD12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD12@")).trim());
    LD13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD13@")).trim());
    LD14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD14@")).trim());
    LD15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD15@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLD01@")).trim());
    AD1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AD1@")).trim());
    AD2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AD2@")).trim());
    AD3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AD3@")).trim());
    AD4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AD4@")).trim());
    MALIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    WMAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAG@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    String affUnite = lexique.HostFieldGetData("P23FLI").trim();
    
    if (affUnite.equals("1")) {
      labelUnite.setText("Affichage en unités de stock");
    }
    else if (affUnite.equals("2")) {
      labelUnite.setText("Affichage en unités de conditionnement");
    }
    else if (affUnite.equals("3")) {
      labelUnite.setText("Affichage en mètres lineaires");
    }
    else {
      labelUnite.setText("");
    }
    
    String titre = Constantes.normerTexte(lexique.HostFieldGetData("HLD02"));
    String titres[] = titre.split("\\s+");
    if (titres.length > 0) {
      AD1.setText(titres[0]);
    }
    if (titres.length > 1) {
      AD2.setText(titres[1]);
    }
    if (titres.length > 2) {
      AD3.setText(titres[2]);
    }
    if (titres.length > 3) {
      AD4.setText(titres[3]);
    }
    
    WTP01.setVisible(!lexique.HostFieldGetData("LD01").trim().equals(""));
    WTP02.setVisible(!lexique.HostFieldGetData("LD02").trim().equals(""));
    WTP03.setVisible(!lexique.HostFieldGetData("LD03").trim().equals(""));
    WTP04.setVisible(!lexique.HostFieldGetData("LD04").trim().equals(""));
    WTP05.setVisible(!lexique.HostFieldGetData("LD05").trim().equals(""));
    WTP06.setVisible(!lexique.HostFieldGetData("LD06").trim().equals(""));
    WTP07.setVisible(!lexique.HostFieldGetData("LD07").trim().equals(""));
    WTP08.setVisible(!lexique.HostFieldGetData("LD08").trim().equals(""));
    WTP09.setVisible(!lexique.HostFieldGetData("LD09").trim().equals(""));
    WTP10.setVisible(!lexique.HostFieldGetData("LD10").trim().equals(""));
    WTP11.setVisible(!lexique.HostFieldGetData("LD11").trim().equals(""));
    WTP12.setVisible(!lexique.HostFieldGetData("LD12").trim().equals(""));
    WTP13.setVisible(!lexique.HostFieldGetData("LD13").trim().equals(""));
    WTP14.setVisible(!lexique.HostFieldGetData("LD14").trim().equals(""));
    WTP15.setVisible(!lexique.HostFieldGetData("LD15").trim().equals(""));
    
    LD01.setVisible(!lexique.HostFieldGetData("LD01").trim().equals(""));
    LD02.setVisible(!lexique.HostFieldGetData("LD02").trim().equals(""));
    LD03.setVisible(!lexique.HostFieldGetData("LD03").trim().equals(""));
    LD04.setVisible(!lexique.HostFieldGetData("LD04").trim().equals(""));
    LD05.setVisible(!lexique.HostFieldGetData("LD05").trim().equals(""));
    LD06.setVisible(!lexique.HostFieldGetData("LD06").trim().equals(""));
    LD07.setVisible(!lexique.HostFieldGetData("LD07").trim().equals(""));
    LD08.setVisible(!lexique.HostFieldGetData("LD08").trim().equals(""));
    LD09.setVisible(!lexique.HostFieldGetData("LD09").trim().equals(""));
    LD10.setVisible(!lexique.HostFieldGetData("LD10").trim().equals(""));
    LD11.setVisible(!lexique.HostFieldGetData("LD11").trim().equals(""));
    LD12.setVisible(!lexique.HostFieldGetData("LD12").trim().equals(""));
    LD13.setVisible(!lexique.HostFieldGetData("LD13").trim().equals(""));
    LD14.setVisible(!lexique.HostFieldGetData("LD14").trim().equals(""));
    LD15.setVisible(!lexique.HostFieldGetData("LD15").trim().equals(""));
    
    PL01.setVisible(lexique.HostFieldGetData("PL01").trim().equals("+"));
    PL02.setVisible(lexique.HostFieldGetData("PL02").trim().equals("+"));
    PL03.setVisible(lexique.HostFieldGetData("PL03").trim().equals("+"));
    PL04.setVisible(lexique.HostFieldGetData("PL04").trim().equals("+"));
    PL05.setVisible(lexique.HostFieldGetData("PL05").trim().equals("+"));
    PL06.setVisible(lexique.HostFieldGetData("PL06").trim().equals("+"));
    PL07.setVisible(lexique.HostFieldGetData("PL07").trim().equals("+"));
    PL08.setVisible(lexique.HostFieldGetData("PL08").trim().equals("+"));
    PL09.setVisible(lexique.HostFieldGetData("PL09").trim().equals("+"));
    PL10.setVisible(lexique.HostFieldGetData("PL10").trim().equals("+"));
    PL11.setVisible(lexique.HostFieldGetData("PL11").trim().equals("+"));
    PL12.setVisible(lexique.HostFieldGetData("PL12").trim().equals("+"));
    PL13.setVisible(lexique.HostFieldGetData("PL13").trim().equals("+"));
    PL14.setVisible(lexique.HostFieldGetData("PL14").trim().equals("+"));
    PL15.setVisible(lexique.HostFieldGetData("PL15").trim().equals("+"));
    
    // icones
    p_bpresentation.setCodeEtablissement(WETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData(BTD.getInvoker().getName(), 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP02ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP02", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP03ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP03", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP04ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP04", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP05ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP05", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP06ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP06", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP07ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP07", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP08ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP08", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP09ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP09", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP10", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP11ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP11", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP12ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP12", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP13ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP13", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP14ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP14", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP15ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP15", 0, "1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WART = new RiZoneSortie();
    WETB = new RiZoneSortie();
    OBJ_53 = new JLabel();
    OBJ_66 = new JLabel();
    A1LIBR = new RiZoneSortie();
    p_tete_droite = new JPanel();
    labelUnite = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    WTP01 = new SNBoutonDetail();
    LD01 = new RiZoneSortie();
    AD11 = new XRiTextField();
    PL01 = new SNBoutonInformation();
    AD21 = new XRiTextField();
    AD31 = new XRiTextField();
    AD41 = new XRiTextField();
    WTP02 = new SNBoutonDetail();
    LD02 = new RiZoneSortie();
    AD12 = new XRiTextField();
    PL02 = new SNBoutonInformation();
    AD22 = new XRiTextField();
    AD32 = new XRiTextField();
    AD42 = new XRiTextField();
    WTP03 = new SNBoutonDetail();
    LD03 = new RiZoneSortie();
    AD13 = new XRiTextField();
    PL03 = new SNBoutonInformation();
    AD23 = new XRiTextField();
    AD33 = new XRiTextField();
    AD43 = new XRiTextField();
    WTP04 = new SNBoutonDetail();
    LD04 = new RiZoneSortie();
    AD14 = new XRiTextField();
    PL04 = new SNBoutonInformation();
    AD24 = new XRiTextField();
    AD34 = new XRiTextField();
    AD44 = new XRiTextField();
    WTP05 = new SNBoutonDetail();
    LD05 = new RiZoneSortie();
    AD15 = new XRiTextField();
    PL05 = new SNBoutonInformation();
    AD25 = new XRiTextField();
    AD35 = new XRiTextField();
    AD45 = new XRiTextField();
    WTP06 = new SNBoutonDetail();
    LD06 = new RiZoneSortie();
    AD16 = new XRiTextField();
    PL06 = new SNBoutonInformation();
    AD26 = new XRiTextField();
    AD36 = new XRiTextField();
    AD46 = new XRiTextField();
    WTP07 = new SNBoutonDetail();
    LD07 = new RiZoneSortie();
    AD17 = new XRiTextField();
    PL07 = new SNBoutonInformation();
    AD27 = new XRiTextField();
    AD37 = new XRiTextField();
    AD47 = new XRiTextField();
    WTP08 = new SNBoutonDetail();
    LD08 = new RiZoneSortie();
    AD18 = new XRiTextField();
    PL08 = new SNBoutonInformation();
    AD28 = new XRiTextField();
    AD38 = new XRiTextField();
    AD48 = new XRiTextField();
    WTP09 = new SNBoutonDetail();
    LD09 = new RiZoneSortie();
    AD19 = new XRiTextField();
    PL09 = new SNBoutonInformation();
    AD29 = new XRiTextField();
    AD39 = new XRiTextField();
    AD49 = new XRiTextField();
    WTP10 = new SNBoutonDetail();
    LD10 = new RiZoneSortie();
    AD110 = new XRiTextField();
    PL10 = new SNBoutonInformation();
    AD210 = new XRiTextField();
    AD310 = new XRiTextField();
    AD410 = new XRiTextField();
    WTP11 = new SNBoutonDetail();
    LD11 = new RiZoneSortie();
    PL11 = new SNBoutonInformation();
    WTP12 = new SNBoutonDetail();
    LD12 = new RiZoneSortie();
    PL12 = new SNBoutonInformation();
    WTP13 = new SNBoutonDetail();
    LD13 = new RiZoneSortie();
    PL13 = new SNBoutonInformation();
    WTP14 = new SNBoutonDetail();
    LD14 = new RiZoneSortie();
    PL14 = new SNBoutonInformation();
    WTP15 = new SNBoutonDetail();
    LD15 = new RiZoneSortie();
    PL15 = new SNBoutonInformation();
    label1 = new JLabel();
    AD1 = new JLabel();
    AD2 = new JLabel();
    AD3 = new JLabel();
    AD4 = new JLabel();
    OBJ_67 = new JLabel();
    MALIB = new RiZoneSortie();
    WMAG = new RiZoneSortie();
    AD111 = new XRiTextField();
    AD211 = new XRiTextField();
    AD311 = new XRiTextField();
    AD411 = new XRiTextField();
    AD112 = new XRiTextField();
    AD212 = new XRiTextField();
    AD312 = new XRiTextField();
    AD412 = new XRiTextField();
    AD113 = new XRiTextField();
    AD213 = new XRiTextField();
    AD313 = new XRiTextField();
    AD413 = new XRiTextField();
    AD114 = new XRiTextField();
    AD214 = new XRiTextField();
    AD314 = new XRiTextField();
    AD414 = new XRiTextField();
    AD115 = new XRiTextField();
    AD215 = new XRiTextField();
    AD315 = new XRiTextField();
    AD415 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Adresses de stockage des lots");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(900, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
          
          // ---- WART ----
          WART.setText("@WART@");
          WART.setOpaque(false);
          WART.setName("WART");
          p_tete_gauche.add(WART);
          WART.setBounds(290, 2, 210, WART.getPreferredSize().height);
          
          // ---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setText("@WETB@");
          WETB.setOpaque(false);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(135, 2, 40, WETB.getPreferredSize().height);
          
          // ---- OBJ_53 ----
          OBJ_53.setText("Etablissement");
          OBJ_53.setName("OBJ_53");
          p_tete_gauche.add(OBJ_53);
          OBJ_53.setBounds(10, 0, 100, 28);
          
          // ---- OBJ_66 ----
          OBJ_66.setText("Article");
          OBJ_66.setName("OBJ_66");
          p_tete_gauche.add(OBJ_66);
          OBJ_66.setBounds(230, 0, 60, 28);
          
          // ---- A1LIBR ----
          A1LIBR.setText("@A1LIBR@");
          A1LIBR.setOpaque(false);
          A1LIBR.setName("A1LIBR");
          p_tete_gauche.add(A1LIBR);
          A1LIBR.setBounds(505, 2, 210, A1LIBR.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- labelUnite ----
          labelUnite.setText("labelUnite");
          labelUnite.setHorizontalAlignment(SwingConstants.RIGHT);
          labelUnite.setFont(labelUnite.getFont().deriveFont(labelUnite.getFont().getStyle() | Font.BOLD));
          labelUnite.setName("labelUnite");
          p_tete_droite.add(labelUnite);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 290));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(870, 530));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(870, 381));
          p_contenu.setName("p_contenu");
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(780, 100, 25, 175);
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(780, 299, 25, 175);
            
            // ---- WTP01 ----
            WTP01.setComponentPopupMenu(BTD);
            WTP01.setName("WTP01");
            WTP01.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP01ActionPerformed(e);
              }
            });
            panel1.add(WTP01);
            WTP01.setBounds(20, 100, 20, 24);
            
            // ---- LD01 ----
            LD01.setText("@LD01@");
            LD01.setComponentPopupMenu(null);
            LD01.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD01.setName("LD01");
            panel1.add(LD01);
            LD01.setBounds(45, 100, 460, LD01.getPreferredSize().height);
            
            // ---- AD11 ----
            AD11.setComponentPopupMenu(null);
            AD11.setName("AD11");
            panel1.add(AD11);
            AD11.setBounds(523, 98, 40, AD11.getPreferredSize().height);
            
            // ---- PL01 ----
            PL01.setComponentPopupMenu(null);
            PL01.setToolTipText("D'autres adresses existent");
            PL01.setName("PL01");
            panel1.add(PL01);
            PL01.setBounds(740, 100, 24, PL01.getPreferredSize().height);
            
            // ---- AD21 ----
            AD21.setComponentPopupMenu(null);
            AD21.setName("AD21");
            panel1.add(AD21);
            AD21.setBounds(581, 98, 40, AD21.getPreferredSize().height);
            
            // ---- AD31 ----
            AD31.setComponentPopupMenu(null);
            AD31.setName("AD31");
            panel1.add(AD31);
            AD31.setBounds(639, 98, 40, AD31.getPreferredSize().height);
            
            // ---- AD41 ----
            AD41.setComponentPopupMenu(null);
            AD41.setName("AD41");
            panel1.add(AD41);
            AD41.setBounds(697, 98, 40, AD41.getPreferredSize().height);
            
            // ---- WTP02 ----
            WTP02.setComponentPopupMenu(BTD);
            WTP02.setName("WTP02");
            WTP02.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP02ActionPerformed(e);
              }
            });
            panel1.add(WTP02);
            WTP02.setBounds(20, 125, 20, 24);
            
            // ---- LD02 ----
            LD02.setText("@LD02@");
            LD02.setComponentPopupMenu(null);
            LD02.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD02.setName("LD02");
            panel1.add(LD02);
            LD02.setBounds(45, 125, 460, LD02.getPreferredSize().height);
            
            // ---- AD12 ----
            AD12.setComponentPopupMenu(null);
            AD12.setName("AD12");
            panel1.add(AD12);
            AD12.setBounds(523, 123, 40, AD12.getPreferredSize().height);
            
            // ---- PL02 ----
            PL02.setComponentPopupMenu(null);
            PL02.setToolTipText("D'autres adresses existent");
            PL02.setName("PL02");
            panel1.add(PL02);
            PL02.setBounds(740, 125, 24, PL02.getPreferredSize().height);
            
            // ---- AD22 ----
            AD22.setComponentPopupMenu(null);
            AD22.setName("AD22");
            panel1.add(AD22);
            AD22.setBounds(581, 123, 40, AD22.getPreferredSize().height);
            
            // ---- AD32 ----
            AD32.setComponentPopupMenu(null);
            AD32.setName("AD32");
            panel1.add(AD32);
            AD32.setBounds(639, 123, 40, AD32.getPreferredSize().height);
            
            // ---- AD42 ----
            AD42.setComponentPopupMenu(null);
            AD42.setName("AD42");
            panel1.add(AD42);
            AD42.setBounds(697, 123, 40, AD42.getPreferredSize().height);
            
            // ---- WTP03 ----
            WTP03.setComponentPopupMenu(BTD);
            WTP03.setName("WTP03");
            WTP03.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP03ActionPerformed(e);
              }
            });
            panel1.add(WTP03);
            WTP03.setBounds(20, 150, 20, 24);
            
            // ---- LD03 ----
            LD03.setText("@LD03@");
            LD03.setComponentPopupMenu(null);
            LD03.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD03.setName("LD03");
            panel1.add(LD03);
            LD03.setBounds(45, 150, 460, 24);
            
            // ---- AD13 ----
            AD13.setComponentPopupMenu(null);
            AD13.setName("AD13");
            panel1.add(AD13);
            AD13.setBounds(523, 148, 40, 28);
            
            // ---- PL03 ----
            PL03.setComponentPopupMenu(null);
            PL03.setToolTipText("D'autres adresses existent");
            PL03.setName("PL03");
            panel1.add(PL03);
            PL03.setBounds(740, 150, 24, 24);
            
            // ---- AD23 ----
            AD23.setComponentPopupMenu(null);
            AD23.setName("AD23");
            panel1.add(AD23);
            AD23.setBounds(581, 148, 40, 28);
            
            // ---- AD33 ----
            AD33.setComponentPopupMenu(null);
            AD33.setName("AD33");
            panel1.add(AD33);
            AD33.setBounds(639, 148, 40, 28);
            
            // ---- AD43 ----
            AD43.setComponentPopupMenu(null);
            AD43.setName("AD43");
            panel1.add(AD43);
            AD43.setBounds(697, 148, 40, 28);
            
            // ---- WTP04 ----
            WTP04.setComponentPopupMenu(BTD);
            WTP04.setName("WTP04");
            WTP04.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP04ActionPerformed(e);
              }
            });
            panel1.add(WTP04);
            WTP04.setBounds(20, 175, 20, 24);
            
            // ---- LD04 ----
            LD04.setText("@LD04@");
            LD04.setComponentPopupMenu(null);
            LD04.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD04.setName("LD04");
            panel1.add(LD04);
            LD04.setBounds(45, 175, 460, 24);
            
            // ---- AD14 ----
            AD14.setComponentPopupMenu(null);
            AD14.setName("AD14");
            panel1.add(AD14);
            AD14.setBounds(523, 173, 40, 28);
            
            // ---- PL04 ----
            PL04.setComponentPopupMenu(null);
            PL04.setToolTipText("D'autres adresses existent");
            PL04.setName("PL04");
            panel1.add(PL04);
            PL04.setBounds(740, 175, 24, 24);
            
            // ---- AD24 ----
            AD24.setComponentPopupMenu(null);
            AD24.setName("AD24");
            panel1.add(AD24);
            AD24.setBounds(581, 173, 40, 28);
            
            // ---- AD34 ----
            AD34.setComponentPopupMenu(null);
            AD34.setName("AD34");
            panel1.add(AD34);
            AD34.setBounds(639, 173, 40, 28);
            
            // ---- AD44 ----
            AD44.setComponentPopupMenu(null);
            AD44.setName("AD44");
            panel1.add(AD44);
            AD44.setBounds(697, 173, 40, 28);
            
            // ---- WTP05 ----
            WTP05.setComponentPopupMenu(BTD);
            WTP05.setName("WTP05");
            WTP05.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP05ActionPerformed(e);
              }
            });
            panel1.add(WTP05);
            WTP05.setBounds(20, 200, 20, 24);
            
            // ---- LD05 ----
            LD05.setText("@LD05@");
            LD05.setComponentPopupMenu(null);
            LD05.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD05.setName("LD05");
            panel1.add(LD05);
            LD05.setBounds(45, 200, 460, 24);
            
            // ---- AD15 ----
            AD15.setComponentPopupMenu(null);
            AD15.setName("AD15");
            panel1.add(AD15);
            AD15.setBounds(523, 198, 40, 28);
            
            // ---- PL05 ----
            PL05.setComponentPopupMenu(null);
            PL05.setToolTipText("D'autres adresses existent");
            PL05.setName("PL05");
            panel1.add(PL05);
            PL05.setBounds(740, 200, 24, 24);
            
            // ---- AD25 ----
            AD25.setComponentPopupMenu(null);
            AD25.setName("AD25");
            panel1.add(AD25);
            AD25.setBounds(581, 198, 40, 28);
            
            // ---- AD35 ----
            AD35.setComponentPopupMenu(null);
            AD35.setName("AD35");
            panel1.add(AD35);
            AD35.setBounds(639, 198, 40, 28);
            
            // ---- AD45 ----
            AD45.setComponentPopupMenu(null);
            AD45.setName("AD45");
            panel1.add(AD45);
            AD45.setBounds(697, 198, 40, 28);
            
            // ---- WTP06 ----
            WTP06.setComponentPopupMenu(BTD);
            WTP06.setName("WTP06");
            WTP06.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP06ActionPerformed(e);
              }
            });
            panel1.add(WTP06);
            WTP06.setBounds(20, 225, 20, 24);
            
            // ---- LD06 ----
            LD06.setText("@LD06@");
            LD06.setComponentPopupMenu(null);
            LD06.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD06.setName("LD06");
            panel1.add(LD06);
            LD06.setBounds(45, 225, 460, 24);
            
            // ---- AD16 ----
            AD16.setComponentPopupMenu(null);
            AD16.setName("AD16");
            panel1.add(AD16);
            AD16.setBounds(523, 223, 40, 28);
            
            // ---- PL06 ----
            PL06.setComponentPopupMenu(null);
            PL06.setToolTipText("D'autres adresses existent");
            PL06.setName("PL06");
            panel1.add(PL06);
            PL06.setBounds(740, 225, 24, 24);
            
            // ---- AD26 ----
            AD26.setComponentPopupMenu(null);
            AD26.setName("AD26");
            panel1.add(AD26);
            AD26.setBounds(581, 223, 40, 28);
            
            // ---- AD36 ----
            AD36.setComponentPopupMenu(null);
            AD36.setName("AD36");
            panel1.add(AD36);
            AD36.setBounds(639, 223, 40, 28);
            
            // ---- AD46 ----
            AD46.setComponentPopupMenu(null);
            AD46.setName("AD46");
            panel1.add(AD46);
            AD46.setBounds(697, 223, 40, 28);
            
            // ---- WTP07 ----
            WTP07.setComponentPopupMenu(BTD);
            WTP07.setName("WTP07");
            WTP07.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP07ActionPerformed(e);
              }
            });
            panel1.add(WTP07);
            WTP07.setBounds(20, 250, 20, 24);
            
            // ---- LD07 ----
            LD07.setText("@LD07@");
            LD07.setComponentPopupMenu(null);
            LD07.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD07.setName("LD07");
            panel1.add(LD07);
            LD07.setBounds(45, 250, 460, 24);
            
            // ---- AD17 ----
            AD17.setComponentPopupMenu(null);
            AD17.setName("AD17");
            panel1.add(AD17);
            AD17.setBounds(523, 248, 40, 28);
            
            // ---- PL07 ----
            PL07.setComponentPopupMenu(null);
            PL07.setToolTipText("D'autres adresses existent");
            PL07.setName("PL07");
            panel1.add(PL07);
            PL07.setBounds(740, 250, 24, 24);
            
            // ---- AD27 ----
            AD27.setComponentPopupMenu(null);
            AD27.setName("AD27");
            panel1.add(AD27);
            AD27.setBounds(581, 248, 40, 28);
            
            // ---- AD37 ----
            AD37.setComponentPopupMenu(null);
            AD37.setName("AD37");
            panel1.add(AD37);
            AD37.setBounds(639, 248, 40, 28);
            
            // ---- AD47 ----
            AD47.setComponentPopupMenu(null);
            AD47.setName("AD47");
            panel1.add(AD47);
            AD47.setBounds(697, 248, 40, 28);
            
            // ---- WTP08 ----
            WTP08.setComponentPopupMenu(BTD);
            WTP08.setName("WTP08");
            WTP08.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP08ActionPerformed(e);
              }
            });
            panel1.add(WTP08);
            WTP08.setBounds(20, 275, 20, 24);
            
            // ---- LD08 ----
            LD08.setText("@LD08@");
            LD08.setComponentPopupMenu(null);
            LD08.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD08.setName("LD08");
            panel1.add(LD08);
            LD08.setBounds(45, 275, 460, 24);
            
            // ---- AD18 ----
            AD18.setComponentPopupMenu(null);
            AD18.setName("AD18");
            panel1.add(AD18);
            AD18.setBounds(523, 273, 40, 28);
            
            // ---- PL08 ----
            PL08.setComponentPopupMenu(null);
            PL08.setToolTipText("D'autres adresses existent");
            PL08.setName("PL08");
            panel1.add(PL08);
            PL08.setBounds(740, 275, 24, 24);
            
            // ---- AD28 ----
            AD28.setComponentPopupMenu(null);
            AD28.setName("AD28");
            panel1.add(AD28);
            AD28.setBounds(581, 273, 40, 28);
            
            // ---- AD38 ----
            AD38.setComponentPopupMenu(null);
            AD38.setName("AD38");
            panel1.add(AD38);
            AD38.setBounds(639, 273, 40, 28);
            
            // ---- AD48 ----
            AD48.setComponentPopupMenu(null);
            AD48.setName("AD48");
            panel1.add(AD48);
            AD48.setBounds(697, 273, 40, 28);
            
            // ---- WTP09 ----
            WTP09.setComponentPopupMenu(BTD);
            WTP09.setName("WTP09");
            WTP09.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP09ActionPerformed(e);
              }
            });
            panel1.add(WTP09);
            WTP09.setBounds(20, 300, 20, 24);
            
            // ---- LD09 ----
            LD09.setText("@LD09@");
            LD09.setComponentPopupMenu(null);
            LD09.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD09.setName("LD09");
            panel1.add(LD09);
            LD09.setBounds(45, 300, 460, 24);
            
            // ---- AD19 ----
            AD19.setComponentPopupMenu(null);
            AD19.setName("AD19");
            panel1.add(AD19);
            AD19.setBounds(523, 298, 40, 28);
            
            // ---- PL09 ----
            PL09.setComponentPopupMenu(null);
            PL09.setToolTipText("D'autres adresses existent");
            PL09.setName("PL09");
            panel1.add(PL09);
            PL09.setBounds(740, 300, 24, 24);
            
            // ---- AD29 ----
            AD29.setComponentPopupMenu(null);
            AD29.setName("AD29");
            panel1.add(AD29);
            AD29.setBounds(581, 298, 40, 28);
            
            // ---- AD39 ----
            AD39.setComponentPopupMenu(null);
            AD39.setName("AD39");
            panel1.add(AD39);
            AD39.setBounds(639, 298, 40, 28);
            
            // ---- AD49 ----
            AD49.setComponentPopupMenu(null);
            AD49.setName("AD49");
            panel1.add(AD49);
            AD49.setBounds(697, 298, 40, 28);
            
            // ---- WTP10 ----
            WTP10.setComponentPopupMenu(BTD);
            WTP10.setName("WTP10");
            WTP10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP10ActionPerformed(e);
              }
            });
            panel1.add(WTP10);
            WTP10.setBounds(20, 325, 20, 24);
            
            // ---- LD10 ----
            LD10.setText("@LD10@");
            LD10.setComponentPopupMenu(null);
            LD10.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD10.setName("LD10");
            panel1.add(LD10);
            LD10.setBounds(45, 325, 460, 24);
            
            // ---- AD110 ----
            AD110.setComponentPopupMenu(null);
            AD110.setName("AD110");
            panel1.add(AD110);
            AD110.setBounds(523, 323, 40, 28);
            
            // ---- PL10 ----
            PL10.setComponentPopupMenu(null);
            PL10.setToolTipText("D'autres adresses existent");
            PL10.setName("PL10");
            panel1.add(PL10);
            PL10.setBounds(740, 325, 24, 24);
            
            // ---- AD210 ----
            AD210.setComponentPopupMenu(null);
            AD210.setName("AD210");
            panel1.add(AD210);
            AD210.setBounds(581, 323, 40, 28);
            
            // ---- AD310 ----
            AD310.setComponentPopupMenu(null);
            AD310.setName("AD310");
            panel1.add(AD310);
            AD310.setBounds(639, 323, 40, 28);
            
            // ---- AD410 ----
            AD410.setComponentPopupMenu(null);
            AD410.setName("AD410");
            panel1.add(AD410);
            AD410.setBounds(697, 323, 40, 28);
            
            // ---- WTP11 ----
            WTP11.setComponentPopupMenu(BTD);
            WTP11.setName("WTP11");
            WTP11.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP11ActionPerformed(e);
              }
            });
            panel1.add(WTP11);
            WTP11.setBounds(20, 350, 20, 24);
            
            // ---- LD11 ----
            LD11.setText("@LD11@");
            LD11.setComponentPopupMenu(null);
            LD11.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD11.setName("LD11");
            panel1.add(LD11);
            LD11.setBounds(45, 350, 460, 24);
            
            // ---- PL11 ----
            PL11.setComponentPopupMenu(null);
            PL11.setToolTipText("D'autres adresses existent");
            PL11.setName("PL11");
            panel1.add(PL11);
            PL11.setBounds(740, 350, 24, 24);
            
            // ---- WTP12 ----
            WTP12.setComponentPopupMenu(BTD);
            WTP12.setName("WTP12");
            WTP12.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP12ActionPerformed(e);
              }
            });
            panel1.add(WTP12);
            WTP12.setBounds(20, 375, 20, 24);
            
            // ---- LD12 ----
            LD12.setText("@LD12@");
            LD12.setComponentPopupMenu(null);
            LD12.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD12.setName("LD12");
            panel1.add(LD12);
            LD12.setBounds(45, 375, 460, 24);
            
            // ---- PL12 ----
            PL12.setComponentPopupMenu(null);
            PL12.setToolTipText("D'autres adresses existent");
            PL12.setName("PL12");
            panel1.add(PL12);
            PL12.setBounds(740, 375, 24, 24);
            
            // ---- WTP13 ----
            WTP13.setComponentPopupMenu(BTD);
            WTP13.setName("WTP13");
            WTP13.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP13ActionPerformed(e);
              }
            });
            panel1.add(WTP13);
            WTP13.setBounds(20, 400, 20, 24);
            
            // ---- LD13 ----
            LD13.setText("@LD13@");
            LD13.setComponentPopupMenu(null);
            LD13.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD13.setName("LD13");
            panel1.add(LD13);
            LD13.setBounds(45, 400, 460, 24);
            
            // ---- PL13 ----
            PL13.setComponentPopupMenu(null);
            PL13.setToolTipText("D'autres adresses existent");
            PL13.setName("PL13");
            panel1.add(PL13);
            PL13.setBounds(740, 400, 24, 24);
            
            // ---- WTP14 ----
            WTP14.setComponentPopupMenu(BTD);
            WTP14.setName("WTP14");
            WTP14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP14ActionPerformed(e);
              }
            });
            panel1.add(WTP14);
            WTP14.setBounds(20, 425, 20, 24);
            
            // ---- LD14 ----
            LD14.setText("@LD14@");
            LD14.setComponentPopupMenu(null);
            LD14.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD14.setName("LD14");
            panel1.add(LD14);
            LD14.setBounds(45, 425, 460, 24);
            
            // ---- PL14 ----
            PL14.setComponentPopupMenu(null);
            PL14.setToolTipText("D'autres adresses existent");
            PL14.setName("PL14");
            panel1.add(PL14);
            PL14.setBounds(740, 425, 24, 24);
            
            // ---- WTP15 ----
            WTP15.setComponentPopupMenu(BTD);
            WTP15.setName("WTP15");
            WTP15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTP15ActionPerformed(e);
              }
            });
            panel1.add(WTP15);
            WTP15.setBounds(20, 450, 20, 24);
            
            // ---- LD15 ----
            LD15.setText("@LD15@");
            LD15.setComponentPopupMenu(null);
            LD15.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD15.setName("LD15");
            panel1.add(LD15);
            LD15.setBounds(45, 450, 460, 24);
            
            // ---- PL15 ----
            PL15.setComponentPopupMenu(null);
            PL15.setToolTipText("D'autres adresses existent");
            PL15.setName("PL15");
            panel1.add(PL15);
            PL15.setBounds(740, 450, 24, 24);
            
            // ---- label1 ----
            label1.setText("@HLD01@");
            label1.setFont(new Font("Courier New", Font.BOLD, 12));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(45, 70, 460, 30);
            
            // ---- AD1 ----
            AD1.setText("@AD1@");
            AD1.setFont(AD1.getFont().deriveFont(AD1.getFont().getStyle() | Font.BOLD));
            AD1.setHorizontalAlignment(SwingConstants.CENTER);
            AD1.setName("AD1");
            panel1.add(AD1);
            AD1.setBounds(514, 70, 58, 30);
            
            // ---- AD2 ----
            AD2.setText("@AD2@");
            AD2.setFont(AD2.getFont().deriveFont(AD2.getFont().getStyle() | Font.BOLD));
            AD2.setHorizontalAlignment(SwingConstants.CENTER);
            AD2.setName("AD2");
            panel1.add(AD2);
            AD2.setBounds(572, 70, 58, 30);
            
            // ---- AD3 ----
            AD3.setText("@AD3@");
            AD3.setFont(AD3.getFont().deriveFont(AD3.getFont().getStyle() | Font.BOLD));
            AD3.setHorizontalAlignment(SwingConstants.CENTER);
            AD3.setName("AD3");
            panel1.add(AD3);
            AD3.setBounds(630, 70, 58, 30);
            
            // ---- AD4 ----
            AD4.setText("@AD4@");
            AD4.setFont(AD4.getFont().deriveFont(AD4.getFont().getStyle() | Font.BOLD));
            AD4.setHorizontalAlignment(SwingConstants.CENTER);
            AD4.setName("AD4");
            panel1.add(AD4);
            AD4.setBounds(688, 70, 58, 30);
            
            // ---- OBJ_67 ----
            OBJ_67.setText("Magasin");
            OBJ_67.setName("OBJ_67");
            panel1.add(OBJ_67);
            OBJ_67.setBounds(45, 20, 60, 28);
            
            // ---- MALIB ----
            MALIB.setText("@MALIB@");
            MALIB.setOpaque(false);
            MALIB.setName("MALIB");
            panel1.add(MALIB);
            MALIB.setBounds(195, 22, 310, 24);
            
            // ---- WMAG ----
            WMAG.setText("@WMAG@");
            WMAG.setOpaque(false);
            WMAG.setName("WMAG");
            panel1.add(WMAG);
            WMAG.setBounds(140, 22, 34, WMAG.getPreferredSize().height);
            
            // ---- AD111 ----
            AD111.setComponentPopupMenu(null);
            AD111.setName("AD111");
            panel1.add(AD111);
            AD111.setBounds(523, 348, 40, AD111.getPreferredSize().height);
            
            // ---- AD211 ----
            AD211.setComponentPopupMenu(null);
            AD211.setName("AD211");
            panel1.add(AD211);
            AD211.setBounds(581, 348, 40, AD211.getPreferredSize().height);
            
            // ---- AD311 ----
            AD311.setComponentPopupMenu(null);
            AD311.setName("AD311");
            panel1.add(AD311);
            AD311.setBounds(639, 348, 40, AD311.getPreferredSize().height);
            
            // ---- AD411 ----
            AD411.setComponentPopupMenu(null);
            AD411.setName("AD411");
            panel1.add(AD411);
            AD411.setBounds(697, 348, 40, AD411.getPreferredSize().height);
            
            // ---- AD112 ----
            AD112.setComponentPopupMenu(null);
            AD112.setName("AD112");
            panel1.add(AD112);
            AD112.setBounds(523, 373, 40, AD112.getPreferredSize().height);
            
            // ---- AD212 ----
            AD212.setComponentPopupMenu(null);
            AD212.setName("AD212");
            panel1.add(AD212);
            AD212.setBounds(581, 373, 40, AD212.getPreferredSize().height);
            
            // ---- AD312 ----
            AD312.setComponentPopupMenu(null);
            AD312.setName("AD312");
            panel1.add(AD312);
            AD312.setBounds(639, 373, 40, AD312.getPreferredSize().height);
            
            // ---- AD412 ----
            AD412.setComponentPopupMenu(null);
            AD412.setName("AD412");
            panel1.add(AD412);
            AD412.setBounds(697, 373, 40, AD412.getPreferredSize().height);
            
            // ---- AD113 ----
            AD113.setComponentPopupMenu(null);
            AD113.setName("AD113");
            panel1.add(AD113);
            AD113.setBounds(523, 398, 40, AD113.getPreferredSize().height);
            
            // ---- AD213 ----
            AD213.setComponentPopupMenu(null);
            AD213.setName("AD213");
            panel1.add(AD213);
            AD213.setBounds(581, 398, 40, AD213.getPreferredSize().height);
            
            // ---- AD313 ----
            AD313.setComponentPopupMenu(null);
            AD313.setName("AD313");
            panel1.add(AD313);
            AD313.setBounds(639, 398, 40, AD313.getPreferredSize().height);
            
            // ---- AD413 ----
            AD413.setComponentPopupMenu(null);
            AD413.setName("AD413");
            panel1.add(AD413);
            AD413.setBounds(697, 398, 40, AD413.getPreferredSize().height);
            
            // ---- AD114 ----
            AD114.setComponentPopupMenu(null);
            AD114.setName("AD114");
            panel1.add(AD114);
            AD114.setBounds(523, 423, 40, AD114.getPreferredSize().height);
            
            // ---- AD214 ----
            AD214.setComponentPopupMenu(null);
            AD214.setName("AD214");
            panel1.add(AD214);
            AD214.setBounds(581, 423, 40, AD214.getPreferredSize().height);
            
            // ---- AD314 ----
            AD314.setComponentPopupMenu(null);
            AD314.setName("AD314");
            panel1.add(AD314);
            AD314.setBounds(639, 423, 40, AD314.getPreferredSize().height);
            
            // ---- AD414 ----
            AD414.setComponentPopupMenu(null);
            AD414.setName("AD414");
            panel1.add(AD414);
            AD414.setBounds(697, 423, 40, AD414.getPreferredSize().height);
            
            // ---- AD115 ----
            AD115.setComponentPopupMenu(null);
            AD115.setName("AD115");
            panel1.add(AD115);
            AD115.setBounds(523, 448, 40, AD115.getPreferredSize().height);
            
            // ---- AD215 ----
            AD215.setComponentPopupMenu(null);
            AD215.setName("AD215");
            panel1.add(AD215);
            AD215.setBounds(581, 448, 40, AD215.getPreferredSize().height);
            
            // ---- AD315 ----
            AD315.setComponentPopupMenu(null);
            AD315.setName("AD315");
            panel1.add(AD315);
            AD315.setBounds(639, 448, 40, AD315.getPreferredSize().height);
            
            // ---- AD415 ----
            AD415.setComponentPopupMenu(null);
            AD415.setName("AD415");
            panel1.add(AD415);
            AD415.setBounds(697, 448, 40, AD415.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 844, Short.MAX_VALUE).addContainerGap()));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 502, Short.MAX_VALUE).addContainerGap()));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("D\u00e9tail");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie WART;
  private RiZoneSortie WETB;
  private JLabel OBJ_53;
  private JLabel OBJ_66;
  private RiZoneSortie A1LIBR;
  private JPanel p_tete_droite;
  private JLabel labelUnite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private SNBoutonDetail WTP01;
  private RiZoneSortie LD01;
  private XRiTextField AD11;
  private SNBoutonInformation PL01;
  private XRiTextField AD21;
  private XRiTextField AD31;
  private XRiTextField AD41;
  private SNBoutonDetail WTP02;
  private RiZoneSortie LD02;
  private XRiTextField AD12;
  private SNBoutonInformation PL02;
  private XRiTextField AD22;
  private XRiTextField AD32;
  private XRiTextField AD42;
  private SNBoutonDetail WTP03;
  private RiZoneSortie LD03;
  private XRiTextField AD13;
  private SNBoutonInformation PL03;
  private XRiTextField AD23;
  private XRiTextField AD33;
  private XRiTextField AD43;
  private SNBoutonDetail WTP04;
  private RiZoneSortie LD04;
  private XRiTextField AD14;
  private SNBoutonInformation PL04;
  private XRiTextField AD24;
  private XRiTextField AD34;
  private XRiTextField AD44;
  private SNBoutonDetail WTP05;
  private RiZoneSortie LD05;
  private XRiTextField AD15;
  private SNBoutonInformation PL05;
  private XRiTextField AD25;
  private XRiTextField AD35;
  private XRiTextField AD45;
  private SNBoutonDetail WTP06;
  private RiZoneSortie LD06;
  private XRiTextField AD16;
  private SNBoutonInformation PL06;
  private XRiTextField AD26;
  private XRiTextField AD36;
  private XRiTextField AD46;
  private SNBoutonDetail WTP07;
  private RiZoneSortie LD07;
  private XRiTextField AD17;
  private SNBoutonInformation PL07;
  private XRiTextField AD27;
  private XRiTextField AD37;
  private XRiTextField AD47;
  private SNBoutonDetail WTP08;
  private RiZoneSortie LD08;
  private XRiTextField AD18;
  private SNBoutonInformation PL08;
  private XRiTextField AD28;
  private XRiTextField AD38;
  private XRiTextField AD48;
  private SNBoutonDetail WTP09;
  private RiZoneSortie LD09;
  private XRiTextField AD19;
  private SNBoutonInformation PL09;
  private XRiTextField AD29;
  private XRiTextField AD39;
  private XRiTextField AD49;
  private SNBoutonDetail WTP10;
  private RiZoneSortie LD10;
  private XRiTextField AD110;
  private SNBoutonInformation PL10;
  private XRiTextField AD210;
  private XRiTextField AD310;
  private XRiTextField AD410;
  private SNBoutonDetail WTP11;
  private RiZoneSortie LD11;
  private SNBoutonInformation PL11;
  private SNBoutonDetail WTP12;
  private RiZoneSortie LD12;
  private SNBoutonInformation PL12;
  private SNBoutonDetail WTP13;
  private RiZoneSortie LD13;
  private SNBoutonInformation PL13;
  private SNBoutonDetail WTP14;
  private RiZoneSortie LD14;
  private SNBoutonInformation PL14;
  private SNBoutonDetail WTP15;
  private RiZoneSortie LD15;
  private SNBoutonInformation PL15;
  private JLabel label1;
  private JLabel AD1;
  private JLabel AD2;
  private JLabel AD3;
  private JLabel AD4;
  private JLabel OBJ_67;
  private RiZoneSortie MALIB;
  private RiZoneSortie WMAG;
  private XRiTextField AD111;
  private XRiTextField AD211;
  private XRiTextField AD311;
  private XRiTextField AD411;
  private XRiTextField AD112;
  private XRiTextField AD212;
  private XRiTextField AD312;
  private XRiTextField AD412;
  private XRiTextField AD113;
  private XRiTextField AD213;
  private XRiTextField AD313;
  private XRiTextField AD413;
  private XRiTextField AD114;
  private XRiTextField AD214;
  private XRiTextField AD314;
  private XRiTextField AD414;
  private XRiTextField AD115;
  private XRiTextField AD215;
  private XRiTextField AD315;
  private XRiTextField AD415;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
