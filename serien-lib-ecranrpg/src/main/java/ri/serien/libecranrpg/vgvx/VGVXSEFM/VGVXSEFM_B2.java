
package ri.serien.libecranrpg.vgvx.VGVXSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXSEFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] A_Value = { "", "0", "1", "2", "3", };
  private String[] B_Value = { "", "0", "1", "2", };
  private String[] D_Value = { "", "0", "1", "2", "3", "4" };
  private String[] E_Value = { "", "0", "1", "2", "3", "4", "9" };
  private String[] F_Value = { "", "0", "1", "2", "3", "4", "5", "9" };
  private String[] G_Value = { "", "0", "9" };
  private String[] H_Value = { "0", "1", "2", "3", "4", "5", "6" };
  
  public VGVXSEFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SET164.setValeurs(D_Value, null);
    SET161.setValeurs(E_Value, null);
    SET153.setValeurs(A_Value, null);
    SET031.setValeurs(F_Value, null);
    SET152.setValeursSelection("0", "1");
    SET037.setValeursSelection("0", "1");
    SET147.setValeursSelection("0", "1");
    SET143.setValeursSelection("0", "1");
    SET042.setValeursSelection("0", "1");
    SET041.setValeursSelection("0", "1");
    SET040.setValeursSelection("0", "1");
    SET039.setValeursSelection("0", "1");
    SET038.setValeursSelection("0", "1");
    SET036.setValeursSelection("0", "1");
    SET062.setValeursSelection("0", "1");
    SET056.setValeurs(B_Value, null);
    SET055.setValeurs(B_Value, null);
    SET060.setValeurs(A_Value, null);
    SET034.setValeursSelection("0", "1");
    SET061.setValeursSelection("0", "1");
    SET174.setValeursSelection("0", "1");
    SET175.setValeursSelection("0", "1");
    SET177.setValeurs(G_Value, null);
    SET151.setValeurs(H_Value, null);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Sécurité de la gestion des ventes et des achats (@LOCUSR@)")).trim());
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_97.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    V06F1.setVisible(lexique.isTrue("N53"));
    V06F.setVisible(!V06F1.isVisible());
    
    // Gestion des tops mals initialisés : si on a une case à cocher, on lit la zone correspondante. Si blanc décoche
    // comme avec la valeur 1.
    // SEUL LE 0 COCHE (autorisé).
    for (int i = 0; i < getAllComponents(this).size(); i++) {
      if (getAllComponents(this).get(i) instanceof XRiCheckBox) {
        if (lexique.HostFieldGetData(getAllComponents(this).get(i).getName()).trim().equals("")) {
          ((XRiCheckBox) getAllComponents(this).get(i)).setSelected(false);
        }
      }
    }
    
    riSousMenu6.setEnabled(lexique.isTrue("51"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (V06F1.isVisible()) {
      V06F1.setText("F");
    }
    else {
      lexique.HostFieldPutData("V06F1", 0, "F");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  public static List<Component> getAllComponents(final Container c) {
    Component[] comps = c.getComponents();
    List<Component> compList = new ArrayList<Component>();
    for (Component comp : comps) {
      compList.add(comp);
      if (comp instanceof Container) {
        compList.addAll(getAllComponents((Container) comp));
      }
    }
    return compList;
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    INDUSR = new RiZoneSortie();
    OBJ_35 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_40 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_97 = new JLabel();
    OBJ_96 = new JLabel();
    pnlSud = new JPanel();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    pnlContenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    pnlHaut = new SNPanel();
    SECLUS = new XRiTextField();
    SEPEUS = new XRiTextField();
    SEMAUS = new XRiTextField();
    OBJ_92 = new JLabel();
    OBJ_93 = new JLabel();
    SETCUS = new XRiTextField();
    OBJ_94 = new JLabel();
    OBJ_81 = new JLabel();
    SET057 = new XRiTextField();
    lab3 = new JLabel();
    pnlGauche = new SNPanel();
    OBJ_90 = new JLabel();
    OBJ_121 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_186 = new JLabel();
    OBJ_187 = new JLabel();
    OBJ_190 = new JLabel();
    OBJ_194 = new JLabel();
    OBJ_195 = new JLabel();
    OBJ_196 = new JLabel();
    OBJ_197 = new JLabel();
    OBJ_198 = new JLabel();
    OBJ_199 = new JLabel();
    OBJ_80 = new JLabel();
    lab002 = new JLabel();
    lab003 = new JLabel();
    lab004 = new JLabel();
    SET061 = new XRiCheckBox();
    SET034 = new XRiCheckBox();
    SET056 = new XRiComboBox();
    SET062 = new XRiCheckBox();
    OBJ_213 = new JLabel();
    OBJ_214 = new JLabel();
    SET031 = new XRiComboBox();
    OBJ_200 = new JLabel();
    SET032 = new XRiTextField();
    SET033 = new XRiTextField();
    SET063 = new XRiTextField();
    SET144 = new XRiTextField();
    SET148 = new XRiTextField();
    SET159 = new XRiTextField();
    SET162 = new XRiTextField();
    SET165 = new XRiTextField();
    OBJ_172 = new JLabel();
    OBJ_181 = new JLabel();
    OBJ_193 = new JLabel();
    OBJ_166 = new JLabel();
    OBJ_154 = new JLabel();
    OBJ_160 = new JLabel();
    SET169 = new XRiTextField();
    OBJ_219 = new JLabel();
    OBJ_220 = new JLabel();
    SET055 = new XRiComboBox();
    OBJ_223 = new JLabel();
    OBJ_224 = new JLabel();
    SET036 = new XRiCheckBox();
    OBJ_83 = new JLabel();
    OBJ_95 = new JLabel();
    SET037 = new XRiCheckBox();
    SET060 = new XRiComboBox();
    pnlDroite = new SNPanel();
    OBJ_201 = new JLabel();
    OBJ_202 = new JLabel();
    OBJ_203 = new JLabel();
    OBJ_204 = new JLabel();
    OBJ_205 = new JLabel();
    OBJ_206 = new JLabel();
    OBJ_207 = new JLabel();
    OBJ_208 = new JLabel();
    OBJ_209 = new JLabel();
    OBJ_210 = new JLabel();
    SET038 = new XRiCheckBox();
    SET039 = new XRiCheckBox();
    SET040 = new XRiCheckBox();
    SET041 = new XRiCheckBox();
    SET042 = new XRiCheckBox();
    SET143 = new XRiCheckBox();
    SET147 = new XRiCheckBox();
    OBJ_215 = new JLabel();
    OBJ_216 = new JLabel();
    OBJ_217 = new JLabel();
    SET152 = new XRiCheckBox();
    SET153 = new XRiComboBox();
    label15 = new JLabel();
    SET160 = new XRiTextField();
    SET161 = new XRiComboBox();
    SET164 = new XRiComboBox();
    OBJ_211 = new JLabel();
    OBJ_212 = new JLabel();
    OBJ_221 = new JLabel();
    SET170 = new XRiTextField();
    OBJ_222 = new JLabel();
    OBJ_225 = new JLabel();
    SET174 = new XRiCheckBox();
    OBJ_227 = new JLabel();
    OBJ_98 = new JLabel();
    SET175 = new XRiCheckBox();
    SET177 = new XRiComboBox();
    OBJ_218 = new JLabel();
    OBJ_228 = new JLabel();
    SET151 = new XRiComboBox();
    pnlBas = new SNPanel();
    V06F1 = new XRiTextField();
    V06F = new XRiTextField();
    OBJ_69 = new SNBoutonLeger();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 de la gestion des ventes et des achats (@LOCUSR@)");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_34 ----
          OBJ_34.setText("Utilisateur");
          OBJ_34.setName("OBJ_34");

          //---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setText("@INDUSR@");
          INDUSR.setOpaque(false);
          INDUSR.setName("INDUSR");

          //---- OBJ_35 ----
          OBJ_35.setText("Etablissement");
          OBJ_35.setName("OBJ_35");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");

          //---- OBJ_40 ----
          OBJ_40.setText("@DGNOM@");
          OBJ_40.setOpaque(false);
          OBJ_40.setName("OBJ_40");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(75, 75, 75)
                    .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
                .addGap(65, 65, 65)
                .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 339, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_97 ----
          OBJ_97.setText("@WPAGE@");
          OBJ_97.setName("OBJ_97");
          p_tete_droite.add(OBJ_97);

          //---- OBJ_96 ----
          OBJ_96.setText("Page");
          OBJ_96.setName("OBJ_96");
          p_tete_droite.add(OBJ_96);
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());

      //======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Tous droits sur tout");
              riSousMenu_bt6.setToolTipText("Tous droits sur tout");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Valider les modifications");
              riSousMenu_bt7.setToolTipText("Valider les modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);

      //======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(1000, 600));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Objet de la s\u00e9curit\u00e9 (GVM)");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //======== pnlHaut ========
          {
            pnlHaut.setBorder(new TitledBorder(""));
            pnlHaut.setOpaque(false);
            pnlHaut.setMinimumSize(new Dimension(937, 36));
            pnlHaut.setPreferredSize(new Dimension(937, 36));
            pnlHaut.setName("pnlHaut");
            pnlHaut.setLayout(null);

            //---- SECLUS ----
            SECLUS.setComponentPopupMenu(BTD);
            SECLUS.setName("SECLUS");
            pnlHaut.add(SECLUS);
            SECLUS.setBounds(808, 10, 58, SECLUS.getPreferredSize().height);

            //---- SEPEUS ----
            SEPEUS.setComponentPopupMenu(BTD);
            SEPEUS.setName("SEPEUS");
            pnlHaut.add(SEPEUS);
            SEPEUS.setBounds(658, 10, 40, SEPEUS.getPreferredSize().height);

            //---- SEMAUS ----
            SEMAUS.setComponentPopupMenu(BTD);
            SEMAUS.setName("SEMAUS");
            pnlHaut.add(SEMAUS);
            SEMAUS.setBounds(558, 10, 30, SEMAUS.getPreferredSize().height);

            //---- OBJ_92 ----
            OBJ_92.setText("Magasin");
            OBJ_92.setName("OBJ_92");
            pnlHaut.add(OBJ_92);
            OBJ_92.setBounds(503, 14, 55, 20);

            //---- OBJ_93 ----
            OBJ_93.setText("vendeur");
            OBJ_93.setName("OBJ_93");
            pnlHaut.add(OBJ_93);
            OBJ_93.setBounds(603, 14, 50, 20);

            //---- SETCUS ----
            SETCUS.setComponentPopupMenu(BTD);
            SETCUS.setName("SETCUS");
            pnlHaut.add(SETCUS);
            SETCUS.setBounds(910, 10, 20, SETCUS.getPreferredSize().height);

            //---- OBJ_94 ----
            OBJ_94.setText("T");
            OBJ_94.setName("OBJ_94");
            pnlHaut.add(OBJ_94);
            OBJ_94.setBounds(898, 14, 12, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("57");
            OBJ_81.setFont(OBJ_81.getFont().deriveFont(OBJ_81.getFont().getStyle() | Font.BOLD));
            OBJ_81.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_81.setName("OBJ_81");
            pnlHaut.add(OBJ_81);
            OBJ_81.setBounds(18, 16, 21, OBJ_81.getPreferredSize().height);

            //---- SET057 ----
            SET057.setComponentPopupMenu(BTD);
            SET057.setName("SET057");
            pnlHaut.add(SET057);
            SET057.setBounds(50, 10, 20, SET057.getPreferredSize().height);

            //---- lab3 ----
            lab3.setComponentPopupMenu(BTD);
            lab3.setText("Protection");
            lab3.setName("lab3");
            pnlHaut.add(lab3);
            lab3.setBounds(80, 10, 385, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < pnlHaut.getComponentCount(); i++) {
                Rectangle bounds = pnlHaut.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlHaut.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlHaut.setMinimumSize(preferredSize);
              pnlHaut.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel1ContentContainer.add(pnlHaut);
          pnlHaut.setBounds(10, 5, 950, 45);

          //======== pnlGauche ========
          {
            pnlGauche.setBorder(new TitledBorder(""));
            pnlGauche.setOpaque(false);
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(null);

            //---- OBJ_90 ----
            OBJ_90.setText("32");
            OBJ_90.setFont(OBJ_90.getFont().deriveFont(OBJ_90.getFont().getStyle() | Font.BOLD));
            OBJ_90.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_90.setName("OBJ_90");
            pnlGauche.add(OBJ_90);
            OBJ_90.setBounds(15, 30, 31, 25);

            //---- OBJ_121 ----
            OBJ_121.setText("33");
            OBJ_121.setFont(OBJ_121.getFont().deriveFont(OBJ_121.getFont().getStyle() | Font.BOLD));
            OBJ_121.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_121.setName("OBJ_121");
            pnlGauche.add(OBJ_121);
            OBJ_121.setBounds(15, 55, 31, 25);

            //---- OBJ_122 ----
            OBJ_122.setText("60");
            OBJ_122.setFont(OBJ_122.getFont().deriveFont(OBJ_122.getFont().getStyle() | Font.BOLD));
            OBJ_122.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_122.setName("OBJ_122");
            pnlGauche.add(OBJ_122);
            OBJ_122.setBounds(15, 80, 31, 25);

            //---- OBJ_186 ----
            OBJ_186.setText("61");
            OBJ_186.setFont(OBJ_186.getFont().deriveFont(OBJ_186.getFont().getStyle() | Font.BOLD));
            OBJ_186.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_186.setName("OBJ_186");
            pnlGauche.add(OBJ_186);
            OBJ_186.setBounds(15, 105, 31, 25);

            //---- OBJ_187 ----
            OBJ_187.setText("34");
            OBJ_187.setFont(OBJ_187.getFont().deriveFont(OBJ_187.getFont().getStyle() | Font.BOLD));
            OBJ_187.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_187.setName("OBJ_187");
            pnlGauche.add(OBJ_187);
            OBJ_187.setBounds(15, 130, 31, 25);

            //---- OBJ_190 ----
            OBJ_190.setText("55");
            OBJ_190.setFont(OBJ_190.getFont().deriveFont(OBJ_190.getFont().getStyle() | Font.BOLD));
            OBJ_190.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_190.setName("OBJ_190");
            pnlGauche.add(OBJ_190);
            OBJ_190.setBounds(15, 155, 31, 25);

            //---- OBJ_194 ----
            OBJ_194.setText("56");
            OBJ_194.setFont(OBJ_194.getFont().deriveFont(OBJ_194.getFont().getStyle() | Font.BOLD));
            OBJ_194.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_194.setName("OBJ_194");
            pnlGauche.add(OBJ_194);
            OBJ_194.setBounds(15, 180, 31, 25);

            //---- OBJ_195 ----
            OBJ_195.setText("62");
            OBJ_195.setFont(OBJ_195.getFont().deriveFont(OBJ_195.getFont().getStyle() | Font.BOLD));
            OBJ_195.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_195.setName("OBJ_195");
            pnlGauche.add(OBJ_195);
            OBJ_195.setBounds(15, 205, 31, 25);

            //---- OBJ_196 ----
            OBJ_196.setText("63");
            OBJ_196.setFont(OBJ_196.getFont().deriveFont(OBJ_196.getFont().getStyle() | Font.BOLD));
            OBJ_196.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_196.setName("OBJ_196");
            pnlGauche.add(OBJ_196);
            OBJ_196.setBounds(15, 230, 31, 25);

            //---- OBJ_197 ----
            OBJ_197.setText("144");
            OBJ_197.setFont(OBJ_197.getFont().deriveFont(OBJ_197.getFont().getStyle() | Font.BOLD));
            OBJ_197.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_197.setName("OBJ_197");
            pnlGauche.add(OBJ_197);
            OBJ_197.setBounds(15, 255, 31, 25);

            //---- OBJ_198 ----
            OBJ_198.setText("148");
            OBJ_198.setFont(OBJ_198.getFont().deriveFont(OBJ_198.getFont().getStyle() | Font.BOLD));
            OBJ_198.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_198.setName("OBJ_198");
            pnlGauche.add(OBJ_198);
            OBJ_198.setBounds(15, 280, 31, 25);

            //---- OBJ_199 ----
            OBJ_199.setText("159");
            OBJ_199.setFont(OBJ_199.getFont().deriveFont(OBJ_199.getFont().getStyle() | Font.BOLD));
            OBJ_199.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_199.setName("OBJ_199");
            pnlGauche.add(OBJ_199);
            OBJ_199.setBounds(15, 305, 31, 25);

            //---- OBJ_80 ----
            OBJ_80.setText("31");
            OBJ_80.setFont(OBJ_80.getFont().deriveFont(OBJ_80.getFont().getStyle() | Font.BOLD));
            OBJ_80.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_80.setName("OBJ_80");
            pnlGauche.add(OBJ_80);
            OBJ_80.setBounds(15, 5, 31, 25);

            //---- lab002 ----
            lab002.setComponentPopupMenu(BTD);
            lab002.setText("Modification prix ou condition");
            lab002.setName("lab002");
            pnlGauche.add(lab002);
            lab002.setBounds(80, 28, 385, 28);

            //---- lab003 ----
            lab003.setComponentPopupMenu(BTD);
            lab003.setText("Cr\u00e9ation d'avoir");
            lab003.setName("lab003");
            pnlGauche.add(lab003);
            lab003.setBounds(80, 53, 385, 28);

            //---- lab004 ----
            lab004.setToolTipText("Valeur entre 0 et 9");
            lab004.setComponentPopupMenu(BTD);
            lab004.setText("Modification avoir automatique");
            lab004.setName("lab004");
            pnlGauche.add(lab004);
            lab004.setBounds(295, 78, 170, 28);

            //---- SET061 ----
            SET061.setToolTipText("Valeur entre 0 et 9");
            SET061.setComponentPopupMenu(BTD);
            SET061.setText("Un seul avoir automatique par facture");
            SET061.setName("SET061");
            pnlGauche.add(SET061);
            SET061.setBounds(50, 108, 405, SET061.getPreferredSize().height);

            //---- SET034 ----
            SET034.setToolTipText("Valeur entre 0 et 9");
            SET034.setComponentPopupMenu(BTD);
            SET034.setText("Modification des modalit\u00e9s de r\u00e8glement");
            SET034.setName("SET034");
            pnlGauche.add(SET034);
            SET034.setBounds(50, 133, 405, SET034.getPreferredSize().height);

            //---- SET056 ----
            SET056.setToolTipText("Valeur entre 0 et 9");
            SET056.setComponentPopupMenu(BTD);
            SET056.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Annulation bon et devis autoris\u00e9e",
              "Option interdite",
              "Annulation devis autoris\u00e9e"
            }));
            SET056.setName("SET056");
            pnlGauche.add(SET056);
            SET056.setBounds(50, 179, 230, 26);

            //---- SET062 ----
            SET062.setToolTipText("Valeur entre 0 et 9");
            SET062.setComponentPopupMenu(BTD);
            SET062.setText("Modification commissionnement");
            SET062.setName("SET062");
            pnlGauche.add(SET062);
            SET062.setBounds(50, 208, 405, SET062.getPreferredSize().height);

            //---- OBJ_213 ----
            OBJ_213.setText("162");
            OBJ_213.setFont(OBJ_213.getFont().deriveFont(OBJ_213.getFont().getStyle() | Font.BOLD));
            OBJ_213.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_213.setName("OBJ_213");
            pnlGauche.add(OBJ_213);
            OBJ_213.setBounds(15, 330, 31, 25);

            //---- OBJ_214 ----
            OBJ_214.setText("165");
            OBJ_214.setFont(OBJ_214.getFont().deriveFont(OBJ_214.getFont().getStyle() | Font.BOLD));
            OBJ_214.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_214.setName("OBJ_214");
            pnlGauche.add(OBJ_214);
            OBJ_214.setBounds(15, 355, 31, 25);

            //---- SET031 ----
            SET031.setComponentPopupMenu(BTD);
            SET031.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "Droits VAL, EXP et FAC",
              "Droits VAL et EXP",
              "Droits VAL uniquement",
              "Droits ATT seulement",
              "Affichage seulement",
              "Responsable commercial"
            }));
            SET031.setName("SET031");
            pnlGauche.add(SET031);
            SET031.setBounds(50, 4, 210, SET031.getPreferredSize().height);

            //---- OBJ_200 ----
            OBJ_200.setText("Gestion des bons et factures");
            OBJ_200.setName("OBJ_200");
            pnlGauche.add(OBJ_200);
            OBJ_200.setBounds(265, 3, 205, 28);

            //---- SET032 ----
            SET032.setComponentPopupMenu(BTD);
            SET032.setName("SET032");
            pnlGauche.add(SET032);
            SET032.setBounds(50, 28, 20, SET032.getPreferredSize().height);

            //---- SET033 ----
            SET033.setComponentPopupMenu(BTD);
            SET033.setName("SET033");
            pnlGauche.add(SET033);
            SET033.setBounds(50, 53, 20, SET033.getPreferredSize().height);

            //---- SET063 ----
            SET063.setComponentPopupMenu(BTD);
            SET063.setName("SET063");
            pnlGauche.add(SET063);
            SET063.setBounds(50, 228, 20, SET063.getPreferredSize().height);

            //---- SET144 ----
            SET144.setComponentPopupMenu(BTD);
            SET144.setName("SET144");
            pnlGauche.add(SET144);
            SET144.setBounds(50, 253, 20, SET144.getPreferredSize().height);

            //---- SET148 ----
            SET148.setComponentPopupMenu(BTD);
            SET148.setName("SET148");
            pnlGauche.add(SET148);
            SET148.setBounds(50, 278, 20, SET148.getPreferredSize().height);

            //---- SET159 ----
            SET159.setComponentPopupMenu(BTD);
            SET159.setName("SET159");
            pnlGauche.add(SET159);
            SET159.setBounds(50, 303, 20, SET159.getPreferredSize().height);

            //---- SET162 ----
            SET162.setComponentPopupMenu(BTD);
            SET162.setName("SET162");
            pnlGauche.add(SET162);
            SET162.setBounds(50, 328, 20, SET162.getPreferredSize().height);

            //---- SET165 ----
            SET165.setComponentPopupMenu(BTD);
            SET165.setName("SET165");
            pnlGauche.add(SET165);
            SET165.setBounds(50, 353, 20, SET165.getPreferredSize().height);

            //---- OBJ_172 ----
            OBJ_172.setText("Pas de contr\u00f4le de marge mini");
            OBJ_172.setName("OBJ_172");
            pnlGauche.add(OBJ_172);
            OBJ_172.setBounds(80, 303, 385, 28);

            //---- OBJ_181 ----
            OBJ_181.setText("Type d'enchainement d'\u00e9dition de bon");
            OBJ_181.setName("OBJ_181");
            pnlGauche.add(OBJ_181);
            OBJ_181.setBounds(80, 328, 385, 28);

            //---- OBJ_193 ----
            OBJ_193.setText("Cr\u00e9ation d'article provisoire");
            OBJ_193.setName("OBJ_193");
            pnlGauche.add(OBJ_193);
            OBJ_193.setBounds(80, 353, 385, 28);

            //---- OBJ_166 ----
            OBJ_166.setText("Changement de tiers sur bon");
            OBJ_166.setName("OBJ_166");
            pnlGauche.add(OBJ_166);
            OBJ_166.setBounds(80, 278, 385, 28);

            //---- OBJ_154 ----
            OBJ_154.setText("Modification date traitement");
            OBJ_154.setName("OBJ_154");
            pnlGauche.add(OBJ_154);
            OBJ_154.setBounds(80, 228, 385, 28);

            //---- OBJ_160 ----
            OBJ_160.setText("Extraction obligatoire sur exp\u00e9dition");
            OBJ_160.setName("OBJ_160");
            pnlGauche.add(OBJ_160);
            OBJ_160.setBounds(80, 253, 385, 28);

            //---- SET169 ----
            SET169.setComponentPopupMenu(BTD);
            SET169.setName("SET169");
            pnlGauche.add(SET169);
            SET169.setBounds(50, 378, 20, SET169.getPreferredSize().height);

            //---- OBJ_219 ----
            OBJ_219.setText("169");
            OBJ_219.setFont(OBJ_219.getFont().deriveFont(OBJ_219.getFont().getStyle() | Font.BOLD));
            OBJ_219.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_219.setName("OBJ_219");
            pnlGauche.add(OBJ_219);
            OBJ_219.setBounds(15, 380, 31, 25);

            //---- OBJ_220 ----
            OBJ_220.setText("Modification du prix ou des conditions sur les devis");
            OBJ_220.setName("OBJ_220");
            pnlGauche.add(OBJ_220);
            OBJ_220.setBounds(80, 378, 385, 28);

            //---- SET055 ----
            SET055.setToolTipText("Valeur entre 0 et 9");
            SET055.setComponentPopupMenu(BTD);
            SET055.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Option autoris\u00e9e",
              "Option interdite",
              "Option autoris\u00e9e, marge au PRS"
            }));
            SET055.setName("SET055");
            pnlGauche.add(SET055);
            SET055.setBounds(50, 154, 230, 26);

            //---- OBJ_223 ----
            OBJ_223.setText("Possibilit\u00e9s pour l'option MAR");
            OBJ_223.setName("OBJ_223");
            pnlGauche.add(OBJ_223);
            OBJ_223.setBounds(285, 153, 185, 28);

            //---- OBJ_224 ----
            OBJ_224.setText("Annulation de bons et de lignes");
            OBJ_224.setName("OBJ_224");
            pnlGauche.add(OBJ_224);
            OBJ_224.setBounds(285, 178, 185, 28);

            //---- SET036 ----
            SET036.setToolTipText("Valeur entre 0 et 9");
            SET036.setComponentPopupMenu(BTD);
            SET036.setText("Edition de bons de commande");
            SET036.setName("SET036");
            pnlGauche.add(SET036);
            SET036.setBounds(50, 408, 405, SET036.getPreferredSize().height);

            //---- OBJ_83 ----
            OBJ_83.setText("36");
            OBJ_83.setFont(OBJ_83.getFont().deriveFont(OBJ_83.getFont().getStyle() | Font.BOLD));
            OBJ_83.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_83.setName("OBJ_83");
            pnlGauche.add(OBJ_83);
            OBJ_83.setBounds(15, 405, 31, 25);

            //---- OBJ_95 ----
            OBJ_95.setText("37");
            OBJ_95.setFont(OBJ_95.getFont().deriveFont(OBJ_95.getFont().getStyle() | Font.BOLD));
            OBJ_95.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_95.setName("OBJ_95");
            pnlGauche.add(OBJ_95);
            OBJ_95.setBounds(15, 430, 31, 25);

            //---- SET037 ----
            SET037.setToolTipText("Valeur entre 0 et 9");
            SET037.setComponentPopupMenu(BTD);
            SET037.setText("Edition de factures");
            SET037.setName("SET037");
            pnlGauche.add(SET037);
            SET037.setBounds(50, 433, 405, SET037.getPreferredSize().height);

            //---- SET060 ----
            SET060.setPreferredSize(new Dimension(206, 26));
            SET060.setMinimumSize(new Dimension(206, 26));
            SET060.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "Cr\u00e9ation autoris\u00e9e, modification interdite",
              "Cr\u00e9ation interdite, modification autoris\u00e9e",
              "Cr\u00e9ation interdite, modification interdite"
            }));
            SET060.setName("SET060");
            pnlGauche.add(SET060);
            SET060.setBounds(50, 80, 240, SET060.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < pnlGauche.getComponentCount(); i++) {
                Rectangle bounds = pnlGauche.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlGauche.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlGauche.setMinimumSize(preferredSize);
              pnlGauche.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel1ContentContainer.add(pnlGauche);
          pnlGauche.setBounds(10, 55, 470, 460);

          //======== pnlDroite ========
          {
            pnlDroite.setBorder(new TitledBorder(""));
            pnlDroite.setOpaque(false);
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(null);

            //---- OBJ_201 ----
            OBJ_201.setText("38");
            OBJ_201.setFont(OBJ_201.getFont().deriveFont(OBJ_201.getFont().getStyle() | Font.BOLD));
            OBJ_201.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_201.setName("OBJ_201");
            pnlDroite.add(OBJ_201);
            OBJ_201.setBounds(15, 5, 31, 23);

            //---- OBJ_202 ----
            OBJ_202.setText("39");
            OBJ_202.setFont(OBJ_202.getFont().deriveFont(OBJ_202.getFont().getStyle() | Font.BOLD));
            OBJ_202.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_202.setName("OBJ_202");
            pnlDroite.add(OBJ_202);
            OBJ_202.setBounds(15, 29, 31, 26);

            //---- OBJ_203 ----
            OBJ_203.setText("40");
            OBJ_203.setFont(OBJ_203.getFont().deriveFont(OBJ_203.getFont().getStyle() | Font.BOLD));
            OBJ_203.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_203.setName("OBJ_203");
            pnlDroite.add(OBJ_203);
            OBJ_203.setBounds(15, 56, 31, 24);

            //---- OBJ_204 ----
            OBJ_204.setText("41");
            OBJ_204.setFont(OBJ_204.getFont().deriveFont(OBJ_204.getFont().getStyle() | Font.BOLD));
            OBJ_204.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_204.setName("OBJ_204");
            pnlDroite.add(OBJ_204);
            OBJ_204.setBounds(15, 81, 31, 27);

            //---- OBJ_205 ----
            OBJ_205.setText("42");
            OBJ_205.setFont(OBJ_205.getFont().deriveFont(OBJ_205.getFont().getStyle() | Font.BOLD));
            OBJ_205.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_205.setName("OBJ_205");
            pnlDroite.add(OBJ_205);
            OBJ_205.setBounds(15, 109, 31, 25);

            //---- OBJ_206 ----
            OBJ_206.setText("143");
            OBJ_206.setFont(OBJ_206.getFont().deriveFont(OBJ_206.getFont().getStyle() | Font.BOLD));
            OBJ_206.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_206.setName("OBJ_206");
            pnlDroite.add(OBJ_206);
            OBJ_206.setBounds(15, 135, 31, 23);

            //---- OBJ_207 ----
            OBJ_207.setText("147");
            OBJ_207.setFont(OBJ_207.getFont().deriveFont(OBJ_207.getFont().getStyle() | Font.BOLD));
            OBJ_207.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_207.setName("OBJ_207");
            pnlDroite.add(OBJ_207);
            OBJ_207.setBounds(15, 159, 31, 26);

            //---- OBJ_208 ----
            OBJ_208.setText("151");
            OBJ_208.setFont(OBJ_208.getFont().deriveFont(OBJ_208.getFont().getStyle() | Font.BOLD));
            OBJ_208.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_208.setName("OBJ_208");
            pnlDroite.add(OBJ_208);
            OBJ_208.setBounds(15, 186, 31, 24);

            //---- OBJ_209 ----
            OBJ_209.setText("152");
            OBJ_209.setFont(OBJ_209.getFont().deriveFont(OBJ_209.getFont().getStyle() | Font.BOLD));
            OBJ_209.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_209.setName("OBJ_209");
            pnlDroite.add(OBJ_209);
            OBJ_209.setBounds(15, 211, 31, 27);

            //---- OBJ_210 ----
            OBJ_210.setText("153");
            OBJ_210.setFont(OBJ_210.getFont().deriveFont(OBJ_210.getFont().getStyle() | Font.BOLD));
            OBJ_210.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_210.setName("OBJ_210");
            pnlDroite.add(OBJ_210);
            OBJ_210.setBounds(15, 236, 31, 25);

            //---- SET038 ----
            SET038.setToolTipText("Valeur entre 0 et 9");
            SET038.setComponentPopupMenu(BTD);
            SET038.setText("Edition de bons d'exp\u00e9dition");
            SET038.setName("SET038");
            pnlDroite.add(SET038);
            SET038.setBounds(55, 7, 405, SET038.getPreferredSize().height);

            //---- SET039 ----
            SET039.setToolTipText("Valeur entre 0 et 9");
            SET039.setComponentPopupMenu(BTD);
            SET039.setText("Edition de bons de facturation");
            SET039.setName("SET039");
            pnlDroite.add(SET039);
            SET039.setBounds(55, 30, 405, SET039.getPreferredSize().height);

            //---- SET040 ----
            SET040.setToolTipText("Valeur entre 0 et 9");
            SET040.setComponentPopupMenu(BTD);
            SET040.setText("Edition de relev\u00e9s");
            SET040.setName("SET040");
            pnlDroite.add(SET040);
            SET040.setBounds(55, 60, 405, SET040.getPreferredSize().height);

            //---- SET041 ----
            SET041.setToolTipText("Valeur entre 0 et 9");
            SET041.setComponentPopupMenu(BTD);
            SET041.setText("Edition de traites");
            SET041.setName("SET041");
            pnlDroite.add(SET041);
            SET041.setBounds(55, 85, 405, SET041.getPreferredSize().height);

            //---- SET042 ----
            SET042.setToolTipText("Valeur entre 0 et 9");
            SET042.setComponentPopupMenu(BTD);
            SET042.setText("Etats de bons");
            SET042.setName("SET042");
            pnlDroite.add(SET042);
            SET042.setBounds(55, 110, 405, SET042.getPreferredSize().height);

            //---- SET143 ----
            SET143.setToolTipText("Valeur entre 0 et 9");
            SET143.setComponentPopupMenu(BTD);
            SET143.setText("Modification magasin en \u00e9dition de bons");
            SET143.setName("SET143");
            pnlDroite.add(SET143);
            SET143.setBounds(55, 135, 405, SET143.getPreferredSize().height);

            //---- SET147 ----
            SET147.setToolTipText("Valeur entre 0 et 9");
            SET147.setComponentPopupMenu(BTD);
            SET147.setText("Etats des livraisons");
            SET147.setName("SET147");
            pnlDroite.add(SET147);
            SET147.setBounds(55, 160, 405, SET147.getPreferredSize().height);

            //---- OBJ_215 ----
            OBJ_215.setText("160");
            OBJ_215.setFont(OBJ_215.getFont().deriveFont(OBJ_215.getFont().getStyle() | Font.BOLD));
            OBJ_215.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_215.setName("OBJ_215");
            pnlDroite.add(OBJ_215);
            OBJ_215.setBounds(15, 263, 31, 23);

            //---- OBJ_216 ----
            OBJ_216.setText("161");
            OBJ_216.setFont(OBJ_216.getFont().deriveFont(OBJ_216.getFont().getStyle() | Font.BOLD));
            OBJ_216.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_216.setName("OBJ_216");
            pnlDroite.add(OBJ_216);
            OBJ_216.setBounds(15, 289, 31, 26);

            //---- OBJ_217 ----
            OBJ_217.setText("164");
            OBJ_217.setFont(OBJ_217.getFont().deriveFont(OBJ_217.getFont().getStyle() | Font.BOLD));
            OBJ_217.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_217.setName("OBJ_217");
            pnlDroite.add(OBJ_217);
            OBJ_217.setBounds(15, 316, 31, 24);

            //---- SET152 ----
            SET152.setToolTipText("Valeur entre 0 et 9");
            SET152.setComponentPopupMenu(BTD);
            SET152.setText("Type de client unique");
            SET152.setName("SET152");
            pnlDroite.add(SET152);
            SET152.setBounds(55, 215, 405, SET152.getPreferredSize().height);

            //---- SET153 ----
            SET153.setComponentPopupMenu(BTD);
            SET153.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit"
            }));
            SET153.setName("SET153");
            pnlDroite.add(SET153);
            SET153.setBounds(new Rectangle(new Point(55, 235), SET153.getPreferredSize()));

            //---- label15 ----
            label15.setText("Gestion des quotas");
            label15.setName("label15");
            pnlDroite.add(label15);
            label15.setBounds(244, 235, 220, 26);

            //---- SET160 ----
            SET160.setComponentPopupMenu(BTD);
            SET160.setName("SET160");
            pnlDroite.add(SET160);
            SET160.setBounds(55, 260, 20, SET160.getPreferredSize().height);

            //---- SET161 ----
            SET161.setComponentPopupMenu(BTD);
            SET161.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "Droits HOM, EXP et FAC",
              "Droits HOM et EXP",
              "Droits FAC uniquement",
              "Droits ATT seulement",
              "Superviseur"
            }));
            SET161.setName("SET161");
            pnlDroite.add(SET161);
            SET161.setBounds(55, 290, 210, SET161.getPreferredSize().height);

            //---- SET164 ----
            SET164.setComponentPopupMenu(BTD);
            SET164.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "Droits HOM, EXP et FAC",
              "Droits HOM et EXP",
              "Droits FAC uniquement",
              "Droits ATT seulement"
            }));
            SET164.setName("SET164");
            pnlDroite.add(SET164);
            SET164.setBounds(55, 315, 210, SET164.getPreferredSize().height);

            //---- OBJ_211 ----
            OBJ_211.setText("Gestion des devis");
            OBJ_211.setName("OBJ_211");
            pnlDroite.add(OBJ_211);
            OBJ_211.setBounds(270, 290, 194, 26);

            //---- OBJ_212 ----
            OBJ_212.setText("Gestion des not\u00e9s");
            OBJ_212.setName("OBJ_212");
            pnlDroite.add(OBJ_212);
            OBJ_212.setBounds(270, 315, 194, 26);

            //---- OBJ_221 ----
            OBJ_221.setText("Saisie quantit\u00e9 non affect\u00e9e");
            OBJ_221.setName("OBJ_221");
            pnlDroite.add(OBJ_221);
            OBJ_221.setBounds(85, 340, 385, 28);

            //---- SET170 ----
            SET170.setComponentPopupMenu(BTD);
            SET170.setName("SET170");
            pnlDroite.add(SET170);
            SET170.setBounds(55, 340, 20, SET170.getPreferredSize().height);

            //---- OBJ_222 ----
            OBJ_222.setText("170");
            OBJ_222.setFont(OBJ_222.getFont().deriveFont(OBJ_222.getFont().getStyle() | Font.BOLD));
            OBJ_222.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_222.setName("OBJ_222");
            pnlDroite.add(OBJ_222);
            OBJ_222.setBounds(15, 341, 31, 27);

            //---- OBJ_225 ----
            OBJ_225.setText("Modification du prix de revient");
            OBJ_225.setName("OBJ_225");
            pnlDroite.add(OBJ_225);
            OBJ_225.setBounds(85, 260, 385, 28);

            //---- SET174 ----
            SET174.setComponentPopupMenu(BTD);
            SET174.setText("Saisie d'articles hors gamme");
            SET174.setName("SET174");
            pnlDroite.add(SET174);
            SET174.setBounds(55, 370, 405, SET174.getPreferredSize().height);

            //---- OBJ_227 ----
            OBJ_227.setText("174");
            OBJ_227.setFont(OBJ_227.getFont().deriveFont(OBJ_227.getFont().getStyle() | Font.BOLD));
            OBJ_227.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_227.setName("OBJ_227");
            pnlDroite.add(OBJ_227);
            OBJ_227.setBounds(15, 369, 31, 25);

            //---- OBJ_98 ----
            OBJ_98.setText("175");
            OBJ_98.setFont(OBJ_98.getFont().deriveFont(OBJ_98.getFont().getStyle() | Font.BOLD));
            OBJ_98.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_98.setName("OBJ_98");
            pnlDroite.add(OBJ_98);
            OBJ_98.setBounds(15, 395, 31, 23);

            //---- SET175 ----
            SET175.setToolTipText("Valeur entre 0 et 9");
            SET175.setComponentPopupMenu(BTD);
            SET175.setText("Changement client en compte pour un document au comptoir");
            SET175.setName("SET175");
            pnlDroite.add(SET175);
            SET175.setBounds(55, 395, 405, SET175.getPreferredSize().height);

            //---- SET177 ----
            SET177.setComponentPopupMenu(BTD);
            SET177.setModel(new DefaultComboBoxModel(new String[] {
              "Aucun droit ",
              "Tous les droits",
              "Droits superviseur"
            }));
            SET177.setName("SET177");
            pnlDroite.add(SET177);
            SET177.setBounds(55, 415, 210, SET177.getPreferredSize().height);

            //---- OBJ_218 ----
            OBJ_218.setText("177");
            OBJ_218.setFont(OBJ_218.getFont().deriveFont(OBJ_218.getFont().getStyle() | Font.BOLD));
            OBJ_218.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_218.setName("OBJ_218");
            pnlDroite.add(OBJ_218);
            OBJ_218.setBounds(15, 416, 31, 24);

            //---- OBJ_228 ----
            OBJ_228.setText("Gestion des chantiers");
            OBJ_228.setName("OBJ_228");
            pnlDroite.add(OBJ_228);
            OBJ_228.setBounds(270, 415, 194, 26);

            //---- SET151 ----
            SET151.setModel(new DefaultComboBoxModel(new String[] {
              "Modification bon pr\u00e9par\u00e9 ou exp\u00e9di\u00e9 autoris\u00e9e",
              "Modification bon pr\u00e9par\u00e9 et r\u00e9serv\u00e9 interdit",
              "Modification bon pr\u00e9par\u00e9 et exp\u00e9di\u00e9 interdit",
              "Modification bon exp\u00e9di\u00e9 interdit",
              "Modification bon r\u00e9serv\u00e9 interdit",
              "Modification bon exp\u00e9di\u00e9 interdit, option pied de page autoris\u00e9e",
              "Modification bon r\u00e9serv\u00e9 interdit mais option en-tete autoris\u00e9e"
            }));
            SET151.setName("SET151");
            pnlDroite.add(SET151);
            SET151.setBounds(55, 185, 400, SET151.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < pnlDroite.getComponentCount(); i++) {
                Rectangle bounds = pnlDroite.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlDroite.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlDroite.setMinimumSize(preferredSize);
              pnlDroite.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel1ContentContainer.add(pnlDroite);
          pnlDroite.setBounds(490, 55, 470, 460);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(15, 10, 974, 550);

        //======== pnlBas ========
        {
          pnlBas.setOpaque(false);
          pnlBas.setName("pnlBas");
          pnlBas.setLayout(null);

          //---- V06F1 ----
          V06F1.setComponentPopupMenu(BTD);
          V06F1.setName("V06F1");
          pnlBas.add(V06F1);
          V06F1.setBounds(215, 0, 25, V06F1.getPreferredSize().height);

          //---- V06F ----
          V06F.setComponentPopupMenu(BTD);
          V06F.setName("V06F");
          pnlBas.add(V06F);
          V06F.setBounds(215, 0, 25, V06F.getPreferredSize().height);

          //---- OBJ_69 ----
          OBJ_69.setText("Aller \u00e0 la page");
          OBJ_69.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_69.setName("OBJ_69");
          OBJ_69.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_69ActionPerformed(e);
            }
          });
          pnlBas.add(OBJ_69);
          OBJ_69.setBounds(new Rectangle(new Point(70, 0), OBJ_69.getPreferredSize()));

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < pnlBas.getComponentCount(); i++) {
              Rectangle bounds = pnlBas.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlBas.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlBas.setMinimumSize(preferredSize);
            pnlBas.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(pnlBas);
        pnlBas.setBounds(732, 560, 267, 34);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_34;
  private RiZoneSortie INDUSR;
  private JLabel OBJ_35;
  private RiZoneSortie INDETB;
  private RiZoneSortie OBJ_40;
  private JPanel p_tete_droite;
  private JLabel OBJ_97;
  private JLabel OBJ_96;
  private JPanel pnlSud;
  private JPanel pnlMenus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel pnlContenu;
  private JXTitledPanel xTitledPanel1;
  private SNPanel pnlHaut;
  private XRiTextField SECLUS;
  private XRiTextField SEPEUS;
  private XRiTextField SEMAUS;
  private JLabel OBJ_92;
  private JLabel OBJ_93;
  private XRiTextField SETCUS;
  private JLabel OBJ_94;
  private JLabel OBJ_81;
  private XRiTextField SET057;
  private JLabel lab3;
  private SNPanel pnlGauche;
  private JLabel OBJ_90;
  private JLabel OBJ_121;
  private JLabel OBJ_122;
  private JLabel OBJ_186;
  private JLabel OBJ_187;
  private JLabel OBJ_190;
  private JLabel OBJ_194;
  private JLabel OBJ_195;
  private JLabel OBJ_196;
  private JLabel OBJ_197;
  private JLabel OBJ_198;
  private JLabel OBJ_199;
  private JLabel OBJ_80;
  private JLabel lab002;
  private JLabel lab003;
  private JLabel lab004;
  private XRiCheckBox SET061;
  private XRiCheckBox SET034;
  private XRiComboBox SET056;
  private XRiCheckBox SET062;
  private JLabel OBJ_213;
  private JLabel OBJ_214;
  private XRiComboBox SET031;
  private JLabel OBJ_200;
  private XRiTextField SET032;
  private XRiTextField SET033;
  private XRiTextField SET063;
  private XRiTextField SET144;
  private XRiTextField SET148;
  private XRiTextField SET159;
  private XRiTextField SET162;
  private XRiTextField SET165;
  private JLabel OBJ_172;
  private JLabel OBJ_181;
  private JLabel OBJ_193;
  private JLabel OBJ_166;
  private JLabel OBJ_154;
  private JLabel OBJ_160;
  private XRiTextField SET169;
  private JLabel OBJ_219;
  private JLabel OBJ_220;
  private XRiComboBox SET055;
  private JLabel OBJ_223;
  private JLabel OBJ_224;
  private XRiCheckBox SET036;
  private JLabel OBJ_83;
  private JLabel OBJ_95;
  private XRiCheckBox SET037;
  private XRiComboBox SET060;
  private SNPanel pnlDroite;
  private JLabel OBJ_201;
  private JLabel OBJ_202;
  private JLabel OBJ_203;
  private JLabel OBJ_204;
  private JLabel OBJ_205;
  private JLabel OBJ_206;
  private JLabel OBJ_207;
  private JLabel OBJ_208;
  private JLabel OBJ_209;
  private JLabel OBJ_210;
  private XRiCheckBox SET038;
  private XRiCheckBox SET039;
  private XRiCheckBox SET040;
  private XRiCheckBox SET041;
  private XRiCheckBox SET042;
  private XRiCheckBox SET143;
  private XRiCheckBox SET147;
  private JLabel OBJ_215;
  private JLabel OBJ_216;
  private JLabel OBJ_217;
  private XRiCheckBox SET152;
  private XRiComboBox SET153;
  private JLabel label15;
  private XRiTextField SET160;
  private XRiComboBox SET161;
  private XRiComboBox SET164;
  private JLabel OBJ_211;
  private JLabel OBJ_212;
  private JLabel OBJ_221;
  private XRiTextField SET170;
  private JLabel OBJ_222;
  private JLabel OBJ_225;
  private XRiCheckBox SET174;
  private JLabel OBJ_227;
  private JLabel OBJ_98;
  private XRiCheckBox SET175;
  private XRiComboBox SET177;
  private JLabel OBJ_218;
  private JLabel OBJ_228;
  private XRiComboBox SET151;
  private SNPanel pnlBas;
  private XRiTextField V06F1;
  private XRiTextField V06F;
  private SNBoutonLeger OBJ_69;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
