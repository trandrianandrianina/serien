
package ri.serien.libecranrpg.vgvx.VGVX05FM;
// Nom Fichier: pop_VGVX05FM_FMTCD_1030.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class VGVX05FM_CD extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVX05FM_CD(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    S5T08.setValeursSelection("1", "0");
    S5T02.setValeursSelection("1", "0");
    S5T01.setValeursSelection("1", "0");
    S5T03.setValeursSelection("1", "0");
    S5T09.setValeursSelection("1", "0");
    S5T05.setValeursSelection("1", "0");
    S5T07.setValeursSelection("1", "0");
    
    // Bouton par défaut
    setDefaultButton(val);
    
    
    val.setIcon(lexique.chargerImage("images/OK_p.png", true));
    retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle("Duplication des fiches liées");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    S5T07 = new XRiCheckBox();
    S5T05 = new XRiCheckBox();
    S5T09 = new XRiCheckBox();
    S5T03 = new XRiCheckBox();
    S5T01 = new XRiCheckBox();
    S5T02 = new XRiCheckBox();
    S5T08 = new XRiCheckBox();
    xTitledSeparator1 = new JXTitledSeparator();
    val = new JButton();
    retour = new JButton();

    //======== this ========
    setBackground(new Color(90, 90, 90));
    setPreferredSize(new Dimension(515, 240));
    setMinimumSize(new Dimension(515, 240));
    setName("this");

    //---- S5T07 ----
    S5T07.setText("Bloc-notes");
    S5T07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    S5T07.setName("S5T07");

    //---- S5T05 ----
    S5T05.setText("Extensions");
    S5T05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    S5T05.setName("S5T05");

    //---- S5T09 ----
    S5T09.setText("Tarifs de vente");
    S5T09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    S5T09.setName("S5T09");

    //---- S5T03 ----
    S5T03.setText("Conditions d'achat");
    S5T03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    S5T03.setName("S5T03");

    //---- S5T01 ----
    S5T01.setText("Adresses de stockage");
    S5T01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    S5T01.setName("S5T01");

    //---- S5T02 ----
    S5T02.setText("Articles li\u00e9s");
    S5T02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    S5T02.setName("S5T02");

    //---- S5T08 ----
    S5T08.setText("Liaisons libell\u00e9s");
    S5T08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    S5T08.setName("S5T08");

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Duplication des fiches li\u00e9es");
    xTitledSeparator1.setName("xTitledSeparator1");

    //---- val ----
    val.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    val.setText("Valider");
    val.setFont(val.getFont().deriveFont(val.getFont().getStyle() | Font.BOLD, val.getFont().getSize() + 2f));
    val.setName("val");
    val.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_19ActionPerformed(e);
      }
    });

    //---- retour ----
    retour.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    retour.setText("Retour");
    retour.setFont(retour.getFont().deriveFont(retour.getFont().getStyle() | Font.BOLD, retour.getFont().getSize() + 2f));
    retour.setName("retour");
    retour.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        retourActionPerformed(e);
      }
    });

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 480, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(65, 65, 65)
          .addComponent(S5T07, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
          .addGap(136, 136, 136)
          .addComponent(S5T01, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(65, 65, 65)
          .addComponent(S5T05, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
          .addGap(135, 135, 135)
          .addComponent(S5T02, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(65, 65, 65)
          .addComponent(S5T09, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
          .addGap(111, 111, 111)
          .addComponent(S5T08, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(65, 65, 65)
          .addComponent(S5T03, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(230, 230, 230)
          .addComponent(val, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
          .addGap(5, 5, 5)
          .addComponent(retour, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(21, 21, 21)
          .addGroup(layout.createParallelGroup()
            .addComponent(S5T07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addComponent(S5T01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          .addGap(8, 8, 8)
          .addGroup(layout.createParallelGroup()
            .addComponent(S5T05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addComponent(S5T02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          .addGap(8, 8, 8)
          .addGroup(layout.createParallelGroup()
            .addComponent(S5T09, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
            .addComponent(S5T08, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          .addGap(8, 8, 8)
          .addComponent(S5T03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
          .addGap(36, 36, 36)
          .addGroup(layout.createParallelGroup()
            .addComponent(val, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addComponent(retour, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
    );
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private XRiCheckBox S5T07;
  private XRiCheckBox S5T05;
  private XRiCheckBox S5T09;
  private XRiCheckBox S5T03;
  private XRiCheckBox S5T01;
  private XRiCheckBox S5T02;
  private XRiCheckBox S5T08;
  private JXTitledSeparator xTitledSeparator1;
  private JButton val;
  private JButton retour;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
