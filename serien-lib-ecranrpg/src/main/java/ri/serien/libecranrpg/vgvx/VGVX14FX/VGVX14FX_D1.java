
package ri.serien.libecranrpg.vgvx.VGVX14FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVX14FX_D1 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_ATTENTE = "Mettre en attente";
  private static final String BOUTON_ANNULATION = "Annuler le bordereau";
  
  private String[] _LD01_Title = { "HLD01", };
  private String[][] _LD01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _LD01_Width = { 37, };
  private String[] listeCellules =
      { "LD01", "LD02", "LD03", "LD04", "LD05", "LD06", "LD07", "LD08", "LD09", "LD10", "LD11", "LD12", "LD13", "LD14", "LD15" };
  private Color[][] couleurLigne = new Color[listeCellules.length][1];
  boolean isConsult = false;
  private String[] WREP4_Value = { "", "E", "C", "F", "V" };
  
  public VGVX14FX_D1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    LD01.setAspectTable(null, _LD01_Title, _LD01_Data, _LD01_Width, false, null, couleurLigne, null, null);
    WREP4.setValeurs(WREP4_Value, null);
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_ATTENTE, 't', true);
    snBarreBouton.ajouterBouton(BOUTON_ANNULATION, 'n', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.RETOURNER_LIGNES, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, false);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Pied de bordereau de stock : @TITB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    gererCouleurListe();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    isConsult = lexique.isTrue("46");
    
    // Code établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "E1ETB");
    snEtablissement.setEnabled(false);
    
    // Magasins
    snMagasinEmetteur.setSession(getSession());
    snMagasinEmetteur.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinEmetteur.charger(false);
    snMagasinEmetteur.setSelectionParChampRPG(lexique, "E1MAG");
    
    snMagasinRecepteur.setSession(getSession());
    snMagasinRecepteur.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasinRecepteur.charger(false);
    snMagasinRecepteur.setSelectionParChampRPG(lexique, "E1MAG2");
    snMagasinRecepteur.setVisible(lexique.isTrue("(34)"));
    lbMagasinRecepteur.setVisible(snMagasinRecepteur.isVisible());
    lbMagasinRecepteur.setText(lexique.HostFieldGetData("TITR26"));
    
    // Tiers
    snClient1.setSession(getSession());
    snClient1.setIdEtablissement(snEtablissement.getIdSelection());
    snClient1.charger(false);
    snClient1.setSelectionParChampRPG(lexique, "EMTTI1", "EMTTI2");
    snClient1.setVisible(lexique.isTrue("71"));
    lbTiers1.setVisible(snClient1.isVisible());
    
    snClient2.setSession(getSession());
    snClient2.setIdEtablissement(snEtablissement.getIdSelection());
    snClient2.charger(false);
    snClient2.setSelectionParChampRPG(lexique, "TRSTI1", "TRSTI2");
    snClient2.setVisible(lexique.isTrue("(34) AND (73)"));
    lbTiers2.setVisible(snClient2.isVisible());
    
    pnlOptionEdition.setVisible(!isConsult);
    
    // Boutons
    traiterVisibiliteBouton();
    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  /**
   * Traiter la visibilité des boutons
   */
  private void traiterVisibiliteBouton() {
    boolean actif = true;
    // Validation
    if (isConsult || lexique.isTrue("21")) {
      actif = false;
    }
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
    
    // Autres boutons
    actif = true;
    if (isConsult) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_ANNULATION, actif);
    snBarreBouton.activerBouton(BOUTON_ATTENTE, actif);
    snBarreBouton.activerBouton(EnumBouton.RETOURNER_LIGNES, actif);
    snBarreBouton.activerBouton(EnumBouton.QUITTER, !actif);
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostFieldPutData("WREP13", 0, "HOM");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.RETOURNER_LIGNES)) {
        lexique.HostFieldPutData("WREP13", 0, "COR");
        lexique.HostScreenSendKey(this, "ENTER");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(BOUTON_ANNULATION)) {
        lexique.HostFieldPutData("WREP13", 0, "ANN");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(BOUTON_ATTENTE)) {
        lexique.HostFieldPutData("WREP13", 0, "ATT");
        lexique.HostScreenSendKey(this, "ENTER");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // permet de gérer la couleur des lignes de la liste en fonction du type de ligne (article, commentaire , edition,
  // annulé...)
  private void gererCouleurListe() {
    // couleurLigne = new Color[listeCellules.length][1];
    String chaineTest = null;
    for (int i = 0; i < listeCellules.length; i++) {
      chaineTest = lexique.HostFieldGetData(listeCellules[i]).trim();
      if ((chaineTest != null) && !chaineTest.equals("") && (chaineTest.length() > 7)) {
        if (chaineTest.substring(4, 7).trim().equals("")) {
          couleurLigne[i][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
        }
        else {
          couleurLigne[i][0] = Color.BLACK;
        }
      }
      else {
        couleurLigne[i][0] = Color.BLACK;
      }
      ;
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    sNPanelContenu1 = new SNPanelContenu();
    pnlInformations = new SNPanel();
    pnlGauche = new SNPanel();
    lbNumero = new SNLabelChamp();
    WNUM = new XRiTextField();
    lbDateTraitement = new SNLabelChamp();
    WDTRTX = new XRiCalendrier();
    lbMagasinEmetteur = new SNLabelChamp();
    snMagasinEmetteur = new SNMagasin();
    lbTiers1 = new SNLabelChamp();
    snClient1 = new SNClient();
    pnlDroite = new SNPanel();
    lbE1ETB = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbMagasinRecepteur = new SNLabelChamp();
    snMagasinRecepteur = new SNMagasin();
    lbTiers2 = new SNLabelChamp();
    snClient2 = new SNClient();
    pnlBordereau = new SNPanelTitre();
    SCROLLPANE_OBJ_20 = new JScrollPane();
    LD01 = new XRiTable();
    sNPanel2 = new SNPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    pnlOptionEdition = new SNPanelTitre();
    lbEdition = new SNLabelChamp();
    WREP4 = new XRiComboBox();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Pied de bordereau de stock : @TITB@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== sNPanelContenu1 ========
    {
      sNPanelContenu1.setName("sNPanelContenu1");
      sNPanelContenu1.setLayout(new GridBagLayout());
      ((GridBagLayout)sNPanelContenu1.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)sNPanelContenu1.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
      ((GridBagLayout)sNPanelContenu1.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)sNPanelContenu1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

      //======== pnlInformations ========
      {
        pnlInformations.setName("pnlInformations");
        pnlInformations.setLayout(new GridLayout(1, 2, 5, 0));

        //======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbNumero ----
          lbNumero.setText("Num\u00e9ro");
          lbNumero.setMaximumSize(new Dimension(200, 30));
          lbNumero.setMinimumSize(new Dimension(200, 30));
          lbNumero.setPreferredSize(new Dimension(200, 30));
          lbNumero.setInheritsPopupMenu(false);
          lbNumero.setName("lbNumero");
          pnlGauche.add(lbNumero, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- WNUM ----
          WNUM.setComponentPopupMenu(null);
          WNUM.setMinimumSize(new Dimension(70, 30));
          WNUM.setMaximumSize(new Dimension(70, 30));
          WNUM.setPreferredSize(new Dimension(70, 30));
          WNUM.setFont(new Font("sansserif", Font.PLAIN, 14));
          WNUM.setName("WNUM");
          pnlGauche.add(WNUM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbDateTraitement ----
          lbDateTraitement.setText("Date de traitement");
          lbDateTraitement.setMaximumSize(new Dimension(200, 30));
          lbDateTraitement.setMinimumSize(new Dimension(200, 30));
          lbDateTraitement.setPreferredSize(new Dimension(200, 30));
          lbDateTraitement.setName("lbDateTraitement");
          pnlGauche.add(lbDateTraitement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- WDTRTX ----
          WDTRTX.setComponentPopupMenu(null);
          WDTRTX.setOpaque(false);
          WDTRTX.setMaximumSize(new Dimension(110, 30));
          WDTRTX.setMinimumSize(new Dimension(110, 30));
          WDTRTX.setPreferredSize(new Dimension(110, 30));
          WDTRTX.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDTRTX.setName("WDTRTX");
          pnlGauche.add(WDTRTX, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbMagasinEmetteur ----
          lbMagasinEmetteur.setText("Magasin \u00e9metteur");
          lbMagasinEmetteur.setPreferredSize(new Dimension(200, 30));
          lbMagasinEmetteur.setMaximumSize(new Dimension(200, 30));
          lbMagasinEmetteur.setMinimumSize(new Dimension(200, 30));
          lbMagasinEmetteur.setName("lbMagasinEmetteur");
          pnlGauche.add(lbMagasinEmetteur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snMagasinEmetteur ----
          snMagasinEmetteur.setPreferredSize(new Dimension(400, 30));
          snMagasinEmetteur.setMinimumSize(new Dimension(400, 30));
          snMagasinEmetteur.setMaximumSize(new Dimension(350, 30));
          snMagasinEmetteur.setName("snMagasinEmetteur");
          pnlGauche.add(snMagasinEmetteur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbTiers1 ----
          lbTiers1.setText("Tiers");
          lbTiers1.setName("lbTiers1");
          pnlGauche.add(lbTiers1, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snClient1 ----
          snClient1.setFont(new Font("sansserif", Font.PLAIN, 14));
          snClient1.setEnabled(false);
          snClient1.setPreferredSize(new Dimension(320, 30));
          snClient1.setMinimumSize(new Dimension(300, 30));
          snClient1.setName("snClient1");
          pnlGauche.add(snClient1, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformations.add(pnlGauche);

        //======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbE1ETB ----
          lbE1ETB.setText("Etablissement");
          lbE1ETB.setMaximumSize(new Dimension(200, 30));
          lbE1ETB.setMinimumSize(new Dimension(200, 30));
          lbE1ETB.setPreferredSize(new Dimension(200, 30));
          lbE1ETB.setInheritsPopupMenu(false);
          lbE1ETB.setName("lbE1ETB");
          pnlDroite.add(lbE1ETB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          pnlDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbMagasinRecepteur ----
          lbMagasinRecepteur.setText(" Magasin r\u00e9cepteur");
          lbMagasinRecepteur.setMaximumSize(new Dimension(200, 30));
          lbMagasinRecepteur.setMinimumSize(new Dimension(200, 30));
          lbMagasinRecepteur.setPreferredSize(new Dimension(200, 30));
          lbMagasinRecepteur.setName("lbMagasinRecepteur");
          pnlDroite.add(lbMagasinRecepteur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snMagasinRecepteur ----
          snMagasinRecepteur.setMaximumSize(new Dimension(500, 30));
          snMagasinRecepteur.setMinimumSize(new Dimension(400, 30));
          snMagasinRecepteur.setPreferredSize(new Dimension(400, 30));
          snMagasinRecepteur.setName("snMagasinRecepteur");
          pnlDroite.add(snMagasinRecepteur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbTiers2 ----
          lbTiers2.setText("Tiers");
          lbTiers2.setName("lbTiers2");
          pnlDroite.add(lbTiers2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snClient2 ----
          snClient2.setFont(new Font("sansserif", Font.PLAIN, 14));
          snClient2.setEnabled(false);
          snClient2.setMinimumSize(new Dimension(300, 30));
          snClient2.setPreferredSize(new Dimension(320, 30));
          snClient2.setName("snClient2");
          pnlDroite.add(snClient2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformations.add(pnlDroite);
      }
      sNPanelContenu1.add(pnlInformations, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlBordereau ========
      {
        pnlBordereau.setTitre("R\u00e9capitulatif du bordereau");
        pnlBordereau.setName("pnlBordereau");
        pnlBordereau.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlBordereau.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlBordereau.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlBordereau.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlBordereau.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== SCROLLPANE_OBJ_20 ========
        {
          SCROLLPANE_OBJ_20.setPreferredSize(new Dimension(350, 275));
          SCROLLPANE_OBJ_20.setMinimumSize(new Dimension(350, 275));
          SCROLLPANE_OBJ_20.setMaximumSize(new Dimension(350, 275));
          SCROLLPANE_OBJ_20.setName("SCROLLPANE_OBJ_20");

          //---- LD01 ----
          LD01.setMinimumSize(new Dimension(400, 240));
          LD01.setPreferredSize(new Dimension(400, 240));
          LD01.setEnabled(false);
          LD01.setName("LD01");
          SCROLLPANE_OBJ_20.setViewportView(LD01);
        }
        pnlBordereau.add(SCROLLPANE_OBJ_20, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== sNPanel2 ========
        {
          sNPanel2.setName("sNPanel2");
          sNPanel2.setLayout(new GridBagLayout());
          ((GridBagLayout)sNPanel2.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)sNPanel2.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)sNPanel2.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)sNPanel2.getLayout()).rowWeights = new double[] {1.0, 1.0, 1.0E-4};

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setMaximumSize(new Dimension(25, 125));
          BT_PGUP.setMinimumSize(new Dimension(25, 125));
          BT_PGUP.setPreferredSize(new Dimension(25, 125));
          BT_PGUP.setName("BT_PGUP");
          sNPanel2.add(BT_PGUP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setMaximumSize(new Dimension(25, 125));
          BT_PGDOWN.setMinimumSize(new Dimension(25, 125));
          BT_PGDOWN.setPreferredSize(new Dimension(25, 125));
          BT_PGDOWN.setName("BT_PGDOWN");
          sNPanel2.add(BT_PGDOWN, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlBordereau.add(sNPanel2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(pnlBordereau, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlOptionEdition ========
      {
        pnlOptionEdition.setTitre("Edition");
        pnlOptionEdition.setName("pnlOptionEdition");
        pnlOptionEdition.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlOptionEdition.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlOptionEdition.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlOptionEdition.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlOptionEdition.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- lbEdition ----
        lbEdition.setText("Option d'\u00e9dition");
        lbEdition.setName("lbEdition");
        pnlOptionEdition.add(lbEdition, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- WREP4 ----
        WREP4.setModel(new DefaultComboBoxModel(new String[] {
          "Pas d'\u00e9dition",
          "Edition non chiffr\u00e9",
          "Edition chiffr\u00e9e",
          "Edition non chiffr\u00e9e sur papier facture",
          "Edition chiffr\u00e9e sur papier facture"
        }));
        WREP4.setComponentPopupMenu(null);
        WREP4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        WREP4.setFont(new Font("sansserif", Font.PLAIN, 14));
        WREP4.setName("WREP4");
        pnlOptionEdition.add(WREP4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(pnlOptionEdition, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelContenu1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelContenu1;
  private SNPanel pnlInformations;
  private SNPanel pnlGauche;
  private SNLabelChamp lbNumero;
  private XRiTextField WNUM;
  private SNLabelChamp lbDateTraitement;
  private XRiCalendrier WDTRTX;
  private SNLabelChamp lbMagasinEmetteur;
  private SNMagasin snMagasinEmetteur;
  private SNLabelChamp lbTiers1;
  private SNClient snClient1;
  private SNPanel pnlDroite;
  private SNLabelChamp lbE1ETB;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbMagasinRecepteur;
  private SNMagasin snMagasinRecepteur;
  private SNLabelChamp lbTiers2;
  private SNClient snClient2;
  private SNPanelTitre pnlBordereau;
  private JScrollPane SCROLLPANE_OBJ_20;
  private XRiTable LD01;
  private SNPanel sNPanel2;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private SNPanelTitre pnlOptionEdition;
  private SNLabelChamp lbEdition;
  private XRiComboBox WREP4;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
