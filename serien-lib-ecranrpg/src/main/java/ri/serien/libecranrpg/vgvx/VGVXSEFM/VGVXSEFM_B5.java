
package ri.serien.libecranrpg.vgvx.VGVXSEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVXSEFM_B5 extends SNPanelEcranRPG implements ioFrame {
   
  private String[] A_Value = { "", "0", "1", "2", "3", };
  private String[] D_Value = { "", "0", "1", "2", };
  
  public VGVXSEFM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SET118.setValeurs(A_Value, null);
    SET116.setValeurs(A_Value, null);
    SET114.setValeurs(A_Value, null);
    SET112.setValeurs(A_Value, null);
    SET111.setValeurs(A_Value, null);
    SET103.setValeurs(A_Value, null);
    SET101.setValeurs(A_Value, null);
    SET150.setValeurs(D_Value, null);
    SET120.setValeursSelection("0", "1");
    SET117.setValeursSelection("0", "1");
    SET119.setValeursSelection("0", "1");
    SET115.setValeursSelection("0", "1");
    SET113.setValeursSelection("0", "1");
    SET157.setValeursSelection("0", "1");
    SET102.setValeursSelection("0", "1");
    SET149.setValeursSelection("0", "1");
    SET106.setValeursSelection("0", "1");
    SET105.setValeursSelection("0", "1");
    SET104.setValeursSelection("0", "1");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Sécurité de la gestion des ventes et des achats (@LOCUSR@)")).trim());
    INDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_141.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAGE@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // V06F.setVisible(!lexique.HostFieldGetData("V01F").trim().equalsIgnoreCase("MODIFIC."));
    V06F1.setVisible(lexique.isTrue("N53"));
    V06F.setVisible(!V06F1.isVisible());
    
    riSousMenu6.setEnabled(lexique.isTrue("51"));
    
    // Gestion des tops mals initialisés : si on a une case à cocher, on lit la zone correspondante. Si blanc décoche
    // comme avec la valeur 1.
    // SEUL LE 0 COCHE (autorisé).
    for (int i = 0; i < getAllComponents(this).size(); i++) {
      if (getAllComponents(this).get(i) instanceof XRiCheckBox) {
        if (lexique.HostFieldGetData(getAllComponents(this).get(i).getName()).trim().equals("")) {
          ((XRiCheckBox) getAllComponents(this).get(i)).setSelected(false);
        }
      }
    }
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (V06F1.isVisible()) {
      V06F1.setText("F");
    }
    else {
      lexique.HostFieldPutData("V06F1", 0, "F");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  public static List<Component> getAllComponents(final Container c) {
    Component[] comps = c.getComponents();
    List<Component> compList = new ArrayList<Component>();
    for (Component comp : comps) {
      compList.add(comp);
      if (comp instanceof Container) {
        compList.addAll(getAllComponents((Container) comp));
      }
    }
    return compList;
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    INDUSR = new RiZoneSortie();
    OBJ_36 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_40 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_141 = new JLabel();
    OBJ_135 = new JLabel();
    pnlSud = new SNPanelFond();
    pnlMenus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    scroll_droite3 = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    pnlBas = new SNPanel();
    V06F = new XRiTextField();
    V06F1 = new XRiTextField();
    OBJ_97 = new SNBoutonLeger();
    xTitledPanel1 = new JXTitledPanel();
    pnlGauche = new SNPanel();
    OBJ_125 = new JLabel();
    OBJ_126 = new JLabel();
    OBJ_127 = new JLabel();
    OBJ_186 = new JLabel();
    OBJ_189 = new JLabel();
    OBJ_193 = new JLabel();
    OBJ_294 = new JLabel();
    OBJ_195 = new JLabel();
    OBJ_128 = new JLabel();
    SET104 = new XRiCheckBox();
    SET105 = new XRiCheckBox();
    SET106 = new XRiCheckBox();
    SET149 = new XRiCheckBox();
    SET102 = new XRiCheckBox();
    SET157 = new XRiCheckBox();
    SET150 = new XRiComboBox();
    OBJ_200 = new JLabel();
    SET101 = new XRiComboBox();
    label15 = new JLabel();
    SET103 = new XRiComboBox();
    label16 = new JLabel();
    pnlDroite = new SNPanel();
    OBJ_201 = new JLabel();
    OBJ_202 = new JLabel();
    OBJ_203 = new JLabel();
    OBJ_204 = new JLabel();
    OBJ_205 = new JLabel();
    OBJ_206 = new JLabel();
    OBJ_207 = new JLabel();
    OBJ_208 = new JLabel();
    OBJ_209 = new JLabel();
    OBJ_210 = new JLabel();
    SET113 = new XRiCheckBox();
    SET115 = new XRiCheckBox();
    SET119 = new XRiCheckBox();
    SET117 = new XRiCheckBox();
    SET111 = new XRiComboBox();
    label17 = new JLabel();
    SET112 = new XRiComboBox();
    label18 = new JLabel();
    SET114 = new XRiComboBox();
    label19 = new JLabel();
    SET116 = new XRiComboBox();
    label20 = new JLabel();
    SET118 = new XRiComboBox();
    label21 = new JLabel();
    SET120 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 de la gestion des ventes et des achats (@LOCUSR@)");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_34 ----
          OBJ_34.setText("Utilisateur");
          OBJ_34.setName("OBJ_34");

          //---- INDUSR ----
          INDUSR.setComponentPopupMenu(BTD);
          INDUSR.setOpaque(false);
          INDUSR.setText("@INDUSR@");
          INDUSR.setName("INDUSR");

          //---- OBJ_36 ----
          OBJ_36.setText("Etablissement");
          OBJ_36.setName("OBJ_36");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setAlignmentX(4.0F);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_40 ----
          OBJ_40.setText("@DGNOM@");
          OBJ_40.setOpaque(false);
          OBJ_40.setName("OBJ_40");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(75, 75, 75)
                    .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
                .addGap(65, 65, 65)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 339, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDUSR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_141 ----
          OBJ_141.setText("@WPAGE@");
          OBJ_141.setName("OBJ_141");
          p_tete_droite.add(OBJ_141);

          //---- OBJ_135 ----
          OBJ_135.setText("Page");
          OBJ_135.setName("OBJ_135");
          p_tete_droite.add(OBJ_135);
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());

      //======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createGrayLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== scroll_droite3 ========
          {
            scroll_droite3.setBackground(new Color(238, 239, 241));
            scroll_droite3.setPreferredSize(new Dimension(16, 520));
            scroll_droite3.setBorder(null);
            scroll_droite3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
            scroll_droite3.setName("scroll_droite3");

            //======== menus_haut ========
            {
              menus_haut.setMinimumSize(new Dimension(160, 520));
              menus_haut.setPreferredSize(new Dimension(160, 520));
              menus_haut.setBackground(new Color(238, 239, 241));
              menus_haut.setAutoscrolls(true);
              menus_haut.setName("menus_haut");
              menus_haut.setLayout(new VerticalLayout());

              //======== riMenu_V01F ========
              {
                riMenu_V01F.setMinimumSize(new Dimension(104, 50));
                riMenu_V01F.setPreferredSize(new Dimension(170, 50));
                riMenu_V01F.setMaximumSize(new Dimension(104, 50));
                riMenu_V01F.setName("riMenu_V01F");

                //---- riMenu_bt_V01F ----
                riMenu_bt_V01F.setText("@V01F@");
                riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
                riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
                riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
                riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
                riMenu_bt_V01F.setName("riMenu_bt_V01F");
                riMenu_V01F.add(riMenu_bt_V01F);
              }
              menus_haut.add(riMenu_V01F);

              //======== riSousMenu_consult ========
              {
                riSousMenu_consult.setName("riSousMenu_consult");

                //---- riSousMenu_bt_consult ----
                riSousMenu_bt_consult.setText("Consultation");
                riSousMenu_bt_consult.setToolTipText("Consultation");
                riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
                riSousMenu_consult.add(riSousMenu_bt_consult);
              }
              menus_haut.add(riSousMenu_consult);

              //======== riSousMenu_modif ========
              {
                riSousMenu_modif.setName("riSousMenu_modif");

                //---- riSousMenu_bt_modif ----
                riSousMenu_bt_modif.setText("Modification");
                riSousMenu_bt_modif.setToolTipText("Modification");
                riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
                riSousMenu_modif.add(riSousMenu_bt_modif);
              }
              menus_haut.add(riSousMenu_modif);

              //======== riSousMenu_crea ========
              {
                riSousMenu_crea.setName("riSousMenu_crea");

                //---- riSousMenu_bt_crea ----
                riSousMenu_bt_crea.setText("Cr\u00e9ation");
                riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
                riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
                riSousMenu_crea.add(riSousMenu_bt_crea);
              }
              menus_haut.add(riSousMenu_crea);

              //======== riSousMenu_suppr ========
              {
                riSousMenu_suppr.setName("riSousMenu_suppr");

                //---- riSousMenu_bt_suppr ----
                riSousMenu_bt_suppr.setText("Annulation");
                riSousMenu_bt_suppr.setToolTipText("Annulation");
                riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
                riSousMenu_suppr.add(riSousMenu_bt_suppr);
              }
              menus_haut.add(riSousMenu_suppr);

              //======== riSousMenuF_dupli ========
              {
                riSousMenuF_dupli.setName("riSousMenuF_dupli");

                //---- riSousMenu_bt_dupli ----
                riSousMenu_bt_dupli.setText("Duplication");
                riSousMenu_bt_dupli.setToolTipText("Duplication");
                riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
                riSousMenuF_dupli.add(riSousMenu_bt_dupli);
              }
              menus_haut.add(riSousMenuF_dupli);

              //======== riSousMenu_rappel ========
              {
                riSousMenu_rappel.setName("riSousMenu_rappel");

                //---- riSousMenu_bt_rappel ----
                riSousMenu_bt_rappel.setText("Rappel");
                riSousMenu_bt_rappel.setToolTipText("Rappel");
                riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
                riSousMenu_rappel.add(riSousMenu_bt_rappel);
              }
              menus_haut.add(riSousMenu_rappel);

              //======== riSousMenu_reac ========
              {
                riSousMenu_reac.setName("riSousMenu_reac");

                //---- riSousMenu_bt_reac ----
                riSousMenu_bt_reac.setText("R\u00e9activation");
                riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
                riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
                riSousMenu_reac.add(riSousMenu_bt_reac);
              }
              menus_haut.add(riSousMenu_reac);

              //======== riSousMenu_destr ========
              {
                riSousMenu_destr.setName("riSousMenu_destr");

                //---- riSousMenu_bt_destr ----
                riSousMenu_bt_destr.setText("Suppression");
                riSousMenu_bt_destr.setToolTipText("Suppression");
                riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
                riSousMenu_destr.add(riSousMenu_bt_destr);
              }
              menus_haut.add(riSousMenu_destr);

              //======== riMenu2 ========
              {
                riMenu2.setName("riMenu2");

                //---- riMenu_bt2 ----
                riMenu_bt2.setText("Options");
                riMenu_bt2.setName("riMenu_bt2");
                riMenu2.add(riMenu_bt2);
              }
              menus_haut.add(riMenu2);

              //======== riSousMenu6 ========
              {
                riSousMenu6.setName("riSousMenu6");

                //---- riSousMenu_bt6 ----
                riSousMenu_bt6.setText("Tous droits sur tout");
                riSousMenu_bt6.setToolTipText("Tous droits sur tout");
                riSousMenu_bt6.setName("riSousMenu_bt6");
                riSousMenu_bt6.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riSousMenu_bt6ActionPerformed(e);
                  }
                });
                riSousMenu6.add(riSousMenu_bt6);
              }
              menus_haut.add(riSousMenu6);

              //======== riSousMenu7 ========
              {
                riSousMenu7.setName("riSousMenu7");

                //---- riSousMenu_bt7 ----
                riSousMenu_bt7.setText("Valider les modifications");
                riSousMenu_bt7.setToolTipText("Valider les modifications");
                riSousMenu_bt7.setName("riSousMenu_bt7");
                riSousMenu_bt7.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riSousMenu_bt7ActionPerformed(e);
                  }
                });
                riSousMenu7.add(riSousMenu_bt7);
              }
              menus_haut.add(riSousMenu7);
            }
            scroll_droite3.setViewportView(menus_haut);
          }
          scroll_droite.setViewportView(scroll_droite3);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);

      //======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(1000, 600));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);

        //======== pnlBas ========
        {
          pnlBas.setOpaque(false);
          pnlBas.setName("pnlBas");
          pnlBas.setLayout(null);

          //---- V06F ----
          V06F.setComponentPopupMenu(BTD);
          V06F.setName("V06F");
          pnlBas.add(V06F);
          V06F.setBounds(220, 0, 25, V06F.getPreferredSize().height);

          //---- V06F1 ----
          V06F1.setComponentPopupMenu(BTD);
          V06F1.setName("V06F1");
          pnlBas.add(V06F1);
          V06F1.setBounds(220, 0, 25, V06F1.getPreferredSize().height);

          //---- OBJ_97 ----
          OBJ_97.setText("Aller \u00e0 la page");
          OBJ_97.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_97.setName("OBJ_97");
          OBJ_97.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_69ActionPerformed(e);
            }
          });
          pnlBas.add(OBJ_97);
          OBJ_97.setBounds(new Rectangle(new Point(75, 0), OBJ_97.getPreferredSize()));

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < pnlBas.getComponentCount(); i++) {
              Rectangle bounds = pnlBas.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlBas.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlBas.setMinimumSize(preferredSize);
            pnlBas.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(pnlBas);
        pnlBas.setBounds(732, 560, 267, 34);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
          xTitledPanel1.setTitle("Objet de la s\u00e9curit\u00e9 (GPM et PRM)");
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //======== pnlGauche ========
          {
            pnlGauche.setBorder(new TitledBorder("Gestion de la production"));
            pnlGauche.setOpaque(false);
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(null);

            //---- OBJ_125 ----
            OBJ_125.setText("102");
            OBJ_125.setFont(OBJ_125.getFont().deriveFont(OBJ_125.getFont().getStyle() | Font.BOLD));
            OBJ_125.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_125.setName("OBJ_125");
            pnlGauche.add(OBJ_125);
            OBJ_125.setBounds(10, 90, 21, 16);

            //---- OBJ_126 ----
            OBJ_126.setText("103");
            OBJ_126.setFont(OBJ_126.getFont().deriveFont(OBJ_126.getFont().getStyle() | Font.BOLD));
            OBJ_126.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_126.setName("OBJ_126");
            pnlGauche.add(OBJ_126);
            OBJ_126.setBounds(10, 130, 21, 16);

            //---- OBJ_127 ----
            OBJ_127.setText("104");
            OBJ_127.setFont(OBJ_127.getFont().deriveFont(OBJ_127.getFont().getStyle() | Font.BOLD));
            OBJ_127.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_127.setName("OBJ_127");
            pnlGauche.add(OBJ_127);
            OBJ_127.setBounds(10, 170, 21, 16);

            //---- OBJ_186 ----
            OBJ_186.setText("105");
            OBJ_186.setFont(OBJ_186.getFont().deriveFont(OBJ_186.getFont().getStyle() | Font.BOLD));
            OBJ_186.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_186.setName("OBJ_186");
            pnlGauche.add(OBJ_186);
            OBJ_186.setBounds(10, 210, 21, 16);

            //---- OBJ_189 ----
            OBJ_189.setText("106");
            OBJ_189.setFont(OBJ_189.getFont().deriveFont(OBJ_189.getFont().getStyle() | Font.BOLD));
            OBJ_189.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_189.setName("OBJ_189");
            pnlGauche.add(OBJ_189);
            OBJ_189.setBounds(10, 250, 21, 16);

            //---- OBJ_193 ----
            OBJ_193.setText("149");
            OBJ_193.setFont(OBJ_193.getFont().deriveFont(OBJ_193.getFont().getStyle() | Font.BOLD));
            OBJ_193.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_193.setName("OBJ_193");
            pnlGauche.add(OBJ_193);
            OBJ_193.setBounds(10, 290, 21, 16);

            //---- OBJ_294 ----
            OBJ_294.setText("150");
            OBJ_294.setFont(OBJ_294.getFont().deriveFont(OBJ_294.getFont().getStyle() | Font.BOLD));
            OBJ_294.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_294.setName("OBJ_294");
            pnlGauche.add(OBJ_294);
            OBJ_294.setBounds(10, 330, 21, 16);

            //---- OBJ_195 ----
            OBJ_195.setText("157");
            OBJ_195.setFont(OBJ_195.getFont().deriveFont(OBJ_195.getFont().getStyle() | Font.BOLD));
            OBJ_195.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_195.setName("OBJ_195");
            pnlGauche.add(OBJ_195);
            OBJ_195.setBounds(10, 370, 21, 16);

            //---- OBJ_128 ----
            OBJ_128.setText("101");
            OBJ_128.setFont(OBJ_128.getFont().deriveFont(OBJ_128.getFont().getStyle() | Font.BOLD));
            OBJ_128.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_128.setName("OBJ_128");
            pnlGauche.add(OBJ_128);
            OBJ_128.setBounds(10, 50, 21, 16);

            //---- SET104 ----
            SET104.setToolTipText("Valeur entre 0 et 9");
            SET104.setComponentPopupMenu(BTD);
            SET104.setText("Edition des O.F");
            SET104.setName("SET104");
            pnlGauche.add(SET104);
            SET104.setBounds(40, 170, 405, SET104.getPreferredSize().height);

            //---- SET105 ----
            SET105.setToolTipText("Valeur entre 0 et 9");
            SET105.setComponentPopupMenu(BTD);
            SET105.setText("Purge des O.F");
            SET105.setName("SET105");
            pnlGauche.add(SET105);
            SET105.setBounds(40, 210, 405, SET105.getPreferredSize().height);

            //---- SET106 ----
            SET106.setToolTipText("Valeur entre 0 et 9");
            SET106.setComponentPopupMenu(BTD);
            SET106.setText("Mise \u00e0 jour de prix");
            SET106.setName("SET106");
            pnlGauche.add(SET106);
            SET106.setBounds(40, 250, 405, SET106.getPreferredSize().height);

            //---- SET149 ----
            SET149.setToolTipText("Valeur entre 0 et 9");
            SET149.setComponentPopupMenu(BTD);
            SET149.setText("Affectation visualisation planning");
            SET149.setName("SET149");
            pnlGauche.add(SET149);
            SET149.setBounds(40, 290, 405, SET149.getPreferredSize().height);

            //---- SET102 ----
            SET102.setToolTipText("Valeur entre 0 et 9");
            SET102.setComponentPopupMenu(BTD);
            SET102.setText("Liste des nomenclatures");
            SET102.setName("SET102");
            pnlGauche.add(SET102);
            SET102.setBounds(50, 90, 405, SET102.getPreferredSize().height);

            //---- SET157 ----
            SET157.setToolTipText("Valeur entre 0 et 9");
            SET157.setComponentPopupMenu(BTD);
            SET157.setText("Modification commissionnement");
            SET157.setName("SET157");
            pnlGauche.add(SET157);
            SET157.setBounds(40, 365, 405, SET157.getPreferredSize().height);

            //---- SET150 ----
            SET150.setComponentPopupMenu(BTD);
            SET150.setModel(new DefaultComboBoxModel(new String[] {
              "  ",
              "Tous les droits ",
              "Droits sur les options : ATT, RES, DER, LFA, ANN, ADS, AFA ",
              "Droits dur les options : ATT, RES, DER, ANN, ADS, AFA."
            }));
            SET150.setName("SET150");
            pnlGauche.add(SET150);
            SET150.setBounds(40, 325, 210, SET150.getPreferredSize().height);

            //---- OBJ_200 ----
            OBJ_200.setText("Options possibles sur O.F");
            OBJ_200.setName("OBJ_200");
            pnlGauche.add(OBJ_200);
            OBJ_200.setBounds(255, 325, 205, 26);

            //---- SET101 ----
            SET101.setComponentPopupMenu(BTD);
            SET101.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit"
            }));
            SET101.setName("SET101");
            pnlGauche.add(SET101);
            SET101.setBounds(45, 45, 180, SET101.getPreferredSize().height);

            //---- label15 ----
            label15.setText("Gestion des nomenclatures");
            label15.setName("label15");
            pnlGauche.add(label15);
            label15.setBounds(230, 45, 230, 26);

            //---- SET103 ----
            SET103.setComponentPopupMenu(BTD);
            SET103.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit"
            }));
            SET103.setName("SET103");
            pnlGauche.add(SET103);
            SET103.setBounds(45, 120, 180, 28);

            //---- label16 ----
            label16.setText("Gestion des O.F");
            label16.setName("label16");
            pnlGauche.add(label16);
            label16.setBounds(230, 121, 230, 26);
          }
          xTitledPanel1ContentContainer.add(pnlGauche);
          pnlGauche.setBounds(10, 55, 470, 450);

          //======== pnlDroite ========
          {
            pnlDroite.setBorder(new TitledBorder("Gestion des prospects"));
            pnlDroite.setOpaque(false);
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(null);

            //---- OBJ_201 ----
            OBJ_201.setText("111");
            OBJ_201.setFont(OBJ_201.getFont().deriveFont(OBJ_201.getFont().getStyle() | Font.BOLD));
            OBJ_201.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_201.setName("OBJ_201");
            pnlDroite.add(OBJ_201);
            OBJ_201.setBounds(10, 50, 21, 16);

            //---- OBJ_202 ----
            OBJ_202.setText("112");
            OBJ_202.setFont(OBJ_202.getFont().deriveFont(OBJ_202.getFont().getStyle() | Font.BOLD));
            OBJ_202.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_202.setName("OBJ_202");
            pnlDroite.add(OBJ_202);
            OBJ_202.setBounds(10, 90, 21, 16);

            //---- OBJ_203 ----
            OBJ_203.setText("113");
            OBJ_203.setFont(OBJ_203.getFont().deriveFont(OBJ_203.getFont().getStyle() | Font.BOLD));
            OBJ_203.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_203.setName("OBJ_203");
            pnlDroite.add(OBJ_203);
            OBJ_203.setBounds(10, 130, 21, 16);

            //---- OBJ_204 ----
            OBJ_204.setText("114");
            OBJ_204.setFont(OBJ_204.getFont().deriveFont(OBJ_204.getFont().getStyle() | Font.BOLD));
            OBJ_204.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_204.setName("OBJ_204");
            pnlDroite.add(OBJ_204);
            OBJ_204.setBounds(10, 170, 21, 16);

            //---- OBJ_205 ----
            OBJ_205.setText("115");
            OBJ_205.setFont(OBJ_205.getFont().deriveFont(OBJ_205.getFont().getStyle() | Font.BOLD));
            OBJ_205.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_205.setName("OBJ_205");
            pnlDroite.add(OBJ_205);
            OBJ_205.setBounds(10, 210, 21, 16);

            //---- OBJ_206 ----
            OBJ_206.setText("116");
            OBJ_206.setFont(OBJ_206.getFont().deriveFont(OBJ_206.getFont().getStyle() | Font.BOLD));
            OBJ_206.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_206.setName("OBJ_206");
            pnlDroite.add(OBJ_206);
            OBJ_206.setBounds(10, 250, 21, 16);

            //---- OBJ_207 ----
            OBJ_207.setText("117");
            OBJ_207.setFont(OBJ_207.getFont().deriveFont(OBJ_207.getFont().getStyle() | Font.BOLD));
            OBJ_207.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_207.setName("OBJ_207");
            pnlDroite.add(OBJ_207);
            OBJ_207.setBounds(10, 290, 21, 16);

            //---- OBJ_208 ----
            OBJ_208.setText("118");
            OBJ_208.setFont(OBJ_208.getFont().deriveFont(OBJ_208.getFont().getStyle() | Font.BOLD));
            OBJ_208.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_208.setName("OBJ_208");
            pnlDroite.add(OBJ_208);
            OBJ_208.setBounds(10, 330, 21, 16);

            //---- OBJ_209 ----
            OBJ_209.setText("119");
            OBJ_209.setFont(OBJ_209.getFont().deriveFont(OBJ_209.getFont().getStyle() | Font.BOLD));
            OBJ_209.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_209.setName("OBJ_209");
            pnlDroite.add(OBJ_209);
            OBJ_209.setBounds(10, 370, 21, 16);

            //---- OBJ_210 ----
            OBJ_210.setText("120");
            OBJ_210.setFont(OBJ_210.getFont().deriveFont(OBJ_210.getFont().getStyle() | Font.BOLD));
            OBJ_210.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_210.setName("OBJ_210");
            pnlDroite.add(OBJ_210);
            OBJ_210.setBounds(10, 410, 21, 16);

            //---- SET113 ----
            SET113.setToolTipText("Valeur entre 0 et 9");
            SET113.setComponentPopupMenu(BTD);
            SET113.setText("Listes et \u00e9tiquettes prospects");
            SET113.setName("SET113");
            pnlDroite.add(SET113);
            SET113.setBounds(40, 129, 405, SET113.getPreferredSize().height);

            //---- SET115 ----
            SET115.setToolTipText("Valeur entre 0 et 9");
            SET115.setComponentPopupMenu(BTD);
            SET115.setText("Listes des affaires");
            SET115.setName("SET115");
            pnlDroite.add(SET115);
            SET115.setBounds(40, 209, 405, SET115.getPreferredSize().height);

            //---- SET119 ----
            SET119.setToolTipText("Valeur entre 0 et 9");
            SET119.setComponentPopupMenu(BTD);
            SET119.setText("Listes et \u00e9ditions des Mailings");
            SET119.setName("SET119");
            pnlDroite.add(SET119);
            SET119.setBounds(40, 369, 405, SET119.getPreferredSize().height);

            //---- SET117 ----
            SET117.setToolTipText("Valeur entre 0 et 9");
            SET117.setComponentPopupMenu(BTD);
            SET117.setText("Listes et \u00e9tiquettes des actions");
            SET117.setName("SET117");
            pnlDroite.add(SET117);
            SET117.setBounds(40, 289, 405, SET117.getPreferredSize().height);

            //---- SET111 ----
            SET111.setComponentPopupMenu(BTD);
            SET111.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit"
            }));
            SET111.setName("SET111");
            pnlDroite.add(SET111);
            SET111.setBounds(40, 44, 180, SET111.getPreferredSize().height);

            //---- label17 ----
            label17.setText("Personnalisation");
            label17.setName("label17");
            pnlDroite.add(label17);
            label17.setBounds(225, 44, 230, 26);

            //---- SET112 ----
            SET112.setComponentPopupMenu(BTD);
            SET112.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit"
            }));
            SET112.setName("SET112");
            pnlDroite.add(SET112);
            SET112.setBounds(40, 84, 180, SET112.getPreferredSize().height);

            //---- label18 ----
            label18.setText("Gestion des prospects");
            label18.setName("label18");
            pnlDroite.add(label18);
            label18.setBounds(225, 84, 230, 26);

            //---- SET114 ----
            SET114.setComponentPopupMenu(BTD);
            SET114.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit"
            }));
            SET114.setName("SET114");
            pnlDroite.add(SET114);
            SET114.setBounds(40, 164, 180, SET114.getPreferredSize().height);

            //---- label19 ----
            label19.setText("Gestion des affaires");
            label19.setName("label19");
            pnlDroite.add(label19);
            label19.setBounds(225, 164, 230, 26);

            //---- SET116 ----
            SET116.setComponentPopupMenu(BTD);
            SET116.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit"
            }));
            SET116.setName("SET116");
            pnlDroite.add(SET116);
            SET116.setBounds(40, 244, 180, SET116.getPreferredSize().height);

            //---- label20 ----
            label20.setText("Gestion des actions commerciales");
            label20.setName("label20");
            pnlDroite.add(label20);
            label20.setBounds(225, 244, 230, 26);

            //---- SET118 ----
            SET118.setComponentPopupMenu(BTD);
            SET118.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Tous les droits",
              "consultation et modification",
              "consultation seulement",
              "Aucun droit"
            }));
            SET118.setName("SET118");
            pnlDroite.add(SET118);
            SET118.setBounds(40, 324, 180, SET118.getPreferredSize().height);

            //---- label21 ----
            label21.setText("Gestion mailings");
            label21.setName("label21");
            pnlDroite.add(label21);
            label21.setBounds(225, 324, 230, 26);

            //---- SET120 ----
            SET120.setToolTipText("Valeur entre 0 et 9");
            SET120.setComponentPopupMenu(BTD);
            SET120.setText("Mise \u00e0 jour des prospects \u00e0 partir des clients");
            SET120.setName("SET120");
            pnlDroite.add(SET120);
            SET120.setBounds(40, 409, 405, SET120.getPreferredSize().height);
          }
          xTitledPanel1ContentContainer.add(pnlDroite);
          pnlDroite.setBounds(490, 55, 470, 450);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(15, 10, 974, 550);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }


    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel pnlNord;
    private SNBandeauTitre p_bpresentation;
    private JMenuBar barre_tete;
    private JPanel p_tete_gauche;
    private JLabel OBJ_34;
    private RiZoneSortie INDUSR;
    private JLabel OBJ_36;
    private RiZoneSortie INDETB;
    private RiZoneSortie OBJ_40;
    private JPanel p_tete_droite;
    private JLabel OBJ_141;
    private JLabel OBJ_135;
    private SNPanelFond pnlSud;
    private JPanel pnlMenus;
    private JPanel menus_bas;
    private RiMenu navig_erreurs;
    private RiMenu_bt bouton_erreurs;
    private RiMenu navig_valid;
    private RiMenu_bt bouton_valider;
    private RiMenu navig_retour;
    private RiMenu_bt bouton_retour;
    private JScrollPane scroll_droite;
    private JScrollPane scroll_droite3;
    private JPanel menus_haut;
    private RiMenu riMenu_V01F;
    private RiMenu_bt riMenu_bt_V01F;
    private RiSousMenu riSousMenu_consult;
    private RiSousMenu_bt riSousMenu_bt_consult;
    private RiSousMenu riSousMenu_modif;
    private RiSousMenu_bt riSousMenu_bt_modif;
    private RiSousMenu riSousMenu_crea;
    private RiSousMenu_bt riSousMenu_bt_crea;
    private RiSousMenu riSousMenu_suppr;
    private RiSousMenu_bt riSousMenu_bt_suppr;
    private RiSousMenu riSousMenuF_dupli;
    private RiSousMenu_bt riSousMenu_bt_dupli;
    private RiSousMenu riSousMenu_rappel;
    private RiSousMenu_bt riSousMenu_bt_rappel;
    private RiSousMenu riSousMenu_reac;
    private RiSousMenu_bt riSousMenu_bt_reac;
    private RiSousMenu riSousMenu_destr;
    private RiSousMenu_bt riSousMenu_bt_destr;
    private RiMenu riMenu2;
    private RiMenu_bt riMenu_bt2;
    private RiSousMenu riSousMenu6;
    private RiSousMenu_bt riSousMenu_bt6;
    private RiSousMenu riSousMenu7;
    private RiSousMenu_bt riSousMenu_bt7;
    private SNPanelContenu pnlContenu;
    private SNPanel pnlBas;
    private XRiTextField V06F;
    private XRiTextField V06F1;
    private SNBoutonLeger OBJ_97;
    private JXTitledPanel xTitledPanel1;
    private SNPanel pnlGauche;
    private JLabel OBJ_125;
    private JLabel OBJ_126;
    private JLabel OBJ_127;
    private JLabel OBJ_186;
    private JLabel OBJ_189;
    private JLabel OBJ_193;
    private JLabel OBJ_294;
    private JLabel OBJ_195;
    private JLabel OBJ_128;
    private XRiCheckBox SET104;
    private XRiCheckBox SET105;
    private XRiCheckBox SET106;
    private XRiCheckBox SET149;
    private XRiCheckBox SET102;
    private XRiCheckBox SET157;
    private XRiComboBox SET150;
    private JLabel OBJ_200;
    private XRiComboBox SET101;
    private JLabel label15;
    private XRiComboBox SET103;
    private JLabel label16;
    private SNPanel pnlDroite;
    private JLabel OBJ_201;
    private JLabel OBJ_202;
    private JLabel OBJ_203;
    private JLabel OBJ_204;
    private JLabel OBJ_205;
    private JLabel OBJ_206;
    private JLabel OBJ_207;
    private JLabel OBJ_208;
    private JLabel OBJ_209;
    private JLabel OBJ_210;
    private XRiCheckBox SET113;
    private XRiCheckBox SET115;
    private XRiCheckBox SET119;
    private XRiCheckBox SET117;
    private XRiComboBox SET111;
    private JLabel label17;
    private XRiComboBox SET112;
    private JLabel label18;
    private XRiComboBox SET114;
    private JLabel label19;
    private XRiComboBox SET116;
    private JLabel label20;
    private XRiComboBox SET118;
    private JLabel label21;
    private XRiCheckBox SET120;
    private JPopupMenu BTD;
    private JMenuItem OBJ_19;
    private JMenuItem OBJ_18;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
