
package ri.serien.libecranrpg.sndr.SNDRCVFM;
// Nom Fichier: p_SNDRCVFM_FMTA1_294.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SNDRCVFM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  private boolean in96 = false;
  
  public SNDRCVFM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    Trace.activerSortieConsole(true);
    
    // Ajout
    initDiverses();
    
    // Initialisation de la barre de boutons
    snBarreBouton.supprimerTout();
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbTitre.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT1@")).trim() + " "
        + lexique.TranslationTable(interpreteurD.analyseExpression("@TIT2@")).trim());
    lbTitre.setVisible(!lbTitre.getText().isEmpty());
    lbLigne1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB01@")).trim());
    lbLigne1.setVisible(!lbLigne1.getText().isEmpty());
    lbLigne2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB02@")).trim());
    lbLigne2.setVisible(!lbLigne2.getText().isEmpty());
    lbLigne3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB03@")).trim());
    lbLigne3.setVisible(!lbLigne3.getText().isEmpty());
    lbLigne4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB04@")).trim());
    lbLigne4.setVisible(!lbLigne4.getText().isEmpty());
    lbLigne5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB05@")).trim());
    lbLigne5.setVisible(!lbLigne5.getText().isEmpty());
    lbLigne6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB06@")).trim());
    lbLigne6.setVisible(!lbLigne6.getText().isEmpty());
    lbLigne7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB07@")).trim());
    lbLigne7.setVisible(!lbLigne7.getText().isEmpty());
    lbLigne8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB08@")).trim());
    lbLigne8.setVisible(!lbLigne8.getText().isEmpty());
    lbLigne9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB09@")).trim());
    lbLigne9.setVisible(!lbLigne9.getText().isEmpty());
    lbLigne10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB10@")).trim());
    lbLigne10.setVisible(!lbLigne10.getText().isEmpty());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    in96 = lexique.isTrue("96");
    
    if ((lexique.HostFieldGetData("LIB01").trim().equalsIgnoreCase("*** SAUVEGARDE DEMANDE de TRAVAIL A PLANIFIER ***"))) {
      lbLigne1.setVisible(in96);
    }
    if ((lexique.HostFieldGetData("LIB02").trim().equalsIgnoreCase("*** SAUVEGARDE DEMANDE de TRAVAIL A PLANIFIER ***"))) {
      lbLigne2.setVisible(in96);
    }
    if ((lexique.HostFieldGetData("LIB03").trim().equalsIgnoreCase("*** SAUVEGARDE DEMANDE de TRAVAIL A PLANIFIER ***"))) {
      lbLigne3.setVisible(in96);
    }
    if ((lexique.HostFieldGetData("LIB04").trim().equalsIgnoreCase("*** SAUVEGARDE DEMANDE de TRAVAIL A PLANIFIER ***"))) {
      lbLigne4.setVisible(in96);
    }
    if ((lexique.HostFieldGetData("LIB05").trim().equalsIgnoreCase("*** SAUVEGARDE DEMANDE de TRAVAIL A PLANIFIER ***"))) {
      lbLigne5.setVisible(in96);
    }
    if ((lexique.HostFieldGetData("LIB06").trim().equalsIgnoreCase("*** SAUVEGARDE DEMANDE de TRAVAIL A PLANIFIER ***"))) {
      lbLigne6.setVisible(in96);
    }
    if ((lexique.HostFieldGetData("LIB07").trim().equalsIgnoreCase("*** SAUVEGARDE DEMANDE de TRAVAIL A PLANIFIER ***"))) {
      lbLigne7.setVisible(in96);
    }
    if ((lexique.HostFieldGetData("LIB08").trim().equalsIgnoreCase("*** SAUVEGARDE DEMANDE de TRAVAIL A PLANIFIER ***"))) {
      lbLigne8.setVisible(in96);
    }
    if ((lexique.HostFieldGetData("LIB09").trim().equalsIgnoreCase("*** SAUVEGARDE DEMANDE de TRAVAIL A PLANIFIER ***"))) {
      lbLigne9.setVisible(in96);
    }
    if ((lexique.HostFieldGetData("LIB10").trim().equalsIgnoreCase("*** SAUVEGARDE DEMANDE de TRAVAIL A PLANIFIER ***"))) {
      lbLigne10.setVisible(in96);
    }
    
    // TODO Icones
    lbLogoSerieN.setIcon(lexique.chargerImage("images/logoSerieN.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@ENVP@ - @TITPG1@ @TITPG2@"));
    
    // Supprime le bouton "Valider" en cas d'écran de notification de fin de traitement
    if (lexique.HostFieldGetData("REP").equalsIgnoreCase("FIN")) {
      snBarreBouton.supprimerBouton(snBarreBouton.getBouton(EnumBouton.VALIDER));
    }
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostFieldPutData("REP", 0, "OUI");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    lbDemandeConfirmation = new SNLabelTitre();
    lbLogoSerieN = new SNLabelChamp();
    pnlDetailsOperation = new SNPanelTitre();
    lbTitre = new SNLabelTitre();
    lbLigne1 = new SNLabelChamp();
    lbLigne2 = new SNLabelChamp();
    lbLigne3 = new SNLabelChamp();
    lbLigne4 = new SNLabelChamp();
    lbLigne5 = new SNLabelChamp();
    lbLigne6 = new SNLabelChamp();
    lbLigne7 = new SNLabelChamp();
    lbLigne8 = new SNLabelChamp();
    lbLigne9 = new SNLabelChamp();
    lbLigne10 = new SNLabelChamp();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1160, 680));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Message du programme");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
      
      // ---- lbDemandeConfirmation ----
      lbDemandeConfirmation.setText("DEMANDE DE CONFIRMATION D'OPERATION");
      lbDemandeConfirmation.setHorizontalAlignment(SwingConstants.CENTER);
      lbDemandeConfirmation.setForeground(Color.red);
      lbDemandeConfirmation.setFont(new Font("sansserif", Font.BOLD, 17));
      lbDemandeConfirmation.setName("lbDemandeConfirmation");
      pnlContenu.add(lbDemandeConfirmation,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbLogoSerieN ----
      lbLogoSerieN.setHorizontalAlignment(SwingConstants.CENTER);
      lbLogoSerieN.setName("lbLogoSerieN");
      pnlContenu.add(lbLogoSerieN,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlDetailsOperation ========
      {
        pnlDetailsOperation.setName("pnlDetailsOperation");
        pnlDetailsOperation.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDetailsOperation.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDetailsOperation.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlDetailsOperation.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDetailsOperation.getLayout()).rowWeights =
            new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbTitre ----
        lbTitre.setText("@TIT1@");
        lbTitre.setHorizontalAlignment(SwingConstants.CENTER);
        lbTitre.setVerticalAlignment(SwingConstants.CENTER);
        lbTitre.setName("lbTitre");
        pnlDetailsOperation.add(lbTitre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLigne1 ----
        lbLigne1.setText("@LIB01@");
        lbLigne1.setHorizontalAlignment(SwingConstants.CENTER);
        lbLigne1.setName("lbLigne1");
        pnlDetailsOperation.add(lbLigne1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLigne2 ----
        lbLigne2.setText("@LIB02@");
        lbLigne2.setHorizontalAlignment(SwingConstants.CENTER);
        lbLigne2.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbLigne2.setName("lbLigne2");
        pnlDetailsOperation.add(lbLigne2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLigne3 ----
        lbLigne3.setText("@LIB03@");
        lbLigne3.setHorizontalAlignment(SwingConstants.CENTER);
        lbLigne3.setName("lbLigne3");
        pnlDetailsOperation.add(lbLigne3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLigne4 ----
        lbLigne4.setText("@LIB04@");
        lbLigne4.setHorizontalAlignment(SwingConstants.CENTER);
        lbLigne4.setName("lbLigne4");
        pnlDetailsOperation.add(lbLigne4, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLigne5 ----
        lbLigne5.setText("@LIB05@");
        lbLigne5.setHorizontalAlignment(SwingConstants.CENTER);
        lbLigne5.setBackground(new Color(214, 217, 223));
        lbLigne5.setName("lbLigne5");
        pnlDetailsOperation.add(lbLigne5, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLigne6 ----
        lbLigne6.setText("@LIB06@");
        lbLigne6.setHorizontalAlignment(SwingConstants.CENTER);
        lbLigne6.setName("lbLigne6");
        pnlDetailsOperation.add(lbLigne6, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLigne7 ----
        lbLigne7.setText("@LIB07@");
        lbLigne7.setHorizontalAlignment(SwingConstants.CENTER);
        lbLigne7.setName("lbLigne7");
        pnlDetailsOperation.add(lbLigne7, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLigne8 ----
        lbLigne8.setText("@LIB08@");
        lbLigne8.setHorizontalAlignment(SwingConstants.CENTER);
        lbLigne8.setName("lbLigne8");
        pnlDetailsOperation.add(lbLigne8, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLigne9 ----
        lbLigne9.setText("@LIB09@");
        lbLigne9.setHorizontalAlignment(SwingConstants.CENTER);
        lbLigne9.setName("lbLigne9");
        pnlDetailsOperation.add(lbLigne9, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLigne10 ----
        lbLigne10.setText("@LIB10@");
        lbLigne10.setHorizontalAlignment(SwingConstants.CENTER);
        lbLigne10.setName("lbLigne10");
        pnlDetailsOperation.add(lbLigne10, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDetailsOperation, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNLabelTitre lbDemandeConfirmation;
  private SNLabelChamp lbLogoSerieN;
  private SNPanelTitre pnlDetailsOperation;
  private SNLabelTitre lbTitre;
  private SNLabelChamp lbLigne1;
  private SNLabelChamp lbLigne2;
  private SNLabelChamp lbLigne3;
  private SNLabelChamp lbLigne4;
  private SNLabelChamp lbLigne5;
  private SNLabelChamp lbLigne6;
  private SNLabelChamp lbLigne7;
  private SNLabelChamp lbLigne8;
  private SNLabelChamp lbLigne9;
  private SNLabelChamp lbLigne10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
