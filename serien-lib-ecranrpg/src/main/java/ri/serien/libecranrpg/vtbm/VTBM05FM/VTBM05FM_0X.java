
package ri.serien.libecranrpg.vtbm.VTBM05FM;
// Nom Fichier: pop_VCGM05FM_FMTERR_367.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VTBM05FM_0X extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VTBM05FM_0X(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OK);
    
    // Menu Command
    setVisible(false);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    OK.setIcon(lexique.chargerImage("images/OK.png", true));
    button1.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("ERREURS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OKActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER");
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OK = new JButton();
    button1 = new JButton();
    OBJ_5 = new JLabel();

    //======== this ========
    setBackground(new Color(204, 204, 204));
    setVisible(false);
    setName("this");
    setLayout(null);

    setPreferredSize(new Dimension(85, 45));

    //---- OK ----
    OK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OK.setToolTipText("Oui");
    OK.setName("OK");
    OK.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OKActionPerformed(e);
      }
    });

    //---- button1 ----
    button1.setToolTipText("Non");
    button1.setName("button1");
    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button1ActionPerformed(e);
      }
    });

    //---- OBJ_5 ----
    OBJ_5.setText("Veuillez confimer");
    OBJ_5.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_5.setForeground(Color.white);
    OBJ_5.setFont(OBJ_5.getFont().deriveFont(OBJ_5.getFont().getStyle() | Font.BOLD, OBJ_5.getFont().getSize() + 1f));
    OBJ_5.setName("OBJ_5");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JButton OK;
  private JButton button1;
  private JLabel OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
