
package ri.serien.libecranrpg.vtbm.VTBM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class VTBM05FM_21 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VTBM05FM_21(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WPT04.setValeursSelection("1", " ");
    WPT03.setValeursSelection("1", " ");
    WPT02.setValeursSelection("1", " ");
    WPT01.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WPT01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARRP1@")).trim());
    WPT02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARRP2@")).trim());
    WPT03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARRP3@")).trim());
    WPT04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARRP4@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // WPT04.setSelected(lexique.HostFieldGetData("WPT04").equalsIgnoreCase("1"));
    // WPT03.setSelected(lexique.HostFieldGetData("WPT03").equalsIgnoreCase("1"));
    // WPT02.setSelected(lexique.HostFieldGetData("WPT02").equalsIgnoreCase("1"));
    // WPT01.setSelected(lexique.HostFieldGetData("WPT01").equalsIgnoreCase("1"));
    WPT04.setText(lexique.HostFieldGetData("ARRP4"));
    WPT03.setText(lexique.HostFieldGetData("ARRP3"));
    WPT02.setText(lexique.HostFieldGetData("ARRP2"));
    WPT01.setText(lexique.HostFieldGetData("ARRP1"));
    
    WPT01.setVisible(!WPT01.getText().trim().equals(""));
    WPT02.setVisible(!WPT01.getText().trim().equals(""));
    WPT03.setVisible(!WPT01.getText().trim().equals(""));
    WPT04.setVisible(!WPT01.getText().trim().equals(""));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Tableau de bord"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WPT04.isSelected())
    // lexique.HostFieldPutData("WPT04", 0, "1");
    // else
    // lexique.HostFieldPutData("WPT04", 0, " ");
    // if (WPT03.isSelected())
    // lexique.HostFieldPutData("WPT03", 0, "1");
    // else
    // lexique.HostFieldPutData("WPT03", 0, " ");
    // if (WPT02.isSelected())
    // lexique.HostFieldPutData("WPT02", 0, "1");
    // else
    // lexique.HostFieldPutData("WPT02", 0, " ");
    // if (WPT01.isSelected())
    // lexique.HostFieldPutData("WPT01", 0, "1");
    // else
    // lexique.HostFieldPutData("WPT01", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    WPT01 = new XRiCheckBox();
    WPT02 = new XRiCheckBox();
    WPT03 = new XRiCheckBox();
    WPT04 = new XRiCheckBox();
    P_PnlOpts = new JPanel();
    OBJ_23 = new JButton();
    separator1 = compFactory.createSeparator("Choix de la p\u00e9riode");
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(440, 240));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- WPT01 ----
          WPT01.setText("@ARRP1@");
          WPT01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WPT01.setName("WPT01");
          p_recup.add(WPT01);
          WPT01.setBounds(35, 55, 177, 20);

          //---- WPT02 ----
          WPT02.setText("@ARRP2@");
          WPT02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WPT02.setName("WPT02");
          p_recup.add(WPT02);
          WPT02.setBounds(35, 88, 177, 20);

          //---- WPT03 ----
          WPT03.setText("@ARRP3@");
          WPT03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WPT03.setName("WPT03");
          p_recup.add(WPT03);
          WPT03.setBounds(35, 121, 177, 20);

          //---- WPT04 ----
          WPT04.setText("@ARRP4@");
          WPT04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WPT04.setName("WPT04");
          p_recup.add(WPT04);
          WPT04.setBounds(35, 154, 177, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);

            //---- OBJ_23 ----
            OBJ_23.setText("OK");
            OBJ_23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_23.setName("OBJ_23");
            OBJ_23.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_23ActionPerformed(e);
              }
            });
            P_PnlOpts.add(OBJ_23);
            OBJ_23.setBounds(5, 6, 40, 40);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- separator1 ----
          separator1.setName("separator1");
          p_recup.add(separator1);
          separator1.setBounds(10, 15, 225, separator1.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiCheckBox WPT01;
  private XRiCheckBox WPT02;
  private XRiCheckBox WPT03;
  private XRiCheckBox WPT04;
  private JPanel P_PnlOpts;
  private JButton OBJ_23;
  private JComponent separator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
