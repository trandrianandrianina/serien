
package ri.serien.libecranrpg.vtbm.VTBM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VTBM05FM_11 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WPF1_Top = { "WPF1", "WPF2", "WPF3", "WPF4", "WPF5", "WPF6", "WPF7", "WPF8", "WPF9", "WPF10", "WPF11", "WPF12", "WPF13",
      "WPF14", "WPF15", "WPF16", "WPF17", "WPF18", "WPF19", "WPF20", "WPF21", "WPF22", "WPF23", "WPF24", "WPF25", "WPF26", "WPF27", "WPF28",
      "WPF29", "WPF30", };
  private String[] _WPF1_Title = { "Code", "Libellé de la fonction", };
  private String[][] _WPF1_Data = { { "WCF1", "WLF1", }, { "WCF2", "WLF2", }, { "WCF3", "WLF3", }, { "WCF4", "WLF4", }, { "WCF5", "WLF5", },
      { "WCF6", "WLF6", }, { "WCF7", "WLF7", }, { "WCF8", "WLF8", }, { "WCF9", "WLF9", }, { "WCF10", "WLF10", }, { "WCF11", "WLF11", },
      { "WCF12", "WLF12", }, { "WCF13", "WLF13", }, { "WCF14", "WLF14", }, { "WCF15", "WLF15", }, { "WCF16", "WLF16", },
      { "WCF17", "WLF17", }, { "WCF18", "WLF18", }, { "WCF19", "WLF19", }, { "WCF20", "WLF20", }, { "WCF21", "WLF21", },
      { "WCF22", "WLF22", }, { "WCF23", "WLF23", }, { "WCF24", "WLF24", }, { "WCF25", "WLF25", }, { "WCF26", "WLF26", },
      { "WCF27", "WLF27", }, { "WCF28", "WLF28", }, { "WCF29", "WLF29", }, { "WCF30", "WLF30", }, };
  private int[] _WPF1_Width = { 66, 339, };
  
  public VTBM05FM_11(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WPF1.setAspectTable(_WPF1_Top, _WPF1_Title, _WPF1_Data, _WPF1_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WLBDOS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLBDOS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WPF1_Top);
    
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    label1.setVisible(lexique.isTrue("N29"));
    label2.setVisible(lexique.isTrue("(N70) AND (N29)"));
    
    p_bpresentation.setCodeEtablissement(WDOSL.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WDOSL.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WPF1.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WPF1MouseClicked(MouseEvent e) {
    if (WPF1.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_31_OBJ_31 = new JLabel();
    WDOSL = new XRiTextField();
    WLBDOS = new RiZoneSortie();
    OBJ_42_OBJ_42 = new JLabel();
    WPERDX = new XRiCalendrier();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST1 = new JScrollPane();
    WPF1 = new XRiTable();
    label1 = new JLabel();
    label2 = new JLabel();
    WSANL = new XRiTextField();
    WHIEL = new XRiTextField();
    WNIVL = new XRiTextField();
    WGANL = new XRiTextField();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau de bord");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_31_OBJ_31 ----
          OBJ_31_OBJ_31.setText("Soci\u00e9t\u00e9");
          OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");
          p_tete_gauche.add(OBJ_31_OBJ_31);
          OBJ_31_OBJ_31.setBounds(10, 5, 52, 18);

          //---- WDOSL ----
          WDOSL.setComponentPopupMenu(BTD);
          WDOSL.setName("WDOSL");
          p_tete_gauche.add(WDOSL);
          WDOSL.setBounds(65, 0, 40, WDOSL.getPreferredSize().height);

          //---- WLBDOS ----
          WLBDOS.setText("@WLBDOS@");
          WLBDOS.setOpaque(false);
          WLBDOS.setName("WLBDOS");
          p_tete_gauche.add(WLBDOS);
          WLBDOS.setBounds(115, 2, 345, WLBDOS.getPreferredSize().height);

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("P\u00e9riode de r\u00e9f\u00e9rence pour les tableaux de bord");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
          p_tete_gauche.add(OBJ_42_OBJ_42);
          OBJ_42_OBJ_42.setBounds(480, 4, 280, 20);

          //---- WPERDX ----
          WPERDX.setComponentPopupMenu(null);
          WPERDX.setTypeSaisie(6);
          WPERDX.setName("WPERDX");
          p_tete_gauche.add(WPERDX);
          WPERDX.setBounds(760, 0, 90, WPERDX.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 300));
            menus_haut.setPreferredSize(new Dimension(160, 300));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(780, 600));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Sommaire du tableau de bord"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST1 ========
            {
              SCROLLPANE_LIST1.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST1.setName("SCROLLPANE_LIST1");

              //---- WPF1 ----
              WPF1.setComponentPopupMenu(BTD);
              WPF1.setName("WPF1");
              WPF1.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WPF1MouseClicked(e);
                }
              });
              SCROLLPANE_LIST1.setViewportView(WPF1);
            }
            panel1.add(SCROLLPANE_LIST1);
            SCROLLPANE_LIST1.setBounds(265, 35, 423, 510);

            //---- label1 ----
            label1.setText("Section analytique");
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(25, 110, 170, 24);

            //---- label2 ----
            label2.setText("Groupe analytique");
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(25, 185, 170, 24);

            //---- WSANL ----
            WSANL.setName("WSANL");
            panel1.add(WSANL);
            WSANL.setBounds(25, 135, 64, WSANL.getPreferredSize().height);

            //---- WHIEL ----
            WHIEL.setName("WHIEL");
            panel1.add(WHIEL);
            WHIEL.setBounds(25, 209, 44, WHIEL.getPreferredSize().height);

            //---- WNIVL ----
            WNIVL.setName("WNIVL");
            panel1.add(WNIVL);
            WNIVL.setBounds(70, 209, 24, WNIVL.getPreferredSize().height);

            //---- WGANL ----
            WGANL.setName("WGANL");
            panel1.add(WGANL);
            WGANL.setBounds(95, 209, 64, WGANL.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 717, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 565, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_31_OBJ_31;
  private XRiTextField WDOSL;
  private RiZoneSortie WLBDOS;
  private JLabel OBJ_42_OBJ_42;
  private XRiCalendrier WPERDX;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST1;
  private XRiTable WPF1;
  private JLabel label1;
  private JLabel label2;
  private XRiTextField WSANL;
  private XRiTextField WHIEL;
  private XRiTextField WNIVL;
  private XRiTextField WGANL;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
