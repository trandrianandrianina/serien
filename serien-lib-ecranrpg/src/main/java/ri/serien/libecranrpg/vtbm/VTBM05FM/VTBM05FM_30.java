
package ri.serien.libecranrpg.vtbm.VTBM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class VTBM05FM_30 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = null;
  private String[] _WLB011_Title = { "Compte", "Libellé du compte", "Eléments", "   D é b i t      C r é d i t  ", };
  private String[][] _WLB011_Data = { { "WLB011", "WLB012", "WLB013", "WLB014", }, { "WLB021", "WLB022", "WLB023", "WLB024", },
      { "WLB031", "WLB032", "WLB033", "WLB034", }, { "WLB041", "WLB042", "WLB043", "WLB044", }, { "WLB051", "WLB052", "WLB053", "WLB054", },
      { "WLB061", "WLB062", "WLB063", "WLB064", }, { "WLB071", "WLB072", "WLB073", "WLB074", }, { "WLB081", "WLB082", "WLB083", "WLB084", },
      { "WLB091", "WLB092", "WLB093", "WLB094", }, { "WLB101", "WLB102", "WLB103", "WLB104", }, { "WLB111", "WLB112", "WLB113", "WLB114", },
      { "WLB121", "WLB122", "WLB123", "WLB124", }, { "WLB131", "WLB132", "WLB133", "WLB134", }, { "WLB141", "WLB142", "WLB143", "WLB144", },
      { "WLB151", "WLB152", "WLB153", "WLB154", }, { "WLB161", "WLB162", "WLB163", "WLB164", }, { "WLB171", "WLB172", "WLB173", "WLB174", },
      { "WLB181", "WLB182", "WLB183", "WLB184", }, };
  private int[] _WLB011_Width = { 54, 216, 63, 220, };
  
  public VTBM05FM_30(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WLB011.setAspectTable(_WTP01_Top, _WLB011_Title, _WLB011_Data, _WLB011_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WDOSL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDOSL@")).trim());
    OBJ_51_OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLBDOS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST3, LIST3.get_LIST_Title_Data_Brut(), null);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    panel3.setVisible(lexique.isTrue("N29"));
    panel4.setVisible(lexique.isTrue("(27) AND (29)"));
    
    if (lexique.isTrue("70")) {
      panel1.setTitle("Balance par affaire");
    }
    else if (lexique.isTrue("71")) {
      panel1.setTitle("Balance analytique");
    }
    else {
      panel1.setTitle("Balance par axes");
    }
    
    p_bpresentation.setCodeEtablissement(WDOSL.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WDOSL.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void DoubleMouseClicked(MouseEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_49_OBJ_49 = new JLabel();
    WDOSL = new RiZoneSortie();
    OBJ_51_OBJ_51 = new RiZoneSortie();
    WDATEX = new XRiCalendrier();
    label2 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JXTitledPanel();
    SCROLLPANE_LIST3 = new JScrollPane();
    WLB011 = new XRiTable();
    OBJ_42_OBJ_42 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    WLPERX = new XRiTextField();
    panel2 = new JPanel();
    panel3 = new JPanel();
    label1 = new JLabel();
    WSANL = new XRiTextField();
    panel4 = new JPanel();
    label4 = new JLabel();
    LAXE2 = new XRiTextField();
    LAXE3 = new XRiTextField();
    LAXE4 = new XRiTextField();
    LAXE5 = new XRiTextField();
    LAXE6 = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau de bord");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(800, 32));
          p_tete_gauche.setMinimumSize(new Dimension(800, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("Soci\u00e9t\u00e9");
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");

          //---- WDOSL ----
          WDOSL.setOpaque(false);
          WDOSL.setText("@WDOSL@");
          WDOSL.setName("WDOSL");

          //---- OBJ_51_OBJ_51 ----
          OBJ_51_OBJ_51.setText("@WLBDOS@");
          OBJ_51_OBJ_51.setOpaque(false);
          OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");

          //---- WDATEX ----
          WDATEX.setOpaque(false);
          WDATEX.setName("WDATEX");

          //---- label2 ----
          label2.setText("Date de traitement");
          label2.setName("label2");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(WDOSL, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, 354, GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91)
                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WDATEX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(WDOSL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_51_OBJ_51, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WDATEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 250));
            menus_haut.setPreferredSize(new Dimension(160, 250));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(820, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setTitle("Balance par axes");
            panel1.setBorder(new DropShadowBorder());
            panel1.setTitleFont(panel1.getTitleFont().deriveFont(panel1.getTitleFont().getStyle() | Font.BOLD));
            panel1.setMinimumSize(new Dimension(168, 500));
            panel1.setPreferredSize(new Dimension(168, 520));
            panel1.setName("panel1");
            Container panel1ContentContainer = panel1.getContentContainer();
            panel1ContentContainer.setLayout(null);

            //======== SCROLLPANE_LIST3 ========
            {
              SCROLLPANE_LIST3.setName("SCROLLPANE_LIST3");

              //---- WLB011 ----
              WLB011.setName("WLB011");
              SCROLLPANE_LIST3.setViewportView(WLB011);
            }
            panel1ContentContainer.add(SCROLLPANE_LIST3);
            SCROLLPANE_LIST3.setBounds(20, 135, 719, 318);

            //---- OBJ_42_OBJ_42 ----
            OBJ_42_OBJ_42.setText("Arr\u00e9t\u00e9 des comptes");
            OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
            panel1ContentContainer.add(OBJ_42_OBJ_42);
            OBJ_42_OBJ_42.setBounds(20, 15, 155, 23);

            //---- BT_PGUP ----
            BT_PGUP.setName("BT_PGUP");
            panel1ContentContainer.add(BT_PGUP);
            BT_PGUP.setBounds(745, 135, 25, 145);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1ContentContainer.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(745, 305, 25, 145);

            //---- WLPERX ----
            WLPERX.setName("WLPERX");
            panel1ContentContainer.add(WLPERX);
            WLPERX.setBounds(195, 15, 124, WLPERX.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Analytique"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //======== panel3 ========
              {
                panel3.setOpaque(false);
                panel3.setName("panel3");
                panel3.setLayout(null);

                //---- label1 ----
                label1.setText("Section");
                label1.setName("label1");
                panel3.add(label1);
                label1.setBounds(10, 8, 55, 23);

                //---- WSANL ----
                WSANL.setName("WSANL");
                panel3.add(WSANL);
                WSANL.setBounds(65, 5, 64, WSANL.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel3.getComponentCount(); i++) {
                    Rectangle bounds = panel3.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel3.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel3.setMinimumSize(preferredSize);
                  panel3.setPreferredSize(preferredSize);
                }
              }
              panel2.add(panel3);
              panel3.setBounds(10, 25, 275, 45);

              //======== panel4 ========
              {
                panel4.setOpaque(false);
                panel4.setName("panel4");
                panel4.setLayout(null);

                //---- label4 ----
                label4.setText("Axes");
                label4.setName("label4");
                panel4.add(label4);
                label4.setBounds(10, 8, 45, 23);

                //---- LAXE2 ----
                LAXE2.setName("LAXE2");
                panel4.add(LAXE2);
                LAXE2.setBounds(60, 5, 64, LAXE2.getPreferredSize().height);

                //---- LAXE3 ----
                LAXE3.setName("LAXE3");
                panel4.add(LAXE3);
                LAXE3.setBounds(126, 5, 64, LAXE3.getPreferredSize().height);

                //---- LAXE4 ----
                LAXE4.setName("LAXE4");
                panel4.add(LAXE4);
                LAXE4.setBounds(192, 5, 64, LAXE4.getPreferredSize().height);

                //---- LAXE5 ----
                LAXE5.setName("LAXE5");
                panel4.add(LAXE5);
                LAXE5.setBounds(258, 5, 64, LAXE5.getPreferredSize().height);

                //---- LAXE6 ----
                LAXE6.setName("LAXE6");
                panel4.add(LAXE6);
                LAXE6.setBounds(324, 5, 64, LAXE6.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel4.getComponentCount(); i++) {
                    Rectangle bounds = panel4.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel4.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel4.setMinimumSize(preferredSize);
                  panel4.setPreferredSize(preferredSize);
                }
              }
              panel2.add(panel4);
              panel4.setBounds(305, 25, 405, 45);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1ContentContainer.add(panel2);
            panel2.setBounds(20, 55, 719, 80);
          }
          p_contenu.add(panel1);
          panel1.setBounds(10, 10, 795, 500);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_49_OBJ_49;
  private RiZoneSortie WDOSL;
  private RiZoneSortie OBJ_51_OBJ_51;
  private XRiCalendrier WDATEX;
  private JLabel label2;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel panel1;
  private JScrollPane SCROLLPANE_LIST3;
  private XRiTable WLB011;
  private JLabel OBJ_42_OBJ_42;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiTextField WLPERX;
  private JPanel panel2;
  private JPanel panel3;
  private JLabel label1;
  private XRiTextField WSANL;
  private JPanel panel4;
  private JLabel label4;
  private XRiTextField LAXE2;
  private XRiTextField LAXE3;
  private XRiTextField LAXE4;
  private XRiTextField LAXE5;
  private XRiTextField LAXE6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
