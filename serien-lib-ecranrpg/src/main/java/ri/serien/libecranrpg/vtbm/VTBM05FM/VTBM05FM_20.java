
package ri.serien.libecranrpg.vtbm.VTBM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VTBM05FM_20 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] WARR_Title = { "", "@ARRP1@", "@ARRP2@", "@ARRP3@", "@ARRP4@" };
  private String[] WARR_Value = null;
  // private ArrayList<String> WARR_Value = null;
  // private ArrayList<String> WARR_libelle = null;
  
  public VTBM05FM_20(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WARR.setValeurs(WARR_Value, WARR_Title);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WDOSL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDOSL@")).trim());
    WLBDOS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLBDOS@")).trim());
  }
  
  @Override
  public void setData() {
    // gererWARR();
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // on recharge les valeurs de top
    String value1 = lexique.HostFieldGetData("ARRP1").substring(0, 1);
    String value2 = lexique.HostFieldGetData("ARRP2").substring(0, 1);
    String value3 = lexique.HostFieldGetData("ARRP3").substring(0, 1);
    String value4 = lexique.HostFieldGetData("ARRP4").substring(0, 1);
    WARR_Value = new String[] { "", value1, value2, value3, value4 };
    WARR.setValeurs(WARR_Value, WARR_Title);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("WARR", 0, WARR_Value.get(WARR.getSelectedIndex()));
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  /*
  private void gererWARR()
  {
    WARR_Value = new ArrayList<String>();
    WARR_libelle =new ArrayList<String>();
    
    WARR_Value.add("");
    WARR_libelle.add("");
    
    if(!lexique.HostFieldGetData("ARRP1").trim().equals(""))
    {
      WARR_Value.add(lexique.HostFieldGetData("ARRP1").trim().substring(0,1));
      WARR_libelle.add(lexique.HostFieldGetData("ARRP1"));
    }
    
    if(!lexique.HostFieldGetData("ARRP2").trim().equals(""))
    {
      WARR_Value.add(lexique.HostFieldGetData("ARRP1").trim().substring(0,1));
      WARR_libelle.add(lexique.HostFieldGetData("ARRP2"));
    }
    
    if(!lexique.HostFieldGetData("ARRP3").trim().equals(""))
    {
      WARR_Value.add(lexique.HostFieldGetData("ARRP1").trim().substring(0,1));
      WARR_libelle.add(lexique.HostFieldGetData("ARRP3"));
    }
    
    if(!lexique.HostFieldGetData("ARRP4").trim().equals(""))
    {
      WARR_Value.add(lexique.HostFieldGetData("ARRP1").trim().substring(0,1));
      WARR_libelle.add(lexique.HostFieldGetData("ARRP4"));
    }
    
    String[] libelles = new String[WARR_libelle.size()];
    for(int i=0; i<libelles.length; i++)
      libelles[i] = WARR_libelle.get(i);
    
    WARR.setModel(new DefaultComboBoxModel(libelles));
  }*/
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WDOS");
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WARR = new XRiComboBox();
    OBJ_19_OBJ_19 = new JLabel();
    OBJ_23_OBJ_23 = new JLabel();
    OBJ_22_OBJ_22 = new JLabel();
    OBJ_17_OBJ_17 = new JLabel();
    OBJ_18_OBJ_18 = new JLabel();
    WNCGD = new XRiTextField();
    WNCGF = new XRiTextField();
    WNIVE = new XRiSpinner();
    OBJ_26_OBJ_26 = new JLabel();
    WDOSL = new RiZoneSortie();
    WLBDOS = new RiZoneSortie();
    label1 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(600, 265));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 250));
            menus_haut.setPreferredSize(new Dimension(160, 250));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu1 ========
            {
              riMenu1.setName("riMenu1");

              //---- riMenu_bt1 ----
              riMenu_bt1.setText("Options");
              riMenu_bt1.setName("riMenu_bt1");
              riMenu1.add(riMenu_bt1);
            }
            menus_haut.add(riMenu1);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Choix \u00e9tablissement");
              riSousMenu_bt1.setToolTipText("Choix \u00e9tablissement");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("P\u00e9riodes"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WARR ----
          WARR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WARR.setName("WARR");
          panel1.add(WARR);
          WARR.setBounds(175, 100, 159, WARR.getPreferredSize().height);

          //---- OBJ_19_OBJ_19 ----
          OBJ_19_OBJ_19.setText("Num\u00e9ro de compte");
          OBJ_19_OBJ_19.setName("OBJ_19_OBJ_19");
          panel1.add(OBJ_19_OBJ_19);
          OBJ_19_OBJ_19.setBounds(30, 64, 135, 20);

          //---- OBJ_23_OBJ_23 ----
          OBJ_23_OBJ_23.setText("Niveau d'affichage");
          OBJ_23_OBJ_23.setName("OBJ_23_OBJ_23");
          panel1.add(OBJ_23_OBJ_23);
          OBJ_23_OBJ_23.setBounds(30, 144, 135, 20);

          //---- OBJ_22_OBJ_22 ----
          OBJ_22_OBJ_22.setText("P\u00e9riode \u00e0 traiter");
          OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");
          panel1.add(OBJ_22_OBJ_22);
          OBJ_22_OBJ_22.setBounds(30, 103, 135, 20);

          //---- OBJ_17_OBJ_17 ----
          OBJ_17_OBJ_17.setText("D\u00e9but");
          OBJ_17_OBJ_17.setName("OBJ_17_OBJ_17");
          panel1.add(OBJ_17_OBJ_17);
          OBJ_17_OBJ_17.setBounds(175, 35, 58, 20);

          //---- OBJ_18_OBJ_18 ----
          OBJ_18_OBJ_18.setText("Fin");
          OBJ_18_OBJ_18.setName("OBJ_18_OBJ_18");
          panel1.add(OBJ_18_OBJ_18);
          OBJ_18_OBJ_18.setBounds(265, 35, 48, 20);

          //---- WNCGD ----
          WNCGD.setComponentPopupMenu(BTD);
          WNCGD.setName("WNCGD");
          panel1.add(WNCGD);
          WNCGD.setBounds(175, 60, 70, WNCGD.getPreferredSize().height);

          //---- WNCGF ----
          WNCGF.setComponentPopupMenu(BTD);
          WNCGF.setName("WNCGF");
          panel1.add(WNCGF);
          WNCGF.setBounds(265, 60, 70, WNCGF.getPreferredSize().height);

          //---- WNIVE ----
          WNIVE.setComponentPopupMenu(BTD);
          WNIVE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WNIVE.setModel(new SpinnerListModel(new String[] {"", "1", "2", "3", "4", "5", "6"}));
          WNIVE.setName("WNIVE");
          panel1.add(WNIVE);
          WNIVE.setBounds(175, 140, 46, 28);

          //---- OBJ_26_OBJ_26 ----
          OBJ_26_OBJ_26.setText("/");
          OBJ_26_OBJ_26.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_26_OBJ_26.setName("OBJ_26_OBJ_26");
          panel1.add(OBJ_26_OBJ_26);
          OBJ_26_OBJ_26.setBounds(245, 64, 22, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 50, 400, 200);

        //---- WDOSL ----
        WDOSL.setText("@WDOSL@");
        WDOSL.setName("WDOSL");
        p_contenu.add(WDOSL);
        WDOSL.setBounds(110, 15, 40, WDOSL.getPreferredSize().height);

        //---- WLBDOS ----
        WLBDOS.setText("@WLBDOS@");
        WLBDOS.setName("WLBDOS");
        p_contenu.add(WLBDOS);
        WLBDOS.setBounds(160, 15, 250, WLBDOS.getPreferredSize().height);

        //---- label1 ----
        label1.setText("Etablissement");
        label1.setName("label1");
        p_contenu.add(label1);
        label1.setBounds(15, 17, 95, 20);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiComboBox WARR;
  private JLabel OBJ_19_OBJ_19;
  private JLabel OBJ_23_OBJ_23;
  private JLabel OBJ_22_OBJ_22;
  private JLabel OBJ_17_OBJ_17;
  private JLabel OBJ_18_OBJ_18;
  private XRiTextField WNCGD;
  private XRiTextField WNCGF;
  private XRiSpinner WNIVE;
  private JLabel OBJ_26_OBJ_26;
  private RiZoneSortie WDOSL;
  private RiZoneSortie WLBDOS;
  private JLabel label1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
