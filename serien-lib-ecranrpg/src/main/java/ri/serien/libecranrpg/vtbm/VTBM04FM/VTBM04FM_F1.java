
package ri.serien.libecranrpg.vtbm.VTBM04FM;
// Nom Fichier: pop_VCGM05FM_FMTERR_367.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VTBM04FM_F1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VTBM04FM_F1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OK);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    OK.setIcon(lexique.chargerImage("images/OK.png", true));
    button1.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("ERREURS"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OKActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER");
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_5 = new JLabel();
    OK = new JButton();
    button1 = new JButton();

    //======== this ========
    setBackground(new Color(106, 23, 21));
    setName("this");
    setLayout(null);

    //---- OBJ_5 ----
    OBJ_5.setText("Veuillez confimer");
    OBJ_5.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_5.setForeground(Color.white);
    OBJ_5.setFont(OBJ_5.getFont().deriveFont(OBJ_5.getFont().getStyle() | Font.BOLD, OBJ_5.getFont().getSize() + 1f));
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(15, 10, 310, 20);

    //---- OK ----
    OK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OK.setToolTipText("?0X.OK.toolTipText?");
    OK.setText("Enregistrer");
    OK.setName("OK");
    OK.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OKActionPerformed(e);
      }
    });
    add(OK);
    OK.setBounds(95, 55, 160, 35);

    //---- button1 ----
    button1.setToolTipText("?0X.button1.toolTipText?");
    button1.setText("Corriger");
    button1.setName("button1");
    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button1ActionPerformed(e);
      }
    });
    add(button1);
    button1.setBounds(95, 95, 160, 35);

    setPreferredSize(new Dimension(340, 160));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_5;
  private JButton OK;
  private JButton button1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
