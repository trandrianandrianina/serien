
package ri.serien.libecranrpg.vgpm.VGPM03P1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGPM03P1_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGPM03P1_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@PG01LI@")).trim()));
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_69 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_71 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_73 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    LZ01 = new XRiTextField();
    lbLibelle1 = new JLabel();
    P1T01X = new XRiTextField();
    lbLibelle2 = new JLabel();
    LZ02 = new XRiTextField();
    P1T02X = new XRiTextField();
    lbLibelle3 = new JLabel();
    LZ03 = new XRiTextField();
    P1T03X = new XRiTextField();
    lbLibelle4 = new JLabel();
    LZ04 = new XRiTextField();
    P1T04X = new XRiTextField();
    lbLibelle5 = new JLabel();
    LZ05 = new XRiTextField();
    P1T05X = new XRiTextField();
    lbLibelle6 = new JLabel();
    LZ06 = new XRiTextField();
    P1T06X = new XRiTextField();
    lbLibelle7 = new JLabel();
    LZ07 = new XRiTextField();
    P1T07X = new XRiTextField();
    lbLibelle8 = new JLabel();
    LZ08 = new XRiTextField();
    P1T08X = new XRiTextField();
    lbLibelle9 = new JLabel();
    LZ09 = new XRiTextField();
    P1T09X = new XRiTextField();
    lbLibelle10 = new JLabel();
    LZ10 = new XRiTextField();
    P1T10X = new XRiTextField();
    lbLibelle11 = new JLabel();
    LZ11 = new XRiTextField();
    P1T11X = new XRiTextField();
    lbLibelle12 = new JLabel();
    LZ12 = new XRiTextField();
    P1T12X = new XRiTextField();
    lbLibelle13 = new JLabel();
    LZ13 = new XRiTextField();
    P1T13X = new XRiTextField();
    lbLibelle14 = new JLabel();
    LZ14 = new XRiTextField();
    P1T14X = new XRiTextField();
    lbLibelle15 = new JLabel();
    LZ15 = new XRiTextField();
    P1T15X = new XRiTextField();
    lbLibelle16 = new JLabel();
    LZ16 = new XRiTextField();
    P1T16X = new XRiTextField();
    lbLibelle17 = new JLabel();
    LZ17 = new XRiTextField();
    P1T17X = new XRiTextField();
    lbLibelle18 = new JLabel();
    LZ18 = new XRiTextField();
    P1T18X = new XRiTextField();
    lbLibelle19 = new JLabel();
    LZ19 = new XRiTextField();
    P1T19X = new XRiTextField();
    lbLibelle20 = new JLabel();
    LZ20 = new XRiTextField();
    P1T20X = new XRiTextField();
    lbLibelle21 = new JLabel();
    LZ21 = new XRiTextField();
    P1T21X = new XRiTextField();
    lbLibelle22 = new JLabel();
    LZ22 = new XRiTextField();
    P1T22X = new XRiTextField();
    lbLibelle23 = new JLabel();
    LZ23 = new XRiTextField();
    P1T23X = new XRiTextField();
    lbLibelle24 = new JLabel();
    LZ24 = new XRiTextField();
    P1T24X = new XRiTextField();
    lbLibelle25 = new JLabel();
    LZ25 = new XRiTextField();
    P1T25X = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la production");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_69 ----
          OBJ_69.setText("Etablissement");
          OBJ_69.setName("OBJ_69");
          p_tete_gauche.add(OBJ_69);
          OBJ_69.setBounds(5, 5, 93, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(100, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_71 ----
          OBJ_71.setText("Code");
          OBJ_71.setName("OBJ_71");
          p_tete_gauche.add(OBJ_71);
          OBJ_71.setBounds(160, 5, 36, 18);

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");
          p_tete_gauche.add(INDTYP);
          INDTYP.setBounds(200, 0, 30, INDTYP.getPreferredSize().height);

          //---- OBJ_73 ----
          OBJ_73.setText("Ordre");
          OBJ_73.setName("OBJ_73");
          p_tete_gauche.add(OBJ_73);
          OBJ_73.setBounds(250, 5, 39, 18);

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");
          p_tete_gauche.add(INDIND);
          INDIND.setBounds(300, 0, 60, INDIND.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new BorderLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(410, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(new GridBagLayout());
          ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {1019, 0};
          ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {505, 0, 0};
          ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("@PG01LI@"));
            panel1.setOpaque(false);
            panel1.setMinimumSize(new Dimension(389, 490));
            panel1.setPreferredSize(new Dimension(389, 490));
            panel1.setName("panel1");
            panel1.setLayout(new GridBagLayout());
            ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {55, 76, 120, 95, 55, 0, 115, 0};
            ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {0, 33, 33, 33, 33, 33, 34, 33, 33, 33, 33, 33, 28, 0};
            ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- LZ01 ----
            LZ01.setComponentPopupMenu(BTD);
            LZ01.setMinimumSize(new Dimension(210, 28));
            LZ01.setPreferredSize(new Dimension(210, 28));
            LZ01.setName("LZ01");
            panel1.add(LZ01, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle1 ----
            lbLibelle1.setText("01");
            lbLibelle1.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle1.setName("lbLibelle1");
            panel1.add(lbLibelle1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T01X ----
            P1T01X.setComponentPopupMenu(BTD);
            P1T01X.setMinimumSize(new Dimension(110, 28));
            P1T01X.setPreferredSize(new Dimension(110, 28));
            P1T01X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T01X.setName("P1T01X");
            panel1.add(P1T01X, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle2 ----
            lbLibelle2.setText("02");
            lbLibelle2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle2.setName("lbLibelle2");
            panel1.add(lbLibelle2, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ02 ----
            LZ02.setComponentPopupMenu(BTD);
            LZ02.setMinimumSize(new Dimension(210, 28));
            LZ02.setPreferredSize(new Dimension(210, 28));
            LZ02.setName("LZ02");
            panel1.add(LZ02, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T02X ----
            P1T02X.setComponentPopupMenu(BTD);
            P1T02X.setMinimumSize(new Dimension(110, 28));
            P1T02X.setPreferredSize(new Dimension(110, 28));
            P1T02X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T02X.setName("P1T02X");
            panel1.add(P1T02X, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbLibelle3 ----
            lbLibelle3.setText("03");
            lbLibelle3.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle3.setName("lbLibelle3");
            panel1.add(lbLibelle3, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ03 ----
            LZ03.setComponentPopupMenu(BTD);
            LZ03.setMinimumSize(new Dimension(210, 28));
            LZ03.setPreferredSize(new Dimension(210, 28));
            LZ03.setName("LZ03");
            panel1.add(LZ03, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T03X ----
            P1T03X.setComponentPopupMenu(BTD);
            P1T03X.setMinimumSize(new Dimension(110, 28));
            P1T03X.setPreferredSize(new Dimension(110, 28));
            P1T03X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T03X.setName("P1T03X");
            panel1.add(P1T03X, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle4 ----
            lbLibelle4.setText("04");
            lbLibelle4.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle4.setName("lbLibelle4");
            panel1.add(lbLibelle4, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ04 ----
            LZ04.setComponentPopupMenu(BTD);
            LZ04.setMinimumSize(new Dimension(210, 28));
            LZ04.setPreferredSize(new Dimension(210, 28));
            LZ04.setName("LZ04");
            panel1.add(LZ04, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T04X ----
            P1T04X.setComponentPopupMenu(BTD);
            P1T04X.setMinimumSize(new Dimension(110, 28));
            P1T04X.setPreferredSize(new Dimension(110, 28));
            P1T04X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T04X.setName("P1T04X");
            panel1.add(P1T04X, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbLibelle5 ----
            lbLibelle5.setText("05");
            lbLibelle5.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle5.setName("lbLibelle5");
            panel1.add(lbLibelle5, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ05 ----
            LZ05.setComponentPopupMenu(BTD);
            LZ05.setMinimumSize(new Dimension(210, 28));
            LZ05.setPreferredSize(new Dimension(210, 28));
            LZ05.setName("LZ05");
            panel1.add(LZ05, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T05X ----
            P1T05X.setComponentPopupMenu(BTD);
            P1T05X.setMinimumSize(new Dimension(110, 28));
            P1T05X.setPreferredSize(new Dimension(110, 28));
            P1T05X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T05X.setName("P1T05X");
            panel1.add(P1T05X, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle6 ----
            lbLibelle6.setText("06");
            lbLibelle6.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle6.setName("lbLibelle6");
            panel1.add(lbLibelle6, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ06 ----
            LZ06.setComponentPopupMenu(BTD);
            LZ06.setMinimumSize(new Dimension(210, 28));
            LZ06.setPreferredSize(new Dimension(210, 28));
            LZ06.setName("LZ06");
            panel1.add(LZ06, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T06X ----
            P1T06X.setComponentPopupMenu(BTD);
            P1T06X.setMinimumSize(new Dimension(110, 28));
            P1T06X.setPreferredSize(new Dimension(110, 28));
            P1T06X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T06X.setName("P1T06X");
            panel1.add(P1T06X, new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbLibelle7 ----
            lbLibelle7.setText("07");
            lbLibelle7.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle7.setName("lbLibelle7");
            panel1.add(lbLibelle7, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ07 ----
            LZ07.setComponentPopupMenu(BTD);
            LZ07.setMinimumSize(new Dimension(210, 28));
            LZ07.setPreferredSize(new Dimension(210, 28));
            LZ07.setName("LZ07");
            panel1.add(LZ07, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T07X ----
            P1T07X.setComponentPopupMenu(BTD);
            P1T07X.setMinimumSize(new Dimension(110, 28));
            P1T07X.setPreferredSize(new Dimension(110, 28));
            P1T07X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T07X.setName("P1T07X");
            panel1.add(P1T07X, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle8 ----
            lbLibelle8.setText("08");
            lbLibelle8.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle8.setName("lbLibelle8");
            panel1.add(lbLibelle8, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ08 ----
            LZ08.setComponentPopupMenu(BTD);
            LZ08.setMinimumSize(new Dimension(210, 28));
            LZ08.setPreferredSize(new Dimension(210, 28));
            LZ08.setName("LZ08");
            panel1.add(LZ08, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T08X ----
            P1T08X.setComponentPopupMenu(BTD);
            P1T08X.setMinimumSize(new Dimension(110, 28));
            P1T08X.setPreferredSize(new Dimension(110, 28));
            P1T08X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T08X.setName("P1T08X");
            panel1.add(P1T08X, new GridBagConstraints(6, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbLibelle9 ----
            lbLibelle9.setText("09");
            lbLibelle9.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle9.setName("lbLibelle9");
            panel1.add(lbLibelle9, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ09 ----
            LZ09.setComponentPopupMenu(BTD);
            LZ09.setMinimumSize(new Dimension(210, 28));
            LZ09.setPreferredSize(new Dimension(210, 28));
            LZ09.setName("LZ09");
            panel1.add(LZ09, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T09X ----
            P1T09X.setComponentPopupMenu(BTD);
            P1T09X.setMinimumSize(new Dimension(110, 28));
            P1T09X.setPreferredSize(new Dimension(110, 28));
            P1T09X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T09X.setName("P1T09X");
            panel1.add(P1T09X, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle10 ----
            lbLibelle10.setText("10");
            lbLibelle10.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle10.setName("lbLibelle10");
            panel1.add(lbLibelle10, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ10 ----
            LZ10.setComponentPopupMenu(BTD);
            LZ10.setMinimumSize(new Dimension(210, 28));
            LZ10.setPreferredSize(new Dimension(210, 28));
            LZ10.setName("LZ10");
            panel1.add(LZ10, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T10X ----
            P1T10X.setComponentPopupMenu(BTD);
            P1T10X.setMinimumSize(new Dimension(110, 28));
            P1T10X.setPreferredSize(new Dimension(110, 28));
            P1T10X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T10X.setName("P1T10X");
            panel1.add(P1T10X, new GridBagConstraints(6, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbLibelle11 ----
            lbLibelle11.setText("11");
            lbLibelle11.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle11.setName("lbLibelle11");
            panel1.add(lbLibelle11, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ11 ----
            LZ11.setComponentPopupMenu(BTD);
            LZ11.setMinimumSize(new Dimension(210, 28));
            LZ11.setPreferredSize(new Dimension(210, 28));
            LZ11.setName("LZ11");
            panel1.add(LZ11, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T11X ----
            P1T11X.setComponentPopupMenu(BTD);
            P1T11X.setMinimumSize(new Dimension(110, 28));
            P1T11X.setPreferredSize(new Dimension(110, 28));
            P1T11X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T11X.setName("P1T11X");
            panel1.add(P1T11X, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle12 ----
            lbLibelle12.setText("12");
            lbLibelle12.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle12.setName("lbLibelle12");
            panel1.add(lbLibelle12, new GridBagConstraints(4, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ12 ----
            LZ12.setComponentPopupMenu(BTD);
            LZ12.setMinimumSize(new Dimension(210, 28));
            LZ12.setPreferredSize(new Dimension(210, 28));
            LZ12.setName("LZ12");
            panel1.add(LZ12, new GridBagConstraints(5, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T12X ----
            P1T12X.setComponentPopupMenu(BTD);
            P1T12X.setMinimumSize(new Dimension(110, 28));
            P1T12X.setPreferredSize(new Dimension(110, 28));
            P1T12X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T12X.setName("P1T12X");
            panel1.add(P1T12X, new GridBagConstraints(6, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbLibelle13 ----
            lbLibelle13.setText("13");
            lbLibelle13.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle13.setName("lbLibelle13");
            panel1.add(lbLibelle13, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ13 ----
            LZ13.setComponentPopupMenu(BTD);
            LZ13.setMinimumSize(new Dimension(210, 28));
            LZ13.setPreferredSize(new Dimension(210, 28));
            LZ13.setName("LZ13");
            panel1.add(LZ13, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T13X ----
            P1T13X.setComponentPopupMenu(BTD);
            P1T13X.setMinimumSize(new Dimension(110, 28));
            P1T13X.setPreferredSize(new Dimension(110, 28));
            P1T13X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T13X.setName("P1T13X");
            panel1.add(P1T13X, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle14 ----
            lbLibelle14.setText("14");
            lbLibelle14.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle14.setName("lbLibelle14");
            panel1.add(lbLibelle14, new GridBagConstraints(4, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ14 ----
            LZ14.setComponentPopupMenu(BTD);
            LZ14.setMinimumSize(new Dimension(210, 28));
            LZ14.setPreferredSize(new Dimension(210, 28));
            LZ14.setName("LZ14");
            panel1.add(LZ14, new GridBagConstraints(5, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T14X ----
            P1T14X.setComponentPopupMenu(BTD);
            P1T14X.setMinimumSize(new Dimension(110, 28));
            P1T14X.setPreferredSize(new Dimension(110, 28));
            P1T14X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T14X.setName("P1T14X");
            panel1.add(P1T14X, new GridBagConstraints(6, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbLibelle15 ----
            lbLibelle15.setText("15");
            lbLibelle15.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle15.setName("lbLibelle15");
            panel1.add(lbLibelle15, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ15 ----
            LZ15.setComponentPopupMenu(BTD);
            LZ15.setMinimumSize(new Dimension(210, 28));
            LZ15.setPreferredSize(new Dimension(210, 28));
            LZ15.setName("LZ15");
            panel1.add(LZ15, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T15X ----
            P1T15X.setComponentPopupMenu(BTD);
            P1T15X.setMinimumSize(new Dimension(110, 28));
            P1T15X.setPreferredSize(new Dimension(110, 28));
            P1T15X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T15X.setName("P1T15X");
            panel1.add(P1T15X, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle16 ----
            lbLibelle16.setText("16");
            lbLibelle16.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle16.setName("lbLibelle16");
            panel1.add(lbLibelle16, new GridBagConstraints(4, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ16 ----
            LZ16.setComponentPopupMenu(BTD);
            LZ16.setMinimumSize(new Dimension(210, 28));
            LZ16.setPreferredSize(new Dimension(210, 28));
            LZ16.setName("LZ16");
            panel1.add(LZ16, new GridBagConstraints(5, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T16X ----
            P1T16X.setComponentPopupMenu(BTD);
            P1T16X.setMinimumSize(new Dimension(110, 28));
            P1T16X.setPreferredSize(new Dimension(110, 28));
            P1T16X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T16X.setName("P1T16X");
            panel1.add(P1T16X, new GridBagConstraints(6, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbLibelle17 ----
            lbLibelle17.setText("17");
            lbLibelle17.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle17.setName("lbLibelle17");
            panel1.add(lbLibelle17, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ17 ----
            LZ17.setComponentPopupMenu(BTD);
            LZ17.setMinimumSize(new Dimension(210, 28));
            LZ17.setPreferredSize(new Dimension(210, 28));
            LZ17.setName("LZ17");
            panel1.add(LZ17, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T17X ----
            P1T17X.setComponentPopupMenu(BTD);
            P1T17X.setMinimumSize(new Dimension(110, 28));
            P1T17X.setPreferredSize(new Dimension(110, 28));
            P1T17X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T17X.setName("P1T17X");
            panel1.add(P1T17X, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle18 ----
            lbLibelle18.setText("18");
            lbLibelle18.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle18.setName("lbLibelle18");
            panel1.add(lbLibelle18, new GridBagConstraints(4, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ18 ----
            LZ18.setComponentPopupMenu(BTD);
            LZ18.setMinimumSize(new Dimension(210, 28));
            LZ18.setPreferredSize(new Dimension(210, 28));
            LZ18.setName("LZ18");
            panel1.add(LZ18, new GridBagConstraints(5, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T18X ----
            P1T18X.setComponentPopupMenu(BTD);
            P1T18X.setMinimumSize(new Dimension(110, 28));
            P1T18X.setPreferredSize(new Dimension(110, 28));
            P1T18X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T18X.setName("P1T18X");
            panel1.add(P1T18X, new GridBagConstraints(6, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbLibelle19 ----
            lbLibelle19.setText("19");
            lbLibelle19.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle19.setName("lbLibelle19");
            panel1.add(lbLibelle19, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ19 ----
            LZ19.setComponentPopupMenu(BTD);
            LZ19.setMinimumSize(new Dimension(210, 28));
            LZ19.setPreferredSize(new Dimension(210, 28));
            LZ19.setName("LZ19");
            panel1.add(LZ19, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T19X ----
            P1T19X.setComponentPopupMenu(BTD);
            P1T19X.setMinimumSize(new Dimension(110, 28));
            P1T19X.setPreferredSize(new Dimension(110, 28));
            P1T19X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T19X.setName("P1T19X");
            panel1.add(P1T19X, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle20 ----
            lbLibelle20.setText("20");
            lbLibelle20.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle20.setName("lbLibelle20");
            panel1.add(lbLibelle20, new GridBagConstraints(4, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ20 ----
            LZ20.setComponentPopupMenu(BTD);
            LZ20.setMinimumSize(new Dimension(210, 28));
            LZ20.setPreferredSize(new Dimension(210, 28));
            LZ20.setName("LZ20");
            panel1.add(LZ20, new GridBagConstraints(5, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T20X ----
            P1T20X.setComponentPopupMenu(BTD);
            P1T20X.setMinimumSize(new Dimension(110, 28));
            P1T20X.setPreferredSize(new Dimension(110, 28));
            P1T20X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T20X.setName("P1T20X");
            panel1.add(P1T20X, new GridBagConstraints(6, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbLibelle21 ----
            lbLibelle21.setText("21");
            lbLibelle21.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle21.setName("lbLibelle21");
            panel1.add(lbLibelle21, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ21 ----
            LZ21.setComponentPopupMenu(BTD);
            LZ21.setMinimumSize(new Dimension(210, 28));
            LZ21.setPreferredSize(new Dimension(210, 28));
            LZ21.setName("LZ21");
            panel1.add(LZ21, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T21X ----
            P1T21X.setComponentPopupMenu(BTD);
            P1T21X.setMinimumSize(new Dimension(110, 28));
            P1T21X.setPreferredSize(new Dimension(110, 28));
            P1T21X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T21X.setName("P1T21X");
            panel1.add(P1T21X, new GridBagConstraints(2, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle22 ----
            lbLibelle22.setText("22");
            lbLibelle22.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle22.setName("lbLibelle22");
            panel1.add(lbLibelle22, new GridBagConstraints(4, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ22 ----
            LZ22.setComponentPopupMenu(BTD);
            LZ22.setMinimumSize(new Dimension(210, 28));
            LZ22.setPreferredSize(new Dimension(210, 28));
            LZ22.setName("LZ22");
            panel1.add(LZ22, new GridBagConstraints(5, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T22X ----
            P1T22X.setComponentPopupMenu(BTD);
            P1T22X.setMinimumSize(new Dimension(110, 28));
            P1T22X.setPreferredSize(new Dimension(110, 28));
            P1T22X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T22X.setName("P1T22X");
            panel1.add(P1T22X, new GridBagConstraints(6, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbLibelle23 ----
            lbLibelle23.setText("23");
            lbLibelle23.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle23.setName("lbLibelle23");
            panel1.add(lbLibelle23, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ23 ----
            LZ23.setComponentPopupMenu(BTD);
            LZ23.setMinimumSize(new Dimension(210, 28));
            LZ23.setPreferredSize(new Dimension(210, 28));
            LZ23.setName("LZ23");
            panel1.add(LZ23, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T23X ----
            P1T23X.setComponentPopupMenu(BTD);
            P1T23X.setMinimumSize(new Dimension(110, 28));
            P1T23X.setPreferredSize(new Dimension(110, 28));
            P1T23X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T23X.setName("P1T23X");
            panel1.add(P1T23X, new GridBagConstraints(2, 11, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle24 ----
            lbLibelle24.setText("24");
            lbLibelle24.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle24.setName("lbLibelle24");
            panel1.add(lbLibelle24, new GridBagConstraints(4, 11, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LZ24 ----
            LZ24.setComponentPopupMenu(BTD);
            LZ24.setMinimumSize(new Dimension(210, 28));
            LZ24.setPreferredSize(new Dimension(210, 28));
            LZ24.setName("LZ24");
            panel1.add(LZ24, new GridBagConstraints(5, 11, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- P1T24X ----
            P1T24X.setComponentPopupMenu(BTD);
            P1T24X.setMinimumSize(new Dimension(110, 28));
            P1T24X.setPreferredSize(new Dimension(110, 28));
            P1T24X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T24X.setName("P1T24X");
            panel1.add(P1T24X, new GridBagConstraints(6, 11, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbLibelle25 ----
            lbLibelle25.setText("25");
            lbLibelle25.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLibelle25.setName("lbLibelle25");
            panel1.add(lbLibelle25, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- LZ25 ----
            LZ25.setComponentPopupMenu(BTD);
            LZ25.setMinimumSize(new Dimension(210, 28));
            LZ25.setPreferredSize(new Dimension(210, 28));
            LZ25.setName("LZ25");
            panel1.add(LZ25, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- P1T25X ----
            P1T25X.setComponentPopupMenu(BTD);
            P1T25X.setMinimumSize(new Dimension(110, 28));
            P1T25X.setPreferredSize(new Dimension(110, 28));
            P1T25X.setHorizontalAlignment(SwingConstants.RIGHT);
            P1T25X.setName("P1T25X");
            panel1.add(P1T25X, new GridBagConstraints(2, 12, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          }
          p_contenu.add(panel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 0), 0, 0));
        }
        p_centrage.add(p_contenu, BorderLayout.CENTER);
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_69;
  private XRiTextField INDETB;
  private JLabel OBJ_71;
  private XRiTextField INDTYP;
  private JLabel OBJ_73;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField LZ01;
  private JLabel lbLibelle1;
  private XRiTextField P1T01X;
  private JLabel lbLibelle2;
  private XRiTextField LZ02;
  private XRiTextField P1T02X;
  private JLabel lbLibelle3;
  private XRiTextField LZ03;
  private XRiTextField P1T03X;
  private JLabel lbLibelle4;
  private XRiTextField LZ04;
  private XRiTextField P1T04X;
  private JLabel lbLibelle5;
  private XRiTextField LZ05;
  private XRiTextField P1T05X;
  private JLabel lbLibelle6;
  private XRiTextField LZ06;
  private XRiTextField P1T06X;
  private JLabel lbLibelle7;
  private XRiTextField LZ07;
  private XRiTextField P1T07X;
  private JLabel lbLibelle8;
  private XRiTextField LZ08;
  private XRiTextField P1T08X;
  private JLabel lbLibelle9;
  private XRiTextField LZ09;
  private XRiTextField P1T09X;
  private JLabel lbLibelle10;
  private XRiTextField LZ10;
  private XRiTextField P1T10X;
  private JLabel lbLibelle11;
  private XRiTextField LZ11;
  private XRiTextField P1T11X;
  private JLabel lbLibelle12;
  private XRiTextField LZ12;
  private XRiTextField P1T12X;
  private JLabel lbLibelle13;
  private XRiTextField LZ13;
  private XRiTextField P1T13X;
  private JLabel lbLibelle14;
  private XRiTextField LZ14;
  private XRiTextField P1T14X;
  private JLabel lbLibelle15;
  private XRiTextField LZ15;
  private XRiTextField P1T15X;
  private JLabel lbLibelle16;
  private XRiTextField LZ16;
  private XRiTextField P1T16X;
  private JLabel lbLibelle17;
  private XRiTextField LZ17;
  private XRiTextField P1T17X;
  private JLabel lbLibelle18;
  private XRiTextField LZ18;
  private XRiTextField P1T18X;
  private JLabel lbLibelle19;
  private XRiTextField LZ19;
  private XRiTextField P1T19X;
  private JLabel lbLibelle20;
  private XRiTextField LZ20;
  private XRiTextField P1T20X;
  private JLabel lbLibelle21;
  private XRiTextField LZ21;
  private XRiTextField P1T21X;
  private JLabel lbLibelle22;
  private XRiTextField LZ22;
  private XRiTextField P1T22X;
  private JLabel lbLibelle23;
  private XRiTextField LZ23;
  private XRiTextField P1T23X;
  private JLabel lbLibelle24;
  private XRiTextField LZ24;
  private XRiTextField P1T24X;
  private JLabel lbLibelle25;
  private XRiTextField LZ25;
  private XRiTextField P1T25X;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
