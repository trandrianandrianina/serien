
package ri.serien.libecranrpg.vgpm.VGPM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGPM06FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGPM06FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("SAISIE DES HEURES"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    panel2 = new JPanel();
    OBJ_45 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_49 = new JLabel();
    LPQT3 = new XRiTextField();
    LPQT4 = new XRiTextField();
    LPQT5 = new XRiTextField();
    OBJ_39 = new JLabel();
    OBJ_42 = new JLabel();
    LPDTEX = new XRiCalendrier();
    OBJ_37 = new JLabel();
    HRE1 = new XRiTextField();
    MIN1 = new XRiTextField();
    HRE2 = new XRiTextField();
    MIN2 = new XRiTextField();
    INNOM = new XRiTextField();
    LBORD = new XRiTextField();
    OBJ_27 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_28 = new JLabel();
    LPNUM = new XRiTextField();
    LPINT = new XRiTextField();
    OBJ_32 = new JLabel();
    OBJ_31 = new JLabel();
    INDETB = new XRiTextField();
    LBNL1 = new XRiTextField();
    LBNL2 = new XRiTextField();
    LBNL3 = new XRiTextField();
    LPTYP = new XRiTextField();
    LPSUF = new XRiTextField();
    OBJ_29 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_55 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 450));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Saisie"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_45 ----
            OBJ_45.setText("Soit un temps pass\u00e9 de");
            OBJ_45.setName("OBJ_45");
            panel2.add(OBJ_45);
            OBJ_45.setBounds(25, 90, 144, 22);

            //---- OBJ_47 ----
            OBJ_47.setText("Quantit\u00e9s correctes");
            OBJ_47.setName("OBJ_47");
            panel2.add(OBJ_47);
            OBJ_47.setBounds(25, 150, 118, 20);

            //---- OBJ_49 ----
            OBJ_49.setText("Quantit\u00e9s rebut\u00e9es");
            OBJ_49.setName("OBJ_49");
            panel2.add(OBJ_49);
            OBJ_49.setBounds(25, 185, 115, 20);

            //---- LPQT3 ----
            LPQT3.setName("LPQT3");
            panel2.add(LPQT3);
            LPQT3.setBounds(285, 87, 106, LPQT3.getPreferredSize().height);

            //---- LPQT4 ----
            LPQT4.setName("LPQT4");
            panel2.add(LPQT4);
            LPQT4.setBounds(285, 146, 106, LPQT4.getPreferredSize().height);

            //---- LPQT5 ----
            LPQT5.setName("LPQT5");
            panel2.add(LPQT5);
            LPQT5.setBounds(285, 181, 106, LPQT5.getPreferredSize().height);

            //---- OBJ_39 ----
            OBJ_39.setText("Heure de d\u00e9but");
            OBJ_39.setName("OBJ_39");
            panel2.add(OBJ_39);
            OBJ_39.setBounds(170, 44, 96, 20);

            //---- OBJ_42 ----
            OBJ_42.setText("Heure de fin");
            OBJ_42.setName("OBJ_42");
            panel2.add(OBJ_42);
            OBJ_42.setBounds(400, 44, 75, 20);

            //---- LPDTEX ----
            LPDTEX.setName("LPDTEX");
            panel2.add(LPDTEX);
            LPDTEX.setBounds(25, 40, 105, LPDTEX.getPreferredSize().height);

            //---- OBJ_37 ----
            OBJ_37.setText("Date");
            OBJ_37.setName("OBJ_37");
            panel2.add(OBJ_37);
            OBJ_37.setBounds(25, 20, 33, 20);

            //---- HRE1 ----
            HRE1.setToolTipText("Heure sous la forme HH");
            HRE1.setName("HRE1");
            panel2.add(HRE1);
            HRE1.setBounds(285, 40, 26, HRE1.getPreferredSize().height);

            //---- MIN1 ----
            MIN1.setToolTipText("Minutes sous la forme MM");
            MIN1.setName("MIN1");
            panel2.add(MIN1);
            MIN1.setBounds(320, 40, 26, MIN1.getPreferredSize().height);

            //---- HRE2 ----
            HRE2.setToolTipText("Heure sous la forme HH");
            HRE2.setName("HRE2");
            panel2.add(HRE2);
            HRE2.setBounds(490, 40, 26, HRE2.getPreferredSize().height);

            //---- MIN2 ----
            MIN2.setToolTipText("Minutes sous la forme MM");
            MIN2.setName("MIN2");
            panel2.add(MIN2);
            MIN2.setBounds(525, 40, 26, MIN2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(10, 185, 590, 230);

          //---- INNOM ----
          INNOM.setName("INNOM");
          panel1.add(INNOM);
          INNOM.setBounds(195, 127, 310, INNOM.getPreferredSize().height);

          //---- LBORD ----
          LBORD.setName("LBORD");
          panel1.add(LBORD);
          LBORD.setBounds(535, 75, 44, LBORD.getPreferredSize().height);

          //---- OBJ_27 ----
          OBJ_27.setText("Etablissement");
          OBJ_27.setName("OBJ_27");
          panel1.add(OBJ_27);
          OBJ_27.setBounds(30, 50, 88, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("Intervenant");
          OBJ_35.setName("OBJ_35");
          panel1.add(OBJ_35);
          OBJ_35.setBounds(30, 130, 69, 22);

          //---- OBJ_30 ----
          OBJ_30.setText("Niveaux");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(240, 50, 52, 20);

          //---- OBJ_28 ----
          OBJ_28.setText("Num\u00e9ro");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(135, 50, 51, 20);

          //---- LPNUM ----
          LPNUM.setName("LPNUM");
          panel1.add(LPNUM);
          LPNUM.setBounds(135, 75, 58, LPNUM.getPreferredSize().height);

          //---- LPINT ----
          LPINT.setComponentPopupMenu(BTD);
          LPINT.setName("LPINT");
          panel1.add(LPINT);
          LPINT.setBounds(135, 127, 42, LPINT.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("Ordre");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(535, 50, 37, 20);

          //---- OBJ_31 ----
          OBJ_31.setText("Type");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(475, 50, 36, 20);

          //---- INDETB ----
          INDETB.setName("INDETB");
          panel1.add(INDETB);
          INDETB.setBounds(30, 75, 40, INDETB.getPreferredSize().height);

          //---- LBNL1 ----
          LBNL1.setName("LBNL1");
          panel1.add(LBNL1);
          LBNL1.setBounds(255, 75, 34, LBNL1.getPreferredSize().height);

          //---- LBNL2 ----
          LBNL2.setName("LBNL2");
          panel1.add(LBNL2);
          LBNL2.setBounds(325, 75, 34, LBNL2.getPreferredSize().height);

          //---- LBNL3 ----
          LBNL3.setName("LBNL3");
          panel1.add(LBNL3);
          LBNL3.setBounds(395, 75, 34, LBNL3.getPreferredSize().height);

          //---- LPTYP ----
          LPTYP.setName("LPTYP");
          panel1.add(LPTYP);
          LPTYP.setBounds(475, 75, 44, LPTYP.getPreferredSize().height);

          //---- LPSUF ----
          LPSUF.setName("LPSUF");
          panel1.add(LPSUF);
          LPSUF.setBounds(195, 75, 18, LPSUF.getPreferredSize().height);

          //---- OBJ_29 ----
          OBJ_29.setText("1");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(240, 80, 12, 18);

          //---- OBJ_54 ----
          OBJ_54.setText("2");
          OBJ_54.setName("OBJ_54");
          panel1.add(OBJ_54);
          OBJ_54.setBounds(310, 80, 12, 18);

          //---- OBJ_55 ----
          OBJ_55.setText("3");
          OBJ_55.setName("OBJ_55");
          panel1.add(OBJ_55);
          OBJ_55.setBounds(380, 80, 12, 18);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 615, 430);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JPanel panel2;
  private JLabel OBJ_45;
  private JLabel OBJ_47;
  private JLabel OBJ_49;
  private XRiTextField LPQT3;
  private XRiTextField LPQT4;
  private XRiTextField LPQT5;
  private JLabel OBJ_39;
  private JLabel OBJ_42;
  private XRiCalendrier LPDTEX;
  private JLabel OBJ_37;
  private XRiTextField HRE1;
  private XRiTextField MIN1;
  private XRiTextField HRE2;
  private XRiTextField MIN2;
  private XRiTextField INNOM;
  private XRiTextField LBORD;
  private JLabel OBJ_27;
  private JLabel OBJ_35;
  private JLabel OBJ_30;
  private JLabel OBJ_28;
  private XRiTextField LPNUM;
  private XRiTextField LPINT;
  private JLabel OBJ_32;
  private JLabel OBJ_31;
  private XRiTextField INDETB;
  private XRiTextField LBNL1;
  private XRiTextField LBNL2;
  private XRiTextField LBNL3;
  private XRiTextField LPTYP;
  private XRiTextField LPSUF;
  private JLabel OBJ_29;
  private JLabel OBJ_54;
  private JLabel OBJ_55;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
