
package ri.serien.libecranrpg.vgpm.VGPM07FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGPM07FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGPM07FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    panel2.setVisible(lexique.isTrue("N80"));
    panel3.setVisible(lexique.isTrue("N80"));
    panel4.setVisible(lexique.isTrue("80"));
    lbProrata.setVisible(lexique.HostFieldGetData("PRORAT").trim().equals("1"));
    WSER_OBJ_50_35.setSelected(lexique.HostFieldGetData("WSER").equalsIgnoreCase("L"));
    WSER.setSelected(lexique.HostFieldGetData("WSER").equalsIgnoreCase("S"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("CONSOMMATIONS MATIERES"));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (WSER_OBJ_50_35.isSelected()) {
      lexique.HostFieldPutData("WSER", 0, "L");
    }
    else {
      lexique.HostFieldPutData("WSER", 0, " ");
    }
    if (WSER.isSelected()) {
      lexique.HostFieldPutData("WSER", 0, "S");
    }
    else {
      lexique.HostFieldPutData("WSER", 0, " ");
    }
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    LPNUM = new XRiTextField();
    INDETB = new XRiTextField();
    LPSUF = new XRiTextField();
    panel2 = new JPanel();
    LBNL1 = new XRiTextField();
    OBJ_36 = new JLabel();
    OBJ_37 = new JLabel();
    LBNL2 = new XRiTextField();
    LBNL3 = new XRiTextField();
    OBJ_38 = new JLabel();
    LPQT3 = new XRiTextField();
    OBJ_56 = new JLabel();
    OBJ_47 = new JLabel();
    LPDTEX = new XRiCalendrier();
    OBJ_57 = new JLabel();
    LPVDE = new XRiTextField();
    WSER_OBJ_50_35 = new JCheckBox();
    WSER = new JCheckBox();
    panel4 = new JPanel();
    EBART = new XRiTextField();
    OBJ_44 = new JLabel();
    panel3 = new JPanel();
    OBJ_42 = new JLabel();
    LPTYP = new XRiTextField();
    OBJ_43 = new JLabel();
    LBORD = new XRiTextField();
    lbProrata = new JLabel();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(760, 320));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Composants au prorata");
            riSousMenu_bt6.setToolTipText("G\u00e9n\u00e9ration de composants au prorata");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_27 ----
          OBJ_27.setText("Etablissement");
          OBJ_27.setName("OBJ_27");
          panel1.add(OBJ_27);
          OBJ_27.setBounds(45, 15, 88, 20);

          //---- OBJ_28 ----
          OBJ_28.setText("Num\u00e9ro");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(150, 15, 51, 20);

          //---- LPNUM ----
          LPNUM.setComponentPopupMenu(BTDA);
          LPNUM.setName("LPNUM");
          panel1.add(LPNUM);
          LPNUM.setBounds(150, 40, 60, LPNUM.getPreferredSize().height);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTDA);
          INDETB.setName("INDETB");
          panel1.add(INDETB);
          INDETB.setBounds(45, 40, 44, INDETB.getPreferredSize().height);

          //---- LPSUF ----
          LPSUF.setComponentPopupMenu(BTDA);
          LPSUF.setName("LPSUF");
          panel1.add(LPSUF);
          LPSUF.setBounds(215, 40, 20, LPSUF.getPreferredSize().height);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Niveaux"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- LBNL1 ----
            LBNL1.setComponentPopupMenu(BTDA);
            LBNL1.setName("LBNL1");
            panel2.add(LBNL1);
            LBNL1.setBounds(30, 30, 34, LBNL1.getPreferredSize().height);

            //---- OBJ_36 ----
            OBJ_36.setText("1");
            OBJ_36.setName("OBJ_36");
            panel2.add(OBJ_36);
            OBJ_36.setBounds(15, 35, 12, 18);

            //---- OBJ_37 ----
            OBJ_37.setText("2");
            OBJ_37.setName("OBJ_37");
            panel2.add(OBJ_37);
            OBJ_37.setBounds(100, 35, 15, 18);

            //---- LBNL2 ----
            LBNL2.setComponentPopupMenu(BTDA);
            LBNL2.setName("LBNL2");
            panel2.add(LBNL2);
            LBNL2.setBounds(115, 30, 34, LBNL2.getPreferredSize().height);

            //---- LBNL3 ----
            LBNL3.setComponentPopupMenu(BTDA);
            LBNL3.setName("LBNL3");
            panel2.add(LBNL3);
            LBNL3.setBounds(200, 30, 34, LBNL3.getPreferredSize().height);

            //---- OBJ_38 ----
            OBJ_38.setText("3");
            OBJ_38.setName("OBJ_38");
            panel2.add(OBJ_38);
            OBJ_38.setBounds(185, 35, 12, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(275, 15, 260, 75);

          //---- LPQT3 ----
          LPQT3.setComponentPopupMenu(BTDA);
          LPQT3.setName("LPQT3");
          panel1.add(LPQT3);
          LPQT3.setBounds(150, 208, 114, LPQT3.getPreferredSize().height);

          //---- OBJ_56 ----
          OBJ_56.setText("Quantit\u00e9");
          OBJ_56.setName("OBJ_56");
          panel1.add(OBJ_56);
          OBJ_56.setBounds(45, 212, 52, 20);

          //---- OBJ_47 ----
          OBJ_47.setText("Date");
          OBJ_47.setName("OBJ_47");
          panel1.add(OBJ_47);
          OBJ_47.setBounds(45, 170, 33, 20);

          //---- LPDTEX ----
          LPDTEX.setComponentPopupMenu(BTDA);
          LPDTEX.setName("LPDTEX");
          panel1.add(LPDTEX);
          LPDTEX.setBounds(150, 166, 105, LPDTEX.getPreferredSize().height);

          //---- OBJ_57 ----
          OBJ_57.setText("Magasinier");
          OBJ_57.setName("OBJ_57");
          panel1.add(OBJ_57);
          OBJ_57.setBounds(45, 254, 70, 20);

          //---- LPVDE ----
          LPVDE.setComponentPopupMenu(BTD);
          LPVDE.setName("LPVDE");
          panel1.add(LPVDE);
          LPVDE.setBounds(150, 250, 44, LPVDE.getPreferredSize().height);

          //---- WSER_OBJ_50_35 ----
          WSER_OBJ_50_35.setText("Lot");
          WSER_OBJ_50_35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSER_OBJ_50_35.setName("WSER_OBJ_50_35");
          panel1.add(WSER_OBJ_50_35);
          WSER_OBJ_50_35.setBounds(302, 212, 80, 20);

          //---- WSER ----
          WSER.setText("S\u00e9rie");
          WSER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSER.setName("WSER");
          panel1.add(WSER);
          WSER.setBounds(420, 212, 80, 20);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- EBART ----
            EBART.setComponentPopupMenu(BTDA);
            EBART.setName("EBART");
            panel4.add(EBART);
            EBART.setBounds(15, 25, 214, EBART.getPreferredSize().height);

            //---- OBJ_44 ----
            OBJ_44.setText("Article");
            OBJ_44.setName("OBJ_44");
            panel4.add(OBJ_44);
            OBJ_44.setBounds(20, 5, 36, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel4);
          panel4.setBounds(30, 90, 480, 70);

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_42 ----
            OBJ_42.setText("Type");
            OBJ_42.setName("OBJ_42");
            panel3.add(OBJ_42);
            OBJ_42.setBounds(20, 0, 36, 20);

            //---- LPTYP ----
            LPTYP.setComponentPopupMenu(BTDA);
            LPTYP.setName("LPTYP");
            panel3.add(LPTYP);
            LPTYP.setBounds(15, 25, 34, LPTYP.getPreferredSize().height);

            //---- OBJ_43 ----
            OBJ_43.setText("Ordre");
            OBJ_43.setName("OBJ_43");
            panel3.add(OBJ_43);
            OBJ_43.setBounds(125, 0, 37, 20);

            //---- LBORD ----
            LBORD.setComponentPopupMenu(BTDA);
            LBORD.setName("LBORD");
            panel3.add(LBORD);
            LBORD.setBounds(120, 25, 214, LBORD.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel3);
          panel3.setBounds(30, 90, 480, 70);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 570, 300);

        //---- lbProrata ----
        lbProrata.setText("G\u00e9n\u00e9ration au prorata");
        lbProrata.setHorizontalAlignment(SwingConstants.RIGHT);
        lbProrata.setFont(lbProrata.getFont().deriveFont(lbProrata.getFont().getStyle() | Font.BOLD, lbProrata.getFont().getSize() + 3f));
        lbProrata.setName("lbProrata");
        p_contenu.add(lbProrata);
        lbProrata.setBounds(330, 0, 250, 35);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private XRiTextField LPNUM;
  private XRiTextField INDETB;
  private XRiTextField LPSUF;
  private JPanel panel2;
  private XRiTextField LBNL1;
  private JLabel OBJ_36;
  private JLabel OBJ_37;
  private XRiTextField LBNL2;
  private XRiTextField LBNL3;
  private JLabel OBJ_38;
  private XRiTextField LPQT3;
  private JLabel OBJ_56;
  private JLabel OBJ_47;
  private XRiCalendrier LPDTEX;
  private JLabel OBJ_57;
  private XRiTextField LPVDE;
  private JCheckBox WSER_OBJ_50_35;
  private JCheckBox WSER;
  private JPanel panel4;
  private XRiTextField EBART;
  private JLabel OBJ_44;
  private JPanel panel3;
  private JLabel OBJ_42;
  private XRiTextField LPTYP;
  private JLabel OBJ_43;
  private XRiTextField LBORD;
  private JLabel lbProrata;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
