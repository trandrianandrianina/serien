
package ri.serien.libecranrpg.vgpm.VGPM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGPM05FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGPM05FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator2.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("Année @WAN1@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator3.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("Année @WAN2@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI13R@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WAN3@")).trim());
    MOI01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI01@")).trim());
    MOI2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI02@")).trim());
    MOI03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI03@")).trim());
    MOI4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI04@")).trim());
    MOI05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI05@")).trim());
    MOI06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI06@")).trim());
    MOI07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI07@")).trim());
    MOI08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI08@")).trim());
    MOI09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI09@")).trim());
    MOI10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI10@")).trim());
    MOI11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI11@")).trim());
    MOI12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MOI12@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("SAISIE DES INTERVENANTS"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD1.getInvoker().getName());
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD1.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    INNOM = new XRiTextField();
    INCLK = new XRiTextField();
    OBJ_38 = new JLabel();
    OBJ_42 = new JLabel();
    INCOD = new XRiTextField();
    INMAT = new XRiTextField();
    OBJ_39 = new JLabel();
    INDETB = new XRiTextField();
    INETBP = new XRiTextField();
    OBJ_44 = new JLabel();
    OBJ_40 = new JLabel();
    separator4 = compFactory.createSeparator("Paie");
    panel2 = new JPanel();
    OBJ_46 = new JLabel();
    OBJ_47 = new JLabel();
    INV25 = new XRiTextField();
    MOI01 = new JLabel();
    INV01 = new XRiTextField();
    INV02 = new XRiTextField();
    MOI2 = new JLabel();
    MOI03 = new JLabel();
    INV03 = new XRiTextField();
    MOI4 = new JLabel();
    INV04 = new XRiTextField();
    MOI05 = new JLabel();
    INV05 = new XRiTextField();
    INV06 = new XRiTextField();
    MOI06 = new JLabel();
    MOI07 = new JLabel();
    INV07 = new XRiTextField();
    MOI08 = new JLabel();
    INV08 = new XRiTextField();
    MOI09 = new JLabel();
    INV09 = new XRiTextField();
    MOI10 = new JLabel();
    INV10 = new XRiTextField();
    MOI11 = new JLabel();
    INV11 = new XRiTextField();
    MOI12 = new JLabel();
    INV12 = new XRiTextField();
    INV13 = new XRiTextField();
    INV14 = new XRiTextField();
    INV15 = new XRiTextField();
    INV16 = new XRiTextField();
    INV17 = new XRiTextField();
    INV18 = new XRiTextField();
    INV19 = new XRiTextField();
    INV20 = new XRiTextField();
    INV21 = new XRiTextField();
    INV22 = new XRiTextField();
    INV23 = new XRiTextField();
    INV24 = new XRiTextField();
    separator1 = compFactory.createSeparator("Mois");
    separator2 = compFactory.createSeparator("Ann\u00e9e @WAN1@");
    separator3 = compFactory.createSeparator("Ann\u00e9e @WAN2@");
    BTD1 = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(735, 680));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Intervenant"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- INNOM ----
          INNOM.setComponentPopupMenu(BTD);
          INNOM.setName("INNOM");
          panel1.add(INNOM);
          INNOM.setBounds(185, 65, 310, INNOM.getPreferredSize().height);

          //---- INCLK ----
          INCLK.setComponentPopupMenu(BTD);
          INCLK.setName("INCLK");
          panel1.add(INCLK);
          INCLK.setBounds(135, 100, 160, INCLK.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setText("Etablissement");
          OBJ_38.setName("OBJ_38");
          panel1.add(OBJ_38);
          OBJ_38.setBounds(30, 34, 100, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("Classement");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(30, 104, 76, 20);

          //---- INCOD ----
          INCOD.setComponentPopupMenu(BTD);
          INCOD.setName("INCOD");
          panel1.add(INCOD);
          INCOD.setBounds(135, 65, 42, INCOD.getPreferredSize().height);

          //---- INMAT ----
          INMAT.setComponentPopupMenu(BTD);
          INMAT.setName("INMAT");
          panel1.add(INMAT);
          INMAT.setBounds(305, 171, 42, INMAT.getPreferredSize().height);

          //---- OBJ_39 ----
          OBJ_39.setText("Code");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(30, 69, 41, 20);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          panel1.add(INDETB);
          INDETB.setBounds(135, 30, 40, INDETB.getPreferredSize().height);

          //---- INETBP ----
          INETBP.setComponentPopupMenu(BTD);
          INETBP.setName("INETBP");
          panel1.add(INETBP);
          INETBP.setBounds(135, 171, 40, INETBP.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Matricule");
          OBJ_44.setName("OBJ_44");
          panel1.add(OBJ_44);
          OBJ_44.setBounds(225, 175, 70, 20);

          //---- OBJ_40 ----
          OBJ_40.setText("Etablissement");
          OBJ_40.setName("OBJ_40");
          panel1.add(OBJ_40);
          OBJ_40.setBounds(30, 175, 100, 20);

          //---- separator4 ----
          separator4.setName("separator4");
          panel1.add(separator4);
          separator4.setBounds(20, 145, 500, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 545, 225);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Co\u00fbts horaires"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_46 ----
          OBJ_46.setText("@MOI13R@");
          OBJ_46.setName("OBJ_46");
          panel2.add(OBJ_46);
          OBJ_46.setBounds(35, 385, 100, 20);

          //---- OBJ_47 ----
          OBJ_47.setText("@WAN3@");
          OBJ_47.setName("OBJ_47");
          panel2.add(OBJ_47);
          OBJ_47.setBounds(140, 385, 30, 20);

          //---- INV25 ----
          INV25.setComponentPopupMenu(BTD);
          INV25.setName("INV25");
          panel2.add(INV25);
          INV25.setBounds(175, 380, 106, INV25.getPreferredSize().height);

          //---- MOI01 ----
          MOI01.setText("@MOI01@");
          MOI01.setName("MOI01");
          panel2.add(MOI01);
          MOI01.setBounds(35, 54, 140, 20);

          //---- INV01 ----
          INV01.setComponentPopupMenu(BTD);
          INV01.setName("INV01");
          panel2.add(INV01);
          INV01.setBounds(175, 50, 106, INV01.getPreferredSize().height);

          //---- INV02 ----
          INV02.setComponentPopupMenu(BTD);
          INV02.setName("INV02");
          panel2.add(INV02);
          INV02.setBounds(175, 77, 106, INV02.getPreferredSize().height);

          //---- MOI2 ----
          MOI2.setText("@MOI02@");
          MOI2.setName("MOI2");
          panel2.add(MOI2);
          MOI2.setBounds(35, 81, 140, 20);

          //---- MOI03 ----
          MOI03.setText("@MOI03@");
          MOI03.setName("MOI03");
          panel2.add(MOI03);
          MOI03.setBounds(35, 108, 140, 20);

          //---- INV03 ----
          INV03.setComponentPopupMenu(BTD);
          INV03.setName("INV03");
          panel2.add(INV03);
          INV03.setBounds(175, 104, 106, INV03.getPreferredSize().height);

          //---- MOI4 ----
          MOI4.setText("@MOI04@");
          MOI4.setName("MOI4");
          panel2.add(MOI4);
          MOI4.setBounds(35, 135, 140, 20);

          //---- INV04 ----
          INV04.setComponentPopupMenu(BTD);
          INV04.setName("INV04");
          panel2.add(INV04);
          INV04.setBounds(175, 131, 106, INV04.getPreferredSize().height);

          //---- MOI05 ----
          MOI05.setText("@MOI05@");
          MOI05.setName("MOI05");
          panel2.add(MOI05);
          MOI05.setBounds(35, 162, 140, 20);

          //---- INV05 ----
          INV05.setComponentPopupMenu(BTD);
          INV05.setName("INV05");
          panel2.add(INV05);
          INV05.setBounds(175, 158, 106, INV05.getPreferredSize().height);

          //---- INV06 ----
          INV06.setComponentPopupMenu(BTD);
          INV06.setName("INV06");
          panel2.add(INV06);
          INV06.setBounds(175, 185, 106, INV06.getPreferredSize().height);

          //---- MOI06 ----
          MOI06.setText("@MOI06@");
          MOI06.setName("MOI06");
          panel2.add(MOI06);
          MOI06.setBounds(35, 189, 140, 20);

          //---- MOI07 ----
          MOI07.setText("@MOI07@");
          MOI07.setName("MOI07");
          panel2.add(MOI07);
          MOI07.setBounds(35, 216, 140, 20);

          //---- INV07 ----
          INV07.setComponentPopupMenu(BTD);
          INV07.setName("INV07");
          panel2.add(INV07);
          INV07.setBounds(175, 212, 106, INV07.getPreferredSize().height);

          //---- MOI08 ----
          MOI08.setText("@MOI08@");
          MOI08.setName("MOI08");
          panel2.add(MOI08);
          MOI08.setBounds(35, 243, 140, 20);

          //---- INV08 ----
          INV08.setComponentPopupMenu(BTD);
          INV08.setName("INV08");
          panel2.add(INV08);
          INV08.setBounds(175, 239, 106, INV08.getPreferredSize().height);

          //---- MOI09 ----
          MOI09.setText("@MOI09@");
          MOI09.setName("MOI09");
          panel2.add(MOI09);
          MOI09.setBounds(35, 270, 140, 20);

          //---- INV09 ----
          INV09.setComponentPopupMenu(BTD);
          INV09.setName("INV09");
          panel2.add(INV09);
          INV09.setBounds(175, 266, 106, INV09.getPreferredSize().height);

          //---- MOI10 ----
          MOI10.setText("@MOI10@");
          MOI10.setName("MOI10");
          panel2.add(MOI10);
          MOI10.setBounds(35, 297, 140, 20);

          //---- INV10 ----
          INV10.setComponentPopupMenu(BTD);
          INV10.setName("INV10");
          panel2.add(INV10);
          INV10.setBounds(175, 293, 106, INV10.getPreferredSize().height);

          //---- MOI11 ----
          MOI11.setText("@MOI11@");
          MOI11.setName("MOI11");
          panel2.add(MOI11);
          MOI11.setBounds(35, 324, 140, 20);

          //---- INV11 ----
          INV11.setComponentPopupMenu(BTD);
          INV11.setName("INV11");
          panel2.add(INV11);
          INV11.setBounds(175, 320, 106, INV11.getPreferredSize().height);

          //---- MOI12 ----
          MOI12.setText("@MOI12@");
          MOI12.setName("MOI12");
          panel2.add(MOI12);
          MOI12.setBounds(35, 351, 140, 20);

          //---- INV12 ----
          INV12.setComponentPopupMenu(BTD);
          INV12.setName("INV12");
          panel2.add(INV12);
          INV12.setBounds(175, 347, 106, INV12.getPreferredSize().height);

          //---- INV13 ----
          INV13.setComponentPopupMenu(BTD);
          INV13.setName("INV13");
          panel2.add(INV13);
          INV13.setBounds(330, 50, 106, INV13.getPreferredSize().height);

          //---- INV14 ----
          INV14.setComponentPopupMenu(BTD);
          INV14.setName("INV14");
          panel2.add(INV14);
          INV14.setBounds(330, 77, 106, INV14.getPreferredSize().height);

          //---- INV15 ----
          INV15.setComponentPopupMenu(BTD);
          INV15.setName("INV15");
          panel2.add(INV15);
          INV15.setBounds(330, 104, 106, INV15.getPreferredSize().height);

          //---- INV16 ----
          INV16.setComponentPopupMenu(BTD);
          INV16.setName("INV16");
          panel2.add(INV16);
          INV16.setBounds(330, 131, 106, INV16.getPreferredSize().height);

          //---- INV17 ----
          INV17.setComponentPopupMenu(BTD);
          INV17.setName("INV17");
          panel2.add(INV17);
          INV17.setBounds(330, 158, 106, INV17.getPreferredSize().height);

          //---- INV18 ----
          INV18.setComponentPopupMenu(BTD);
          INV18.setName("INV18");
          panel2.add(INV18);
          INV18.setBounds(330, 185, 106, INV18.getPreferredSize().height);

          //---- INV19 ----
          INV19.setComponentPopupMenu(BTD);
          INV19.setName("INV19");
          panel2.add(INV19);
          INV19.setBounds(330, 212, 106, INV19.getPreferredSize().height);

          //---- INV20 ----
          INV20.setComponentPopupMenu(BTD);
          INV20.setName("INV20");
          panel2.add(INV20);
          INV20.setBounds(330, 239, 106, INV20.getPreferredSize().height);

          //---- INV21 ----
          INV21.setComponentPopupMenu(BTD);
          INV21.setName("INV21");
          panel2.add(INV21);
          INV21.setBounds(330, 266, 106, INV21.getPreferredSize().height);

          //---- INV22 ----
          INV22.setComponentPopupMenu(BTD);
          INV22.setName("INV22");
          panel2.add(INV22);
          INV22.setBounds(330, 293, 106, INV22.getPreferredSize().height);

          //---- INV23 ----
          INV23.setComponentPopupMenu(BTD);
          INV23.setName("INV23");
          panel2.add(INV23);
          INV23.setBounds(330, 320, 106, INV23.getPreferredSize().height);

          //---- INV24 ----
          INV24.setComponentPopupMenu(BTD);
          INV24.setName("INV24");
          panel2.add(INV24);
          INV24.setBounds(330, 347, 106, INV24.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          panel2.add(separator1);
          separator1.setBounds(35, 30, 125, 20);

          //---- separator2 ----
          separator2.setName("separator2");
          panel2.add(separator2);
          separator2.setBounds(175, 30, 106, 20);

          //---- separator3 ----
          separator3.setName("separator3");
          panel2.add(separator3);
          separator3.setBounds(330, 30, 106, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 245, 545, 425);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD1 ========
    {
      BTD1.setName("BTD1");

      //---- OBJ_16 ----
      OBJ_16.setText("Choix possibles");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD1.add(OBJ_16);

      //---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD1.add(OBJ_15);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField INNOM;
  private XRiTextField INCLK;
  private JLabel OBJ_38;
  private JLabel OBJ_42;
  private XRiTextField INCOD;
  private XRiTextField INMAT;
  private JLabel OBJ_39;
  private XRiTextField INDETB;
  private XRiTextField INETBP;
  private JLabel OBJ_44;
  private JLabel OBJ_40;
  private JComponent separator4;
  private JPanel panel2;
  private JLabel OBJ_46;
  private JLabel OBJ_47;
  private XRiTextField INV25;
  private JLabel MOI01;
  private XRiTextField INV01;
  private XRiTextField INV02;
  private JLabel MOI2;
  private JLabel MOI03;
  private XRiTextField INV03;
  private JLabel MOI4;
  private XRiTextField INV04;
  private JLabel MOI05;
  private XRiTextField INV05;
  private XRiTextField INV06;
  private JLabel MOI06;
  private JLabel MOI07;
  private XRiTextField INV07;
  private JLabel MOI08;
  private XRiTextField INV08;
  private JLabel MOI09;
  private XRiTextField INV09;
  private JLabel MOI10;
  private XRiTextField INV10;
  private JLabel MOI11;
  private XRiTextField INV11;
  private JLabel MOI12;
  private XRiTextField INV12;
  private XRiTextField INV13;
  private XRiTextField INV14;
  private XRiTextField INV15;
  private XRiTextField INV16;
  private XRiTextField INV17;
  private XRiTextField INV18;
  private XRiTextField INV19;
  private XRiTextField INV20;
  private XRiTextField INV21;
  private XRiTextField INV22;
  private XRiTextField INV23;
  private XRiTextField INV24;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  private JPopupMenu BTD1;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_15;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
