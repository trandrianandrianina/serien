
package ri.serien.libecranrpg.vgpm.VGPM03CP;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGPM03CP_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGPM03CP_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    CPREG.setValeursSelection("1", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@PG01LI@")).trim()));
    CPLZ1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPLZ1@")).trim());
    CPLZ2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPLZ2@")).trim());
    CPLZ3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPLZ3@")).trim());
    CPLZ4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPLZ4@")).trim());
    CPLZ5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPLZ5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_69 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_71 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_73 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    CPETC = new XRiTextField();
    lbEtbComptable = new JLabel();
    CPREG = new XRiCheckBox();
    lbJournal = new JLabel();
    lbFolio = new JLabel();
    CPCJO = new XRiTextField();
    CPCFO = new XRiTextField();
    CPLZ1 = new RiZoneSortie();
    lbLibelle = new JLabel();
    lbDebit = new JLabel();
    lbCredit = new JLabel();
    CPCO1D = new XRiTextField();
    CPCO1C = new XRiTextField();
    CPLZ2 = new RiZoneSortie();
    CPCO2D = new XRiTextField();
    CPCO2C = new XRiTextField();
    CPLZ3 = new RiZoneSortie();
    CPCO3D = new XRiTextField();
    CPCO3C = new XRiTextField();
    CPLZ4 = new RiZoneSortie();
    CPCO4D = new XRiTextField();
    CPCO4C = new XRiTextField();
    CPLZ5 = new RiZoneSortie();
    CPCO5D = new XRiTextField();
    CPCO5C = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la production");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_69 ----
          OBJ_69.setText("Etablissement");
          OBJ_69.setName("OBJ_69");
          p_tete_gauche.add(OBJ_69);
          OBJ_69.setBounds(5, 5, 93, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(100, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_71 ----
          OBJ_71.setText("Code");
          OBJ_71.setName("OBJ_71");
          p_tete_gauche.add(OBJ_71);
          OBJ_71.setBounds(160, 5, 36, 18);

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");
          p_tete_gauche.add(INDTYP);
          INDTYP.setBounds(200, 0, 30, INDTYP.getPreferredSize().height);

          //---- OBJ_73 ----
          OBJ_73.setText("Ordre");
          OBJ_73.setName("OBJ_73");
          p_tete_gauche.add(OBJ_73);
          OBJ_73.setBounds(250, 5, 39, 18);

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");
          p_tete_gauche.add(INDIND);
          INDIND.setBounds(300, 0, 60, INDIND.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new BorderLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(410, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(new GridBagLayout());
          ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {1019, 0};
          ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {395, 0, 0};
          ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("@PG01LI@"));
            panel1.setOpaque(false);
            panel1.setMinimumSize(new Dimension(389, 380));
            panel1.setPreferredSize(new Dimension(389, 380));
            panel1.setName("panel1");
            panel1.setLayout(new GridBagLayout());
            ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {178, 76, 69, 64, 0};
            ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {0, 0, 0, 35, 0, 0, 0, 0, 0, 34, 0};
            ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- CPETC ----
            CPETC.setComponentPopupMenu(BTD);
            CPETC.setMinimumSize(new Dimension(40, 28));
            CPETC.setPreferredSize(new Dimension(40, 28));
            CPETC.setName("CPETC");
            panel1.add(CPETC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbEtbComptable ----
            lbEtbComptable.setText("Etablissement comptable");
            lbEtbComptable.setName("lbEtbComptable");
            panel1.add(lbEtbComptable, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPREG ----
            CPREG.setText("Regroupement des lignes comptables");
            CPREG.setMaximumSize(new Dimension(290, 30));
            CPREG.setMinimumSize(new Dimension(290, 30));
            CPREG.setPreferredSize(new Dimension(290, 30));
            CPREG.setName("CPREG");
            panel1.add(CPREG, new GridBagConstraints(0, 9, 3, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbJournal ----
            lbJournal.setText("Code journal");
            lbJournal.setName("lbJournal");
            panel1.add(lbJournal, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbFolio ----
            lbFolio.setText("Code folio");
            lbFolio.setName("lbFolio");
            panel1.add(lbFolio, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPCJO ----
            CPCJO.setComponentPopupMenu(BTD);
            CPCJO.setPreferredSize(new Dimension(35, 28));
            CPCJO.setMinimumSize(new Dimension(35, 28));
            CPCJO.setName("CPCJO");
            panel1.add(CPCJO, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPCFO ----
            CPCFO.setComponentPopupMenu(BTD);
            CPCFO.setMinimumSize(new Dimension(50, 28));
            CPCFO.setPreferredSize(new Dimension(50, 28));
            CPCFO.setName("CPCFO");
            panel1.add(CPCFO, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPLZ1 ----
            CPLZ1.setText("@CPLZ1@");
            CPLZ1.setPreferredSize(new Dimension(210, 24));
            CPLZ1.setMinimumSize(new Dimension(210, 24));
            CPLZ1.setMaximumSize(new Dimension(210, 24));
            CPLZ1.setName("CPLZ1");
            panel1.add(CPLZ1, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLibelle ----
            lbLibelle.setText("Libell\u00e9");
            lbLibelle.setName("lbLibelle");
            panel1.add(lbLibelle, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbDebit ----
            lbDebit.setText("D\u00e9bit");
            lbDebit.setHorizontalAlignment(SwingConstants.CENTER);
            lbDebit.setName("lbDebit");
            panel1.add(lbDebit, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbCredit ----
            lbCredit.setText("Cr\u00e9dit");
            lbCredit.setHorizontalAlignment(SwingConstants.CENTER);
            lbCredit.setName("lbCredit");
            panel1.add(lbCredit, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- CPCO1D ----
            CPCO1D.setComponentPopupMenu(BTD);
            CPCO1D.setPreferredSize(new Dimension(40, 28));
            CPCO1D.setMinimumSize(new Dimension(40, 28));
            CPCO1D.setName("CPCO1D");
            panel1.add(CPCO1D, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPCO1C ----
            CPCO1C.setComponentPopupMenu(BTD);
            CPCO1C.setMinimumSize(new Dimension(40, 28));
            CPCO1C.setPreferredSize(new Dimension(40, 28));
            CPCO1C.setName("CPCO1C");
            panel1.add(CPCO1C, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- CPLZ2 ----
            CPLZ2.setText("@CPLZ2@");
            CPLZ2.setMaximumSize(new Dimension(210, 24));
            CPLZ2.setMinimumSize(new Dimension(210, 24));
            CPLZ2.setPreferredSize(new Dimension(210, 24));
            CPLZ2.setName("CPLZ2");
            panel1.add(CPLZ2, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPCO2D ----
            CPCO2D.setComponentPopupMenu(BTD);
            CPCO2D.setMinimumSize(new Dimension(40, 28));
            CPCO2D.setPreferredSize(new Dimension(40, 28));
            CPCO2D.setName("CPCO2D");
            panel1.add(CPCO2D, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPCO2C ----
            CPCO2C.setComponentPopupMenu(BTD);
            CPCO2C.setMinimumSize(new Dimension(40, 28));
            CPCO2C.setPreferredSize(new Dimension(40, 28));
            CPCO2C.setName("CPCO2C");
            panel1.add(CPCO2C, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- CPLZ3 ----
            CPLZ3.setText("@CPLZ3@");
            CPLZ3.setMaximumSize(new Dimension(210, 24));
            CPLZ3.setMinimumSize(new Dimension(210, 24));
            CPLZ3.setPreferredSize(new Dimension(210, 24));
            CPLZ3.setName("CPLZ3");
            panel1.add(CPLZ3, new GridBagConstraints(0, 6, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPCO3D ----
            CPCO3D.setComponentPopupMenu(BTD);
            CPCO3D.setMinimumSize(new Dimension(40, 28));
            CPCO3D.setPreferredSize(new Dimension(40, 28));
            CPCO3D.setName("CPCO3D");
            panel1.add(CPCO3D, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPCO3C ----
            CPCO3C.setComponentPopupMenu(BTD);
            CPCO3C.setMinimumSize(new Dimension(40, 28));
            CPCO3C.setPreferredSize(new Dimension(40, 28));
            CPCO3C.setName("CPCO3C");
            panel1.add(CPCO3C, new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- CPLZ4 ----
            CPLZ4.setText("@CPLZ4@");
            CPLZ4.setMaximumSize(new Dimension(210, 24));
            CPLZ4.setMinimumSize(new Dimension(210, 24));
            CPLZ4.setPreferredSize(new Dimension(210, 24));
            CPLZ4.setName("CPLZ4");
            panel1.add(CPLZ4, new GridBagConstraints(0, 7, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPCO4D ----
            CPCO4D.setComponentPopupMenu(BTD);
            CPCO4D.setMinimumSize(new Dimension(40, 28));
            CPCO4D.setPreferredSize(new Dimension(40, 28));
            CPCO4D.setName("CPCO4D");
            panel1.add(CPCO4D, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPCO4C ----
            CPCO4C.setComponentPopupMenu(BTD);
            CPCO4C.setMinimumSize(new Dimension(40, 28));
            CPCO4C.setPreferredSize(new Dimension(40, 28));
            CPCO4C.setName("CPCO4C");
            panel1.add(CPCO4C, new GridBagConstraints(3, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- CPLZ5 ----
            CPLZ5.setText("@CPLZ5@");
            CPLZ5.setMaximumSize(new Dimension(210, 24));
            CPLZ5.setMinimumSize(new Dimension(210, 24));
            CPLZ5.setPreferredSize(new Dimension(210, 24));
            CPLZ5.setName("CPLZ5");
            panel1.add(CPLZ5, new GridBagConstraints(0, 8, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPCO5D ----
            CPCO5D.setComponentPopupMenu(BTD);
            CPCO5D.setMinimumSize(new Dimension(40, 28));
            CPCO5D.setPreferredSize(new Dimension(40, 28));
            CPCO5D.setName("CPCO5D");
            panel1.add(CPCO5D, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CPCO5C ----
            CPCO5C.setComponentPopupMenu(BTD);
            CPCO5C.setMinimumSize(new Dimension(40, 28));
            CPCO5C.setPreferredSize(new Dimension(40, 28));
            CPCO5C.setName("CPCO5C");
            panel1.add(CPCO5C, new GridBagConstraints(3, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 0), 0, 0));
          }
          p_contenu.add(panel1, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0,
            GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
            new Insets(10, 10, 15, 10), 0, 0));
        }
        p_centrage.add(p_contenu, BorderLayout.CENTER);
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_69;
  private XRiTextField INDETB;
  private JLabel OBJ_71;
  private XRiTextField INDTYP;
  private JLabel OBJ_73;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField CPETC;
  private JLabel lbEtbComptable;
  private XRiCheckBox CPREG;
  private JLabel lbJournal;
  private JLabel lbFolio;
  private XRiTextField CPCJO;
  private XRiTextField CPCFO;
  private RiZoneSortie CPLZ1;
  private JLabel lbLibelle;
  private JLabel lbDebit;
  private JLabel lbCredit;
  private XRiTextField CPCO1D;
  private XRiTextField CPCO1C;
  private RiZoneSortie CPLZ2;
  private XRiTextField CPCO2D;
  private XRiTextField CPCO2C;
  private RiZoneSortie CPLZ3;
  private XRiTextField CPCO3D;
  private XRiTextField CPCO3C;
  private RiZoneSortie CPLZ4;
  private XRiTextField CPCO4D;
  private XRiTextField CPCO4C;
  private RiZoneSortie CPLZ5;
  private XRiTextField CPCO5D;
  private XRiTextField CPCO5C;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
