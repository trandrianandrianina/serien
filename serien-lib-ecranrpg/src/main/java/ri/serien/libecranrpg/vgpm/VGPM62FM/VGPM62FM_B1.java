
package ri.serien.libecranrpg.vgpm.VGPM62FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGPM62FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGPM62FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    ATLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATLIB@")).trim());
    LIB01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB01@")).trim());
    LIB02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB02@")).trim());
    LIB03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB03@")).trim());
    LIB04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB04@")).trim());
    LIB05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB05@")).trim());
    LIB06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB06@")).trim());
    LIB07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB07@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_49 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_50 = new JLabel();
    INDATE = new XRiTextField();
    OBJ_57 = new JLabel();
    INDANN = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_75 = new JLabel();
    ATLIB = new RiZoneSortie();
    LIB01 = new RiZoneSortie();
    LIB02 = new RiZoneSortie();
    LIB03 = new RiZoneSortie();
    LIB04 = new RiZoneSortie();
    LIB05 = new RiZoneSortie();
    LIB06 = new RiZoneSortie();
    LIB07 = new RiZoneSortie();
    ROH01 = new XRiTextField();
    ROH02 = new XRiTextField();
    ROH03 = new XRiTextField();
    ROH04 = new XRiTextField();
    ROH05 = new XRiTextField();
    ROH06 = new XRiTextField();
    ROH07 = new XRiTextField();
    ROH08 = new XRiTextField();
    ROH09 = new XRiTextField();
    ROH10 = new XRiTextField();
    ROH11 = new XRiTextField();
    ROH12 = new XRiTextField();
    ROH13 = new XRiTextField();
    ROH14 = new XRiTextField();
    ROH15 = new XRiTextField();
    ROH16 = new XRiTextField();
    ROH17 = new XRiTextField();
    ROH18 = new XRiTextField();
    ROH19 = new XRiTextField();
    ROH20 = new XRiTextField();
    ROH21 = new XRiTextField();
    ROH22 = new XRiTextField();
    ROH23 = new XRiTextField();
    ROH24 = new XRiTextField();
    ROH25 = new XRiTextField();
    ROH26 = new XRiTextField();
    ROH27 = new XRiTextField();
    ROH28 = new XRiTextField();
    ROH29 = new XRiTextField();
    ROH30 = new XRiTextField();
    ROH31 = new XRiTextField();
    ROH32 = new XRiTextField();
    ROH33 = new XRiTextField();
    ROH34 = new XRiTextField();
    ROH35 = new XRiTextField();
    ROH36 = new XRiTextField();
    ROH37 = new XRiTextField();
    ROH38 = new XRiTextField();
    ROH39 = new XRiTextField();
    ROH40 = new XRiTextField();
    ROH41 = new XRiTextField();
    ROH42 = new XRiTextField();
    ROH43 = new XRiTextField();
    ROH44 = new XRiTextField();
    ROH45 = new XRiTextField();
    ROH46 = new XRiTextField();
    ROH47 = new XRiTextField();
    ROH48 = new XRiTextField();
    ROH49 = new XRiTextField();
    ROH50 = new XRiTextField();
    ROH51 = new XRiTextField();
    ROH52 = new XRiTextField();
    separator1 = compFactory.createSeparator("Codes horaires", SwingConstants.CENTER);
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des roulements");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_49 ----
          OBJ_49.setText("Etablissement");
          OBJ_49.setName("OBJ_49");
          p_tete_gauche.add(OBJ_49);
          OBJ_49.setBounds(5, 5, 120, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(130, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_50 ----
          OBJ_50.setText("Atelier");
          OBJ_50.setName("OBJ_50");
          p_tete_gauche.add(OBJ_50);
          OBJ_50.setBounds(195, 5, 80, 18);

          //---- INDATE ----
          INDATE.setComponentPopupMenu(BTD);
          INDATE.setName("INDATE");
          p_tete_gauche.add(INDATE);
          INDATE.setBounds(280, 0, 60, INDATE.getPreferredSize().height);

          //---- OBJ_57 ----
          OBJ_57.setText("Ann\u00e9e");
          OBJ_57.setName("OBJ_57");
          p_tete_gauche.add(OBJ_57);
          OBJ_57.setBounds(395, 5, 80, 18);

          //---- INDANN ----
          INDANN.setComponentPopupMenu(BTD);
          INDANN.setName("INDANN");
          p_tete_gauche.add(INDANN);
          INDANN.setBounds(490, 0, 42, INDANN.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(860, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("D\u00e9tail des codes horaires"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_75 ----
            OBJ_75.setText("Libell\u00e9 de l'atelier");
            OBJ_75.setName("OBJ_75");
            panel1.add(OBJ_75);
            OBJ_75.setBounds(25, 42, 180, 25);

            //---- ATLIB ----
            ATLIB.setText("@ATLIB@");
            ATLIB.setName("ATLIB");
            panel1.add(ATLIB);
            ATLIB.setBounds(210, 42, 318, ATLIB.getPreferredSize().height);

            //---- LIB01 ----
            LIB01.setText("@LIB01@");
            LIB01.setName("LIB01");
            panel1.add(LIB01);
            LIB01.setBounds(25, 120, 310, LIB01.getPreferredSize().height);

            //---- LIB02 ----
            LIB02.setText("@LIB02@");
            LIB02.setName("LIB02");
            panel1.add(LIB02);
            LIB02.setBounds(25, 160, 310, LIB02.getPreferredSize().height);

            //---- LIB03 ----
            LIB03.setText("@LIB03@");
            LIB03.setName("LIB03");
            panel1.add(LIB03);
            LIB03.setBounds(25, 200, 310, LIB03.getPreferredSize().height);

            //---- LIB04 ----
            LIB04.setText("@LIB04@");
            LIB04.setName("LIB04");
            panel1.add(LIB04);
            LIB04.setBounds(25, 240, 310, LIB04.getPreferredSize().height);

            //---- LIB05 ----
            LIB05.setText("@LIB05@");
            LIB05.setName("LIB05");
            panel1.add(LIB05);
            LIB05.setBounds(25, 280, 310, LIB05.getPreferredSize().height);

            //---- LIB06 ----
            LIB06.setText("@LIB06@");
            LIB06.setName("LIB06");
            panel1.add(LIB06);
            LIB06.setBounds(25, 320, 310, LIB06.getPreferredSize().height);

            //---- LIB07 ----
            LIB07.setText("@LIB07@");
            LIB07.setName("LIB07");
            panel1.add(LIB07);
            LIB07.setBounds(25, 360, 310, LIB07.getPreferredSize().height);

            //---- ROH01 ----
            ROH01.setName("ROH01");
            panel1.add(ROH01);
            ROH01.setBounds(345, 118, 50, ROH01.getPreferredSize().height);

            //---- ROH02 ----
            ROH02.setName("ROH02");
            panel1.add(ROH02);
            ROH02.setBounds(402, 118, 50, ROH02.getPreferredSize().height);

            //---- ROH03 ----
            ROH03.setName("ROH03");
            panel1.add(ROH03);
            ROH03.setBounds(459, 118, 50, ROH03.getPreferredSize().height);

            //---- ROH04 ----
            ROH04.setName("ROH04");
            panel1.add(ROH04);
            ROH04.setBounds(516, 118, 50, ROH04.getPreferredSize().height);

            //---- ROH05 ----
            ROH05.setName("ROH05");
            panel1.add(ROH05);
            ROH05.setBounds(573, 118, 50, ROH05.getPreferredSize().height);

            //---- ROH06 ----
            ROH06.setName("ROH06");
            panel1.add(ROH06);
            ROH06.setBounds(630, 118, 50, ROH06.getPreferredSize().height);

            //---- ROH07 ----
            ROH07.setName("ROH07");
            panel1.add(ROH07);
            ROH07.setBounds(687, 118, 50, ROH07.getPreferredSize().height);

            //---- ROH08 ----
            ROH08.setName("ROH08");
            panel1.add(ROH08);
            ROH08.setBounds(744, 118, 50, ROH08.getPreferredSize().height);

            //---- ROH09 ----
            ROH09.setName("ROH09");
            panel1.add(ROH09);
            ROH09.setBounds(345, 158, 50, ROH09.getPreferredSize().height);

            //---- ROH10 ----
            ROH10.setName("ROH10");
            panel1.add(ROH10);
            ROH10.setBounds(402, 158, 50, ROH10.getPreferredSize().height);

            //---- ROH11 ----
            ROH11.setName("ROH11");
            panel1.add(ROH11);
            ROH11.setBounds(459, 158, 50, ROH11.getPreferredSize().height);

            //---- ROH12 ----
            ROH12.setName("ROH12");
            panel1.add(ROH12);
            ROH12.setBounds(516, 158, 50, ROH12.getPreferredSize().height);

            //---- ROH13 ----
            ROH13.setName("ROH13");
            panel1.add(ROH13);
            ROH13.setBounds(573, 158, 50, ROH13.getPreferredSize().height);

            //---- ROH14 ----
            ROH14.setName("ROH14");
            panel1.add(ROH14);
            ROH14.setBounds(630, 158, 50, ROH14.getPreferredSize().height);

            //---- ROH15 ----
            ROH15.setName("ROH15");
            panel1.add(ROH15);
            ROH15.setBounds(687, 158, 50, ROH15.getPreferredSize().height);

            //---- ROH16 ----
            ROH16.setName("ROH16");
            panel1.add(ROH16);
            ROH16.setBounds(744, 158, 50, ROH16.getPreferredSize().height);

            //---- ROH17 ----
            ROH17.setName("ROH17");
            panel1.add(ROH17);
            ROH17.setBounds(345, 198, 50, ROH17.getPreferredSize().height);

            //---- ROH18 ----
            ROH18.setName("ROH18");
            panel1.add(ROH18);
            ROH18.setBounds(402, 198, 50, ROH18.getPreferredSize().height);

            //---- ROH19 ----
            ROH19.setName("ROH19");
            panel1.add(ROH19);
            ROH19.setBounds(459, 198, 50, ROH19.getPreferredSize().height);

            //---- ROH20 ----
            ROH20.setName("ROH20");
            panel1.add(ROH20);
            ROH20.setBounds(516, 198, 50, ROH20.getPreferredSize().height);

            //---- ROH21 ----
            ROH21.setName("ROH21");
            panel1.add(ROH21);
            ROH21.setBounds(573, 198, 50, ROH21.getPreferredSize().height);

            //---- ROH22 ----
            ROH22.setName("ROH22");
            panel1.add(ROH22);
            ROH22.setBounds(630, 198, 50, ROH22.getPreferredSize().height);

            //---- ROH23 ----
            ROH23.setName("ROH23");
            panel1.add(ROH23);
            ROH23.setBounds(687, 198, 50, ROH23.getPreferredSize().height);

            //---- ROH24 ----
            ROH24.setName("ROH24");
            panel1.add(ROH24);
            ROH24.setBounds(744, 198, 50, ROH24.getPreferredSize().height);

            //---- ROH25 ----
            ROH25.setName("ROH25");
            panel1.add(ROH25);
            ROH25.setBounds(345, 238, 50, ROH25.getPreferredSize().height);

            //---- ROH26 ----
            ROH26.setName("ROH26");
            panel1.add(ROH26);
            ROH26.setBounds(402, 238, 50, ROH26.getPreferredSize().height);

            //---- ROH27 ----
            ROH27.setName("ROH27");
            panel1.add(ROH27);
            ROH27.setBounds(459, 238, 50, ROH27.getPreferredSize().height);

            //---- ROH28 ----
            ROH28.setName("ROH28");
            panel1.add(ROH28);
            ROH28.setBounds(516, 238, 50, ROH28.getPreferredSize().height);

            //---- ROH29 ----
            ROH29.setName("ROH29");
            panel1.add(ROH29);
            ROH29.setBounds(573, 238, 50, ROH29.getPreferredSize().height);

            //---- ROH30 ----
            ROH30.setName("ROH30");
            panel1.add(ROH30);
            ROH30.setBounds(630, 238, 50, ROH30.getPreferredSize().height);

            //---- ROH31 ----
            ROH31.setName("ROH31");
            panel1.add(ROH31);
            ROH31.setBounds(687, 238, 50, ROH31.getPreferredSize().height);

            //---- ROH32 ----
            ROH32.setName("ROH32");
            panel1.add(ROH32);
            ROH32.setBounds(744, 238, 50, ROH32.getPreferredSize().height);

            //---- ROH33 ----
            ROH33.setName("ROH33");
            panel1.add(ROH33);
            ROH33.setBounds(345, 278, 50, 28);

            //---- ROH34 ----
            ROH34.setName("ROH34");
            panel1.add(ROH34);
            ROH34.setBounds(402, 278, 50, 28);

            //---- ROH35 ----
            ROH35.setName("ROH35");
            panel1.add(ROH35);
            ROH35.setBounds(459, 278, 50, 28);

            //---- ROH36 ----
            ROH36.setName("ROH36");
            panel1.add(ROH36);
            ROH36.setBounds(516, 278, 50, 28);

            //---- ROH37 ----
            ROH37.setName("ROH37");
            panel1.add(ROH37);
            ROH37.setBounds(573, 278, 50, 28);

            //---- ROH38 ----
            ROH38.setName("ROH38");
            panel1.add(ROH38);
            ROH38.setBounds(630, 278, 50, 28);

            //---- ROH39 ----
            ROH39.setName("ROH39");
            panel1.add(ROH39);
            ROH39.setBounds(687, 278, 50, 28);

            //---- ROH40 ----
            ROH40.setName("ROH40");
            panel1.add(ROH40);
            ROH40.setBounds(744, 278, 50, 28);

            //---- ROH41 ----
            ROH41.setName("ROH41");
            panel1.add(ROH41);
            ROH41.setBounds(345, 318, 50, ROH41.getPreferredSize().height);

            //---- ROH42 ----
            ROH42.setName("ROH42");
            panel1.add(ROH42);
            ROH42.setBounds(402, 318, 50, ROH42.getPreferredSize().height);

            //---- ROH43 ----
            ROH43.setName("ROH43");
            panel1.add(ROH43);
            ROH43.setBounds(459, 318, 50, ROH43.getPreferredSize().height);

            //---- ROH44 ----
            ROH44.setName("ROH44");
            panel1.add(ROH44);
            ROH44.setBounds(516, 318, 50, ROH44.getPreferredSize().height);

            //---- ROH45 ----
            ROH45.setName("ROH45");
            panel1.add(ROH45);
            ROH45.setBounds(573, 318, 50, ROH45.getPreferredSize().height);

            //---- ROH46 ----
            ROH46.setName("ROH46");
            panel1.add(ROH46);
            ROH46.setBounds(630, 318, 50, ROH46.getPreferredSize().height);

            //---- ROH47 ----
            ROH47.setName("ROH47");
            panel1.add(ROH47);
            ROH47.setBounds(687, 318, 50, ROH47.getPreferredSize().height);

            //---- ROH48 ----
            ROH48.setName("ROH48");
            panel1.add(ROH48);
            ROH48.setBounds(744, 318, 50, ROH48.getPreferredSize().height);

            //---- ROH49 ----
            ROH49.setName("ROH49");
            panel1.add(ROH49);
            ROH49.setBounds(345, 358, 50, ROH49.getPreferredSize().height);

            //---- ROH50 ----
            ROH50.setName("ROH50");
            panel1.add(ROH50);
            ROH50.setBounds(402, 358, 50, ROH50.getPreferredSize().height);

            //---- ROH51 ----
            ROH51.setName("ROH51");
            panel1.add(ROH51);
            ROH51.setBounds(459, 358, 50, ROH51.getPreferredSize().height);

            //---- ROH52 ----
            ROH52.setName("ROH52");
            panel1.add(ROH52);
            ROH52.setBounds(516, 358, 50, ROH52.getPreferredSize().height);

            //---- separator1 ----
            separator1.setName("separator1");
            panel1.add(separator1);
            separator1.setBounds(345, 95, 445, separator1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 432, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Duplication");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_49;
  private XRiTextField INDETB;
  private JLabel OBJ_50;
  private XRiTextField INDATE;
  private JLabel OBJ_57;
  private XRiTextField INDANN;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_75;
  private RiZoneSortie ATLIB;
  private RiZoneSortie LIB01;
  private RiZoneSortie LIB02;
  private RiZoneSortie LIB03;
  private RiZoneSortie LIB04;
  private RiZoneSortie LIB05;
  private RiZoneSortie LIB06;
  private RiZoneSortie LIB07;
  private XRiTextField ROH01;
  private XRiTextField ROH02;
  private XRiTextField ROH03;
  private XRiTextField ROH04;
  private XRiTextField ROH05;
  private XRiTextField ROH06;
  private XRiTextField ROH07;
  private XRiTextField ROH08;
  private XRiTextField ROH09;
  private XRiTextField ROH10;
  private XRiTextField ROH11;
  private XRiTextField ROH12;
  private XRiTextField ROH13;
  private XRiTextField ROH14;
  private XRiTextField ROH15;
  private XRiTextField ROH16;
  private XRiTextField ROH17;
  private XRiTextField ROH18;
  private XRiTextField ROH19;
  private XRiTextField ROH20;
  private XRiTextField ROH21;
  private XRiTextField ROH22;
  private XRiTextField ROH23;
  private XRiTextField ROH24;
  private XRiTextField ROH25;
  private XRiTextField ROH26;
  private XRiTextField ROH27;
  private XRiTextField ROH28;
  private XRiTextField ROH29;
  private XRiTextField ROH30;
  private XRiTextField ROH31;
  private XRiTextField ROH32;
  private XRiTextField ROH33;
  private XRiTextField ROH34;
  private XRiTextField ROH35;
  private XRiTextField ROH36;
  private XRiTextField ROH37;
  private XRiTextField ROH38;
  private XRiTextField ROH39;
  private XRiTextField ROH40;
  private XRiTextField ROH41;
  private XRiTextField ROH42;
  private XRiTextField ROH43;
  private XRiTextField ROH44;
  private XRiTextField ROH45;
  private XRiTextField ROH46;
  private XRiTextField ROH47;
  private XRiTextField ROH48;
  private XRiTextField ROH49;
  private XRiTextField ROH50;
  private XRiTextField ROH51;
  private XRiTextField ROH52;
  private JComponent separator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
