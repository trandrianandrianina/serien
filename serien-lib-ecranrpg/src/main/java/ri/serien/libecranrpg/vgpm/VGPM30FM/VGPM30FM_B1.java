
package ri.serien.libecranrpg.vgpm.VGPM30FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGPM30FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WL501_Title = { "ENT", };
  private String[][] _WL501_Data = { { "WL501", }, };
  private int[] _WL501_Width = { 496, };
  // private String[] _LIST_Top=null;
  // private String[][] _LIST_Title_Data_Brut=null;
  
  public VGPM30FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    setDialog(true);
    
    // Ajout
    initDiverses();
    WL501.setAspectTable(null, _WL501_Title, _WL501_Data, _WL501_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    ENCLA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENCLA1@")).trim());
    ENCLA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENCLA2@")).trim());
    OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLZ1@")).trim());
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLZ2@")).trim());
    QMIX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QMIX@")).trim());
    QTEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTEX@")).trim());
    A1FAM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1FAM@")).trim());
    ENUNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENUNS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, _LIST_Title_Data_Brut, _LIST_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    ENUNS.setVisible(lexique.isPresent("ENUNS"));
    A1FAM.setVisible(lexique.isPresent("A1FAM"));
    QTEX.setVisible(lexique.isPresent("QTEX"));
    QMIX.setVisible(lexique.isPresent("QMIX"));
    OBJ_24.setVisible(lexique.isPresent("WLZ2"));
    OBJ_22.setVisible(lexique.isPresent("WLZ1"));
    ENCLA2.setVisible(lexique.isPresent("ENCLA2"));
    ENCLA1.setVisible(lexique.isPresent("ENCLA1"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WL501 = new XRiTable();
    panel1 = new JPanel();
    ENCLA1 = new RiZoneSortie();
    ENCLA2 = new RiZoneSortie();
    OBJ_22 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_30 = new JLabel();
    QMIX = new RiZoneSortie();
    QTEX = new RiZoneSortie();
    OBJ_28 = new JLabel();
    OBJ_26 = new JLabel();
    A1FAM = new RiZoneSortie();
    ENUNS = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setComponentPopupMenu(BTD);
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- WL501 ----
          WL501.setName("WL501");
          SCROLLPANE_LIST.setViewportView(WL501);
        }

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("D\u00e9tail"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- ENCLA1 ----
          ENCLA1.setText("@ENCLA1@");
          ENCLA1.setName("ENCLA1");
          panel1.add(ENCLA1);
          ENCLA1.setBounds(220, 40, 192, ENCLA1.getPreferredSize().height);

          //---- ENCLA2 ----
          ENCLA2.setText("@ENCLA2@");
          ENCLA2.setName("ENCLA2");
          panel1.add(ENCLA2);
          ENCLA2.setBounds(220, 70, 192, ENCLA2.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("@WLZ1@");
          OBJ_22.setName("OBJ_22");
          panel1.add(OBJ_22);
          OBJ_22.setBounds(25, 42, 170, 20);

          //---- OBJ_24 ----
          OBJ_24.setText("@WLZ2@");
          OBJ_24.setName("OBJ_24");
          panel1.add(OBJ_24);
          OBJ_24.setBounds(25, 72, 170, 20);

          //---- OBJ_32 ----
          OBJ_32.setText("Quantit\u00e9 \u00e9conomique");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(25, 182, 130, 20);

          //---- OBJ_30 ----
          OBJ_30.setText("Quantit\u00e9 minimale");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(25, 152, 109, 20);

          //---- QMIX ----
          QMIX.setText("@QMIX@");
          QMIX.setName("QMIX");
          panel1.add(QMIX);
          QMIX.setBounds(220, 150, 106, QMIX.getPreferredSize().height);

          //---- QTEX ----
          QTEX.setText("@QTEX@");
          QTEX.setName("QTEX");
          panel1.add(QTEX);
          QTEX.setBounds(220, 180, 106, QTEX.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setText("Unit\u00e9 de stock");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(220, 112, 105, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("Famille");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(25, 112, 48, 20);

          //---- A1FAM ----
          A1FAM.setText("@A1FAM@");
          A1FAM.setName("A1FAM");
          panel1.add(A1FAM);
          A1FAM.setBounds(90, 110, 34, A1FAM.getPreferredSize().height);

          //---- ENUNS ----
          ENUNS.setText("@ENUNS@");
          ENUNS.setName("ENUNS");
          panel1.add(ENUNS);
          ENUNS.setBounds(345, 110, 24, ENUNS.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 505, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 505, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
              .addGap(21, 21, 21)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WL501;
  private JPanel panel1;
  private RiZoneSortie ENCLA1;
  private RiZoneSortie ENCLA2;
  private JLabel OBJ_22;
  private JLabel OBJ_24;
  private JLabel OBJ_32;
  private JLabel OBJ_30;
  private RiZoneSortie QMIX;
  private RiZoneSortie QTEX;
  private JLabel OBJ_28;
  private JLabel OBJ_26;
  private RiZoneSortie A1FAM;
  private RiZoneSortie ENUNS;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
