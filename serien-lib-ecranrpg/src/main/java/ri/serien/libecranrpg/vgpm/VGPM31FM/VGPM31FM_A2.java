
package ri.serien.libecranrpg.vgpm.VGPM31FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGPM31FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGPM31FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@WTIT@")).trim()));
    WL201.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL201@")).trim());
    WL202.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL202@")).trim());
    WL203.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL203@")).trim());
    WL204.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL204@")).trim());
    WL205.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL205@")).trim());
    WL206.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL206@")).trim());
    WL207.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL207@")).trim());
    WL208.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL208@")).trim());
    WL209.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL209@")).trim());
    WL210.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL210@")).trim());
    WL211.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL211@")).trim());
    WL212.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL212@")).trim());
    WL213.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL213@")).trim());
    WL214.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL214@")).trim());
    WL215.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WL215@")).trim());
    P31QTX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P31QTX@")).trim());
    K27QTF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@K27QTF@")).trim());
    EBUNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBUNS@")).trim());
    P31ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P31ART@")).trim());
    EBLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBLIB@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HLD01@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    gererLesErreurs("19");
    
    
    
    label6.setText(lexique.HostFieldGetData("HLD01").substring(13));
    
    // TODO Icones
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WL201 = new RiZoneSortie();
    QTE01 = new XRiTextField();
    QTE02 = new XRiTextField();
    WL202 = new RiZoneSortie();
    QTE03 = new XRiTextField();
    WL203 = new RiZoneSortie();
    QTE04 = new XRiTextField();
    WL204 = new RiZoneSortie();
    QTE05 = new XRiTextField();
    WL205 = new RiZoneSortie();
    QTE06 = new XRiTextField();
    WL206 = new RiZoneSortie();
    QTE07 = new XRiTextField();
    WL207 = new RiZoneSortie();
    QTE08 = new XRiTextField();
    WL208 = new RiZoneSortie();
    QTE09 = new XRiTextField();
    WL209 = new RiZoneSortie();
    QTE10 = new XRiTextField();
    WL210 = new RiZoneSortie();
    QTE11 = new XRiTextField();
    WL211 = new RiZoneSortie();
    QTE12 = new XRiTextField();
    WL212 = new RiZoneSortie();
    QTE13 = new XRiTextField();
    WL213 = new RiZoneSortie();
    QTE14 = new XRiTextField();
    WL214 = new RiZoneSortie();
    QTE15 = new XRiTextField();
    WL215 = new RiZoneSortie();
    P31QTX = new RiZoneSortie();
    K27QTF = new RiZoneSortie();
    EBUNS = new RiZoneSortie();
    P31ART = new RiZoneSortie();
    EBLIB = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(945, 585));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("@WTIT@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WL201 ----
          WL201.setText("@WL201@");
          WL201.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL201.setName("WL201");
          panel1.add(WL201);
          WL201.setBounds(140, 127, 600, WL201.getPreferredSize().height);

          //---- QTE01 ----
          QTE01.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE01.setName("QTE01");
          panel1.add(QTE01);
          QTE01.setBounds(20, 125, 108, QTE01.getPreferredSize().height);

          //---- QTE02 ----
          QTE02.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE02.setName("QTE02");
          panel1.add(QTE02);
          QTE02.setBounds(20, 153, 108, QTE02.getPreferredSize().height);

          //---- WL202 ----
          WL202.setText("@WL202@");
          WL202.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL202.setName("WL202");
          panel1.add(WL202);
          WL202.setBounds(140, 155, 600, WL202.getPreferredSize().height);

          //---- QTE03 ----
          QTE03.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE03.setName("QTE03");
          panel1.add(QTE03);
          QTE03.setBounds(20, 181, 108, QTE03.getPreferredSize().height);

          //---- WL203 ----
          WL203.setText("@WL203@");
          WL203.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL203.setName("WL203");
          panel1.add(WL203);
          WL203.setBounds(140, 183, 600, WL203.getPreferredSize().height);

          //---- QTE04 ----
          QTE04.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE04.setName("QTE04");
          panel1.add(QTE04);
          QTE04.setBounds(20, 209, 108, QTE04.getPreferredSize().height);

          //---- WL204 ----
          WL204.setText("@WL204@");
          WL204.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL204.setName("WL204");
          panel1.add(WL204);
          WL204.setBounds(140, 211, 600, WL204.getPreferredSize().height);

          //---- QTE05 ----
          QTE05.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE05.setName("QTE05");
          panel1.add(QTE05);
          QTE05.setBounds(20, 237, 108, QTE05.getPreferredSize().height);

          //---- WL205 ----
          WL205.setText("@WL205@");
          WL205.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL205.setName("WL205");
          panel1.add(WL205);
          WL205.setBounds(140, 239, 600, WL205.getPreferredSize().height);

          //---- QTE06 ----
          QTE06.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE06.setName("QTE06");
          panel1.add(QTE06);
          QTE06.setBounds(20, 265, 108, QTE06.getPreferredSize().height);

          //---- WL206 ----
          WL206.setText("@WL206@");
          WL206.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL206.setName("WL206");
          panel1.add(WL206);
          WL206.setBounds(140, 267, 600, WL206.getPreferredSize().height);

          //---- QTE07 ----
          QTE07.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE07.setName("QTE07");
          panel1.add(QTE07);
          QTE07.setBounds(20, 293, 108, QTE07.getPreferredSize().height);

          //---- WL207 ----
          WL207.setText("@WL207@");
          WL207.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL207.setName("WL207");
          panel1.add(WL207);
          WL207.setBounds(140, 295, 600, WL207.getPreferredSize().height);

          //---- QTE08 ----
          QTE08.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE08.setName("QTE08");
          panel1.add(QTE08);
          QTE08.setBounds(20, 321, 108, QTE08.getPreferredSize().height);

          //---- WL208 ----
          WL208.setText("@WL208@");
          WL208.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL208.setName("WL208");
          panel1.add(WL208);
          WL208.setBounds(140, 323, 600, WL208.getPreferredSize().height);

          //---- QTE09 ----
          QTE09.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE09.setName("QTE09");
          panel1.add(QTE09);
          QTE09.setBounds(20, 349, 108, QTE09.getPreferredSize().height);

          //---- WL209 ----
          WL209.setText("@WL209@");
          WL209.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL209.setName("WL209");
          panel1.add(WL209);
          WL209.setBounds(140, 351, 600, WL209.getPreferredSize().height);

          //---- QTE10 ----
          QTE10.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE10.setName("QTE10");
          panel1.add(QTE10);
          QTE10.setBounds(20, 377, 108, QTE10.getPreferredSize().height);

          //---- WL210 ----
          WL210.setText("@WL210@");
          WL210.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL210.setName("WL210");
          panel1.add(WL210);
          WL210.setBounds(140, 379, 600, WL210.getPreferredSize().height);

          //---- QTE11 ----
          QTE11.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE11.setName("QTE11");
          panel1.add(QTE11);
          QTE11.setBounds(20, 405, 108, QTE11.getPreferredSize().height);

          //---- WL211 ----
          WL211.setText("@WL211@");
          WL211.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL211.setName("WL211");
          panel1.add(WL211);
          WL211.setBounds(140, 407, 600, WL211.getPreferredSize().height);

          //---- QTE12 ----
          QTE12.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE12.setName("QTE12");
          panel1.add(QTE12);
          QTE12.setBounds(20, 433, 108, QTE12.getPreferredSize().height);

          //---- WL212 ----
          WL212.setText("@WL212@");
          WL212.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL212.setName("WL212");
          panel1.add(WL212);
          WL212.setBounds(140, 435, 600, WL212.getPreferredSize().height);

          //---- QTE13 ----
          QTE13.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE13.setName("QTE13");
          panel1.add(QTE13);
          QTE13.setBounds(20, 461, 108, QTE13.getPreferredSize().height);

          //---- WL213 ----
          WL213.setText("@WL213@");
          WL213.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL213.setName("WL213");
          panel1.add(WL213);
          WL213.setBounds(140, 463, 600, WL213.getPreferredSize().height);

          //---- QTE14 ----
          QTE14.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE14.setName("QTE14");
          panel1.add(QTE14);
          QTE14.setBounds(20, 489, 108, QTE14.getPreferredSize().height);

          //---- WL214 ----
          WL214.setText("@WL214@");
          WL214.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL214.setName("WL214");
          panel1.add(WL214);
          WL214.setBounds(140, 491, 600, WL214.getPreferredSize().height);

          //---- QTE15 ----
          QTE15.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE15.setName("QTE15");
          panel1.add(QTE15);
          QTE15.setBounds(20, 517, 108, QTE15.getPreferredSize().height);

          //---- WL215 ----
          WL215.setText("@WL215@");
          WL215.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
          WL215.setName("WL215");
          panel1.add(WL215);
          WL215.setBounds(140, 519, 600, WL215.getPreferredSize().height);

          //---- P31QTX ----
          P31QTX.setText("@P31QTX@");
          P31QTX.setHorizontalAlignment(SwingConstants.RIGHT);
          P31QTX.setName("P31QTX");
          panel1.add(P31QTX);
          P31QTX.setBounds(140, 35, 108, P31QTX.getPreferredSize().height);

          //---- K27QTF ----
          K27QTF.setText("@K27QTF@");
          K27QTF.setHorizontalAlignment(SwingConstants.RIGHT);
          K27QTF.setName("K27QTF");
          panel1.add(K27QTF);
          K27QTF.setBounds(140, 65, 108, K27QTF.getPreferredSize().height);

          //---- EBUNS ----
          EBUNS.setText("@EBUNS@");
          EBUNS.setName("EBUNS");
          panel1.add(EBUNS);
          EBUNS.setBounds(255, 35, 34, EBUNS.getPreferredSize().height);

          //---- P31ART ----
          P31ART.setText("@P31ART@");
          P31ART.setName("P31ART");
          panel1.add(P31ART);
          P31ART.setBounds(430, 35, 210, P31ART.getPreferredSize().height);

          //---- EBLIB ----
          EBLIB.setText("@EBLIB@");
          EBLIB.setName("EBLIB");
          panel1.add(EBLIB);
          EBLIB.setBounds(430, 65, 310, EBLIB.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Quantit\u00e9 \u00e0 fabriquer");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(20, 35, 120, 24);

          //---- label2 ----
          label2.setText("Quantit\u00e9 fabriqu\u00e9e");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(20, 65, 120, 24);

          //---- label3 ----
          label3.setText("Code article");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(340, 35, 90, 24);

          //---- label4 ----
          label4.setText("Libell\u00e9 article");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(340, 65, 90, 24);

          //---- label5 ----
          label5.setText("Quantit\u00e9");
          label5.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(20, 105, 108, 21);

          //---- label6 ----
          label6.setText("@HLD01@");
          label6.setFont(new Font(Font.MONOSPACED, Font.BOLD, 12));
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(145, 105, 595, 21);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 15, 755, 560);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie WL201;
  private XRiTextField QTE01;
  private XRiTextField QTE02;
  private RiZoneSortie WL202;
  private XRiTextField QTE03;
  private RiZoneSortie WL203;
  private XRiTextField QTE04;
  private RiZoneSortie WL204;
  private XRiTextField QTE05;
  private RiZoneSortie WL205;
  private XRiTextField QTE06;
  private RiZoneSortie WL206;
  private XRiTextField QTE07;
  private RiZoneSortie WL207;
  private XRiTextField QTE08;
  private RiZoneSortie WL208;
  private XRiTextField QTE09;
  private RiZoneSortie WL209;
  private XRiTextField QTE10;
  private RiZoneSortie WL210;
  private XRiTextField QTE11;
  private RiZoneSortie WL211;
  private XRiTextField QTE12;
  private RiZoneSortie WL212;
  private XRiTextField QTE13;
  private RiZoneSortie WL213;
  private XRiTextField QTE14;
  private RiZoneSortie WL214;
  private XRiTextField QTE15;
  private RiZoneSortie WL215;
  private RiZoneSortie P31QTX;
  private RiZoneSortie K27QTF;
  private RiZoneSortie EBUNS;
  private RiZoneSortie P31ART;
  private RiZoneSortie EBLIB;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
