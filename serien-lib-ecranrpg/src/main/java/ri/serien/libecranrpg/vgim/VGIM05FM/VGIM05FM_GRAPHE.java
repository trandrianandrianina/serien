
package ri.serien.libecranrpg.vgim.VGIM05FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class VGIM05FM_GRAPHE extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_PIE3D);
  private int ligne = 0;
  
  public VGIM05FM_GRAPHE(JPanel panel, Lexical lex, iData iD, int lig) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    ligne = lig;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    // GRAPHE
    String[] libelle = { "Amortissement antérieur", "Amortissement sur l'exercice", "Valeur résiduelle" };
    String[] ligneDonnee = new String[3];
    
    // Chargement des données
    ligneDonnee[0] = lexique.HostFieldGetData("G" + (ligne < 10 ? "0" + (ligne) : (ligne)));
    ligneDonnee[1] = lexique.HostFieldGetData("H" + (ligne < 10 ? "0" + (ligne) : (ligne)));
    ligneDonnee[2] = lexique.HostFieldGetData("J" + (ligne < 10 ? "0" + (ligne) : (ligne)));
    
    // Préparation des données
    Object[][] data = new Object[libelle.length][2];
    
    for (int i = 0; i < libelle.length; i++) {
      data[i][0] = libelle[i];
      ligneDonnee[i] = ligneDonnee[i].replaceAll("\\s", "0");
      data[i][1] = Double.parseDouble(ligneDonnee[i]);
    }
    
    graphe.setDonnee(data, "", false);
    graphe.getGraphe("Etat de l'amortissement d'une immobilisation (pour une valeur d'acquisition de "
        + lexique.HostFieldGetData("F" + (ligne < 10 ? "0" + (ligne) : (ligne))) + ")", false);
    
    // couleurs
    
    l_graphe.setIcon(graphe.getPicture(l_graphe.getWidth(), l_graphe.getHeight()));
    
    
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle("Immobilisations");
  }
  
  public void getData() {
    
  }
  
  public void reveiller() {
    setVisible(true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    setVisible(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    l_graphe = new JLabel();
    OBJ_10 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1100, 690));
    setResizable(false);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      
      // ---- l_graphe ----
      l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe.setComponentPopupMenu(null);
      l_graphe.setBackground(new Color(214, 217, 223));
      l_graphe.setName("l_graphe");
      
      // ---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setToolTipText("Retour");
      OBJ_10.setText("Retour");
      OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD, OBJ_10.getFont().getSize() + 3f));
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(945, 945, 945)
              .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE).addGap(7, 7, 7))
          .addGroup(P_CentreLayout.createSequentialGroup().addContainerGap()
              .addComponent(l_graphe, GroupLayout.DEFAULT_SIZE, 1068, Short.MAX_VALUE).addContainerGap()));
      P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(15, 15, 15)
              .addComponent(l_graphe, GroupLayout.PREFERRED_SIZE, 580, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
              .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JLabel l_graphe;
  private JButton OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
  public static class PieRenderer {
    private Color[] color;
    
    public PieRenderer(Color[] color) {
      this.color = color;
    }
    
    public void setColor(PiePlot plot, DefaultPieDataset dataset) {
      List<Comparable<?>> keys = dataset.getKeys();
      int aInt;
      
      for (int i = 0; i < keys.size(); i++) {
        aInt = i % this.color.length;
        plot.setSectionPaint(keys.get(i), this.color[aInt]);
      }
    }
  }
}
