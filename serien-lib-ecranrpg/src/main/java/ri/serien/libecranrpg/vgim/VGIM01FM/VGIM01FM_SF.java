
package ri.serien.libecranrpg.vgim.VGIM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGIM01FM_SF extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] FAMDA_Value = { "", "L", "D", "I", "F", "N", };
  
  public VGIM01FM_SF(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    SFMDA.setValeurs(FAMDA_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("FAMDA", 0, FAMDA_Value[FAMDA.getSelectedIndex()]);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_39 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_60 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_61 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    SFLIB = new XRiTextField();
    OBJ_76 = new JLabel();
    OBJ_78 = new JLabel();
    SFGEO = new XRiTextField();
    OBJ_75 = new JLabel();
    SFSAN = new XRiTextField();
    panel1 = new JPanel();
    OBJ_77 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_85 = new JLabel();
    SFNCA = new XRiTextField();
    SFNCD = new XRiTextField();
    SFNCI = new XRiTextField();
    panel2 = new JPanel();
    OBJ_87 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_89 = new JLabel();
    SFNDA = new XRiTextField();
    SFNDD = new XRiTextField();
    SFNDR = new XRiTextField();
    panel3 = new JPanel();
    SFMDA = new XRiComboBox();
    OBJ_83 = new JLabel();
    OBJ_82 = new JLabel();
    SFTXL = new XRiTextField();
    SFTXD = new XRiTextField();
    OBJ_81 = new JLabel();
    OBJ_80 = new JLabel();
    SFDUA2 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(930, 620));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation des immobilisations");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_39 ----
          OBJ_39.setText("Etablissement");
          OBJ_39.setName("OBJ_39");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");

          //---- OBJ_60 ----
          OBJ_60.setText("Code");
          OBJ_60.setName("OBJ_60");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setText("@INDTYP@");
          INDTYP.setOpaque(false);
          INDTYP.setName("INDTYP");

          //---- OBJ_61 ----
          OBJ_61.setText("Ordre");
          OBJ_61.setName("OBJ_61");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setText("@INDIND@");
          INDIND.setOpaque(false);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(730, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Famille d'immobilisation");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- SFLIB ----
            SFLIB.setComponentPopupMenu(null);
            SFLIB.setName("SFLIB");
            xTitledPanel1ContentContainer.add(SFLIB);
            SFLIB.setBounds(180, 25, 310, SFLIB.getPreferredSize().height);

            //---- OBJ_76 ----
            OBJ_76.setText("Code g\u00e9ographique");
            OBJ_76.setName("OBJ_76");
            xTitledPanel1ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(55, 64, 125, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("Code analytique");
            OBJ_78.setName("OBJ_78");
            xTitledPanel1ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(335, 64, 102, 20);

            //---- SFGEO ----
            SFGEO.setComponentPopupMenu(null);
            SFGEO.setName("SFGEO");
            xTitledPanel1ContentContainer.add(SFGEO);
            SFGEO.setBounds(180, 60, 60, SFGEO.getPreferredSize().height);

            //---- OBJ_75 ----
            OBJ_75.setText("Libell\u00e9");
            OBJ_75.setName("OBJ_75");
            xTitledPanel1ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(55, 29, 61, 20);

            //---- SFSAN ----
            SFSAN.setComponentPopupMenu(null);
            SFSAN.setName("SFSAN");
            xTitledPanel1ContentContainer.add(SFSAN);
            SFSAN.setBounds(440, 60, 50, SFSAN.getPreferredSize().height);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("Num\u00e9ros de postes de comptabilisation"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_77 ----
              OBJ_77.setText("Amortissement");
              OBJ_77.setName("OBJ_77");
              panel1.add(OBJ_77);
              OBJ_77.setBounds(35, 39, 100, 20);

              //---- OBJ_86 ----
              OBJ_86.setText("immobilisation");
              OBJ_86.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_86.setName("OBJ_86");
              panel1.add(OBJ_86);
              OBJ_86.setBounds(320, 39, 95, 20);

              //---- OBJ_85 ----
              OBJ_85.setText("Dotation");
              OBJ_85.setName("OBJ_85");
              panel1.add(OBJ_85);
              OBJ_85.setBounds(220, 40, 60, 20);

              //---- SFNCA ----
              SFNCA.setComponentPopupMenu(null);
              SFNCA.setName("SFNCA");
              panel1.add(SFNCA);
              SFNCA.setBounds(155, 35, 34, SFNCA.getPreferredSize().height);

              //---- SFNCD ----
              SFNCD.setComponentPopupMenu(null);
              SFNCD.setName("SFNCD");
              panel1.add(SFNCD);
              SFNCD.setBounds(285, 35, 34, SFNCD.getPreferredSize().height);

              //---- SFNCI ----
              SFNCI.setComponentPopupMenu(null);
              SFNCI.setName("SFNCI");
              panel1.add(SFNCI);
              SFNCI.setBounds(430, 35, 34, SFNCI.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(25, 110, 605, 85);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Num\u00e9ros de postes de comptabilisation : amortissement d\u00e9rogatoire"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_87 ----
              OBJ_87.setText("Amortissement");
              OBJ_87.setName("OBJ_87");
              panel2.add(OBJ_87);
              OBJ_87.setBounds(35, 39, 100, 20);

              //---- OBJ_88 ----
              OBJ_88.setText("Dotation");
              OBJ_88.setName("OBJ_88");
              panel2.add(OBJ_88);
              OBJ_88.setBounds(220, 39, 60, 20);

              //---- OBJ_89 ----
              OBJ_89.setText("Reprise");
              OBJ_89.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_89.setName("OBJ_89");
              panel2.add(OBJ_89);
              OBJ_89.setBounds(360, 39, 55, 20);

              //---- SFNDA ----
              SFNDA.setComponentPopupMenu(null);
              SFNDA.setName("SFNDA");
              panel2.add(SFNDA);
              SFNDA.setBounds(155, 35, 34, SFNDA.getPreferredSize().height);

              //---- SFNDD ----
              SFNDD.setComponentPopupMenu(null);
              SFNDD.setName("SFNDD");
              panel2.add(SFNDD);
              SFNDD.setBounds(285, 35, 34, SFNDD.getPreferredSize().height);

              //---- SFNDR ----
              SFNDR.setComponentPopupMenu(null);
              SFNDR.setName("SFNDR");
              panel2.add(SFNDR);
              SFNDR.setBounds(430, 35, 34, SFNDR.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(25, 205, 605, 85);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Amortissement"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- SFMDA ----
              SFMDA.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "Lin\u00e9aire",
                "D\u00e9gressif",
                "Impos\u00e9",
                "Fixe",
                "Nul"
              }));
              SFMDA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SFMDA.setName("SFMDA");
              panel3.add(SFMDA);
              SFMDA.setBounds(75, 36, 95, SFMDA.getPreferredSize().height);

              //---- OBJ_83 ----
              OBJ_83.setText("Taux d\u00e9gr\u00e9ssif");
              OBJ_83.setName("OBJ_83");
              panel3.add(OBJ_83);
              OBJ_83.setBounds(430, 39, 93, 20);

              //---- OBJ_82 ----
              OBJ_82.setText("Taux lin\u00e9aire");
              OBJ_82.setName("OBJ_82");
              panel3.add(OBJ_82);
              OBJ_82.setBounds(275, 39, 81, 20);

              //---- SFTXL ----
              SFTXL.setComponentPopupMenu(null);
              SFTXL.setName("SFTXL");
              panel3.add(SFTXL);
              SFTXL.setBounds(355, 35, 50, SFTXL.getPreferredSize().height);

              //---- SFTXD ----
              SFTXD.setComponentPopupMenu(null);
              SFTXD.setName("SFTXD");
              panel3.add(SFTXD);
              SFTXD.setBounds(520, 35, 50, SFTXD.getPreferredSize().height);

              //---- OBJ_81 ----
              OBJ_81.setText("Dur\u00e9e");
              OBJ_81.setName("OBJ_81");
              panel3.add(OBJ_81);
              OBJ_81.setBounds(185, 39, 45, 20);

              //---- OBJ_80 ----
              OBJ_80.setText("Mode");
              OBJ_80.setName("OBJ_80");
              panel3.add(OBJ_80);
              OBJ_80.setBounds(35, 39, 39, 20);

              //---- SFDUA2 ----
              SFDUA2.setComponentPopupMenu(null);
              SFDUA2.setName("SFDUA2");
              panel3.add(SFDUA2);
              SFDUA2.setBounds(230, 35, 28, SFDUA2.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(25, 300, 605, 85);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 662, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 448, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(37, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_39;
  private RiZoneSortie INDETB;
  private JLabel OBJ_60;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_61;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField SFLIB;
  private JLabel OBJ_76;
  private JLabel OBJ_78;
  private XRiTextField SFGEO;
  private JLabel OBJ_75;
  private XRiTextField SFSAN;
  private JPanel panel1;
  private JLabel OBJ_77;
  private JLabel OBJ_86;
  private JLabel OBJ_85;
  private XRiTextField SFNCA;
  private XRiTextField SFNCD;
  private XRiTextField SFNCI;
  private JPanel panel2;
  private JLabel OBJ_87;
  private JLabel OBJ_88;
  private JLabel OBJ_89;
  private XRiTextField SFNDA;
  private XRiTextField SFNDD;
  private XRiTextField SFNDR;
  private JPanel panel3;
  private XRiComboBox SFMDA;
  private JLabel OBJ_83;
  private JLabel OBJ_82;
  private XRiTextField SFTXL;
  private XRiTextField SFTXD;
  private JLabel OBJ_81;
  private JLabel OBJ_80;
  private XRiTextField SFDUA2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
