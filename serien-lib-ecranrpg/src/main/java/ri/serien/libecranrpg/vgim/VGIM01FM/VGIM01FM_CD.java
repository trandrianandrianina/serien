
package ri.serien.libecranrpg.vgim.VGIM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGIM01FM_CD extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIM01FM_CD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_39 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_60 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_62 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel1 = new JPanel();
    panel2 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    CDD01 = new XRiTextField();
    CDC01 = new XRiTextField();
    CDD02 = new XRiTextField();
    CDC02 = new XRiTextField();
    CDD03 = new XRiTextField();
    CDC03 = new XRiTextField();
    CDD04 = new XRiTextField();
    CDC04 = new XRiTextField();
    CDD05 = new XRiTextField();
    CDC05 = new XRiTextField();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    panel3 = new JPanel();
    label8 = new JLabel();
    label9 = new JLabel();
    CDD06 = new XRiTextField();
    CDC06 = new XRiTextField();
    CDD07 = new XRiTextField();
    CDC07 = new XRiTextField();
    CDD08 = new XRiTextField();
    CDC08 = new XRiTextField();
    CDD09 = new XRiTextField();
    CDC09 = new XRiTextField();
    CDD10 = new XRiTextField();
    CDC10 = new XRiTextField();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    panel4 = new JPanel();
    label15 = new JLabel();
    label16 = new JLabel();
    CDD11 = new XRiTextField();
    CDC11 = new XRiTextField();
    CDD12 = new XRiTextField();
    CDC12 = new XRiTextField();
    CDD13 = new XRiTextField();
    CDC13 = new XRiTextField();
    CDD14 = new XRiTextField();
    CDC14 = new XRiTextField();
    CDD15 = new XRiTextField();
    CDC15 = new XRiTextField();
    label17 = new JLabel();
    label18 = new JLabel();
    label19 = new JLabel();
    label20 = new JLabel();
    label21 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation des immobilisations");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_39 ----
          OBJ_39.setText("Etablissement");
          OBJ_39.setName("OBJ_39");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_60 ----
          OBJ_60.setText("Code");
          OBJ_60.setName("OBJ_60");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");

          //---- OBJ_62 ----
          OBJ_62.setText("Ordre");
          OBJ_62.setName("OBJ_62");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(640, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Coefficients d\u00e9gressifs");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel1 ========
            {
              panel1.setName("panel1");
              panel1.setLayout(null);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(new Rectangle(new Point(692, 45), panel1.getPreferredSize()));

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("D\u00e9gressif normal"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- label1 ----
              label1.setText("Dur\u00e9e");
              label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
              label1.setName("label1");
              panel2.add(label1);
              label1.setBounds(30, 47, 80, 24);

              //---- label2 ----
              label2.setText("Coefficient");
              label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
              label2.setName("label2");
              panel2.add(label2);
              label2.setBounds(30, 77, 80, 24);

              //---- CDD01 ----
              CDD01.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD01.setName("CDD01");
              panel2.add(CDD01);
              CDD01.setBounds(115, 45, 30, CDD01.getPreferredSize().height);

              //---- CDC01 ----
              CDC01.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC01.setName("CDC01");
              panel2.add(CDC01);
              CDC01.setBounds(115, 75, 60, CDC01.getPreferredSize().height);

              //---- CDD02 ----
              CDD02.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD02.setName("CDD02");
              panel2.add(CDD02);
              CDD02.setBounds(195, 45, 30, CDD02.getPreferredSize().height);

              //---- CDC02 ----
              CDC02.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC02.setName("CDC02");
              panel2.add(CDC02);
              CDC02.setBounds(195, 75, 60, CDC02.getPreferredSize().height);

              //---- CDD03 ----
              CDD03.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD03.setName("CDD03");
              panel2.add(CDD03);
              CDD03.setBounds(275, 45, 30, CDD03.getPreferredSize().height);

              //---- CDC03 ----
              CDC03.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC03.setName("CDC03");
              panel2.add(CDC03);
              CDC03.setBounds(275, 75, 60, CDC03.getPreferredSize().height);

              //---- CDD04 ----
              CDD04.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD04.setName("CDD04");
              panel2.add(CDD04);
              CDD04.setBounds(355, 45, 30, CDD04.getPreferredSize().height);

              //---- CDC04 ----
              CDC04.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC04.setName("CDC04");
              panel2.add(CDC04);
              CDC04.setBounds(355, 75, 60, CDC04.getPreferredSize().height);

              //---- CDD05 ----
              CDD05.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD05.setName("CDD05");
              panel2.add(CDD05);
              CDD05.setBounds(435, 45, 30, CDD05.getPreferredSize().height);

              //---- CDC05 ----
              CDC05.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC05.setName("CDC05");
              panel2.add(CDC05);
              CDC05.setBounds(435, 75, 60, CDC05.getPreferredSize().height);

              //---- label3 ----
              label3.setText("1");
              label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
              label3.setHorizontalAlignment(SwingConstants.CENTER);
              label3.setName("label3");
              panel2.add(label3);
              label3.setBounds(115, 25, 27, 21);

              //---- label4 ----
              label4.setText("2");
              label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
              label4.setHorizontalAlignment(SwingConstants.CENTER);
              label4.setName("label4");
              panel2.add(label4);
              label4.setBounds(197, 25, 27, 21);

              //---- label5 ----
              label5.setText("3");
              label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
              label5.setHorizontalAlignment(SwingConstants.CENTER);
              label5.setName("label5");
              panel2.add(label5);
              label5.setBounds(277, 25, 27, 21);

              //---- label6 ----
              label6.setText("4");
              label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
              label6.setHorizontalAlignment(SwingConstants.CENTER);
              label6.setName("label6");
              panel2.add(label6);
              label6.setBounds(355, 25, 27, 21);

              //---- label7 ----
              label7.setText("5");
              label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
              label7.setHorizontalAlignment(SwingConstants.CENTER);
              label7.setName("label7");
              panel2.add(label7);
              label7.setBounds(435, 25, 27, 21);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(25, 15, 530, 125);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("D\u00e9gressif major\u00e9"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- label8 ----
              label8.setText("Dur\u00e9e");
              label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
              label8.setName("label8");
              panel3.add(label8);
              label8.setBounds(30, 40, 80, 24);

              //---- label9 ----
              label9.setText("Coefficient");
              label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
              label9.setName("label9");
              panel3.add(label9);
              label9.setBounds(30, 77, 80, 24);

              //---- CDD06 ----
              CDD06.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD06.setName("CDD06");
              panel3.add(CDD06);
              CDD06.setBounds(115, 45, 30, CDD06.getPreferredSize().height);

              //---- CDC06 ----
              CDC06.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC06.setName("CDC06");
              panel3.add(CDC06);
              CDC06.setBounds(115, 75, 60, CDC06.getPreferredSize().height);

              //---- CDD07 ----
              CDD07.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD07.setName("CDD07");
              panel3.add(CDD07);
              CDD07.setBounds(195, 45, 30, CDD07.getPreferredSize().height);

              //---- CDC07 ----
              CDC07.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC07.setName("CDC07");
              panel3.add(CDC07);
              CDC07.setBounds(195, 75, 60, CDC07.getPreferredSize().height);

              //---- CDD08 ----
              CDD08.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD08.setName("CDD08");
              panel3.add(CDD08);
              CDD08.setBounds(275, 45, 30, CDD08.getPreferredSize().height);

              //---- CDC08 ----
              CDC08.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC08.setName("CDC08");
              panel3.add(CDC08);
              CDC08.setBounds(275, 75, 60, CDC08.getPreferredSize().height);

              //---- CDD09 ----
              CDD09.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD09.setName("CDD09");
              panel3.add(CDD09);
              CDD09.setBounds(355, 45, 30, CDD09.getPreferredSize().height);

              //---- CDC09 ----
              CDC09.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC09.setName("CDC09");
              panel3.add(CDC09);
              CDC09.setBounds(355, 75, 60, CDC09.getPreferredSize().height);

              //---- CDD10 ----
              CDD10.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD10.setName("CDD10");
              panel3.add(CDD10);
              CDD10.setBounds(435, 45, 30, CDD10.getPreferredSize().height);

              //---- CDC10 ----
              CDC10.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC10.setName("CDC10");
              panel3.add(CDC10);
              CDC10.setBounds(435, 75, 60, CDC10.getPreferredSize().height);

              //---- label10 ----
              label10.setText("1");
              label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
              label10.setHorizontalAlignment(SwingConstants.CENTER);
              label10.setName("label10");
              panel3.add(label10);
              label10.setBounds(117, 25, 27, 21);

              //---- label11 ----
              label11.setText("2");
              label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
              label11.setHorizontalAlignment(SwingConstants.CENTER);
              label11.setName("label11");
              panel3.add(label11);
              label11.setBounds(197, 25, 27, 21);

              //---- label12 ----
              label12.setText("3");
              label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
              label12.setHorizontalAlignment(SwingConstants.CENTER);
              label12.setName("label12");
              panel3.add(label12);
              label12.setBounds(277, 25, 27, 21);

              //---- label13 ----
              label13.setText("4");
              label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
              label13.setHorizontalAlignment(SwingConstants.CENTER);
              label13.setName("label13");
              panel3.add(label13);
              label13.setBounds(357, 25, 27, 21);

              //---- label14 ----
              label14.setText("5");
              label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
              label14.setHorizontalAlignment(SwingConstants.CENTER);
              label14.setName("label14");
              panel3.add(label14);
              label14.setBounds(437, 25, 27, 21);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(25, 140, 530, 125);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("D\u00e9gressif minor\u00e9"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- label15 ----
              label15.setText("Dur\u00e9e");
              label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
              label15.setName("label15");
              panel4.add(label15);
              label15.setBounds(30, 47, 80, 24);

              //---- label16 ----
              label16.setText("Coefficient");
              label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
              label16.setName("label16");
              panel4.add(label16);
              label16.setBounds(30, 77, 80, 24);

              //---- CDD11 ----
              CDD11.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD11.setName("CDD11");
              panel4.add(CDD11);
              CDD11.setBounds(115, 45, 30, CDD11.getPreferredSize().height);

              //---- CDC11 ----
              CDC11.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC11.setName("CDC11");
              panel4.add(CDC11);
              CDC11.setBounds(115, 75, 60, CDC11.getPreferredSize().height);

              //---- CDD12 ----
              CDD12.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD12.setName("CDD12");
              panel4.add(CDD12);
              CDD12.setBounds(195, 45, 30, CDD12.getPreferredSize().height);

              //---- CDC12 ----
              CDC12.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC12.setName("CDC12");
              panel4.add(CDC12);
              CDC12.setBounds(195, 75, 60, CDC12.getPreferredSize().height);

              //---- CDD13 ----
              CDD13.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD13.setName("CDD13");
              panel4.add(CDD13);
              CDD13.setBounds(275, 45, 30, CDD13.getPreferredSize().height);

              //---- CDC13 ----
              CDC13.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC13.setName("CDC13");
              panel4.add(CDC13);
              CDC13.setBounds(275, 75, 60, CDC13.getPreferredSize().height);

              //---- CDD14 ----
              CDD14.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD14.setName("CDD14");
              panel4.add(CDD14);
              CDD14.setBounds(355, 45, 30, CDD14.getPreferredSize().height);

              //---- CDC14 ----
              CDC14.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC14.setName("CDC14");
              panel4.add(CDC14);
              CDC14.setBounds(355, 75, 60, CDC14.getPreferredSize().height);

              //---- CDD15 ----
              CDD15.setHorizontalAlignment(SwingConstants.RIGHT);
              CDD15.setName("CDD15");
              panel4.add(CDD15);
              CDD15.setBounds(435, 45, 30, CDD15.getPreferredSize().height);

              //---- CDC15 ----
              CDC15.setHorizontalAlignment(SwingConstants.RIGHT);
              CDC15.setName("CDC15");
              panel4.add(CDC15);
              CDC15.setBounds(435, 75, 60, CDC15.getPreferredSize().height);

              //---- label17 ----
              label17.setText("1");
              label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
              label17.setHorizontalAlignment(SwingConstants.CENTER);
              label17.setName("label17");
              panel4.add(label17);
              label17.setBounds(117, 25, 27, 21);

              //---- label18 ----
              label18.setText("2");
              label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
              label18.setHorizontalAlignment(SwingConstants.CENTER);
              label18.setName("label18");
              panel4.add(label18);
              label18.setBounds(197, 25, 27, 21);

              //---- label19 ----
              label19.setText("3");
              label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
              label19.setHorizontalAlignment(SwingConstants.CENTER);
              label19.setName("label19");
              panel4.add(label19);
              label19.setBounds(277, 25, 27, 21);

              //---- label20 ----
              label20.setText("4");
              label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
              label20.setHorizontalAlignment(SwingConstants.CENTER);
              label20.setName("label20");
              panel4.add(label20);
              label20.setBounds(357, 25, 27, 21);

              //---- label21 ----
              label21.setText("5");
              label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD));
              label21.setHorizontalAlignment(SwingConstants.CENTER);
              label21.setName("label21");
              panel4.add(label21);
              label21.setBounds(437, 25, 27, 21);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel4);
            panel4.setBounds(25, 265, 530, 125);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 584, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 436, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_39;
  private RiZoneSortie INDETB;
  private JLabel OBJ_60;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_62;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel1;
  private JPanel panel2;
  private JLabel label1;
  private JLabel label2;
  private XRiTextField CDD01;
  private XRiTextField CDC01;
  private XRiTextField CDD02;
  private XRiTextField CDC02;
  private XRiTextField CDD03;
  private XRiTextField CDC03;
  private XRiTextField CDD04;
  private XRiTextField CDC04;
  private XRiTextField CDD05;
  private XRiTextField CDC05;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JPanel panel3;
  private JLabel label8;
  private JLabel label9;
  private XRiTextField CDD06;
  private XRiTextField CDC06;
  private XRiTextField CDD07;
  private XRiTextField CDC07;
  private XRiTextField CDD08;
  private XRiTextField CDC08;
  private XRiTextField CDD09;
  private XRiTextField CDC09;
  private XRiTextField CDD10;
  private XRiTextField CDC10;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JPanel panel4;
  private JLabel label15;
  private JLabel label16;
  private XRiTextField CDD11;
  private XRiTextField CDC11;
  private XRiTextField CDD12;
  private XRiTextField CDC12;
  private XRiTextField CDD13;
  private XRiTextField CDC13;
  private XRiTextField CDD14;
  private XRiTextField CDC14;
  private XRiTextField CDD15;
  private XRiTextField CDC15;
  private JLabel label17;
  private JLabel label18;
  private JLabel label19;
  private JLabel label20;
  private JLabel label21;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
