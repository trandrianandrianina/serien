
package ri.serien.libecranrpg.vgim.VGIM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGIM03FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIM03FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX1.setValeurs("1", "RB");
    TIDX6.setValeurs("6", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX8.setValeurs("8", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX10.setValeurs("10", "RB");
    TIDX7.setValeurs("7", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX9.setValeurs("9", "RB");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // TIDX1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // TIDX6.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("6"));
    // TIDX3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // TIDX8.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("8"));
    // TIDX5.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    // TIDX2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // TIDX10.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("10"));
    // TIDX7.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("7"));
    // TIDX4.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("4"));
    // TIDX9.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("9"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("IMMOBILISATIONS"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TIDX1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (TIDX6.isSelected())
    // lexique.HostFieldPutData("RB", 0, "6");
    // if (TIDX3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (TIDX8.isSelected())
    // lexique.HostFieldPutData("RB", 0, "8");
    // if (TIDX5.isSelected())
    // lexique.HostFieldPutData("RB", 0, "5");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (TIDX10.isSelected())
    // lexique.HostFieldPutData("RB", 0, "10");
    // if (TIDX7.isSelected())
    // lexique.HostFieldPutData("RB", 0, "7");
    // if (TIDX4.isSelected())
    // lexique.HostFieldPutData("RB", 0, "4");
    // if (TIDX9.isSelected())
    // lexique.HostFieldPutData("RB", 0, "9");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX9 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    TIDX10 = new XRiRadioButton();
    TIDX2 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    ARG2 = new XRiTextField();
    ARG3 = new XRiTextField();
    ARG9D = new XRiTextField();
    ARG9M = new XRiTextField();
    ARG4 = new XRiTextField();
    OBJ_33_OBJ_33 = new JLabel();
    ARG10 = new XRiTextField();
    ARG1 = new XRiTextField();
    ARG5A = new XRiTextField();
    ARG6A = new XRiTextField();
    ARG8 = new XRiTextField();
    ARG7 = new XRiTextField();
    WEXT = new XRiTextField();
    OBJ_52_OBJ_52 = new JLabel();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(700, 390));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- TIDX9 ----
          TIDX9.setText("Valeur d'acquisition comprise entre");
          TIDX9.setToolTipText("Tri\u00e9 par");
          TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX9.setName("TIDX9");

          //---- TIDX4 ----
          TIDX4.setText("Groupe, famille, sous-famille");
          TIDX4.setToolTipText("Tri\u00e9 par");
          TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX4.setName("TIDX4");

          //---- TIDX7 ----
          TIDX7.setText("Section");
          TIDX7.setToolTipText("Tri\u00e9 par");
          TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX7.setName("TIDX7");

          //---- TIDX10 ----
          TIDX10.setText("Par fournisseur");
          TIDX10.setToolTipText("Tri\u00e9 par");
          TIDX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX10.setName("TIDX10");

          //---- TIDX2 ----
          TIDX2.setText("Recherche alphab\u00e9tique");
          TIDX2.setToolTipText("Tri\u00e9 par");
          TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2.setName("TIDX2");

          //---- TIDX5 ----
          TIDX5.setText("Date d'acquisition");
          TIDX5.setToolTipText("Tri\u00e9 par");
          TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX5.setName("TIDX5");

          //---- TIDX8 ----
          TIDX8.setText("Service");
          TIDX8.setToolTipText("Tri\u00e9 par");
          TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX8.setName("TIDX8");

          //---- TIDX3 ----
          TIDX3.setText("D\u00e9signation num\u00e9ro 2");
          TIDX3.setToolTipText("Tri\u00e9 par");
          TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX3.setName("TIDX3");

          //---- TIDX6 ----
          TIDX6.setText("Date de sortie");
          TIDX6.setToolTipText("Tri\u00e9 par");
          TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX6.setName("TIDX6");

          //---- TIDX1 ----
          TIDX1.setText("Code immobilisation");
          TIDX1.setToolTipText("Tri\u00e9 par");
          TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX1.setName("TIDX1");

          //---- ARG2 ----
          ARG2.setComponentPopupMenu(BTDA);
          ARG2.setName("ARG2");

          //---- ARG3 ----
          ARG3.setComponentPopupMenu(BTDA);
          ARG3.setName("ARG3");

          //---- ARG9D ----
          ARG9D.setComponentPopupMenu(BTDA);
          ARG9D.setName("ARG9D");

          //---- ARG9M ----
          ARG9M.setComponentPopupMenu(BTDA);
          ARG9M.setName("ARG9M");

          //---- ARG4 ----
          ARG4.setComponentPopupMenu(BTD);
          ARG4.setName("ARG4");

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("Extension");
          OBJ_33_OBJ_33.setComponentPopupMenu(BTD);
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");

          //---- ARG10 ----
          ARG10.setComponentPopupMenu(BTDA);
          ARG10.setName("ARG10");

          //---- ARG1 ----
          ARG1.setComponentPopupMenu(BTDA);
          ARG1.setName("ARG1");

          //---- ARG5A ----
          ARG5A.setComponentPopupMenu(BTDA);
          ARG5A.setName("ARG5A");

          //---- ARG6A ----
          ARG6A.setComponentPopupMenu(BTDA);
          ARG6A.setName("ARG6A");

          //---- ARG8 ----
          ARG8.setComponentPopupMenu(BTD);
          ARG8.setName("ARG8");

          //---- ARG7 ----
          ARG7.setComponentPopupMenu(BTD);
          ARG7.setName("ARG7");

          //---- WEXT ----
          WEXT.setComponentPopupMenu(BTD);
          WEXT.setName("WEXT");

          //---- OBJ_52_OBJ_52 ----
          OBJ_52_OBJ_52.setText("et");
          OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX2, GroupLayout.PREFERRED_SIZE, 174, GroupLayout.PREFERRED_SIZE)
                    .addGap(71, 71, 71)
                    .addComponent(ARG2, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX3, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
                    .addGap(89, 89, 89)
                    .addComponent(ARG3, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX4, GroupLayout.PREFERRED_SIZE, 193, GroupLayout.PREFERRED_SIZE)
                    .addGap(52, 52, 52)
                    .addComponent(ARG4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(WEXT, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX5, GroupLayout.PREFERRED_SIZE, 174, GroupLayout.PREFERRED_SIZE)
                    .addGap(71, 71, 71)
                    .addComponent(ARG5A, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX6, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
                    .addGap(89, 89, 89)
                    .addComponent(ARG6A, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX7, GroupLayout.PREFERRED_SIZE, 193, GroupLayout.PREFERRED_SIZE)
                    .addGap(52, 52, 52)
                    .addComponent(ARG7, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX8, GroupLayout.PREFERRED_SIZE, 174, GroupLayout.PREFERRED_SIZE)
                    .addGap(71, 71, 71)
                    .addComponent(ARG8, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX9, GroupLayout.PREFERRED_SIZE, 234, GroupLayout.PREFERRED_SIZE)
                    .addGap(11, 11, 11)
                    .addComponent(ARG9D, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(ARG9M, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX10, GroupLayout.PREFERRED_SIZE, 193, GroupLayout.PREFERRED_SIZE)
                    .addGap(52, 52, 52)
                    .addComponent(ARG10, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX1, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
                    .addGap(98, 98, 98)
                    .addComponent(ARG1, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WEXT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG5A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX6, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG6A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX7, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX8, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX9, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG9D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_52_OBJ_52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG9M, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 15, 510, 365);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX9);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDX10);
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TIDX8);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX1);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX7;
  private XRiRadioButton TIDX10;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX5;
  private XRiRadioButton TIDX8;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX1;
  private XRiTextField ARG2;
  private XRiTextField ARG3;
  private XRiTextField ARG9D;
  private XRiTextField ARG9M;
  private XRiTextField ARG4;
  private JLabel OBJ_33_OBJ_33;
  private XRiTextField ARG10;
  private XRiTextField ARG1;
  private XRiTextField ARG5A;
  private XRiTextField ARG6A;
  private XRiTextField ARG8;
  private XRiTextField ARG7;
  private XRiTextField WEXT;
  private JLabel OBJ_52_OBJ_52;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
