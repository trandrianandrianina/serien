
package ri.serien.libecranrpg.vgim.VGIMSEFM;
// Nom Fichier: i_VGIMSEFM_FMTB_FMTF1_61.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGIMSEFM_TB extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIMSEFM_TB(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    SEACT.setValeursSelection("OUI", "NON");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITRE@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_37_OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUMR@")).trim());
    OBJ_41_OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIST@")).trim());
    OBJ_78_OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCUSR@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    NCRI.setVisible(lexique.isPresent("NCRI"));
    SEU018.setEnabled(lexique.isPresent("SEU018"));
    SEU019.setEnabled(lexique.isPresent("SEU019"));
    SEU020.setEnabled(lexique.isPresent("SEU020"));
    SEU014.setEnabled(lexique.isPresent("SEU014"));
    SEU016.setEnabled(lexique.isPresent("SEU016"));
    SEU015.setEnabled(lexique.isPresent("SEU015"));
    SEU017.setEnabled(lexique.isPresent("SEU017"));
    SEU011.setEnabled(lexique.isPresent("SEU011"));
    SEU012.setEnabled(lexique.isPresent("SEU012"));
    SEU013.setEnabled(lexique.isPresent("SEU013"));
    SEU009.setEnabled(lexique.isPresent("SEU009"));
    SEU010.setEnabled(lexique.isPresent("SEU010"));
    SEU008.setEnabled(lexique.isPresent("SEU008"));
    SEU007.setEnabled(lexique.isPresent("SEU007"));
    SEU005.setEnabled(lexique.isPresent("SEU005"));
    SEU006.setEnabled(lexique.isPresent("SEU006"));
    SEU004.setEnabled(lexique.isPresent("SEU004"));
    SEU003.setEnabled(lexique.isPresent("SEU003"));
    SEU001.setEnabled(lexique.isPresent("SEU001"));
    SEU002.setEnabled(lexique.isPresent("SEU002"));
    SENS.setVisible(lexique.isPresent("SENS"));
    OBJ_37_OBJ_37.setVisible(lexique.isPresent("SENS"));
    // OBJ_29_OBJ_29.setVisible( lexique.isPresent("V01F"));
    // SEACT.setVisible( lexique.isPresent("SEACT"));
    // SEACT.setSelected(lexique.HostFieldGetData("SEACT").equalsIgnoreCase("OUI"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(lexique.chargerImage("images/logo32.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (SEACT.isSelected())
    // lexique.HostFieldPutData("SEACT", 0, "OUI");
    // else
    // lexique.HostFieldPutData("SEACT", 0, "NON");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F10"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F16"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F18"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgimse"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    l_V01F = new JLabel();
    P_Centre = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    SEACT = new XRiCheckBox();
    OBJ_37_OBJ_37 = new JLabel();
    SENS = new XRiTextField();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_39_OBJ_39 = new JLabel();
    NCRI = new XRiTextField();
    SEU002 = new XRiTextField();
    SEU001 = new XRiTextField();
    SEU003 = new XRiTextField();
    SEU004 = new XRiTextField();
    SEU006 = new XRiTextField();
    SEU005 = new XRiTextField();
    SEU007 = new XRiTextField();
    SEU008 = new XRiTextField();
    SEU010 = new XRiTextField();
    SEU009 = new XRiTextField();
    SEU013 = new XRiTextField();
    SEU012 = new XRiTextField();
    SEU011 = new XRiTextField();
    SEU017 = new XRiTextField();
    SEU015 = new XRiTextField();
    SEU016 = new XRiTextField();
    SEU014 = new XRiTextField();
    SEU020 = new XRiTextField();
    SEU019 = new XRiTextField();
    SEU018 = new XRiTextField();
    OBJ_78_OBJ_78 = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      CMD.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Tri par \u00e9tablissement ou code utilisateur");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Fonctions");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
      CMD.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Cr\u00e9ation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Modification");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      CMD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Interrogation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Annulation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      CMD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      CMD.add(OBJ_14);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITRE@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setName("P_Infos");
        P_Infos.setLayout(null);

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });
        P_Infos.add(bt_Fonctions);
        bt_Fonctions.setBounds(883, 5, 115, bt_Fonctions.getPreferredSize().height);

        //---- l_V01F ----
        l_V01F.setText("@V01F@");
        l_V01F.setFont(new Font("sansserif", Font.BOLD, 12));
        l_V01F.setName("l_V01F");
        P_Infos.add(l_V01F);
        l_V01F.setBounds(21, 9, 80, 20);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < P_Infos.getComponentCount(); i++) {
            Rectangle bounds = P_Infos.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = P_Infos.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          P_Infos.setMinimumSize(preferredSize);
          P_Infos.setPreferredSize(preferredSize);
        }
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //======== xTitledPanel1 ========
      {
        xTitledPanel1.setTitle("S\u00e9curit\u00e9");
        xTitledPanel1.setName("xTitledPanel1");
        Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
        xTitledPanel1ContentContainer.setLayout(null);

        //---- SEACT ----
        SEACT.setText("S\u00e9curit\u00e9 Active");
        SEACT.setComponentPopupMenu(BTD);
        SEACT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        SEACT.setName("SEACT");
        xTitledPanel1ContentContainer.add(SEACT);
        SEACT.setBounds(10, 10, 177, 20);

        //---- OBJ_37_OBJ_37 ----
        OBJ_37_OBJ_37.setText("@NUMR@");
        OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
        xTitledPanel1ContentContainer.add(OBJ_37_OBJ_37);
        OBJ_37_OBJ_37.setBounds(10, 35, 148, 20);

        //---- SENS ----
        SENS.setComponentPopupMenu(BTD);
        SENS.setName("SENS");
        xTitledPanel1ContentContainer.add(SENS);
        SENS.setBounds(255, 35, 76, SENS.getPreferredSize().height);

        //---- OBJ_41_OBJ_41 ----
        OBJ_41_OBJ_41.setText("@LIST@");
        OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
        xTitledPanel1ContentContainer.add(OBJ_41_OBJ_41);
        OBJ_41_OBJ_41.setBounds(10, 65, 537, 20);

        //---- OBJ_39_OBJ_39 ----
        OBJ_39_OBJ_39.setText("Num\u00e9ro de contr\u00f4le RI");
        OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");
        xTitledPanel1ContentContainer.add(OBJ_39_OBJ_39);
        OBJ_39_OBJ_39.setBounds(10, 65, 156, 20);

        //---- NCRI ----
        NCRI.setComponentPopupMenu(BTD);
        NCRI.setName("NCRI");
        xTitledPanel1ContentContainer.add(NCRI);
        NCRI.setBounds(320, 40, 58, 20);

        //---- SEU002 ----
        SEU002.setComponentPopupMenu(BTD);
        SEU002.setName("SEU002");
        xTitledPanel1ContentContainer.add(SEU002);
        SEU002.setBounds(10, 90, 110, SEU002.getPreferredSize().height);

        //---- SEU001 ----
        SEU001.setComponentPopupMenu(BTD);
        SEU001.setName("SEU001");
        xTitledPanel1ContentContainer.add(SEU001);
        SEU001.setBounds(135, 90, 110, SEU001.getPreferredSize().height);

        //---- SEU003 ----
        SEU003.setComponentPopupMenu(BTD);
        SEU003.setName("SEU003");
        xTitledPanel1ContentContainer.add(SEU003);
        SEU003.setBounds(255, 90, 110, SEU003.getPreferredSize().height);

        //---- SEU004 ----
        SEU004.setComponentPopupMenu(BTD);
        SEU004.setName("SEU004");
        xTitledPanel1ContentContainer.add(SEU004);
        SEU004.setBounds(380, 90, 110, SEU004.getPreferredSize().height);

        //---- SEU006 ----
        SEU006.setComponentPopupMenu(BTD);
        SEU006.setName("SEU006");
        xTitledPanel1ContentContainer.add(SEU006);
        SEU006.setBounds(500, 90, 110, SEU006.getPreferredSize().height);

        //---- SEU005 ----
        SEU005.setComponentPopupMenu(BTD);
        SEU005.setName("SEU005");
        xTitledPanel1ContentContainer.add(SEU005);
        SEU005.setBounds(10, 118, 110, SEU005.getPreferredSize().height);

        //---- SEU007 ----
        SEU007.setComponentPopupMenu(BTD);
        SEU007.setName("SEU007");
        xTitledPanel1ContentContainer.add(SEU007);
        SEU007.setBounds(135, 118, 110, SEU007.getPreferredSize().height);

        //---- SEU008 ----
        SEU008.setComponentPopupMenu(BTD);
        SEU008.setName("SEU008");
        xTitledPanel1ContentContainer.add(SEU008);
        SEU008.setBounds(255, 118, 110, SEU008.getPreferredSize().height);

        //---- SEU010 ----
        SEU010.setComponentPopupMenu(BTD);
        SEU010.setName("SEU010");
        xTitledPanel1ContentContainer.add(SEU010);
        SEU010.setBounds(380, 118, 110, SEU010.getPreferredSize().height);

        //---- SEU009 ----
        SEU009.setComponentPopupMenu(BTD);
        SEU009.setName("SEU009");
        xTitledPanel1ContentContainer.add(SEU009);
        SEU009.setBounds(500, 118, 110, SEU009.getPreferredSize().height);

        //---- SEU013 ----
        SEU013.setComponentPopupMenu(BTD);
        SEU013.setName("SEU013");
        xTitledPanel1ContentContainer.add(SEU013);
        SEU013.setBounds(10, 146, 110, SEU013.getPreferredSize().height);

        //---- SEU012 ----
        SEU012.setComponentPopupMenu(BTD);
        SEU012.setName("SEU012");
        xTitledPanel1ContentContainer.add(SEU012);
        SEU012.setBounds(135, 146, 110, SEU012.getPreferredSize().height);

        //---- SEU011 ----
        SEU011.setComponentPopupMenu(BTD);
        SEU011.setName("SEU011");
        xTitledPanel1ContentContainer.add(SEU011);
        SEU011.setBounds(255, 146, 110, SEU011.getPreferredSize().height);

        //---- SEU017 ----
        SEU017.setComponentPopupMenu(BTD);
        SEU017.setName("SEU017");
        xTitledPanel1ContentContainer.add(SEU017);
        SEU017.setBounds(380, 146, 110, SEU017.getPreferredSize().height);

        //---- SEU015 ----
        SEU015.setComponentPopupMenu(BTD);
        SEU015.setName("SEU015");
        xTitledPanel1ContentContainer.add(SEU015);
        SEU015.setBounds(500, 146, 110, SEU015.getPreferredSize().height);

        //---- SEU016 ----
        SEU016.setComponentPopupMenu(BTD);
        SEU016.setName("SEU016");
        xTitledPanel1ContentContainer.add(SEU016);
        SEU016.setBounds(10, 174, 110, SEU016.getPreferredSize().height);

        //---- SEU014 ----
        SEU014.setComponentPopupMenu(BTD);
        SEU014.setName("SEU014");
        xTitledPanel1ContentContainer.add(SEU014);
        SEU014.setBounds(135, 174, 110, SEU014.getPreferredSize().height);

        //---- SEU020 ----
        SEU020.setComponentPopupMenu(BTD);
        SEU020.setName("SEU020");
        xTitledPanel1ContentContainer.add(SEU020);
        SEU020.setBounds(255, 174, 110, SEU020.getPreferredSize().height);

        //---- SEU019 ----
        SEU019.setComponentPopupMenu(BTD);
        SEU019.setName("SEU019");
        xTitledPanel1ContentContainer.add(SEU019);
        SEU019.setBounds(380, 174, 110, SEU019.getPreferredSize().height);

        //---- SEU018 ----
        SEU018.setComponentPopupMenu(BTD);
        SEU018.setName("SEU018");
        xTitledPanel1ContentContainer.add(SEU018);
        SEU018.setBounds(500, 174, 110, SEU018.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
            Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = xTitledPanel1ContentContainer.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
          xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
        }
      }
      P_Centre.add(xTitledPanel1);
      xTitledPanel1.setBounds(15, 10, 635, 250);

      //---- OBJ_78_OBJ_78 ----
      OBJ_78_OBJ_78.setText("@LOCUSR@");
      OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");
      P_Centre.add(OBJ_78_OBJ_78);
      OBJ_78_OBJ_78.setBounds(845, 10, 138, 16);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel l_V01F;
  private JPanel P_Centre;
  private JXTitledPanel xTitledPanel1;
  private XRiCheckBox SEACT;
  private JLabel OBJ_37_OBJ_37;
  private XRiTextField SENS;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_39_OBJ_39;
  private XRiTextField NCRI;
  private XRiTextField SEU002;
  private XRiTextField SEU001;
  private XRiTextField SEU003;
  private XRiTextField SEU004;
  private XRiTextField SEU006;
  private XRiTextField SEU005;
  private XRiTextField SEU007;
  private XRiTextField SEU008;
  private XRiTextField SEU010;
  private XRiTextField SEU009;
  private XRiTextField SEU013;
  private XRiTextField SEU012;
  private XRiTextField SEU011;
  private XRiTextField SEU017;
  private XRiTextField SEU015;
  private XRiTextField SEU016;
  private XRiTextField SEU014;
  private XRiTextField SEU020;
  private XRiTextField SEU019;
  private XRiTextField SEU018;
  private JLabel OBJ_78_OBJ_78;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
