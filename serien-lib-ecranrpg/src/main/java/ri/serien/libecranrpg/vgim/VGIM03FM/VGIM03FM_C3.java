
package ri.serien.libecranrpg.vgim.VGIM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGIM03FM_C3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIM03FM_C3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    IMREN.setEnabled(lexique.isPresent("IMREN"));
    IMDEG.setEnabled(lexique.isPresent("IMDEG"));
    UIMNDR.setEnabled(lexique.isPresent("UIMNDR"));
    UIMNDD.setEnabled(lexique.isPresent("UIMNDD"));
    UIMNDA.setEnabled(lexique.isPresent("UIMNDA"));
    IMNDR.setEnabled(lexique.isPresent("IMNDR"));
    IMNDD.setEnabled(lexique.isPresent("IMNDD"));
    IMNDA.setEnabled(lexique.isPresent("IMNDA"));
    IMDUE.setEnabled(lexique.isPresent("IMDUE"));
    // IMDFEX.setEnabled( lexique.isPresent("IMDFEX"));
    IMEMR.setEnabled(lexique.isPresent("IMEMR"));
    IMEVR.setEnabled(lexique.isPresent("IMEVR"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("IMMOBILISATION"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_32_OBJ_32 = new JLabel();
    OBJ_38_OBJ_38 = new JLabel();
    OBJ_34_OBJ_34 = new JLabel();
    OBJ_52_OBJ_52 = new JLabel();
    OBJ_46_OBJ_46 = new JLabel();
    IMEVR = new XRiTextField();
    IMEMR = new XRiTextField();
    OBJ_36_OBJ_36 = new JLabel();
    OBJ_48_OBJ_48 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_43_OBJ_43 = new JLabel();
    IMDFEX = new XRiCalendrier();
    IMDUE = new XRiTextField();
    IMNDA = new XRiTextField();
    IMNDD = new XRiTextField();
    IMNDR = new XRiTextField();
    UIMNDA = new XRiTextField();
    UIMNDD = new XRiTextField();
    UIMNDR = new XRiTextField();
    IMDEG = new XRiTextField();
    IMREN = new XRiTextField();
    xTitledSeparator2 = new JXTitledSeparator();
    xTitledSeparator3 = new JXTitledSeparator();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(690, 290));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Amortissement \u00e9conomique"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Amortissement d\u00e9rogatoire");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          panel1.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(35, 45, 165, 20);

          //---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("Valeur estim\u00e9e reprise");
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");
          panel1.add(OBJ_38_OBJ_38);
          OBJ_38_OBJ_38.setBounds(218, 85, 138, 20);

          //---- OBJ_34_OBJ_34 ----
          OBJ_34_OBJ_34.setText("Fin d'amortissement");
          OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
          panel1.add(OBJ_34_OBJ_34);
          OBJ_34_OBJ_34.setBounds(283, 50, 123, 20);

          //---- OBJ_52_OBJ_52 ----
          OBJ_52_OBJ_52.setText("Valeurs / d\u00e9faut");
          OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");
          panel1.add(OBJ_52_OBJ_52);
          OBJ_52_OBJ_52.setBounds(48, 230, 96, 20);

          //---- OBJ_46_OBJ_46 ----
          OBJ_46_OBJ_46.setText("Amortissement");
          OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");
          panel1.add(OBJ_46_OBJ_46);
          OBJ_46_OBJ_46.setBounds(48, 200, 92, 20);

          //---- IMEVR ----
          IMEVR.setComponentPopupMenu(BTD);
          IMEVR.setName("IMEVR");
          panel1.add(IMEVR);
          IMEVR.setBounds(365, 80, 108, IMEVR.getPreferredSize().height);

          //---- IMEMR ----
          IMEMR.setComponentPopupMenu(BTDA);
          IMEMR.setName("IMEMR");
          panel1.add(IMEMR);
          IMEMR.setBounds(103, 135, 108, IMEMR.getPreferredSize().height);

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("Dur\u00e9e");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");
          panel1.add(OBJ_36_OBJ_36);
          OBJ_36_OBJ_36.setBounds(48, 85, 58, 20);

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("Dotation");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
          panel1.add(OBJ_48_OBJ_48);
          OBJ_48_OBJ_48.setBounds(218, 200, 53, 20);

          //---- OBJ_50_OBJ_50 ----
          OBJ_50_OBJ_50.setText("Reprise");
          OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
          panel1.add(OBJ_50_OBJ_50);
          OBJ_50_OBJ_50.setBounds(353, 200, 51, 20);

          //---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("Montant");
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
          panel1.add(OBJ_41_OBJ_41);
          OBJ_41_OBJ_41.setBounds(48, 135, 50, 20);

          //---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("Mode");
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
          panel1.add(OBJ_43_OBJ_43);
          OBJ_43_OBJ_43.setBounds(283, 140, 38, 20);

          //---- IMDFEX ----
          IMDFEX.setComponentPopupMenu(BTDA);
          IMDFEX.setTypeSaisie(6);
          IMDFEX.setName("IMDFEX");
          panel1.add(IMDFEX);
          IMDFEX.setBounds(418, 45, 90, IMDFEX.getPreferredSize().height);

          //---- IMDUE ----
          IMDUE.setComponentPopupMenu(BTD);
          IMDUE.setName("IMDUE");
          panel1.add(IMDUE);
          IMDUE.setBounds(103, 80, 30, IMDUE.getPreferredSize().height);

          //---- IMNDA ----
          IMNDA.setComponentPopupMenu(BTD);
          IMNDA.setName("IMNDA");
          panel1.add(IMNDA);
          IMNDA.setBounds(148, 200, 36, IMNDA.getPreferredSize().height);

          //---- IMNDD ----
          IMNDD.setComponentPopupMenu(BTD);
          IMNDD.setName("IMNDD");
          panel1.add(IMNDD);
          IMNDD.setBounds(283, 200, 36, IMNDD.getPreferredSize().height);

          //---- IMNDR ----
          IMNDR.setComponentPopupMenu(BTD);
          IMNDR.setName("IMNDR");
          panel1.add(IMNDR);
          IMNDR.setBounds(433, 200, 36, IMNDR.getPreferredSize().height);

          //---- UIMNDA ----
          UIMNDA.setComponentPopupMenu(BTDA);
          UIMNDA.setName("UIMNDA");
          panel1.add(UIMNDA);
          UIMNDA.setBounds(148, 230, 36, UIMNDA.getPreferredSize().height);

          //---- UIMNDD ----
          UIMNDD.setComponentPopupMenu(BTDA);
          UIMNDD.setName("UIMNDD");
          panel1.add(UIMNDD);
          UIMNDD.setBounds(283, 230, 36, UIMNDD.getPreferredSize().height);

          //---- UIMNDR ----
          UIMNDR.setComponentPopupMenu(BTDA);
          UIMNDR.setName("UIMNDR");
          panel1.add(UIMNDR);
          UIMNDR.setBounds(433, 230, 36, UIMNDR.getPreferredSize().height);

          //---- IMDEG ----
          IMDEG.setComponentPopupMenu(BTDA);
          IMDEG.setName("IMDEG");
          panel1.add(IMDEG);
          IMDEG.setBounds(195, 40, 24, IMDEG.getPreferredSize().height);

          //---- IMREN ----
          IMREN.setComponentPopupMenu(BTDA);
          IMREN.setName("IMREN");
          panel1.add(IMREN);
          IMREN.setBounds(358, 135, 24, IMREN.getPreferredSize().height);

          //---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("Reprise");
          xTitledSeparator2.setName("xTitledSeparator2");
          panel1.add(xTitledSeparator2);
          xTitledSeparator2.setBounds(25, 110, 510, 20);

          //---- xTitledSeparator3 ----
          xTitledSeparator3.setTitle("Comptabilisation");
          xTitledSeparator3.setName("xTitledSeparator3");
          panel1.add(xTitledSeparator3);
          xTitledSeparator3.setBounds(23, 175, 445, xTitledSeparator3.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 570, 310);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_32_OBJ_32;
  private JLabel OBJ_38_OBJ_38;
  private JLabel OBJ_34_OBJ_34;
  private JLabel OBJ_52_OBJ_52;
  private JLabel OBJ_46_OBJ_46;
  private XRiTextField IMEVR;
  private XRiTextField IMEMR;
  private JLabel OBJ_36_OBJ_36;
  private JLabel OBJ_48_OBJ_48;
  private JLabel OBJ_50_OBJ_50;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_43_OBJ_43;
  private XRiCalendrier IMDFEX;
  private XRiTextField IMDUE;
  private XRiTextField IMNDA;
  private XRiTextField IMNDD;
  private XRiTextField IMNDR;
  private XRiTextField UIMNDA;
  private XRiTextField UIMNDD;
  private XRiTextField UIMNDR;
  private XRiTextField IMDEG;
  private XRiTextField IMREN;
  private JXTitledSeparator xTitledSeparator2;
  private JXTitledSeparator xTitledSeparator3;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
