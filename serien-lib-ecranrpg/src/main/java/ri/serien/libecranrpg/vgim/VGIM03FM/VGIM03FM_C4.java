
package ri.serien.libecranrpg.vgim.VGIM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGIM03FM_C4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIM03FM_C4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA01@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA02@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA03@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA04@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA05@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA06@")).trim());
    label13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA07@")).trim());
    label15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA08@")).trim());
    label17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA09@")).trim());
    label19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA10@")).trim());
    label21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA11@")).trim());
    label23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA12@")).trim());
    label25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA13@")).trim());
    label27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA14@")).trim());
    label29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA15@")).trim());
    label31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA16@")).trim());
    label33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA17@")).trim());
    label35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA18@")).trim());
    label37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA19@")).trim());
    label39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA20@")).trim());
    label41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA21@")).trim());
    label43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA22@")).trim());
    label45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA23@")).trim());
    label47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA24@")).trim());
    label49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA25@")).trim());
    label51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA26@")).trim());
    label53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA27@")).trim());
    label55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA28@")).trim());
    label57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA29@")).trim());
    label59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA30@")).trim());
    label61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA31@")).trim());
    label63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA32@")).trim());
    label65.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA33@")).trim());
    label67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA34@")).trim());
    label69.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA35@")).trim());
    label71.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA36@")).trim());
    label73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA37@")).trim());
    label75.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA38@")).trim());
    label77.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA39@")).trim());
    label79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA40@")).trim());
    label81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA41@")).trim());
    label83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA42@")).trim());
    label85.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA43@")).trim());
    label87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA44@")).trim());
    label89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA45@")).trim());
    label91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA46@")).trim());
    label93.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA47@")).trim());
    label95.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA48@")).trim());
    label97.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA49@")).trim());
    label99.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA50@")).trim());
    label101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITA51@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("IMMOBILISATION"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    IMA01 = new XRiTextField();
    IMA02 = new XRiTextField();
    label3 = new JLabel();
    label4 = new JLabel();
    IMA03 = new XRiTextField();
    label5 = new JLabel();
    label6 = new JLabel();
    IMA04 = new XRiTextField();
    label7 = new JLabel();
    label8 = new JLabel();
    IMA05 = new XRiTextField();
    label9 = new JLabel();
    label10 = new JLabel();
    IMA06 = new XRiTextField();
    label11 = new JLabel();
    label12 = new JLabel();
    IMA07 = new XRiTextField();
    label13 = new JLabel();
    label14 = new JLabel();
    IMA08 = new XRiTextField();
    label15 = new JLabel();
    label16 = new JLabel();
    IMA09 = new XRiTextField();
    label17 = new JLabel();
    label18 = new JLabel();
    IMA10 = new XRiTextField();
    label19 = new JLabel();
    label20 = new JLabel();
    IMA11 = new XRiTextField();
    label21 = new JLabel();
    label22 = new JLabel();
    IMA12 = new XRiTextField();
    label23 = new JLabel();
    label24 = new JLabel();
    IMA13 = new XRiTextField();
    label25 = new JLabel();
    label26 = new JLabel();
    IMA14 = new XRiTextField();
    label27 = new JLabel();
    label28 = new JLabel();
    IMA15 = new XRiTextField();
    label29 = new JLabel();
    label30 = new JLabel();
    IMA16 = new XRiTextField();
    label31 = new JLabel();
    label32 = new JLabel();
    IMA17 = new XRiTextField();
    label33 = new JLabel();
    label34 = new JLabel();
    IMA18 = new XRiTextField();
    label35 = new JLabel();
    label36 = new JLabel();
    IMA19 = new XRiTextField();
    label37 = new JLabel();
    label38 = new JLabel();
    IMA20 = new XRiTextField();
    label39 = new JLabel();
    label40 = new JLabel();
    IMA21 = new XRiTextField();
    label41 = new JLabel();
    label42 = new JLabel();
    IMA22 = new XRiTextField();
    label43 = new JLabel();
    label44 = new JLabel();
    IMA23 = new XRiTextField();
    label45 = new JLabel();
    label46 = new JLabel();
    IMA24 = new XRiTextField();
    label47 = new JLabel();
    label48 = new JLabel();
    IMA25 = new XRiTextField();
    label49 = new JLabel();
    label50 = new JLabel();
    IMA26 = new XRiTextField();
    label51 = new JLabel();
    label52 = new JLabel();
    IMA27 = new XRiTextField();
    label53 = new JLabel();
    label54 = new JLabel();
    IMA28 = new XRiTextField();
    label55 = new JLabel();
    label56 = new JLabel();
    IMA29 = new XRiTextField();
    label57 = new JLabel();
    label58 = new JLabel();
    IMA30 = new XRiTextField();
    label59 = new JLabel();
    label60 = new JLabel();
    IMA31 = new XRiTextField();
    label61 = new JLabel();
    label62 = new JLabel();
    IMA32 = new XRiTextField();
    label63 = new JLabel();
    label64 = new JLabel();
    IMA33 = new XRiTextField();
    label65 = new JLabel();
    label66 = new JLabel();
    IMA34 = new XRiTextField();
    label67 = new JLabel();
    label68 = new JLabel();
    IMA35 = new XRiTextField();
    label69 = new JLabel();
    label70 = new JLabel();
    IMA36 = new XRiTextField();
    label71 = new JLabel();
    label72 = new JLabel();
    IMA37 = new XRiTextField();
    label73 = new JLabel();
    label74 = new JLabel();
    IMA38 = new XRiTextField();
    label75 = new JLabel();
    label76 = new JLabel();
    IMA39 = new XRiTextField();
    label77 = new JLabel();
    label78 = new JLabel();
    IMA40 = new XRiTextField();
    label79 = new JLabel();
    label80 = new JLabel();
    IMA41 = new XRiTextField();
    label81 = new JLabel();
    label82 = new JLabel();
    IMA42 = new XRiTextField();
    label83 = new JLabel();
    label84 = new JLabel();
    IMA43 = new XRiTextField();
    label85 = new JLabel();
    label86 = new JLabel();
    IMA44 = new XRiTextField();
    label87 = new JLabel();
    label88 = new JLabel();
    IMA45 = new XRiTextField();
    label89 = new JLabel();
    label90 = new JLabel();
    IMA46 = new XRiTextField();
    label91 = new JLabel();
    label92 = new JLabel();
    IMA47 = new XRiTextField();
    label93 = new JLabel();
    label94 = new JLabel();
    IMA48 = new XRiTextField();
    label95 = new JLabel();
    label96 = new JLabel();
    IMA49 = new XRiTextField();
    label97 = new JLabel();
    label98 = new JLabel();
    IMA50 = new XRiTextField();
    label99 = new JLabel();
    label100 = new JLabel();
    IMA51 = new XRiTextField();
    label101 = new JLabel();
    label102 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(815, 590));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Annuit\u00e9s d'amortissement fiscal"));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- label1 ----
          label1.setText("01");
          label1.setHorizontalAlignment(SwingConstants.RIGHT);
          label1.setName("label1");

          //---- label2 ----
          label2.setText("@ITA01@");
          label2.setName("label2");

          //---- IMA01 ----
          IMA01.setName("IMA01");

          //---- IMA02 ----
          IMA02.setName("IMA02");

          //---- label3 ----
          label3.setText("@ITA02@");
          label3.setName("label3");

          //---- label4 ----
          label4.setText("02");
          label4.setHorizontalAlignment(SwingConstants.RIGHT);
          label4.setName("label4");

          //---- IMA03 ----
          IMA03.setName("IMA03");

          //---- label5 ----
          label5.setText("@ITA03@");
          label5.setName("label5");

          //---- label6 ----
          label6.setText("03");
          label6.setHorizontalAlignment(SwingConstants.RIGHT);
          label6.setName("label6");

          //---- IMA04 ----
          IMA04.setName("IMA04");

          //---- label7 ----
          label7.setText("@ITA04@");
          label7.setName("label7");

          //---- label8 ----
          label8.setText("04");
          label8.setHorizontalAlignment(SwingConstants.RIGHT);
          label8.setName("label8");

          //---- IMA05 ----
          IMA05.setName("IMA05");

          //---- label9 ----
          label9.setText("@ITA05@");
          label9.setName("label9");

          //---- label10 ----
          label10.setText("05");
          label10.setHorizontalAlignment(SwingConstants.RIGHT);
          label10.setName("label10");

          //---- IMA06 ----
          IMA06.setName("IMA06");

          //---- label11 ----
          label11.setText("@ITA06@");
          label11.setName("label11");

          //---- label12 ----
          label12.setText("06");
          label12.setHorizontalAlignment(SwingConstants.RIGHT);
          label12.setName("label12");

          //---- IMA07 ----
          IMA07.setName("IMA07");

          //---- label13 ----
          label13.setText("@ITA07@");
          label13.setName("label13");

          //---- label14 ----
          label14.setText("07");
          label14.setHorizontalAlignment(SwingConstants.RIGHT);
          label14.setName("label14");

          //---- IMA08 ----
          IMA08.setName("IMA08");

          //---- label15 ----
          label15.setText("@ITA08@");
          label15.setName("label15");

          //---- label16 ----
          label16.setText("08");
          label16.setHorizontalAlignment(SwingConstants.RIGHT);
          label16.setName("label16");

          //---- IMA09 ----
          IMA09.setName("IMA09");

          //---- label17 ----
          label17.setText("@ITA09@");
          label17.setName("label17");

          //---- label18 ----
          label18.setText("09");
          label18.setHorizontalAlignment(SwingConstants.RIGHT);
          label18.setName("label18");

          //---- IMA10 ----
          IMA10.setName("IMA10");

          //---- label19 ----
          label19.setText("@ITA10@");
          label19.setName("label19");

          //---- label20 ----
          label20.setText("10");
          label20.setHorizontalAlignment(SwingConstants.RIGHT);
          label20.setName("label20");

          //---- IMA11 ----
          IMA11.setName("IMA11");

          //---- label21 ----
          label21.setText("@ITA11@");
          label21.setName("label21");

          //---- label22 ----
          label22.setText("11");
          label22.setHorizontalAlignment(SwingConstants.RIGHT);
          label22.setName("label22");

          //---- IMA12 ----
          IMA12.setName("IMA12");

          //---- label23 ----
          label23.setText("@ITA12@");
          label23.setName("label23");

          //---- label24 ----
          label24.setText("12");
          label24.setHorizontalAlignment(SwingConstants.RIGHT);
          label24.setName("label24");

          //---- IMA13 ----
          IMA13.setName("IMA13");

          //---- label25 ----
          label25.setText("@ITA13@");
          label25.setName("label25");

          //---- label26 ----
          label26.setText("13");
          label26.setHorizontalAlignment(SwingConstants.RIGHT);
          label26.setName("label26");

          //---- IMA14 ----
          IMA14.setName("IMA14");

          //---- label27 ----
          label27.setText("@ITA14@");
          label27.setName("label27");

          //---- label28 ----
          label28.setText("14");
          label28.setHorizontalAlignment(SwingConstants.RIGHT);
          label28.setName("label28");

          //---- IMA15 ----
          IMA15.setName("IMA15");

          //---- label29 ----
          label29.setText("@ITA15@");
          label29.setName("label29");

          //---- label30 ----
          label30.setText("15");
          label30.setHorizontalAlignment(SwingConstants.RIGHT);
          label30.setName("label30");

          //---- IMA16 ----
          IMA16.setName("IMA16");

          //---- label31 ----
          label31.setText("@ITA16@");
          label31.setName("label31");

          //---- label32 ----
          label32.setText("16");
          label32.setHorizontalAlignment(SwingConstants.RIGHT);
          label32.setName("label32");

          //---- IMA17 ----
          IMA17.setName("IMA17");

          //---- label33 ----
          label33.setText("@ITA17@");
          label33.setName("label33");

          //---- label34 ----
          label34.setText("17");
          label34.setHorizontalAlignment(SwingConstants.RIGHT);
          label34.setName("label34");

          //---- IMA18 ----
          IMA18.setName("IMA18");

          //---- label35 ----
          label35.setText("@ITA18@");
          label35.setName("label35");

          //---- label36 ----
          label36.setText("18");
          label36.setHorizontalAlignment(SwingConstants.RIGHT);
          label36.setName("label36");

          //---- IMA19 ----
          IMA19.setName("IMA19");

          //---- label37 ----
          label37.setText("@ITA19@");
          label37.setName("label37");

          //---- label38 ----
          label38.setText("19");
          label38.setHorizontalAlignment(SwingConstants.RIGHT);
          label38.setName("label38");

          //---- IMA20 ----
          IMA20.setName("IMA20");

          //---- label39 ----
          label39.setText("@ITA20@");
          label39.setName("label39");

          //---- label40 ----
          label40.setText("20");
          label40.setHorizontalAlignment(SwingConstants.RIGHT);
          label40.setName("label40");

          //---- IMA21 ----
          IMA21.setName("IMA21");

          //---- label41 ----
          label41.setText("@ITA21@");
          label41.setName("label41");

          //---- label42 ----
          label42.setText("21");
          label42.setHorizontalAlignment(SwingConstants.RIGHT);
          label42.setName("label42");

          //---- IMA22 ----
          IMA22.setName("IMA22");

          //---- label43 ----
          label43.setText("@ITA22@");
          label43.setName("label43");

          //---- label44 ----
          label44.setText("22");
          label44.setHorizontalAlignment(SwingConstants.RIGHT);
          label44.setName("label44");

          //---- IMA23 ----
          IMA23.setName("IMA23");

          //---- label45 ----
          label45.setText("@ITA23@");
          label45.setName("label45");

          //---- label46 ----
          label46.setText("23");
          label46.setHorizontalAlignment(SwingConstants.RIGHT);
          label46.setName("label46");

          //---- IMA24 ----
          IMA24.setName("IMA24");

          //---- label47 ----
          label47.setText("@ITA24@");
          label47.setName("label47");

          //---- label48 ----
          label48.setText("24");
          label48.setHorizontalAlignment(SwingConstants.RIGHT);
          label48.setName("label48");

          //---- IMA25 ----
          IMA25.setName("IMA25");

          //---- label49 ----
          label49.setText("@ITA25@");
          label49.setName("label49");

          //---- label50 ----
          label50.setText("25");
          label50.setHorizontalAlignment(SwingConstants.RIGHT);
          label50.setName("label50");

          //---- IMA26 ----
          IMA26.setName("IMA26");

          //---- label51 ----
          label51.setText("@ITA26@");
          label51.setName("label51");

          //---- label52 ----
          label52.setText("26");
          label52.setHorizontalAlignment(SwingConstants.RIGHT);
          label52.setName("label52");

          //---- IMA27 ----
          IMA27.setName("IMA27");

          //---- label53 ----
          label53.setText("@ITA27@");
          label53.setName("label53");

          //---- label54 ----
          label54.setText("27");
          label54.setHorizontalAlignment(SwingConstants.RIGHT);
          label54.setName("label54");

          //---- IMA28 ----
          IMA28.setName("IMA28");

          //---- label55 ----
          label55.setText("@ITA28@");
          label55.setName("label55");

          //---- label56 ----
          label56.setText("28");
          label56.setHorizontalAlignment(SwingConstants.RIGHT);
          label56.setName("label56");

          //---- IMA29 ----
          IMA29.setName("IMA29");

          //---- label57 ----
          label57.setText("@ITA29@");
          label57.setName("label57");

          //---- label58 ----
          label58.setText("29");
          label58.setHorizontalAlignment(SwingConstants.RIGHT);
          label58.setName("label58");

          //---- IMA30 ----
          IMA30.setName("IMA30");

          //---- label59 ----
          label59.setText("@ITA30@");
          label59.setName("label59");

          //---- label60 ----
          label60.setText("30");
          label60.setHorizontalAlignment(SwingConstants.RIGHT);
          label60.setName("label60");

          //---- IMA31 ----
          IMA31.setName("IMA31");

          //---- label61 ----
          label61.setText("@ITA31@");
          label61.setName("label61");

          //---- label62 ----
          label62.setText("31");
          label62.setHorizontalAlignment(SwingConstants.RIGHT);
          label62.setName("label62");

          //---- IMA32 ----
          IMA32.setName("IMA32");

          //---- label63 ----
          label63.setText("@ITA32@");
          label63.setName("label63");

          //---- label64 ----
          label64.setText("32");
          label64.setHorizontalAlignment(SwingConstants.RIGHT);
          label64.setName("label64");

          //---- IMA33 ----
          IMA33.setName("IMA33");

          //---- label65 ----
          label65.setText("@ITA33@");
          label65.setName("label65");

          //---- label66 ----
          label66.setText("33");
          label66.setHorizontalAlignment(SwingConstants.RIGHT);
          label66.setName("label66");

          //---- IMA34 ----
          IMA34.setName("IMA34");

          //---- label67 ----
          label67.setText("@ITA34@");
          label67.setName("label67");

          //---- label68 ----
          label68.setText("34");
          label68.setHorizontalAlignment(SwingConstants.RIGHT);
          label68.setName("label68");

          //---- IMA35 ----
          IMA35.setName("IMA35");

          //---- label69 ----
          label69.setText("@ITA35@");
          label69.setName("label69");

          //---- label70 ----
          label70.setText("35");
          label70.setHorizontalAlignment(SwingConstants.RIGHT);
          label70.setName("label70");

          //---- IMA36 ----
          IMA36.setName("IMA36");

          //---- label71 ----
          label71.setText("@ITA36@");
          label71.setName("label71");

          //---- label72 ----
          label72.setText("36");
          label72.setHorizontalAlignment(SwingConstants.RIGHT);
          label72.setName("label72");

          //---- IMA37 ----
          IMA37.setName("IMA37");

          //---- label73 ----
          label73.setText("@ITA37@");
          label73.setName("label73");

          //---- label74 ----
          label74.setText("37");
          label74.setHorizontalAlignment(SwingConstants.RIGHT);
          label74.setName("label74");

          //---- IMA38 ----
          IMA38.setName("IMA38");

          //---- label75 ----
          label75.setText("@ITA38@");
          label75.setName("label75");

          //---- label76 ----
          label76.setText("38");
          label76.setHorizontalAlignment(SwingConstants.RIGHT);
          label76.setName("label76");

          //---- IMA39 ----
          IMA39.setName("IMA39");

          //---- label77 ----
          label77.setText("@ITA39@");
          label77.setName("label77");

          //---- label78 ----
          label78.setText("39");
          label78.setHorizontalAlignment(SwingConstants.RIGHT);
          label78.setName("label78");

          //---- IMA40 ----
          IMA40.setName("IMA40");

          //---- label79 ----
          label79.setText("@ITA40@");
          label79.setName("label79");

          //---- label80 ----
          label80.setText("40");
          label80.setHorizontalAlignment(SwingConstants.RIGHT);
          label80.setName("label80");

          //---- IMA41 ----
          IMA41.setName("IMA41");

          //---- label81 ----
          label81.setText("@ITA41@");
          label81.setName("label81");

          //---- label82 ----
          label82.setText("41");
          label82.setHorizontalAlignment(SwingConstants.RIGHT);
          label82.setName("label82");

          //---- IMA42 ----
          IMA42.setName("IMA42");

          //---- label83 ----
          label83.setText("@ITA42@");
          label83.setName("label83");

          //---- label84 ----
          label84.setText("42");
          label84.setHorizontalAlignment(SwingConstants.RIGHT);
          label84.setName("label84");

          //---- IMA43 ----
          IMA43.setName("IMA43");

          //---- label85 ----
          label85.setText("@ITA43@");
          label85.setName("label85");

          //---- label86 ----
          label86.setText("43");
          label86.setHorizontalAlignment(SwingConstants.RIGHT);
          label86.setName("label86");

          //---- IMA44 ----
          IMA44.setName("IMA44");

          //---- label87 ----
          label87.setText("@ITA44@");
          label87.setName("label87");

          //---- label88 ----
          label88.setText("44");
          label88.setHorizontalAlignment(SwingConstants.RIGHT);
          label88.setName("label88");

          //---- IMA45 ----
          IMA45.setName("IMA45");

          //---- label89 ----
          label89.setText("@ITA45@");
          label89.setName("label89");

          //---- label90 ----
          label90.setText("45");
          label90.setHorizontalAlignment(SwingConstants.RIGHT);
          label90.setName("label90");

          //---- IMA46 ----
          IMA46.setName("IMA46");

          //---- label91 ----
          label91.setText("@ITA46@");
          label91.setName("label91");

          //---- label92 ----
          label92.setText("46");
          label92.setHorizontalAlignment(SwingConstants.RIGHT);
          label92.setName("label92");

          //---- IMA47 ----
          IMA47.setName("IMA47");

          //---- label93 ----
          label93.setText("@ITA47@");
          label93.setName("label93");

          //---- label94 ----
          label94.setText("47");
          label94.setHorizontalAlignment(SwingConstants.RIGHT);
          label94.setName("label94");

          //---- IMA48 ----
          IMA48.setName("IMA48");

          //---- label95 ----
          label95.setText("@ITA48@");
          label95.setName("label95");

          //---- label96 ----
          label96.setText("48");
          label96.setHorizontalAlignment(SwingConstants.RIGHT);
          label96.setName("label96");

          //---- IMA49 ----
          IMA49.setName("IMA49");

          //---- label97 ----
          label97.setText("@ITA49@");
          label97.setName("label97");

          //---- label98 ----
          label98.setText("49");
          label98.setHorizontalAlignment(SwingConstants.RIGHT);
          label98.setName("label98");

          //---- IMA50 ----
          IMA50.setName("IMA50");

          //---- label99 ----
          label99.setText("@ITA50@");
          label99.setName("label99");

          //---- label100 ----
          label100.setText("50");
          label100.setHorizontalAlignment(SwingConstants.RIGHT);
          label100.setName("label100");

          //---- IMA51 ----
          IMA51.setName("IMA51");

          //---- label101 ----
          label101.setText("@ITA51@");
          label101.setName("label101");

          //---- label102 ----
          label102.setText("51");
          label102.setHorizontalAlignment(SwingConstants.RIGHT);
          label102.setName("label102");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA01, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA02, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label6, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA03, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label8, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label7, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA04, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label9, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA05, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA06, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label14, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA07, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label16, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA08, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label18, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label17, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA09, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label19, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA10, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label22, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label21, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA11, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label24, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label23, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA12, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label26, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label25, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA13, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label28, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label27, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA14, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label30, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label29, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA15, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label32, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label31, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA16, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA17, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label35, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA18, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA19, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label40, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA20, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA21, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA22, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA23, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA24, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA25, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA26, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA27, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA28, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA29, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label60, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA30, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label62, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA31, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label63, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA32, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label66, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label65, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA33, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA34, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label70, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label69, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA35, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label72, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label71, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA36, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label74, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA37, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label75, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA38, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label77, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA39, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label80, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label79, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA40, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label82, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label81, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA41, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label84, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label83, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA42, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label86, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label85, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA43, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label88, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label87, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA44, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label90, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label89, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA45, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label92, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label91, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA46, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label94, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label93, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA47, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label96, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label95, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA48, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label98, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label97, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA49, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label100, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label99, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA50, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label102, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label101, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMA51, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label1))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label2))
                  .addComponent(IMA01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label4))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label3))
                  .addComponent(IMA02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label6))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label5))
                  .addComponent(IMA03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label8))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label7))
                  .addComponent(IMA04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label10))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label9))
                  .addComponent(IMA05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label12))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label11))
                  .addComponent(IMA06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label14))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label13))
                  .addComponent(IMA07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label16))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label15))
                  .addComponent(IMA08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label18))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label17))
                  .addComponent(IMA09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label20))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label19))
                  .addComponent(IMA10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label22))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label21))
                  .addComponent(IMA11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label24))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label23))
                  .addComponent(IMA12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label26))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label25))
                  .addComponent(IMA13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label28))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label27))
                  .addComponent(IMA14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label30))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label29))
                  .addComponent(IMA15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label32))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label31))
                  .addComponent(IMA16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label34))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label33))
                  .addComponent(IMA17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label36))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label35))
                  .addComponent(IMA18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label38))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label37))
                  .addComponent(IMA19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label40))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label39))
                  .addComponent(IMA20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label42))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label41))
                  .addComponent(IMA21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label44))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label43))
                  .addComponent(IMA22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label46))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label45))
                  .addComponent(IMA23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label48))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label47))
                  .addComponent(IMA24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label50))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label49))
                  .addComponent(IMA25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label52))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label51))
                  .addComponent(IMA26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label54))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label53))
                  .addComponent(IMA27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label56))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label55))
                  .addComponent(IMA28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label58))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label57))
                  .addComponent(IMA29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label60))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label59))
                  .addComponent(IMA30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label62))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label61))
                  .addComponent(IMA31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label64))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label63))
                  .addComponent(IMA32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label66))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label65))
                  .addComponent(IMA33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label68))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label67))
                  .addComponent(IMA34, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label70))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label69))
                  .addComponent(IMA35, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label72))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label71))
                  .addComponent(IMA36, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label74))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label73))
                  .addComponent(IMA37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label76))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label75))
                  .addComponent(IMA38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label78))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label77))
                  .addComponent(IMA39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label80))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label79))
                  .addComponent(IMA40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label82))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label81))
                  .addComponent(IMA41, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label84))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label83))
                  .addComponent(IMA42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label86))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label85))
                  .addComponent(IMA43, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label88))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label87))
                  .addComponent(IMA44, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label90))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label89))
                  .addComponent(IMA45, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label92))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label91))
                  .addComponent(IMA46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label94))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label93))
                  .addComponent(IMA47, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label96))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label95))
                  .addComponent(IMA48, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label98))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label97))
                  .addComponent(IMA49, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label100))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label99))
                  .addComponent(IMA50, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label102))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label101))
                  .addComponent(IMA51, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 625, 570);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label1;
  private JLabel label2;
  private XRiTextField IMA01;
  private XRiTextField IMA02;
  private JLabel label3;
  private JLabel label4;
  private XRiTextField IMA03;
  private JLabel label5;
  private JLabel label6;
  private XRiTextField IMA04;
  private JLabel label7;
  private JLabel label8;
  private XRiTextField IMA05;
  private JLabel label9;
  private JLabel label10;
  private XRiTextField IMA06;
  private JLabel label11;
  private JLabel label12;
  private XRiTextField IMA07;
  private JLabel label13;
  private JLabel label14;
  private XRiTextField IMA08;
  private JLabel label15;
  private JLabel label16;
  private XRiTextField IMA09;
  private JLabel label17;
  private JLabel label18;
  private XRiTextField IMA10;
  private JLabel label19;
  private JLabel label20;
  private XRiTextField IMA11;
  private JLabel label21;
  private JLabel label22;
  private XRiTextField IMA12;
  private JLabel label23;
  private JLabel label24;
  private XRiTextField IMA13;
  private JLabel label25;
  private JLabel label26;
  private XRiTextField IMA14;
  private JLabel label27;
  private JLabel label28;
  private XRiTextField IMA15;
  private JLabel label29;
  private JLabel label30;
  private XRiTextField IMA16;
  private JLabel label31;
  private JLabel label32;
  private XRiTextField IMA17;
  private JLabel label33;
  private JLabel label34;
  private XRiTextField IMA18;
  private JLabel label35;
  private JLabel label36;
  private XRiTextField IMA19;
  private JLabel label37;
  private JLabel label38;
  private XRiTextField IMA20;
  private JLabel label39;
  private JLabel label40;
  private XRiTextField IMA21;
  private JLabel label41;
  private JLabel label42;
  private XRiTextField IMA22;
  private JLabel label43;
  private JLabel label44;
  private XRiTextField IMA23;
  private JLabel label45;
  private JLabel label46;
  private XRiTextField IMA24;
  private JLabel label47;
  private JLabel label48;
  private XRiTextField IMA25;
  private JLabel label49;
  private JLabel label50;
  private XRiTextField IMA26;
  private JLabel label51;
  private JLabel label52;
  private XRiTextField IMA27;
  private JLabel label53;
  private JLabel label54;
  private XRiTextField IMA28;
  private JLabel label55;
  private JLabel label56;
  private XRiTextField IMA29;
  private JLabel label57;
  private JLabel label58;
  private XRiTextField IMA30;
  private JLabel label59;
  private JLabel label60;
  private XRiTextField IMA31;
  private JLabel label61;
  private JLabel label62;
  private XRiTextField IMA32;
  private JLabel label63;
  private JLabel label64;
  private XRiTextField IMA33;
  private JLabel label65;
  private JLabel label66;
  private XRiTextField IMA34;
  private JLabel label67;
  private JLabel label68;
  private XRiTextField IMA35;
  private JLabel label69;
  private JLabel label70;
  private XRiTextField IMA36;
  private JLabel label71;
  private JLabel label72;
  private XRiTextField IMA37;
  private JLabel label73;
  private JLabel label74;
  private XRiTextField IMA38;
  private JLabel label75;
  private JLabel label76;
  private XRiTextField IMA39;
  private JLabel label77;
  private JLabel label78;
  private XRiTextField IMA40;
  private JLabel label79;
  private JLabel label80;
  private XRiTextField IMA41;
  private JLabel label81;
  private JLabel label82;
  private XRiTextField IMA42;
  private JLabel label83;
  private JLabel label84;
  private XRiTextField IMA43;
  private JLabel label85;
  private JLabel label86;
  private XRiTextField IMA44;
  private JLabel label87;
  private JLabel label88;
  private XRiTextField IMA45;
  private JLabel label89;
  private JLabel label90;
  private XRiTextField IMA46;
  private JLabel label91;
  private JLabel label92;
  private XRiTextField IMA47;
  private JLabel label93;
  private JLabel label94;
  private XRiTextField IMA48;
  private JLabel label95;
  private JLabel label96;
  private XRiTextField IMA49;
  private JLabel label97;
  private JLabel label98;
  private XRiTextField IMA50;
  private JLabel label99;
  private JLabel label100;
  private XRiTextField IMA51;
  private JLabel label101;
  private JLabel label102;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
