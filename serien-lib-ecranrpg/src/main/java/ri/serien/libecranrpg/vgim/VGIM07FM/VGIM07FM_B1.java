
package ri.serien.libecranrpg.vgim.VGIM07FM;
// Nom Fichier: pop_VGIM07FM_FMTB1_110.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGIM07FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIM07FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_39.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("IMMOBILISATION"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_38ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_39ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", true);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    IMLB1 = new XRiTextField();
    IMLB2 = new XRiTextField();
    OBJ_35_OBJ_35 = new JLabel();
    IMVAE = new XRiTextField();
    OBJ_37_OBJ_37 = new JLabel();
    OBJ_31_OBJ_31 = new JLabel();
    BT_ENTER = new JButton();
    OBJ_39 = new JButton();
    OBJ_29_OBJ_29 = new JLabel();
    IMVAE2 = new XRiTextField();
    IMORD = new XRiTextField();
    IMQTE = new XRiTextField();
    IMQTE2 = new XRiTextField();
    OBJ_34_OBJ_34 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- IMLB1 ----
    IMLB1.setName("IMLB1");
    add(IMLB1);
    IMLB1.setBounds(140, 30, 310, IMLB1.getPreferredSize().height);

    //---- IMLB2 ----
    IMLB2.setName("IMLB2");
    add(IMLB2);
    IMLB2.setBounds(140, 60, 310, IMLB2.getPreferredSize().height);

    //---- OBJ_35_OBJ_35 ----
    OBJ_35_OBJ_35.setText("1 immobilisation d'une valeur de");
    OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");
    add(OBJ_35_OBJ_35);
    OBJ_35_OBJ_35.setBounds(75, 184, 194, 20);

    //---- IMVAE ----
    IMVAE.setName("IMVAE");
    add(IMVAE);
    IMVAE.setBounds(140, 122, 108, IMVAE.getPreferredSize().height);

    //---- OBJ_37_OBJ_37 ----
    OBJ_37_OBJ_37.setText("Apr\u00e9s le d\u00e9coupage");
    OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
    add(OBJ_37_OBJ_37);
    OBJ_37_OBJ_37.setBounds(20, 156, 127, 17);

    //---- OBJ_31_OBJ_31 ----
    OBJ_31_OBJ_31.setText("Valeur hors taxes");
    OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");
    add(OBJ_31_OBJ_31);
    OBJ_31_OBJ_31.setBounds(20, 123, 106, 20);

    //---- BT_ENTER ----
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_38ActionPerformed(e);
      }
    });
    add(BT_ENTER);
    BT_ENTER.setBounds(360, 225, 56, 40);

    //---- OBJ_39 ----
    OBJ_39.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_39.setName("OBJ_39");
    OBJ_39.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_39ActionPerformed(e);
      }
    });
    add(OBJ_39);
    OBJ_39.setBounds(420, 225, 56, 40);

    //---- OBJ_29_OBJ_29 ----
    OBJ_29_OBJ_29.setText("Quantit\u00e9 origine");
    OBJ_29_OBJ_29.setName("OBJ_29_OBJ_29");
    add(OBJ_29_OBJ_29);
    OBJ_29_OBJ_29.setBounds(20, 97, 96, 20);

    //---- IMVAE2 ----
    IMVAE2.setName("IMVAE2");
    add(IMVAE2);
    IMVAE2.setBounds(265, 180, 108, IMVAE2.getPreferredSize().height);

    //---- IMORD ----
    IMORD.setName("IMORD");
    add(IMORD);
    IMORD.setBounds(20, 30, 68, IMORD.getPreferredSize().height);

    //---- IMQTE ----
    IMQTE.setName("IMQTE");
    add(IMQTE);
    IMQTE.setBounds(140, 96, 36, IMQTE.getPreferredSize().height);

    //---- IMQTE2 ----
    IMQTE2.setName("IMQTE2");
    add(IMQTE2);
    IMQTE2.setBounds(20, 180, 36, IMQTE2.getPreferredSize().height);

    //---- OBJ_34_OBJ_34 ----
    OBJ_34_OBJ_34.setText("x");
    OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
    add(OBJ_34_OBJ_34);
    OBJ_34_OBJ_34.setBounds(60, 184, 9, 20);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("D\u00e9coupage d'immobilisation");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(10, 10, 465, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(480, 270));

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Recherche multi-crit\u00e8res");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
      OBJ_4.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Cr\u00e9ation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Modification");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Interrogation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Annulation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_14);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private XRiTextField IMLB1;
  private XRiTextField IMLB2;
  private JLabel OBJ_35_OBJ_35;
  private XRiTextField IMVAE;
  private JLabel OBJ_37_OBJ_37;
  private JLabel OBJ_31_OBJ_31;
  private JButton BT_ENTER;
  private JButton OBJ_39;
  private JLabel OBJ_29_OBJ_29;
  private XRiTextField IMVAE2;
  private XRiTextField IMORD;
  private XRiTextField IMQTE;
  private XRiTextField IMQTE2;
  private JLabel OBJ_34_OBJ_34;
  private JXTitledSeparator xTitledSeparator1;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
