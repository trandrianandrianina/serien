
package ri.serien.libecranrpg.vgim.VGIM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGIM03FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIM03FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_107_OBJ_107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    OBJ_108_OBJ_108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    OBJ_109_OBJ_109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    OBJ_110_OBJ_110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    OBJ_111_OBJ_111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
    OBJ_142_OBJ_142.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@IMLF1@")).trim());
    OBJ_126_OBJ_126.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Valeur résiduelle @DGEXB@")).trim());
    OBJ_152_OBJ_152.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Amortissement @DGEX0@")).trim());
    OBJ_153_OBJ_153.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Amortissement @DGEX@")).trim());
    WLIBFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIBFA@")).trim());
    JLIBSA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JLIBSA@")).trim());
    JLIBAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JLIBAC@")).trim());
    JLIBSE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JLIBSE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    panel1.setRightDecoration(TCI1);
    panel2.setRightDecoration(TCI2);
    panel3.setRightDecoration(TCI3);
    panel6.setRightDecoration(TCI6);
    panel7.setRightDecoration(TCI7);
    panel8.setRightDecoration(TCI8);
    panel9.setRightDecoration(TCI9);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (lexique.getMode() == Lexical.MODE_MODIFICATION) {
      lexique.HostFieldPutData("TCI4", 1, "X");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
    else if (lexique.getMode() == Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("V06FO", 1, "4");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void TCI4ActionPerformed(ActionEvent e) {
    if (lexique.getMode() == Lexical.MODE_MODIFICATION) {
      lexique.HostFieldPutData("TCI4", 1, "X");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
    else if (lexique.getMode() == Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("V06FO", 1, "4");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
  }
  
  private void TCI1ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI1").contentEquals(" ")) {
      lexique.HostFieldPutData("TCI1", 1, "X");
      lexique.HostScreenSendKey(this, "Enter", false);
      
    }
    else {
      lexique.HostFieldPutData("V06FO", 1, "1");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
  }
  
  private void TCI2ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI2").contentEquals(" ")) {
      lexique.HostFieldPutData("TCI2", 1, "X");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
    else {
      lexique.HostFieldPutData("V06FO", 1, "2");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
  }
  
  private void TCI3ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI3").contentEquals(" ")) {
      lexique.HostFieldPutData("TCI3", 1, "X");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
    else {
      lexique.HostFieldPutData("V06FO", 1, "3");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
  }
  
  private void TCI5ActionPerformed(ActionEvent e) {
    if (lexique.getMode() == Lexical.MODE_MODIFICATION) {
      lexique.HostFieldPutData("TCI5", 1, "X");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
    else if (lexique.getMode() == Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("V06FO", 1, "5");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
  }
  
  private void TCI6ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI6").contentEquals(" ")) {
      lexique.HostFieldPutData("TCI6", 1, "X");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
    else {
      lexique.HostFieldPutData("V06FO", 1, "6");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
  }
  
  private void TCI7ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI7").contentEquals(" ")) {
      lexique.HostFieldPutData("TCI7", 1, "X");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
    else {
      lexique.HostFieldPutData("V06FO", 1, "7");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
  }
  
  private void TCI9ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI9").contentEquals(" ")) {
      lexique.HostFieldPutData("TCI9", 1, "X");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
    else {
      lexique.HostFieldPutData("V06FO", 1, "9");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
  }
  
  private void TCI8ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI8").contentEquals(" ")) {
      lexique.HostFieldPutData("TCI8", 1, "X");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
    else {
      lexique.HostFieldPutData("V06FO", 1, "8");
      lexique.HostScreenSendKey(this, "Enter", false);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_33_OBJ_33 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_31_OBJ_31 = new JLabel();
    INDORD = new XRiTextField();
    OBJ_35_OBJ_35 = new JLabel();
    INDXXX = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel10 = new JPanel();
    IMLB1 = new XRiTextField();
    IMLB2 = new XRiTextField();
    pane11 = new JPanel();
    OBJ_113_OBJ_113 = new JLabel();
    IMGEO = new XRiTextField();
    OBJ_112_OBJ_112 = new JLabel();
    IMTOP1 = new XRiTextField();
    IMTOP2 = new XRiTextField();
    IMTOP3 = new XRiTextField();
    IMTOP4 = new XRiTextField();
    IMTOP5 = new XRiTextField();
    OBJ_107_OBJ_107 = new RiZoneSortie();
    OBJ_108_OBJ_108 = new RiZoneSortie();
    OBJ_109_OBJ_109 = new RiZoneSortie();
    OBJ_110_OBJ_110 = new RiZoneSortie();
    OBJ_111_OBJ_111 = new RiZoneSortie();
    IMIN1 = new XRiTextField();
    panel1 = new JXTitledPanel();
    OBJ_131_OBJ_131 = new JLabel();
    IMDTEX = new XRiCalendrier();
    OBJ_132_OBJ_132 = new JLabel();
    IMMDE = new XRiTextField();
    OBJ_133_OBJ_133 = new JLabel();
    IMVAE = new XRiTextField();
    panel2 = new JXTitledPanel();
    OBJ_135_OBJ_135 = new JLabel();
    OBJ_136_OBJ_136 = new JLabel();
    IMDDAX = new XRiTextField();
    OBJ_134_OBJ_134 = new JLabel();
    IMJDA = new XRiTextField();
    IMMDA = new XRiTextField();
    IMTXL = new XRiTextField();
    IMTXD = new XRiTextField();
    OBJ_140_OBJ_140 = new JLabel();
    IMDUAR = new XRiTextField();
    OBJ_138_OBJ_138 = new JLabel();
    OBJ_137_OBJ_137 = new JLabel();
    panel3 = new JXTitledPanel();
    OBJ_136_OBJ_138 = new JLabel();
    TCI5 = new SNBoutonDetail();
    panel6 = new JXTitledPanel();
    OBJ_142_OBJ_142 = new RiZoneSortie();
    OBJ_141_OBJ_141 = new JLabel();
    IMFRS = new XRiTextField();
    IMCOL = new XRiTextField();
    IMNPC = new XRiTextField();
    OBJ_143_OBJ_143 = new JLabel();
    panel7 = new JXTitledPanel();
    IMDTSX = new XRiCalendrier();
    OBJ_145_OBJ_145 = new JLabel();
    OBJ_144_OBJ_144 = new JLabel();
    IMMDS = new XRiTextField();
    IMVAS = new XRiTextField();
    OBJ_146_OBJ_146 = new JLabel();
    panel9 = new JXTitledPanel();
    OBJ_150_OBJ_150 = new JLabel();
    LBMODX = new XRiCalendrier();
    panel8 = new JXTitledPanel();
    OBJ_126_OBJ_126 = new JLabel();
    OBJ_152_OBJ_152 = new JLabel();
    OBJ_153_OBJ_153 = new JLabel();
    WVARES = new XRiTextField();
    WMAEX1 = new XRiTextField();
    WMAEX0 = new XRiTextField();
    panel12 = new JPanel();
    WLIBFA = new RiZoneSortie();
    JLIBSA = new RiZoneSortie();
    OBJ_117_OBJ_117 = new JLabel();
    IMGFS = new XRiTextField();
    IMSAN = new XRiTextField();
    OBJ_119_OBJ_119 = new JLabel();
    JLIBAC = new RiZoneSortie();
    JLIBSE = new RiZoneSortie();
    IMACT = new XRiTextField();
    IMSER = new XRiTextField();
    OBJ_123_OBJ_123 = new JLabel();
    OBJ_122_OBJ_122 = new JLabel();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    TCI1 = new SNBoutonDetail();
    TCI2 = new SNBoutonDetail();
    TCI3 = new SNBoutonDetail();
    TCI6 = new SNBoutonDetail();
    TCI7 = new SNBoutonDetail();
    TCI9 = new SNBoutonDetail();
    TCI8 = new SNBoutonDetail();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Fiche immobilisation");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("Etablissement");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTDA);
          INDETB.setName("INDETB");

          //---- OBJ_31_OBJ_31 ----
          OBJ_31_OBJ_31.setText("Immobilisation");
          OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");

          //---- INDORD ----
          INDORD.setComponentPopupMenu(BTDA);
          INDORD.setName("INDORD");

          //---- OBJ_35_OBJ_35 ----
          OBJ_35_OBJ_35.setText("Suffixe");
          OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");

          //---- INDXXX ----
          INDXXX.setComponentPopupMenu(BTDA);
          INDXXX.setName("INDXXX");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_31_OBJ_31, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDORD, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(OBJ_35_OBJ_35, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDXXX, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_33_OBJ_33))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_31_OBJ_31, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDORD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_35_OBJ_35))
              .addComponent(INDXXX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Extentions");
              riSousMenu_bt6.setToolTipText("Extentions de la fiche immobilisation");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Annuit\u00e9s");
              riSousMenu_bt7.setToolTipText("Annuit\u00e9s");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-note");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Documents li\u00e9s");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(960, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel10 ========
          {
            panel10.setOpaque(false);
            panel10.setBorder(new TitledBorder(""));
            panel10.setName("panel10");
            panel10.setLayout(null);

            //---- IMLB1 ----
            IMLB1.setComponentPopupMenu(BTDA);
            IMLB1.setFont(IMLB1.getFont().deriveFont(IMLB1.getFont().getStyle() | Font.BOLD, IMLB1.getFont().getSize() + 3f));
            IMLB1.setName("IMLB1");
            panel10.add(IMLB1);
            IMLB1.setBounds(15, 15, 465, 35);

            //---- IMLB2 ----
            IMLB2.setComponentPopupMenu(BTDA);
            IMLB2.setFont(IMLB2.getFont().deriveFont(IMLB2.getFont().getStyle() | Font.BOLD, IMLB2.getFont().getSize() + 1f));
            IMLB2.setName("IMLB2");
            panel10.add(IMLB2);
            IMLB2.setBounds(15, 55, 465, IMLB2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel10.getComponentCount(); i++) {
                Rectangle bounds = panel10.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel10.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel10.setMinimumSize(preferredSize);
              panel10.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel10);
          panel10.setBounds(13, 14, 495, 105);

          //======== pane11 ========
          {
            pane11.setOpaque(false);
            pane11.setBorder(new TitledBorder(""));
            pane11.setName("pane11");
            pane11.setLayout(null);

            //---- OBJ_113_OBJ_113 ----
            OBJ_113_OBJ_113.setText("Zone g\u00e9ographique");
            OBJ_113_OBJ_113.setName("OBJ_113_OBJ_113");
            pane11.add(OBJ_113_OBJ_113);
            OBJ_113_OBJ_113.setBounds(212, 59, 128, 20);

            //---- IMGEO ----
            IMGEO.setComponentPopupMenu(BTD);
            IMGEO.setName("IMGEO");
            pane11.add(IMGEO);
            IMGEO.setBounds(339, 55, 66, IMGEO.getPreferredSize().height);

            //---- OBJ_112_OBJ_112 ----
            OBJ_112_OBJ_112.setText("Extention ligne");
            OBJ_112_OBJ_112.setName("OBJ_112_OBJ_112");
            pane11.add(OBJ_112_OBJ_112);
            OBJ_112_OBJ_112.setBounds(15, 59, 120, 20);

            //---- IMTOP1 ----
            IMTOP1.setEditable(false);
            IMTOP1.setComponentPopupMenu(BTD);
            IMTOP1.setName("IMTOP1");
            pane11.add(IMTOP1);
            IMTOP1.setBounds(50, 13, 30, IMTOP1.getPreferredSize().height);

            //---- IMTOP2 ----
            IMTOP2.setEditable(false);
            IMTOP2.setComponentPopupMenu(BTD);
            IMTOP2.setName("IMTOP2");
            pane11.add(IMTOP2);
            IMTOP2.setBounds(131, 13, 30, IMTOP2.getPreferredSize().height);

            //---- IMTOP3 ----
            IMTOP3.setEditable(false);
            IMTOP3.setComponentPopupMenu(BTD);
            IMTOP3.setName("IMTOP3");
            pane11.add(IMTOP3);
            IMTOP3.setBounds(212, 13, 30, IMTOP3.getPreferredSize().height);

            //---- IMTOP4 ----
            IMTOP4.setEditable(false);
            IMTOP4.setComponentPopupMenu(BTD);
            IMTOP4.setName("IMTOP4");
            pane11.add(IMTOP4);
            IMTOP4.setBounds(293, 15, 30, IMTOP4.getPreferredSize().height);

            //---- IMTOP5 ----
            IMTOP5.setEditable(false);
            IMTOP5.setComponentPopupMenu(BTD);
            IMTOP5.setName("IMTOP5");
            pane11.add(IMTOP5);
            IMTOP5.setBounds(374, 15, 30, IMTOP5.getPreferredSize().height);

            //---- OBJ_107_OBJ_107 ----
            OBJ_107_OBJ_107.setText("@WTI1@");
            OBJ_107_OBJ_107.setName("OBJ_107_OBJ_107");
            pane11.add(OBJ_107_OBJ_107);
            OBJ_107_OBJ_107.setBounds(15, 15, 33, OBJ_107_OBJ_107.getPreferredSize().height);

            //---- OBJ_108_OBJ_108 ----
            OBJ_108_OBJ_108.setText("@WTI2@");
            OBJ_108_OBJ_108.setName("OBJ_108_OBJ_108");
            pane11.add(OBJ_108_OBJ_108);
            OBJ_108_OBJ_108.setBounds(96, 15, 33, OBJ_108_OBJ_108.getPreferredSize().height);

            //---- OBJ_109_OBJ_109 ----
            OBJ_109_OBJ_109.setText("@WTI3@");
            OBJ_109_OBJ_109.setName("OBJ_109_OBJ_109");
            pane11.add(OBJ_109_OBJ_109);
            OBJ_109_OBJ_109.setBounds(177, 15, 33, OBJ_109_OBJ_109.getPreferredSize().height);

            //---- OBJ_110_OBJ_110 ----
            OBJ_110_OBJ_110.setText("@WTI4@");
            OBJ_110_OBJ_110.setName("OBJ_110_OBJ_110");
            pane11.add(OBJ_110_OBJ_110);
            OBJ_110_OBJ_110.setBounds(258, 15, 33, OBJ_110_OBJ_110.getPreferredSize().height);

            //---- OBJ_111_OBJ_111 ----
            OBJ_111_OBJ_111.setText("@WTI5@");
            OBJ_111_OBJ_111.setName("OBJ_111_OBJ_111");
            pane11.add(OBJ_111_OBJ_111);
            OBJ_111_OBJ_111.setBounds(339, 15, 33, OBJ_111_OBJ_111.getPreferredSize().height);

            //---- IMIN1 ----
            IMIN1.setEditable(false);
            IMIN1.setComponentPopupMenu(BTD);
            IMIN1.setName("IMIN1");
            pane11.add(IMIN1);
            IMIN1.setBounds(141, 55, 20, IMIN1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < pane11.getComponentCount(); i++) {
                Rectangle bounds = pane11.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pane11.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pane11.setMinimumSize(preferredSize);
              pane11.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(pane11);
          pane11.setBounds(528, 14, 420, 105);

          //======== panel1 ========
          {
            panel1.setBorder(new DropShadowBorder());
            panel1.setTitle("Entr\u00e9e d'actif");
            panel1.setName("panel1");
            Container panel1ContentContainer = panel1.getContentContainer();
            panel1ContentContainer.setLayout(null);

            //---- OBJ_131_OBJ_131 ----
            OBJ_131_OBJ_131.setText("Date");
            OBJ_131_OBJ_131.setName("OBJ_131_OBJ_131");
            panel1ContentContainer.add(OBJ_131_OBJ_131);
            OBJ_131_OBJ_131.setBounds(10, 9, 32, 20);

            //---- IMDTEX ----
            IMDTEX.setComponentPopupMenu(BTDA);
            IMDTEX.setName("IMDTEX");
            panel1ContentContainer.add(IMDTEX);
            IMDTEX.setBounds(75, 5, 105, IMDTEX.getPreferredSize().height);

            //---- OBJ_132_OBJ_132 ----
            OBJ_132_OBJ_132.setText("Mode");
            OBJ_132_OBJ_132.setName("OBJ_132_OBJ_132");
            panel1ContentContainer.add(OBJ_132_OBJ_132);
            OBJ_132_OBJ_132.setBounds(200, 10, 38, 20);

            //---- IMMDE ----
            IMMDE.setComponentPopupMenu(BTDA);
            IMMDE.setName("IMMDE");
            panel1ContentContainer.add(IMMDE);
            IMMDE.setBounds(245, 5, 24, IMMDE.getPreferredSize().height);

            //---- OBJ_133_OBJ_133 ----
            OBJ_133_OBJ_133.setText("Valeur H.T");
            OBJ_133_OBJ_133.setName("OBJ_133_OBJ_133");
            panel1ContentContainer.add(OBJ_133_OBJ_133);
            OBJ_133_OBJ_133.setBounds(10, 45, 67, 20);

            //---- IMVAE ----
            IMVAE.setComponentPopupMenu(BTDA);
            IMVAE.setName("IMVAE");
            panel1ContentContainer.add(IMVAE);
            IMVAE.setBounds(75, 40, 108, IMVAE.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = panel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1ContentContainer.setMinimumSize(preferredSize);
              panel1ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(13, 228, 337, 135);

          //======== panel2 ========
          {
            panel2.setBorder(new DropShadowBorder());
            panel2.setTitle("Amortissement fiscal");
            panel2.setName("panel2");
            Container panel2ContentContainer = panel2.getContentContainer();
            panel2ContentContainer.setLayout(null);

            //---- OBJ_135_OBJ_135 ----
            OBJ_135_OBJ_135.setText("/ MM.AA");
            OBJ_135_OBJ_135.setName("OBJ_135_OBJ_135");
            panel2ContentContainer.add(OBJ_135_OBJ_135);
            OBJ_135_OBJ_135.setBounds(85, 10, 50, 20);

            //---- OBJ_136_OBJ_136 ----
            OBJ_136_OBJ_136.setText("Mode");
            OBJ_136_OBJ_136.setName("OBJ_136_OBJ_136");
            panel2ContentContainer.add(OBJ_136_OBJ_136);
            OBJ_136_OBJ_136.setBounds(205, 10, 38, 20);

            //---- IMDDAX ----
            IMDDAX.setComponentPopupMenu(BTDA);
            IMDDAX.setName("IMDDAX");
            panel2ContentContainer.add(IMDDAX);
            IMDDAX.setBounds(135, 5, 52, IMDDAX.getPreferredSize().height);

            //---- OBJ_134_OBJ_134 ----
            OBJ_134_OBJ_134.setText("jour");
            OBJ_134_OBJ_134.setName("OBJ_134_OBJ_134");
            panel2ContentContainer.add(OBJ_134_OBJ_134);
            OBJ_134_OBJ_134.setBounds(10, 10, 25, 20);

            //---- IMJDA ----
            IMJDA.setComponentPopupMenu(BTDA);
            IMJDA.setName("IMJDA");
            panel2ContentContainer.add(IMJDA);
            IMJDA.setBounds(50, 5, 30, IMJDA.getPreferredSize().height);

            //---- IMMDA ----
            IMMDA.setComponentPopupMenu(BTDA);
            IMMDA.setName("IMMDA");
            panel2ContentContainer.add(IMMDA);
            IMMDA.setBounds(245, 5, 24, IMMDA.getPreferredSize().height);

            //---- IMTXL ----
            IMTXL.setComponentPopupMenu(BTDA);
            IMTXL.setName("IMTXL");
            panel2ContentContainer.add(IMTXL);
            IMTXL.setBounds(135, 38, 60, IMTXL.getPreferredSize().height);

            //---- IMTXD ----
            IMTXD.setComponentPopupMenu(BTDA);
            IMTXD.setName("IMTXD");
            panel2ContentContainer.add(IMTXD);
            IMTXD.setBounds(240, 40, 60, IMTXD.getPreferredSize().height);

            //---- OBJ_140_OBJ_140 ----
            OBJ_140_OBJ_140.setText("Dur\u00e9e");
            OBJ_140_OBJ_140.setName("OBJ_140_OBJ_140");
            panel2ContentContainer.add(OBJ_140_OBJ_140);
            OBJ_140_OBJ_140.setBounds(10, 45, 45, 15);

            //---- IMDUAR ----
            IMDUAR.setComponentPopupMenu(BTDA);
            IMDUAR.setName("IMDUAR");
            panel2ContentContainer.add(IMDUAR);
            IMDUAR.setBounds(50, 38, 30, IMDUAR.getPreferredSize().height);

            //---- OBJ_138_OBJ_138 ----
            OBJ_138_OBJ_138.setText("D");
            OBJ_138_OBJ_138.setName("OBJ_138_OBJ_138");
            panel2ContentContainer.add(OBJ_138_OBJ_138);
            OBJ_138_OBJ_138.setBounds(205, 42, 13, 20);

            //---- OBJ_137_OBJ_137 ----
            OBJ_137_OBJ_137.setText("L");
            OBJ_137_OBJ_137.setName("OBJ_137_OBJ_137");
            panel2ContentContainer.add(OBJ_137_OBJ_137);
            OBJ_137_OBJ_137.setBounds(85, 42, 10, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = panel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2ContentContainer.setMinimumSize(preferredSize);
              panel2ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(358, 228, 317, 135);

          //======== panel3 ========
          {
            panel3.setBorder(new DropShadowBorder());
            panel3.setTitle("Economique");
            panel3.setName("panel3");
            Container panel3ContentContainer = panel3.getContentContainer();
            panel3ContentContainer.setLayout(null);

            //---- OBJ_136_OBJ_138 ----
            OBJ_136_OBJ_138.setText("Annuit\u00e9s d'amortissement");
            OBJ_136_OBJ_138.setName("OBJ_136_OBJ_138");
            panel3ContentContainer.add(OBJ_136_OBJ_138);
            OBJ_136_OBJ_138.setBounds(15, 10, 185, 20);

            //---- TCI5 ----
            TCI5.setText("");
            TCI5.setToolTipText("Annuit\u00e9s");
            TCI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TCI5.setName("TCI5");
            TCI5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                TCI5ActionPerformed(e);
              }
            });
            panel3ContentContainer.add(TCI5);
            TCI5.setBounds(215, 6, 23, 29);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = panel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3ContentContainer.setMinimumSize(preferredSize);
              panel3ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel3);
          panel3.setBounds(686, 226, 260, 135);

          //======== panel6 ========
          {
            panel6.setBorder(new DropShadowBorder());
            panel6.setTitle("Achat");
            panel6.setName("panel6");
            Container panel6ContentContainer = panel6.getContentContainer();

            //---- OBJ_142_OBJ_142 ----
            OBJ_142_OBJ_142.setText("@IMLF1@");
            OBJ_142_OBJ_142.setName("OBJ_142_OBJ_142");

            //---- OBJ_141_OBJ_141 ----
            OBJ_141_OBJ_141.setText("Fournisseur");
            OBJ_141_OBJ_141.setName("OBJ_141_OBJ_141");

            //---- IMFRS ----
            IMFRS.setComponentPopupMenu(BTD);
            IMFRS.setName("IMFRS");

            //---- IMCOL ----
            IMCOL.setComponentPopupMenu(BTD);
            IMCOL.setName("IMCOL");

            //---- IMNPC ----
            IMNPC.setComponentPopupMenu(BTDA);
            IMNPC.setName("IMNPC");

            //---- OBJ_143_OBJ_143 ----
            OBJ_143_OBJ_143.setText("Pi\u00e8ce");
            OBJ_143_OBJ_143.setName("OBJ_143_OBJ_143");

            GroupLayout panel6ContentContainerLayout = new GroupLayout(panel6ContentContainer);
            panel6ContentContainer.setLayout(panel6ContentContainerLayout);
            panel6ContentContainerLayout.setHorizontalGroup(
              panel6ContentContainerLayout.createParallelGroup()
                .addGroup(panel6ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel6ContentContainerLayout.createParallelGroup()
                    .addGroup(panel6ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_141_OBJ_141, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
                      .addGap(2, 2, 2)
                      .addComponent(IMCOL, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(IMFRS, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_142_OBJ_142, GroupLayout.PREFERRED_SIZE, 231, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel6ContentContainerLayout.createSequentialGroup()
                      .addComponent(OBJ_143_OBJ_143, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(IMNPC, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE))))
            );
            panel6ContentContainerLayout.setVerticalGroup(
              panel6ContentContainerLayout.createParallelGroup()
                .addGroup(panel6ContentContainerLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel6ContentContainerLayout.createParallelGroup()
                    .addGroup(panel6ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_141_OBJ_141, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(IMCOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(IMFRS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel6ContentContainerLayout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_142_OBJ_142, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(2, 2, 2)
                  .addGroup(panel6ContentContainerLayout.createParallelGroup()
                    .addGroup(panel6ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_143_OBJ_143, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(IMNPC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }
          p_contenu.add(panel6);
          panel6.setBounds(13, 376, 415, 105);

          //======== panel7 ========
          {
            panel7.setBorder(new DropShadowBorder());
            panel7.setTitle("Sortie d'actif");
            panel7.setName("panel7");
            Container panel7ContentContainer = panel7.getContentContainer();
            panel7ContentContainer.setLayout(null);

            //---- IMDTSX ----
            IMDTSX.setComponentPopupMenu(BTDA);
            IMDTSX.setName("IMDTSX");
            panel7ContentContainer.add(IMDTSX);
            IMDTSX.setBounds(50, 5, 105, IMDTSX.getPreferredSize().height);

            //---- OBJ_145_OBJ_145 ----
            OBJ_145_OBJ_145.setText("Mode");
            OBJ_145_OBJ_145.setName("OBJ_145_OBJ_145");
            panel7ContentContainer.add(OBJ_145_OBJ_145);
            OBJ_145_OBJ_145.setBounds(160, 9, 40, 20);

            //---- OBJ_144_OBJ_144 ----
            OBJ_144_OBJ_144.setText("Date");
            OBJ_144_OBJ_144.setName("OBJ_144_OBJ_144");
            panel7ContentContainer.add(OBJ_144_OBJ_144);
            OBJ_144_OBJ_144.setBounds(15, 9, 32, 20);

            //---- IMMDS ----
            IMMDS.setComponentPopupMenu(BTDA);
            IMMDS.setName("IMMDS");
            panel7ContentContainer.add(IMMDS);
            IMMDS.setBounds(195, 5, 24, IMMDS.getPreferredSize().height);

            //---- IMVAS ----
            IMVAS.setComponentPopupMenu(BTDA);
            IMVAS.setName("IMVAS");
            panel7ContentContainer.add(IMVAS);
            IMVAS.setBounds(111, 40, 108, IMVAS.getPreferredSize().height);

            //---- OBJ_146_OBJ_146 ----
            OBJ_146_OBJ_146.setText("Valeur H.T");
            OBJ_146_OBJ_146.setName("OBJ_146_OBJ_146");
            panel7ContentContainer.add(OBJ_146_OBJ_146);
            OBJ_146_OBJ_146.setBounds(15, 44, 90, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel7ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = panel7ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel7ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel7ContentContainer.setMinimumSize(preferredSize);
              panel7ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel7);
          panel7.setBounds(438, 376, 237, 105);

          //======== panel9 ========
          {
            panel9.setBorder(new DropShadowBorder());
            panel9.setTitle("Historique des modifications");
            panel9.setName("panel9");
            Container panel9ContentContainer = panel9.getContentContainer();
            panel9ContentContainer.setLayout(null);

            //---- OBJ_150_OBJ_150 ----
            OBJ_150_OBJ_150.setText("Date de derni\u00e8re modification");
            OBJ_150_OBJ_150.setName("OBJ_150_OBJ_150");
            panel9ContentContainer.add(OBJ_150_OBJ_150);
            OBJ_150_OBJ_150.setBounds(20, 10, 205, OBJ_150_OBJ_150.getPreferredSize().height);

            //---- LBMODX ----
            LBMODX.setComponentPopupMenu(BTD);
            LBMODX.setName("LBMODX");
            panel9ContentContainer.add(LBMODX);
            LBMODX.setBounds(20, 40, 105, LBMODX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel9ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = panel9ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel9ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel9ContentContainer.setMinimumSize(preferredSize);
              panel9ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel9);
          panel9.setBounds(686, 376, 260, 105);

          //======== panel8 ========
          {
            panel8.setBorder(new DropShadowBorder());
            panel8.setTitle("Tableau d'amortissement");
            panel8.setName("panel8");
            Container panel8ContentContainer = panel8.getContentContainer();
            panel8ContentContainer.setLayout(null);

            //---- OBJ_126_OBJ_126 ----
            OBJ_126_OBJ_126.setText("Valeur r\u00e9siduelle @DGEXB@");
            OBJ_126_OBJ_126.setName("OBJ_126_OBJ_126");
            panel8ContentContainer.add(OBJ_126_OBJ_126);
            OBJ_126_OBJ_126.setBounds(318, 14, 150, 20);

            //---- OBJ_152_OBJ_152 ----
            OBJ_152_OBJ_152.setText("Amortissement @DGEX0@");
            OBJ_152_OBJ_152.setName("OBJ_152_OBJ_152");
            panel8ContentContainer.add(OBJ_152_OBJ_152);
            OBJ_152_OBJ_152.setBounds(10, 15, 150, 18);

            //---- OBJ_153_OBJ_153 ----
            OBJ_153_OBJ_153.setText("Amortissement @DGEX@");
            OBJ_153_OBJ_153.setName("OBJ_153_OBJ_153");
            panel8ContentContainer.add(OBJ_153_OBJ_153);
            OBJ_153_OBJ_153.setBounds(645, 15, 150, 18);

            //---- WVARES ----
            WVARES.setComponentPopupMenu(BTD);
            WVARES.setName("WVARES");
            panel8ContentContainer.add(WVARES);
            WVARES.setBounds(485, 10, 92, WVARES.getPreferredSize().height);

            //---- WMAEX1 ----
            WMAEX1.setComponentPopupMenu(BTD);
            WMAEX1.setName("WMAEX1");
            panel8ContentContainer.add(WMAEX1);
            WMAEX1.setBounds(810, 10, 92, WMAEX1.getPreferredSize().height);

            //---- WMAEX0 ----
            WMAEX0.setComponentPopupMenu(BTD);
            WMAEX0.setName("WMAEX0");
            panel8ContentContainer.add(WMAEX0);
            WMAEX0.setBounds(175, 10, 92, WMAEX0.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel8ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = panel8ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel8ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel8ContentContainer.setMinimumSize(preferredSize);
              panel8ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel8);
          panel8.setBounds(13, 488, 935, 85);

          //======== panel12 ========
          {
            panel12.setOpaque(false);
            panel12.setName("panel12");
            panel12.setLayout(null);

            //---- WLIBFA ----
            WLIBFA.setText("@WLIBFA@");
            WLIBFA.setName("WLIBFA");
            panel12.add(WLIBFA);
            WLIBFA.setBounds(255, 7, 217, WLIBFA.getPreferredSize().height);

            //---- JLIBSA ----
            JLIBSA.setText("@JLIBSA@");
            JLIBSA.setName("JLIBSA");
            panel12.add(JLIBSA);
            JLIBSA.setBounds(255, 42, 217, JLIBSA.getPreferredSize().height);

            //---- OBJ_117_OBJ_117 ----
            OBJ_117_OBJ_117.setText("Groupe/Famille/Sous famille");
            OBJ_117_OBJ_117.setName("OBJ_117_OBJ_117");
            panel12.add(OBJ_117_OBJ_117);
            OBJ_117_OBJ_117.setBounds(20, 10, 173, 18);

            //---- IMGFS ----
            IMGFS.setComponentPopupMenu(BTD);
            IMGFS.setName("IMGFS");
            panel12.add(IMGFS);
            IMGFS.setBounds(195, 5, 52, IMGFS.getPreferredSize().height);

            //---- IMSAN ----
            IMSAN.setComponentPopupMenu(BTD);
            IMSAN.setName("IMSAN");
            panel12.add(IMSAN);
            IMSAN.setBounds(195, 40, 44, IMSAN.getPreferredSize().height);

            //---- OBJ_119_OBJ_119 ----
            OBJ_119_OBJ_119.setText("Section");
            OBJ_119_OBJ_119.setName("OBJ_119_OBJ_119");
            panel12.add(OBJ_119_OBJ_119);
            OBJ_119_OBJ_119.setBounds(20, 45, 175, 18);

            //---- JLIBAC ----
            JLIBAC.setText("@JLIBAC@");
            JLIBAC.setName("JLIBAC");
            panel12.add(JLIBAC);
            JLIBAC.setBounds(700, 7, 217, JLIBAC.getPreferredSize().height);

            //---- JLIBSE ----
            JLIBSE.setText("@JLIBSE@");
            JLIBSE.setName("JLIBSE");
            panel12.add(JLIBSE);
            JLIBSE.setBounds(700, 42, 217, JLIBSE.getPreferredSize().height);

            //---- IMACT ----
            IMACT.setComponentPopupMenu(BTD);
            IMACT.setName("IMACT");
            panel12.add(IMACT);
            IMACT.setBounds(635, 5, 52, IMACT.getPreferredSize().height);

            //---- IMSER ----
            IMSER.setComponentPopupMenu(BTD);
            IMSER.setName("IMSER");
            panel12.add(IMSER);
            IMSER.setBounds(635, 40, 52, IMSER.getPreferredSize().height);

            //---- OBJ_123_OBJ_123 ----
            OBJ_123_OBJ_123.setText("Service");
            OBJ_123_OBJ_123.setName("OBJ_123_OBJ_123");
            panel12.add(OBJ_123_OBJ_123);
            OBJ_123_OBJ_123.setBounds(530, 44, 105, 20);

            //---- OBJ_122_OBJ_122 ----
            OBJ_122_OBJ_122.setText("Affaire");
            OBJ_122_OBJ_122.setName("OBJ_122_OBJ_122");
            panel12.add(OBJ_122_OBJ_122);
            OBJ_122_OBJ_122.setBounds(530, 10, 105, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel12.getComponentCount(); i++) {
                Rectangle bounds = panel12.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel12.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel12.setMinimumSize(preferredSize);
              panel12.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel12);
          panel12.setBounds(15, 125, 930, 75);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_21 ----
      OBJ_21.setText("Choix possibles");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }

    //---- TCI1 ----
    TCI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI1.setBorder(null);
    TCI1.setToolTipText("Entr\u00e9e d'actif");
    TCI1.setName("TCI1");
    TCI1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI1ActionPerformed(e);
      }
    });

    //---- TCI2 ----
    TCI2.setText("");
    TCI2.setToolTipText("Amortissement fiscal");
    TCI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI2.setName("TCI2");
    TCI2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI2ActionPerformed(e);
      }
    });

    //---- TCI3 ----
    TCI3.setText("");
    TCI3.setToolTipText("Economique");
    TCI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI3.setName("TCI3");
    TCI3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI3ActionPerformed(e);
      }
    });

    //---- TCI6 ----
    TCI6.setText("");
    TCI6.setToolTipText("Achat");
    TCI6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI6.setName("TCI6");
    TCI6.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI6ActionPerformed(e);
      }
    });

    //---- TCI7 ----
    TCI7.setText("");
    TCI7.setToolTipText("Sortie d'actif");
    TCI7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI7.setName("TCI7");
    TCI7.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI7ActionPerformed(e);
      }
    });

    //---- TCI9 ----
    TCI9.setText("");
    TCI9.setToolTipText("Historique des modifications");
    TCI9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI9.setName("TCI9");
    TCI9.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI9ActionPerformed(e);
      }
    });

    //---- TCI8 ----
    TCI8.setText("");
    TCI8.setToolTipText("Tableau d'amortissement");
    TCI8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI8.setName("TCI8");
    TCI8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI8ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_33_OBJ_33;
  private XRiTextField INDETB;
  private JLabel OBJ_31_OBJ_31;
  private XRiTextField INDORD;
  private JLabel OBJ_35_OBJ_35;
  private XRiTextField INDXXX;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel10;
  private XRiTextField IMLB1;
  private XRiTextField IMLB2;
  private JPanel pane11;
  private JLabel OBJ_113_OBJ_113;
  private XRiTextField IMGEO;
  private JLabel OBJ_112_OBJ_112;
  private XRiTextField IMTOP1;
  private XRiTextField IMTOP2;
  private XRiTextField IMTOP3;
  private XRiTextField IMTOP4;
  private XRiTextField IMTOP5;
  private RiZoneSortie OBJ_107_OBJ_107;
  private RiZoneSortie OBJ_108_OBJ_108;
  private RiZoneSortie OBJ_109_OBJ_109;
  private RiZoneSortie OBJ_110_OBJ_110;
  private RiZoneSortie OBJ_111_OBJ_111;
  private XRiTextField IMIN1;
  private JXTitledPanel panel1;
  private JLabel OBJ_131_OBJ_131;
  private XRiCalendrier IMDTEX;
  private JLabel OBJ_132_OBJ_132;
  private XRiTextField IMMDE;
  private JLabel OBJ_133_OBJ_133;
  private XRiTextField IMVAE;
  private JXTitledPanel panel2;
  private JLabel OBJ_135_OBJ_135;
  private JLabel OBJ_136_OBJ_136;
  private XRiTextField IMDDAX;
  private JLabel OBJ_134_OBJ_134;
  private XRiTextField IMJDA;
  private XRiTextField IMMDA;
  private XRiTextField IMTXL;
  private XRiTextField IMTXD;
  private JLabel OBJ_140_OBJ_140;
  private XRiTextField IMDUAR;
  private JLabel OBJ_138_OBJ_138;
  private JLabel OBJ_137_OBJ_137;
  private JXTitledPanel panel3;
  private JLabel OBJ_136_OBJ_138;
  private SNBoutonDetail TCI5;
  private JXTitledPanel panel6;
  private RiZoneSortie OBJ_142_OBJ_142;
  private JLabel OBJ_141_OBJ_141;
  private XRiTextField IMFRS;
  private XRiTextField IMCOL;
  private XRiTextField IMNPC;
  private JLabel OBJ_143_OBJ_143;
  private JXTitledPanel panel7;
  private XRiCalendrier IMDTSX;
  private JLabel OBJ_145_OBJ_145;
  private JLabel OBJ_144_OBJ_144;
  private XRiTextField IMMDS;
  private XRiTextField IMVAS;
  private JLabel OBJ_146_OBJ_146;
  private JXTitledPanel panel9;
  private JLabel OBJ_150_OBJ_150;
  private XRiCalendrier LBMODX;
  private JXTitledPanel panel8;
  private JLabel OBJ_126_OBJ_126;
  private JLabel OBJ_152_OBJ_152;
  private JLabel OBJ_153_OBJ_153;
  private XRiTextField WVARES;
  private XRiTextField WMAEX1;
  private XRiTextField WMAEX0;
  private JPanel panel12;
  private RiZoneSortie WLIBFA;
  private RiZoneSortie JLIBSA;
  private JLabel OBJ_117_OBJ_117;
  private XRiTextField IMGFS;
  private XRiTextField IMSAN;
  private JLabel OBJ_119_OBJ_119;
  private RiZoneSortie JLIBAC;
  private RiZoneSortie JLIBSE;
  private XRiTextField IMACT;
  private XRiTextField IMSER;
  private JLabel OBJ_123_OBJ_123;
  private JLabel OBJ_122_OBJ_122;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_20;
  private SNBoutonDetail TCI1;
  private SNBoutonDetail TCI2;
  private SNBoutonDetail TCI3;
  private SNBoutonDetail TCI6;
  private SNBoutonDetail TCI7;
  private SNBoutonDetail TCI9;
  private SNBoutonDetail TCI8;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
