
package ri.serien.libecranrpg.vgim.VGIM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGIM03FM_C6 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIM03FM_C6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    IMCOL.setEnabled(lexique.isPresent("IMCOL"));
    IMDUG.setEnabled(lexique.isPresent("IMDUG"));
    IMDUM.setEnabled(lexique.isPresent("IMDUM"));
    // IMDDGX.setEnabled( lexique.isPresent("IMDDGX"));
    // IMDDMX.setEnabled( lexique.isPresent("IMDDMX"));
    IMNPC.setEnabled(lexique.isPresent("IMNPC"));
    IMFRS.setEnabled(lexique.isPresent("IMFRS"));
    IMRCM.setEnabled(lexique.isPresent("IMRCM"));
    IMRFR.setEnabled(lexique.isPresent("IMRFR"));
    IMLF2.setEnabled(lexique.isPresent("IMLF2"));
    IMLF1.setEnabled(lexique.isPresent("IMLF1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("IMMOBILISATION"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(9, 37);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    IMLF1 = new XRiTextField();
    IMLF2 = new XRiTextField();
    OBJ_32_OBJ_32 = new JLabel();
    IMRFR = new XRiTextField();
    IMRCM = new XRiTextField();
    OBJ_33_OBJ_33 = new JLabel();
    OBJ_31_OBJ_31 = new JLabel();
    OBJ_34_OBJ_34 = new JLabel();
    OBJ_36_OBJ_36 = new JLabel();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_38_OBJ_38 = new JLabel();
    IMFRS = new XRiTextField();
    IMNPC = new XRiTextField();
    OBJ_35_OBJ_35 = new JLabel();
    OBJ_37_OBJ_37 = new JLabel();
    IMDDMX = new XRiCalendrier();
    IMDDGX = new XRiCalendrier();
    IMDUM = new XRiTextField();
    IMDUG = new XRiTextField();
    IMCOL = new XRiTextField();
    xTitledSeparator2 = new JXTitledSeparator();
    xTitledSeparator3 = new JXTitledSeparator();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(685, 375));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Fournisseur");
            riSousMenu_bt6.setToolTipText("Visualisation du fournisseur");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Achat"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- IMLF1 ----
          IMLF1.setComponentPopupMenu(BTDA);
          IMLF1.setName("IMLF1");
          panel1.add(IMLF1);
          IMLF1.setBounds(138, 80, 310, IMLF1.getPreferredSize().height);

          //---- IMLF2 ----
          IMLF2.setComponentPopupMenu(BTDA);
          IMLF2.setName("IMLF2");
          panel1.add(IMLF2);
          IMLF2.setBounds(138, 105, 310, IMLF2.getPreferredSize().height);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("R\u00e9f\u00e9rence Immobilisation");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          panel1.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(33, 154, 155, 20);

          //---- IMRFR ----
          IMRFR.setComponentPopupMenu(BTDA);
          IMRFR.setName("IMRFR");
          panel1.add(IMRFR);
          IMRFR.setBounds(198, 150, 180, IMRFR.getPreferredSize().height);

          //---- IMRCM ----
          IMRCM.setComponentPopupMenu(BTDA);
          IMRCM.setName("IMRCM");
          panel1.add(IMRCM);
          IMRCM.setBounds(203, 205, 180, IMRCM.getPreferredSize().height);

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("R\u00e9f\u00e9rence du contrat");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
          panel1.add(OBJ_33_OBJ_33);
          OBJ_33_OBJ_33.setBounds(33, 209, 127, 20);

          //---- OBJ_31_OBJ_31 ----
          OBJ_31_OBJ_31.setText("Raison sociale");
          OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");
          panel1.add(OBJ_31_OBJ_31);
          OBJ_31_OBJ_31.setBounds(33, 84, 93, 20);

          //---- OBJ_34_OBJ_34 ----
          OBJ_34_OBJ_34.setText("Date de d\u00e9part");
          OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
          panel1.add(OBJ_34_OBJ_34);
          OBJ_34_OBJ_34.setBounds(33, 239, 93, 20);

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("Date de d\u00e9part");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");
          panel1.add(OBJ_36_OBJ_36);
          OBJ_36_OBJ_36.setBounds(33, 299, 93, 20);

          //---- OBJ_53_OBJ_53 ----
          OBJ_53_OBJ_53.setText("Pi\u00e8ce num\u00e9ro");
          OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");
          panel1.add(OBJ_53_OBJ_53);
          OBJ_53_OBJ_53.setBounds(278, 49, 86, 20);

          //---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("Fournisseur");
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");
          panel1.add(OBJ_38_OBJ_38);
          OBJ_38_OBJ_38.setBounds(33, 49, 73, 20);

          //---- IMFRS ----
          IMFRS.setComponentPopupMenu(BTD);
          IMFRS.setName("IMFRS");
          panel1.add(IMFRS);
          IMFRS.setBounds(165, 45, 60, IMFRS.getPreferredSize().height);

          //---- IMNPC ----
          IMNPC.setComponentPopupMenu(BTDA);
          IMNPC.setName("IMNPC");
          panel1.add(IMNPC);
          IMNPC.setBounds(378, 45, 68, IMNPC.getPreferredSize().height);

          //---- OBJ_35_OBJ_35 ----
          OBJ_35_OBJ_35.setText("Dur\u00e9e");
          OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");
          panel1.add(OBJ_35_OBJ_35);
          OBJ_35_OBJ_35.setBounds(305, 239, 47, 20);

          //---- OBJ_37_OBJ_37 ----
          OBJ_37_OBJ_37.setText("Dur\u00e9e");
          OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
          panel1.add(OBJ_37_OBJ_37);
          OBJ_37_OBJ_37.setBounds(305, 299, 47, 20);

          //---- IMDDMX ----
          IMDDMX.setComponentPopupMenu(BTDA);
          IMDDMX.setTypeSaisie(6);
          IMDDMX.setName("IMDDMX");
          panel1.add(IMDDMX);
          IMDDMX.setBounds(203, 235, 90, IMDDMX.getPreferredSize().height);

          //---- IMDDGX ----
          IMDDGX.setComponentPopupMenu(BTDA);
          IMDDGX.setTypeSaisie(6);
          IMDDGX.setName("IMDDGX");
          panel1.add(IMDDGX);
          IMDDGX.setBounds(203, 295, 90, IMDDGX.getPreferredSize().height);

          //---- IMDUM ----
          IMDUM.setComponentPopupMenu(BTDA);
          IMDUM.setName("IMDUM");
          panel1.add(IMDUM);
          IMDUM.setBounds(353, 235, 30, IMDUM.getPreferredSize().height);

          //---- IMDUG ----
          IMDUG.setComponentPopupMenu(BTDA);
          IMDUG.setName("IMDUG");
          panel1.add(IMDUG);
          IMDUG.setBounds(353, 295, 30, IMDUG.getPreferredSize().height);

          //---- IMCOL ----
          IMCOL.setComponentPopupMenu(BTD);
          IMCOL.setName("IMCOL");
          panel1.add(IMCOL);
          IMCOL.setBounds(138, 45, 24, IMCOL.getPreferredSize().height);

          //---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("Maintenance");
          xTitledSeparator2.setName("xTitledSeparator2");
          panel1.add(xTitledSeparator2);
          xTitledSeparator2.setBounds(25, 185, 433, xTitledSeparator2.getPreferredSize().height);

          //---- xTitledSeparator3 ----
          xTitledSeparator3.setTitle("Garantie");
          xTitledSeparator3.setName("xTitledSeparator3");
          panel1.add(xTitledSeparator3);
          xTitledSeparator3.setBounds(25, 270, 433, xTitledSeparator3.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 495, 355);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField IMLF1;
  private XRiTextField IMLF2;
  private JLabel OBJ_32_OBJ_32;
  private XRiTextField IMRFR;
  private XRiTextField IMRCM;
  private JLabel OBJ_33_OBJ_33;
  private JLabel OBJ_31_OBJ_31;
  private JLabel OBJ_34_OBJ_34;
  private JLabel OBJ_36_OBJ_36;
  private JLabel OBJ_53_OBJ_53;
  private JLabel OBJ_38_OBJ_38;
  private XRiTextField IMFRS;
  private XRiTextField IMNPC;
  private JLabel OBJ_35_OBJ_35;
  private JLabel OBJ_37_OBJ_37;
  private XRiCalendrier IMDDMX;
  private XRiCalendrier IMDDGX;
  private XRiTextField IMDUM;
  private XRiTextField IMDUG;
  private XRiTextField IMCOL;
  private JXTitledSeparator xTitledSeparator2;
  private JXTitledSeparator xTitledSeparator3;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
