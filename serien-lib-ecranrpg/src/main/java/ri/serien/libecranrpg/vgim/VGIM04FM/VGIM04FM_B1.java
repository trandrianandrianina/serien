
package ri.serien.libecranrpg.vgim.VGIM04FM;
// Nom Fichier: b_VGIM04FM_FMTB0_FMTF1_107.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGIM04FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIM04FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    OP2.setValeursSelection("X", " ");
    OP1.setValeursSelection("X", " ");
    OP3.setValeursSelection("X", " ");
    REPEX.setValeursSelection("OUI", "NON");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    DGDE1X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGDE1X@")).trim());
    DGFE1X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGFE1X@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    REPEX = new XRiCheckBox();
    OP3 = new XRiCheckBox();
    OP1 = new XRiCheckBox();
    OBJ_53_OBJ_53 = new JLabel();
    OP2 = new XRiCheckBox();
    OBJ_73_OBJ_73 = new JLabel();
    OBJ_65_OBJ_65 = new JLabel();
    OBJ_61_OBJ_61 = new JLabel();
    OBJ_58_OBJ_58 = new JLabel();
    GFSDEB = new XRiTextField();
    GFSFIN = new XRiTextField();
    AFFDEB = new XRiTextField();
    AFFFIN = new XRiTextField();
    ZGEDEB = new XRiTextField();
    ZGEFIN = new XRiTextField();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    OBJ_69_OBJ_69 = new JLabel();
    SECDEB = new XRiTextField();
    SECFIN = new XRiTextField();
    MOADEB = new XRiTextField();
    MOAFIN = new XRiTextField();
    MOEDEB = new XRiTextField();
    MOEFIN = new XRiTextField();
    OBJ_48_OBJ_48 = new JLabel();
    OBJ_55_OBJ_55 = new JLabel();
    OBJ_59_OBJ_59 = new JLabel();
    OBJ_63_OBJ_63 = new JLabel();
    OBJ_67_OBJ_67 = new JLabel();
    OBJ_71_OBJ_71 = new JLabel();
    OBJ_75_OBJ_75 = new JLabel();
    separator1 = compFactory.createSeparator("Groupe, famille, sous-famille");
    separator2 = compFactory.createSeparator("  ");
    DGDE1X = new RiZoneSortie();
    DGFE1X = new RiZoneSortie();
    OBJ_38_OBJ_38 = new JLabel();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau de bord des immobilisations");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(580, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- REPEX ----
          REPEX.setText("Traitement de l'exercice ant\u00e9rieur");
          REPEX.setComponentPopupMenu(BTD);
          REPEX.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPEX.setName("REPEX");

          //---- OP3 ----
          OP3.setText("Cumul sur la sous-famille");
          OP3.setComponentPopupMenu(BTD);
          OP3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OP3.setName("OP3");

          //---- OP1 ----
          OP1.setText("Cumul sur le groupe");
          OP1.setComponentPopupMenu(BTD);
          OP1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OP1.setName("OP1");

          //---- OBJ_53_OBJ_53 ----
          OBJ_53_OBJ_53.setText("Mode d'amortissement");
          OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");

          //---- OP2 ----
          OP2.setText("Cumul sur la famille");
          OP2.setComponentPopupMenu(BTD);
          OP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OP2.setName("OP2");

          //---- OBJ_73_OBJ_73 ----
          OBJ_73_OBJ_73.setText("Zone g\u00e9ographique");
          OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");

          //---- OBJ_65_OBJ_65 ----
          OBJ_65_OBJ_65.setText("Section analytique");
          OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");

          //---- OBJ_61_OBJ_61 ----
          OBJ_61_OBJ_61.setText("Mode d'entr\u00e9e");
          OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");

          //---- OBJ_58_OBJ_58 ----
          OBJ_58_OBJ_58.setText("Date d'entr\u00e9e");
          OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");

          //---- GFSDEB ----
          GFSDEB.setComponentPopupMenu(BTD);
          GFSDEB.setName("GFSDEB");

          //---- GFSFIN ----
          GFSFIN.setComponentPopupMenu(BTD);
          GFSFIN.setName("GFSFIN");

          //---- AFFDEB ----
          AFFDEB.setComponentPopupMenu(BTD);
          AFFDEB.setName("AFFDEB");

          //---- AFFFIN ----
          AFFFIN.setComponentPopupMenu(BTD);
          AFFFIN.setName("AFFFIN");

          //---- ZGEDEB ----
          ZGEDEB.setComponentPopupMenu(BTD);
          ZGEDEB.setName("ZGEDEB");

          //---- ZGEFIN ----
          ZGEFIN.setComponentPopupMenu(BTD);
          ZGEFIN.setName("ZGEFIN");

          //---- DATDEB ----
          DATDEB.setComponentPopupMenu(BTD);
          DATDEB.setName("DATDEB");

          //---- DATFIN ----
          DATFIN.setComponentPopupMenu(BTD);
          DATFIN.setName("DATFIN");

          //---- OBJ_69_OBJ_69 ----
          OBJ_69_OBJ_69.setText("Affaire");
          OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");

          //---- SECDEB ----
          SECDEB.setComponentPopupMenu(BTD);
          SECDEB.setName("SECDEB");

          //---- SECFIN ----
          SECFIN.setComponentPopupMenu(BTD);
          SECFIN.setName("SECFIN");

          //---- MOADEB ----
          MOADEB.setComponentPopupMenu(BTD);
          MOADEB.setName("MOADEB");

          //---- MOAFIN ----
          MOAFIN.setComponentPopupMenu(BTD);
          MOAFIN.setName("MOAFIN");

          //---- MOEDEB ----
          MOEDEB.setComponentPopupMenu(BTD);
          MOEDEB.setName("MOEDEB");

          //---- MOEFIN ----
          MOEFIN.setComponentPopupMenu(BTD);
          MOEFIN.setName("MOEFIN");

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("\u00e0");
          OBJ_48_OBJ_48.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");

          //---- OBJ_55_OBJ_55 ----
          OBJ_55_OBJ_55.setText("\u00e0");
          OBJ_55_OBJ_55.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");

          //---- OBJ_59_OBJ_59 ----
          OBJ_59_OBJ_59.setText("\u00e0");
          OBJ_59_OBJ_59.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");

          //---- OBJ_63_OBJ_63 ----
          OBJ_63_OBJ_63.setText("\u00e0");
          OBJ_63_OBJ_63.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");

          //---- OBJ_67_OBJ_67 ----
          OBJ_67_OBJ_67.setText("\u00e0");
          OBJ_67_OBJ_67.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");

          //---- OBJ_71_OBJ_71 ----
          OBJ_71_OBJ_71.setText("\u00e0");
          OBJ_71_OBJ_71.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");

          //---- OBJ_75_OBJ_75 ----
          OBJ_75_OBJ_75.setText("\u00e0");
          OBJ_75_OBJ_75.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");

          //---- separator1 ----
          separator1.setName("separator1");

          //---- separator2 ----
          separator2.setName("separator2");

          //---- DGDE1X ----
          DGDE1X.setText("@DGDE1X@");
          DGDE1X.setName("DGDE1X");

          //---- DGFE1X ----
          DGFE1X.setText("@DGFE1X@");
          DGFE1X.setName("DGFE1X");

          //---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("\u00e0");
          OBJ_38_OBJ_38.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 500, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(DGDE1X, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
                    .addGap(3, 3, 3)
                    .addComponent(DGFE1X, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 500, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(GFSDEB, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(GFSFIN, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OP1, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OP2, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(204, 204, 204)
                .addComponent(OP3, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(separator2, GroupLayout.PREFERRED_SIZE, 500, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_53_OBJ_53, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(MOADEB, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addGap(88, 88, 88)
                .addComponent(OBJ_55_OBJ_55, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(MOAFIN, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_58_OBJ_58, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_59_OBJ_59, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_61_OBJ_61, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(MOEDEB, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addGap(88, 88, 88)
                .addComponent(OBJ_63_OBJ_63, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(MOEFIN, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(SECDEB, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(66, 66, 66)
                .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(SECFIN, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_69_OBJ_69, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(AFFDEB, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58)
                .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(AFFFIN, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_73_OBJ_73, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(ZGEDEB, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58)
                .addComponent(OBJ_75_OBJ_75, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(ZGEFIN, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(REPEX, GroupLayout.PREFERRED_SIZE, 259, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(DGDE1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(DGFE1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGap(21, 21, 21)
                .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(GFSDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(GFSFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OP1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(4, 4, 4)
                    .addComponent(OP2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(4, 4, 4)
                .addComponent(OP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(separator2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_53_OBJ_53))
                  .addComponent(MOADEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_55_OBJ_55))
                  .addComponent(MOAFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_58_OBJ_58))
                  .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_59_OBJ_59))
                  .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_61_OBJ_61))
                  .addComponent(MOEDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_63_OBJ_63))
                  .addComponent(MOEFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_65_OBJ_65))
                  .addComponent(SECDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_67_OBJ_67))
                  .addComponent(SECFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_69_OBJ_69))
                  .addComponent(AFFDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_71_OBJ_71))
                  .addComponent(AFFFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_73_OBJ_73))
                  .addComponent(ZGEDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_75_OBJ_75))
                  .addComponent(ZGEFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addComponent(REPEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private XRiCheckBox REPEX;
  private XRiCheckBox OP3;
  private XRiCheckBox OP1;
  private JLabel OBJ_53_OBJ_53;
  private XRiCheckBox OP2;
  private JLabel OBJ_73_OBJ_73;
  private JLabel OBJ_65_OBJ_65;
  private JLabel OBJ_61_OBJ_61;
  private JLabel OBJ_58_OBJ_58;
  private XRiTextField GFSDEB;
  private XRiTextField GFSFIN;
  private XRiTextField AFFDEB;
  private XRiTextField AFFFIN;
  private XRiTextField ZGEDEB;
  private XRiTextField ZGEFIN;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private JLabel OBJ_69_OBJ_69;
  private XRiTextField SECDEB;
  private XRiTextField SECFIN;
  private XRiTextField MOADEB;
  private XRiTextField MOAFIN;
  private XRiTextField MOEDEB;
  private XRiTextField MOEFIN;
  private JLabel OBJ_48_OBJ_48;
  private JLabel OBJ_55_OBJ_55;
  private JLabel OBJ_59_OBJ_59;
  private JLabel OBJ_63_OBJ_63;
  private JLabel OBJ_67_OBJ_67;
  private JLabel OBJ_71_OBJ_71;
  private JLabel OBJ_75_OBJ_75;
  private JComponent separator1;
  private JComponent separator2;
  private RiZoneSortie DGDE1X;
  private RiZoneSortie DGFE1X;
  private JLabel OBJ_38_OBJ_38;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
