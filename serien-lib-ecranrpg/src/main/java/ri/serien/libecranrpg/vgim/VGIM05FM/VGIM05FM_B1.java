
package ri.serien.libecranrpg.vgim.VGIM05FM;
// Nom Fichier: pop_VGIM05FM_FMTB1_108.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGIM05FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _S01_Tops = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", "T09", "T10", "T11", "T12", "T13", "T14", "T15" };
  private int[] _S01_Justification =
      { SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  private String[] _S01_Title = { "Groupe", "Famille", "Val. acquisition", "Amort. antérieur", "Amort. exercice", "Val. résiduelle", };
  private String[][] _S01_Data = { { "S01", "LE01", "F01", "G01", "H01", "J01", }, { "S02", "LE02", "F02", "G02", "H02", "J02", },
      { "S03", "LE03", "F03", "G03", "H03", "J03", }, { "S04", "LE04", "F04", "G04", "H04", "J04", },
      { "S05", "LE05", "F05", "G05", "H05", "J05", }, { "S06", "LE06", "F06", "G06", "H06", "J06", },
      { "S07", "LE07", "F07", "G07", "H07", "J07", }, { "S08", "LE08", "F08", "G08", "H08", "J08", },
      { "S09", "LE09", "F09", "G09", "H09", "J09", }, { "S10", "LE10", "F10", "G10", "H10", "J10", },
      { "S11", "LE11", "F11", "G11", "H11", "J11", }, { "S12", "LE12", "F12", "G12", "H12", "J12", },
      { "S13", "LE13", "F13", "G13", "H13", "J13", }, { "S14", "LE14", "F14", "G14", "H14", "J14", },
      { "S15", "LE15", "F15", "G15", "H15", "J15", }, };
  private int[] _S01_Width = { 51, 144, 98, 98, 98, 98, };
  private VGIM05FM_GRAPHE graphe = null;
  
  public VGIM05FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    S01.setAspectTable(_S01_Tops, _S01_Title, _S01_Data, _S01_Width, false, _S01_Justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    OBJ_53.setVisible(!lexique.HostFieldGetData("S15").trim().equalsIgnoreCase(""));
    OBJ_52.setVisible(!lexique.HostFieldGetData("S14").trim().equalsIgnoreCase(""));
    OBJ_51.setVisible(!lexique.HostFieldGetData("S13").trim().equalsIgnoreCase(""));
    OBJ_50.setVisible(!lexique.HostFieldGetData("S12").trim().equalsIgnoreCase(""));
    OBJ_49.setVisible(!lexique.HostFieldGetData("S11").trim().equalsIgnoreCase(""));
    OBJ_48.setVisible(!lexique.HostFieldGetData("S10").trim().equalsIgnoreCase(""));
    OBJ_47.setVisible(!lexique.HostFieldGetData("S09").trim().equalsIgnoreCase(""));
    OBJ_46.setVisible(!lexique.HostFieldGetData("S08").trim().equalsIgnoreCase(""));
    OBJ_45.setVisible(!lexique.HostFieldGetData("S07").trim().equalsIgnoreCase(""));
    OBJ_44.setVisible(!lexique.HostFieldGetData("S06").trim().equalsIgnoreCase(""));
    OBJ_43.setVisible(!lexique.HostFieldGetData("S05").trim().equalsIgnoreCase(""));
    OBJ_42.setVisible(!lexique.HostFieldGetData("S04").trim().equalsIgnoreCase(""));
    OBJ_41.setVisible(!lexique.HostFieldGetData("S03").trim().equalsIgnoreCase(""));
    OBJ_40.setVisible(!lexique.HostFieldGetData("S02").trim().equalsIgnoreCase(""));
    OBJ_39.setVisible(!lexique.HostFieldGetData("S01").trim().equalsIgnoreCase(""));
    
    // TODO Icones
    OBJ_53.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_52.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_51.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_50.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_49.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_48.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_47.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_46.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_45.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_44.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_43.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_42.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_41.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_40.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_39.setIcon(lexique.chargerImage("images/stat011.gif", true));
    OBJ_38.setIcon(lexique.chargerImage("images/stat03.gif", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("IMMOBILISATION"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    // graphe sur total
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    S01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_39ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 1);
    }
  }
  
  private void OBJ_40ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 2);
    }
  }
  
  private void OBJ_41ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 3);
    }
  }
  
  private void OBJ_42ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 4);
    }
  }
  
  private void OBJ_43ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 5);
    }
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 6);
    }
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 7);
    }
  }
  
  private void OBJ_46ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 8);
    }
  }
  
  private void OBJ_47ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 9);
    }
  }
  
  private void OBJ_48ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 10);
    }
  }
  
  private void OBJ_49ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 11);
    }
  }
  
  private void OBJ_50ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 12);
    }
  }
  
  private void OBJ_51ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 13);
    }
  }
  
  private void OBJ_52ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 14);
    }
  }
  
  private void OBJ_53ActionPerformed(ActionEvent e) {
    if (graphe != null) {
      graphe.reveiller();
    }
    else {
      graphe = new VGIM05FM_GRAPHE(this, lexique, interpreteurD, 15);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_16 = new JMenuItem();
    OBJ_38 = new JButton();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    S01 = new XRiTable();
    OBJ_30 = new JButton();
    TOT1 = new XRiTextField();
    TOT2 = new XRiTextField();
    TOT3 = new XRiTextField();
    TOT4 = new XRiTextField();
    OBJ_39 = new JButton();
    OBJ_40 = new JButton();
    OBJ_41 = new JButton();
    OBJ_42 = new JButton();
    OBJ_43 = new JButton();
    OBJ_44 = new JButton();
    OBJ_45 = new JButton();
    OBJ_46 = new JButton();
    OBJ_47 = new JButton();
    OBJ_48 = new JButton();
    OBJ_49 = new JButton();
    OBJ_50 = new JButton();
    OBJ_51 = new JButton();
    OBJ_52 = new JButton();
    OBJ_53 = new JButton();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }

    //---- OBJ_38 ----
    OBJ_38.setText("");
    OBJ_38.setToolTipText("Graphe statistique sur le total");
    OBJ_38.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_38.setName("OBJ_38");

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau de bord des immobilisations");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== SCROLLPANE_LIST2 ========
          {
            SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

            //---- S01 ----
            S01.setName("S01");
            SCROLLPANE_LIST2.setViewportView(S01);
          }

          //---- OBJ_30 ----
          OBJ_30.setText("TOTAL");
          OBJ_30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_30.setName("OBJ_30");
          OBJ_30.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_30ActionPerformed(e);
            }
          });

          //---- TOT1 ----
          TOT1.setName("TOT1");

          //---- TOT2 ----
          TOT2.setName("TOT2");

          //---- TOT3 ----
          TOT3.setName("TOT3");

          //---- TOT4 ----
          TOT4.setName("TOT4");

          //---- OBJ_39 ----
          OBJ_39.setText("");
          OBJ_39.setToolTipText("Graphe statistique");
          OBJ_39.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_39.setName("OBJ_39");
          OBJ_39.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_39ActionPerformed(e);
            }
          });

          //---- OBJ_40 ----
          OBJ_40.setText("");
          OBJ_40.setToolTipText("Graphe statistique");
          OBJ_40.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_40.setName("OBJ_40");
          OBJ_40.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_40ActionPerformed(e);
            }
          });

          //---- OBJ_41 ----
          OBJ_41.setText("");
          OBJ_41.setToolTipText("Graphe statistique");
          OBJ_41.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_41.setName("OBJ_41");
          OBJ_41.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_41ActionPerformed(e);
            }
          });

          //---- OBJ_42 ----
          OBJ_42.setText("");
          OBJ_42.setToolTipText("Graphe statistique");
          OBJ_42.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_42.setName("OBJ_42");
          OBJ_42.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_42ActionPerformed(e);
            }
          });

          //---- OBJ_43 ----
          OBJ_43.setText("");
          OBJ_43.setToolTipText("Graphe statistique");
          OBJ_43.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_43.setName("OBJ_43");
          OBJ_43.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_43ActionPerformed(e);
            }
          });

          //---- OBJ_44 ----
          OBJ_44.setText("");
          OBJ_44.setToolTipText("Graphe statistique");
          OBJ_44.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_44.setName("OBJ_44");
          OBJ_44.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_44ActionPerformed(e);
            }
          });

          //---- OBJ_45 ----
          OBJ_45.setText("");
          OBJ_45.setToolTipText("Graphe statistique");
          OBJ_45.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_45.setName("OBJ_45");
          OBJ_45.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_45ActionPerformed(e);
            }
          });

          //---- OBJ_46 ----
          OBJ_46.setText("");
          OBJ_46.setToolTipText("Graphe statistique");
          OBJ_46.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_46.setName("OBJ_46");
          OBJ_46.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_46ActionPerformed(e);
            }
          });

          //---- OBJ_47 ----
          OBJ_47.setText("");
          OBJ_47.setToolTipText("Graphe statistique");
          OBJ_47.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_47.setName("OBJ_47");
          OBJ_47.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_47ActionPerformed(e);
            }
          });

          //---- OBJ_48 ----
          OBJ_48.setText("");
          OBJ_48.setToolTipText("Graphe statistique");
          OBJ_48.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_48.setName("OBJ_48");
          OBJ_48.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_48ActionPerformed(e);
            }
          });

          //---- OBJ_49 ----
          OBJ_49.setText("");
          OBJ_49.setToolTipText("Graphe statistique");
          OBJ_49.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_49.setName("OBJ_49");
          OBJ_49.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_49ActionPerformed(e);
            }
          });

          //---- OBJ_50 ----
          OBJ_50.setText("");
          OBJ_50.setToolTipText("Graphe statistique");
          OBJ_50.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_50.setName("OBJ_50");
          OBJ_50.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_50ActionPerformed(e);
            }
          });

          //---- OBJ_51 ----
          OBJ_51.setText("");
          OBJ_51.setToolTipText("Graphe statistique");
          OBJ_51.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_51.setName("OBJ_51");
          OBJ_51.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_51ActionPerformed(e);
            }
          });

          //---- OBJ_52 ----
          OBJ_52.setText("");
          OBJ_52.setToolTipText("Graphe statistique");
          OBJ_52.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_52.setName("OBJ_52");
          OBJ_52.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_52ActionPerformed(e);
            }
          });

          //---- OBJ_53 ----
          OBJ_53.setText("");
          OBJ_53.setToolTipText("Graphe statistique");
          OBJ_53.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_53.setName("OBJ_53");
          OBJ_53.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_53ActionPerformed(e);
            }
          });

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addComponent(SCROLLPANE_LIST2, GroupLayout.PREFERRED_SIZE, 820, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addGap(75, 75, 75)
                .addComponent(TOT1, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(TOT2, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(TOT3, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(TOT4, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
                  .addComponent(SCROLLPANE_LIST2, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                  .addComponent(TOT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TOT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TOT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(TOT4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_16;
  private JButton OBJ_38;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable S01;
  private JButton OBJ_30;
  private XRiTextField TOT1;
  private XRiTextField TOT2;
  private XRiTextField TOT3;
  private XRiTextField TOT4;
  private JButton OBJ_39;
  private JButton OBJ_40;
  private JButton OBJ_41;
  private JButton OBJ_42;
  private JButton OBJ_43;
  private JButton OBJ_44;
  private JButton OBJ_45;
  private JButton OBJ_46;
  private JButton OBJ_47;
  private JButton OBJ_48;
  private JButton OBJ_49;
  private JButton OBJ_50;
  private JButton OBJ_51;
  private JButton OBJ_52;
  private JButton OBJ_53;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
