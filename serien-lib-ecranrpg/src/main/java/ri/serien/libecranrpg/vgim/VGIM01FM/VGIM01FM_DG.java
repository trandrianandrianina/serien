
package ri.serien.libecranrpg.vgim.VGIM01FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGIM01FM_DG extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] DGCCO_Value = { "", "E", "M" };
  
  public VGIM01FM_DG(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    DGCCO.setValeurs(DGCCO_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    DXDVL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DXDVL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // DGCCO.setSelectedIndex(getIndice("DGCCO", DGCCO_Value));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("DGCCO", 0, DGCCO_Value[DGCCO.getSelectedIndex()]);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_61 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_62 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    label1 = new JLabel();
    panel1 = new JPanel();
    label6 = new JLabel();
    DGDEBX = new XRiCalendrier();
    label7 = new JLabel();
    DGFINX = new XRiCalendrier();
    label8 = new JLabel();
    DGMEX = new XRiTextField();
    label9 = new JLabel();
    DGMPR = new XRiTextField();
    panel2 = new JPanel();
    label10 = new JLabel();
    DGDE1X = new XRiCalendrier();
    label11 = new JLabel();
    DGFE1X = new XRiCalendrier();
    DGDP1X = new XRiCalendrier();
    label12 = new JLabel();
    DGFP1X = new XRiCalendrier();
    label13 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    panel3 = new JPanel();
    DGNCA = new XRiTextField();
    DGNDA = new XRiTextField();
    DGNCD = new XRiTextField();
    DGNDD = new XRiTextField();
    DGNCI = new XRiTextField();
    NDR = new XRiTextField();
    label15 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    label18 = new JLabel();
    label19 = new JLabel();
    label20 = new JLabel();
    label5 = new JLabel();
    DGNOM = new XRiTextField();
    DGCPL = new XRiTextField();
    DGNBS = new XRiTextField();
    DGCCO = new XRiComboBox();
    DGSOC = new XRiTextField();
    DGCJM = new XRiTextField();
    DGDEV = new XRiTextField();
    DXDVL = new RiZoneSortie();
    label21 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1000, 660));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation des immobilisations");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_61 ----
          OBJ_61.setText("Code");
          OBJ_61.setName("OBJ_61");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");

          //---- OBJ_62 ----
          OBJ_62.setText("Ordre");
          OBJ_62.setName("OBJ_62");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 551));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(900, 551));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Description g\u00e9n\u00e9rale");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- label1 ----
            label1.setText("Nom ou raison sociale");
            label1.setName("label1");
            xTitledPanel1ContentContainer.add(label1);
            label1.setBounds(20, 19, 135, 20);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("Exercice de d\u00e9marrage"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- label6 ----
              label6.setText("Du");
              label6.setName("label6");
              panel1.add(label6);
              label6.setBounds(28, 46, 22, label6.getPreferredSize().height);

              //---- DGDEBX ----
              DGDEBX.setTypeSaisie(6);
              DGDEBX.setName("DGDEBX");
              panel1.add(DGDEBX);
              DGDEBX.setBounds(60, 40, 90, DGDEBX.getPreferredSize().height);

              //---- label7 ----
              label7.setText("au");
              label7.setHorizontalAlignment(SwingConstants.CENTER);
              label7.setName("label7");
              panel1.add(label7);
              label7.setBounds(150, 46, 55, label7.getPreferredSize().height);

              //---- DGFINX ----
              DGFINX.setTypeSaisie(6);
              DGFINX.setName("DGFINX");
              panel1.add(DGFINX);
              DGFINX.setBounds(203, 40, 90, DGFINX.getPreferredSize().height);

              //---- label8 ----
              label8.setText("Nombre de mois par exercice");
              label8.setName("label8");
              panel1.add(label8);
              label8.setBounds(325, 44, 175, 20);

              //---- DGMEX ----
              DGMEX.setName("DGMEX");
              panel1.add(DGMEX);
              DGMEX.setBounds(500, 40, 30, DGMEX.getPreferredSize().height);

              //---- label9 ----
              label9.setText("Nombre de mois par p\u00e9riode");
              label9.setName("label9");
              panel1.add(label9);
              label9.setBounds(560, 44, 170, 20);

              //---- DGMPR ----
              DGMPR.setName("DGMPR");
              panel1.add(DGMPR);
              DGMPR.setBounds(730, 40, 30, DGMPR.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(15, 65, 835, 90);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Exercice en cours"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- label10 ----
              label10.setText("Du");
              label10.setName("label10");
              panel2.add(label10);
              label10.setBounds(28, 44, 27, 20);

              //---- DGDE1X ----
              DGDE1X.setTypeSaisie(6);
              DGDE1X.setName("DGDE1X");
              panel2.add(DGDE1X);
              DGDE1X.setBounds(60, 40, 90, DGDE1X.getPreferredSize().height);

              //---- label11 ----
              label11.setText("au");
              label11.setHorizontalAlignment(SwingConstants.CENTER);
              label11.setName("label11");
              panel2.add(label11);
              label11.setBounds(150, 44, 55, 20);

              //---- DGFE1X ----
              DGFE1X.setTypeSaisie(6);
              DGFE1X.setName("DGFE1X");
              panel2.add(DGFE1X);
              DGFE1X.setBounds(205, 40, 90, DGFE1X.getPreferredSize().height);

              //---- DGDP1X ----
              DGDP1X.setTypeSaisie(6);
              DGDP1X.setName("DGDP1X");
              panel2.add(DGDP1X);
              DGDP1X.setBounds(435, 40, 90, DGDP1X.getPreferredSize().height);

              //---- label12 ----
              label12.setText("P\u00e9riode en cours");
              label12.setName("label12");
              panel2.add(label12);
              label12.setBounds(325, 44, 105, 20);

              //---- DGFP1X ----
              DGFP1X.setTypeSaisie(6);
              DGFP1X.setName("DGFP1X");
              panel2.add(DGFP1X);
              DGFP1X.setBounds(570, 40, 90, DGFP1X.getPreferredSize().height);

              //---- label13 ----
              label13.setText("/");
              label13.setHorizontalAlignment(SwingConstants.CENTER);
              label13.setName("label13");
              panel2.add(label13);
              label13.setBounds(525, 46, 45, label13.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(15, 155, 835, 90);

            //---- label2 ----
            label2.setText("Comptabilisation");
            label2.setName("label2");
            xTitledPanel1ContentContainer.add(label2);
            label2.setBounds(20, 260, 105, 20);

            //---- label3 ----
            label3.setText("Etablissement comptable");
            label3.setName("label3");
            xTitledPanel1ContentContainer.add(label3);
            label3.setBounds(320, 260, 170, 20);

            //---- label4 ----
            label4.setText("Code journal d'amortissement");
            label4.setName("label4");
            xTitledPanel1ContentContainer.add(label4);
            label4.setBounds(560, 260, 180, 20);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Num\u00e9ros de postes comptables"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- DGNCA ----
              DGNCA.setComponentPopupMenu(BTD);
              DGNCA.setName("DGNCA");
              panel3.add(DGNCA);
              DGNCA.setBounds(205, 40, 40, DGNCA.getPreferredSize().height);

              //---- DGNDA ----
              DGNDA.setComponentPopupMenu(BTD);
              DGNDA.setName("DGNDA");
              panel3.add(DGNDA);
              DGNDA.setBounds(205, 80, 40, DGNDA.getPreferredSize().height);

              //---- DGNCD ----
              DGNCD.setComponentPopupMenu(BTD);
              DGNCD.setName("DGNCD");
              panel3.add(DGNCD);
              DGNCD.setBounds(470, 40, 40, DGNCD.getPreferredSize().height);

              //---- DGNDD ----
              DGNDD.setComponentPopupMenu(BTD);
              DGNDD.setName("DGNDD");
              panel3.add(DGNDD);
              DGNDD.setBounds(470, 80, 40, DGNDD.getPreferredSize().height);

              //---- DGNCI ----
              DGNCI.setComponentPopupMenu(BTD);
              DGNCI.setName("DGNCI");
              panel3.add(DGNCI);
              DGNCI.setBounds(730, 40, 40, DGNCI.getPreferredSize().height);

              //---- NDR ----
              NDR.setComponentPopupMenu(BTD);
              NDR.setName("NDR");
              panel3.add(NDR);
              NDR.setBounds(730, 80, 40, NDR.getPreferredSize().height);

              //---- label15 ----
              label15.setText("Amortissement");
              label15.setName("label15");
              panel3.add(label15);
              label15.setBounds(30, 44, 105, 20);

              //---- label16 ----
              label16.setText("Amortissement d\u00e9rogatoire");
              label16.setName("label16");
              panel3.add(label16);
              label16.setBounds(30, 85, 160, 20);

              //---- label17 ----
              label17.setText("Dotation");
              label17.setName("label17");
              panel3.add(label17);
              label17.setBounds(305, 44, 84, 20);

              //---- label18 ----
              label18.setText("Dotation exceptionnelle");
              label18.setName("label18");
              panel3.add(label18);
              label18.setBounds(305, 82, 145, 20);

              //---- label19 ----
              label19.setText("Immobilisation");
              label19.setName("label19");
              panel3.add(label19);
              label19.setBounds(585, 44, 115, 20);

              //---- label20 ----
              label20.setText("Reprise exceptionnelle");
              label20.setName("label20");
              panel3.add(label20);
              label20.setBounds(585, 86, 135, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(15, 300, 835, 135);

            //---- label5 ----
            label5.setText("Devise locale");
            label5.setName("label5");
            xTitledPanel1ContentContainer.add(label5);
            label5.setBounds(25, 450, 110, 20);

            //---- DGNOM ----
            DGNOM.setName("DGNOM");
            xTitledPanel1ContentContainer.add(DGNOM);
            DGNOM.setBounds(155, 15, 300, DGNOM.getPreferredSize().height);

            //---- DGCPL ----
            DGCPL.setName("DGCPL");
            xTitledPanel1ContentContainer.add(DGCPL);
            DGCPL.setBounds(460, 15, 300, DGCPL.getPreferredSize().height);

            //---- DGNBS ----
            DGNBS.setName("DGNBS");
            xTitledPanel1ContentContainer.add(DGNBS);
            DGNBS.setBounds(800, 15, 40, DGNBS.getPreferredSize().height);

            //---- DGCCO ----
            DGCCO.setModel(new DefaultComboBoxModel(new String[] {
              "\u00e0 l'exercice",
              "au mois"
            }));
            DGCCO.setName("DGCCO");
            xTitledPanel1ContentContainer.add(DGCCO);
            DGCCO.setBounds(130, 257, 135, DGCCO.getPreferredSize().height);

            //---- DGSOC ----
            DGSOC.setComponentPopupMenu(BTD);
            DGSOC.setName("DGSOC");
            xTitledPanel1ContentContainer.add(DGSOC);
            DGSOC.setBounds(485, 256, 40, DGSOC.getPreferredSize().height);

            //---- DGCJM ----
            DGCJM.setName("DGCJM");
            xTitledPanel1ContentContainer.add(DGCJM);
            DGCJM.setBounds(745, 256, 34, DGCJM.getPreferredSize().height);

            //---- DGDEV ----
            DGDEV.setComponentPopupMenu(BTD);
            DGDEV.setName("DGDEV");
            xTitledPanel1ContentContainer.add(DGDEV);
            DGDEV.setBounds(155, 445, 40, DGDEV.getPreferredSize().height);

            //---- DXDVL ----
            DXDVL.setText("@DXDVL@");
            DXDVL.setName("DXDVL");
            xTitledPanel1ContentContainer.add(DXDVL);
            DXDVL.setBounds(205, 447, 320, DXDVL.getPreferredSize().height);

            //---- label21 ----
            label21.setText("NbS");
            label21.setName("label21");
            xTitledPanel1ContentContainer.add(label21);
            label21.setBounds(765, 19, 35, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 875, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 515, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(13, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDETB;
  private JLabel OBJ_61;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_62;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel label1;
  private JPanel panel1;
  private JLabel label6;
  private XRiCalendrier DGDEBX;
  private JLabel label7;
  private XRiCalendrier DGFINX;
  private JLabel label8;
  private XRiTextField DGMEX;
  private JLabel label9;
  private XRiTextField DGMPR;
  private JPanel panel2;
  private JLabel label10;
  private XRiCalendrier DGDE1X;
  private JLabel label11;
  private XRiCalendrier DGFE1X;
  private XRiCalendrier DGDP1X;
  private JLabel label12;
  private XRiCalendrier DGFP1X;
  private JLabel label13;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JPanel panel3;
  private XRiTextField DGNCA;
  private XRiTextField DGNDA;
  private XRiTextField DGNCD;
  private XRiTextField DGNDD;
  private XRiTextField DGNCI;
  private XRiTextField NDR;
  private JLabel label15;
  private JLabel label16;
  private JLabel label17;
  private JLabel label18;
  private JLabel label19;
  private JLabel label20;
  private JLabel label5;
  private XRiTextField DGNOM;
  private XRiTextField DGCPL;
  private XRiTextField DGNBS;
  private XRiComboBox DGCCO;
  private XRiTextField DGSOC;
  private XRiTextField DGCJM;
  private XRiTextField DGDEV;
  private RiZoneSortie DXDVL;
  private JLabel label21;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
