
package ri.serien.libecranrpg.vgim.VGIMSEFM;
// Nom Fichier: i_VGIMSEFM_FMTB1_FMTF1_62.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGIMSEFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGIMSEFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDUSR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUSR@")).trim());
    INDETB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCUSR@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    pnlNord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDUSR2 = new RiZoneSortie();
    OBJ_64 = new JLabel();
    INDETB2 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_78 = new JLabel();
    pnlSud = new SNPanelFond();
    pnlMenus = new SNPanelContenu();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    xTitledPanel1 = new JXTitledPanel();
    panel1 = new JPanel();
    OBJ_94_OBJ_94 = new JLabel();
    OBJ_92_OBJ_92 = new JLabel();
    OBJ_108_OBJ_108 = new JLabel();
    OBJ_110_OBJ_110 = new JLabel();
    OBJ_98_OBJ_98 = new JLabel();
    OBJ_96_OBJ_96 = new JLabel();
    OBJ_89_OBJ_89 = new JLabel();
    OBJ_102_OBJ_102 = new JLabel();
    OBJ_104_OBJ_104 = new JLabel();
    OBJ_106_OBJ_106 = new JLabel();
    OBJ_90_OBJ_90 = new JLabel();
    OBJ_100_OBJ_100 = new JLabel();
    SET001 = new XRiTextField();
    SET002 = new XRiTextField();
    SET003 = new XRiTextField();
    SET004 = new XRiTextField();
    SET005 = new XRiTextField();
    SET006 = new XRiTextField();
    SET007 = new XRiTextField();
    SET008 = new XRiTextField();
    SET009 = new XRiTextField();
    SET010 = new XRiTextField();
    SET011 = new XRiTextField();
    SET012 = new XRiTextField();
    OBJ_76_OBJ_76 = new JLabel();
    OBJ_78_OBJ_78 = new JLabel();
    OBJ_79_OBJ_79 = new JLabel();
    OBJ_80_OBJ_80 = new JLabel();
    OBJ_81_OBJ_81 = new JLabel();
    OBJ_82_OBJ_82 = new JLabel();
    OBJ_83_OBJ_83 = new JLabel();
    OBJ_84_OBJ_84 = new JLabel();
    OBJ_85_OBJ_85 = new JLabel();
    OBJ_86_OBJ_86 = new JLabel();
    OBJ_87_OBJ_87 = new JLabel();
    OBJ_88_OBJ_88 = new JLabel();
    OBJ_68 = new SNBoutonLeger();
    V06F = new XRiTextField();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("S\u00e9curit\u00e9 des immobilisations");
      p_bpresentation.setName("p_bpresentation");
      pnlNord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_42 ----
          OBJ_42.setText("Utilisateur");
          OBJ_42.setName("OBJ_42");
          p_tete_gauche.add(OBJ_42);
          OBJ_42.setBounds(5, 5, 78, 20);

          //---- INDUSR2 ----
          INDUSR2.setComponentPopupMenu(BTD);
          INDUSR2.setOpaque(false);
          INDUSR2.setText("@INDUSR@");
          INDUSR2.setName("INDUSR2");
          p_tete_gauche.add(INDUSR2);
          INDUSR2.setBounds(110, 3, 110, INDUSR2.getPreferredSize().height);

          //---- OBJ_64 ----
          OBJ_64.setText("Etablissement");
          OBJ_64.setName("OBJ_64");
          p_tete_gauche.add(OBJ_64);
          OBJ_64.setBounds(280, 5, 97, 20);

          //---- INDETB2 ----
          INDETB2.setComponentPopupMenu(BTD);
          INDETB2.setOpaque(false);
          INDETB2.setText("@INDETB@");
          INDETB2.setName("INDETB2");
          p_tete_gauche.add(INDETB2);
          INDETB2.setBounds(380, 3, 40, INDETB2.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_78 ----
          OBJ_78.setText("@LOCUSR@");
          OBJ_78.setName("OBJ_78");
          p_tete_droite.add(OBJ_78);
        }
        barre_tete.add(p_tete_droite);
      }
      pnlNord.add(barre_tete);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());

      //======== pnlMenus ========
      {
        pnlMenus.setPreferredSize(new Dimension(170, 0));
        pnlMenus.setMinimumSize(new Dimension(170, 0));
        pnlMenus.setBackground(new Color(238, 239, 241));
        pnlMenus.setBorder(LineBorder.createBlackLineBorder());
        pnlMenus.setName("pnlMenus");
        pnlMenus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        pnlMenus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        pnlMenus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlSud.add(pnlMenus, BorderLayout.LINE_END);

      //======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(490, 490));
        pnlContenu.setBackground(new Color(239, 239, 222));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitle("Objet de la s\u00e9curit\u00e9 (G.I.M)");
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- OBJ_94_OBJ_94 ----
            OBJ_94_OBJ_94.setText("Edition des immobilisations avec valeur");
            OBJ_94_OBJ_94.setFont(OBJ_94_OBJ_94.getFont().deriveFont(OBJ_94_OBJ_94.getFont().getSize() + 3f));
            OBJ_94_OBJ_94.setName("OBJ_94_OBJ_94");

            //---- OBJ_92_OBJ_92 ----
            OBJ_92_OBJ_92.setText("Edition des immobilisations sans valeur");
            OBJ_92_OBJ_92.setFont(OBJ_92_OBJ_92.getFont().deriveFont(OBJ_92_OBJ_92.getFont().getSize() + 3f));
            OBJ_92_OBJ_92.setName("OBJ_92_OBJ_92");

            //---- OBJ_108_OBJ_108 ----
            OBJ_108_OBJ_108.setText("Comptabilisation simul\u00e9e d'amortissement");
            OBJ_108_OBJ_108.setFont(OBJ_108_OBJ_108.getFont().deriveFont(OBJ_108_OBJ_108.getFont().getSize() + 3f));
            OBJ_108_OBJ_108.setName("OBJ_108_OBJ_108");

            //---- OBJ_110_OBJ_110 ----
            OBJ_110_OBJ_110.setText("Arr\u00eat\u00e9 de fin de p\u00e9riode ou d'exercice");
            OBJ_110_OBJ_110.setFont(OBJ_110_OBJ_110.getFont().deriveFont(OBJ_110_OBJ_110.getFont().getSize() + 3f));
            OBJ_110_OBJ_110.setName("OBJ_110_OBJ_110");

            //---- OBJ_98_OBJ_98 ----
            OBJ_98_OBJ_98.setText("Ech\u00e9ancier d'amortissements");
            OBJ_98_OBJ_98.setFont(OBJ_98_OBJ_98.getFont().deriveFont(OBJ_98_OBJ_98.getFont().getSize() + 3f));
            OBJ_98_OBJ_98.setName("OBJ_98_OBJ_98");

            //---- OBJ_96_OBJ_96 ----
            OBJ_96_OBJ_96.setText("Amortissements de l'exercice");
            OBJ_96_OBJ_96.setFont(OBJ_96_OBJ_96.getFont().deriveFont(OBJ_96_OBJ_96.getFont().getSize() + 3f));
            OBJ_96_OBJ_96.setName("OBJ_96_OBJ_96");

            //---- OBJ_89_OBJ_89 ----
            OBJ_89_OBJ_89.setText("Personnalisation du syst\u00e8me");
            OBJ_89_OBJ_89.setFont(OBJ_89_OBJ_89.getFont().deriveFont(OBJ_89_OBJ_89.getFont().getSize() + 3f));
            OBJ_89_OBJ_89.setName("OBJ_89_OBJ_89");

            //---- OBJ_102_OBJ_102 ----
            OBJ_102_OBJ_102.setText("Ventilation de l'amortissement de l'exercice");
            OBJ_102_OBJ_102.setFont(OBJ_102_OBJ_102.getFont().deriveFont(OBJ_102_OBJ_102.getFont().getSize() + 3f));
            OBJ_102_OBJ_102.setName("OBJ_102_OBJ_102");

            //---- OBJ_104_OBJ_104 ----
            OBJ_104_OBJ_104.setText("Pr\u00e9paration de l'\u00e9tat 2054N");
            OBJ_104_OBJ_104.setFont(OBJ_104_OBJ_104.getFont().deriveFont(OBJ_104_OBJ_104.getFont().getSize() + 3f));
            OBJ_104_OBJ_104.setName("OBJ_104_OBJ_104");

            //---- OBJ_106_OBJ_106 ----
            OBJ_106_OBJ_106.setText("Pr\u00e9paration de l'\u00e9tat 2055N");
            OBJ_106_OBJ_106.setFont(OBJ_106_OBJ_106.getFont().deriveFont(OBJ_106_OBJ_106.getFont().getSize() + 3f));
            OBJ_106_OBJ_106.setName("OBJ_106_OBJ_106");

            //---- OBJ_90_OBJ_90 ----
            OBJ_90_OBJ_90.setText("Gestion des immobilisations");
            OBJ_90_OBJ_90.setFont(OBJ_90_OBJ_90.getFont().deriveFont(OBJ_90_OBJ_90.getFont().getSize() + 3f));
            OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");

            //---- OBJ_100_OBJ_100 ----
            OBJ_100_OBJ_100.setText("Etat de cessions de l'exercice");
            OBJ_100_OBJ_100.setFont(OBJ_100_OBJ_100.getFont().deriveFont(OBJ_100_OBJ_100.getFont().getSize() + 3f));
            OBJ_100_OBJ_100.setName("OBJ_100_OBJ_100");

            //---- SET001 ----
            SET001.setComponentPopupMenu(BTD);
            SET001.setName("SET001");

            //---- SET002 ----
            SET002.setComponentPopupMenu(BTD);
            SET002.setName("SET002");

            //---- SET003 ----
            SET003.setComponentPopupMenu(BTD);
            SET003.setName("SET003");

            //---- SET004 ----
            SET004.setComponentPopupMenu(BTD);
            SET004.setName("SET004");

            //---- SET005 ----
            SET005.setComponentPopupMenu(BTD);
            SET005.setName("SET005");

            //---- SET006 ----
            SET006.setComponentPopupMenu(BTD);
            SET006.setName("SET006");

            //---- SET007 ----
            SET007.setComponentPopupMenu(BTD);
            SET007.setName("SET007");

            //---- SET008 ----
            SET008.setComponentPopupMenu(BTD);
            SET008.setName("SET008");

            //---- SET009 ----
            SET009.setComponentPopupMenu(BTD);
            SET009.setName("SET009");

            //---- SET010 ----
            SET010.setComponentPopupMenu(BTD);
            SET010.setName("SET010");

            //---- SET011 ----
            SET011.setComponentPopupMenu(BTD);
            SET011.setName("SET011");

            //---- SET012 ----
            SET012.setComponentPopupMenu(BTD);
            SET012.setName("SET012");

            //---- OBJ_76_OBJ_76 ----
            OBJ_76_OBJ_76.setText("01");
            OBJ_76_OBJ_76.setFont(OBJ_76_OBJ_76.getFont().deriveFont(OBJ_76_OBJ_76.getFont().getStyle() | Font.BOLD));
            OBJ_76_OBJ_76.setName("OBJ_76_OBJ_76");

            //---- OBJ_78_OBJ_78 ----
            OBJ_78_OBJ_78.setText("02");
            OBJ_78_OBJ_78.setFont(OBJ_78_OBJ_78.getFont().deriveFont(OBJ_78_OBJ_78.getFont().getStyle() | Font.BOLD));
            OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");

            //---- OBJ_79_OBJ_79 ----
            OBJ_79_OBJ_79.setText("03");
            OBJ_79_OBJ_79.setFont(OBJ_79_OBJ_79.getFont().deriveFont(OBJ_79_OBJ_79.getFont().getStyle() | Font.BOLD));
            OBJ_79_OBJ_79.setName("OBJ_79_OBJ_79");

            //---- OBJ_80_OBJ_80 ----
            OBJ_80_OBJ_80.setText("04");
            OBJ_80_OBJ_80.setFont(OBJ_80_OBJ_80.getFont().deriveFont(OBJ_80_OBJ_80.getFont().getStyle() | Font.BOLD));
            OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");

            //---- OBJ_81_OBJ_81 ----
            OBJ_81_OBJ_81.setText("05");
            OBJ_81_OBJ_81.setFont(OBJ_81_OBJ_81.getFont().deriveFont(OBJ_81_OBJ_81.getFont().getStyle() | Font.BOLD));
            OBJ_81_OBJ_81.setName("OBJ_81_OBJ_81");

            //---- OBJ_82_OBJ_82 ----
            OBJ_82_OBJ_82.setText("06");
            OBJ_82_OBJ_82.setFont(OBJ_82_OBJ_82.getFont().deriveFont(OBJ_82_OBJ_82.getFont().getStyle() | Font.BOLD));
            OBJ_82_OBJ_82.setName("OBJ_82_OBJ_82");

            //---- OBJ_83_OBJ_83 ----
            OBJ_83_OBJ_83.setText("07");
            OBJ_83_OBJ_83.setFont(OBJ_83_OBJ_83.getFont().deriveFont(OBJ_83_OBJ_83.getFont().getStyle() | Font.BOLD));
            OBJ_83_OBJ_83.setName("OBJ_83_OBJ_83");

            //---- OBJ_84_OBJ_84 ----
            OBJ_84_OBJ_84.setText("08");
            OBJ_84_OBJ_84.setFont(OBJ_84_OBJ_84.getFont().deriveFont(OBJ_84_OBJ_84.getFont().getStyle() | Font.BOLD));
            OBJ_84_OBJ_84.setName("OBJ_84_OBJ_84");

            //---- OBJ_85_OBJ_85 ----
            OBJ_85_OBJ_85.setText("09");
            OBJ_85_OBJ_85.setFont(OBJ_85_OBJ_85.getFont().deriveFont(OBJ_85_OBJ_85.getFont().getStyle() | Font.BOLD));
            OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");

            //---- OBJ_86_OBJ_86 ----
            OBJ_86_OBJ_86.setText("10");
            OBJ_86_OBJ_86.setFont(OBJ_86_OBJ_86.getFont().deriveFont(OBJ_86_OBJ_86.getFont().getStyle() | Font.BOLD));
            OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");

            //---- OBJ_87_OBJ_87 ----
            OBJ_87_OBJ_87.setText("11");
            OBJ_87_OBJ_87.setFont(OBJ_87_OBJ_87.getFont().deriveFont(OBJ_87_OBJ_87.getFont().getStyle() | Font.BOLD));
            OBJ_87_OBJ_87.setName("OBJ_87_OBJ_87");

            //---- OBJ_88_OBJ_88 ----
            OBJ_88_OBJ_88.setText("12");
            OBJ_88_OBJ_88.setFont(OBJ_88_OBJ_88.getFont().deriveFont(OBJ_88_OBJ_88.getFont().getStyle() | Font.BOLD));
            OBJ_88_OBJ_88.setName("OBJ_88_OBJ_88");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(14, 14, 14)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_79_OBJ_79, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_80_OBJ_80, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_81_OBJ_81, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_82_OBJ_82, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_83_OBJ_83, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_84_OBJ_84, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_85_OBJ_85, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_86_OBJ_86, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_87_OBJ_87, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_88_OBJ_88, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
                  .addGap(14, 14, 14)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(SET003, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SET001, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SET002, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SET010, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SET008, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SET009, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SET007, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SET005, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SET011, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SET006, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SET012, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SET004, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(15, 15, 15)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(OBJ_89_OBJ_89, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_90_OBJ_90, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_92_OBJ_92, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_94_OBJ_94, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_96_OBJ_96, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_98_OBJ_98, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_100_OBJ_100, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_102_OBJ_102, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_104_OBJ_104, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_106_OBJ_106, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_108_OBJ_108, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_110_OBJ_110, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_76_OBJ_76)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_78_OBJ_78)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_79_OBJ_79)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_80_OBJ_80)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_81_OBJ_81)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_82_OBJ_82)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_83_OBJ_83)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_84_OBJ_84)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_85_OBJ_85)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_86_OBJ_86)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_87_OBJ_87)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_88_OBJ_88))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(14, 14, 14)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(52, 52, 52)
                          .addComponent(SET003, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(SET001, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(26, 26, 26)
                          .addComponent(SET002, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(24, 24, 24)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(130, 130, 130)
                          .addComponent(SET010, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(78, 78, 78)
                          .addComponent(SET008, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(104, 104, 104)
                          .addComponent(SET009, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(52, 52, 52)
                          .addComponent(SET007, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(SET005, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(156, 156, 156)
                          .addComponent(SET011, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(26, 26, 26)
                          .addComponent(SET006, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(182, 182, 182)
                          .addComponent(SET012, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(78, 78, 78)
                      .addComponent(SET004, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(18, 18, 18)
                  .addComponent(OBJ_89_OBJ_89)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_90_OBJ_90)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_92_OBJ_92)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_94_OBJ_94)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_96_OBJ_96)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_98_OBJ_98)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_100_OBJ_100)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_102_OBJ_102)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_104_OBJ_104)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_106_OBJ_106)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_108_OBJ_108)
                  .addGap(6, 6, 6)
                  .addComponent(OBJ_110_OBJ_110))
            );
          }
          xTitledPanel1ContentContainer.add(panel1);
          panel1.setBounds(10, 5, 435, 370);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        pnlContenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(15, 10, 464, 421);

        //---- OBJ_68 ----
        OBJ_68.setText("Aller \u00e0 la page");
        OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_68.setName("OBJ_68");
        OBJ_68.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_68ActionPerformed(e);
          }
        });
        pnlContenu.add(OBJ_68);
        OBJ_68.setBounds(new Rectangle(new Point(305, 445), OBJ_68.getPreferredSize()));

        //---- V06F ----
        V06F.setComponentPopupMenu(BTD);
        V06F.setName("V06F");
        pnlContenu.add(V06F);
        V06F.setBounds(450, 445, 25, V06F.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < pnlContenu.getComponentCount(); i++) {
            Rectangle bounds = pnlContenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlContenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlContenu.setMinimumSize(preferredSize);
          pnlContenu.setPreferredSize(preferredSize);
        }
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JPanel pnlNord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDUSR2;
  private JLabel OBJ_64;
  private RiZoneSortie INDETB2;
  private JPanel p_tete_droite;
  private JLabel OBJ_78;
  private SNPanelFond pnlSud;
  private SNPanelContenu pnlMenus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelContenu pnlContenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel1;
  private JLabel OBJ_94_OBJ_94;
  private JLabel OBJ_92_OBJ_92;
  private JLabel OBJ_108_OBJ_108;
  private JLabel OBJ_110_OBJ_110;
  private JLabel OBJ_98_OBJ_98;
  private JLabel OBJ_96_OBJ_96;
  private JLabel OBJ_89_OBJ_89;
  private JLabel OBJ_102_OBJ_102;
  private JLabel OBJ_104_OBJ_104;
  private JLabel OBJ_106_OBJ_106;
  private JLabel OBJ_90_OBJ_90;
  private JLabel OBJ_100_OBJ_100;
  private XRiTextField SET001;
  private XRiTextField SET002;
  private XRiTextField SET003;
  private XRiTextField SET004;
  private XRiTextField SET005;
  private XRiTextField SET006;
  private XRiTextField SET007;
  private XRiTextField SET008;
  private XRiTextField SET009;
  private XRiTextField SET010;
  private XRiTextField SET011;
  private XRiTextField SET012;
  private JLabel OBJ_76_OBJ_76;
  private JLabel OBJ_78_OBJ_78;
  private JLabel OBJ_79_OBJ_79;
  private JLabel OBJ_80_OBJ_80;
  private JLabel OBJ_81_OBJ_81;
  private JLabel OBJ_82_OBJ_82;
  private JLabel OBJ_83_OBJ_83;
  private JLabel OBJ_84_OBJ_84;
  private JLabel OBJ_85_OBJ_85;
  private JLabel OBJ_86_OBJ_86;
  private JLabel OBJ_87_OBJ_87;
  private JLabel OBJ_88_OBJ_88;
  private SNBoutonLeger OBJ_68;
  private XRiTextField V06F;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
