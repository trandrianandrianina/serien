
package ri.serien.libecranrpg.vgim.VGIM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGIM03FM_C2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIM03FM_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    IMRAN.setEnabled(lexique.isPresent("IMRAN"));
    IMMDA.setEnabled(lexique.isPresent("IMMDA"));
    IMDUAR.setEnabled(lexique.isPresent("IMDUAR"));
    IMJDA.setEnabled(lexique.isPresent("IMJDA"));
    UIMNCI.setEnabled(lexique.isPresent("UIMNCI"));
    UIMNCD.setEnabled(lexique.isPresent("UIMNCD"));
    UIMNCA.setEnabled(lexique.isPresent("UIMNCA"));
    IMNCI.setEnabled(lexique.isPresent("IMNCI"));
    IMNCD.setEnabled(lexique.isPresent("IMNCD"));
    IMNCA.setEnabled(lexique.isPresent("IMNCA"));
    // IMDFAX.setEnabled( lexique.isPresent("IMDFAX"));
    // IMDDAX.setEnabled( lexique.isPresent("IMDDAX"));
    IMCOE.setEnabled(lexique.isPresent("IMCOE"));
    IMTXD.setEnabled(lexique.isPresent("IMTXD"));
    IMTXL.setEnabled(lexique.isPresent("IMTXL"));
    IMANF.setEnabled(lexique.isPresent("IMANF"));
    IMAMR.setEnabled(lexique.isPresent("IMAMR"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("IMMOBILISATION"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_32_OBJ_32 = new JLabel();
    OBJ_60_OBJ_60 = new JLabel();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_54_OBJ_54 = new JLabel();
    OBJ_42_OBJ_42 = new JLabel();
    OBJ_46_OBJ_46 = new JLabel();
    OBJ_51_OBJ_51 = new JLabel();
    OBJ_58_OBJ_58 = new JLabel();
    OBJ_39_OBJ_39 = new JLabel();
    IMAMR = new XRiTextField();
    IMANF = new XRiTextField();
    OBJ_44_OBJ_44 = new JLabel();
    OBJ_28_OBJ_28 = new JLabel();
    OBJ_30_OBJ_30 = new JLabel();
    OBJ_56_OBJ_56 = new JLabel();
    IMTXL = new XRiTextField();
    IMTXD = new XRiTextField();
    IMCOE = new XRiTextField();
    OBJ_37_OBJ_37 = new JLabel();
    OBJ_36_OBJ_36 = new JLabel();
    IMNCA = new XRiTextField();
    IMNCD = new XRiTextField();
    IMNCI = new XRiTextField();
    UIMNCA = new XRiTextField();
    UIMNCD = new XRiTextField();
    UIMNCI = new XRiTextField();
    OBJ_41_OBJ_41 = new JLabel();
    IMJDA = new XRiTextField();
    IMDUAR = new XRiTextField();
    IMMDA = new XRiTextField();
    IMRAN = new XRiTextField();
    xTitledSeparator2 = new JXTitledSeparator();
    xTitledSeparator3 = new JXTitledSeparator();
    xTitledSeparator4 = new JXTitledSeparator();
    IMDDAX = new XRiCalendrier();
    IMDFAX = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(705, 400));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Amortissement fiscal"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Fin Amortissement");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          panel1.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(285, 49, 113, 20);

          //---- OBJ_60_OBJ_60 ----
          OBJ_60_OBJ_60.setText("Valeurs / d\u00e9faut");
          OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");
          panel1.add(OBJ_60_OBJ_60);
          OBJ_60_OBJ_60.setBounds(38, 324, 96, 20);

          //---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("Amortissement");
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
          panel1.add(OBJ_49_OBJ_49);
          OBJ_49_OBJ_49.setBounds(38, 224, 92, 20);

          //---- OBJ_54_OBJ_54 ----
          OBJ_54_OBJ_54.setText("Amortissement");
          OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
          panel1.add(OBJ_54_OBJ_54);
          OBJ_54_OBJ_54.setBounds(38, 294, 92, 20);

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("D\u00e9gressif");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
          panel1.add(OBJ_42_OBJ_42);
          OBJ_42_OBJ_42.setBounds(268, 139, 89, 20);

          //---- OBJ_46_OBJ_46 ----
          OBJ_46_OBJ_46.setText("Annuit\u00e9 fixe");
          OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");
          panel1.add(OBJ_46_OBJ_46);
          OBJ_46_OBJ_46.setBounds(268, 169, 89, 20);

          //---- OBJ_51_OBJ_51 ----
          OBJ_51_OBJ_51.setText("Mode");
          OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
          panel1.add(OBJ_51_OBJ_51);
          OBJ_51_OBJ_51.setBounds(268, 224, 89, 20);

          //---- OBJ_58_OBJ_58 ----
          OBJ_58_OBJ_58.setText("Immobilisation");
          OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");
          panel1.add(OBJ_58_OBJ_58);
          OBJ_58_OBJ_58.setBounds(313, 294, 89, 20);

          //---- OBJ_39_OBJ_39 ----
          OBJ_39_OBJ_39.setText("Taux lin\u00e9aire");
          OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");
          panel1.add(OBJ_39_OBJ_39);
          OBJ_39_OBJ_39.setBounds(38, 139, 80, 20);

          //---- IMAMR ----
          IMAMR.setComponentPopupMenu(BTD);
          IMAMR.setName("IMAMR");
          panel1.add(IMAMR);
          IMAMR.setBounds(148, 220, 77, IMAMR.getPreferredSize().height);

          //---- IMANF ----
          IMANF.setComponentPopupMenu(BTD);
          IMANF.setName("IMANF");
          panel1.add(IMANF);
          IMANF.setBounds(358, 165, 100, IMANF.getPreferredSize().height);

          //---- OBJ_44_OBJ_44 ----
          OBJ_44_OBJ_44.setText("Coefficient");
          OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
          panel1.add(OBJ_44_OBJ_44);
          OBJ_44_OBJ_44.setBounds(38, 169, 65, 20);

          //---- OBJ_28_OBJ_28 ----
          OBJ_28_OBJ_28.setText("D\u00e9but jour");
          OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");
          panel1.add(OBJ_28_OBJ_28);
          OBJ_28_OBJ_28.setBounds(38, 49, 64, 20);

          //---- OBJ_30_OBJ_30 ----
          OBJ_30_OBJ_30.setText("/ MM.AA");
          OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");
          panel1.add(OBJ_30_OBJ_30);
          OBJ_30_OBJ_30.setBounds(140, 49, 53, 20);

          //---- OBJ_56_OBJ_56 ----
          OBJ_56_OBJ_56.setText("Dotation");
          OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
          panel1.add(OBJ_56_OBJ_56);
          OBJ_56_OBJ_56.setBounds(203, 294, 53, 20);

          //---- IMTXL ----
          IMTXL.setComponentPopupMenu(BTD);
          IMTXL.setName("IMTXL");
          panel1.add(IMTXL);
          IMTXL.setBounds(148, 135, 60, IMTXL.getPreferredSize().height);

          //---- IMTXD ----
          IMTXD.setComponentPopupMenu(BTD);
          IMTXD.setName("IMTXD");
          panel1.add(IMTXD);
          IMTXD.setBounds(358, 135, 60, IMTXD.getPreferredSize().height);

          //---- IMCOE ----
          IMCOE.setComponentPopupMenu(BTD);
          IMCOE.setName("IMCOE");
          panel1.add(IMCOE);
          IMCOE.setBounds(148, 165, 60, IMCOE.getPreferredSize().height);

          //---- OBJ_37_OBJ_37 ----
          OBJ_37_OBJ_37.setText("Dur\u00e9e utilisation");
          OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
          panel1.add(OBJ_37_OBJ_37);
          OBJ_37_OBJ_37.setBounds(148, 104, 101, 20);

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("Mode");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");
          panel1.add(OBJ_36_OBJ_36);
          OBJ_36_OBJ_36.setBounds(38, 104, 38, 20);

          //---- IMNCA ----
          IMNCA.setComponentPopupMenu(BTD);
          IMNCA.setName("IMNCA");
          panel1.add(IMNCA);
          IMNCA.setBounds(148, 290, 36, IMNCA.getPreferredSize().height);

          //---- IMNCD ----
          IMNCD.setComponentPopupMenu(BTD);
          IMNCD.setName("IMNCD");
          panel1.add(IMNCD);
          IMNCD.setBounds(268, 290, 36, IMNCD.getPreferredSize().height);

          //---- IMNCI ----
          IMNCI.setComponentPopupMenu(BTD);
          IMNCI.setName("IMNCI");
          panel1.add(IMNCI);
          IMNCI.setBounds(418, 290, 36, IMNCI.getPreferredSize().height);

          //---- UIMNCA ----
          UIMNCA.setComponentPopupMenu(BTD);
          UIMNCA.setName("UIMNCA");
          panel1.add(UIMNCA);
          UIMNCA.setBounds(148, 320, 36, UIMNCA.getPreferredSize().height);

          //---- UIMNCD ----
          UIMNCD.setComponentPopupMenu(BTD);
          UIMNCD.setName("UIMNCD");
          panel1.add(UIMNCD);
          UIMNCD.setBounds(268, 320, 36, UIMNCD.getPreferredSize().height);

          //---- UIMNCI ----
          UIMNCI.setComponentPopupMenu(BTD);
          UIMNCI.setName("UIMNCI");
          panel1.add(UIMNCI);
          UIMNCI.setBounds(418, 320, 36, UIMNCI.getPreferredSize().height);

          //---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("ou");
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
          panel1.add(OBJ_41_OBJ_41);
          OBJ_41_OBJ_41.setBounds(228, 139, 20, 20);

          //---- IMJDA ----
          IMJDA.setComponentPopupMenu(BTD);
          IMJDA.setName("IMJDA");
          panel1.add(IMJDA);
          IMJDA.setBounds(105, 45, 30, IMJDA.getPreferredSize().height);

          //---- IMDUAR ----
          IMDUAR.setComponentPopupMenu(BTD);
          IMDUAR.setName("IMDUAR");
          panel1.add(IMDUAR);
          IMDUAR.setBounds(268, 100, 26, IMDUAR.getPreferredSize().height);

          //---- IMMDA ----
          IMMDA.setComponentPopupMenu(BTD);
          IMMDA.setName("IMMDA");
          panel1.add(IMMDA);
          IMMDA.setBounds(108, 100, 24, IMMDA.getPreferredSize().height);

          //---- IMRAN ----
          IMRAN.setComponentPopupMenu(BTD);
          IMRAN.setName("IMRAN");
          panel1.add(IMRAN);
          IMRAN.setBounds(358, 220, 24, IMRAN.getPreferredSize().height);

          //---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("Calcul");
          xTitledSeparator2.setName("xTitledSeparator2");
          panel1.add(xTitledSeparator2);
          xTitledSeparator2.setBounds(25, 80, 460, xTitledSeparator2.getPreferredSize().height);

          //---- xTitledSeparator3 ----
          xTitledSeparator3.setTitle("Reprise");
          xTitledSeparator3.setName("xTitledSeparator3");
          panel1.add(xTitledSeparator3);
          xTitledSeparator3.setBounds(25, 200, 460, xTitledSeparator3.getPreferredSize().height);

          //---- xTitledSeparator4 ----
          xTitledSeparator4.setTitle("Comptabilisation");
          xTitledSeparator4.setName("xTitledSeparator4");
          panel1.add(xTitledSeparator4);
          xTitledSeparator4.setBounds(25, 270, 460, xTitledSeparator4.getPreferredSize().height);

          //---- IMDDAX ----
          IMDDAX.setComponentPopupMenu(BTD);
          IMDDAX.setTypeSaisie(6);
          IMDDAX.setName("IMDDAX");
          panel1.add(IMDDAX);
          IMDDAX.setBounds(190, 45, 90, IMDDAX.getPreferredSize().height);

          //---- IMDFAX ----
          IMDFAX.setComponentPopupMenu(BTD);
          IMDFAX.setTypeSaisie(6);
          IMDFAX.setName("IMDFAX");
          panel1.add(IMDFAX);
          IMDFAX.setBounds(400, 45, 90, IMDFAX.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 515, 380);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_32_OBJ_32;
  private JLabel OBJ_60_OBJ_60;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_54_OBJ_54;
  private JLabel OBJ_42_OBJ_42;
  private JLabel OBJ_46_OBJ_46;
  private JLabel OBJ_51_OBJ_51;
  private JLabel OBJ_58_OBJ_58;
  private JLabel OBJ_39_OBJ_39;
  private XRiTextField IMAMR;
  private XRiTextField IMANF;
  private JLabel OBJ_44_OBJ_44;
  private JLabel OBJ_28_OBJ_28;
  private JLabel OBJ_30_OBJ_30;
  private JLabel OBJ_56_OBJ_56;
  private XRiTextField IMTXL;
  private XRiTextField IMTXD;
  private XRiTextField IMCOE;
  private JLabel OBJ_37_OBJ_37;
  private JLabel OBJ_36_OBJ_36;
  private XRiTextField IMNCA;
  private XRiTextField IMNCD;
  private XRiTextField IMNCI;
  private XRiTextField UIMNCA;
  private XRiTextField UIMNCD;
  private XRiTextField UIMNCI;
  private JLabel OBJ_41_OBJ_41;
  private XRiTextField IMJDA;
  private XRiTextField IMDUAR;
  private XRiTextField IMMDA;
  private XRiTextField IMRAN;
  private JXTitledSeparator xTitledSeparator2;
  private JXTitledSeparator xTitledSeparator3;
  private JXTitledSeparator xTitledSeparator4;
  private XRiCalendrier IMDDAX;
  private XRiCalendrier IMDFAX;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
