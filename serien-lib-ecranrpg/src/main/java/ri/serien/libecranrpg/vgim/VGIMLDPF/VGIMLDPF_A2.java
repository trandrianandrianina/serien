
package ri.serien.libecranrpg.vgim.VGIMLDPF;
// Nom Fichier: i_VGIMLDPF_WGIMLDA2_WGIMLDF_90.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGIMLDPF_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIMLDPF_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    CHOISIR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+1=@")).trim());
    OBJ_17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+2=@")).trim());
    OBJ_18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+3=@")).trim());
    OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+4=@")).trim());
    OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+5=@")).trim());
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+6=@")).trim());
    OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+7=@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FONCTION/+8=@")).trim());
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITRE@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_67_OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Saisir sous la forme : @UTM@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_67_OBJ_67.setVisible(!lexique.HostFieldGetData("UTM").trim().equalsIgnoreCase(""));
    // OBJ_45_OBJ_45.setVisible( lexique.isPresent("V01F"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(lexique.chargerImage("images/logo32.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F16"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F18"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="Enter"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgimld"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_62ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    l_V01F = new JLabel();
    OBJ_67_OBJ_67 = new JLabel();
    IND = new XRiTextField();
    OBJ_62_OBJ_62 = new JLabel();
    OBJ_63_OBJ_63 = new JLabel();
    ETB = new XRiTextField();
    OBJ_54_OBJ_54 = new JLabel();
    TYP = new XRiTextField();
    P_Centre = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    LIST2 = new JTable();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_62 = new JButton();
    OBJ_68 = new JButton();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      CMD.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Recherche multi-crit\u00e8res");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);
      CMD.addSeparator();

      //---- OBJ_9 ----
      OBJ_9.setText("Cr\u00e9ation");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);

      //---- OBJ_10 ----
      OBJ_10.setText("Modification");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Interrogation");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      CMD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Annulation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Duplication");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      CMD.add(OBJ_13);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("@FONCTION/+1=@");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_17 ----
      OBJ_17.setText("@FONCTION/+2=@");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("@FONCTION/+3=@");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("@FONCTION/+4=@");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_20 ----
      OBJ_20.setText("@FONCTION/+5=@");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("@FONCTION/+6=@");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("@FONCTION/+7=@");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("@FONCTION/+8=@");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);
      BTD.addSeparator();

      //---- OBJ_24 ----
      OBJ_24.setText("Aide en ligne");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITRE@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setName("P_Infos");
        P_Infos.setLayout(null);

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });
        P_Infos.add(bt_Fonctions);
        bt_Fonctions.setBounds(883, 5, 115, bt_Fonctions.getPreferredSize().height);

        //---- l_V01F ----
        l_V01F.setText("@V01F@");
        l_V01F.setFont(new Font("sansserif", Font.BOLD, 12));
        l_V01F.setName("l_V01F");
        P_Infos.add(l_V01F);
        l_V01F.setBounds(21, 9, 80, 20);

        //---- OBJ_67_OBJ_67 ----
        OBJ_67_OBJ_67.setText("Saisir sous la forme : @UTM@");
        OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");
        P_Infos.add(OBJ_67_OBJ_67);
        OBJ_67_OBJ_67.setBounds(370, 10, 200, 16);

        //---- IND ----
        IND.setComponentPopupMenu(BTD);
        IND.setName("IND");
        P_Infos.add(IND);
        IND.setBounds(310, 5, 52, IND.getPreferredSize().height);

        //---- OBJ_62_OBJ_62 ----
        OBJ_62_OBJ_62.setText("Code");
        OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");
        P_Infos.add(OBJ_62_OBJ_62);
        OBJ_62_OBJ_62.setBounds(195, 10, 36, 18);

        //---- OBJ_63_OBJ_63 ----
        OBJ_63_OBJ_63.setText("Ordre");
        OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");
        P_Infos.add(OBJ_63_OBJ_63);
        OBJ_63_OBJ_63.setBounds(275, 10, 36, 18);

        //---- ETB ----
        ETB.setComponentPopupMenu(BTD);
        ETB.setName("ETB");
        P_Infos.add(ETB);
        ETB.setBounds(150, 5, 40, ETB.getPreferredSize().height);

        //---- OBJ_54_OBJ_54 ----
        OBJ_54_OBJ_54.setText("Etb");
        OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
        P_Infos.add(OBJ_54_OBJ_54);
        OBJ_54_OBJ_54.setBounds(120, 10, 27, 18);

        //---- TYP ----
        TYP.setComponentPopupMenu(BTD);
        TYP.setName("TYP");
        P_Infos.add(TYP);
        TYP.setBounds(230, 5, 30, TYP.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < P_Infos.getComponentCount(); i++) {
            Rectangle bounds = P_Infos.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = P_Infos.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          P_Infos.setMinimumSize(preferredSize);
          P_Infos.setPreferredSize(preferredSize);
        }
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //======== SCROLLPANE_LIST2 ========
      {
        SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
        SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

        //---- LIST2 ----
        LIST2.setModel(new DefaultTableModel(
          new Object[][] {
            {"@LD01@"},
            {"@LD02@"},
            {"@LD03@"},
            {"@LD04@"},
            {"@LD05@"},
            {"@LD06@"},
            {"@LD07@"},
            {"@LD08@"},
            {"@LD09@"},
            {"@LD10@"},
            {"@LD11@"},
            {"@LD12@"},
            {"@LD13@"},
            {"@LD14@"},
            {"@LD15@"},
          },
          new String[] {
            "@HLD01@"
          }
        ) {
          boolean[] columnEditable = new boolean[] {
            false
          };
          @Override
          public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnEditable[columnIndex];
          }
        });
        {
          TableColumnModel cm = LIST2.getColumnModel();
          cm.getColumn(0).setPreferredWidth(541);
        }
        LIST2.setFont(new Font("Courier New", Font.PLAIN, 12));
        LIST2.setName("LIST2");
        SCROLLPANE_LIST2.setViewportView(LIST2);
      }
      P_Centre.add(SCROLLPANE_LIST2);
      SCROLLPANE_LIST2.setBounds(30, 45, 558, 265);

      //======== xTitledPanel1 ========
      {
        xTitledPanel1.setTitle("R\u00e9sultat de la recherche");
        xTitledPanel1.setName("xTitledPanel1");
        Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
        xTitledPanel1ContentContainer.setLayout(null);

        //---- OBJ_62 ----
        OBJ_62.setText("");
        OBJ_62.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_62.setName("OBJ_62");
        OBJ_62.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_62ActionPerformed(e);
          }
        });
        xTitledPanel1ContentContainer.add(OBJ_62);
        OBJ_62.setBounds(580, 10, 25, 125);

        //---- OBJ_68 ----
        OBJ_68.setText("");
        OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_68.setName("OBJ_68");
        OBJ_68.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_68ActionPerformed(e);
          }
        });
        xTitledPanel1ContentContainer.add(OBJ_68);
        OBJ_68.setBounds(580, 150, 25, 125);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
            Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = xTitledPanel1ContentContainer.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
          xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
        }
      }
      P_Centre.add(xTitledPanel1);
      xTitledPanel1.setBounds(15, 10, 630, 315);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel l_V01F;
  private JLabel OBJ_67_OBJ_67;
  private XRiTextField IND;
  private JLabel OBJ_62_OBJ_62;
  private JLabel OBJ_63_OBJ_63;
  private XRiTextField ETB;
  private JLabel OBJ_54_OBJ_54;
  private XRiTextField TYP;
  private JPanel P_Centre;
  private JScrollPane SCROLLPANE_LIST2;
  private JTable LIST2;
  private JXTitledPanel xTitledPanel1;
  private JButton OBJ_62;
  private JButton OBJ_68;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
