
package ri.serien.libecranrpg.vgim.VGIM08FM;
// Nom Fichier: pop_VGIM08FM_FMTB1_117.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGIM08FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LIST_Top=null;
  private String[] _WVAE01_Title = { "", "Valeur H.T.", "Mode", "Durée", "T.Lin", "T.Deg", "D", "Durée", "Val.reprise", };
  private String[][] _WVAE01_Data = { { "null", "WVAE01", "WMDA01", "WDUA01", "WTXL01", "WTXD01", "WDEG01", "WDUE01", "WEVR01", }, };
  private int[] _WVAE01_Width = { 24, 87, 31, 38, 45, 45, 17, 45, 87, };
  private String[] _WXXX02_Title = { "", "Valeur H.T.", "Mode", "Durée", "T.Lin", "T.Deg", "D", "Durée", "Val.reprise", };
  private String[][] _WXXX02_Data = { { "WXXX02", "WVAE02", "WMDA02", "WDUA02", "WTXL02", "WTXD02", "WDEG02", "WDUE02", "WEVR02", },
      { "WXXX03", "WVAE03", "WMDA03", "WDUA03", "WTXL03", "WTXD03", "WDEG03", "WDUE03", "WEVR03", },
      { "WXXX04", "WVAE04", "WMDA04", "WDUA04", "WTXL04", "WTXD04", "WDEG04", "WDUE04", "WEVR04", },
      { "WXXX05", "WVAE05", "WMDA05", "WDUA05", "WTXL05", "WTXD05", "WDEG05", "WDUE05", "WEVR05", },
      { "WXXX06", "WVAE06", "WMDA06", "WDUA06", "WTXL06", "WTXD06", "WDEG06", "WDUE06", "WEVR06", },
      { "WXXX07", "WVAE07", "WMDA07", "WDUA07", "WTXL07", "WTXD07", "WDEG07", "WDUE07", "WEVR07", },
      { "WXXX08", "WVAE08", "WMDA08", "WDUA08", "WTXL08", "WTXD08", "WDEG08", "WDUE08", "WEVR08", },
      { "WXXX09", "WVAE09", "WMDA09", "WDUA09", "WTXL09", "WTXD09", "WDEG09", "WDUE09", "WEVR09", },
      { "WXXX10", "WVAE10", "WMDA10", "WDUA10", "WTXL10", "WTXD10", "WDEG10", "WDUE10", "WEVR10", }, };
  private int[] _WXXX02_Width = { 24, 87, 31, 38, 45, 45, 17, 45, 87, };
  // private String[][] _LIST_Title_Data_Brut=null;
  // private String[] _LIST2_Top=null;
  // private String[][] _LIST2_Title_Data_Brut=null;
  
  public VGIM08FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    // _LIST2_Title_Data_Brut = initTable(LIST2, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    
    // Ajout
    initDiverses();
    WVAE01.setAspectTable(null, _WVAE01_Title, _WVAE01_Data, _WVAE01_Width, false, null, null, null, null);
    WXXX02.setAspectTable(null, _WXXX02_Title, _WXXX02_Data, _WXXX02_Width, false, null, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(OBJ_36);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_38_OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1@")).trim());
    OBJ_15_OBJ_15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDDA01@")).trim());
    xTitledSeparator1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@WORD01@ @WLB101@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, _LIST_Title_Data_Brut, _LIST_Top);
    // majTable(LIST2, _LIST2_Title_Data_Brut, _LIST2_Top);
    
    
    
    OBJ_15_OBJ_15.setVisible(lexique.isPresent("WDDA01"));
    OBJ_38_OBJ_38.setVisible(lexique.isPresent("L1"));
    
    // TODO Icones
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="ENTER"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    SCROLLPANE_LIST2 = new JScrollPane();
    WXXX02 = new XRiTable();
    SCROLLPANE_LIST = new JScrollPane();
    WVAE01 = new XRiTable();
    OBJ_38_OBJ_38 = new JLabel();
    OBJ_14_OBJ_14 = new JLabel();
    OBJ_36 = new JButton();
    OBJ_37 = new JButton();
    OBJ_15_OBJ_15 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();

    //======== this ========
    setName("this");
    setLayout(null);

    //======== SCROLLPANE_LIST2 ========
    {
      SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

      //---- WXXX02 ----
      WXXX02.setName("WXXX02");
      SCROLLPANE_LIST2.setViewportView(WXXX02);
    }
    add(SCROLLPANE_LIST2);
    SCROLLPANE_LIST2.setBounds(25, 125, 545, 175);

    //======== SCROLLPANE_LIST ========
    {
      SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

      //---- WVAE01 ----
      WVAE01.setName("WVAE01");
      SCROLLPANE_LIST.setViewportView(WVAE01);
    }
    add(SCROLLPANE_LIST);
    SCROLLPANE_LIST.setBounds(25, 80, 545, 45);

    //---- OBJ_38_OBJ_38 ----
    OBJ_38_OBJ_38.setText("@L1@");
    OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");
    add(OBJ_38_OBJ_38);
    OBJ_38_OBJ_38.setBounds(25, 60, 446, 16);

    //---- OBJ_14_OBJ_14 ----
    OBJ_14_OBJ_14.setText("Date  amortissement");
    OBJ_14_OBJ_14.setName("OBJ_14_OBJ_14");
    add(OBJ_14_OBJ_14);
    OBJ_14_OBJ_14.setBounds(25, 35, 126, 20);

    //---- OBJ_36 ----
    OBJ_36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_36.setName("OBJ_36");
    OBJ_36.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_36ActionPerformed(e);
      }
    });
    add(OBJ_36);
    OBJ_36.setBounds(450, 310, 56, 40);

    //---- OBJ_37 ----
    OBJ_37.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_37.setName("OBJ_37");
    OBJ_37.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_37ActionPerformed(e);
      }
    });
    add(OBJ_37);
    OBJ_37.setBounds(515, 310, 56, 40);

    //---- OBJ_15_OBJ_15 ----
    OBJ_15_OBJ_15.setText("@WDDA01@");
    OBJ_15_OBJ_15.setName("OBJ_15_OBJ_15");
    add(OBJ_15_OBJ_15);
    OBJ_15_OBJ_15.setBounds(160, 35, 64, 20);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("@WORD01@ @WLB101@");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(10, 15, 570, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(610, 360));

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Exploitation");
      OBJ_6.setName("OBJ_6");
      OBJ_4.add(OBJ_6);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WXXX02;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WVAE01;
  private JLabel OBJ_38_OBJ_38;
  private JLabel OBJ_14_OBJ_14;
  private JButton OBJ_36;
  private JButton OBJ_37;
  private JLabel OBJ_15_OBJ_15;
  private JXTitledSeparator xTitledSeparator1;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
