
package ri.serien.libecranrpg.vgim.VGIMMIFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGIMMIFM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIMMIFM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDX3.setValeurs("3", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX1.setValeurs("1", "RB");
    TIDX10.setValeurs("10", "RB");
    TIDX8.setValeurs("8", "RB");
    TIDX7.setValeurs("7", "RB");
    TIDX6.setValeurs("6", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX9.setValeurs("9", "RB");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    WETB.setEnabled(lexique.isPresent("WETB"));
    ARG10.setEnabled(lexique.isPresent("ARG10"));
    ARG9M.setEnabled(lexique.isPresent("ARG9M"));
    ARG9D.setEnabled(lexique.isPresent("ARG9D"));
    ARG7.setEnabled(lexique.isPresent("ARG7"));
    ARG8.setEnabled(lexique.isPresent("ARG8"));
    ARG4.setEnabled(lexique.isPresent("ARG4"));
    ARG1.setEnabled(lexique.isPresent("ARG1"));
    ARG6A.setEnabled(lexique.isPresent("ARG6A"));
    ARG5A.setEnabled(lexique.isPresent("ARG5A"));
    ARG3.setEnabled(lexique.isPresent("ARG3"));
    ARG2.setEnabled(lexique.isPresent("ARG2"));
    // TIDX3.setEnabled( lexique.isPresent("RB"));
    // TIDX3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // OBJ_30_OBJ_30.setVisible( lexique.isPresent("V01F"));
    // TIDX2.setEnabled( lexique.isPresent("RB"));
    // TIDX2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    // TIDX1.setEnabled( lexique.isPresent("RB"));
    // TIDX1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // TIDX10.setEnabled( lexique.isPresent("RB"));
    // TIDX10.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("10"));
    // TIDX8.setEnabled( lexique.isPresent("RB"));
    // TIDX8.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("8"));
    // TIDX7.setEnabled( lexique.isPresent("RB"));
    // TIDX7.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("7"));
    // TIDX6.setEnabled( lexique.isPresent("RB"));
    // TIDX6.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("6"));
    // TIDX5.setEnabled( lexique.isPresent("RB"));
    // TIDX5.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("5"));
    // TIDX4.setEnabled( lexique.isPresent("RB"));
    // TIDX4.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("4"));
    // TIDX9.setEnabled( lexique.isPresent("RB"));
    // TIDX9.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("9"));
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TIDX3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (TIDX1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (TIDX10.isSelected())
    // lexique.HostFieldPutData("RB", 0, "10");
    // if (TIDX8.isSelected())
    // lexique.HostFieldPutData("RB", 0, "8");
    // if (TIDX7.isSelected())
    // lexique.HostFieldPutData("RB", 0, "7");
    // if (TIDX6.isSelected())
    // lexique.HostFieldPutData("RB", 0, "6");
    // if (TIDX5.isSelected())
    // lexique.HostFieldPutData("RB", 0, "5");
    // if (TIDX4.isSelected())
    // lexique.HostFieldPutData("RB", 0, "4");
    // if (TIDX9.isSelected())
    // lexique.HostFieldPutData("RB", 0, "9");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgimmi"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_28_OBJ_28 = new JLabel();
    WETB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    TIDX9 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    TIDX10 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    TIDX2 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    ARG2 = new XRiTextField();
    ARG3 = new XRiTextField();
    ARG5A = new XRiTextField();
    ARG6A = new XRiTextField();
    ARG1 = new XRiTextField();
    ARG4 = new XRiTextField();
    ARG8 = new XRiTextField();
    ARG7 = new XRiTextField();
    ARG9D = new XRiTextField();
    ARG9M = new XRiTextField();
    ARG10 = new XRiTextField();
    OBJ_72_OBJ_72 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Modification fiche d'immobilisations");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_28_OBJ_28 ----
          OBJ_28_OBJ_28.setText("Etablissement");
          OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");
          p_tete_gauche.add(OBJ_28_OBJ_28);
          OBJ_28_OBJ_28.setBounds(5, 4, 93, 20);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(95, 0, 40, WETB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Recherche multi-crit\u00e8res");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- TIDX9 ----
            TIDX9.setText("Valeur d'acquisition comprise entre");
            TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX9.setName("TIDX9");
            xTitledPanel1ContentContainer.add(TIDX9);
            TIDX9.setBounds(20, 215, 234, 20);

            //---- TIDX4 ----
            TIDX4.setText("Groupe, Famille, Sous-Famille");
            TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX4.setName("TIDX4");
            xTitledPanel1ContentContainer.add(TIDX4);
            TIDX4.setBounds(20, 75, 205, 20);

            //---- TIDX5 ----
            TIDX5.setText("Date acquisition");
            TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX5.setName("TIDX5");
            xTitledPanel1ContentContainer.add(TIDX5);
            TIDX5.setBounds(20, 103, 205, 20);

            //---- TIDX6 ----
            TIDX6.setText("Date sortie");
            TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX6.setName("TIDX6");
            xTitledPanel1ContentContainer.add(TIDX6);
            TIDX6.setBounds(20, 131, 205, 20);

            //---- TIDX7 ----
            TIDX7.setText("Section");
            TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX7.setName("TIDX7");
            xTitledPanel1ContentContainer.add(TIDX7);
            TIDX7.setBounds(20, 159, 205, 20);

            //---- TIDX8 ----
            TIDX8.setText("Service");
            TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX8.setName("TIDX8");
            xTitledPanel1ContentContainer.add(TIDX8);
            TIDX8.setBounds(20, 187, 205, 20);

            //---- TIDX10 ----
            TIDX10.setText("Par fournisseur");
            TIDX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX10.setName("TIDX10");
            xTitledPanel1ContentContainer.add(TIDX10);
            TIDX10.setBounds(20, 243, 205, 20);

            //---- TIDX1 ----
            TIDX1.setText("Code Immobilisation");
            TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX1.setName("TIDX1");
            xTitledPanel1ContentContainer.add(TIDX1);
            TIDX1.setBounds(20, 271, 205, 20);

            //---- TIDX2 ----
            TIDX2.setText("Recherche Alphab\u00e9tique");
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setName("TIDX2");
            xTitledPanel1ContentContainer.add(TIDX2);
            TIDX2.setBounds(20, 19, 174, 20);

            //---- TIDX3 ----
            TIDX3.setText("D\u00e9signation N\u00b0 2");
            TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX3.setName("TIDX3");
            xTitledPanel1ContentContainer.add(TIDX3);
            TIDX3.setBounds(20, 47, 154, 20);

            //---- ARG2 ----
            ARG2.setComponentPopupMenu(BTD);
            ARG2.setName("ARG2");
            xTitledPanel1ContentContainer.add(ARG2);
            ARG2.setBounds(265, 15, 160, ARG2.getPreferredSize().height);

            //---- ARG3 ----
            ARG3.setComponentPopupMenu(BTD);
            ARG3.setName("ARG3");
            xTitledPanel1ContentContainer.add(ARG3);
            ARG3.setBounds(265, 43, 160, ARG3.getPreferredSize().height);

            //---- ARG5A ----
            ARG5A.setComponentPopupMenu(BTD);
            ARG5A.setName("ARG5A");
            xTitledPanel1ContentContainer.add(ARG5A);
            ARG5A.setBounds(265, 99, 76, ARG5A.getPreferredSize().height);

            //---- ARG6A ----
            ARG6A.setComponentPopupMenu(BTD);
            ARG6A.setName("ARG6A");
            xTitledPanel1ContentContainer.add(ARG6A);
            ARG6A.setBounds(265, 127, 76, ARG6A.getPreferredSize().height);

            //---- ARG1 ----
            ARG1.setComponentPopupMenu(BTD);
            ARG1.setName("ARG1");
            xTitledPanel1ContentContainer.add(ARG1);
            ARG1.setBounds(265, 267, 68, ARG1.getPreferredSize().height);

            //---- ARG4 ----
            ARG4.setComponentPopupMenu(BTD);
            ARG4.setName("ARG4");
            xTitledPanel1ContentContainer.add(ARG4);
            ARG4.setBounds(265, 71, 60, ARG4.getPreferredSize().height);

            //---- ARG8 ----
            ARG8.setComponentPopupMenu(BTD);
            ARG8.setName("ARG8");
            xTitledPanel1ContentContainer.add(ARG8);
            ARG8.setBounds(265, 183, 60, ARG8.getPreferredSize().height);

            //---- ARG7 ----
            ARG7.setComponentPopupMenu(BTD);
            ARG7.setName("ARG7");
            xTitledPanel1ContentContainer.add(ARG7);
            ARG7.setBounds(265, 155, 44, ARG7.getPreferredSize().height);

            //---- ARG9D ----
            ARG9D.setComponentPopupMenu(BTD);
            ARG9D.setName("ARG9D");
            xTitledPanel1ContentContainer.add(ARG9D);
            ARG9D.setBounds(265, 211, 92, ARG9D.getPreferredSize().height);

            //---- ARG9M ----
            ARG9M.setComponentPopupMenu(BTD);
            ARG9M.setName("ARG9M");
            xTitledPanel1ContentContainer.add(ARG9M);
            ARG9M.setBounds(385, 211, 92, ARG9M.getPreferredSize().height);

            //---- ARG10 ----
            ARG10.setComponentPopupMenu(BTD);
            ARG10.setName("ARG10");
            xTitledPanel1ContentContainer.add(ARG10);
            ARG10.setBounds(265, 239, 68, ARG10.getPreferredSize().height);

            //---- OBJ_72_OBJ_72 ----
            OBJ_72_OBJ_72.setText("et");
            OBJ_72_OBJ_72.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");
            xTitledPanel1ContentContainer.add(OBJ_72_OBJ_72);
            OBJ_72_OBJ_72.setBounds(360, 215, 24, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 515, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(30, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX9);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDX8);
    RB_GRP.add(TIDX10);
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX3);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_28_OBJ_28;
  private XRiTextField WETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX5;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX7;
  private XRiRadioButton TIDX8;
  private XRiRadioButton TIDX10;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG2;
  private XRiTextField ARG3;
  private XRiTextField ARG5A;
  private XRiTextField ARG6A;
  private XRiTextField ARG1;
  private XRiTextField ARG4;
  private XRiTextField ARG8;
  private XRiTextField ARG7;
  private XRiTextField ARG9D;
  private XRiTextField ARG9M;
  private XRiTextField ARG10;
  private JLabel OBJ_72_OBJ_72;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
