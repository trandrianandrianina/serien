
package ri.serien.libecranrpg.vgim.VGIM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SpinnerListModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGIM03FM_C1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIM03FM_C1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_35_OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBVAR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_30_OBJ_30.setVisible(lexique.isPresent("WEUR"));
    WEUR.setVisible(lexique.isPresent("WEUR"));
    IMMDE.setEnabled(lexique.isPresent("IMMDE"));
    IMCBF.setEnabled(lexique.isPresent("IMCBF"));
    IMNUE.setEnabled(lexique.isPresent("IMNUE"));
    IMDTEX.setEnabled(lexique.isPresent("IMDTEX"));
    IMVAR.setVisible(lexique.isPresent("IMVAR"));
    IMVDP.setEnabled(lexique.isPresent("IMVDP"));
    IMTVO.setEnabled(lexique.isPresent("IMTVO"));
    IMVAO.setEnabled(lexique.isPresent("IMVAO"));
    IMBAS.setEnabled(lexique.isPresent("IMBAS"));
    IMDFI.setEnabled(lexique.isPresent("IMDFI"));
    IMTVE.setEnabled(lexique.isPresent("IMTVE"));
    IMVAE.setEnabled(lexique.isPresent("IMVAE"));
    OBJ_35_OBJ_35.setVisible(lexique.isPresent("LIBVAR"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("IMMOBILISATION"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    xTitledSeparator2 = new JXTitledSeparator();
    OBJ_48_OBJ_48 = new JLabel();
    IMVAO = new XRiTextField();
    OBJ_46_OBJ_46 = new JLabel();
    IMTVO = new XRiTextField();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_35_OBJ_35 = new JLabel();
    OBJ_43_OBJ_43 = new JLabel();
    OBJ_38_OBJ_38 = new JLabel();
    IMVAE = new XRiTextField();
    IMTVE = new XRiTextField();
    IMDFI = new XRiTextField();
    IMBAS = new XRiTextField();
    IMVDP = new XRiTextField();
    IMVAR = new XRiTextField();
    OBJ_32_OBJ_32 = new JLabel();
    OBJ_40_OBJ_40 = new JLabel();
    IMDTEX = new XRiCalendrier();
    OBJ_42_OBJ_42 = new JLabel();
    OBJ_26_OBJ_26 = new JLabel();
    OBJ_33_OBJ_33 = new JLabel();
    OBJ_28_OBJ_28 = new JLabel();
    IMNUE = new XRiTextField();
    IMCBF = new XRiSpinner();
    IMMDE = new XRiTextField();
    WEUR = new XRiTextField();
    OBJ_30_OBJ_30 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(725, 330));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Entr\u00e9e d'actif"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("Origine");
          xTitledSeparator2.setName("xTitledSeparator2");
          panel1.add(xTitledSeparator2);
          xTitledSeparator2.setBounds(10, 245, 510, xTitledSeparator2.getPreferredSize().height);

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("Valeur HT d'origine");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
          panel1.add(OBJ_48_OBJ_48);
          OBJ_48_OBJ_48.setBounds(20, 269, 121, 20);

          //---- IMVAO ----
          IMVAO.setComponentPopupMenu(BTD);
          IMVAO.setName("IMVAO");
          panel1.add(IMVAO);
          IMVAO.setBounds(140, 265, 108, IMVAO.getPreferredSize().height);

          //---- OBJ_46_OBJ_46 ----
          OBJ_46_OBJ_46.setText("T.V.A");
          OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");
          panel1.add(OBJ_46_OBJ_46);
          OBJ_46_OBJ_46.setBounds(345, 269, 36, 20);

          //---- IMTVO ----
          IMTVO.setComponentPopupMenu(BTD);
          IMTVO.setName("IMTVO");
          panel1.add(IMTVO);
          IMTVO.setBounds(405, 265, 108, IMTVO.getPreferredSize().height);

          //---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("Valeur d\u00e9pr\u00e9ci\u00e9e");
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
          panel1.add(OBJ_49_OBJ_49);
          OBJ_49_OBJ_49.setBounds(20, 209, 107, 20);

          //---- OBJ_35_OBJ_35 ----
          OBJ_35_OBJ_35.setText("@LIBVAR@");
          OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");
          panel1.add(OBJ_35_OBJ_35);
          OBJ_35_OBJ_35.setBounds(20, 104, 105, 20);

          //---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("Base taxable");
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
          panel1.add(OBJ_43_OBJ_43);
          OBJ_43_OBJ_43.setBounds(20, 174, 101, 20);

          //---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("D\u00e9gr\u00e8vement fiscal");
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");
          panel1.add(OBJ_38_OBJ_38);
          OBJ_38_OBJ_38.setBounds(20, 139, 123, 20);

          //---- IMVAE ----
          IMVAE.setComponentPopupMenu(BTD);
          IMVAE.setName("IMVAE");
          panel1.add(IMVAE);
          IMVAE.setBounds(140, 65, 108, IMVAE.getPreferredSize().height);

          //---- IMTVE ----
          IMTVE.setComponentPopupMenu(BTD);
          IMTVE.setName("IMTVE");
          panel1.add(IMTVE);
          IMTVE.setBounds(395, 65, 108, IMTVE.getPreferredSize().height);

          //---- IMDFI ----
          IMDFI.setComponentPopupMenu(BTD);
          IMDFI.setName("IMDFI");
          panel1.add(IMDFI);
          IMDFI.setBounds(140, 135, 108, IMDFI.getPreferredSize().height);

          //---- IMBAS ----
          IMBAS.setComponentPopupMenu(BTD);
          IMBAS.setName("IMBAS");
          panel1.add(IMBAS);
          IMBAS.setBounds(140, 170, 108, IMBAS.getPreferredSize().height);

          //---- IMVDP ----
          IMVDP.setComponentPopupMenu(BTD);
          IMVDP.setName("IMVDP");
          panel1.add(IMVDP);
          IMVDP.setBounds(140, 205, 108, IMVDP.getPreferredSize().height);

          //---- IMVAR ----
          IMVAR.setName("IMVAR");
          panel1.add(IMVAR);
          IMVAR.setBounds(140, 100, 108, IMVAR.getPreferredSize().height);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Valeur H.T");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          panel1.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(20, 69, 67, 20);

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("Nombre");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
          panel1.add(OBJ_40_OBJ_40);
          OBJ_40_OBJ_40.setBounds(395, 139, 60, 20);

          //---- IMDTEX ----
          IMDTEX.setComponentPopupMenu(BTD);
          IMDTEX.setName("IMDTEX");
          panel1.add(IMDTEX);
          IMDTEX.setBounds(140, 30, 105, IMDTEX.getPreferredSize().height);

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("Base F");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
          panel1.add(OBJ_42_OBJ_42);
          OBJ_42_OBJ_42.setBounds(275, 139, 50, 20);

          //---- OBJ_26_OBJ_26 ----
          OBJ_26_OBJ_26.setText("Mode");
          OBJ_26_OBJ_26.setName("OBJ_26_OBJ_26");
          panel1.add(OBJ_26_OBJ_26);
          OBJ_26_OBJ_26.setBounds(275, 34, 38, 20);

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("T.V.A");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
          panel1.add(OBJ_33_OBJ_33);
          OBJ_33_OBJ_33.setBounds(355, 70, 36, 20);

          //---- OBJ_28_OBJ_28 ----
          OBJ_28_OBJ_28.setText("Date");
          OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");
          panel1.add(OBJ_28_OBJ_28);
          OBJ_28_OBJ_28.setBounds(20, 34, 80, 20);

          //---- IMNUE ----
          IMNUE.setComponentPopupMenu(BTD);
          IMNUE.setName("IMNUE");
          panel1.add(IMNUE);
          IMNUE.setBounds(467, 135, 36, IMNUE.getPreferredSize().height);

          //---- IMCBF ----
          IMCBF.setComponentPopupMenu(BTD);
          IMCBF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          IMCBF.setModel(new SpinnerListModel(new String[] {"", "P", "F"}));
          IMCBF.setName("IMCBF");
          panel1.add(IMCBF);
          IMCBF.setBounds(320, 135, 45, IMCBF.getPreferredSize().height);

          //---- IMMDE ----
          IMMDE.setComponentPopupMenu(BTD);
          IMMDE.setName("IMMDE");
          panel1.add(IMMDE);
          IMMDE.setBounds(320, 30, 24, IMMDE.getPreferredSize().height);

          //---- WEUR ----
          WEUR.setName("WEUR");
          panel1.add(WEUR);
          WEUR.setBounds(320, 65, 20, WEUR.getPreferredSize().height);

          //---- OBJ_30_OBJ_30 ----
          OBJ_30_OBJ_30.setText("E");
          OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");
          panel1.add(OBJ_30_OBJ_30);
          OBJ_30_OBJ_30.setBounds(275, 69, 15, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 535, 310);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledSeparator xTitledSeparator2;
  private JLabel OBJ_48_OBJ_48;
  private XRiTextField IMVAO;
  private JLabel OBJ_46_OBJ_46;
  private XRiTextField IMTVO;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_35_OBJ_35;
  private JLabel OBJ_43_OBJ_43;
  private JLabel OBJ_38_OBJ_38;
  private XRiTextField IMVAE;
  private XRiTextField IMTVE;
  private XRiTextField IMDFI;
  private XRiTextField IMBAS;
  private XRiTextField IMVDP;
  private XRiTextField IMVAR;
  private JLabel OBJ_32_OBJ_32;
  private JLabel OBJ_40_OBJ_40;
  private XRiCalendrier IMDTEX;
  private JLabel OBJ_42_OBJ_42;
  private JLabel OBJ_26_OBJ_26;
  private JLabel OBJ_33_OBJ_33;
  private JLabel OBJ_28_OBJ_28;
  private XRiTextField IMNUE;
  private XRiSpinner IMCBF;
  private XRiTextField IMMDE;
  private XRiTextField WEUR;
  private JLabel OBJ_30_OBJ_30;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
