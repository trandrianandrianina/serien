
package ri.serien.libecranrpg.vgim.VGIM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGIM03FM_C5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIM03FM_C5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC01@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC02@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC03@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC04@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC05@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC06@")).trim());
    label13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC07@")).trim());
    label15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC08@")).trim());
    label17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC09@")).trim());
    label19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC10@")).trim());
    label21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC11@")).trim());
    label23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC12@")).trim());
    label25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC13@")).trim());
    label27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC14@")).trim());
    label29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC15@")).trim());
    label31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC16@")).trim());
    label33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC17@")).trim());
    label35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC18@")).trim());
    label37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC19@")).trim());
    label39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC20@")).trim());
    label41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC21@")).trim());
    label43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC22@")).trim());
    label45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC23@")).trim());
    label47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC24@")).trim());
    label49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC25@")).trim());
    label51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC26@")).trim());
    label53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC27@")).trim());
    label55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC28@")).trim());
    label57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC29@")).trim());
    label59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC30@")).trim());
    label61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC31@")).trim());
    label63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC32@")).trim());
    label65.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC33@")).trim());
    label67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC34@")).trim());
    label69.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC35@")).trim());
    label71.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC36@")).trim());
    label73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC37@")).trim());
    label75.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC38@")).trim());
    label77.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC39@")).trim());
    label79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC40@")).trim());
    label81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC41@")).trim());
    label83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC42@")).trim());
    label85.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC43@")).trim());
    label87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC44@")).trim());
    label89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC45@")).trim());
    label91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC46@")).trim());
    label93.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC47@")).trim());
    label95.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC48@")).trim());
    label97.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC49@")).trim());
    label99.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC50@")).trim());
    label101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ITC51@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("IMMOBILISATION"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label2 = new JLabel();
    IMC01 = new XRiTextField();
    IMC02 = new XRiTextField();
    label3 = new JLabel();
    label4 = new JLabel();
    IMC03 = new XRiTextField();
    label5 = new JLabel();
    label6 = new JLabel();
    IMC04 = new XRiTextField();
    label7 = new JLabel();
    IMC05 = new XRiTextField();
    label9 = new JLabel();
    label10 = new JLabel();
    IMC06 = new XRiTextField();
    label11 = new JLabel();
    label12 = new JLabel();
    IMC07 = new XRiTextField();
    label13 = new JLabel();
    IMC08 = new XRiTextField();
    label15 = new JLabel();
    label16 = new JLabel();
    IMC09 = new XRiTextField();
    label17 = new JLabel();
    label18 = new JLabel();
    IMC10 = new XRiTextField();
    label19 = new JLabel();
    IMC11 = new XRiTextField();
    label21 = new JLabel();
    label22 = new JLabel();
    IMC12 = new XRiTextField();
    label23 = new JLabel();
    label24 = new JLabel();
    IMC13 = new XRiTextField();
    label25 = new JLabel();
    IMC14 = new XRiTextField();
    label27 = new JLabel();
    label28 = new JLabel();
    IMC15 = new XRiTextField();
    label29 = new JLabel();
    label30 = new JLabel();
    IMC16 = new XRiTextField();
    label31 = new JLabel();
    IMC17 = new XRiTextField();
    label33 = new JLabel();
    label34 = new JLabel();
    IMC18 = new XRiTextField();
    label35 = new JLabel();
    label36 = new JLabel();
    IMC19 = new XRiTextField();
    label37 = new JLabel();
    IMC20 = new XRiTextField();
    label39 = new JLabel();
    label40 = new JLabel();
    IMC21 = new XRiTextField();
    label41 = new JLabel();
    label42 = new JLabel();
    IMC22 = new XRiTextField();
    label43 = new JLabel();
    IMC23 = new XRiTextField();
    label45 = new JLabel();
    label46 = new JLabel();
    IMC24 = new XRiTextField();
    label47 = new JLabel();
    label48 = new JLabel();
    IMC25 = new XRiTextField();
    label49 = new JLabel();
    IMC26 = new XRiTextField();
    label51 = new JLabel();
    label52 = new JLabel();
    IMC27 = new XRiTextField();
    label53 = new JLabel();
    label54 = new JLabel();
    IMC28 = new XRiTextField();
    label55 = new JLabel();
    IMC29 = new XRiTextField();
    label57 = new JLabel();
    label58 = new JLabel();
    IMC30 = new XRiTextField();
    label59 = new JLabel();
    label60 = new JLabel();
    IMC31 = new XRiTextField();
    label61 = new JLabel();
    IMC32 = new XRiTextField();
    label63 = new JLabel();
    label64 = new JLabel();
    IMC33 = new XRiTextField();
    label65 = new JLabel();
    label66 = new JLabel();
    IMC34 = new XRiTextField();
    label67 = new JLabel();
    IMC35 = new XRiTextField();
    label69 = new JLabel();
    label70 = new JLabel();
    IMC36 = new XRiTextField();
    label71 = new JLabel();
    label72 = new JLabel();
    IMC37 = new XRiTextField();
    label73 = new JLabel();
    IMC38 = new XRiTextField();
    label75 = new JLabel();
    label76 = new JLabel();
    IMC39 = new XRiTextField();
    label77 = new JLabel();
    label78 = new JLabel();
    IMC40 = new XRiTextField();
    label79 = new JLabel();
    IMC41 = new XRiTextField();
    label81 = new JLabel();
    label82 = new JLabel();
    IMC42 = new XRiTextField();
    label83 = new JLabel();
    label84 = new JLabel();
    IMC43 = new XRiTextField();
    label85 = new JLabel();
    IMC44 = new XRiTextField();
    label87 = new JLabel();
    label88 = new JLabel();
    IMC45 = new XRiTextField();
    label89 = new JLabel();
    label90 = new JLabel();
    IMC46 = new XRiTextField();
    label91 = new JLabel();
    IMC47 = new XRiTextField();
    label93 = new JLabel();
    label94 = new JLabel();
    IMC48 = new XRiTextField();
    label95 = new JLabel();
    label96 = new JLabel();
    IMC49 = new XRiTextField();
    label97 = new JLabel();
    IMC50 = new XRiTextField();
    label99 = new JLabel();
    label100 = new JLabel();
    IMC51 = new XRiTextField();
    label101 = new JLabel();
    label102 = new JLabel();
    label1 = new JLabel();
    label8 = new JLabel();
    label14 = new JLabel();
    label20 = new JLabel();
    label26 = new JLabel();
    label32 = new JLabel();
    label38 = new JLabel();
    label44 = new JLabel();
    label50 = new JLabel();
    label56 = new JLabel();
    label62 = new JLabel();
    label68 = new JLabel();
    label74 = new JLabel();
    label80 = new JLabel();
    label86 = new JLabel();
    label92 = new JLabel();
    label98 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(820, 595));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Annuit\u00e9s d'amortissement \u00e9conomique"));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- label2 ----
          label2.setText("@ITC01@");
          label2.setName("label2");

          //---- IMC01 ----
          IMC01.setName("IMC01");

          //---- IMC02 ----
          IMC02.setName("IMC02");

          //---- label3 ----
          label3.setText("@ITC02@");
          label3.setName("label3");

          //---- label4 ----
          label4.setText("02");
          label4.setHorizontalAlignment(SwingConstants.RIGHT);
          label4.setName("label4");

          //---- IMC03 ----
          IMC03.setName("IMC03");

          //---- label5 ----
          label5.setText("@ITC03@");
          label5.setName("label5");

          //---- label6 ----
          label6.setText("03");
          label6.setHorizontalAlignment(SwingConstants.RIGHT);
          label6.setName("label6");

          //---- IMC04 ----
          IMC04.setName("IMC04");

          //---- label7 ----
          label7.setText("@ITC04@");
          label7.setName("label7");

          //---- IMC05 ----
          IMC05.setName("IMC05");

          //---- label9 ----
          label9.setText("@ITC05@");
          label9.setName("label9");

          //---- label10 ----
          label10.setText("05");
          label10.setHorizontalAlignment(SwingConstants.RIGHT);
          label10.setName("label10");

          //---- IMC06 ----
          IMC06.setName("IMC06");

          //---- label11 ----
          label11.setText("@ITC06@");
          label11.setName("label11");

          //---- label12 ----
          label12.setText("06");
          label12.setHorizontalAlignment(SwingConstants.RIGHT);
          label12.setName("label12");

          //---- IMC07 ----
          IMC07.setName("IMC07");

          //---- label13 ----
          label13.setText("@ITC07@");
          label13.setName("label13");

          //---- IMC08 ----
          IMC08.setName("IMC08");

          //---- label15 ----
          label15.setText("@ITC08@");
          label15.setName("label15");

          //---- label16 ----
          label16.setText("08");
          label16.setHorizontalAlignment(SwingConstants.RIGHT);
          label16.setName("label16");

          //---- IMC09 ----
          IMC09.setName("IMC09");

          //---- label17 ----
          label17.setText("@ITC09@");
          label17.setName("label17");

          //---- label18 ----
          label18.setText("09");
          label18.setHorizontalAlignment(SwingConstants.RIGHT);
          label18.setName("label18");

          //---- IMC10 ----
          IMC10.setName("IMC10");

          //---- label19 ----
          label19.setText("@ITC10@");
          label19.setName("label19");

          //---- IMC11 ----
          IMC11.setName("IMC11");

          //---- label21 ----
          label21.setText("@ITC11@");
          label21.setName("label21");

          //---- label22 ----
          label22.setText("11");
          label22.setHorizontalAlignment(SwingConstants.RIGHT);
          label22.setName("label22");

          //---- IMC12 ----
          IMC12.setName("IMC12");

          //---- label23 ----
          label23.setText("@ITC12@");
          label23.setName("label23");

          //---- label24 ----
          label24.setText("12");
          label24.setHorizontalAlignment(SwingConstants.RIGHT);
          label24.setName("label24");

          //---- IMC13 ----
          IMC13.setName("IMC13");

          //---- label25 ----
          label25.setText("@ITC13@");
          label25.setName("label25");

          //---- IMC14 ----
          IMC14.setName("IMC14");

          //---- label27 ----
          label27.setText("@ITC14@");
          label27.setName("label27");

          //---- label28 ----
          label28.setText("14");
          label28.setHorizontalAlignment(SwingConstants.RIGHT);
          label28.setName("label28");

          //---- IMC15 ----
          IMC15.setName("IMC15");

          //---- label29 ----
          label29.setText("@ITC15@");
          label29.setName("label29");

          //---- label30 ----
          label30.setText("15");
          label30.setHorizontalAlignment(SwingConstants.RIGHT);
          label30.setName("label30");

          //---- IMC16 ----
          IMC16.setName("IMC16");

          //---- label31 ----
          label31.setText("@ITC16@");
          label31.setName("label31");

          //---- IMC17 ----
          IMC17.setName("IMC17");

          //---- label33 ----
          label33.setText("@ITC17@");
          label33.setName("label33");

          //---- label34 ----
          label34.setText("17");
          label34.setHorizontalAlignment(SwingConstants.RIGHT);
          label34.setName("label34");

          //---- IMC18 ----
          IMC18.setName("IMC18");

          //---- label35 ----
          label35.setText("@ITC18@");
          label35.setName("label35");

          //---- label36 ----
          label36.setText("18");
          label36.setHorizontalAlignment(SwingConstants.RIGHT);
          label36.setName("label36");

          //---- IMC19 ----
          IMC19.setName("IMC19");

          //---- label37 ----
          label37.setText("@ITC19@");
          label37.setName("label37");

          //---- IMC20 ----
          IMC20.setName("IMC20");

          //---- label39 ----
          label39.setText("@ITC20@");
          label39.setName("label39");

          //---- label40 ----
          label40.setText("20");
          label40.setHorizontalAlignment(SwingConstants.RIGHT);
          label40.setName("label40");

          //---- IMC21 ----
          IMC21.setName("IMC21");

          //---- label41 ----
          label41.setText("@ITC21@");
          label41.setName("label41");

          //---- label42 ----
          label42.setText("21");
          label42.setHorizontalAlignment(SwingConstants.RIGHT);
          label42.setName("label42");

          //---- IMC22 ----
          IMC22.setName("IMC22");

          //---- label43 ----
          label43.setText("@ITC22@");
          label43.setName("label43");

          //---- IMC23 ----
          IMC23.setName("IMC23");

          //---- label45 ----
          label45.setText("@ITC23@");
          label45.setName("label45");

          //---- label46 ----
          label46.setText("23");
          label46.setHorizontalAlignment(SwingConstants.RIGHT);
          label46.setName("label46");

          //---- IMC24 ----
          IMC24.setName("IMC24");

          //---- label47 ----
          label47.setText("@ITC24@");
          label47.setName("label47");

          //---- label48 ----
          label48.setText("24");
          label48.setHorizontalAlignment(SwingConstants.RIGHT);
          label48.setName("label48");

          //---- IMC25 ----
          IMC25.setName("IMC25");

          //---- label49 ----
          label49.setText("@ITC25@");
          label49.setName("label49");

          //---- IMC26 ----
          IMC26.setName("IMC26");

          //---- label51 ----
          label51.setText("@ITC26@");
          label51.setName("label51");

          //---- label52 ----
          label52.setText("26");
          label52.setHorizontalAlignment(SwingConstants.RIGHT);
          label52.setName("label52");

          //---- IMC27 ----
          IMC27.setName("IMC27");

          //---- label53 ----
          label53.setText("@ITC27@");
          label53.setName("label53");

          //---- label54 ----
          label54.setText("27");
          label54.setHorizontalAlignment(SwingConstants.RIGHT);
          label54.setName("label54");

          //---- IMC28 ----
          IMC28.setName("IMC28");

          //---- label55 ----
          label55.setText("@ITC28@");
          label55.setName("label55");

          //---- IMC29 ----
          IMC29.setName("IMC29");

          //---- label57 ----
          label57.setText("@ITC29@");
          label57.setName("label57");

          //---- label58 ----
          label58.setText("29");
          label58.setHorizontalAlignment(SwingConstants.RIGHT);
          label58.setName("label58");

          //---- IMC30 ----
          IMC30.setName("IMC30");

          //---- label59 ----
          label59.setText("@ITC30@");
          label59.setName("label59");

          //---- label60 ----
          label60.setText("30");
          label60.setHorizontalAlignment(SwingConstants.RIGHT);
          label60.setName("label60");

          //---- IMC31 ----
          IMC31.setName("IMC31");

          //---- label61 ----
          label61.setText("@ITC31@");
          label61.setName("label61");

          //---- IMC32 ----
          IMC32.setName("IMC32");

          //---- label63 ----
          label63.setText("@ITC32@");
          label63.setName("label63");

          //---- label64 ----
          label64.setText("32");
          label64.setHorizontalAlignment(SwingConstants.RIGHT);
          label64.setName("label64");

          //---- IMC33 ----
          IMC33.setName("IMC33");

          //---- label65 ----
          label65.setText("@ITC33@");
          label65.setName("label65");

          //---- label66 ----
          label66.setText("33");
          label66.setHorizontalAlignment(SwingConstants.RIGHT);
          label66.setName("label66");

          //---- IMC34 ----
          IMC34.setName("IMC34");

          //---- label67 ----
          label67.setText("@ITC34@");
          label67.setName("label67");

          //---- IMC35 ----
          IMC35.setName("IMC35");

          //---- label69 ----
          label69.setText("@ITC35@");
          label69.setName("label69");

          //---- label70 ----
          label70.setText("35");
          label70.setHorizontalAlignment(SwingConstants.RIGHT);
          label70.setName("label70");

          //---- IMC36 ----
          IMC36.setName("IMC36");

          //---- label71 ----
          label71.setText("@ITC36@");
          label71.setName("label71");

          //---- label72 ----
          label72.setText("36");
          label72.setHorizontalAlignment(SwingConstants.RIGHT);
          label72.setName("label72");

          //---- IMC37 ----
          IMC37.setName("IMC37");

          //---- label73 ----
          label73.setText("@ITC37@");
          label73.setName("label73");

          //---- IMC38 ----
          IMC38.setName("IMC38");

          //---- label75 ----
          label75.setText("@ITC38@");
          label75.setName("label75");

          //---- label76 ----
          label76.setText("38");
          label76.setHorizontalAlignment(SwingConstants.RIGHT);
          label76.setName("label76");

          //---- IMC39 ----
          IMC39.setName("IMC39");

          //---- label77 ----
          label77.setText("@ITC39@");
          label77.setName("label77");

          //---- label78 ----
          label78.setText("39");
          label78.setHorizontalAlignment(SwingConstants.RIGHT);
          label78.setName("label78");

          //---- IMC40 ----
          IMC40.setName("IMC40");

          //---- label79 ----
          label79.setText("@ITC40@");
          label79.setName("label79");

          //---- IMC41 ----
          IMC41.setName("IMC41");

          //---- label81 ----
          label81.setText("@ITC41@");
          label81.setName("label81");

          //---- label82 ----
          label82.setText("41");
          label82.setHorizontalAlignment(SwingConstants.RIGHT);
          label82.setName("label82");

          //---- IMC42 ----
          IMC42.setName("IMC42");

          //---- label83 ----
          label83.setText("@ITC42@");
          label83.setName("label83");

          //---- label84 ----
          label84.setText("42");
          label84.setHorizontalAlignment(SwingConstants.RIGHT);
          label84.setName("label84");

          //---- IMC43 ----
          IMC43.setName("IMC43");

          //---- label85 ----
          label85.setText("@ITC43@");
          label85.setName("label85");

          //---- IMC44 ----
          IMC44.setName("IMC44");

          //---- label87 ----
          label87.setText("@ITC44@");
          label87.setName("label87");

          //---- label88 ----
          label88.setText("44");
          label88.setHorizontalAlignment(SwingConstants.RIGHT);
          label88.setName("label88");

          //---- IMC45 ----
          IMC45.setName("IMC45");

          //---- label89 ----
          label89.setText("@ITC45@");
          label89.setName("label89");

          //---- label90 ----
          label90.setText("45");
          label90.setHorizontalAlignment(SwingConstants.RIGHT);
          label90.setName("label90");

          //---- IMC46 ----
          IMC46.setName("IMC46");

          //---- label91 ----
          label91.setText("@ITC46@");
          label91.setName("label91");

          //---- IMC47 ----
          IMC47.setName("IMC47");

          //---- label93 ----
          label93.setText("@ITC47@");
          label93.setName("label93");

          //---- label94 ----
          label94.setText("47");
          label94.setHorizontalAlignment(SwingConstants.RIGHT);
          label94.setName("label94");

          //---- IMC48 ----
          IMC48.setName("IMC48");

          //---- label95 ----
          label95.setText("@ITC48@");
          label95.setName("label95");

          //---- label96 ----
          label96.setText("48");
          label96.setHorizontalAlignment(SwingConstants.RIGHT);
          label96.setName("label96");

          //---- IMC49 ----
          IMC49.setName("IMC49");

          //---- label97 ----
          label97.setText("@ITC49@");
          label97.setName("label97");

          //---- IMC50 ----
          IMC50.setName("IMC50");

          //---- label99 ----
          label99.setText("@ITC50@");
          label99.setName("label99");

          //---- label100 ----
          label100.setText("50");
          label100.setHorizontalAlignment(SwingConstants.RIGHT);
          label100.setName("label100");

          //---- IMC51 ----
          IMC51.setName("IMC51");

          //---- label101 ----
          label101.setText("@ITC51@");
          label101.setName("label101");

          //---- label102 ----
          label102.setText("51");
          label102.setHorizontalAlignment(SwingConstants.RIGHT);
          label102.setName("label102");

          //---- label1 ----
          label1.setText("01");
          label1.setHorizontalAlignment(SwingConstants.RIGHT);
          label1.setName("label1");

          //---- label8 ----
          label8.setText("04");
          label8.setHorizontalAlignment(SwingConstants.RIGHT);
          label8.setName("label8");

          //---- label14 ----
          label14.setText("07");
          label14.setHorizontalAlignment(SwingConstants.RIGHT);
          label14.setName("label14");

          //---- label20 ----
          label20.setText("10");
          label20.setHorizontalAlignment(SwingConstants.RIGHT);
          label20.setName("label20");

          //---- label26 ----
          label26.setText("13");
          label26.setHorizontalAlignment(SwingConstants.RIGHT);
          label26.setName("label26");

          //---- label32 ----
          label32.setText("16");
          label32.setHorizontalAlignment(SwingConstants.RIGHT);
          label32.setName("label32");

          //---- label38 ----
          label38.setText("19");
          label38.setHorizontalAlignment(SwingConstants.RIGHT);
          label38.setName("label38");

          //---- label44 ----
          label44.setText("22");
          label44.setHorizontalAlignment(SwingConstants.RIGHT);
          label44.setName("label44");

          //---- label50 ----
          label50.setText("25");
          label50.setHorizontalAlignment(SwingConstants.RIGHT);
          label50.setName("label50");

          //---- label56 ----
          label56.setText("28");
          label56.setHorizontalAlignment(SwingConstants.RIGHT);
          label56.setName("label56");

          //---- label62 ----
          label62.setText("31");
          label62.setHorizontalAlignment(SwingConstants.RIGHT);
          label62.setName("label62");

          //---- label68 ----
          label68.setText("34");
          label68.setHorizontalAlignment(SwingConstants.RIGHT);
          label68.setName("label68");

          //---- label74 ----
          label74.setText("37");
          label74.setHorizontalAlignment(SwingConstants.RIGHT);
          label74.setName("label74");

          //---- label80 ----
          label80.setText("40");
          label80.setHorizontalAlignment(SwingConstants.RIGHT);
          label80.setName("label80");

          //---- label86 ----
          label86.setText("43");
          label86.setHorizontalAlignment(SwingConstants.RIGHT);
          label86.setName("label86");

          //---- label92 ----
          label92.setText("46");
          label92.setHorizontalAlignment(SwingConstants.RIGHT);
          label92.setName("label92");

          //---- label98 ----
          label98.setText("49");
          label98.setHorizontalAlignment(SwingConstants.RIGHT);
          label98.setName("label98");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC01, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC02, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label6, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC03, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label8, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label7, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC04, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label9, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC05, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC06, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label14, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC07, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label16, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC08, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label18, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label17, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC09, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label19, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC10, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label22, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label21, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC11, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label24, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label23, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC12, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label26, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label25, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC13, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label28, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label27, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC14, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label30, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label29, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC15, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label32, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label31, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC16, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC17, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label35, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC18, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC19, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label40, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC20, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC21, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC22, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC23, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC24, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC25, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC26, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC27, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC28, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label57, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC29, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label60, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label59, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC30, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label62, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC31, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label63, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC32, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label66, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label65, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC33, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC34, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label70, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label69, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC35, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label72, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label71, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC36, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label74, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC37, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label75, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC38, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label77, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC39, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label80, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label79, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC40, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label82, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label81, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC41, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label84, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label83, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC42, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label86, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label85, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC43, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label88, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label87, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC44, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label90, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label89, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC45, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label92, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label91, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC46, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label94, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label93, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC47, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label96, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label95, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC48, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label98, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label97, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC49, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(55, 55, 55)
                    .addComponent(label100, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label99, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC50, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(label102, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(label101, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(IMC51, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label1))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label2))
                  .addComponent(IMC01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label4))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label3))
                  .addComponent(IMC02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label6))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label5))
                  .addComponent(IMC03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label8))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label7))
                  .addComponent(IMC04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label10))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label9))
                  .addComponent(IMC05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label12))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label11))
                  .addComponent(IMC06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label14))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label13))
                  .addComponent(IMC07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label16))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label15))
                  .addComponent(IMC08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label18))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label17))
                  .addComponent(IMC09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label20))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label19))
                  .addComponent(IMC10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label22))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label21))
                  .addComponent(IMC11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label24))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label23))
                  .addComponent(IMC12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label26))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label25))
                  .addComponent(IMC13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label28))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label27))
                  .addComponent(IMC14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label30))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label29))
                  .addComponent(IMC15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label32))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label31))
                  .addComponent(IMC16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label34))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label33))
                  .addComponent(IMC17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label36))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label35))
                  .addComponent(IMC18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label38))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label37))
                  .addComponent(IMC19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label40))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label39))
                  .addComponent(IMC20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label42))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label41))
                  .addComponent(IMC21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label44))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label43))
                  .addComponent(IMC22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label46))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label45))
                  .addComponent(IMC23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label48))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label47))
                  .addComponent(IMC24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label50))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label49))
                  .addComponent(IMC25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label52))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label51))
                  .addComponent(IMC26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label54))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label53))
                  .addComponent(IMC27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label56))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label55))
                  .addComponent(IMC28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label58))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label57))
                  .addComponent(IMC29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label60))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label59))
                  .addComponent(IMC30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label62))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label61))
                  .addComponent(IMC31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label64))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label63))
                  .addComponent(IMC32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label66))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label65))
                  .addComponent(IMC33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label68))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label67))
                  .addComponent(IMC34, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label70))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label69))
                  .addComponent(IMC35, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label72))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label71))
                  .addComponent(IMC36, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label74))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label73))
                  .addComponent(IMC37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label76))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label75))
                  .addComponent(IMC38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label78))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label77))
                  .addComponent(IMC39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label80))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label79))
                  .addComponent(IMC40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label82))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label81))
                  .addComponent(IMC41, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label84))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label83))
                  .addComponent(IMC42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label86))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label85))
                  .addComponent(IMC43, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label88))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label87))
                  .addComponent(IMC44, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label90))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label89))
                  .addComponent(IMC45, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label92))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label91))
                  .addComponent(IMC46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label94))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label93))
                  .addComponent(IMC47, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label96))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label95))
                  .addComponent(IMC48, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label98))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label97))
                  .addComponent(IMC49, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label100))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label99))
                  .addComponent(IMC50, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label102))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(label101))
                  .addComponent(IMC51, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 630, 575);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label2;
  private XRiTextField IMC01;
  private XRiTextField IMC02;
  private JLabel label3;
  private JLabel label4;
  private XRiTextField IMC03;
  private JLabel label5;
  private JLabel label6;
  private XRiTextField IMC04;
  private JLabel label7;
  private XRiTextField IMC05;
  private JLabel label9;
  private JLabel label10;
  private XRiTextField IMC06;
  private JLabel label11;
  private JLabel label12;
  private XRiTextField IMC07;
  private JLabel label13;
  private XRiTextField IMC08;
  private JLabel label15;
  private JLabel label16;
  private XRiTextField IMC09;
  private JLabel label17;
  private JLabel label18;
  private XRiTextField IMC10;
  private JLabel label19;
  private XRiTextField IMC11;
  private JLabel label21;
  private JLabel label22;
  private XRiTextField IMC12;
  private JLabel label23;
  private JLabel label24;
  private XRiTextField IMC13;
  private JLabel label25;
  private XRiTextField IMC14;
  private JLabel label27;
  private JLabel label28;
  private XRiTextField IMC15;
  private JLabel label29;
  private JLabel label30;
  private XRiTextField IMC16;
  private JLabel label31;
  private XRiTextField IMC17;
  private JLabel label33;
  private JLabel label34;
  private XRiTextField IMC18;
  private JLabel label35;
  private JLabel label36;
  private XRiTextField IMC19;
  private JLabel label37;
  private XRiTextField IMC20;
  private JLabel label39;
  private JLabel label40;
  private XRiTextField IMC21;
  private JLabel label41;
  private JLabel label42;
  private XRiTextField IMC22;
  private JLabel label43;
  private XRiTextField IMC23;
  private JLabel label45;
  private JLabel label46;
  private XRiTextField IMC24;
  private JLabel label47;
  private JLabel label48;
  private XRiTextField IMC25;
  private JLabel label49;
  private XRiTextField IMC26;
  private JLabel label51;
  private JLabel label52;
  private XRiTextField IMC27;
  private JLabel label53;
  private JLabel label54;
  private XRiTextField IMC28;
  private JLabel label55;
  private XRiTextField IMC29;
  private JLabel label57;
  private JLabel label58;
  private XRiTextField IMC30;
  private JLabel label59;
  private JLabel label60;
  private XRiTextField IMC31;
  private JLabel label61;
  private XRiTextField IMC32;
  private JLabel label63;
  private JLabel label64;
  private XRiTextField IMC33;
  private JLabel label65;
  private JLabel label66;
  private XRiTextField IMC34;
  private JLabel label67;
  private XRiTextField IMC35;
  private JLabel label69;
  private JLabel label70;
  private XRiTextField IMC36;
  private JLabel label71;
  private JLabel label72;
  private XRiTextField IMC37;
  private JLabel label73;
  private XRiTextField IMC38;
  private JLabel label75;
  private JLabel label76;
  private XRiTextField IMC39;
  private JLabel label77;
  private JLabel label78;
  private XRiTextField IMC40;
  private JLabel label79;
  private XRiTextField IMC41;
  private JLabel label81;
  private JLabel label82;
  private XRiTextField IMC42;
  private JLabel label83;
  private JLabel label84;
  private XRiTextField IMC43;
  private JLabel label85;
  private XRiTextField IMC44;
  private JLabel label87;
  private JLabel label88;
  private XRiTextField IMC45;
  private JLabel label89;
  private JLabel label90;
  private XRiTextField IMC46;
  private JLabel label91;
  private XRiTextField IMC47;
  private JLabel label93;
  private JLabel label94;
  private XRiTextField IMC48;
  private JLabel label95;
  private JLabel label96;
  private XRiTextField IMC49;
  private JLabel label97;
  private XRiTextField IMC50;
  private JLabel label99;
  private JLabel label100;
  private XRiTextField IMC51;
  private JLabel label101;
  private JLabel label102;
  private JLabel label1;
  private JLabel label8;
  private JLabel label14;
  private JLabel label20;
  private JLabel label26;
  private JLabel label32;
  private JLabel label38;
  private JLabel label44;
  private JLabel label50;
  private JLabel label56;
  private JLabel label62;
  private JLabel label68;
  private JLabel label74;
  private JLabel label80;
  private JLabel label86;
  private JLabel label92;
  private JLabel label98;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
