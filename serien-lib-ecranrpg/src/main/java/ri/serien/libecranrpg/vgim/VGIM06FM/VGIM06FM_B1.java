
package ri.serien.libecranrpg.vgim.VGIM06FM;
// Nom Fichier: pop_VGIM06FM_FMTB1_FMTF1_109.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGIM06FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGIM06FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_28);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_31_OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP01@")).trim());
    OBJ_32_OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP02@")).trim());
    OBJ_33_OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP03@")).trim());
    OBJ_34_OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP04@")).trim());
    OBJ_35_OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP05@")).trim());
    OBJ_36_OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP06@")).trim());
    OBJ_37_OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP07@")).trim());
    OBJ_38_OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP08@")).trim());
    OBJ_39_OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP09@")).trim());
    OBJ_40_OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP10@")).trim());
    OBJ_41_OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP11@")).trim());
    OBJ_42_OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP12@")).trim());
    OBJ_43_OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP13@")).trim());
    OBJ_44_OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP14@")).trim());
    OBJ_45_OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP15@")).trim());
    OBJ_46_OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP16@")).trim());
    OBJ_47_OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP17@")).trim());
    OBJ_48_OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP18@")).trim());
    xTitledSeparator1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("Immobilisation @IMORD@ @IMXXX@ -  @IMLB1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    P18.setVisible(lexique.HostFieldGetData("TST18T").equalsIgnoreCase("P"));
    P17.setVisible(lexique.HostFieldGetData("TST17T").equalsIgnoreCase("P"));
    P16.setVisible(lexique.HostFieldGetData("TST16T").equalsIgnoreCase("P"));
    P15.setVisible(lexique.HostFieldGetData("TST15T").equalsIgnoreCase("P"));
    P14.setVisible(lexique.HostFieldGetData("TST14T").equalsIgnoreCase("P"));
    P13.setVisible(lexique.HostFieldGetData("TST13T").equalsIgnoreCase("P"));
    P12.setVisible(lexique.HostFieldGetData("TST12T").equalsIgnoreCase("P"));
    P11.setVisible(lexique.HostFieldGetData("TST11T").equalsIgnoreCase("P"));
    P10.setVisible(lexique.HostFieldGetData("TST10T").equalsIgnoreCase("P"));
    P09.setVisible(lexique.HostFieldGetData("TST09T").equalsIgnoreCase("P"));
    P08.setVisible(lexique.HostFieldGetData("TST08T").equalsIgnoreCase("P"));
    P07.setVisible(lexique.HostFieldGetData("TST07T").equalsIgnoreCase("P"));
    P06.setVisible(lexique.HostFieldGetData("TST06T").equalsIgnoreCase("P"));
    P05.setVisible(lexique.HostFieldGetData("TST05T").equalsIgnoreCase("P"));
    P04.setVisible(lexique.HostFieldGetData("TST04T").equalsIgnoreCase("P"));
    P03.setVisible(lexique.HostFieldGetData("TST03T").equalsIgnoreCase("P"));
    P02.setVisible(lexique.HostFieldGetData("TST02T").equalsIgnoreCase("P"));
    P01.setVisible(lexique.HostFieldGetData("TST01T").equalsIgnoreCase("P"));
    M18.setVisible(lexique.HostFieldGetData("TST18T").equalsIgnoreCase("M"));
    M17.setVisible(lexique.HostFieldGetData("TST17T").equalsIgnoreCase("M"));
    M16.setVisible(lexique.HostFieldGetData("TST16T").equalsIgnoreCase("M"));
    M15.setVisible(lexique.HostFieldGetData("TST15T").equalsIgnoreCase("M"));
    M14.setVisible(lexique.HostFieldGetData("TST14T").equalsIgnoreCase("M"));
    M13.setVisible(lexique.HostFieldGetData("TST13T").equalsIgnoreCase("M"));
    M12.setVisible(lexique.HostFieldGetData("TST12T").equalsIgnoreCase("M"));
    M11.setVisible(lexique.HostFieldGetData("TST11T").equalsIgnoreCase("M"));
    M10.setVisible(lexique.HostFieldGetData("TST10T").equalsIgnoreCase("M"));
    M09.setVisible(lexique.HostFieldGetData("TST09T").equalsIgnoreCase("M"));
    M08.setVisible(lexique.HostFieldGetData("TST08T").equalsIgnoreCase("M"));
    M07.setVisible(lexique.HostFieldGetData("TST07T").equalsIgnoreCase("M"));
    M06.setVisible(lexique.HostFieldGetData("TST06T").equalsIgnoreCase("M"));
    M05.setVisible(lexique.HostFieldGetData("TST05T").equalsIgnoreCase("M"));
    M04.setVisible(lexique.HostFieldGetData("TST04T").equalsIgnoreCase("M"));
    M03.setVisible(lexique.HostFieldGetData("TST03T").equalsIgnoreCase("M"));
    M02.setVisible(lexique.HostFieldGetData("TST02T").equalsIgnoreCase("M"));
    M01.setVisible(lexique.HostFieldGetData("TST01T").equalsIgnoreCase("M"));
    ER18.setVisible(!lexique.HostFieldGetData("ER18").trim().equalsIgnoreCase(""));
    ER17.setVisible(!lexique.HostFieldGetData("ER17").trim().equalsIgnoreCase(""));
    ER16.setVisible(!lexique.HostFieldGetData("ER16").trim().equalsIgnoreCase(""));
    ER15.setVisible(!lexique.HostFieldGetData("ER15").trim().equalsIgnoreCase(""));
    ER14.setVisible(!lexique.HostFieldGetData("ER14").trim().equalsIgnoreCase(""));
    ER13.setVisible(!lexique.HostFieldGetData("ER13").trim().equalsIgnoreCase(""));
    ER12.setVisible(!lexique.HostFieldGetData("ER12").trim().equalsIgnoreCase(""));
    ER11.setVisible(!lexique.HostFieldGetData("ER11").trim().equalsIgnoreCase(""));
    ER10.setVisible(!lexique.HostFieldGetData("ER10").trim().equalsIgnoreCase(""));
    ER09.setVisible(!lexique.HostFieldGetData("ER09").trim().equalsIgnoreCase(""));
    ER08.setVisible(!lexique.HostFieldGetData("ER08").trim().equalsIgnoreCase(""));
    ER07.setVisible(!lexique.HostFieldGetData("ER07").trim().equalsIgnoreCase(""));
    ER06.setVisible(!lexique.HostFieldGetData("ER06").trim().equalsIgnoreCase(""));
    ER05.setVisible(!lexique.HostFieldGetData("ER05").trim().equalsIgnoreCase(""));
    ER04.setVisible(!lexique.HostFieldGetData("ER04").trim().equalsIgnoreCase(""));
    ER03.setVisible(!lexique.HostFieldGetData("ER03").trim().equalsIgnoreCase("") & lexique.isPresent("ER03"));
    ER02.setVisible(!lexique.HostFieldGetData("ER02").trim().equalsIgnoreCase(""));
    ER01.setVisible(!lexique.HostFieldGetData("ER01").trim().equalsIgnoreCase(""));
    OBJ_48_OBJ_48.setVisible(lexique.isPresent("ZP18"));
    OBJ_47_OBJ_47.setVisible(lexique.isPresent("ZP17"));
    OBJ_46_OBJ_46.setVisible(lexique.isPresent("ZP16"));
    OBJ_45_OBJ_45.setVisible(lexique.isPresent("ZP15"));
    OBJ_44_OBJ_44.setVisible(lexique.isPresent("ZP14"));
    OBJ_43_OBJ_43.setVisible(lexique.isPresent("ZP13"));
    OBJ_42_OBJ_42.setVisible(lexique.isPresent("ZP12"));
    OBJ_41_OBJ_41.setVisible(lexique.isPresent("ZP11"));
    OBJ_40_OBJ_40.setVisible(lexique.isPresent("ZP10"));
    OBJ_39_OBJ_39.setVisible(lexique.isPresent("ZP09"));
    OBJ_38_OBJ_38.setVisible(lexique.isPresent("ZP08"));
    OBJ_37_OBJ_37.setVisible(lexique.isPresent("ZP07"));
    OBJ_36_OBJ_36.setVisible(lexique.isPresent("ZP06"));
    OBJ_35_OBJ_35.setVisible(lexique.isPresent("ZP05"));
    OBJ_34_OBJ_34.setVisible(lexique.isPresent("ZP04"));
    OBJ_33_OBJ_33.setVisible(lexique.isPresent("ZP03"));
    OBJ_32_OBJ_32.setVisible(lexique.isPresent("ZP02"));
    OBJ_31_OBJ_31.setVisible(lexique.isPresent("ZP01"));
    G18.setVisible(lexique.HostFieldGetData("TST18T").equalsIgnoreCase("G"));
    G17.setVisible(lexique.HostFieldGetData("TST17T").equalsIgnoreCase("G"));
    G16.setVisible(lexique.HostFieldGetData("TST16T").equalsIgnoreCase("G"));
    G15.setVisible(lexique.HostFieldGetData("TST15T").equalsIgnoreCase("G"));
    G14.setVisible(lexique.HostFieldGetData("TST14T").equalsIgnoreCase("G"));
    G13.setVisible(lexique.HostFieldGetData("TST13T").equalsIgnoreCase("G"));
    G12.setVisible(lexique.HostFieldGetData("TST12T").equalsIgnoreCase("G"));
    G11.setVisible(lexique.HostFieldGetData("TST11T").equalsIgnoreCase("G"));
    G10.setVisible(lexique.HostFieldGetData("TST10T").equalsIgnoreCase("G"));
    G09.setVisible(lexique.HostFieldGetData("TST09T").equalsIgnoreCase("G"));
    G08.setVisible(lexique.HostFieldGetData("TST08T").equalsIgnoreCase("G"));
    G07.setVisible(lexique.HostFieldGetData("TST07T").equalsIgnoreCase("G"));
    G06.setVisible(lexique.HostFieldGetData("TST06T").equalsIgnoreCase("G"));
    G05.setVisible(lexique.HostFieldGetData("TST05T").equalsIgnoreCase("G"));
    G04.setVisible(lexique.HostFieldGetData("TST04T").equalsIgnoreCase("G"));
    G03.setVisible(lexique.HostFieldGetData("TST03T").equalsIgnoreCase("G"));
    G02.setVisible(lexique.HostFieldGetData("TST02T").equalsIgnoreCase("G"));
    G01.setVisible(lexique.HostFieldGetData("TST01T").equalsIgnoreCase("G"));
    T18.setVisible(lexique.HostFieldGetData("TST18T").equalsIgnoreCase("T"));
    T17.setVisible(lexique.HostFieldGetData("TST17T").equalsIgnoreCase("T"));
    T16.setVisible(lexique.HostFieldGetData("TST16T").equalsIgnoreCase("T"));
    T15.setVisible(lexique.HostFieldGetData("TST15T").equalsIgnoreCase("T"));
    T14.setVisible(lexique.HostFieldGetData("TST14T").equalsIgnoreCase("T"));
    T13.setVisible(lexique.HostFieldGetData("TST13T").equalsIgnoreCase("T"));
    T12.setVisible(lexique.HostFieldGetData("TST12T").equalsIgnoreCase("T"));
    T11.setVisible(lexique.HostFieldGetData("TST11T").equalsIgnoreCase("T"));
    T10.setVisible(lexique.HostFieldGetData("TST10T").equalsIgnoreCase("T"));
    T09.setVisible(lexique.HostFieldGetData("TST09T").equalsIgnoreCase("T"));
    T08.setVisible(lexique.HostFieldGetData("TST08T").equalsIgnoreCase("T"));
    T07.setVisible(lexique.HostFieldGetData("TST07T").equalsIgnoreCase("T"));
    T06.setVisible(lexique.HostFieldGetData("TST06T").equalsIgnoreCase("T"));
    T04.setVisible(lexique.HostFieldGetData("TST04T").equalsIgnoreCase("T"));
    T03.setVisible(lexique.HostFieldGetData("TST03T").equalsIgnoreCase("T"));
    T02.setVisible(lexique.HostFieldGetData("TST02T").equalsIgnoreCase("T"));
    T01.setVisible(lexique.HostFieldGetData("TST01T").equalsIgnoreCase("T"));
    T05.setVisible(lexique.HostFieldGetData("TST05T").equalsIgnoreCase("T"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("EXTENSION DE LA FICHE IMMO (zones personnalisées @TYPE@)"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="ENTER"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="F12"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F16"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F16", true);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F18"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F18", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vtim"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    T05 = new XRiTextField();
    T01 = new XRiTextField();
    T02 = new XRiTextField();
    T03 = new XRiTextField();
    T04 = new XRiTextField();
    T06 = new XRiTextField();
    T07 = new XRiTextField();
    T08 = new XRiTextField();
    T09 = new XRiTextField();
    T10 = new XRiTextField();
    T11 = new XRiTextField();
    T12 = new XRiTextField();
    T13 = new XRiTextField();
    T14 = new XRiTextField();
    T15 = new XRiTextField();
    T16 = new XRiTextField();
    T17 = new XRiTextField();
    T18 = new XRiTextField();
    G01 = new XRiTextField();
    G02 = new XRiTextField();
    G03 = new XRiTextField();
    G04 = new XRiTextField();
    G05 = new XRiTextField();
    G06 = new XRiTextField();
    G07 = new XRiTextField();
    G08 = new XRiTextField();
    G09 = new XRiTextField();
    G10 = new XRiTextField();
    G11 = new XRiTextField();
    G12 = new XRiTextField();
    G13 = new XRiTextField();
    G14 = new XRiTextField();
    G15 = new XRiTextField();
    G16 = new XRiTextField();
    G17 = new XRiTextField();
    G18 = new XRiTextField();
    OBJ_31_OBJ_31 = new JLabel();
    OBJ_32_OBJ_32 = new JLabel();
    OBJ_33_OBJ_33 = new JLabel();
    OBJ_34_OBJ_34 = new JLabel();
    OBJ_35_OBJ_35 = new JLabel();
    OBJ_36_OBJ_36 = new JLabel();
    OBJ_37_OBJ_37 = new JLabel();
    OBJ_38_OBJ_38 = new JLabel();
    OBJ_39_OBJ_39 = new JLabel();
    OBJ_40_OBJ_40 = new JLabel();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_42_OBJ_42 = new JLabel();
    OBJ_43_OBJ_43 = new JLabel();
    OBJ_44_OBJ_44 = new JLabel();
    OBJ_45_OBJ_45 = new JLabel();
    OBJ_46_OBJ_46 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_48_OBJ_48 = new JLabel();
    ER01 = new XRiTextField();
    ER02 = new XRiTextField();
    ER03 = new XRiTextField();
    ER04 = new XRiTextField();
    ER05 = new XRiTextField();
    ER06 = new XRiTextField();
    ER07 = new XRiTextField();
    ER08 = new XRiTextField();
    ER09 = new XRiTextField();
    ER10 = new XRiTextField();
    ER11 = new XRiTextField();
    ER12 = new XRiTextField();
    ER13 = new XRiTextField();
    ER14 = new XRiTextField();
    ER15 = new XRiTextField();
    ER16 = new XRiTextField();
    ER17 = new XRiTextField();
    ER18 = new XRiTextField();
    OBJ_28 = new JButton();
    OBJ_29 = new JButton();
    M01 = new XRiTextField();
    M02 = new XRiTextField();
    M03 = new XRiTextField();
    M04 = new XRiTextField();
    M05 = new XRiTextField();
    M06 = new XRiTextField();
    M07 = new XRiTextField();
    M08 = new XRiTextField();
    M09 = new XRiTextField();
    M10 = new XRiTextField();
    M11 = new XRiTextField();
    M12 = new XRiTextField();
    M13 = new XRiTextField();
    M14 = new XRiTextField();
    M15 = new XRiTextField();
    M16 = new XRiTextField();
    M17 = new XRiTextField();
    M18 = new XRiTextField();
    P01 = new XRiTextField();
    P02 = new XRiTextField();
    P03 = new XRiTextField();
    P04 = new XRiTextField();
    P05 = new XRiTextField();
    P06 = new XRiTextField();
    P07 = new XRiTextField();
    P08 = new XRiTextField();
    P09 = new XRiTextField();
    P10 = new XRiTextField();
    P11 = new XRiTextField();
    P12 = new XRiTextField();
    P13 = new XRiTextField();
    P14 = new XRiTextField();
    P15 = new XRiTextField();
    P16 = new XRiTextField();
    P17 = new XRiTextField();
    P18 = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- T05 ----
    T05.setComponentPopupMenu(BTD);
    T05.setName("T05");
    add(T05);
    T05.setBounds(210, 140, 310, T05.getPreferredSize().height);

    //---- T01 ----
    T01.setComponentPopupMenu(BTD);
    T01.setName("T01");
    add(T01);
    T01.setBounds(210, 40, 310, T01.getPreferredSize().height);

    //---- T02 ----
    T02.setComponentPopupMenu(BTD);
    T02.setName("T02");
    add(T02);
    T02.setBounds(210, 65, 310, T02.getPreferredSize().height);

    //---- T03 ----
    T03.setComponentPopupMenu(BTD);
    T03.setName("T03");
    add(T03);
    T03.setBounds(210, 90, 310, T03.getPreferredSize().height);

    //---- T04 ----
    T04.setComponentPopupMenu(BTD);
    T04.setName("T04");
    add(T04);
    T04.setBounds(210, 115, 310, T04.getPreferredSize().height);

    //---- T06 ----
    T06.setComponentPopupMenu(BTD);
    T06.setName("T06");
    add(T06);
    T06.setBounds(210, 165, 310, T06.getPreferredSize().height);

    //---- T07 ----
    T07.setComponentPopupMenu(BTD);
    T07.setName("T07");
    add(T07);
    T07.setBounds(210, 190, 310, T07.getPreferredSize().height);

    //---- T08 ----
    T08.setComponentPopupMenu(BTD);
    T08.setName("T08");
    add(T08);
    T08.setBounds(210, 215, 310, T08.getPreferredSize().height);

    //---- T09 ----
    T09.setComponentPopupMenu(BTD);
    T09.setName("T09");
    add(T09);
    T09.setBounds(210, 240, 310, T09.getPreferredSize().height);

    //---- T10 ----
    T10.setComponentPopupMenu(BTD);
    T10.setName("T10");
    add(T10);
    T10.setBounds(210, 265, 310, T10.getPreferredSize().height);

    //---- T11 ----
    T11.setComponentPopupMenu(BTD);
    T11.setName("T11");
    add(T11);
    T11.setBounds(210, 290, 310, T11.getPreferredSize().height);

    //---- T12 ----
    T12.setComponentPopupMenu(BTD);
    T12.setName("T12");
    add(T12);
    T12.setBounds(210, 315, 310, T12.getPreferredSize().height);

    //---- T13 ----
    T13.setComponentPopupMenu(BTD);
    T13.setName("T13");
    add(T13);
    T13.setBounds(210, 340, 310, T13.getPreferredSize().height);

    //---- T14 ----
    T14.setComponentPopupMenu(BTD);
    T14.setName("T14");
    add(T14);
    T14.setBounds(210, 365, 310, T14.getPreferredSize().height);

    //---- T15 ----
    T15.setComponentPopupMenu(BTD);
    T15.setName("T15");
    add(T15);
    T15.setBounds(210, 390, 310, T15.getPreferredSize().height);

    //---- T16 ----
    T16.setComponentPopupMenu(BTD);
    T16.setName("T16");
    add(T16);
    T16.setBounds(210, 415, 310, T16.getPreferredSize().height);

    //---- T17 ----
    T17.setComponentPopupMenu(BTD);
    T17.setName("T17");
    add(T17);
    T17.setBounds(210, 440, 310, T17.getPreferredSize().height);

    //---- T18 ----
    T18.setComponentPopupMenu(BTD);
    T18.setName("T18");
    add(T18);
    T18.setBounds(210, 465, 310, T18.getPreferredSize().height);

    //---- G01 ----
    G01.setComponentPopupMenu(BTD);
    G01.setName("G01");
    add(G01);
    G01.setBounds(213, 41, 150, 20);

    //---- G02 ----
    G02.setComponentPopupMenu(BTD);
    G02.setName("G02");
    add(G02);
    G02.setBounds(213, 65, 150, 20);

    //---- G03 ----
    G03.setComponentPopupMenu(BTD);
    G03.setName("G03");
    add(G03);
    G03.setBounds(213, 89, 150, 20);

    //---- G04 ----
    G04.setComponentPopupMenu(BTD);
    G04.setName("G04");
    add(G04);
    G04.setBounds(213, 113, 150, 20);

    //---- G05 ----
    G05.setComponentPopupMenu(BTD);
    G05.setName("G05");
    add(G05);
    G05.setBounds(213, 137, 150, 20);

    //---- G06 ----
    G06.setComponentPopupMenu(BTD);
    G06.setName("G06");
    add(G06);
    G06.setBounds(213, 161, 150, 20);

    //---- G07 ----
    G07.setComponentPopupMenu(BTD);
    G07.setName("G07");
    add(G07);
    G07.setBounds(213, 185, 150, 20);

    //---- G08 ----
    G08.setComponentPopupMenu(BTD);
    G08.setName("G08");
    add(G08);
    G08.setBounds(213, 209, 150, 20);

    //---- G09 ----
    G09.setComponentPopupMenu(BTD);
    G09.setName("G09");
    add(G09);
    G09.setBounds(213, 233, 150, 20);

    //---- G10 ----
    G10.setComponentPopupMenu(BTD);
    G10.setName("G10");
    add(G10);
    G10.setBounds(213, 257, 150, 20);

    //---- G11 ----
    G11.setComponentPopupMenu(BTD);
    G11.setName("G11");
    add(G11);
    G11.setBounds(213, 281, 150, 20);

    //---- G12 ----
    G12.setComponentPopupMenu(BTD);
    G12.setName("G12");
    add(G12);
    G12.setBounds(213, 305, 150, 20);

    //---- G13 ----
    G13.setComponentPopupMenu(BTD);
    G13.setName("G13");
    add(G13);
    G13.setBounds(213, 329, 150, 20);

    //---- G14 ----
    G14.setComponentPopupMenu(BTD);
    G14.setName("G14");
    add(G14);
    G14.setBounds(213, 353, 150, 20);

    //---- G15 ----
    G15.setComponentPopupMenu(BTD);
    G15.setName("G15");
    add(G15);
    G15.setBounds(213, 377, 150, 20);

    //---- G16 ----
    G16.setComponentPopupMenu(BTD);
    G16.setName("G16");
    add(G16);
    G16.setBounds(213, 401, 150, 20);

    //---- G17 ----
    G17.setComponentPopupMenu(BTD);
    G17.setName("G17");
    add(G17);
    G17.setBounds(213, 425, 150, 20);

    //---- G18 ----
    G18.setComponentPopupMenu(BTD);
    G18.setName("G18");
    add(G18);
    G18.setBounds(213, 449, 150, 20);

    //---- OBJ_31_OBJ_31 ----
    OBJ_31_OBJ_31.setText("@ZP01@");
    OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");
    add(OBJ_31_OBJ_31);
    OBJ_31_OBJ_31.setBounds(36, 43, 165, 16);

    //---- OBJ_32_OBJ_32 ----
    OBJ_32_OBJ_32.setText("@ZP02@");
    OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
    add(OBJ_32_OBJ_32);
    OBJ_32_OBJ_32.setBounds(36, 67, 165, 16);

    //---- OBJ_33_OBJ_33 ----
    OBJ_33_OBJ_33.setText("@ZP03@");
    OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
    add(OBJ_33_OBJ_33);
    OBJ_33_OBJ_33.setBounds(36, 91, 165, 16);

    //---- OBJ_34_OBJ_34 ----
    OBJ_34_OBJ_34.setText("@ZP04@");
    OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
    add(OBJ_34_OBJ_34);
    OBJ_34_OBJ_34.setBounds(36, 115, 165, 16);

    //---- OBJ_35_OBJ_35 ----
    OBJ_35_OBJ_35.setText("@ZP05@");
    OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");
    add(OBJ_35_OBJ_35);
    OBJ_35_OBJ_35.setBounds(36, 139, 165, 16);

    //---- OBJ_36_OBJ_36 ----
    OBJ_36_OBJ_36.setText("@ZP06@");
    OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");
    add(OBJ_36_OBJ_36);
    OBJ_36_OBJ_36.setBounds(36, 163, 165, 16);

    //---- OBJ_37_OBJ_37 ----
    OBJ_37_OBJ_37.setText("@ZP07@");
    OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
    add(OBJ_37_OBJ_37);
    OBJ_37_OBJ_37.setBounds(36, 187, 165, 16);

    //---- OBJ_38_OBJ_38 ----
    OBJ_38_OBJ_38.setText("@ZP08@");
    OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");
    add(OBJ_38_OBJ_38);
    OBJ_38_OBJ_38.setBounds(36, 211, 165, 16);

    //---- OBJ_39_OBJ_39 ----
    OBJ_39_OBJ_39.setText("@ZP09@");
    OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");
    add(OBJ_39_OBJ_39);
    OBJ_39_OBJ_39.setBounds(36, 235, 165, 16);

    //---- OBJ_40_OBJ_40 ----
    OBJ_40_OBJ_40.setText("@ZP10@");
    OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
    add(OBJ_40_OBJ_40);
    OBJ_40_OBJ_40.setBounds(36, 259, 165, 16);

    //---- OBJ_41_OBJ_41 ----
    OBJ_41_OBJ_41.setText("@ZP11@");
    OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
    add(OBJ_41_OBJ_41);
    OBJ_41_OBJ_41.setBounds(36, 283, 165, 16);

    //---- OBJ_42_OBJ_42 ----
    OBJ_42_OBJ_42.setText("@ZP12@");
    OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
    add(OBJ_42_OBJ_42);
    OBJ_42_OBJ_42.setBounds(36, 307, 165, 16);

    //---- OBJ_43_OBJ_43 ----
    OBJ_43_OBJ_43.setText("@ZP13@");
    OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
    add(OBJ_43_OBJ_43);
    OBJ_43_OBJ_43.setBounds(36, 331, 165, 16);

    //---- OBJ_44_OBJ_44 ----
    OBJ_44_OBJ_44.setText("@ZP14@");
    OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
    add(OBJ_44_OBJ_44);
    OBJ_44_OBJ_44.setBounds(36, 355, 165, 16);

    //---- OBJ_45_OBJ_45 ----
    OBJ_45_OBJ_45.setText("@ZP15@");
    OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");
    add(OBJ_45_OBJ_45);
    OBJ_45_OBJ_45.setBounds(36, 379, 165, 16);

    //---- OBJ_46_OBJ_46 ----
    OBJ_46_OBJ_46.setText("@ZP16@");
    OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");
    add(OBJ_46_OBJ_46);
    OBJ_46_OBJ_46.setBounds(36, 403, 165, 16);

    //---- OBJ_47_OBJ_47 ----
    OBJ_47_OBJ_47.setText("@ZP17@");
    OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
    add(OBJ_47_OBJ_47);
    OBJ_47_OBJ_47.setBounds(36, 427, 165, 16);

    //---- OBJ_48_OBJ_48 ----
    OBJ_48_OBJ_48.setText("@ZP18@");
    OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
    add(OBJ_48_OBJ_48);
    OBJ_48_OBJ_48.setBounds(36, 451, 165, 16);

    //---- ER01 ----
    ER01.setComponentPopupMenu(BTD);
    ER01.setName("ER01");
    add(ER01);
    ER01.setBounds(550, 40, 150, ER01.getPreferredSize().height);

    //---- ER02 ----
    ER02.setComponentPopupMenu(BTD);
    ER02.setName("ER02");
    add(ER02);
    ER02.setBounds(550, 65, 150, ER02.getPreferredSize().height);

    //---- ER03 ----
    ER03.setComponentPopupMenu(BTD);
    ER03.setName("ER03");
    add(ER03);
    ER03.setBounds(550, 90, 150, ER03.getPreferredSize().height);

    //---- ER04 ----
    ER04.setComponentPopupMenu(BTD);
    ER04.setName("ER04");
    add(ER04);
    ER04.setBounds(550, 115, 150, ER04.getPreferredSize().height);

    //---- ER05 ----
    ER05.setComponentPopupMenu(BTD);
    ER05.setName("ER05");
    add(ER05);
    ER05.setBounds(550, 140, 150, ER05.getPreferredSize().height);

    //---- ER06 ----
    ER06.setComponentPopupMenu(BTD);
    ER06.setName("ER06");
    add(ER06);
    ER06.setBounds(550, 165, 150, ER06.getPreferredSize().height);

    //---- ER07 ----
    ER07.setComponentPopupMenu(BTD);
    ER07.setName("ER07");
    add(ER07);
    ER07.setBounds(550, 190, 150, ER07.getPreferredSize().height);

    //---- ER08 ----
    ER08.setComponentPopupMenu(BTD);
    ER08.setName("ER08");
    add(ER08);
    ER08.setBounds(550, 215, 150, ER08.getPreferredSize().height);

    //---- ER09 ----
    ER09.setComponentPopupMenu(BTD);
    ER09.setName("ER09");
    add(ER09);
    ER09.setBounds(550, 240, 150, ER09.getPreferredSize().height);

    //---- ER10 ----
    ER10.setComponentPopupMenu(BTD);
    ER10.setName("ER10");
    add(ER10);
    ER10.setBounds(550, 265, 150, ER10.getPreferredSize().height);

    //---- ER11 ----
    ER11.setComponentPopupMenu(BTD);
    ER11.setName("ER11");
    add(ER11);
    ER11.setBounds(550, 290, 150, ER11.getPreferredSize().height);

    //---- ER12 ----
    ER12.setComponentPopupMenu(BTD);
    ER12.setName("ER12");
    add(ER12);
    ER12.setBounds(550, 315, 150, ER12.getPreferredSize().height);

    //---- ER13 ----
    ER13.setComponentPopupMenu(BTD);
    ER13.setName("ER13");
    add(ER13);
    ER13.setBounds(550, 340, 150, ER13.getPreferredSize().height);

    //---- ER14 ----
    ER14.setComponentPopupMenu(BTD);
    ER14.setName("ER14");
    add(ER14);
    ER14.setBounds(550, 365, 150, ER14.getPreferredSize().height);

    //---- ER15 ----
    ER15.setComponentPopupMenu(BTD);
    ER15.setName("ER15");
    add(ER15);
    ER15.setBounds(550, 390, 150, ER15.getPreferredSize().height);

    //---- ER16 ----
    ER16.setComponentPopupMenu(BTD);
    ER16.setName("ER16");
    add(ER16);
    ER16.setBounds(550, 415, 150, ER16.getPreferredSize().height);

    //---- ER17 ----
    ER17.setComponentPopupMenu(BTD);
    ER17.setName("ER17");
    add(ER17);
    ER17.setBounds(550, 440, 150, ER17.getPreferredSize().height);

    //---- ER18 ----
    ER18.setComponentPopupMenu(BTD);
    ER18.setName("ER18");
    add(ER18);
    ER18.setBounds(550, 465, 150, ER18.getPreferredSize().height);

    //---- OBJ_28 ----
    OBJ_28.setText(" ");
    OBJ_28.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_28.setName("OBJ_28");
    OBJ_28.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_28ActionPerformed(e);
      }
    });
    add(OBJ_28);
    OBJ_28.setBounds(580, 500, 56, 40);

    //---- OBJ_29 ----
    OBJ_29.setText(" ");
    OBJ_29.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_29.setName("OBJ_29");
    OBJ_29.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_29ActionPerformed(e);
      }
    });
    add(OBJ_29);
    OBJ_29.setBounds(640, 500, 56, 40);

    //---- M01 ----
    M01.setComponentPopupMenu(BTD);
    M01.setName("M01");
    add(M01);
    M01.setBounds(213, 41, 90, 20);

    //---- M02 ----
    M02.setComponentPopupMenu(BTD);
    M02.setName("M02");
    add(M02);
    M02.setBounds(213, 65, 90, 20);

    //---- M03 ----
    M03.setComponentPopupMenu(BTD);
    M03.setName("M03");
    add(M03);
    M03.setBounds(213, 89, 90, 20);

    //---- M04 ----
    M04.setComponentPopupMenu(BTD);
    M04.setName("M04");
    add(M04);
    M04.setBounds(213, 113, 90, 20);

    //---- M05 ----
    M05.setComponentPopupMenu(BTD);
    M05.setName("M05");
    add(M05);
    M05.setBounds(213, 137, 90, 20);

    //---- M06 ----
    M06.setComponentPopupMenu(BTD);
    M06.setName("M06");
    add(M06);
    M06.setBounds(213, 161, 90, 20);

    //---- M07 ----
    M07.setComponentPopupMenu(BTD);
    M07.setName("M07");
    add(M07);
    M07.setBounds(213, 185, 90, 20);

    //---- M08 ----
    M08.setComponentPopupMenu(BTD);
    M08.setName("M08");
    add(M08);
    M08.setBounds(213, 209, 90, 20);

    //---- M09 ----
    M09.setComponentPopupMenu(BTD);
    M09.setName("M09");
    add(M09);
    M09.setBounds(213, 233, 90, 20);

    //---- M10 ----
    M10.setComponentPopupMenu(BTD);
    M10.setName("M10");
    add(M10);
    M10.setBounds(213, 257, 90, 20);

    //---- M11 ----
    M11.setComponentPopupMenu(BTD);
    M11.setName("M11");
    add(M11);
    M11.setBounds(213, 281, 90, 20);

    //---- M12 ----
    M12.setComponentPopupMenu(BTD);
    M12.setName("M12");
    add(M12);
    M12.setBounds(213, 305, 90, 20);

    //---- M13 ----
    M13.setComponentPopupMenu(BTD);
    M13.setName("M13");
    add(M13);
    M13.setBounds(213, 329, 90, 20);

    //---- M14 ----
    M14.setComponentPopupMenu(BTD);
    M14.setName("M14");
    add(M14);
    M14.setBounds(213, 353, 90, 20);

    //---- M15 ----
    M15.setComponentPopupMenu(BTD);
    M15.setName("M15");
    add(M15);
    M15.setBounds(213, 377, 90, 20);

    //---- M16 ----
    M16.setComponentPopupMenu(BTD);
    M16.setName("M16");
    add(M16);
    M16.setBounds(213, 401, 90, 20);

    //---- M17 ----
    M17.setComponentPopupMenu(BTD);
    M17.setName("M17");
    add(M17);
    M17.setBounds(213, 425, 90, 20);

    //---- M18 ----
    M18.setComponentPopupMenu(BTD);
    M18.setName("M18");
    add(M18);
    M18.setBounds(213, 449, 90, 20);

    //---- P01 ----
    P01.setComponentPopupMenu(BTD);
    P01.setName("P01");
    add(P01);
    P01.setBounds(213, 41, 37, 20);

    //---- P02 ----
    P02.setComponentPopupMenu(BTD);
    P02.setName("P02");
    add(P02);
    P02.setBounds(213, 65, 37, 20);

    //---- P03 ----
    P03.setComponentPopupMenu(BTD);
    P03.setName("P03");
    add(P03);
    P03.setBounds(213, 89, 37, 20);

    //---- P04 ----
    P04.setComponentPopupMenu(BTD);
    P04.setName("P04");
    add(P04);
    P04.setBounds(213, 113, 37, 20);

    //---- P05 ----
    P05.setComponentPopupMenu(BTD);
    P05.setName("P05");
    add(P05);
    P05.setBounds(213, 137, 37, 20);

    //---- P06 ----
    P06.setComponentPopupMenu(BTD);
    P06.setName("P06");
    add(P06);
    P06.setBounds(213, 161, 37, 20);

    //---- P07 ----
    P07.setComponentPopupMenu(BTD);
    P07.setName("P07");
    add(P07);
    P07.setBounds(213, 185, 37, 20);

    //---- P08 ----
    P08.setComponentPopupMenu(BTD);
    P08.setName("P08");
    add(P08);
    P08.setBounds(213, 209, 37, 20);

    //---- P09 ----
    P09.setComponentPopupMenu(BTD);
    P09.setName("P09");
    add(P09);
    P09.setBounds(213, 233, 37, 20);

    //---- P10 ----
    P10.setComponentPopupMenu(BTD);
    P10.setName("P10");
    add(P10);
    P10.setBounds(213, 257, 37, 20);

    //---- P11 ----
    P11.setComponentPopupMenu(BTD);
    P11.setName("P11");
    add(P11);
    P11.setBounds(213, 281, 37, 20);

    //---- P12 ----
    P12.setComponentPopupMenu(BTD);
    P12.setName("P12");
    add(P12);
    P12.setBounds(213, 305, 37, 20);

    //---- P13 ----
    P13.setComponentPopupMenu(BTD);
    P13.setName("P13");
    add(P13);
    P13.setBounds(213, 329, 37, 20);

    //---- P14 ----
    P14.setComponentPopupMenu(BTD);
    P14.setName("P14");
    add(P14);
    P14.setBounds(213, 353, 37, 20);

    //---- P15 ----
    P15.setComponentPopupMenu(BTD);
    P15.setName("P15");
    add(P15);
    P15.setBounds(213, 377, 37, 20);

    //---- P16 ----
    P16.setComponentPopupMenu(BTD);
    P16.setName("P16");
    add(P16);
    P16.setBounds(213, 401, 37, 20);

    //---- P17 ----
    P17.setComponentPopupMenu(BTD);
    P17.setName("P17");
    add(P17);
    P17.setBounds(213, 425, 37, 20);

    //---- P18 ----
    P18.setComponentPopupMenu(BTD);
    P18.setName("P18");
    add(P18);
    P18.setBounds(213, 449, 37, 20);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Immobilisation @IMORD@ @IMXXX@ -  @IMLB1@");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(10, 10, 700, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(730, 555));

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Recherche multi-crit\u00e8res");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
      OBJ_4.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Cr\u00e9ation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Modification");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Interrogation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Annulation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_14);
      OBJ_4.addSeparator();

      //---- OBJ_15 ----
      OBJ_15.setText("Exploitation");
      OBJ_15.setName("OBJ_15");
      OBJ_4.add(OBJ_15);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private XRiTextField T05;
  private XRiTextField T01;
  private XRiTextField T02;
  private XRiTextField T03;
  private XRiTextField T04;
  private XRiTextField T06;
  private XRiTextField T07;
  private XRiTextField T08;
  private XRiTextField T09;
  private XRiTextField T10;
  private XRiTextField T11;
  private XRiTextField T12;
  private XRiTextField T13;
  private XRiTextField T14;
  private XRiTextField T15;
  private XRiTextField T16;
  private XRiTextField T17;
  private XRiTextField T18;
  private XRiTextField G01;
  private XRiTextField G02;
  private XRiTextField G03;
  private XRiTextField G04;
  private XRiTextField G05;
  private XRiTextField G06;
  private XRiTextField G07;
  private XRiTextField G08;
  private XRiTextField G09;
  private XRiTextField G10;
  private XRiTextField G11;
  private XRiTextField G12;
  private XRiTextField G13;
  private XRiTextField G14;
  private XRiTextField G15;
  private XRiTextField G16;
  private XRiTextField G17;
  private XRiTextField G18;
  private JLabel OBJ_31_OBJ_31;
  private JLabel OBJ_32_OBJ_32;
  private JLabel OBJ_33_OBJ_33;
  private JLabel OBJ_34_OBJ_34;
  private JLabel OBJ_35_OBJ_35;
  private JLabel OBJ_36_OBJ_36;
  private JLabel OBJ_37_OBJ_37;
  private JLabel OBJ_38_OBJ_38;
  private JLabel OBJ_39_OBJ_39;
  private JLabel OBJ_40_OBJ_40;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_42_OBJ_42;
  private JLabel OBJ_43_OBJ_43;
  private JLabel OBJ_44_OBJ_44;
  private JLabel OBJ_45_OBJ_45;
  private JLabel OBJ_46_OBJ_46;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_48_OBJ_48;
  private XRiTextField ER01;
  private XRiTextField ER02;
  private XRiTextField ER03;
  private XRiTextField ER04;
  private XRiTextField ER05;
  private XRiTextField ER06;
  private XRiTextField ER07;
  private XRiTextField ER08;
  private XRiTextField ER09;
  private XRiTextField ER10;
  private XRiTextField ER11;
  private XRiTextField ER12;
  private XRiTextField ER13;
  private XRiTextField ER14;
  private XRiTextField ER15;
  private XRiTextField ER16;
  private XRiTextField ER17;
  private XRiTextField ER18;
  private JButton OBJ_28;
  private JButton OBJ_29;
  private XRiTextField M01;
  private XRiTextField M02;
  private XRiTextField M03;
  private XRiTextField M04;
  private XRiTextField M05;
  private XRiTextField M06;
  private XRiTextField M07;
  private XRiTextField M08;
  private XRiTextField M09;
  private XRiTextField M10;
  private XRiTextField M11;
  private XRiTextField M12;
  private XRiTextField M13;
  private XRiTextField M14;
  private XRiTextField M15;
  private XRiTextField M16;
  private XRiTextField M17;
  private XRiTextField M18;
  private XRiTextField P01;
  private XRiTextField P02;
  private XRiTextField P03;
  private XRiTextField P04;
  private XRiTextField P05;
  private XRiTextField P06;
  private XRiTextField P07;
  private XRiTextField P08;
  private XRiTextField P09;
  private XRiTextField P10;
  private XRiTextField P11;
  private XRiTextField P12;
  private XRiTextField P13;
  private XRiTextField P14;
  private XRiTextField P15;
  private XRiTextField P16;
  private XRiTextField P17;
  private XRiTextField P18;
  private JXTitledSeparator xTitledSeparator1;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
