
package ri.serien.libecranrpg.s137.S137303F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class S137303F_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] WTNS_Value = { " ", "1", "2", "3", "4", "5", "6", "9" };
  
  public S137303F_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTNS.setValeurs(WTNS_Value, null);
    WTOU2.setValeursSelection("**", "  ");
    WTOU.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    l_LOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_30_OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_35_OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBDEV@")).trim());
    OBJ_34_OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMAG@")).trim());
    OBJ_32_OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEPD@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEPF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    WMAG.setEnabled(lexique.isPresent("WMAG"));
    WDEV.setEnabled(lexique.isPresent("WDEV"));
    WETB.setVisible(lexique.isPresent("WETB"));
    // WTOU2.setSelected(lexique.HostFieldGetData("WTOU2").equalsIgnoreCase("**"));
    WTNS.setEnabled(!WTOU2.isSelected());
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!WTOU.isSelected());
    OBJ_34_OBJ_34.setVisible(lexique.isPresent("LIBMAG"));
    OBJ_35_OBJ_35.setVisible(lexique.isPresent("LIBDEV"));
    OBJ_30_OBJ_30.setVisible(lexique.isPresent("DGNOM"));
    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOU2.isSelected())
    // lexique.HostFieldPutData("WTOU2", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU2", 0, " ");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    // lexique.HostFieldPutData("WTNS", 0, WTNS_Value[WTNS.getSelectedIndex()]);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void f4ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void f1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    WTNS.setEnabled(!WTNS.isEnabled());
  }
  
  private void riBoutonRecherche1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    l_LOCTP = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_28 = new JXTitledSeparator();
    OBJ_29 = new JXTitledSeparator();
    OBJ_27 = new JXTitledSeparator();
    OBJ_30_OBJ_30 = new RiZoneSortie();
    OBJ_35_OBJ_35 = new RiZoneSortie();
    OBJ_34_OBJ_34 = new RiZoneSortie();
    WTOU = new XRiCheckBox();
    WTOU2 = new XRiCheckBox();
    OBJ_33_OBJ_33 = new JLabel();
    OBJ_36_OBJ_36 = new JLabel();
    OBJ_37_OBJ_37 = new JLabel();
    OBJ_32_OBJ_32 = new RiZoneSortie();
    WETB = new RiZoneSortie();
    WDEV = new XRiTextField();
    WMAG = new XRiTextField();
    P_SEL0 = new JPanel();
    OBJ_39_OBJ_39 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    DEBICD = new XRiTextField();
    FINICD = new XRiTextField();
    riZoneSortie1 = new RiZoneSortie();
    riZoneSortie2 = new RiZoneSortie();
    riBoutonRecherche1 = new SNBoutonRecherche();
    WTNS = new XRiComboBox();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    f4 = new JMenuItem();
    f1 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- l_LOCTP ----
          l_LOCTP.setText("@LOCTP@");
          l_LOCTP.setFont(new Font("sansserif", Font.BOLD, 12));
          l_LOCTP.setPreferredSize(new Dimension(120, 20));
          l_LOCTP.setHorizontalTextPosition(SwingConstants.LEADING);
          l_LOCTP.setHorizontalAlignment(SwingConstants.RIGHT);
          l_LOCTP.setName("l_LOCTP");
          p_tete_droite.add(l_LOCTP);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Exportation tableur");
            riSousMenu_bt6.setToolTipText("Exportation vers tableur");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(680, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_28 ----
          OBJ_28.setTitle("Plage de codes postaux ou d\u00e9partements des clients");
          OBJ_28.setName("OBJ_28");

          //---- OBJ_29 ----
          OBJ_29.setTitle("Etablissement s\u00e9lectionn\u00e9");
          OBJ_29.setName("OBJ_29");

          //---- OBJ_27 ----
          OBJ_27.setTitle("");
          OBJ_27.setName("OBJ_27");

          //---- OBJ_30_OBJ_30 ----
          OBJ_30_OBJ_30.setText("@DGNOM@");
          OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");

          //---- OBJ_35_OBJ_35 ----
          OBJ_35_OBJ_35.setText("@LIBDEV@");
          OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");

          //---- OBJ_34_OBJ_34 ----
          OBJ_34_OBJ_34.setText("@LIBMAG@");
          OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setComponentPopupMenu(BTDA);
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          WTOU.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          //---- WTOU2 ----
          WTOU2.setText("S\u00e9lection compl\u00e8te");
          WTOU2.setComponentPopupMenu(BTDA);
          WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU2.setName("WTOU2");
          WTOU2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOU2ActionPerformed(e);
            }
          });

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("Top d'attention");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("Code magasin");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");

          //---- OBJ_37_OBJ_37 ----
          OBJ_37_OBJ_37.setText("Devise");
          OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("@WENCX@");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setText("@WETB@");
          WETB.setName("WETB");

          //---- WDEV ----
          WDEV.setComponentPopupMenu(BTD);
          WDEV.setName("WDEV");

          //---- WMAG ----
          WMAG.setComponentPopupMenu(BTD);
          WMAG.setName("WMAG");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_39_OBJ_39 ----
            OBJ_39_OBJ_39.setText("Code de d\u00e9but");
            OBJ_39_OBJ_39.setName("OBJ_39_OBJ_39");
            P_SEL0.add(OBJ_39_OBJ_39);
            OBJ_39_OBJ_39.setBounds(15, 19, 100, 20);

            //---- OBJ_47_OBJ_47 ----
            OBJ_47_OBJ_47.setText("Code de fin");
            OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
            P_SEL0.add(OBJ_47_OBJ_47);
            OBJ_47_OBJ_47.setBounds(15, 55, 100, 20);

            //---- DEBICD ----
            DEBICD.setComponentPopupMenu(BTD);
            DEBICD.setName("DEBICD");
            P_SEL0.add(DEBICD);
            DEBICD.setBounds(135, 15, 55, DEBICD.getPreferredSize().height);

            //---- FINICD ----
            FINICD.setComponentPopupMenu(BTD);
            FINICD.setName("FINICD");
            P_SEL0.add(FINICD);
            FINICD.setBounds(135, 50, 55, FINICD.getPreferredSize().height);

            //---- riZoneSortie1 ----
            riZoneSortie1.setText("@DEPD@");
            riZoneSortie1.setName("riZoneSortie1");
            P_SEL0.add(riZoneSortie1);
            riZoneSortie1.setBounds(215, 17, 280, riZoneSortie1.getPreferredSize().height);

            //---- riZoneSortie2 ----
            riZoneSortie2.setText("@DEPF@");
            riZoneSortie2.setName("riZoneSortie2");
            P_SEL0.add(riZoneSortie2);
            riZoneSortie2.setBounds(215, 52, 280, riZoneSortie2.getPreferredSize().height);
          }

          //---- riBoutonRecherche1 ----
          riBoutonRecherche1.setToolTipText("Changement d'\u00e9tablissement");
          riBoutonRecherche1.setName("riBoutonRecherche1");
          riBoutonRecherche1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonRecherche1ActionPerformed(e);
            }
          });

          //---- WTNS ----
          WTNS.setModel(new DefaultComboBoxModel(new String[] {
            "Client actif",
            "Alerte en rouge",
            "Client interdit",
            "Livraison interdite si d\u00e9pass.",
            "Attente si d\u00e9passement",
            "Acompte obligatoire",
            "Paiement \u00e0 la commande",
            "Client d\u00e9sactiv\u00e9"
          }));
          WTNS.setToolTipText("Restrictions d'utilisation de ce client");
          WTNS.setComponentPopupMenu(BTDA);
          WTNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTNS.setName("WTNS");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 615, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(riBoutonRecherche1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(67, 67, 67)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_30_OBJ_30, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 615, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(94, 94, 94)
                    .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 615, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                    .addGap(80, 80, 80)
                    .addComponent(WMAG, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_34_OBJ_34, GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(OBJ_37_OBJ_37, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                    .addGap(90, 90, 90)
                    .addComponent(WDEV, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_35_OBJ_35, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(49, 49, 49)
                    .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
                    .addGap(51, 51, 51)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(WTNS, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE))))
                .addGap(39, 39, 39))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(riBoutonRecherche1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(OBJ_30_OBJ_30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WMAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_34_OBJ_34, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(17, 17, 17)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_33_OBJ_33, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WTOU2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addComponent(WTNS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_37_OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WDEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_35_OBJ_35, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- f4 ----
      f4.setText("Choix possibles");
      f4.setName("f4");
      f4.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          f4ActionPerformed(e);
        }
      });
      BTD.add(f4);

      //---- f1 ----
      f1.setText("Aide en ligne");
      f1.setName("f1");
      f1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          f1ActionPerformed(e);
        }
      });
      BTD.add(f1);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel l_LOCTP;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator OBJ_28;
  private JXTitledSeparator OBJ_29;
  private JXTitledSeparator OBJ_27;
  private RiZoneSortie OBJ_30_OBJ_30;
  private RiZoneSortie OBJ_35_OBJ_35;
  private RiZoneSortie OBJ_34_OBJ_34;
  private XRiCheckBox WTOU;
  private XRiCheckBox WTOU2;
  private JLabel OBJ_33_OBJ_33;
  private JLabel OBJ_36_OBJ_36;
  private JLabel OBJ_37_OBJ_37;
  private RiZoneSortie OBJ_32_OBJ_32;
  private RiZoneSortie WETB;
  private XRiTextField WDEV;
  private XRiTextField WMAG;
  private JPanel P_SEL0;
  private JLabel OBJ_39_OBJ_39;
  private JLabel OBJ_47_OBJ_47;
  private XRiTextField DEBICD;
  private XRiTextField FINICD;
  private RiZoneSortie riZoneSortie1;
  private RiZoneSortie riZoneSortie2;
  private SNBoutonRecherche riBoutonRecherche1;
  private XRiComboBox WTNS;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem f4;
  private JMenuItem f1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
