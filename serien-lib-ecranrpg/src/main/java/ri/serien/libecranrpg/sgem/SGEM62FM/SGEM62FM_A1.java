
package ri.serien.libecranrpg.sgem.SGEM62FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGEM62FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGEM62FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    OPTTRT.setValeurs("OUI", OPTTRT_GRP);
    OPTTRT_NON.setValeurs("NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_49_OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNUPAS@")).trim());
    OBJ_49_OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // OPTTRT2.setEnabled( lexique.isPresent("OPTTRT"));
    // OPTTRT2.setSelected(lexique.HostFieldGetData("OPTTRT").equalsIgnoreCase("OUI"));
    // OPTTRT_OBJ_30.setEnabled( lexique.isPresent("OPTTRT"));
    // OPTTRT_OBJ_30.setSelected(lexique.HostFieldGetData("OPTTRT").equalsIgnoreCase("NON"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (OPTTRT2.isSelected())
    // lexique.HostFieldPutData("OPTTRT", 0, "OUI");
    // if (OPTTRT_OBJ_30.isSelected())
    // lexique.HostFieldPutData("OPTTRT", 0, "NON");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledSeparator1 = new JXTitledSeparator();
    OPTTRT_NON = new XRiRadioButton();
    OPTTRT = new XRiRadioButton();
    OBJ_28_OBJ_28 = new JLabel();
    OBJ_49_OBJ_49 = new RiZoneSortie();
    OBJ_28_OBJ_29 = new JLabel();
    separator1 = new JSeparator();
    OBJ_49_OBJ_50 = new RiZoneSortie();
    OPTTRT_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(750, 280));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Options de traitement");
          xTitledSeparator1.setName("xTitledSeparator1");

          //---- OPTTRT_NON ----
          OPTTRT_NON.setText("Effacement du fichier et traitement normal");
          OPTTRT_NON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTTRT_NON.setName("OPTTRT_NON");

          //---- OPTTRT ----
          OPTTRT.setText("Essais de reprise");
          OPTTRT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPTTRT.setName("OPTTRT");

          //---- OBJ_28_OBJ_28 ----
          OBJ_28_OBJ_28.setText("Dernier num\u00e9ro de passage en comptabilit\u00e9");
          OBJ_28_OBJ_28.setFont(OBJ_28_OBJ_28.getFont().deriveFont(OBJ_28_OBJ_28.getFont().getStyle() | Font.BOLD, OBJ_28_OBJ_28.getFont().getSize() + 3f));
          OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");

          //---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("@WNUPAS@");
          OBJ_49_OBJ_49.setFont(OBJ_49_OBJ_49.getFont().deriveFont(OBJ_49_OBJ_49.getFont().getStyle() | Font.BOLD, OBJ_49_OBJ_49.getFont().getSize() + 3f));
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");

          //---- OBJ_28_OBJ_29 ----
          OBJ_28_OBJ_29.setText("Fichier de comptabilisation d\u00e9tect\u00e9 avec le num\u00e9ro de passage");
          OBJ_28_OBJ_29.setFont(OBJ_28_OBJ_29.getFont().deriveFont(OBJ_28_OBJ_29.getFont().getStyle() | Font.BOLD, OBJ_28_OBJ_29.getFont().getSize() + 3f));
          OBJ_28_OBJ_29.setName("OBJ_28_OBJ_29");

          //---- separator1 ----
          separator1.setName("separator1");

          //---- OBJ_49_OBJ_50 ----
          OBJ_49_OBJ_50.setText("@WPAS@");
          OBJ_49_OBJ_50.setFont(OBJ_49_OBJ_50.getFont().deriveFont(OBJ_49_OBJ_50.getFont().getStyle() | Font.BOLD, OBJ_49_OBJ_50.getFont().getSize() + 3f));
          OBJ_49_OBJ_50.setName("OBJ_49_OBJ_50");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 700, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_28_OBJ_28, GroupLayout.PREFERRED_SIZE, 460, GroupLayout.PREFERRED_SIZE)
                .addGap(70, 70, 70)
                .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_28_OBJ_29, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_49_OBJ_50, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 700, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OPTTRT_NON, GroupLayout.PREFERRED_SIZE, 306, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OPTTRT, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_28_OBJ_28, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_49_OBJ_49, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_28_OBJ_29, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_49_OBJ_50, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(OPTTRT_NON, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OPTTRT, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //---- OPTTRT_GRP ----
    OPTTRT_GRP.add(OPTTRT_NON);
    OPTTRT_GRP.add(OPTTRT);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator xTitledSeparator1;
  private XRiRadioButton OPTTRT_NON;
  private XRiRadioButton OPTTRT;
  private JLabel OBJ_28_OBJ_28;
  private RiZoneSortie OBJ_49_OBJ_49;
  private JLabel OBJ_28_OBJ_29;
  private JSeparator separator1;
  private RiZoneSortie OBJ_49_OBJ_50;
  private ButtonGroup OPTTRT_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
