
package ri.serien.libecranrpg.sgem.SGEM63FM;
// Nom Fichier: p_SGEM40FM_FMTA0_FMTF1_93.java

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGEM63FM_A0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGEM63FM_A0(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    // setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    OBJ_43.setIcon(lexique.chargerImage("images/msgbox03.gif", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgem61"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_39 = new JPanel();
    OBJ_42_OBJ_42 = new JLabel();
    OBJ_40_OBJ_40 = new JLabel();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setName("this");

    //======== OBJ_39 ========
    {
      OBJ_39.setName("OBJ_39");
      OBJ_39.setLayout(null);

      //---- OBJ_42_OBJ_42 ----
      OBJ_42_OBJ_42.setText("Il faut le d\u00e9truire et relancer le traitement");
      OBJ_42_OBJ_42.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_42_OBJ_42.setFont(OBJ_42_OBJ_42.getFont().deriveFont(OBJ_42_OBJ_42.getFont().getSize() + 3f));
      OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
      OBJ_39.add(OBJ_42_OBJ_42);
      OBJ_42_OBJ_42.setBounds(87, 50, 373, 20);

      //---- OBJ_40_OBJ_40 ----
      OBJ_40_OBJ_40.setText("Un fichier de comptabilisation existe sur disque.");
      OBJ_40_OBJ_40.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_40_OBJ_40.setFont(OBJ_40_OBJ_40.getFont().deriveFont(OBJ_40_OBJ_40.getFont().getSize() + 3f));
      OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
      OBJ_39.add(OBJ_40_OBJ_40);
      OBJ_40_OBJ_40.setBounds(87, 10, 373, 20);

      //---- OBJ_41_OBJ_41 ----
      OBJ_41_OBJ_41.setText("Ce fichier n'est pas utilisable.");
      OBJ_41_OBJ_41.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_41_OBJ_41.setFont(OBJ_41_OBJ_41.getFont().deriveFont(OBJ_41_OBJ_41.getFont().getSize() + 3f));
      OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
      OBJ_39.add(OBJ_41_OBJ_41);
      OBJ_41_OBJ_41.setBounds(87, 30, 373, 20);

      //---- OBJ_43 ----
      OBJ_43.setIcon(new ImageIcon("images/msgbox03.gif"));
      OBJ_43.setName("OBJ_43");
      OBJ_39.add(OBJ_43);
      OBJ_43.setBounds(5, 10, 52, 52);
    }

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addContainerGap()
          .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 475, GroupLayout.PREFERRED_SIZE)
          .addContainerGap(23, Short.MAX_VALUE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addContainerGap()
          .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
          .addContainerGap(16, Short.MAX_VALUE))
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
      OBJ_4.addSeparator();

      //---- OBJ_9 ----
      OBJ_9.setText("Exploitation");
      OBJ_9.setName("OBJ_9");
      OBJ_4.add(OBJ_9);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel OBJ_39;
  private JLabel OBJ_42_OBJ_42;
  private JLabel OBJ_40_OBJ_40;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_43;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
