
package ri.serien.libecranrpg.sgem.SGEM61FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGEM61FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGEM61FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    EDTERR.setValeursSelection("OUI", "NON");
    EDTHLD.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_50_OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN001@")).trim());
    OBJ_51_OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGCPLX@")).trim());
    OBJ_52_OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS001@")).trim());
    OBJ_25_OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EXER@")).trim());
    OBJ_26_OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE001@")).trim());
    OBJ_55_OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNUPAS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_55_OBJ_55.setVisible(lexique.isPresent("WNUPAS"));
    // DATLIM.setEnabled( lexique.isPresent("DATLIM"));
    OBJ_52_OBJ_52.setVisible(lexique.isPresent("ETS001"));
    OBJ_26_OBJ_26.setVisible(lexique.isPresent("ETE001"));
    // EDTHLD.setEnabled( lexique.isPresent("EDTHLD"));
    // EDTHLD.setSelected(lexique.HostFieldGetData("EDTHLD").equalsIgnoreCase("OUI"));
    // EDTERR.setEnabled( lexique.isPresent("EDTERR"));
    // EDTERR.setSelected(lexique.HostFieldGetData("EDTERR").equalsIgnoreCase("OUI"));
    OBJ_51_OBJ_51.setVisible(lexique.isPresent("DGCPLX"));
    OBJ_50_OBJ_50.setVisible(lexique.isPresent("ETN001"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (EDTHLD.isSelected())
    // lexique.HostFieldPutData("EDTHLD", 0, "OUI");
    // else
    // lexique.HostFieldPutData("EDTHLD", 0, "NON");
    // if (EDTERR.isSelected())
    // lexique.HostFieldPutData("EDTERR", 0, "OUI");
    // else
    // lexique.HostFieldPutData("EDTERR", 0, "NON");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_50_OBJ_50 = new RiZoneSortie();
    OBJ_51_OBJ_51 = new RiZoneSortie();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_48_OBJ_48 = new JLabel();
    OBJ_52_OBJ_52 = new RiZoneSortie();
    OBJ_25_OBJ_25 = new JLabel();
    OBJ_26_OBJ_26 = new RiZoneSortie();
    EDTHLD = new XRiCheckBox();
    EDTERR = new XRiCheckBox();
    OBJ_56_OBJ_56 = new JLabel();
    OBJ_54_OBJ_54 = new JLabel();
    OBJ_55_OBJ_55 = new RiZoneSortie();
    DATLIM = new XRiCalendrier();
    xTitledSeparator1 = new JXTitledSeparator();
    separator1 = new JSeparator();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(740, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- OBJ_50_OBJ_50 ----
          OBJ_50_OBJ_50.setText("@ETN001@");
          OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
          p_contenu.add(OBJ_50_OBJ_50);
          OBJ_50_OBJ_50.setBounds(290, 85, 345, OBJ_50_OBJ_50.getPreferredSize().height);

          //---- OBJ_51_OBJ_51 ----
          OBJ_51_OBJ_51.setText("@DGCPLX@");
          OBJ_51_OBJ_51.setName("OBJ_51_OBJ_51");
          p_contenu.add(OBJ_51_OBJ_51);
          OBJ_51_OBJ_51.setBounds(290, 115, 345, OBJ_51_OBJ_51.getPreferredSize().height);

          //---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("Nom ou raison sociale");
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
          p_contenu.add(OBJ_49_OBJ_49);
          OBJ_49_OBJ_49.setBounds(45, 87, 163, 20);

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("Etablissement");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
          p_contenu.add(OBJ_48_OBJ_48);
          OBJ_48_OBJ_48.setBounds(45, 57, 120, 20);

          //---- OBJ_52_OBJ_52 ----
          OBJ_52_OBJ_52.setText("@ETS001@");
          OBJ_52_OBJ_52.setName("OBJ_52_OBJ_52");
          p_contenu.add(OBJ_52_OBJ_52);
          OBJ_52_OBJ_52.setBounds(290, 55, 40, OBJ_52_OBJ_52.getPreferredSize().height);

          //---- OBJ_25_OBJ_25 ----
          OBJ_25_OBJ_25.setText("@EXER@");
          OBJ_25_OBJ_25.setName("OBJ_25_OBJ_25");
          p_contenu.add(OBJ_25_OBJ_25);
          OBJ_25_OBJ_25.setBounds(45, 187, 195, 20);

          //---- OBJ_26_OBJ_26 ----
          OBJ_26_OBJ_26.setText("@ETE001@");
          OBJ_26_OBJ_26.setName("OBJ_26_OBJ_26");
          p_contenu.add(OBJ_26_OBJ_26);
          OBJ_26_OBJ_26.setBounds(290, 185, 127, OBJ_26_OBJ_26.getPreferredSize().height);

          //---- EDTHLD ----
          EDTHLD.setText("Suspension \u00e9dition");
          EDTHLD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTHLD.setName("EDTHLD");
          p_contenu.add(EDTHLD);
          EDTHLD.setBounds(45, 220, 154, 20);

          //---- EDTERR ----
          EDTERR.setText("Edition erreurs uniquement");
          EDTERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTERR.setName("EDTERR");
          p_contenu.add(EDTERR);
          EDTERR.setBounds(45, 250, 195, 20);

          //---- OBJ_56_OBJ_56 ----
          OBJ_56_OBJ_56.setText("Date limite de comptabilisation");
          OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
          p_contenu.add(OBJ_56_OBJ_56);
          OBJ_56_OBJ_56.setBounds(45, 285, 205, 20);

          //---- OBJ_54_OBJ_54 ----
          OBJ_54_OBJ_54.setText("Dernier num\u00e9ro de passage en compta");
          OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
          p_contenu.add(OBJ_54_OBJ_54);
          OBJ_54_OBJ_54.setBounds(45, 317, 240, 20);

          //---- OBJ_55_OBJ_55 ----
          OBJ_55_OBJ_55.setText("@WNUPAS@");
          OBJ_55_OBJ_55.setName("OBJ_55_OBJ_55");
          p_contenu.add(OBJ_55_OBJ_55);
          OBJ_55_OBJ_55.setBounds(290, 315, 45, OBJ_55_OBJ_55.getPreferredSize().height);

          //---- DATLIM ----
          DATLIM.setName("DATLIM");
          p_contenu.add(DATLIM);
          DATLIM.setBounds(290, 281, 105, DATLIM.getPreferredSize().height);

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Etablissement");
          xTitledSeparator1.setName("xTitledSeparator1");
          p_contenu.add(xTitledSeparator1);
          xTitledSeparator1.setBounds(25, 25, 680, 14);

          //---- separator1 ----
          separator1.setName("separator1");
          p_contenu.add(separator1);
          separator1.setBounds(25, 160, 680, 12);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private RiZoneSortie OBJ_50_OBJ_50;
  private RiZoneSortie OBJ_51_OBJ_51;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_48_OBJ_48;
  private RiZoneSortie OBJ_52_OBJ_52;
  private JLabel OBJ_25_OBJ_25;
  private RiZoneSortie OBJ_26_OBJ_26;
  private XRiCheckBox EDTHLD;
  private XRiCheckBox EDTERR;
  private JLabel OBJ_56_OBJ_56;
  private JLabel OBJ_54_OBJ_54;
  private RiZoneSortie OBJ_55_OBJ_55;
  private XRiCalendrier DATLIM;
  private JXTitledSeparator xTitledSeparator1;
  private JSeparator separator1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
