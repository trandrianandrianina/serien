
package ri.serien.libecranrpg.sgem.SGEM55FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGEM55FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGEM55FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_32_OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_32_OBJ_32.setVisible(lexique.isPresent("UCLEX"));
    DORIB.setEnabled(lexique.isPresent("DORIB"));
    P55VDE.setEnabled(lexique.isPresent("P55VDE"));
    P55ETB.setEnabled(lexique.isPresent("P55ETB"));
    DOGUI.setEnabled(lexique.isPresent("DOGUI"));
    DOBQE.setEnabled(lexique.isPresent("DOBQE"));
    P55NUM.setEnabled(lexique.isPresent("P55NUM"));
    P55MTT.setEnabled(lexique.isPresent("P55MTT"));
    DOCPT.setEnabled(lexique.isPresent("DOCPT"));
    DODO2.setEnabled(lexique.isPresent("DODO2"));
    DODO1.setEnabled(lexique.isPresent("DODO1"));
    DORIE.setEnabled(lexique.isPresent("DORIE"));
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Génération des règlements"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    DORIE = new XRiTextField();
    OBJ_30_OBJ_30 = new JLabel();
    DODO1 = new XRiTextField();
    DODO2 = new XRiTextField();
    DOCPT = new XRiTextField();
    P55MTT = new XRiTextField();
    OBJ_15_OBJ_15 = new JLabel();
    OBJ_19_OBJ_19 = new JLabel();
    OBJ_33_OBJ_33 = new JLabel();
    OBJ_31_OBJ_31 = new JLabel();
    P55NUM = new XRiTextField();
    DOBQE = new XRiTextField();
    DOGUI = new XRiTextField();
    P55ETB = new XRiTextField();
    P55VDE = new XRiTextField();
    DORIB = new XRiTextField();
    OBJ_32_OBJ_32 = new RiZoneSortie();
    xTitledSeparator1 = new JXTitledSeparator();
    BTD = new JPopupMenu();
    OBJ_8 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(795, 215));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- DORIE ----
          DORIE.setComponentPopupMenu(BTD);
          DORIE.setName("DORIE");
          p_recup.add(DORIE);
          DORIE.setBounds(290, 150, 310, DORIE.getPreferredSize().height);

          //---- OBJ_30_OBJ_30 ----
          OBJ_30_OBJ_30.setText("N\u00b0Bqe    N\u00b0Gui      N\u00b0de Compte         Cl\u00e9");
          OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");
          p_recup.add(OBJ_30_OBJ_30);
          OBJ_30_OBJ_30.setBounds(295, 95, 231, 20);

          //---- DODO1 ----
          DODO1.setComponentPopupMenu(BTD);
          DODO1.setName("DODO1");
          p_recup.add(DODO1);
          DODO1.setBounds(100, 120, 172, DODO1.getPreferredSize().height);

          //---- DODO2 ----
          DODO2.setComponentPopupMenu(BTD);
          DODO2.setName("DODO2");
          p_recup.add(DODO2);
          DODO2.setBounds(100, 150, 172, DODO2.getPreferredSize().height);

          //---- DOCPT ----
          DOCPT.setComponentPopupMenu(BTD);
          DOCPT.setName("DOCPT");
          p_recup.add(DOCPT);
          DOCPT.setBounds(390, 120, 100, DOCPT.getPreferredSize().height);

          //---- P55MTT ----
          P55MTT.setName("P55MTT");
          p_recup.add(P55MTT);
          P55MTT.setBounds(200, 28, 110, P55MTT.getPreferredSize().height);

          //---- OBJ_15_OBJ_15 ----
          OBJ_15_OBJ_15.setText("Commande");
          OBJ_15_OBJ_15.setName("OBJ_15_OBJ_15");
          p_recup.add(OBJ_15_OBJ_15);
          OBJ_15_OBJ_15.setBounds(22, 32, 83, 20);

          //---- OBJ_19_OBJ_19 ----
          OBJ_19_OBJ_19.setText("Vendeur");
          OBJ_19_OBJ_19.setName("OBJ_19_OBJ_19");
          p_recup.add(OBJ_19_OBJ_19);
          OBJ_19_OBJ_19.setBounds(325, 32, 60, 20);

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("Banque");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
          p_recup.add(OBJ_33_OBJ_33);
          OBJ_33_OBJ_33.setBounds(22, 124, 83, 20);

          //---- OBJ_31_OBJ_31 ----
          OBJ_31_OBJ_31.setText("Guichet");
          OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");
          p_recup.add(OBJ_31_OBJ_31);
          OBJ_31_OBJ_31.setBounds(22, 154, 83, 20);

          //---- P55NUM ----
          P55NUM.setName("P55NUM");
          p_recup.add(P55NUM);
          P55NUM.setBounds(140, 28, 60, P55NUM.getPreferredSize().height);

          //---- DOBQE ----
          DOBQE.setComponentPopupMenu(BTD);
          DOBQE.setName("DOBQE");
          p_recup.add(DOBQE);
          DOBQE.setBounds(290, 120, 52, DOBQE.getPreferredSize().height);

          //---- DOGUI ----
          DOGUI.setComponentPopupMenu(BTD);
          DOGUI.setName("DOGUI");
          p_recup.add(DOGUI);
          DOGUI.setBounds(340, 120, 52, DOGUI.getPreferredSize().height);

          //---- P55ETB ----
          P55ETB.setName("P55ETB");
          p_recup.add(P55ETB);
          P55ETB.setBounds(100, 28, 40, P55ETB.getPreferredSize().height);

          //---- P55VDE ----
          P55VDE.setComponentPopupMenu(BTD);
          P55VDE.setName("P55VDE");
          p_recup.add(P55VDE);
          P55VDE.setBounds(385, 28, 40, P55VDE.getPreferredSize().height);

          //---- DORIB ----
          DORIB.setComponentPopupMenu(BTD);
          DORIB.setName("DORIB");
          p_recup.add(DORIB);
          DORIB.setBounds(490, 120, 30, DORIB.getPreferredSize().height);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("@UCLEX@");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          p_recup.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(525, 122, 31, OBJ_32_OBJ_32.getPreferredSize().height);

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Domiciliation");
          xTitledSeparator1.setName("xTitledSeparator1");
          p_recup.add(xTitledSeparator1);
          xTitledSeparator1.setBounds(15, 70, 590, xTitledSeparator1.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.WEST);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_8 ----
      OBJ_8.setText("Aide en ligne");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      BTD.add(OBJ_8);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiTextField DORIE;
  private JLabel OBJ_30_OBJ_30;
  private XRiTextField DODO1;
  private XRiTextField DODO2;
  private XRiTextField DOCPT;
  private XRiTextField P55MTT;
  private JLabel OBJ_15_OBJ_15;
  private JLabel OBJ_19_OBJ_19;
  private JLabel OBJ_33_OBJ_33;
  private JLabel OBJ_31_OBJ_31;
  private XRiTextField P55NUM;
  private XRiTextField DOBQE;
  private XRiTextField DOGUI;
  private XRiTextField P55ETB;
  private XRiTextField P55VDE;
  private XRiTextField DORIB;
  private RiZoneSortie OBJ_32_OBJ_32;
  private JXTitledSeparator xTitledSeparator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_8;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
