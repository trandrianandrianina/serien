
package ri.serien.libecranrpg.sgem.SGEM60FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGEM60FM_A0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGEM60FM_A0(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_contenu = new JPanel();
    OBJ_40_OBJ_40 = new JLabel();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_42_OBJ_42 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(455, 130));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_contenu ========
    {
      p_contenu.setPreferredSize(new Dimension(900, 580));
      p_contenu.setBorder(new LineBorder(Color.darkGray));
      p_contenu.setBackground(new Color(239, 239, 222));
      p_contenu.setName("p_contenu");
      p_contenu.setLayout(null);

      //---- OBJ_40_OBJ_40 ----
      OBJ_40_OBJ_40.setText("Un fichier de comptabilisation existe d\u00e9j\u00e0 sur disque.");
      OBJ_40_OBJ_40.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_40_OBJ_40.setFont(OBJ_40_OBJ_40.getFont().deriveFont(OBJ_40_OBJ_40.getFont().getSize() + 3f));
      OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
      p_contenu.add(OBJ_40_OBJ_40);
      OBJ_40_OBJ_40.setBounds(0, 25, 455, 20);

      //---- OBJ_41_OBJ_41 ----
      OBJ_41_OBJ_41.setText("Ce fichier n'est pas utilisable,");
      OBJ_41_OBJ_41.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_41_OBJ_41.setFont(OBJ_41_OBJ_41.getFont().deriveFont(OBJ_41_OBJ_41.getFont().getSize() + 3f));
      OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
      p_contenu.add(OBJ_41_OBJ_41);
      OBJ_41_OBJ_41.setBounds(0, 50, 455, 20);

      //---- OBJ_42_OBJ_42 ----
      OBJ_42_OBJ_42.setText("il faut le d\u00e9truire et relancer le traitement.");
      OBJ_42_OBJ_42.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_42_OBJ_42.setFont(OBJ_42_OBJ_42.getFont().deriveFont(OBJ_42_OBJ_42.getFont().getSize() + 3f));
      OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
      p_contenu.add(OBJ_42_OBJ_42);
      OBJ_42_OBJ_42.setBounds(0, 75, 455, 20);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < p_contenu.getComponentCount(); i++) {
          Rectangle bounds = p_contenu.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_contenu.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_contenu.setMinimumSize(preferredSize);
        p_contenu.setPreferredSize(preferredSize);
      }
    }
    add(p_contenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_contenu;
  private JLabel OBJ_40_OBJ_40;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_42_OBJ_42;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
