
package ri.serien.libecranrpg.sgem.SGEM40FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;

/**
 * @author Stéphane Vénéri
 */
public class SGEM40FM_A0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGEM40FM_A0(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    OBJ_39 = new JPanel();
    OBJ_42_OBJ_42 = new JLabel();
    OBJ_40_OBJ_40 = new JLabel();
    OBJ_41_OBJ_41 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(685, 115));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== OBJ_39 ========
        {
          OBJ_39.setOpaque(false);
          OBJ_39.setName("OBJ_39");

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("Il faut le d\u00e9truire et relancer le traitement");
          OBJ_42_OBJ_42.setFont(OBJ_42_OBJ_42.getFont().deriveFont(OBJ_42_OBJ_42.getFont().getStyle() | Font.BOLD, OBJ_42_OBJ_42.getFont().getSize() + 3f));
          OBJ_42_OBJ_42.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("Un fichier de comptabilisation existe d\u00e9j\u00e0 sur disque.");
          OBJ_40_OBJ_40.setFont(OBJ_40_OBJ_40.getFont().deriveFont(OBJ_40_OBJ_40.getFont().getStyle() | Font.BOLD, OBJ_40_OBJ_40.getFont().getSize() + 3f));
          OBJ_40_OBJ_40.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

          //---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("Ce fichier n'est pas utilisable.");
          OBJ_41_OBJ_41.setFont(OBJ_41_OBJ_41.getFont().deriveFont(OBJ_41_OBJ_41.getFont().getStyle() | Font.BOLD, OBJ_41_OBJ_41.getFont().getSize() + 3f));
          OBJ_41_OBJ_41.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");

          GroupLayout OBJ_39Layout = new GroupLayout(OBJ_39);
          OBJ_39.setLayout(OBJ_39Layout);
          OBJ_39Layout.setHorizontalGroup(
            OBJ_39Layout.createParallelGroup()
              .addGroup(OBJ_39Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(OBJ_39Layout.createParallelGroup()
                  .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_41_OBJ_41, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE)))
          );
          OBJ_39Layout.setVerticalGroup(
            OBJ_39Layout.createParallelGroup()
              .addGroup(OBJ_39Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_40_OBJ_40)
                .addGap(12, 12, 12)
                .addComponent(OBJ_41_OBJ_41)
                .addGap(12, 12, 12)
                .addComponent(OBJ_42_OBJ_42))
          );
        }
        p_contenu.add(OBJ_39);
        OBJ_39.setBounds(5, 5, 504, 105);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel OBJ_39;
  private JLabel OBJ_42_OBJ_42;
  private JLabel OBJ_40_OBJ_40;
  private JLabel OBJ_41_OBJ_41;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
