
package ri.serien.libecranrpg.sgem.SGEM61FM;
// Nom Fichier: p_SGEM40FM_FMTA0_FMTF1_93.java

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SGEM61FM_A0 extends SNPanelEcranRPG implements ioFrame {
  
  public SGEM61FM_A0(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    
    OBJ_43.setIcon(lexique.chargerImage("images/msgbox03.gif", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_39 = new JPanel();
    OBJ_42_OBJ_42 = new JLabel();
    OBJ_40_OBJ_40 = new JLabel();
    OBJ_41_OBJ_41 = new JLabel();
    OBJ_43 = new JLabel();
    
    // ======== this ========
    setName("this");
    
    // ======== OBJ_39 ========
    {
      OBJ_39.setName("OBJ_39");
      OBJ_39.setLayout(null);
      
      // ---- OBJ_42_OBJ_42 ----
      OBJ_42_OBJ_42.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_42_OBJ_42.setFont(OBJ_42_OBJ_42.getFont().deriveFont(OBJ_42_OBJ_42.getFont().getSize() + 3f));
      OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");
      OBJ_39.add(OBJ_42_OBJ_42);
      OBJ_42_OBJ_42.setBounds(87, 50, 373, 20);
      
      // ---- OBJ_40_OBJ_40 ----
      OBJ_40_OBJ_40.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_40_OBJ_40.setFont(OBJ_40_OBJ_40.getFont().deriveFont(OBJ_40_OBJ_40.getFont().getSize() + 3f));
      OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
      OBJ_39.add(OBJ_40_OBJ_40);
      OBJ_40_OBJ_40.setBounds(87, 10, 373, 20);
      
      // ---- OBJ_41_OBJ_41 ----
      OBJ_41_OBJ_41.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_41_OBJ_41.setFont(OBJ_41_OBJ_41.getFont().deriveFont(OBJ_41_OBJ_41.getFont().getSize() + 3f));
      OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
      OBJ_39.add(OBJ_41_OBJ_41);
      OBJ_41_OBJ_41.setBounds(87, 30, 373, 20);
      
      // ---- OBJ_43 ----
      OBJ_43.setIcon(new ImageIcon("images/msgbox03.gif"));
      OBJ_43.setName("OBJ_43");
      OBJ_39.add(OBJ_43);
      OBJ_43.setBounds(5, 10, 52, 52);
    }
    
    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(layout.createParallelGroup().addGroup(layout.createSequentialGroup().addContainerGap()
        .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 475, GroupLayout.PREFERRED_SIZE).addContainerGap(29, Short.MAX_VALUE)));
    layout.setVerticalGroup(layout.createParallelGroup().addGroup(layout.createSequentialGroup().addContainerGap()
        .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE).addContainerGap(23, Short.MAX_VALUE)));
    
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel OBJ_39;
  private JLabel OBJ_42_OBJ_42;
  private JLabel OBJ_40_OBJ_40;
  private JLabel OBJ_41_OBJ_41;
  private JLabel OBJ_43;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
