
package ri.serien.libecranrpg.sgem.SGEM43FM;
// Nom Fichier: pop_SGEM43FM_FMTP0_184.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JButton;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGEM43FM_P0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGEM43FM_P0(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_5);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    FOLIO = new XRiTextField();
    LIGNE = new XRiTextField();
    OBJ_5 = new JButton();
    xTitledSeparator1 = new JXTitledSeparator();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- FOLIO ----
    FOLIO.setName("FOLIO");
    add(FOLIO);
    FOLIO.setBounds(10, 30, 360, FOLIO.getPreferredSize().height);

    //---- LIGNE ----
    LIGNE.setName("LIGNE");
    add(LIGNE);
    LIGNE.setBounds(10, 68, 360, LIGNE.getPreferredSize().height);

    //---- OBJ_5 ----
    OBJ_5.setText("");
    OBJ_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_5.setName("OBJ_5");
    add(OBJ_5);
    OBJ_5.setBounds(310, 100, 54, 44);

    //---- xTitledSeparator1 ----
    xTitledSeparator1.setTitle("Traitement en cours");
    xTitledSeparator1.setName("xTitledSeparator1");
    add(xTitledSeparator1);
    xTitledSeparator1.setBounds(10, 10, 360, xTitledSeparator1.getPreferredSize().height);

    setPreferredSize(new Dimension(385, 150));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private XRiTextField FOLIO;
  private XRiTextField LIGNE;
  private JButton OBJ_5;
  private JXTitledSeparator xTitledSeparator1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
