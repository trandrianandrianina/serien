
package ri.serien.libecranrpg.vtim.VTIM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VTIM13FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VTIM13FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    CHNC2X.setValeursSelection("OUI", "NON");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WTAU.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTAU@")).trim());
    CHMFT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CHMFT@")).trim());
    WFTTC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WFTTC@")).trim());
    CHBRE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EFBRE@")).trim());
    CHSRE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EFSRE@")).trim());
    CHAD1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CHAD1@")).trim());
    CHAD2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CHAD2@")).trim());
    CHAD3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CHAD3@")).trim());
    UCLEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    UCLEX.setVisible((!lexique.HostFieldGetData("UCLEX").trim().equals("")) && (lexique.isTrue("19")));
    label18.setVisible(lexique.isPresent("CHETB2"));
    panel2.setVisible(lexique.isTrue("95"));
    if (lexique.isTrue("94")) {
      p_bpresentation.setText("Gestion des cartes impayées");
    }
    else {
      p_bpresentation.setText("Gestion des TIP impayés");
    }
    
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    INDETB = new XRiTextField();
    OBJ_57 = new JLabel();
    OBJ_67 = new JLabel();
    INDNUM = new XRiTextField();
    INDSUF = new XRiTextField();
    DRGX = new XRiCalendrier();
    OBJ_69 = new JLabel();
    WVDE = new XRiTextField();
    OBJ_70 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    CHMTT = new XRiTextField();
    label2 = new JLabel();
    CHDEMX = new XRiCalendrier();
    separator1 = compFactory.createSeparator("Frais/imputation");
    label4 = new JLabel();
    EFMFH = new XRiTextField();
    label5 = new JLabel();
    EFIN2 = new XRiTextField();
    label6 = new JLabel();
    WTAU = new RiZoneSortie();
    label7 = new JLabel();
    CHMFT = new RiZoneSortie();
    WFTTC = new RiZoneSortie();
    label8 = new JLabel();
    panel4 = new JPanel();
    label10 = new JLabel();
    CHDREX = new XRiCalendrier();
    label11 = new JLabel();
    label13 = new JLabel();
    CHBRE = new RiZoneSortie();
    CHSRE = new RiZoneSortie();
    CHNC2X = new XRiCheckBox();
    label9 = new JLabel();
    CHCAN = new XRiTextField();
    label12 = new JLabel();
    CHTCA = new XRiTextField();
    label19 = new JLabel();
    CHTCA2 = new XRiTextField();
    CHTAUT = new XRiTextField();
    CHTCA3 = new XRiTextField();
    CHTCA4 = new XRiTextField();
    label20 = new JLabel();
    label21 = new JLabel();
    label22 = new JLabel();
    label23 = new JLabel();
    label24 = new JLabel();
    CHCL2 = new XRiTextField();
    label3 = new JLabel();
    CHMOV = new XRiTextField();
    CHANVR = new XRiTextField();
    panel3 = new JPanel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label18 = new JLabel();
    CHNCG = new XRiTextField();
    CHCLA = new XRiTextField();
    CHNOM = new XRiTextField();
    CHAD1 = new RiZoneSortie();
    CHAD2 = new RiZoneSortie();
    CHAD3 = new RiZoneSortie();
    CHETB2 = new XRiTextField();
    CHNCA = new XRiTextField();
    panel2 = new JPanel();
    CHCBQ = new XRiTextField();
    CHCGU = new XRiTextField();
    CHNCB = new XRiTextField();
    CHRIB = new XRiTextField();
    UCLEX = new RiZoneSortie();
    OBJ_22_OBJ_25 = new JLabel();
    OBJ_22_OBJ_26 = new JLabel();
    OBJ_22_OBJ_27 = new JLabel();
    OBJ_22_OBJ_28 = new JLabel();
    OBJ_22_OBJ_24 = new JLabel();
    CHDOM = new XRiTextField();
    OBJ_22_OBJ_22 = new JLabel();
    CHRIE = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des cartes impay\u00e9es");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(120, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_57 ----
          OBJ_57.setText("Etablissement");
          OBJ_57.setName("OBJ_57");
          p_tete_gauche.add(OBJ_57);
          OBJ_57.setBounds(5, 0, 90, 28);

          //---- OBJ_67 ----
          OBJ_67.setText("Num\u00e9ro d'impay\u00e9");
          OBJ_67.setName("OBJ_67");
          p_tete_gauche.add(OBJ_67);
          OBJ_67.setBounds(185, 0, 115, 28);

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");
          p_tete_gauche.add(INDNUM);
          INDNUM.setBounds(300, 0, 68, INDNUM.getPreferredSize().height);

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(BTD);
          INDSUF.setName("INDSUF");
          p_tete_gauche.add(INDSUF);
          INDSUF.setBounds(370, 0, 28, INDSUF.getPreferredSize().height);

          //---- DRGX ----
          DRGX.setName("DRGX");
          p_tete_gauche.add(DRGX);
          DRGX.setBounds(535, 0, 105, DRGX.getPreferredSize().height);

          //---- OBJ_69 ----
          OBJ_69.setText("Date op\u00e9ration");
          OBJ_69.setName("OBJ_69");
          p_tete_gauche.add(OBJ_69);
          OBJ_69.setBounds(430, 0, 105, 28);

          //---- WVDE ----
          WVDE.setComponentPopupMenu(BTD);
          WVDE.setName("WVDE");
          p_tete_gauche.add(WVDE);
          WVDE.setBounds(740, 0, 40, WVDE.getPreferredSize().height);

          //---- OBJ_70 ----
          OBJ_70.setText("Vendeur");
          OBJ_70.setName("OBJ_70");
          p_tete_gauche.add(OBJ_70);
          OBJ_70.setBounds(660, 0, 85, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(870, 620));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(870, 620));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Effet"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- label1 ----
            label1.setText("Montant");
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(25, 35, 65, 28);

            //---- CHMTT ----
            CHMTT.setComponentPopupMenu(BTD);
            CHMTT.setName("CHMTT");
            panel1.add(CHMTT);
            CHMTT.setBounds(90, 35, 130, CHMTT.getPreferredSize().height);

            //---- label2 ----
            label2.setText("Emission");
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(255, 35, 65, 28);

            //---- CHDEMX ----
            CHDEMX.setEnabled(false);
            CHDEMX.setName("CHDEMX");
            panel1.add(CHDEMX);
            CHDEMX.setBounds(320, 35, 105, CHDEMX.getPreferredSize().height);

            //---- separator1 ----
            separator1.setName("separator1");
            panel1.add(separator1);
            separator1.setBounds(25, 80, 795, separator1.getPreferredSize().height);

            //---- label4 ----
            label4.setText("Hors taxes");
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(70, 100, 70, 28);

            //---- EFMFH ----
            EFMFH.setComponentPopupMenu(BTD);
            EFMFH.setName("EFMFH");
            panel1.add(EFMFH);
            EFMFH.setBounds(140, 100, 80, EFMFH.getPreferredSize().height);

            //---- label5 ----
            label5.setText("R");
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(255, 100, 20, 28);

            //---- EFIN2 ----
            EFIN2.setComponentPopupMenu(BTD);
            EFIN2.setName("EFIN2");
            panel1.add(EFIN2);
            EFIN2.setBounds(275, 100, 24, EFIN2.getPreferredSize().height);

            //---- label6 ----
            label6.setText("Taux de TVA");
            label6.setName("label6");
            panel1.add(label6);
            label6.setBounds(320, 100, 75, 28);

            //---- WTAU ----
            WTAU.setText("@WTAU@");
            WTAU.setHorizontalAlignment(SwingConstants.RIGHT);
            WTAU.setName("WTAU");
            panel1.add(WTAU);
            WTAU.setBounds(400, 100, 50, WTAU.getPreferredSize().height);

            //---- label7 ----
            label7.setText("Montant de la TVA");
            label7.setName("label7");
            panel1.add(label7);
            label7.setBounds(465, 100, 110, 28);

            //---- CHMFT ----
            CHMFT.setText("@CHMFT@");
            CHMFT.setHorizontalAlignment(SwingConstants.RIGHT);
            CHMFT.setName("CHMFT");
            panel1.add(CHMFT);
            CHMFT.setBounds(575, 100, 80, CHMFT.getPreferredSize().height);

            //---- WFTTC ----
            WFTTC.setText("@WFTTC@");
            WFTTC.setHorizontalAlignment(SwingConstants.RIGHT);
            WFTTC.setName("WFTTC");
            panel1.add(WFTTC);
            WFTTC.setBounds(710, 100, 80, WFTTC.getPreferredSize().height);

            //---- label8 ----
            label8.setText("TTC");
            label8.setName("label8");
            panel1.add(label8);
            label8.setBounds(675, 100, 40, 28);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder(""));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- label10 ----
              label10.setText("Remise en banque");
              label10.setName("label10");
              panel4.add(label10);
              label10.setBounds(45, 10, 115, 28);

              //---- CHDREX ----
              CHDREX.setName("CHDREX");
              panel4.add(CHDREX);
              CHDREX.setBounds(180, 10, 105, CHDREX.getPreferredSize().height);

              //---- label11 ----
              label11.setText("Banque");
              label11.setName("label11");
              panel4.add(label11);
              label11.setBounds(295, 10, 60, 28);

              //---- label13 ----
              label13.setText("Suffixe");
              label13.setName("label13");
              panel4.add(label13);
              label13.setBounds(475, 10, 70, 28);

              //---- CHBRE ----
              CHBRE.setText("@EFBRE@");
              CHBRE.setName("CHBRE");
              panel4.add(CHBRE);
              CHBRE.setBounds(375, 10, 34, CHBRE.getPreferredSize().height);

              //---- CHSRE ----
              CHSRE.setText("@EFSRE@");
              CHSRE.setName("CHSRE");
              panel4.add(CHSRE);
              CHSRE.setBounds(550, 10, 34, CHSRE.getPreferredSize().height);

              //---- CHNC2X ----
              CHNC2X.setText("Comptabilis\u00e9");
              CHNC2X.setName("CHNC2X");
              panel4.add(CHNC2X);
              CHNC2X.setBounds(660, 10, 120, 25);
            }
            panel1.add(panel4);
            panel4.setBounds(25, 135, 795, 50);

            //---- label9 ----
            label9.setText("Num\u00e9ro de carte");
            label9.setName("label9");
            panel1.add(label9);
            label9.setBounds(25, 200, 115, 28);

            //---- CHCAN ----
            CHCAN.setComponentPopupMenu(BTD);
            CHCAN.setName("CHCAN");
            panel1.add(CHCAN);
            CHCAN.setBounds(140, 200, 200, CHCAN.getPreferredSize().height);

            //---- label12 ----
            label12.setText("Type");
            label12.setName("label12");
            panel1.add(label12);
            label12.setBounds(355, 200, 45, 28);

            //---- CHTCA ----
            CHTCA.setName("CHTCA");
            panel1.add(CHTCA);
            CHTCA.setBounds(400, 200, 24, CHTCA.getPreferredSize().height);

            //---- label19 ----
            label19.setText("Validation");
            label19.setName("label19");
            panel1.add(label19);
            label19.setBounds(500, 200, 65, 28);

            //---- CHTCA2 ----
            CHTCA2.setName("CHTCA2");
            panel1.add(CHTCA2);
            CHTCA2.setBounds(575, 200, 28, CHTCA2.getPreferredSize().height);

            //---- CHTAUT ----
            CHTAUT.setName("CHTAUT");
            panel1.add(CHTAUT);
            CHTAUT.setBounds(725, 200, 70, CHTAUT.getPreferredSize().height);

            //---- CHTCA3 ----
            CHTCA3.setName("CHTCA3");
            panel1.add(CHTCA3);
            CHTCA3.setBounds(575, 225, 28, CHTCA3.getPreferredSize().height);

            //---- CHTCA4 ----
            CHTCA4.setName("CHTCA4");
            panel1.add(CHTCA4);
            CHTCA4.setBounds(765, 225, 28, CHTCA4.getPreferredSize().height);

            //---- label20 ----
            label20.setText("Type de paiement");
            label20.setName("label20");
            panel1.add(label20);
            label20.setBounds(455, 225, 110, 28);

            //---- label21 ----
            label21.setText("Mode de saisie");
            label21.setName("label21");
            panel1.add(label21);
            label21.setBounds(665, 225, 100, 28);

            //---- label22 ----
            label22.setText("Mode");
            label22.setHorizontalAlignment(SwingConstants.CENTER);
            label22.setName("label22");
            panel1.add(label22);
            label22.setBounds(571, 178, 37, 28);

            //---- label23 ----
            label23.setText("Mois");
            label23.setHorizontalAlignment(SwingConstants.CENTER);
            label23.setName("label23");
            panel1.add(label23);
            label23.setBounds(610, 178, 50, 28);

            //---- label24 ----
            label24.setText("Autorisation");
            label24.setHorizontalAlignment(SwingConstants.CENTER);
            label24.setName("label24");
            panel1.add(label24);
            label24.setBounds(725, 178, 70, 28);

            //---- CHCL2 ----
            CHCL2.setComponentPopupMenu(BTD);
            CHCL2.setName("CHCL2");
            panel1.add(CHCL2);
            CHCL2.setBounds(660, 35, 160, CHCL2.getPreferredSize().height);

            //---- label3 ----
            label3.setText("Classement 2");
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(540, 35, 115, 28);

            //---- CHMOV ----
            CHMOV.setName("CHMOV");
            panel1.add(CHMOV);
            CHMOV.setBounds(615, 200, 28, CHMOV.getPreferredSize().height);

            //---- CHANVR ----
            CHANVR.setName("CHANVR");
            panel1.add(CHANVR);
            CHANVR.setBounds(645, 200, 28, CHANVR.getPreferredSize().height);
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Tir\u00e9"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- label14 ----
            label14.setText("Compte");
            label14.setName("label14");
            panel3.add(label14);
            label14.setBounds(70, 30, 110, 28);

            //---- label15 ----
            label15.setText("Classement");
            label15.setName("label15");
            panel3.add(label15);
            label15.setBounds(205, 30, 110, 28);

            //---- label16 ----
            label16.setText("Nom ou raison sociale");
            label16.setName("label16");
            panel3.add(label16);
            label16.setBounds(445, 30, 200, 28);

            //---- label18 ----
            label18.setText("Etablissement c\u00f4t\u00e9 ventes");
            label18.setName("label18");
            panel3.add(label18);
            label18.setBounds(70, 138, 155, 28);

            //---- CHNCG ----
            CHNCG.setComponentPopupMenu(BTD);
            CHNCG.setName("CHNCG");
            panel3.add(CHNCG);
            CHNCG.setBounds(70, 55, 60, CHNCG.getPreferredSize().height);

            //---- CHCLA ----
            CHCLA.setComponentPopupMenu(BTD);
            CHCLA.setName("CHCLA");
            panel3.add(CHCLA);
            CHCLA.setBounds(205, 55, 160, CHCLA.getPreferredSize().height);

            //---- CHNOM ----
            CHNOM.setComponentPopupMenu(BTD);
            CHNOM.setName("CHNOM");
            panel3.add(CHNOM);
            CHNOM.setBounds(445, 55, 310, CHNOM.getPreferredSize().height);

            //---- CHAD1 ----
            CHAD1.setText("@CHAD1@");
            CHAD1.setName("CHAD1");
            panel3.add(CHAD1);
            CHAD1.setBounds(445, 90, 310, CHAD1.getPreferredSize().height);

            //---- CHAD2 ----
            CHAD2.setText("@CHAD2@");
            CHAD2.setName("CHAD2");
            panel3.add(CHAD2);
            CHAD2.setBounds(445, 115, 310, CHAD2.getPreferredSize().height);

            //---- CHAD3 ----
            CHAD3.setText("@CHAD3@");
            CHAD3.setName("CHAD3");
            panel3.add(CHAD3);
            CHAD3.setBounds(445, 140, 310, CHAD3.getPreferredSize().height);

            //---- CHETB2 ----
            CHETB2.setComponentPopupMenu(BTD);
            CHETB2.setName("CHETB2");
            panel3.add(CHETB2);
            CHETB2.setBounds(325, 140, 40, CHETB2.getPreferredSize().height);

            //---- CHNCA ----
            CHNCA.setComponentPopupMenu(BTD);
            CHNCA.setName("CHNCA");
            panel3.add(CHNCA);
            CHNCA.setBounds(130, 55, 60, CHNCA.getPreferredSize().height);
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Domiciliation"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- CHCBQ ----
            CHCBQ.setToolTipText("num\u00e9ro de banque");
            CHCBQ.setComponentPopupMenu(BTD);
            CHCBQ.setName("CHCBQ");
            panel2.add(CHCBQ);
            CHCBQ.setBounds(445, 50, 50, CHCBQ.getPreferredSize().height);

            //---- CHCGU ----
            CHCGU.setToolTipText("num\u00e9ro de guichet");
            CHCGU.setComponentPopupMenu(BTD);
            CHCGU.setName("CHCGU");
            panel2.add(CHCGU);
            CHCGU.setBounds(495, 50, 50, CHCGU.getPreferredSize().height);

            //---- CHNCB ----
            CHNCB.setToolTipText("num\u00e9ro de compte");
            CHNCB.setComponentPopupMenu(BTD);
            CHNCB.setName("CHNCB");
            panel2.add(CHNCB);
            CHNCB.setBounds(550, 50, 121, CHNCB.getPreferredSize().height);

            //---- CHRIB ----
            CHRIB.setToolTipText("Cl\u00e9");
            CHRIB.setComponentPopupMenu(BTD);
            CHRIB.setName("CHRIB");
            panel2.add(CHRIB);
            CHRIB.setBounds(727, 50, 28, CHRIB.getPreferredSize().height);

            //---- UCLEX ----
            UCLEX.setText("@UCLEX@");
            UCLEX.setForeground(Color.red);
            UCLEX.setToolTipText("Cl\u00e9 r\u00e9elle");
            UCLEX.setName("UCLEX");
            panel2.add(UCLEX);
            UCLEX.setBounds(780, 50, 25, UCLEX.getPreferredSize().height);

            //---- OBJ_22_OBJ_25 ----
            OBJ_22_OBJ_25.setText("Banque");
            OBJ_22_OBJ_25.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22_OBJ_25.setFont(OBJ_22_OBJ_25.getFont().deriveFont(OBJ_22_OBJ_25.getFont().getStyle() | Font.BOLD));
            OBJ_22_OBJ_25.setName("OBJ_22_OBJ_25");
            panel2.add(OBJ_22_OBJ_25);
            OBJ_22_OBJ_25.setBounds(445, 25, 50, 28);

            //---- OBJ_22_OBJ_26 ----
            OBJ_22_OBJ_26.setText("Guichet");
            OBJ_22_OBJ_26.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22_OBJ_26.setFont(OBJ_22_OBJ_26.getFont().deriveFont(OBJ_22_OBJ_26.getFont().getStyle() | Font.BOLD));
            OBJ_22_OBJ_26.setName("OBJ_22_OBJ_26");
            panel2.add(OBJ_22_OBJ_26);
            OBJ_22_OBJ_26.setBounds(495, 25, 50, 28);

            //---- OBJ_22_OBJ_27 ----
            OBJ_22_OBJ_27.setText("Num\u00e9ro de compte");
            OBJ_22_OBJ_27.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22_OBJ_27.setFont(OBJ_22_OBJ_27.getFont().deriveFont(OBJ_22_OBJ_27.getFont().getStyle() | Font.BOLD));
            OBJ_22_OBJ_27.setName("OBJ_22_OBJ_27");
            panel2.add(OBJ_22_OBJ_27);
            OBJ_22_OBJ_27.setBounds(550, 25, 121, 28);

            //---- OBJ_22_OBJ_28 ----
            OBJ_22_OBJ_28.setText("Cl\u00e9");
            OBJ_22_OBJ_28.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22_OBJ_28.setFont(OBJ_22_OBJ_28.getFont().deriveFont(OBJ_22_OBJ_28.getFont().getStyle() | Font.BOLD));
            OBJ_22_OBJ_28.setName("OBJ_22_OBJ_28");
            panel2.add(OBJ_22_OBJ_28);
            OBJ_22_OBJ_28.setBounds(727, 25, 28, 28);

            //---- OBJ_22_OBJ_24 ----
            OBJ_22_OBJ_24.setText("Banque");
            OBJ_22_OBJ_24.setName("OBJ_22_OBJ_24");
            panel2.add(OBJ_22_OBJ_24);
            OBJ_22_OBJ_24.setBounds(70, 50, 70, 28);

            //---- CHDOM ----
            CHDOM.setComponentPopupMenu(BTD);
            CHDOM.setName("CHDOM");
            panel2.add(CHDOM);
            CHDOM.setBounds(145, 50, 220, CHDOM.getPreferredSize().height);

            //---- OBJ_22_OBJ_22 ----
            OBJ_22_OBJ_22.setText("RIB");
            OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");
            panel2.add(OBJ_22_OBJ_22);
            OBJ_22_OBJ_22.setBounds(390, 50, 55, 28);

            //---- CHRIE ----
            CHRIE.setComponentPopupMenu(BTD);
            CHRIE.setName("CHRIE");
            panel2.add(CHRIE);
            CHRIE.setBounds(445, 80, 310, CHRIE.getPreferredSize().height);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 844, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 844, GroupLayout.PREFERRED_SIZE))
                    .addGap(0, 0, Short.MAX_VALUE))
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 264, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField INDETB;
  private JLabel OBJ_57;
  private JLabel OBJ_67;
  private XRiTextField INDNUM;
  private XRiTextField INDSUF;
  private XRiCalendrier DRGX;
  private JLabel OBJ_69;
  private XRiTextField WVDE;
  private JLabel OBJ_70;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label1;
  private XRiTextField CHMTT;
  private JLabel label2;
  private XRiCalendrier CHDEMX;
  private JComponent separator1;
  private JLabel label4;
  private XRiTextField EFMFH;
  private JLabel label5;
  private XRiTextField EFIN2;
  private JLabel label6;
  private RiZoneSortie WTAU;
  private JLabel label7;
  private RiZoneSortie CHMFT;
  private RiZoneSortie WFTTC;
  private JLabel label8;
  private JPanel panel4;
  private JLabel label10;
  private XRiCalendrier CHDREX;
  private JLabel label11;
  private JLabel label13;
  private RiZoneSortie CHBRE;
  private RiZoneSortie CHSRE;
  private XRiCheckBox CHNC2X;
  private JLabel label9;
  private XRiTextField CHCAN;
  private JLabel label12;
  private XRiTextField CHTCA;
  private JLabel label19;
  private XRiTextField CHTCA2;
  private XRiTextField CHTAUT;
  private XRiTextField CHTCA3;
  private XRiTextField CHTCA4;
  private JLabel label20;
  private JLabel label21;
  private JLabel label22;
  private JLabel label23;
  private JLabel label24;
  private XRiTextField CHCL2;
  private JLabel label3;
  private XRiTextField CHMOV;
  private XRiTextField CHANVR;
  private JPanel panel3;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label18;
  private XRiTextField CHNCG;
  private XRiTextField CHCLA;
  private XRiTextField CHNOM;
  private RiZoneSortie CHAD1;
  private RiZoneSortie CHAD2;
  private RiZoneSortie CHAD3;
  private XRiTextField CHETB2;
  private XRiTextField CHNCA;
  private JPanel panel2;
  private XRiTextField CHCBQ;
  private XRiTextField CHCGU;
  private XRiTextField CHNCB;
  private XRiTextField CHRIB;
  private RiZoneSortie UCLEX;
  private JLabel OBJ_22_OBJ_25;
  private JLabel OBJ_22_OBJ_26;
  private JLabel OBJ_22_OBJ_27;
  private JLabel OBJ_22_OBJ_28;
  private JLabel OBJ_22_OBJ_24;
  private XRiTextField CHDOM;
  private JLabel OBJ_22_OBJ_22;
  private XRiTextField CHRIE;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
