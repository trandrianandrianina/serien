
package ri.serien.libecranrpg.rgam.RGAM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGAM13FM_A2 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] ARG4A_Value = { "0", "1", "4", "6", "7" };
  private String[] ARG4A_Title = { "Attente", "Validé", "Réceptionné", "Facturé", "Comptabilisé" };
  private String[] PLA4A_Value = { "0", "1", "4", "6", "7", };
  private String[] PLA4A_Title = { "Attente", "Validé", "Réceptionné", "Facturé", "Comptabilisé" };
  private String[] fac_Value = { "1", "6", "7" };
  private String[] fac2_Value = { "1", "6", "7" };
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LDG01", }, { "LDG02", }, { "LDG03", }, { "LDG04", }, { "LDG05", }, { "LDG06", }, { "LDG07", },
      { "LDG08", }, { "LDG09", }, { "LDG10", }, { "LDG11", }, { "LDG12", }, { "LDG13", }, { "LDG14", }, { "LDG15", }, };
  private int[] _WTP01_Width = { 558, };
  private String[] ARG12A_Value = { "", "I", "D" };
  private String[] ARG12A_Title = { "Autres", "Interne", "Direct usine" };
  
  private boolean isFacture = false;
  
  /**
   * Constructeur.
   */
  public RGAM13FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TRTOPT.setValeursSelection("1", " ");
    VERR.setValeursSelection("1", "0");
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    ARG4A.setValeurs(ARG4A_Value, ARG4A_Title);
    PLA4A.setValeurs(PLA4A_Value, PLA4A_Title);
    ARG4AF.setValeurs(fac_Value, null);
    PLA4AF.setValeurs(fac2_Value, null);
    ARG12A.setValeurs(ARG12A_Value, ARG12A_Title);
    SCAN12.setValeurs("E", SCAN12_GRP);
    SCAN12_E.setValeurs("N");
    SCAN12_T.setValeurs(" ");
    DOCSPE.setValeursSelection("1", "");
    
    // Activer la mémorisation de la ligne sélectionnée
    WTP01.setMemorisationLigneSelectionnee(true);
    
    
    BT_FIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    BT_DEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("GESTION DE @GESTION@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOMFRS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Contruction du nom du fichier si F10 (VEXP0A)
    lexique.setNomFichierTableur("Gestion de " + lexique.HostFieldGetData("GESTION").trim());
    
    // Init spécifiques...
    isFacture = lexique.isTrue("85");
    
    panel56.setVisible(isFacture);
    panel51.setVisible(!isFacture);
    
    OBJ_56.setVisible(ARG2N.isVisible());
    OBJ_57.setVisible(ARG8N.isVisible());
    OBJ_60.setVisible(lexique.isPresent("NOMFRS"));
    
    tous.setSelected(ARG4A.getSelectedIndex() == 9);
    if (tous.isSelected()) {
      ARG4A.setVisible(false);
      PLA4A.setVisible(false);
      OBJ_158.setVisible(false);
    }
    else {
      ARG4A.setVisible(true);
      PLA4A.setVisible(true);
      OBJ_158.setVisible(true);
    }
    
    ARG4AF.setSelectedIndex(getIndice("ARG4A", fac_Value));
    PLA4AF.setSelectedIndex(getIndice("PLA4A", fac2_Value));
    
    // Gestion du texte sur la position de la recherche
    if (lexique.isTrue("47")) {
      ((TitledBorder) panel2.getBorder()).setTitle("Résultat de la recherche - Vous êtes au début");
    }
    else if (lexique.isTrue("48")) {
      ((TitledBorder) panel2.getBorder()).setTitle("Résultat de la recherche - Vous êtes à la fin");
    }
    
    miCommandeInitiale.setVisible(lexique.HostFieldGetData("ARG3A").trim().equals("E"));
    
    p_bpresentation.setCodeEtablissement(WETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    if (isFacture) {
      if (tousF.isSelected()) {
        lexique.HostFieldPutData("ARG4A", 0, "9");
      }
      else {
        lexique.HostFieldPutData("ARG4A", 0, fac_Value[ARG4AF.getSelectedIndex()]);
        lexique.HostFieldPutData("PLA4A", 0, fac2_Value[PLA4AF.getSelectedIndex()]);
      }
    }
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("7");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("9");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("Z");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("P");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("È");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("B");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void TRIAGEActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void tousActionPerformed(ActionEvent e) {
    WTP01.getSelectionModel().clearSelection();
    if (tous.isSelected()) {
      lexique.HostFieldPutData("ARG4A", 0, "9");
      ARG4A.setVisible(false);
      PLA4A.setVisible(false);
      OBJ_158.setVisible(false);
    }
    else {
      ARG4A.setVisible(true);
      ARG4A.setSelectedIndex(0);
      PLA4A.setVisible(true);
      OBJ_158.setVisible(true);
    }
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void tousFActionPerformed(ActionEvent e) {
    WTP01.getSelectionModel().clearSelection();
    if (tousF.isSelected()) {
      lexique.HostFieldPutData("ARG4AF", 0, "9");
      ARG4AF.setVisible(false);
      PLA4AF.setVisible(false);
      OBJ_59.setVisible(false);
    }
    else {
      ARG4AF.setVisible(true);
      ARG4AF.setSelectedIndex(0);
      PLA4AF.setVisible(true);
      OBJ_59.setVisible(true);
    }
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void BT_DEBActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("");
    WTP01.getSelectionModel().clearSelection();
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void BT_FINActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("");
    WTP01.getSelectionModel().clearSelection();
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void miCommandeInitialeActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("I");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void miEditionActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void ARG4AActionPerformed(ActionEvent e) {
    try {
      WTP01.getSelectionModel().clearSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ARG12AActionPerformed(ActionEvent e) {
    try {
      WTP01.getSelectionModel().clearSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void DOCSPEActionPerformed(ActionEvent e) {
    try {
      WTP01.getSelectionModel().clearSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ARG2NFocusGained(FocusEvent e) {
    try {
      WTP01.getSelectionModel().clearSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ARG8NFocusGained(FocusEvent e) {
    try {
      WTP01.getSelectionModel().clearSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ARG9NXFocusGained(FocusEvent e) {
    try {
      WTP01.getSelectionModel().clearSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ARGDOCFocusGained(FocusEvent e) {
    try {
      WTP01.getSelectionModel().clearSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void SCAN12ItemStateChanged(ItemEvent e) {
    try {
      WTP01.getSelectionModel().clearSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void SCAN12_EItemStateChanged(ItemEvent e) {
    try {
      WTP01.getSelectionModel().clearSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void PLA4AFActionPerformed(ActionEvent e) {
    try {
      WTP01.getSelectionModel().clearSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void PLA4AActionPerformed(ActionEvent e) {
    try {
      WTP01.getSelectionModel().clearSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_50 = new JLabel();
    OBJ_45 = new JLabel();
    WDAT1X = new XRiCalendrier();
    VERR = new XRiCheckBox();
    INDNUM = new XRiTextField();
    OBJ_47 = new JLabel();
    WETB = new XRiTextField();
    INDSUF = new XRiTextField();
    TRTOPT = new XRiCheckBox();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_60 = new RiZoneSortie();
    OBJ_56 = new JLabel();
    OBJ_57 = new JLabel();
    ARG2N = new XRiTextField();
    ARG9NX = new XRiTextField();
    ARG8N = new XRiTextField();
    OBJ_67 = new JLabel();
    ARGDOC = new XRiTextField();
    DOCSPE = new XRiCheckBox();
    OBJ_34 = new JLabel();
    ARG12A = new XRiComboBox();
    SCAN12 = new XRiRadioButton();
    SCAN12_E = new XRiRadioButton();
    SCAN12_T = new XRiRadioButton();
    panel56 = new JPanel();
    ARG4AF = new XRiComboBox();
    PLA4AF = new XRiComboBox();
    OBJ_58 = new JLabel();
    OBJ_59 = new JLabel();
    tousF = new JCheckBox();
    panel51 = new JPanel();
    ARG4A = new XRiComboBox();
    PLA4A = new XRiComboBox();
    OBJ_55 = new JLabel();
    OBJ_158 = new JLabel();
    tous = new JCheckBox();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_DEB = new JButton();
    BT_FIN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    miEdition = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    miCommandeInitiale = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    BTD2 = new JPopupMenu();
    TRIAGE2 = new JMenuItem();
    OBJ_38 = new JMenuItem();
    SCAN12_GRP = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1024, 600));
    setPreferredSize(new Dimension(1024, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("GESTION DE @GESTION@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(900, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_50 ----
          OBJ_50.setText("Date de traitement");
          OBJ_50.setName("OBJ_50");
          
          // ---- OBJ_45 ----
          OBJ_45.setText("Etablissement");
          OBJ_45.setName("OBJ_45");
          
          // ---- WDAT1X ----
          WDAT1X.setComponentPopupMenu(null);
          WDAT1X.setFont(WDAT1X.getFont().deriveFont(WDAT1X.getFont().getStyle() | Font.BOLD));
          WDAT1X.setName("WDAT1X");
          
          // ---- VERR ----
          VERR.setText("Verrouill\u00e9s");
          VERR.setToolTipText("Liste avec ou sans les bons verrouill\u00e9s");
          VERR.setComponentPopupMenu(null);
          VERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          VERR.setName("VERR");
          
          // ---- INDNUM ----
          INDNUM.setComponentPopupMenu(null);
          INDNUM.setName("INDNUM");
          
          // ---- OBJ_47 ----
          OBJ_47.setText("Num\u00e9ro");
          OBJ_47.setName("OBJ_47");
          
          // ---- WETB ----
          WETB.setComponentPopupMenu(null);
          WETB.setName("WETB");
          
          // ---- INDSUF ----
          INDSUF.setComponentPopupMenu(null);
          INDSUF.setName("INDSUF");
          
          // ---- TRTOPT ----
          TRTOPT.setText("Traitements group\u00e9s");
          TRTOPT.setToolTipText("Traitements group\u00e9s sur la page courante");
          TRTOPT.setComponentPopupMenu(null);
          TRTOPT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TRTOPT.setName("TRTOPT");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(25, 25, 25)
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(VERR, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE).addGap(52, 52, 52)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(WDAT1X, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
                  .addComponent(TRTOPT, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(6, 6, 6).addComponent(VERR, GroupLayout.PREFERRED_SIZE, 16,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(WDAT1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(6, 6, 6).addComponent(TRTOPT, GroupLayout.PREFERRED_SIZE, 16,
                  GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Autres vues");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Totalisation");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Exportation tableur");
              riSousMenu_bt7.setToolTipText("Transfert de la liste obtenue vers le tableur");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Planning d'arrivages");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Affichage des lignes");
              riSousMenu_bt9.setToolTipText("Affichage des lignes");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Stock. personnalisation ");
              riSousMenu_bt14.setToolTipText("Stockage personnalisation ");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1040, 530));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1040, 520));
          p_contenu.setName("p_contenu");
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- OBJ_60 ----
            OBJ_60.setText("@NOMFRS@");
            OBJ_60.setName("OBJ_60");
            panel1.add(OBJ_60);
            OBJ_60.setBounds(630, 12, 310, OBJ_60.getPreferredSize().height);
            
            // ---- OBJ_56 ----
            OBJ_56.setText("Num\u00e9ro d\u00e9but");
            OBJ_56.setName("OBJ_56");
            panel1.add(OBJ_56);
            OBJ_56.setBounds(25, 14, 120, 20);
            
            // ---- OBJ_57 ----
            OBJ_57.setText("Fournisseur");
            OBJ_57.setName("OBJ_57");
            panel1.add(OBJ_57);
            OBJ_57.setBounds(395, 14, 100, 20);
            
            // ---- ARG2N ----
            ARG2N.setComponentPopupMenu(null);
            ARG2N.setName("ARG2N");
            ARG2N.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                ARG2NFocusGained(e);
              }
            });
            panel1.add(ARG2N);
            ARG2N.setBounds(160, 10, 70, ARG2N.getPreferredSize().height);
            
            // ---- ARG9NX ----
            ARG9NX.setComponentPopupMenu(null);
            ARG9NX.setName("ARG9NX");
            ARG9NX.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                ARG9NXFocusGained(e);
              }
            });
            panel1.add(ARG9NX);
            ARG9NX.setBounds(555, 10, 70, ARG9NX.getPreferredSize().height);
            
            // ---- ARG8N ----
            ARG8N.setComponentPopupMenu(null);
            ARG8N.setName("ARG8N");
            ARG8N.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                ARG8NFocusGained(e);
              }
            });
            panel1.add(ARG8N);
            ARG8N.setBounds(530, 10, 24, ARG8N.getPreferredSize().height);
            
            // ---- OBJ_67 ----
            OBJ_67.setText("Recherche document");
            OBJ_67.setName("OBJ_67");
            panel1.add(OBJ_67);
            OBJ_67.setBounds(25, 85, 135, 28);
            
            // ---- ARGDOC ----
            ARGDOC.setComponentPopupMenu(BTD2);
            ARGDOC.setName("ARGDOC");
            ARGDOC.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                ARGDOCFocusGained(e);
              }
            });
            panel1.add(ARGDOC);
            ARGDOC.setBounds(160, 85, 302, ARGDOC.getPreferredSize().height);
            
            // ---- DOCSPE ----
            DOCSPE.setText("Contient des articles sp\u00e9ciaux");
            DOCSPE.setName("DOCSPE");
            DOCSPE.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                DOCSPEActionPerformed(e);
              }
            });
            panel1.add(DOCSPE);
            DOCSPE.setBounds(530, 56, 425, DOCSPE.getPreferredSize().height);
            
            // ---- OBJ_34 ----
            OBJ_34.setText("Type");
            OBJ_34.setName("OBJ_34");
            panel1.add(OBJ_34);
            OBJ_34.setBounds(530, 86, 61, 26);
            
            // ---- ARG12A ----
            ARG12A.setComponentPopupMenu(null);
            ARG12A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG12A.setName("ARG12A");
            ARG12A.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ARG12AActionPerformed(e);
              }
            });
            panel1.add(ARG12A);
            ARG12A.setBounds(595, 86, 122, ARG12A.getPreferredSize().height);
            
            // ---- SCAN12 ----
            SCAN12.setText("Inclus");
            SCAN12.setComponentPopupMenu(null);
            SCAN12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN12.setName("SCAN12");
            SCAN12.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                SCAN12ItemStateChanged(e);
              }
            });
            panel1.add(SCAN12);
            SCAN12.setBounds(725, 86, 70, 26);
            
            // ---- SCAN12_E ----
            SCAN12_E.setText("Exclu");
            SCAN12_E.setComponentPopupMenu(null);
            SCAN12_E.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN12_E.setName("SCAN12_E");
            SCAN12_E.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                SCAN12_EItemStateChanged(e);
              }
            });
            panel1.add(SCAN12_E);
            SCAN12_E.setBounds(802, 86, 65, 26);
            
            // ---- SCAN12_T ----
            SCAN12_T.setText("Tous");
            SCAN12_T.setComponentPopupMenu(null);
            SCAN12_T.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN12_T.setName("SCAN12_T");
            panel1.add(SCAN12_T);
            SCAN12_T.setBounds(874, 86, 65, 26);
            
            // ======== panel56 ========
            {
              panel56.setOpaque(false);
              panel56.setName("panel56");
              panel56.setLayout(new GridBagLayout());
              ((GridBagLayout) panel56.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) panel56.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) panel56.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) panel56.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- ARG4AF ----
              ARG4AF.setModel(new DefaultComboBoxModel(new String[] { "Attente", "Factur\u00e9", "Comptabilis\u00e9", "Tous" }));
              ARG4AF.setComponentPopupMenu(null);
              ARG4AF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG4AF.setName("ARG4AF");
              ARG4AF.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  ARG4AActionPerformed(e);
                }
              });
              panel56.add(ARG4AF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PLA4AF ----
              PLA4AF.setModel(new DefaultComboBoxModel(new String[] { "Attente", "Factur\u00e9", "Comptabilis\u00e9" }));
              PLA4AF.setComponentPopupMenu(null);
              PLA4AF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              PLA4AF.setName("PLA4AF");
              PLA4AF.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  PLA4AFActionPerformed(e);
                }
              });
              panel56.add(PLA4AF, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- OBJ_58 ----
              OBJ_58.setText("Etat des factures");
              OBJ_58.setMinimumSize(new Dimension(130, 30));
              OBJ_58.setMaximumSize(new Dimension(130, 30));
              OBJ_58.setPreferredSize(new Dimension(130, 30));
              OBJ_58.setName("OBJ_58");
              panel56.add(OBJ_58, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_59 ----
              OBJ_59.setText("\u00e0");
              OBJ_59.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_59.setName("OBJ_59");
              panel56.add(OBJ_59, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tousF ----
              tousF.setText("Tous");
              tousF.setToolTipText("Tous");
              tousF.setComponentPopupMenu(null);
              tousF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              tousF.setMaximumSize(new Dimension(80, 30));
              tousF.setMinimumSize(new Dimension(80, 30));
              tousF.setPreferredSize(new Dimension(80, 30));
              tousF.setName("tousF");
              tousF.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  tousFActionPerformed(e);
                }
              });
              panel56.add(tousF, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
            }
            panel1.add(panel56);
            panel56.setBounds(25, 40, 460, panel56.getPreferredSize().height);
            
            // ======== panel51 ========
            {
              panel51.setOpaque(false);
              panel51.setName("panel51");
              panel51.setLayout(new GridBagLayout());
              ((GridBagLayout) panel51.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) panel51.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) panel51.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) panel51.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- ARG4A ----
              ARG4A.setComponentPopupMenu(null);
              ARG4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG4A.setName("ARG4A");
              ARG4A.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  ARG4AActionPerformed(e);
                }
              });
              panel51.add(ARG4A, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PLA4A ----
              PLA4A.setComponentPopupMenu(null);
              PLA4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              PLA4A.setName("PLA4A");
              PLA4A.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  PLA4AActionPerformed(e);
                }
              });
              panel51.add(PLA4A, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- OBJ_55 ----
              OBJ_55.setText("Etat des bons");
              OBJ_55.setMaximumSize(new Dimension(130, 30));
              OBJ_55.setMinimumSize(new Dimension(130, 30));
              OBJ_55.setPreferredSize(new Dimension(130, 30));
              OBJ_55.setName("OBJ_55");
              panel51.add(OBJ_55, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_158 ----
              OBJ_158.setText("\u00e0");
              OBJ_158.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_158.setName("OBJ_158");
              panel51.add(OBJ_158, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tous ----
              tous.setText("Tous");
              tous.setToolTipText("Tous");
              tous.setComponentPopupMenu(null);
              tous.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              tous.setMaximumSize(new Dimension(80, 30));
              tous.setMinimumSize(new Dimension(80, 30));
              tous.setPreferredSize(new Dimension(80, 30));
              tous.setName("tous");
              tous.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  tousActionPerformed(e);
                }
              });
              panel51.add(tous, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
            }
            panel1.add(panel51);
            panel51.setBounds(25, 40, 460, panel51.getPreferredSize().height);
          }
          
          // ======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");
              
              // ---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel2.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(15, 40, 960, 270);
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(980, 75, 25, 95);
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(980, 180, 25, 95);
            
            // ---- BT_DEB ----
            BT_DEB.setText("");
            BT_DEB.setToolTipText("D\u00e9but de la liste");
            BT_DEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_DEB.setName("BT_DEB");
            BT_DEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_DEBActionPerformed(e);
              }
            });
            panel2.add(BT_DEB);
            BT_DEB.setBounds(980, 40, 25, 30);
            
            // ---- BT_FIN ----
            BT_FIN.setText("");
            BT_FIN.setToolTipText("Fin de la liste");
            BT_FIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_FIN.setName("BT_FIN");
            BT_FIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_FINActionPerformed(e);
              }
            });
            panel2.add(BT_FIN);
            BT_FIN.setBounds(980, 280, 25, 30);
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 1024, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 1024, GroupLayout.PREFERRED_SIZE))));
          p_contenuLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] { panel1, panel2 });
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING,
              p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE).addGap(18, 18, 18)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 346, GroupLayout.PREFERRED_SIZE).addContainerGap()));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Extraire");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      
      // ---- OBJ_13 ----
      OBJ_13.setText("Dupliquer");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
      
      // ---- miEdition ----
      miEdition.setText("Editer");
      miEdition.setToolTipText("Editer");
      miEdition.setName("miEdition");
      miEdition.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miEditionActionPerformed(e);
        }
      });
      BTD.add(miEdition);
      
      // ---- OBJ_14 ----
      OBJ_14.setText("Extraction sur un autre magasin");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
      
      // ---- OBJ_15 ----
      OBJ_15.setText("Options");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Dossier et taux de change");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Affichage");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Suivi de commande");
      OBJ_19.setToolTipText("Suivi de commande");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
      
      // ---- miCommandeInitiale ----
      miCommandeInitiale.setText("Commande initiale");
      miCommandeInitiale.setToolTipText("Commande initiale");
      miCommandeInitiale.setName("miCommandeInitiale");
      miCommandeInitiale.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miCommandeInitialeActionPerformed(e);
        }
      });
      BTD.add(miCommandeInitiale);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("D\u00e9verrouiller");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Exportation vers un tableur");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
      
      // ---- OBJ_22 ----
      OBJ_22.setText("Planning des arrivages");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
      
      // ---- OBJ_25 ----
      OBJ_25.setText("Lignes tri\u00e9es par date");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Adresse de stockage");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
      
      // ---- OBJ_23 ----
      OBJ_23.setText("D\u00e9tail retour fournisseur");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);
    }
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- TRIAGE2 ----
      TRIAGE2.setText("Tri\u00e9 par...");
      TRIAGE2.setName("TRIAGE2");
      TRIAGE2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          TRIAGEActionPerformed(e);
        }
      });
      BTD2.add(TRIAGE2);
      BTD2.addSeparator();
      
      // ---- OBJ_38 ----
      OBJ_38.setText("Aide en ligne");
      OBJ_38.setName("OBJ_38");
      OBJ_38.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_38);
    }
    
    // ---- SCAN12_GRP ----
    SCAN12_GRP.add(SCAN12);
    SCAN12_GRP.add(SCAN12_E);
    SCAN12_GRP.add(SCAN12_T);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_50;
  private JLabel OBJ_45;
  private XRiCalendrier WDAT1X;
  private XRiCheckBox VERR;
  private XRiTextField INDNUM;
  private JLabel OBJ_47;
  private XRiTextField WETB;
  private XRiTextField INDSUF;
  private XRiCheckBox TRTOPT;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie OBJ_60;
  private JLabel OBJ_56;
  private JLabel OBJ_57;
  private XRiTextField ARG2N;
  private XRiTextField ARG9NX;
  private XRiTextField ARG8N;
  private JLabel OBJ_67;
  private XRiTextField ARGDOC;
  private XRiCheckBox DOCSPE;
  private JLabel OBJ_34;
  private XRiComboBox ARG12A;
  private XRiRadioButton SCAN12;
  private XRiRadioButton SCAN12_E;
  private XRiRadioButton SCAN12_T;
  private JPanel panel56;
  private XRiComboBox ARG4AF;
  private XRiComboBox PLA4AF;
  private JLabel OBJ_58;
  private JLabel OBJ_59;
  private JCheckBox tousF;
  private JPanel panel51;
  private XRiComboBox ARG4A;
  private XRiComboBox PLA4A;
  private JLabel OBJ_55;
  private JLabel OBJ_158;
  private JCheckBox tous;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_DEB;
  private JButton BT_FIN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem miEdition;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JMenuItem miCommandeInitiale;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_23;
  private JPopupMenu BTD2;
  private JMenuItem TRIAGE2;
  private JMenuItem OBJ_38;
  private ButtonGroup SCAN12_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
