//$$david$$ ££03/01/11££ -> tests et modifs

package ri.serien.libecranrpg.rgam.RGAM04FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGAM04FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 558, };
  private String[] listeCellules =
      { "LD01", "LD02", "LD03", "LD04", "LD05", "LD06", "LD07", "LD08", "LD09", "LD10", "LD11", "LD12", "LD13", "LD14", "LD15" };
  private Color[][] couleurLigne = new Color[listeCellules.length][1];
  
  public RGAM04FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, couleurLigne, null, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    gererCouleurListe();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_41.setVisible(!lexique.HostFieldGetData("WREST").trim().equalsIgnoreCase(""));
    OBJ_55.setIcon(lexique.chargerImage("images/pfin20.png", true));
    OBJ_53.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Recherche fournisseurs"));
    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  // permet de gérer la couleur des lignes de la liste en fonction du type de ligne (article, commentaire , edition,
  // annulé...)
  private void gererCouleurListe() {
    // couleurLigne = new Color[listeCellules.length][1]; // Risque de fuites mémoires
    String chaineTest = null;
    for (int i = 0; i < listeCellules.length; i++) {
      chaineTest = lexique.HostFieldGetData(listeCellules[i]).trim();
      if ((chaineTest != null) && !chaineTest.equals("")) {
        if (chaineTest.matches("^(.)+(\\*Désact)$")) {
          couleurLigne[i][0] = Constantes.COULEUR_LISTE_ANNULATION;
        }
        else if ((chaineTest.matches("^(.)+(\\*Litige)(.)?$")) || (chaineTest.matches("^(.)+(\\*Erroné)(.)?$"))) {
          couleurLigne[i][0] = Constantes.COULEUR_LISTE_ERREUR;
        }
        else {
          couleurLigne[i][0] = Color.BLACK;
        }
      }
      else {
        couleurLigne[i][0] = Color.BLACK;
      }
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "2", "Enter");
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "4", "Enter");
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "5", "Enter");
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "?", "Enter");
    WTP01.setValeurTop("?");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "D", "Enter");
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "S", "Enter");
    WTP01.setValeurTop("S");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "H", "Enter");
    WTP01.setValeurTop("H");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "ENTER", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void OBJ_53ActionPerformed(ActionEvent e) {
    // lexique.HostFieldPutData("WTP01", -1, " ");
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_55ActionPerformed(ActionEvent e) {
    // lexique.HostFieldPutData("WTP01", -1, " ");
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_83 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_84 = new JLabel();
    INDFRS = new XRiTextField();
    WETB = new XRiTextField();
    INDCOL = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    p_contenu = new SNPanelContenu();
    panel1 = new SNPanelTitre();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    sNPanel1 = new SNPanel();
    OBJ_53 = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_55 = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    TRIAGE = new JMenuItem();
    OBJ_30 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Recherche fournisseurs");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
          
          // ---- OBJ_83 ----
          OBJ_83.setText("Etablissement");
          OBJ_83.setName("OBJ_83");
          p_tete_gauche.add(OBJ_83);
          OBJ_83.setBounds(10, 5, 102, 20);
          
          // ---- OBJ_41 ----
          OBJ_41.setText("Restriction");
          OBJ_41.setName("OBJ_41");
          p_tete_gauche.add(OBJ_41);
          OBJ_41.setBounds(395, 5, 330, 20);
          
          // ---- OBJ_84 ----
          OBJ_84.setText("Fournisseur");
          OBJ_84.setName("OBJ_84");
          p_tete_gauche.add(OBJ_84);
          OBJ_84.setBounds(190, 6, 78, 18);
          
          // ---- INDFRS ----
          INDFRS.setToolTipText("Num\u00e9ro fournisseur");
          INDFRS.setComponentPopupMenu(BTD);
          INDFRS.setName("INDFRS");
          p_tete_gauche.add(INDFRS);
          INDFRS.setBounds(300, 1, 60, INDFRS.getPreferredSize().height);
          
          // ---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(110, 1, 40, WETB.getPreferredSize().height);
          
          // ---- INDCOL ----
          INDCOL.setToolTipText("Collectif");
          INDCOL.setComponentPopupMenu(BTD);
          INDCOL.setName("INDCOL");
          p_tete_gauche.add(INDCOL);
          INDCOL.setBounds(275, 1, 22, INDCOL.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Stock. personnalisation ");
              riSousMenu_bt6.setToolTipText("Stockage personnalisation ");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed(e));
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Totalisation sur  demande");
              riSousMenu_bt7.setToolTipText("Totalisation sur  demande");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(e -> riSousMenu_bt7ActionPerformed(e));
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("fournisseurs d\u00e9sactiv\u00e9s");
              riSousMenu_bt8.setToolTipText("On / Off fournisseurs d\u00e9sactiv\u00e9s");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(e -> riSousMenu_bt8ActionPerformed(e));
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Autres vues");
              riSousMenu_bt1.setToolTipText("Autre vue");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(e -> riSousMenu_bt1ActionPerformed(e));
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");
              
              // ---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Edition tableur");
              riSousMenu_bt2.setToolTipText("Transfert de la liste obtenue vers le tableur");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(e -> riSousMenu_bt2ActionPerformed(e));
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setPreferredSize(new Dimension(710, 400));
        p_contenu.setOpaque(true);
        p_contenu.setBackground(new Color(239, 239, 222));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(new BorderLayout());
        
        // ======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setMinimumSize(new Dimension(617, 275));
          panel1.setPreferredSize(new Dimension(617, 275));
          panel1.setName("panel1");
          panel1.setLayout(new GridBagLayout());
          ((GridBagLayout) panel1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) panel1.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) panel1.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) panel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setPreferredSize(new Dimension(1000, 270));
            SCROLLPANE_LIST.setMinimumSize(new Dimension(1000, 270));
            SCROLLPANE_LIST.setBackground(Color.white);
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");
            
            // ---- WTP01 ----
            WTP01.setComponentPopupMenu(BTD);
            WTP01.setMinimumSize(new Dimension(1000, 240));
            WTP01.setPreferredSize(new Dimension(1000, 240));
            WTP01.setPreferredScrollableViewportSize(new Dimension(450, 450));
            WTP01.setMaximumSize(new Dimension(2147483647, 235));
            WTP01.setName("WTP01");
            WTP01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(WTP01);
          }
          panel1.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== sNPanel1 ========
          {
            sNPanel1.setName("sNPanel1");
            sNPanel1.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- OBJ_53 ----
            OBJ_53.setText("");
            OBJ_53.setToolTipText("D\u00e9but de la liste");
            OBJ_53.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_53.setMaximumSize(new Dimension(25, 30));
            OBJ_53.setMinimumSize(new Dimension(25, 30));
            OBJ_53.setPreferredSize(new Dimension(25, 30));
            OBJ_53.setName("OBJ_53");
            OBJ_53.addActionListener(e -> OBJ_53ActionPerformed(e));
            sNPanel1.add(OBJ_53, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setMaximumSize(new Dimension(25, 100));
            BT_PGUP.setMinimumSize(new Dimension(25, 100));
            BT_PGUP.setPreferredSize(new Dimension(25, 100));
            BT_PGUP.setName("BT_PGUP");
            sNPanel1.add(BT_PGUP, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setMaximumSize(new Dimension(25, 100));
            BT_PGDOWN.setMinimumSize(new Dimension(25, 100));
            BT_PGDOWN.setPreferredSize(new Dimension(25, 100));
            BT_PGDOWN.setName("BT_PGDOWN");
            sNPanel1.add(BT_PGDOWN, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OBJ_55 ----
            OBJ_55.setText("");
            OBJ_55.setToolTipText("Fin de la liste");
            OBJ_55.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_55.setMaximumSize(new Dimension(25, 30));
            OBJ_55.setMinimumSize(new Dimension(25, 30));
            OBJ_55.setPreferredSize(new Dimension(25, 30));
            OBJ_55.setName("OBJ_55");
            OBJ_55.addActionListener(e -> OBJ_55ActionPerformed(e));
            sNPanel1.add(OBJ_55, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          panel1.add(sNPanel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(panel1, BorderLayout.CENTER);
      }
      p_sud.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(e -> CHOISIRActionPerformed(e));
      BTD.add(CHOISIR);
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Modifier");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(e -> OBJ_18ActionPerformed(e));
      BTD.add(OBJ_18);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Annuler");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(e -> OBJ_19ActionPerformed(e));
      BTD.add(OBJ_19);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Interroger");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(e -> OBJ_20ActionPerformed(e));
      BTD.add(OBJ_20);
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Options");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(e -> OBJ_21ActionPerformed(e));
      BTD.add(OBJ_21);
      
      // ---- OBJ_25 ----
      OBJ_25.setText("D\u00e9verrouiller");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(e -> OBJ_25ActionPerformed(e));
      BTD.add(OBJ_25);
      
      // ---- OBJ_26 ----
      OBJ_26.setText("Application sp\u00e9cifique");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(e -> OBJ_26ActionPerformed(e));
      BTD.add(OBJ_26);
      
      // ---- OBJ_27 ----
      OBJ_27.setText("Historique de modifications");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(e -> OBJ_27ActionPerformed(e));
      BTD.add(OBJ_27);
    }
    
    // ---- TRIAGE ----
    TRIAGE.setText("Tri\u00e9 par ...");
    TRIAGE.setName("TRIAGE");
    
    // ---- OBJ_30 ----
    OBJ_30.setText("Invite");
    OBJ_30.setName("OBJ_30");
    OBJ_30.addActionListener(e -> OBJ_30ActionPerformed(e));
    
    // ---- OBJ_29 ----
    OBJ_29.setText("Aide en ligne");
    OBJ_29.setName("OBJ_29");
    OBJ_29.addActionListener(e -> OBJ_29ActionPerformed(e));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_83;
  private JLabel OBJ_41;
  private JLabel OBJ_84;
  private XRiTextField INDFRS;
  private XRiTextField WETB;
  private XRiTextField INDCOL;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private SNPanelContenu p_contenu;
  private SNPanelTitre panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private SNPanel sNPanel1;
  private JButton OBJ_53;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton OBJ_55;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_27;
  private JMenuItem TRIAGE;
  private JMenuItem OBJ_30;
  private JMenuItem OBJ_29;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
