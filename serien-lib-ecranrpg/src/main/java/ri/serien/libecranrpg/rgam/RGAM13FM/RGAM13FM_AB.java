
package ri.serien.libecranrpg.rgam.RGAM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGAM13FM_AB extends SNPanelEcranRPG implements ioFrame {
  
  private String[] SCAN30_Value = { "", "E", "N", };
  private String[] ARG13A_Value = { "", "N", "E", };
  private String[] ARG3A_Value = { "E", "F", };
  private String[] PLA4A_Value = { "0", "1", "4", "6", "7", };
  private String[] ARG4A_Value = { "0", "1", "4", "6", "7", };
  private String[] ARG7A_Value = { "", "M", "F", "I", };
  private boolean touslescrit = false;
  private boolean isFacture = false;
  private String[] fac_Value = { "1", "6", "7" };
  private String[] fac2_Value = { "1", "6", "7" };
  private String[] ARG12A_Value = { "", "I", "D" };
  private String[] ARG12A_Title = { "Autres", "Interne", "Direct usine" };
  private boolean desactiverBoutonRapprochement = false;
  
  public RGAM13FM_AB(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDX22.setValeurs("22", "RBC");
    TIDX21.setValeurs("21", "RBC");
    TIDX32.setValeurs("32", "RBC");
    TIDX20.setValeurs("20", "RBC");
    TIDX14.setValeurs("14", "RBC");
    TIDX31.setValeurs("31", "RBC");
    // SCAN38.setValeurs("38", "RBC");
    TIDX23.setValeurs("23", "RBC");
    TIDX10.setValeurs("10", "RBC");
    TIDX6.setValeurs("6", "RBC");
    TIDX9.setValeurs("9", "RBC");
    TIDX5.setValeurs("5", "RBC");
    TIDX36.setValeurs("36", "RBC");
    TIDX2.setValeurs("2", "RBC");
    TIDX18.setValeurs("18", "RBC");
    TIDX17.setValeurs("17", "RBC");
    TIDX28.setValeurs("28", "RBC");
    TIDX19.setValeurs("27", "RBC");
    TIDX35.setValeurs("35", "RBC");
    TIDX34.setValeurs("34", "RBC");
    TIDX33.setValeurs("33", "RBC");
    TIDX16.setValeurs("16", "RBC");
    TIDX15.setValeurs("15", "RBC");
    TIDX26.setValeurs("26", "RBC");
    SCAN30.setValeurs(SCAN30_Value, null);
    ARG13A.setValeurs(ARG13A_Value, null);
    ARG7A.setValeurs(ARG7A_Value, null);
    ARG3A.setValeurs(ARG3A_Value, null);
    PLA4A.setValeurs(PLA4A_Value, null);
    ARG4A.setValeurs(ARG4A_Value, null);
    
    VERR.setValeursSelection("1", "0");
    ARG4AF.setValeurs(fac_Value, null);
    PLA4AF.setValeurs(fac2_Value, null);
    ARG12A.setValeurs(ARG12A_Value, ARG12A_Title);
    SCAN12.setValeurs("E", SCAN12_GRP);
    SCAN12_E.setValeurs("N");
    SCAN12_T.setValeurs(" ");
    DOCSPE.setValeursSelection("1", "");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("GESTION DE @GESTION@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    TIDX33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP01LR@")).trim());
    TIDX34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP02LR@")).trim());
    TIDX35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZP03LR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    desactiverBoutonRapprochement = lexique.isTrue("(N82) AND (N85)");
    isFacture = lexique.isTrue("85");
    
    // détermine si on est en affichage tous critères ou principaux
    touslescrit = !lexique.isTrue("61");
    
    // titre.setText(titre.getText().toLowerCase());
    pnlEtatFacture.setVisible(isFacture);
    pnlEtatBon.setVisible(!isFacture);
    // riMenu1.setVisible(!desactiverBoutonRapprochement);
    riMenu1.setVisible(isFacture);
    
    if (isFacture) {
      TIDX2.setText("Numéro de facture");
    }
    else {
      TIDX2.setText("Numéro de bon");
    }
    
    /*
    panel2.setVisible(!isFacture);
    TIDX21.setVisible(!isFacture);
    TIDX32.setVisible(!isFacture);
    TIDX23.setVisible(!isFacture);
    OBJ_91.setVisible(!isFacture);
    OBJ_92.setVisible(!isFacture);
    OBJ_93.setVisible(!isFacture);
    ARG21D.setVisible(!isFacture);
    ARG32D.setVisible(!isFacture);
    ARG23D.setVisible(!isFacture);
    OBJ_96.setVisible(!isFacture);
    OBJ_97.setVisible(!isFacture);
    OBJ_98.setVisible(!isFacture);
    PLA21D.setVisible(!isFacture);
    PLA32D.setVisible(!isFacture);
    PLA23D.setVisible(!isFacture);
    riMenu1.setVisible(isFacture);
    riSousMenu1.setEnabled(isFacture);
    riSousMenu1.setVisible(isFacture);
    */
    
    pnlDateValidation.setVisible(TIDX21.isVisible());
    pnlDateReception.setVisible(TIDX22.isVisible());
    pnlDateReceptionPrevue.setVisible(TIDX32.isVisible());
    pnlDateFacturation.setVisible(TIDX23.isVisible());
    
    tous.setEnabled(lexique.isPresent("ARG4A"));
    tous.setSelected(ARG4A.getSelectedIndex() == 9);
    if (tous.isSelected()) {
      ARG4A.setVisible(false);
      PLA4A.setVisible(false);
      OBJ_58.setVisible(false);
    }
    else {
      ARG4A.setVisible(true);
      PLA4A.setVisible(true);
      OBJ_58.setVisible(true);
    }
    
    lb_requete.setVisible(lexique.HostFieldGetData("AFFREQ").trim().equalsIgnoreCase("1"));
    
    if (touslescrit) {
      riSousMenu_bt6.setText("Critères principaux");
    }
    else {
      riSousMenu_bt6.setText("Tous les critères");
    }
    
    voirCriteres();
    
    TIDX33.setVisible(!lexique.HostFieldGetData("ZP01LR").trim().equalsIgnoreCase(""));
    TIDX34.setVisible(!lexique.HostFieldGetData("ZP02LR").trim().equalsIgnoreCase(""));
    TIDX35.setVisible(!lexique.HostFieldGetData("ZP03LR").trim().equalsIgnoreCase(""));
    ARG33A.setVisible(!lexique.HostFieldGetData("ZP01LR").trim().equalsIgnoreCase(""));
    ARG34A.setVisible(!lexique.HostFieldGetData("ZP02LR").trim().equalsIgnoreCase(""));
    ARG35A.setVisible(!lexique.HostFieldGetData("ZP03LR").trim().equalsIgnoreCase(""));
    separator1.setVisible(!lexique.HostFieldGetData("ZP01LR").trim().equalsIgnoreCase(""));
    
    ARG4AF.setSelectedIndex(getIndice("ARG4A", fac_Value));
    PLA4AF.setSelectedIndex(getIndice("PLA4A", fac2_Value));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt1.setIcon(lexique.chargerImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    if (isFacture) {
      if (tousF.isSelected()) {
        lexique.HostFieldPutData("ARG4A", 0, "9");
      }
      else {
        lexique.HostFieldPutData("ARG4A", 0, fac_Value[ARG4AF.getSelectedIndex()]);
        lexique.HostFieldPutData("PLA4A", 0, fac2_Value[PLA4AF.getSelectedIndex()]);
      }
    }
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    panel52.setVisible(!touslescrit);
    panel55.setVisible(!touslescrit);
    panel7.setVisible(!touslescrit);
    lbArticle.setVisible(!touslescrit);
    ARG37A.setVisible(!touslescrit);
    panel9.setVisible(!touslescrit);
    
    if (touslescrit) {
      // retaille des panels
      panel5.setMaximumSize(new Dimension(460, 160));
      panel6.setMaximumSize(new Dimension(460, 85));
      panel8.setMaximumSize(new Dimension(460, 167));
      
      // bouton critères
      riSousMenu_bt6.setText("Tous les critères");
      touslescrit = false;
      lexique.SetOn(61);
      
      repaint();
    }
    else {
      // retaille des panels
      panel5.setPreferredSize(new Dimension(460, 240));
      panel6.setSize(460, 115);
      panel8.setSize(460, 198);
      
      // bouton critères
      riSousMenu_bt6.setText("Critères principaux");
      touslescrit = true;
      lexique.SetOff(61);
      repaint();
    }
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    if (interpreteurD.analyseExpression("@AFFREQ@").equalsIgnoreCase("1")) {
      lexique.HostFieldPutData("AFFREQ", 0, " ");
      lb_requete.setVisible(false);
    }
    else {
      lexique.HostFieldPutData("AFFREQ", 0, "1");
      lb_requete.setVisible(true);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgam"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void tousActionPerformed(ActionEvent e) {
    if (tous.isSelected()) {
      lexique.HostFieldPutData("ARG4A", 0, "9");
      ARG4A.setVisible(false);
      PLA4A.setVisible(false);
      OBJ_58.setVisible(false);
    }
    else {
      ARG4A.setVisible(true);
      ARG4A.setSelectedIndex(0);
      PLA4A.setVisible(true);
      OBJ_58.setVisible(true);
    }
  }
  
  private void ARG4AActionPerformed(ActionEvent e) {
    // tous.setSelected(ARG4A.getSelectedIndex() == 6);
  }
  
  private void tousFActionPerformed(ActionEvent e) {
    if (tousF.isSelected()) {
      lexique.HostFieldPutData("ARG4AF", 0, "9");
      ARG4AF.setVisible(false);
      PLA4AF.setVisible(false);
      OBJ_59.setVisible(false);
    }
    else {
      ARG4AF.setVisible(true);
      ARG4AF.setSelectedIndex(0);
      PLA4AF.setVisible(true);
      OBJ_59.setVisible(true);
    }
    
  }
  
  private void voirCriteres() {
    panel52.setVisible(touslescrit);
    panel55.setVisible(touslescrit);
    panel7.setVisible(touslescrit);
    lbArticle.setVisible(touslescrit);
    ARG37A.setVisible(touslescrit);
    panel9.setVisible(touslescrit);
    
    if (!touslescrit) {
      // retaille des panels
      panel5.setMaximumSize(new Dimension(460, 160));
      panel6.setMaximumSize(new Dimension(460, 85));
      panel8.setMaximumSize(new Dimension(460, 167));
    }
    else {
      // retaille des panels
      panel5.setSize(460, 240);
      panel6.setSize(460, 115);
      panel8.setSize(460, 198);
      
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_50 = new JLabel();
    OBJ_45 = new JLabel();
    WDAT1X = new XRiCalendrier();
    VERR = new XRiCheckBox();
    INDNUM = new XRiTextField();
    OBJ_47 = new JLabel();
    INDETB = new XRiTextField();
    INDSUF = new XRiTextField();
    p_tete_droite = new JPanel();
    lb_requete = new JLabel();
    p_sud = new SNPanelFond();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    panel5 = new JPanel();
    pnlEtatBon = new JPanel();
    ARG4A = new XRiComboBox();
    PLA4A = new XRiComboBox();
    OBJ_55 = new JLabel();
    OBJ_58 = new JLabel();
    tous = new JCheckBox();
    pnlEtatFacture = new JPanel();
    ARG4AF = new XRiComboBox();
    PLA4AF = new XRiComboBox();
    OBJ_56 = new JLabel();
    tousF = new JCheckBox();
    OBJ_59 = new JLabel();
    panel52 = new JPanel();
    TIDX2 = new XRiRadioButton();
    ARG2N = new XRiTextField();
    PLA2N = new XRiTextField();
    OBJ_65 = new JLabel();
    panel2 = new JPanel();
    TIDX36 = new XRiRadioButton();
    ARG36N = new XRiTextField();
    PLA36N = new XRiTextField();
    OBJ_71 = new JLabel();
    panel53 = new JPanel();
    TIDX5 = new XRiRadioButton();
    ARG5A = new XRiTextField();
    panel54 = new JPanel();
    TIDX9 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    ARG9NX = new XRiTextField();
    ARG6A = new XRiTextField();
    ARG8N = new XRiTextField();
    panel55 = new JPanel();
    TIDX10 = new XRiRadioButton();
    OBJ_95 = new JLabel();
    ARG10A = new XRiTextField();
    ARG11A = new XRiTextField();
    ARGDOC = new XRiTextField();
    OBJ_66 = new JLabel();
    DOCSPE = new XRiCheckBox();
    lbArticle = new JLabel();
    ARG37A = new XRiTextField();
    panel6 = new JPanel();
    panel61 = new JPanel();
    TIDX26 = new XRiRadioButton();
    ARG26A = new XRiTextField();
    panel62 = new JPanel();
    TIDX15 = new XRiRadioButton();
    ARG15A = new XRiTextField();
    TIDX16 = new XRiRadioButton();
    ARG16A = new XRiTextField();
    panel7 = new JPanel();
    TIDX33 = new XRiRadioButton();
    TIDX34 = new XRiRadioButton();
    TIDX35 = new XRiRadioButton();
    TIDX19 = new XRiRadioButton();
    TIDX28 = new XRiRadioButton();
    ARG28A = new XRiTextField();
    TIDX17 = new XRiRadioButton();
    ARG17A = new XRiTextField();
    TIDX18 = new XRiRadioButton();
    ARG27A = new XRiTextField();
    ARG18A = new XRiTextField();
    ARG34A = new XRiTextField();
    ARG35A = new XRiTextField();
    separator1 = new JSeparator();
    ARG33A = new XRiTextField();
    pnllDroite = new SNPanel();
    panel8 = new JPanel();
    WNOM = new XRiTextField();
    OBJ_67 = new JLabel();
    OBJ_60 = new JLabel();
    WCDP = new XRiTextField();
    OBJ_73 = new JLabel();
    WTEL = new XRiTextField();
    OBJ_79 = new JLabel();
    ARG39A = new XRiTextField();
    panel4 = new JPanel();
    OBJ_32 = new JLabel();
    ARG7A = new XRiComboBox();
    OBJ_33 = new JLabel();
    ARG13A = new XRiComboBox();
    ARG12A = new XRiComboBox();
    SCAN12 = new XRiRadioButton();
    SCAN12_E = new XRiRadioButton();
    SCAN12_T = new XRiRadioButton();
    OBJ_34 = new JLabel();
    panel9 = new JPanel();
    TIDX20 = new XRiRadioButton();
    pnlDateCreation = new SNPanel();
    OBJ_89 = new JLabel();
    ARG20D = new XRiCalendrier();
    OBJ_90 = new JLabel();
    PLA20D = new XRiCalendrier();
    TIDX21 = new XRiRadioButton();
    pnlDateValidation = new SNPanel();
    OBJ_91 = new JLabel();
    ARG21D = new XRiCalendrier();
    OBJ_96 = new JLabel();
    PLA21D = new XRiCalendrier();
    TIDX32 = new XRiRadioButton();
    pnlDateReceptionPrevue = new SNPanel();
    OBJ_92 = new JLabel();
    ARG32D = new XRiCalendrier();
    OBJ_97 = new JLabel();
    PLA32D = new XRiCalendrier();
    TIDX22 = new XRiRadioButton();
    pnlDateReception = new SNPanel();
    OBJ_93 = new JLabel();
    ARG22D = new XRiCalendrier();
    OBJ_98 = new JLabel();
    PLA22D = new XRiCalendrier();
    TIDX23 = new XRiRadioButton();
    pnlDateFacturation = new SNPanel();
    OBJ_94 = new JLabel();
    ARG23D = new XRiCalendrier();
    OBJ_99 = new JLabel();
    PLA23D = new XRiCalendrier();
    SCAN38 = new XRiRadioButton();
    ARG38N = new XRiTextField();
    TIDX31 = new XRiRadioButton();
    pnlMontant = new SNPanel();
    OBJ_119 = new JLabel();
    ARG31N = new XRiTextField();
    OBJ_120 = new JLabel();
    PLA31N = new XRiTextField();
    TIDX14 = new XRiRadioButton();
    pnlDevise = new SNPanel();
    ARG14A = new XRiTextField();
    OBJ_139 = new JLabel();
    ARG30A = new XRiTextField();
    SCAN30 = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_31 = new JLabel();
    ARG3A = new XRiComboBox();
    ARG37A2 = new XRiTextField();
    pnlDateCreation9 = new SNPanel();
    SCAN12_GRP = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("GESTION DE @GESTION@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(900, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_50 ----
          OBJ_50.setText("Date de traitement");
          OBJ_50.setName("OBJ_50");
          
          // ---- OBJ_45 ----
          OBJ_45.setText("Etablissement");
          OBJ_45.setName("OBJ_45");
          
          // ---- WDAT1X ----
          WDAT1X.setComponentPopupMenu(null);
          WDAT1X.setFont(WDAT1X.getFont().deriveFont(WDAT1X.getFont().getStyle() | Font.BOLD));
          WDAT1X.setName("WDAT1X");
          
          // ---- VERR ----
          VERR.setText("Verrouill\u00e9s");
          VERR.setToolTipText("Liste avec ou sans les bons verrouill\u00e9s");
          VERR.setComponentPopupMenu(null);
          VERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          VERR.setName("VERR");
          
          // ---- INDNUM ----
          INDNUM.setComponentPopupMenu(null);
          INDNUM.setName("INDNUM");
          
          // ---- OBJ_47 ----
          OBJ_47.setText("Num\u00e9ro");
          OBJ_47.setName("OBJ_47");
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");
          
          // ---- INDSUF ----
          INDSUF.setComponentPopupMenu(null);
          INDSUF.setName("INDSUF");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(25, 25, 25)
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(VERR, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE).addGap(55, 55, 55)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WDAT1X, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(6, 6, 6).addComponent(VERR, GroupLayout.PREFERRED_SIZE, 16,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(WDAT1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lb_requete ----
          lb_requete.setText("Affichage requ\u00eate SQL");
          lb_requete.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_requete.setFont(lb_requete.getFont().deriveFont(lb_requete.getFont().getStyle() | Font.BOLD));
          lb_requete.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_requete.setName("lb_requete");
          p_tete_droite.add(lb_requete);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Tous les crit\u00e8res");
              riSousMenu_bt6.setToolTipText("Tous les crit\u00e8res de recherche");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed(e));
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Visualisation requ\u00eate");
              riSousMenu_bt9.setToolTipText("Visualisation de la requ\u00eate SQL");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(e -> riSousMenu_bt9ActionPerformed(e));
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riMenu1 ========
            {
              riMenu1.setName("riMenu1");
              
              // ---- riMenu_bt1 ----
              riMenu_bt1.setText("Fonctions");
              riMenu_bt1.setName("riMenu_bt1");
              riMenu1.add(riMenu_bt1);
            }
            menus_haut.add(riMenu1);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Rapprochement");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(e -> riSousMenu_bt1ActionPerformed(e));
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridLayout(1, 2));
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 1.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== panel5 ========
          {
            panel5.setBorder(new TitledBorder(""));
            panel5.setOpaque(false);
            panel5.setComponentPopupMenu(null);
            panel5.setMinimumSize(new Dimension(445, 450));
            panel5.setPreferredSize(new Dimension(450, 450));
            panel5.setName("panel5");
            panel5.setLayout(new GridBagLayout());
            ((GridBagLayout) panel5.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) panel5.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 26, 0 };
            ((GridBagLayout) panel5.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) panel5.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
            
            // ======== pnlEtatBon ========
            {
              pnlEtatBon.setOpaque(false);
              pnlEtatBon.setName("pnlEtatBon");
              pnlEtatBon.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEtatBon.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlEtatBon.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlEtatBon.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlEtatBon.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- ARG4A ----
              ARG4A.setModel(new DefaultComboBoxModel<>(
                  new String[] { "Attente", "Valid\u00e9", "R\u00e9ceptionn\u00e9", "Factur\u00e9", "Comptabilis\u00e9" }));
              ARG4A.setComponentPopupMenu(null);
              ARG4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG4A.setName("ARG4A");
              ARG4A.addActionListener(e -> ARG4AActionPerformed(e));
              pnlEtatBon.add(ARG4A, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PLA4A ----
              PLA4A.setModel(new DefaultComboBoxModel<>(
                  new String[] { "Attente", "Valid\u00e9", "R\u00e9ceptionn\u00e9", "Factur\u00e9", "Comptabilis\u00e9" }));
              PLA4A.setComponentPopupMenu(null);
              PLA4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              PLA4A.setName("PLA4A");
              pnlEtatBon.add(PLA4A, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- OBJ_55 ----
              OBJ_55.setText("Etat des bons");
              OBJ_55.setMinimumSize(new Dimension(120, 30));
              OBJ_55.setMaximumSize(new Dimension(120, 30));
              OBJ_55.setPreferredSize(new Dimension(120, 30));
              OBJ_55.setName("OBJ_55");
              pnlEtatBon.add(OBJ_55, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_58 ----
              OBJ_58.setText("\u00e0");
              OBJ_58.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_58.setName("OBJ_58");
              pnlEtatBon.add(OBJ_58, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tous ----
              tous.setText("Tous");
              tous.setToolTipText("Tous");
              tous.setComponentPopupMenu(null);
              tous.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              tous.setMaximumSize(new Dimension(80, 30));
              tous.setMinimumSize(new Dimension(80, 30));
              tous.setPreferredSize(new Dimension(80, 30));
              tous.setName("tous");
              tous.addActionListener(e -> tousActionPerformed(e));
              pnlEtatBon.add(tous, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
            }
            panel5.add(pnlEtatBon, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlEtatFacture ========
            {
              pnlEtatFacture.setOpaque(false);
              pnlEtatFacture.setName("pnlEtatFacture");
              pnlEtatFacture.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEtatFacture.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlEtatFacture.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlEtatFacture.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlEtatFacture.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- ARG4AF ----
              ARG4AF.setModel(new DefaultComboBoxModel<>(new String[] { "Attente", "Factur\u00e9", "Comptabilis\u00e9" }));
              ARG4AF.setComponentPopupMenu(null);
              ARG4AF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG4AF.setName("ARG4AF");
              ARG4AF.addActionListener(e -> ARG4AActionPerformed(e));
              pnlEtatFacture.add(ARG4AF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PLA4AF ----
              PLA4AF.setModel(new DefaultComboBoxModel<>(new String[] { "Attente", "Factur\u00e9", "Comptabilis\u00e9" }));
              PLA4AF.setComponentPopupMenu(null);
              PLA4AF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              PLA4AF.setName("PLA4AF");
              pnlEtatFacture.add(PLA4AF, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- OBJ_56 ----
              OBJ_56.setText("Etat des factures");
              OBJ_56.setMinimumSize(new Dimension(120, 30));
              OBJ_56.setPreferredSize(new Dimension(120, 30));
              OBJ_56.setMaximumSize(new Dimension(120, 30));
              OBJ_56.setName("OBJ_56");
              pnlEtatFacture.add(OBJ_56, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tousF ----
              tousF.setText("Tous");
              tousF.setToolTipText("Tous");
              tousF.setComponentPopupMenu(null);
              tousF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              tousF.setMinimumSize(new Dimension(80, 30));
              tousF.setPreferredSize(new Dimension(80, 30));
              tousF.setMaximumSize(new Dimension(80, 18));
              tousF.setName("tousF");
              tousF.addActionListener(e -> tousFActionPerformed(e));
              pnlEtatFacture.add(tousF, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_59 ----
              OBJ_59.setText("\u00e0");
              OBJ_59.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_59.setName("OBJ_59");
              pnlEtatFacture.add(OBJ_59, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
            }
            panel5.add(pnlEtatFacture, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== panel52 ========
            {
              panel52.setOpaque(false);
              panel52.setComponentPopupMenu(null);
              panel52.setName("panel52");
              panel52.setLayout(null);
              
              // ---- TIDX2 ----
              TIDX2.setText("Num\u00e9ro de bon");
              TIDX2.setComponentPopupMenu(null);
              TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX2.setName("TIDX2");
              panel52.add(TIDX2);
              TIDX2.setBounds(5, 4, 150, 20);
              
              // ---- ARG2N ----
              ARG2N.setComponentPopupMenu(BTD2);
              ARG2N.setName("ARG2N");
              panel52.add(ARG2N);
              ARG2N.setBounds(180, 0, 70, ARG2N.getPreferredSize().height);
              
              // ---- PLA2N ----
              PLA2N.setComponentPopupMenu(BTD2);
              PLA2N.setName("PLA2N");
              panel52.add(PLA2N);
              PLA2N.setBounds(315, 0, 70, PLA2N.getPreferredSize().height);
              
              // ---- OBJ_65 ----
              OBJ_65.setText("\u00e0");
              OBJ_65.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_65.setName("OBJ_65");
              panel52.add(OBJ_65);
              OBJ_65.setBounds(275, 4, 16, 20);
              
              // ======== panel2 ========
              {
                panel2.setOpaque(false);
                panel2.setName("panel2");
                panel2.setLayout(null);
                
                // ---- TIDX36 ----
                TIDX36.setText("Num\u00e9ro de facture");
                TIDX36.setComponentPopupMenu(null);
                TIDX36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                TIDX36.setName("TIDX36");
                panel2.add(TIDX36);
                TIDX36.setBounds(5, 5, 150, 20);
                
                // ---- ARG36N ----
                ARG36N.setComponentPopupMenu(BTD2);
                ARG36N.setName("ARG36N");
                panel2.add(ARG36N);
                ARG36N.setBounds(180, 0, 80, ARG36N.getPreferredSize().height);
                
                // ---- PLA36N ----
                PLA36N.setComponentPopupMenu(BTD2);
                PLA36N.setName("PLA36N");
                panel2.add(PLA36N);
                PLA36N.setBounds(315, 0, 80, PLA36N.getPreferredSize().height);
                
                // ---- OBJ_71 ----
                OBJ_71.setText("\u00e0");
                OBJ_71.setHorizontalAlignment(SwingConstants.CENTER);
                OBJ_71.setName("OBJ_71");
                panel2.add(OBJ_71);
                OBJ_71.setBounds(275, 5, 16, 20);
                
                { // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < panel2.getComponentCount(); i++) {
                    Rectangle bounds = panel2.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel2.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel2.setMinimumSize(preferredSize);
                  panel2.setPreferredSize(preferredSize);
                }
              }
              panel52.add(panel2);
              panel2.setBounds(0, 30, 415, 30);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel52.getComponentCount(); i++) {
                  Rectangle bounds = panel52.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel52.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel52.setMinimumSize(preferredSize);
                panel52.setPreferredSize(preferredSize);
              }
            }
            panel5.add(panel52, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== panel53 ========
            {
              panel53.setOpaque(false);
              panel53.setComponentPopupMenu(null);
              panel53.setName("panel53");
              panel53.setLayout(null);
              
              // ---- TIDX5 ----
              TIDX5.setText("Magasin");
              TIDX5.setComponentPopupMenu(null);
              TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX5.setName("TIDX5");
              panel53.add(TIDX5);
              TIDX5.setBounds(5, 4, 150, 20);
              
              // ---- ARG5A ----
              ARG5A.setComponentPopupMenu(BTD);
              ARG5A.setName("ARG5A");
              panel53.add(ARG5A);
              ARG5A.setBounds(180, 0, 34, ARG5A.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel53.getComponentCount(); i++) {
                  Rectangle bounds = panel53.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel53.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel53.setMinimumSize(preferredSize);
                panel53.setPreferredSize(preferredSize);
              }
            }
            panel5.add(panel53, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== panel54 ========
            {
              panel54.setOpaque(false);
              panel54.setComponentPopupMenu(null);
              panel54.setName("panel54");
              panel54.setLayout(null);
              
              // ---- TIDX9 ----
              TIDX9.setText("Num\u00e9ro fournisseur");
              TIDX9.setComponentPopupMenu(null);
              TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX9.setName("TIDX9");
              panel54.add(TIDX9);
              TIDX9.setBounds(5, 4, 150, 20);
              
              // ---- TIDX6 ----
              TIDX6.setText("Acheteur");
              TIDX6.setComponentPopupMenu(null);
              TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX6.setName("TIDX6");
              panel54.add(TIDX6);
              TIDX6.setBounds(5, 34, 150, 20);
              
              // ---- ARG9NX ----
              ARG9NX.setComponentPopupMenu(BTD);
              ARG9NX.setHorizontalAlignment(SwingConstants.RIGHT);
              ARG9NX.setName("ARG9NX");
              panel54.add(ARG9NX);
              ARG9NX.setBounds(202, 0, 70, ARG9NX.getPreferredSize().height);
              
              // ---- ARG6A ----
              ARG6A.setComponentPopupMenu(BTD);
              ARG6A.setName("ARG6A");
              panel54.add(ARG6A);
              ARG6A.setBounds(180, 30, 40, ARG6A.getPreferredSize().height);
              
              // ---- ARG8N ----
              ARG8N.setComponentPopupMenu(BTD);
              ARG8N.setName("ARG8N");
              panel54.add(ARG8N);
              ARG8N.setBounds(180, 0, 20, ARG8N.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel54.getComponentCount(); i++) {
                  Rectangle bounds = panel54.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel54.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel54.setMinimumSize(preferredSize);
                panel54.setPreferredSize(preferredSize);
              }
            }
            panel5.add(panel54, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== panel55 ========
            {
              panel55.setOpaque(false);
              panel55.setComponentPopupMenu(null);
              panel55.setName("panel55");
              panel55.setLayout(null);
              
              // ---- TIDX10 ----
              TIDX10.setText("Mode d'exp\u00e9dition");
              TIDX10.setComponentPopupMenu(null);
              TIDX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX10.setName("TIDX10");
              panel55.add(TIDX10);
              TIDX10.setBounds(5, 4, 150, 20);
              
              // ---- OBJ_95 ----
              OBJ_95.setText("Transporteur");
              OBJ_95.setName("OBJ_95");
              panel55.add(OBJ_95);
              OBJ_95.setBounds(230, 4, 80, 20);
              
              // ---- ARG10A ----
              ARG10A.setComponentPopupMenu(BTD);
              ARG10A.setName("ARG10A");
              panel55.add(ARG10A);
              ARG10A.setBounds(180, 0, 34, ARG10A.getPreferredSize().height);
              
              // ---- ARG11A ----
              ARG11A.setComponentPopupMenu(BTD);
              ARG11A.setName("ARG11A");
              panel55.add(ARG11A);
              ARG11A.setBounds(315, 0, 34, ARG11A.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel55.getComponentCount(); i++) {
                  Rectangle bounds = panel55.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel55.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel55.setMinimumSize(preferredSize);
                panel55.setPreferredSize(preferredSize);
              }
            }
            panel5.add(panel55, new GridBagConstraints(0, 6, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- ARGDOC ----
            ARGDOC.setComponentPopupMenu(BTD2);
            ARGDOC.setName("ARGDOC");
            panel5.add(ARGDOC, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OBJ_66 ----
            OBJ_66.setText("Recherche document");
            OBJ_66.setName("OBJ_66");
            panel5.add(OBJ_66, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- DOCSPE ----
            DOCSPE.setText("Contient des articles sp\u00e9ciaux");
            DOCSPE.setName("DOCSPE");
            panel5.add(DOCSPE, new GridBagConstraints(0, 7, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticle ----
            lbArticle.setText("Article");
            lbArticle.setPreferredSize(new Dimension(33, 25));
            lbArticle.setMinimumSize(new Dimension(33, 25));
            lbArticle.setMaximumSize(new Dimension(33, 25));
            lbArticle.setName("lbArticle");
            panel5.add(lbArticle, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- ARG37A ----
            ARG37A.setComponentPopupMenu(BTD2);
            ARG37A.setMinimumSize(new Dimension(210, 28));
            ARG37A.setMaximumSize(new Dimension(210, 28));
            ARG37A.setPreferredSize(new Dimension(210, 28));
            ARG37A.setName("ARG37A");
            panel5.add(ARG37A, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlGauche.add(panel5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== panel6 ========
          {
            panel6.setBorder(new TitledBorder(""));
            panel6.setOpaque(false);
            panel6.setComponentPopupMenu(null);
            panel6.setMinimumSize(new Dimension(474, 110));
            panel6.setPreferredSize(new Dimension(458, 110));
            panel6.setName("panel6");
            panel6.setLayout(new GridBagLayout());
            ((GridBagLayout) panel6.getLayout()).columnWidths = new int[] { 454, 0 };
            ((GridBagLayout) panel6.getLayout()).rowHeights = new int[] { 0, 68, 0 };
            ((GridBagLayout) panel6.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) panel6.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ======== panel61 ========
            {
              panel61.setOpaque(false);
              panel61.setComponentPopupMenu(null);
              panel61.setName("panel61");
              panel61.setLayout(new GridBagLayout());
              ((GridBagLayout) panel61.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) panel61.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) panel61.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) panel61.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- TIDX26 ----
              TIDX26.setText("Commande initiale");
              TIDX26.setComponentPopupMenu(null);
              TIDX26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX26.setPreferredSize(new Dimension(170, 18));
              TIDX26.setMinimumSize(new Dimension(170, 18));
              TIDX26.setName("TIDX26");
              panel61.add(TIDX26, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG26A ----
              ARG26A.setComponentPopupMenu(BTD2);
              ARG26A.setPreferredSize(new Dimension(110, 28));
              ARG26A.setMinimumSize(new Dimension(110, 28));
              ARG26A.setName("ARG26A");
              panel61.add(ARG26A, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            panel6.add(panel61, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== panel62 ========
            {
              panel62.setOpaque(false);
              panel62.setComponentPopupMenu(null);
              panel62.setName("panel62");
              panel62.setLayout(new GridBagLayout());
              ((GridBagLayout) panel62.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) panel62.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) panel62.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) panel62.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ---- TIDX15 ----
              TIDX15.setText("R\u00e9f\u00e9rence commande");
              TIDX15.setComponentPopupMenu(null);
              TIDX15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX15.setPreferredSize(new Dimension(170, 18));
              TIDX15.setMinimumSize(new Dimension(170, 18));
              TIDX15.setName("TIDX15");
              panel62.add(TIDX15, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG15A ----
              ARG15A.setComponentPopupMenu(BTD2);
              ARG15A.setPreferredSize(new Dimension(110, 28));
              ARG15A.setMinimumSize(new Dimension(110, 28));
              ARG15A.setName("ARG15A");
              panel62.add(ARG15A, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- TIDX16 ----
              TIDX16.setText("R\u00e9f\u00e9rence llivraison");
              TIDX16.setComponentPopupMenu(null);
              TIDX16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX16.setPreferredSize(new Dimension(170, 18));
              TIDX16.setMinimumSize(new Dimension(170, 18));
              TIDX16.setName("TIDX16");
              panel62.add(TIDX16, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG16A ----
              ARG16A.setComponentPopupMenu(BTD2);
              ARG16A.setPreferredSize(new Dimension(110, 28));
              ARG16A.setMinimumSize(new Dimension(110, 28));
              ARG16A.setName("ARG16A");
              panel62.add(ARG16A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            panel6.add(panel62, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(panel6, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== panel7 ========
          {
            panel7.setBorder(new TitledBorder(""));
            panel7.setOpaque(false);
            panel7.setComponentPopupMenu(null);
            panel7.setMinimumSize(new Dimension(445, 160));
            panel7.setPreferredSize(new Dimension(429, 160));
            panel7.setName("panel7");
            panel7.setLayout(new GridBagLayout());
            ((GridBagLayout) panel7.getLayout()).columnWidths = new int[] { 114, 93, 25, 0, 82, 0 };
            ((GridBagLayout) panel7.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) panel7.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) panel7.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- TIDX33 ----
            TIDX33.setText("@ZP01LR@");
            TIDX33.setComponentPopupMenu(null);
            TIDX33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX33.setName("TIDX33");
            panel7.add(TIDX33, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- TIDX34 ----
            TIDX34.setText("@ZP02LR@");
            TIDX34.setComponentPopupMenu(null);
            TIDX34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX34.setName("TIDX34");
            panel7.add(TIDX34, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- TIDX35 ----
            TIDX35.setText("@ZP03LR@");
            TIDX35.setComponentPopupMenu(null);
            TIDX35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX35.setName("TIDX35");
            panel7.add(TIDX35, new GridBagConstraints(3, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- TIDX19 ----
            TIDX19.setText("Code dossier");
            TIDX19.setComponentPopupMenu(null);
            TIDX19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX19.setName("TIDX19");
            panel7.add(TIDX19, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- TIDX28 ----
            TIDX28.setText("Code container");
            TIDX28.setComponentPopupMenu(null);
            TIDX28.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX28.setName("TIDX28");
            panel7.add(TIDX28, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- ARG28A ----
            ARG28A.setComponentPopupMenu(BTD2);
            ARG28A.setName("ARG28A");
            panel7.add(ARG28A, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- TIDX17 ----
            TIDX17.setText("Section");
            TIDX17.setComponentPopupMenu(null);
            TIDX17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX17.setName("TIDX17");
            panel7.add(TIDX17, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- ARG17A ----
            ARG17A.setComponentPopupMenu(BTD);
            ARG17A.setName("ARG17A");
            panel7.add(ARG17A, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- TIDX18 ----
            TIDX18.setText("Affaire");
            TIDX18.setComponentPopupMenu(null);
            TIDX18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX18.setName("TIDX18");
            panel7.add(TIDX18, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- ARG27A ----
            ARG27A.setComponentPopupMenu(BTD2);
            ARG27A.setName("ARG27A");
            panel7.add(ARG27A, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- ARG18A ----
            ARG18A.setComponentPopupMenu(BTD);
            ARG18A.setName("ARG18A");
            panel7.add(ARG18A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- ARG34A ----
            ARG34A.setComponentPopupMenu(BTD);
            ARG34A.setName("ARG34A");
            panel7.add(ARG34A, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- ARG35A ----
            ARG35A.setComponentPopupMenu(BTD);
            ARG35A.setName("ARG35A");
            panel7.add(ARG35A, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- separator1 ----
            separator1.setComponentPopupMenu(null);
            separator1.setName("separator1");
            panel7.add(separator1, new GridBagConstraints(0, 2, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- ARG33A ----
            ARG33A.setComponentPopupMenu(BTD);
            ARG33A.setName("ARG33A");
            panel7.add(ARG33A, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
          }
          pnlGauche.add(panel7, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlGauche);
        
        // ======== pnllDroite ========
        {
          pnllDroite.setName("pnllDroite");
          pnllDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnllDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnllDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnllDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnllDroite.getLayout()).rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0E-4 };
          
          // ======== panel8 ========
          {
            panel8.setBorder(new TitledBorder("Recherche fournisseur"));
            panel8.setOpaque(false);
            panel8.setMinimumSize(new Dimension(533, 170));
            panel8.setPreferredSize(new Dimension(517, 170));
            panel8.setName("panel8");
            panel8.setLayout(new GridBagLayout());
            ((GridBagLayout) panel8.getLayout()).columnWidths = new int[] { 175, 254, 0 };
            ((GridBagLayout) panel8.getLayout()).rowHeights = new int[] { 26, 25, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) panel8.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) panel8.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0E-4 };
            
            // ---- WNOM ----
            WNOM.setComponentPopupMenu(BTD2);
            WNOM.setName("WNOM");
            panel8.add(WNOM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OBJ_67 ----
            OBJ_67.setText("Code postal");
            OBJ_67.setName("OBJ_67");
            panel8.add(OBJ_67, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_60 ----
            OBJ_60.setText("Nom");
            OBJ_60.setName("OBJ_60");
            panel8.add(OBJ_60, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WCDP ----
            WCDP.setComponentPopupMenu(BTD2);
            WCDP.setName("WCDP");
            panel8.add(WCDP, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OBJ_73 ----
            OBJ_73.setText("T\u00e9l\u00e9phone");
            OBJ_73.setName("OBJ_73");
            panel8.add(OBJ_73, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WTEL ----
            WTEL.setComponentPopupMenu(BTD2);
            WTEL.setName("WTEL");
            panel8.add(WTEL, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OBJ_79 ----
            OBJ_79.setText("Nom saisi");
            OBJ_79.setName("OBJ_79");
            panel8.add(OBJ_79, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- ARG39A ----
            ARG39A.setComponentPopupMenu(BTD2);
            ARG39A.setName("ARG39A");
            panel8.add(ARG39A, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
          }
          pnllDroite.add(panel8, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== panel4 ========
          {
            panel4.setBorder(new TitledBorder(""));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(new GridBagLayout());
            ((GridBagLayout) panel4.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) panel4.getLayout()).rowHeights = new int[] { 32, 32, 25, 0 };
            ((GridBagLayout) panel4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) panel4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- OBJ_32 ----
            OBJ_32.setText("Nature");
            OBJ_32.setName("OBJ_32");
            panel4.add(OBJ_32, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- ARG7A ----
            ARG7A.setModel(new DefaultComboBoxModel<>(new String[] { "Toutes", "Marchandises", "Frais", "Immobilisations" }));
            ARG7A.setComponentPopupMenu(null);
            ARG7A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG7A.setName("ARG7A");
            panel4.add(ARG7A, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_33 ----
            OBJ_33.setText("Facturation");
            OBJ_33.setName("OBJ_33");
            panel4.add(OBJ_33, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- ARG13A ----
            ARG13A.setModel(new DefaultComboBoxModel<>(new String[] { "Toutes", "Normale", "Export" }));
            ARG13A.setComponentPopupMenu(null);
            ARG13A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG13A.setName("ARG13A");
            panel4.add(ARG13A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- ARG12A ----
            ARG12A.setComponentPopupMenu(null);
            ARG12A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG12A.setName("ARG12A");
            panel4.add(ARG12A, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- SCAN12 ----
            SCAN12.setText("Inclus");
            SCAN12.setComponentPopupMenu(null);
            SCAN12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN12.setName("SCAN12");
            panel4.add(SCAN12, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- SCAN12_E ----
            SCAN12_E.setText("Exclu");
            SCAN12_E.setComponentPopupMenu(null);
            SCAN12_E.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN12_E.setName("SCAN12_E");
            panel4.add(SCAN12_E, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- SCAN12_T ----
            SCAN12_T.setText("Tous");
            SCAN12_T.setComponentPopupMenu(null);
            SCAN12_T.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN12_T.setName("SCAN12_T");
            panel4.add(SCAN12_T, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- OBJ_34 ----
            OBJ_34.setText("Type");
            OBJ_34.setName("OBJ_34");
            panel4.add(OBJ_34, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          pnllDroite.add(panel4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== panel9 ========
          {
            panel9.setBorder(new TitledBorder(""));
            panel9.setOpaque(false);
            panel9.setName("panel9");
            panel9.setLayout(new GridBagLayout());
            ((GridBagLayout) panel9.getLayout()).columnWidths = new int[] { 106, 0, 0 };
            ((GridBagLayout) panel9.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) panel9.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) panel9.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- TIDX20 ----
            TIDX20.setText("Cr\u00e9ation");
            TIDX20.setComponentPopupMenu(BTD);
            TIDX20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX20.setName("TIDX20");
            panel9.add(TIDX20, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateCreation ========
            {
              pnlDateCreation.setName("pnlDateCreation");
              pnlDateCreation.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateCreation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateCreation.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateCreation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDateCreation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- OBJ_89 ----
              OBJ_89.setText("Du");
              OBJ_89.setName("OBJ_89");
              pnlDateCreation.add(OBJ_89, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG20D ----
              ARG20D.setComponentPopupMenu(BTD2);
              ARG20D.setMinimumSize(new Dimension(110, 28));
              ARG20D.setPreferredSize(new Dimension(110, 28));
              ARG20D.setName("ARG20D");
              pnlDateCreation.add(ARG20D, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_90 ----
              OBJ_90.setText("au");
              OBJ_90.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_90.setName("OBJ_90");
              pnlDateCreation.add(OBJ_90, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PLA20D ----
              PLA20D.setComponentPopupMenu(BTD2);
              PLA20D.setMinimumSize(new Dimension(110, 28));
              PLA20D.setPreferredSize(new Dimension(110, 28));
              PLA20D.setName("PLA20D");
              pnlDateCreation.add(PLA20D, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            panel9.add(pnlDateCreation, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- TIDX21 ----
            TIDX21.setText("Validation");
            TIDX21.setComponentPopupMenu(BTD);
            TIDX21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX21.setName("TIDX21");
            panel9.add(TIDX21, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateValidation ========
            {
              pnlDateValidation.setName("pnlDateValidation");
              pnlDateValidation.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateValidation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateValidation.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateValidation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDateValidation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- OBJ_91 ----
              OBJ_91.setText("Du");
              OBJ_91.setName("OBJ_91");
              pnlDateValidation.add(OBJ_91, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG21D ----
              ARG21D.setComponentPopupMenu(BTD2);
              ARG21D.setMinimumSize(new Dimension(110, 28));
              ARG21D.setPreferredSize(new Dimension(110, 28));
              ARG21D.setName("ARG21D");
              pnlDateValidation.add(ARG21D, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_96 ----
              OBJ_96.setText("au");
              OBJ_96.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_96.setName("OBJ_96");
              pnlDateValidation.add(OBJ_96, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PLA21D ----
              PLA21D.setComponentPopupMenu(BTD2);
              PLA21D.setMinimumSize(new Dimension(110, 28));
              PLA21D.setPreferredSize(new Dimension(110, 28));
              PLA21D.setName("PLA21D");
              pnlDateValidation.add(PLA21D, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            panel9.add(pnlDateValidation, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- TIDX32 ----
            TIDX32.setText("R\u00e9ception pr\u00e9vue");
            TIDX32.setComponentPopupMenu(BTD);
            TIDX32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX32.setName("TIDX32");
            panel9.add(TIDX32, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateReceptionPrevue ========
            {
              pnlDateReceptionPrevue.setName("pnlDateReceptionPrevue");
              pnlDateReceptionPrevue.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateReceptionPrevue.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateReceptionPrevue.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateReceptionPrevue.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDateReceptionPrevue.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- OBJ_92 ----
              OBJ_92.setText("Du");
              OBJ_92.setName("OBJ_92");
              pnlDateReceptionPrevue.add(OBJ_92, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG32D ----
              ARG32D.setComponentPopupMenu(BTD2);
              ARG32D.setMinimumSize(new Dimension(110, 28));
              ARG32D.setPreferredSize(new Dimension(110, 28));
              ARG32D.setName("ARG32D");
              pnlDateReceptionPrevue.add(ARG32D, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_97 ----
              OBJ_97.setText("au");
              OBJ_97.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_97.setName("OBJ_97");
              pnlDateReceptionPrevue.add(OBJ_97, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PLA32D ----
              PLA32D.setComponentPopupMenu(BTD2);
              PLA32D.setMinimumSize(new Dimension(110, 28));
              PLA32D.setPreferredSize(new Dimension(110, 28));
              PLA32D.setName("PLA32D");
              pnlDateReceptionPrevue.add(PLA32D, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            panel9.add(pnlDateReceptionPrevue, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- TIDX22 ----
            TIDX22.setText("R\u00e9ception");
            TIDX22.setComponentPopupMenu(BTD);
            TIDX22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX22.setName("TIDX22");
            panel9.add(TIDX22, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateReception ========
            {
              pnlDateReception.setName("pnlDateReception");
              pnlDateReception.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateReception.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateReception.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateReception.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDateReception.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- OBJ_93 ----
              OBJ_93.setText("Du");
              OBJ_93.setName("OBJ_93");
              pnlDateReception.add(OBJ_93, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG22D ----
              ARG22D.setComponentPopupMenu(BTD2);
              ARG22D.setMinimumSize(new Dimension(110, 28));
              ARG22D.setPreferredSize(new Dimension(110, 28));
              ARG22D.setName("ARG22D");
              pnlDateReception.add(ARG22D, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_98 ----
              OBJ_98.setText("au");
              OBJ_98.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_98.setName("OBJ_98");
              pnlDateReception.add(OBJ_98, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PLA22D ----
              PLA22D.setComponentPopupMenu(BTD2);
              PLA22D.setMinimumSize(new Dimension(110, 28));
              PLA22D.setPreferredSize(new Dimension(110, 28));
              PLA22D.setName("PLA22D");
              pnlDateReception.add(PLA22D, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            panel9.add(pnlDateReception, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- TIDX23 ----
            TIDX23.setText("Facturation");
            TIDX23.setComponentPopupMenu(BTD);
            TIDX23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX23.setName("TIDX23");
            panel9.add(TIDX23, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateFacturation ========
            {
              pnlDateFacturation.setName("pnlDateFacturation");
              pnlDateFacturation.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateFacturation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateFacturation.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateFacturation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDateFacturation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- OBJ_94 ----
              OBJ_94.setText("Du");
              OBJ_94.setName("OBJ_94");
              pnlDateFacturation.add(OBJ_94, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG23D ----
              ARG23D.setComponentPopupMenu(BTD2);
              ARG23D.setMinimumSize(new Dimension(110, 28));
              ARG23D.setPreferredSize(new Dimension(110, 28));
              ARG23D.setName("ARG23D");
              pnlDateFacturation.add(ARG23D, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_99 ----
              OBJ_99.setText("au");
              OBJ_99.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_99.setName("OBJ_99");
              pnlDateFacturation.add(OBJ_99, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PLA23D ----
              PLA23D.setComponentPopupMenu(BTD2);
              PLA23D.setMinimumSize(new Dimension(110, 28));
              PLA23D.setPreferredSize(new Dimension(110, 28));
              PLA23D.setName("PLA23D");
              pnlDateFacturation.add(PLA23D, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            panel9.add(pnlDateFacturation, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- SCAN38 ----
            SCAN38.setText("Num\u00e9ro de rapprochement");
            SCAN38.setComponentPopupMenu(BTD);
            SCAN38.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN38.setName("SCAN38");
            panel9.add(SCAN38, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- ARG38N ----
            ARG38N.setComponentPopupMenu(BTD2);
            ARG38N.setMinimumSize(new Dimension(100, 28));
            ARG38N.setPreferredSize(new Dimension(100, 28));
            ARG38N.setName("ARG38N");
            panel9.add(ARG38N, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- TIDX31 ----
            TIDX31.setText("Montant HT");
            TIDX31.setComponentPopupMenu(BTD);
            TIDX31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX31.setName("TIDX31");
            panel9.add(TIDX31, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlMontant ========
            {
              pnlMontant.setName("pnlMontant");
              pnlMontant.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMontant.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlMontant.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlMontant.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlMontant.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- OBJ_119 ----
              OBJ_119.setText("De");
              OBJ_119.setName("OBJ_119");
              pnlMontant.add(OBJ_119, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG31N ----
              ARG31N.setComponentPopupMenu(BTD2);
              ARG31N.setPreferredSize(new Dimension(110, 28));
              ARG31N.setMinimumSize(new Dimension(110, 28));
              ARG31N.setName("ARG31N");
              pnlMontant.add(ARG31N, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_120 ----
              OBJ_120.setText("\u00e0");
              OBJ_120.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_120.setName("OBJ_120");
              pnlMontant.add(OBJ_120, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- PLA31N ----
              PLA31N.setComponentPopupMenu(BTD2);
              PLA31N.setPreferredSize(new Dimension(110, 28));
              PLA31N.setMinimumSize(new Dimension(110, 28));
              PLA31N.setName("PLA31N");
              pnlMontant.add(PLA31N, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            panel9.add(pnlMontant, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- TIDX14 ----
            TIDX14.setText("Devise");
            TIDX14.setComponentPopupMenu(BTD);
            TIDX14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX14.setName("TIDX14");
            panel9.add(TIDX14, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlDevise ========
            {
              pnlDevise.setName("pnlDevise");
              pnlDevise.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDevise.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlDevise.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDevise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlDevise.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- ARG14A ----
              ARG14A.setComponentPopupMenu(BTD);
              ARG14A.setMinimumSize(new Dimension(40, 28));
              ARG14A.setPreferredSize(new Dimension(40, 28));
              ARG14A.setName("ARG14A");
              pnlDevise.add(ARG14A, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_139 ----
              OBJ_139.setText("Avoir");
              OBJ_139.setName("OBJ_139");
              pnlDevise.add(OBJ_139, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG30A ----
              ARG30A.setComponentPopupMenu(BTD);
              ARG30A.setMinimumSize(new Dimension(40, 28));
              ARG30A.setPreferredSize(new Dimension(40, 28));
              ARG30A.setName("ARG30A");
              pnlDevise.add(ARG30A, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- SCAN30 ----
              SCAN30.setModel(new DefaultComboBoxModel<>(new String[] { "", "Inclus", "Exclus" }));
              SCAN30.setComponentPopupMenu(null);
              SCAN30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SCAN30.setName("SCAN30");
              pnlDevise.add(SCAN30, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            panel9.add(pnlDevise, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnllDroite.add(panel9, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnllDroite);
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(e -> OBJ_12ActionPerformed(e));
      BTD.add(OBJ_12);
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(e -> OBJ_11ActionPerformed(e));
      BTD.add(OBJ_11);
    }
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- OBJ_14 ----
      OBJ_14.setText("Aide en ligne");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(e -> OBJ_11ActionPerformed(e));
      BTD2.add(OBJ_14);
    }
    
    // ---- OBJ_31 ----
    OBJ_31.setText("Type de recherche");
    OBJ_31.setName("OBJ_31");
    
    // ---- ARG3A ----
    ARG3A.setModel(new DefaultComboBoxModel<>(new String[] { "Bons", "Factures" }));
    ARG3A.setComponentPopupMenu(null);
    ARG3A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    ARG3A.setName("ARG3A");
    
    // ---- ARG37A2 ----
    ARG37A2.setComponentPopupMenu(BTD2);
    ARG37A2.setMinimumSize(new Dimension(210, 28));
    ARG37A2.setMaximumSize(new Dimension(210, 28));
    ARG37A2.setPreferredSize(new Dimension(210, 28));
    ARG37A2.setName("ARG37A2");
    
    // ======== pnlDateCreation9 ========
    {
      pnlDateCreation9.setName("pnlDateCreation9");
      pnlDateCreation9.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlDateCreation9.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlDateCreation9.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlDateCreation9.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlDateCreation9.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
    }
    
    // ---- RBC_GRP ----
    ButtonGroup RBC_GRP = new ButtonGroup();
    RBC_GRP.add(TIDX2);
    RBC_GRP.add(TIDX36);
    RBC_GRP.add(TIDX5);
    RBC_GRP.add(TIDX9);
    RBC_GRP.add(TIDX6);
    RBC_GRP.add(TIDX10);
    RBC_GRP.add(TIDX26);
    RBC_GRP.add(TIDX15);
    RBC_GRP.add(TIDX16);
    RBC_GRP.add(TIDX33);
    RBC_GRP.add(TIDX34);
    RBC_GRP.add(TIDX35);
    RBC_GRP.add(TIDX19);
    RBC_GRP.add(TIDX28);
    RBC_GRP.add(TIDX17);
    RBC_GRP.add(TIDX18);
    RBC_GRP.add(TIDX20);
    RBC_GRP.add(TIDX21);
    RBC_GRP.add(TIDX32);
    RBC_GRP.add(TIDX22);
    RBC_GRP.add(TIDX23);
    RBC_GRP.add(SCAN38);
    RBC_GRP.add(TIDX31);
    RBC_GRP.add(TIDX14);
    
    // ---- SCAN12_GRP ----
    SCAN12_GRP.add(SCAN12);
    SCAN12_GRP.add(SCAN12_E);
    SCAN12_GRP.add(SCAN12_T);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_50;
  private JLabel OBJ_45;
  private XRiCalendrier WDAT1X;
  private XRiCheckBox VERR;
  private XRiTextField INDNUM;
  private JLabel OBJ_47;
  private XRiTextField INDETB;
  private XRiTextField INDSUF;
  private JPanel p_tete_droite;
  private JLabel lb_requete;
  private SNPanelFond p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private JPanel panel5;
  private JPanel pnlEtatBon;
  private XRiComboBox ARG4A;
  private XRiComboBox PLA4A;
  private JLabel OBJ_55;
  private JLabel OBJ_58;
  private JCheckBox tous;
  private JPanel pnlEtatFacture;
  private XRiComboBox ARG4AF;
  private XRiComboBox PLA4AF;
  private JLabel OBJ_56;
  private JCheckBox tousF;
  private JLabel OBJ_59;
  private JPanel panel52;
  private XRiRadioButton TIDX2;
  private XRiTextField ARG2N;
  private XRiTextField PLA2N;
  private JLabel OBJ_65;
  private JPanel panel2;
  private XRiRadioButton TIDX36;
  private XRiTextField ARG36N;
  private XRiTextField PLA36N;
  private JLabel OBJ_71;
  private JPanel panel53;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG5A;
  private JPanel panel54;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDX6;
  private XRiTextField ARG9NX;
  private XRiTextField ARG6A;
  private XRiTextField ARG8N;
  private JPanel panel55;
  private XRiRadioButton TIDX10;
  private JLabel OBJ_95;
  private XRiTextField ARG10A;
  private XRiTextField ARG11A;
  private XRiTextField ARGDOC;
  private JLabel OBJ_66;
  private XRiCheckBox DOCSPE;
  private JLabel lbArticle;
  private XRiTextField ARG37A;
  private JPanel panel6;
  private JPanel panel61;
  private XRiRadioButton TIDX26;
  private XRiTextField ARG26A;
  private JPanel panel62;
  private XRiRadioButton TIDX15;
  private XRiTextField ARG15A;
  private XRiRadioButton TIDX16;
  private XRiTextField ARG16A;
  private JPanel panel7;
  private XRiRadioButton TIDX33;
  private XRiRadioButton TIDX34;
  private XRiRadioButton TIDX35;
  private XRiRadioButton TIDX19;
  private XRiRadioButton TIDX28;
  private XRiTextField ARG28A;
  private XRiRadioButton TIDX17;
  private XRiTextField ARG17A;
  private XRiRadioButton TIDX18;
  private XRiTextField ARG27A;
  private XRiTextField ARG18A;
  private XRiTextField ARG34A;
  private XRiTextField ARG35A;
  private JSeparator separator1;
  private XRiTextField ARG33A;
  private SNPanel pnllDroite;
  private JPanel panel8;
  private XRiTextField WNOM;
  private JLabel OBJ_67;
  private JLabel OBJ_60;
  private XRiTextField WCDP;
  private JLabel OBJ_73;
  private XRiTextField WTEL;
  private JLabel OBJ_79;
  private XRiTextField ARG39A;
  private JPanel panel4;
  private JLabel OBJ_32;
  private XRiComboBox ARG7A;
  private JLabel OBJ_33;
  private XRiComboBox ARG13A;
  private XRiComboBox ARG12A;
  private XRiRadioButton SCAN12;
  private XRiRadioButton SCAN12_E;
  private XRiRadioButton SCAN12_T;
  private JLabel OBJ_34;
  private JPanel panel9;
  private XRiRadioButton TIDX20;
  private SNPanel pnlDateCreation;
  private JLabel OBJ_89;
  private XRiCalendrier ARG20D;
  private JLabel OBJ_90;
  private XRiCalendrier PLA20D;
  private XRiRadioButton TIDX21;
  private SNPanel pnlDateValidation;
  private JLabel OBJ_91;
  private XRiCalendrier ARG21D;
  private JLabel OBJ_96;
  private XRiCalendrier PLA21D;
  private XRiRadioButton TIDX32;
  private SNPanel pnlDateReceptionPrevue;
  private JLabel OBJ_92;
  private XRiCalendrier ARG32D;
  private JLabel OBJ_97;
  private XRiCalendrier PLA32D;
  private XRiRadioButton TIDX22;
  private SNPanel pnlDateReception;
  private JLabel OBJ_93;
  private XRiCalendrier ARG22D;
  private JLabel OBJ_98;
  private XRiCalendrier PLA22D;
  private XRiRadioButton TIDX23;
  private SNPanel pnlDateFacturation;
  private JLabel OBJ_94;
  private XRiCalendrier ARG23D;
  private JLabel OBJ_99;
  private XRiCalendrier PLA23D;
  private XRiRadioButton SCAN38;
  private XRiTextField ARG38N;
  private XRiRadioButton TIDX31;
  private SNPanel pnlMontant;
  private JLabel OBJ_119;
  private XRiTextField ARG31N;
  private JLabel OBJ_120;
  private XRiTextField PLA31N;
  private XRiRadioButton TIDX14;
  private SNPanel pnlDevise;
  private XRiTextField ARG14A;
  private JLabel OBJ_139;
  private XRiTextField ARG30A;
  private XRiComboBox SCAN30;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_14;
  private JLabel OBJ_31;
  private XRiComboBox ARG3A;
  private XRiTextField ARG37A2;
  private SNPanel pnlDateCreation9;
  private ButtonGroup SCAN12_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
