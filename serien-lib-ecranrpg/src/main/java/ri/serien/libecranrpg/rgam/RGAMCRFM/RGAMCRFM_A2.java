
package ri.serien.libecranrpg.rgam.RGAMCRFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class RGAMCRFM_A2 extends SNPanelEcranRPG implements ioFrame {
  // Constantes
  private static final String BOUTON_CREER = "Créer";
  private static final String BOUTON_CHOISIR = "Choisir";
  private static final String BOUTON_MODIFIER = "Modifier";
  private static final String BOUTON_EXPORTATION_TABLEUR = "Exporter";
  
  // Variables
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1L", };
  private String[][] _WTP01_Data = { { "LDL01", }, { "LDL02", }, { "LDL03", }, { "LDL04", }, { "LDL05", }, { "LDL06", }, { "LDL07", },
      { "LDL08", }, { "LDL09", }, { "LDL10", }, { "LDL11", }, { "LDL12", }, { "LDL13", }, { "LDL14", }, { "LDL15", }, };
  private int[] _WTP01_Width = { 541, };
  
  public RGAMCRFM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    // Barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_CREER, 'E', true);
    snBarreBouton.ajouterBouton(BOUTON_CHOISIR, 'H', true);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER, 'M', true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTATION_TABLEUR, 'E', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPGM@")).trim());
    lbRegroupement.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBREF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    rafraichirEtablissement();
    rafraichirFournisseur();
    rafraichirBoutons();
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPGM@"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snFournisseur.renseignerChampRPG(lexique, "WCOL", "WFRS");
  }
  
  /**
   * Initialise le composant établissement.
   */
  private void rafraichirEtablissement() {
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    snEtablissement.setEnabled(false);
  }
  
  /**
   * Initialise le composant fournisseur suivant l'établissement.
   */
  private void rafraichirFournisseur() {
    boolean venteComptoir = lexique.isTrue("92");
    
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(false);
    snFournisseur.setSelectionParChampRPG(lexique, "WCOL", "WFRS");
    snFournisseur.setEnabled(false);
  }
  
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_CREER, lexique.getMode() != Lexical.MODE_CREATION);
    snBarreBouton.activerBouton(BOUTON_CHOISIR, WTP01.getSelectedRows().length > 0);
    snBarreBouton.activerBouton(BOUTON_MODIFIER, WTP01.getSelectedRows().length > 0);
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_CREER)) {
        lexique.HostScreenSendKey(this, "F13");
      }
      else if (pSNBouton.isBouton(BOUTON_CHOISIR)) {
        WTP01.setValeurTop("1");
        lexique.HostScreenSendKey(this, "Enter");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER)) {
        WTP01.setValeurTop("2");
        lexique.HostScreenSendKey(this, "Enter");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTATION_TABLEUR)) {
        lexique.HostScreenSendKey(this, "F9");
        lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPGM").trim());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    rafraichirBoutons();
  }
  
  private void btnChoixActionPerformed(ActionEvent e) {
    try {
      lexique.HostCursorPut("REFRECH");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btDebutListeActionPerformed(ActionEvent e) {
    try {
      lexique.HostFieldPutData("WSUIS", 0, "D");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PAGEDOWN");
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void btFinListeActionPerformed(ActionEvent e) {
    try {
      lexique.HostFieldPutData("WSUIS", 0, "F");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlReappro = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    lbRegroupement = new SNLabelChamp();
    REFRECH = new XRiTextField();
    pnlListeCriteres = new SNPanelTitre();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    sNPanel1 = new SNPanel();
    btDebutListe = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    btFinListe = new JButton();
    BTD = new JPopupMenu();
    btnChoix = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TITPGM@ @SOUTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlReappro ========
      {
        pnlReappro.setName("pnlReappro");
        pnlReappro.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlReappro.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlReappro.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlReappro.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlReappro.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setMaximumSize(new Dimension(250, 30));
        lbEtablissement.setMinimumSize(new Dimension(250, 30));
        lbEtablissement.setPreferredSize(new Dimension(250, 30));
        lbEtablissement.setName("lbEtablissement");
        pnlReappro.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setName("snEtablissement");
        pnlReappro.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbFournisseur ----
        lbFournisseur.setText("Fournisseur");
        lbFournisseur.setName("lbFournisseur");
        pnlReappro.add(lbFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snFournisseur ----
        snFournisseur.setName("snFournisseur");
        pnlReappro.add(snFournisseur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbRegroupement ----
        lbRegroupement.setText("@LIBREF@");
        lbRegroupement.setName("lbRegroupement");
        pnlReappro.add(lbRegroupement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- REFRECH ----
        REFRECH.setComponentPopupMenu(BTD);
        REFRECH.setName("REFRECH");
        pnlReappro.add(REFRECH, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlReappro,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlListeCriteres ========
      {
        pnlListeCriteres.setName("pnlListeCriteres");
        pnlListeCriteres.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlListeCriteres.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlListeCriteres.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlListeCriteres.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlListeCriteres.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setComponentPopupMenu(BTD);
          SCROLLPANE_LIST.setPreferredSize(new Dimension(1000, 270));
          SCROLLPANE_LIST.setMinimumSize(new Dimension(1000, 270));
          SCROLLPANE_LIST.setBackground(Color.white);
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");
          
          // ---- WTP01 ----
          WTP01.setComponentPopupMenu(null);
          WTP01.setMinimumSize(new Dimension(1000, 240));
          WTP01.setPreferredSize(new Dimension(1000, 240));
          WTP01.setPreferredScrollableViewportSize(new Dimension(450, 450));
          WTP01.setMaximumSize(new Dimension(2147483647, 235));
          WTP01.setName("WTP01");
          WTP01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTP01MouseClicked(e);
            }
          });
          SCROLLPANE_LIST.setViewportView(WTP01);
        }
        pnlListeCriteres.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== sNPanel1 ========
        {
          sNPanel1.setName("sNPanel1");
          sNPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- btDebutListe ----
          btDebutListe.setText("");
          btDebutListe.setToolTipText("D\u00e9but de la liste");
          btDebutListe.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          btDebutListe.setMaximumSize(new Dimension(25, 30));
          btDebutListe.setMinimumSize(new Dimension(25, 30));
          btDebutListe.setPreferredSize(new Dimension(25, 30));
          btDebutListe.setName("btDebutListe");
          btDebutListe.addActionListener(e -> btDebutListeActionPerformed(e));
          sNPanel1.add(btDebutListe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setMaximumSize(new Dimension(25, 100));
          BT_PGUP.setMinimumSize(new Dimension(25, 100));
          BT_PGUP.setPreferredSize(new Dimension(25, 100));
          BT_PGUP.setName("BT_PGUP");
          sNPanel1.add(BT_PGUP, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setMaximumSize(new Dimension(25, 100));
          BT_PGDOWN.setMinimumSize(new Dimension(25, 100));
          BT_PGDOWN.setPreferredSize(new Dimension(25, 100));
          BT_PGDOWN.setName("BT_PGDOWN");
          sNPanel1.add(BT_PGDOWN, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- btFinListe ----
          btFinListe.setText("");
          btFinListe.setToolTipText("Fin de la liste");
          btFinListe.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          btFinListe.setMaximumSize(new Dimension(25, 30));
          btFinListe.setMinimumSize(new Dimension(25, 30));
          btFinListe.setPreferredSize(new Dimension(25, 30));
          btFinListe.setName("btFinListe");
          btFinListe.addActionListener(e -> btFinListeActionPerformed(e));
          sNPanel1.add(btFinListe, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlListeCriteres.add(sNPanel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlListeCriteres,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- btnChoix ----
      btnChoix.setText("Choix possibles");
      btnChoix.setFont(new Font("sansserif", Font.PLAIN, 14));
      btnChoix.setName("btnChoix");
      btnChoix.addActionListener(e -> btnChoixActionPerformed(e));
      BTD.add(btnChoix);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlReappro;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNLabelChamp lbRegroupement;
  private XRiTextField REFRECH;
  private SNPanelTitre pnlListeCriteres;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private SNPanel sNPanel1;
  private JButton btDebutListe;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton btFinListe;
  private JPopupMenu BTD;
  private JMenuItem btnChoix;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
