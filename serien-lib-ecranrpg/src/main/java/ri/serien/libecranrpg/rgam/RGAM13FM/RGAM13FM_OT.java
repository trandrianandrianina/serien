
package ri.serien.libecranrpg.rgam.RGAM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class RGAM13FM_OT extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public RGAM13FM_OT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    NBBON.setEnabled(lexique.isPresent("NBBON"));
    TOT3.setVisible(lexique.isPresent("TOT3"));
    TOTX3.setVisible(lexique.isPresent("TOTX3"));
    TOT1.setVisible(lexique.isPresent("TOT1"));
    TOTX1.setVisible(lexique.isPresent("TOTX1"));
    OBJ_15.setVisible(lexique.isPresent("TOT3"));
    
    if (lexique.isTrue("75")) {
      OBJ_18.setText("Nombre d'articles");
    }
    else {
      OBJ_18.setText("Nombre de bons");
    }
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Recherche des commandes d'achats"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    OBJ_15 = new JLabel();
    OBJ_12 = new JLabel();
    TOTX1 = new XRiTextField();
    TOT1 = new XRiTextField();
    TOTX3 = new XRiTextField();
    TOT3 = new XRiTextField();
    OBJ_18 = new JLabel();
    NBBON = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(490, 120));
    setPreferredSize(new Dimension(490, 120));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //---- OBJ_15 ----
        OBJ_15.setText("Montant total TTC");
        OBJ_15.setName("OBJ_15");

        //---- OBJ_12 ----
        OBJ_12.setText("Montant total H.T");
        OBJ_12.setName("OBJ_12");

        //---- TOTX1 ----
        TOTX1.setName("TOTX1");

        //---- TOT1 ----
        TOT1.setName("TOT1");

        //---- TOTX3 ----
        TOTX3.setName("TOTX3");

        //---- TOT3 ----
        TOT3.setName("TOT3");

        //---- OBJ_18 ----
        OBJ_18.setText("Nombre de bons");
        OBJ_18.setName("OBJ_18");

        //---- NBBON ----
        NBBON.setName("NBBON");

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(OBJ_12, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
                  .addGap(31, 31, 31)
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addComponent(TOT1, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TOTX1, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(OBJ_15, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE)
                  .addGap(26, 26, 26)
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addComponent(TOT3, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TOTX3, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
                  .addGap(33, 33, 33)
                  .addComponent(NBBON, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(OBJ_12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addComponent(TOT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(TOTX1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(2, 2, 2)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(OBJ_15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addComponent(TOT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(TOTX3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(2, 2, 2)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
                .addComponent(NBBON, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JLabel OBJ_15;
  private JLabel OBJ_12;
  private XRiTextField TOTX1;
  private XRiTextField TOT1;
  private XRiTextField TOTX3;
  private XRiTextField TOT3;
  private JLabel OBJ_18;
  private XRiTextField NBBON;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
