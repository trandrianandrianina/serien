//$$david$$ ££03/01/11££ -> tests et modifs

package ri.serien.libecranrpg.rgam.RGAM04FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGAM04FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  private boolean criteresAvances = true;
  private boolean initCriteres = false;
  
  public RGAM04FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDX33.setValeurs("33", "RBC");
    TIDX16.setValeurs("16", "RBC");
    TIDX21.setValeurs("21", "RBC");
    TIDX12.setValeurs("12", "RBC");
    TIDX11.setValeurs("11", "RBC");
    TIDX7.setValeurs("7", "RBC");
    TIDX24.setValeurs("24", "RBC");
    TIDX28.setValeurs("28", "RBC");
    TIDX27.setValeurs("27", "RBC");
    TIDX26.setValeurs("26", "RBC");
    TIDX10.setValeurs("10", "RBC");
    TIDX9.setValeurs("9", "RBC");
    TIDX14.setValeurs("14", "RBC");
    TIDX6.setValeurs("6", "RBC");
    TIDX5.setValeurs("5", "RBC");
    TIDX8.setValeurs("8", "RBC");
    TIDX2.setValeurs("2", "RBC");
    TIDX13.setValeurs("13", "RBC");
    SCAN39.setValeursSelection("S", " ");
    SCAN40.setValeursSelection("S", " ");
    SCAN38.setValeursSelection("S", " ");
    SCAN37.setValeursSelection("S", " ");
    SCAN36.setValeursSelection("S", " ");
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation
        .setText(lexique.TranslationTable(interpreteurD.analyseExpression("Recherche multi-critères de fournisseurs @LIBPG@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    TIDX9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL01@")).trim());
    TIDX10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL02@")).trim());
    TIDX26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL03@")).trim());
    TIDX27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL04@")).trim());
    TIDX28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL05@")).trim());
    OBJ_134.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP01@")).trim());
    WZP02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP02@")).trim());
    WZP03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP03@")).trim());
    WZP04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP04@")).trim());
    WZP01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZP05@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    barre_dupl.setVisible(lexique.isTrue("56"));
    ARG28A.setVisible(!lexique.HostFieldGetData("ZPL05").trim().isEmpty());
    ARG27A.setVisible(!lexique.HostFieldGetData("ZPL04").trim().isEmpty());
    ARG26A.setVisible(!lexique.HostFieldGetData("ZPL03").trim().isEmpty());
    ARG10A.setVisible(!lexique.HostFieldGetData("ZPL02").trim().isEmpty());
    ARG9A.setVisible(!lexique.HostFieldGetData("ZPL01").trim().isEmpty());
    ARG8A.setEnabled(lexique.isPresent("ARG8A"));
    ARG40A.setVisible(!lexique.HostFieldGetData("WZP05").trim().isEmpty());
    SCAN40.setVisible(!lexique.HostFieldGetData("WZP05").trim().isEmpty());
    ARG39A.setVisible(!lexique.HostFieldGetData("WZP04").trim().isEmpty());
    SCAN39.setVisible(!lexique.HostFieldGetData("WZP04").trim().isEmpty());
    ARG38A.setVisible(!lexique.HostFieldGetData("WZP03").trim().isEmpty());
    SCAN38.setVisible(!lexique.HostFieldGetData("WZP03").trim().isEmpty());
    ARG37A.setVisible(!lexique.HostFieldGetData("WZP02").trim().isEmpty());
    SCAN37.setVisible(!lexique.HostFieldGetData("WZP02").trim().isEmpty());
    ARG36A.setVisible(!lexique.HostFieldGetData("WZP01").trim().isEmpty());
    SCAN36.setVisible(!lexique.HostFieldGetData("WZP01").trim().isEmpty());
    TIDX28.setVisible(!lexique.HostFieldGetData("ZPL01").trim().isEmpty());
    TIDX27.setVisible(!lexique.HostFieldGetData("ZPL01").trim().isEmpty());
    TIDX26.setVisible(!lexique.HostFieldGetData("ZPL01").trim().isEmpty());
    TIDX10.setVisible(!lexique.HostFieldGetData("ZPL01").trim().isEmpty());
    TIDX9.setVisible(!lexique.HostFieldGetData("ZPL01").trim().isEmpty());
    lb_requete.setVisible(interpreteurD.analyseExpression("@AFFREQ@").equals("1"));
    
    initCriteres = false;
    criteresAvances = lexique.isTrue("61");
    changerModeRecherche();
    
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void changerModeRecherche() {
    if (initCriteres) {
      criteresAvances = !criteresAvances;
    }
    else {
      initCriteres = true;
    }
    
    panel2.setVisible(criteresAvances);
    panel3.setVisible(criteresAvances);
    panel4.setVisible(criteresAvances);
    panel7.setVisible(criteresAvances);
    if (panel4.isVisible()) {
      panel4.setVisible(ARG9A.isVisible());
    }
    if (panel7.isVisible()) {
      panel7.setVisible(ARG36A.isVisible());
    }
    
    // Affichage dynamique des blocs
    if (criteresAvances) {
      if (panel4.isVisible()) {
        panel7.setLocation(270, 350);
      }
      else {
        panel7.setLocation(10, 350);
      }
      
      riSousMenu_bt14.setText("Critères principaux");
      lexique.SetOn(61);
    }
    else {
      riSousMenu_bt14.setText("Tous les critères");
      lexique.SetOff(61);
    }
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (interpreteurD.analyseExpression("@AFFREQ@").equalsIgnoreCase("1")) {
      lexique.HostFieldPutData("AFFREQ", 0, " ");
      lb_requete.setVisible(false);
    }
    else {
      lexique.HostFieldPutData("AFFREQ", 0, "1");
      lb_requete.setVisible(true);
    }
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    changerModeRecherche();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_43 = new JLabel();
    OBJ_45 = new JLabel();
    INDFRS = new XRiTextField();
    INDETB = new XRiTextField();
    INDCOL = new XRiTextField();
    p_tete_droite = new JPanel();
    lb_requete = new JLabel();
    barre_dupl = new JMenuBar();
    p_tete_dupl = new JPanel();
    OBJ_44 = new JLabel();
    OBJ_46 = new JLabel();
    WIFRS3 = new XRiTextField();
    WIETB3 = new XRiTextField();
    WICOL3 = new XRiTextField();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel5 = new JPanel();
    panel1 = new JPanel();
    TIDX13 = new XRiRadioButton();
    ARG13A = new XRiTextField();
    SCAN19 = new XRiTextField();
    ARG19A = new XRiTextField();
    OBJ_99 = new JLabel();
    ARG3N = new XRiTextField();
    ARG2N = new XRiTextField();
    TIDX2 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    TIDX14 = new XRiRadioButton();
    ARG8A = new XRiTextField();
    ARG4A = new XRiTextField();
    ARG5N = new XRiTextField();
    ARG6A = new XRiTextField();
    ARG14A = new XRiTextField();
    panel3 = new JPanel();
    OBJ_119 = new JLabel();
    WBON = new XRiTextField();
    OBJ_121 = new JLabel();
    ARG32A = new XRiTextField();
    panel4 = new JPanel();
    TIDX9 = new XRiRadioButton();
    TIDX10 = new XRiRadioButton();
    ARG9A = new XRiTextField();
    ARG10A = new XRiTextField();
    TIDX26 = new XRiRadioButton();
    TIDX27 = new XRiRadioButton();
    TIDX28 = new XRiRadioButton();
    ARG26A = new XRiTextField();
    ARG27A = new XRiTextField();
    ARG28A = new XRiTextField();
    panel2 = new JPanel();
    TIDX24 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    TIDX11 = new XRiRadioButton();
    TIDX12 = new XRiRadioButton();
    TIDX21 = new XRiRadioButton();
    TIDX16 = new XRiRadioButton();
    TIDX33 = new XRiRadioButton();
    OBJ_109 = new JLabel();
    OBJ_111 = new JLabel();
    ARG31A = new XRiTextField();
    ARG34A = new XRiTextField();
    ARG33A = new XRiTextField();
    ARG16A = new XRiTextField();
    ARG17A = new XRiTextField();
    ARG12A = new XRiTextField();
    ARG11A = new XRiTextField();
    ARG7A = new XRiTextField();
    ARG24A = new XRiTextField();
    ARG25A = new XRiTextField();
    ARG21D = new XRiCalendrier();
    TIDX23 = new XRiRadioButton();
    ARG22N = new XRiTextField();
    ARG23N = new XRiTextField();
    panel7 = new JPanel();
    OBJ_134 = new JLabel();
    ARG36A = new XRiTextField();
    SCAN36 = new XRiCheckBox();
    WZP02 = new JLabel();
    ARG37A = new XRiTextField();
    SCAN37 = new XRiCheckBox();
    WZP03 = new JLabel();
    ARG38A = new XRiTextField();
    SCAN38 = new XRiCheckBox();
    ARG39A = new XRiTextField();
    WZP04 = new JLabel();
    WZP01 = new JLabel();
    ARG40A = new XRiTextField();
    SCAN40 = new XRiCheckBox();
    SCAN39 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    BTDA = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(1050, 650));
    setPreferredSize(new Dimension(1050, 650));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Recherche multi-crit\u00e8res de fournisseurs @LIBPG@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
          
          // ---- OBJ_43 ----
          OBJ_43.setText("Etablissement");
          OBJ_43.setName("OBJ_43");
          p_tete_gauche.add(OBJ_43);
          OBJ_43.setBounds(5, 5, 102, 20);
          
          // ---- OBJ_45 ----
          OBJ_45.setText("Fournisseur");
          OBJ_45.setName("OBJ_45");
          p_tete_gauche.add(OBJ_45);
          OBJ_45.setBounds(190, 6, 78, 18);
          
          // ---- INDFRS ----
          INDFRS.setToolTipText("Num\u00e9ro fournisseur");
          INDFRS.setComponentPopupMenu(null);
          INDFRS.setName("INDFRS");
          p_tete_gauche.add(INDFRS);
          INDFRS.setBounds(305, 1, 60, INDFRS.getPreferredSize().height);
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(115, 1, 40, INDETB.getPreferredSize().height);
          
          // ---- INDCOL ----
          INDCOL.setToolTipText("Collectif");
          INDCOL.setComponentPopupMenu(null);
          INDCOL.setName("INDCOL");
          p_tete_gauche.add(INDCOL);
          INDCOL.setBounds(280, 1, 22, INDCOL.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(200, 24));
          p_tete_droite.setMinimumSize(new Dimension(200, 24));
          p_tete_droite.setName("p_tete_droite");
          
          // ---- lb_requete ----
          lb_requete.setText("Affichage requ\u00eate SQL");
          lb_requete.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_requete.setFont(lb_requete.getFont().deriveFont(lb_requete.getFont().getStyle() | Font.BOLD));
          lb_requete.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_requete.setName("lb_requete");
          
          GroupLayout p_tete_droiteLayout = new GroupLayout(p_tete_droite);
          p_tete_droite.setLayout(p_tete_droiteLayout);
          p_tete_droiteLayout.setHorizontalGroup(p_tete_droiteLayout.createParallelGroup()
              .addGroup(p_tete_droiteLayout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(lb_requete, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE)));
          p_tete_droiteLayout.setVerticalGroup(
              p_tete_droiteLayout.createParallelGroup().addComponent(lb_requete, GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
      
      // ======== barre_dupl ========
      {
        barre_dupl.setMinimumSize(new Dimension(111, 34));
        barre_dupl.setPreferredSize(new Dimension(111, 34));
        barre_dupl.setName("barre_dupl");
        
        // ======== p_tete_dupl ========
        {
          p_tete_dupl.setOpaque(false);
          p_tete_dupl.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_dupl.setPreferredSize(new Dimension(700, 0));
          p_tete_dupl.setMinimumSize(new Dimension(700, 0));
          p_tete_dupl.setName("p_tete_dupl");
          p_tete_dupl.setLayout(null);
          
          // ---- OBJ_44 ----
          OBJ_44.setText("Par duplication de ");
          OBJ_44.setFont(OBJ_44.getFont().deriveFont(OBJ_44.getFont().getStyle() | Font.BOLD));
          OBJ_44.setName("OBJ_44");
          p_tete_dupl.add(OBJ_44);
          OBJ_44.setBounds(5, 5, 110, 20);
          
          // ---- OBJ_46 ----
          OBJ_46.setText("Fournisseur");
          OBJ_46.setName("OBJ_46");
          p_tete_dupl.add(OBJ_46);
          OBJ_46.setBounds(190, 6, 78, 18);
          
          // ---- WIFRS3 ----
          WIFRS3.setToolTipText("Num\u00e9ro fournisseur");
          WIFRS3.setComponentPopupMenu(null);
          WIFRS3.setName("WIFRS3");
          p_tete_dupl.add(WIFRS3);
          WIFRS3.setBounds(305, 1, 60, WIFRS3.getPreferredSize().height);
          
          // ---- WIETB3 ----
          WIETB3.setComponentPopupMenu(null);
          WIETB3.setName("WIETB3");
          p_tete_dupl.add(WIETB3);
          WIETB3.setBounds(115, 1, 40, WIETB3.getPreferredSize().height);
          
          // ---- WICOL3 ----
          WICOL3.setToolTipText("Collectif");
          WICOL3.setComponentPopupMenu(null);
          WICOL3.setName("WICOL3");
          p_tete_dupl.add(WICOL3);
          WICOL3.setBounds(280, 1, 22, WICOL3.getPreferredSize().height);
        }
        barre_dupl.add(p_tete_dupl);
      }
      p_nord.add(barre_dupl);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 340));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Tous les crit\u00e8res");
              riSousMenu_bt14.setToolTipText("Tous les crit\u00e8res");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Visualisation requ\u00eate");
              riSousMenu_bt7.setToolTipText("Visualisation requ\u00eate");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setPreferredSize(new Dimension(1200, 585));
        p_centrage.setMinimumSize(new Dimension(1200, 585));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(856, 560));
          p_contenu.setName("p_contenu");
          
          // ======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);
            
            // ======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("Crit\u00e8res principaux"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);
              
              // ---- TIDX13 ----
              TIDX13.setText("Code alphab\u00e9tique");
              TIDX13.setComponentPopupMenu(null);
              TIDX13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX13.setToolTipText("Tri sur cet argument");
              TIDX13.setName("TIDX13");
              panel1.add(TIDX13);
              TIDX13.setBounds(20, 32, 158, 20);
              
              // ---- ARG13A ----
              ARG13A.setComponentPopupMenu(BTD2);
              ARG13A.setName("ARG13A");
              panel1.add(ARG13A);
              ARG13A.setBounds(198, 28, 167, ARG13A.getPreferredSize().height);
              
              // ---- SCAN19 ----
              SCAN19.setComponentPopupMenu(BTD2);
              SCAN19.setName("SCAN19");
              panel1.add(SCAN19);
              SCAN19.setBounds(341, 54, 24, SCAN19.getPreferredSize().height);
              
              // ---- ARG19A ----
              ARG19A.setComponentPopupMenu(BTD2);
              ARG19A.setName("ARG19A");
              panel1.add(ARG19A);
              ARG19A.setBounds(313, 54, 24, ARG19A.getPreferredSize().height);
              
              // ---- OBJ_99 ----
              OBJ_99.setText("LIT");
              OBJ_99.setName("OBJ_99");
              panel1.add(OBJ_99);
              OBJ_99.setBounds(287, 58, 22, 20);
              
              // ---- ARG3N ----
              ARG3N.setComponentPopupMenu(BTD2);
              ARG3N.setHorizontalAlignment(SwingConstants.RIGHT);
              ARG3N.setName("ARG3N");
              panel1.add(ARG3N);
              ARG3N.setBounds(223, 54, 58, ARG3N.getPreferredSize().height);
              
              // ---- ARG2N ----
              ARG2N.setComponentPopupMenu(BTD2);
              ARG2N.setName("ARG2N");
              panel1.add(ARG2N);
              ARG2N.setBounds(198, 54, 24, ARG2N.getPreferredSize().height);
              
              // ---- TIDX2 ----
              TIDX2.setText("Num\u00e9ro fournisseur");
              TIDX2.setComponentPopupMenu(null);
              TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX2.setToolTipText("Tri sur cet argument");
              TIDX2.setName("TIDX2");
              panel1.add(TIDX2);
              TIDX2.setBounds(20, 58, 158, 20);
              
              // ---- TIDX8 ----
              TIDX8.setText("Num\u00e9ro de t\u00e9l\u00e9phone");
              TIDX8.setComponentPopupMenu(null);
              TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX8.setToolTipText("Tri sur cet argument");
              TIDX8.setName("TIDX8");
              panel1.add(TIDX8);
              TIDX8.setBounds(20, 84, 160, 20);
              
              // ---- TIDX5 ----
              TIDX5.setText("Code pays / code postal");
              TIDX5.setComponentPopupMenu(null);
              TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX5.setToolTipText("Tri sur cet argument");
              TIDX5.setName("TIDX5");
              panel1.add(TIDX5);
              TIDX5.setBounds(20, 110, 175, 20);
              
              // ---- TIDX6 ----
              TIDX6.setText("Cat\u00e9gorie");
              TIDX6.setComponentPopupMenu(null);
              TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX6.setToolTipText("Tri sur cet argument");
              TIDX6.setName("TIDX6");
              panel1.add(TIDX6);
              TIDX6.setBounds(20, 136, 84, 20);
              
              // ---- TIDX14 ----
              TIDX14.setText("Code devise");
              TIDX14.setComponentPopupMenu(null);
              TIDX14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX14.setToolTipText("Tri sur cet argument");
              TIDX14.setName("TIDX14");
              panel1.add(TIDX14);
              TIDX14.setBounds(20, 162, 158, 20);
              
              // ---- ARG8A ----
              ARG8A.setComponentPopupMenu(BTD2);
              ARG8A.setName("ARG8A");
              panel1.add(ARG8A);
              ARG8A.setBounds(198, 80, 120, ARG8A.getPreferredSize().height);
              
              // ---- ARG4A ----
              ARG4A.setComponentPopupMenu(BTD2);
              ARG4A.setName("ARG4A");
              panel1.add(ARG4A);
              ARG4A.setBounds(198, 106, 40, ARG4A.getPreferredSize().height);
              
              // ---- ARG5N ----
              ARG5N.setComponentPopupMenu(BTD2);
              ARG5N.setName("ARG5N");
              panel1.add(ARG5N);
              ARG5N.setBounds(238, 106, 60, ARG5N.getPreferredSize().height);
              
              // ---- ARG6A ----
              ARG6A.setComponentPopupMenu(BTD);
              ARG6A.setName("ARG6A");
              panel1.add(ARG6A);
              ARG6A.setBounds(198, 132, 40, ARG6A.getPreferredSize().height);
              
              // ---- ARG14A ----
              ARG14A.setComponentPopupMenu(BTD);
              ARG14A.setName("ARG14A");
              panel1.add(ARG14A);
              ARG14A.setBounds(198, 158, 40, ARG14A.getPreferredSize().height);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel5.add(panel1);
            panel1.setBounds(10, 10, 440, 235);
            
            // ======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Acc\u00e8s direct"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);
              
              // ---- OBJ_119 ----
              OBJ_119.setText("Num\u00e9ro de bon");
              OBJ_119.setName("OBJ_119");
              panel3.add(OBJ_119);
              OBJ_119.setBounds(20, 32, 96, 20);
              
              // ---- WBON ----
              WBON.setComponentPopupMenu(BTD2);
              WBON.setHorizontalAlignment(SwingConstants.RIGHT);
              WBON.setName("WBON");
              panel3.add(WBON);
              WBON.setBounds(198, 28, 58, WBON.getPreferredSize().height);
              
              // ---- OBJ_121 ----
              OBJ_121.setText("Type facturation");
              OBJ_121.setName("OBJ_121");
              panel3.add(OBJ_121);
              OBJ_121.setBounds(20, 58, 117, 20);
              
              // ---- ARG32A ----
              ARG32A.setComponentPopupMenu(BTD2);
              ARG32A.setName("ARG32A");
              panel3.add(ARG32A);
              ARG32A.setBounds(198, 54, 24, ARG32A.getPreferredSize().height);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel5.add(panel3);
            panel3.setBounds(10, 245, 440, 95);
            
            // ======== panel4 ========
            {
              panel4.setBorder(new TitledBorder(""));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);
              
              // ---- TIDX9 ----
              TIDX9.setText("@ZPL01@");
              TIDX9.setComponentPopupMenu(null);
              TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX9.setToolTipText("Tri sur cet argument");
              TIDX9.setName("TIDX9");
              panel4.add(TIDX9);
              TIDX9.setBounds(12, 19, 168, 20);
              
              // ---- TIDX10 ----
              TIDX10.setText("@ZPL02@");
              TIDX10.setComponentPopupMenu(null);
              TIDX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX10.setToolTipText("Tri sur cet argument");
              TIDX10.setName("TIDX10");
              panel4.add(TIDX10);
              TIDX10.setBounds(12, 44, 168, 20);
              
              // ---- ARG9A ----
              ARG9A.setComponentPopupMenu(BTD);
              ARG9A.setName("ARG9A");
              panel4.add(ARG9A);
              ARG9A.setBounds(198, 15, 34, ARG9A.getPreferredSize().height);
              
              // ---- ARG10A ----
              ARG10A.setComponentPopupMenu(BTD);
              ARG10A.setName("ARG10A");
              panel4.add(ARG10A);
              ARG10A.setBounds(198, 40, 34, ARG10A.getPreferredSize().height);
              
              // ---- TIDX26 ----
              TIDX26.setText("@ZPL03@");
              TIDX26.setComponentPopupMenu(null);
              TIDX26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX26.setToolTipText("Tri sur cet argument");
              TIDX26.setName("TIDX26");
              panel4.add(TIDX26);
              TIDX26.setBounds(12, 69, 168, 20);
              
              // ---- TIDX27 ----
              TIDX27.setText("@ZPL04@");
              TIDX27.setComponentPopupMenu(null);
              TIDX27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX27.setToolTipText("Tri sur cet argument");
              TIDX27.setName("TIDX27");
              panel4.add(TIDX27);
              TIDX27.setBounds(12, 94, 168, 20);
              
              // ---- TIDX28 ----
              TIDX28.setText("@ZPL05@");
              TIDX28.setComponentPopupMenu(null);
              TIDX28.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX28.setToolTipText("Tri sur cet argument");
              TIDX28.setName("TIDX28");
              panel4.add(TIDX28);
              TIDX28.setBounds(12, 119, 168, 20);
              
              // ---- ARG26A ----
              ARG26A.setComponentPopupMenu(BTD);
              ARG26A.setName("ARG26A");
              panel4.add(ARG26A);
              ARG26A.setBounds(198, 65, 34, ARG26A.getPreferredSize().height);
              
              // ---- ARG27A ----
              ARG27A.setComponentPopupMenu(BTD);
              ARG27A.setName("ARG27A");
              panel4.add(ARG27A);
              ARG27A.setBounds(198, 90, 34, ARG27A.getPreferredSize().height);
              
              // ---- ARG28A ----
              ARG28A.setComponentPopupMenu(BTD);
              ARG28A.setName("ARG28A");
              panel4.add(ARG28A);
              ARG28A.setBounds(198, 115, 34, ARG28A.getPreferredSize().height);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel5.add(panel4);
            panel4.setBounds(10, 350, 250, 165);
            
            // ======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Autres crit\u00e8res"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);
              
              // ---- TIDX24 ----
              TIDX24.setText("N\u00b0 Ident.(pays/num\u00e9ro)");
              TIDX24.setComponentPopupMenu(null);
              TIDX24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX24.setToolTipText("Tri sur cet argument");
              TIDX24.setName("TIDX24");
              panel2.add(TIDX24);
              TIDX24.setBounds(20, 32, 158, 20);
              
              // ---- TIDX7 ----
              TIDX7.setText("Code APE");
              TIDX7.setComponentPopupMenu(null);
              TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX7.setToolTipText("Tri sur cet argument");
              TIDX7.setName("TIDX7");
              panel2.add(TIDX7);
              TIDX7.setBounds(20, 58, 158, 20);
              
              // ---- TIDX11 ----
              TIDX11.setText("Mode d'exp\u00e9dition");
              TIDX11.setComponentPopupMenu(null);
              TIDX11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX11.setToolTipText("Tri sur cet argument");
              TIDX11.setName("TIDX11");
              panel2.add(TIDX11);
              TIDX11.setBounds(20, 84, 158, 20);
              
              // ---- TIDX12 ----
              TIDX12.setText("Code transporteur");
              TIDX12.setComponentPopupMenu(null);
              TIDX12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX12.setToolTipText("Tri sur cet argument");
              TIDX12.setName("TIDX12");
              panel2.add(TIDX12);
              TIDX12.setBounds(20, 110, 158, 20);
              
              // ---- TIDX21 ----
              TIDX21.setText("Date de cr\u00e9ation");
              TIDX21.setComponentPopupMenu(null);
              TIDX21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX21.setToolTipText("Tri sur cet argument");
              TIDX21.setName("TIDX21");
              panel2.add(TIDX21);
              TIDX21.setBounds(20, 136, 158, 20);
              
              // ---- TIDX16 ----
              TIDX16.setText("R\u00e9glement/\u00e9ch\u00e9ance");
              TIDX16.setComponentPopupMenu(null);
              TIDX16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX16.setToolTipText("Tri sur cet argument");
              TIDX16.setName("TIDX16");
              panel2.add(TIDX16);
              TIDX16.setBounds(20, 162, 156, 20);
              
              // ---- TIDX33 ----
              TIDX33.setText("Marque");
              TIDX33.setComponentPopupMenu(null);
              TIDX33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX33.setToolTipText("Tri sur cet argument");
              TIDX33.setName("TIDX33");
              panel2.add(TIDX33);
              TIDX33.setBounds(20, 188, 156, 20);
              
              // ---- OBJ_109 ----
              OBJ_109.setText("Code alphab\u00e9tique 2");
              OBJ_109.setName("OBJ_109");
              panel2.add(OBJ_109);
              OBJ_109.setBounds(22, 266, 135, 20);
              
              // ---- OBJ_111 ----
              OBJ_111.setText("Recherche sur ville");
              OBJ_111.setName("OBJ_111");
              panel2.add(OBJ_111);
              OBJ_111.setBounds(22, 292, 117, 20);
              
              // ---- ARG31A ----
              ARG31A.setComponentPopupMenu(BTD);
              ARG31A.setName("ARG31A");
              panel2.add(ARG31A);
              ARG31A.setBounds(160, 288, 160, ARG31A.getPreferredSize().height);
              
              // ---- ARG34A ----
              ARG34A.setComponentPopupMenu(BTD);
              ARG34A.setName("ARG34A");
              panel2.add(ARG34A);
              ARG34A.setBounds(160, 262, 160, ARG34A.getPreferredSize().height);
              
              // ---- ARG33A ----
              ARG33A.setComponentPopupMenu(BTD2);
              ARG33A.setName("ARG33A");
              panel2.add(ARG33A);
              ARG33A.setBounds(205, 184, 115, ARG33A.getPreferredSize().height);
              
              // ---- ARG16A ----
              ARG16A.setComponentPopupMenu(BTD);
              ARG16A.setName("ARG16A");
              panel2.add(ARG16A);
              ARG16A.setBounds(205, 158, 34, ARG16A.getPreferredSize().height);
              
              // ---- ARG17A ----
              ARG17A.setComponentPopupMenu(BTD);
              ARG17A.setName("ARG17A");
              panel2.add(ARG17A);
              ARG17A.setBounds(239, 158, 34, ARG17A.getPreferredSize().height);
              
              // ---- ARG12A ----
              ARG12A.setComponentPopupMenu(BTD);
              ARG12A.setName("ARG12A");
              panel2.add(ARG12A);
              ARG12A.setBounds(205, 106, 34, ARG12A.getPreferredSize().height);
              
              // ---- ARG11A ----
              ARG11A.setComponentPopupMenu(BTD);
              ARG11A.setName("ARG11A");
              panel2.add(ARG11A);
              ARG11A.setBounds(205, 80, 34, ARG11A.getPreferredSize().height);
              
              // ---- ARG7A ----
              ARG7A.setComponentPopupMenu(BTD2);
              ARG7A.setName("ARG7A");
              panel2.add(ARG7A);
              ARG7A.setBounds(205, 54, 60, ARG7A.getPreferredSize().height);
              
              // ---- ARG24A ----
              ARG24A.setComponentPopupMenu(BTD2);
              ARG24A.setName("ARG24A");
              panel2.add(ARG24A);
              ARG24A.setBounds(205, 28, 34, ARG24A.getPreferredSize().height);
              
              // ---- ARG25A ----
              ARG25A.setComponentPopupMenu(BTD2);
              ARG25A.setName("ARG25A");
              panel2.add(ARG25A);
              ARG25A.setBounds(239, 28, 100, ARG25A.getPreferredSize().height);
              
              // ---- ARG21D ----
              ARG21D.setName("ARG21D");
              panel2.add(ARG21D);
              ARG21D.setBounds(205, 132, 105, ARG21D.getPreferredSize().height);
              
              // ---- TIDX23 ----
              TIDX23.setText("Frs de regroupement");
              TIDX23.setComponentPopupMenu(null);
              TIDX23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX23.setToolTipText("Tri sur cet argument");
              TIDX23.setName("TIDX23");
              panel2.add(TIDX23);
              TIDX23.setBounds(20, 214, 185, 20);
              
              // ---- ARG22N ----
              ARG22N.setComponentPopupMenu(BTD2);
              ARG22N.setName("ARG22N");
              panel2.add(ARG22N);
              ARG22N.setBounds(205, 210, 24, ARG22N.getPreferredSize().height);
              
              // ---- ARG23N ----
              ARG23N.setComponentPopupMenu(BTD2);
              ARG23N.setHorizontalAlignment(SwingConstants.RIGHT);
              ARG23N.setName("ARG23N");
              panel2.add(ARG23N);
              ARG23N.setBounds(230, 210, 58, ARG23N.getPreferredSize().height);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel5.add(panel2);
            panel2.setBounds(455, 10, 360, 330);
            
            // ======== panel7 ========
            {
              panel7.setBorder(new TitledBorder(""));
              panel7.setOpaque(false);
              panel7.setName("panel7");
              panel7.setLayout(null);
              
              // ---- OBJ_134 ----
              OBJ_134.setText("@WZP01@");
              OBJ_134.setName("OBJ_134");
              panel7.add(OBJ_134);
              OBJ_134.setBounds(20, 19, 160, 20);
              
              // ---- ARG36A ----
              ARG36A.setComponentPopupMenu(BTD2);
              ARG36A.setName("ARG36A");
              panel7.add(ARG36A);
              ARG36A.setBounds(205, 15, 160, ARG36A.getPreferredSize().height);
              
              // ---- SCAN36 ----
              SCAN36.setText("Scan");
              SCAN36.setToolTipText("Recherche sur l'ensemble du terme");
              SCAN36.setComponentPopupMenu(BTDA);
              SCAN36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SCAN36.setName("SCAN36");
              panel7.add(SCAN36);
              SCAN36.setBounds(370, 19, 55, 20);
              
              // ---- WZP02 ----
              WZP02.setText("@WZP02@");
              WZP02.setName("WZP02");
              panel7.add(WZP02);
              WZP02.setBounds(20, 42, 160, 20);
              
              // ---- ARG37A ----
              ARG37A.setComponentPopupMenu(BTD2);
              ARG37A.setName("ARG37A");
              panel7.add(ARG37A);
              ARG37A.setBounds(205, 40, 160, ARG37A.getPreferredSize().height);
              
              // ---- SCAN37 ----
              SCAN37.setText("Scan");
              SCAN37.setToolTipText("Recherche sur l'ensemble du terme");
              SCAN37.setComponentPopupMenu(BTDA);
              SCAN37.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SCAN37.setName("SCAN37");
              panel7.add(SCAN37);
              SCAN37.setBounds(370, 42, 55, 20);
              
              // ---- WZP03 ----
              WZP03.setText("@WZP03@");
              WZP03.setName("WZP03");
              panel7.add(WZP03);
              WZP03.setBounds(20, 69, 160, 20);
              
              // ---- ARG38A ----
              ARG38A.setComponentPopupMenu(BTD2);
              ARG38A.setName("ARG38A");
              panel7.add(ARG38A);
              ARG38A.setBounds(205, 65, 160, ARG38A.getPreferredSize().height);
              
              // ---- SCAN38 ----
              SCAN38.setText("Scan");
              SCAN38.setToolTipText("Recherche sur l'ensemble du terme");
              SCAN38.setComponentPopupMenu(BTDA);
              SCAN38.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SCAN38.setName("SCAN38");
              panel7.add(SCAN38);
              SCAN38.setBounds(370, 69, 55, 20);
              
              // ---- ARG39A ----
              ARG39A.setComponentPopupMenu(BTD2);
              ARG39A.setName("ARG39A");
              panel7.add(ARG39A);
              ARG39A.setBounds(205, 90, 160, ARG39A.getPreferredSize().height);
              
              // ---- WZP04 ----
              WZP04.setText("@WZP04@");
              WZP04.setName("WZP04");
              panel7.add(WZP04);
              WZP04.setBounds(20, 94, 160, 20);
              
              // ---- WZP01 ----
              WZP01.setText("@WZP05@");
              WZP01.setName("WZP01");
              panel7.add(WZP01);
              WZP01.setBounds(20, 119, 160, 20);
              
              // ---- ARG40A ----
              ARG40A.setComponentPopupMenu(BTD2);
              ARG40A.setName("ARG40A");
              panel7.add(ARG40A);
              ARG40A.setBounds(205, 115, 160, ARG40A.getPreferredSize().height);
              
              // ---- SCAN40 ----
              SCAN40.setText("Scan");
              SCAN40.setToolTipText("Recherche sur l'ensemble du terme");
              SCAN40.setComponentPopupMenu(BTDA);
              SCAN40.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SCAN40.setName("SCAN40");
              panel7.add(SCAN40);
              SCAN40.setBounds(370, 119, 55, 20);
              
              // ---- SCAN39 ----
              SCAN39.setText("Scan");
              SCAN39.setToolTipText("Recherche sur l'ensemble du terme");
              SCAN39.setComponentPopupMenu(BTDA);
              SCAN39.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SCAN39.setName("SCAN39");
              panel7.add(SCAN39);
              SCAN39.setBounds(370, 94, 55, 20);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel7.getComponentCount(); i++) {
                  Rectangle bounds = panel7.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel7.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel7.setMinimumSize(preferredSize);
                panel7.setPreferredSize(preferredSize);
              }
            }
            panel5.add(panel7);
            panel7.setBounds(270, 350, 544, 165);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 830, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 535, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    
    // ======== BTDA ========
    {
      BTDA.setName("BTDA");
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_20);
    }
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_21);
    }
    
    // ---- RBC_GRP ----
    ButtonGroup RBC_GRP = new ButtonGroup();
    RBC_GRP.add(TIDX13);
    RBC_GRP.add(TIDX2);
    RBC_GRP.add(TIDX8);
    RBC_GRP.add(TIDX5);
    RBC_GRP.add(TIDX6);
    RBC_GRP.add(TIDX14);
    RBC_GRP.add(TIDX9);
    RBC_GRP.add(TIDX10);
    RBC_GRP.add(TIDX26);
    RBC_GRP.add(TIDX27);
    RBC_GRP.add(TIDX28);
    RBC_GRP.add(TIDX24);
    RBC_GRP.add(TIDX7);
    RBC_GRP.add(TIDX11);
    RBC_GRP.add(TIDX12);
    RBC_GRP.add(TIDX21);
    RBC_GRP.add(TIDX16);
    RBC_GRP.add(TIDX33);
    RBC_GRP.add(TIDX23);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_43;
  private JLabel OBJ_45;
  private XRiTextField INDFRS;
  private XRiTextField INDETB;
  private XRiTextField INDCOL;
  private JPanel p_tete_droite;
  private JLabel lb_requete;
  private JMenuBar barre_dupl;
  private JPanel p_tete_dupl;
  private JLabel OBJ_44;
  private JLabel OBJ_46;
  private XRiTextField WIFRS3;
  private XRiTextField WIETB3;
  private XRiTextField WICOL3;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel5;
  private JPanel panel1;
  private XRiRadioButton TIDX13;
  private XRiTextField ARG13A;
  private XRiTextField SCAN19;
  private XRiTextField ARG19A;
  private JLabel OBJ_99;
  private XRiTextField ARG3N;
  private XRiTextField ARG2N;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX8;
  private XRiRadioButton TIDX5;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX14;
  private XRiTextField ARG8A;
  private XRiTextField ARG4A;
  private XRiTextField ARG5N;
  private XRiTextField ARG6A;
  private XRiTextField ARG14A;
  private JPanel panel3;
  private JLabel OBJ_119;
  private XRiTextField WBON;
  private JLabel OBJ_121;
  private XRiTextField ARG32A;
  private JPanel panel4;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDX10;
  private XRiTextField ARG9A;
  private XRiTextField ARG10A;
  private XRiRadioButton TIDX26;
  private XRiRadioButton TIDX27;
  private XRiRadioButton TIDX28;
  private XRiTextField ARG26A;
  private XRiTextField ARG27A;
  private XRiTextField ARG28A;
  private JPanel panel2;
  private XRiRadioButton TIDX24;
  private XRiRadioButton TIDX7;
  private XRiRadioButton TIDX11;
  private XRiRadioButton TIDX12;
  private XRiRadioButton TIDX21;
  private XRiRadioButton TIDX16;
  private XRiRadioButton TIDX33;
  private JLabel OBJ_109;
  private JLabel OBJ_111;
  private XRiTextField ARG31A;
  private XRiTextField ARG34A;
  private XRiTextField ARG33A;
  private XRiTextField ARG16A;
  private XRiTextField ARG17A;
  private XRiTextField ARG12A;
  private XRiTextField ARG11A;
  private XRiTextField ARG7A;
  private XRiTextField ARG24A;
  private XRiTextField ARG25A;
  private XRiCalendrier ARG21D;
  private XRiRadioButton TIDX23;
  private XRiTextField ARG22N;
  private XRiTextField ARG23N;
  private JPanel panel7;
  private JLabel OBJ_134;
  private XRiTextField ARG36A;
  private XRiCheckBox SCAN36;
  private JLabel WZP02;
  private XRiTextField ARG37A;
  private XRiCheckBox SCAN37;
  private JLabel WZP03;
  private XRiTextField ARG38A;
  private XRiCheckBox SCAN38;
  private XRiTextField ARG39A;
  private JLabel WZP04;
  private JLabel WZP01;
  private XRiTextField ARG40A;
  private XRiCheckBox SCAN40;
  private XRiCheckBox SCAN39;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_20;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_21;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
