
package ri.serien.libecranrpg.sgim.SGIMDEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGIMDEFM_16 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] REPCUM_Value = { "OUI", "NON", };
  private String[] _PAT001_Top = { "PAT001", "PAT002", "PAT003", "PAT004", "PAT005", };
  private String[] _PAT001_Title = { "Etb", "Nom ou raison sociale", "Mois", "Exercice", };
  private String[][] _PAT001_Data = { { "PAS001", "PAN001", "PAM001", "PAL001", }, { "PAS002", "PAN002", "PAM002", "PAL002", },
      { "PAS003", "PAN003", "PAM003", "PAL003", }, { "PAS004", "PAN004", "PAM004", "PAL004", },
      { "PAS005", "PAN005", "PAM005", "PAL005", }, };
  private int[] _PAT001_Width = { 22, 177, 36, 106, };
  
  public SGIMDEFM_16(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    REPCUM.setValeurs(REPCUM_Value, null);
    TTETB.setValeursSelection("OUI", "NON");
    PAT001.setAspectTable(_PAT001_Top, _PAT001_Title, _PAT001_Data, _PAT001_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _PAT001_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    BT_TOUS.setSelected(lexique.HostFieldGetData("UETB1").equalsIgnoreCase("**"));
    UETB8.setEnabled(!lexique.HostFieldGetData("UETB1").trim().equalsIgnoreCase("**"));
    UETB7.setEnabled(!lexique.HostFieldGetData("UETB1").trim().equalsIgnoreCase("**"));
    UETB6.setEnabled(!lexique.HostFieldGetData("UETB1").trim().equalsIgnoreCase("**"));
    UETB5.setEnabled(!lexique.HostFieldGetData("UETB1").trim().equalsIgnoreCase("**"));
    UETB4.setEnabled(!lexique.HostFieldGetData("UETB1").trim().equalsIgnoreCase("**"));
    UETB3.setEnabled(!lexique.HostFieldGetData("UETB1").trim().equalsIgnoreCase("**"));
    UETB2.setEnabled(!lexique.HostFieldGetData("UETB1").trim().equalsIgnoreCase("**"));
    UETB1.setEnabled(!lexique.HostFieldGetData("UETB1").trim().equalsIgnoreCase("**"));
    // TTETB.setEnabled( lexique.isPresent("TTETB"));
    // TTETB.setSelected(lexique.HostFieldGetData("TTETB").equalsIgnoreCase("OUI"));
    
    // REPCUM.setEnabled( lexique.isPresent("REPCUM"));
    // REPCUM.setSelectedIndex(getIndice("REPCUM", REPCUM_Value));
    
    panel1.setVisible(lexique.isTrue("91"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (BT_TOUS.isSelected()) {
      lexique.HostFieldPutData("UETB1", 0, "**");
    }
    else {
      lexique.HostFieldPutData("UETB1", 0, "  ");
      // if (TTETB.isSelected())
      // lexique.HostFieldPutData("TTETB", 0, "OUI");
      // else
      // lexique.HostFieldPutData("TTETB", 0, "NON");
      // lexique.HostFieldPutData("REPCUM", 0, REPCUM_Value[REPCUM.getSelectedIndex()]);
    }
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void PAT001MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _PAT001_Top, "1", "ENTER", e);
    if (PAT001.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void BT_TOUSActionPerformed(ActionEvent e) {
    panel6.setVisible(!BT_TOUS.isSelected());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_64_OBJ_64 = new JLabel();
    REPCUM = new XRiComboBox();
    xTitledSeparator1 = new JXTitledSeparator();
    sep_etablissement2 = new JXTitledSeparator();
    panel6 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    PAT001 = new XRiTable();
    UETB1 = new XRiTextField();
    UETB2 = new XRiTextField();
    UETB3 = new XRiTextField();
    UETB4 = new XRiTextField();
    UETB5 = new XRiTextField();
    UETB6 = new XRiTextField();
    UETB7 = new XRiTextField();
    UETB8 = new XRiTextField();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_TOUS = new JCheckBox();
    panel1 = new JPanel();
    OBJ_54_OBJ_54 = new JLabel();
    MOIDEB = new XRiTextField();
    OBJ_56_OBJ_56 = new JLabel();
    MOIFIN = new XRiTextField();
    TTETB = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_64_OBJ_64 ----
          OBJ_64_OBJ_64.setText("Cumul des \u00e9critures / comptes section");
          OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");

          //---- REPCUM ----
          REPCUM.setModel(new DefaultComboBoxModel(new String[] {
            "Cumul des \u00e9critures",
            "D\u00e9tail des immos"
          }));
          REPCUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPCUM.setName("REPCUM");

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("S\u00e9lection \u00e9tablissement(s)");
          xTitledSeparator1.setName("xTitledSeparator1");

          //---- sep_etablissement2 ----
          sep_etablissement2.setTitle(" ");
          sep_etablissement2.setName("sep_etablissement2");

          //======== panel6 ========
          {
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- PAT001 ----
              PAT001.setName("PAT001");
              PAT001.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  PAT001MouseClicked(e);
                }
              });
              SCROLLPANE_LIST2.setViewportView(PAT001);
            }
            panel6.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(5, 40, 450, 110);

            //---- UETB1 ----
            UETB1.setComponentPopupMenu(BTD);
            UETB1.setName("UETB1");
            panel6.add(UETB1);
            UETB1.setBounds(70, 5, 40, UETB1.getPreferredSize().height);

            //---- UETB2 ----
            UETB2.setComponentPopupMenu(BTD);
            UETB2.setName("UETB2");
            panel6.add(UETB2);
            UETB2.setBounds(120, 5, 40, UETB2.getPreferredSize().height);

            //---- UETB3 ----
            UETB3.setComponentPopupMenu(BTD);
            UETB3.setName("UETB3");
            panel6.add(UETB3);
            UETB3.setBounds(165, 5, 40, UETB3.getPreferredSize().height);

            //---- UETB4 ----
            UETB4.setComponentPopupMenu(BTD);
            UETB4.setName("UETB4");
            panel6.add(UETB4);
            UETB4.setBounds(215, 5, 40, UETB4.getPreferredSize().height);

            //---- UETB5 ----
            UETB5.setComponentPopupMenu(BTD);
            UETB5.setName("UETB5");
            panel6.add(UETB5);
            UETB5.setBounds(265, 5, 40, UETB5.getPreferredSize().height);

            //---- UETB6 ----
            UETB6.setComponentPopupMenu(BTD);
            UETB6.setName("UETB6");
            panel6.add(UETB6);
            UETB6.setBounds(315, 5, 40, UETB6.getPreferredSize().height);

            //---- UETB7 ----
            UETB7.setComponentPopupMenu(BTD);
            UETB7.setName("UETB7");
            panel6.add(UETB7);
            UETB7.setBounds(365, 5, 40, UETB7.getPreferredSize().height);

            //---- UETB8 ----
            UETB8.setComponentPopupMenu(BTD);
            UETB8.setName("UETB8");
            panel6.add(UETB8);
            UETB8.setBounds(410, 5, 40, UETB8.getPreferredSize().height);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel6.add(BT_PGUP);
            BT_PGUP.setBounds(470, 40, 25, 55);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel6.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(470, 95, 25, 55);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }

          //---- BT_TOUS ----
          BT_TOUS.setText("Tous");
          BT_TOUS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_TOUS.setName("BT_TOUS");
          BT_TOUS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_TOUSActionPerformed(e);
            }
          });

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_54_OBJ_54 ----
            OBJ_54_OBJ_54.setText("P\u00e9riode \u00e0 \u00e9diter");
            OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
            panel1.add(OBJ_54_OBJ_54);
            OBJ_54_OBJ_54.setBounds(15, 9, 99, 20);

            //---- MOIDEB ----
            MOIDEB.setComponentPopupMenu(BTD);
            MOIDEB.setName("MOIDEB");
            panel1.add(MOIDEB);
            MOIDEB.setBounds(190, 5, 52, MOIDEB.getPreferredSize().height);

            //---- OBJ_56_OBJ_56 ----
            OBJ_56_OBJ_56.setText("\u00e0");
            OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
            panel1.add(OBJ_56_OBJ_56);
            OBJ_56_OBJ_56.setBounds(250, 11, 12, OBJ_56_OBJ_56.getPreferredSize().height);

            //---- MOIFIN ----
            MOIFIN.setComponentPopupMenu(BTD);
            MOIFIN.setName("MOIFIN");
            panel1.add(MOIFIN);
            MOIFIN.setBounds(270, 5, 52, MOIFIN.getPreferredSize().height);

            //---- TTETB ----
            TTETB.setText("Tous \u00e9tablissements confondus");
            TTETB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TTETB.setName("TTETB");
            panel1.add(TTETB);
            TTETB.setBounds(15, 45, 216, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(BT_TOUS, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 500, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement2, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, 234, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(REPCUM, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(BT_TOUS, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
                .addComponent(sep_etablissement2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_64_OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(REPCUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel OBJ_64_OBJ_64;
  private XRiComboBox REPCUM;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator sep_etablissement2;
  private JPanel panel6;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable PAT001;
  private XRiTextField UETB1;
  private XRiTextField UETB2;
  private XRiTextField UETB3;
  private XRiTextField UETB4;
  private XRiTextField UETB5;
  private XRiTextField UETB6;
  private XRiTextField UETB7;
  private XRiTextField UETB8;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JCheckBox BT_TOUS;
  private JPanel panel1;
  private JLabel OBJ_54_OBJ_54;
  private XRiTextField MOIDEB;
  private JLabel OBJ_56_OBJ_56;
  private XRiTextField MOIFIN;
  private XRiCheckBox TTETB;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
