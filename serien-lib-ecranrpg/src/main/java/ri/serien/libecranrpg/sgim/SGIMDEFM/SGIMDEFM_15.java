
package ri.serien.libecranrpg.sgim.SGIMDEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGIMDEFM_15 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] REPCUM_Value = { "OUI", "NON", };
  
  public SGIMDEFM_15(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    REPCUM.setValeurs(REPCUM_Value, null);
    TTETB.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN001@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPLNOM@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS001@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    // REPCUM.setEnabled( lexique.isPresent("REPCUM"));
    // REPCUM.setSelectedIndex(getIndice("REPCUM", REPCUM_Value));
    // TTETB.setSelected(lexique.HostFieldGetData("TTETB").equalsIgnoreCase("OUI"));
    panel1.setVisible(lexique.isTrue("91"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TTETB.isSelected())
    // lexique.HostFieldPutData("TTETB", 0, "OUI");
    // else
    // lexique.HostFieldPutData("TTETB", 0, "NON");
    // lexique.HostFieldPutData("REPCUM", 0, REPCUM_Value[REPCUM.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    REPCUM = new XRiComboBox();
    OBJ_61_OBJ_61 = new JLabel();
    sep_etablissement2 = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement2 = new SNBoutonRecherche();
    sep_etablissement3 = new JXTitledSeparator();
    OBJ_60_OBJ_60 = new JLabel();
    OBJ_64_OBJ_64 = new JLabel();
    ETM001 = new XRiTextField();
    DGDE1X = new XRiTextField();
    DGFE1X = new XRiTextField();
    OBJ_62_OBJ_62 = new JLabel();
    sep_etablissement4 = new JXTitledSeparator();
    panel1 = new JPanel();
    OBJ_54_OBJ_54 = new JLabel();
    MOIDEB = new XRiTextField();
    OBJ_56_OBJ_56 = new JLabel();
    MOIFIN = new XRiTextField();
    TTETB = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- REPCUM ----
          REPCUM.setModel(new DefaultComboBoxModel(new String[] {
            "Cumul des \u00e9critures",
            "D\u00e9tail des immos"
          }));
          REPCUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPCUM.setName("REPCUM");
          p_contenu.add(REPCUM);
          REPCUM.setBounds(290, 311, 185, REPCUM.getPreferredSize().height);

          //---- OBJ_61_OBJ_61 ----
          OBJ_61_OBJ_61.setText("Cumul des \u00e9critures / comptes section");
          OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");
          p_contenu.add(OBJ_61_OBJ_61);
          OBJ_61_OBJ_61.setBounds(50, 314, 234, 20);

          //---- sep_etablissement2 ----
          sep_etablissement2.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement2.setName("sep_etablissement2");
          p_contenu.add(sep_etablissement2);
          sep_etablissement2.setBounds(35, 35, 625, sep_etablissement2.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@ETN001@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(205, 60, 260, z_dgnom_.getPreferredSize().height);

          //---- z_wencx_ ----
          z_wencx_.setText("@CPLNOM@");
          z_wencx_.setName("z_wencx_");
          p_contenu.add(z_wencx_);
          z_wencx_.setBounds(205, 90, 260, z_wencx_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@ETS001@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(50, 75, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement2 ----
          bouton_etablissement2.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement2.setName("bouton_etablissement2");
          bouton_etablissement2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement2);
          bouton_etablissement2.setBounds(new Rectangle(new Point(95, 75), bouton_etablissement2.getPreferredSize()));

          //---- sep_etablissement3 ----
          sep_etablissement3.setTitle(" ");
          sep_etablissement3.setName("sep_etablissement3");
          p_contenu.add(sep_etablissement3);
          sep_etablissement3.setBounds(35, 130, 625, sep_etablissement3.getPreferredSize().height);

          //---- OBJ_60_OBJ_60 ----
          OBJ_60_OBJ_60.setText("Exercice en cours");
          OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");
          p_contenu.add(OBJ_60_OBJ_60);
          OBJ_60_OBJ_60.setBounds(50, 155, 155, 20);

          //---- OBJ_64_OBJ_64 ----
          OBJ_64_OBJ_64.setText("Mois en cours");
          OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");
          p_contenu.add(OBJ_64_OBJ_64);
          OBJ_64_OBJ_64.setBounds(50, 185, 155, 20);

          //---- ETM001 ----
          ETM001.setName("ETM001");
          p_contenu.add(ETM001);
          ETM001.setBounds(205, 180, 52, ETM001.getPreferredSize().height);

          //---- DGDE1X ----
          DGDE1X.setName("DGDE1X");
          p_contenu.add(DGDE1X);
          DGDE1X.setBounds(205, 150, 52, DGDE1X.getPreferredSize().height);

          //---- DGFE1X ----
          DGFE1X.setName("DGFE1X");
          p_contenu.add(DGFE1X);
          DGFE1X.setBounds(290, 150, 52, DGFE1X.getPreferredSize().height);

          //---- OBJ_62_OBJ_62 ----
          OBJ_62_OBJ_62.setText("\u00e0");
          OBJ_62_OBJ_62.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");
          p_contenu.add(OBJ_62_OBJ_62);
          OBJ_62_OBJ_62.setBounds(255, 155, 35, 20);

          //---- sep_etablissement4 ----
          sep_etablissement4.setTitle(" ");
          sep_etablissement4.setName("sep_etablissement4");
          p_contenu.add(sep_etablissement4);
          sep_etablissement4.setBounds(35, 220, 625, sep_etablissement4.getPreferredSize().height);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_54_OBJ_54 ----
            OBJ_54_OBJ_54.setText("P\u00e9riode \u00e0 \u00e9diter");
            OBJ_54_OBJ_54.setName("OBJ_54_OBJ_54");
            panel1.add(OBJ_54_OBJ_54);
            OBJ_54_OBJ_54.setBounds(15, 9, 99, 20);

            //---- MOIDEB ----
            MOIDEB.setComponentPopupMenu(BTD);
            MOIDEB.setName("MOIDEB");
            panel1.add(MOIDEB);
            MOIDEB.setBounds(165, 5, 52, MOIDEB.getPreferredSize().height);

            //---- OBJ_56_OBJ_56 ----
            OBJ_56_OBJ_56.setText("\u00e0");
            OBJ_56_OBJ_56.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
            panel1.add(OBJ_56_OBJ_56);
            OBJ_56_OBJ_56.setBounds(215, 11, 30, OBJ_56_OBJ_56.getPreferredSize().height);

            //---- MOIFIN ----
            MOIFIN.setComponentPopupMenu(BTD);
            MOIFIN.setName("MOIFIN");
            panel1.add(MOIFIN);
            MOIFIN.setBounds(245, 5, 52, MOIFIN.getPreferredSize().height);

            //---- TTETB ----
            TTETB.setText("Tous \u00e9tablissements confondus");
            TTETB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TTETB.setName("TTETB");
            panel1.add(TTETB);
            TTETB.setBounds(15, 45, 216, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(40, 235, 510, 75);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private XRiComboBox REPCUM;
  private JLabel OBJ_61_OBJ_61;
  private JXTitledSeparator sep_etablissement2;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement2;
  private JXTitledSeparator sep_etablissement3;
  private JLabel OBJ_60_OBJ_60;
  private JLabel OBJ_64_OBJ_64;
  private XRiTextField ETM001;
  private XRiTextField DGDE1X;
  private XRiTextField DGFE1X;
  private JLabel OBJ_62_OBJ_62;
  private JXTitledSeparator sep_etablissement4;
  private JPanel panel1;
  private JLabel OBJ_54_OBJ_54;
  private XRiTextField MOIDEB;
  private JLabel OBJ_56_OBJ_56;
  private XRiTextField MOIFIN;
  private XRiCheckBox TTETB;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
