
package ri.serien.libecranrpg.sgim.SGIMDEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGIMDEFM_04 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] UCBF_Value = { "", "P", "F", };
  private String[] UORI_Value = { "", "O", "N", };
  private String[] UCOD_Value = { "", "**", "ENT", "SOR", "E/S", };
  private String[] _PAT001_Top = { "PAT001", "PAT002", "PAT003", "PAT004", "PAT005", };
  private String[] _PAT001_Title = { "Etb", "Nom ou raison sociale", "Mois", "Exercice", };
  private String[][] _PAT001_Data = { { "PAS001", "PAN001", "PAM001", "PAL001", }, { "PAS002", "PAN002", "PAM002", "PAL002", },
      { "PAS003", "PAN003", "PAM003", "PAL003", }, { "PAS004", "PAN004", "PAM004", "PAL004", },
      { "PAS005", "PAN005", "PAM005", "PAL005", }, };
  private int[] _PAT001_Width = { 22, 177, 36, 106, };
  
  public SGIMDEFM_04(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    UCOD.setValeurs(UCOD_Value, null);
    UCBF.setValeurs(UCBF_Value, null);
    UORI.setValeurs(UORI_Value, null);
    UTOT.setValeursSelection("OUI", "NON");
    UTRI.setValeursSelection("OUI", "NON");
    PAT001.setAspectTable(_PAT001_Top, _PAT001_Title, _PAT001_Data, _PAT001_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    SALIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SALIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _PAT001_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    
    USAN_CHK.setSelected(lexique.HostFieldGetData("USAN").equalsIgnoreCase("**"));
    P_SEL0.setVisible(!USAN_CHK.isSelected());
    // USAN.setVisible(!lexique.HostFieldGetData("USAN").trim().equalsIgnoreCase("**"));
    // SALIB.setVisible(!lexique.HostFieldGetData("USAN").trim().equalsIgnoreCase("**"));
    // UTRI.setSelected(lexique.HostFieldGetData("UTRI").equalsIgnoreCase("OUI"));
    // UTOT.setSelected(lexique.HostFieldGetData("UTOT").equalsIgnoreCase("OUI"));
    
    UETB1_CHK.setSelected(lexique.HostFieldGetData("UETB1").equalsIgnoreCase("**"));
    panel6.setVisible(!UETB1_CHK.isSelected());
    
    // UCBF.setEnabled( lexique.isPresent("UCBF"));
    // UORI.setEnabled( lexique.isPresent("UORI"));
    // UCOD.setEnabled( lexique.isPresent("UCOD"));
    MOIFIN.setEnabled(lexique.isPresent("MOIFIN"));
    MOIDEB.setEnabled(lexique.isPresent("MOIDEB"));
    
    // UCOD.setSelectedIndex(getIndice("UCOD", UCOD_Value));
    // UORI.setSelectedIndex(getIndice("UORI", UORI_Value));
    // UCBF.setSelectedIndex(getIndice("UCBF", UCBF_Value));
    
    sep_etablissement4.setVisible(lexique.isTrue("91"));
    sep_etablissement5.setVisible(lexique.isTrue("92"));
    sep_etablissement6.setVisible(lexique.isTrue("93"));
    sep_etablissement7.setVisible(lexique.isTrue("94"));
    sep_etablissement8.setVisible(lexique.isTrue("95"));
    panel1.setVisible(lexique.isTrue("91"));
    panel2.setVisible(lexique.isTrue("92"));
    panel3.setVisible(lexique.isTrue("93"));
    panel4.setVisible(lexique.isTrue("94"));
    panel5.setVisible(lexique.isTrue("95"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
    
    

    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (USAN_CHK.isSelected()) {
      lexique.HostFieldPutData("USAN", 0, "**");
    }
    
    if (UETB1_CHK.isSelected()) {
      lexique.HostFieldPutData("UETB1", 0, "**");
    }
    
    // if (UTRI.isSelected())
    // lexique.HostFieldPutData("UTRI", 0, "OUI");
    // else
    // lexique.HostFieldPutData("UTRI", 0, "NON");
    // if (UTOT.isSelected())
    // lexique.HostFieldPutData("UTOT", 0, "OUI");
    // else
    // lexique.HostFieldPutData("UTOT", 0, "NON");
    
    // lexique.HostFieldPutData("UCBF", 0, UCBF_Value[UCBF.getSelectedIndex()]);
    // lexique.HostFieldPutData("UORI", 0, UORI_Value[UORI.getSelectedIndex()]);
    // lexique.HostFieldPutData("UCOD", 0, UCOD_Value[UCOD.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void PAT001MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _PAT001_Top, "1", "ENTER", e);
    if (PAT001.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTDI.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDI.getInvoker().getName());
  }
  
  private void BT_TOUSActionPerformed(ActionEvent e) {
    panel6.setVisible(!panel6.isVisible());
    if (!UETB1_CHK.isSelected()) {
      UETB1.setText("");
    }
  }
  
  private void WUSANActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
    if (USAN_CHK.isSelected()) {
      USAN.setText("");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    UETB1_CHK = new JCheckBox();
    xTitledSeparator1 = new JXTitledSeparator();
    sep_etablissement2 = new JXTitledSeparator();
    OBJ_60_OBJ_60 = new JLabel();
    OBJ_64_OBJ_64 = new JLabel();
    ETM001 = new XRiTextField();
    DGDE1X = new XRiTextField();
    DGFE1X = new XRiTextField();
    OBJ_62_OBJ_62 = new JLabel();
    sep_etablissement3 = new JXTitledSeparator();
    UORI = new XRiComboBox();
    UCBF = new XRiComboBox();
    UCOD = new XRiComboBox();
    OBJ_72_OBJ_72 = new JLabel();
    OBJ_80_OBJ_80 = new JLabel();
    OBJ_67_OBJ_67 = new JLabel();
    OBJ_71_OBJ_71 = new JLabel();
    MOIDEB = new XRiTextField();
    MOIFIN = new XRiTextField();
    OBJ_69_OBJ_69 = new JLabel();
    USAN_CHK = new JCheckBox();
    sep_etablissement4 = new JXTitledSeparator();
    panel1 = new JPanel();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    sep_etablissement5 = new JXTitledSeparator();
    sep_etablissement6 = new JXTitledSeparator();
    sep_etablissement7 = new JXTitledSeparator();
    sep_etablissement8 = new JXTitledSeparator();
    panel5 = new JPanel();
    label9 = new JLabel();
    label10 = new JLabel();
    CPTDEB = new XRiTextField();
    CPTFIN = new XRiTextField();
    panel2 = new JPanel();
    label3 = new JLabel();
    label4 = new JLabel();
    GEODEB = new XRiTextField();
    GEOFIN = new XRiTextField();
    panel3 = new JPanel();
    label5 = new JLabel();
    label6 = new JLabel();
    SANDEB = new XRiTextField();
    SANFIN = new XRiTextField();
    panel4 = new JPanel();
    label7 = new JLabel();
    label8 = new JLabel();
    IMODEB = new XRiTextField();
    IMOFIN = new XRiTextField();
    UTRI = new XRiCheckBox();
    UTOT = new XRiCheckBox();
    panel6 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    PAT001 = new XRiTable();
    UETB1 = new XRiTextField();
    UETB2 = new XRiTextField();
    UETB3 = new XRiTextField();
    UETB4 = new XRiTextField();
    UETB5 = new XRiTextField();
    UETB6 = new XRiTextField();
    UETB7 = new XRiTextField();
    UETB8 = new XRiTextField();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    P_SEL0 = new JPanel();
    OBJ_86_OBJ_86 = new JLabel();
    USAN = new XRiTextField();
    SALIB = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    BTDI = new JPopupMenu();
    OBJ_22 = new JMenuItem();
    OBJ_21 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- UETB1_CHK ----
          UETB1_CHK.setText("Tous");
          UETB1_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          UETB1_CHK.setName("UETB1_CHK");
          UETB1_CHK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_TOUSActionPerformed(e);
            }
          });
          p_contenu.add(UETB1_CHK);
          UETB1_CHK.setBounds(55, 65, 65, 20);

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("S\u00e9lection \u00e9tablissement(s)");
          xTitledSeparator1.setName("xTitledSeparator1");
          p_contenu.add(xTitledSeparator1);
          xTitledSeparator1.setBounds(35, 35, 625, xTitledSeparator1.getPreferredSize().height);

          //---- sep_etablissement2 ----
          sep_etablissement2.setTitle(" ");
          sep_etablissement2.setName("sep_etablissement2");
          p_contenu.add(sep_etablissement2);
          sep_etablissement2.setBounds(35, 215, 625, sep_etablissement2.getPreferredSize().height);

          //---- OBJ_60_OBJ_60 ----
          OBJ_60_OBJ_60.setText("Exercice en cours");
          OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");
          p_contenu.add(OBJ_60_OBJ_60);
          OBJ_60_OBJ_60.setBounds(50, 240, 155, 20);

          //---- OBJ_64_OBJ_64 ----
          OBJ_64_OBJ_64.setText("Mois en cours");
          OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");
          p_contenu.add(OBJ_64_OBJ_64);
          OBJ_64_OBJ_64.setBounds(50, 270, 155, 20);

          //---- ETM001 ----
          ETM001.setName("ETM001");
          p_contenu.add(ETM001);
          ETM001.setBounds(205, 265, 52, ETM001.getPreferredSize().height);

          //---- DGDE1X ----
          DGDE1X.setName("DGDE1X");
          p_contenu.add(DGDE1X);
          DGDE1X.setBounds(205, 235, 52, DGDE1X.getPreferredSize().height);

          //---- DGFE1X ----
          DGFE1X.setName("DGFE1X");
          p_contenu.add(DGFE1X);
          DGFE1X.setBounds(290, 235, 52, DGFE1X.getPreferredSize().height);

          //---- OBJ_62_OBJ_62 ----
          OBJ_62_OBJ_62.setText("\u00e0");
          OBJ_62_OBJ_62.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");
          p_contenu.add(OBJ_62_OBJ_62);
          OBJ_62_OBJ_62.setBounds(255, 240, 35, 20);

          //---- sep_etablissement3 ----
          sep_etablissement3.setTitle(" ");
          sep_etablissement3.setName("sep_etablissement3");
          p_contenu.add(sep_etablissement3);
          sep_etablissement3.setBounds(35, 295, 625, sep_etablissement3.getPreferredSize().height);

          //---- UORI ----
          UORI.setModel(new DefaultComboBoxModel(new String[] {
            "Toutes origines",
            "Occasion",
            "Neuf"
          }));
          UORI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          UORI.setName("UORI");
          p_contenu.add(UORI);
          UORI.setBounds(205, 370, 120, UORI.getPreferredSize().height);

          //---- UCBF ----
          UCBF.setModel(new DefaultComboBoxModel(new String[] {
            "Professionelle",
            "Fonci\u00e8re"
          }));
          UCBF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          UCBF.setName("UCBF");
          p_contenu.add(UCBF);
          UCBF.setBounds(205, 400, 120, UCBF.getPreferredSize().height);

          //---- UCOD ----
          UCOD.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "**",
            "Entr\u00e9e",
            "Sortie",
            "Entr\u00e9e / Sortie"
          }));
          UCOD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          UCOD.setName("UCOD");
          p_contenu.add(UCOD);
          UCOD.setBounds(205, 340, 120, UCOD.getPreferredSize().height);

          //---- OBJ_72_OBJ_72 ----
          OBJ_72_OBJ_72.setText("Origine immobilisation");
          OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");
          p_contenu.add(OBJ_72_OBJ_72);
          OBJ_72_OBJ_72.setBounds(50, 375, 155, 20);

          //---- OBJ_80_OBJ_80 ----
          OBJ_80_OBJ_80.setText("Base fiscale");
          OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
          p_contenu.add(OBJ_80_OBJ_80);
          OBJ_80_OBJ_80.setBounds(50, 405, 155, 20);

          //---- OBJ_67_OBJ_67 ----
          OBJ_67_OBJ_67.setText("P\u00e9riode \u00e0 \u00e9diter");
          OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");
          p_contenu.add(OBJ_67_OBJ_67);
          OBJ_67_OBJ_67.setBounds(50, 315, 155, 20);

          //---- OBJ_71_OBJ_71 ----
          OBJ_71_OBJ_71.setText("Code s\u00e9lection");
          OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");
          p_contenu.add(OBJ_71_OBJ_71);
          OBJ_71_OBJ_71.setBounds(50, 345, 155, 20);

          //---- MOIDEB ----
          MOIDEB.setComponentPopupMenu(BTD);
          MOIDEB.setName("MOIDEB");
          p_contenu.add(MOIDEB);
          MOIDEB.setBounds(205, 310, 52, MOIDEB.getPreferredSize().height);

          //---- MOIFIN ----
          MOIFIN.setComponentPopupMenu(BTD);
          MOIFIN.setName("MOIFIN");
          p_contenu.add(MOIFIN);
          MOIFIN.setBounds(290, 310, 52, MOIFIN.getPreferredSize().height);

          //---- OBJ_69_OBJ_69 ----
          OBJ_69_OBJ_69.setText("\u00e0");
          OBJ_69_OBJ_69.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");
          p_contenu.add(OBJ_69_OBJ_69);
          OBJ_69_OBJ_69.setBounds(255, 315, 35, 20);

          //---- USAN_CHK ----
          USAN_CHK.setText("Toutes les sections");
          USAN_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          USAN_CHK.setName("USAN_CHK");
          USAN_CHK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WUSANActionPerformed(e);
            }
          });
          p_contenu.add(USAN_CHK);
          USAN_CHK.setBounds(50, 437, 135, 20);

          //---- sep_etablissement4 ----
          sep_etablissement4.setTitle("Plage de familles");
          sep_etablissement4.setName("sep_etablissement4");
          p_contenu.add(sep_etablissement4);
          sep_etablissement4.setBounds(35, 465, 625, sep_etablissement4.getPreferredSize().height);

          //======== panel1 ========
          {
            panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- FAMDEB ----
            FAMDEB.setComponentPopupMenu(BTDI);
            FAMDEB.setName("FAMDEB");

            //---- FAMFIN ----
            FAMFIN.setComponentPopupMenu(BTDI);
            FAMFIN.setName("FAMFIN");

            //---- label1 ----
            label1.setText("Famille de d\u00e9but");
            label1.setName("label1");

            //---- label2 ----
            label2.setText("Famille de fin");
            label2.setName("label2");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(28, 28, 28)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(label1, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(FAMDEB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(label2, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(FAMFIN, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label1))
                    .addComponent(FAMDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label2))
                    .addComponent(FAMFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }
          p_contenu.add(panel1);
          panel1.setBounds(50, 490, 300, 95);

          //---- sep_etablissement5 ----
          sep_etablissement5.setTitle("Plage de zones g\u00e9ographiques");
          sep_etablissement5.setName("sep_etablissement5");
          p_contenu.add(sep_etablissement5);
          sep_etablissement5.setBounds(35, 465, 625, sep_etablissement5.getPreferredSize().height);

          //---- sep_etablissement6 ----
          sep_etablissement6.setTitle("Plage de sections");
          sep_etablissement6.setName("sep_etablissement6");
          p_contenu.add(sep_etablissement6);
          sep_etablissement6.setBounds(35, 465, 625, sep_etablissement6.getPreferredSize().height);

          //---- sep_etablissement7 ----
          sep_etablissement7.setTitle("Plage d'immobilisations");
          sep_etablissement7.setName("sep_etablissement7");
          p_contenu.add(sep_etablissement7);
          sep_etablissement7.setBounds(35, 465, 625, sep_etablissement7.getPreferredSize().height);

          //---- sep_etablissement8 ----
          sep_etablissement8.setTitle("Plage de comptes");
          sep_etablissement8.setName("sep_etablissement8");
          p_contenu.add(sep_etablissement8);
          sep_etablissement8.setBounds(35, 465, 625, sep_etablissement8.getPreferredSize().height);

          //======== panel5 ========
          {
            panel5.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel5.setOpaque(false);
            panel5.setName("panel5");

            //---- label9 ----
            label9.setText("Compte de d\u00e9but");
            label9.setName("label9");

            //---- label10 ----
            label10.setText("Compte de fin");
            label10.setName("label10");

            //---- CPTDEB ----
            CPTDEB.setComponentPopupMenu(BTDI);
            CPTDEB.setName("CPTDEB");

            //---- CPTFIN ----
            CPTFIN.setComponentPopupMenu(BTDI);
            CPTFIN.setName("CPTFIN");

            GroupLayout panel5Layout = new GroupLayout(panel5);
            panel5.setLayout(panel5Layout);
            panel5Layout.setHorizontalGroup(
              panel5Layout.createParallelGroup()
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(28, 28, 28)
                  .addGroup(panel5Layout.createParallelGroup()
                    .addGroup(panel5Layout.createSequentialGroup()
                      .addComponent(label9, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                      .addComponent(CPTDEB, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel5Layout.createSequentialGroup()
                      .addComponent(label10, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                      .addComponent(CPTFIN, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE))))
            );
            panel5Layout.setVerticalGroup(
              panel5Layout.createParallelGroup()
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addGroup(panel5Layout.createParallelGroup()
                    .addGroup(panel5Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label9))
                    .addComponent(CPTDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(panel5Layout.createParallelGroup()
                    .addGroup(panel5Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label10))
                    .addComponent(CPTFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }
          p_contenu.add(panel5);
          panel5.setBounds(50, 490, 300, 95);

          //======== panel2 ========
          {
            panel2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //---- label3 ----
            label3.setText("Zone de d\u00e9but");
            label3.setName("label3");

            //---- label4 ----
            label4.setText("Zone de fin");
            label4.setName("label4");

            //---- GEODEB ----
            GEODEB.setComponentPopupMenu(BTDI);
            GEODEB.setName("GEODEB");

            //---- GEOFIN ----
            GEOFIN.setComponentPopupMenu(BTDI);
            GEOFIN.setName("GEOFIN");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(28, 28, 28)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(label3, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(GEODEB, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(label4, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
                      .addComponent(GEOFIN, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))))
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label3))
                    .addComponent(GEODEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label4))
                    .addComponent(GEOFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }
          p_contenu.add(panel2);
          panel2.setBounds(50, 490, 300, 95);

          //======== panel3 ========
          {
            panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel3.setOpaque(false);
            panel3.setName("panel3");

            //---- label5 ----
            label5.setText("Section de d\u00e9but");
            label5.setName("label5");

            //---- label6 ----
            label6.setText("Section de fin");
            label6.setName("label6");

            //---- SANDEB ----
            SANDEB.setComponentPopupMenu(BTDI);
            SANDEB.setName("SANDEB");

            //---- SANFIN ----
            SANFIN.setComponentPopupMenu(BTDI);
            SANFIN.setName("SANFIN");

            GroupLayout panel3Layout = new GroupLayout(panel3);
            panel3.setLayout(panel3Layout);
            panel3Layout.setHorizontalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(28, 28, 28)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(label5, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                      .addComponent(SANDEB, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(label6, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                      .addComponent(SANFIN, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))))
            );
            panel3Layout.setVerticalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label5))
                    .addComponent(SANDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label6))
                    .addComponent(SANFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }
          p_contenu.add(panel3);
          panel3.setBounds(50, 490, 300, 95);

          //======== panel4 ========
          {
            panel4.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel4.setOpaque(false);
            panel4.setName("panel4");

            //---- label7 ----
            label7.setText("Immobilisation de d\u00e9but");
            label7.setName("label7");

            //---- label8 ----
            label8.setText("immobilisation de fin");
            label8.setName("label8");

            //---- IMODEB ----
            IMODEB.setComponentPopupMenu(BTD);
            IMODEB.setName("IMODEB");

            //---- IMOFIN ----
            IMOFIN.setComponentPopupMenu(BTD);
            IMOFIN.setName("IMOFIN");

            GroupLayout panel4Layout = new GroupLayout(panel4);
            panel4.setLayout(panel4Layout);
            panel4Layout.setHorizontalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(28, 28, 28)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addComponent(label7, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                      .addComponent(IMODEB, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addComponent(label8, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                      .addComponent(IMOFIN, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))))
            );
            panel4Layout.setVerticalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(13, 13, 13)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label7))
                    .addComponent(IMODEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label8))
                    .addComponent(IMOFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }
          p_contenu.add(panel4);
          panel4.setBounds(50, 490, 300, 95);

          //---- UTRI ----
          UTRI.setText("Tri par date d'entr\u00e9e dans l'actif");
          UTRI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          UTRI.setName("UTRI");
          p_contenu.add(UTRI);
          UTRI.setBounds(380, 510, 213, 20);

          //---- UTOT ----
          UTOT.setText("Totalisations uniquement");
          UTOT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          UTOT.setName("UTOT");
          p_contenu.add(UTOT);
          UTOT.setBounds(380, 545, 213, 20);

          //======== panel6 ========
          {
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- PAT001 ----
              PAT001.setName("PAT001");
              PAT001.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  PAT001MouseClicked(e);
                }
              });
              SCROLLPANE_LIST2.setViewportView(PAT001);
            }
            panel6.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(5, 40, 450, 110);

            //---- UETB1 ----
            UETB1.setComponentPopupMenu(BTD);
            UETB1.setName("UETB1");
            panel6.add(UETB1);
            UETB1.setBounds(70, 5, 40, UETB1.getPreferredSize().height);

            //---- UETB2 ----
            UETB2.setComponentPopupMenu(BTD);
            UETB2.setName("UETB2");
            panel6.add(UETB2);
            UETB2.setBounds(120, 5, 40, UETB2.getPreferredSize().height);

            //---- UETB3 ----
            UETB3.setComponentPopupMenu(BTD);
            UETB3.setName("UETB3");
            panel6.add(UETB3);
            UETB3.setBounds(165, 5, 40, UETB3.getPreferredSize().height);

            //---- UETB4 ----
            UETB4.setComponentPopupMenu(BTD);
            UETB4.setName("UETB4");
            panel6.add(UETB4);
            UETB4.setBounds(215, 5, 40, UETB4.getPreferredSize().height);

            //---- UETB5 ----
            UETB5.setComponentPopupMenu(BTD);
            UETB5.setName("UETB5");
            panel6.add(UETB5);
            UETB5.setBounds(265, 5, 40, UETB5.getPreferredSize().height);

            //---- UETB6 ----
            UETB6.setComponentPopupMenu(BTD);
            UETB6.setName("UETB6");
            panel6.add(UETB6);
            UETB6.setBounds(315, 5, 40, UETB6.getPreferredSize().height);

            //---- UETB7 ----
            UETB7.setComponentPopupMenu(BTD);
            UETB7.setName("UETB7");
            panel6.add(UETB7);
            UETB7.setBounds(365, 5, 40, UETB7.getPreferredSize().height);

            //---- UETB8 ----
            UETB8.setComponentPopupMenu(BTD);
            UETB8.setName("UETB8");
            panel6.add(UETB8);
            UETB8.setBounds(410, 5, 40, UETB8.getPreferredSize().height);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel6.add(BT_PGUP);
            BT_PGUP.setBounds(470, 40, 25, 55);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel6.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(470, 95, 25, 55);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel6);
          panel6.setBounds(125, 55, 500, 160);

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_86_OBJ_86 ----
            OBJ_86_OBJ_86.setText("Code section");
            OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");
            P_SEL0.add(OBJ_86_OBJ_86);
            OBJ_86_OBJ_86.setBounds(15, 9, 100, 20);

            //---- USAN ----
            USAN.setComponentPopupMenu(BTD);
            USAN.setName("USAN");
            P_SEL0.add(USAN);
            USAN.setBounds(115, 5, 44, USAN.getPreferredSize().height);

            //---- SALIB ----
            SALIB.setText("@SALIB@");
            SALIB.setName("SALIB");
            P_SEL0.add(SALIB);
            SALIB.setBounds(175, 7, 212, SALIB.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL0.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL0.setMinimumSize(preferredSize);
              P_SEL0.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(P_SEL0);
          P_SEL0.setBounds(205, 430, 400, 35);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //======== BTDI ========
    {
      BTDI.setName("BTDI");

      //---- OBJ_22 ----
      OBJ_22.setText("Choix possibles");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTDI.add(OBJ_22);

      //---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTDI.add(OBJ_21);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JCheckBox UETB1_CHK;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator sep_etablissement2;
  private JLabel OBJ_60_OBJ_60;
  private JLabel OBJ_64_OBJ_64;
  private XRiTextField ETM001;
  private XRiTextField DGDE1X;
  private XRiTextField DGFE1X;
  private JLabel OBJ_62_OBJ_62;
  private JXTitledSeparator sep_etablissement3;
  private XRiComboBox UORI;
  private XRiComboBox UCBF;
  private XRiComboBox UCOD;
  private JLabel OBJ_72_OBJ_72;
  private JLabel OBJ_80_OBJ_80;
  private JLabel OBJ_67_OBJ_67;
  private JLabel OBJ_71_OBJ_71;
  private XRiTextField MOIDEB;
  private XRiTextField MOIFIN;
  private JLabel OBJ_69_OBJ_69;
  private JCheckBox USAN_CHK;
  private JXTitledSeparator sep_etablissement4;
  private JPanel panel1;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private JLabel label1;
  private JLabel label2;
  private JXTitledSeparator sep_etablissement5;
  private JXTitledSeparator sep_etablissement6;
  private JXTitledSeparator sep_etablissement7;
  private JXTitledSeparator sep_etablissement8;
  private JPanel panel5;
  private JLabel label9;
  private JLabel label10;
  private XRiTextField CPTDEB;
  private XRiTextField CPTFIN;
  private JPanel panel2;
  private JLabel label3;
  private JLabel label4;
  private XRiTextField GEODEB;
  private XRiTextField GEOFIN;
  private JPanel panel3;
  private JLabel label5;
  private JLabel label6;
  private XRiTextField SANDEB;
  private XRiTextField SANFIN;
  private JPanel panel4;
  private JLabel label7;
  private JLabel label8;
  private XRiTextField IMODEB;
  private XRiTextField IMOFIN;
  private XRiCheckBox UTRI;
  private XRiCheckBox UTOT;
  private JPanel panel6;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable PAT001;
  private XRiTextField UETB1;
  private XRiTextField UETB2;
  private XRiTextField UETB3;
  private XRiTextField UETB4;
  private XRiTextField UETB5;
  private XRiTextField UETB6;
  private XRiTextField UETB7;
  private XRiTextField UETB8;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel P_SEL0;
  private JLabel OBJ_86_OBJ_86;
  private XRiTextField USAN;
  private RiZoneSortie SALIB;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JPopupMenu BTDI;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_21;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
