
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_TO extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM01FX_TO(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TJDI15.setValeursSelection("1", " ");
    TJDI14.setValeursSelection("1", " ");
    TJDI13.setValeursSelection("1", " ");
    TJDI12.setValeursSelection("1", " ");
    TJDI11.setValeursSelection("1", " ");
    TJSA15.setValeursSelection("1", " ");
    TJSA14.setValeursSelection("1", " ");
    TJSA13.setValeursSelection("1", " ");
    TJSA12.setValeursSelection("1", " ");
    TJSA11.setValeursSelection("1", " ");
    TJVE15.setValeursSelection("1", " ");
    TJVE14.setValeursSelection("1", " ");
    TJVE13.setValeursSelection("1", " ");
    TJVE12.setValeursSelection("1", " ");
    TJVE11.setValeursSelection("1", " ");
    TJJE15.setValeursSelection("1", " ");
    TJJE14.setValeursSelection("1", " ");
    TJJE13.setValeursSelection("1", " ");
    TJJE12.setValeursSelection("1", " ");
    TJJE11.setValeursSelection("1", " ");
    TJME15.setValeursSelection("1", " ");
    TJME14.setValeursSelection("1", " ");
    TJME13.setValeursSelection("1", " ");
    TJME12.setValeursSelection("1", " ");
    TJME11.setValeursSelection("1", " ");
    TJMA15.setValeursSelection("1", " ");
    TJMA14.setValeursSelection("1", " ");
    TJMA13.setValeursSelection("1", " ");
    TJMA12.setValeursSelection("1", " ");
    TJMA11.setValeursSelection("1", " ");
    TJLU15.setValeursSelection("1", " ");
    TJLU14.setValeursSelection("1", " ");
    TJLU13.setValeursSelection("1", " ");
    TJLU12.setValeursSelection("1", " ");
    TJLU11.setValeursSelection("1", " ");
    TJDI25.setValeursSelection("1", " ");
    TJDI24.setValeursSelection("1", " ");
    TJDI23.setValeursSelection("1", " ");
    TJDI22.setValeursSelection("1", " ");
    TJDI21.setValeursSelection("1", " ");
    TJSA25.setValeursSelection("1", " ");
    TJSA24.setValeursSelection("1", " ");
    TJSA23.setValeursSelection("1", " ");
    TJSA22.setValeursSelection("1", " ");
    TJSA21.setValeursSelection("1", " ");
    TJVE25.setValeursSelection("1", " ");
    TJVE24.setValeursSelection("1", " ");
    TJVE23.setValeursSelection("1", " ");
    TJVE22.setValeursSelection("1", " ");
    TJVE21.setValeursSelection("1", " ");
    TJJE25.setValeursSelection("1", " ");
    TJJE24.setValeursSelection("1", " ");
    TJJE23.setValeursSelection("1", " ");
    TJJE22.setValeursSelection("1", " ");
    TJJE21.setValeursSelection("1", " ");
    TJME25.setValeursSelection("1", " ");
    TJME24.setValeursSelection("1", " ");
    TJME23.setValeursSelection("1", " ");
    TJME22.setValeursSelection("1", " ");
    TJME21.setValeursSelection("1", " ");
    TJMA25.setValeursSelection("1", " ");
    TJMA24.setValeursSelection("1", " ");
    TJMA23.setValeursSelection("1", " ");
    TJMA22.setValeursSelection("1", " ");
    TJMA21.setValeursSelection("1", " ");
    TJLU25.setValeursSelection("1", " ");
    TJLU24.setValeursSelection("1", " ");
    TJLU23.setValeursSelection("1", " ");
    TJLU22.setValeursSelection("1", " ");
    TJLU21.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel5 = new JXTitledPanel();
    panel6 = new JPanel();
    OBJ_62 = new JLabel();
    TOLIB = new XRiTextField();
    OBJ_110 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_96 = new JLabel();
    TOGEO = new XRiTextField();
    TORGR = new XRiTextField();
    OBJ_97 = new JLabel();
    OBJ_98 = new JLabel();
    TOMEX = new XRiTextField();
    TOMAG = new XRiTextField();
    TOREP = new XRiTextField();
    TODEL = new XRiTextField();
    TOTYP = new XRiTextField();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_74 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_79 = new JLabel();
    TODX1 = new XRiCalendrier();
    TODX3 = new XRiCalendrier();
    TODX2 = new XRiCalendrier();
    TODX4 = new XRiCalendrier();
    panel5 = new JPanel();
    OBJ_180 = new JLabel();
    OBJ_178 = new JLabel();
    OBJ_173 = new JLabel();
    OBJ_176 = new JLabel();
    OBJ_179 = new JLabel();
    OBJ_175 = new JLabel();
    OBJ_177 = new JLabel();
    OBJ_174 = new JLabel();
    TJLU21 = new XRiCheckBox();
    TJLU22 = new XRiCheckBox();
    TJLU23 = new XRiCheckBox();
    TJLU24 = new XRiCheckBox();
    TJLU25 = new XRiCheckBox();
    TJMA21 = new XRiCheckBox();
    TJMA22 = new XRiCheckBox();
    TJMA23 = new XRiCheckBox();
    TJMA24 = new XRiCheckBox();
    TJMA25 = new XRiCheckBox();
    TJME21 = new XRiCheckBox();
    TJME22 = new XRiCheckBox();
    TJME23 = new XRiCheckBox();
    TJME24 = new XRiCheckBox();
    TJME25 = new XRiCheckBox();
    TJJE21 = new XRiCheckBox();
    TJJE22 = new XRiCheckBox();
    TJJE23 = new XRiCheckBox();
    TJJE24 = new XRiCheckBox();
    TJJE25 = new XRiCheckBox();
    TJVE21 = new XRiCheckBox();
    TJVE22 = new XRiCheckBox();
    TJVE23 = new XRiCheckBox();
    TJVE24 = new XRiCheckBox();
    TJVE25 = new XRiCheckBox();
    TJSA21 = new XRiCheckBox();
    TJSA22 = new XRiCheckBox();
    TJSA23 = new XRiCheckBox();
    TJSA24 = new XRiCheckBox();
    TJSA25 = new XRiCheckBox();
    TJDI21 = new XRiCheckBox();
    TJDI22 = new XRiCheckBox();
    TJDI23 = new XRiCheckBox();
    TJDI24 = new XRiCheckBox();
    TJDI25 = new XRiCheckBox();
    panel4 = new JPanel();
    label16 = new JLabel();
    label17 = new JLabel();
    label18 = new JLabel();
    label15 = new JLabel();
    label19 = new JLabel();
    panel1 = new JPanel();
    OBJ_156 = new JLabel();
    OBJ_134 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_145 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_85 = new JLabel();
    TJLU11 = new XRiCheckBox();
    TJLU12 = new XRiCheckBox();
    TJLU13 = new XRiCheckBox();
    TJLU14 = new XRiCheckBox();
    TJLU15 = new XRiCheckBox();
    TJMA11 = new XRiCheckBox();
    TJMA12 = new XRiCheckBox();
    TJMA13 = new XRiCheckBox();
    TJMA14 = new XRiCheckBox();
    TJMA15 = new XRiCheckBox();
    TJME11 = new XRiCheckBox();
    TJME12 = new XRiCheckBox();
    TJME13 = new XRiCheckBox();
    TJME14 = new XRiCheckBox();
    TJME15 = new XRiCheckBox();
    TJJE11 = new XRiCheckBox();
    TJJE12 = new XRiCheckBox();
    TJJE13 = new XRiCheckBox();
    TJJE14 = new XRiCheckBox();
    TJJE15 = new XRiCheckBox();
    TJVE11 = new XRiCheckBox();
    TJVE12 = new XRiCheckBox();
    TJVE13 = new XRiCheckBox();
    TJVE14 = new XRiCheckBox();
    TJVE15 = new XRiCheckBox();
    TJSA11 = new XRiCheckBox();
    TJSA12 = new XRiCheckBox();
    TJSA13 = new XRiCheckBox();
    TJSA14 = new XRiCheckBox();
    TJSA15 = new XRiCheckBox();
    TJDI11 = new XRiCheckBox();
    TJDI12 = new XRiCheckBox();
    TJDI13 = new XRiCheckBox();
    TJDI14 = new XRiCheckBox();
    TJDI15 = new XRiCheckBox();
    panel3 = new JPanel();
    label4 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(680, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel5 ========
          {
            xTitledPanel5.setTitle("Tourn\u00e9e");
            xTitledPanel5.setBorder(new DropShadowBorder());
            xTitledPanel5.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel5.setName("xTitledPanel5");
            Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();

            //======== panel6 ========
            {
              panel6.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);

              //---- OBJ_62 ----
              OBJ_62.setText("Libell\u00e9");
              OBJ_62.setName("OBJ_62");
              panel6.add(OBJ_62);
              OBJ_62.setBounds(10, 14, 43, 20);

              //---- TOLIB ----
              TOLIB.setComponentPopupMenu(BTD);
              TOLIB.setName("TOLIB");
              panel6.add(TOLIB);
              TOLIB.setBounds(120, 10, 410, TOLIB.getPreferredSize().height);
            }

            //---- OBJ_110 ----
            OBJ_110.setText("N\u00b0 de tourn\u00e9e regroup\u00e9");
            OBJ_110.setName("OBJ_110");

            //---- OBJ_84 ----
            OBJ_84.setText("Zone g\u00e9ographique");
            OBJ_84.setName("OBJ_84");

            //---- OBJ_73 ----
            OBJ_73.setText("Code repr\u00e9sentant");
            OBJ_73.setName("OBJ_73");

            //---- OBJ_83 ----
            OBJ_83.setText("Mode d'exp\u00e9dition");
            OBJ_83.setName("OBJ_83");

            //---- OBJ_96 ----
            OBJ_96.setText("Magasin");
            OBJ_96.setName("OBJ_96");

            //---- TOGEO ----
            TOGEO.setComponentPopupMenu(BTDA);
            TOGEO.setName("TOGEO");

            //---- TORGR ----
            TORGR.setComponentPopupMenu(BTDA);
            TORGR.setName("TORGR");

            //---- OBJ_97 ----
            OBJ_97.setText("D\u00e9lai");
            OBJ_97.setName("OBJ_97");

            //---- OBJ_98 ----
            OBJ_98.setText("Type");
            OBJ_98.setName("OBJ_98");

            //---- TOMEX ----
            TOMEX.setComponentPopupMenu(BTDA);
            TOMEX.setName("TOMEX");

            //---- TOMAG ----
            TOMAG.setComponentPopupMenu(BTDA);
            TOMAG.setName("TOMAG");

            //---- TOREP ----
            TOREP.setComponentPopupMenu(BTDA);
            TOREP.setName("TOREP");

            //---- TODEL ----
            TODEL.setComponentPopupMenu(BTDA);
            TODEL.setName("TODEL");

            //---- TOTYP ----
            TOTYP.setComponentPopupMenu(BTDA);
            TOTYP.setName("TOTYP");

            GroupLayout xTitledPanel5ContentContainerLayout = new GroupLayout(xTitledPanel5ContentContainer);
            xTitledPanel5ContentContainer.setLayout(xTitledPanel5ContentContainerLayout);
            xTitledPanel5ContentContainerLayout.setHorizontalGroup(
              xTitledPanel5ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel5ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 550, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel5ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                  .addGap(36, 36, 36)
                  .addComponent(TOMEX, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(95, 95, 95)
                  .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(TOGEO, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel5ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(OBJ_96, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addGap(95, 95, 95)
                  .addComponent(TOMAG, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(95, 95, 95)
                  .addComponent(OBJ_97, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                  .addGap(119, 119, 119)
                  .addComponent(TODEL, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                  .addGap(52, 52, 52)
                  .addComponent(OBJ_98, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                  .addGap(4, 4, 4)
                  .addComponent(TOTYP, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel5ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(OBJ_110, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)
                  .addGap(8, 8, 8)
                  .addComponent(TORGR, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGap(65, 65, 65)
                  .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                  .addGap(41, 41, 41)
                  .addComponent(TOREP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
            );
            xTitledPanel5ContentContainerLayout.setVerticalGroup(
              xTitledPanel5ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel5ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addGroup(xTitledPanel5ContentContainerLayout.createParallelGroup()
                    .addComponent(TOMEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TOGEO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel5ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel5ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel5ContentContainerLayout.createParallelGroup()
                    .addComponent(TOMAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TODEL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TOTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel5ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel5ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_96, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_97, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_98, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel5ContentContainerLayout.createParallelGroup()
                    .addComponent(TORGR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(TOREP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel5ContentContainerLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addGroup(xTitledPanel5ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_110, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))))
            );
          }

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Jours de tourn\u00e9e");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //---- OBJ_74 ----
            OBJ_74.setText("\u00e0 partir");
            OBJ_74.setName("OBJ_74");
            xTitledPanel4ContentContainer.add(OBJ_74);
            OBJ_74.setBounds(25, 14, 57, 20);

            //---- OBJ_75 ----
            OBJ_75.setText("du");
            OBJ_75.setName("OBJ_75");
            xTitledPanel4ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(155, 14, 18, 20);

            //---- OBJ_76 ----
            OBJ_76.setText("du");
            OBJ_76.setName("OBJ_76");
            xTitledPanel4ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(410, 14, 18, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("ou");
            OBJ_77.setName("OBJ_77");
            xTitledPanel4ContentContainer.add(OBJ_77);
            OBJ_77.setBounds(25, 49, 18, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("du");
            OBJ_78.setName("OBJ_78");
            xTitledPanel4ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(155, 49, 18, 20);

            //---- OBJ_79 ----
            OBJ_79.setText("du");
            OBJ_79.setName("OBJ_79");
            xTitledPanel4ContentContainer.add(OBJ_79);
            OBJ_79.setBounds(410, 49, 18, 20);

            //---- TODX1 ----
            TODX1.setName("TODX1");
            xTitledPanel4ContentContainer.add(TODX1);
            TODX1.setBounds(190, 10, 90, TODX1.getPreferredSize().height);

            //---- TODX3 ----
            TODX3.setName("TODX3");
            xTitledPanel4ContentContainer.add(TODX3);
            TODX3.setBounds(190, 45, 90, TODX3.getPreferredSize().height);

            //---- TODX2 ----
            TODX2.setName("TODX2");
            xTitledPanel4ContentContainer.add(TODX2);
            TODX2.setBounds(450, 10, 90, TODX2.getPreferredSize().height);

            //---- TODX4 ----
            TODX4.setName("TODX4");
            xTitledPanel4ContentContainer.add(TODX4);
            TODX4.setBounds(450, 45, 90, TODX4.getPreferredSize().height);

            //======== panel5 ========
            {
              panel5.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- OBJ_180 ----
              OBJ_180.setText("Dimanche");
              OBJ_180.setName("OBJ_180");
              panel5.add(OBJ_180);
              OBJ_180.setBounds(5, 145, 64, 20);

              //---- OBJ_178 ----
              OBJ_178.setText("Vendredi");
              OBJ_178.setName("OBJ_178");
              panel5.add(OBJ_178);
              OBJ_178.setBounds(5, 105, 58, 20);

              //---- OBJ_173 ----
              OBJ_173.setText("Semaine");
              OBJ_173.setName("OBJ_173");
              panel5.add(OBJ_173);
              OBJ_173.setBounds(5, 5, 57, 20);

              //---- OBJ_176 ----
              OBJ_176.setText("Mercredi");
              OBJ_176.setName("OBJ_176");
              panel5.add(OBJ_176);
              OBJ_176.setBounds(5, 65, 56, 20);

              //---- OBJ_179 ----
              OBJ_179.setText("Samedi");
              OBJ_179.setName("OBJ_179");
              panel5.add(OBJ_179);
              OBJ_179.setBounds(5, 125, 50, 20);

              //---- OBJ_175 ----
              OBJ_175.setText("Mardi");
              OBJ_175.setName("OBJ_175");
              panel5.add(OBJ_175);
              OBJ_175.setBounds(5, 45, 37, 20);

              //---- OBJ_177 ----
              OBJ_177.setText("Jeudi");
              OBJ_177.setName("OBJ_177");
              panel5.add(OBJ_177);
              OBJ_177.setBounds(5, 85, 36, 20);

              //---- OBJ_174 ----
              OBJ_174.setText("Lundi");
              OBJ_174.setName("OBJ_174");
              panel5.add(OBJ_174);
              OBJ_174.setBounds(5, 25, 35, 20);

              //---- TJLU21 ----
              TJLU21.setText("");
              TJLU21.setComponentPopupMenu(BTDA);
              TJLU21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJLU21.setName("TJLU21");
              panel5.add(TJLU21);
              TJLU21.setBounds(115, 25, 15, 20);

              //---- TJLU22 ----
              TJLU22.setText("");
              TJLU22.setComponentPopupMenu(BTDA);
              TJLU22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJLU22.setName("TJLU22");
              panel5.add(TJLU22);
              TJLU22.setBounds(135, 25, 15, 20);

              //---- TJLU23 ----
              TJLU23.setText("");
              TJLU23.setComponentPopupMenu(BTDA);
              TJLU23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJLU23.setName("TJLU23");
              panel5.add(TJLU23);
              TJLU23.setBounds(155, 25, 15, 20);

              //---- TJLU24 ----
              TJLU24.setText("");
              TJLU24.setComponentPopupMenu(BTDA);
              TJLU24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJLU24.setName("TJLU24");
              panel5.add(TJLU24);
              TJLU24.setBounds(175, 25, 15, 20);

              //---- TJLU25 ----
              TJLU25.setText("");
              TJLU25.setComponentPopupMenu(BTDA);
              TJLU25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJLU25.setName("TJLU25");
              panel5.add(TJLU25);
              TJLU25.setBounds(195, 25, 15, 20);

              //---- TJMA21 ----
              TJMA21.setText("");
              TJMA21.setComponentPopupMenu(BTDA);
              TJMA21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJMA21.setName("TJMA21");
              panel5.add(TJMA21);
              TJMA21.setBounds(115, 45, 15, 20);

              //---- TJMA22 ----
              TJMA22.setText("");
              TJMA22.setComponentPopupMenu(BTDA);
              TJMA22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJMA22.setName("TJMA22");
              panel5.add(TJMA22);
              TJMA22.setBounds(135, 45, 15, 20);

              //---- TJMA23 ----
              TJMA23.setText("");
              TJMA23.setComponentPopupMenu(BTDA);
              TJMA23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJMA23.setName("TJMA23");
              panel5.add(TJMA23);
              TJMA23.setBounds(155, 45, 15, 20);

              //---- TJMA24 ----
              TJMA24.setText("");
              TJMA24.setComponentPopupMenu(BTDA);
              TJMA24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJMA24.setName("TJMA24");
              panel5.add(TJMA24);
              TJMA24.setBounds(175, 45, 15, 20);

              //---- TJMA25 ----
              TJMA25.setText("");
              TJMA25.setComponentPopupMenu(BTDA);
              TJMA25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJMA25.setName("TJMA25");
              panel5.add(TJMA25);
              TJMA25.setBounds(195, 45, 15, 20);

              //---- TJME21 ----
              TJME21.setText("");
              TJME21.setComponentPopupMenu(BTDA);
              TJME21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJME21.setName("TJME21");
              panel5.add(TJME21);
              TJME21.setBounds(115, 65, 15, 20);

              //---- TJME22 ----
              TJME22.setText("");
              TJME22.setComponentPopupMenu(BTDA);
              TJME22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJME22.setName("TJME22");
              panel5.add(TJME22);
              TJME22.setBounds(135, 65, 15, 20);

              //---- TJME23 ----
              TJME23.setText("");
              TJME23.setComponentPopupMenu(BTDA);
              TJME23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJME23.setName("TJME23");
              panel5.add(TJME23);
              TJME23.setBounds(155, 65, 15, 20);

              //---- TJME24 ----
              TJME24.setText("");
              TJME24.setComponentPopupMenu(BTDA);
              TJME24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJME24.setName("TJME24");
              panel5.add(TJME24);
              TJME24.setBounds(175, 65, 15, 20);

              //---- TJME25 ----
              TJME25.setText("");
              TJME25.setComponentPopupMenu(BTDA);
              TJME25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJME25.setName("TJME25");
              panel5.add(TJME25);
              TJME25.setBounds(195, 65, 15, 20);

              //---- TJJE21 ----
              TJJE21.setText("");
              TJJE21.setComponentPopupMenu(BTDA);
              TJJE21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJJE21.setName("TJJE21");
              panel5.add(TJJE21);
              TJJE21.setBounds(115, 85, 15, 20);

              //---- TJJE22 ----
              TJJE22.setText("");
              TJJE22.setComponentPopupMenu(BTDA);
              TJJE22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJJE22.setName("TJJE22");
              panel5.add(TJJE22);
              TJJE22.setBounds(135, 85, 15, 20);

              //---- TJJE23 ----
              TJJE23.setText("");
              TJJE23.setComponentPopupMenu(BTDA);
              TJJE23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJJE23.setName("TJJE23");
              panel5.add(TJJE23);
              TJJE23.setBounds(155, 85, 15, 20);

              //---- TJJE24 ----
              TJJE24.setText("");
              TJJE24.setComponentPopupMenu(BTDA);
              TJJE24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJJE24.setName("TJJE24");
              panel5.add(TJJE24);
              TJJE24.setBounds(175, 85, 15, 20);

              //---- TJJE25 ----
              TJJE25.setText("");
              TJJE25.setComponentPopupMenu(BTDA);
              TJJE25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJJE25.setName("TJJE25");
              panel5.add(TJJE25);
              TJJE25.setBounds(195, 85, 15, 20);

              //---- TJVE21 ----
              TJVE21.setText("");
              TJVE21.setComponentPopupMenu(BTDA);
              TJVE21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJVE21.setName("TJVE21");
              panel5.add(TJVE21);
              TJVE21.setBounds(115, 105, 15, 20);

              //---- TJVE22 ----
              TJVE22.setText("");
              TJVE22.setComponentPopupMenu(BTDA);
              TJVE22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJVE22.setName("TJVE22");
              panel5.add(TJVE22);
              TJVE22.setBounds(135, 105, 15, 20);

              //---- TJVE23 ----
              TJVE23.setText("");
              TJVE23.setComponentPopupMenu(BTDA);
              TJVE23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJVE23.setName("TJVE23");
              panel5.add(TJVE23);
              TJVE23.setBounds(155, 105, 15, 20);

              //---- TJVE24 ----
              TJVE24.setText("");
              TJVE24.setComponentPopupMenu(BTDA);
              TJVE24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJVE24.setName("TJVE24");
              panel5.add(TJVE24);
              TJVE24.setBounds(175, 105, 15, 20);

              //---- TJVE25 ----
              TJVE25.setText("");
              TJVE25.setComponentPopupMenu(BTDA);
              TJVE25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJVE25.setName("TJVE25");
              panel5.add(TJVE25);
              TJVE25.setBounds(195, 105, 15, 20);

              //---- TJSA21 ----
              TJSA21.setText("");
              TJSA21.setComponentPopupMenu(BTDA);
              TJSA21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJSA21.setName("TJSA21");
              panel5.add(TJSA21);
              TJSA21.setBounds(115, 125, 15, 20);

              //---- TJSA22 ----
              TJSA22.setText("");
              TJSA22.setComponentPopupMenu(BTDA);
              TJSA22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJSA22.setName("TJSA22");
              panel5.add(TJSA22);
              TJSA22.setBounds(135, 125, 15, 20);

              //---- TJSA23 ----
              TJSA23.setText("");
              TJSA23.setComponentPopupMenu(BTDA);
              TJSA23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJSA23.setName("TJSA23");
              panel5.add(TJSA23);
              TJSA23.setBounds(155, 125, 15, 20);

              //---- TJSA24 ----
              TJSA24.setText("");
              TJSA24.setComponentPopupMenu(BTDA);
              TJSA24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJSA24.setName("TJSA24");
              panel5.add(TJSA24);
              TJSA24.setBounds(175, 125, 15, 20);

              //---- TJSA25 ----
              TJSA25.setText("");
              TJSA25.setComponentPopupMenu(BTDA);
              TJSA25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJSA25.setName("TJSA25");
              panel5.add(TJSA25);
              TJSA25.setBounds(195, 125, 15, 20);

              //---- TJDI21 ----
              TJDI21.setText("");
              TJDI21.setComponentPopupMenu(BTDA);
              TJDI21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJDI21.setName("TJDI21");
              panel5.add(TJDI21);
              TJDI21.setBounds(115, 145, 15, 20);

              //---- TJDI22 ----
              TJDI22.setText("");
              TJDI22.setComponentPopupMenu(BTDA);
              TJDI22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJDI22.setName("TJDI22");
              panel5.add(TJDI22);
              TJDI22.setBounds(135, 145, 15, 20);

              //---- TJDI23 ----
              TJDI23.setText("");
              TJDI23.setComponentPopupMenu(BTDA);
              TJDI23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJDI23.setName("TJDI23");
              panel5.add(TJDI23);
              TJDI23.setBounds(155, 145, 15, 20);

              //---- TJDI24 ----
              TJDI24.setText("");
              TJDI24.setComponentPopupMenu(BTDA);
              TJDI24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJDI24.setName("TJDI24");
              panel5.add(TJDI24);
              TJDI24.setBounds(175, 145, 15, 20);

              //---- TJDI25 ----
              TJDI25.setText("");
              TJDI25.setComponentPopupMenu(BTDA);
              TJDI25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJDI25.setName("TJDI25");
              panel5.add(TJDI25);
              TJDI25.setBounds(195, 145, 15, 20);

              //======== panel4 ========
              {
                panel4.setOpaque(false);
                panel4.setName("panel4");
                panel4.setLayout(null);

                //---- label16 ----
                label16.setText("1");
                label16.setName("label16");
                panel4.add(label16);
                label16.setBounds(5, 0, 10, 20);

                //---- label17 ----
                label17.setText("2");
                label17.setName("label17");
                panel4.add(label17);
                label17.setBounds(25, 0, 10, 20);

                //---- label18 ----
                label18.setText("3");
                label18.setName("label18");
                panel4.add(label18);
                label18.setBounds(45, 0, 10, 20);

                //---- label15 ----
                label15.setText("4");
                label15.setName("label15");
                panel4.add(label15);
                label15.setBounds(65, 0, 10, 20);

                //---- label19 ----
                label19.setText("5");
                label19.setName("label19");
                panel4.add(label19);
                label19.setBounds(85, 0, 10, 20);
              }
              panel5.add(panel4);
              panel4.setBounds(115, 5, 105, 20);
            }
            xTitledPanel4ContentContainer.add(panel5);
            panel5.setBounds(315, 80, 225, 175);

            //======== panel1 ========
            {
              panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_156 ----
              OBJ_156.setText("Dimanche");
              OBJ_156.setName("OBJ_156");
              panel1.add(OBJ_156);
              OBJ_156.setBounds(5, 145, 64, 20);

              //---- OBJ_134 ----
              OBJ_134.setText("Vendredi");
              OBJ_134.setName("OBJ_134");
              panel1.add(OBJ_134);
              OBJ_134.setBounds(5, 105, 58, 20);

              //---- OBJ_81 ----
              OBJ_81.setText("Semaine");
              OBJ_81.setName("OBJ_81");
              panel1.add(OBJ_81);
              OBJ_81.setBounds(5, 5, 57, 20);

              //---- OBJ_111 ----
              OBJ_111.setText("Mercredi");
              OBJ_111.setName("OBJ_111");
              panel1.add(OBJ_111);
              OBJ_111.setBounds(5, 65, 56, 20);

              //---- OBJ_145 ----
              OBJ_145.setText("Samedi");
              OBJ_145.setName("OBJ_145");
              panel1.add(OBJ_145);
              OBJ_145.setBounds(5, 125, 50, 20);

              //---- OBJ_99 ----
              OBJ_99.setText("Mardi");
              OBJ_99.setName("OBJ_99");
              panel1.add(OBJ_99);
              OBJ_99.setBounds(5, 45, 37, 20);

              //---- OBJ_122 ----
              OBJ_122.setText("Jeudi");
              OBJ_122.setName("OBJ_122");
              panel1.add(OBJ_122);
              OBJ_122.setBounds(5, 85, 36, 20);

              //---- OBJ_85 ----
              OBJ_85.setText("Lundi");
              OBJ_85.setName("OBJ_85");
              panel1.add(OBJ_85);
              OBJ_85.setBounds(5, 25, 35, 20);

              //---- TJLU11 ----
              TJLU11.setText("");
              TJLU11.setComponentPopupMenu(BTDA);
              TJLU11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJLU11.setName("TJLU11");
              panel1.add(TJLU11);
              TJLU11.setBounds(155, 25, 15, 20);

              //---- TJLU12 ----
              TJLU12.setText("");
              TJLU12.setComponentPopupMenu(BTDA);
              TJLU12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJLU12.setName("TJLU12");
              panel1.add(TJLU12);
              TJLU12.setBounds(175, 25, 15, 20);

              //---- TJLU13 ----
              TJLU13.setText("");
              TJLU13.setComponentPopupMenu(BTDA);
              TJLU13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJLU13.setName("TJLU13");
              panel1.add(TJLU13);
              TJLU13.setBounds(195, 25, 15, 20);

              //---- TJLU14 ----
              TJLU14.setText("");
              TJLU14.setComponentPopupMenu(BTDA);
              TJLU14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJLU14.setName("TJLU14");
              panel1.add(TJLU14);
              TJLU14.setBounds(215, 25, 15, 20);

              //---- TJLU15 ----
              TJLU15.setText("");
              TJLU15.setComponentPopupMenu(BTDA);
              TJLU15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJLU15.setName("TJLU15");
              panel1.add(TJLU15);
              TJLU15.setBounds(235, 25, 15, 20);

              //---- TJMA11 ----
              TJMA11.setText("");
              TJMA11.setComponentPopupMenu(BTDA);
              TJMA11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJMA11.setName("TJMA11");
              panel1.add(TJMA11);
              TJMA11.setBounds(155, 45, 15, 20);

              //---- TJMA12 ----
              TJMA12.setText("");
              TJMA12.setComponentPopupMenu(BTDA);
              TJMA12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJMA12.setName("TJMA12");
              panel1.add(TJMA12);
              TJMA12.setBounds(175, 45, 15, 20);

              //---- TJMA13 ----
              TJMA13.setText("");
              TJMA13.setComponentPopupMenu(BTDA);
              TJMA13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJMA13.setName("TJMA13");
              panel1.add(TJMA13);
              TJMA13.setBounds(195, 45, 15, 20);

              //---- TJMA14 ----
              TJMA14.setText("");
              TJMA14.setComponentPopupMenu(BTDA);
              TJMA14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJMA14.setName("TJMA14");
              panel1.add(TJMA14);
              TJMA14.setBounds(215, 45, 15, 20);

              //---- TJMA15 ----
              TJMA15.setText("");
              TJMA15.setComponentPopupMenu(BTDA);
              TJMA15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJMA15.setName("TJMA15");
              panel1.add(TJMA15);
              TJMA15.setBounds(235, 45, 15, 20);

              //---- TJME11 ----
              TJME11.setText("");
              TJME11.setComponentPopupMenu(BTDA);
              TJME11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJME11.setName("TJME11");
              panel1.add(TJME11);
              TJME11.setBounds(155, 65, 15, 20);

              //---- TJME12 ----
              TJME12.setText("");
              TJME12.setComponentPopupMenu(BTDA);
              TJME12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJME12.setName("TJME12");
              panel1.add(TJME12);
              TJME12.setBounds(175, 65, 15, 20);

              //---- TJME13 ----
              TJME13.setText("");
              TJME13.setComponentPopupMenu(BTDA);
              TJME13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJME13.setName("TJME13");
              panel1.add(TJME13);
              TJME13.setBounds(195, 65, 15, 20);

              //---- TJME14 ----
              TJME14.setText("");
              TJME14.setComponentPopupMenu(BTDA);
              TJME14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJME14.setName("TJME14");
              panel1.add(TJME14);
              TJME14.setBounds(215, 65, 15, 20);

              //---- TJME15 ----
              TJME15.setText("");
              TJME15.setComponentPopupMenu(BTDA);
              TJME15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJME15.setName("TJME15");
              panel1.add(TJME15);
              TJME15.setBounds(235, 65, 15, 20);

              //---- TJJE11 ----
              TJJE11.setText("");
              TJJE11.setComponentPopupMenu(BTDA);
              TJJE11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJJE11.setName("TJJE11");
              panel1.add(TJJE11);
              TJJE11.setBounds(155, 85, 15, 20);

              //---- TJJE12 ----
              TJJE12.setText("");
              TJJE12.setComponentPopupMenu(BTDA);
              TJJE12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJJE12.setName("TJJE12");
              panel1.add(TJJE12);
              TJJE12.setBounds(175, 85, 15, 20);

              //---- TJJE13 ----
              TJJE13.setText("");
              TJJE13.setComponentPopupMenu(BTDA);
              TJJE13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJJE13.setName("TJJE13");
              panel1.add(TJJE13);
              TJJE13.setBounds(195, 85, 15, 20);

              //---- TJJE14 ----
              TJJE14.setText("");
              TJJE14.setComponentPopupMenu(BTDA);
              TJJE14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJJE14.setName("TJJE14");
              panel1.add(TJJE14);
              TJJE14.setBounds(215, 85, 15, 20);

              //---- TJJE15 ----
              TJJE15.setText("");
              TJJE15.setComponentPopupMenu(BTDA);
              TJJE15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJJE15.setName("TJJE15");
              panel1.add(TJJE15);
              TJJE15.setBounds(235, 85, 15, 20);

              //---- TJVE11 ----
              TJVE11.setText("");
              TJVE11.setComponentPopupMenu(BTDA);
              TJVE11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJVE11.setName("TJVE11");
              panel1.add(TJVE11);
              TJVE11.setBounds(155, 105, 15, 20);

              //---- TJVE12 ----
              TJVE12.setText("");
              TJVE12.setComponentPopupMenu(BTDA);
              TJVE12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJVE12.setName("TJVE12");
              panel1.add(TJVE12);
              TJVE12.setBounds(175, 105, 15, 20);

              //---- TJVE13 ----
              TJVE13.setText("");
              TJVE13.setComponentPopupMenu(BTDA);
              TJVE13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJVE13.setName("TJVE13");
              panel1.add(TJVE13);
              TJVE13.setBounds(195, 105, 15, 20);

              //---- TJVE14 ----
              TJVE14.setText("");
              TJVE14.setComponentPopupMenu(BTDA);
              TJVE14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJVE14.setName("TJVE14");
              panel1.add(TJVE14);
              TJVE14.setBounds(215, 105, 15, 20);

              //---- TJVE15 ----
              TJVE15.setText("");
              TJVE15.setComponentPopupMenu(BTDA);
              TJVE15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJVE15.setName("TJVE15");
              panel1.add(TJVE15);
              TJVE15.setBounds(235, 105, 15, 20);

              //---- TJSA11 ----
              TJSA11.setText("");
              TJSA11.setComponentPopupMenu(BTDA);
              TJSA11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJSA11.setName("TJSA11");
              panel1.add(TJSA11);
              TJSA11.setBounds(155, 125, 15, 20);

              //---- TJSA12 ----
              TJSA12.setText("");
              TJSA12.setComponentPopupMenu(BTDA);
              TJSA12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJSA12.setName("TJSA12");
              panel1.add(TJSA12);
              TJSA12.setBounds(175, 125, 15, 20);

              //---- TJSA13 ----
              TJSA13.setText("");
              TJSA13.setComponentPopupMenu(BTDA);
              TJSA13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJSA13.setName("TJSA13");
              panel1.add(TJSA13);
              TJSA13.setBounds(195, 125, 15, 20);

              //---- TJSA14 ----
              TJSA14.setText("");
              TJSA14.setComponentPopupMenu(BTDA);
              TJSA14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJSA14.setName("TJSA14");
              panel1.add(TJSA14);
              TJSA14.setBounds(215, 125, 15, 20);

              //---- TJSA15 ----
              TJSA15.setText("");
              TJSA15.setComponentPopupMenu(BTDA);
              TJSA15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJSA15.setName("TJSA15");
              panel1.add(TJSA15);
              TJSA15.setBounds(235, 125, 15, 20);

              //---- TJDI11 ----
              TJDI11.setText("");
              TJDI11.setComponentPopupMenu(BTDA);
              TJDI11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJDI11.setName("TJDI11");
              panel1.add(TJDI11);
              TJDI11.setBounds(155, 145, 15, 20);

              //---- TJDI12 ----
              TJDI12.setText("");
              TJDI12.setComponentPopupMenu(BTDA);
              TJDI12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJDI12.setName("TJDI12");
              panel1.add(TJDI12);
              TJDI12.setBounds(175, 145, 15, 20);

              //---- TJDI13 ----
              TJDI13.setText("");
              TJDI13.setComponentPopupMenu(BTDA);
              TJDI13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJDI13.setName("TJDI13");
              panel1.add(TJDI13);
              TJDI13.setBounds(195, 145, 15, 20);

              //---- TJDI14 ----
              TJDI14.setText("");
              TJDI14.setComponentPopupMenu(BTDA);
              TJDI14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJDI14.setName("TJDI14");
              panel1.add(TJDI14);
              TJDI14.setBounds(215, 145, 15, 20);

              //---- TJDI15 ----
              TJDI15.setText("");
              TJDI15.setComponentPopupMenu(BTDA);
              TJDI15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TJDI15.setName("TJDI15");
              panel1.add(TJDI15);
              TJDI15.setBounds(235, 145, 15, 20);

              //======== panel3 ========
              {
                panel3.setOpaque(false);
                panel3.setName("panel3");
                panel3.setLayout(null);

                //---- label4 ----
                label4.setText("4");
                label4.setName("label4");
                panel3.add(label4);
                label4.setBounds(70, 3, 10, 20);

                //---- label11 ----
                label11.setText("1");
                label11.setName("label11");
                panel3.add(label11);
                label11.setBounds(10, 3, 10, 20);

                //---- label12 ----
                label12.setText("2");
                label12.setName("label12");
                panel3.add(label12);
                label12.setBounds(30, 3, 10, 20);

                //---- label13 ----
                label13.setText("3");
                label13.setName("label13");
                panel3.add(label13);
                label13.setBounds(50, 3, 10, 20);

                //---- label14 ----
                label14.setText("5");
                label14.setName("label14");
                panel3.add(label14);
                label14.setBounds(90, 3, 10, 20);
              }
              panel1.add(panel3);
              panel3.setBounds(150, 3, 105, 25);
            }
            xTitledPanel4ContentContainer.add(panel1);
            panel1.setBounds(15, 80, 265, 175);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(36, Short.MAX_VALUE)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 610, GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(33, Short.MAX_VALUE)
                .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("Invite");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel5;
  private JPanel panel6;
  private JLabel OBJ_62;
  private XRiTextField TOLIB;
  private JLabel OBJ_110;
  private JLabel OBJ_84;
  private JLabel OBJ_73;
  private JLabel OBJ_83;
  private JLabel OBJ_96;
  private XRiTextField TOGEO;
  private XRiTextField TORGR;
  private JLabel OBJ_97;
  private JLabel OBJ_98;
  private XRiTextField TOMEX;
  private XRiTextField TOMAG;
  private XRiTextField TOREP;
  private XRiTextField TODEL;
  private XRiTextField TOTYP;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_74;
  private JLabel OBJ_75;
  private JLabel OBJ_76;
  private JLabel OBJ_77;
  private JLabel OBJ_78;
  private JLabel OBJ_79;
  private XRiCalendrier TODX1;
  private XRiCalendrier TODX3;
  private XRiCalendrier TODX2;
  private XRiCalendrier TODX4;
  private JPanel panel5;
  private JLabel OBJ_180;
  private JLabel OBJ_178;
  private JLabel OBJ_173;
  private JLabel OBJ_176;
  private JLabel OBJ_179;
  private JLabel OBJ_175;
  private JLabel OBJ_177;
  private JLabel OBJ_174;
  private XRiCheckBox TJLU21;
  private XRiCheckBox TJLU22;
  private XRiCheckBox TJLU23;
  private XRiCheckBox TJLU24;
  private XRiCheckBox TJLU25;
  private XRiCheckBox TJMA21;
  private XRiCheckBox TJMA22;
  private XRiCheckBox TJMA23;
  private XRiCheckBox TJMA24;
  private XRiCheckBox TJMA25;
  private XRiCheckBox TJME21;
  private XRiCheckBox TJME22;
  private XRiCheckBox TJME23;
  private XRiCheckBox TJME24;
  private XRiCheckBox TJME25;
  private XRiCheckBox TJJE21;
  private XRiCheckBox TJJE22;
  private XRiCheckBox TJJE23;
  private XRiCheckBox TJJE24;
  private XRiCheckBox TJJE25;
  private XRiCheckBox TJVE21;
  private XRiCheckBox TJVE22;
  private XRiCheckBox TJVE23;
  private XRiCheckBox TJVE24;
  private XRiCheckBox TJVE25;
  private XRiCheckBox TJSA21;
  private XRiCheckBox TJSA22;
  private XRiCheckBox TJSA23;
  private XRiCheckBox TJSA24;
  private XRiCheckBox TJSA25;
  private XRiCheckBox TJDI21;
  private XRiCheckBox TJDI22;
  private XRiCheckBox TJDI23;
  private XRiCheckBox TJDI24;
  private XRiCheckBox TJDI25;
  private JPanel panel4;
  private JLabel label16;
  private JLabel label17;
  private JLabel label18;
  private JLabel label15;
  private JLabel label19;
  private JPanel panel1;
  private JLabel OBJ_156;
  private JLabel OBJ_134;
  private JLabel OBJ_81;
  private JLabel OBJ_111;
  private JLabel OBJ_145;
  private JLabel OBJ_99;
  private JLabel OBJ_122;
  private JLabel OBJ_85;
  private XRiCheckBox TJLU11;
  private XRiCheckBox TJLU12;
  private XRiCheckBox TJLU13;
  private XRiCheckBox TJLU14;
  private XRiCheckBox TJLU15;
  private XRiCheckBox TJMA11;
  private XRiCheckBox TJMA12;
  private XRiCheckBox TJMA13;
  private XRiCheckBox TJMA14;
  private XRiCheckBox TJMA15;
  private XRiCheckBox TJME11;
  private XRiCheckBox TJME12;
  private XRiCheckBox TJME13;
  private XRiCheckBox TJME14;
  private XRiCheckBox TJME15;
  private XRiCheckBox TJJE11;
  private XRiCheckBox TJJE12;
  private XRiCheckBox TJJE13;
  private XRiCheckBox TJJE14;
  private XRiCheckBox TJJE15;
  private XRiCheckBox TJVE11;
  private XRiCheckBox TJVE12;
  private XRiCheckBox TJVE13;
  private XRiCheckBox TJVE14;
  private XRiCheckBox TJVE15;
  private XRiCheckBox TJSA11;
  private XRiCheckBox TJSA12;
  private XRiCheckBox TJSA13;
  private XRiCheckBox TJSA14;
  private XRiCheckBox TJSA15;
  private XRiCheckBox TJDI11;
  private XRiCheckBox TJDI12;
  private XRiCheckBox TJDI13;
  private XRiCheckBox TJDI14;
  private XRiCheckBox TJDI15;
  private JPanel panel3;
  private JLabel label4;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
