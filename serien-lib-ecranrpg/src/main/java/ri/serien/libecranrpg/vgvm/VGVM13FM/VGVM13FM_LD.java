
package ri.serien.libecranrpg.vgvm.VGVM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM13FM_LD extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] WSELD_Value = { "", "D", "P", };
  private String[] _LD01_Title = { "TETLD", };
  private String[] _LD01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11", "WTP12",
      "WTP13", "WTP14", "WTP15", };
  private String[][] _LD01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _LD01_Width = { 565, };
  private String[] WTNOM_Value = { "", "L", "F", "D", };
  private boolean suisUnBon = false;
  
  public VGVM13FM_LD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WSELD.setValeurs(WSELD_Value, null);
    WTNOM.setValeurs(WTNOM_Value, null);
    LD01.setAspectTable(_LD01_Top, _LD01_Title, _LD01_Data, _LD01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LTIT@")).trim());
    OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    OBJ_133.setBorder(new TitledBorder(new EtchedBorder(), lexique.TranslationTable(interpreteurD.analyseExpression("@BONTRAITE@  @NCLE@")).trim(), TitledBorder.LEADING, TitledBorder.TOP));
    OBJ_131.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DBUT@")).trim());
    OBJ_132.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P13RES@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    suisUnBon = lexique.isTrue("92");
    
    OBJ_33.setVisible(!suisUnBon);
    OBJ_34.setVisible(!suisUnBon);
    OBJ_35.setVisible(!suisUnBon);
    OBJ_36.setVisible(!suisUnBon);
    OBJ_37.setVisible(!suisUnBon);
    OBJ_39.setVisible(!suisUnBon);
    OBJ_40.setVisible(!suisUnBon);
    OBJ_41.setVisible(!suisUnBon);
    OBJ_42.setVisible(!suisUnBon);
    OBJ_43.setVisible(!suisUnBon);
    OBJ_44.setVisible(!suisUnBon);
    OBJ_45.setVisible(!suisUnBon);
    OBJ_46.setVisible(!suisUnBon);
    OBJ_47.setVisible(!suisUnBon);
    OBJ_48.setVisible(!suisUnBon);
    OBJ_49.setVisible(!suisUnBon);
    OBJ_50.setVisible(!suisUnBon);
    OBJ_51.setVisible(!suisUnBon);
    
    scroll_droite.setVisible(!suisUnBon);
    
    // labels----------------------------------------------------------------------------
    OBJ_81.setVisible(WNUM.isVisible());
    OBJ_119.setVisible(WIN7.isVisible());
    OBJ_115.setVisible(WNOM.isVisible());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17", false);
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    
    // Contruction du nom du fichier pour l'exportation vers Excel
    lexique.setNomFichierTableur("Edition des bons de ventes");
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTDA.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _LIST_Top, "A", "Enter");
    LD01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _LIST_Top, "A", "Enter");
    LD01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("H");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("F");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_39ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_40ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("?");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_41ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("M");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_42ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("K");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_43ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("Y");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("W");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("à");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_46ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("R");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_47ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("é");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_48ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("ê");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_49ActionPerformed(ActionEvent e) {
    // Contruction du nom du fichier pour l'exportation vers Excel
    lexique.setNomFichierTableur("Edition des bons de ventes");
    LD01.setValeurTop("Z");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_50ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("ë");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_51ActionPerformed(ActionEvent e) {
    LD01.setValeurTop("È");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_135ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F7"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void OBJ_136ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F7"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void OBJ_137ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F7"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void OBJ_138ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F9"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    // ScriptCall("GVM1101")
  }
  
  private void LD01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _LIST_Top, "A", "Enter",e);
    if (LD01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_80 = new JLabel();
    WETB = new XRiTextField();
    OBJ_81 = new JLabel();
    WNUM = new XRiTextField();
    WSUF = new XRiTextField();
    OBJ_58 = new RiZoneSortie();
    WTFA = new XRiTextField();
    WIN18 = new XRiTextField();
    WE1NFA = new XRiTextField();
    OBJ_90 = new RiZoneSortie();
    WNUMF = new XRiTextField();
    WDTFX = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    LD01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel1 = new JPanel();
    WTNOM = new XRiComboBox();
    WSELD = new XRiComboBox();
    WNOM = new XRiTextField();
    OBJ_115 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_119 = new JLabel();
    WNFAD = new XRiTextField();
    WBOND = new XRiTextField();
    OBJ_112 = new JLabel();
    WSAN = new XRiTextField();
    WCLI = new XRiTextField();
    WACT = new XRiTextField();
    WSELZ = new XRiTextField();
    OBJ_98 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_95 = new JLabel();
    WVDE = new XRiTextField();
    OBJ_96 = new JLabel();
    WLIV = new XRiTextField();
    WREP = new XRiTextField();
    WIN7 = new XRiTextField();
    OBJ_97 = new JLabel();
    OBJ_116 = new JLabel();
    BTDA = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();
    OBJ_133 = new JPanel();
    OBJ_135 = new JButton();
    OBJ_136 = new JButton();
    OBJ_137 = new JButton();
    OBJ_138 = new JButton();
    OBJ_131 = new JLabel();
    OBJ_132 = new JLabel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_33 = new JMenuItem();
    OBJ_34 = new JMenuItem();
    OBJ_35 = new JMenuItem();
    OBJ_36 = new JMenuItem();
    OBJ_37 = new JMenuItem();
    OBJ_39 = new JMenuItem();
    OBJ_40 = new JMenuItem();
    OBJ_41 = new JMenuItem();
    OBJ_42 = new JMenuItem();
    OBJ_43 = new JMenuItem();
    OBJ_44 = new JMenuItem();
    OBJ_45 = new JMenuItem();
    OBJ_46 = new JMenuItem();
    OBJ_47 = new JMenuItem();
    OBJ_48 = new JMenuItem();
    OBJ_49 = new JMenuItem();
    OBJ_50 = new JMenuItem();
    OBJ_51 = new JMenuItem();
    riBoutonDetail1 = new SNBoutonDetail();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@LTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(950, 32));
          p_tete_gauche.setMinimumSize(new Dimension(950, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_80 ----
          OBJ_80.setText("Etablissement");
          OBJ_80.setName("OBJ_80");

          //---- WETB ----
          WETB.setComponentPopupMenu(BTDA);
          WETB.setName("WETB");

          //---- OBJ_81 ----
          OBJ_81.setText("Num\u00e9ro");
          OBJ_81.setName("OBJ_81");

          //---- WNUM ----
          WNUM.setName("WNUM");

          //---- WSUF ----
          WSUF.setName("WSUF");

          //---- OBJ_58 ----
          OBJ_58.setText("@CLNOM@");
          OBJ_58.setOpaque(false);
          OBJ_58.setName("OBJ_58");

          //---- WTFA ----
          WTFA.setName("WTFA");

          //---- WIN18 ----
          WIN18.setName("WIN18");

          //---- WE1NFA ----
          WE1NFA.setName("WE1NFA");

          //---- OBJ_90 ----
          OBJ_90.setText("Recherche");
          OBJ_90.setOpaque(false);
          OBJ_90.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_90.setName("OBJ_90");

          //---- WNUMF ----
          WNUMF.setName("WNUMF");

          //---- WDTFX ----
          WDTFX.setComponentPopupMenu(BTDA);
          WDTFX.setName("WDTFX");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WNUMF, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WDTFX, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(60, 60, 60)
                    .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 376, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(WTFA, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WIN18, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WE1NFA, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_90, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_80))
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_81))
              .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WNUMF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WDTFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addComponent(WTFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WIN18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WE1NFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_90, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(60, 0));
          p_tete_droite.setMinimumSize(new Dimension(60, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Totalisation demande");
              riSousMenu_bt6.setToolTipText("Totalisation sur la demande en cours");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Lancement facturation");
              riSousMenu_bt7.setToolTipText("Lancement de la facturation");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Regroupement logistique");
              riSousMenu_bt8.setToolTipText("Regroupement logistique");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Restriction de recherche");
              riSousMenu_bt9.setToolTipText("Restriction de la recherche");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Disponibilit\u00e9 stock");
              riSousMenu_bt10.setToolTipText("Contr\u00f4le disponibilit\u00e9 stock");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Lignes par groupe/famille");
              riSousMenu_bt11.setToolTipText("D\u00e9tail des lignes tri\u00e9es par Groupe/Famille");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("S\u00e9lection traitem. group\u00e9");
              riSousMenu_bt12.setToolTipText("On/Off s\u00e9lection multiple pour traitement group\u00e9");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Exportation vers tableur");
              riSousMenu_bt13.setToolTipText("Exportation de la s\u00e9lection vers tableur");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 510));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- LD01 ----
            LD01.setComponentPopupMenu(BTD);
            LD01.setName("LD01");
            LD01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(LD01);
          }

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WTNOM ----
            WTNOM.setModel(new DefaultComboBoxModel(new String[] {
              "client ou prospect r\u00e9f\u00e9renc\u00e9",
              "l'adresse de livraison",
              "l'adresse de facturation",
              "prospect"
            }));
            WTNOM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTNOM.setName("WTNOM");
            panel1.add(WTNOM);
            WTNOM.setBounds(219, 95, 195, WTNOM.getPreferredSize().height);

            //---- WSELD ----
            WSELD.setModel(new DefaultComboBoxModel(new String[] {
              "Bons",
              "Devis",
              "Prospects"
            }));
            WSELD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WSELD.setName("WSELD");
            panel1.add(WSELD);
            WSELD.setBounds(550, 96, 100, WSELD.getPreferredSize().height);

            //---- WNOM ----
            WNOM.setName("WNOM");
            panel1.add(WNOM);
            WNOM.setBounds(25, 95, 160, WNOM.getPreferredSize().height);

            //---- OBJ_115 ----
            OBJ_115.setText("Recherche par nom");
            OBJ_115.setName("OBJ_115");
            panel1.add(OBJ_115);
            OBJ_115.setBounds(25, 75, 121, 20);

            //---- OBJ_111 ----
            OBJ_111.setText("Repr\u00e9sentant");
            OBJ_111.setName("OBJ_111");
            panel1.add(OBJ_111);
            OBJ_111.setBounds(425, 45, 84, 18);

            //---- OBJ_119 ----
            OBJ_119.setText("S\u00e9lection");
            OBJ_119.setName("OBJ_119");
            panel1.add(OBJ_119);
            OBJ_119.setBounds(447, 100, 59, 18);

            //---- WNFAD ----
            WNFAD.setComponentPopupMenu(BTDA);
            WNFAD.setName("WNFAD");
            panel1.add(WNFAD);
            WNFAD.setBounds(25, 40, 66, WNFAD.getPreferredSize().height);

            //---- WBOND ----
            WBOND.setName("WBOND");
            panel1.add(WBOND);
            WBOND.setBounds(25, 40, 58, WBOND.getPreferredSize().height);

            //---- OBJ_112 ----
            OBJ_112.setText("Vendeur");
            OBJ_112.setName("OBJ_112");
            panel1.add(OBJ_112);
            OBJ_112.setBounds(555, 45, 54, 18);

            //---- WSAN ----
            WSAN.setComponentPopupMenu(BTDA);
            WSAN.setName("WSAN");
            panel1.add(WSAN);
            WSAN.setBounds(95, 40, 50, WSAN.getPreferredSize().height);

            //---- WCLI ----
            WCLI.setComponentPopupMenu(BTDA);
            WCLI.setName("WCLI");
            panel1.add(WCLI);
            WCLI.setBounds(320, 40, 58, WCLI.getPreferredSize().height);

            //---- WACT ----
            WACT.setComponentPopupMenu(BTDA);
            WACT.setName("WACT");
            panel1.add(WACT);
            WACT.setBounds(150, 40, 50, WACT.getPreferredSize().height);

            //---- WSELZ ----
            WSELZ.setComponentPopupMenu(BTDA);
            WSELZ.setName("WSELZ");
            panel1.add(WSELZ);
            WSELZ.setBounds(205, 40, 60, WSELZ.getPreferredSize().height);

            //---- OBJ_98 ----
            OBJ_98.setText("Prospect");
            OBJ_98.setName("OBJ_98");
            panel1.add(OBJ_98);
            OBJ_98.setBounds(383, 23, 57, 14);

            //---- OBJ_99 ----
            OBJ_99.setText("Client");
            OBJ_99.setName("OBJ_99");
            panel1.add(OBJ_99);
            OBJ_99.setBounds(322, 20, 57, 21);

            //---- OBJ_94 ----
            OBJ_94.setText("N\u00b0D\u00e9but");
            OBJ_94.setName("OBJ_94");
            panel1.add(OBJ_94);
            OBJ_94.setBounds(27, 20, 63, 20);

            //---- OBJ_95 ----
            OBJ_95.setText("Section");
            OBJ_95.setName("OBJ_95");
            panel1.add(OBJ_95);
            OBJ_95.setBounds(97, 20, 48, 20);

            //---- WVDE ----
            WVDE.setComponentPopupMenu(BTDA);
            WVDE.setName("WVDE");
            panel1.add(WVDE);
            WVDE.setBounds(610, 40, 40, WVDE.getPreferredSize().height);

            //---- OBJ_96 ----
            OBJ_96.setText("Affaire");
            OBJ_96.setName("OBJ_96");
            panel1.add(OBJ_96);
            OBJ_96.setBounds(152, 20, 48, 20);

            //---- WLIV ----
            WLIV.setComponentPopupMenu(BTDA);
            WLIV.setName("WLIV");
            panel1.add(WLIV);
            WLIV.setBounds(380, 40, 34, WLIV.getPreferredSize().height);

            //---- WREP ----
            WREP.setComponentPopupMenu(BTDA);
            WREP.setName("WREP");
            panel1.add(WREP);
            WREP.setBounds(510, 40, 34, WREP.getPreferredSize().height);

            //---- WIN7 ----
            WIN7.setComponentPopupMenu(BTDA);
            WIN7.setName("WIN7");
            panel1.add(WIN7);
            WIN7.setBounds(510, 95, 24, WIN7.getPreferredSize().height);

            //---- OBJ_97 ----
            OBJ_97.setText("Etat");
            OBJ_97.setName("OBJ_97");
            panel1.add(OBJ_97);
            OBJ_97.setBounds(207, 20, 38, 20);

            //---- OBJ_116 ----
            OBJ_116.setText("de");
            OBJ_116.setName("OBJ_116");
            panel1.add(OBJ_116);
            OBJ_116.setBounds(195, 100, 19, OBJ_116.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 645, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                      .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))))
                .addGap(49, 49, 49))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 269, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
                    .addGap(21, 21, 21)
                    .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_6);

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== OBJ_133 ========
    {
      OBJ_133.setBorder(new TitledBorder(new EtchedBorder(), "@BONTRAITE@  @NCLE@", TitledBorder.LEADING, TitledBorder.TOP));
      OBJ_133.setOpaque(false);
      OBJ_133.setName("OBJ_133");
      OBJ_133.setLayout(null);

      //---- OBJ_135 ----
      OBJ_135.setText("Edition");
      OBJ_135.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_135.setName("OBJ_135");
      OBJ_135.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_135ActionPerformed(e);
        }
      });
      OBJ_133.add(OBJ_135);
      OBJ_135.setBounds(20, 15, 118, 24);

      //---- OBJ_136 ----
      OBJ_136.setText("Traitement");
      OBJ_136.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_136.setName("OBJ_136");
      OBJ_136.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_136ActionPerformed(e);
        }
      });
      OBJ_133.add(OBJ_136);
      OBJ_136.setBounds(15, 21, 118, 24);

      //---- OBJ_137 ----
      OBJ_137.setText("Facturation");
      OBJ_137.setToolTipText("Lancement de la facturation");
      OBJ_137.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_137.setName("OBJ_137");
      OBJ_137.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_137ActionPerformed(e);
        }
      });
      OBJ_133.add(OBJ_137);
      OBJ_137.setBounds(15, 21, 118, 24);

      //---- OBJ_138 ----
      OBJ_138.setText("Bons s\u00e9lectionn\u00e9s");
      OBJ_138.setToolTipText("Liste des bons s\u00e9lectionn\u00e9s seulement");
      OBJ_138.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_138.setName("OBJ_138");
      OBJ_138.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_138ActionPerformed(e);
        }
      });
      OBJ_133.add(OBJ_138);
      OBJ_138.setBounds(145, 20, 135, 24);

      OBJ_133.setPreferredSize(new Dimension(290, 55));
    }

    //---- OBJ_131 ----
    OBJ_131.setText("@DBUT@");
    OBJ_131.setName("OBJ_131");

    //---- OBJ_132 ----
    OBJ_132.setText("@P13RES@");
    OBJ_132.setName("OBJ_132");

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setHorizontalTextPosition(SwingConstants.LEADING);
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_22 ----
      OBJ_22.setText("Afficher");
      OBJ_22.setHorizontalTextPosition(SwingConstants.LEADING);
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_33 ----
      OBJ_33.setText("Historique du bon");
      OBJ_33.setHorizontalTextPosition(SwingConstants.LEADING);
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed(e);
        }
      });
      BTD.add(OBJ_33);

      //---- OBJ_34 ----
      OBJ_34.setText("Duplication avec changement de tiers");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed(e);
        }
      });
      BTD.add(OBJ_34);

      //---- OBJ_35 ----
      OBJ_35.setText("Duplication avec changement de type de facturation");
      OBJ_35.setName("OBJ_35");
      OBJ_35.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed(e);
        }
      });
      BTD.add(OBJ_35);

      //---- OBJ_36 ----
      OBJ_36.setText("Edition");
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      BTD.add(OBJ_36);

      //---- OBJ_37 ----
      OBJ_37.setText("D\u00e9verrouillage");
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_37ActionPerformed(e);
        }
      });
      BTD.add(OBJ_37);

      //---- OBJ_39 ----
      OBJ_39.setText("Gestion des Locations");
      OBJ_39.setName("OBJ_39");
      OBJ_39.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_39ActionPerformed(e);
        }
      });
      BTD.add(OBJ_39);

      //---- OBJ_40 ----
      OBJ_40.setText("Calcul de la cl\u00e9 d'autorisation");
      OBJ_40.setName("OBJ_40");
      OBJ_40.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_40ActionPerformed(e);
        }
      });
      BTD.add(OBJ_40);

      //---- OBJ_41 ----
      OBJ_41.setText("Modification en t\u00eate apres facturation");
      OBJ_41.setName("OBJ_41");
      OBJ_41.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_41ActionPerformed(e);
        }
      });
      BTD.add(OBJ_41);

      //---- OBJ_42 ----
      OBJ_42.setText("Options clients");
      OBJ_42.setName("OBJ_42");
      OBJ_42.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_42ActionPerformed(e);
        }
      });
      BTD.add(OBJ_42);

      //---- OBJ_43 ----
      OBJ_43.setText("Extension de bon");
      OBJ_43.setName("OBJ_43");
      OBJ_43.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_43ActionPerformed(e);
        }
      });
      BTD.add(OBJ_43);

      //---- OBJ_44 ----
      OBJ_44.setText("Saisie prix consommateur");
      OBJ_44.setName("OBJ_44");
      OBJ_44.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_44ActionPerformed(e);
        }
      });
      BTD.add(OBJ_44);

      //---- OBJ_45 ----
      OBJ_45.setText("Avoir modifi\u00e9");
      OBJ_45.setName("OBJ_45");
      OBJ_45.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_45ActionPerformed(e);
        }
      });
      BTD.add(OBJ_45);

      //---- OBJ_46 ----
      OBJ_46.setText("Rechiffrer");
      OBJ_46.setName("OBJ_46");
      OBJ_46.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_46ActionPerformed(e);
        }
      });
      BTD.add(OBJ_46);

      //---- OBJ_47 ----
      OBJ_47.setText("Affichage affectation adresse de stock");
      OBJ_47.setName("OBJ_47");
      OBJ_47.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_47ActionPerformed(e);
        }
      });
      BTD.add(OBJ_47);

      //---- OBJ_48 ----
      OBJ_48.setText("D\u00e9tail pr\u00e9paration");
      OBJ_48.setName("OBJ_48");
      OBJ_48.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_48ActionPerformed(e);
        }
      });
      BTD.add(OBJ_48);

      //---- OBJ_49 ----
      OBJ_49.setText("Exportation du bon vers excel");
      OBJ_49.setName("OBJ_49");
      OBJ_49.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_49ActionPerformed(e);
        }
      });
      BTD.add(OBJ_49);

      //---- OBJ_50 ----
      OBJ_50.setText("D\u00e9tail emballage");
      OBJ_50.setName("OBJ_50");
      OBJ_50.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_50ActionPerformed(e);
        }
      });
      BTD.add(OBJ_50);

      //---- OBJ_51 ----
      OBJ_51.setText("D\u00e9tail exp\u00e9dition");
      OBJ_51.setName("OBJ_51");
      OBJ_51.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_51ActionPerformed(e);
        }
      });
      BTD.add(OBJ_51);
    }

    //---- riBoutonDetail1 ----
    riBoutonDetail1.setToolTipText("Autres \u00e9tats");
    riBoutonDetail1.setName("riBoutonDetail1");
    riBoutonDetail1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        riBoutonDetail1ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_80;
  private XRiTextField WETB;
  private JLabel OBJ_81;
  private XRiTextField WNUM;
  private XRiTextField WSUF;
  private RiZoneSortie OBJ_58;
  private XRiTextField WTFA;
  private XRiTextField WIN18;
  private XRiTextField WE1NFA;
  private RiZoneSortie OBJ_90;
  private XRiTextField WNUMF;
  private XRiTextField WDTFX;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LD01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel1;
  private XRiComboBox WTNOM;
  private XRiComboBox WSELD;
  private XRiTextField WNOM;
  private JLabel OBJ_115;
  private JLabel OBJ_111;
  private JLabel OBJ_119;
  private XRiTextField WNFAD;
  private XRiTextField WBOND;
  private JLabel OBJ_112;
  private XRiTextField WSAN;
  private XRiTextField WCLI;
  private XRiTextField WACT;
  private XRiTextField WSELZ;
  private JLabel OBJ_98;
  private JLabel OBJ_99;
  private JLabel OBJ_94;
  private JLabel OBJ_95;
  private XRiTextField WVDE;
  private JLabel OBJ_96;
  private XRiTextField WLIV;
  private XRiTextField WREP;
  private XRiTextField WIN7;
  private JLabel OBJ_97;
  private JLabel OBJ_116;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  private JPanel OBJ_133;
  private JButton OBJ_135;
  private JButton OBJ_136;
  private JButton OBJ_137;
  private JButton OBJ_138;
  private JLabel OBJ_131;
  private JLabel OBJ_132;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_33;
  private JMenuItem OBJ_34;
  private JMenuItem OBJ_35;
  private JMenuItem OBJ_36;
  private JMenuItem OBJ_37;
  private JMenuItem OBJ_39;
  private JMenuItem OBJ_40;
  private JMenuItem OBJ_41;
  private JMenuItem OBJ_42;
  private JMenuItem OBJ_43;
  private JMenuItem OBJ_44;
  private JMenuItem OBJ_45;
  private JMenuItem OBJ_46;
  private JMenuItem OBJ_47;
  private JMenuItem OBJ_48;
  private JMenuItem OBJ_49;
  private JMenuItem OBJ_50;
  private JMenuItem OBJ_51;
  private SNBoutonDetail riBoutonDetail1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
