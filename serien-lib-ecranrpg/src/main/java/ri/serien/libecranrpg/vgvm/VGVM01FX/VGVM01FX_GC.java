
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_GC extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM01FX_GC(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel4 = new JXTitledPanel();
    panel1 = new JPanel();
    GCT01 = new XRiTextField();
    GCD01 = new XRiTextField();
    GCC01 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    GCT02 = new XRiTextField();
    GCD02 = new XRiTextField();
    GCC02 = new XRiTextField();
    GCT03 = new XRiTextField();
    GCD03 = new XRiTextField();
    GCC03 = new XRiTextField();
    GCT04 = new XRiTextField();
    GCD04 = new XRiTextField();
    GCC04 = new XRiTextField();
    GCT05 = new XRiTextField();
    GCD05 = new XRiTextField();
    GCC05 = new XRiTextField();
    GCT06 = new XRiTextField();
    GCD06 = new XRiTextField();
    GCC06 = new XRiTextField();
    GCT07 = new XRiTextField();
    GCD07 = new XRiTextField();
    GCC07 = new XRiTextField();
    GCT08 = new XRiTextField();
    GCD08 = new XRiTextField();
    GCC08 = new XRiTextField();
    GCT09 = new XRiTextField();
    GCD09 = new XRiTextField();
    GCC09 = new XRiTextField();
    GCT10 = new XRiTextField();
    GCD10 = new XRiTextField();
    GCC10 = new XRiTextField();
    GCT11 = new XRiTextField();
    GCD11 = new XRiTextField();
    GCC11 = new XRiTextField();
    GCT12 = new XRiTextField();
    GCD12 = new XRiTextField();
    GCC12 = new XRiTextField();
    GCT13 = new XRiTextField();
    GCD13 = new XRiTextField();
    GCC13 = new XRiTextField();
    GCT14 = new XRiTextField();
    GCD14 = new XRiTextField();
    GCC14 = new XRiTextField();
    GCT15 = new XRiTextField();
    GCD15 = new XRiTextField();
    GCC15 = new XRiTextField();
    GCT16 = new XRiTextField();
    GCD16 = new XRiTextField();
    GCC16 = new XRiTextField();
    GCT17 = new XRiTextField();
    GCD17 = new XRiTextField();
    GCC17 = new XRiTextField();
    GCT18 = new XRiTextField();
    GCD18 = new XRiTextField();
    GCC18 = new XRiTextField();
    GCT19 = new XRiTextField();
    GCD19 = new XRiTextField();
    GCC19 = new XRiTextField();
    GCT20 = new XRiTextField();
    GCD20 = new XRiTextField();
    GCC20 = new XRiTextField();
    GCT21 = new XRiTextField();
    GCD21 = new XRiTextField();
    GCC21 = new XRiTextField();
    GCT22 = new XRiTextField();
    GCD22 = new XRiTextField();
    GCC22 = new XRiTextField();
    GCT23 = new XRiTextField();
    GCD23 = new XRiTextField();
    GCC23 = new XRiTextField();
    GCT24 = new XRiTextField();
    GCD24 = new XRiTextField();
    GCC24 = new XRiTextField();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(840, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Comptabilisation des gratuits");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();

            //======== panel1 ========
            {
              panel1.setName("panel1");
              panel1.setLayout(null);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }

            //---- GCT01 ----
            GCT01.setName("GCT01");

            //---- GCD01 ----
            GCD01.setName("GCD01");

            //---- GCC01 ----
            GCC01.setName("GCC01");

            //---- label1 ----
            label1.setText("Type de gratuit");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");

            //---- label2 ----
            label2.setText("Poste d\u00e9bit");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");

            //---- label3 ----
            label3.setText("Poste cr\u00e9dit");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");

            //---- GCT02 ----
            GCT02.setName("GCT02");

            //---- GCD02 ----
            GCD02.setName("GCD02");

            //---- GCC02 ----
            GCC02.setName("GCC02");

            //---- GCT03 ----
            GCT03.setName("GCT03");

            //---- GCD03 ----
            GCD03.setName("GCD03");

            //---- GCC03 ----
            GCC03.setName("GCC03");

            //---- GCT04 ----
            GCT04.setName("GCT04");

            //---- GCD04 ----
            GCD04.setName("GCD04");

            //---- GCC04 ----
            GCC04.setName("GCC04");

            //---- GCT05 ----
            GCT05.setName("GCT05");

            //---- GCD05 ----
            GCD05.setName("GCD05");

            //---- GCC05 ----
            GCC05.setName("GCC05");

            //---- GCT06 ----
            GCT06.setName("GCT06");

            //---- GCD06 ----
            GCD06.setName("GCD06");

            //---- GCC06 ----
            GCC06.setName("GCC06");

            //---- GCT07 ----
            GCT07.setName("GCT07");

            //---- GCD07 ----
            GCD07.setName("GCD07");

            //---- GCC07 ----
            GCC07.setName("GCC07");

            //---- GCT08 ----
            GCT08.setName("GCT08");

            //---- GCD08 ----
            GCD08.setName("GCD08");

            //---- GCC08 ----
            GCC08.setName("GCC08");

            //---- GCT09 ----
            GCT09.setName("GCT09");

            //---- GCD09 ----
            GCD09.setName("GCD09");

            //---- GCC09 ----
            GCC09.setName("GCC09");

            //---- GCT10 ----
            GCT10.setName("GCT10");

            //---- GCD10 ----
            GCD10.setName("GCD10");

            //---- GCC10 ----
            GCC10.setName("GCC10");

            //---- GCT11 ----
            GCT11.setName("GCT11");

            //---- GCD11 ----
            GCD11.setName("GCD11");

            //---- GCC11 ----
            GCC11.setName("GCC11");

            //---- GCT12 ----
            GCT12.setName("GCT12");

            //---- GCD12 ----
            GCD12.setName("GCD12");

            //---- GCC12 ----
            GCC12.setName("GCC12");

            //---- GCT13 ----
            GCT13.setName("GCT13");

            //---- GCD13 ----
            GCD13.setName("GCD13");

            //---- GCC13 ----
            GCC13.setName("GCC13");

            //---- GCT14 ----
            GCT14.setName("GCT14");

            //---- GCD14 ----
            GCD14.setName("GCD14");

            //---- GCC14 ----
            GCC14.setName("GCC14");

            //---- GCT15 ----
            GCT15.setName("GCT15");

            //---- GCD15 ----
            GCD15.setName("GCD15");

            //---- GCC15 ----
            GCC15.setName("GCC15");

            //---- GCT16 ----
            GCT16.setName("GCT16");

            //---- GCD16 ----
            GCD16.setName("GCD16");

            //---- GCC16 ----
            GCC16.setName("GCC16");

            //---- GCT17 ----
            GCT17.setName("GCT17");

            //---- GCD17 ----
            GCD17.setName("GCD17");

            //---- GCC17 ----
            GCC17.setName("GCC17");

            //---- GCT18 ----
            GCT18.setName("GCT18");

            //---- GCD18 ----
            GCD18.setName("GCD18");

            //---- GCC18 ----
            GCC18.setName("GCC18");

            //---- GCT19 ----
            GCT19.setName("GCT19");

            //---- GCD19 ----
            GCD19.setName("GCD19");

            //---- GCC19 ----
            GCC19.setName("GCC19");

            //---- GCT20 ----
            GCT20.setName("GCT20");

            //---- GCD20 ----
            GCD20.setName("GCD20");

            //---- GCC20 ----
            GCC20.setName("GCC20");

            //---- GCT21 ----
            GCT21.setName("GCT21");

            //---- GCD21 ----
            GCD21.setName("GCD21");

            //---- GCC21 ----
            GCC21.setName("GCC21");

            //---- GCT22 ----
            GCT22.setName("GCT22");

            //---- GCD22 ----
            GCD22.setName("GCD22");

            //---- GCC22 ----
            GCC22.setName("GCC22");

            //---- GCT23 ----
            GCT23.setName("GCT23");

            //---- GCD23 ----
            GCD23.setName("GCD23");

            //---- GCC23 ----
            GCC23.setName("GCC23");

            //---- GCT24 ----
            GCT24.setName("GCT24");

            //---- GCD24 ----
            GCD24.setName("GCD24");

            //---- GCC24 ----
            GCC24.setName("GCC24");

            //---- label4 ----
            label4.setText("Type de gratuit");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");

            //---- label5 ----
            label5.setText("Poste d\u00e9bit");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setName("label5");

            //---- label6 ----
            label6.setText("Poste cr\u00e9dit");
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setName("label6");

            GroupLayout xTitledPanel4ContentContainerLayout = new GroupLayout(xTitledPanel4ContentContainer);
            xTitledPanel4ContentContainer.setLayout(xTitledPanel4ContentContainerLayout);
            xTitledPanel4ContentContainerLayout.setHorizontalGroup(
              xTitledPanel4ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(label1)
                  .addGap(25, 25, 25)
                  .addComponent(label2)
                  .addGap(27, 27, 27)
                  .addComponent(label3)
                  .addGap(169, 169, 169)
                  .addComponent(label4)
                  .addGap(26, 26, 26)
                  .addComponent(label5)
                  .addGap(29, 29, 29)
                  .addComponent(label6))
                .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                  .addGap(50, 50, 50)
                  .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                    .addComponent(GCT03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT08, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT09, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(70, 70, 70)
                  .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                    .addComponent(GCD01, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD04, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD02, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD03, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD06, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD09, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD07, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD08, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD12, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD11, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD05, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(52, 52, 52)
                  .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                    .addComponent(GCC03, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC02, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC07, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC12, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC05, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC09, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC11, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC06, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC08, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC01, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC04, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(218, 218, 218)
                  .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                    .addComponent(GCT18, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT21, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT17, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT14, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT16, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT19, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT24, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT23, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCT22, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(70, 70, 70)
                  .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                    .addComponent(GCD14, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD13, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD16, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD17, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD18, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD15, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD21, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD20, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD23, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD24, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD22, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCD19, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGap(55, 55, 55)
                  .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                    .addComponent(GCC14, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC13, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC15, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC20, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC17, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC21, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC19, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC24, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC23, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC18, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC22, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addComponent(GCC16, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
            );
            xTitledPanel4ContentContainerLayout.setVerticalGroup(
              xTitledPanel4ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                    .addComponent(label1)
                    .addComponent(label2)
                    .addComponent(label3)
                    .addComponent(label4)
                    .addComponent(label5)
                    .addComponent(label6))
                  .addGap(14, 14, 14)
                  .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(GCT03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(GCT01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(GCT02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(GCT04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(GCT05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(GCT10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(GCT08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(GCT11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(GCT12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(GCT07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(GCT09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGap(125, 125, 125)
                      .addComponent(GCT06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                        .addComponent(GCD01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(GCD04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(GCD02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(GCD03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                        .addComponent(GCD06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(GCD09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(GCD07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(GCD08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(GCD12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(GCD10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(GCD11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(GCD05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(GCC03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(GCC02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(GCC07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(GCC12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(GCC05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(GCC09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(GCC11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(GCC10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(GCC06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(GCC08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addComponent(GCC01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(GCC04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(GCT18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(GCT21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(GCT17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(GCT14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(GCT20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(GCT16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(GCT19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(GCT13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(GCT15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(GCT24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(GCT23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGap(225, 225, 225)
                      .addComponent(GCT22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(GCD14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(GCD13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(GCD16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(GCD17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(GCD18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(GCD15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(GCD21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(GCD20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(GCD23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(GCD24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(GCD22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGap(150, 150, 150)
                      .addComponent(GCD19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(GCC14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(GCC13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(GCC15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(xTitledPanel4ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(GCC20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(GCC17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(GCC21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(GCC19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(GCC24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(GCC23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(GCC18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(GCC22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(xTitledPanel4ContentContainerLayout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(GCC16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(xTitledPanel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(xTitledPanel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel4;
  private JPanel panel1;
  private XRiTextField GCT01;
  private XRiTextField GCD01;
  private XRiTextField GCC01;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private XRiTextField GCT02;
  private XRiTextField GCD02;
  private XRiTextField GCC02;
  private XRiTextField GCT03;
  private XRiTextField GCD03;
  private XRiTextField GCC03;
  private XRiTextField GCT04;
  private XRiTextField GCD04;
  private XRiTextField GCC04;
  private XRiTextField GCT05;
  private XRiTextField GCD05;
  private XRiTextField GCC05;
  private XRiTextField GCT06;
  private XRiTextField GCD06;
  private XRiTextField GCC06;
  private XRiTextField GCT07;
  private XRiTextField GCD07;
  private XRiTextField GCC07;
  private XRiTextField GCT08;
  private XRiTextField GCD08;
  private XRiTextField GCC08;
  private XRiTextField GCT09;
  private XRiTextField GCD09;
  private XRiTextField GCC09;
  private XRiTextField GCT10;
  private XRiTextField GCD10;
  private XRiTextField GCC10;
  private XRiTextField GCT11;
  private XRiTextField GCD11;
  private XRiTextField GCC11;
  private XRiTextField GCT12;
  private XRiTextField GCD12;
  private XRiTextField GCC12;
  private XRiTextField GCT13;
  private XRiTextField GCD13;
  private XRiTextField GCC13;
  private XRiTextField GCT14;
  private XRiTextField GCD14;
  private XRiTextField GCC14;
  private XRiTextField GCT15;
  private XRiTextField GCD15;
  private XRiTextField GCC15;
  private XRiTextField GCT16;
  private XRiTextField GCD16;
  private XRiTextField GCC16;
  private XRiTextField GCT17;
  private XRiTextField GCD17;
  private XRiTextField GCC17;
  private XRiTextField GCT18;
  private XRiTextField GCD18;
  private XRiTextField GCC18;
  private XRiTextField GCT19;
  private XRiTextField GCD19;
  private XRiTextField GCC19;
  private XRiTextField GCT20;
  private XRiTextField GCD20;
  private XRiTextField GCC20;
  private XRiTextField GCT21;
  private XRiTextField GCD21;
  private XRiTextField GCC21;
  private XRiTextField GCT22;
  private XRiTextField GCD22;
  private XRiTextField GCC22;
  private XRiTextField GCT23;
  private XRiTextField GCD23;
  private XRiTextField GCC23;
  private XRiTextField GCT24;
  private XRiTextField GCD24;
  private XRiTextField GCC24;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
