
package ri.serien.libecranrpg.vgvm.VGVM10FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM10FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] WTRT_Value = { "", "S", "R", "F", "D", };
  // private String[] _UTIT1_Title={"UTIT1", "TITQ1", "TITQ2", "TITQ3", };
  // private String[][] _UTIT1_Data={{"L301", "WQ101", "WQ201", "WQ301", }, };
  // private int[] _UTIT1_Width={470, 66, 66, 64, };
  private String[] listeCellules =
      { "L301", "L302", "L303", "L304", "L305", "L306", "L307", "L308", "L309", "L310", "L311", "L312", "L313", "L314", "L315" };
  // private int[] _LIST_Justification={SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT,
  // SwingConstants.RIGHT};
  private Color[][] couleurLigne = new Color[listeCellules.length][1];
  
  public VGVM10FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTRT.setValeurs(WTRT_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    E1NUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NUM@")).trim());
    CLCLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCLI@")).trim());
    E1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ETB@")).trim());
    CLLIV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLLIV@")).trim());
    E1SUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1SUF@")).trim());
    OBJ_65.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIBR@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQ2T1@")).trim());
    OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQ2T2@")).trim());
    OBJ_64.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1MAG@")).trim());
    L301.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L301@")).trim());
    L302.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L302@")).trim());
    L303.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L303@")).trim());
    L304.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L304@")).trim());
    L305.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L305@")).trim());
    L306.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L306@")).trim());
    L307.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L307@")).trim());
    L308.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L308@")).trim());
    L309.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L309@")).trim());
    L310.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L310@")).trim());
    L311.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L311@")).trim());
    L312.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L312@")).trim());
    L313.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L313@")).trim());
    L314.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L314@")).trim());
    L315.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L315@")).trim());
    UTIT1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UTIT1@")).trim());
    TITQ1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITQ1@")).trim());
    TITQ2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITQ2@")).trim());
    TITQ3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITQ3@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESD@")).trim());
    OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESG@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    gererCouleurListe();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), null,_LIST_Justification,couleurLigne,null,null);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    
    // TODO Init
    // spécifiques...----------------------------------------------------------------------------------------------------
    
    E1SUF.setVisible(lexique.isPresent("E1SUF"));
    WIN3D.setEnabled(lexique.isPresent("WIN3D"));
    CLLIV.setVisible(lexique.isPresent("CLLIV"));
    OBJ_64.setVisible(lexique.isPresent("E1MAG"));
    CLCLI.setVisible(lexique.isPresent("CLCLI"));
    E1NUM.setVisible(lexique.isPresent("E1NUM"));
    OBJ_63.setVisible(lexique.isPresent("WQ2T2"));
    OBJ_62.setVisible(lexique.isPresent("WQ2T1"));
    WARTD.setEnabled(lexique.isPresent("WARTD"));
    OBJ_65.setVisible(lexique.isPresent("MALIBR"));
    OBJ_59.setVisible(lexique.isPresent("MESG"));
    OBJ_60.setVisible(lexique.isPresent("MESD"));
    // WTRT.setVisible( lexique.isPresent("WTRT"));
    relique.setVisible(lexique.isTrue("54"));
    
    Font police_liste = new Font("Monospaced", Font.PLAIN, 11);
    
    // L301.getTableHeader().setFont(police_liste);
    L301.setFont(police_liste);
    L302.setFont(police_liste);
    L303.setFont(police_liste);
    L304.setFont(police_liste);
    L305.setFont(police_liste);
    L306.setFont(police_liste);
    L307.setFont(police_liste);
    L308.setFont(police_liste);
    L309.setFont(police_liste);
    L310.setFont(police_liste);
    L311.setFont(police_liste);
    L312.setFont(police_liste);
    L313.setFont(police_liste);
    L314.setFont(police_liste);
    L315.setFont(police_liste);
    
    // sécuriser les zones de saisie
    WQ301.setEditable(!L301.getText().trim().equals(""));
    WQ302.setEditable(!L302.getText().trim().equals(""));
    WQ303.setEditable(!L303.getText().trim().equals(""));
    WQ304.setEditable(!L304.getText().trim().equals(""));
    WQ305.setEditable(!L305.getText().trim().equals(""));
    WQ306.setEditable(!L306.getText().trim().equals(""));
    WQ307.setEditable(!L307.getText().trim().equals(""));
    WQ308.setEditable(!L308.getText().trim().equals(""));
    WQ309.setEditable(!L309.getText().trim().equals(""));
    WQ310.setEditable(!L310.getText().trim().equals(""));
    WQ311.setEditable(!L311.getText().trim().equals(""));
    WQ312.setEditable(!L312.getText().trim().equals(""));
    WQ313.setEditable(!L313.getText().trim().equals(""));
    WQ314.setEditable(!L314.getText().trim().equals(""));
    WQ315.setEditable(!L315.getText().trim().equals(""));
    
    p_bpresentation.setCodeEtablissement(E1ETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(E1ETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("WTRT", 0, WTRT_Value[WTRT.getSelectedIndex()]);
  }
  
  // permet de gérer la couleur des lignes de la liste en fonction du type de ligne (article, commentaire , edition,
  // annulé...)
  private void gererCouleurListe() {
    // couleurLigne = new Color[listeCellules.length][1];
    String chaineTest = null;
    for (int i = 0; i < listeCellules.length; i++) {
      chaineTest = lexique.HostFieldGetData(listeCellules[i]).trim();
      if ((chaineTest != null) && (chaineTest.startsWith("*Commentaires*"))) {
        couleurLigne[i][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
      }
      else {
        couleurLigne[i][0] = Color.BLACK;
      }
    }
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
    // lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(popupMenu1.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new RiZoneSortie();
    E1NUM = new RiZoneSortie();
    CLCLI = new RiZoneSortie();
    OBJ_36 = new JLabel();
    OBJ_37 = new JLabel();
    E1ETB = new RiZoneSortie();
    CLLIV = new RiZoneSortie();
    OBJ_34 = new JLabel();
    E1SUF = new RiZoneSortie();
    p_tete_droite = new JPanel();
    relique = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WARTD = new XRiTextField();
    WTRT = new XRiComboBox();
    OBJ_53 = new JLabel();
    WIN3D = new XRiTextField();
    panel2 = new JPanel();
    OBJ_65 = new RiZoneSortie();
    OBJ_62 = new RiZoneSortie();
    OBJ_63 = new RiZoneSortie();
    OBJ_61 = new JLabel();
    OBJ_64 = new RiZoneSortie();
    label1 = new JLabel();
    L301 = new RiZoneSortie();
    L302 = new RiZoneSortie();
    L303 = new RiZoneSortie();
    L304 = new RiZoneSortie();
    L305 = new RiZoneSortie();
    L306 = new RiZoneSortie();
    L307 = new RiZoneSortie();
    L308 = new RiZoneSortie();
    L309 = new RiZoneSortie();
    L310 = new RiZoneSortie();
    L311 = new RiZoneSortie();
    L312 = new RiZoneSortie();
    L313 = new RiZoneSortie();
    L314 = new RiZoneSortie();
    L315 = new RiZoneSortie();
    WQ101 = new XRiTextField();
    WQ201 = new XRiTextField();
    WQ301 = new XRiTextField();
    WQ102 = new XRiTextField();
    WQ103 = new XRiTextField();
    WQ104 = new XRiTextField();
    WQ105 = new XRiTextField();
    WQ106 = new XRiTextField();
    WQ107 = new XRiTextField();
    WQ108 = new XRiTextField();
    WQ109 = new XRiTextField();
    WQ110 = new XRiTextField();
    WQ111 = new XRiTextField();
    WQ112 = new XRiTextField();
    WQ113 = new XRiTextField();
    WQ114 = new XRiTextField();
    WQ115 = new XRiTextField();
    WQ202 = new XRiTextField();
    WQ203 = new XRiTextField();
    WQ204 = new XRiTextField();
    WQ205 = new XRiTextField();
    WQ206 = new XRiTextField();
    WQ207 = new XRiTextField();
    WQ208 = new XRiTextField();
    WQ209 = new XRiTextField();
    WQ210 = new XRiTextField();
    WQ211 = new XRiTextField();
    WQ212 = new XRiTextField();
    WQ213 = new XRiTextField();
    WQ214 = new XRiTextField();
    WQ215 = new XRiTextField();
    WQ302 = new XRiTextField();
    WQ303 = new XRiTextField();
    WQ304 = new XRiTextField();
    WQ305 = new XRiTextField();
    WQ306 = new XRiTextField();
    WQ307 = new XRiTextField();
    WQ308 = new XRiTextField();
    WQ309 = new XRiTextField();
    WQ310 = new XRiTextField();
    WQ311 = new XRiTextField();
    WQ312 = new XRiTextField();
    WQ313 = new XRiTextField();
    WQ314 = new XRiTextField();
    WQ315 = new XRiTextField();
    UTIT1 = new RiZoneSortie();
    TITQ1 = new RiZoneSortie();
    TITQ2 = new RiZoneSortie();
    TITQ3 = new RiZoneSortie();
    panel3 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_60 = new JLabel();
    OBJ_59 = new JLabel();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    popupMenu1 = new JPopupMenu();
    menuItem1 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1010, 710));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Bon de commande - extraction");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setMaximumSize(new Dimension(690, 28));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_44 ----
          OBJ_44.setText("@CLNOM@");
          OBJ_44.setOpaque(false);
          OBJ_44.setName("OBJ_44");

          //---- E1NUM ----
          E1NUM.setOpaque(false);
          E1NUM.setText("@E1NUM@");
          E1NUM.setName("E1NUM");

          //---- CLCLI ----
          CLCLI.setOpaque(false);
          CLCLI.setText("@CLCLI@");
          CLCLI.setHorizontalAlignment(SwingConstants.RIGHT);
          CLCLI.setName("CLCLI");

          //---- OBJ_36 ----
          OBJ_36.setText("Num\u00e9ro");
          OBJ_36.setName("OBJ_36");

          //---- OBJ_37 ----
          OBJ_37.setText("Client");
          OBJ_37.setName("OBJ_37");

          //---- E1ETB ----
          E1ETB.setOpaque(false);
          E1ETB.setText("@E1ETB@");
          E1ETB.setName("E1ETB");

          //---- CLLIV ----
          CLLIV.setOpaque(false);
          CLLIV.setText("@CLLIV@");
          CLLIV.setName("CLLIV");

          //---- OBJ_34 ----
          OBJ_34.setText("Etablissement");
          OBJ_34.setName("OBJ_34");

          //---- E1SUF ----
          E1SUF.setOpaque(false);
          E1SUF.setText("@E1SUF@");
          E1SUF.setName("E1SUF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(58, 58, 58)
                    .addComponent(E1NUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addComponent(E1SUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(CLCLI, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(CLLIV, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(E1NUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(E1SUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(CLCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(CLLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- relique ----
          relique.setText("Pas de reliquat autoris\u00e9");
          relique.setFont(relique.getFont().deriveFont(relique.getFont().getStyle() | Font.BOLD));
          relique.setForeground(new Color(204, 0, 0));
          relique.setPreferredSize(new Dimension(180, 16));
          relique.setMinimumSize(new Dimension(180, 16));
          relique.setMaximumSize(new Dimension(180, 16));
          relique.setName("relique");
          p_tete_droite.add(relique);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 120));
            menus_haut.setPreferredSize(new Dimension(160, 120));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("ON / OFF palette, volume");
              riSousMenu_bt6.setToolTipText("ON / OFF palette, volume");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(810, 620));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(810, 610));
          p_contenu.setMaximumSize(new Dimension(810, 610));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Recherche, traitement group\u00e9"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WARTD ----
            WARTD.setToolTipText("D\u00e9but code article pour recherche ou traitement group\u00e9");
            WARTD.setComponentPopupMenu(BTD);
            WARTD.setName("WARTD");
            panel1.add(WARTD);
            WARTD.setBounds(20, 35, 210, WARTD.getPreferredSize().height);

            //---- WTRT ----
            WTRT.setModel(new DefaultComboBoxModel(new String[] {
              "recherche article",
              "solde de commande",
              "mise en reliquat",
              "exp\u00e9dition forc\u00e9e",
              "exp\u00e9dition disponible"
            }));
            WTRT.setComponentPopupMenu(BTD);
            WTRT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTRT.setName("WTRT");
            panel1.add(WTRT);
            WTRT.setBounds(250, 36, 165, WTRT.getPreferredSize().height);

            //---- OBJ_53 ----
            OBJ_53.setText("Regroupement");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(450, 39, 93, 20);

            //---- WIN3D ----
            WIN3D.setToolTipText("<HTML>Zone regroupement de ligne<BR>T=tableau  V=vecteur</HTML>");
            WIN3D.setComponentPopupMenu(BTD);
            WIN3D.setName("WIN3D");
            panel1.add(WIN3D);
            WIN3D.setBounds(545, 35, 24, WIN3D.getPreferredSize().height);
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Extraction"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_65 ----
            OBJ_65.setText("@MALIBR@");
            OBJ_65.setName("OBJ_65");
            panel2.add(OBJ_65);
            OBJ_65.setBounds(519, 35, 200, OBJ_65.getPreferredSize().height);

            //---- OBJ_62 ----
            OBJ_62.setText("@WQ2T1@");
            OBJ_62.setName("OBJ_62");
            panel2.add(OBJ_62);
            OBJ_62.setBounds(130, 35, 98, OBJ_62.getPreferredSize().height);

            //---- OBJ_63 ----
            OBJ_63.setText("@WQ2T2@");
            OBJ_63.setName("OBJ_63");
            panel2.add(OBJ_63);
            OBJ_63.setBounds(235, 35, 98, OBJ_63.getPreferredSize().height);

            //---- OBJ_61 ----
            OBJ_61.setText("Palettes/Volume");
            OBJ_61.setName("OBJ_61");
            panel2.add(OBJ_61);
            OBJ_61.setBounds(22, 37, 108, 20);

            //---- OBJ_64 ----
            OBJ_64.setText("@E1MAG@");
            OBJ_64.setName("OBJ_64");
            panel2.add(OBJ_64);
            OBJ_64.setBounds(480, 35, 34, OBJ_64.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Magasin");
            label1.setName("label1");
            panel2.add(label1);
            label1.setBounds(420, 37, 60, 20);

            //---- L301 ----
            L301.setText("@L301@");
            L301.setFont(L301.getFont().deriveFont(L301.getFont().getSize() - 1f));
            L301.setName("L301");
            panel2.add(L301);
            L301.setBounds(20, 95, 470, 22);

            //---- L302 ----
            L302.setText("@L302@");
            L302.setFont(L302.getFont().deriveFont(L302.getFont().getSize() - 1f));
            L302.setName("L302");
            panel2.add(L302);
            L302.setBounds(20, 120, 470, 22);

            //---- L303 ----
            L303.setText("@L303@");
            L303.setFont(L303.getFont().deriveFont(L303.getFont().getSize() - 1f));
            L303.setName("L303");
            panel2.add(L303);
            L303.setBounds(20, 145, 470, 22);

            //---- L304 ----
            L304.setText("@L304@");
            L304.setFont(L304.getFont().deriveFont(L304.getFont().getSize() - 1f));
            L304.setName("L304");
            panel2.add(L304);
            L304.setBounds(20, 170, 470, 22);

            //---- L305 ----
            L305.setText("@L305@");
            L305.setFont(L305.getFont().deriveFont(L305.getFont().getSize() - 1f));
            L305.setName("L305");
            panel2.add(L305);
            L305.setBounds(20, 195, 470, 22);

            //---- L306 ----
            L306.setText("@L306@");
            L306.setFont(L306.getFont().deriveFont(L306.getFont().getSize() - 1f));
            L306.setName("L306");
            panel2.add(L306);
            L306.setBounds(20, 220, 470, 22);

            //---- L307 ----
            L307.setText("@L307@");
            L307.setFont(L307.getFont().deriveFont(L307.getFont().getSize() - 1f));
            L307.setName("L307");
            panel2.add(L307);
            L307.setBounds(20, 245, 470, 22);

            //---- L308 ----
            L308.setText("@L308@");
            L308.setFont(L308.getFont().deriveFont(L308.getFont().getSize() - 1f));
            L308.setName("L308");
            panel2.add(L308);
            L308.setBounds(20, 270, 470, 22);

            //---- L309 ----
            L309.setText("@L309@");
            L309.setFont(L309.getFont().deriveFont(L309.getFont().getSize() - 1f));
            L309.setName("L309");
            panel2.add(L309);
            L309.setBounds(20, 295, 470, 22);

            //---- L310 ----
            L310.setText("@L310@");
            L310.setFont(L310.getFont().deriveFont(L310.getFont().getSize() - 1f));
            L310.setName("L310");
            panel2.add(L310);
            L310.setBounds(20, 320, 470, 22);

            //---- L311 ----
            L311.setText("@L311@");
            L311.setFont(L311.getFont().deriveFont(L311.getFont().getSize() - 1f));
            L311.setName("L311");
            panel2.add(L311);
            L311.setBounds(20, 345, 470, 22);

            //---- L312 ----
            L312.setText("@L312@");
            L312.setFont(L312.getFont().deriveFont(L312.getFont().getSize() - 1f));
            L312.setName("L312");
            panel2.add(L312);
            L312.setBounds(20, 370, 470, 22);

            //---- L313 ----
            L313.setText("@L313@");
            L313.setFont(L313.getFont().deriveFont(L313.getFont().getSize() - 1f));
            L313.setName("L313");
            panel2.add(L313);
            L313.setBounds(20, 395, 470, 22);

            //---- L314 ----
            L314.setText("@L314@");
            L314.setFont(L314.getFont().deriveFont(L314.getFont().getSize() - 1f));
            L314.setName("L314");
            panel2.add(L314);
            L314.setBounds(20, 420, 470, 22);

            //---- L315 ----
            L315.setText("@L315@");
            L315.setFont(L315.getFont().deriveFont(L315.getFont().getSize() - 1f));
            L315.setName("L315");
            panel2.add(L315);
            L315.setBounds(20, 445, 470, 22);

            //---- WQ101 ----
            WQ101.setFont(WQ101.getFont().deriveFont(WQ101.getFont().getSize() - 1f));
            WQ101.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ101.setName("WQ101");
            panel2.add(WQ101);
            WQ101.setBounds(495, 94, 75, 25);

            //---- WQ201 ----
            WQ201.setFont(WQ201.getFont().deriveFont(WQ201.getFont().getSize() - 1f));
            WQ201.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ201.setName("WQ201");
            panel2.add(WQ201);
            WQ201.setBounds(570, 94, 75, 25);

            //---- WQ301 ----
            WQ301.setFont(WQ301.getFont().deriveFont(WQ301.getFont().getSize() - 1f));
            WQ301.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ301.setComponentPopupMenu(popupMenu1);
            WQ301.setNextFocusableComponent(WQ302);
            WQ301.setName("WQ301");
            panel2.add(WQ301);
            WQ301.setBounds(645, 94, 75, 25);

            //---- WQ102 ----
            WQ102.setFont(WQ102.getFont().deriveFont(WQ102.getFont().getSize() - 1f));
            WQ102.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ102.setName("WQ102");
            panel2.add(WQ102);
            WQ102.setBounds(495, 119, 75, 25);

            //---- WQ103 ----
            WQ103.setFont(WQ103.getFont().deriveFont(WQ103.getFont().getSize() - 1f));
            WQ103.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ103.setName("WQ103");
            panel2.add(WQ103);
            WQ103.setBounds(495, 144, 75, 25);

            //---- WQ104 ----
            WQ104.setFont(WQ104.getFont().deriveFont(WQ104.getFont().getSize() - 1f));
            WQ104.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ104.setName("WQ104");
            panel2.add(WQ104);
            WQ104.setBounds(495, 169, 75, 25);

            //---- WQ105 ----
            WQ105.setFont(WQ105.getFont().deriveFont(WQ105.getFont().getSize() - 1f));
            WQ105.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ105.setName("WQ105");
            panel2.add(WQ105);
            WQ105.setBounds(495, 194, 75, 25);

            //---- WQ106 ----
            WQ106.setFont(WQ106.getFont().deriveFont(WQ106.getFont().getSize() - 1f));
            WQ106.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ106.setName("WQ106");
            panel2.add(WQ106);
            WQ106.setBounds(495, 219, 75, 25);

            //---- WQ107 ----
            WQ107.setFont(WQ107.getFont().deriveFont(WQ107.getFont().getSize() - 1f));
            WQ107.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ107.setName("WQ107");
            panel2.add(WQ107);
            WQ107.setBounds(495, 244, 75, 25);

            //---- WQ108 ----
            WQ108.setFont(WQ108.getFont().deriveFont(WQ108.getFont().getSize() - 1f));
            WQ108.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ108.setName("WQ108");
            panel2.add(WQ108);
            WQ108.setBounds(495, 269, 75, 25);

            //---- WQ109 ----
            WQ109.setFont(WQ109.getFont().deriveFont(WQ109.getFont().getSize() - 1f));
            WQ109.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ109.setName("WQ109");
            panel2.add(WQ109);
            WQ109.setBounds(495, 294, 75, 25);

            //---- WQ110 ----
            WQ110.setFont(WQ110.getFont().deriveFont(WQ110.getFont().getSize() - 1f));
            WQ110.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ110.setName("WQ110");
            panel2.add(WQ110);
            WQ110.setBounds(495, 319, 75, 25);

            //---- WQ111 ----
            WQ111.setFont(WQ111.getFont().deriveFont(WQ111.getFont().getSize() - 1f));
            WQ111.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ111.setName("WQ111");
            panel2.add(WQ111);
            WQ111.setBounds(495, 344, 75, 25);

            //---- WQ112 ----
            WQ112.setFont(WQ112.getFont().deriveFont(WQ112.getFont().getSize() - 1f));
            WQ112.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ112.setName("WQ112");
            panel2.add(WQ112);
            WQ112.setBounds(495, 369, 75, 25);

            //---- WQ113 ----
            WQ113.setFont(WQ113.getFont().deriveFont(WQ113.getFont().getSize() - 1f));
            WQ113.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ113.setName("WQ113");
            panel2.add(WQ113);
            WQ113.setBounds(495, 394, 75, 25);

            //---- WQ114 ----
            WQ114.setFont(WQ114.getFont().deriveFont(WQ114.getFont().getSize() - 1f));
            WQ114.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ114.setName("WQ114");
            panel2.add(WQ114);
            WQ114.setBounds(495, 419, 75, 25);

            //---- WQ115 ----
            WQ115.setFont(WQ115.getFont().deriveFont(WQ115.getFont().getSize() - 1f));
            WQ115.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ115.setName("WQ115");
            panel2.add(WQ115);
            WQ115.setBounds(495, 444, 75, 25);

            //---- WQ202 ----
            WQ202.setFont(WQ202.getFont().deriveFont(WQ202.getFont().getSize() - 1f));
            WQ202.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ202.setName("WQ202");
            panel2.add(WQ202);
            WQ202.setBounds(570, 119, 75, 25);

            //---- WQ203 ----
            WQ203.setFont(WQ203.getFont().deriveFont(WQ203.getFont().getSize() - 1f));
            WQ203.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ203.setName("WQ203");
            panel2.add(WQ203);
            WQ203.setBounds(570, 144, 75, 25);

            //---- WQ204 ----
            WQ204.setFont(WQ204.getFont().deriveFont(WQ204.getFont().getSize() - 1f));
            WQ204.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ204.setName("WQ204");
            panel2.add(WQ204);
            WQ204.setBounds(570, 169, 75, 25);

            //---- WQ205 ----
            WQ205.setFont(WQ205.getFont().deriveFont(WQ205.getFont().getSize() - 1f));
            WQ205.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ205.setName("WQ205");
            panel2.add(WQ205);
            WQ205.setBounds(570, 194, 75, 25);

            //---- WQ206 ----
            WQ206.setFont(WQ206.getFont().deriveFont(WQ206.getFont().getSize() - 1f));
            WQ206.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ206.setName("WQ206");
            panel2.add(WQ206);
            WQ206.setBounds(570, 219, 75, 25);

            //---- WQ207 ----
            WQ207.setFont(WQ207.getFont().deriveFont(WQ207.getFont().getSize() - 1f));
            WQ207.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ207.setName("WQ207");
            panel2.add(WQ207);
            WQ207.setBounds(570, 244, 75, 25);

            //---- WQ208 ----
            WQ208.setFont(WQ208.getFont().deriveFont(WQ208.getFont().getSize() - 1f));
            WQ208.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ208.setName("WQ208");
            panel2.add(WQ208);
            WQ208.setBounds(570, 269, 75, 25);

            //---- WQ209 ----
            WQ209.setFont(WQ209.getFont().deriveFont(WQ209.getFont().getSize() - 1f));
            WQ209.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ209.setName("WQ209");
            panel2.add(WQ209);
            WQ209.setBounds(570, 294, 75, 25);

            //---- WQ210 ----
            WQ210.setFont(WQ210.getFont().deriveFont(WQ210.getFont().getSize() - 1f));
            WQ210.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ210.setName("WQ210");
            panel2.add(WQ210);
            WQ210.setBounds(570, 319, 75, 25);

            //---- WQ211 ----
            WQ211.setFont(WQ211.getFont().deriveFont(WQ211.getFont().getSize() - 1f));
            WQ211.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ211.setName("WQ211");
            panel2.add(WQ211);
            WQ211.setBounds(570, 344, 75, 25);

            //---- WQ212 ----
            WQ212.setFont(WQ212.getFont().deriveFont(WQ212.getFont().getSize() - 1f));
            WQ212.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ212.setName("WQ212");
            panel2.add(WQ212);
            WQ212.setBounds(570, 369, 75, 25);

            //---- WQ213 ----
            WQ213.setFont(WQ213.getFont().deriveFont(WQ213.getFont().getSize() - 1f));
            WQ213.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ213.setName("WQ213");
            panel2.add(WQ213);
            WQ213.setBounds(570, 394, 75, 25);

            //---- WQ214 ----
            WQ214.setFont(WQ214.getFont().deriveFont(WQ214.getFont().getSize() - 1f));
            WQ214.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ214.setName("WQ214");
            panel2.add(WQ214);
            WQ214.setBounds(570, 419, 75, 25);

            //---- WQ215 ----
            WQ215.setFont(WQ215.getFont().deriveFont(WQ215.getFont().getSize() - 1f));
            WQ215.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ215.setName("WQ215");
            panel2.add(WQ215);
            WQ215.setBounds(570, 444, 75, 25);

            //---- WQ302 ----
            WQ302.setFont(WQ302.getFont().deriveFont(WQ302.getFont().getSize() - 1f));
            WQ302.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ302.setComponentPopupMenu(popupMenu1);
            WQ302.setNextFocusableComponent(WQ303);
            WQ302.setName("WQ302");
            panel2.add(WQ302);
            WQ302.setBounds(645, 119, 75, 25);

            //---- WQ303 ----
            WQ303.setFont(WQ303.getFont().deriveFont(WQ303.getFont().getSize() - 1f));
            WQ303.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ303.setComponentPopupMenu(popupMenu1);
            WQ303.setNextFocusableComponent(WQ304);
            WQ303.setName("WQ303");
            panel2.add(WQ303);
            WQ303.setBounds(645, 144, 75, 25);

            //---- WQ304 ----
            WQ304.setFont(WQ304.getFont().deriveFont(WQ304.getFont().getSize() - 1f));
            WQ304.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ304.setComponentPopupMenu(popupMenu1);
            WQ304.setName("WQ304");
            panel2.add(WQ304);
            WQ304.setBounds(645, 169, 75, 25);

            //---- WQ305 ----
            WQ305.setFont(WQ305.getFont().deriveFont(WQ305.getFont().getSize() - 1f));
            WQ305.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ305.setComponentPopupMenu(popupMenu1);
            WQ305.setName("WQ305");
            panel2.add(WQ305);
            WQ305.setBounds(645, 194, 75, 25);

            //---- WQ306 ----
            WQ306.setFont(WQ306.getFont().deriveFont(WQ306.getFont().getSize() - 1f));
            WQ306.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ306.setComponentPopupMenu(popupMenu1);
            WQ306.setName("WQ306");
            panel2.add(WQ306);
            WQ306.setBounds(645, 219, 75, 25);

            //---- WQ307 ----
            WQ307.setFont(WQ307.getFont().deriveFont(WQ307.getFont().getSize() - 1f));
            WQ307.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ307.setComponentPopupMenu(popupMenu1);
            WQ307.setName("WQ307");
            panel2.add(WQ307);
            WQ307.setBounds(645, 244, 75, 25);

            //---- WQ308 ----
            WQ308.setFont(WQ308.getFont().deriveFont(WQ308.getFont().getSize() - 1f));
            WQ308.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ308.setComponentPopupMenu(popupMenu1);
            WQ308.setName("WQ308");
            panel2.add(WQ308);
            WQ308.setBounds(645, 269, 75, 25);

            //---- WQ309 ----
            WQ309.setFont(WQ309.getFont().deriveFont(WQ309.getFont().getSize() - 1f));
            WQ309.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ309.setComponentPopupMenu(popupMenu1);
            WQ309.setName("WQ309");
            panel2.add(WQ309);
            WQ309.setBounds(645, 294, 75, 25);

            //---- WQ310 ----
            WQ310.setFont(WQ310.getFont().deriveFont(WQ310.getFont().getSize() - 1f));
            WQ310.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ310.setComponentPopupMenu(popupMenu1);
            WQ310.setName("WQ310");
            panel2.add(WQ310);
            WQ310.setBounds(645, 319, 75, 25);

            //---- WQ311 ----
            WQ311.setFont(WQ311.getFont().deriveFont(WQ311.getFont().getSize() - 1f));
            WQ311.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ311.setComponentPopupMenu(popupMenu1);
            WQ311.setName("WQ311");
            panel2.add(WQ311);
            WQ311.setBounds(645, 344, 75, 25);

            //---- WQ312 ----
            WQ312.setFont(WQ312.getFont().deriveFont(WQ312.getFont().getSize() - 1f));
            WQ312.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ312.setComponentPopupMenu(popupMenu1);
            WQ312.setName("WQ312");
            panel2.add(WQ312);
            WQ312.setBounds(645, 369, 75, 25);

            //---- WQ313 ----
            WQ313.setFont(WQ313.getFont().deriveFont(WQ313.getFont().getSize() - 1f));
            WQ313.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ313.setComponentPopupMenu(popupMenu1);
            WQ313.setName("WQ313");
            panel2.add(WQ313);
            WQ313.setBounds(645, 394, 75, 25);

            //---- WQ314 ----
            WQ314.setFont(WQ314.getFont().deriveFont(WQ314.getFont().getSize() - 1f));
            WQ314.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ314.setComponentPopupMenu(popupMenu1);
            WQ314.setName("WQ314");
            panel2.add(WQ314);
            WQ314.setBounds(645, 419, 75, 25);

            //---- WQ315 ----
            WQ315.setFont(WQ315.getFont().deriveFont(WQ315.getFont().getSize() - 1f));
            WQ315.setHorizontalAlignment(SwingConstants.RIGHT);
            WQ315.setComponentPopupMenu(popupMenu1);
            WQ315.setName("WQ315");
            panel2.add(WQ315);
            WQ315.setBounds(645, 444, 75, 25);

            //---- UTIT1 ----
            UTIT1.setText("@UTIT1@");
            UTIT1.setFont(new Font(Font.MONOSPACED, Font.BOLD, 11));
            UTIT1.setName("UTIT1");
            panel2.add(UTIT1);
            UTIT1.setBounds(20, 70, 470, 22);

            //---- TITQ1 ----
            TITQ1.setText("@TITQ1@");
            TITQ1.setFont(new Font(Font.MONOSPACED, Font.BOLD, 11));
            TITQ1.setHorizontalAlignment(SwingConstants.CENTER);
            TITQ1.setName("TITQ1");
            panel2.add(TITQ1);
            TITQ1.setBounds(496, 70, 73, 22);

            //---- TITQ2 ----
            TITQ2.setText("@TITQ2@");
            TITQ2.setFont(new Font(Font.MONOSPACED, Font.BOLD, 11));
            TITQ2.setHorizontalAlignment(SwingConstants.CENTER);
            TITQ2.setName("TITQ2");
            panel2.add(TITQ2);
            TITQ2.setBounds(571, 70, 73, 22);

            //---- TITQ3 ----
            TITQ3.setText("@TITQ3@");
            TITQ3.setFont(new Font(Font.MONOSPACED, Font.BOLD, 11));
            TITQ3.setHorizontalAlignment(SwingConstants.CENTER);
            TITQ3.setName("TITQ3");
            panel2.add(TITQ3);
            TITQ3.setBounds(646, 70, 73, 22);

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- BT_PGUP ----
              BT_PGUP.setText("");
              BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
              BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGUP.setName("BT_PGUP");
              panel3.add(BT_PGUP);
              BT_PGUP.setBounds(5, 5, 25, 170);

              //---- BT_PGDOWN ----
              BT_PGDOWN.setText("");
              BT_PGDOWN.setToolTipText("Page suivante");
              BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGDOWN.setName("BT_PGDOWN");
              panel3.add(BT_PGDOWN);
              BT_PGDOWN.setBounds(5, 210, 25, 170);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel2.add(panel3);
            panel3.setBounds(720, 90, 35, 385);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 770, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 770, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 490, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }

    //---- OBJ_60 ----
    OBJ_60.setText("@MESD@");
    OBJ_60.setHorizontalAlignment(SwingConstants.RIGHT);
    OBJ_60.setName("OBJ_60");

    //---- OBJ_59 ----
    OBJ_59.setText("@MESG@");
    OBJ_59.setForeground(Color.red);
    OBJ_59.setName("OBJ_59");

    //======== riSousMenu1 ========
    {
      riSousMenu1.setName("riSousMenu1");

      //---- riSousMenu_bt1 ----
      riSousMenu_bt1.setText("Affichage \u00e9tendu");
      riSousMenu_bt1.setName("riSousMenu_bt1");
      riSousMenu1.add(riSousMenu_bt1);
    }

    //======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");

      //---- menuItem1 ----
      menuItem1.setText("Options article");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem1);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie OBJ_44;
  private RiZoneSortie E1NUM;
  private RiZoneSortie CLCLI;
  private JLabel OBJ_36;
  private JLabel OBJ_37;
  private RiZoneSortie E1ETB;
  private RiZoneSortie CLLIV;
  private JLabel OBJ_34;
  private RiZoneSortie E1SUF;
  private JPanel p_tete_droite;
  private JLabel relique;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField WARTD;
  private XRiComboBox WTRT;
  private JLabel OBJ_53;
  private XRiTextField WIN3D;
  private JPanel panel2;
  private RiZoneSortie OBJ_65;
  private RiZoneSortie OBJ_62;
  private RiZoneSortie OBJ_63;
  private JLabel OBJ_61;
  private RiZoneSortie OBJ_64;
  private JLabel label1;
  private RiZoneSortie L301;
  private RiZoneSortie L302;
  private RiZoneSortie L303;
  private RiZoneSortie L304;
  private RiZoneSortie L305;
  private RiZoneSortie L306;
  private RiZoneSortie L307;
  private RiZoneSortie L308;
  private RiZoneSortie L309;
  private RiZoneSortie L310;
  private RiZoneSortie L311;
  private RiZoneSortie L312;
  private RiZoneSortie L313;
  private RiZoneSortie L314;
  private RiZoneSortie L315;
  private XRiTextField WQ101;
  private XRiTextField WQ201;
  private XRiTextField WQ301;
  private XRiTextField WQ102;
  private XRiTextField WQ103;
  private XRiTextField WQ104;
  private XRiTextField WQ105;
  private XRiTextField WQ106;
  private XRiTextField WQ107;
  private XRiTextField WQ108;
  private XRiTextField WQ109;
  private XRiTextField WQ110;
  private XRiTextField WQ111;
  private XRiTextField WQ112;
  private XRiTextField WQ113;
  private XRiTextField WQ114;
  private XRiTextField WQ115;
  private XRiTextField WQ202;
  private XRiTextField WQ203;
  private XRiTextField WQ204;
  private XRiTextField WQ205;
  private XRiTextField WQ206;
  private XRiTextField WQ207;
  private XRiTextField WQ208;
  private XRiTextField WQ209;
  private XRiTextField WQ210;
  private XRiTextField WQ211;
  private XRiTextField WQ212;
  private XRiTextField WQ213;
  private XRiTextField WQ214;
  private XRiTextField WQ215;
  private XRiTextField WQ302;
  private XRiTextField WQ303;
  private XRiTextField WQ304;
  private XRiTextField WQ305;
  private XRiTextField WQ306;
  private XRiTextField WQ307;
  private XRiTextField WQ308;
  private XRiTextField WQ309;
  private XRiTextField WQ310;
  private XRiTextField WQ311;
  private XRiTextField WQ312;
  private XRiTextField WQ313;
  private XRiTextField WQ314;
  private XRiTextField WQ315;
  private RiZoneSortie UTIT1;
  private RiZoneSortie TITQ1;
  private RiZoneSortie TITQ2;
  private RiZoneSortie TITQ3;
  private JPanel panel3;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  private JLabel OBJ_60;
  private JLabel OBJ_59;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
