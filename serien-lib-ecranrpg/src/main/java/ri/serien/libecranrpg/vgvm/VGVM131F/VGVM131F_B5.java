
package ri.serien.libecranrpg.vgvm.VGVM131F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGVM131F_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM131F_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    EDTBON.setValeursSelection("1", "");
    EDTSIT.setValeursSelection("1", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLIB@")).trim());
    NTNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NTNUM@")).trim());
    TOTTTC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTTTC@")).trim());
    TOTRGL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTRGL@")).trim());
    OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDIF@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB01@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB02@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB03@")).trim());
    riZoneSortie4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB04@")).trim());
    LIB05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB05@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    if (lexique.isTrue("50")) {
      OBJ_34.setText("A rendre");
    }
    else {
      OBJ_34.setText("Reste à régler");
    }
    
    // Titre
    setTitle("Gestion d'une préparation");
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_21 = new RiZoneSortie();
    OBJ_17 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_19 = new JLabel();
    NTDT1X = new XRiCalendrier();
    NTNUM = new RiZoneSortie();
    NTCTR = new XRiTextField();
    EDTSIT = new XRiCheckBox();
    EDTBON = new XRiCheckBox();
    OBJ_34 = new JLabel();
    TOTTTC = new RiZoneSortie();
    TOTRGL = new RiZoneSortie();
    OBJ_35 = new RiZoneSortie();
    OBJ_30 = new JLabel();
    WFOR = new XRiTextField();
    OBJ_32 = new JLabel();
    riZoneSortie1 = new RiZoneSortie();
    MRG01 = new XRiTextField();
    MTT01 = new XRiTextField();
    RF01 = new XRiTextField();
    DOM01 = new XRiTextField();
    riZoneSortie2 = new RiZoneSortie();
    MRG02 = new XRiTextField();
    MTT02 = new XRiTextField();
    RF02 = new XRiTextField();
    DOM02 = new XRiTextField();
    riZoneSortie3 = new RiZoneSortie();
    MRG03 = new XRiTextField();
    MTT03 = new XRiTextField();
    RF03 = new XRiTextField();
    DOM03 = new XRiTextField();
    riZoneSortie4 = new RiZoneSortie();
    MRG04 = new XRiTextField();
    MTT04 = new XRiTextField();
    RF04 = new XRiTextField();
    DOM04 = new XRiTextField();
    LIB05 = new RiZoneSortie();
    MRG05 = new XRiTextField();
    MTT05 = new XRiTextField();
    RF05 = new XRiTextField();
    DOM05 = new XRiTextField();
    OBJ_31 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_38 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(845, 410));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_21 ----
          OBJ_21.setText("@TRLIB@");
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(440, 47, 195, 24);

          //---- OBJ_17 ----
          OBJ_17.setText("R\u00e8glement N\u00b0");
          OBJ_17.setName("OBJ_17");
          panel1.add(OBJ_17);
          OBJ_17.setBounds(30, 13, 86, 28);

          //---- OBJ_22 ----
          OBJ_22.setText("R\u00e8glement le");
          OBJ_22.setName("OBJ_22");
          panel1.add(OBJ_22);
          OBJ_22.setBounds(30, 45, 83, 28);

          //---- OBJ_19 ----
          OBJ_19.setText("Transporteur");
          OBJ_19.setName("OBJ_19");
          panel1.add(OBJ_19);
          OBJ_19.setBounds(312, 45, 80, 28);

          //---- NTDT1X ----
          NTDT1X.setComponentPopupMenu(BTD);
          NTDT1X.setName("NTDT1X");
          panel1.add(NTDT1X);
          NTDT1X.setBounds(125, 45, 105, NTDT1X.getPreferredSize().height);

          //---- NTNUM ----
          NTNUM.setComponentPopupMenu(BTD);
          NTNUM.setText("@NTNUM@");
          NTNUM.setHorizontalAlignment(SwingConstants.RIGHT);
          NTNUM.setName("NTNUM");
          panel1.add(NTNUM);
          NTNUM.setBounds(125, 15, 62, NTNUM.getPreferredSize().height);

          //---- NTCTR ----
          NTCTR.setComponentPopupMenu(BTD);
          NTCTR.setName("NTCTR");
          panel1.add(NTCTR);
          NTCTR.setBounds(400, 45, 34, NTCTR.getPreferredSize().height);

          //---- EDTSIT ----
          EDTSIT.setText("Edition de la situation de compte");
          EDTSIT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTSIT.setName("EDTSIT");
          panel1.add(EDTSIT);
          EDTSIT.setBounds(30, 335, 295, 28);

          //---- EDTBON ----
          EDTBON.setText("Edition des r\u00e8glements");
          EDTBON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTBON.setName("EDTBON");
          panel1.add(EDTBON);
          EDTBON.setBounds(30, 305, 295, 28);

          //---- OBJ_34 ----
          OBJ_34.setText("Reste \u00e0 r\u00e9gler");
          OBJ_34.setName("OBJ_34");
          panel1.add(OBJ_34);
          OBJ_34.setBounds(400, 266, 88, 28);

          //---- TOTTTC ----
          TOTTTC.setComponentPopupMenu(BTD);
          TOTTTC.setText("@TOTTTC@");
          TOTTTC.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTTTC.setName("TOTTTC");
          panel1.add(TOTTTC);
          TOTTTC.setBounds(97, 268, 85, TOTTTC.getPreferredSize().height);

          //---- TOTRGL ----
          TOTRGL.setComponentPopupMenu(BTD);
          TOTRGL.setText("@TOTRGL@");
          TOTRGL.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTRGL.setName("TOTRGL");
          panel1.add(TOTRGL);
          TOTRGL.setBounds(282, 268, 85, 24);

          //---- OBJ_35 ----
          OBJ_35.setText("@WDIF@");
          OBJ_35.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_35.setName("OBJ_35");
          panel1.add(OBJ_35);
          OBJ_35.setBounds(495, 268, 85, 24);

          //---- OBJ_30 ----
          OBJ_30.setText("A r\u00e8gler");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(30, 266, 50, 28);

          //---- WFOR ----
          WFOR.setToolTipText("For\u00e7age");
          WFOR.setComponentPopupMenu(BTD);
          WFOR.setName("WFOR");
          panel1.add(WFOR);
          WFOR.setBounds(585, 266, 50, WFOR.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("Re\u00e7u");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(239, 266, 35, 28);

          //---- riZoneSortie1 ----
          riZoneSortie1.setText("@LIB01@");
          riZoneSortie1.setName("riZoneSortie1");
          panel1.add(riZoneSortie1);
          riZoneSortie1.setBounds(30, 120, 110, riZoneSortie1.getPreferredSize().height);

          //---- MRG01 ----
          MRG01.setComponentPopupMenu(BTD);
          MRG01.setName("MRG01");
          panel1.add(MRG01);
          MRG01.setBounds(148, 118, 34, MRG01.getPreferredSize().height);

          //---- MTT01 ----
          MTT01.setName("MTT01");
          panel1.add(MTT01);
          MTT01.setBounds(190, 118, 84, MTT01.getPreferredSize().height);

          //---- RF01 ----
          RF01.setName("RF01");
          panel1.add(RF01);
          RF01.setBounds(282, 118, 110, RF01.getPreferredSize().height);

          //---- DOM01 ----
          DOM01.setName("DOM01");
          panel1.add(DOM01);
          DOM01.setBounds(400, 118, 235, DOM01.getPreferredSize().height);

          //---- riZoneSortie2 ----
          riZoneSortie2.setText("@LIB02@");
          riZoneSortie2.setName("riZoneSortie2");
          panel1.add(riZoneSortie2);
          riZoneSortie2.setBounds(30, 145, 110, riZoneSortie2.getPreferredSize().height);

          //---- MRG02 ----
          MRG02.setComponentPopupMenu(BTD);
          MRG02.setName("MRG02");
          panel1.add(MRG02);
          MRG02.setBounds(148, 143, 34, MRG02.getPreferredSize().height);

          //---- MTT02 ----
          MTT02.setName("MTT02");
          panel1.add(MTT02);
          MTT02.setBounds(190, 143, 84, MTT02.getPreferredSize().height);

          //---- RF02 ----
          RF02.setName("RF02");
          panel1.add(RF02);
          RF02.setBounds(282, 143, 110, RF02.getPreferredSize().height);

          //---- DOM02 ----
          DOM02.setName("DOM02");
          panel1.add(DOM02);
          DOM02.setBounds(400, 143, 235, DOM02.getPreferredSize().height);

          //---- riZoneSortie3 ----
          riZoneSortie3.setText("@LIB03@");
          riZoneSortie3.setName("riZoneSortie3");
          panel1.add(riZoneSortie3);
          riZoneSortie3.setBounds(30, 170, 110, riZoneSortie3.getPreferredSize().height);

          //---- MRG03 ----
          MRG03.setComponentPopupMenu(BTD);
          MRG03.setName("MRG03");
          panel1.add(MRG03);
          MRG03.setBounds(148, 168, 34, MRG03.getPreferredSize().height);

          //---- MTT03 ----
          MTT03.setName("MTT03");
          panel1.add(MTT03);
          MTT03.setBounds(190, 168, 84, MTT03.getPreferredSize().height);

          //---- RF03 ----
          RF03.setName("RF03");
          panel1.add(RF03);
          RF03.setBounds(282, 168, 110, RF03.getPreferredSize().height);

          //---- DOM03 ----
          DOM03.setName("DOM03");
          panel1.add(DOM03);
          DOM03.setBounds(400, 168, 235, DOM03.getPreferredSize().height);

          //---- riZoneSortie4 ----
          riZoneSortie4.setText("@LIB04@");
          riZoneSortie4.setName("riZoneSortie4");
          panel1.add(riZoneSortie4);
          riZoneSortie4.setBounds(30, 195, 110, riZoneSortie4.getPreferredSize().height);

          //---- MRG04 ----
          MRG04.setComponentPopupMenu(BTD);
          MRG04.setName("MRG04");
          panel1.add(MRG04);
          MRG04.setBounds(148, 193, 34, MRG04.getPreferredSize().height);

          //---- MTT04 ----
          MTT04.setName("MTT04");
          panel1.add(MTT04);
          MTT04.setBounds(190, 193, 84, MTT04.getPreferredSize().height);

          //---- RF04 ----
          RF04.setName("RF04");
          panel1.add(RF04);
          RF04.setBounds(282, 193, 110, RF04.getPreferredSize().height);

          //---- DOM04 ----
          DOM04.setName("DOM04");
          panel1.add(DOM04);
          DOM04.setBounds(400, 193, 235, DOM04.getPreferredSize().height);

          //---- LIB05 ----
          LIB05.setText("@LIB05@");
          LIB05.setName("LIB05");
          panel1.add(LIB05);
          LIB05.setBounds(30, 220, 110, LIB05.getPreferredSize().height);

          //---- MRG05 ----
          MRG05.setComponentPopupMenu(BTD);
          MRG05.setName("MRG05");
          panel1.add(MRG05);
          MRG05.setBounds(148, 218, 34, MRG05.getPreferredSize().height);

          //---- MTT05 ----
          MTT05.setName("MTT05");
          panel1.add(MTT05);
          MTT05.setBounds(190, 218, 84, MTT05.getPreferredSize().height);

          //---- RF05 ----
          RF05.setName("RF05");
          panel1.add(RF05);
          RF05.setBounds(282, 218, 110, RF05.getPreferredSize().height);

          //---- DOM05 ----
          DOM05.setName("DOM05");
          panel1.add(DOM05);
          DOM05.setBounds(400, 218, 235, DOM05.getPreferredSize().height);

          //---- OBJ_31 ----
          OBJ_31.setText("R\u00e8glement");
          OBJ_31.setFont(OBJ_31.getFont().deriveFont(OBJ_31.getFont().getStyle() | Font.BOLD));
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(30, 95, 110, 28);

          //---- OBJ_33 ----
          OBJ_33.setText("Montant");
          OBJ_33.setFont(OBJ_33.getFont().deriveFont(OBJ_33.getFont().getStyle() | Font.BOLD));
          OBJ_33.setName("OBJ_33");
          panel1.add(OBJ_33);
          OBJ_33.setBounds(190, 95, 84, 28);

          //---- OBJ_36 ----
          OBJ_36.setText("R\u00e9f\u00e9rence");
          OBJ_36.setFont(OBJ_36.getFont().deriveFont(OBJ_36.getFont().getStyle() | Font.BOLD));
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(282, 95, 110, 28);

          //---- OBJ_37 ----
          OBJ_37.setText("Nom de la banque - Ville");
          OBJ_37.setFont(OBJ_37.getFont().deriveFont(OBJ_37.getFont().getStyle() | Font.BOLD));
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(400, 95, 235, 28);

          //---- OBJ_38 ----
          OBJ_38.setText("Mrg.");
          OBJ_38.setFont(OBJ_38.getFont().deriveFont(OBJ_38.getFont().getStyle() | Font.BOLD));
          OBJ_38.setName("OBJ_38");
          panel1.add(OBJ_38);
          OBJ_38.setBounds(150, 95, 34, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie OBJ_21;
  private JLabel OBJ_17;
  private JLabel OBJ_22;
  private JLabel OBJ_19;
  private XRiCalendrier NTDT1X;
  private RiZoneSortie NTNUM;
  private XRiTextField NTCTR;
  private XRiCheckBox EDTSIT;
  private XRiCheckBox EDTBON;
  private JLabel OBJ_34;
  private RiZoneSortie TOTTTC;
  private RiZoneSortie TOTRGL;
  private RiZoneSortie OBJ_35;
  private JLabel OBJ_30;
  private XRiTextField WFOR;
  private JLabel OBJ_32;
  private RiZoneSortie riZoneSortie1;
  private XRiTextField MRG01;
  private XRiTextField MTT01;
  private XRiTextField RF01;
  private XRiTextField DOM01;
  private RiZoneSortie riZoneSortie2;
  private XRiTextField MRG02;
  private XRiTextField MTT02;
  private XRiTextField RF02;
  private XRiTextField DOM02;
  private RiZoneSortie riZoneSortie3;
  private XRiTextField MRG03;
  private XRiTextField MTT03;
  private XRiTextField RF03;
  private XRiTextField DOM03;
  private RiZoneSortie riZoneSortie4;
  private XRiTextField MRG04;
  private XRiTextField MTT04;
  private XRiTextField RF04;
  private XRiTextField DOM04;
  private RiZoneSortie LIB05;
  private XRiTextField MRG05;
  private XRiTextField MTT05;
  private XRiTextField RF05;
  private XRiTextField DOM05;
  private JLabel OBJ_31;
  private JLabel OBJ_33;
  private JLabel OBJ_36;
  private JLabel OBJ_37;
  private JLabel OBJ_38;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
