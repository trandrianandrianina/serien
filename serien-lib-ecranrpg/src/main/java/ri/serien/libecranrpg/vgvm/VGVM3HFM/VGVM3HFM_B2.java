
package ri.serien.libecranrpg.vgvm.VGVM3HFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM3HFM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM3HFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    
    DPAUT.setValeurs("OUI", buttonGroup1);
    DPAUT_N.setValeurs("NON");
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    E1VDE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1VDE@")).trim());
    E1MAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1MAG@")).trim());
    E1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ETB@")).trim());
    E1NUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NUM@")).trim());
    DPUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DPUSR@")).trim());
    DPTIM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DPTIM1@")).trim());
    E1TTC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TTC@")).trim());
    DPRES.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DPRES@")).trim());
    DPTIM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DPTIM2@")).trim());
    DPTIM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DPTIM3@")).trim());
    E1SUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1SUF@")).trim());
    DPART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DPART@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    gererLesErreurs("19");
    
    
    String dptim = lexique.HostFieldGetData("DPTIM");
    if (dptim.length() == 5) { // Pour les heures inférieures à 10 car le 0 est supprimé à cause du code d'édition
      dptim = '0' + dptim;
    }
    DPTIM1.setText(dptim.substring(0, 2));
    DPTIM2.setText(dptim.substring(2, 4));
    DPTIM3.setText(dptim.substring(4, 6));
    
    // TODO Icones
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E1NUM");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    E1VDE = new RiZoneSortie();
    E1MAG = new RiZoneSortie();
    label2 = new JLabel();
    E1ETB = new RiZoneSortie();
    E1NUM = new RiZoneSortie();
    DPUSR = new RiZoneSortie();
    label3 = new JLabel();
    label4 = new JLabel();
    DPCREX = new XRiCalendrier();
    DPTIM1 = new RiZoneSortie();
    label5 = new JLabel();
    E1TTC = new RiZoneSortie();
    label6 = new JLabel();
    DPRES = new RiZoneSortie();
    label8 = new JLabel();
    panel2 = new JPanel();
    label7 = new JLabel();
    DPAUT = new XRiRadioButton();
    DPAUT_N = new XRiRadioButton();
    DPTIM2 = new RiZoneSortie();
    DPTIM3 = new RiZoneSortie();
    label10 = new JLabel();
    label9 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    E1SUF = new RiZoneSortie();
    riBoutonDetail1 = new SNBoutonDetail();
    DPMOT = new XRiTextField();
    label14 = new JLabel();
    label15 = new JLabel();
    DPART = new RiZoneSortie();
    buttonGroup1 = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(1015, 285));
    setPreferredSize(new Dimension(1015, 285));
    setMaximumSize(new Dimension(1015, 285));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("D\u00e9blocage prix de vente client sur le bon"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- label1 ----
          label1.setText("Vendeur");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(500, 60, 60, 24);

          //---- E1VDE ----
          E1VDE.setText("@E1VDE@");
          E1VDE.setName("E1VDE");
          panel1.add(E1VDE);
          E1VDE.setBounds(565, 60, 45, E1VDE.getPreferredSize().height);

          //---- E1MAG ----
          E1MAG.setText("@E1MAG@");
          E1MAG.setName("E1MAG");
          panel1.add(E1MAG);
          E1MAG.setBounds(770, 60, 34, E1MAG.getPreferredSize().height);

          //---- label2 ----
          label2.setText("Magasin");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(660, 60, 75, 24);

          //---- E1ETB ----
          E1ETB.setText("@E1ETB@");
          E1ETB.setName("E1ETB");
          panel1.add(E1ETB);
          E1ETB.setBounds(130, 60, 44, E1ETB.getPreferredSize().height);

          //---- E1NUM ----
          E1NUM.setText("@E1NUM@");
          E1NUM.setName("E1NUM");
          panel1.add(E1NUM);
          E1NUM.setBounds(340, 60, 64, E1NUM.getPreferredSize().height);

          //---- DPUSR ----
          DPUSR.setText("@DPUSR@");
          DPUSR.setName("DPUSR");
          panel1.add(DPUSR);
          DPUSR.setBounds(130, 90, 114, DPUSR.getPreferredSize().height);

          //---- label3 ----
          label3.setText("Utilisateur");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(30, 90, 95, 24);

          //---- label4 ----
          label4.setText("Date et heure");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(565, 90, 95, 24);

          //---- DPCREX ----
          DPCREX.setName("DPCREX");
          panel1.add(DPCREX);
          DPCREX.setBounds(660, 88, 105, DPCREX.getPreferredSize().height);

          //---- DPTIM1 ----
          DPTIM1.setText("@DPTIM1@");
          DPTIM1.setHorizontalAlignment(SwingConstants.RIGHT);
          DPTIM1.setName("DPTIM1");
          panel1.add(DPTIM1);
          DPTIM1.setBounds(605, 120, 30, DPTIM1.getPreferredSize().height);

          //---- label5 ----
          label5.setText("Montant TTC");
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(30, 120, 95, 24);

          //---- E1TTC ----
          E1TTC.setText("@E1TTC@");
          E1TTC.setHorizontalAlignment(SwingConstants.RIGHT);
          E1TTC.setName("E1TTC");
          panel1.add(E1TTC);
          E1TTC.setBounds(130, 120, 114, E1TTC.getPreferredSize().height);

          //---- label6 ----
          label6.setText("Responsable");
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(320, 90, 95, 24);

          //---- DPRES ----
          DPRES.setText("@DPRES@");
          DPRES.setName("DPRES");
          panel1.add(DPRES);
          DPRES.setBounds(410, 90, 114, 24);

          //---- label8 ----
          label8.setText("\u00e0");
          label8.setHorizontalTextPosition(SwingConstants.CENTER);
          label8.setName("label8");
          panel1.add(label8);
          label8.setBounds(565, 120, 40, 24);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- label7 ----
            label7.setText("D\u00e9blocage");
            label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD, label7.getFont().getSize() + 3f));
            label7.setName("label7");
            panel2.add(label7);
            label7.setBounds(15, 15, 100, 24);

            //---- DPAUT ----
            DPAUT.setText("OUI");
            DPAUT.setName("DPAUT");
            panel2.add(DPAUT);
            DPAUT.setBounds(195, 15, 55, 24);

            //---- DPAUT_N ----
            DPAUT_N.setText("NON");
            DPAUT_N.setName("DPAUT_N");
            panel2.add(DPAUT_N);
            DPAUT_N.setBounds(280, 15, 55, 24);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(455, 190, 350, 55);

          //---- DPTIM2 ----
          DPTIM2.setText("@DPTIM2@");
          DPTIM2.setHorizontalAlignment(SwingConstants.RIGHT);
          DPTIM2.setName("DPTIM2");
          panel1.add(DPTIM2);
          DPTIM2.setBounds(660, 120, 30, DPTIM2.getPreferredSize().height);

          //---- DPTIM3 ----
          DPTIM3.setText("@DPTIM3@");
          DPTIM3.setHorizontalAlignment(SwingConstants.RIGHT);
          DPTIM3.setName("DPTIM3");
          panel1.add(DPTIM3);
          DPTIM3.setBounds(735, 120, 30, DPTIM3.getPreferredSize().height);

          //---- label10 ----
          label10.setText("mn");
          label10.setHorizontalAlignment(SwingConstants.CENTER);
          label10.setName("label10");
          panel1.add(label10);
          label10.setBounds(695, 120, 20, 24);

          //---- label9 ----
          label9.setText("h");
          label9.setHorizontalAlignment(SwingConstants.CENTER);
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(635, 120, 20, 24);

          //---- label11 ----
          label11.setText("s");
          label11.setHorizontalAlignment(SwingConstants.CENTER);
          label11.setName("label11");
          panel1.add(label11);
          label11.setBounds(765, 120, 20, 24);

          //---- label12 ----
          label12.setText("Etablissement");
          label12.setName("label12");
          panel1.add(label12);
          label12.setBounds(30, 60, 95, 24);

          //---- label13 ----
          label13.setText("Num\u00e9ro de bon");
          label13.setName("label13");
          panel1.add(label13);
          label13.setBounds(220, 60, 100, 24);

          //---- E1SUF ----
          E1SUF.setText("@E1SUF@");
          E1SUF.setName("E1SUF");
          panel1.add(E1SUF);
          E1SUF.setBounds(410, 60, 24, E1SUF.getPreferredSize().height);

          //---- riBoutonDetail1 ----
          riBoutonDetail1.setName("riBoutonDetail1");
          riBoutonDetail1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetail1ActionPerformed(e);
            }
          });
          panel1.add(riBoutonDetail1);
          riBoutonDetail1.setBounds(new Rectangle(new Point(320, 63), riBoutonDetail1.getPreferredSize()));

          //---- DPMOT ----
          DPMOT.setName("DPMOT");
          panel1.add(DPMOT);
          DPMOT.setBounds(130, 147, 675, 30);

          //---- label14 ----
          label14.setText("Motif");
          label14.setName("label14");
          panel1.add(label14);
          label14.setBounds(30, 150, 95, 24);

          //---- label15 ----
          label15.setText("Code article");
          label15.setName("label15");
          panel1.add(label15);
          label15.setBounds(30, 30, 95, 24);

          //---- DPART ----
          DPART.setText("@DPART@");
          DPART.setName("DPART");
          panel1.add(DPART);
          DPART.setBounds(130, 30, 210, DPART.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 821, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- buttonGroup1 ----
    buttonGroup1.add(DPAUT);
    buttonGroup1.add(DPAUT_N);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label1;
  private RiZoneSortie E1VDE;
  private RiZoneSortie E1MAG;
  private JLabel label2;
  private RiZoneSortie E1ETB;
  private RiZoneSortie E1NUM;
  private RiZoneSortie DPUSR;
  private JLabel label3;
  private JLabel label4;
  private XRiCalendrier DPCREX;
  private RiZoneSortie DPTIM1;
  private JLabel label5;
  private RiZoneSortie E1TTC;
  private JLabel label6;
  private RiZoneSortie DPRES;
  private JLabel label8;
  private JPanel panel2;
  private JLabel label7;
  private XRiRadioButton DPAUT;
  private XRiRadioButton DPAUT_N;
  private RiZoneSortie DPTIM2;
  private RiZoneSortie DPTIM3;
  private JLabel label10;
  private JLabel label9;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private RiZoneSortie E1SUF;
  private SNBoutonDetail riBoutonDetail1;
  private XRiTextField DPMOT;
  private JLabel label14;
  private JLabel label15;
  private RiZoneSortie DPART;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
