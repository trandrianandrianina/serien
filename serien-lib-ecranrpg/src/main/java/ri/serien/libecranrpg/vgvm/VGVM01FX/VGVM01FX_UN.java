
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_UN extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] UNDEC_Value = { "", "0", "1", "2", "3", };
  private String[] UNNAT_Value = { "", "N", "L", "S", "V", "P", "T", };
  private String[] UNCND_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "X", };
  private ODialog dialog_SUSPECT = null;
  
  public VGVM01FX_UN(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    UNDEC.setValeurs(UNDEC_Value, null);
    UNNAT.setValeurs(UNNAT_Value, null);
    UNCND.setValeurs(UNCND_Value, null);
    UNQTD.setValeursSelection("1", " ");
    UNCRDM.setValeursSelection("1", " ");
    UNCCOL.setValeursSelection("1", " ");
    UNCVOL.setValeursSelection("1", " ");
    UNCPDS.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL201@")).trim());
    OBJ_110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL202@")).trim());
    OBJ_114.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL203@")).trim());
    OBJ_118.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL204@")).trim());
    OBJ_122.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL205@")).trim());
    OBJ_125.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL206@")).trim());
    OBJ_129.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL207@")).trim());
    OBJ_134.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL208@")).trim());
    OBJ_138.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL209@")).trim());
    OBJ_142.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUL210@")).trim());
    OBJ_105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL01R@")).trim());
    OBJ_108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL02R@")).trim());
    OBJ_112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL03R@")).trim());
    OBJ_116.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL04R@")).trim());
    OBJ_120.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL05R@")).trim());
    OBJ_123.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL06R@")).trim());
    OBJ_127.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL07R@")).trim());
    OBJ_132.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL08R@")).trim());
    OBJ_136.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL09R@")).trim());
    OBJ_140.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UL10R@")).trim());
    INDUNU.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUNU@")).trim());
    INDUN2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDUN2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    OBJ_101.setVisible(lexique.isTrue("23"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (dialog_SUSPECT == null) {
      dialog_SUSPECT = new ODialog((Window) getTopLevelAncestor(), new UN_SUSPECT(this));
    }
    dialog_SUSPECT.affichePopupPerso();
  }

  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel8 = new JXTitledPanel();
    OBJ_95 = new JLabel();
    UNLIB = new XRiTextField();
    OBJ_97 = new JLabel();
    UNLIB2 = new XRiTextField();
    panel2 = new JPanel();
    OBJ_107 = new JLabel();
    OBJ_110 = new JLabel();
    OBJ_114 = new JLabel();
    OBJ_118 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_129 = new JLabel();
    OBJ_134 = new JLabel();
    OBJ_138 = new JLabel();
    OBJ_142 = new JLabel();
    UNK01 = new XRiTextField();
    UNK02 = new XRiTextField();
    UNK03 = new XRiTextField();
    UNK04 = new XRiTextField();
    UNK05 = new XRiTextField();
    UNK06 = new XRiTextField();
    UNK07 = new XRiTextField();
    UNK08 = new XRiTextField();
    UNK09 = new XRiTextField();
    UNK10 = new XRiTextField();
    OBJ_105 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_112 = new JLabel();
    OBJ_116 = new JLabel();
    OBJ_120 = new JLabel();
    OBJ_123 = new JLabel();
    OBJ_127 = new JLabel();
    OBJ_132 = new JLabel();
    OBJ_136 = new JLabel();
    OBJ_140 = new JLabel();
    UNU1 = new XRiTextField();
    UNU2 = new XRiTextField();
    UNU3 = new XRiTextField();
    UNU4 = new XRiTextField();
    UNU5 = new XRiTextField();
    UNU6 = new XRiTextField();
    UNU7 = new XRiTextField();
    UNU8 = new XRiTextField();
    UNU9 = new XRiTextField();
    UNU10 = new XRiTextField();
    OBJ_106 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_117 = new JLabel();
    OBJ_121 = new JLabel();
    OBJ_124 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_133 = new JLabel();
    OBJ_137 = new JLabel();
    OBJ_141 = new JLabel();
    OBJ_102 = new JLabel();
    INDUNU = new RiZoneSortie();
    OBJ_61 = new JLabel();
    INDUN2 = new RiZoneSortie();
    UNKUNN = new XRiTextField();
    panel5 = new JPanel();
    OBJ_143 = new JLabel();
    OBJ_101 = new JLabel();
    UNCND = new XRiComboBox();
    OBJ_130 = new JLabel();
    UNART = new XRiTextField();
    UNETQ = new XRiTextField();
    OBJ_135 = new JLabel();
    UNREFU = new XRiTextField();
    OBJ_139 = new JLabel();
    UNTAR = new XRiTextField();
    panel6 = new JPanel();
    OBJ_96 = new JLabel();
    UNNAT = new XRiComboBox();
    OBJ_100 = new JLabel();
    UNDEC = new XRiComboBox();
    panel3 = new JPanel();
    OBJ_126 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_119 = new JLabel();
    OBJ_115 = new JLabel();
    UNCMAN = new XRiTextField();
    UNLONG = new XRiTextField();
    UNLARG = new XRiTextField();
    UNHAUT = new XRiTextField();
    panel4 = new JPanel();
    UNCPDS = new XRiCheckBox();
    UNCVOL = new XRiCheckBox();
    UNCCOL = new XRiCheckBox();
    UNCRDM = new XRiCheckBox();
    UNQTD = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Quantit\u00e9s suspectes");
              riSousMenu_bt7.setToolTipText("quantit\u00e9s suspectes pour alerte en saisie de vente");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Historique modifications");
              riSousMenu_bt8.setToolTipText("Historique des modifications");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(760, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel8 ========
          {
            xTitledPanel8.setTitle("Code unit\u00e9 de mesure");
            xTitledPanel8.setBorder(new DropShadowBorder());
            xTitledPanel8.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel8.setName("xTitledPanel8");
            Container xTitledPanel8ContentContainer = xTitledPanel8.getContentContainer();
            xTitledPanel8ContentContainer.setLayout(null);

            //---- OBJ_95 ----
            OBJ_95.setText("Libell\u00e9 unit\u00e9");
            OBJ_95.setName("OBJ_95");
            xTitledPanel8ContentContainer.add(OBJ_95);
            OBJ_95.setBounds(10, 10, 75, 20);

            //---- UNLIB ----
            UNLIB.setComponentPopupMenu(BTD);
            UNLIB.setName("UNLIB");
            xTitledPanel8ContentContainer.add(UNLIB);
            UNLIB.setBounds(85, 5, 160, UNLIB.getPreferredSize().height);

            //---- OBJ_97 ----
            OBJ_97.setText("Libell\u00e9 court");
            OBJ_97.setName("OBJ_97");
            xTitledPanel8ContentContainer.add(OBJ_97);
            OBJ_97.setBounds(255, 10, 75, 20);

            //---- UNLIB2 ----
            UNLIB2.setComponentPopupMenu(BTD);
            UNLIB2.setName("UNLIB2");
            xTitledPanel8ContentContainer.add(UNLIB2);
            UNLIB2.setBounds(330, 5, 60, UNLIB2.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder(""));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_107 ----
              OBJ_107.setText("@WUL201@");
              OBJ_107.setName("OBJ_107");
              panel2.add(OBJ_107);
              OBJ_107.setBounds(255, 75, 76, 28);

              //---- OBJ_110 ----
              OBJ_110.setText("@WUL202@");
              OBJ_110.setName("OBJ_110");
              panel2.add(OBJ_110);
              OBJ_110.setBounds(255, 100, 76, 28);

              //---- OBJ_114 ----
              OBJ_114.setText("@WUL203@");
              OBJ_114.setName("OBJ_114");
              panel2.add(OBJ_114);
              OBJ_114.setBounds(255, 125, 76, 28);

              //---- OBJ_118 ----
              OBJ_118.setText("@WUL204@");
              OBJ_118.setName("OBJ_118");
              panel2.add(OBJ_118);
              OBJ_118.setBounds(255, 150, 76, 28);

              //---- OBJ_122 ----
              OBJ_122.setText("@WUL205@");
              OBJ_122.setName("OBJ_122");
              panel2.add(OBJ_122);
              OBJ_122.setBounds(255, 175, 76, 28);

              //---- OBJ_125 ----
              OBJ_125.setText("@WUL206@");
              OBJ_125.setName("OBJ_125");
              panel2.add(OBJ_125);
              OBJ_125.setBounds(255, 200, 76, 28);

              //---- OBJ_129 ----
              OBJ_129.setText("@WUL207@");
              OBJ_129.setName("OBJ_129");
              panel2.add(OBJ_129);
              OBJ_129.setBounds(255, 225, 76, 28);

              //---- OBJ_134 ----
              OBJ_134.setText("@WUL208@");
              OBJ_134.setName("OBJ_134");
              panel2.add(OBJ_134);
              OBJ_134.setBounds(255, 250, 76, 28);

              //---- OBJ_138 ----
              OBJ_138.setText("@WUL209@");
              OBJ_138.setName("OBJ_138");
              panel2.add(OBJ_138);
              OBJ_138.setBounds(255, 275, 76, 28);

              //---- OBJ_142 ----
              OBJ_142.setText("@WUL210@");
              OBJ_142.setName("OBJ_142");
              panel2.add(OBJ_142);
              OBJ_142.setBounds(255, 300, 76, 28);

              //---- UNK01 ----
              UNK01.setComponentPopupMenu(BTD);
              UNK01.setName("UNK01");
              panel2.add(UNK01);
              UNK01.setBounds(170, 75, 74, UNK01.getPreferredSize().height);

              //---- UNK02 ----
              UNK02.setComponentPopupMenu(BTD);
              UNK02.setName("UNK02");
              panel2.add(UNK02);
              UNK02.setBounds(170, 100, 74, UNK02.getPreferredSize().height);

              //---- UNK03 ----
              UNK03.setComponentPopupMenu(BTD);
              UNK03.setName("UNK03");
              panel2.add(UNK03);
              UNK03.setBounds(170, 125, 74, UNK03.getPreferredSize().height);

              //---- UNK04 ----
              UNK04.setComponentPopupMenu(BTD);
              UNK04.setName("UNK04");
              panel2.add(UNK04);
              UNK04.setBounds(170, 150, 74, UNK04.getPreferredSize().height);

              //---- UNK05 ----
              UNK05.setComponentPopupMenu(BTD);
              UNK05.setName("UNK05");
              panel2.add(UNK05);
              UNK05.setBounds(170, 175, 74, UNK05.getPreferredSize().height);

              //---- UNK06 ----
              UNK06.setComponentPopupMenu(BTD);
              UNK06.setName("UNK06");
              panel2.add(UNK06);
              UNK06.setBounds(170, 200, 74, UNK06.getPreferredSize().height);

              //---- UNK07 ----
              UNK07.setComponentPopupMenu(BTD);
              UNK07.setName("UNK07");
              panel2.add(UNK07);
              UNK07.setBounds(170, 225, 74, UNK07.getPreferredSize().height);

              //---- UNK08 ----
              UNK08.setComponentPopupMenu(BTD);
              UNK08.setName("UNK08");
              panel2.add(UNK08);
              UNK08.setBounds(170, 250, 74, UNK08.getPreferredSize().height);

              //---- UNK09 ----
              UNK09.setComponentPopupMenu(BTD);
              UNK09.setName("UNK09");
              panel2.add(UNK09);
              UNK09.setBounds(170, 275, 74, UNK09.getPreferredSize().height);

              //---- UNK10 ----
              UNK10.setComponentPopupMenu(BTD);
              UNK10.setName("UNK10");
              panel2.add(UNK10);
              UNK10.setBounds(170, 300, 74, UNK10.getPreferredSize().height);

              //---- OBJ_105 ----
              OBJ_105.setText("@UL01R@");
              OBJ_105.setName("OBJ_105");
              panel2.add(OBJ_105);
              OBJ_105.setBounds(15, 75, 70, 28);

              //---- OBJ_108 ----
              OBJ_108.setText("@UL02R@");
              OBJ_108.setName("OBJ_108");
              panel2.add(OBJ_108);
              OBJ_108.setBounds(15, 100, 70, 28);

              //---- OBJ_112 ----
              OBJ_112.setText("@UL03R@");
              OBJ_112.setName("OBJ_112");
              panel2.add(OBJ_112);
              OBJ_112.setBounds(15, 125, 70, 28);

              //---- OBJ_116 ----
              OBJ_116.setText("@UL04R@");
              OBJ_116.setName("OBJ_116");
              panel2.add(OBJ_116);
              OBJ_116.setBounds(15, 150, 70, 28);

              //---- OBJ_120 ----
              OBJ_120.setText("@UL05R@");
              OBJ_120.setName("OBJ_120");
              panel2.add(OBJ_120);
              OBJ_120.setBounds(15, 175, 70, 28);

              //---- OBJ_123 ----
              OBJ_123.setText("@UL06R@");
              OBJ_123.setName("OBJ_123");
              panel2.add(OBJ_123);
              OBJ_123.setBounds(15, 200, 70, 28);

              //---- OBJ_127 ----
              OBJ_127.setText("@UL07R@");
              OBJ_127.setName("OBJ_127");
              panel2.add(OBJ_127);
              OBJ_127.setBounds(15, 225, 70, 28);

              //---- OBJ_132 ----
              OBJ_132.setText("@UL08R@");
              OBJ_132.setName("OBJ_132");
              panel2.add(OBJ_132);
              OBJ_132.setBounds(15, 250, 70, 28);

              //---- OBJ_136 ----
              OBJ_136.setText("@UL09R@");
              OBJ_136.setName("OBJ_136");
              panel2.add(OBJ_136);
              OBJ_136.setBounds(15, 275, 70, 28);

              //---- OBJ_140 ----
              OBJ_140.setText("@UL10R@");
              OBJ_140.setName("OBJ_140");
              panel2.add(OBJ_140);
              OBJ_140.setBounds(15, 300, 70, 28);

              //---- UNU1 ----
              UNU1.setComponentPopupMenu(BTD);
              UNU1.setName("UNU1");
              panel2.add(UNU1);
              UNU1.setBounds(95, 75, 30, UNU1.getPreferredSize().height);

              //---- UNU2 ----
              UNU2.setComponentPopupMenu(BTD);
              UNU2.setName("UNU2");
              panel2.add(UNU2);
              UNU2.setBounds(95, 100, 30, UNU2.getPreferredSize().height);

              //---- UNU3 ----
              UNU3.setComponentPopupMenu(BTD);
              UNU3.setName("UNU3");
              panel2.add(UNU3);
              UNU3.setBounds(95, 125, 30, UNU3.getPreferredSize().height);

              //---- UNU4 ----
              UNU4.setComponentPopupMenu(BTD);
              UNU4.setName("UNU4");
              panel2.add(UNU4);
              UNU4.setBounds(95, 150, 30, UNU4.getPreferredSize().height);

              //---- UNU5 ----
              UNU5.setComponentPopupMenu(BTD);
              UNU5.setName("UNU5");
              panel2.add(UNU5);
              UNU5.setBounds(95, 175, 30, UNU5.getPreferredSize().height);

              //---- UNU6 ----
              UNU6.setComponentPopupMenu(BTD);
              UNU6.setName("UNU6");
              panel2.add(UNU6);
              UNU6.setBounds(95, 200, 30, UNU6.getPreferredSize().height);

              //---- UNU7 ----
              UNU7.setComponentPopupMenu(BTD);
              UNU7.setName("UNU7");
              panel2.add(UNU7);
              UNU7.setBounds(95, 225, 30, UNU7.getPreferredSize().height);

              //---- UNU8 ----
              UNU8.setComponentPopupMenu(BTD);
              UNU8.setName("UNU8");
              panel2.add(UNU8);
              UNU8.setBounds(95, 250, 30, UNU8.getPreferredSize().height);

              //---- UNU9 ----
              UNU9.setComponentPopupMenu(BTD);
              UNU9.setName("UNU9");
              panel2.add(UNU9);
              UNU9.setBounds(95, 275, 30, UNU9.getPreferredSize().height);

              //---- UNU10 ----
              UNU10.setComponentPopupMenu(BTD);
              UNU10.setName("UNU10");
              panel2.add(UNU10);
              UNU10.setBounds(95, 300, 30, UNU10.getPreferredSize().height);

              //---- OBJ_106 ----
              OBJ_106.setText("=");
              OBJ_106.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_106.setName("OBJ_106");
              panel2.add(OBJ_106);
              OBJ_106.setBounds(139, 80, 17, 20);

              //---- OBJ_109 ----
              OBJ_109.setText("=");
              OBJ_109.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_109.setName("OBJ_109");
              panel2.add(OBJ_109);
              OBJ_109.setBounds(139, 105, 17, 20);

              //---- OBJ_113 ----
              OBJ_113.setText("=");
              OBJ_113.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_113.setName("OBJ_113");
              panel2.add(OBJ_113);
              OBJ_113.setBounds(139, 130, 17, 20);

              //---- OBJ_117 ----
              OBJ_117.setText("=");
              OBJ_117.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_117.setName("OBJ_117");
              panel2.add(OBJ_117);
              OBJ_117.setBounds(139, 155, 17, 20);

              //---- OBJ_121 ----
              OBJ_121.setText("=");
              OBJ_121.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_121.setName("OBJ_121");
              panel2.add(OBJ_121);
              OBJ_121.setBounds(139, 180, 17, 20);

              //---- OBJ_124 ----
              OBJ_124.setText("=");
              OBJ_124.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_124.setName("OBJ_124");
              panel2.add(OBJ_124);
              OBJ_124.setBounds(139, 205, 17, 20);

              //---- OBJ_128 ----
              OBJ_128.setText("=");
              OBJ_128.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_128.setName("OBJ_128");
              panel2.add(OBJ_128);
              OBJ_128.setBounds(139, 230, 17, 20);

              //---- OBJ_133 ----
              OBJ_133.setText("=");
              OBJ_133.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_133.setName("OBJ_133");
              panel2.add(OBJ_133);
              OBJ_133.setBounds(139, 255, 17, 20);

              //---- OBJ_137 ----
              OBJ_137.setText("=");
              OBJ_137.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_137.setName("OBJ_137");
              panel2.add(OBJ_137);
              OBJ_137.setBounds(139, 280, 17, 20);

              //---- OBJ_141 ----
              OBJ_141.setText("=");
              OBJ_141.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_141.setName("OBJ_141");
              panel2.add(OBJ_141);
              OBJ_141.setBounds(139, 305, 17, 20);

              //---- OBJ_102 ----
              OBJ_102.setText("1 Unit\u00e9 --> Nombre de");
              OBJ_102.setName("OBJ_102");
              panel2.add(OBJ_102);
              OBJ_102.setBounds(15, 40, 130, 28);

              //---- INDUNU ----
              INDUNU.setText("@INDUNU@");
              INDUNU.setName("INDUNU");
              panel2.add(INDUNU);
              INDUNU.setBounds(170, 40, 30, 28);

              //---- OBJ_61 ----
              OBJ_61.setText("Nombre d'unit\u00e9s dans un ");
              OBJ_61.setName("OBJ_61");
              panel2.add(OBJ_61);
              OBJ_61.setBounds(15, 10, 150, 28);

              //---- INDUN2 ----
              INDUN2.setText("@INDUN2@");
              INDUN2.setName("INDUN2");
              panel2.add(INDUN2);
              INDUN2.setBounds(170, 10, 30, 28);

              //---- UNKUNN ----
              UNKUNN.setComponentPopupMenu(BTD);
              UNKUNN.setName("UNKUNN");
              panel2.add(UNKUNN);
              UNKUNN.setBounds(210, 10, 52, UNKUNN.getPreferredSize().height);
            }
            xTitledPanel8ContentContainer.add(panel2);
            panel2.setBounds(10, 40, 380, 345);

            //======== panel5 ========
            {
              panel5.setBorder(new TitledBorder(""));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- OBJ_143 ----
              OBJ_143.setText("Pas de conditionnement sur \u00e9dition");
              OBJ_143.setName("OBJ_143");
              panel5.add(OBJ_143);
              OBJ_143.setBounds(15, 83, 230, 20);

              //---- OBJ_101 ----
              OBJ_101.setText("Unit\u00e9 r\u00e9f\u00e9rence d'encombrement");
              OBJ_101.setName("OBJ_101");
              panel5.add(OBJ_101);
              OBJ_101.setBounds(15, 109, 230, 20);

              //---- UNCND ----
              UNCND.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "1. Bons",
                "2. Facture",
                "3. Bons et factures",
                "4. Bons avec prix en unit\u00e9 de conditionnement",
                "5. Factures avec prix en unit\u00e9 de conditionnement",
                "6. Bons et factures avec prix en unit\u00e9 de conditionnement",
                "7. = 4 si quantit\u00e9 multiple de conditionnement",
                "8. = 5 si quantit\u00e9 multiple de conditionnement",
                "9. = 6 si quantit\u00e9 multiple de conditionnement",
                "10. Quantit\u00e9 et unit\u00e9 en unit\u00e9 de conditionnement sur bon",
                "11. Quantit\u00e9 et unit\u00e9 en unit\u00e9 de conditionnement sur facture",
                "12. Quantit\u00e9 et unit\u00e9 en unit\u00e9 de conditionnement sur bon et facture",
                "13. Quantit\u00e9 en unit\u00e9 de commande"
              }));
              UNCND.setComponentPopupMenu(BTD);
              UNCND.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UNCND.setName("UNCND");
              panel5.add(UNCND);
              UNCND.setBounds(255, 80, 405, UNCND.getPreferredSize().height);

              //---- OBJ_130 ----
              OBJ_130.setText("Article associ\u00e9");
              OBJ_130.setName("OBJ_130");
              panel5.add(OBJ_130);
              OBJ_130.setBounds(15, 10, 230, 20);

              //---- UNART ----
              UNART.setComponentPopupMenu(BTD);
              UNART.setName("UNART");
              panel5.add(UNART);
              UNART.setBounds(255, 5, 110, UNART.getPreferredSize().height);

              //---- UNETQ ----
              UNETQ.setComponentPopupMenu(BTD);
              UNETQ.setName("UNETQ");
              panel5.add(UNETQ);
              UNETQ.setBounds(255, 30, 28, UNETQ.getPreferredSize().height);

              //---- OBJ_135 ----
              OBJ_135.setText("Nombre d'\u00e9tiquettes transporteur");
              OBJ_135.setName("OBJ_135");
              panel5.add(OBJ_135);
              OBJ_135.setBounds(15, 34, 230, 20);

              //---- UNREFU ----
              UNREFU.setComponentPopupMenu(BTD);
              UNREFU.setName("UNREFU");
              panel5.add(UNREFU);
              UNREFU.setBounds(255, 105, 28, UNREFU.getPreferredSize().height);

              //---- OBJ_139 ----
              OBJ_139.setText("Tare (en KG)");
              OBJ_139.setName("OBJ_139");
              panel5.add(OBJ_139);
              OBJ_139.setBounds(15, 59, 230, 20);

              //---- UNTAR ----
              UNTAR.setComponentPopupMenu(BTD);
              UNTAR.setName("UNTAR");
              panel5.add(UNTAR);
              UNTAR.setBounds(255, 55, 74, UNTAR.getPreferredSize().height);
            }
            xTitledPanel8ContentContainer.add(panel5);
            panel5.setBounds(10, 390, 705, 145);

            //======== panel6 ========
            {
              panel6.setBorder(new TitledBorder(""));
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);

              //---- OBJ_96 ----
              OBJ_96.setText("Syst\u00e8me de mesure");
              OBJ_96.setName("OBJ_96");
              panel6.add(OBJ_96);
              OBJ_96.setBounds(10, 13, 136, 20);

              //---- UNNAT ----
              UNNAT.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "D\u00e9nombrement",
                "Longueur",
                "Surface",
                "Volume",
                "Poids",
                "Temps"
              }));
              UNNAT.setComponentPopupMenu(BTD);
              UNNAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UNNAT.setName("UNNAT");
              panel6.add(UNNAT);
              UNNAT.setBounds(160, 10, 150, UNNAT.getPreferredSize().height);

              //---- OBJ_100 ----
              OBJ_100.setText("Nombre de d\u00e9cimales");
              OBJ_100.setName("OBJ_100");
              panel6.add(OBJ_100);
              OBJ_100.setBounds(10, 48, 138, 20);

              //---- UNDEC ----
              UNDEC.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "n",
                "n,0",
                "n,00",
                "n,000"
              }));
              UNDEC.setComponentPopupMenu(BTD);
              UNDEC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UNDEC.setName("UNDEC");
              panel6.add(UNDEC);
              UNDEC.setBounds(160, 45, 65, UNDEC.getPreferredSize().height);
            }
            xTitledPanel8ContentContainer.add(panel6);
            panel6.setBounds(395, 5, 320, 100);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Dimension en cm"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_126 ----
              OBJ_126.setText("Charge maximum");
              OBJ_126.setName("OBJ_126");
              panel3.add(OBJ_126);
              OBJ_126.setBounds(20, 109, 130, 28);

              //---- OBJ_111 ----
              OBJ_111.setText("Longueur");
              OBJ_111.setName("OBJ_111");
              panel3.add(OBJ_111);
              OBJ_111.setBounds(20, 25, 130, 28);

              //---- OBJ_119 ----
              OBJ_119.setText("Hauteur");
              OBJ_119.setName("OBJ_119");
              panel3.add(OBJ_119);
              OBJ_119.setBounds(20, 81, 130, 28);

              //---- OBJ_115 ----
              OBJ_115.setText("Largeur");
              OBJ_115.setName("OBJ_115");
              panel3.add(OBJ_115);
              OBJ_115.setBounds(20, 53, 130, 28);

              //---- UNCMAN ----
              UNCMAN.setComponentPopupMenu(BTD);
              UNCMAN.setHorizontalAlignment(SwingConstants.RIGHT);
              UNCMAN.setName("UNCMAN");
              panel3.add(UNCMAN);
              UNCMAN.setBounds(160, 109, 44, UNCMAN.getPreferredSize().height);

              //---- UNLONG ----
              UNLONG.setComponentPopupMenu(BTD);
              UNLONG.setHorizontalAlignment(SwingConstants.RIGHT);
              UNLONG.setName("UNLONG");
              panel3.add(UNLONG);
              UNLONG.setBounds(160, 25, 36, UNLONG.getPreferredSize().height);

              //---- UNLARG ----
              UNLARG.setComponentPopupMenu(BTD);
              UNLARG.setHorizontalAlignment(SwingConstants.RIGHT);
              UNLARG.setName("UNLARG");
              panel3.add(UNLARG);
              UNLARG.setBounds(160, 53, 36, UNLARG.getPreferredSize().height);

              //---- UNHAUT ----
              UNHAUT.setComponentPopupMenu(BTD);
              UNHAUT.setHorizontalAlignment(SwingConstants.RIGHT);
              UNHAUT.setName("UNHAUT");
              panel3.add(UNHAUT);
              UNHAUT.setBounds(160, 81, 36, UNHAUT.getPreferredSize().height);
            }
            xTitledPanel8ContentContainer.add(panel3);
            panel3.setBounds(395, 107, 320, 150);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder(""));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- UNCPDS ----
              UNCPDS.setText("Cumul pour poids");
              UNCPDS.setComponentPopupMenu(BTD);
              UNCPDS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UNCPDS.setName("UNCPDS");
              panel4.add(UNCPDS);
              UNCPDS.setBounds(15, 10, 129, 20);

              //---- UNCVOL ----
              UNCVOL.setText("Cumul pour volume");
              UNCVOL.setComponentPopupMenu(BTD);
              UNCVOL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UNCVOL.setName("UNCVOL");
              panel4.add(UNCVOL);
              UNCVOL.setBounds(15, 30, 139, 20);

              //---- UNCCOL ----
              UNCCOL.setText("Cumul pour colis");
              UNCCOL.setComponentPopupMenu(BTD);
              UNCCOL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UNCCOL.setName("UNCCOL");
              panel4.add(UNCCOL);
              UNCCOL.setBounds(15, 50, 123, 20);

              //---- UNCRDM ----
              UNCRDM.setText("Cumul pour rendement (GPM)");
              UNCRDM.setComponentPopupMenu(BTD);
              UNCRDM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UNCRDM.setName("UNCRDM");
              panel4.add(UNCRDM);
              UNCRDM.setBounds(15, 70, 195, 20);

              //---- UNQTD ----
              UNQTD.setText("Quantit\u00e9 \"1\" par d\u00e9faut");
              UNQTD.setComponentPopupMenu(BTD);
              UNQTD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              UNQTD.setName("UNQTD");
              panel4.add(UNQTD);
              UNQTD.setBounds(15, 90, 157, 20);
            }
            xTitledPanel8ContentContainer.add(panel4);
            panel4.setBounds(395, 260, 320, 125);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel8ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel8ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel8ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel8ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel8ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel8, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel8, GroupLayout.PREFERRED_SIZE, 573, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel8;
  private JLabel OBJ_95;
  private XRiTextField UNLIB;
  private JLabel OBJ_97;
  private XRiTextField UNLIB2;
  private JPanel panel2;
  private JLabel OBJ_107;
  private JLabel OBJ_110;
  private JLabel OBJ_114;
  private JLabel OBJ_118;
  private JLabel OBJ_122;
  private JLabel OBJ_125;
  private JLabel OBJ_129;
  private JLabel OBJ_134;
  private JLabel OBJ_138;
  private JLabel OBJ_142;
  private XRiTextField UNK01;
  private XRiTextField UNK02;
  private XRiTextField UNK03;
  private XRiTextField UNK04;
  private XRiTextField UNK05;
  private XRiTextField UNK06;
  private XRiTextField UNK07;
  private XRiTextField UNK08;
  private XRiTextField UNK09;
  private XRiTextField UNK10;
  private JLabel OBJ_105;
  private JLabel OBJ_108;
  private JLabel OBJ_112;
  private JLabel OBJ_116;
  private JLabel OBJ_120;
  private JLabel OBJ_123;
  private JLabel OBJ_127;
  private JLabel OBJ_132;
  private JLabel OBJ_136;
  private JLabel OBJ_140;
  private XRiTextField UNU1;
  private XRiTextField UNU2;
  private XRiTextField UNU3;
  private XRiTextField UNU4;
  private XRiTextField UNU5;
  private XRiTextField UNU6;
  private XRiTextField UNU7;
  private XRiTextField UNU8;
  private XRiTextField UNU9;
  private XRiTextField UNU10;
  private JLabel OBJ_106;
  private JLabel OBJ_109;
  private JLabel OBJ_113;
  private JLabel OBJ_117;
  private JLabel OBJ_121;
  private JLabel OBJ_124;
  private JLabel OBJ_128;
  private JLabel OBJ_133;
  private JLabel OBJ_137;
  private JLabel OBJ_141;
  private JLabel OBJ_102;
  private RiZoneSortie INDUNU;
  private JLabel OBJ_61;
  private RiZoneSortie INDUN2;
  private XRiTextField UNKUNN;
  private JPanel panel5;
  private JLabel OBJ_143;
  private JLabel OBJ_101;
  private XRiComboBox UNCND;
  private JLabel OBJ_130;
  private XRiTextField UNART;
  private XRiTextField UNETQ;
  private JLabel OBJ_135;
  private XRiTextField UNREFU;
  private JLabel OBJ_139;
  private XRiTextField UNTAR;
  private JPanel panel6;
  private JLabel OBJ_96;
  private XRiComboBox UNNAT;
  private JLabel OBJ_100;
  private XRiComboBox UNDEC;
  private JPanel panel3;
  private JLabel OBJ_126;
  private JLabel OBJ_111;
  private JLabel OBJ_119;
  private JLabel OBJ_115;
  private XRiTextField UNCMAN;
  private XRiTextField UNLONG;
  private XRiTextField UNLARG;
  private XRiTextField UNHAUT;
  private JPanel panel4;
  private XRiCheckBox UNCPDS;
  private XRiCheckBox UNCVOL;
  private XRiCheckBox UNCCOL;
  private XRiCheckBox UNCRDM;
  private XRiCheckBox UNQTD;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
