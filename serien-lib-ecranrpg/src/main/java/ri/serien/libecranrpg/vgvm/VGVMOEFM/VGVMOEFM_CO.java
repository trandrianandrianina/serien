
package ri.serien.libecranrpg.vgvm.VGVMOEFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;

/**
 * @author Stéphane Vénéri
 */
public class VGVMOEFM_CO extends SNPanelEcranRPG implements ioFrame {
  
   
  private JButton[] boutons = null;
  private int hauteur = 0;
  private String[] TPENV_Value = new String[] { "", "1", "2" };
  
  public VGVMOEFM_CO(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bt_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TPENV.setValeurs(TPENV_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    B01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO1@")).trim());
    B02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO2@")).trim());
    B03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO3@")).trim());
    B04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO4@")).trim());
    B05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO5@")).trim());
    B06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO6@")).trim());
    B07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO7@")).trim());
    B08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO8@")).trim());
    B09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO9@")).trim());
    B10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO10@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression(" @TBOT@")).trim());
    B11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO11@")).trim());
    B12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO12@")).trim());
    B13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO13@")).trim());
    B14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO14@")).trim());
    B15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO15@")).trim());
    B16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO16@")).trim());
    B17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO17@")).trim());
    B18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO18@")).trim());
    B19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO19@")).trim());
    B20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TBO20@")).trim());
    FAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FAX@")).trim());
    MAIL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAIL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    boutons = new JButton[] { B01, B02, B03, B04, B05, B06, B07, B08, B09, B10, B11, B12, B13, B14, B15, B16, B17, B18, B19, B20 };
    hauteur = 810;
    
    
    
    for (int i = 0; i < boutons.length; i++) {
      boutons[i].setVisible(!lexique.HostFieldGetData("TBO" + (i + 1)).trim().equals(""));
    }
    
    for (int i = 0; i < boutons.length; i++) {
      if (!boutons[i].isVisible()) {
        hauteur -= 30;
      }
    }
    
    this.setPreferredSize(new Dimension(360, hauteur));
    
    if (lexique.HostFieldGetData("FAX").trim().equalsIgnoreCase("")) {
      FAX.setText("Aucun numéro de fax sélectionné");
    }
    if (lexique.HostFieldGetData("MAIL").trim().equalsIgnoreCase("")) {
      MAIL.setText("Aucune adresse mail sélectionnée");
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix d'options"));
    
    bt_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bt_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void B01ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B02ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "2");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B03ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "3");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B04ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "4");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B05ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "5");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B06ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "6");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B07ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "7");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B08ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "8");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B09ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "9");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "A");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B11ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "B");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B12ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "C");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B13ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B14ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "E");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B15ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B16ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "G");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B17ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "H");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B18ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "I");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B19ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "J");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void B20ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ACT", 0, "K");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    B01 = new JButton();
    B02 = new JButton();
    B03 = new JButton();
    B04 = new JButton();
    B05 = new JButton();
    B06 = new JButton();
    B07 = new JButton();
    B08 = new JButton();
    B09 = new JButton();
    B10 = new JButton();
    bt_retour = new JButton();
    label1 = new JLabel();
    B11 = new JButton();
    B12 = new JButton();
    B13 = new JButton();
    B14 = new JButton();
    B15 = new JButton();
    B16 = new JButton();
    B17 = new JButton();
    B18 = new JButton();
    B19 = new JButton();
    B20 = new JButton();
    TPENV = new XRiComboBox();
    riBoutonDetail1 = new SNBoutonDetail();
    FAX = new RiZoneSortie();
    MAIL = new RiZoneSortie();
    label2 = new JLabel();
    label3 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(1, 1));
    setPreferredSize(new Dimension(360, 810));
    setMaximumSize(new Dimension(360, 810));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setBackground(new Color(90, 90, 90));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(90, 90, 90));
        p_contenu.setOpaque(false);
        p_contenu.setName("p_contenu");

        //---- B01 ----
        B01.setText("@TBO1@");
        B01.setHorizontalAlignment(SwingConstants.LEADING);
        B01.setPreferredSize(new Dimension(20, 25));
        B01.setMargin(new Insets(0, 0, 0, 0));
        B01.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B01.setName("B01");
        B01.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B01ActionPerformed(e);
          }
        });

        //---- B02 ----
        B02.setText("@TBO2@");
        B02.setHorizontalAlignment(SwingConstants.LEFT);
        B02.setPreferredSize(new Dimension(20, 25));
        B02.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B02.setName("B02");
        B02.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B02ActionPerformed(e);
          }
        });

        //---- B03 ----
        B03.setText("@TBO3@");
        B03.setHorizontalAlignment(SwingConstants.LEFT);
        B03.setPreferredSize(new Dimension(20, 25));
        B03.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B03.setName("B03");
        B03.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B03ActionPerformed(e);
          }
        });

        //---- B04 ----
        B04.setText("@TBO4@");
        B04.setHorizontalAlignment(SwingConstants.LEFT);
        B04.setPreferredSize(new Dimension(20, 25));
        B04.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B04.setName("B04");
        B04.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B04ActionPerformed(e);
          }
        });

        //---- B05 ----
        B05.setText("@TBO5@");
        B05.setHorizontalAlignment(SwingConstants.LEFT);
        B05.setPreferredSize(new Dimension(20, 25));
        B05.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B05.setName("B05");
        B05.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B05ActionPerformed(e);
          }
        });

        //---- B06 ----
        B06.setText("@TBO6@");
        B06.setHorizontalAlignment(SwingConstants.LEFT);
        B06.setPreferredSize(new Dimension(20, 25));
        B06.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B06.setName("B06");
        B06.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B06ActionPerformed(e);
          }
        });

        //---- B07 ----
        B07.setText("@TBO7@");
        B07.setHorizontalAlignment(SwingConstants.LEFT);
        B07.setPreferredSize(new Dimension(20, 25));
        B07.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B07.setName("B07");
        B07.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B07ActionPerformed(e);
          }
        });

        //---- B08 ----
        B08.setText("@TBO8@");
        B08.setHorizontalAlignment(SwingConstants.LEFT);
        B08.setPreferredSize(new Dimension(20, 25));
        B08.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B08.setName("B08");
        B08.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B08ActionPerformed(e);
          }
        });

        //---- B09 ----
        B09.setText("@TBO9@");
        B09.setHorizontalAlignment(SwingConstants.LEFT);
        B09.setPreferredSize(new Dimension(20, 25));
        B09.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B09.setName("B09");
        B09.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B09ActionPerformed(e);
          }
        });

        //---- B10 ----
        B10.setText("@TBO10@");
        B10.setHorizontalAlignment(SwingConstants.LEFT);
        B10.setPreferredSize(new Dimension(20, 25));
        B10.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B10.setName("B10");
        B10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B10ActionPerformed(e);
          }
        });

        //---- bt_retour ----
        bt_retour.setText("Retour");
        bt_retour.setHorizontalAlignment(SwingConstants.LEFT);
        bt_retour.setPreferredSize(new Dimension(20, 35));
        bt_retour.setFont(bt_retour.getFont().deriveFont(bt_retour.getFont().getStyle() | Font.BOLD));
        bt_retour.setMargin(new Insets(0, 0, 0, 0));
        bt_retour.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_retour.setName("bt_retour");
        bt_retour.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_retourActionPerformed(e);
          }
        });

        //---- label1 ----
        label1.setText(" @TBOT@");
        label1.setFont(new Font(Font.MONOSPACED, Font.BOLD, 15));
        label1.setForeground(Color.white);
        label1.setName("label1");

        //---- B11 ----
        B11.setText("@TBO11@");
        B11.setHorizontalAlignment(SwingConstants.LEFT);
        B11.setPreferredSize(new Dimension(20, 25));
        B11.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B11.setName("B11");
        B11.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B11ActionPerformed(e);
          }
        });

        //---- B12 ----
        B12.setText("@TBO12@");
        B12.setHorizontalAlignment(SwingConstants.LEFT);
        B12.setPreferredSize(new Dimension(20, 25));
        B12.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B12.setName("B12");
        B12.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B12ActionPerformed(e);
          }
        });

        //---- B13 ----
        B13.setHorizontalAlignment(SwingConstants.LEFT);
        B13.setPreferredSize(new Dimension(20, 25));
        B13.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B13.setText("@TBO13@");
        B13.setName("B13");
        B13.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B13ActionPerformed(e);
          }
        });

        //---- B14 ----
        B14.setText("@TBO14@");
        B14.setHorizontalAlignment(SwingConstants.LEFT);
        B14.setPreferredSize(new Dimension(20, 25));
        B14.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B14.setName("B14");
        B14.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B14ActionPerformed(e);
          }
        });

        //---- B15 ----
        B15.setText("@TBO15@");
        B15.setHorizontalAlignment(SwingConstants.LEFT);
        B15.setPreferredSize(new Dimension(20, 25));
        B15.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B15.setName("B15");
        B15.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B15ActionPerformed(e);
          }
        });

        //---- B16 ----
        B16.setText("@TBO16@");
        B16.setHorizontalAlignment(SwingConstants.LEFT);
        B16.setPreferredSize(new Dimension(20, 25));
        B16.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B16.setName("B16");
        B16.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B16ActionPerformed(e);
          }
        });

        //---- B17 ----
        B17.setText("@TBO17@");
        B17.setHorizontalAlignment(SwingConstants.LEFT);
        B17.setPreferredSize(new Dimension(20, 25));
        B17.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B17.setName("B17");
        B17.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B17ActionPerformed(e);
          }
        });

        //---- B18 ----
        B18.setText("@TBO18@");
        B18.setHorizontalAlignment(SwingConstants.LEFT);
        B18.setPreferredSize(new Dimension(20, 25));
        B18.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B18.setName("B18");
        B18.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B18ActionPerformed(e);
          }
        });

        //---- B19 ----
        B19.setText("@TBO19@");
        B19.setHorizontalAlignment(SwingConstants.LEFT);
        B19.setPreferredSize(new Dimension(20, 25));
        B19.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B19.setName("B19");
        B19.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B19ActionPerformed(e);
          }
        });

        //---- B20 ----
        B20.setText("@TBO20@");
        B20.setHorizontalAlignment(SwingConstants.LEFT);
        B20.setPreferredSize(new Dimension(20, 25));
        B20.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        B20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        B20.setName("B20");
        B20.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            B20ActionPerformed(e);
          }
        });

        //---- TPENV ----
        TPENV.setModel(new DefaultComboBoxModel(new String[] {
          "\u00e9dition en PDF ",
          "envoi par mail",
          "envoi par fax"
        }));
        TPENV.setName("TPENV");

        //---- riBoutonDetail1 ----
        riBoutonDetail1.setToolTipText("Choix des coordonn\u00e9es d'envoi");
        riBoutonDetail1.setName("riBoutonDetail1");
        riBoutonDetail1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            riBoutonDetail1ActionPerformed(e);
          }
        });

        //---- FAX ----
        FAX.setText("@FAX@");
        FAX.setName("FAX");

        //---- MAIL ----
        MAIL.setText("@MAIL@");
        MAIL.setName("MAIL");

        //---- label2 ----
        label2.setText("fax");
        label2.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        label2.setForeground(Color.white);
        label2.setName("label2");

        //---- label3 ----
        label3.setText("mail");
        label3.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        label3.setForeground(Color.white);
        label3.setName("label3");

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(TPENV, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
              .addContainerGap())
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(label1, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B01, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B02, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B03, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B04, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B05, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B06, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B07, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B08, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B09, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B10, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B11, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B12, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B13, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B14, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B15, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B16, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B17, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B18, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B19, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(21, 21, 21)
                  .addComponent(B20, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(44, 44, 44)
                      .addComponent(FAX, GroupLayout.PREFERRED_SIZE, 287, GroupLayout.PREFERRED_SIZE))))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGap(44, 44, 44)
                      .addComponent(MAIL, GroupLayout.PREFERRED_SIZE, 287, GroupLayout.PREFERRED_SIZE))
                    .addGroup(p_contenuLayout.createSequentialGroup()
                      .addGroup(p_contenuLayout.createParallelGroup()
                        .addComponent(label3, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                        .addComponent(riBoutonDetail1, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
                      .addGap(151, 151, 151)
                      .addComponent(bt_retour, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)))))
              .addGap(0, 4, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(label1, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(B01, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B02, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B03, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B04, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B05, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B06, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B07, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B08, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B09, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B10, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B11, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B12, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B13, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B14, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B15, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B16, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B17, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B18, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B19, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(B20, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addGap(12, 12, 12)
              .addComponent(TPENV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addComponent(FAX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              .addGap(10, 10, 10)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(1, 1, 1)
                  .addComponent(MAIL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addComponent(label3, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(7, 7, 7)
                  .addComponent(bt_retour, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(18, 18, 18)
                  .addComponent(riBoutonDetail1, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JButton B01;
  private JButton B02;
  private JButton B03;
  private JButton B04;
  private JButton B05;
  private JButton B06;
  private JButton B07;
  private JButton B08;
  private JButton B09;
  private JButton B10;
  private JButton bt_retour;
  private JLabel label1;
  private JButton B11;
  private JButton B12;
  private JButton B13;
  private JButton B14;
  private JButton B15;
  private JButton B16;
  private JButton B17;
  private JButton B18;
  private JButton B19;
  private JButton B20;
  private XRiComboBox TPENV;
  private SNBoutonDetail riBoutonDetail1;
  private RiZoneSortie FAX;
  private RiZoneSortie MAIL;
  private JLabel label2;
  private JLabel label3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
