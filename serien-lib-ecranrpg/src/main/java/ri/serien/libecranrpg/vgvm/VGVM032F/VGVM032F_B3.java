
package ri.serien.libecranrpg.vgvm.VGVM032F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM032F_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM032F_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTNS@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    CLDAT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLDAT2@")).trim());
    OBJ_102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    OBJ_104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TI3@")).trim());
    OBJ_100.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    OBJ_109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    OBJ_111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    CLTNS.setVisible(lexique.isPresent("CLTNS"));
    TCI12.setVisible(lexique.isPresent("TCI12"));
    CLIN9.setVisible(lexique.isPresent("CLIN9"));
    OBJ_85.setVisible(lexique.HostFieldGetData("POSTIT").equalsIgnoreCase("+"));
    CLSUF3.setEnabled(lexique.isPresent("CLSUF3"));
    CLSUF2.setEnabled(lexique.isPresent("CLSUF2"));
    CLSUF1.setEnabled(lexique.isPresent("CLSUF1"));
    OBJ_100.setVisible(lexique.isPresent("WTI1"));
    CLTOP5.setVisible(lexique.isPresent("CLTOP5"));
    OBJ_111.setVisible(lexique.isPresent("WTI5"));
    CLTOP4.setEnabled(lexique.isPresent("CLTOP4"));
    OBJ_109.setVisible(lexique.isPresent("WTI4"));
    CLTOP3.setEnabled(lexique.isPresent("CLTOP3"));
    OBJ_104.setVisible(lexique.isPresent("TI3"));
    CLTOP2.setEnabled(lexique.isPresent("CLTOP2"));
    OBJ_102.setVisible(lexique.isPresent("WTI2"));
    CLTOP1.setEnabled(lexique.isPresent("CLTOP1"));
    OBJ_74.setVisible(!lexique.HostFieldGetData("&L000017").trim().equalsIgnoreCase("="));
    CLCAT.setVisible(lexique.isPresent("CLCAT"));
    CLCOP.setVisible(lexique.isPresent("CLCOP"));
    CLCDP.setVisible(lexique.isPresent("CLCDP"));
    CLAPEN.setVisible(!lexique.HostFieldGetData("&L000017").trim().equalsIgnoreCase("="));
    CLAPEN.setEnabled(lexique.isPresent("CLAPEN"));
    WCIV.setVisible(lexique.isPresent("WCIV"));
    CLNUM3.setEnabled(lexique.isPresent("CLNUM3"));
    CLNUM2.setEnabled(lexique.isPresent("CLNUM2"));
    CLNUM1.setEnabled(lexique.isPresent("CLNUM1"));
    RECAT.setEnabled(lexique.isPresent("RECAT"));
    REPOS.setEnabled(lexique.isPresent("REPOS"));
    OBJ_82.setVisible(lexique.isPresent("CLPAY"));
    CCLIBR.setVisible(lexique.isPresent("CCLIBR"));
    CLT3.setEnabled(lexique.isPresent("CLT3"));
    CLT2.setEnabled(lexique.isPresent("CLT2"));
    CLT1.setEnabled(lexique.isPresent("CLT1"));
    OBJ_49.setVisible(lexique.isPresent("LIBTNS"));
    CLOBS.setEnabled(lexique.isPresent("CLOBS"));
    CLV3.setEnabled(lexique.isPresent("CLV3"));
    CLV2.setEnabled(lexique.isPresent("CLV2"));
    CLV1.setEnabled(lexique.isPresent("CLV1"));
    CLFAX.setVisible(lexique.isPresent("CLFAX"));
    RETEL2.setEnabled(lexique.isPresent("RETEL2"));
    CLTEL.setVisible(lexique.isPresent("CLTEL"));
    CLCLK.setVisible(lexique.isPresent("CLCLK"));
    CLVILR.setVisible(lexique.isPresent("CLVILR"));
    CLPAY.setVisible(lexique.isPresent("CLPAY"));
    CLN3.setEnabled(lexique.isPresent("CLN3"));
    CLN2.setEnabled(lexique.isPresent("CLN2"));
    CLN1.setEnabled(lexique.isPresent("CLN1"));
    RENET1.setEnabled(lexique.isPresent("RENET1"));
    CLLOC.setVisible(lexique.isPresent("CLLOC"));
    CLRUE.setVisible(lexique.isPresent("CLRUE"));
    CLCPL.setVisible(lexique.isPresent("CLCPL"));
    WNOMR.setVisible(lexique.isPresent("WNOMR"));
    CLNOM.setVisible(lexique.isPresent("CLNOM"));
    OBJ_63.setVisible(lexique.isTrue("66"));
    CLDAT2.setVisible(lexique.isTrue("66"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Fiche client @SOUTIT@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDCLI = new XRiTextField();
    INDLIV = new XRiTextField();
    CCGSP = new XRiTextField();
    CLTNS = new XRiTextField();
    p_tete_droite = new JPanel();
    OBJ_49 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    CLNOM = new XRiTextField();
    WNOMR = new XRiTextField();
    CLCPL = new XRiTextField();
    CLRUE = new XRiTextField();
    CLLOC = new XRiTextField();
    CLPAY = new XRiTextField();
    CLVILR = new XRiTextField();
    OBJ_57 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_82 = new JLabel();
    WCIV = new XRiTextField();
    CLCDP = new XRiTextField();
    CLCOP = new XRiTextField();
    OBJ_64 = new JLabel();
    CLDAT2 = new RiZoneSortie();
    panel2 = new JPanel();
    CLCLK = new XRiTextField();
    CCLIBR = new XRiTextField();
    OBJ_58 = new JLabel();
    OBJ_63 = new JLabel();
    CLAPEN = new XRiTextField();
    CLCAT = new XRiTextField();
    OBJ_71 = new JLabel();
    OBJ_74 = new JLabel();
    CLIN9 = new XRiTextField();
    TCI12 = new XRiTextField();
    CLTEL = new XRiTextField();
    RETEL2 = new XRiTextField();
    CLFAX = new XRiTextField();
    REPOS = new XRiTextField();
    RECAT = new XRiTextField();
    OBJ_96 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_86 = new JLabel();
    CLOBS = new XRiTextField();
    OBJ_85 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_97 = new JLabel();
    panel3 = new JPanel();
    RENET1 = new XRiTextField();
    panel4 = new JPanel();
    CLTOP1 = new XRiTextField();
    OBJ_102 = new RiZoneSortie();
    CLTOP2 = new XRiTextField();
    OBJ_104 = new RiZoneSortie();
    CLTOP3 = new XRiTextField();
    OBJ_100 = new RiZoneSortie();
    OBJ_109 = new RiZoneSortie();
    CLTOP4 = new XRiTextField();
    OBJ_111 = new RiZoneSortie();
    CLTOP5 = new XRiTextField();
    panel5 = new JPanel();
    CLN1 = new XRiTextField();
    CLN2 = new XRiTextField();
    CLN3 = new XRiTextField();
    CLV1 = new XRiTextField();
    CLV2 = new XRiTextField();
    CLV3 = new XRiTextField();
    CLT1 = new XRiTextField();
    CLT2 = new XRiTextField();
    CLT3 = new XRiTextField();
    CLNUM1 = new XRiTextField();
    CLNUM2 = new XRiTextField();
    CLNUM3 = new XRiTextField();
    CLSUF1 = new XRiTextField();
    CLSUF2 = new XRiTextField();
    CLSUF3 = new XRiTextField();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Client");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTDA);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Client");
          OBJ_44.setName("OBJ_44");

          //---- INDCLI ----
          INDCLI.setComponentPopupMenu(BTDA);
          INDCLI.setName("INDCLI");

          //---- INDLIV ----
          INDLIV.setToolTipText("Suffixe de livraison");
          INDLIV.setComponentPopupMenu(BTDA);
          INDLIV.setName("INDLIV");

          //---- CCGSP ----
          CCGSP.setComponentPopupMenu(BTDA);
          CCGSP.setName("CCGSP");

          //---- CLTNS ----
          CLTNS.setComponentPopupMenu(BTD);
          CLTNS.setName("CLTNS");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(CCGSP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(CLTNS, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(CCGSP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(CLTNS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_49 ----
          OBJ_49.setText("@LIBTNS@");
          OBJ_49.setName("OBJ_49");
          p_tete_droite.add(OBJ_49);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBackground(new Color(214, 217, 223));
            panel1.setBorder(new TitledBorder("Adresse"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- CLNOM ----
            CLNOM.setComponentPopupMenu(BTDA);
            CLNOM.setName("CLNOM");
            panel1.add(CLNOM);
            CLNOM.setBounds(120, 30, 310, CLNOM.getPreferredSize().height);

            //---- WNOMR ----
            WNOMR.setComponentPopupMenu(BTDA);
            WNOMR.setName("WNOMR");
            panel1.add(WNOMR);
            WNOMR.setBounds(160, 30, 270, WNOMR.getPreferredSize().height);

            //---- CLCPL ----
            CLCPL.setComponentPopupMenu(BTD);
            CLCPL.setName("CLCPL");
            panel1.add(CLCPL);
            CLCPL.setBounds(120, 60, 310, CLCPL.getPreferredSize().height);

            //---- CLRUE ----
            CLRUE.setComponentPopupMenu(BTDA);
            CLRUE.setName("CLRUE");
            panel1.add(CLRUE);
            CLRUE.setBounds(120, 90, 310, CLRUE.getPreferredSize().height);

            //---- CLLOC ----
            CLLOC.setComponentPopupMenu(BTDA);
            CLLOC.setName("CLLOC");
            panel1.add(CLLOC);
            CLLOC.setBounds(120, 120, 310, CLLOC.getPreferredSize().height);

            //---- CLPAY ----
            CLPAY.setComponentPopupMenu(BTDA);
            CLPAY.setName("CLPAY");
            panel1.add(CLPAY);
            CLPAY.setBounds(120, 180, 270, CLPAY.getPreferredSize().height);

            //---- CLVILR ----
            CLVILR.setComponentPopupMenu(BTD);
            CLVILR.setName("CLVILR");
            panel1.add(CLVILR);
            CLVILR.setBounds(180, 150, 250, CLVILR.getPreferredSize().height);

            //---- OBJ_57 ----
            OBJ_57.setText("Nom");
            OBJ_57.setName("OBJ_57");
            panel1.add(OBJ_57);
            OBJ_57.setBounds(25, 34, 85, 20);

            //---- OBJ_72 ----
            OBJ_72.setText("Adresse");
            OBJ_72.setName("OBJ_72");
            panel1.add(OBJ_72);
            OBJ_72.setBounds(25, 109, 85, 20);

            //---- OBJ_75 ----
            OBJ_75.setText("Localit\u00e9");
            OBJ_75.setName("OBJ_75");
            panel1.add(OBJ_75);
            OBJ_75.setBounds(25, 134, 85, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("Code postal");
            OBJ_77.setName("OBJ_77");
            panel1.add(OBJ_77);
            OBJ_77.setBounds(25, 159, 85, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("Pays");
            OBJ_82.setName("OBJ_82");
            panel1.add(OBJ_82);
            OBJ_82.setBounds(25, 184, 85, 20);

            //---- WCIV ----
            WCIV.setComponentPopupMenu(BTDA);
            WCIV.setName("WCIV");
            panel1.add(WCIV);
            WCIV.setBounds(120, 30, 40, WCIV.getPreferredSize().height);

            //---- CLCDP ----
            CLCDP.setComponentPopupMenu(BTD);
            CLCDP.setName("CLCDP");
            panel1.add(CLCDP);
            CLCDP.setBounds(120, 150, 60, CLCDP.getPreferredSize().height);

            //---- CLCOP ----
            CLCOP.setComponentPopupMenu(BTDA);
            CLCOP.setName("CLCOP");
            panel1.add(CLCOP);
            CLCOP.setBounds(390, 180, 40, CLCOP.getPreferredSize().height);

            //---- OBJ_64 ----
            OBJ_64.setText("Identification export/import");
            OBJ_64.setName("OBJ_64");
            panel1.add(OBJ_64);
            OBJ_64.setBounds(25, 212, 310, 20);

            //---- CLDAT2 ----
            CLDAT2.setText("@CLDAT2@");
            CLDAT2.setName("CLDAT2");
            panel1.add(CLDAT2);
            CLDAT2.setBounds(350, 210, 80, CLDAT2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- CLCLK ----
            CLCLK.setComponentPopupMenu(BTDA);
            CLCLK.setName("CLCLK");
            panel2.add(CLCLK);
            CLCLK.setBounds(120, 5, 210, CLCLK.getPreferredSize().height);

            //---- CCLIBR ----
            CCLIBR.setName("CCLIBR");
            panel2.add(CCLIBR);
            CCLIBR.setBounds(165, 35, 116, CCLIBR.getPreferredSize().height);

            //---- OBJ_58 ----
            OBJ_58.setText("Classement");
            OBJ_58.setName("OBJ_58");
            panel2.add(OBJ_58);
            OBJ_58.setBounds(15, 9, 105, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("Cat\u00e9gorie");
            OBJ_63.setName("OBJ_63");
            panel2.add(OBJ_63);
            OBJ_63.setBounds(15, 39, 105, 20);

            //---- CLAPEN ----
            CLAPEN.setComponentPopupMenu(BTD);
            CLAPEN.setName("CLAPEN");
            panel2.add(CLAPEN);
            CLAPEN.setBounds(120, 95, 60, CLAPEN.getPreferredSize().height);

            //---- CLCAT ----
            CLCAT.setComponentPopupMenu(BTD);
            CLCAT.setName("CLCAT");
            panel2.add(CLCAT);
            CLCAT.setBounds(120, 35, 40, CLCAT.getPreferredSize().height);

            //---- OBJ_71 ----
            OBJ_71.setText("E");
            OBJ_71.setName("OBJ_71");
            panel2.add(OBJ_71);
            OBJ_71.setBounds(300, 39, 30, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("APE");
            OBJ_74.setName("OBJ_74");
            panel2.add(OBJ_74);
            OBJ_74.setBounds(15, 99, 105, 20);

            //---- CLIN9 ----
            CLIN9.setComponentPopupMenu(BTDA);
            CLIN9.setName("CLIN9");
            panel2.add(CLIN9);
            CLIN9.setBounds(345, 5, 20, CLIN9.getPreferredSize().height);

            //---- TCI12 ----
            TCI12.setComponentPopupMenu(BTD);
            TCI12.setName("TCI12");
            panel2.add(TCI12);
            TCI12.setBounds(345, 35, 20, TCI12.getPreferredSize().height);

            //---- CLTEL ----
            CLTEL.setToolTipText("Extension de la fiche client");
            CLTEL.setComponentPopupMenu(BTDA);
            CLTEL.setName("CLTEL");
            panel2.add(CLTEL);
            CLTEL.setBounds(120, 155, 210, CLTEL.getPreferredSize().height);

            //---- RETEL2 ----
            RETEL2.setComponentPopupMenu(BTD);
            RETEL2.setName("RETEL2");
            panel2.add(RETEL2);
            RETEL2.setBounds(120, 185, 210, RETEL2.getPreferredSize().height);

            //---- CLFAX ----
            CLFAX.setToolTipText("Num\u00e9ro de fax");
            CLFAX.setComponentPopupMenu(BTDA);
            CLFAX.setName("CLFAX");
            panel2.add(CLFAX);
            CLFAX.setBounds(120, 215, 210, CLFAX.getPreferredSize().height);

            //---- REPOS ----
            REPOS.setComponentPopupMenu(BTD);
            REPOS.setName("REPOS");
            panel2.add(REPOS);
            REPOS.setBounds(120, 125, 60, REPOS.getPreferredSize().height);

            //---- RECAT ----
            RECAT.setComponentPopupMenu(BTD);
            RECAT.setName("RECAT");
            panel2.add(RECAT);
            RECAT.setBounds(290, 125, 40, RECAT.getPreferredSize().height);

            //---- OBJ_96 ----
            OBJ_96.setText("Poste");
            OBJ_96.setName("OBJ_96");
            panel2.add(OBJ_96);
            OBJ_96.setBounds(15, 129, 105, 20);

            //---- OBJ_98 ----
            OBJ_98.setText("Fnx");
            OBJ_98.setName("OBJ_98");
            panel2.add(OBJ_98);
            OBJ_98.setBounds(245, 129, 27, 20);

            //---- OBJ_86 ----
            OBJ_86.setText("Observation");
            OBJ_86.setName("OBJ_86");
            panel2.add(OBJ_86);
            OBJ_86.setBounds(15, 69, 105, 20);

            //---- CLOBS ----
            CLOBS.setComponentPopupMenu(BTD);
            CLOBS.setName("CLOBS");
            panel2.add(CLOBS);
            CLOBS.setBounds(120, 65, 210, CLOBS.getPreferredSize().height);

            //---- OBJ_85 ----
            OBJ_85.setIcon(new ImageIcon("images/rouge2.gif"));
            OBJ_85.setToolTipText("M\u00e9mo");
            OBJ_85.setName("OBJ_85");
            panel2.add(OBJ_85);
            OBJ_85.setBounds(345, 70, 19, 20);

            //---- OBJ_99 ----
            OBJ_99.setText("Fax");
            OBJ_99.setName("OBJ_99");
            panel2.add(OBJ_99);
            OBJ_99.setBounds(15, 219, 105, 20);

            //---- OBJ_97 ----
            OBJ_97.setText("T\u00e9l\u00e9phone");
            OBJ_97.setName("OBJ_97");
            panel2.add(OBJ_97);
            OBJ_97.setBounds(15, 159, 105, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setBorder(new TitledBorder("Email"));
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- RENET1 ----
            RENET1.setComponentPopupMenu(BTD);
            RENET1.setName("RENET1");
            panel3.add(RENET1);
            RENET1.setBounds(15, 30, 670, RENET1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setBorder(new TitledBorder("Zones personalisables"));
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- CLTOP1 ----
            CLTOP1.setComponentPopupMenu(BTD);
            CLTOP1.setName("CLTOP1");
            panel4.add(CLTOP1);
            CLTOP1.setBounds(50, 33, 30, CLTOP1.getPreferredSize().height);

            //---- OBJ_102 ----
            OBJ_102.setText("@WTI2@");
            OBJ_102.setName("OBJ_102");
            panel4.add(OBJ_102);
            OBJ_102.setBounds(157, 35, 30, OBJ_102.getPreferredSize().height);

            //---- CLTOP2 ----
            CLTOP2.setComponentPopupMenu(BTD);
            CLTOP2.setName("CLTOP2");
            panel4.add(CLTOP2);
            CLTOP2.setBounds(195, 33, 30, 28);

            //---- OBJ_104 ----
            OBJ_104.setText("@TI3@");
            OBJ_104.setName("OBJ_104");
            panel4.add(OBJ_104);
            OBJ_104.setBounds(299, 35, 30, OBJ_104.getPreferredSize().height);

            //---- CLTOP3 ----
            CLTOP3.setComponentPopupMenu(BTD);
            CLTOP3.setName("CLTOP3");
            panel4.add(CLTOP3);
            CLTOP3.setBounds(335, 33, 30, 28);

            //---- OBJ_100 ----
            OBJ_100.setText("@WTI1@");
            OBJ_100.setName("OBJ_100");
            panel4.add(OBJ_100);
            OBJ_100.setBounds(15, 35, 30, OBJ_100.getPreferredSize().height);

            //---- OBJ_109 ----
            OBJ_109.setText("@WTI4@");
            OBJ_109.setName("OBJ_109");
            panel4.add(OBJ_109);
            OBJ_109.setBounds(441, 35, 30, OBJ_109.getPreferredSize().height);

            //---- CLTOP4 ----
            CLTOP4.setComponentPopupMenu(BTD);
            CLTOP4.setName("CLTOP4");
            panel4.add(CLTOP4);
            CLTOP4.setBounds(480, 33, 30, 28);

            //---- OBJ_111 ----
            OBJ_111.setText("@WTI5@");
            OBJ_111.setName("OBJ_111");
            panel4.add(OBJ_111);
            OBJ_111.setBounds(590, 35, 30, OBJ_111.getPreferredSize().height);

            //---- CLTOP5 ----
            CLTOP5.setComponentPopupMenu(BTD);
            CLTOP5.setName("CLTOP5");
            panel4.add(CLTOP5);
            CLTOP5.setBounds(625, 33, 30, 28);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setBorder(new TitledBorder(""));
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- CLN1 ----
            CLN1.setComponentPopupMenu(BTD);
            CLN1.setName("CLN1");
            panel5.add(CLN1);
            CLN1.setBounds(115, 15, 250, CLN1.getPreferredSize().height);

            //---- CLN2 ----
            CLN2.setComponentPopupMenu(BTD);
            CLN2.setName("CLN2");
            panel5.add(CLN2);
            CLN2.setBounds(115, 45, 250, CLN2.getPreferredSize().height);

            //---- CLN3 ----
            CLN3.setComponentPopupMenu(BTD);
            CLN3.setName("CLN3");
            panel5.add(CLN3);
            CLN3.setBounds(115, 75, 250, CLN3.getPreferredSize().height);

            //---- CLV1 ----
            CLV1.setComponentPopupMenu(BTD);
            CLV1.setName("CLV1");
            panel5.add(CLV1);
            CLV1.setBounds(380, 15, 161, CLV1.getPreferredSize().height);

            //---- CLV2 ----
            CLV2.setComponentPopupMenu(BTD);
            CLV2.setName("CLV2");
            panel5.add(CLV2);
            CLV2.setBounds(380, 45, 161, CLV2.getPreferredSize().height);

            //---- CLV3 ----
            CLV3.setComponentPopupMenu(BTD);
            CLV3.setName("CLV3");
            panel5.add(CLV3);
            CLV3.setBounds(380, 75, 161, CLV3.getPreferredSize().height);

            //---- CLT1 ----
            CLT1.setComponentPopupMenu(BTD);
            CLT1.setName("CLT1");
            panel5.add(CLT1);
            CLT1.setBounds(550, 15, 108, CLT1.getPreferredSize().height);

            //---- CLT2 ----
            CLT2.setComponentPopupMenu(BTD);
            CLT2.setName("CLT2");
            panel5.add(CLT2);
            CLT2.setBounds(550, 45, 108, CLT2.getPreferredSize().height);

            //---- CLT3 ----
            CLT3.setComponentPopupMenu(BTD);
            CLT3.setName("CLT3");
            panel5.add(CLT3);
            CLT3.setBounds(550, 75, 108, CLT3.getPreferredSize().height);

            //---- CLNUM1 ----
            CLNUM1.setComponentPopupMenu(BTD);
            CLNUM1.setName("CLNUM1");
            panel5.add(CLNUM1);
            CLNUM1.setBounds(15, 15, 52, CLNUM1.getPreferredSize().height);

            //---- CLNUM2 ----
            CLNUM2.setComponentPopupMenu(BTD);
            CLNUM2.setName("CLNUM2");
            panel5.add(CLNUM2);
            CLNUM2.setBounds(15, 45, 52, CLNUM2.getPreferredSize().height);

            //---- CLNUM3 ----
            CLNUM3.setComponentPopupMenu(BTD);
            CLNUM3.setName("CLNUM3");
            panel5.add(CLNUM3);
            CLNUM3.setBounds(15, 75, 52, CLNUM3.getPreferredSize().height);

            //---- CLSUF1 ----
            CLSUF1.setComponentPopupMenu(BTD);
            CLSUF1.setName("CLSUF1");
            panel5.add(CLSUF1);
            CLSUF1.setBounds(80, 15, 24, CLSUF1.getPreferredSize().height);

            //---- CLSUF2 ----
            CLSUF2.setComponentPopupMenu(BTD);
            CLSUF2.setName("CLSUF2");
            panel5.add(CLSUF2);
            CLSUF2.setBounds(80, 45, 24, CLSUF2.getPreferredSize().height);

            //---- CLSUF3 ----
            CLSUF3.setComponentPopupMenu(BTD);
            CLSUF3.setName("CLSUF3");
            panel5.add(CLSUF3);
            CLSUF3.setBounds(80, 75, 24, CLSUF3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 876, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel4, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addGroup(GroupLayout.Alignment.LEADING, p_contenuLayout.createSequentialGroup()
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 481, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                  .addComponent(panel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel5, GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDCLI;
  private XRiTextField INDLIV;
  private XRiTextField CCGSP;
  private XRiTextField CLTNS;
  private JPanel p_tete_droite;
  private JLabel OBJ_49;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField CLNOM;
  private XRiTextField WNOMR;
  private XRiTextField CLCPL;
  private XRiTextField CLRUE;
  private XRiTextField CLLOC;
  private XRiTextField CLPAY;
  private XRiTextField CLVILR;
  private JLabel OBJ_57;
  private JLabel OBJ_72;
  private JLabel OBJ_75;
  private JLabel OBJ_77;
  private JLabel OBJ_82;
  private XRiTextField WCIV;
  private XRiTextField CLCDP;
  private XRiTextField CLCOP;
  private JLabel OBJ_64;
  private RiZoneSortie CLDAT2;
  private JPanel panel2;
  private XRiTextField CLCLK;
  private XRiTextField CCLIBR;
  private JLabel OBJ_58;
  private JLabel OBJ_63;
  private XRiTextField CLAPEN;
  private XRiTextField CLCAT;
  private JLabel OBJ_71;
  private JLabel OBJ_74;
  private XRiTextField CLIN9;
  private XRiTextField TCI12;
  private XRiTextField CLTEL;
  private XRiTextField RETEL2;
  private XRiTextField CLFAX;
  private XRiTextField REPOS;
  private XRiTextField RECAT;
  private JLabel OBJ_96;
  private JLabel OBJ_98;
  private JLabel OBJ_86;
  private XRiTextField CLOBS;
  private JLabel OBJ_85;
  private JLabel OBJ_99;
  private JLabel OBJ_97;
  private JPanel panel3;
  private XRiTextField RENET1;
  private JPanel panel4;
  private XRiTextField CLTOP1;
  private RiZoneSortie OBJ_102;
  private XRiTextField CLTOP2;
  private RiZoneSortie OBJ_104;
  private XRiTextField CLTOP3;
  private RiZoneSortie OBJ_100;
  private RiZoneSortie OBJ_109;
  private XRiTextField CLTOP4;
  private RiZoneSortie OBJ_111;
  private XRiTextField CLTOP5;
  private JPanel panel5;
  private XRiTextField CLN1;
  private XRiTextField CLN2;
  private XRiTextField CLN3;
  private XRiTextField CLV1;
  private XRiTextField CLV2;
  private XRiTextField CLV3;
  private XRiTextField CLT1;
  private XRiTextField CLT2;
  private XRiTextField CLT3;
  private XRiTextField CLNUM1;
  private XRiTextField CLNUM2;
  private XRiTextField CLNUM3;
  private XRiTextField CLSUF1;
  private XRiTextField CLSUF2;
  private XRiTextField CLSUF3;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
