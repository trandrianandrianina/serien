
package ri.serien.libecranrpg.vgvm.VGVM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM14FM_LD extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _LDEZ_Title = { "LDET", };
  private String[][] _LDEZ_Data = { { "LDEZ", }, };
  private int[] _LDEZ_Width = { 555, };
  private String[] _WTP01_Title = { "TETLD", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 558, };
  
  public VGVM14FM_LD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    LDEZ.setAspectTable(null, _LDEZ_Title, _LDEZ_Data, _LDEZ_Width, false, null, null, null, null);
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITRE@ @SOUTIT@")).trim());
    P14NUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P14NUM@")).trim());
    E1NFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NFA@")).trim());
    P14ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P14ETB@")).trim());
    E1SUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1SUF@")).trim());
    OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCPL@")).trim());
    OBJ_83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLTEL@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTRI@")).trim());
    OBJ_94.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DBUT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST2, LIST2.get_LIST_Title_Data_Brut(), _WTP01_Top);
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(),null);
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    E1SUF.setVisible(lexique.isPresent("E1SUF"));
    P14ETB.setVisible(lexique.isPresent("P14ETB"));
    WNLIS.setVisible(!lexique.HostFieldGetData("SOUTIT").trim().equalsIgnoreCase("(Multi-bons)"));
    WNLIS.setEnabled(lexique.isPresent("WNLIS"));
    P14NUM.setVisible(lexique.isPresent("P14NUM"));
    E1NFA.setVisible(lexique.isPresent("E1NFA"));
    OBJ_46.setVisible(!lexique.HostFieldGetData("TITRE").trim().equalsIgnoreCase("COMMANDE MULTIPLE"));
    OBJ_44.setVisible(lexique.HostFieldGetData("TITRE").equalsIgnoreCase("COMMANDE MULTIPLE"));
    OBJ_42.setVisible(!lexique.HostFieldGetData("TITRE").trim().equalsIgnoreCase("COMMANDE MULTIPLE"));
    WARGC.setVisible(lexique.isPresent("WARGC"));
    OBJ_96.setVisible(lexique.isPresent("WARGC"));
    OBJ_87.setVisible(lexique.isPresent("LIBTRI"));
    OBJ_86.setVisible(OBJ_87.isVisible());
    OBJ_93.setVisible(!lexique.HostFieldGetData("SOUTIT").trim().equalsIgnoreCase("(Multi-bons)"));
    OBJ_81.setVisible(lexique.isPresent("CLCPL"));
    OBJ_48.setVisible(lexique.isPresent("CLNOM"));
    OBJ_32.setVisible(!lexique.HostFieldGetData("TITRE").trim().equalsIgnoreCase("COMMANDE MULTIPLE"));
    OBJ_31.setVisible(!lexique.HostFieldGetData("TITRE").trim().equalsIgnoreCase("COMMANDE MULTIPLE"));
    OBJ_30.setVisible(lexique.HostFieldGetData("SOUTIT").equalsIgnoreCase("(Multi-bons)"));
    OBJ_29.setVisible(lexique.HostFieldGetData("SOUTIT").equalsIgnoreCase("(Multi-bons)"));
    OBJ_11.setVisible(!lexique.HostFieldGetData("SOUTIT").trim().equalsIgnoreCase("(Multi-bons)"));
    OBJ_9.setVisible(!lexique.HostFieldGetData("SOUTIT").trim().equalsIgnoreCase("(Multi-bons)"));
    
    p_bpresentation.setCodeEtablissement(P14ETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(P14ETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void VALActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "3", "Enter");
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "6", "Enter");
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "9", "Enter");
    WTP01.setValeurTop("9");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    
    // lexique.validSelection(LIST2, _WTP01_Top, "O", "Enter");
    WTP01.setValeurTop("O");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    
    // lexique.validSelection(LIST2, _WTP01_Top, "X", "Enter");
    WTP01.setValeurTop("X");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
    
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_46ActionPerformed(ActionEvent e) {
    
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST2, _WTP01_Top, "2", "Enter",e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void OBJ_42ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // Scriptcall ("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_52 = new JLabel();
    P14NUM = new RiZoneSortie();
    E1NFA = new RiZoneSortie();
    P14ETB = new RiZoneSortie();
    OBJ_51 = new JLabel();
    E1SUF = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    OBJ_48 = new RiZoneSortie();
    OBJ_81 = new RiZoneSortie();
    OBJ_83 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    panel3 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    WTP01 = new XRiTable();
    SCROLLPANE_LIST = new JScrollPane();
    LDEZ = new XRiTable();
    OBJ_93 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_86 = new JLabel();
    WNLIS = new XRiTextField();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_96 = new JLabel();
    WARGC = new XRiTextField();
    BTD = new JPopupMenu();
    VAL = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    OBJ_32 = new JMenuItem();
    OBJ_94 = new JLabel();
    OBJ_8 = new JMenuItem();
    label4 = new JLabel();
    OBJ_9 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_44 = new JButton();
    OBJ_46 = new JButton();
    OBJ_42 = new JButton();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_30 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(1100, 710));
    setPreferredSize(new Dimension(1100, 710));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITRE@ @SOUTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_52 ----
          OBJ_52.setText("Num\u00e9ro");
          OBJ_52.setName("OBJ_52");

          //---- P14NUM ----
          P14NUM.setComponentPopupMenu(BTD);
          P14NUM.setOpaque(false);
          P14NUM.setText("@P14NUM@");
          P14NUM.setName("P14NUM");

          //---- E1NFA ----
          E1NFA.setComponentPopupMenu(null);
          E1NFA.setText("@E1NFA@");
          E1NFA.setOpaque(false);
          E1NFA.setName("E1NFA");

          //---- P14ETB ----
          P14ETB.setComponentPopupMenu(null);
          P14ETB.setOpaque(false);
          P14ETB.setText("@P14ETB@");
          P14ETB.setName("P14ETB");

          //---- OBJ_51 ----
          OBJ_51.setText("Etablissement");
          OBJ_51.setName("OBJ_51");

          //---- E1SUF ----
          E1SUF.setComponentPopupMenu(null);
          E1SUF.setOpaque(false);
          E1SUF.setText("@E1SUF@");
          E1SUF.setName("E1SUF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_51)
                .addGap(10, 10, 10)
                .addComponent(P14ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_52)
                .addGap(10, 10, 10)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(E1NFA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(P14NUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(E1SUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(P14ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(E1NFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(P14NUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(E1SUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 300));
            menus_haut.setPreferredSize(new Dimension(160, 300));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Autres vues");
              riSousMenu_bt6.setToolTipText("Autres vues");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Lignes non stock ");
              riSousMenu_bt7.setToolTipText("Lignes non stock ");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Tri pour affichage");
              riSousMenu_bt9.setToolTipText("Tri pour affichage");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Affichage en-t\u00eate");
              riSousMenu_bt10.setToolTipText("Affichage en-t\u00eate");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(740, 610));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(740, 610));
          p_contenu.setName("p_contenu");

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder("Client"));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OBJ_48 ----
            OBJ_48.setText("@CLNOM@");
            OBJ_48.setFont(OBJ_48.getFont().deriveFont(OBJ_48.getFont().getStyle() | Font.BOLD));
            OBJ_48.setName("OBJ_48");
            panel4.add(OBJ_48);
            OBJ_48.setBounds(120, 35, 310, OBJ_48.getPreferredSize().height);

            //---- OBJ_81 ----
            OBJ_81.setText("@CLCPL@");
            OBJ_81.setName("OBJ_81");
            panel4.add(OBJ_81);
            OBJ_81.setBounds(120, 65, 310, OBJ_81.getPreferredSize().height);

            //---- OBJ_83 ----
            OBJ_83.setText("@CLTEL@");
            OBJ_83.setName("OBJ_83");
            panel4.add(OBJ_83);
            OBJ_83.setBounds(120, 95, 210, OBJ_83.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Nom");
            label1.setName("label1");
            panel4.add(label1);
            label1.setBounds(35, 37, 80, 20);

            //---- label2 ----
            label2.setText("Compl\u00e9ment ");
            label2.setName("label2");
            panel4.add(label2);
            label2.setBounds(35, 67, 80, 20);

            //---- label3 ----
            label3.setText("T\u00e9l\u00e9phone");
            label3.setName("label3");
            panel4.add(label3);
            label3.setBounds(35, 97, 75, 21);
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Affichage"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST2.setViewportView(WTP01);
            }
            panel3.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(35, 90, 558, 270);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- LDEZ ----
              LDEZ.setName("LDEZ");
              SCROLLPANE_LIST.setViewportView(LDEZ);
            }
            panel3.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(35, 40, 558, 45);

            //---- OBJ_93 ----
            OBJ_93.setText("Num\u00e9ro de la premi\u00e8re ligne \u00e0 afficher");
            OBJ_93.setName("OBJ_93");
            panel3.add(OBJ_93);
            OBJ_93.setBounds(80, 370, 237, 18);

            //---- OBJ_87 ----
            OBJ_87.setText("@LIBTRI@");
            OBJ_87.setName("OBJ_87");
            panel3.add(OBJ_87);
            OBJ_87.setBounds(180, -1, 187, 20);

            //---- OBJ_86 ----
            OBJ_86.setText("Tri\u00e9 par");
            OBJ_86.setName("OBJ_86");
            panel3.add(OBJ_86);
            OBJ_86.setBounds(120, -1, 55, 20);

            //---- WNLIS ----
            WNLIS.setName("WNLIS");
            panel3.add(WNLIS);
            WNLIS.setBounds(35, 365, 42, WNLIS.getPreferredSize().height);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel3.add(BT_PGUP);
            BT_PGUP.setBounds(600, 95, 25, 120);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel3.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(600, 235, 25, 120);

            //---- OBJ_96 ----
            OBJ_96.setText("Article / gencod");
            OBJ_96.setName("OBJ_96");
            panel3.add(OBJ_96);
            OBJ_96.setBounds(355, 369, 96, 20);

            //---- WARGC ----
            WARGC.setComponentPopupMenu(BTD);
            WARGC.setName("WARGC");
            panel3.add(WARGC);
            WARGC.setBounds(455, 365, 140, WARGC.getPreferredSize().height);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 660, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 660, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 419, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- VAL ----
      VAL.setText("Affichage");
      VAL.setName("VAL");
      VAL.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          VALActionPerformed(e);
        }
      });
      BTD.add(VAL);
      BTD.addSeparator();

      //---- OBJ_19 ----
      OBJ_19.setText("Fiche article");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_20 ----
      OBJ_20.setText("Interrogation article");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("Adresse stock");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_31 ----
      OBJ_31.setText("Options article");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD.add(OBJ_31);

      //---- OBJ_32 ----
      OBJ_32.setText("Lien sur achats");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      BTD.add(OBJ_32);
    }

    //---- OBJ_94 ----
    OBJ_94.setText("@DBUT@");
    OBJ_94.setName("OBJ_94");

    //---- OBJ_8 ----
    OBJ_8.setText("Totalisation");
    OBJ_8.setName("OBJ_8");
    OBJ_8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_8ActionPerformed(e);
      }
    });

    //---- label4 ----
    label4.setText("<- A conditionner");
    label4.setName("label4");

    //---- OBJ_9 ----
    OBJ_9.setText("tri des lignes d'une commande ou d'un devis en attente");
    OBJ_9.setName("OBJ_9");
    OBJ_9.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_9ActionPerformed(e);
      }
    });

    //---- OBJ_11 ----
    OBJ_11.setText("Lignes non affect\u00e9es seules ou non");
    OBJ_11.setName("OBJ_11");
    OBJ_11.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_11ActionPerformed(e);
      }
    });

    //---- OBJ_44 ----
    OBJ_44.setText("");
    OBJ_44.setToolTipText("Recherche article pour cr\u00e9ation ligne");
    OBJ_44.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_44.setPreferredSize(new Dimension(40, 40));
    OBJ_44.setName("OBJ_44");
    OBJ_44.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_44ActionPerformed(e);
      }
    });

    //---- OBJ_46 ----
    OBJ_46.setText("");
    OBJ_46.setToolTipText("Renum\u00e9rotation des lignes");
    OBJ_46.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_46.setPreferredSize(new Dimension(40, 40));
    OBJ_46.setName("OBJ_46");
    OBJ_46.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_46ActionPerformed(e);
      }
    });

    //---- OBJ_42 ----
    OBJ_42.setText("");
    OBJ_42.setToolTipText("Cr\u00e9ation d'un nouveau bon");
    OBJ_42.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_42.setPreferredSize(new Dimension(40, 40));
    OBJ_42.setName("OBJ_42");
    OBJ_42.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_42ActionPerformed(e);
      }
    });

    //---- OBJ_22 ----
    OBJ_22.setText("Suppression");
    OBJ_22.setName("OBJ_22");
    OBJ_22.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_22ActionPerformed(e);
      }
    });

    //---- OBJ_23 ----
    OBJ_23.setText("Traitement ligne");
    OBJ_23.setName("OBJ_23");
    OBJ_23.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_23ActionPerformed(e);
      }
    });

    //---- OBJ_24 ----
    OBJ_24.setText("S\u00e9lection");
    OBJ_24.setName("OBJ_24");
    OBJ_24.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_24ActionPerformed(e);
      }
    });

    //---- OBJ_25 ----
    OBJ_25.setText("Copie");
    OBJ_25.setName("OBJ_25");
    OBJ_25.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_25ActionPerformed(e);
      }
    });

    //---- OBJ_28 ----
    OBJ_28.setText("D\u00e9rogation ou Affaire");
    OBJ_28.setName("OBJ_28");
    OBJ_28.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_28ActionPerformed(e);
      }
    });

    //---- OBJ_29 ----
    OBJ_29.setText("Gestion d'un forfait commentaire avec ligne d'\u00e9cart");
    OBJ_29.setName("OBJ_29");
    OBJ_29.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_29ActionPerformed(e);
      }
    });

    //---- OBJ_30 ----
    OBJ_30.setText("Traitement de groupe de ligne");
    OBJ_30.setName("OBJ_30");
    OBJ_30.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_30ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_52;
  private RiZoneSortie P14NUM;
  private RiZoneSortie E1NFA;
  private RiZoneSortie P14ETB;
  private JLabel OBJ_51;
  private RiZoneSortie E1SUF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel4;
  private RiZoneSortie OBJ_48;
  private RiZoneSortie OBJ_81;
  private RiZoneSortie OBJ_83;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JPanel panel3;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WTP01;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LDEZ;
  private JLabel OBJ_93;
  private JLabel OBJ_87;
  private JLabel OBJ_86;
  private XRiTextField WNLIS;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_96;
  private XRiTextField WARGC;
  private JPopupMenu BTD;
  private JMenuItem VAL;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_31;
  private JMenuItem OBJ_32;
  private JLabel OBJ_94;
  private JMenuItem OBJ_8;
  private JLabel label4;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_11;
  private JButton OBJ_44;
  private JButton OBJ_46;
  private JButton OBJ_42;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_28;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_30;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
