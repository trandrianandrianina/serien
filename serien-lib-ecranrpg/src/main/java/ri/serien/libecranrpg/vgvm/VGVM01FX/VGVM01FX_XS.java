
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_XS extends SNPanelEcranRPG implements ioFrame {
  
  // Boutons
  private static final String BOUTON_MODIFICATION = "Modifier";
  private static final String BOUTON_EDITION = "Editer";
  private static final String BOUTON_HISTORIQUE_MODIFICATIONS = "Voir historique modifications";
  
  private String[] tableValeurRPGEtablissement = new String[25];
  private boolean[] tableValeurRPGTop = new boolean[75];
  private int[] tableValeurRPGSpecificite = new int[75];
  private int nombreEtablissementSaisissable = 0;
  private List<SNPanel> listePanelSaisie = null;
  private List<SNEtablissement> listeComposantEtablissement = null;
  private List<JCheckBox> listeComposantTop = null;
  private List<XRiComboBox> listeComposantSpecificite = null;
  private String[] listeChampEtablissement =
      { "ETB01", "ETB02", "ETB03", "ETB04", "ETB05", "ETB06", "ETB07", "ETB08", "ETB09", "ETB10", "ETB11", "ETB12", "ETB13", "ETB14",
          "ETB15", "ETB16", "ETB17", "ETB18", "ETB19", "ETB20", "ETB21", "ETB22", "ETB23", "ETB24", "ETB25" };
  private boolean isConsultation = true;
  
  public VGVM01FX_XS(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_MODIFICATION, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_EDITION, 'e', true);
    snBarreBouton.ajouterBouton(BOUTON_HISTORIQUE_MODIFICATIONS, 'h', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
    
    pnlScroll.setBackground(SNCharteGraphique.COULEUR_FOND);
    
    // Initialisation de la liste des panels de saisie
    listePanelSaisie = new ArrayList<SNPanel>();
    listePanelSaisie.add(pnlSaisie1);
    listePanelSaisie.add(pnlSaisie2);
    listePanelSaisie.add(pnlSaisie3);
    listePanelSaisie.add(pnlSaisie4);
    listePanelSaisie.add(pnlSaisie5);
    listePanelSaisie.add(pnlSaisie6);
    listePanelSaisie.add(pnlSaisie7);
    listePanelSaisie.add(pnlSaisie8);
    listePanelSaisie.add(pnlSaisie9);
    listePanelSaisie.add(pnlSaisie10);
    listePanelSaisie.add(pnlSaisie11);
    listePanelSaisie.add(pnlSaisie12);
    listePanelSaisie.add(pnlSaisie13);
    listePanelSaisie.add(pnlSaisie14);
    listePanelSaisie.add(pnlSaisie15);
    listePanelSaisie.add(pnlSaisie16);
    listePanelSaisie.add(pnlSaisie17);
    listePanelSaisie.add(pnlSaisie18);
    listePanelSaisie.add(pnlSaisie19);
    listePanelSaisie.add(pnlSaisie20);
    listePanelSaisie.add(pnlSaisie21);
    listePanelSaisie.add(pnlSaisie22);
    listePanelSaisie.add(pnlSaisie23);
    listePanelSaisie.add(pnlSaisie24);
    listePanelSaisie.add(pnlSaisie25);
    
    // Initialisation de la liste des composants établissement
    listeComposantEtablissement = new ArrayList<SNEtablissement>();
    listeComposantEtablissement.add(snEtablissementSaisie1);
    listeComposantEtablissement.add(snEtablissementSaisie2);
    listeComposantEtablissement.add(snEtablissementSaisie3);
    listeComposantEtablissement.add(snEtablissementSaisie4);
    listeComposantEtablissement.add(snEtablissementSaisie5);
    listeComposantEtablissement.add(snEtablissementSaisie6);
    listeComposantEtablissement.add(snEtablissementSaisie7);
    listeComposantEtablissement.add(snEtablissementSaisie8);
    listeComposantEtablissement.add(snEtablissementSaisie9);
    listeComposantEtablissement.add(snEtablissementSaisie10);
    listeComposantEtablissement.add(snEtablissementSaisie11);
    listeComposantEtablissement.add(snEtablissementSaisie12);
    listeComposantEtablissement.add(snEtablissementSaisie13);
    listeComposantEtablissement.add(snEtablissementSaisie14);
    listeComposantEtablissement.add(snEtablissementSaisie15);
    listeComposantEtablissement.add(snEtablissementSaisie16);
    listeComposantEtablissement.add(snEtablissementSaisie17);
    listeComposantEtablissement.add(snEtablissementSaisie18);
    listeComposantEtablissement.add(snEtablissementSaisie19);
    listeComposantEtablissement.add(snEtablissementSaisie20);
    listeComposantEtablissement.add(snEtablissementSaisie21);
    listeComposantEtablissement.add(snEtablissementSaisie22);
    listeComposantEtablissement.add(snEtablissementSaisie23);
    listeComposantEtablissement.add(snEtablissementSaisie24);
    listeComposantEtablissement.add(snEtablissementSaisie25);
    
    // Initialisation de la liste des composants top
    listeComposantTop = new ArrayList<JCheckBox>();
    listeComposantTop.add(T101);
    listeComposantTop.add(T201);
    listeComposantTop.add(T301);
    listeComposantTop.add(T102);
    listeComposantTop.add(T202);
    listeComposantTop.add(T302);
    listeComposantTop.add(T103);
    listeComposantTop.add(T203);
    listeComposantTop.add(T303);
    listeComposantTop.add(T104);
    listeComposantTop.add(T204);
    listeComposantTop.add(T304);
    listeComposantTop.add(T105);
    listeComposantTop.add(T205);
    listeComposantTop.add(T305);
    listeComposantTop.add(T106);
    listeComposantTop.add(T206);
    listeComposantTop.add(T306);
    listeComposantTop.add(T107);
    listeComposantTop.add(T207);
    listeComposantTop.add(T307);
    listeComposantTop.add(T108);
    listeComposantTop.add(T208);
    listeComposantTop.add(T308);
    listeComposantTop.add(T109);
    listeComposantTop.add(T209);
    listeComposantTop.add(T309);
    listeComposantTop.add(T110);
    listeComposantTop.add(T210);
    listeComposantTop.add(T310);
    listeComposantTop.add(T111);
    listeComposantTop.add(T211);
    listeComposantTop.add(T311);
    listeComposantTop.add(T112);
    listeComposantTop.add(T212);
    listeComposantTop.add(T312);
    listeComposantTop.add(T113);
    listeComposantTop.add(T213);
    listeComposantTop.add(T313);
    listeComposantTop.add(T114);
    listeComposantTop.add(T214);
    listeComposantTop.add(T314);
    listeComposantTop.add(T115);
    listeComposantTop.add(T215);
    listeComposantTop.add(T315);
    listeComposantTop.add(T116);
    listeComposantTop.add(T216);
    listeComposantTop.add(T316);
    listeComposantTop.add(T117);
    listeComposantTop.add(T217);
    listeComposantTop.add(T317);
    listeComposantTop.add(T118);
    listeComposantTop.add(T218);
    listeComposantTop.add(T318);
    listeComposantTop.add(T119);
    listeComposantTop.add(T219);
    listeComposantTop.add(T319);
    listeComposantTop.add(T120);
    listeComposantTop.add(T220);
    listeComposantTop.add(T320);
    listeComposantTop.add(T121);
    listeComposantTop.add(T221);
    listeComposantTop.add(T321);
    listeComposantTop.add(T122);
    listeComposantTop.add(T222);
    listeComposantTop.add(T322);
    listeComposantTop.add(T123);
    listeComposantTop.add(T223);
    listeComposantTop.add(T323);
    listeComposantTop.add(T124);
    listeComposantTop.add(T224);
    listeComposantTop.add(T324);
    listeComposantTop.add(T125);
    listeComposantTop.add(T225);
    listeComposantTop.add(T325);
    
    // Initialisation de la liste des composants spécificités
    listeComposantSpecificite = new ArrayList<XRiComboBox>();
    listeComposantSpecificite.add(T401);
    listeComposantSpecificite.add(T402);
    listeComposantSpecificite.add(T403);
    listeComposantSpecificite.add(T404);
    listeComposantSpecificite.add(T405);
    listeComposantSpecificite.add(T406);
    listeComposantSpecificite.add(T407);
    listeComposantSpecificite.add(T408);
    listeComposantSpecificite.add(T409);
    listeComposantSpecificite.add(T410);
    listeComposantSpecificite.add(T411);
    listeComposantSpecificite.add(T412);
    listeComposantSpecificite.add(T413);
    listeComposantSpecificite.add(T414);
    listeComposantSpecificite.add(T415);
    listeComposantSpecificite.add(T416);
    listeComposantSpecificite.add(T417);
    listeComposantSpecificite.add(T418);
    listeComposantSpecificite.add(T419);
    listeComposantSpecificite.add(T420);
    listeComposantSpecificite.add(T421);
    listeComposantSpecificite.add(T422);
    listeComposantSpecificite.add(T423);
    listeComposantSpecificite.add(T424);
    listeComposantSpecificite.add(T425);
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    /**
     * Chargement des données RPG.
     * Les données sont stockées dans 3 champs alphanumériques de 75 caractères de long (MS1, MS2 et MS3)
     */
    for (int i = 0; i < tableValeurRPGEtablissement.length; i++) {
      tableValeurRPGEtablissement[i] = lexique.HostFieldGetData("MS1").substring(i * 3, (i * 3 + 3));
    }
    for (int i = 0; i < tableValeurRPGTop.length; i++) {
      tableValeurRPGTop[i] = !lexique.HostFieldGetData("MS2").substring(i, (i + 1)).equals(" ");
    }
    for (int i = 0; i < tableValeurRPGSpecificite.length; i++) {
      if (!lexique.HostFieldGetData("MS3").substring(i, (i + 1)).trim().equals("")) {
        tableValeurRPGSpecificite[i] = Integer.parseInt(lexique.HostFieldGetData("MS3").substring(i, (i + 1)));
      }
      else {
        tableValeurRPGSpecificite[i] = 0;
      }
    }
    
    // Etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    nombreEtablissementSaisissable = snEtablissement.getListe().size() - 1;
    
    // Visibilité des panels (suivant le nombre d'établissements saisissables)
    for (int i = 0; i < listePanelSaisie.size(); i++) {
      listePanelSaisie.get(i).setVisible(false);
    }
    
    // Mise à jour des établissements
    for (int i = 0; i < nombreEtablissementSaisissable; i++) {
      // Visibilité du panel de saisie
      listePanelSaisie.get(i).setVisible(true);
      // Initialisation du composant établissement
      listeComposantEtablissement.get(i).setSession(getSession());
      listeComposantEtablissement.get(i).charger(false);
      // On retire l'établissement en cours de la liste proposée
      listeComposantEtablissement.get(i).getListe().remove(snEtablissement.getSelection());
      // Si il y a une valeur RPG on la sélectionne dans le composant
      if (!tableValeurRPGEtablissement[i].trim().isEmpty()
          && !tableValeurRPGEtablissement[i].trim().equals(snEtablissement.getCodeSelection())) {
        IdEtablissement idEtablissement = IdEtablissement.getInstance(tableValeurRPGEtablissement[i]);
        listeComposantEtablissement.get(i).setIdSelection(idEtablissement);
      }
      listeComposantEtablissement.get(i).setEnabled(!isConsultation);
    }
    
    // Mise à jour des tops
    for (int i = 0; i < nombreEtablissementSaisissable; i++) {
      int indexTop = i * 3;
      listeComposantTop.get(indexTop).setSelected(tableValeurRPGTop[indexTop]);
      listeComposantTop.get(indexTop + 1).setSelected(tableValeurRPGTop[indexTop + 1]);
      listeComposantTop.get(indexTop + 2).setSelected(tableValeurRPGTop[indexTop + 2]);
      listeComposantTop.get(indexTop).setEnabled(!isConsultation);
      listeComposantTop.get(indexTop + 1).setEnabled(!isConsultation);
      listeComposantTop.get(indexTop + 2).setEnabled(!isConsultation);
    }
    
    // Mise à jour des spécificités
    for (int i = 0; i < nombreEtablissementSaisissable; i++) {
      listeComposantSpecificite.get(i).removeAllItems();
      // Chargement des combos suivant le contexte : ME article, client ou fournisseur
      if (lexique.HostFieldGetData("INDIND").trim().equals("ART") || lexique.HostFieldGetData("INDIND").trim().equals("MEART")) {
        listeComposantSpecificite.get(i).addItem("Pas de spécificité");
        listeComposantSpecificite.get(i).addItem("Article actif non stocké");
        listeComposantSpecificite.get(i).addItem("Article désactivé");
      }
      else if (lexique.HostFieldGetData("INDIND").trim().equals("CLI") || lexique.HostFieldGetData("INDIND").trim().equals("MECLI")) {
        listeComposantSpecificite.get(i).addItem("Pas de spécificité");
        listeComposantSpecificite.get(i).addItem("Client en statut désactivé");
      }
      else if (lexique.HostFieldGetData("INDIND").trim().equals("FRS") || lexique.HostFieldGetData("INDIND").trim().equals("MEFRS")) {
        listeComposantSpecificite.get(i).addItem("Pas de spécificité");
        listeComposantSpecificite.get(i).addItem("Fournisseur en statut désactivé");
      }
      else {
        listeComposantSpecificite.get(i).removeAllItems();
      }
      
      // Sélection
      try {
        int indexSpe = i * 3;
        if (tableValeurRPGSpecificite[indexSpe] > 0) {
          listeComposantSpecificite.get(i).setSelectedIndex(1);
        }
        else if (tableValeurRPGSpecificite[indexSpe + 1] > 0) {
          listeComposantSpecificite.get(i).setSelectedIndex(2);
        }
        else if (tableValeurRPGSpecificite[indexSpe + 2] > 0) {
          listeComposantSpecificite.get(i).setSelectedIndex(3);
        }
      }
      catch (IllegalArgumentException e) {
        listeComposantSpecificite.get(i).setSelectedIndex(0);
      }
      listeComposantSpecificite.get(i).setEnabled(!isConsultation);
    }
    
    rafraichirBoutons();
    
    

    p_bpresentation.setCodeEtablissement(snEtablissement.getIdSelection().getTexte());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    String etablissements = "";
    String tops = "";
    String specificites = "";
    
    /**
     * Reconstitution des données RPG.
     * Les données sont stockées dans 3 champs alphanumériques de 75 caractères de long (MS1, MS2 et MS3)
     */
    // Etablissements
    for (int i = 0; i < listeComposantEtablissement.size(); i++) {
      if (listeComposantEtablissement.get(i).getCodeSelection() == null
          || listeComposantEtablissement.get(i).getCodeSelection().trim().isEmpty()) {
        etablissements += "   ";
      }
      else {
        etablissements += listeComposantEtablissement.get(i).getCodeSelection();
      }
    }
    // Tops
    for (int i = 0; i < listeComposantTop.size(); i++) {
      if (listeComposantTop.get(i).isSelected()) {
        tops += "1";
      }
      else {
        tops += " ";
      }
    }
    // Specificités
    for (int i = 0; i < listeComposantSpecificite.size(); i++) {
      if (listeComposantSpecificite.get(i).getSelectedIndex() <= 0) {
        specificites += "   ";
      }
      else if (listeComposantSpecificite.get(i).getSelectedIndex() == 1) {
        specificites += "1  ";
      }
      else if (listeComposantSpecificite.get(i).getSelectedIndex() == 2) {
        specificites += " 1 ";
      }
      else if (listeComposantSpecificite.get(i).getSelectedIndex() == 3) {
        specificites += "  1";
      }
    }
    
    lexique.HostFieldPutData("MS1", 0, etablissements);
    lexique.HostFieldPutData("MS2", 0, tops);
    lexique.HostFieldPutData("MS3", 0, specificites);
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFICATION)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_EDITION)) {
        lexique.HostScreenSendKey(this, "F9");
      }
      else if (pSNBouton.isBouton(BOUTON_HISTORIQUE_MODIFICATIONS)) {
        lexique.HostScreenSendKey(this, "F15");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_MODIFICATION, isConsultation);
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, isConsultation);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.ANNULER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.FERMER, isConsultation);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlPersonnalisation = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbCodeCategorie = new SNLabelChamp();
    INDIND = new XRiTextField();
    sNPanelTitre1 = new SNPanelTitre();
    scrollPane1 = new JScrollPane();
    pnlScroll = new SNPanel();
    pnlSaisie1 = new SNPanel();
    lbEtablissementSaisie1 = new SNLabelChamp();
    snEtablissementSaisie1 = new SNEtablissement();
    T101 = new JCheckBox();
    T201 = new JCheckBox();
    T301 = new JCheckBox();
    lbSpecificite1 = new SNLabelChamp();
    T401 = new XRiComboBox();
    pnlSaisie2 = new SNPanel();
    lbEtablissementSaisie2 = new SNLabelChamp();
    snEtablissementSaisie2 = new SNEtablissement();
    T102 = new JCheckBox();
    T202 = new JCheckBox();
    T302 = new JCheckBox();
    lbSpecificite2 = new SNLabelChamp();
    T402 = new XRiComboBox();
    pnlSaisie3 = new SNPanel();
    lbEtablissementSaisie3 = new SNLabelChamp();
    snEtablissementSaisie3 = new SNEtablissement();
    T103 = new JCheckBox();
    T203 = new JCheckBox();
    T303 = new JCheckBox();
    lbSpecificite3 = new SNLabelChamp();
    T403 = new XRiComboBox();
    pnlSaisie4 = new SNPanel();
    lbEtablissementSaisie4 = new SNLabelChamp();
    snEtablissementSaisie4 = new SNEtablissement();
    T104 = new JCheckBox();
    T204 = new JCheckBox();
    T304 = new JCheckBox();
    lbSpecificite4 = new SNLabelChamp();
    T404 = new XRiComboBox();
    pnlSaisie5 = new SNPanel();
    lbEtablissementSaisie5 = new SNLabelChamp();
    snEtablissementSaisie5 = new SNEtablissement();
    T105 = new JCheckBox();
    T205 = new JCheckBox();
    T305 = new JCheckBox();
    lbSpecificite5 = new SNLabelChamp();
    T405 = new XRiComboBox();
    pnlSaisie6 = new SNPanel();
    lbEtablissementSaisie6 = new SNLabelChamp();
    snEtablissementSaisie6 = new SNEtablissement();
    T106 = new JCheckBox();
    T206 = new JCheckBox();
    T306 = new JCheckBox();
    lbSpecificite6 = new SNLabelChamp();
    T406 = new XRiComboBox();
    pnlSaisie7 = new SNPanel();
    lbEtablissementSaisie7 = new SNLabelChamp();
    snEtablissementSaisie7 = new SNEtablissement();
    T107 = new JCheckBox();
    T207 = new JCheckBox();
    T307 = new JCheckBox();
    lbSpecificite7 = new SNLabelChamp();
    T407 = new XRiComboBox();
    pnlSaisie8 = new SNPanel();
    lbEtablissementSaisie8 = new SNLabelChamp();
    snEtablissementSaisie8 = new SNEtablissement();
    T108 = new JCheckBox();
    T208 = new JCheckBox();
    T308 = new JCheckBox();
    lbSpecificite8 = new SNLabelChamp();
    T408 = new XRiComboBox();
    pnlSaisie9 = new SNPanel();
    lbEtablissementSaisie9 = new SNLabelChamp();
    snEtablissementSaisie9 = new SNEtablissement();
    T109 = new JCheckBox();
    T209 = new JCheckBox();
    T309 = new JCheckBox();
    lbSpecificite9 = new SNLabelChamp();
    T409 = new XRiComboBox();
    pnlSaisie10 = new SNPanel();
    lbEtablissementSaisie10 = new SNLabelChamp();
    snEtablissementSaisie10 = new SNEtablissement();
    T110 = new JCheckBox();
    T210 = new JCheckBox();
    T310 = new JCheckBox();
    lbSpecificite10 = new SNLabelChamp();
    T410 = new XRiComboBox();
    pnlSaisie11 = new SNPanel();
    lbEtablissementSaisie11 = new SNLabelChamp();
    snEtablissementSaisie11 = new SNEtablissement();
    T111 = new JCheckBox();
    T211 = new JCheckBox();
    T311 = new JCheckBox();
    lbSpecificite11 = new SNLabelChamp();
    T411 = new XRiComboBox();
    pnlSaisie12 = new SNPanel();
    lbEtablissementSaisie12 = new SNLabelChamp();
    snEtablissementSaisie12 = new SNEtablissement();
    T112 = new JCheckBox();
    T212 = new JCheckBox();
    T312 = new JCheckBox();
    lbSpecificite12 = new SNLabelChamp();
    T412 = new XRiComboBox();
    pnlSaisie13 = new SNPanel();
    lbEtablissementSaisie13 = new SNLabelChamp();
    snEtablissementSaisie13 = new SNEtablissement();
    T113 = new JCheckBox();
    T213 = new JCheckBox();
    T313 = new JCheckBox();
    lbSpecificite13 = new SNLabelChamp();
    T413 = new XRiComboBox();
    pnlSaisie14 = new SNPanel();
    lbEtablissementSaisie14 = new SNLabelChamp();
    snEtablissementSaisie14 = new SNEtablissement();
    T114 = new JCheckBox();
    T214 = new JCheckBox();
    T314 = new JCheckBox();
    lbSpecificite14 = new SNLabelChamp();
    T414 = new XRiComboBox();
    pnlSaisie15 = new SNPanel();
    lbEtablissementSaisie15 = new SNLabelChamp();
    snEtablissementSaisie15 = new SNEtablissement();
    T115 = new JCheckBox();
    T215 = new JCheckBox();
    T315 = new JCheckBox();
    lbSpecificite15 = new SNLabelChamp();
    T415 = new XRiComboBox();
    pnlSaisie16 = new SNPanel();
    lbEtablissementSaisie16 = new SNLabelChamp();
    snEtablissementSaisie16 = new SNEtablissement();
    T116 = new JCheckBox();
    T216 = new JCheckBox();
    T316 = new JCheckBox();
    lbSpecificite16 = new SNLabelChamp();
    T416 = new XRiComboBox();
    pnlSaisie17 = new SNPanel();
    lbEtablissementSaisie17 = new SNLabelChamp();
    snEtablissementSaisie17 = new SNEtablissement();
    T117 = new JCheckBox();
    T217 = new JCheckBox();
    T317 = new JCheckBox();
    lbSpecificite17 = new SNLabelChamp();
    T417 = new XRiComboBox();
    pnlSaisie18 = new SNPanel();
    lbEtablissementSaisie18 = new SNLabelChamp();
    snEtablissementSaisie18 = new SNEtablissement();
    T118 = new JCheckBox();
    T218 = new JCheckBox();
    T318 = new JCheckBox();
    lbSpecificite18 = new SNLabelChamp();
    T418 = new XRiComboBox();
    pnlSaisie19 = new SNPanel();
    lbEtablissementSaisie19 = new SNLabelChamp();
    snEtablissementSaisie19 = new SNEtablissement();
    T119 = new JCheckBox();
    T219 = new JCheckBox();
    T319 = new JCheckBox();
    lbSpecificite19 = new SNLabelChamp();
    T419 = new XRiComboBox();
    pnlSaisie20 = new SNPanel();
    lbEtablissementSaisie20 = new SNLabelChamp();
    snEtablissementSaisie20 = new SNEtablissement();
    T120 = new JCheckBox();
    T220 = new JCheckBox();
    T320 = new JCheckBox();
    lbSpecificite20 = new SNLabelChamp();
    T420 = new XRiComboBox();
    pnlSaisie21 = new SNPanel();
    lbEtablissementSaisie21 = new SNLabelChamp();
    snEtablissementSaisie21 = new SNEtablissement();
    T121 = new JCheckBox();
    T221 = new JCheckBox();
    T321 = new JCheckBox();
    lbSpecificite21 = new SNLabelChamp();
    T421 = new XRiComboBox();
    pnlSaisie22 = new SNPanel();
    lbEtablissementSaisie22 = new SNLabelChamp();
    snEtablissementSaisie22 = new SNEtablissement();
    T122 = new JCheckBox();
    T222 = new JCheckBox();
    T322 = new JCheckBox();
    lbSpecificite22 = new SNLabelChamp();
    T422 = new XRiComboBox();
    pnlSaisie23 = new SNPanel();
    lbEtablissementSaisie23 = new SNLabelChamp();
    snEtablissementSaisie23 = new SNEtablissement();
    T123 = new JCheckBox();
    T223 = new JCheckBox();
    T323 = new JCheckBox();
    lbSpecificite23 = new SNLabelChamp();
    T423 = new XRiComboBox();
    pnlSaisie24 = new SNPanel();
    lbEtablissementSaisie24 = new SNLabelChamp();
    snEtablissementSaisie24 = new SNEtablissement();
    T124 = new JCheckBox();
    T224 = new JCheckBox();
    T324 = new JCheckBox();
    lbSpecificite24 = new SNLabelChamp();
    T424 = new XRiComboBox();
    pnlSaisie25 = new SNPanel();
    lbEtablissementSaisie25 = new SNLabelChamp();
    snEtablissementSaisie25 = new SNEtablissement();
    T125 = new JCheckBox();
    T225 = new JCheckBox();
    T325 = new JCheckBox();
    lbSpecificite25 = new SNLabelChamp();
    T425 = new XRiComboBox();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlPersonnalisation ========
      {
        pnlPersonnalisation.setName("pnlPersonnalisation");
        pnlPersonnalisation.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement en cours");
        lbEtablissement.setName("lbEtablissement");
        pnlPersonnalisation.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setMaximumSize(new Dimension(500, 30));
        snEtablissement.setMinimumSize(new Dimension(500, 30));
        snEtablissement.setPreferredSize(new Dimension(500, 30));
        snEtablissement.setName("snEtablissement");
        pnlPersonnalisation.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCodeCategorie ----
        lbCodeCategorie.setText("code");
        lbCodeCategorie.setName("lbCodeCategorie");
        pnlPersonnalisation.add(lbCodeCategorie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- INDIND ----
        INDIND.setPreferredSize(new Dimension(80, 30));
        INDIND.setMinimumSize(new Dimension(80, 30));
        INDIND.setMaximumSize(new Dimension(80, 30));
        INDIND.setFont(new Font("sansserif", Font.PLAIN, 14));
        INDIND.setEnabled(false);
        INDIND.setName("INDIND");
        pnlPersonnalisation.add(INDIND, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlPersonnalisation,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== sNPanelTitre1 ========
      {
        sNPanelTitre1.setTitre(
            "Modes dans lesquels les informations seront dupliqu\u00e9es de l'\u00e9tablissement en cours vers les \u00e9tablissements ci dessous");
        sNPanelTitre1.setName("sNPanelTitre1");
        sNPanelTitre1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelTitre1.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) sNPanelTitre1.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) sNPanelTitre1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanelTitre1.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== scrollPane1 ========
        {
          scrollPane1.setBackground(new Color(239, 239, 222));
          scrollPane1.setName("scrollPane1");
          
          // ======== pnlScroll ========
          {
            pnlScroll.setOpaque(true);
            pnlScroll.setName("pnlScroll");
            pnlScroll.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlScroll.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlScroll.getLayout()).rowHeights =
                new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlScroll.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlScroll.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlSaisie1 ========
            {
              pnlSaisie1.setName("pnlSaisie1");
              pnlSaisie1.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie1.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie1 ----
              lbEtablissementSaisie1.setText("Etablissement");
              lbEtablissementSaisie1.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie1.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie1.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie1.setName("lbEtablissementSaisie1");
              pnlSaisie1.add(lbEtablissementSaisie1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie1 ----
              snEtablissementSaisie1.setName("snEtablissementSaisie1");
              pnlSaisie1.add(snEtablissementSaisie1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T101 ----
              T101.setText("cr\u00e9ation");
              T101.setFont(new Font("sansserif", Font.PLAIN, 14));
              T101.setName("T101");
              pnlSaisie1.add(T101, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T201 ----
              T201.setText("modification");
              T201.setFont(new Font("sansserif", Font.PLAIN, 14));
              T201.setName("T201");
              pnlSaisie1.add(T201, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T301 ----
              T301.setText("annulation");
              T301.setFont(new Font("sansserif", Font.PLAIN, 14));
              T301.setName("T301");
              pnlSaisie1.add(T301, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite1 ----
              lbSpecificite1.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite1.setMaximumSize(new Dimension(100, 30));
              lbSpecificite1.setMinimumSize(new Dimension(100, 30));
              lbSpecificite1.setPreferredSize(new Dimension(100, 30));
              lbSpecificite1.setName("lbSpecificite1");
              pnlSaisie1.add(lbSpecificite1, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T401 ----
              T401.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T401.setFont(new Font("sansserif", Font.PLAIN, 14));
              T401.setName("T401");
              pnlSaisie1.add(T401, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie2 ========
            {
              pnlSaisie2.setName("pnlSaisie2");
              pnlSaisie2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie2.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie2.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie2 ----
              lbEtablissementSaisie2.setText("Etablissement");
              lbEtablissementSaisie2.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie2.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie2.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie2.setName("lbEtablissementSaisie2");
              pnlSaisie2.add(lbEtablissementSaisie2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie2 ----
              snEtablissementSaisie2.setName("snEtablissementSaisie2");
              pnlSaisie2.add(snEtablissementSaisie2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T102 ----
              T102.setText("cr\u00e9ation");
              T102.setFont(new Font("sansserif", Font.PLAIN, 14));
              T102.setName("T102");
              pnlSaisie2.add(T102, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T202 ----
              T202.setText("modification");
              T202.setFont(new Font("sansserif", Font.PLAIN, 14));
              T202.setName("T202");
              pnlSaisie2.add(T202, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T302 ----
              T302.setText("annulation");
              T302.setFont(new Font("sansserif", Font.PLAIN, 14));
              T302.setName("T302");
              pnlSaisie2.add(T302, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite2 ----
              lbSpecificite2.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite2.setMaximumSize(new Dimension(100, 30));
              lbSpecificite2.setMinimumSize(new Dimension(100, 30));
              lbSpecificite2.setPreferredSize(new Dimension(100, 30));
              lbSpecificite2.setName("lbSpecificite2");
              pnlSaisie2.add(lbSpecificite2, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T402 ----
              T402.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T402.setFont(new Font("sansserif", Font.PLAIN, 14));
              T402.setName("T402");
              pnlSaisie2.add(T402, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie3 ========
            {
              pnlSaisie3.setName("pnlSaisie3");
              pnlSaisie3.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie3.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie3.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie3.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie3 ----
              lbEtablissementSaisie3.setText("Etablissement");
              lbEtablissementSaisie3.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie3.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie3.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie3.setName("lbEtablissementSaisie3");
              pnlSaisie3.add(lbEtablissementSaisie3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie3 ----
              snEtablissementSaisie3.setName("snEtablissementSaisie3");
              pnlSaisie3.add(snEtablissementSaisie3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T103 ----
              T103.setText("cr\u00e9ation");
              T103.setFont(new Font("sansserif", Font.PLAIN, 14));
              T103.setName("T103");
              pnlSaisie3.add(T103, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T203 ----
              T203.setText("modification");
              T203.setFont(new Font("sansserif", Font.PLAIN, 14));
              T203.setName("T203");
              pnlSaisie3.add(T203, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T303 ----
              T303.setText("annulation");
              T303.setFont(new Font("sansserif", Font.PLAIN, 14));
              T303.setName("T303");
              pnlSaisie3.add(T303, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite3 ----
              lbSpecificite3.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite3.setMaximumSize(new Dimension(100, 30));
              lbSpecificite3.setMinimumSize(new Dimension(100, 30));
              lbSpecificite3.setPreferredSize(new Dimension(100, 30));
              lbSpecificite3.setName("lbSpecificite3");
              pnlSaisie3.add(lbSpecificite3, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T403 ----
              T403.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T403.setFont(new Font("sansserif", Font.PLAIN, 14));
              T403.setName("T403");
              pnlSaisie3.add(T403, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie4 ========
            {
              pnlSaisie4.setName("pnlSaisie4");
              pnlSaisie4.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie4.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie4.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie4.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie4 ----
              lbEtablissementSaisie4.setText("Etablissement");
              lbEtablissementSaisie4.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie4.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie4.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie4.setName("lbEtablissementSaisie4");
              pnlSaisie4.add(lbEtablissementSaisie4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie4 ----
              snEtablissementSaisie4.setName("snEtablissementSaisie4");
              pnlSaisie4.add(snEtablissementSaisie4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T104 ----
              T104.setText("cr\u00e9ation");
              T104.setFont(new Font("sansserif", Font.PLAIN, 14));
              T104.setName("T104");
              pnlSaisie4.add(T104, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T204 ----
              T204.setText("modification");
              T204.setFont(new Font("sansserif", Font.PLAIN, 14));
              T204.setName("T204");
              pnlSaisie4.add(T204, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T304 ----
              T304.setText("annulation");
              T304.setFont(new Font("sansserif", Font.PLAIN, 14));
              T304.setName("T304");
              pnlSaisie4.add(T304, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite4 ----
              lbSpecificite4.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite4.setMaximumSize(new Dimension(100, 30));
              lbSpecificite4.setMinimumSize(new Dimension(100, 30));
              lbSpecificite4.setPreferredSize(new Dimension(100, 30));
              lbSpecificite4.setName("lbSpecificite4");
              pnlSaisie4.add(lbSpecificite4, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T404 ----
              T404.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T404.setFont(new Font("sansserif", Font.PLAIN, 14));
              T404.setName("T404");
              pnlSaisie4.add(T404, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie5 ========
            {
              pnlSaisie5.setName("pnlSaisie5");
              pnlSaisie5.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie5.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie5.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie5.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie5.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie5 ----
              lbEtablissementSaisie5.setText("Etablissement");
              lbEtablissementSaisie5.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie5.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie5.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie5.setName("lbEtablissementSaisie5");
              pnlSaisie5.add(lbEtablissementSaisie5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie5 ----
              snEtablissementSaisie5.setName("snEtablissementSaisie5");
              pnlSaisie5.add(snEtablissementSaisie5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T105 ----
              T105.setText("cr\u00e9ation");
              T105.setFont(new Font("sansserif", Font.PLAIN, 14));
              T105.setName("T105");
              pnlSaisie5.add(T105, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T205 ----
              T205.setText("modification");
              T205.setFont(new Font("sansserif", Font.PLAIN, 14));
              T205.setName("T205");
              pnlSaisie5.add(T205, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T305 ----
              T305.setText("annulation");
              T305.setFont(new Font("sansserif", Font.PLAIN, 14));
              T305.setName("T305");
              pnlSaisie5.add(T305, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite5 ----
              lbSpecificite5.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite5.setMaximumSize(new Dimension(100, 30));
              lbSpecificite5.setMinimumSize(new Dimension(100, 30));
              lbSpecificite5.setPreferredSize(new Dimension(100, 30));
              lbSpecificite5.setName("lbSpecificite5");
              pnlSaisie5.add(lbSpecificite5, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T405 ----
              T405.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T405.setFont(new Font("sansserif", Font.PLAIN, 14));
              T405.setName("T405");
              pnlSaisie5.add(T405, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie6 ========
            {
              pnlSaisie6.setName("pnlSaisie6");
              pnlSaisie6.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie6.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie6.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie6.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie6.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie6 ----
              lbEtablissementSaisie6.setText("Etablissement");
              lbEtablissementSaisie6.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie6.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie6.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie6.setName("lbEtablissementSaisie6");
              pnlSaisie6.add(lbEtablissementSaisie6, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie6 ----
              snEtablissementSaisie6.setName("snEtablissementSaisie6");
              pnlSaisie6.add(snEtablissementSaisie6, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T106 ----
              T106.setText("cr\u00e9ation");
              T106.setFont(new Font("sansserif", Font.PLAIN, 14));
              T106.setName("T106");
              pnlSaisie6.add(T106, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T206 ----
              T206.setText("modification");
              T206.setFont(new Font("sansserif", Font.PLAIN, 14));
              T206.setName("T206");
              pnlSaisie6.add(T206, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T306 ----
              T306.setText("annulation");
              T306.setFont(new Font("sansserif", Font.PLAIN, 14));
              T306.setName("T306");
              pnlSaisie6.add(T306, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite6 ----
              lbSpecificite6.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite6.setMaximumSize(new Dimension(100, 30));
              lbSpecificite6.setMinimumSize(new Dimension(100, 30));
              lbSpecificite6.setPreferredSize(new Dimension(100, 30));
              lbSpecificite6.setName("lbSpecificite6");
              pnlSaisie6.add(lbSpecificite6, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T406 ----
              T406.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T406.setFont(new Font("sansserif", Font.PLAIN, 14));
              T406.setName("T406");
              pnlSaisie6.add(T406, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie7 ========
            {
              pnlSaisie7.setName("pnlSaisie7");
              pnlSaisie7.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie7.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie7.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie7.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie7.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie7 ----
              lbEtablissementSaisie7.setText("Etablissement");
              lbEtablissementSaisie7.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie7.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie7.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie7.setName("lbEtablissementSaisie7");
              pnlSaisie7.add(lbEtablissementSaisie7, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie7 ----
              snEtablissementSaisie7.setName("snEtablissementSaisie7");
              pnlSaisie7.add(snEtablissementSaisie7, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T107 ----
              T107.setText("cr\u00e9ation");
              T107.setFont(new Font("sansserif", Font.PLAIN, 14));
              T107.setName("T107");
              pnlSaisie7.add(T107, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T207 ----
              T207.setText("modification");
              T207.setFont(new Font("sansserif", Font.PLAIN, 14));
              T207.setName("T207");
              pnlSaisie7.add(T207, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T307 ----
              T307.setText("annulation");
              T307.setFont(new Font("sansserif", Font.PLAIN, 14));
              T307.setName("T307");
              pnlSaisie7.add(T307, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite7 ----
              lbSpecificite7.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite7.setMaximumSize(new Dimension(100, 30));
              lbSpecificite7.setMinimumSize(new Dimension(100, 30));
              lbSpecificite7.setPreferredSize(new Dimension(100, 30));
              lbSpecificite7.setName("lbSpecificite7");
              pnlSaisie7.add(lbSpecificite7, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T407 ----
              T407.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T407.setFont(new Font("sansserif", Font.PLAIN, 14));
              T407.setName("T407");
              pnlSaisie7.add(T407, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie7, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie8 ========
            {
              pnlSaisie8.setName("pnlSaisie8");
              pnlSaisie8.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie8.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie8.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie8.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie8.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie8 ----
              lbEtablissementSaisie8.setText("Etablissement");
              lbEtablissementSaisie8.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie8.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie8.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie8.setName("lbEtablissementSaisie8");
              pnlSaisie8.add(lbEtablissementSaisie8, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie8 ----
              snEtablissementSaisie8.setName("snEtablissementSaisie8");
              pnlSaisie8.add(snEtablissementSaisie8, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T108 ----
              T108.setText("cr\u00e9ation");
              T108.setFont(new Font("sansserif", Font.PLAIN, 14));
              T108.setName("T108");
              pnlSaisie8.add(T108, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T208 ----
              T208.setText("modification");
              T208.setFont(new Font("sansserif", Font.PLAIN, 14));
              T208.setName("T208");
              pnlSaisie8.add(T208, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T308 ----
              T308.setText("annulation");
              T308.setFont(new Font("sansserif", Font.PLAIN, 14));
              T308.setName("T308");
              pnlSaisie8.add(T308, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite8 ----
              lbSpecificite8.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite8.setMaximumSize(new Dimension(100, 30));
              lbSpecificite8.setMinimumSize(new Dimension(100, 30));
              lbSpecificite8.setPreferredSize(new Dimension(100, 30));
              lbSpecificite8.setName("lbSpecificite8");
              pnlSaisie8.add(lbSpecificite8, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T408 ----
              T408.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T408.setFont(new Font("sansserif", Font.PLAIN, 14));
              T408.setName("T408");
              pnlSaisie8.add(T408, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie8, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie9 ========
            {
              pnlSaisie9.setName("pnlSaisie9");
              pnlSaisie9.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie9.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie9.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie9.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie9.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie9 ----
              lbEtablissementSaisie9.setText("Etablissement");
              lbEtablissementSaisie9.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie9.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie9.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie9.setName("lbEtablissementSaisie9");
              pnlSaisie9.add(lbEtablissementSaisie9, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie9 ----
              snEtablissementSaisie9.setName("snEtablissementSaisie9");
              pnlSaisie9.add(snEtablissementSaisie9, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T109 ----
              T109.setText("cr\u00e9ation");
              T109.setFont(new Font("sansserif", Font.PLAIN, 14));
              T109.setName("T109");
              pnlSaisie9.add(T109, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T209 ----
              T209.setText("modification");
              T209.setFont(new Font("sansserif", Font.PLAIN, 14));
              T209.setName("T209");
              pnlSaisie9.add(T209, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T309 ----
              T309.setText("annulation");
              T309.setFont(new Font("sansserif", Font.PLAIN, 14));
              T309.setName("T309");
              pnlSaisie9.add(T309, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite9 ----
              lbSpecificite9.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite9.setMaximumSize(new Dimension(100, 30));
              lbSpecificite9.setMinimumSize(new Dimension(100, 30));
              lbSpecificite9.setPreferredSize(new Dimension(100, 30));
              lbSpecificite9.setName("lbSpecificite9");
              pnlSaisie9.add(lbSpecificite9, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T409 ----
              T409.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T409.setFont(new Font("sansserif", Font.PLAIN, 14));
              T409.setName("T409");
              pnlSaisie9.add(T409, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie9, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie10 ========
            {
              pnlSaisie10.setName("pnlSaisie10");
              pnlSaisie10.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie10.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie10.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie10.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie10.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie10 ----
              lbEtablissementSaisie10.setText("Etablissement");
              lbEtablissementSaisie10.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie10.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie10.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie10.setName("lbEtablissementSaisie10");
              pnlSaisie10.add(lbEtablissementSaisie10, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie10 ----
              snEtablissementSaisie10.setName("snEtablissementSaisie10");
              pnlSaisie10.add(snEtablissementSaisie10, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T110 ----
              T110.setText("cr\u00e9ation");
              T110.setFont(new Font("sansserif", Font.PLAIN, 14));
              T110.setName("T110");
              pnlSaisie10.add(T110, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T210 ----
              T210.setText("modification");
              T210.setFont(new Font("sansserif", Font.PLAIN, 14));
              T210.setName("T210");
              pnlSaisie10.add(T210, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T310 ----
              T310.setText("annulation");
              T310.setFont(new Font("sansserif", Font.PLAIN, 14));
              T310.setName("T310");
              pnlSaisie10.add(T310, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite10 ----
              lbSpecificite10.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite10.setMaximumSize(new Dimension(100, 30));
              lbSpecificite10.setMinimumSize(new Dimension(100, 30));
              lbSpecificite10.setPreferredSize(new Dimension(100, 30));
              lbSpecificite10.setName("lbSpecificite10");
              pnlSaisie10.add(lbSpecificite10, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T410 ----
              T410.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T410.setFont(new Font("sansserif", Font.PLAIN, 14));
              T410.setName("T410");
              pnlSaisie10.add(T410, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie10, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie11 ========
            {
              pnlSaisie11.setName("pnlSaisie11");
              pnlSaisie11.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie11.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie11.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie11.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie11.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie11 ----
              lbEtablissementSaisie11.setText("Etablissement");
              lbEtablissementSaisie11.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie11.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie11.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie11.setName("lbEtablissementSaisie11");
              pnlSaisie11.add(lbEtablissementSaisie11, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie11 ----
              snEtablissementSaisie11.setName("snEtablissementSaisie11");
              pnlSaisie11.add(snEtablissementSaisie11, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T111 ----
              T111.setText("cr\u00e9ation");
              T111.setFont(new Font("sansserif", Font.PLAIN, 14));
              T111.setName("T111");
              pnlSaisie11.add(T111, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T211 ----
              T211.setText("modification");
              T211.setFont(new Font("sansserif", Font.PLAIN, 14));
              T211.setName("T211");
              pnlSaisie11.add(T211, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T311 ----
              T311.setText("annulation");
              T311.setFont(new Font("sansserif", Font.PLAIN, 14));
              T311.setName("T311");
              pnlSaisie11.add(T311, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite11 ----
              lbSpecificite11.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite11.setMaximumSize(new Dimension(100, 30));
              lbSpecificite11.setMinimumSize(new Dimension(100, 30));
              lbSpecificite11.setPreferredSize(new Dimension(100, 30));
              lbSpecificite11.setName("lbSpecificite11");
              pnlSaisie11.add(lbSpecificite11, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T411 ----
              T411.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T411.setFont(new Font("sansserif", Font.PLAIN, 14));
              T411.setName("T411");
              pnlSaisie11.add(T411, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie11, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie12 ========
            {
              pnlSaisie12.setName("pnlSaisie12");
              pnlSaisie12.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie12.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie12.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie12.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie12.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie12 ----
              lbEtablissementSaisie12.setText("Etablissement");
              lbEtablissementSaisie12.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie12.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie12.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie12.setName("lbEtablissementSaisie12");
              pnlSaisie12.add(lbEtablissementSaisie12, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie12 ----
              snEtablissementSaisie12.setName("snEtablissementSaisie12");
              pnlSaisie12.add(snEtablissementSaisie12, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T112 ----
              T112.setText("cr\u00e9ation");
              T112.setFont(new Font("sansserif", Font.PLAIN, 14));
              T112.setName("T112");
              pnlSaisie12.add(T112, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T212 ----
              T212.setText("modification");
              T212.setFont(new Font("sansserif", Font.PLAIN, 14));
              T212.setName("T212");
              pnlSaisie12.add(T212, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T312 ----
              T312.setText("annulation");
              T312.setFont(new Font("sansserif", Font.PLAIN, 14));
              T312.setName("T312");
              pnlSaisie12.add(T312, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite12 ----
              lbSpecificite12.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite12.setMaximumSize(new Dimension(100, 30));
              lbSpecificite12.setMinimumSize(new Dimension(100, 30));
              lbSpecificite12.setPreferredSize(new Dimension(100, 30));
              lbSpecificite12.setName("lbSpecificite12");
              pnlSaisie12.add(lbSpecificite12, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T412 ----
              T412.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T412.setFont(new Font("sansserif", Font.PLAIN, 14));
              T412.setName("T412");
              pnlSaisie12.add(T412, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie12, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie13 ========
            {
              pnlSaisie13.setName("pnlSaisie13");
              pnlSaisie13.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie13.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie13.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie13.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie13.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie13 ----
              lbEtablissementSaisie13.setText("Etablissement");
              lbEtablissementSaisie13.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie13.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie13.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie13.setName("lbEtablissementSaisie13");
              pnlSaisie13.add(lbEtablissementSaisie13, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie13 ----
              snEtablissementSaisie13.setName("snEtablissementSaisie13");
              pnlSaisie13.add(snEtablissementSaisie13, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T113 ----
              T113.setText("cr\u00e9ation");
              T113.setFont(new Font("sansserif", Font.PLAIN, 14));
              T113.setName("T113");
              pnlSaisie13.add(T113, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T213 ----
              T213.setText("modification");
              T213.setFont(new Font("sansserif", Font.PLAIN, 14));
              T213.setName("T213");
              pnlSaisie13.add(T213, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T313 ----
              T313.setText("annulation");
              T313.setFont(new Font("sansserif", Font.PLAIN, 14));
              T313.setName("T313");
              pnlSaisie13.add(T313, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite13 ----
              lbSpecificite13.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite13.setMaximumSize(new Dimension(100, 30));
              lbSpecificite13.setMinimumSize(new Dimension(100, 30));
              lbSpecificite13.setPreferredSize(new Dimension(100, 30));
              lbSpecificite13.setName("lbSpecificite13");
              pnlSaisie13.add(lbSpecificite13, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T413 ----
              T413.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T413.setFont(new Font("sansserif", Font.PLAIN, 14));
              T413.setName("T413");
              pnlSaisie13.add(T413, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie13, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie14 ========
            {
              pnlSaisie14.setName("pnlSaisie14");
              pnlSaisie14.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie14.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie14.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie14.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie14.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie14 ----
              lbEtablissementSaisie14.setText("Etablissement");
              lbEtablissementSaisie14.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie14.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie14.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie14.setName("lbEtablissementSaisie14");
              pnlSaisie14.add(lbEtablissementSaisie14, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie14 ----
              snEtablissementSaisie14.setName("snEtablissementSaisie14");
              pnlSaisie14.add(snEtablissementSaisie14, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T114 ----
              T114.setText("cr\u00e9ation");
              T114.setFont(new Font("sansserif", Font.PLAIN, 14));
              T114.setName("T114");
              pnlSaisie14.add(T114, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T214 ----
              T214.setText("modification");
              T214.setFont(new Font("sansserif", Font.PLAIN, 14));
              T214.setName("T214");
              pnlSaisie14.add(T214, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T314 ----
              T314.setText("annulation");
              T314.setFont(new Font("sansserif", Font.PLAIN, 14));
              T314.setName("T314");
              pnlSaisie14.add(T314, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite14 ----
              lbSpecificite14.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite14.setMaximumSize(new Dimension(100, 30));
              lbSpecificite14.setMinimumSize(new Dimension(100, 30));
              lbSpecificite14.setPreferredSize(new Dimension(100, 30));
              lbSpecificite14.setName("lbSpecificite14");
              pnlSaisie14.add(lbSpecificite14, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T414 ----
              T414.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T414.setFont(new Font("sansserif", Font.PLAIN, 14));
              T414.setName("T414");
              pnlSaisie14.add(T414, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie14, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie15 ========
            {
              pnlSaisie15.setName("pnlSaisie15");
              pnlSaisie15.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie15.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie15.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie15.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie15.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie15 ----
              lbEtablissementSaisie15.setText("Etablissement");
              lbEtablissementSaisie15.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie15.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie15.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie15.setName("lbEtablissementSaisie15");
              pnlSaisie15.add(lbEtablissementSaisie15, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie15 ----
              snEtablissementSaisie15.setName("snEtablissementSaisie15");
              pnlSaisie15.add(snEtablissementSaisie15, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T115 ----
              T115.setText("cr\u00e9ation");
              T115.setFont(new Font("sansserif", Font.PLAIN, 14));
              T115.setName("T115");
              pnlSaisie15.add(T115, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T215 ----
              T215.setText("modification");
              T215.setFont(new Font("sansserif", Font.PLAIN, 14));
              T215.setName("T215");
              pnlSaisie15.add(T215, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T315 ----
              T315.setText("annulation");
              T315.setFont(new Font("sansserif", Font.PLAIN, 14));
              T315.setName("T315");
              pnlSaisie15.add(T315, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite15 ----
              lbSpecificite15.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite15.setMaximumSize(new Dimension(100, 30));
              lbSpecificite15.setMinimumSize(new Dimension(100, 30));
              lbSpecificite15.setPreferredSize(new Dimension(100, 30));
              lbSpecificite15.setName("lbSpecificite15");
              pnlSaisie15.add(lbSpecificite15, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T415 ----
              T415.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T415.setFont(new Font("sansserif", Font.PLAIN, 14));
              T415.setName("T415");
              pnlSaisie15.add(T415, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie15, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie16 ========
            {
              pnlSaisie16.setName("pnlSaisie16");
              pnlSaisie16.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie16.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie16.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie16.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie16.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie16 ----
              lbEtablissementSaisie16.setText("Etablissement");
              lbEtablissementSaisie16.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie16.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie16.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie16.setName("lbEtablissementSaisie16");
              pnlSaisie16.add(lbEtablissementSaisie16, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie16 ----
              snEtablissementSaisie16.setName("snEtablissementSaisie16");
              pnlSaisie16.add(snEtablissementSaisie16, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T116 ----
              T116.setText("cr\u00e9ation");
              T116.setFont(new Font("sansserif", Font.PLAIN, 14));
              T116.setName("T116");
              pnlSaisie16.add(T116, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T216 ----
              T216.setText("modification");
              T216.setFont(new Font("sansserif", Font.PLAIN, 14));
              T216.setName("T216");
              pnlSaisie16.add(T216, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T316 ----
              T316.setText("annulation");
              T316.setFont(new Font("sansserif", Font.PLAIN, 14));
              T316.setName("T316");
              pnlSaisie16.add(T316, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite16 ----
              lbSpecificite16.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite16.setMaximumSize(new Dimension(100, 30));
              lbSpecificite16.setMinimumSize(new Dimension(100, 30));
              lbSpecificite16.setPreferredSize(new Dimension(100, 30));
              lbSpecificite16.setName("lbSpecificite16");
              pnlSaisie16.add(lbSpecificite16, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T416 ----
              T416.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T416.setFont(new Font("sansserif", Font.PLAIN, 14));
              T416.setName("T416");
              pnlSaisie16.add(T416, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie16, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie17 ========
            {
              pnlSaisie17.setName("pnlSaisie17");
              pnlSaisie17.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie17.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie17.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie17.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie17.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie17 ----
              lbEtablissementSaisie17.setText("Etablissement");
              lbEtablissementSaisie17.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie17.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie17.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie17.setName("lbEtablissementSaisie17");
              pnlSaisie17.add(lbEtablissementSaisie17, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie17 ----
              snEtablissementSaisie17.setName("snEtablissementSaisie17");
              pnlSaisie17.add(snEtablissementSaisie17, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T117 ----
              T117.setText("cr\u00e9ation");
              T117.setFont(new Font("sansserif", Font.PLAIN, 14));
              T117.setName("T117");
              pnlSaisie17.add(T117, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T217 ----
              T217.setText("modification");
              T217.setFont(new Font("sansserif", Font.PLAIN, 14));
              T217.setName("T217");
              pnlSaisie17.add(T217, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T317 ----
              T317.setText("annulation");
              T317.setFont(new Font("sansserif", Font.PLAIN, 14));
              T317.setName("T317");
              pnlSaisie17.add(T317, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite17 ----
              lbSpecificite17.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite17.setMaximumSize(new Dimension(100, 30));
              lbSpecificite17.setMinimumSize(new Dimension(100, 30));
              lbSpecificite17.setPreferredSize(new Dimension(100, 30));
              lbSpecificite17.setName("lbSpecificite17");
              pnlSaisie17.add(lbSpecificite17, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T417 ----
              T417.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T417.setFont(new Font("sansserif", Font.PLAIN, 14));
              T417.setName("T417");
              pnlSaisie17.add(T417, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie17, new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie18 ========
            {
              pnlSaisie18.setName("pnlSaisie18");
              pnlSaisie18.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie18.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie18.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie18.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie18.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie18 ----
              lbEtablissementSaisie18.setText("Etablissement");
              lbEtablissementSaisie18.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie18.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie18.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie18.setName("lbEtablissementSaisie18");
              pnlSaisie18.add(lbEtablissementSaisie18, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie18 ----
              snEtablissementSaisie18.setName("snEtablissementSaisie18");
              pnlSaisie18.add(snEtablissementSaisie18, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T118 ----
              T118.setText("cr\u00e9ation");
              T118.setFont(new Font("sansserif", Font.PLAIN, 14));
              T118.setName("T118");
              pnlSaisie18.add(T118, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T218 ----
              T218.setText("modification");
              T218.setFont(new Font("sansserif", Font.PLAIN, 14));
              T218.setName("T218");
              pnlSaisie18.add(T218, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T318 ----
              T318.setText("annulation");
              T318.setFont(new Font("sansserif", Font.PLAIN, 14));
              T318.setName("T318");
              pnlSaisie18.add(T318, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite18 ----
              lbSpecificite18.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite18.setMaximumSize(new Dimension(100, 30));
              lbSpecificite18.setMinimumSize(new Dimension(100, 30));
              lbSpecificite18.setPreferredSize(new Dimension(100, 30));
              lbSpecificite18.setName("lbSpecificite18");
              pnlSaisie18.add(lbSpecificite18, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T418 ----
              T418.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T418.setFont(new Font("sansserif", Font.PLAIN, 14));
              T418.setName("T418");
              pnlSaisie18.add(T418, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie18, new GridBagConstraints(0, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie19 ========
            {
              pnlSaisie19.setName("pnlSaisie19");
              pnlSaisie19.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie19.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie19.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie19.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie19.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie19 ----
              lbEtablissementSaisie19.setText("Etablissement");
              lbEtablissementSaisie19.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie19.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie19.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie19.setName("lbEtablissementSaisie19");
              pnlSaisie19.add(lbEtablissementSaisie19, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie19 ----
              snEtablissementSaisie19.setName("snEtablissementSaisie19");
              pnlSaisie19.add(snEtablissementSaisie19, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T119 ----
              T119.setText("cr\u00e9ation");
              T119.setFont(new Font("sansserif", Font.PLAIN, 14));
              T119.setName("T119");
              pnlSaisie19.add(T119, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T219 ----
              T219.setText("modification");
              T219.setFont(new Font("sansserif", Font.PLAIN, 14));
              T219.setName("T219");
              pnlSaisie19.add(T219, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T319 ----
              T319.setText("annulation");
              T319.setFont(new Font("sansserif", Font.PLAIN, 14));
              T319.setName("T319");
              pnlSaisie19.add(T319, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite19 ----
              lbSpecificite19.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite19.setMaximumSize(new Dimension(100, 30));
              lbSpecificite19.setMinimumSize(new Dimension(100, 30));
              lbSpecificite19.setPreferredSize(new Dimension(100, 30));
              lbSpecificite19.setName("lbSpecificite19");
              pnlSaisie19.add(lbSpecificite19, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T419 ----
              T419.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T419.setFont(new Font("sansserif", Font.PLAIN, 14));
              T419.setName("T419");
              pnlSaisie19.add(T419, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie19, new GridBagConstraints(0, 18, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie20 ========
            {
              pnlSaisie20.setName("pnlSaisie20");
              pnlSaisie20.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie20.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie20.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie20.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie20.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie20 ----
              lbEtablissementSaisie20.setText("Etablissement");
              lbEtablissementSaisie20.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie20.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie20.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie20.setName("lbEtablissementSaisie20");
              pnlSaisie20.add(lbEtablissementSaisie20, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie20 ----
              snEtablissementSaisie20.setName("snEtablissementSaisie20");
              pnlSaisie20.add(snEtablissementSaisie20, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T120 ----
              T120.setText("cr\u00e9ation");
              T120.setFont(new Font("sansserif", Font.PLAIN, 14));
              T120.setName("T120");
              pnlSaisie20.add(T120, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T220 ----
              T220.setText("modification");
              T220.setFont(new Font("sansserif", Font.PLAIN, 14));
              T220.setName("T220");
              pnlSaisie20.add(T220, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T320 ----
              T320.setText("annulation");
              T320.setFont(new Font("sansserif", Font.PLAIN, 14));
              T320.setName("T320");
              pnlSaisie20.add(T320, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite20 ----
              lbSpecificite20.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite20.setMaximumSize(new Dimension(100, 30));
              lbSpecificite20.setMinimumSize(new Dimension(100, 30));
              lbSpecificite20.setPreferredSize(new Dimension(100, 30));
              lbSpecificite20.setName("lbSpecificite20");
              pnlSaisie20.add(lbSpecificite20, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T420 ----
              T420.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T420.setFont(new Font("sansserif", Font.PLAIN, 14));
              T420.setName("T420");
              pnlSaisie20.add(T420, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie20, new GridBagConstraints(0, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie21 ========
            {
              pnlSaisie21.setName("pnlSaisie21");
              pnlSaisie21.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie21.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie21.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie21.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie21.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie21 ----
              lbEtablissementSaisie21.setText("Etablissement");
              lbEtablissementSaisie21.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie21.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie21.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie21.setName("lbEtablissementSaisie21");
              pnlSaisie21.add(lbEtablissementSaisie21, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie21 ----
              snEtablissementSaisie21.setName("snEtablissementSaisie21");
              pnlSaisie21.add(snEtablissementSaisie21, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T121 ----
              T121.setText("cr\u00e9ation");
              T121.setFont(new Font("sansserif", Font.PLAIN, 14));
              T121.setName("T121");
              pnlSaisie21.add(T121, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T221 ----
              T221.setText("modification");
              T221.setFont(new Font("sansserif", Font.PLAIN, 14));
              T221.setName("T221");
              pnlSaisie21.add(T221, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T321 ----
              T321.setText("annulation");
              T321.setFont(new Font("sansserif", Font.PLAIN, 14));
              T321.setName("T321");
              pnlSaisie21.add(T321, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite21 ----
              lbSpecificite21.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite21.setMaximumSize(new Dimension(100, 30));
              lbSpecificite21.setMinimumSize(new Dimension(100, 30));
              lbSpecificite21.setPreferredSize(new Dimension(100, 30));
              lbSpecificite21.setName("lbSpecificite21");
              pnlSaisie21.add(lbSpecificite21, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T421 ----
              T421.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T421.setFont(new Font("sansserif", Font.PLAIN, 14));
              T421.setName("T421");
              pnlSaisie21.add(T421, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie21, new GridBagConstraints(0, 20, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie22 ========
            {
              pnlSaisie22.setName("pnlSaisie22");
              pnlSaisie22.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie22.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie22.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie22.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie22.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie22 ----
              lbEtablissementSaisie22.setText("Etablissement");
              lbEtablissementSaisie22.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie22.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie22.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie22.setName("lbEtablissementSaisie22");
              pnlSaisie22.add(lbEtablissementSaisie22, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie22 ----
              snEtablissementSaisie22.setName("snEtablissementSaisie22");
              pnlSaisie22.add(snEtablissementSaisie22, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T122 ----
              T122.setText("cr\u00e9ation");
              T122.setFont(new Font("sansserif", Font.PLAIN, 14));
              T122.setName("T122");
              pnlSaisie22.add(T122, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T222 ----
              T222.setText("modification");
              T222.setFont(new Font("sansserif", Font.PLAIN, 14));
              T222.setName("T222");
              pnlSaisie22.add(T222, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T322 ----
              T322.setText("annulation");
              T322.setFont(new Font("sansserif", Font.PLAIN, 14));
              T322.setName("T322");
              pnlSaisie22.add(T322, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite22 ----
              lbSpecificite22.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite22.setMaximumSize(new Dimension(100, 30));
              lbSpecificite22.setMinimumSize(new Dimension(100, 30));
              lbSpecificite22.setPreferredSize(new Dimension(100, 30));
              lbSpecificite22.setName("lbSpecificite22");
              pnlSaisie22.add(lbSpecificite22, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T422 ----
              T422.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T422.setFont(new Font("sansserif", Font.PLAIN, 14));
              T422.setName("T422");
              pnlSaisie22.add(T422, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie22, new GridBagConstraints(0, 21, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie23 ========
            {
              pnlSaisie23.setName("pnlSaisie23");
              pnlSaisie23.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie23.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie23.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie23.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie23.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie23 ----
              lbEtablissementSaisie23.setText("Etablissement");
              lbEtablissementSaisie23.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie23.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie23.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie23.setName("lbEtablissementSaisie23");
              pnlSaisie23.add(lbEtablissementSaisie23, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie23 ----
              snEtablissementSaisie23.setName("snEtablissementSaisie23");
              pnlSaisie23.add(snEtablissementSaisie23, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T123 ----
              T123.setText("cr\u00e9ation");
              T123.setFont(new Font("sansserif", Font.PLAIN, 14));
              T123.setName("T123");
              pnlSaisie23.add(T123, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T223 ----
              T223.setText("modification");
              T223.setFont(new Font("sansserif", Font.PLAIN, 14));
              T223.setName("T223");
              pnlSaisie23.add(T223, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T323 ----
              T323.setText("annulation");
              T323.setFont(new Font("sansserif", Font.PLAIN, 14));
              T323.setName("T323");
              pnlSaisie23.add(T323, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite23 ----
              lbSpecificite23.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite23.setMaximumSize(new Dimension(100, 30));
              lbSpecificite23.setMinimumSize(new Dimension(100, 30));
              lbSpecificite23.setPreferredSize(new Dimension(100, 30));
              lbSpecificite23.setName("lbSpecificite23");
              pnlSaisie23.add(lbSpecificite23, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T423 ----
              T423.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T423.setFont(new Font("sansserif", Font.PLAIN, 14));
              T423.setName("T423");
              pnlSaisie23.add(T423, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie23, new GridBagConstraints(0, 22, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie24 ========
            {
              pnlSaisie24.setName("pnlSaisie24");
              pnlSaisie24.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie24.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie24.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie24.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie24.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie24 ----
              lbEtablissementSaisie24.setText("Etablissement");
              lbEtablissementSaisie24.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie24.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie24.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie24.setName("lbEtablissementSaisie24");
              pnlSaisie24.add(lbEtablissementSaisie24, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie24 ----
              snEtablissementSaisie24.setName("snEtablissementSaisie24");
              pnlSaisie24.add(snEtablissementSaisie24, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T124 ----
              T124.setText("cr\u00e9ation");
              T124.setFont(new Font("sansserif", Font.PLAIN, 14));
              T124.setName("T124");
              pnlSaisie24.add(T124, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T224 ----
              T224.setText("modification");
              T224.setFont(new Font("sansserif", Font.PLAIN, 14));
              T224.setName("T224");
              pnlSaisie24.add(T224, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T324 ----
              T324.setText("annulation");
              T324.setFont(new Font("sansserif", Font.PLAIN, 14));
              T324.setName("T324");
              pnlSaisie24.add(T324, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite24 ----
              lbSpecificite24.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite24.setMaximumSize(new Dimension(100, 30));
              lbSpecificite24.setMinimumSize(new Dimension(100, 30));
              lbSpecificite24.setPreferredSize(new Dimension(100, 30));
              lbSpecificite24.setName("lbSpecificite24");
              pnlSaisie24.add(lbSpecificite24, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T424 ----
              T424.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T424.setFont(new Font("sansserif", Font.PLAIN, 14));
              T424.setName("T424");
              pnlSaisie24.add(T424, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie24, new GridBagConstraints(0, 23, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlSaisie25 ========
            {
              pnlSaisie25.setName("pnlSaisie25");
              pnlSaisie25.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSaisie25.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlSaisie25.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSaisie25.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSaisie25.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementSaisie25 ----
              lbEtablissementSaisie25.setText("Etablissement");
              lbEtablissementSaisie25.setMaximumSize(new Dimension(100, 30));
              lbEtablissementSaisie25.setMinimumSize(new Dimension(100, 30));
              lbEtablissementSaisie25.setPreferredSize(new Dimension(100, 30));
              lbEtablissementSaisie25.setName("lbEtablissementSaisie25");
              pnlSaisie25.add(lbEtablissementSaisie25, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementSaisie25 ----
              snEtablissementSaisie25.setName("snEtablissementSaisie25");
              pnlSaisie25.add(snEtablissementSaisie25, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T125 ----
              T125.setText("cr\u00e9ation");
              T125.setFont(new Font("sansserif", Font.PLAIN, 14));
              T125.setName("T125");
              pnlSaisie25.add(T125, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T225 ----
              T225.setText("modification");
              T225.setFont(new Font("sansserif", Font.PLAIN, 14));
              T225.setName("T225");
              pnlSaisie25.add(T225, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T325 ----
              T325.setText("annulation");
              T325.setFont(new Font("sansserif", Font.PLAIN, 14));
              T325.setName("T325");
              pnlSaisie25.add(T325, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSpecificite25 ----
              lbSpecificite25.setText("Sp\u00e9cificit\u00e9");
              lbSpecificite25.setMaximumSize(new Dimension(100, 30));
              lbSpecificite25.setMinimumSize(new Dimension(100, 30));
              lbSpecificite25.setPreferredSize(new Dimension(100, 30));
              lbSpecificite25.setName("lbSpecificite25");
              pnlSaisie25.add(lbSpecificite25, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- T425 ----
              T425.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de sp\u00e9cificit\u00e9", "Article actif non stock\u00e9", "Article d\u00e9sactiv\u00e9" }));
              T425.setFont(new Font("sansserif", Font.PLAIN, 14));
              T425.setName("T425");
              pnlSaisie25.add(T425, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlScroll.add(pnlSaisie25, new GridBagConstraints(0, 24, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          scrollPane1.setViewportView(pnlScroll);
        }
        sNPanelTitre1.add(scrollPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(sNPanelTitre1,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlPersonnalisation;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbCodeCategorie;
  private XRiTextField INDIND;
  private SNPanelTitre sNPanelTitre1;
  private JScrollPane scrollPane1;
  private SNPanel pnlScroll;
  private SNPanel pnlSaisie1;
  private SNLabelChamp lbEtablissementSaisie1;
  private SNEtablissement snEtablissementSaisie1;
  private JCheckBox T101;
  private JCheckBox T201;
  private JCheckBox T301;
  private SNLabelChamp lbSpecificite1;
  private XRiComboBox T401;
  private SNPanel pnlSaisie2;
  private SNLabelChamp lbEtablissementSaisie2;
  private SNEtablissement snEtablissementSaisie2;
  private JCheckBox T102;
  private JCheckBox T202;
  private JCheckBox T302;
  private SNLabelChamp lbSpecificite2;
  private XRiComboBox T402;
  private SNPanel pnlSaisie3;
  private SNLabelChamp lbEtablissementSaisie3;
  private SNEtablissement snEtablissementSaisie3;
  private JCheckBox T103;
  private JCheckBox T203;
  private JCheckBox T303;
  private SNLabelChamp lbSpecificite3;
  private XRiComboBox T403;
  private SNPanel pnlSaisie4;
  private SNLabelChamp lbEtablissementSaisie4;
  private SNEtablissement snEtablissementSaisie4;
  private JCheckBox T104;
  private JCheckBox T204;
  private JCheckBox T304;
  private SNLabelChamp lbSpecificite4;
  private XRiComboBox T404;
  private SNPanel pnlSaisie5;
  private SNLabelChamp lbEtablissementSaisie5;
  private SNEtablissement snEtablissementSaisie5;
  private JCheckBox T105;
  private JCheckBox T205;
  private JCheckBox T305;
  private SNLabelChamp lbSpecificite5;
  private XRiComboBox T405;
  private SNPanel pnlSaisie6;
  private SNLabelChamp lbEtablissementSaisie6;
  private SNEtablissement snEtablissementSaisie6;
  private JCheckBox T106;
  private JCheckBox T206;
  private JCheckBox T306;
  private SNLabelChamp lbSpecificite6;
  private XRiComboBox T406;
  private SNPanel pnlSaisie7;
  private SNLabelChamp lbEtablissementSaisie7;
  private SNEtablissement snEtablissementSaisie7;
  private JCheckBox T107;
  private JCheckBox T207;
  private JCheckBox T307;
  private SNLabelChamp lbSpecificite7;
  private XRiComboBox T407;
  private SNPanel pnlSaisie8;
  private SNLabelChamp lbEtablissementSaisie8;
  private SNEtablissement snEtablissementSaisie8;
  private JCheckBox T108;
  private JCheckBox T208;
  private JCheckBox T308;
  private SNLabelChamp lbSpecificite8;
  private XRiComboBox T408;
  private SNPanel pnlSaisie9;
  private SNLabelChamp lbEtablissementSaisie9;
  private SNEtablissement snEtablissementSaisie9;
  private JCheckBox T109;
  private JCheckBox T209;
  private JCheckBox T309;
  private SNLabelChamp lbSpecificite9;
  private XRiComboBox T409;
  private SNPanel pnlSaisie10;
  private SNLabelChamp lbEtablissementSaisie10;
  private SNEtablissement snEtablissementSaisie10;
  private JCheckBox T110;
  private JCheckBox T210;
  private JCheckBox T310;
  private SNLabelChamp lbSpecificite10;
  private XRiComboBox T410;
  private SNPanel pnlSaisie11;
  private SNLabelChamp lbEtablissementSaisie11;
  private SNEtablissement snEtablissementSaisie11;
  private JCheckBox T111;
  private JCheckBox T211;
  private JCheckBox T311;
  private SNLabelChamp lbSpecificite11;
  private XRiComboBox T411;
  private SNPanel pnlSaisie12;
  private SNLabelChamp lbEtablissementSaisie12;
  private SNEtablissement snEtablissementSaisie12;
  private JCheckBox T112;
  private JCheckBox T212;
  private JCheckBox T312;
  private SNLabelChamp lbSpecificite12;
  private XRiComboBox T412;
  private SNPanel pnlSaisie13;
  private SNLabelChamp lbEtablissementSaisie13;
  private SNEtablissement snEtablissementSaisie13;
  private JCheckBox T113;
  private JCheckBox T213;
  private JCheckBox T313;
  private SNLabelChamp lbSpecificite13;
  private XRiComboBox T413;
  private SNPanel pnlSaisie14;
  private SNLabelChamp lbEtablissementSaisie14;
  private SNEtablissement snEtablissementSaisie14;
  private JCheckBox T114;
  private JCheckBox T214;
  private JCheckBox T314;
  private SNLabelChamp lbSpecificite14;
  private XRiComboBox T414;
  private SNPanel pnlSaisie15;
  private SNLabelChamp lbEtablissementSaisie15;
  private SNEtablissement snEtablissementSaisie15;
  private JCheckBox T115;
  private JCheckBox T215;
  private JCheckBox T315;
  private SNLabelChamp lbSpecificite15;
  private XRiComboBox T415;
  private SNPanel pnlSaisie16;
  private SNLabelChamp lbEtablissementSaisie16;
  private SNEtablissement snEtablissementSaisie16;
  private JCheckBox T116;
  private JCheckBox T216;
  private JCheckBox T316;
  private SNLabelChamp lbSpecificite16;
  private XRiComboBox T416;
  private SNPanel pnlSaisie17;
  private SNLabelChamp lbEtablissementSaisie17;
  private SNEtablissement snEtablissementSaisie17;
  private JCheckBox T117;
  private JCheckBox T217;
  private JCheckBox T317;
  private SNLabelChamp lbSpecificite17;
  private XRiComboBox T417;
  private SNPanel pnlSaisie18;
  private SNLabelChamp lbEtablissementSaisie18;
  private SNEtablissement snEtablissementSaisie18;
  private JCheckBox T118;
  private JCheckBox T218;
  private JCheckBox T318;
  private SNLabelChamp lbSpecificite18;
  private XRiComboBox T418;
  private SNPanel pnlSaisie19;
  private SNLabelChamp lbEtablissementSaisie19;
  private SNEtablissement snEtablissementSaisie19;
  private JCheckBox T119;
  private JCheckBox T219;
  private JCheckBox T319;
  private SNLabelChamp lbSpecificite19;
  private XRiComboBox T419;
  private SNPanel pnlSaisie20;
  private SNLabelChamp lbEtablissementSaisie20;
  private SNEtablissement snEtablissementSaisie20;
  private JCheckBox T120;
  private JCheckBox T220;
  private JCheckBox T320;
  private SNLabelChamp lbSpecificite20;
  private XRiComboBox T420;
  private SNPanel pnlSaisie21;
  private SNLabelChamp lbEtablissementSaisie21;
  private SNEtablissement snEtablissementSaisie21;
  private JCheckBox T121;
  private JCheckBox T221;
  private JCheckBox T321;
  private SNLabelChamp lbSpecificite21;
  private XRiComboBox T421;
  private SNPanel pnlSaisie22;
  private SNLabelChamp lbEtablissementSaisie22;
  private SNEtablissement snEtablissementSaisie22;
  private JCheckBox T122;
  private JCheckBox T222;
  private JCheckBox T322;
  private SNLabelChamp lbSpecificite22;
  private XRiComboBox T422;
  private SNPanel pnlSaisie23;
  private SNLabelChamp lbEtablissementSaisie23;
  private SNEtablissement snEtablissementSaisie23;
  private JCheckBox T123;
  private JCheckBox T223;
  private JCheckBox T323;
  private SNLabelChamp lbSpecificite23;
  private XRiComboBox T423;
  private SNPanel pnlSaisie24;
  private SNLabelChamp lbEtablissementSaisie24;
  private SNEtablissement snEtablissementSaisie24;
  private JCheckBox T124;
  private JCheckBox T224;
  private JCheckBox T324;
  private SNLabelChamp lbSpecificite24;
  private XRiComboBox T424;
  private SNPanel pnlSaisie25;
  private SNLabelChamp lbEtablissementSaisie25;
  private SNEtablissement snEtablissementSaisie25;
  private JCheckBox T125;
  private JCheckBox T225;
  private JCheckBox T325;
  private SNLabelChamp lbSpecificite25;
  private XRiComboBox T425;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
