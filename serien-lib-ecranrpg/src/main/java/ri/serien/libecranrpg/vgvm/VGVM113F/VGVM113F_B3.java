
package ri.serien.libecranrpg.vgvm.VGVM113F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;

/**
 * @author Stéphane Vénéri
 */
public class VGVM113F_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM113F_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WPVB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPVB1@")).trim());
    PA1NBN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA1NBN@")).trim());
    WCLIX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCLIX@")).trim());
    PA2NBN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PA2NBN@")).trim());
    PROMO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PROMO@")).trim());
    WPROMO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPROMO@")).trim());
    WPREF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPREF@")).trim());
    WPFAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPFAC@")).trim());
    WQFAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQFAC@")).trim());
    WPBON.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPBON@")).trim());
    WQBON.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQBON@")).trim());
    A1RTA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1RTA@")).trim());
    WTAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTAR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Derniers prix pratiqués   @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(15, 24);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riBoutonDetail2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(16, 24);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_contenu2 = new JPanel();
    OBJ_36 = new JLabel();
    OBJ_17 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_23 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_11 = new JLabel();
    WPVB1 = new RiZoneSortie();
    PA1NBN = new RiZoneSortie();
    WCLIX = new RiZoneSortie();
    PA2NBN = new RiZoneSortie();
    OBJ_51 = new RiZoneSortie();
    PROMO = new RiZoneSortie();
    WPROMO = new RiZoneSortie();
    WPREF = new RiZoneSortie();
    WPFAC = new RiZoneSortie();
    WQFAC = new RiZoneSortie();
    WPBON = new RiZoneSortie();
    WQBON = new RiZoneSortie();
    OBJ_20 = new JLabel();
    OBJ_26 = new JLabel();
    WDDPRO = new XRiCalendrier();
    WDFPRO = new XRiCalendrier();
    WDDREF = new XRiCalendrier();
    WDFREF = new XRiCalendrier();
    WDFAC = new XRiCalendrier();
    WDBON = new XRiCalendrier();
    OBJ_15 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_49 = new RiZoneSortie();
    OBJ_13 = new JLabel();
    A1RTA = new RiZoneSortie();
    WTAR = new RiZoneSortie();
    OBJ_43 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_47 = new JLabel();
    riBoutonDetail1 = new SNBoutonDetail();
    riBoutonDetail2 = new SNBoutonDetail();
    OBJ_35 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(920, 250));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_contenu2 ========
        {
          p_contenu2.setBorder(new TitledBorder(""));
          p_contenu2.setOpaque(false);
          p_contenu2.setName("p_contenu2");
          p_contenu2.setLayout(null);

          //---- OBJ_36 ----
          OBJ_36.setText("Derni\u00e8re commande");
          OBJ_36.setFont(OBJ_36.getFont().deriveFont(OBJ_36.getFont().getStyle() | Font.BOLD));
          OBJ_36.setName("OBJ_36");
          p_contenu2.add(OBJ_36);
          OBJ_36.setBounds(22, 162, 128, 20);

          //---- OBJ_17 ----
          OBJ_17.setText("Condition promo");
          OBJ_17.setFont(OBJ_17.getFont().deriveFont(OBJ_17.getFont().getStyle() | Font.BOLD));
          OBJ_17.setName("OBJ_17");
          p_contenu2.add(OBJ_17);
          OBJ_17.setBounds(22, 72, 128, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("Dernier bon factur\u00e9");
          OBJ_29.setFont(OBJ_29.getFont().deriveFont(OBJ_29.getFont().getStyle() | Font.BOLD));
          OBJ_29.setName("OBJ_29");
          p_contenu2.add(OBJ_29);
          OBJ_29.setBounds(22, 132, 128, 20);

          //---- OBJ_23 ----
          OBJ_23.setText("R\u00e9f\u00e9rencement");
          OBJ_23.setFont(OBJ_23.getFont().deriveFont(OBJ_23.getFont().getStyle() | Font.BOLD));
          OBJ_23.setName("OBJ_23");
          p_contenu2.add(OBJ_23);
          OBJ_23.setBounds(22, 102, 128, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_contenu2.add(P_PnlOpts);
          P_PnlOpts.setBounds(1034, 15, 55, 516);

          //---- OBJ_11 ----
          OBJ_11.setText("Prix de base");
          OBJ_11.setFont(OBJ_11.getFont().deriveFont(OBJ_11.getFont().getStyle() | Font.BOLD));
          OBJ_11.setName("OBJ_11");
          p_contenu2.add(OBJ_11);
          OBJ_11.setBounds(22, 42, 133, 20);

          //---- WPVB1 ----
          WPVB1.setText("@WPVB1@");
          WPVB1.setBorder(new BevelBorder(BevelBorder.LOWERED));
          WPVB1.setHorizontalAlignment(SwingConstants.RIGHT);
          WPVB1.setName("WPVB1");
          p_contenu2.add(WPVB1);
          WPVB1.setBounds(275, 40, 100, WPVB1.getPreferredSize().height);

          //---- PA1NBN ----
          PA1NBN.setText("@PA1NBN@");
          PA1NBN.setHorizontalAlignment(SwingConstants.RIGHT);
          PA1NBN.setName("PA1NBN");
          p_contenu2.add(PA1NBN);
          PA1NBN.setBounds(155, 160, 83, PA1NBN.getPreferredSize().height);

          //---- WCLIX ----
          WCLIX.setText("@WCLIX@");
          WCLIX.setName("WCLIX");
          p_contenu2.add(WCLIX);
          WCLIX.setBounds(155, 100, 83, WCLIX.getPreferredSize().height);

          //---- PA2NBN ----
          PA2NBN.setText("@PA2NBN@");
          PA2NBN.setHorizontalAlignment(SwingConstants.RIGHT);
          PA2NBN.setName("PA2NBN");
          p_contenu2.add(PA2NBN);
          PA2NBN.setBounds(155, 130, 83, PA2NBN.getPreferredSize().height);

          //---- OBJ_51 ----
          OBJ_51.setText("Aucun bon");
          OBJ_51.setName("OBJ_51");
          p_contenu2.add(OBJ_51);
          OBJ_51.setBounds(155, 130, 81, OBJ_51.getPreferredSize().height);

          //---- PROMO ----
          PROMO.setText("@PROMO@");
          PROMO.setName("PROMO");
          p_contenu2.add(PROMO);
          PROMO.setBounds(155, 70, 76, PROMO.getPreferredSize().height);

          //---- WPROMO ----
          WPROMO.setText("@WPROMO@");
          WPROMO.setBorder(new BevelBorder(BevelBorder.LOWERED));
          WPROMO.setHorizontalAlignment(SwingConstants.RIGHT);
          WPROMO.setName("WPROMO");
          p_contenu2.add(WPROMO);
          WPROMO.setBounds(275, 70, 100, WPROMO.getPreferredSize().height);

          //---- WPREF ----
          WPREF.setText("@WPREF@");
          WPREF.setBorder(new BevelBorder(BevelBorder.LOWERED));
          WPREF.setHorizontalAlignment(SwingConstants.RIGHT);
          WPREF.setName("WPREF");
          p_contenu2.add(WPREF);
          WPREF.setBounds(275, 100, 100, WPREF.getPreferredSize().height);

          //---- WPFAC ----
          WPFAC.setText("@WPFAC@");
          WPFAC.setBorder(new BevelBorder(BevelBorder.LOWERED));
          WPFAC.setHorizontalAlignment(SwingConstants.RIGHT);
          WPFAC.setName("WPFAC");
          p_contenu2.add(WPFAC);
          WPFAC.setBounds(new Rectangle(new Point(275, 130), WPFAC.getPreferredSize()));

          //---- WQFAC ----
          WQFAC.setText("@WQFAC@");
          WQFAC.setHorizontalAlignment(SwingConstants.RIGHT);
          WQFAC.setName("WQFAC");
          p_contenu2.add(WQFAC);
          WQFAC.setBounds(470, 130, 100, WQFAC.getPreferredSize().height);

          //---- WPBON ----
          WPBON.setText("@WPBON@");
          WPBON.setBorder(new BevelBorder(BevelBorder.LOWERED));
          WPBON.setHorizontalAlignment(SwingConstants.RIGHT);
          WPBON.setName("WPBON");
          p_contenu2.add(WPBON);
          WPBON.setBounds(275, 160, 100, WPBON.getPreferredSize().height);

          //---- WQBON ----
          WQBON.setText("@WQBON@");
          WQBON.setHorizontalAlignment(SwingConstants.RIGHT);
          WQBON.setName("WQBON");
          p_contenu2.add(WQBON);
          WQBON.setBounds(new Rectangle(new Point(470, 160), WQBON.getPreferredSize()));

          //---- OBJ_20 ----
          OBJ_20.setText("Validit\u00e9");
          OBJ_20.setFont(OBJ_20.getFont().deriveFont(OBJ_20.getFont().getStyle() | Font.BOLD));
          OBJ_20.setName("OBJ_20");
          p_contenu2.add(OBJ_20);
          OBJ_20.setBounds(410, 72, 59, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("Validit\u00e9");
          OBJ_26.setFont(OBJ_26.getFont().deriveFont(OBJ_26.getFont().getStyle() | Font.BOLD));
          OBJ_26.setName("OBJ_26");
          p_contenu2.add(OBJ_26);
          OBJ_26.setBounds(410, 102, 59, 20);

          //---- WDDPRO ----
          WDDPRO.setName("WDDPRO");
          p_contenu2.add(WDDPRO);
          WDDPRO.setBounds(470, 68, 105, WDDPRO.getPreferredSize().height);

          //---- WDFPRO ----
          WDFPRO.setName("WDFPRO");
          p_contenu2.add(WDFPRO);
          WDFPRO.setBounds(600, 68, 105, WDFPRO.getPreferredSize().height);

          //---- WDDREF ----
          WDDREF.setName("WDDREF");
          p_contenu2.add(WDDREF);
          WDDREF.setBounds(470, 98, 105, WDDREF.getPreferredSize().height);

          //---- WDFREF ----
          WDFREF.setName("WDFREF");
          p_contenu2.add(WDFREF);
          WDFREF.setBounds(600, 98, 105, WDFREF.getPreferredSize().height);

          //---- WDFAC ----
          WDFAC.setName("WDFAC");
          p_contenu2.add(WDFAC);
          WDFAC.setBounds(600, 128, 105, WDFAC.getPreferredSize().height);

          //---- WDBON ----
          WDBON.setName("WDBON");
          p_contenu2.add(WDBON);
          WDBON.setBounds(600, 158, 105, WDBON.getPreferredSize().height);

          //---- OBJ_15 ----
          OBJ_15.setText("Colonne");
          OBJ_15.setName("OBJ_15");
          p_contenu2.add(OBJ_15);
          OBJ_15.setBounds(540, 42, 53, 20);

          //---- OBJ_32 ----
          OBJ_32.setText("Quantit\u00e9");
          OBJ_32.setFont(OBJ_32.getFont().deriveFont(OBJ_32.getFont().getStyle() | Font.BOLD));
          OBJ_32.setName("OBJ_32");
          p_contenu2.add(OBJ_32);
          OBJ_32.setBounds(410, 132, 59, 20);

          //---- OBJ_38 ----
          OBJ_38.setText("Quantit\u00e9");
          OBJ_38.setFont(OBJ_38.getFont().deriveFont(OBJ_38.getFont().getStyle() | Font.BOLD));
          OBJ_38.setName("OBJ_38");
          p_contenu2.add(OBJ_38);
          OBJ_38.setBounds(410, 165, 59, 20);

          //---- OBJ_49 ----
          OBJ_49.setText("Aucune");
          OBJ_49.setName("OBJ_49");
          p_contenu2.add(OBJ_49);
          OBJ_49.setBounds(155, 160, 83, OBJ_49.getPreferredSize().height);

          //---- OBJ_13 ----
          OBJ_13.setText("Tarif");
          OBJ_13.setFont(OBJ_13.getFont().deriveFont(OBJ_13.getFont().getStyle() | Font.BOLD));
          OBJ_13.setName("OBJ_13");
          p_contenu2.add(OBJ_13);
          OBJ_13.setBounds(410, 42, 42, 20);

          //---- A1RTA ----
          A1RTA.setText("@A1RTA@");
          A1RTA.setName("A1RTA");
          p_contenu2.add(A1RTA);
          A1RTA.setBounds(470, 40, 60, A1RTA.getPreferredSize().height);

          //---- WTAR ----
          WTAR.setText("@WTAR@");
          WTAR.setName("WTAR");
          p_contenu2.add(WTAR);
          WTAR.setBounds(600, 40, 26, WTAR.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setText("Prix");
          OBJ_43.setFont(OBJ_43.getFont().deriveFont(OBJ_43.getFont().getStyle() | Font.BOLD));
          OBJ_43.setName("OBJ_43");
          p_contenu2.add(OBJ_43);
          OBJ_43.setBounds(280, 15, 25, 20);

          //---- OBJ_34 ----
          OBJ_34.setText("le");
          OBJ_34.setName("OBJ_34");
          p_contenu2.add(OBJ_34);
          OBJ_34.setBounds(580, 132, 18, 20);

          //---- OBJ_48 ----
          OBJ_48.setText("Le");
          OBJ_48.setName("OBJ_48");
          p_contenu2.add(OBJ_48);
          OBJ_48.setBounds(555, 160, 18, 20);

          //---- OBJ_46 ----
          OBJ_46.setText("\u00e0");
          OBJ_46.setName("OBJ_46");
          p_contenu2.add(OBJ_46);
          OBJ_46.setBounds(580, 72, 11, 20);

          //---- OBJ_47 ----
          OBJ_47.setText("\u00e0");
          OBJ_47.setName("OBJ_47");
          p_contenu2.add(OBJ_47);
          OBJ_47.setBounds(580, 102, 11, 20);

          //---- riBoutonDetail1 ----
          riBoutonDetail1.setToolTipText("Affichage du bon");
          riBoutonDetail1.setName("riBoutonDetail1");
          riBoutonDetail1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetail1ActionPerformed(e);
            }
          });
          p_contenu2.add(riBoutonDetail1);
          riBoutonDetail1.setBounds(new Rectangle(new Point(241, 133), riBoutonDetail1.getPreferredSize()));

          //---- riBoutonDetail2 ----
          riBoutonDetail2.setName("riBoutonDetail2");
          riBoutonDetail2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetail2ActionPerformed(e);
            }
          });
          p_contenu2.add(riBoutonDetail2);
          riBoutonDetail2.setBounds(new Rectangle(new Point(241, 163), riBoutonDetail2.getPreferredSize()));

          //---- OBJ_35 ----
          OBJ_35.setText("le");
          OBJ_35.setName("OBJ_35");
          p_contenu2.add(OBJ_35);
          OBJ_35.setBounds(580, 162, 18, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu2.getComponentCount(); i++) {
              Rectangle bounds = p_contenu2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu2.setMinimumSize(preferredSize);
            p_contenu2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_contenu2, GroupLayout.DEFAULT_SIZE, 725, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_contenu2, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
              .addGap(20, 20, 20))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_contenu2;
  private JLabel OBJ_36;
  private JLabel OBJ_17;
  private JLabel OBJ_29;
  private JLabel OBJ_23;
  private JPanel P_PnlOpts;
  private JLabel OBJ_11;
  private RiZoneSortie WPVB1;
  private RiZoneSortie PA1NBN;
  private RiZoneSortie WCLIX;
  private RiZoneSortie PA2NBN;
  private RiZoneSortie OBJ_51;
  private RiZoneSortie PROMO;
  private RiZoneSortie WPROMO;
  private RiZoneSortie WPREF;
  private RiZoneSortie WPFAC;
  private RiZoneSortie WQFAC;
  private RiZoneSortie WPBON;
  private RiZoneSortie WQBON;
  private JLabel OBJ_20;
  private JLabel OBJ_26;
  private XRiCalendrier WDDPRO;
  private XRiCalendrier WDFPRO;
  private XRiCalendrier WDDREF;
  private XRiCalendrier WDFREF;
  private XRiCalendrier WDFAC;
  private XRiCalendrier WDBON;
  private JLabel OBJ_15;
  private JLabel OBJ_32;
  private JLabel OBJ_38;
  private RiZoneSortie OBJ_49;
  private JLabel OBJ_13;
  private RiZoneSortie A1RTA;
  private RiZoneSortie WTAR;
  private JLabel OBJ_43;
  private JLabel OBJ_34;
  private JLabel OBJ_48;
  private JLabel OBJ_46;
  private JLabel OBJ_47;
  private SNBoutonDetail riBoutonDetail1;
  private SNBoutonDetail riBoutonDetail2;
  private JLabel OBJ_35;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
