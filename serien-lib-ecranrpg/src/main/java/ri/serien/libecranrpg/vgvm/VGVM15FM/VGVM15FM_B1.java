
package ri.serien.libecranrpg.vgvm.VGVM15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM15FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM15FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Gestion de numéros de colis"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    L1LIB1 = new XRiTextField();
    L1LIB3 = new XRiTextField();
    L1LIB4 = new XRiTextField();
    L1LIB2 = new XRiTextField();
    OBJ_26 = new JLabel();
    OBJ_25 = new JLabel();
    WBON = new XRiTextField();
    L1NLI = new XRiTextField();
    CO11 = new XRiTextField();
    CO12 = new XRiTextField();
    CO13 = new XRiTextField();
    CO14 = new XRiTextField();
    CO15 = new XRiTextField();
    CO16 = new XRiTextField();
    CO17 = new XRiTextField();
    CO18 = new XRiTextField();
    CO19 = new XRiTextField();
    CO21 = new XRiTextField();
    CO22 = new XRiTextField();
    CO23 = new XRiTextField();
    CO24 = new XRiTextField();
    CO25 = new XRiTextField();
    CO26 = new XRiTextField();
    CO27 = new XRiTextField();
    CO28 = new XRiTextField();
    CO29 = new XRiTextField();
    CO31 = new XRiTextField();
    CO32 = new XRiTextField();
    CO33 = new XRiTextField();
    CO34 = new XRiTextField();
    CO35 = new XRiTextField();
    CO36 = new XRiTextField();
    CO37 = new XRiTextField();
    CO38 = new XRiTextField();
    CO39 = new XRiTextField();
    CO41 = new XRiTextField();
    CO42 = new XRiTextField();
    CO43 = new XRiTextField();
    CO44 = new XRiTextField();
    CO45 = new XRiTextField();
    CO46 = new XRiTextField();
    CO47 = new XRiTextField();
    CO48 = new XRiTextField();
    CO49 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(820, 310));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Num\u00e9ros suppl\u00e9mentaires"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- L1LIB1 ----
          L1LIB1.setName("L1LIB1");
          panel1.add(L1LIB1);
          L1LIB1.setBounds(25, 110, 232, L1LIB1.getPreferredSize().height);

          //---- L1LIB3 ----
          L1LIB3.setName("L1LIB3");
          panel1.add(L1LIB3);
          L1LIB3.setBounds(25, 176, 232, L1LIB3.getPreferredSize().height);

          //---- L1LIB4 ----
          L1LIB4.setName("L1LIB4");
          panel1.add(L1LIB4);
          L1LIB4.setBounds(25, 209, 232, L1LIB4.getPreferredSize().height);

          //---- L1LIB2 ----
          L1LIB2.setName("L1LIB2");
          panel1.add(L1LIB2);
          L1LIB2.setBounds(25, 143, 232, L1LIB2.getPreferredSize().height);

          //---- OBJ_26 ----
          OBJ_26.setText("Num\u00e9ro de ligne");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(25, 74, 102, 20);

          //---- OBJ_25 ----
          OBJ_25.setText("Num\u00e9ro de bon");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(25, 44, 96, 20);

          //---- WBON ----
          WBON.setName("WBON");
          panel1.add(WBON);
          WBON.setBounds(145, 40, 70, WBON.getPreferredSize().height);

          //---- L1NLI ----
          L1NLI.setName("L1NLI");
          panel1.add(L1NLI);
          L1NLI.setBounds(145, 70, 34, L1NLI.getPreferredSize().height);

          //---- CO11 ----
          CO11.setName("CO11");
          panel1.add(CO11);
          CO11.setBounds(355, 35, 60, CO11.getPreferredSize().height);

          //---- CO12 ----
          CO12.setName("CO12");
          panel1.add(CO12);
          CO12.setBounds(355, 60, 60, CO12.getPreferredSize().height);

          //---- CO13 ----
          CO13.setName("CO13");
          panel1.add(CO13);
          CO13.setBounds(355, 85, 60, CO13.getPreferredSize().height);

          //---- CO14 ----
          CO14.setName("CO14");
          panel1.add(CO14);
          CO14.setBounds(355, 110, 60, CO14.getPreferredSize().height);

          //---- CO15 ----
          CO15.setName("CO15");
          panel1.add(CO15);
          CO15.setBounds(355, 135, 60, CO15.getPreferredSize().height);

          //---- CO16 ----
          CO16.setName("CO16");
          panel1.add(CO16);
          CO16.setBounds(355, 160, 60, CO16.getPreferredSize().height);

          //---- CO17 ----
          CO17.setName("CO17");
          panel1.add(CO17);
          CO17.setBounds(355, 185, 60, CO17.getPreferredSize().height);

          //---- CO18 ----
          CO18.setName("CO18");
          panel1.add(CO18);
          CO18.setBounds(355, 210, 60, CO18.getPreferredSize().height);

          //---- CO19 ----
          CO19.setName("CO19");
          panel1.add(CO19);
          CO19.setBounds(355, 235, 60, CO19.getPreferredSize().height);

          //---- CO21 ----
          CO21.setName("CO21");
          panel1.add(CO21);
          CO21.setBounds(415, 35, 60, CO21.getPreferredSize().height);

          //---- CO22 ----
          CO22.setName("CO22");
          panel1.add(CO22);
          CO22.setBounds(415, 60, 60, CO22.getPreferredSize().height);

          //---- CO23 ----
          CO23.setName("CO23");
          panel1.add(CO23);
          CO23.setBounds(415, 85, 60, CO23.getPreferredSize().height);

          //---- CO24 ----
          CO24.setName("CO24");
          panel1.add(CO24);
          CO24.setBounds(415, 110, 60, CO24.getPreferredSize().height);

          //---- CO25 ----
          CO25.setName("CO25");
          panel1.add(CO25);
          CO25.setBounds(415, 135, 60, CO25.getPreferredSize().height);

          //---- CO26 ----
          CO26.setName("CO26");
          panel1.add(CO26);
          CO26.setBounds(415, 160, 60, CO26.getPreferredSize().height);

          //---- CO27 ----
          CO27.setName("CO27");
          panel1.add(CO27);
          CO27.setBounds(415, 185, 60, CO27.getPreferredSize().height);

          //---- CO28 ----
          CO28.setName("CO28");
          panel1.add(CO28);
          CO28.setBounds(415, 210, 60, CO28.getPreferredSize().height);

          //---- CO29 ----
          CO29.setName("CO29");
          panel1.add(CO29);
          CO29.setBounds(415, 235, 60, CO29.getPreferredSize().height);

          //---- CO31 ----
          CO31.setName("CO31");
          panel1.add(CO31);
          CO31.setBounds(475, 35, 60, CO31.getPreferredSize().height);

          //---- CO32 ----
          CO32.setName("CO32");
          panel1.add(CO32);
          CO32.setBounds(475, 60, 60, CO32.getPreferredSize().height);

          //---- CO33 ----
          CO33.setName("CO33");
          panel1.add(CO33);
          CO33.setBounds(475, 85, 60, CO33.getPreferredSize().height);

          //---- CO34 ----
          CO34.setName("CO34");
          panel1.add(CO34);
          CO34.setBounds(475, 110, 60, CO34.getPreferredSize().height);

          //---- CO35 ----
          CO35.setName("CO35");
          panel1.add(CO35);
          CO35.setBounds(475, 135, 60, CO35.getPreferredSize().height);

          //---- CO36 ----
          CO36.setName("CO36");
          panel1.add(CO36);
          CO36.setBounds(475, 160, 60, CO36.getPreferredSize().height);

          //---- CO37 ----
          CO37.setName("CO37");
          panel1.add(CO37);
          CO37.setBounds(475, 185, 60, CO37.getPreferredSize().height);

          //---- CO38 ----
          CO38.setName("CO38");
          panel1.add(CO38);
          CO38.setBounds(475, 210, 60, CO38.getPreferredSize().height);

          //---- CO39 ----
          CO39.setName("CO39");
          panel1.add(CO39);
          CO39.setBounds(475, 235, 60, CO39.getPreferredSize().height);

          //---- CO41 ----
          CO41.setName("CO41");
          panel1.add(CO41);
          CO41.setBounds(535, 35, 60, CO41.getPreferredSize().height);

          //---- CO42 ----
          CO42.setName("CO42");
          panel1.add(CO42);
          CO42.setBounds(535, 60, 60, CO42.getPreferredSize().height);

          //---- CO43 ----
          CO43.setName("CO43");
          panel1.add(CO43);
          CO43.setBounds(535, 85, 60, CO43.getPreferredSize().height);

          //---- CO44 ----
          CO44.setName("CO44");
          panel1.add(CO44);
          CO44.setBounds(535, 110, 60, CO44.getPreferredSize().height);

          //---- CO45 ----
          CO45.setName("CO45");
          panel1.add(CO45);
          CO45.setBounds(535, 135, 60, CO45.getPreferredSize().height);

          //---- CO46 ----
          CO46.setName("CO46");
          panel1.add(CO46);
          CO46.setBounds(535, 160, 60, CO46.getPreferredSize().height);

          //---- CO47 ----
          CO47.setName("CO47");
          panel1.add(CO47);
          CO47.setBounds(535, 185, 60, CO47.getPreferredSize().height);

          //---- CO48 ----
          CO48.setName("CO48");
          panel1.add(CO48);
          CO48.setBounds(535, 210, 60, CO48.getPreferredSize().height);

          //---- CO49 ----
          CO49.setName("CO49");
          panel1.add(CO49);
          CO49.setBounds(535, 235, 60, CO49.getPreferredSize().height);

          //---- label1 ----
          label1.setText("1");
          label1.setHorizontalAlignment(SwingConstants.RIGHT);
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(320, 35, 30, 28);

          //---- label2 ----
          label2.setText("5");
          label2.setHorizontalAlignment(SwingConstants.RIGHT);
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(320, 60, 30, 28);

          //---- label3 ----
          label3.setText("9");
          label3.setHorizontalAlignment(SwingConstants.RIGHT);
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(320, 85, 30, 28);

          //---- label4 ----
          label4.setText("13");
          label4.setHorizontalAlignment(SwingConstants.RIGHT);
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(320, 110, 30, 28);

          //---- label5 ----
          label5.setText("17");
          label5.setHorizontalAlignment(SwingConstants.RIGHT);
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(320, 135, 30, 28);

          //---- label6 ----
          label6.setText("21");
          label6.setHorizontalAlignment(SwingConstants.RIGHT);
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(320, 160, 30, 28);

          //---- label7 ----
          label7.setText("25");
          label7.setHorizontalAlignment(SwingConstants.RIGHT);
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(320, 185, 30, 28);

          //---- label8 ----
          label8.setText("29");
          label8.setHorizontalAlignment(SwingConstants.RIGHT);
          label8.setName("label8");
          panel1.add(label8);
          label8.setBounds(320, 210, 30, 28);

          //---- label9 ----
          label9.setText("33");
          label9.setHorizontalAlignment(SwingConstants.RIGHT);
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(320, 235, 30, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 623, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }

    //======== riMenu_V01F ========
    {
      riMenu_V01F.setMinimumSize(new Dimension(104, 50));
      riMenu_V01F.setPreferredSize(new Dimension(170, 50));
      riMenu_V01F.setMaximumSize(new Dimension(104, 50));
      riMenu_V01F.setName("riMenu_V01F");

      //---- riMenu_bt_V01F ----
      riMenu_bt_V01F.setText("@V01F@");
      riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
      riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
      riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
      riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
      riMenu_bt_V01F.setName("riMenu_bt_V01F");
      riMenu_V01F.add(riMenu_bt_V01F);
    }

    //======== riSousMenu_consult ========
    {
      riSousMenu_consult.setName("riSousMenu_consult");

      //---- riSousMenu_bt_consult ----
      riSousMenu_bt_consult.setText("Consultation");
      riSousMenu_bt_consult.setToolTipText("Consultation");
      riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
      riSousMenu_consult.add(riSousMenu_bt_consult);
    }

    //======== riSousMenu_modif ========
    {
      riSousMenu_modif.setName("riSousMenu_modif");

      //---- riSousMenu_bt_modif ----
      riSousMenu_bt_modif.setText("Modification");
      riSousMenu_bt_modif.setToolTipText("Modification");
      riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
      riSousMenu_modif.add(riSousMenu_bt_modif);
    }

    //======== riSousMenu_crea ========
    {
      riSousMenu_crea.setName("riSousMenu_crea");

      //---- riSousMenu_bt_crea ----
      riSousMenu_bt_crea.setText("Cr\u00e9ation");
      riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
      riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
      riSousMenu_crea.add(riSousMenu_bt_crea);
    }

    //======== riSousMenu_suppr ========
    {
      riSousMenu_suppr.setName("riSousMenu_suppr");

      //---- riSousMenu_bt_suppr ----
      riSousMenu_bt_suppr.setText("Annulation");
      riSousMenu_bt_suppr.setToolTipText("Annulation");
      riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
      riSousMenu_suppr.add(riSousMenu_bt_suppr);
    }

    //======== riSousMenuF_dupli ========
    {
      riSousMenuF_dupli.setName("riSousMenuF_dupli");

      //---- riSousMenu_bt_dupli ----
      riSousMenu_bt_dupli.setText("Duplication");
      riSousMenu_bt_dupli.setToolTipText("Duplication");
      riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
      riSousMenuF_dupli.add(riSousMenu_bt_dupli);
    }

    //======== riSousMenu_rappel ========
    {
      riSousMenu_rappel.setName("riSousMenu_rappel");

      //---- riSousMenu_bt_rappel ----
      riSousMenu_bt_rappel.setText("Rappel");
      riSousMenu_bt_rappel.setToolTipText("Rappel");
      riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
      riSousMenu_rappel.add(riSousMenu_bt_rappel);
    }

    //======== riSousMenu_reac ========
    {
      riSousMenu_reac.setName("riSousMenu_reac");

      //---- riSousMenu_bt_reac ----
      riSousMenu_bt_reac.setText("R\u00e9activation");
      riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
      riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
      riSousMenu_reac.add(riSousMenu_bt_reac);
    }

    //======== riSousMenu_destr ========
    {
      riSousMenu_destr.setName("riSousMenu_destr");

      //---- riSousMenu_bt_destr ----
      riSousMenu_bt_destr.setText("Suppression");
      riSousMenu_bt_destr.setToolTipText("Suppression");
      riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
      riSousMenu_destr.add(riSousMenu_bt_destr);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField L1LIB1;
  private XRiTextField L1LIB3;
  private XRiTextField L1LIB4;
  private XRiTextField L1LIB2;
  private JLabel OBJ_26;
  private JLabel OBJ_25;
  private XRiTextField WBON;
  private XRiTextField L1NLI;
  private XRiTextField CO11;
  private XRiTextField CO12;
  private XRiTextField CO13;
  private XRiTextField CO14;
  private XRiTextField CO15;
  private XRiTextField CO16;
  private XRiTextField CO17;
  private XRiTextField CO18;
  private XRiTextField CO19;
  private XRiTextField CO21;
  private XRiTextField CO22;
  private XRiTextField CO23;
  private XRiTextField CO24;
  private XRiTextField CO25;
  private XRiTextField CO26;
  private XRiTextField CO27;
  private XRiTextField CO28;
  private XRiTextField CO29;
  private XRiTextField CO31;
  private XRiTextField CO32;
  private XRiTextField CO33;
  private XRiTextField CO34;
  private XRiTextField CO35;
  private XRiTextField CO36;
  private XRiTextField CO37;
  private XRiTextField CO38;
  private XRiTextField CO39;
  private XRiTextField CO41;
  private XRiTextField CO42;
  private XRiTextField CO43;
  private XRiTextField CO44;
  private XRiTextField CO45;
  private XRiTextField CO46;
  private XRiTextField CO47;
  private XRiTextField CO48;
  private XRiTextField CO49;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
