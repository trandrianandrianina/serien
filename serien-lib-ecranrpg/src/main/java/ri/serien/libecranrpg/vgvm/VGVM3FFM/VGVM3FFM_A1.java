
package ri.serien.libecranrpg.vgvm.VGVM3FFM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JPanel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Ecran de validation des demandes d'autorisation pour les différés.
 */
public class VGVM3FFM_A1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  public VGVM3FFM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    
    // Initialiser les boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    // gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Afficher le logo
    pnlBpresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Mettre à jour l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "Enter");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    pnlBpresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlInformationDocument = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbNumeroDocument = new SNLabelChamp();
    pnlNumeroDocument = new SNPanel();
    WBON = new XRiTextField();
    WSUF = new XRiTextField();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());

      //---- pnlBpresentation ----
      pnlBpresentation.setText("Autoriser les demandes de r\u00e9glements diff\u00e9r\u00e9s ");
      pnlBpresentation.setName("pnlBpresentation");
      pnlNord.add(pnlBpresentation);
    }
    add(pnlNord, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());

      //======== pnlInformationDocument ========
      {
        pnlInformationDocument.setTitre("Document");
        pnlInformationDocument.setName("pnlInformationDocument");
        pnlInformationDocument.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlInformationDocument.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlInformationDocument.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlInformationDocument.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlInformationDocument.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlInformationDocument.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snEtablissement ----
        snEtablissement.setMinimumSize(new Dimension(320, 30));
        snEtablissement.setPreferredSize(new Dimension(320, 30));
        snEtablissement.setName("snEtablissement");
        snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snEtablissementValueChanged(e);
          }
        });
        pnlInformationDocument.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbNumeroDocument ----
        lbNumeroDocument.setText("Num\u00e9ro du document ");
        lbNumeroDocument.setName("lbNumeroDocument");
        pnlInformationDocument.add(lbNumeroDocument, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlNumeroDocument ========
        {
          pnlNumeroDocument.setName("pnlNumeroDocument");
          pnlNumeroDocument.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlNumeroDocument.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlNumeroDocument.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlNumeroDocument.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlNumeroDocument.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- WBON ----
          WBON.setMinimumSize(new Dimension(65, 30));
          WBON.setPreferredSize(new Dimension(100, 30));
          WBON.setFont(WBON.getFont().deriveFont(WBON.getFont().getSize() + 2f));
          WBON.setName("WBON");
          pnlNumeroDocument.add(WBON, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WSUF ----
          WSUF.setPreferredSize(new Dimension(25, 30));
          WSUF.setMinimumSize(new Dimension(25, 30));
          WSUF.setFont(WSUF.getFont().deriveFont(WSUF.getFont().getSize() + 2f));
          WSUF.setName("WSUF");
          pnlNumeroDocument.add(WSUF, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformationDocument.add(pnlNumeroDocument, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlInformationDocument, BorderLayout.NORTH);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre pnlBpresentation;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlInformationDocument;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbNumeroDocument;
  private SNPanel pnlNumeroDocument;
  private XRiTextField WBON;
  private XRiTextField WSUF;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
