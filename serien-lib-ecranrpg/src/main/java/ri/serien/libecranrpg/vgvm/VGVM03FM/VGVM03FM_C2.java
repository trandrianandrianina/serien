
package ri.serien.libecranrpg.vgvm.VGVM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM03FM_C2 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] EBPRP_Value = { "", "1", "2", };
  
  /**
   * Constructeur.
   */
  public VGVM03FM_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    majGeneration();
    
    // Ajout
    initDiverses();
    EBPRP.setValeurs(EBPRP_Value, null);
    
    EBVEH.setValeursSelection("1", "");
    EBCHAN.setValeursSelection("1", "");
    CLIN2.setValeursSelection("1", " ");
    CLIN1.setValeursSelection("1", " ");
    EBIN10.setValeursSelection("1", " ");
    CLIN30.setValeursSelection("1", "");
    EBREFC.setValeursSelection("1", "");
    
    setCloseKey("F2", "F9", "F12", "F20", "F21");
    
    // Titre
    setTitle("Livraison");
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZGLIB@")).trim());
    OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EXLIB@")).trim());
    OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@KVLIB@")).trim());
    OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRPKG@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLIB@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL16R@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    boolean in52 = lexique.isTrue("52");
    boolean in53 = lexique.isTrue("53");
    
    // visibilité de certains menus
    riSousMenu7.setEnabled(in52);
    riSousMenu8.setEnabled(in52);
    riSousMenu9.setEnabled(in52);
    
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    boolean isModif = (!lexique.isTrue("53"));
    
    navig_valid.setVisible(isModif);
    
    // La gestion spécifique des cases à cocher Référence courte obligatoire et référence longue obligatoire
    // Il s'agit de 2 zones spécifiques à la fiche client, respectivement EBREFC et CLIN30
    // Hors si la PS157 est levée alors il faut imposer la réf courte pour tous les clients
    // Si la PS329 est levée alors il faut imposer la réf longue pour tous les clients.
    // il a été choisi d'afficher ces informations de manière graphique sur la fiche en client en cochant les case correspondantes
    
    // On force l'affichage en Interrogation uniquement
    OBJ_58.setVisible(lexique.isPresent("CLMAG"));
    
    OBJ_28.setVisible(CLIN1.isVisible() || CLIN2.isVisible());
    
    OBJ_39.setVisible(CLCAN.isVisible());
    OBJ_53.setVisible(CLCTR.isVisible());
    OBJ_55.setVisible(CLCTR.isVisible());
    OBJ_60.setVisible(CLMAG.isVisible());
    OBJ_36.setVisible(CLMEX.isVisible());
    
    gene0.setEnabled(!in53);
    gene1.setEnabled(!in53);
    gene2.setEnabled(!in53);
    livr1.setEnabled(!in53);
    livr2.setEnabled(!in53);
    
    if ((lexique.HostFieldGetData("CLIN5").trim().equals("1")) || (lexique.HostFieldGetData("CLIN5").trim().equals("2"))) {
      gene0.setSelected(false);
      gene1.setSelected(true);
      gene2.setSelected(false);
      livr1.setSelected(lexique.HostFieldGetData("CLIN5").trim().equals("2"));
    }
    else if ((lexique.HostFieldGetData("CLIN5").trim().equals("5")) || (lexique.HostFieldGetData("CLIN5").trim().equals("6"))) {
      gene0.setSelected(false);
      gene1.setSelected(false);
      gene2.setSelected(true);
      livr2.setSelected(lexique.HostFieldGetData("CLIN5").trim().equals("6"));
    }
    else {
      gene0.setSelected(true);
      gene1.setSelected(false);
      gene2.setSelected(false);
      livr1.setSelected(false);
      livr2.setSelected(false);
    }
    
    majGeneration();
  }
  
  private void majGeneration() {
    if (gene1.isSelected()) {
      livr1.setVisible(true);
      livr2.setVisible(false);
      livr2.setSelected(false);
    }
    else if (gene2.isSelected()) {
      livr1.setVisible(false);
      livr1.setSelected(false);
      livr2.setVisible(true);
    }
    else {
      livr1.setVisible(false);
      livr1.setSelected(false);
      livr2.setVisible(false);
      livr2.setSelected(false);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
    String valeur = "";
    
    if (gene1.isSelected()) {
      if (livr1.isSelected()) {
        valeur = "2";
      }
      else {
        valeur = "1";
      }
    }
    else if (gene2.isSelected()) {
      if (livr2.isSelected()) {
        valeur = "6";
      }
      else {
        valeur = "5";
      }
    }
    
    lexique.HostFieldPutData("CLIN5", 0, valeur);
    
  }
  
  private void gene0ActionPerformed(ActionEvent e) {
    majGeneration();
  }
  
  private void gene1ActionPerformed(ActionEvent e) {
    majGeneration();
  }
  
  private void gene2ActionPerformed(ActionEvent e) {
    majGeneration();
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void inviteActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void aideActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_32 = new JPanel();
    OBJ_35 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_39 = new JLabel();
    CLGEO = new XRiTextField();
    CLMEX = new XRiTextField();
    CLCAN = new XRiTextField();
    panel2 = new JPanel();
    gene0 = new JRadioButton();
    gene1 = new JRadioButton();
    gene2 = new JRadioButton();
    livr1 = new JCheckBox();
    livr2 = new JCheckBox();
    OBJ_48 = new JPanel();
    panel1 = new JPanel();
    OBJ_51 = new JLabel();
    CLOTO = new XRiTextField();
    OBJ_65 = new JLabel();
    CLFRP = new XRiTextField();
    OBJ_68 = new JLabel();
    CLIN14 = new XRiTextField();
    CLIN24 = new XRiTextField();
    CLIN25 = new XRiTextField();
    OBJ_67 = new JLabel();
    panel3 = new JPanel();
    OBJ_60 = new RiZoneSortie();
    OBJ_55 = new RiZoneSortie();
    OBJ_61 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_58 = new JLabel();
    ZPC16R = new XRiTextField();
    CLTRA = new XRiTextField();
    CLCRT = new XRiTextField();
    CLCTR = new XRiTextField();
    CLMAG = new XRiTextField();
    OBJ_85 = new JLabel();
    EBPRP = new XRiComboBox();
    EBVEH = new XRiCheckBox();
    EBCHAN = new XRiCheckBox();
    EBIN10 = new XRiCheckBox();
    CLIN30 = new XRiCheckBox();
    EBREFC = new XRiCheckBox();
    OBJ_28 = new JPanel();
    CLIN2 = new XRiCheckBox();
    CLIN1 = new XRiCheckBox();
    BTD = new JPopupMenu();
    invite = new JMenuItem();
    aide = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(985, 530));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Commandes non livr\u00e9es");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Cr\u00e9er commande");
            riSousMenu_bt7.setToolTipText("Cr\u00e9er commande");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Cr\u00e9er devis");
            riSousMenu_bt8.setToolTipText("Cr\u00e9er devis");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);

          //======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");

            //---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Tourn\u00e9e");
            riSousMenu_bt9.setToolTipText("Tourn\u00e9e");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== OBJ_32 ========
        {
          OBJ_32.setBorder(new TitledBorder("Divers"));
          OBJ_32.setOpaque(false);
          OBJ_32.setName("OBJ_32");
          OBJ_32.setLayout(null);

          //---- OBJ_35 ----
          OBJ_35.setText("@ZGLIB@");
          OBJ_35.setName("OBJ_35");
          OBJ_32.add(OBJ_35);
          OBJ_35.setBounds(220, 34, 165, 20);

          //---- OBJ_38 ----
          OBJ_38.setText("@EXLIB@");
          OBJ_38.setName("OBJ_38");
          OBJ_32.add(OBJ_38);
          OBJ_38.setBounds(220, 64, 165, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("@KVLIB@");
          OBJ_41.setName("OBJ_41");
          OBJ_32.add(OBJ_41);
          OBJ_41.setBounds(220, 94, 165, 20);

          //---- OBJ_33 ----
          OBJ_33.setText("Zone g\u00e9ographique");
          OBJ_33.setName("OBJ_33");
          OBJ_32.add(OBJ_33);
          OBJ_33.setBounds(20, 34, 125, 20);

          //---- OBJ_36 ----
          OBJ_36.setText("Mode d'exp\u00e9dition");
          OBJ_36.setName("OBJ_36");
          OBJ_32.add(OBJ_36);
          OBJ_36.setBounds(20, 64, 125, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("Canal de vente");
          OBJ_39.setName("OBJ_39");
          OBJ_32.add(OBJ_39);
          OBJ_39.setBounds(20, 94, 125, 20);

          //---- CLGEO ----
          CLGEO.setComponentPopupMenu(BTD);
          CLGEO.setName("CLGEO");
          OBJ_32.add(CLGEO);
          CLGEO.setBounds(155, 30, 60, CLGEO.getPreferredSize().height);

          //---- CLMEX ----
          CLMEX.setComponentPopupMenu(BTD);
          CLMEX.setName("CLMEX");
          OBJ_32.add(CLMEX);
          CLMEX.setBounds(155, 60, 30, CLMEX.getPreferredSize().height);

          //---- CLCAN ----
          CLCAN.setComponentPopupMenu(BTD);
          CLCAN.setName("CLCAN");
          OBJ_32.add(CLCAN);
          CLCAN.setBounds(155, 90, 40, CLCAN.getPreferredSize().height);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("G\u00e9n\u00e9ration d'achat"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- gene0 ----
            gene0.setText("Pas de g\u00e9n\u00e9ration");
            gene0.setName("gene0");
            gene0.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                gene0ActionPerformed(e);
              }
            });
            panel2.add(gene0);
            gene0.setBounds(20, 25, 150, 20);

            //---- gene1 ----
            gene1.setText("G\u00e9n\u00e9ration diff\u00e9r\u00e9e");
            gene1.setName("gene1");
            gene1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                gene1ActionPerformed(e);
              }
            });
            panel2.add(gene1);
            gene1.setBounds(20, 45, 150, 20);

            //---- gene2 ----
            gene2.setText("G\u00e9n\u00e9ration imm\u00e9diate");
            gene2.setName("gene2");
            gene2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                gene2ActionPerformed(e);
              }
            });
            panel2.add(gene2);
            gene2.setBounds(20, 65, 150, 20);

            //---- livr1 ----
            livr1.setText("livraison directe assur\u00e9e");
            livr1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            livr1.setName("livr1");
            panel2.add(livr1);
            livr1.setBounds(190, 45, 165, 20);

            //---- livr2 ----
            livr2.setText("livraison directe assur\u00e9e");
            livr2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            livr2.setName("livr2");
            panel2.add(livr2);
            livr2.setBounds(190, 65, 165, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          OBJ_32.add(panel2);
          panel2.setBounds(390, 30, 365, 100);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_32.getComponentCount(); i++) {
              Rectangle bounds = OBJ_32.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_32.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_32.setMinimumSize(preferredSize);
            OBJ_32.setPreferredSize(preferredSize);
          }
        }

        //======== OBJ_48 ========
        {
          OBJ_48.setBorder(new TitledBorder("Transport"));
          OBJ_48.setOpaque(false);
          OBJ_48.setName("OBJ_48");
          OBJ_48.setLayout(null);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_51 ----
            OBJ_51.setText("Ordre tourn\u00e9e");
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(10, 9, 185, 20);

            //---- CLOTO ----
            CLOTO.setComponentPopupMenu(null);
            CLOTO.setName("CLOTO");
            panel1.add(CLOTO);
            CLOTO.setBounds(195, 5, 52, CLOTO.getPreferredSize().height);

            //---- OBJ_65 ----
            OBJ_65.setText("Commande mini. pour Franco");
            OBJ_65.setName("OBJ_65");
            panel1.add(OBJ_65);
            OBJ_65.setBounds(10, 39, 185, 20);

            //---- CLFRP ----
            CLFRP.setComponentPopupMenu(null);
            CLFRP.setName("CLFRP");
            panel1.add(CLFRP);
            CLFRP.setBounds(195, 35, 68, CLFRP.getPreferredSize().height);

            //---- OBJ_68 ----
            OBJ_68.setText("Frais d'exp\u00e9dition");
            OBJ_68.setName("OBJ_68");
            panel1.add(OBJ_68);
            OBJ_68.setBounds(10, 70, 185, 20);

            //---- CLIN14 ----
            CLIN14.setComponentPopupMenu(BTD);
            CLIN14.setName("CLIN14");
            panel1.add(CLIN14);
            CLIN14.setBounds(195, 66, 25, CLIN14.getPreferredSize().height);

            //---- CLIN24 ----
            CLIN24.setComponentPopupMenu(BTD);
            CLIN24.setName("CLIN24");
            panel1.add(CLIN24);
            CLIN24.setBounds(227, 66, 25, CLIN24.getPreferredSize().height);

            //---- CLIN25 ----
            CLIN25.setComponentPopupMenu(BTD);
            CLIN25.setName("CLIN25");
            panel1.add(CLIN25);
            CLIN25.setBounds(259, 66, 25, CLIN25.getPreferredSize().height);

            //---- OBJ_67 ----
            OBJ_67.setText("@FRPKG@");
            OBJ_67.setName("OBJ_67");
            panel1.add(OBJ_67);
            OBJ_67.setBounds(10, 100, 275, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          OBJ_48.add(panel1);
          panel1.setBounds(405, 25, 350, 130);

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_60 ----
            OBJ_60.setText("@MALIB@");
            OBJ_60.setName("OBJ_60");
            panel3.add(OBJ_60);
            OBJ_60.setBounds(185, 97, 170, OBJ_60.getPreferredSize().height);

            //---- OBJ_55 ----
            OBJ_55.setText("@TRLIB@");
            OBJ_55.setName("OBJ_55");
            panel3.add(OBJ_55);
            OBJ_55.setBounds(185, 37, 170, OBJ_55.getPreferredSize().height);

            //---- OBJ_61 ----
            OBJ_61.setText("Client transitaire");
            OBJ_61.setName("OBJ_61");
            panel3.add(OBJ_61);
            OBJ_61.setBounds(10, 129, 120, 20);

            //---- OBJ_49 ----
            OBJ_49.setText("Regroupem. transport");
            OBJ_49.setName("OBJ_49");
            panel3.add(OBJ_49);
            OBJ_49.setBounds(10, 9, 145, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("Transporteur habituel");
            OBJ_53.setName("OBJ_53");
            panel3.add(OBJ_53);
            OBJ_53.setBounds(10, 39, 130, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("@ZPL16R@");
            OBJ_56.setName("OBJ_56");
            panel3.add(OBJ_56);
            OBJ_56.setBounds(10, 69, 140, 20);

            //---- OBJ_58 ----
            OBJ_58.setText("Magasin");
            OBJ_58.setName("OBJ_58");
            panel3.add(OBJ_58);
            OBJ_58.setBounds(10, 99, 90, 20);

            //---- ZPC16R ----
            ZPC16R.setName("ZPC16R");
            panel3.add(ZPC16R);
            ZPC16R.setBounds(145, 65, 60, ZPC16R.getPreferredSize().height);

            //---- CLTRA ----
            CLTRA.setName("CLTRA");
            panel3.add(CLTRA);
            CLTRA.setBounds(145, 125, 60, CLTRA.getPreferredSize().height);

            //---- CLCRT ----
            CLCRT.setName("CLCRT");
            panel3.add(CLCRT);
            CLCRT.setBounds(145, 5, 40, CLCRT.getPreferredSize().height);

            //---- CLCTR ----
            CLCTR.setComponentPopupMenu(BTD);
            CLCTR.setName("CLCTR");
            panel3.add(CLCTR);
            CLCTR.setBounds(145, 35, 34, CLCTR.getPreferredSize().height);

            //---- CLMAG ----
            CLMAG.setComponentPopupMenu(BTD);
            CLMAG.setName("CLMAG");
            panel3.add(CLMAG);
            CLMAG.setBounds(145, 95, 34, CLMAG.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          OBJ_48.add(panel3);
          panel3.setBounds(10, 25, 385, 155);

          //---- OBJ_85 ----
          OBJ_85.setText("\"Pris par\"");
          OBJ_85.setName("OBJ_85");
          OBJ_48.add(OBJ_85);
          OBJ_85.setBounds(20, 185, 110, 28);

          //---- EBPRP ----
          EBPRP.setModel(new DefaultComboBoxModel(new String[] {
            "Non obligatoire",
            "Obligatoire avec Gencod",
            "Obligatoire avec contact"
          }));
          EBPRP.setComponentPopupMenu(null);
          EBPRP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EBPRP.setName("EBPRP");
          OBJ_48.add(EBPRP);
          EBPRP.setBounds(155, 186, 206, EBPRP.getPreferredSize().height);

          //---- EBVEH ----
          EBVEH.setText("V\u00e9hicule obligatoire");
          EBVEH.setComponentPopupMenu(null);
          EBVEH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EBVEH.setName("EBVEH");
          OBJ_48.add(EBVEH);
          EBVEH.setBounds(415, 185, 245, 28);

          //---- EBCHAN ----
          EBCHAN.setText("Chantier obligatoire");
          EBCHAN.setComponentPopupMenu(null);
          EBCHAN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EBCHAN.setName("EBCHAN");
          OBJ_48.add(EBCHAN);
          EBCHAN.setBounds(415, 215, 245, 28);

          //---- EBIN10 ----
          EBIN10.setText("Gestion des palettes par chantier");
          EBIN10.setComponentPopupMenu(null);
          EBIN10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EBIN10.setName("EBIN10");
          OBJ_48.add(EBIN10);
          EBIN10.setBounds(415, 155, 245, 28);

          //---- CLIN30 ----
          CLIN30.setText("R\u00e9f\u00e9rence longue obligatoire sur les documents de ventes");
          CLIN30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CLIN30.setName("CLIN30");
          OBJ_48.add(CLIN30);
          CLIN30.setBounds(20, 240, 355, 28);

          //---- EBREFC ----
          EBREFC.setText("R\u00e9f\u00e9rence courte obligatoire sur les documents de ventes");
          EBREFC.setComponentPopupMenu(null);
          EBREFC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EBREFC.setName("EBREFC");
          OBJ_48.add(EBREFC);
          EBREFC.setBounds(20, 215, 345, 28);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_48.getComponentCount(); i++) {
              Rectangle bounds = OBJ_48.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_48.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_48.setMinimumSize(preferredSize);
            OBJ_48.setPreferredSize(preferredSize);
          }
        }

        //======== OBJ_28 ========
        {
          OBJ_28.setBorder(new TitledBorder(""));
          OBJ_28.setOpaque(false);
          OBJ_28.setName("OBJ_28");
          OBJ_28.setLayout(null);

          //---- CLIN2 ----
          CLIN2.setText("Ce client n'accepte pas de reliquat (les articles non disponibles seront annul\u00e9s de sa commande)");
          CLIN2.setToolTipText("Ce client n'accepte pas de reliquat (les articles non disponibles seront annul\u00e9s de sa commande)");
          CLIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CLIN2.setName("CLIN2");
          OBJ_28.add(CLIN2);
          CLIN2.setBounds(20, 15, 735, 20);

          //---- CLIN1 ----
          CLIN1.setText("Ce client n'accepte pas les livraisons partielles (la livraison est report\u00e9e jusqu'\u00e0 disponibilit\u00e9 compl\u00e8te de la commande)");
          CLIN1.setToolTipText("Ce client n'accepte pas les livraisons partielles (la livraison est report\u00e9e jusqu'\u00e0 disponibilit\u00e9 compl\u00e8te de la commande)");
          CLIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CLIN1.setName("CLIN1");
          OBJ_28.add(CLIN1);
          CLIN1.setBounds(20, 45, 735, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_28.getComponentCount(); i++) {
              Rectangle bounds = OBJ_28.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_28.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_28.setMinimumSize(preferredSize);
            OBJ_28.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 770, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 770, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 770, GroupLayout.PREFERRED_SIZE))
              .addGap(33, 33, 33))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(OBJ_48, GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
              .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- invite ----
      invite.setText("Choix possibles");
      invite.setName("invite");
      invite.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          inviteActionPerformed(e);
        }
      });
      BTD.add(invite);

      //---- aide ----
      aide.setText("Aide en ligne");
      aide.setName("aide");
      aide.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          aideActionPerformed(e);
        }
      });
      BTD.add(aide);
    }

    //---- GenAch ----
    ButtonGroup GenAch = new ButtonGroup();
    GenAch.add(gene0);
    GenAch.add(gene1);
    GenAch.add(gene2);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private JPanel p_contenu;
  private JPanel OBJ_32;
  private JLabel OBJ_35;
  private JLabel OBJ_38;
  private JLabel OBJ_41;
  private JLabel OBJ_33;
  private JLabel OBJ_36;
  private JLabel OBJ_39;
  private XRiTextField CLGEO;
  private XRiTextField CLMEX;
  private XRiTextField CLCAN;
  private JPanel panel2;
  private JRadioButton gene0;
  private JRadioButton gene1;
  private JRadioButton gene2;
  private JCheckBox livr1;
  private JCheckBox livr2;
  private JPanel OBJ_48;
  private JPanel panel1;
  private JLabel OBJ_51;
  private XRiTextField CLOTO;
  private JLabel OBJ_65;
  private XRiTextField CLFRP;
  private JLabel OBJ_68;
  private XRiTextField CLIN14;
  private XRiTextField CLIN24;
  private XRiTextField CLIN25;
  private JLabel OBJ_67;
  private JPanel panel3;
  private RiZoneSortie OBJ_60;
  private RiZoneSortie OBJ_55;
  private JLabel OBJ_61;
  private JLabel OBJ_49;
  private JLabel OBJ_53;
  private JLabel OBJ_56;
  private JLabel OBJ_58;
  private XRiTextField ZPC16R;
  private XRiTextField CLTRA;
  private XRiTextField CLCRT;
  private XRiTextField CLCTR;
  private XRiTextField CLMAG;
  private JLabel OBJ_85;
  private XRiComboBox EBPRP;
  private XRiCheckBox EBVEH;
  private XRiCheckBox EBCHAN;
  private XRiCheckBox EBIN10;
  private XRiCheckBox CLIN30;
  private XRiCheckBox EBREFC;
  private JPanel OBJ_28;
  private XRiCheckBox CLIN2;
  private XRiCheckBox CLIN1;
  private JPopupMenu BTD;
  private JMenuItem invite;
  private JMenuItem aide;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
