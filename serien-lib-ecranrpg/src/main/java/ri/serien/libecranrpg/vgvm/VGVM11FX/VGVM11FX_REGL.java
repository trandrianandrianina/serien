
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_REGL extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM11FX_REGL(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
    setCloseKey("F4");
    
    
    bouton_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Réglements"));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LRG11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRG11@")).trim());
    LRG12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRG12@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    if (oFrameParent instanceof VGVM11FX_A2) {
      ((VGVM11FX_A2) oFrameParent).isEnModeRegl = true;
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    if (oFrameParent instanceof VGVM11FX_A2) {
      ((VGVM11FX_A2) oFrameParent).isEnModeRegl = true;
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
    if (oFrameParent instanceof VGVM11FX_A2) {
      ((VGVM11FX_A2) oFrameParent).isEnModeRegl = false;
    }
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(popupMenu1.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    E1RG1 = new XRiTextField();
    E1RG2 = new XRiTextField();
    E1DTE1 = new XRiCalendrier();
    E1PC1 = new XRiTextField();
    label8 = new JLabel();
    label9 = new JLabel();
    E1PC2 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    E1EC1 = new XRiTextField();
    E1EC2 = new XRiTextField();
    label4 = new JLabel();
    E1DTE2 = new XRiCalendrier();
    label5 = new JLabel();
    E1MRG1 = new XRiTextField();
    E1MRG2 = new XRiTextField();
    LRG11 = new RiZoneSortie();
    LRG12 = new RiZoneSortie();
    popupMenu1 = new JPopupMenu();
    menuItem1 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(1010, 260));
    setPreferredSize(new Dimension(980, 180));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("R\u00e9glements"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- E1RG1 ----
          E1RG1.setHorizontalAlignment(SwingConstants.RIGHT);
          E1RG1.setComponentPopupMenu(popupMenu1);
          E1RG1.setName("E1RG1");
          panel3.add(E1RG1);
          E1RG1.setBounds(40, 60, 34, E1RG1.getPreferredSize().height);

          //---- E1RG2 ----
          E1RG2.setHorizontalAlignment(SwingConstants.RIGHT);
          E1RG2.setComponentPopupMenu(popupMenu1);
          E1RG2.setName("E1RG2");
          panel3.add(E1RG2);
          E1RG2.setBounds(40, 90, 34, E1RG2.getPreferredSize().height);

          //---- E1DTE1 ----
          E1DTE1.setName("E1DTE1");
          panel3.add(E1DTE1);
          E1DTE1.setBounds(120, 60, 105, E1DTE1.getPreferredSize().height);

          //---- E1PC1 ----
          E1PC1.setHorizontalAlignment(SwingConstants.RIGHT);
          E1PC1.setName("E1PC1");
          panel3.add(E1PC1);
          E1PC1.setBounds(230, 60, 30, E1PC1.getPreferredSize().height);

          //---- label8 ----
          label8.setText("%");
          label8.setName("label8");
          panel3.add(label8);
          label8.setBounds(238, 40, 20, 20);

          //---- label9 ----
          label9.setText("Montant");
          label9.setName("label9");
          panel3.add(label9);
          label9.setBounds(270, 40, 60, 20);

          //---- E1PC2 ----
          E1PC2.setHorizontalAlignment(SwingConstants.RIGHT);
          E1PC2.setName("E1PC2");
          panel3.add(E1PC2);
          E1PC2.setBounds(230, 90, 30, E1PC2.getPreferredSize().height);

          //---- label1 ----
          label1.setText("1");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");
          panel3.add(label1);
          label1.setBounds(25, 66, 15, label1.getPreferredSize().height);

          //---- label2 ----
          label2.setText("2");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel3.add(label2);
          label2.setBounds(25, 96, 15, label2.getPreferredSize().height);

          //---- label3 ----
          label3.setText("R\u00e9gl");
          label3.setName("label3");
          panel3.add(label3);
          label3.setBounds(45, 40, 30, 20);

          //---- E1EC1 ----
          E1EC1.setComponentPopupMenu(popupMenu1);
          E1EC1.setName("E1EC1");
          panel3.add(E1EC1);
          E1EC1.setBounds(80, 60, 30, E1EC1.getPreferredSize().height);

          //---- E1EC2 ----
          E1EC2.setComponentPopupMenu(popupMenu1);
          E1EC2.setName("E1EC2");
          panel3.add(E1EC2);
          E1EC2.setBounds(80, 90, 30, E1EC2.getPreferredSize().height);

          //---- label4 ----
          label4.setText("Ech");
          label4.setName("label4");
          panel3.add(label4);
          label4.setBounds(85, 40, 30, 20);

          //---- E1DTE2 ----
          E1DTE2.setName("E1DTE2");
          panel3.add(E1DTE2);
          E1DTE2.setBounds(120, 90, 105, E1DTE2.getPreferredSize().height);

          //---- label5 ----
          label5.setText("Date ech\u00e9ance");
          label5.setName("label5");
          panel3.add(label5);
          label5.setBounds(120, 40, 100, 20);

          //---- E1MRG1 ----
          E1MRG1.setHorizontalAlignment(SwingConstants.RIGHT);
          E1MRG1.setName("E1MRG1");
          panel3.add(E1MRG1);
          E1MRG1.setBounds(265, 60, 120, E1MRG1.getPreferredSize().height);

          //---- E1MRG2 ----
          E1MRG2.setHorizontalAlignment(SwingConstants.RIGHT);
          E1MRG2.setName("E1MRG2");
          panel3.add(E1MRG2);
          E1MRG2.setBounds(265, 90, 120, E1MRG2.getPreferredSize().height);

          //---- LRG11 ----
          LRG11.setText("@LRG11@");
          LRG11.setName("LRG11");
          panel3.add(LRG11);
          LRG11.setBounds(395, 62, 330, LRG11.getPreferredSize().height);

          //---- LRG12 ----
          LRG12.setText("@LRG12@");
          LRG12.setName("LRG12");
          panel3.add(LRG12);
          LRG12.setBounds(395, 92, 330, LRG12.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 786, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");

      //---- menuItem1 ----
      menuItem1.setText("Choix possibles");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem1);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private JPanel p_contenu;
  private JPanel panel3;
  private XRiTextField E1RG1;
  private XRiTextField E1RG2;
  private XRiCalendrier E1DTE1;
  private XRiTextField E1PC1;
  private JLabel label8;
  private JLabel label9;
  private XRiTextField E1PC2;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private XRiTextField E1EC1;
  private XRiTextField E1EC2;
  private JLabel label4;
  private XRiCalendrier E1DTE2;
  private JLabel label5;
  private XRiTextField E1MRG1;
  private XRiTextField E1MRG2;
  private RiZoneSortie LRG11;
  private RiZoneSortie LRG12;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
