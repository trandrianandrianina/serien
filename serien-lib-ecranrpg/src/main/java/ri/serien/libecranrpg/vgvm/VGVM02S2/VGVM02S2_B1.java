
package ri.serien.libecranrpg.vgvm.VGVM02S2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM02S2_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM02S2_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void ARG3AActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_43 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46 = new JLabel();
    INDIND = new RiZoneSortie();
    OBJ_34 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OP01 = new XRiComboBox();
    OP02 = new XRiComboBox();
    OP03 = new XRiComboBox();
    OP04 = new XRiComboBox();
    OP05 = new XRiComboBox();
    OP06 = new XRiComboBox();
    OP07 = new XRiComboBox();
    OP08 = new XRiComboBox();
    OP09 = new XRiComboBox();
    OP10 = new XRiComboBox();
    OP11 = new XRiComboBox();
    OP12 = new XRiComboBox();
    OP13 = new XRiComboBox();
    OP14 = new XRiComboBox();
    O201 = new XRiComboBox();
    O202 = new XRiComboBox();
    O203 = new XRiComboBox();
    O204 = new XRiComboBox();
    O205 = new XRiComboBox();
    O206 = new XRiComboBox();
    O207 = new XRiComboBox();
    O208 = new XRiComboBox();
    O209 = new XRiComboBox();
    O210 = new XRiComboBox();
    O211 = new XRiComboBox();
    O212 = new XRiComboBox();
    O213 = new XRiComboBox();
    O214 = new XRiComboBox();
    CD01 = new XRiTextField();
    CD02 = new XRiTextField();
    CD03 = new XRiTextField();
    CD04 = new XRiTextField();
    CD05 = new XRiTextField();
    CD06 = new XRiTextField();
    CD07 = new XRiTextField();
    CD08 = new XRiTextField();
    CD09 = new XRiTextField();
    CD10 = new XRiTextField();
    CD11 = new XRiTextField();
    CD12 = new XRiTextField();
    CD13 = new XRiTextField();
    CD14 = new XRiTextField();
    IN01 = new XRiSpinner();
    IN02 = new XRiSpinner();
    IN03 = new XRiSpinner();
    IN04 = new XRiSpinner();
    IN05 = new XRiSpinner();
    IN06 = new XRiSpinner();
    IN07 = new XRiSpinner();
    IN08 = new XRiSpinner();
    IN09 = new XRiSpinner();
    IN10 = new XRiSpinner();
    IN11 = new XRiSpinner();
    IN12 = new XRiSpinner();
    IN13 = new XRiSpinner();
    IN14 = new XRiSpinner();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    S2LIB = new XRiTextField();
    S2CAR = new XRiTextField();
    S2DEC = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_43 ----
          OBJ_43.setText("Etablissement");
          OBJ_43.setName("OBJ_43");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");

          //---- OBJ_34 ----
          OBJ_34.setText("Formules \u00e9dition statistiques");
          OBJ_34.setFont(new Font("sansserif", Font.BOLD, 12));
          OBJ_34.setName("OBJ_34");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 193, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(309, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Signification de la formule");
              riSousMenu_bt7.setToolTipText("Signification de la formule");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 280));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Formule"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OP01 ----
            OP01.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP01.setComponentPopupMenu(BTD);
            OP01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP01.setName("OP01");
            panel2.add(OP01);
            OP01.setBounds(221, 30, 50, 26);

            //---- OP02 ----
            OP02.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP02.setComponentPopupMenu(BTD);
            OP02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP02.setName("OP02");
            panel2.add(OP02);
            OP02.setBounds(484, 30, 50, 26);

            //---- OP03 ----
            OP03.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP03.setComponentPopupMenu(BTD);
            OP03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP03.setName("OP03");
            panel2.add(OP03);
            OP03.setBounds(747, 30, 50, OP03.getPreferredSize().height);

            //---- OP04 ----
            OP04.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP04.setComponentPopupMenu(BTD);
            OP04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP04.setName("OP04");
            panel2.add(OP04);
            OP04.setBounds(221, 56, 50, 26);

            //---- OP05 ----
            OP05.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP05.setComponentPopupMenu(BTD);
            OP05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP05.setName("OP05");
            panel2.add(OP05);
            OP05.setBounds(485, 56, 50, 26);

            //---- OP06 ----
            OP06.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP06.setComponentPopupMenu(BTD);
            OP06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP06.setName("OP06");
            panel2.add(OP06);
            OP06.setBounds(747, 56, 50, 26);

            //---- OP07 ----
            OP07.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP07.setComponentPopupMenu(BTD);
            OP07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP07.setName("OP07");
            panel2.add(OP07);
            OP07.setBounds(221, 82, 50, 26);

            //---- OP08 ----
            OP08.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP08.setComponentPopupMenu(BTD);
            OP08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP08.setName("OP08");
            panel2.add(OP08);
            OP08.setBounds(485, 82, 50, 26);

            //---- OP09 ----
            OP09.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP09.setComponentPopupMenu(BTD);
            OP09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP09.setName("OP09");
            panel2.add(OP09);
            OP09.setBounds(747, 82, 50, 26);

            //---- OP10 ----
            OP10.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP10.setComponentPopupMenu(BTD);
            OP10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP10.setName("OP10");
            panel2.add(OP10);
            OP10.setBounds(221, 108, 50, 26);

            //---- OP11 ----
            OP11.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP11.setComponentPopupMenu(BTD);
            OP11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP11.setName("OP11");
            panel2.add(OP11);
            OP11.setBounds(485, 108, 50, 26);

            //---- OP12 ----
            OP12.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP12.setComponentPopupMenu(BTD);
            OP12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP12.setName("OP12");
            panel2.add(OP12);
            OP12.setBounds(747, 108, 50, 26);

            //---- OP13 ----
            OP13.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP13.setComponentPopupMenu(BTD);
            OP13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP13.setName("OP13");
            panel2.add(OP13);
            OP13.setBounds(221, 134, 50, 26);

            //---- OP14 ----
            OP14.setModel(new DefaultComboBoxModel(new String[] {
              "+",
              "-",
              "*",
              "/",
              "%",
              "M"
            }));
            OP14.setComponentPopupMenu(BTD);
            OP14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OP14.setName("OP14");
            panel2.add(OP14);
            OP14.setBounds(485, 134, 50, 26);

            //---- O201 ----
            O201.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O201.setComponentPopupMenu(BTD);
            O201.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O201.setName("O201");
            panel2.add(O201);
            O201.setBounds(15, 30, 55, 26);

            //---- O202 ----
            O202.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O202.setComponentPopupMenu(BTD);
            O202.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O202.setName("O202");
            panel2.add(O202);
            O202.setBounds(278, 30, 55, 26);

            //---- O203 ----
            O203.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O203.setComponentPopupMenu(BTD);
            O203.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O203.setName("O203");
            panel2.add(O203);
            O203.setBounds(541, 30, 55, 26);

            //---- O204 ----
            O204.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O204.setComponentPopupMenu(BTD);
            O204.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O204.setName("O204");
            panel2.add(O204);
            O204.setBounds(15, 56, 55, 26);

            //---- O205 ----
            O205.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O205.setComponentPopupMenu(BTD);
            O205.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O205.setName("O205");
            panel2.add(O205);
            O205.setBounds(278, 56, 55, 26);

            //---- O206 ----
            O206.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O206.setComponentPopupMenu(BTD);
            O206.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O206.setName("O206");
            panel2.add(O206);
            O206.setBounds(541, 56, 55, 26);

            //---- O207 ----
            O207.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O207.setComponentPopupMenu(BTD);
            O207.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O207.setName("O207");
            panel2.add(O207);
            O207.setBounds(15, 82, 55, 26);

            //---- O208 ----
            O208.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O208.setComponentPopupMenu(BTD);
            O208.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O208.setName("O208");
            panel2.add(O208);
            O208.setBounds(278, 82, 55, 26);

            //---- O209 ----
            O209.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O209.setComponentPopupMenu(BTD);
            O209.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O209.setName("O209");
            panel2.add(O209);
            O209.setBounds(541, 82, 55, 26);

            //---- O210 ----
            O210.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O210.setComponentPopupMenu(BTD);
            O210.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O210.setName("O210");
            panel2.add(O210);
            O210.setBounds(15, 108, 55, 26);

            //---- O211 ----
            O211.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O211.setComponentPopupMenu(BTD);
            O211.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O211.setName("O211");
            panel2.add(O211);
            O211.setBounds(278, 108, 55, 26);

            //---- O212 ----
            O212.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O212.setComponentPopupMenu(BTD);
            O212.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O212.setName("O212");
            panel2.add(O212);
            O212.setBounds(541, 108, 55, 26);

            //---- O213 ----
            O213.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O213.setComponentPopupMenu(BTD);
            O213.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O213.setName("O213");
            panel2.add(O213);
            O213.setBounds(15, 134, 55, 26);

            //---- O214 ----
            O214.setModel(new DefaultComboBoxModel(new String[] {
              "M",
              "%"
            }));
            O214.setComponentPopupMenu(BTD);
            O214.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            O214.setName("O214");
            panel2.add(O214);
            O214.setBounds(278, 134, 55, 26);

            //---- CD01 ----
            CD01.setComponentPopupMenu(BTD);
            CD01.setName("CD01");
            panel2.add(CD01);
            CD01.setBounds(77, 29, 80, CD01.getPreferredSize().height);

            //---- CD02 ----
            CD02.setComponentPopupMenu(BTD);
            CD02.setName("CD02");
            panel2.add(CD02);
            CD02.setBounds(340, 29, 80, CD02.getPreferredSize().height);

            //---- CD03 ----
            CD03.setComponentPopupMenu(BTD);
            CD03.setName("CD03");
            panel2.add(CD03);
            CD03.setBounds(603, 29, 80, CD03.getPreferredSize().height);

            //---- CD04 ----
            CD04.setComponentPopupMenu(BTD);
            CD04.setName("CD04");
            panel2.add(CD04);
            CD04.setBounds(77, 55, 80, CD04.getPreferredSize().height);

            //---- CD05 ----
            CD05.setComponentPopupMenu(BTD);
            CD05.setName("CD05");
            panel2.add(CD05);
            CD05.setBounds(340, 55, 80, CD05.getPreferredSize().height);

            //---- CD06 ----
            CD06.setComponentPopupMenu(BTD);
            CD06.setName("CD06");
            panel2.add(CD06);
            CD06.setBounds(603, 55, 80, CD06.getPreferredSize().height);

            //---- CD07 ----
            CD07.setComponentPopupMenu(BTD);
            CD07.setName("CD07");
            panel2.add(CD07);
            CD07.setBounds(77, 81, 80, CD07.getPreferredSize().height);

            //---- CD08 ----
            CD08.setComponentPopupMenu(BTD);
            CD08.setName("CD08");
            panel2.add(CD08);
            CD08.setBounds(340, 81, 80, CD08.getPreferredSize().height);

            //---- CD09 ----
            CD09.setComponentPopupMenu(BTD);
            CD09.setName("CD09");
            panel2.add(CD09);
            CD09.setBounds(603, 81, 80, CD09.getPreferredSize().height);

            //---- CD10 ----
            CD10.setComponentPopupMenu(BTD);
            CD10.setName("CD10");
            panel2.add(CD10);
            CD10.setBounds(77, 107, 80, CD10.getPreferredSize().height);

            //---- CD11 ----
            CD11.setComponentPopupMenu(BTD);
            CD11.setName("CD11");
            panel2.add(CD11);
            CD11.setBounds(340, 107, 80, CD11.getPreferredSize().height);

            //---- CD12 ----
            CD12.setComponentPopupMenu(BTD);
            CD12.setName("CD12");
            panel2.add(CD12);
            CD12.setBounds(603, 107, 80, CD12.getPreferredSize().height);

            //---- CD13 ----
            CD13.setComponentPopupMenu(BTD);
            CD13.setName("CD13");
            panel2.add(CD13);
            CD13.setBounds(77, 133, 80, CD13.getPreferredSize().height);

            //---- CD14 ----
            CD14.setComponentPopupMenu(BTD);
            CD14.setName("CD14");
            panel2.add(CD14);
            CD14.setBounds(340, 133, 80, CD14.getPreferredSize().height);

            //---- IN01 ----
            IN01.setComponentPopupMenu(BTD);
            IN01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN01.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN01.setName("IN01");
            panel2.add(IN01);
            IN01.setBounds(164, 29, 50, IN01.getPreferredSize().height);

            //---- IN02 ----
            IN02.setComponentPopupMenu(BTD);
            IN02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN02.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN02.setName("IN02");
            panel2.add(IN02);
            IN02.setBounds(427, 29, 50, IN02.getPreferredSize().height);

            //---- IN03 ----
            IN03.setComponentPopupMenu(BTD);
            IN03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN03.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN03.setName("IN03");
            panel2.add(IN03);
            IN03.setBounds(690, 29, 50, IN03.getPreferredSize().height);

            //---- IN04 ----
            IN04.setComponentPopupMenu(BTD);
            IN04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN04.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN04.setName("IN04");
            panel2.add(IN04);
            IN04.setBounds(164, 55, 50, IN04.getPreferredSize().height);

            //---- IN05 ----
            IN05.setComponentPopupMenu(BTD);
            IN05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN05.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN05.setName("IN05");
            panel2.add(IN05);
            IN05.setBounds(427, 55, 50, IN05.getPreferredSize().height);

            //---- IN06 ----
            IN06.setComponentPopupMenu(BTD);
            IN06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN06.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN06.setName("IN06");
            panel2.add(IN06);
            IN06.setBounds(690, 55, 50, IN06.getPreferredSize().height);

            //---- IN07 ----
            IN07.setComponentPopupMenu(BTD);
            IN07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN07.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN07.setName("IN07");
            panel2.add(IN07);
            IN07.setBounds(164, 81, 50, IN07.getPreferredSize().height);

            //---- IN08 ----
            IN08.setComponentPopupMenu(BTD);
            IN08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN08.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN08.setName("IN08");
            panel2.add(IN08);
            IN08.setBounds(427, 81, 50, IN08.getPreferredSize().height);

            //---- IN09 ----
            IN09.setComponentPopupMenu(BTD);
            IN09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN09.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN09.setName("IN09");
            panel2.add(IN09);
            IN09.setBounds(690, 81, 50, IN09.getPreferredSize().height);

            //---- IN10 ----
            IN10.setComponentPopupMenu(BTD);
            IN10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN10.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN10.setName("IN10");
            panel2.add(IN10);
            IN10.setBounds(164, 107, 50, IN10.getPreferredSize().height);

            //---- IN11 ----
            IN11.setComponentPopupMenu(BTD);
            IN11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN11.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN11.setName("IN11");
            panel2.add(IN11);
            IN11.setBounds(427, 107, 50, IN11.getPreferredSize().height);

            //---- IN12 ----
            IN12.setComponentPopupMenu(BTD);
            IN12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN12.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN12.setName("IN12");
            panel2.add(IN12);
            IN12.setBounds(690, 107, 50, IN12.getPreferredSize().height);

            //---- IN13 ----
            IN13.setComponentPopupMenu(BTD);
            IN13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN13.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN13.setName("IN13");
            panel2.add(IN13);
            IN13.setBounds(164, 133, 50, IN13.getPreferredSize().height);

            //---- IN14 ----
            IN14.setComponentPopupMenu(BTD);
            IN14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            IN14.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"}) {
              { setValue("1"); }
            });
            IN14.setName("IN14");
            panel2.add(IN14);
            IN14.setBounds(427, 133, 50, IN14.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(13, 91, 824, 175);

          //---- label1 ----
          label1.setText("Libell\u00e9 de la formule");
          label1.setName("label1");
          p_contenu.add(label1);
          label1.setBounds(40, 19, 155, 28);

          //---- label2 ----
          label2.setText("nombre de caract\u00e8res");
          label2.setName("label2");
          p_contenu.add(label2);
          label2.setBounds(40, 55, 155, 28);

          //---- label3 ----
          label3.setText("dont d\u00e9cimales");
          label3.setName("label3");
          p_contenu.add(label3);
          label3.setBounds(240, 55, 110, 28);

          //---- S2LIB ----
          S2LIB.setName("S2LIB");
          p_contenu.add(S2LIB);
          S2LIB.setBounds(195, 19, 314, S2LIB.getPreferredSize().height);

          //---- S2CAR ----
          S2CAR.setName("S2CAR");
          p_contenu.add(S2CAR);
          S2CAR.setBounds(195, 55, 34, S2CAR.getPreferredSize().height);

          //---- S2DEC ----
          S2DEC.setName("S2DEC");
          p_contenu.add(S2DEC);
          S2DEC.setBounds(350, 55, 24, S2DEC.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_43;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46;
  private RiZoneSortie INDIND;
  private JLabel OBJ_34;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiComboBox OP01;
  private XRiComboBox OP02;
  private XRiComboBox OP03;
  private XRiComboBox OP04;
  private XRiComboBox OP05;
  private XRiComboBox OP06;
  private XRiComboBox OP07;
  private XRiComboBox OP08;
  private XRiComboBox OP09;
  private XRiComboBox OP10;
  private XRiComboBox OP11;
  private XRiComboBox OP12;
  private XRiComboBox OP13;
  private XRiComboBox OP14;
  private XRiComboBox O201;
  private XRiComboBox O202;
  private XRiComboBox O203;
  private XRiComboBox O204;
  private XRiComboBox O205;
  private XRiComboBox O206;
  private XRiComboBox O207;
  private XRiComboBox O208;
  private XRiComboBox O209;
  private XRiComboBox O210;
  private XRiComboBox O211;
  private XRiComboBox O212;
  private XRiComboBox O213;
  private XRiComboBox O214;
  private XRiTextField CD01;
  private XRiTextField CD02;
  private XRiTextField CD03;
  private XRiTextField CD04;
  private XRiTextField CD05;
  private XRiTextField CD06;
  private XRiTextField CD07;
  private XRiTextField CD08;
  private XRiTextField CD09;
  private XRiTextField CD10;
  private XRiTextField CD11;
  private XRiTextField CD12;
  private XRiTextField CD13;
  private XRiTextField CD14;
  private XRiSpinner IN01;
  private XRiSpinner IN02;
  private XRiSpinner IN03;
  private XRiSpinner IN04;
  private XRiSpinner IN05;
  private XRiSpinner IN06;
  private XRiSpinner IN07;
  private XRiSpinner IN08;
  private XRiSpinner IN09;
  private XRiSpinner IN10;
  private XRiSpinner IN11;
  private XRiSpinner IN12;
  private XRiSpinner IN13;
  private XRiSpinner IN14;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private XRiTextField S2LIB;
  private XRiTextField S2CAR;
  private XRiTextField S2DEC;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
