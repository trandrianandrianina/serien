
package ri.serien.libecranrpg.vgvm.VGVM032F;
// MON PACKAGE

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class VGVM032F_FAC extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private boolean isConsult = false;
  
  public VGVM032F_FAC(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    isConsult = lexique.isTrue("53");
    
    // Valeurs ++++++++++++++++++++++++++++++++++++++++++++++++
    WCIVF.setText(lexique.HostFieldGetData("WCIVF").trim());
    WNOMFR.setText(lexique.HostFieldGetData("WNOMFR").trim());
    WCPLF.setText(lexique.HostFieldGetData("WCPLF").trim());
    WRUEF.setText(lexique.HostFieldGetData("WRUEF").trim());
    WLOCF.setText(lexique.HostFieldGetData("WLOCF").trim());
    WCDPF.setText(lexique.HostFieldGetData("WCDPF").trim());
    WVILF.setText(lexique.HostFieldGetData("WVILF").trim());
    WPAYF.setText(lexique.HostFieldGetData("WPAYF").trim());
    WCOPF.setText(lexique.HostFieldGetData("WCOPF").trim());
    
    // consultation ou modification
    WCIVF.setEditable(!isConsult);
    WNOMFR.setEditable(!isConsult);
    WCPLF.setEditable(!isConsult);
    WRUEF.setEditable(!isConsult);
    WLOCF.setEditable(!isConsult);
    WCDPF.setEditable(!isConsult);
    WVILF.setEditable(!isConsult);
    WPAYF.setEditable(!isConsult);
    WCOPF.setEditable(!isConsult);
    
    bouton_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Adresse client"));
  }
  
  public void getData() {
    lexique.HostFieldPutData("WCIVF", 0, WCIVF.getText());
    lexique.HostFieldPutData("WNOMFR", 0, WNOMFR.getText());
    lexique.HostFieldPutData("WCPLF", 0, WCPLF.getText());
    lexique.HostFieldPutData("WRUEF", 0, WRUEF.getText());
    lexique.HostFieldPutData("WLOCF", 0, WLOCF.getText());
    lexique.HostFieldPutData("WCDPF", 0, WCDPF.getText());
    lexique.HostFieldPutData("WVILF", 0, WVILF.getText());
    lexique.HostFieldPutData("WPAYF", 0, WPAYF.getText());
    lexique.HostFieldPutData("WCOPF", 0, WCOPF.getText());
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    getData();
    dispose();
  }
  
  public JDialog getDialog1() {
    return this;
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    this.dispose();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    this.setVisible(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    WNOMFR = new XRiTextField();
    WCPLF = new XRiTextField();
    WRUEF = new XRiTextField();
    WLOCF = new XRiTextField();
    WPAYF = new XRiTextField();
    WVILF = new XRiTextField();
    OBJ_54 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    WCIVF = new XRiTextField();
    WCDPF = new XRiTextField();
    WCOPF = new XRiTextField();
    BTD = new JPopupMenu();
    menuItem1 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(770, 310));
    setResizable(false);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        
        // ======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Adresse de facturation"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          
          // ---- WNOMFR ----
          WNOMFR.setComponentPopupMenu(BTD);
          WNOMFR.setName("WNOMFR");
          
          // ---- WCPLF ----
          WCPLF.setComponentPopupMenu(BTD);
          WCPLF.setName("WCPLF");
          
          // ---- WRUEF ----
          WRUEF.setComponentPopupMenu(BTD);
          WRUEF.setName("WRUEF");
          
          // ---- WLOCF ----
          WLOCF.setComponentPopupMenu(BTD);
          WLOCF.setName("WLOCF");
          
          // ---- WPAYF ----
          WPAYF.setComponentPopupMenu(BTD);
          WPAYF.setName("WPAYF");
          
          // ---- WVILF ----
          WVILF.setComponentPopupMenu(BTD);
          WVILF.setName("WVILF");
          
          // ---- OBJ_54 ----
          OBJ_54.setText("Nom");
          OBJ_54.setName("OBJ_54");
          
          // ---- OBJ_55 ----
          OBJ_55.setText("Adresse");
          OBJ_55.setName("OBJ_55");
          
          // ---- OBJ_57 ----
          OBJ_57.setText("Localit\u00e9");
          OBJ_57.setName("OBJ_57");
          
          // ---- OBJ_59 ----
          OBJ_59.setText("Code postal");
          OBJ_59.setName("OBJ_59");
          
          // ---- OBJ_60 ----
          OBJ_60.setText("Pays");
          OBJ_60.setName("OBJ_60");
          
          // ---- WCIVF ----
          WCIVF.setComponentPopupMenu(BTD);
          WCIVF.setName("WCIVF");
          
          // ---- WCDPF ----
          WCDPF.setComponentPopupMenu(BTD);
          WCDPF.setName("WCDPF");
          
          // ---- WCOPF ----
          WCOPF.setComponentPopupMenu(BTD);
          WCOPF.setName("WCOPF");
          
          GroupLayout panel3Layout = new GroupLayout(panel3);
          panel3.setLayout(panel3Layout);
          panel3Layout.setHorizontalGroup(panel3Layout.createParallelGroup()
              .addGroup(panel3Layout.createSequentialGroup().addGap(14, 14, 14)
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WCIVF, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WNOMFR, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel3Layout.createSequentialGroup().addGap(199, 199, 199).addComponent(WCPLF, GroupLayout.PREFERRED_SIZE, 310,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(panel3Layout.createSequentialGroup().addGap(14, 14, 14)
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WRUEF, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel3Layout.createSequentialGroup().addGap(14, 14, 14)
                  .addComponent(OBJ_57, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WLOCF, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel3Layout.createSequentialGroup().addGap(14, 14, 14)
                  .addComponent(OBJ_59, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WCDPF, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WVILF, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel3Layout.createSequentialGroup().addGap(14, 14, 14)
                  .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WPAYF, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                  .addComponent(WCOPF, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)));
          panel3Layout
              .setVerticalGroup(
                  panel3Layout.createParallelGroup()
                      .addGroup(panel3Layout.createSequentialGroup().addGap(15, 15, 15)
                          .addGroup(panel3Layout.createParallelGroup()
                              .addGroup(panel3Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_54,
                                  GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                              .addComponent(WCIVF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addComponent(WNOMFR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGap(3, 3, 3)
                          .addComponent(WCPLF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGap(3, 3, 3)
                          .addGroup(panel3Layout.createParallelGroup()
                              .addGroup(panel3Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_55,
                                  GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                              .addComponent(WRUEF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGap(3, 3, 3)
                          .addGroup(panel3Layout.createParallelGroup()
                              .addGroup(panel3Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_57,
                                  GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                              .addComponent(WLOCF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGap(3, 3, 3)
                          .addGroup(panel3Layout.createParallelGroup()
                              .addGroup(panel3Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_59,
                                  GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                              .addComponent(WCDPF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addComponent(WVILF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGap(3, 3, 3)
                          .addGroup(panel3Layout.createParallelGroup()
                              .addGroup(panel3Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_60,
                                  GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                              .addComponent(WPAYF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addComponent(WCOPF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addContainerGap().addComponent(panel3, GroupLayout.DEFAULT_SIZE, 568, Short.MAX_VALUE).addContainerGap()));
        p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addContainerGap().addComponent(panel3, GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE).addContainerGap()));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    contentPane.add(p_principal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- menuItem1 ----
      menuItem1.setText("Choix possibles");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD.add(menuItem1);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel3;
  private XRiTextField WNOMFR;
  private XRiTextField WCPLF;
  private XRiTextField WRUEF;
  private XRiTextField WLOCF;
  private XRiTextField WPAYF;
  private XRiTextField WVILF;
  private JLabel OBJ_54;
  private JLabel OBJ_55;
  private JLabel OBJ_57;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private XRiTextField WCIVF;
  private XRiTextField WCDPF;
  private XRiTextField WCOPF;
  private JPopupMenu BTD;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
