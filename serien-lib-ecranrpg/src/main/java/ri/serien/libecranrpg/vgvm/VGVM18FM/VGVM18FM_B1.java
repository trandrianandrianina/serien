
package ri.serien.libecranrpg.vgvm.VGVM18FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM18FM_B1 extends SNPanelEcranRPG implements ioFrame {
  private boolean isConsultation = true;
  
  public VGVM18FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WUNCND.setValeursSelection("1", " ");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCNV@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel1.setBorder(new TitledBorder("Saisie détaillée"));
    
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Articles liés @TITRE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isConsultation = lexique.isTrue("(N91) AND (N92)");
    
    riSousMenu1.setVisible(lexique.isTrue("54"));
    riSousMenu_bt1.setVisible(lexique.isTrue("54"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    if (lexique.TranslationTable(interpreteurD.analyseExpression("@TITRE@")).trim().equalsIgnoreCase("Automatiques")) {
      menuItem2.setText("Transformer en article lié proposé");
    }
    else {
      menuItem2.setText("Transformer en article lié automatiquement");
    }
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("WETB"));
    
    // Articles
    ARL01.setSession(getSession());
    ARL01.setIdEtablissement(idEtablissement);
    ARL01.charger(false);
    ARL01.setSelectionParChampRPG(lexique, "ARL01");
    ARL01.setEnabled(!isConsultation);
    
    ARL02.setSession(getSession());
    ARL02.setIdEtablissement(idEtablissement);
    ARL02.charger(false);
    ARL02.setSelectionParChampRPG(lexique, "ARL02");
    ARL02.setEnabled(!isConsultation);
    
    ARL03.setSession(getSession());
    ARL03.setIdEtablissement(idEtablissement);
    ARL03.charger(false);
    ARL03.setSelectionParChampRPG(lexique, "ARL03");
    ARL03.setEnabled(!isConsultation);
    
    ARL04.setSession(getSession());
    ARL04.setIdEtablissement(idEtablissement);
    ARL04.charger(false);
    ARL04.setSelectionParChampRPG(lexique, "ARL04");
    ARL04.setEnabled(!isConsultation);
    
    ARL05.setSession(getSession());
    ARL05.setIdEtablissement(idEtablissement);
    ARL05.charger(false);
    ARL05.setSelectionParChampRPG(lexique, "ARL05");
    ARL05.setEnabled(!isConsultation);
    
    ARL06.setSession(getSession());
    ARL06.setIdEtablissement(idEtablissement);
    ARL06.charger(false);
    ARL06.setSelectionParChampRPG(lexique, "ARL06");
    ARL06.setEnabled(!isConsultation);
    
    ARL07.setSession(getSession());
    ARL07.setIdEtablissement(idEtablissement);
    ARL07.charger(false);
    ARL07.setSelectionParChampRPG(lexique, "ARL07");
    ARL07.setEnabled(!isConsultation);
    
    ARL08.setSession(getSession());
    ARL08.setIdEtablissement(idEtablissement);
    ARL08.charger(false);
    ARL08.setSelectionParChampRPG(lexique, "ARL08");
    ARL08.setEnabled(!isConsultation);
    
    ARL09.setSession(getSession());
    ARL09.setIdEtablissement(idEtablissement);
    ARL09.charger(false);
    ARL09.setSelectionParChampRPG(lexique, "ARL09");
    ARL09.setEnabled(!isConsultation);
    
    ARL10.setSession(getSession());
    ARL10.setIdEtablissement(idEtablissement);
    ARL10.charger(false);
    ARL10.setSelectionParChampRPG(lexique, "ARL10");
    ARL10.setEnabled(!isConsultation);
    
    ARL11.setSession(getSession());
    ARL11.setIdEtablissement(idEtablissement);
    ARL11.charger(false);
    ARL11.setSelectionParChampRPG(lexique, "ARL11");
    ARL11.setEnabled(!isConsultation);
    
    ARL12.setSession(getSession());
    ARL12.setIdEtablissement(idEtablissement);
    ARL12.charger(false);
    ARL12.setSelectionParChampRPG(lexique, "ARL12");
    ARL12.setEnabled(!isConsultation);
    
    ARL13.setSession(getSession());
    ARL13.setIdEtablissement(idEtablissement);
    ARL13.charger(false);
    ARL13.setSelectionParChampRPG(lexique, "ARL13");
    ARL13.setEnabled(!isConsultation);
    
    ARL14.setSession(getSession());
    ARL14.setIdEtablissement(idEtablissement);
    ARL14.charger(false);
    ARL14.setSelectionParChampRPG(lexique, "ARL14");
    ARL14.setEnabled(!isConsultation);
    
    ARL15.setSession(getSession());
    ARL15.setIdEtablissement(idEtablissement);
    ARL15.charger(false);
    ARL15.setSelectionParChampRPG(lexique, "ARL15");
    ARL15.setEnabled(!isConsultation);
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    ARL01.renseignerChampRPG(lexique, "ARL01");
    ARL02.renseignerChampRPG(lexique, "ARL02");
    ARL03.renseignerChampRPG(lexique, "ARL03");
    ARL04.renseignerChampRPG(lexique, "ARL04");
    ARL05.renseignerChampRPG(lexique, "ARL05");
    ARL06.renseignerChampRPG(lexique, "ARL06");
    ARL07.renseignerChampRPG(lexique, "ARL07");
    ARL08.renseignerChampRPG(lexique, "ARL08");
    ARL09.renseignerChampRPG(lexique, "ARL09");
    ARL10.renseignerChampRPG(lexique, "ARL10");
    ARL11.renseignerChampRPG(lexique, "ARL11");
    ARL12.renseignerChampRPG(lexique, "ARL12");
    ARL13.renseignerChampRPG(lexique, "ARL13");
    ARL14.renseignerChampRPG(lexique, "ARL14");
    ARL15.renseignerChampRPG(lexique, "ARL15");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    riZoneSortie1 = new RiZoneSortie();
    label1 = new JLabel();
    riZoneSortie2 = new RiZoneSortie();
    WART = new XRiTextField();
    label2 = new JLabel();
    riZoneSortie3 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label3 = new JLabel();
    QT101 = new XRiTextField();
    QT102 = new XRiTextField();
    QT103 = new XRiTextField();
    QT104 = new XRiTextField();
    QT105 = new XRiTextField();
    QT106 = new XRiTextField();
    QT107 = new XRiTextField();
    QT108 = new XRiTextField();
    QT109 = new XRiTextField();
    QT110 = new XRiTextField();
    QT111 = new XRiTextField();
    QT112 = new XRiTextField();
    QT113 = new XRiTextField();
    QT114 = new XRiTextField();
    QT115 = new XRiTextField();
    label6 = new JLabel();
    TG101 = new XRiTextField();
    TG102 = new XRiTextField();
    TG103 = new XRiTextField();
    TG104 = new XRiTextField();
    TG105 = new XRiTextField();
    TG106 = new XRiTextField();
    TG107 = new XRiTextField();
    TG108 = new XRiTextField();
    TG109 = new XRiTextField();
    TG110 = new XRiTextField();
    TG111 = new XRiTextField();
    TG112 = new XRiTextField();
    TG113 = new XRiTextField();
    TG114 = new XRiTextField();
    TG115 = new XRiTextField();
    PR101 = new XRiTextField();
    PR102 = new XRiTextField();
    PR103 = new XRiTextField();
    PR104 = new XRiTextField();
    PR105 = new XRiTextField();
    PR106 = new XRiTextField();
    label10 = new JLabel();
    PR107 = new XRiTextField();
    PR108 = new XRiTextField();
    PR109 = new XRiTextField();
    PR110 = new XRiTextField();
    PR111 = new XRiTextField();
    PR112 = new XRiTextField();
    PR113 = new XRiTextField();
    PR114 = new XRiTextField();
    PR115 = new XRiTextField();
    QM101 = new XRiTextField();
    QM102 = new XRiTextField();
    QM103 = new XRiTextField();
    QM104 = new XRiTextField();
    QM105 = new XRiTextField();
    QM106 = new XRiTextField();
    QM107 = new XRiTextField();
    QM108 = new XRiTextField();
    QM109 = new XRiTextField();
    QM110 = new XRiTextField();
    QM111 = new XRiTextField();
    QM112 = new XRiTextField();
    QM113 = new XRiTextField();
    QM114 = new XRiTextField();
    QM115 = new XRiTextField();
    label11 = new JLabel();
    label9 = new JLabel();
    QT201 = new XRiTextField();
    QT202 = new XRiTextField();
    QT203 = new XRiTextField();
    QT204 = new XRiTextField();
    QT205 = new XRiTextField();
    QT206 = new XRiTextField();
    QT207 = new XRiTextField();
    QT208 = new XRiTextField();
    QT209 = new XRiTextField();
    QT210 = new XRiTextField();
    QT211 = new XRiTextField();
    QT212 = new XRiTextField();
    QT213 = new XRiTextField();
    QT214 = new XRiTextField();
    QT215 = new XRiTextField();
    TG201 = new XRiTextField();
    TG202 = new XRiTextField();
    TG203 = new XRiTextField();
    TG204 = new XRiTextField();
    TG205 = new XRiTextField();
    TG206 = new XRiTextField();
    TG207 = new XRiTextField();
    TG208 = new XRiTextField();
    TG209 = new XRiTextField();
    TG210 = new XRiTextField();
    TG211 = new XRiTextField();
    TG212 = new XRiTextField();
    TG213 = new XRiTextField();
    TG214 = new XRiTextField();
    TG215 = new XRiTextField();
    PR201 = new XRiTextField();
    PR202 = new XRiTextField();
    PR203 = new XRiTextField();
    PR204 = new XRiTextField();
    PR205 = new XRiTextField();
    PR206 = new XRiTextField();
    PR207 = new XRiTextField();
    PR208 = new XRiTextField();
    PR209 = new XRiTextField();
    PR210 = new XRiTextField();
    PR211 = new XRiTextField();
    PR212 = new XRiTextField();
    PR213 = new XRiTextField();
    PR214 = new XRiTextField();
    PR215 = new XRiTextField();
    label12 = new JLabel();
    QM201 = new XRiTextField();
    QM202 = new XRiTextField();
    QM203 = new XRiTextField();
    QM204 = new XRiTextField();
    QM205 = new XRiTextField();
    QM206 = new XRiTextField();
    QM207 = new XRiTextField();
    QM208 = new XRiTextField();
    QM209 = new XRiTextField();
    QM210 = new XRiTextField();
    QM211 = new XRiTextField();
    QM212 = new XRiTextField();
    QM213 = new XRiTextField();
    QM214 = new XRiTextField();
    QM215 = new XRiTextField();
    label13 = new JLabel();
    WDD1 = new XRiCalendrier();
    label5 = new JLabel();
    WDF1 = new XRiCalendrier();
    WDD2 = new XRiCalendrier();
    label8 = new JLabel();
    WDF2 = new XRiCalendrier();
    WUNCND = new XRiCheckBox();
    separator1 = compFactory.createSeparator("P\u00e9riode 2");
    separator2 = compFactory.createSeparator("P\u00e9riode 1");
    ARL01 = new SNArticle();
    ARL02 = new SNArticle();
    ARL03 = new SNArticle();
    ARL04 = new SNArticle();
    ARL05 = new SNArticle();
    ARL06 = new SNArticle();
    ARL07 = new SNArticle();
    ARL08 = new SNArticle();
    ARL09 = new SNArticle();
    ARL10 = new SNArticle();
    ARL11 = new SNArticle();
    ARL12 = new SNArticle();
    ARL13 = new SNArticle();
    ARL14 = new SNArticle();
    ARL15 = new SNArticle();
    BTD = new JPopupMenu();
    menuItem2 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1120, 710));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Articles li\u00e9s");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(850, 32));
          p_tete_gauche.setMinimumSize(new Dimension(850, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- riZoneSortie1 ----
          riZoneSortie1.setText("@WETB@");
          riZoneSortie1.setOpaque(false);
          riZoneSortie1.setName("riZoneSortie1");
          
          // ---- label1 ----
          label1.setText("Etablissement");
          label1.setName("label1");
          
          // ---- riZoneSortie2 ----
          riZoneSortie2.setText("@WCNV@");
          riZoneSortie2.setOpaque(false);
          riZoneSortie2.setName("riZoneSortie2");
          
          // ---- WART ----
          WART.setName("WART");
          
          // ---- label2 ----
          label2.setText("Article parent");
          label2.setName("label2");
          
          // ---- riZoneSortie3 ----
          riZoneSortie3.setText("@WLIB@");
          riZoneSortie3.setOpaque(false);
          riZoneSortie3.setName("riZoneSortie3");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(label1, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(riZoneSortie2, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(35, 35, 35)
                  .addComponent(label2, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WART, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(riZoneSortie3, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(label1, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(riZoneSortie1,
                  GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(riZoneSortie2,
                  GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(label2, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(WART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(riZoneSortie3,
                  GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("P\u00e9riode 2 vers p\u00e9riode 1");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");
              
              // ---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Saisie simplifi\u00e9e");
              riSousMenu_bt2.setToolTipText("Saisie simplifi\u00e9e des articles li\u00e9s");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt2ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1010, 610));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1010, 610));
          p_contenu.setMaximumSize(new Dimension(1010, 610));
          p_contenu.setName("p_contenu");
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Articles li\u00e9s @TITRE@"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- label3 ----
            label3.setText("Article");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(10, 85, 115, 20);
            
            // ---- QT101 ----
            QT101.setHorizontalAlignment(SwingConstants.TRAILING);
            QT101.setName("QT101");
            panel1.add(QT101);
            QT101.setBounds(465, 105, 55, QT101.getPreferredSize().height);
            
            // ---- QT102 ----
            QT102.setHorizontalAlignment(SwingConstants.TRAILING);
            QT102.setName("QT102");
            panel1.add(QT102);
            QT102.setBounds(465, 135, 55, 28);
            
            // ---- QT103 ----
            QT103.setHorizontalAlignment(SwingConstants.TRAILING);
            QT103.setName("QT103");
            panel1.add(QT103);
            QT103.setBounds(465, 165, 55, 28);
            
            // ---- QT104 ----
            QT104.setHorizontalAlignment(SwingConstants.TRAILING);
            QT104.setName("QT104");
            panel1.add(QT104);
            QT104.setBounds(465, 195, 55, 28);
            
            // ---- QT105 ----
            QT105.setHorizontalAlignment(SwingConstants.TRAILING);
            QT105.setName("QT105");
            panel1.add(QT105);
            QT105.setBounds(465, 225, 55, 28);
            
            // ---- QT106 ----
            QT106.setHorizontalAlignment(SwingConstants.TRAILING);
            QT106.setName("QT106");
            panel1.add(QT106);
            QT106.setBounds(465, 255, 55, 28);
            
            // ---- QT107 ----
            QT107.setHorizontalAlignment(SwingConstants.TRAILING);
            QT107.setName("QT107");
            panel1.add(QT107);
            QT107.setBounds(465, 285, 55, 28);
            
            // ---- QT108 ----
            QT108.setHorizontalAlignment(SwingConstants.TRAILING);
            QT108.setName("QT108");
            panel1.add(QT108);
            QT108.setBounds(465, 315, 55, 28);
            
            // ---- QT109 ----
            QT109.setHorizontalAlignment(SwingConstants.TRAILING);
            QT109.setName("QT109");
            panel1.add(QT109);
            QT109.setBounds(465, 345, 55, 28);
            
            // ---- QT110 ----
            QT110.setHorizontalAlignment(SwingConstants.TRAILING);
            QT110.setName("QT110");
            panel1.add(QT110);
            QT110.setBounds(465, 375, 55, 28);
            
            // ---- QT111 ----
            QT111.setHorizontalAlignment(SwingConstants.TRAILING);
            QT111.setName("QT111");
            panel1.add(QT111);
            QT111.setBounds(465, 405, 55, 28);
            
            // ---- QT112 ----
            QT112.setHorizontalAlignment(SwingConstants.TRAILING);
            QT112.setName("QT112");
            panel1.add(QT112);
            QT112.setBounds(465, 435, 55, 28);
            
            // ---- QT113 ----
            QT113.setHorizontalAlignment(SwingConstants.TRAILING);
            QT113.setName("QT113");
            panel1.add(QT113);
            QT113.setBounds(465, 465, 55, 28);
            
            // ---- QT114 ----
            QT114.setHorizontalAlignment(SwingConstants.TRAILING);
            QT114.setName("QT114");
            panel1.add(QT114);
            QT114.setBounds(465, 495, 55, 28);
            
            // ---- QT115 ----
            QT115.setHorizontalAlignment(SwingConstants.TRAILING);
            QT115.setName("QT115");
            panel1.add(QT115);
            QT115.setBounds(465, 525, 55, 28);
            
            // ---- label6 ----
            label6.setText("Quantit\u00e9");
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setName("label6");
            panel1.add(label6);
            label6.setBounds(465, 85, 55, 20);
            
            // ---- TG101 ----
            TG101.setComponentPopupMenu(BTD);
            TG101.setName("TG101");
            panel1.add(TG101);
            TG101.setBounds(520, 105, 20, TG101.getPreferredSize().height);
            
            // ---- TG102 ----
            TG102.setComponentPopupMenu(BTD);
            TG102.setName("TG102");
            panel1.add(TG102);
            TG102.setBounds(520, 135, 20, 28);
            
            // ---- TG103 ----
            TG103.setComponentPopupMenu(BTD);
            TG103.setName("TG103");
            panel1.add(TG103);
            TG103.setBounds(520, 165, 20, 28);
            
            // ---- TG104 ----
            TG104.setComponentPopupMenu(BTD);
            TG104.setName("TG104");
            panel1.add(TG104);
            TG104.setBounds(520, 195, 20, 28);
            
            // ---- TG105 ----
            TG105.setComponentPopupMenu(BTD);
            TG105.setName("TG105");
            panel1.add(TG105);
            TG105.setBounds(520, 225, 20, 28);
            
            // ---- TG106 ----
            TG106.setComponentPopupMenu(BTD);
            TG106.setName("TG106");
            panel1.add(TG106);
            TG106.setBounds(520, 255, 20, 28);
            
            // ---- TG107 ----
            TG107.setComponentPopupMenu(BTD);
            TG107.setName("TG107");
            panel1.add(TG107);
            TG107.setBounds(520, 285, 20, 28);
            
            // ---- TG108 ----
            TG108.setComponentPopupMenu(BTD);
            TG108.setName("TG108");
            panel1.add(TG108);
            TG108.setBounds(520, 315, 20, 28);
            
            // ---- TG109 ----
            TG109.setComponentPopupMenu(BTD);
            TG109.setName("TG109");
            panel1.add(TG109);
            TG109.setBounds(520, 345, 20, 28);
            
            // ---- TG110 ----
            TG110.setComponentPopupMenu(BTD);
            TG110.setName("TG110");
            panel1.add(TG110);
            TG110.setBounds(520, 375, 20, 28);
            
            // ---- TG111 ----
            TG111.setComponentPopupMenu(BTD);
            TG111.setName("TG111");
            panel1.add(TG111);
            TG111.setBounds(520, 405, 20, 28);
            
            // ---- TG112 ----
            TG112.setComponentPopupMenu(BTD);
            TG112.setName("TG112");
            panel1.add(TG112);
            TG112.setBounds(520, 435, 20, 28);
            
            // ---- TG113 ----
            TG113.setComponentPopupMenu(BTD);
            TG113.setName("TG113");
            panel1.add(TG113);
            TG113.setBounds(520, 465, 20, 28);
            
            // ---- TG114 ----
            TG114.setComponentPopupMenu(BTD);
            TG114.setName("TG114");
            panel1.add(TG114);
            TG114.setBounds(520, 495, 20, 28);
            
            // ---- TG115 ----
            TG115.setComponentPopupMenu(BTD);
            TG115.setName("TG115");
            panel1.add(TG115);
            TG115.setBounds(520, 525, 20, 28);
            
            // ---- PR101 ----
            PR101.setHorizontalAlignment(SwingConstants.TRAILING);
            PR101.setName("PR101");
            panel1.add(PR101);
            PR101.setBounds(540, 105, 95, PR101.getPreferredSize().height);
            
            // ---- PR102 ----
            PR102.setHorizontalAlignment(SwingConstants.TRAILING);
            PR102.setName("PR102");
            panel1.add(PR102);
            PR102.setBounds(540, 135, 95, 28);
            
            // ---- PR103 ----
            PR103.setHorizontalAlignment(SwingConstants.TRAILING);
            PR103.setName("PR103");
            panel1.add(PR103);
            PR103.setBounds(540, 165, 95, 28);
            
            // ---- PR104 ----
            PR104.setHorizontalAlignment(SwingConstants.TRAILING);
            PR104.setName("PR104");
            panel1.add(PR104);
            PR104.setBounds(540, 195, 95, 28);
            
            // ---- PR105 ----
            PR105.setHorizontalAlignment(SwingConstants.TRAILING);
            PR105.setName("PR105");
            panel1.add(PR105);
            PR105.setBounds(540, 225, 95, 28);
            
            // ---- PR106 ----
            PR106.setHorizontalAlignment(SwingConstants.TRAILING);
            PR106.setName("PR106");
            panel1.add(PR106);
            PR106.setBounds(540, 255, 95, 28);
            
            // ---- label10 ----
            label10.setText("Prix");
            label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
            label10.setName("label10");
            panel1.add(label10);
            label10.setBounds(540, 85, 70, 20);
            
            // ---- PR107 ----
            PR107.setHorizontalAlignment(SwingConstants.TRAILING);
            PR107.setName("PR107");
            panel1.add(PR107);
            PR107.setBounds(540, 285, 95, 28);
            
            // ---- PR108 ----
            PR108.setHorizontalAlignment(SwingConstants.TRAILING);
            PR108.setName("PR108");
            panel1.add(PR108);
            PR108.setBounds(540, 315, 95, 28);
            
            // ---- PR109 ----
            PR109.setHorizontalAlignment(SwingConstants.TRAILING);
            PR109.setName("PR109");
            panel1.add(PR109);
            PR109.setBounds(540, 345, 95, 28);
            
            // ---- PR110 ----
            PR110.setHorizontalAlignment(SwingConstants.TRAILING);
            PR110.setName("PR110");
            panel1.add(PR110);
            PR110.setBounds(540, 375, 95, 28);
            
            // ---- PR111 ----
            PR111.setHorizontalAlignment(SwingConstants.TRAILING);
            PR111.setName("PR111");
            panel1.add(PR111);
            PR111.setBounds(540, 405, 95, 28);
            
            // ---- PR112 ----
            PR112.setHorizontalAlignment(SwingConstants.TRAILING);
            PR112.setName("PR112");
            panel1.add(PR112);
            PR112.setBounds(540, 435, 95, 28);
            
            // ---- PR113 ----
            PR113.setHorizontalAlignment(SwingConstants.TRAILING);
            PR113.setName("PR113");
            panel1.add(PR113);
            PR113.setBounds(540, 465, 95, 28);
            
            // ---- PR114 ----
            PR114.setHorizontalAlignment(SwingConstants.TRAILING);
            PR114.setName("PR114");
            panel1.add(PR114);
            PR114.setBounds(540, 495, 95, 28);
            
            // ---- PR115 ----
            PR115.setHorizontalAlignment(SwingConstants.TRAILING);
            PR115.setName("PR115");
            panel1.add(PR115);
            PR115.setBounds(540, 525, 95, 28);
            
            // ---- QM101 ----
            QM101.setHorizontalAlignment(SwingConstants.TRAILING);
            QM101.setName("QM101");
            panel1.add(QM101);
            QM101.setBounds(635, 105, 80, QM101.getPreferredSize().height);
            
            // ---- QM102 ----
            QM102.setHorizontalAlignment(SwingConstants.TRAILING);
            QM102.setName("QM102");
            panel1.add(QM102);
            QM102.setBounds(635, 135, 80, 28);
            
            // ---- QM103 ----
            QM103.setHorizontalAlignment(SwingConstants.TRAILING);
            QM103.setName("QM103");
            panel1.add(QM103);
            QM103.setBounds(635, 165, 80, 28);
            
            // ---- QM104 ----
            QM104.setHorizontalAlignment(SwingConstants.TRAILING);
            QM104.setName("QM104");
            panel1.add(QM104);
            QM104.setBounds(635, 195, 80, 28);
            
            // ---- QM105 ----
            QM105.setHorizontalAlignment(SwingConstants.TRAILING);
            QM105.setName("QM105");
            panel1.add(QM105);
            QM105.setBounds(635, 225, 80, 28);
            
            // ---- QM106 ----
            QM106.setHorizontalAlignment(SwingConstants.TRAILING);
            QM106.setName("QM106");
            panel1.add(QM106);
            QM106.setBounds(635, 255, 80, 28);
            
            // ---- QM107 ----
            QM107.setHorizontalAlignment(SwingConstants.TRAILING);
            QM107.setName("QM107");
            panel1.add(QM107);
            QM107.setBounds(635, 285, 80, 28);
            
            // ---- QM108 ----
            QM108.setHorizontalAlignment(SwingConstants.TRAILING);
            QM108.setName("QM108");
            panel1.add(QM108);
            QM108.setBounds(635, 315, 80, 28);
            
            // ---- QM109 ----
            QM109.setHorizontalAlignment(SwingConstants.TRAILING);
            QM109.setName("QM109");
            panel1.add(QM109);
            QM109.setBounds(635, 345, 80, 28);
            
            // ---- QM110 ----
            QM110.setHorizontalAlignment(SwingConstants.TRAILING);
            QM110.setName("QM110");
            panel1.add(QM110);
            QM110.setBounds(635, 375, 80, 28);
            
            // ---- QM111 ----
            QM111.setHorizontalAlignment(SwingConstants.TRAILING);
            QM111.setName("QM111");
            panel1.add(QM111);
            QM111.setBounds(635, 405, 80, 28);
            
            // ---- QM112 ----
            QM112.setHorizontalAlignment(SwingConstants.TRAILING);
            QM112.setName("QM112");
            panel1.add(QM112);
            QM112.setBounds(635, 435, 80, 28);
            
            // ---- QM113 ----
            QM113.setHorizontalAlignment(SwingConstants.TRAILING);
            QM113.setName("QM113");
            panel1.add(QM113);
            QM113.setBounds(635, 465, 80, 28);
            
            // ---- QM114 ----
            QM114.setHorizontalAlignment(SwingConstants.TRAILING);
            QM114.setName("QM114");
            panel1.add(QM114);
            QM114.setBounds(635, 495, 80, 28);
            
            // ---- QM115 ----
            QM115.setHorizontalAlignment(SwingConstants.TRAILING);
            QM115.setName("QM115");
            panel1.add(QM115);
            QM115.setBounds(635, 525, 80, 28);
            
            // ---- label11 ----
            label11.setText("Tranche");
            label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
            label11.setName("label11");
            panel1.add(label11);
            label11.setBounds(635, 85, 65, 20);
            
            // ---- label9 ----
            label9.setText("Quantit\u00e9");
            label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
            label9.setName("label9");
            panel1.add(label9);
            label9.setBounds(730, 85, 55, 20);
            
            // ---- QT201 ----
            QT201.setHorizontalAlignment(SwingConstants.TRAILING);
            QT201.setName("QT201");
            panel1.add(QT201);
            QT201.setBounds(730, 105, 55, QT201.getPreferredSize().height);
            
            // ---- QT202 ----
            QT202.setHorizontalAlignment(SwingConstants.TRAILING);
            QT202.setName("QT202");
            panel1.add(QT202);
            QT202.setBounds(730, 135, 55, 28);
            
            // ---- QT203 ----
            QT203.setHorizontalAlignment(SwingConstants.TRAILING);
            QT203.setName("QT203");
            panel1.add(QT203);
            QT203.setBounds(730, 165, 55, 28);
            
            // ---- QT204 ----
            QT204.setHorizontalAlignment(SwingConstants.TRAILING);
            QT204.setName("QT204");
            panel1.add(QT204);
            QT204.setBounds(730, 195, 55, 28);
            
            // ---- QT205 ----
            QT205.setHorizontalAlignment(SwingConstants.TRAILING);
            QT205.setName("QT205");
            panel1.add(QT205);
            QT205.setBounds(730, 225, 55, 28);
            
            // ---- QT206 ----
            QT206.setHorizontalAlignment(SwingConstants.TRAILING);
            QT206.setName("QT206");
            panel1.add(QT206);
            QT206.setBounds(730, 255, 55, 28);
            
            // ---- QT207 ----
            QT207.setHorizontalAlignment(SwingConstants.TRAILING);
            QT207.setName("QT207");
            panel1.add(QT207);
            QT207.setBounds(730, 285, 55, 28);
            
            // ---- QT208 ----
            QT208.setHorizontalAlignment(SwingConstants.TRAILING);
            QT208.setName("QT208");
            panel1.add(QT208);
            QT208.setBounds(730, 315, 55, 28);
            
            // ---- QT209 ----
            QT209.setHorizontalAlignment(SwingConstants.TRAILING);
            QT209.setName("QT209");
            panel1.add(QT209);
            QT209.setBounds(730, 345, 55, 28);
            
            // ---- QT210 ----
            QT210.setHorizontalAlignment(SwingConstants.TRAILING);
            QT210.setName("QT210");
            panel1.add(QT210);
            QT210.setBounds(730, 375, 55, 28);
            
            // ---- QT211 ----
            QT211.setHorizontalAlignment(SwingConstants.TRAILING);
            QT211.setName("QT211");
            panel1.add(QT211);
            QT211.setBounds(730, 405, 55, 28);
            
            // ---- QT212 ----
            QT212.setHorizontalAlignment(SwingConstants.TRAILING);
            QT212.setName("QT212");
            panel1.add(QT212);
            QT212.setBounds(730, 435, 55, 28);
            
            // ---- QT213 ----
            QT213.setHorizontalAlignment(SwingConstants.TRAILING);
            QT213.setName("QT213");
            panel1.add(QT213);
            QT213.setBounds(730, 465, 55, 28);
            
            // ---- QT214 ----
            QT214.setHorizontalAlignment(SwingConstants.TRAILING);
            QT214.setName("QT214");
            panel1.add(QT214);
            QT214.setBounds(730, 495, 55, 28);
            
            // ---- QT215 ----
            QT215.setHorizontalAlignment(SwingConstants.TRAILING);
            QT215.setName("QT215");
            panel1.add(QT215);
            QT215.setBounds(730, 525, 55, 28);
            
            // ---- TG201 ----
            TG201.setComponentPopupMenu(BTD);
            TG201.setName("TG201");
            panel1.add(TG201);
            TG201.setBounds(785, 105, 20, TG201.getPreferredSize().height);
            
            // ---- TG202 ----
            TG202.setComponentPopupMenu(BTD);
            TG202.setName("TG202");
            panel1.add(TG202);
            TG202.setBounds(785, 135, 20, 28);
            
            // ---- TG203 ----
            TG203.setComponentPopupMenu(BTD);
            TG203.setName("TG203");
            panel1.add(TG203);
            TG203.setBounds(785, 165, 20, 28);
            
            // ---- TG204 ----
            TG204.setComponentPopupMenu(BTD);
            TG204.setName("TG204");
            panel1.add(TG204);
            TG204.setBounds(785, 195, 20, 28);
            
            // ---- TG205 ----
            TG205.setComponentPopupMenu(BTD);
            TG205.setName("TG205");
            panel1.add(TG205);
            TG205.setBounds(785, 225, 20, 28);
            
            // ---- TG206 ----
            TG206.setComponentPopupMenu(BTD);
            TG206.setName("TG206");
            panel1.add(TG206);
            TG206.setBounds(785, 255, 20, 28);
            
            // ---- TG207 ----
            TG207.setComponentPopupMenu(BTD);
            TG207.setName("TG207");
            panel1.add(TG207);
            TG207.setBounds(785, 285, 20, 28);
            
            // ---- TG208 ----
            TG208.setComponentPopupMenu(BTD);
            TG208.setName("TG208");
            panel1.add(TG208);
            TG208.setBounds(785, 315, 20, 28);
            
            // ---- TG209 ----
            TG209.setComponentPopupMenu(BTD);
            TG209.setName("TG209");
            panel1.add(TG209);
            TG209.setBounds(785, 345, 20, 28);
            
            // ---- TG210 ----
            TG210.setComponentPopupMenu(BTD);
            TG210.setName("TG210");
            panel1.add(TG210);
            TG210.setBounds(785, 375, 20, 28);
            
            // ---- TG211 ----
            TG211.setComponentPopupMenu(BTD);
            TG211.setName("TG211");
            panel1.add(TG211);
            TG211.setBounds(785, 405, 20, 28);
            
            // ---- TG212 ----
            TG212.setComponentPopupMenu(BTD);
            TG212.setName("TG212");
            panel1.add(TG212);
            TG212.setBounds(785, 435, 20, 28);
            
            // ---- TG213 ----
            TG213.setComponentPopupMenu(BTD);
            TG213.setName("TG213");
            panel1.add(TG213);
            TG213.setBounds(785, 465, 20, 28);
            
            // ---- TG214 ----
            TG214.setComponentPopupMenu(BTD);
            TG214.setName("TG214");
            panel1.add(TG214);
            TG214.setBounds(785, 495, 20, 28);
            
            // ---- TG215 ----
            TG215.setComponentPopupMenu(BTD);
            TG215.setName("TG215");
            panel1.add(TG215);
            TG215.setBounds(785, 525, 20, 28);
            
            // ---- PR201 ----
            PR201.setHorizontalAlignment(SwingConstants.TRAILING);
            PR201.setName("PR201");
            panel1.add(PR201);
            PR201.setBounds(805, 105, 95, 28);
            
            // ---- PR202 ----
            PR202.setHorizontalAlignment(SwingConstants.TRAILING);
            PR202.setName("PR202");
            panel1.add(PR202);
            PR202.setBounds(805, 135, 95, 28);
            
            // ---- PR203 ----
            PR203.setHorizontalAlignment(SwingConstants.TRAILING);
            PR203.setName("PR203");
            panel1.add(PR203);
            PR203.setBounds(805, 165, 95, 28);
            
            // ---- PR204 ----
            PR204.setHorizontalAlignment(SwingConstants.TRAILING);
            PR204.setName("PR204");
            panel1.add(PR204);
            PR204.setBounds(805, 195, 95, 28);
            
            // ---- PR205 ----
            PR205.setHorizontalAlignment(SwingConstants.TRAILING);
            PR205.setName("PR205");
            panel1.add(PR205);
            PR205.setBounds(805, 225, 95, 28);
            
            // ---- PR206 ----
            PR206.setHorizontalAlignment(SwingConstants.TRAILING);
            PR206.setName("PR206");
            panel1.add(PR206);
            PR206.setBounds(805, 255, 95, 28);
            
            // ---- PR207 ----
            PR207.setHorizontalAlignment(SwingConstants.TRAILING);
            PR207.setName("PR207");
            panel1.add(PR207);
            PR207.setBounds(805, 285, 95, 28);
            
            // ---- PR208 ----
            PR208.setHorizontalAlignment(SwingConstants.TRAILING);
            PR208.setName("PR208");
            panel1.add(PR208);
            PR208.setBounds(805, 315, 95, 28);
            
            // ---- PR209 ----
            PR209.setHorizontalAlignment(SwingConstants.TRAILING);
            PR209.setName("PR209");
            panel1.add(PR209);
            PR209.setBounds(805, 345, 95, 28);
            
            // ---- PR210 ----
            PR210.setHorizontalAlignment(SwingConstants.TRAILING);
            PR210.setName("PR210");
            panel1.add(PR210);
            PR210.setBounds(805, 375, 95, 28);
            
            // ---- PR211 ----
            PR211.setHorizontalAlignment(SwingConstants.TRAILING);
            PR211.setName("PR211");
            panel1.add(PR211);
            PR211.setBounds(805, 405, 95, 28);
            
            // ---- PR212 ----
            PR212.setHorizontalAlignment(SwingConstants.TRAILING);
            PR212.setName("PR212");
            panel1.add(PR212);
            PR212.setBounds(805, 435, 95, 28);
            
            // ---- PR213 ----
            PR213.setHorizontalAlignment(SwingConstants.TRAILING);
            PR213.setName("PR213");
            panel1.add(PR213);
            PR213.setBounds(805, 465, 95, 28);
            
            // ---- PR214 ----
            PR214.setHorizontalAlignment(SwingConstants.TRAILING);
            PR214.setName("PR214");
            panel1.add(PR214);
            PR214.setBounds(805, 495, 95, 28);
            
            // ---- PR215 ----
            PR215.setHorizontalAlignment(SwingConstants.TRAILING);
            PR215.setName("PR215");
            panel1.add(PR215);
            PR215.setBounds(805, 525, 95, 28);
            
            // ---- label12 ----
            label12.setText("Prix");
            label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
            label12.setName("label12");
            panel1.add(label12);
            label12.setBounds(805, 85, 70, 20);
            
            // ---- QM201 ----
            QM201.setHorizontalAlignment(SwingConstants.TRAILING);
            QM201.setName("QM201");
            panel1.add(QM201);
            QM201.setBounds(900, 105, 80, 28);
            
            // ---- QM202 ----
            QM202.setHorizontalAlignment(SwingConstants.TRAILING);
            QM202.setName("QM202");
            panel1.add(QM202);
            QM202.setBounds(900, 135, 80, 28);
            
            // ---- QM203 ----
            QM203.setHorizontalAlignment(SwingConstants.TRAILING);
            QM203.setName("QM203");
            panel1.add(QM203);
            QM203.setBounds(900, 165, 80, 28);
            
            // ---- QM204 ----
            QM204.setHorizontalAlignment(SwingConstants.TRAILING);
            QM204.setName("QM204");
            panel1.add(QM204);
            QM204.setBounds(900, 195, 80, 28);
            
            // ---- QM205 ----
            QM205.setHorizontalAlignment(SwingConstants.TRAILING);
            QM205.setName("QM205");
            panel1.add(QM205);
            QM205.setBounds(900, 225, 80, 28);
            
            // ---- QM206 ----
            QM206.setHorizontalAlignment(SwingConstants.TRAILING);
            QM206.setName("QM206");
            panel1.add(QM206);
            QM206.setBounds(900, 255, 80, 28);
            
            // ---- QM207 ----
            QM207.setHorizontalAlignment(SwingConstants.TRAILING);
            QM207.setName("QM207");
            panel1.add(QM207);
            QM207.setBounds(900, 285, 80, 28);
            
            // ---- QM208 ----
            QM208.setHorizontalAlignment(SwingConstants.TRAILING);
            QM208.setName("QM208");
            panel1.add(QM208);
            QM208.setBounds(900, 315, 80, 28);
            
            // ---- QM209 ----
            QM209.setHorizontalAlignment(SwingConstants.TRAILING);
            QM209.setName("QM209");
            panel1.add(QM209);
            QM209.setBounds(900, 345, 80, 28);
            
            // ---- QM210 ----
            QM210.setHorizontalAlignment(SwingConstants.TRAILING);
            QM210.setName("QM210");
            panel1.add(QM210);
            QM210.setBounds(900, 375, 80, 28);
            
            // ---- QM211 ----
            QM211.setHorizontalAlignment(SwingConstants.TRAILING);
            QM211.setName("QM211");
            panel1.add(QM211);
            QM211.setBounds(900, 405, 80, 28);
            
            // ---- QM212 ----
            QM212.setHorizontalAlignment(SwingConstants.TRAILING);
            QM212.setName("QM212");
            panel1.add(QM212);
            QM212.setBounds(900, 435, 80, 28);
            
            // ---- QM213 ----
            QM213.setHorizontalAlignment(SwingConstants.TRAILING);
            QM213.setName("QM213");
            panel1.add(QM213);
            QM213.setBounds(900, 465, 80, 28);
            
            // ---- QM214 ----
            QM214.setHorizontalAlignment(SwingConstants.TRAILING);
            QM214.setName("QM214");
            panel1.add(QM214);
            QM214.setBounds(900, 495, 80, 28);
            
            // ---- QM215 ----
            QM215.setHorizontalAlignment(SwingConstants.TRAILING);
            QM215.setName("QM215");
            panel1.add(QM215);
            QM215.setBounds(900, 525, 80, 28);
            
            // ---- label13 ----
            label13.setText("Tranche");
            label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
            label13.setName("label13");
            panel1.add(label13);
            label13.setBounds(900, 85, 65, 20);
            
            // ---- WDD1 ----
            WDD1.setName("WDD1");
            panel1.add(WDD1);
            WDD1.setBounds(465, 50, 105, WDD1.getPreferredSize().height);
            
            // ---- label5 ----
            label5.setText("au");
            label5.setHorizontalAlignment(SwingConstants.CENTER);
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getSize() - 2f));
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(580, 55, 30, 20);
            
            // ---- WDF1 ----
            WDF1.setName("WDF1");
            panel1.add(WDF1);
            WDF1.setBounds(610, 50, 105, WDF1.getPreferredSize().height);
            
            // ---- WDD2 ----
            WDD2.setName("WDD2");
            panel1.add(WDD2);
            WDD2.setBounds(730, 50, 105, WDD2.getPreferredSize().height);
            
            // ---- label8 ----
            label8.setText("au");
            label8.setHorizontalAlignment(SwingConstants.CENTER);
            label8.setFont(label8.getFont().deriveFont(label8.getFont().getSize() - 2f));
            label8.setName("label8");
            panel1.add(label8);
            label8.setBounds(840, 55, 30, 20);
            
            // ---- WDF2 ----
            WDF2.setName("WDF2");
            panel1.add(WDF2);
            WDF2.setBounds(875, 50, 105, WDF2.getPreferredSize().height);
            
            // ---- WUNCND ----
            WUNCND.setText("Unit\u00e9 conditionnement");
            WUNCND.setName("WUNCND");
            panel1.add(WUNCND);
            WUNCND.setBounds(240, 55, 175, WUNCND.getPreferredSize().height);
            
            // ---- separator1 ----
            separator1.setName("separator1");
            panel1.add(separator1);
            separator1.setBounds(730, 30, 250, separator1.getPreferredSize().height);
            
            // ---- separator2 ----
            separator2.setName("separator2");
            panel1.add(separator2);
            separator2.setBounds(465, 30, 250, separator2.getPreferredSize().height);
            
            // ---- ARL01 ----
            ARL01.setComponentPopupMenu(BTD);
            ARL01.setName("ARL01");
            panel1.add(ARL01);
            ARL01.setBounds(10, 104, 440, ARL01.getPreferredSize().height);
            
            // ---- ARL02 ----
            ARL02.setComponentPopupMenu(BTD);
            ARL02.setName("ARL02");
            panel1.add(ARL02);
            ARL02.setBounds(10, 134, 440, ARL02.getPreferredSize().height);
            
            // ---- ARL03 ----
            ARL03.setComponentPopupMenu(BTD);
            ARL03.setName("ARL03");
            panel1.add(ARL03);
            ARL03.setBounds(10, 164, 440, ARL03.getPreferredSize().height);
            
            // ---- ARL04 ----
            ARL04.setComponentPopupMenu(BTD);
            ARL04.setName("ARL04");
            panel1.add(ARL04);
            ARL04.setBounds(10, 194, 440, ARL04.getPreferredSize().height);
            
            // ---- ARL05 ----
            ARL05.setComponentPopupMenu(BTD);
            ARL05.setName("ARL05");
            panel1.add(ARL05);
            ARL05.setBounds(10, 224, 440, ARL05.getPreferredSize().height);
            
            // ---- ARL06 ----
            ARL06.setComponentPopupMenu(BTD);
            ARL06.setName("ARL06");
            panel1.add(ARL06);
            ARL06.setBounds(10, 254, 440, ARL06.getPreferredSize().height);
            
            // ---- ARL07 ----
            ARL07.setComponentPopupMenu(BTD);
            ARL07.setName("ARL07");
            panel1.add(ARL07);
            ARL07.setBounds(10, 284, 440, ARL07.getPreferredSize().height);
            
            // ---- ARL08 ----
            ARL08.setComponentPopupMenu(BTD);
            ARL08.setName("ARL08");
            panel1.add(ARL08);
            ARL08.setBounds(10, 314, 440, ARL08.getPreferredSize().height);
            
            // ---- ARL09 ----
            ARL09.setComponentPopupMenu(BTD);
            ARL09.setName("ARL09");
            panel1.add(ARL09);
            ARL09.setBounds(10, 344, 440, ARL09.getPreferredSize().height);
            
            // ---- ARL10 ----
            ARL10.setComponentPopupMenu(BTD);
            ARL10.setName("ARL10");
            panel1.add(ARL10);
            ARL10.setBounds(10, 374, 440, ARL10.getPreferredSize().height);
            
            // ---- ARL11 ----
            ARL11.setComponentPopupMenu(BTD);
            ARL11.setName("ARL11");
            panel1.add(ARL11);
            ARL11.setBounds(10, 404, 440, ARL11.getPreferredSize().height);
            
            // ---- ARL12 ----
            ARL12.setComponentPopupMenu(BTD);
            ARL12.setName("ARL12");
            panel1.add(ARL12);
            ARL12.setBounds(10, 434, 440, ARL12.getPreferredSize().height);
            
            // ---- ARL13 ----
            ARL13.setComponentPopupMenu(BTD);
            ARL13.setName("ARL13");
            panel1.add(ARL13);
            ARL13.setBounds(10, 464, 440, ARL13.getPreferredSize().height);
            
            // ---- ARL14 ----
            ARL14.setComponentPopupMenu(BTD);
            ARL14.setName("ARL14");
            panel1.add(ARL14);
            ARL14.setBounds(10, 494, 440, ARL14.getPreferredSize().height);
            
            // ---- ARL15 ----
            ARL15.setComponentPopupMenu(BTD);
            ARL15.setName("ARL15");
            panel1.add(ARL15);
            ARL15.setBounds(10, 524, 440, ARL15.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout
              .setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap()));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 566, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(17, Short.MAX_VALUE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- menuItem2 ----
      menuItem2.setText("Transformer");
      menuItem2.setName("menuItem2");
      menuItem2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem2ActionPerformed(e);
        }
      });
      BTD.add(menuItem2);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie riZoneSortie1;
  private JLabel label1;
  private RiZoneSortie riZoneSortie2;
  private XRiTextField WART;
  private JLabel label2;
  private RiZoneSortie riZoneSortie3;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label3;
  private XRiTextField QT101;
  private XRiTextField QT102;
  private XRiTextField QT103;
  private XRiTextField QT104;
  private XRiTextField QT105;
  private XRiTextField QT106;
  private XRiTextField QT107;
  private XRiTextField QT108;
  private XRiTextField QT109;
  private XRiTextField QT110;
  private XRiTextField QT111;
  private XRiTextField QT112;
  private XRiTextField QT113;
  private XRiTextField QT114;
  private XRiTextField QT115;
  private JLabel label6;
  private XRiTextField TG101;
  private XRiTextField TG102;
  private XRiTextField TG103;
  private XRiTextField TG104;
  private XRiTextField TG105;
  private XRiTextField TG106;
  private XRiTextField TG107;
  private XRiTextField TG108;
  private XRiTextField TG109;
  private XRiTextField TG110;
  private XRiTextField TG111;
  private XRiTextField TG112;
  private XRiTextField TG113;
  private XRiTextField TG114;
  private XRiTextField TG115;
  private XRiTextField PR101;
  private XRiTextField PR102;
  private XRiTextField PR103;
  private XRiTextField PR104;
  private XRiTextField PR105;
  private XRiTextField PR106;
  private JLabel label10;
  private XRiTextField PR107;
  private XRiTextField PR108;
  private XRiTextField PR109;
  private XRiTextField PR110;
  private XRiTextField PR111;
  private XRiTextField PR112;
  private XRiTextField PR113;
  private XRiTextField PR114;
  private XRiTextField PR115;
  private XRiTextField QM101;
  private XRiTextField QM102;
  private XRiTextField QM103;
  private XRiTextField QM104;
  private XRiTextField QM105;
  private XRiTextField QM106;
  private XRiTextField QM107;
  private XRiTextField QM108;
  private XRiTextField QM109;
  private XRiTextField QM110;
  private XRiTextField QM111;
  private XRiTextField QM112;
  private XRiTextField QM113;
  private XRiTextField QM114;
  private XRiTextField QM115;
  private JLabel label11;
  private JLabel label9;
  private XRiTextField QT201;
  private XRiTextField QT202;
  private XRiTextField QT203;
  private XRiTextField QT204;
  private XRiTextField QT205;
  private XRiTextField QT206;
  private XRiTextField QT207;
  private XRiTextField QT208;
  private XRiTextField QT209;
  private XRiTextField QT210;
  private XRiTextField QT211;
  private XRiTextField QT212;
  private XRiTextField QT213;
  private XRiTextField QT214;
  private XRiTextField QT215;
  private XRiTextField TG201;
  private XRiTextField TG202;
  private XRiTextField TG203;
  private XRiTextField TG204;
  private XRiTextField TG205;
  private XRiTextField TG206;
  private XRiTextField TG207;
  private XRiTextField TG208;
  private XRiTextField TG209;
  private XRiTextField TG210;
  private XRiTextField TG211;
  private XRiTextField TG212;
  private XRiTextField TG213;
  private XRiTextField TG214;
  private XRiTextField TG215;
  private XRiTextField PR201;
  private XRiTextField PR202;
  private XRiTextField PR203;
  private XRiTextField PR204;
  private XRiTextField PR205;
  private XRiTextField PR206;
  private XRiTextField PR207;
  private XRiTextField PR208;
  private XRiTextField PR209;
  private XRiTextField PR210;
  private XRiTextField PR211;
  private XRiTextField PR212;
  private XRiTextField PR213;
  private XRiTextField PR214;
  private XRiTextField PR215;
  private JLabel label12;
  private XRiTextField QM201;
  private XRiTextField QM202;
  private XRiTextField QM203;
  private XRiTextField QM204;
  private XRiTextField QM205;
  private XRiTextField QM206;
  private XRiTextField QM207;
  private XRiTextField QM208;
  private XRiTextField QM209;
  private XRiTextField QM210;
  private XRiTextField QM211;
  private XRiTextField QM212;
  private XRiTextField QM213;
  private XRiTextField QM214;
  private XRiTextField QM215;
  private JLabel label13;
  private XRiCalendrier WDD1;
  private JLabel label5;
  private XRiCalendrier WDF1;
  private XRiCalendrier WDD2;
  private JLabel label8;
  private XRiCalendrier WDF2;
  private XRiCheckBox WUNCND;
  private JComponent separator1;
  private JComponent separator2;
  private SNArticle ARL01;
  private SNArticle ARL02;
  private SNArticle ARL03;
  private SNArticle ARL04;
  private SNArticle ARL05;
  private SNArticle ARL06;
  private SNArticle ARL07;
  private SNArticle ARL08;
  private SNArticle ARL09;
  private SNArticle ARL10;
  private SNArticle ARL11;
  private SNArticle ARL12;
  private SNArticle ARL13;
  private SNArticle ARL14;
  private SNArticle ARL15;
  private JPopupMenu BTD;
  private JMenuItem menuItem2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
