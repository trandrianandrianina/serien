
package ri.serien.libecranrpg.vgvm.VGVM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM03FM_OB extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM03FM_OB(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    setCloseKey("ENTER", "F12");
    
    
    OBJ_47.setIcon(lexique.chargerImage("images/rouge2.gif", true));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    // Titre
    setTitle("Bloc-notes");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    boolean isModif = (!lexique.isTrue("53"));
    navig_valid.setVisible(isModif);
    
    OBJ_47.setVisible(interpreteurD.analyseExpression("@POSTIT@").equals("+"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OB16 = new XRiTextField();
    OB17 = new XRiTextField();
    OB15 = new XRiTextField();
    OB14 = new XRiTextField();
    OB13 = new XRiTextField();
    OB12 = new XRiTextField();
    OB11 = new XRiTextField();
    OB10 = new XRiTextField();
    OB9 = new XRiTextField();
    OB8 = new XRiTextField();
    OB7 = new XRiTextField();
    OB6 = new XRiTextField();
    OB5 = new XRiTextField();
    OB4 = new XRiTextField();
    OB3 = new XRiTextField();
    OB2 = new XRiTextField();
    OB1 = new XRiTextField();
    CLTEL = new XRiTextField();
    CLFAX = new XRiTextField();
    OBJ_35_OBJ_35 = new JLabel();
    OBJ_37_OBJ_37 = new JLabel();
    panel2 = new JPanel();
    OBJ_47 = new JLabel();
    OB181 = new XRiTextField();
    OB182 = new XRiTextField();
    OB183 = new XRiTextField();
    OB184 = new XRiTextField();
    OB185 = new XRiTextField();
    BTD = new JPopupMenu();
    menuItem1 = new JMenuItem();
    menuItem2 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(810, 640));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 300));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("M\u00e9mo");
              riSousMenu_bt6.setToolTipText("Acc\u00e8s au m\u00e9mo");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Bloc-notes"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OB16 ----
          OB16.setName("OB16");
          panel1.add(OB16);
          OB16.setBounds(20, 410, 579, OB16.getPreferredSize().height);

          //---- OB17 ----
          OB17.setName("OB17");
          panel1.add(OB17);
          OB17.setBounds(20, 435, 579, OB17.getPreferredSize().height);

          //---- OB15 ----
          OB15.setName("OB15");
          panel1.add(OB15);
          OB15.setBounds(20, 385, 579, OB15.getPreferredSize().height);

          //---- OB14 ----
          OB14.setName("OB14");
          panel1.add(OB14);
          OB14.setBounds(20, 360, 579, OB14.getPreferredSize().height);

          //---- OB13 ----
          OB13.setName("OB13");
          panel1.add(OB13);
          OB13.setBounds(20, 335, 579, OB13.getPreferredSize().height);

          //---- OB12 ----
          OB12.setName("OB12");
          panel1.add(OB12);
          OB12.setBounds(20, 310, 579, OB12.getPreferredSize().height);

          //---- OB11 ----
          OB11.setName("OB11");
          panel1.add(OB11);
          OB11.setBounds(20, 285, 579, OB11.getPreferredSize().height);

          //---- OB10 ----
          OB10.setName("OB10");
          panel1.add(OB10);
          OB10.setBounds(20, 260, 579, OB10.getPreferredSize().height);

          //---- OB9 ----
          OB9.setName("OB9");
          panel1.add(OB9);
          OB9.setBounds(20, 235, 579, OB9.getPreferredSize().height);

          //---- OB8 ----
          OB8.setName("OB8");
          panel1.add(OB8);
          OB8.setBounds(20, 210, 579, OB8.getPreferredSize().height);

          //---- OB7 ----
          OB7.setName("OB7");
          panel1.add(OB7);
          OB7.setBounds(20, 185, 579, OB7.getPreferredSize().height);

          //---- OB6 ----
          OB6.setName("OB6");
          panel1.add(OB6);
          OB6.setBounds(20, 160, 579, OB6.getPreferredSize().height);

          //---- OB5 ----
          OB5.setName("OB5");
          panel1.add(OB5);
          OB5.setBounds(20, 135, 579, OB5.getPreferredSize().height);

          //---- OB4 ----
          OB4.setName("OB4");
          panel1.add(OB4);
          OB4.setBounds(20, 110, 579, OB4.getPreferredSize().height);

          //---- OB3 ----
          OB3.setName("OB3");
          panel1.add(OB3);
          OB3.setBounds(20, 85, 579, OB3.getPreferredSize().height);

          //---- OB2 ----
          OB2.setName("OB2");
          panel1.add(OB2);
          OB2.setBounds(20, 60, 579, OB2.getPreferredSize().height);

          //---- OB1 ----
          OB1.setName("OB1");
          panel1.add(OB1);
          OB1.setBounds(20, 35, 579, OB1.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //---- CLTEL ----
        CLTEL.setName("CLTEL");

        //---- CLFAX ----
        CLFAX.setName("CLFAX");

        //---- OBJ_35_OBJ_35 ----
        OBJ_35_OBJ_35.setText("T\u00e9l\u00e9phone");
        OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");

        //---- OBJ_37_OBJ_37 ----
        OBJ_37_OBJ_37.setText("Fax");
        OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Articles ou commentaires g\u00e9n\u00e9r\u00e9s en d\u00e9but de bon"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_47 ----
          OBJ_47.setIcon(null);
          OBJ_47.setToolTipText("informations disponibles dans le m\u00e9mo de cette fiche");
          OBJ_47.setName("OBJ_47");
          panel2.add(OBJ_47);
          OBJ_47.setBounds(23, 40, 25, 24);

          //---- OB181 ----
          OB181.setComponentPopupMenu(BTD);
          OB181.setName("OB181");
          panel2.add(OB181);
          OB181.setBounds(20, 35, 110, OB181.getPreferredSize().height);

          //---- OB182 ----
          OB182.setComponentPopupMenu(BTD);
          OB182.setName("OB182");
          panel2.add(OB182);
          OB182.setBounds(137, 35, 110, OB182.getPreferredSize().height);

          //---- OB183 ----
          OB183.setComponentPopupMenu(BTD);
          OB183.setName("OB183");
          panel2.add(OB183);
          OB183.setBounds(254, 35, 110, OB183.getPreferredSize().height);

          //---- OB184 ----
          OB184.setComponentPopupMenu(BTD);
          OB184.setName("OB184");
          panel2.add(OB184);
          OB184.setBounds(371, 35, 110, OB184.getPreferredSize().height);

          //---- OB185 ----
          OB185.setComponentPopupMenu(BTD);
          OB185.setName("OB185");
          panel2.add(OB185);
          OB185.setBounds(488, 35, 110, OB185.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(35, 35, 35)
              .addComponent(OBJ_35_OBJ_35, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(CLTEL, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(OBJ_37_OBJ_37, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
              .addGap(3, 3, 3)
              .addComponent(CLFAX, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 485, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(CLTEL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(CLFAX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addComponent(OBJ_35_OBJ_35)
                    .addComponent(OBJ_37_OBJ_37))))
              .addGap(7, 7, 7)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- menuItem1 ----
      menuItem1.setText("Choix possibles");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD.add(menuItem1);

      //---- menuItem2 ----
      menuItem2.setText("Aide en ligne");
      menuItem2.setName("menuItem2");
      menuItem2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem2ActionPerformed(e);
        }
      });
      BTD.add(menuItem2);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField OB16;
  private XRiTextField OB17;
  private XRiTextField OB15;
  private XRiTextField OB14;
  private XRiTextField OB13;
  private XRiTextField OB12;
  private XRiTextField OB11;
  private XRiTextField OB10;
  private XRiTextField OB9;
  private XRiTextField OB8;
  private XRiTextField OB7;
  private XRiTextField OB6;
  private XRiTextField OB5;
  private XRiTextField OB4;
  private XRiTextField OB3;
  private XRiTextField OB2;
  private XRiTextField OB1;
  private XRiTextField CLTEL;
  private XRiTextField CLFAX;
  private JLabel OBJ_35_OBJ_35;
  private JLabel OBJ_37_OBJ_37;
  private JPanel panel2;
  private JLabel OBJ_47;
  private XRiTextField OB181;
  private XRiTextField OB182;
  private XRiTextField OB183;
  private XRiTextField OB184;
  private XRiTextField OB185;
  private JPopupMenu BTD;
  private JMenuItem menuItem1;
  private JMenuItem menuItem2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
