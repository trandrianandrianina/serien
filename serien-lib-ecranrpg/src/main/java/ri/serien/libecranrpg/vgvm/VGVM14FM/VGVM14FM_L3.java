
package ri.serien.libecranrpg.vgvm.VGVM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM14FM_L3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _LDEZ_Title = { "LDET", };
  private String[][] _LDEZ_Data = { { "LDEZ", }, };
  private int[] _LDEZ_Width = { 555, };
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "@TETLD/0,58^@", "@TETLD/60,8^@", "@TETLD/69,8^@", };
  private String[][] _WTP01_Data = { { "L301", "W1X01", "W2X01", }, { "L302", "W1X02", "W2X02", }, { "L303", "W1X03", "W2X03", },
      { "L304", "W1X04", "W2X04", }, { "L305", "W1X05", "W2X05", }, { "L306", "W1X06", "W2X06", }, { "L307", "W1X07", "W2X07", },
      { "L308", "W1X08", "W2X08", }, { "L309", "W1X09", "W2X09", }, { "L310", "W1X10", "W2X10", }, { "L311", "W1X11", "W2X11", },
      { "L312", "W1X12", "W2X12", }, { "L313", "W1X13", "W2X13", }, { "L314", "W1X14", "W2X14", }, { "L315", "W1X15", "W2X15", }, };
  private int[] _WTP01_Width = { 417, 60, 58, };
  // private String[] _LIST2_Top=null;
  
  public VGVM14FM_L3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, true, null, null, null, null);
    LDEZ.setAspectTable(null, _LDEZ_Title, _LDEZ_Data, _LDEZ_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITRE@ ")).trim());
    E1NUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NUM@")).trim());
    P14ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P14ETB@")).trim());
    E1SUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1SUF@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTRI@")).trim());
    OBJ_69.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCPL@")).trim());
    OBJ_70.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLTEL@")).trim());
    OBJ_68.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEB@")).trim());
    OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESERR@")).trim());
    OBJ_76.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DBUT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    // majTable( LDEZ, LDEZ.get_LIST_Title_Data_Brut(), _LDEZ_Top);
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    E1SUF.setVisible(lexique.isPresent("E1SUF"));
    P14ETB.setEnabled(lexique.isPresent("P14ETB"));
    WNLIS.setVisible(!lexique.HostFieldGetData("SOUTIT").trim().equalsIgnoreCase("(Multi-bons)"));
    WNLIS.setEnabled(lexique.isPresent("WNLIS"));
    E1NUM.setVisible(lexique.isPresent("E1NUM"));
    E1NFA.setVisible(lexique.isPresent("E1NFA"));
    OBJ_68.setVisible(lexique.isPresent("WEB"));
    OBJ_78.setVisible(lexique.isPresent("MESERR"));
    WARGC.setVisible(lexique.isPresent("WARGC"));
    OBJ_61.setVisible(WARGC.isVisible());
    OBJ_70.setVisible(lexique.isPresent("CLTEL"));
    OBJ_73.setVisible(lexique.isPresent("LIBTRI"));
    OBJ_75.setVisible(!lexique.HostFieldGetData("SOUTIT").trim().equalsIgnoreCase("(Multi-bons)"));
    OBJ_47.setVisible(lexique.isPresent("CLNOM"));
    OBJ_69.setVisible(lexique.isPresent("CLCPL"));
    OBJ_72.setVisible(lexique.isTrue("18"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
    p_bpresentation.setCodeEtablissement(P14ETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(P14ETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "3", "Enter");
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "6", "Enter");
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "9", "Enter");
    WTP01.setValeurTop("9");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "Enter", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WARGC = new XRiTextField();
    OBJ_44 = new JLabel();
    OBJ_61 = new JLabel();
    E1NUM = new RiZoneSortie();
    P14ETB = new RiZoneSortie();
    OBJ_42 = new JLabel();
    E1SUF = new RiZoneSortie();
    OBJ_47 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_73 = new RiZoneSortie();
    OBJ_72 = new JLabel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    SCROLLPANE_LIST2 = new JScrollPane();
    LDEZ = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_75 = new JLabel();
    WNLIS = new XRiTextField();
    OBJ_69 = new RiZoneSortie();
    OBJ_70 = new RiZoneSortie();
    OBJ_68 = new RiZoneSortie();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_79 = new JLabel();
    E1NFA = new XRiTextField();
    OBJ_78 = new JLabel();
    OBJ_76 = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1030, 630));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITRE@ ");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- WARGC ----
          WARGC.setComponentPopupMenu(BTD);
          WARGC.setName("WARGC");
          p_tete_gauche.add(WARGC);
          WARGC.setBounds(595, 1, 140, WARGC.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("N\u00b0 facture");
          OBJ_44.setName("OBJ_44");
          p_tete_gauche.add(OBJ_44);
          OBJ_44.setBounds(145, 5, 65, 20);

          //---- OBJ_61 ----
          OBJ_61.setText("Article");
          OBJ_61.setName("OBJ_61");
          p_tete_gauche.add(OBJ_61);
          OBJ_61.setBounds(555, 5, 45, 20);

          //---- E1NUM ----
          E1NUM.setComponentPopupMenu(BTD);
          E1NUM.setOpaque(false);
          E1NUM.setText("@E1NUM@");
          E1NUM.setHorizontalAlignment(SwingConstants.RIGHT);
          E1NUM.setName("E1NUM");
          p_tete_gauche.add(E1NUM);
          E1NUM.setBounds(210, 3, 60, E1NUM.getPreferredSize().height);

          //---- P14ETB ----
          P14ETB.setComponentPopupMenu(BTD);
          P14ETB.setOpaque(false);
          P14ETB.setText("@P14ETB@");
          P14ETB.setName("P14ETB");
          p_tete_gauche.add(P14ETB);
          P14ETB.setBounds(95, 3, 40, P14ETB.getPreferredSize().height);

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");
          p_tete_gauche.add(OBJ_42);
          OBJ_42.setBounds(5, 5, 90, 20);

          //---- E1SUF ----
          E1SUF.setComponentPopupMenu(BTD);
          E1SUF.setOpaque(false);
          E1SUF.setText("@E1SUF@");
          E1SUF.setName("E1SUF");
          p_tete_gauche.add(E1SUF);
          E1SUF.setBounds(275, 3, 20, E1SUF.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setText("@CLNOM@");
          OBJ_47.setOpaque(false);
          OBJ_47.setName("OBJ_47");
          p_tete_gauche.add(OBJ_47);
          OBJ_47.setBounds(305, 3, 231, OBJ_47.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Lignes/non stock");
              riSousMenu_bt6.setToolTipText("Lignes/non stock");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Autres vues");
              riSousMenu_bt7.setToolTipText("Autres vues");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(830, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Affichage"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_73 ----
            OBJ_73.setText("@LIBTRI@");
            OBJ_73.setName("OBJ_73");
            panel1.add(OBJ_73);
            OBJ_73.setBounds(555, 367, 197, OBJ_73.getPreferredSize().height);

            //---- OBJ_72 ----
            OBJ_72.setText("Tri\u00e9 par");
            OBJ_72.setName("OBJ_72");
            panel1.add(OBJ_72);
            OBJ_72.setBounds(485, 369, 62, 20);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(20, 90, 705, 270);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- LDEZ ----
              LDEZ.setName("LDEZ");
              SCROLLPANE_LIST2.setViewportView(LDEZ);
            }
            panel1.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(20, 40, 705, 45);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(730, 90, 25, 123);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(730, 232, 25, 128);

            //---- OBJ_75 ----
            OBJ_75.setText("Num\u00e9ro de la premi\u00e8re ligne \u00e0 afficher");
            OBJ_75.setName("OBJ_75");
            panel1.add(OBJ_75);
            OBJ_75.setBounds(70, 371, 237, 16);

            //---- WNLIS ----
            WNLIS.setName("WNLIS");
            panel1.add(WNLIS);
            WNLIS.setBounds(20, 365, 44, WNLIS.getPreferredSize().height);
          }

          //---- OBJ_69 ----
          OBJ_69.setText("@CLCPL@");
          OBJ_69.setName("OBJ_69");

          //---- OBJ_70 ----
          OBJ_70.setText("@CLTEL@");
          OBJ_70.setName("OBJ_70");

          //---- OBJ_68 ----
          OBJ_68.setText("@WEB@");
          OBJ_68.setName("OBJ_68");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 303, GroupLayout.PREFERRED_SIZE)
                    .addGap(14, 14, 14)
                    .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 775, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(24, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 413, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Affichage");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_13 ----
      OBJ_13.setText("Fiche article");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Consultation article");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Adresses de stock");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }

    //---- OBJ_79 ----
    OBJ_79.setText("RECHERCHE");
    OBJ_79.setName("OBJ_79");

    //---- E1NFA ----
    E1NFA.setComponentPopupMenu(BTD);
    E1NFA.setName("E1NFA");

    //---- OBJ_78 ----
    OBJ_78.setText("@MESERR@");
    OBJ_78.setName("OBJ_78");

    //---- OBJ_76 ----
    OBJ_76.setText("@DBUT@");
    OBJ_76.setName("OBJ_76");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField WARGC;
  private JLabel OBJ_44;
  private JLabel OBJ_61;
  private RiZoneSortie E1NUM;
  private RiZoneSortie P14ETB;
  private JLabel OBJ_42;
  private RiZoneSortie E1SUF;
  private RiZoneSortie OBJ_47;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie OBJ_73;
  private JLabel OBJ_72;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable LDEZ;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_75;
  private XRiTextField WNLIS;
  private RiZoneSortie OBJ_69;
  private RiZoneSortie OBJ_70;
  private RiZoneSortie OBJ_68;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JLabel OBJ_79;
  private XRiTextField E1NFA;
  private JLabel OBJ_78;
  private RiZoneSortie OBJ_76;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
