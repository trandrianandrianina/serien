//$$david$$ ££06/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vgvm.VGVM35FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.transport.snzonegeographique.SNZoneGeographique;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM35FM_C1 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVM35FM_C1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    // riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    setTitle(interpreteurD.analyseExpression("Fiche représentant"));
    
    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB"));
    
    // Magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "RPMAG");
    snMagasin.setEnabled(!isConsultation);
    
    // Zone géographique
    snZoneGeographique.setSession(getSession());
    snZoneGeographique.setIdEtablissement(idEtablissement);
    snZoneGeographique.charger(false);
    snZoneGeographique.setSelectionParChampRPG(lexique, "RPGEO");
    snZoneGeographique.setEnabled(!isConsultation);
    
    // Vendeur
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(idEtablissement);
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "RPVDE");
    snVendeur.setEnabled(!isConsultation);
    
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snMagasin.renseignerChampRPG(lexique, "RPMAG");
    snZoneGeographique.renseignerChampRPG(lexique, "RPGEO");
    snVendeur.renseignerChampRPG(lexique, "RPVDE");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    sNPanelContenu1 = new SNPanelContenu();
    sNPanelTitre1 = new SNPanelTitre();
    sNLabelChamp1 = new SNLabelChamp();
    RPNOF = new XRiTextField();
    sNLabelChamp2 = new SNLabelChamp();
    snZoneGeographique = new SNZoneGeographique();
    sNLabelChamp3 = new SNLabelChamp();
    snMagasin = new SNMagasin();
    sNLabelChamp4 = new SNLabelChamp();
    snVendeur = new SNVendeur();
    snBarreBouton = new SNBarreBouton();
    panel2 = new JPanel();
    RPGEO = new XRiTextField();
    RPMAG = new XRiTextField();
    RPVDE = new XRiTextField();
    OBJ_28_OBJ_28 = new JLabel();
    OBJ_31_OBJ_31 = new JLabel();
    OBJ_34_OBJ_34 = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(810, 275));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== sNPanelContenu1 ========
    {
      sNPanelContenu1.setName("sNPanelContenu1");
      sNPanelContenu1.setLayout(new GridLayout());
      
      // ======== sNPanelTitre1 ========
      {
        sNPanelTitre1.setTitre("G\u00e9n\u00e9ralit\u00e9s");
        sNPanelTitre1.setName("sNPanelTitre1");
        sNPanelTitre1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelTitre1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) sNPanelTitre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- sNLabelChamp1 ----
        sNLabelChamp1.setText("Notes de frais");
        sNLabelChamp1.setName("sNLabelChamp1");
        sNPanelTitre1.add(sNLabelChamp1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- RPNOF ----
        RPNOF.setMaximumSize(new Dimension(80, 30));
        RPNOF.setMinimumSize(new Dimension(80, 30));
        RPNOF.setPreferredSize(new Dimension(80, 30));
        RPNOF.setName("RPNOF");
        sNPanelTitre1.add(RPNOF, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp2 ----
        sNLabelChamp2.setText("Zone g\u00e9ographique");
        sNLabelChamp2.setName("sNLabelChamp2");
        sNPanelTitre1.add(sNLabelChamp2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snZoneGeographique ----
        snZoneGeographique.setName("snZoneGeographique");
        sNPanelTitre1.add(snZoneGeographique, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp3 ----
        sNLabelChamp3.setText("Magasin");
        sNLabelChamp3.setName("sNLabelChamp3");
        sNPanelTitre1.add(sNLabelChamp3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snMagasin ----
        snMagasin.setName("snMagasin");
        sNPanelTitre1.add(snMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp4 ----
        sNLabelChamp4.setText("Vendeur");
        sNLabelChamp4.setName("sNLabelChamp4");
        sNPanelTitre1.add(sNLabelChamp4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snVendeur ----
        snVendeur.setName("snVendeur");
        sNPanelTitre1.add(snVendeur, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(sNPanelTitre1);
    }
    add(sNPanelContenu1, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== panel2 ========
    {
      panel2.setBorder(new TitledBorder("G\u00e9n\u00e9ralit\u00e9s"));
      panel2.setOpaque(false);
      panel2.setName("panel2");
      panel2.setLayout(null);
      
      // ---- RPGEO ----
      RPGEO.setName("RPGEO");
      panel2.add(RPGEO);
      RPGEO.setBounds(175, 85, 68, RPGEO.getPreferredSize().height);
      
      // ---- RPMAG ----
      RPMAG.setName("RPMAG");
      panel2.add(RPMAG);
      RPMAG.setBounds(175, 120, 34, RPMAG.getPreferredSize().height);
      
      // ---- RPVDE ----
      RPVDE.setName("RPVDE");
      panel2.add(RPVDE);
      RPVDE.setBounds(175, 155, 40, RPVDE.getPreferredSize().height);
      
      // ---- OBJ_28_OBJ_28 ----
      OBJ_28_OBJ_28.setText("Zone g\u00e9ographique");
      OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");
      panel2.add(OBJ_28_OBJ_28);
      OBJ_28_OBJ_28.setBounds(35, 89, 130, 20);
      
      // ---- OBJ_31_OBJ_31 ----
      OBJ_31_OBJ_31.setText("Code magasin");
      OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");
      panel2.add(OBJ_31_OBJ_31);
      OBJ_31_OBJ_31.setBounds(35, 124, 109, 20);
      
      // ---- OBJ_34_OBJ_34 ----
      OBJ_34_OBJ_34.setText("Code vendeur");
      OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
      panel2.add(OBJ_34_OBJ_34);
      OBJ_34_OBJ_34.setBounds(35, 159, 106, 20);
      
      { // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < panel2.getComponentCount(); i++) {
          Rectangle bounds = panel2.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel2.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel2.setMinimumSize(preferredSize);
        panel2.setPreferredSize(preferredSize);
      }
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu sNPanelContenu1;
  private SNPanelTitre sNPanelTitre1;
  private SNLabelChamp sNLabelChamp1;
  private XRiTextField RPNOF;
  private SNLabelChamp sNLabelChamp2;
  private SNZoneGeographique snZoneGeographique;
  private SNLabelChamp sNLabelChamp3;
  private SNMagasin snMagasin;
  private SNLabelChamp sNLabelChamp4;
  private SNVendeur snVendeur;
  private SNBarreBouton snBarreBouton;
  private JPanel panel2;
  private XRiTextField RPGEO;
  private XRiTextField RPMAG;
  private XRiTextField RPVDE;
  private JLabel OBJ_28_OBJ_28;
  private JLabel OBJ_31_OBJ_31;
  private JLabel OBJ_34_OBJ_34;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
