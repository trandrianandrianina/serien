
package ri.serien.libecranrpg.vgvm.VGVMQ1FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVMQ1FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _CAN01_Title = { "Canal", "Quota initial", "Quota actualisé", "Quantité prévue", "Qté en commande", "Quantité livrée",
      "Qté totale vendue", "Solde quota actualisé", };
  private String[][] _CAN01_Data = { { "CAN01", "QTI01", "QTA01", "QTP01", "QTC01", "QTS01", "QTV01", "QTR01", },
      { "CAN02", "QTI02", "QTA02", "QTP02", "QTC02", "QTS02", "QTV02", "QTR02", },
      { "CAN03", "QTI03", "QTA03", "QTP03", "QTC03", "QTS03", "QTV03", "QTR03", },
      { "CAN04", "QTI04", "QTA04", "QTP04", "QTC04", "QTS04", "QTV04", "QTR04", },
      { "CAN05", "QTI05", "QTA05", "QTP05", "QTC05", "QTS05", "QTV05", "QTR05", },
      { "CAN06", "QTI06", "QTA06", "QTP06", "QTC06", "QTS06", "QTV06", "QTR06", },
      { "CAN07", "QTI07", "QTA07", "QTP07", "QTC07", "QTS07", "QTV07", "QTR07", },
      { "CAN08", "QTI08", "QTA08", "QTP08", "QTC08", "QTS08", "QTV08", "QTR08", },
      { "CAN09", "QTI09", "QTA09", "QTP09", "QTC09", "QTS09", "QTV09", "QTR09", },
      { "CAN10", "QTI10", "QTA10", "QTP10", "QTC10", "QTS10", "QTV10", "QTR10", },
      { "CAN11", "QTI11", "QTA11", "QTP11", "QTC11", "QTS11", "QTV11", "QTR11", },
      { "CAN12", "QTI12", "QTA12", "QTP12", "QTC12", "QTS12", "QTV12", "QTR12", },
      { "CAN13", "QTI13", "QTA13", "QTP13", "QTC13", "QTS13", "QTV13", "QTR13", },
      { "CAN14", "QTI14", "QTA14", "QTP14", "QTC14", "QTS14", "QTV14", "QTR14", },
      { "CAN15", "QTI15", "QTA15", "QTP15", "QTC15", "QTS15", "QTV15", "QTR15", }, };
  private int[] _CAN01_Width = { 40, 67, 69, 66, 66, 66, 69, 72, };
  
  public VGVMQ1FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    CAN01.setAspectTable(null, _CAN01_Title, _CAN01_Data, _CAN01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@(TITLE)@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRQSC@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    OBJ_106.setVisible(lexique.isPresent("TOTQNA"));
    OBJ_105.setVisible(lexique.isPresent("TOTQNA"));
    WPER.setVisible(lexique.isPresent("WPER"));
    INDMAG.setVisible(lexique.isPresent("INDMAG"));
    OBJ_52.setVisible(lexique.isPresent("WRQSC"));
    WANNEE.setVisible(lexique.isPresent("WANNEE"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    TOTQNA.setVisible(lexique.isPresent("TOTQNA"));
    TQTA.setVisible(lexique.isPresent("TOTQNA"));
    TSTK0.setVisible(lexique.isPresent("TOTQNA"));
    OBJ_107.setVisible(lexique.isPresent("TOTQNA"));
    INDART.setVisible(lexique.isPresent("INDART"));
    OBJ_46.setVisible(lexique.isPresent("A1LIB"));
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  FICHE QUOTA @PGQ1LB@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_41 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    INDETB = new XRiTextField();
    WANNEE = new XRiTextField();
    INDMAG = new XRiTextField();
    WPER = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    OBJ_44 = new JLabel();
    INDART = new XRiTextField();
    OBJ_46 = new RiZoneSortie();
    OBJ_51 = new JLabel();
    OBJ_52 = new RiZoneSortie();
    OBJ_53 = new JLabel();
    panel1 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    CAN01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel2 = new JPanel();
    OBJ_107 = new JLabel();
    TSTK0 = new XRiTextField();
    TQTA = new XRiTextField();
    TOTQNA = new XRiTextField();
    OBJ_105 = new JLabel();
    OBJ_106 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Quotas");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_41 ----
          OBJ_41.setText("Ann\u00e9e/Per");
          OBJ_41.setName("OBJ_41");
          p_tete_gauche.add(OBJ_41);
          OBJ_41.setBounds(250, 5, 72, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("Etablissement");
          OBJ_39.setName("OBJ_39");
          p_tete_gauche.add(OBJ_39);
          OBJ_39.setBounds(5, 5, 90, 20);

          //---- OBJ_40 ----
          OBJ_40.setText("Magasin");
          OBJ_40.setName("OBJ_40");
          p_tete_gauche.add(OBJ_40);
          OBJ_40.setBounds(155, 5, 55, 20);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(100, 1, 40, INDETB.getPreferredSize().height);

          //---- WANNEE ----
          WANNEE.setComponentPopupMenu(BTD);
          WANNEE.setName("WANNEE");
          p_tete_gauche.add(WANNEE);
          WANNEE.setBounds(325, 1, 42, WANNEE.getPreferredSize().height);

          //---- INDMAG ----
          INDMAG.setComponentPopupMenu(BTD);
          INDMAG.setName("INDMAG");
          p_tete_gauche.add(INDMAG);
          INDMAG.setBounds(210, 1, 30, INDMAG.getPreferredSize().height);

          //---- WPER ----
          WPER.setComponentPopupMenu(BTD);
          WPER.setName("WPER");
          p_tete_gauche.add(WPER);
          WPER.setBounds(370, 1, 26, WPER.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder(""));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_44 ----
            OBJ_44.setText("@(TITLE)@");
            OBJ_44.setName("OBJ_44");
            panel3.add(OBJ_44);
            OBJ_44.setBounds(20, 14, 58, 20);

            //---- INDART ----
            INDART.setComponentPopupMenu(BTD);
            INDART.setName("INDART");
            panel3.add(INDART);
            INDART.setBounds(135, 10, 210, INDART.getPreferredSize().height);

            //---- OBJ_46 ----
            OBJ_46.setText("@A1LIB@");
            OBJ_46.setName("OBJ_46");
            panel3.add(OBJ_46);
            OBJ_46.setBounds(360, 12, 319, OBJ_46.getPreferredSize().height);

            //---- OBJ_51 ----
            OBJ_51.setText("Seuil critique");
            OBJ_51.setName("OBJ_51");
            panel3.add(OBJ_51);
            OBJ_51.setBounds(20, 42, 79, 20);

            //---- OBJ_52 ----
            OBJ_52.setText("@WRQSC@");
            OBJ_52.setName("OBJ_52");
            panel3.add(OBJ_52);
            OBJ_52.setBounds(135, 40, 90, OBJ_52.getPreferredSize().height);

            //---- OBJ_53 ----
            OBJ_53.setText("%");
            OBJ_53.setName("OBJ_53");
            panel3.add(OBJ_53);
            OBJ_53.setBounds(230, 43, 12, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- CAN01 ----
              CAN01.setName("CAN01");
              SCROLLPANE_LIST2.setViewportView(CAN01);
            }
            panel1.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(20, 35, 790, 275);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            BT_PGUP.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_PGUPActionPerformed(e);
              }
            });
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(820, 35, 25, 135);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            BT_PGDOWN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_PGDOWNActionPerformed(e);
              }
            });
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(820, 175, 25, 135);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Total"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_107 ----
            OBJ_107.setText("--> non affect\u00e9");
            OBJ_107.setName("OBJ_107");
            panel2.add(OBJ_107);
            OBJ_107.setBounds(365, 35, 121, 20);

            //---- TSTK0 ----
            TSTK0.setName("TSTK0");
            panel2.add(TSTK0);
            TSTK0.setBounds(25, 30, 74, TSTK0.getPreferredSize().height);

            //---- TQTA ----
            TQTA.setName("TQTA");
            panel2.add(TQTA);
            TQTA.setBounds(153, 30, 74, TQTA.getPreferredSize().height);

            //---- TOTQNA ----
            TOTQNA.setName("TOTQNA");
            panel2.add(TOTQNA);
            TOTQNA.setBounds(281, 30, 82, TOTQNA.getPreferredSize().height);

            //---- OBJ_105 ----
            OBJ_105.setText("-");
            OBJ_105.setHorizontalTextPosition(SwingConstants.CENTER);
            OBJ_105.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_105.setName("OBJ_105");
            panel2.add(OBJ_105);
            OBJ_105.setBounds(101, 34, 50, 20);

            //---- OBJ_106 ----
            OBJ_106.setText("=");
            OBJ_106.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_106.setName("OBJ_106");
            panel2.add(OBJ_106);
            OBJ_106.setBounds(229, 34, 50, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 863, Short.MAX_VALUE)
                  .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(23, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(47, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_41;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private XRiTextField INDETB;
  private XRiTextField WANNEE;
  private XRiTextField INDMAG;
  private XRiTextField WPER;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel3;
  private JLabel OBJ_44;
  private XRiTextField INDART;
  private RiZoneSortie OBJ_46;
  private JLabel OBJ_51;
  private RiZoneSortie OBJ_52;
  private JLabel OBJ_53;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable CAN01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel2;
  private JLabel OBJ_107;
  private XRiTextField TSTK0;
  private XRiTextField TQTA;
  private XRiTextField TOTQNA;
  private JLabel OBJ_105;
  private JLabel OBJ_106;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
