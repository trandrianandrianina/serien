
package ri.serien.libecranrpg.vgvm.VGVM142F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM142F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM142F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    XEDTA.setValeursSelection("1", "0");
    XEDTP.setValeursSelection("1", "0");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    DEBNLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DEBNLI@")).trim());
    TOTMHT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTMHT@")).trim());
    TOTPRV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTPRV@")).trim());
    TOTMAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTMAR@")).trim());
    PCTMAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PCTMAR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Sous total de lignes"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TITLB1 = new XRiTextField();
    TITLB2 = new XRiTextField();
    TOTLB1 = new XRiTextField();
    TOTLB2 = new XRiTextField();
    OBJ_21 = new JLabel();
    OBJ_23 = new JLabel();
    OBJ_29 = new JLabel();
    DEBNLI = new RiZoneSortie();
    FINNLI = new XRiTextField();
    OBJ_24 = new JLabel();
    WIN21 = new XRiTextField();
    OBJ_25 = new JLabel();
    TOTMHT = new RiZoneSortie();
    TOTPRV = new RiZoneSortie();
    TOTMAR = new RiZoneSortie();
    OBJ_30 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_32 = new JLabel();
    PCTMAR = new RiZoneSortie();
    OBJ_33 = new JLabel();
    XEDTA = new XRiCheckBox();
    XEDTP = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(735, 400));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Recalculer");
            riSousMenu_bt6.setToolTipText("Recalculer le sous total");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Supprimer");
            riSousMenu_bt7.setToolTipText("Supprimer la ligne de sous total");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Sous total"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- TITLB1 ----
          TITLB1.setComponentPopupMenu(BTD);
          TITLB1.setName("TITLB1");
          panel1.add(TITLB1);
          TITLB1.setBounds(210, 65, 310, TITLB1.getPreferredSize().height);

          //---- TITLB2 ----
          TITLB2.setComponentPopupMenu(BTD);
          TITLB2.setName("TITLB2");
          panel1.add(TITLB2);
          TITLB2.setBounds(210, 95, 310, TITLB2.getPreferredSize().height);

          //---- TOTLB1 ----
          TOTLB1.setComponentPopupMenu(BTD);
          TOTLB1.setName("TOTLB1");
          panel1.add(TOTLB1);
          TOTLB1.setBounds(210, 155, 310, TOTLB1.getPreferredSize().height);

          //---- TOTLB2 ----
          TOTLB2.setComponentPopupMenu(BTD);
          TOTLB2.setName("TOTLB2");
          panel1.add(TOTLB2);
          TOTLB2.setBounds(210, 185, 310, TOTLB2.getPreferredSize().height);

          //---- OBJ_21 ----
          OBJ_21.setText("Ligne de d\u00e9but");
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(30, 70, 115, 20);

          //---- OBJ_23 ----
          OBJ_23.setText("Titre de la ligne");
          OBJ_23.setFont(OBJ_23.getFont().deriveFont(OBJ_23.getFont().getStyle() | Font.BOLD));
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(210, 45, 160, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("Ligne de fin");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(30, 159, 82, 20);

          //---- DEBNLI ----
          DEBNLI.setComponentPopupMenu(BTD);
          DEBNLI.setText("@DEBNLI@");
          DEBNLI.setHorizontalAlignment(SwingConstants.RIGHT);
          DEBNLI.setName("DEBNLI");
          panel1.add(DEBNLI);
          DEBNLI.setBounds(150, 66, 44, DEBNLI.getPreferredSize().height);

          //---- FINNLI ----
          FINNLI.setComponentPopupMenu(BTD);
          FINNLI.setHorizontalAlignment(SwingConstants.RIGHT);
          FINNLI.setName("FINNLI");
          panel1.add(FINNLI);
          FINNLI.setBounds(150, 155, 44, FINNLI.getPreferredSize().height);

          //---- OBJ_24 ----
          OBJ_24.setText("Regroupement");
          OBJ_24.setName("OBJ_24");
          panel1.add(OBJ_24);
          OBJ_24.setBounds(395, 34, 100, 20);

          //---- WIN21 ----
          WIN21.setComponentPopupMenu(BTD);
          WIN21.setName("WIN21");
          panel1.add(WIN21);
          WIN21.setBounds(500, 30, 20, WIN21.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setText("Texte de la ligne de sous total");
          OBJ_25.setFont(OBJ_25.getFont().deriveFont(OBJ_25.getFont().getStyle() | Font.BOLD));
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(210, 135, 300, 20);

          //---- TOTMHT ----
          TOTMHT.setText("@TOTMHT@");
          TOTMHT.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTMHT.setName("TOTMHT");
          panel1.add(TOTMHT);
          TOTMHT.setBounds(419, 230, 101, TOTMHT.getPreferredSize().height);

          //---- TOTPRV ----
          TOTPRV.setText("@TOTPRV@");
          TOTPRV.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTPRV.setName("TOTPRV");
          panel1.add(TOTPRV);
          TOTPRV.setBounds(419, 265, 101, TOTPRV.getPreferredSize().height);

          //---- TOTMAR ----
          TOTMAR.setText("@TOTMAR@");
          TOTMAR.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTMAR.setName("TOTMAR");
          panel1.add(TOTMAR);
          TOTMAR.setBounds(419, 300, 101, TOTMAR.getPreferredSize().height);

          //---- OBJ_30 ----
          OBJ_30.setText("Montant total H.T.");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(210, 232, 200, 20);

          //---- OBJ_31 ----
          OBJ_31.setText("Prix de revient");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(210, 267, 200, 20);

          //---- OBJ_32 ----
          OBJ_32.setText("Marge");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(210, 302, 75, 20);

          //---- PCTMAR ----
          PCTMAR.setText("@PCTMAR@");
          PCTMAR.setHorizontalAlignment(SwingConstants.RIGHT);
          PCTMAR.setName("PCTMAR");
          panel1.add(PCTMAR);
          PCTMAR.setBounds(295, 300, 44, PCTMAR.getPreferredSize().height);

          //---- OBJ_33 ----
          OBJ_33.setText("%");
          OBJ_33.setName("OBJ_33");
          panel1.add(OBJ_33);
          OBJ_33.setBounds(345, 302, 20, 20);

          //---- XEDTA ----
          XEDTA.setText("Edition du d\u00e9tail des articles");
          XEDTA.setName("XEDTA");
          panel1.add(XEDTA);
          XEDTA.setBounds(30, 340, 185, XEDTA.getPreferredSize().height);

          //---- XEDTP ----
          XEDTP.setText("Edition du d\u00e9tail des prix des articles");
          XEDTP.setName("XEDTP");
          panel1.add(XEDTP);
          XEDTP.setBounds(295, 340, 235, XEDTP.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 544, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField TITLB1;
  private XRiTextField TITLB2;
  private XRiTextField TOTLB1;
  private XRiTextField TOTLB2;
  private JLabel OBJ_21;
  private JLabel OBJ_23;
  private JLabel OBJ_29;
  private RiZoneSortie DEBNLI;
  private XRiTextField FINNLI;
  private JLabel OBJ_24;
  private XRiTextField WIN21;
  private JLabel OBJ_25;
  private RiZoneSortie TOTMHT;
  private RiZoneSortie TOTPRV;
  private RiZoneSortie TOTMAR;
  private JLabel OBJ_30;
  private JLabel OBJ_31;
  private JLabel OBJ_32;
  private RiZoneSortie PCTMAR;
  private JLabel OBJ_33;
  private XRiCheckBox XEDTA;
  private XRiCheckBox XEDTP;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
