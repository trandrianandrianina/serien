
package ri.serien.libecranrpg.vgvm.VGVM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM05FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] WTCD_Value = { "N", "B", "K", "R", "S", "+", "-", "", };
  
  public VGVM05FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    WTCD.setValeurs(WTCD_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    CLNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LD01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD01@")).trim());
    LD02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD02@")).trim());
    LD03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD03@")).trim());
    LD04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD04@")).trim());
    LD05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD05@")).trim());
    LD06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD06@")).trim());
    LD07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD07@")).trim());
    LD08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD08@")).trim());
    LD09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD09@")).trim());
    LD10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD10@")).trim());
    LD11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD11@")).trim());
    LD12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD12@")).trim());
    LD13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD13@")).trim());
    LD14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD14@")).trim());
    LD15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD15@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UTIT@")).trim());
    PL01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL01@")).trim());
    PL02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL02@")).trim());
    PL03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL03@")).trim());
    PL04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL04@")).trim());
    PL05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL05@")).trim());
    PL06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL06@")).trim());
    PL07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL07@")).trim());
    PL08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL08@")).trim());
    PL09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL09@")).trim());
    PL10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL10@")).trim());
    PL11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL11@")).trim());
    PL12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL12@")).trim());
    PL13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL13@")).trim());
    PL14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL14@")).trim());
    PL15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL15@")).trim());
    WLRAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLRAT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    // WTCD.setSelectedIndex(getIndice("WTCD", WTCD_Value));
    
    
    OBJ_82.setVisible(lexique.isPresent("DTDEBX"));
    OBJ_78.setVisible(lexique.isPresent("DTDEBX"));
    // DTFINX.setVisible( lexique.isPresent("DTFINX"));
    // DTDEBX.setVisible( lexique.isPresent("DTDEBX"));
    OBJ_81.setVisible(lexique.isPresent("DTDEBX"));
    OBJ_61.setVisible(lexique.isPresent("WTCD"));
    // WTCD.setVisible( lexique.isPresent("WTCD"));
    label1.setVisible(lexique.isPresent("WRAT"));
    
    // visibilités click droit
    OBJ_18.setVisible(lexique.HostFieldGetData("ULDOPT").contains("2"));
    OBJ_19.setVisible(lexique.HostFieldGetData("ULDOPT").contains("3"));
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("WTCD", 0, WTCD_Value[WTCD.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    String nomZone = BTD.getInvoker().getName();
    int ligne;
    if (nomZone.length() == 5) {
      ligne = Integer.parseInt(nomZone.substring(3, 5));
    }
    else {
      if (nomZone.startsWith("Z")) {
        ligne = Integer.parseInt(nomZone.substring(3, 4));
      }
      else {
        ligne = Integer.parseInt(nomZone.substring(2, 4));
      }
    }
    String top = "WTP" + String.valueOf((ligne) < 10 ? "0" + (ligne) : (ligne));
    lexique.HostFieldPutData(top, 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    String nomZone = BTD.getInvoker().getName();
    int ligne;
    if (nomZone.length() == 5) {
      ligne = Integer.parseInt(nomZone.substring(3, 5));
    }
    else {
      if (nomZone.startsWith("Z")) {
        ligne = Integer.parseInt(nomZone.substring(3, 4));
      }
      else {
        ligne = Integer.parseInt(nomZone.substring(2, 4));
      }
    }
    String top = "WTP" + String.valueOf((ligne) < 10 ? "0" + (ligne) : (ligne));
    lexique.HostFieldPutData(top, 0, "2");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    String nomZone = BTD.getInvoker().getName();
    int ligne;
    if (nomZone.length() == 5) {
      ligne = Integer.parseInt(nomZone.substring(3, 5));
    }
    else {
      if (nomZone.startsWith("Z")) {
        ligne = Integer.parseInt(nomZone.substring(3, 4));
      }
      else {
        ligne = Integer.parseInt(nomZone.substring(2, 4));
      }
    }
    String top = "WTP" + String.valueOf((ligne) < 10 ? "0" + (ligne) : (ligne));
    lexique.HostFieldPutData(top, 0, "3");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void DoubleMouseClicked(MouseEvent e) {
    if (e.getClickCount() == 2) {
      String nomZone = e.getComponent().getName();
      int ligne;
      if (nomZone.length() == 5) {
        ligne = Integer.parseInt(nomZone.substring(3, 5));
      }
      else {
        if (nomZone.startsWith("Z")) {
          ligne = Integer.parseInt(nomZone.substring(3, 4));
        }
        else {
          ligne = Integer.parseInt(nomZone.substring(2, 4));
        }
      }
      String top = "WTP" + String.valueOf((ligne) < 10 ? "0" + (ligne) : (ligne));
      lexique.HostFieldPutData(top, 0, "1");
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WETB = new XRiTextField();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    BT_ChgSoc = new SNBoutonDetail();
    WCNV = new XRiTextField();
    CLNOM = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    p_TCI1 = new JXTitledPanel();
    panel1 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    LD01 = new RiZoneSortie();
    LD02 = new RiZoneSortie();
    LD03 = new RiZoneSortie();
    LD04 = new RiZoneSortie();
    LD05 = new RiZoneSortie();
    LD06 = new RiZoneSortie();
    LD07 = new RiZoneSortie();
    LD08 = new RiZoneSortie();
    LD09 = new RiZoneSortie();
    LD10 = new RiZoneSortie();
    LD11 = new RiZoneSortie();
    LD12 = new RiZoneSortie();
    LD13 = new RiZoneSortie();
    LD14 = new RiZoneSortie();
    LD15 = new RiZoneSortie();
    ZL11 = new XRiTextField();
    ZL12 = new XRiTextField();
    ZL13 = new XRiTextField();
    ZL14 = new XRiTextField();
    ZL15 = new XRiTextField();
    ZL16 = new XRiTextField();
    ZL17 = new XRiTextField();
    ZL18 = new XRiTextField();
    ZL19 = new XRiTextField();
    ZL110 = new XRiTextField();
    ZL111 = new XRiTextField();
    ZL112 = new XRiTextField();
    ZL113 = new XRiTextField();
    ZL114 = new XRiTextField();
    ZL115 = new XRiTextField();
    ZL21 = new XRiTextField();
    ZL22 = new XRiTextField();
    ZL23 = new XRiTextField();
    ZL24 = new XRiTextField();
    ZL25 = new XRiTextField();
    ZL26 = new XRiTextField();
    ZL27 = new XRiTextField();
    ZL28 = new XRiTextField();
    ZL29 = new XRiTextField();
    ZL210 = new XRiTextField();
    ZL211 = new XRiTextField();
    ZL212 = new XRiTextField();
    ZL213 = new XRiTextField();
    ZL214 = new XRiTextField();
    ZL215 = new XRiTextField();
    ZL31 = new XRiTextField();
    ZL32 = new XRiTextField();
    ZL33 = new XRiTextField();
    ZL34 = new XRiTextField();
    ZL35 = new XRiTextField();
    ZL36 = new XRiTextField();
    ZL37 = new XRiTextField();
    ZL38 = new XRiTextField();
    ZL39 = new XRiTextField();
    ZL310 = new XRiTextField();
    ZL311 = new XRiTextField();
    ZL312 = new XRiTextField();
    ZL313 = new XRiTextField();
    ZL314 = new XRiTextField();
    ZL315 = new XRiTextField();
    ZL41 = new XRiTextField();
    ZL42 = new XRiTextField();
    ZL43 = new XRiTextField();
    ZL44 = new XRiTextField();
    ZL45 = new XRiTextField();
    ZL46 = new XRiTextField();
    ZL47 = new XRiTextField();
    ZL48 = new XRiTextField();
    ZL49 = new XRiTextField();
    ZL410 = new XRiTextField();
    ZL411 = new XRiTextField();
    ZL412 = new XRiTextField();
    ZL413 = new XRiTextField();
    ZL414 = new XRiTextField();
    ZL415 = new XRiTextField();
    ZL51 = new XRiTextField();
    ZL52 = new XRiTextField();
    ZL53 = new XRiTextField();
    ZL54 = new XRiTextField();
    ZL55 = new XRiTextField();
    ZL56 = new XRiTextField();
    ZL57 = new XRiTextField();
    ZL58 = new XRiTextField();
    ZL59 = new XRiTextField();
    ZL510 = new XRiTextField();
    ZL511 = new XRiTextField();
    ZL512 = new XRiTextField();
    ZL513 = new XRiTextField();
    ZL514 = new XRiTextField();
    ZL515 = new XRiTextField();
    ZL61 = new XRiTextField();
    ZL62 = new XRiTextField();
    ZL63 = new XRiTextField();
    ZL64 = new XRiTextField();
    ZL65 = new XRiTextField();
    ZL66 = new XRiTextField();
    ZL67 = new XRiTextField();
    ZL68 = new XRiTextField();
    ZL69 = new XRiTextField();
    ZL610 = new XRiTextField();
    ZL611 = new XRiTextField();
    ZL612 = new XRiTextField();
    ZL613 = new XRiTextField();
    ZL614 = new XRiTextField();
    ZL615 = new XRiTextField();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    label5 = new JLabel();
    PL01 = new JLabel();
    PL02 = new JLabel();
    PL03 = new JLabel();
    PL04 = new JLabel();
    PL05 = new JLabel();
    PL06 = new JLabel();
    PL07 = new JLabel();
    PL08 = new JLabel();
    PL09 = new JLabel();
    PL10 = new JLabel();
    PL11 = new JLabel();
    PL12 = new JLabel();
    PL13 = new JLabel();
    PL14 = new JLabel();
    PL15 = new JLabel();
    panel2 = new JPanel();
    WTCD = new XRiComboBox();
    OBJ_61 = new JLabel();
    OBJ_81 = new JLabel();
    DTDEBX = new XRiCalendrier();
    DTFINX = new XRiCalendrier();
    OBJ_78 = new JLabel();
    OBJ_82 = new JLabel();
    label1 = new JLabel();
    WRAT = new XRiTextField();
    WLRAT = new RiZoneSortie();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_25 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Conditions de ventes articles");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");

          //---- OBJ_27 ----
          OBJ_27.setText("Etablissement");
          OBJ_27.setName("OBJ_27");

          //---- OBJ_28 ----
          OBJ_28.setText("Code CN");
          OBJ_28.setName("OBJ_28");

          //---- BT_ChgSoc ----
          BT_ChgSoc.setText("");
          BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
            }
          });

          //---- WCNV ----
          WCNV.setComponentPopupMenu(BTD);
          WCNV.setName("WCNV");

          //---- CLNOM ----
          CLNOM.setComponentPopupMenu(BTD);
          CLNOM.setText("@CLNOM@");
          CLNOM.setOpaque(false);
          CLNOM.setName("CLNOM");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(65, 65, 65)
                    .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addComponent(CLNOM, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(CLNOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Mode saisie pleine page");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Mode visualisation");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Recherche client");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("ON/OFF articles d\u00e9sact.");
              riSousMenu_bt9.setToolTipText("Mise ON/OFF de l'affichage des articles d\u00e9sactiv\u00e9s");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Autre affichage");
              riSousMenu_bt10.setToolTipText("Autre affichage en recherche articles");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(980, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== p_TCI1 ========
          {
            p_TCI1.setTitle("R\u00e9sultat de la recherche");
            p_TCI1.setBorder(new DropShadowBorder());
            p_TCI1.setTitleFont(p_TCI1.getTitleFont().deriveFont(p_TCI1.getTitleFont().getStyle() | Font.BOLD));
            p_TCI1.setName("p_TCI1");
            Container p_TCI1ContentContainer = p_TCI1.getContentContainer();
            p_TCI1ContentContainer.setLayout(null);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder(""));
              panel1.setOpaque(false);
              panel1.setName("panel1");

              //---- BT_PGUP ----
              BT_PGUP.setText("");
              BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
              BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGUP.setName("BT_PGUP");
              BT_PGUP.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  BT_PGUPActionPerformed(e);
                }
              });

              //---- BT_PGDOWN ----
              BT_PGDOWN.setText("");
              BT_PGDOWN.setToolTipText("Page suivante");
              BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGDOWN.setName("BT_PGDOWN");
              BT_PGDOWN.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  BT_PGDOWNActionPerformed(e);
                }
              });

              //---- LD01 ----
              LD01.setText("@LD01@");
              LD01.setComponentPopupMenu(BTD);
              LD01.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD01.setName("LD01");
              LD01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD02 ----
              LD02.setText("@LD02@");
              LD02.setComponentPopupMenu(BTD);
              LD02.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD02.setName("LD02");
              LD02.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD03 ----
              LD03.setText("@LD03@");
              LD03.setComponentPopupMenu(BTD);
              LD03.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD03.setName("LD03");
              LD03.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD04 ----
              LD04.setText("@LD04@");
              LD04.setComponentPopupMenu(BTD);
              LD04.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD04.setName("LD04");
              LD04.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD05 ----
              LD05.setText("@LD05@");
              LD05.setComponentPopupMenu(BTD);
              LD05.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD05.setName("LD05");
              LD05.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD06 ----
              LD06.setText("@LD06@");
              LD06.setComponentPopupMenu(BTD);
              LD06.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD06.setName("LD06");
              LD06.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD07 ----
              LD07.setText("@LD07@");
              LD07.setComponentPopupMenu(BTD);
              LD07.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD07.setName("LD07");
              LD07.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD08 ----
              LD08.setText("@LD08@");
              LD08.setComponentPopupMenu(BTD);
              LD08.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD08.setName("LD08");
              LD08.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD09 ----
              LD09.setText("@LD09@");
              LD09.setComponentPopupMenu(BTD);
              LD09.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD09.setName("LD09");
              LD09.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD10 ----
              LD10.setText("@LD10@");
              LD10.setComponentPopupMenu(BTD);
              LD10.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD10.setName("LD10");
              LD10.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD11 ----
              LD11.setText("@LD11@");
              LD11.setComponentPopupMenu(BTD);
              LD11.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD11.setName("LD11");
              LD11.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD12 ----
              LD12.setText("@LD12@");
              LD12.setComponentPopupMenu(BTD);
              LD12.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD12.setName("LD12");
              LD12.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD13 ----
              LD13.setText("@LD13@");
              LD13.setComponentPopupMenu(BTD);
              LD13.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD13.setName("LD13");
              LD13.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD14 ----
              LD14.setText("@LD14@");
              LD14.setComponentPopupMenu(BTD);
              LD14.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD14.setName("LD14");
              LD14.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- LD15 ----
              LD15.setText("@LD15@");
              LD15.setComponentPopupMenu(BTD);
              LD15.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD15.setName("LD15");
              LD15.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL11 ----
              ZL11.setComponentPopupMenu(BTD);
              ZL11.setName("ZL11");
              ZL11.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL12 ----
              ZL12.setComponentPopupMenu(BTD);
              ZL12.setName("ZL12");
              ZL12.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL13 ----
              ZL13.setComponentPopupMenu(BTD);
              ZL13.setName("ZL13");
              ZL13.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL14 ----
              ZL14.setComponentPopupMenu(BTD);
              ZL14.setName("ZL14");
              ZL14.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL15 ----
              ZL15.setComponentPopupMenu(BTD);
              ZL15.setName("ZL15");
              ZL15.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL16 ----
              ZL16.setComponentPopupMenu(BTD);
              ZL16.setName("ZL16");
              ZL16.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL17 ----
              ZL17.setComponentPopupMenu(BTD);
              ZL17.setName("ZL17");
              ZL17.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL18 ----
              ZL18.setComponentPopupMenu(BTD);
              ZL18.setName("ZL18");
              ZL18.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL19 ----
              ZL19.setComponentPopupMenu(BTD);
              ZL19.setName("ZL19");
              ZL19.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL110 ----
              ZL110.setComponentPopupMenu(BTD);
              ZL110.setName("ZL110");
              ZL110.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL111 ----
              ZL111.setComponentPopupMenu(BTD);
              ZL111.setName("ZL111");
              ZL111.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL112 ----
              ZL112.setComponentPopupMenu(BTD);
              ZL112.setName("ZL112");
              ZL112.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL113 ----
              ZL113.setComponentPopupMenu(BTD);
              ZL113.setName("ZL113");
              ZL113.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL114 ----
              ZL114.setComponentPopupMenu(BTD);
              ZL114.setName("ZL114");
              ZL114.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL115 ----
              ZL115.setComponentPopupMenu(BTD);
              ZL115.setName("ZL115");
              ZL115.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL21 ----
              ZL21.setComponentPopupMenu(BTD);
              ZL21.setName("ZL21");
              ZL21.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL22 ----
              ZL22.setComponentPopupMenu(BTD);
              ZL22.setName("ZL22");
              ZL22.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL23 ----
              ZL23.setComponentPopupMenu(BTD);
              ZL23.setName("ZL23");
              ZL23.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL24 ----
              ZL24.setComponentPopupMenu(BTD);
              ZL24.setName("ZL24");
              ZL24.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL25 ----
              ZL25.setComponentPopupMenu(BTD);
              ZL25.setName("ZL25");
              ZL25.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL26 ----
              ZL26.setComponentPopupMenu(BTD);
              ZL26.setName("ZL26");
              ZL26.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL27 ----
              ZL27.setComponentPopupMenu(BTD);
              ZL27.setName("ZL27");
              ZL27.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL28 ----
              ZL28.setComponentPopupMenu(BTD);
              ZL28.setName("ZL28");
              ZL28.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL29 ----
              ZL29.setComponentPopupMenu(BTD);
              ZL29.setName("ZL29");
              ZL29.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL210 ----
              ZL210.setComponentPopupMenu(BTD);
              ZL210.setName("ZL210");
              ZL210.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL211 ----
              ZL211.setComponentPopupMenu(BTD);
              ZL211.setName("ZL211");
              ZL211.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL212 ----
              ZL212.setComponentPopupMenu(BTD);
              ZL212.setName("ZL212");
              ZL212.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL213 ----
              ZL213.setComponentPopupMenu(BTD);
              ZL213.setName("ZL213");
              ZL213.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL214 ----
              ZL214.setComponentPopupMenu(BTD);
              ZL214.setName("ZL214");
              ZL214.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL215 ----
              ZL215.setComponentPopupMenu(BTD);
              ZL215.setName("ZL215");
              ZL215.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL31 ----
              ZL31.setComponentPopupMenu(BTD);
              ZL31.setName("ZL31");
              ZL31.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL32 ----
              ZL32.setComponentPopupMenu(BTD);
              ZL32.setName("ZL32");
              ZL32.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL33 ----
              ZL33.setComponentPopupMenu(BTD);
              ZL33.setName("ZL33");
              ZL33.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL34 ----
              ZL34.setComponentPopupMenu(BTD);
              ZL34.setName("ZL34");
              ZL34.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL35 ----
              ZL35.setComponentPopupMenu(BTD);
              ZL35.setName("ZL35");
              ZL35.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL36 ----
              ZL36.setComponentPopupMenu(BTD);
              ZL36.setName("ZL36");
              ZL36.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL37 ----
              ZL37.setComponentPopupMenu(BTD);
              ZL37.setName("ZL37");
              ZL37.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL38 ----
              ZL38.setComponentPopupMenu(BTD);
              ZL38.setName("ZL38");
              ZL38.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL39 ----
              ZL39.setComponentPopupMenu(BTD);
              ZL39.setName("ZL39");
              ZL39.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL310 ----
              ZL310.setComponentPopupMenu(BTD);
              ZL310.setName("ZL310");
              ZL310.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL311 ----
              ZL311.setComponentPopupMenu(BTD);
              ZL311.setName("ZL311");
              ZL311.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL312 ----
              ZL312.setComponentPopupMenu(BTD);
              ZL312.setName("ZL312");
              ZL312.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL313 ----
              ZL313.setComponentPopupMenu(BTD);
              ZL313.setName("ZL313");
              ZL313.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL314 ----
              ZL314.setComponentPopupMenu(BTD);
              ZL314.setName("ZL314");
              ZL314.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL315 ----
              ZL315.setComponentPopupMenu(BTD);
              ZL315.setName("ZL315");
              ZL315.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL41 ----
              ZL41.setComponentPopupMenu(BTD);
              ZL41.setName("ZL41");
              ZL41.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL42 ----
              ZL42.setComponentPopupMenu(BTD);
              ZL42.setName("ZL42");
              ZL42.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL43 ----
              ZL43.setComponentPopupMenu(BTD);
              ZL43.setName("ZL43");
              ZL43.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL44 ----
              ZL44.setComponentPopupMenu(BTD);
              ZL44.setName("ZL44");
              ZL44.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL45 ----
              ZL45.setComponentPopupMenu(BTD);
              ZL45.setName("ZL45");
              ZL45.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL46 ----
              ZL46.setComponentPopupMenu(BTD);
              ZL46.setName("ZL46");
              ZL46.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL47 ----
              ZL47.setComponentPopupMenu(BTD);
              ZL47.setName("ZL47");
              ZL47.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL48 ----
              ZL48.setComponentPopupMenu(BTD);
              ZL48.setName("ZL48");
              ZL48.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL49 ----
              ZL49.setComponentPopupMenu(BTD);
              ZL49.setName("ZL49");
              ZL49.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL410 ----
              ZL410.setComponentPopupMenu(BTD);
              ZL410.setName("ZL410");
              ZL410.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL411 ----
              ZL411.setComponentPopupMenu(BTD);
              ZL411.setName("ZL411");
              ZL411.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL412 ----
              ZL412.setComponentPopupMenu(BTD);
              ZL412.setName("ZL412");
              ZL412.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL413 ----
              ZL413.setComponentPopupMenu(BTD);
              ZL413.setName("ZL413");
              ZL413.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL414 ----
              ZL414.setComponentPopupMenu(BTD);
              ZL414.setName("ZL414");
              ZL414.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL415 ----
              ZL415.setComponentPopupMenu(BTD);
              ZL415.setName("ZL415");
              ZL415.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL51 ----
              ZL51.setComponentPopupMenu(BTD);
              ZL51.setName("ZL51");
              ZL51.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL52 ----
              ZL52.setComponentPopupMenu(BTD);
              ZL52.setName("ZL52");
              ZL52.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL53 ----
              ZL53.setComponentPopupMenu(BTD);
              ZL53.setName("ZL53");
              ZL53.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL54 ----
              ZL54.setComponentPopupMenu(BTD);
              ZL54.setName("ZL54");
              ZL54.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL55 ----
              ZL55.setComponentPopupMenu(BTD);
              ZL55.setName("ZL55");
              ZL55.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL56 ----
              ZL56.setComponentPopupMenu(BTD);
              ZL56.setName("ZL56");
              ZL56.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL57 ----
              ZL57.setComponentPopupMenu(BTD);
              ZL57.setName("ZL57");
              ZL57.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL58 ----
              ZL58.setComponentPopupMenu(BTD);
              ZL58.setName("ZL58");
              ZL58.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL59 ----
              ZL59.setComponentPopupMenu(BTD);
              ZL59.setName("ZL59");
              ZL59.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL510 ----
              ZL510.setComponentPopupMenu(BTD);
              ZL510.setName("ZL510");
              ZL510.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL511 ----
              ZL511.setComponentPopupMenu(BTD);
              ZL511.setName("ZL511");
              ZL511.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL512 ----
              ZL512.setComponentPopupMenu(BTD);
              ZL512.setName("ZL512");
              ZL512.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL513 ----
              ZL513.setComponentPopupMenu(BTD);
              ZL513.setName("ZL513");
              ZL513.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL514 ----
              ZL514.setComponentPopupMenu(BTD);
              ZL514.setName("ZL514");
              ZL514.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL515 ----
              ZL515.setComponentPopupMenu(BTD);
              ZL515.setName("ZL515");
              ZL515.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL61 ----
              ZL61.setComponentPopupMenu(BTD);
              ZL61.setName("ZL61");
              ZL61.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL62 ----
              ZL62.setComponentPopupMenu(BTD);
              ZL62.setName("ZL62");
              ZL62.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL63 ----
              ZL63.setComponentPopupMenu(BTD);
              ZL63.setName("ZL63");
              ZL63.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL64 ----
              ZL64.setComponentPopupMenu(BTD);
              ZL64.setName("ZL64");
              ZL64.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL65 ----
              ZL65.setComponentPopupMenu(BTD);
              ZL65.setName("ZL65");
              ZL65.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL66 ----
              ZL66.setComponentPopupMenu(BTD);
              ZL66.setName("ZL66");
              ZL66.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL67 ----
              ZL67.setComponentPopupMenu(BTD);
              ZL67.setName("ZL67");
              ZL67.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL68 ----
              ZL68.setComponentPopupMenu(BTD);
              ZL68.setName("ZL68");
              ZL68.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL69 ----
              ZL69.setComponentPopupMenu(BTD);
              ZL69.setName("ZL69");
              ZL69.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL610 ----
              ZL610.setComponentPopupMenu(BTD);
              ZL610.setName("ZL610");
              ZL610.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL611 ----
              ZL611.setComponentPopupMenu(BTD);
              ZL611.setName("ZL611");
              ZL611.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL612 ----
              ZL612.setComponentPopupMenu(BTD);
              ZL612.setName("ZL612");
              ZL612.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL613 ----
              ZL613.setComponentPopupMenu(BTD);
              ZL613.setName("ZL613");
              ZL613.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL614 ----
              ZL614.setComponentPopupMenu(BTD);
              ZL614.setName("ZL614");
              ZL614.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- ZL615 ----
              ZL615.setComponentPopupMenu(BTD);
              ZL615.setName("ZL615");
              ZL615.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });

              //---- label2 ----
              label2.setText("@UTIT@");
              label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
              label2.setName("label2");

              //---- label3 ----
              label3.setText("T");
              label3.setHorizontalAlignment(SwingConstants.CENTER);
              label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
              label3.setName("label3");

              //---- label4 ----
              label4.setText("Valeur");
              label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
              label4.setName("label4");

              //---- xTitledSeparator1 ----
              xTitledSeparator1.setTitle("Remise");
              xTitledSeparator1.setName("xTitledSeparator1");

              //---- label5 ----
              label5.setText("Coefficient");
              label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
              label5.setName("label5");

              //---- PL01 ----
              PL01.setText("@PL01@");
              PL01.setForeground(new Color(153, 0, 0));
              PL01.setHorizontalAlignment(SwingConstants.CENTER);
              PL01.setName("PL01");

              //---- PL02 ----
              PL02.setText("@PL02@");
              PL02.setForeground(new Color(153, 0, 0));
              PL02.setHorizontalAlignment(SwingConstants.CENTER);
              PL02.setName("PL02");

              //---- PL03 ----
              PL03.setText("@PL03@");
              PL03.setForeground(new Color(153, 0, 0));
              PL03.setHorizontalAlignment(SwingConstants.CENTER);
              PL03.setName("PL03");

              //---- PL04 ----
              PL04.setText("@PL04@");
              PL04.setForeground(new Color(153, 0, 0));
              PL04.setHorizontalAlignment(SwingConstants.CENTER);
              PL04.setName("PL04");

              //---- PL05 ----
              PL05.setText("@PL05@");
              PL05.setForeground(new Color(153, 0, 0));
              PL05.setHorizontalAlignment(SwingConstants.CENTER);
              PL05.setName("PL05");

              //---- PL06 ----
              PL06.setText("@PL06@");
              PL06.setForeground(new Color(153, 0, 0));
              PL06.setHorizontalAlignment(SwingConstants.CENTER);
              PL06.setName("PL06");

              //---- PL07 ----
              PL07.setText("@PL07@");
              PL07.setForeground(new Color(153, 0, 0));
              PL07.setHorizontalAlignment(SwingConstants.CENTER);
              PL07.setName("PL07");

              //---- PL08 ----
              PL08.setText("@PL08@");
              PL08.setForeground(new Color(153, 0, 0));
              PL08.setHorizontalAlignment(SwingConstants.CENTER);
              PL08.setName("PL08");

              //---- PL09 ----
              PL09.setText("@PL09@");
              PL09.setForeground(new Color(153, 0, 0));
              PL09.setHorizontalAlignment(SwingConstants.CENTER);
              PL09.setName("PL09");

              //---- PL10 ----
              PL10.setText("@PL10@");
              PL10.setForeground(new Color(153, 0, 0));
              PL10.setHorizontalAlignment(SwingConstants.CENTER);
              PL10.setName("PL10");

              //---- PL11 ----
              PL11.setText("@PL11@");
              PL11.setForeground(new Color(153, 0, 0));
              PL11.setHorizontalAlignment(SwingConstants.CENTER);
              PL11.setName("PL11");

              //---- PL12 ----
              PL12.setText("@PL12@");
              PL12.setForeground(new Color(153, 0, 0));
              PL12.setHorizontalAlignment(SwingConstants.CENTER);
              PL12.setName("PL12");

              //---- PL13 ----
              PL13.setText("@PL13@");
              PL13.setForeground(new Color(153, 0, 0));
              PL13.setHorizontalAlignment(SwingConstants.CENTER);
              PL13.setName("PL13");

              //---- PL14 ----
              PL14.setText("@PL14@");
              PL14.setForeground(new Color(153, 0, 0));
              PL14.setHorizontalAlignment(SwingConstants.CENTER);
              PL14.setName("PL14");

              //---- PL15 ----
              PL15.setText("@PL15@");
              PL15.setForeground(new Color(153, 0, 0));
              PL15.setHorizontalAlignment(SwingConstants.CENTER);
              PL15.setName("PL15");

              GroupLayout panel1Layout = new GroupLayout(panel1);
              panel1.setLayout(panel1Layout);
              panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addComponent(label2, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(label3, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(label4, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(label5, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(LD01, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD02, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD03, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD04, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD05, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD06, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD07, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD08, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD09, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD10, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD11, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD12, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD13, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD14, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)
                          .addComponent(LD15, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(ZL15, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL16, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL13, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL11, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL14, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL12, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL19, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL114, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL112, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL111, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL115, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL18, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL113, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL110, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL17, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)
                        .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(ZL23, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL22, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL21, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL24, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL212, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL214, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL211, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL213, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL29, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL26, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL210, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL215, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL27, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL28, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL25, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                        .addGap(5, 5, 5)
                        .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(ZL33, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL32, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL36, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL37, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL34, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL35, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL31, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL310, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL311, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL313, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL312, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL314, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL315, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL39, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL38, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
                        .addGap(2, 2, 2)
                        .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(ZL46, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL43, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL48, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL47, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL412, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL44, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL410, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL49, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL411, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL45, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL413, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL42, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL41, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL415, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL414, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
                        .addGap(2, 2, 2)
                        .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(ZL510, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL58, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL53, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL55, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL52, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL57, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL511, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL54, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL59, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL56, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL513, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL515, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL514, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL51, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL512, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)
                        .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(ZL67, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL65, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL69, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL64, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL66, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL63, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL68, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL61, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL62, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL615, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL612, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL611, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL613, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL614, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addComponent(ZL610, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(57, 57, 57)
                            .addGroup(panel1Layout.createParallelGroup()
                              .addComponent(PL07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL08, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL14, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL09, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(PL13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                        .addGap(10, 10, 10)
                        .addGroup(panel1Layout.createParallelGroup()
                          .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                          .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))))
              );
              panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(label2)
                      .addComponent(label3)
                      .addComponent(label4)
                      .addComponent(xTitledSeparator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(label5))
                    .addGap(3, 3, 3)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(panel1Layout.createParallelGroup()
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addComponent(LD01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(1, 1, 1)
                            .addComponent(LD15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGroup(panel1Layout.createParallelGroup()
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(ZL15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(125, 125, 125)
                                .addComponent(ZL16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(ZL13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addComponent(ZL11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addComponent(ZL14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(ZL12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                            .addGap(22, 22, 22)
                            .addGroup(panel1Layout.createParallelGroup()
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(ZL19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(150, 150, 150)
                                .addComponent(ZL114, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(ZL112, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addComponent(ZL111, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(175, 175, 175)
                                .addComponent(ZL115, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addComponent(ZL18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(125, 125, 125)
                                .addComponent(ZL113, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(ZL110, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(150, 150, 150)
                            .addComponent(ZL17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGroup(panel1Layout.createParallelGroup()
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(ZL23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(ZL22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addComponent(ZL21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addComponent(ZL24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                            .addGap(22, 22, 22)
                            .addGroup(panel1Layout.createParallelGroup()
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(150, 150, 150)
                                .addComponent(ZL212, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(200, 200, 200)
                                .addComponent(ZL214, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(125, 125, 125)
                                .addComponent(ZL211, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(175, 175, 175)
                                .addComponent(ZL213, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addComponent(ZL29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addComponent(ZL26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(ZL210, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(225, 225, 225)
                                .addComponent(ZL215, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(ZL27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(ZL28, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(100, 100, 100)
                            .addComponent(ZL25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGroup(panel1Layout.createParallelGroup()
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(ZL33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(ZL32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(125, 125, 125)
                                .addComponent(ZL36, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(150, 150, 150)
                                .addComponent(ZL37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addComponent(ZL34, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(ZL35, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addComponent(ZL31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGap(22, 22, 22)
                            .addGroup(panel1Layout.createParallelGroup()
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(ZL310, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(ZL311, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(ZL313, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addComponent(ZL312, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(125, 125, 125)
                                .addComponent(ZL314, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(150, 150, 150)
                                .addComponent(ZL315, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addComponent(ZL39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(175, 175, 175)
                            .addComponent(ZL38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGroup(panel1Layout.createParallelGroup()
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(125, 125, 125)
                                .addComponent(ZL46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(ZL43, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(175, 175, 175)
                                .addComponent(ZL48, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(150, 150, 150)
                                .addComponent(ZL47, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(275, 275, 275)
                                .addComponent(ZL412, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addComponent(ZL44, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(225, 225, 225)
                                .addComponent(ZL410, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(200, 200, 200)
                                .addComponent(ZL49, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(250, 250, 250)
                                .addComponent(ZL411, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(ZL45, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(300, 300, 300)
                                .addComponent(ZL413, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(ZL42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addComponent(ZL41, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGap(22, 22, 22)
                            .addComponent(ZL415, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(325, 325, 325)
                            .addComponent(ZL414, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addGroup(panel1Layout.createParallelGroup()
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(200, 200, 200)
                                .addComponent(ZL510, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(150, 150, 150)
                                .addComponent(ZL58, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(ZL53, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addComponent(ZL55, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addComponent(ZL52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(125, 125, 125)
                                .addComponent(ZL57, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(225, 225, 225)
                                .addComponent(ZL511, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(ZL54, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(175, 175, 175)
                                .addComponent(ZL59, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(ZL56, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                            .addGap(22, 22, 22)
                            .addGroup(panel1Layout.createParallelGroup()
                              .addComponent(ZL513, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(ZL515, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(ZL514, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                          .addComponent(ZL51, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(275, 275, 275)
                            .addComponent(ZL512, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGroup(panel1Layout.createParallelGroup()
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(150, 150, 150)
                                .addComponent(ZL67, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(ZL65, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(150, 150, 150)
                                .addComponent(PL07, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(200, 200, 200)
                                .addComponent(ZL69, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addComponent(PL04, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addComponent(ZL64, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(125, 125, 125)
                                .addComponent(ZL66, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(175, 175, 175)
                                .addComponent(PL08, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(125, 125, 125)
                                .addComponent(PL06, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(ZL63, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(175, 175, 175)
                                .addComponent(ZL68, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addComponent(PL01, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                              .addComponent(ZL61, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(PL05, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(ZL62, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                            .addGap(22, 22, 22)
                            .addGroup(panel1Layout.createParallelGroup()
                              .addComponent(PL11, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(PL12, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(ZL615, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addComponent(ZL612, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addComponent(PL14, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addComponent(PL15, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                              .addComponent(ZL611, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(ZL613, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addComponent(ZL614, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(25, 25, 25)
                            .addComponent(PL02, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(225, 225, 225)
                            .addComponent(PL10, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(225, 225, 225)
                            .addComponent(ZL610, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(200, 200, 200)
                            .addComponent(PL09, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(50, 50, 50)
                            .addComponent(PL03, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
                          .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(300, 300, 300)
                            .addComponent(PL13, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))))))
              );
            }
            p_TCI1ContentContainer.add(panel1);
            panel1.setBounds(25, 135, 800, 420);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder(""));
              panel2.setOpaque(false);
              panel2.setName("panel2");

              //---- WTCD ----
              WTCD.setModel(new DefaultComboBoxModel(new String[] {
                "Condition en prix net",
                "Condition en prix de base",
                "Condition en coefficient",
                "Condition en remise",
                "Utilisation condition sp\u00e9ciale",
                "Diff\u00e9rentiel en compl\u00e9ment",
                "Diff\u00e9rentiel en diminution",
                "Pas de condition"
              }));
              WTCD.setComponentPopupMenu(BTD);
              WTCD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WTCD.setName("WTCD");

              //---- OBJ_61 ----
              OBJ_61.setText("Type de condition for\u00e7\u00e9");
              OBJ_61.setName("OBJ_61");

              //---- OBJ_81 ----
              OBJ_81.setText("Dates d'application");
              OBJ_81.setName("OBJ_81");

              //---- DTDEBX ----
              DTDEBX.setComponentPopupMenu(BTD);
              DTDEBX.setName("DTDEBX");

              //---- DTFINX ----
              DTFINX.setComponentPopupMenu(BTD);
              DTFINX.setName("DTFINX");

              //---- OBJ_78 ----
              OBJ_78.setText("au");
              OBJ_78.setName("OBJ_78");

              //---- OBJ_82 ----
              OBJ_82.setText("du");
              OBJ_82.setName("OBJ_82");

              //---- label1 ----
              label1.setText("Type de rattachement");
              label1.setName("label1");

              //---- WRAT ----
              WRAT.setName("WRAT");

              //---- WLRAT ----
              WLRAT.setText("@WLRAT@");
              WLRAT.setName("WLRAT");

              GroupLayout panel2Layout = new GroupLayout(panel2);
              panel2.setLayout(panel2Layout);
              panel2Layout.setHorizontalGroup(
                panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(WTCD, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(WRAT, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(WLRAT, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17)
                        .addComponent(DTDEBX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17)
                        .addComponent(DTFINX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))))
              );
              panel2Layout.setVerticalGroup(
                panel2Layout.createParallelGroup()
                  .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WTCD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(3, 3, 3)
                    .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(label1))
                      .addComponent(WRAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(WLRAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGap(3, 3, 3)
                    .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(DTDEBX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(DTFINX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(panel2Layout.createParallelGroup()
                          .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))))
              );
            }
            p_TCI1ContentContainer.add(panel2);
            panel2.setBounds(25, 10, 800, 110);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(p_TCI1, GroupLayout.PREFERRED_SIZE, 858, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(p_TCI1, GroupLayout.PREFERRED_SIZE, 590, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("D\u00e9tail");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_18 ----
      OBJ_18.setText("Niveau sup\u00e9rieur");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Niveau inf\u00e9rieur");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
      BTD.addSeparator();

      //---- OBJ_25 ----
      OBJ_25.setText("Aide en ligne");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField WETB;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private SNBoutonDetail BT_ChgSoc;
  private XRiTextField WCNV;
  private RiZoneSortie CLNOM;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel p_TCI1;
  private JPanel panel1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private RiZoneSortie LD01;
  private RiZoneSortie LD02;
  private RiZoneSortie LD03;
  private RiZoneSortie LD04;
  private RiZoneSortie LD05;
  private RiZoneSortie LD06;
  private RiZoneSortie LD07;
  private RiZoneSortie LD08;
  private RiZoneSortie LD09;
  private RiZoneSortie LD10;
  private RiZoneSortie LD11;
  private RiZoneSortie LD12;
  private RiZoneSortie LD13;
  private RiZoneSortie LD14;
  private RiZoneSortie LD15;
  private XRiTextField ZL11;
  private XRiTextField ZL12;
  private XRiTextField ZL13;
  private XRiTextField ZL14;
  private XRiTextField ZL15;
  private XRiTextField ZL16;
  private XRiTextField ZL17;
  private XRiTextField ZL18;
  private XRiTextField ZL19;
  private XRiTextField ZL110;
  private XRiTextField ZL111;
  private XRiTextField ZL112;
  private XRiTextField ZL113;
  private XRiTextField ZL114;
  private XRiTextField ZL115;
  private XRiTextField ZL21;
  private XRiTextField ZL22;
  private XRiTextField ZL23;
  private XRiTextField ZL24;
  private XRiTextField ZL25;
  private XRiTextField ZL26;
  private XRiTextField ZL27;
  private XRiTextField ZL28;
  private XRiTextField ZL29;
  private XRiTextField ZL210;
  private XRiTextField ZL211;
  private XRiTextField ZL212;
  private XRiTextField ZL213;
  private XRiTextField ZL214;
  private XRiTextField ZL215;
  private XRiTextField ZL31;
  private XRiTextField ZL32;
  private XRiTextField ZL33;
  private XRiTextField ZL34;
  private XRiTextField ZL35;
  private XRiTextField ZL36;
  private XRiTextField ZL37;
  private XRiTextField ZL38;
  private XRiTextField ZL39;
  private XRiTextField ZL310;
  private XRiTextField ZL311;
  private XRiTextField ZL312;
  private XRiTextField ZL313;
  private XRiTextField ZL314;
  private XRiTextField ZL315;
  private XRiTextField ZL41;
  private XRiTextField ZL42;
  private XRiTextField ZL43;
  private XRiTextField ZL44;
  private XRiTextField ZL45;
  private XRiTextField ZL46;
  private XRiTextField ZL47;
  private XRiTextField ZL48;
  private XRiTextField ZL49;
  private XRiTextField ZL410;
  private XRiTextField ZL411;
  private XRiTextField ZL412;
  private XRiTextField ZL413;
  private XRiTextField ZL414;
  private XRiTextField ZL415;
  private XRiTextField ZL51;
  private XRiTextField ZL52;
  private XRiTextField ZL53;
  private XRiTextField ZL54;
  private XRiTextField ZL55;
  private XRiTextField ZL56;
  private XRiTextField ZL57;
  private XRiTextField ZL58;
  private XRiTextField ZL59;
  private XRiTextField ZL510;
  private XRiTextField ZL511;
  private XRiTextField ZL512;
  private XRiTextField ZL513;
  private XRiTextField ZL514;
  private XRiTextField ZL515;
  private XRiTextField ZL61;
  private XRiTextField ZL62;
  private XRiTextField ZL63;
  private XRiTextField ZL64;
  private XRiTextField ZL65;
  private XRiTextField ZL66;
  private XRiTextField ZL67;
  private XRiTextField ZL68;
  private XRiTextField ZL69;
  private XRiTextField ZL610;
  private XRiTextField ZL611;
  private XRiTextField ZL612;
  private XRiTextField ZL613;
  private XRiTextField ZL614;
  private XRiTextField ZL615;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JXTitledSeparator xTitledSeparator1;
  private JLabel label5;
  private JLabel PL01;
  private JLabel PL02;
  private JLabel PL03;
  private JLabel PL04;
  private JLabel PL05;
  private JLabel PL06;
  private JLabel PL07;
  private JLabel PL08;
  private JLabel PL09;
  private JLabel PL10;
  private JLabel PL11;
  private JLabel PL12;
  private JLabel PL13;
  private JLabel PL14;
  private JLabel PL15;
  private JPanel panel2;
  private XRiComboBox WTCD;
  private JLabel OBJ_61;
  private JLabel OBJ_81;
  private XRiCalendrier DTDEBX;
  private XRiCalendrier DTFINX;
  private JLabel OBJ_78;
  private JLabel OBJ_82;
  private JLabel label1;
  private XRiTextField WRAT;
  private RiZoneSortie WLRAT;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_25;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
