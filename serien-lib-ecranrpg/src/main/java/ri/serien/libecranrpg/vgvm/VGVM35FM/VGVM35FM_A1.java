//$$david$$ ££06/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vgvm.VGVM35FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.metier.referentiel.commun.snpays.SNPays;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM35FM_A1 extends SNPanelEcranRPG implements ioFrame {
  private static final String BOUTON_CREER = "Créer";
  
  public VGVM35FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(BOUTON_CREER, 'c', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.RECHERCHER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Ajout
    initDiverses();
    TIDX10.setValeurs("10", "RB");
    TIDX7.setValeurs("7", "RB");
    TIDX5.setValeurs("5", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX9.setValeurs("9", "RB");
    TIDX8.setValeurs("8", "RB");
    TIDX2.setValeurs("2", "RB");
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("INDETB"));
    
    // Etat des actions
    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    boolean isCreation = lexique.getMode() == Lexical.MODE_CREATION;
    
    // Visibilité
    snBarreBouton.activerBouton(BOUTON_CREER, isConsultation);
    snBarreBouton.activerBouton(EnumBouton.RECHERCHER, isConsultation);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
    
    pnlRechercheMultiCritere.setVisible(!lexique.isTrue("51"));
    pnlRepresentantRecherche.setVisible(!lexique.isTrue("51"));
    pnlRepresentantSaisie.setVisible(lexique.isTrue("51"));
    pnlEtablissementRecherche.setVisible(lexique.isTrue("53") || (lexique.isTrue("51") && !lexique.isTrue("56")));
    pnlEtablissementDuplication.setVisible(lexique.isTrue("51") && lexique.isTrue("56"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    
    // Initialisation de l'Etablissment
    snEtablissementDuplication.setSession(getSession());
    snEtablissementDuplication.charger(false);
    snEtablissementDuplication.setSelectionParChampRPG(lexique, "IN3ETB");
    
    chargerComposantCodePostal();
    chargerComposantPays();
    chargerComposantMagasin();
    
    if (lexique.isTrue("51") && !lexique.isTrue("56")) {
      pnlRechercheCreation.setTitre("Création d'un nouveau représentant");
      chargerComposantRepresentantRecherche();
    }
    if (lexique.isTrue("51") && lexique.isTrue("56")) {
      pnlRechercheCreation.setTitre("Création d'un nouveau représentant par duplication");
      chargerComposantRepresentantDuplication();
    }
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    if (!lexique.isTrue("51")) {
      snRepresentantRecherche.renseignerChampRPG(lexique, "INDREP");
    }
    snPays.renseignerChampRPG(lexique, "ARG91");
    snMagasin.renseignerChampRPG(lexique, "ARG7");
    snCodePostalCommune.renseignerChampRPG(lexique, "ARG92", "RESVIL", false);
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_CREER)) {
        lexique.HostScreenSendKey(this, "F13");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // charger composant par fonction
  
  private void chargerComposantRepresentantRecherche() {
    snRepresentantRecherche.setSession(getSession());
    snRepresentantRecherche.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentantRecherche.charger(false);
    snRepresentantRecherche.setSelectionParChampRPG(lexique, "INDREP");
  }
  
  private void chargerComposantCodePostal() {
    snCodePostalCommune.setSession(getSession());
    snCodePostalCommune.setIdEtablissement(snEtablissement.getIdSelection());
    snCodePostalCommune.charger(false);
    snCodePostalCommune.setSelectionParChampRPG(lexique, "ARG92", "RESVIL");
  }
  
  private void chargerComposantPays() {
    snPays.setSession(getSession());
    snPays.setIdEtablissement(snEtablissement.getIdSelection());
    snPays.setAucunAutorise(true);
    snPays.charger(false);
    snPays.setSelectionParChampRPG(lexique, "ARG91");
  }
  
  private void chargerComposantMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setAucunAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "ARG7");
  }
  
  private void chargerComposantRepresentantDuplication() {
    snRepresentantRecherche.setSession(getSession());
    snRepresentantRecherche.setIdEtablissement(snEtablissement.getIdSelection());
    snRepresentantRecherche.charger(false);
    snRepresentantRecherche.setSelectionParChampRPG(lexique, "IN3REP");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    chargerComposantRepresentantRecherche();
    chargerComposantRepresentantDuplication();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlRecherches = new SNPanel();
    pnlRechercheCreation = new SNPanelTitre();
    pnlRepresentant = new SNPanel();
    pnlRepresentantRecherche = new SNPanel();
    lbRepresentantRecherche = new SNLabelChamp();
    snRepresentantRecherche = new SNRepresentant();
    pnlRepresentantSaisie = new SNPanel();
    lbRepresentantSaisie = new SNLabelChamp();
    INDREP = new XRiTextField();
    pnlEtablissement = new SNPanel();
    pnlEtablissementRecherche = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlEtablissementDuplication = new SNPanel();
    lbEtablissementDuplication = new SNLabelChamp();
    snEtablissementDuplication = new SNEtablissement();
    pnlRechercheMultiCritere = new SNPanelTitre();
    TIDX2 = new XRiRadioButton();
    ARG2 = new XRiTextField();
    TIDX8 = new XRiRadioButton();
    ARG8 = new XRiTextField();
    sNLabelChamp1 = new SNLabelChamp();
    pnlPaysCodePostal = new SNPanel();
    snPays = new SNPays();
    TIDX3 = new XRiRadioButton();
    ARG3 = new XRiTextField();
    TIDX4 = new XRiRadioButton();
    ARG4 = new XRiTextField();
    TIDX5 = new XRiRadioButton();
    ARG5 = new XRiTextField();
    TIDX9 = new XRiRadioButton();
    snCodePostalCommune = new SNCodePostalCommune();
    TIDX7 = new XRiRadioButton();
    snMagasin = new SNMagasin();
    TIDX10 = new XRiRadioButton();
    ARG10 = new XRiTextField();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Recherche repr\u00e9sentants");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());
      
      // ======== pnlRecherches ========
      {
        pnlRecherches.setName("pnlRecherches");
        pnlRecherches.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlRecherches.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlRecherches.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlRecherches.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlRecherches.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlRechercheCreation ========
        {
          pnlRechercheCreation.setTitre("Recherche directe");
          pnlRechercheCreation.setName("pnlRechercheCreation");
          pnlRechercheCreation.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRechercheCreation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlRechercheCreation.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRechercheCreation.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlRechercheCreation.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== pnlRepresentant ========
          {
            pnlRepresentant.setName("pnlRepresentant");
            pnlRepresentant.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRepresentant.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlRepresentant.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRepresentant.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRepresentant.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlRepresentantRecherche ========
            {
              pnlRepresentantRecherche.setMaximumSize(new Dimension(475, 30));
              pnlRepresentantRecherche.setName("pnlRepresentantRecherche");
              pnlRepresentantRecherche.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRepresentantRecherche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlRepresentantRecherche.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRepresentantRecherche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlRepresentantRecherche.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
              
              // ---- lbRepresentantRecherche ----
              lbRepresentantRecherche.setText("Repr\u00e9sentant");
              lbRepresentantRecherche.setName("lbRepresentantRecherche");
              pnlRepresentantRecherche.add(lbRepresentantRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snRepresentantRecherche ----
              snRepresentantRecherche.setName("snRepresentantRecherche");
              pnlRepresentantRecherche.add(snRepresentantRecherche, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlRepresentant.add(pnlRepresentantRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlRepresentantSaisie ========
            {
              pnlRepresentantSaisie.setMaximumSize(new Dimension(191, 30));
              pnlRepresentantSaisie.setName("pnlRepresentantSaisie");
              pnlRepresentantSaisie.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRepresentantSaisie.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlRepresentantSaisie.getLayout()).rowHeights = new int[] { 30, 0 };
              ((GridBagLayout) pnlRepresentantSaisie.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRepresentantSaisie.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
              
              // ---- lbRepresentantSaisie ----
              lbRepresentantSaisie.setText("Code repr\u00e9sentant");
              lbRepresentantSaisie.setName("lbRepresentantSaisie");
              pnlRepresentantSaisie.add(lbRepresentantSaisie, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- INDREP ----
              INDREP.setFont(new Font("sansserif", Font.PLAIN, 14));
              INDREP.setMinimumSize(new Dimension(36, 30));
              INDREP.setPreferredSize(new Dimension(36, 30));
              INDREP.setMaximumSize(new Dimension(36, 30));
              INDREP.setName("INDREP");
              pnlRepresentantSaisie.add(INDREP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlRepresentant.add(pnlRepresentantSaisie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRechercheCreation.add(pnlRepresentant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlEtablissementRecherche ========
            {
              pnlEtablissementRecherche.setName("pnlEtablissementRecherche");
              pnlEtablissementRecherche.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEtablissementRecherche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEtablissementRecherche.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlEtablissementRecherche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlEtablissementRecherche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissement ----
              lbEtablissement.setText("Etablissement");
              lbEtablissement.setName("lbEtablissement");
              pnlEtablissementRecherche.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissement ----
              snEtablissement.setName("snEtablissement");
              snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  snEtablissementValueChanged(e);
                }
              });
              pnlEtablissementRecherche.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlEtablissement.add(pnlEtablissementRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlEtablissementDuplication ========
            {
              pnlEtablissementDuplication.setName("pnlEtablissementDuplication");
              pnlEtablissementDuplication.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEtablissementDuplication.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEtablissementDuplication.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlEtablissementDuplication.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlEtablissementDuplication.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbEtablissementDuplication ----
              lbEtablissementDuplication.setText("Etablissement");
              lbEtablissementDuplication.setName("lbEtablissementDuplication");
              pnlEtablissementDuplication.add(lbEtablissementDuplication, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snEtablissementDuplication ----
              snEtablissementDuplication.setName("snEtablissementDuplication");
              pnlEtablissementDuplication.add(snEtablissementDuplication, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlEtablissement.add(pnlEtablissementDuplication, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRechercheCreation.add(pnlEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlRecherches.add(pnlRechercheCreation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlRechercheMultiCritere ========
        {
          pnlRechercheMultiCritere.setTitre("Recherche multi-crit\u00e8res");
          pnlRechercheMultiCritere.setName("pnlRechercheMultiCritere");
          pnlRechercheMultiCritere.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRechercheMultiCritere.getLayout()).columnWidths = new int[] { 0, 263, 0 };
          ((GridBagLayout) pnlRechercheMultiCritere.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlRechercheMultiCritere.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRechercheMultiCritere.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- TIDX2 ----
          TIDX2.setText("Recherche alphab\u00e9tique");
          TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2.setToolTipText("Tri sur cet argument");
          TIDX2.setFont(new Font("sansserif", Font.PLAIN, 14));
          TIDX2.setMaximumSize(new Dimension(250, 30));
          TIDX2.setMinimumSize(new Dimension(250, 30));
          TIDX2.setPreferredSize(new Dimension(250, 30));
          TIDX2.setName("TIDX2");
          pnlRechercheMultiCritere.add(TIDX2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- ARG2 ----
          ARG2.setMinimumSize(new Dimension(215, 30));
          ARG2.setMaximumSize(new Dimension(215, 30));
          ARG2.setPreferredSize(new Dimension(215, 30));
          ARG2.setFont(new Font("sansserif", Font.PLAIN, 14));
          ARG2.setName("ARG2");
          pnlRechercheMultiCritere.add(ARG2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- TIDX8 ----
          TIDX8.setText("Mot de classement 1");
          TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX8.setToolTipText("Tri sur cet argument");
          TIDX8.setFont(new Font("sansserif", Font.PLAIN, 14));
          TIDX8.setMaximumSize(new Dimension(250, 30));
          TIDX8.setMinimumSize(new Dimension(250, 30));
          TIDX8.setPreferredSize(new Dimension(250, 30));
          TIDX8.setName("TIDX8");
          pnlRechercheMultiCritere.add(TIDX8, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- ARG8 ----
          ARG8.setMaximumSize(new Dimension(215, 30));
          ARG8.setMinimumSize(new Dimension(215, 30));
          ARG8.setPreferredSize(new Dimension(215, 30));
          ARG8.setName("ARG8");
          pnlRechercheMultiCritere.add(ARG8, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- sNLabelChamp1 ----
          sNLabelChamp1.setText("Code pays");
          sNLabelChamp1.setHorizontalAlignment(SwingConstants.LEADING);
          sNLabelChamp1.setName("sNLabelChamp1");
          pnlRechercheMultiCritere.add(sNLabelChamp1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlPaysCodePostal ========
          {
            pnlPaysCodePostal.setName("pnlPaysCodePostal");
            pnlPaysCodePostal.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPaysCodePostal.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlPaysCodePostal.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPaysCodePostal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlPaysCodePostal.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- snPays ----
            snPays.setName("snPays");
            pnlPaysCodePostal.add(snPays, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRechercheMultiCritere.add(pnlPaysCodePostal, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- TIDX3 ----
          TIDX3.setText("Type de repr\u00e9sentant");
          TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX3.setToolTipText("Tri sur cet argument");
          TIDX3.setFont(new Font("sansserif", Font.PLAIN, 14));
          TIDX3.setMaximumSize(new Dimension(250, 30));
          TIDX3.setMinimumSize(new Dimension(250, 30));
          TIDX3.setPreferredSize(new Dimension(250, 30));
          TIDX3.setName("TIDX3");
          pnlRechercheMultiCritere.add(TIDX3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- ARG3 ----
          ARG3.setPreferredSize(new Dimension(72, 30));
          ARG3.setMinimumSize(new Dimension(72, 30));
          ARG3.setMaximumSize(new Dimension(72, 30));
          ARG3.setName("ARG3");
          pnlRechercheMultiCritere.add(ARG3, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- TIDX4 ----
          TIDX4.setText("Num\u00e9ro de t\u00e9l\u00e9phone");
          TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX4.setToolTipText("Tri sur cet argument");
          TIDX4.setFont(new Font("sansserif", Font.PLAIN, 14));
          TIDX4.setMaximumSize(new Dimension(250, 30));
          TIDX4.setMinimumSize(new Dimension(250, 30));
          TIDX4.setPreferredSize(new Dimension(250, 30));
          TIDX4.setName("TIDX4");
          pnlRechercheMultiCritere.add(TIDX4, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- ARG4 ----
          ARG4.setMaximumSize(new Dimension(155, 30));
          ARG4.setMinimumSize(new Dimension(155, 30));
          ARG4.setPreferredSize(new Dimension(155, 30));
          ARG4.setName("ARG4");
          pnlRechercheMultiCritere.add(ARG4, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- TIDX5 ----
          TIDX5.setText("Zone g\u00e9ographique");
          TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX5.setToolTipText("Tri sur cet argument");
          TIDX5.setFont(new Font("sansserif", Font.PLAIN, 14));
          TIDX5.setMaximumSize(new Dimension(250, 30));
          TIDX5.setMinimumSize(new Dimension(250, 30));
          TIDX5.setPreferredSize(new Dimension(250, 30));
          TIDX5.setName("TIDX5");
          pnlRechercheMultiCritere.add(TIDX5, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- ARG5 ----
          ARG5.setMaximumSize(new Dimension(72, 30));
          ARG5.setMinimumSize(new Dimension(72, 30));
          ARG5.setPreferredSize(new Dimension(72, 30));
          ARG5.setName("ARG5");
          pnlRechercheMultiCritere.add(ARG5, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- TIDX9 ----
          TIDX9.setText("Code postal ");
          TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX9.setToolTipText("Tri sur cet argument");
          TIDX9.setFont(new Font("sansserif", Font.PLAIN, 14));
          TIDX9.setMaximumSize(new Dimension(250, 30));
          TIDX9.setMinimumSize(new Dimension(250, 30));
          TIDX9.setPreferredSize(new Dimension(250, 30));
          TIDX9.setName("TIDX9");
          pnlRechercheMultiCritere.add(TIDX9, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snCodePostalCommune ----
          snCodePostalCommune.setName("snCodePostalCommune");
          pnlRechercheMultiCritere.add(snCodePostalCommune, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- TIDX7 ----
          TIDX7.setText("Magasin");
          TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX7.setToolTipText("Tri sur cet argument");
          TIDX7.setFont(new Font("sansserif", Font.PLAIN, 14));
          TIDX7.setMaximumSize(new Dimension(250, 30));
          TIDX7.setMinimumSize(new Dimension(250, 30));
          TIDX7.setPreferredSize(new Dimension(250, 30));
          TIDX7.setName("TIDX7");
          pnlRechercheMultiCritere.add(TIDX7, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin ----
          snMagasin.setName("snMagasin");
          pnlRechercheMultiCritere.add(snMagasin, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- TIDX10 ----
          TIDX10.setText("Condition commissionnement");
          TIDX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX10.setToolTipText("Tri sur cet argument");
          TIDX10.setFont(new Font("sansserif", Font.PLAIN, 14));
          TIDX10.setMaximumSize(new Dimension(250, 30));
          TIDX10.setMinimumSize(new Dimension(250, 30));
          TIDX10.setPreferredSize(new Dimension(250, 30));
          TIDX10.setName("TIDX10");
          pnlRechercheMultiCritere.add(TIDX10, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- ARG10 ----
          ARG10.setMaximumSize(new Dimension(60, 30));
          ARG10.setMinimumSize(new Dimension(60, 30));
          ARG10.setPreferredSize(new Dimension(60, 30));
          ARG10.setName("ARG10");
          pnlRechercheMultiCritere.add(ARG10, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlRecherches.add(pnlRechercheMultiCritere, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlRecherches);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX8);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX5);
    RB_GRP.add(TIDX9);
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDX10);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlRecherches;
  private SNPanelTitre pnlRechercheCreation;
  private SNPanel pnlRepresentant;
  private SNPanel pnlRepresentantRecherche;
  private SNLabelChamp lbRepresentantRecherche;
  private SNRepresentant snRepresentantRecherche;
  private SNPanel pnlRepresentantSaisie;
  private SNLabelChamp lbRepresentantSaisie;
  private XRiTextField INDREP;
  private SNPanel pnlEtablissement;
  private SNPanel pnlEtablissementRecherche;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNPanel pnlEtablissementDuplication;
  private SNLabelChamp lbEtablissementDuplication;
  private SNEtablissement snEtablissementDuplication;
  private SNPanelTitre pnlRechercheMultiCritere;
  private XRiRadioButton TIDX2;
  private XRiTextField ARG2;
  private XRiRadioButton TIDX8;
  private XRiTextField ARG8;
  private SNLabelChamp sNLabelChamp1;
  private SNPanel pnlPaysCodePostal;
  private SNPays snPays;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG3;
  private XRiRadioButton TIDX4;
  private XRiTextField ARG4;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG5;
  private XRiRadioButton TIDX9;
  private SNCodePostalCommune snCodePostalCommune;
  private XRiRadioButton TIDX7;
  private SNMagasin snMagasin;
  private XRiRadioButton TIDX10;
  private XRiTextField ARG10;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
