
package ri.serien.libecranrpg.vgvm.VGVM13RF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGVM13RF_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM13RF_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDX20.setValeurs("1", "TIDX20");
    TIDX17.setValeurs("1", "TIDX17");
    TIDX16.setValeurs("1", "TIDX16");
    TIDX12.setValeurs("1", "TIDX12");
    TIDX11.setValeurs("1", "TIDX11");
    TIDX10.setValeurs("1", "TIDX10");
    TIDX9.setValeurs("1", "TIDX9");
    TIDX8.setValeurs("1", "TIDX8");
    TIDX7.setValeurs("1", "TIDX7");
    TIDX6.setValeurs("1", "TIDX6");
    TIDX5.setValeurs("1", "TIDX5");
    TIDX4.setValeurs("1", "TIDX4");
    TIDX14.setValeurs("1", "TIDX14");
    TIDX3.setValeurs("1", "TIDX3");
    TIDX13.setValeurs("1", "TIDX13");
    TIDX1.setValeurs("1", "TIDX1");
    OPTDAT.setValeurs("S", buttonGroup1);
    OPTDAT_P.setValeurs("P");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - LOGISTIQUE @CMDE@"));
    
    

    p_bpresentation.setCodeEtablissement(WETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(2, 51);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    WETB = new XRiTextField();
    WNTO = new XRiTextField();
    OBJ_35 = new JLabel();
    R13SEL = new XRiTextField();
    OBJ_89 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX3 = new XRiRadioButton();
    TIDX11 = new XRiRadioButton();
    TIDX16 = new XRiRadioButton();
    TIDX12 = new XRiRadioButton();
    OBJ_142 = new JLabel();
    TIDX17 = new XRiRadioButton();
    TIDX20 = new XRiRadioButton();
    OBJ_163 = new JLabel();
    TIDX6 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    TIDX9 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    TIDX10 = new XRiRadioButton();
    TIDX14 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    OPTDAT = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    OPTDAT_P = new XRiRadioButton();
    TIDX13 = new XRiRadioButton();
    VOLDEB = new XRiTextField();
    VOLFIN = new XRiTextField();
    PDSDEB = new XRiTextField();
    PDSFIN = new XRiTextField();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    CRTDEB = new XRiTextField();
    ZGEDEB = new XRiTextField();
    ZGEFIN = new XRiTextField();
    CLIDEB = new XRiTextField();
    CLIFIN = new XRiTextField();
    CRTFIN = new XRiTextField();
    ORDDEB = new XRiTextField();
    ORDFIN = new XRiTextField();
    CDPDEB = new XRiTextField();
    CDPFIN = new XRiTextField();
    M2PDEB = new XRiTextField();
    M2PFIN = new XRiTextField();
    LGMDEB = new XRiTextField();
    LGMFIN = new XRiTextField();
    PREDEB = new XRiTextField();
    PREFIN = new XRiTextField();
    CANDEB = new XRiTextField();
    CANFIN = new XRiTextField();
    CATDEB = new XRiTextField();
    CATFIN = new XRiTextField();
    VDEDEB = new XRiTextField();
    VDEFIN = new XRiTextField();
    MAGDEB = new XRiTextField();
    MAGFIN = new XRiTextField();
    MEXDEB = new XRiTextField();
    MEXFIN = new XRiTextField();
    CTRDEB = new XRiTextField();
    CTRFIN = new XRiTextField();
    LIVDEB = new XRiTextField();
    LIVFIN = new XRiTextField();
    OBJ_51 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_106 = new JLabel();
    OBJ_112 = new JLabel();
    OBJ_118 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_131 = new JLabel();
    OBJ_143 = new JLabel();
    OBJ_150 = new JLabel();
    OBJ_157 = new JLabel();
    OBJ_162 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_135 = new JLabel();
    OBJ_138 = new JLabel();
    IN7DEB = new XRiTextField();
    IN7FIN = new XRiTextField();
    OBJ_53 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_97 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_147 = new JLabel();
    OBJ_154 = new JLabel();
    OBJ_161 = new JLabel();
    OBJ_145 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    buttonGroup2 = new ButtonGroup();
    buttonGroup1 = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Logistique : pr\u00e9paration de commandes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_34 ----
          OBJ_34.setText("Etablissement");
          OBJ_34.setName("OBJ_34");
          p_tete_gauche.add(OBJ_34);
          OBJ_34.setBounds(5, 0, 93, 28);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(125, 0, 40, WETB.getPreferredSize().height);

          //---- WNTO ----
          WNTO.setComponentPopupMenu(BTD);
          WNTO.setName("WNTO");
          p_tete_gauche.add(WNTO);
          WNTO.setBounds(305, 0, 50, WNTO.getPreferredSize().height);

          //---- OBJ_35 ----
          OBJ_35.setText("Pr\u00e9paration");
          OBJ_35.setName("OBJ_35");
          p_tete_gauche.add(OBJ_35);
          OBJ_35.setBounds(210, 0, 93, 28);

          //---- R13SEL ----
          R13SEL.setComponentPopupMenu(BTD);
          R13SEL.setName("R13SEL");
          p_tete_gauche.add(R13SEL);
          R13SEL.setBounds(450, 0, 60, R13SEL.getPreferredSize().height);

          //---- OBJ_89 ----
          OBJ_89.setText("Etats");
          OBJ_89.setName("OBJ_89");
          p_tete_gauche.add(OBJ_89);
          OBJ_89.setBounds(400, 0, 50, 28);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Affichage par num\u00e9ro");
              riSousMenu_bt6.setToolTipText("Affichage des bons par num\u00e9ro de pr\u00e9paration");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(730, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("S\u00e9lection des commandes"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- TIDX3 ----
            TIDX3.setText("Regroupement transport");
            TIDX3.setName("TIDX3");
            panel1.add(TIDX3);
            TIDX3.setBounds(40, 91, 170, 25);

            //---- TIDX11 ----
            TIDX11.setText("M\u00b2 au plancher maxmum");
            TIDX11.setName("TIDX11");
            panel1.add(TIDX11);
            TIDX11.setBounds(40, 345, 170, 23);

            //---- TIDX16 ----
            TIDX16.setText("Code client");
            TIDX16.setName("TIDX16");
            panel1.add(TIDX16);
            TIDX16.setBounds(40, 399, 170, 27);

            //---- TIDX12 ----
            TIDX12.setText("Longueur maximum");
            TIDX12.setName("TIDX12");
            panel1.add(TIDX12);
            TIDX12.setBounds(40, 371, 170, 25);

            //---- OBJ_142 ----
            OBJ_142.setText("Cat\u00e9gorie client");
            OBJ_142.setName("OBJ_142");
            panel1.add(OBJ_142);
            OBJ_142.setBounds(40, 429, 170, 24);

            //---- TIDX17 ----
            TIDX17.setText("Pr\u00e9parateur");
            TIDX17.setName("TIDX17");
            panel1.add(TIDX17);
            TIDX17.setBounds(40, 456, 170, 26);

            //---- TIDX20 ----
            TIDX20.setText("Vendeur");
            TIDX20.setName("TIDX20");
            panel1.add(TIDX20);
            TIDX20.setBounds(40, 485, 170, 23);

            //---- OBJ_163 ----
            OBJ_163.setText("Code s\u00e9lection");
            OBJ_163.setName("OBJ_163");
            panel1.add(OBJ_163);
            OBJ_163.setBounds(40, 511, 170, 25);

            //---- TIDX6 ----
            TIDX6.setText("Zone G\u00e9ographique");
            TIDX6.setName("TIDX6");
            panel1.add(TIDX6);
            TIDX6.setBounds(40, 205, 170, 23);

            //---- TIDX1 ----
            TIDX1.setText("Dates de livraison");
            TIDX1.setName("TIDX1");
            panel1.add(TIDX1);
            TIDX1.setBounds(40, 36, 170, 26);

            //---- TIDX7 ----
            TIDX7.setText("Mode d'exp\u00e9dition");
            TIDX7.setName("TIDX7");
            panel1.add(TIDX7);
            TIDX7.setBounds(40, 231, 170, 25);

            //---- TIDX9 ----
            TIDX9.setText("Volume maximum");
            TIDX9.setName("TIDX9");
            panel1.add(TIDX9);
            TIDX9.setBounds(40, 289, 170, 24);

            //---- TIDX8 ----
            TIDX8.setText("Code transporteur");
            TIDX8.setName("TIDX8");
            panel1.add(TIDX8);
            TIDX8.setBounds(40, 259, 170, 27);

            //---- TIDX10 ----
            TIDX10.setText("Poids  maximum");
            TIDX10.setName("TIDX10");
            panel1.add(TIDX10);
            TIDX10.setBounds(40, 316, 170, 26);

            //---- TIDX14 ----
            TIDX14.setText("Ordre / tourn\u00e9e");
            TIDX14.setName("TIDX14");
            panel1.add(TIDX14);
            TIDX14.setBounds(40, 119, 170, 27);

            //---- TIDX4 ----
            TIDX4.setText("Canal de vente");
            TIDX4.setName("TIDX4");
            panel1.add(TIDX4);
            TIDX4.setBounds(40, 149, 170, 24);

            //---- OPTDAT ----
            OPTDAT.setText("Souhait\u00e9e");
            OPTDAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPTDAT.setName("OPTDAT");
            panel1.add(OPTDAT);
            OPTDAT.setBounds(585, 36, 81, 26);

            //---- TIDX5 ----
            TIDX5.setText("Code postal");
            TIDX5.setName("TIDX5");
            panel1.add(TIDX5);
            TIDX5.setBounds(40, 176, 170, 26);

            //---- OPTDAT_P ----
            OPTDAT_P.setText("Pr\u00e9vue");
            OPTDAT_P.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPTDAT_P.setName("OPTDAT_P");
            panel1.add(OPTDAT_P);
            OPTDAT_P.setBounds(515, 36, 65, 26);

            //---- TIDX13 ----
            TIDX13.setText("Magasin");
            TIDX13.setName("TIDX13");
            panel1.add(TIDX13);
            TIDX13.setBounds(40, 65, 170, 23);

            //---- VOLDEB ----
            VOLDEB.setComponentPopupMenu(BTD);
            VOLDEB.setName("VOLDEB");
            panel1.add(VOLDEB);
            VOLDEB.setBounds(240, 287, 74, VOLDEB.getPreferredSize().height);

            //---- VOLFIN ----
            VOLFIN.setComponentPopupMenu(BTD);
            VOLFIN.setName("VOLFIN");
            panel1.add(VOLFIN);
            VOLFIN.setBounds(400, 287, 74, VOLFIN.getPreferredSize().height);

            //---- PDSDEB ----
            PDSDEB.setComponentPopupMenu(BTD);
            PDSDEB.setName("PDSDEB");
            panel1.add(PDSDEB);
            PDSDEB.setBounds(240, 315, 74, PDSDEB.getPreferredSize().height);

            //---- PDSFIN ----
            PDSFIN.setComponentPopupMenu(BTD);
            PDSFIN.setName("PDSFIN");
            panel1.add(PDSFIN);
            PDSFIN.setBounds(400, 315, 74, PDSFIN.getPreferredSize().height);

            //---- DATDEB ----
            DATDEB.setComponentPopupMenu(BTD);
            DATDEB.setName("DATDEB");
            panel1.add(DATDEB);
            DATDEB.setBounds(240, 35, 105, DATDEB.getPreferredSize().height);

            //---- DATFIN ----
            DATFIN.setComponentPopupMenu(BTD);
            DATFIN.setName("DATFIN");
            panel1.add(DATFIN);
            DATFIN.setBounds(400, 35, 105, DATFIN.getPreferredSize().height);

            //---- CRTDEB ----
            CRTDEB.setComponentPopupMenu(BTD);
            CRTDEB.setName("CRTDEB");
            panel1.add(CRTDEB);
            CRTDEB.setBounds(240, 91, 60, CRTDEB.getPreferredSize().height);

            //---- ZGEDEB ----
            ZGEDEB.setComponentPopupMenu(BTD);
            ZGEDEB.setName("ZGEDEB");
            panel1.add(ZGEDEB);
            ZGEDEB.setBounds(240, 203, 60, ZGEDEB.getPreferredSize().height);

            //---- ZGEFIN ----
            ZGEFIN.setComponentPopupMenu(BTD);
            ZGEFIN.setName("ZGEFIN");
            panel1.add(ZGEFIN);
            ZGEFIN.setBounds(400, 203, 60, ZGEFIN.getPreferredSize().height);

            //---- CLIDEB ----
            CLIDEB.setComponentPopupMenu(BTD);
            CLIDEB.setName("CLIDEB");
            panel1.add(CLIDEB);
            CLIDEB.setBounds(240, 399, 70, CLIDEB.getPreferredSize().height);

            //---- CLIFIN ----
            CLIFIN.setComponentPopupMenu(BTD);
            CLIFIN.setName("CLIFIN");
            panel1.add(CLIFIN);
            CLIFIN.setBounds(400, 399, 70, CLIFIN.getPreferredSize().height);

            //---- CRTFIN ----
            CRTFIN.setComponentPopupMenu(BTD);
            CRTFIN.setName("CRTFIN");
            panel1.add(CRTFIN);
            CRTFIN.setBounds(400, 91, 60, CRTFIN.getPreferredSize().height);

            //---- ORDDEB ----
            ORDDEB.setComponentPopupMenu(BTD);
            ORDDEB.setName("ORDDEB");
            panel1.add(ORDDEB);
            ORDDEB.setBounds(240, 119, 50, ORDDEB.getPreferredSize().height);

            //---- ORDFIN ----
            ORDFIN.setComponentPopupMenu(BTD);
            ORDFIN.setName("ORDFIN");
            panel1.add(ORDFIN);
            ORDFIN.setBounds(400, 119, 50, ORDFIN.getPreferredSize().height);

            //---- CDPDEB ----
            CDPDEB.setComponentPopupMenu(BTD);
            CDPDEB.setName("CDPDEB");
            panel1.add(CDPDEB);
            CDPDEB.setBounds(240, 175, 50, CDPDEB.getPreferredSize().height);

            //---- CDPFIN ----
            CDPFIN.setComponentPopupMenu(BTD);
            CDPFIN.setName("CDPFIN");
            panel1.add(CDPFIN);
            CDPFIN.setBounds(400, 175, 50, CDPFIN.getPreferredSize().height);

            //---- M2PDEB ----
            M2PDEB.setComponentPopupMenu(BTD);
            M2PDEB.setName("M2PDEB");
            panel1.add(M2PDEB);
            M2PDEB.setBounds(240, 343, 50, M2PDEB.getPreferredSize().height);

            //---- M2PFIN ----
            M2PFIN.setComponentPopupMenu(BTD);
            M2PFIN.setName("M2PFIN");
            panel1.add(M2PFIN);
            M2PFIN.setBounds(400, 343, 50, M2PFIN.getPreferredSize().height);

            //---- LGMDEB ----
            LGMDEB.setComponentPopupMenu(BTD);
            LGMDEB.setName("LGMDEB");
            panel1.add(LGMDEB);
            LGMDEB.setBounds(240, 371, 50, LGMDEB.getPreferredSize().height);

            //---- LGMFIN ----
            LGMFIN.setComponentPopupMenu(BTD);
            LGMFIN.setName("LGMFIN");
            panel1.add(LGMFIN);
            LGMFIN.setBounds(400, 371, 50, LGMFIN.getPreferredSize().height);

            //---- PREDEB ----
            PREDEB.setComponentPopupMenu(BTD);
            PREDEB.setName("PREDEB");
            panel1.add(PREDEB);
            PREDEB.setBounds(240, 455, 50, PREDEB.getPreferredSize().height);

            //---- PREFIN ----
            PREFIN.setComponentPopupMenu(BTD);
            PREFIN.setName("PREFIN");
            panel1.add(PREFIN);
            PREFIN.setBounds(400, 455, 50, PREFIN.getPreferredSize().height);

            //---- CANDEB ----
            CANDEB.setComponentPopupMenu(BTD);
            CANDEB.setName("CANDEB");
            panel1.add(CANDEB);
            CANDEB.setBounds(240, 147, 40, CANDEB.getPreferredSize().height);

            //---- CANFIN ----
            CANFIN.setComponentPopupMenu(BTD);
            CANFIN.setName("CANFIN");
            panel1.add(CANFIN);
            CANFIN.setBounds(400, 147, 40, CANFIN.getPreferredSize().height);

            //---- CATDEB ----
            CATDEB.setComponentPopupMenu(BTD);
            CATDEB.setName("CATDEB");
            panel1.add(CATDEB);
            CATDEB.setBounds(240, 427, 40, CATDEB.getPreferredSize().height);

            //---- CATFIN ----
            CATFIN.setComponentPopupMenu(BTD);
            CATFIN.setName("CATFIN");
            panel1.add(CATFIN);
            CATFIN.setBounds(400, 427, 40, CATFIN.getPreferredSize().height);

            //---- VDEDEB ----
            VDEDEB.setComponentPopupMenu(BTD);
            VDEDEB.setName("VDEDEB");
            panel1.add(VDEDEB);
            VDEDEB.setBounds(240, 483, 50, VDEDEB.getPreferredSize().height);

            //---- VDEFIN ----
            VDEFIN.setComponentPopupMenu(BTD);
            VDEFIN.setName("VDEFIN");
            panel1.add(VDEFIN);
            VDEFIN.setBounds(400, 483, 50, VDEFIN.getPreferredSize().height);

            //---- MAGDEB ----
            MAGDEB.setComponentPopupMenu(BTD);
            MAGDEB.setName("MAGDEB");
            panel1.add(MAGDEB);
            MAGDEB.setBounds(240, 63, 30, MAGDEB.getPreferredSize().height);

            //---- MAGFIN ----
            MAGFIN.setComponentPopupMenu(BTD);
            MAGFIN.setName("MAGFIN");
            panel1.add(MAGFIN);
            MAGFIN.setBounds(400, 63, 30, MAGFIN.getPreferredSize().height);

            //---- MEXDEB ----
            MEXDEB.setComponentPopupMenu(BTD);
            MEXDEB.setName("MEXDEB");
            panel1.add(MEXDEB);
            MEXDEB.setBounds(240, 231, 30, MEXDEB.getPreferredSize().height);

            //---- MEXFIN ----
            MEXFIN.setComponentPopupMenu(BTD);
            MEXFIN.setName("MEXFIN");
            panel1.add(MEXFIN);
            MEXFIN.setBounds(400, 231, 30, MEXFIN.getPreferredSize().height);

            //---- CTRDEB ----
            CTRDEB.setComponentPopupMenu(BTD);
            CTRDEB.setName("CTRDEB");
            panel1.add(CTRDEB);
            CTRDEB.setBounds(240, 259, 30, CTRDEB.getPreferredSize().height);

            //---- CTRFIN ----
            CTRFIN.setComponentPopupMenu(BTD);
            CTRFIN.setName("CTRFIN");
            panel1.add(CTRFIN);
            CTRFIN.setBounds(400, 259, 30, CTRFIN.getPreferredSize().height);

            //---- LIVDEB ----
            LIVDEB.setComponentPopupMenu(BTD);
            LIVDEB.setName("LIVDEB");
            panel1.add(LIVDEB);
            LIVDEB.setBounds(310, 400, 34, LIVDEB.getPreferredSize().height);

            //---- LIVFIN ----
            LIVFIN.setComponentPopupMenu(BTD);
            LIVFIN.setName("LIVFIN");
            panel1.add(LIVFIN);
            LIVFIN.setBounds(470, 400, 34, LIVFIN.getPreferredSize().height);

            //---- OBJ_51 ----
            OBJ_51.setText("de");
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(215, 67, 19, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("de");
            OBJ_61.setName("OBJ_61");
            panel1.add(OBJ_61);
            OBJ_61.setBounds(215, 95, 19, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("de");
            OBJ_71.setName("OBJ_71");
            panel1.add(OBJ_71);
            OBJ_71.setBounds(215, 123, 19, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("de");
            OBJ_78.setName("OBJ_78");
            panel1.add(OBJ_78);
            OBJ_78.setBounds(215, 151, 19, 20);

            //---- OBJ_91 ----
            OBJ_91.setText("de");
            OBJ_91.setName("OBJ_91");
            panel1.add(OBJ_91);
            OBJ_91.setBounds(215, 207, 19, 20);

            //---- OBJ_100 ----
            OBJ_100.setText("de");
            OBJ_100.setName("OBJ_100");
            panel1.add(OBJ_100);
            OBJ_100.setBounds(215, 235, 19, 20);

            //---- OBJ_106 ----
            OBJ_106.setText("de");
            OBJ_106.setName("OBJ_106");
            panel1.add(OBJ_106);
            OBJ_106.setBounds(215, 263, 19, 20);

            //---- OBJ_112 ----
            OBJ_112.setText("de");
            OBJ_112.setName("OBJ_112");
            panel1.add(OBJ_112);
            OBJ_112.setBounds(215, 291, 19, 20);

            //---- OBJ_118 ----
            OBJ_118.setText("de");
            OBJ_118.setName("OBJ_118");
            panel1.add(OBJ_118);
            OBJ_118.setBounds(215, 319, 19, 20);

            //---- OBJ_125 ----
            OBJ_125.setText("de");
            OBJ_125.setName("OBJ_125");
            panel1.add(OBJ_125);
            OBJ_125.setBounds(215, 347, 19, 20);

            //---- OBJ_131 ----
            OBJ_131.setText("de");
            OBJ_131.setName("OBJ_131");
            panel1.add(OBJ_131);
            OBJ_131.setBounds(215, 375, 19, 20);

            //---- OBJ_143 ----
            OBJ_143.setText("de");
            OBJ_143.setName("OBJ_143");
            panel1.add(OBJ_143);
            OBJ_143.setBounds(215, 431, 19, 20);

            //---- OBJ_150 ----
            OBJ_150.setText("de");
            OBJ_150.setName("OBJ_150");
            panel1.add(OBJ_150);
            OBJ_150.setBounds(215, 459, 19, 20);

            //---- OBJ_157 ----
            OBJ_157.setText("de");
            OBJ_157.setName("OBJ_157");
            panel1.add(OBJ_157);
            OBJ_157.setBounds(215, 487, 19, 20);

            //---- OBJ_162 ----
            OBJ_162.setText("de");
            OBJ_162.setName("OBJ_162");
            panel1.add(OBJ_162);
            OBJ_162.setBounds(215, 515, 19, 20);

            //---- OBJ_43 ----
            OBJ_43.setText("du");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(215, 39, 18, 20);

            //---- OBJ_45 ----
            OBJ_45.setText("au");
            OBJ_45.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(350, 40, 50, 20);

            //---- OBJ_84 ----
            OBJ_84.setText("du");
            OBJ_84.setName("OBJ_84");
            panel1.add(OBJ_84);
            OBJ_84.setBounds(215, 179, 18, 20);

            //---- OBJ_86 ----
            OBJ_86.setText("au");
            OBJ_86.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_86.setName("OBJ_86");
            panel1.add(OBJ_86);
            OBJ_86.setBounds(350, 179, 50, 20);

            //---- OBJ_135 ----
            OBJ_135.setText("au");
            OBJ_135.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_135.setName("OBJ_135");
            panel1.add(OBJ_135);
            OBJ_135.setBounds(350, 403, 50, 20);

            //---- OBJ_138 ----
            OBJ_138.setText("du");
            OBJ_138.setName("OBJ_138");
            panel1.add(OBJ_138);
            OBJ_138.setBounds(215, 403, 18, 20);

            //---- IN7DEB ----
            IN7DEB.setComponentPopupMenu(BTD);
            IN7DEB.setName("IN7DEB");
            panel1.add(IN7DEB);
            IN7DEB.setBounds(240, 511, 20, IN7DEB.getPreferredSize().height);

            //---- IN7FIN ----
            IN7FIN.setComponentPopupMenu(BTD);
            IN7FIN.setName("IN7FIN");
            panel1.add(IN7FIN);
            IN7FIN.setBounds(400, 511, 20, IN7FIN.getPreferredSize().height);

            //---- OBJ_53 ----
            OBJ_53.setText("\u00e0");
            OBJ_53.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(350, 67, 50, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("\u00e0");
            OBJ_63.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_63.setName("OBJ_63");
            panel1.add(OBJ_63);
            OBJ_63.setBounds(350, 95, 50, 20);

            //---- OBJ_73 ----
            OBJ_73.setText("\u00e0");
            OBJ_73.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_73.setName("OBJ_73");
            panel1.add(OBJ_73);
            OBJ_73.setBounds(350, 123, 50, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("\u00e0");
            OBJ_80.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_80.setName("OBJ_80");
            panel1.add(OBJ_80);
            OBJ_80.setBounds(350, 151, 50, 20);

            //---- OBJ_93 ----
            OBJ_93.setText("\u00e0");
            OBJ_93.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_93.setName("OBJ_93");
            panel1.add(OBJ_93);
            OBJ_93.setBounds(350, 207, 50, 20);

            //---- OBJ_97 ----
            OBJ_97.setText("\u00e0");
            OBJ_97.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_97.setName("OBJ_97");
            panel1.add(OBJ_97);
            OBJ_97.setBounds(350, 235, 50, 20);

            //---- OBJ_103 ----
            OBJ_103.setText("\u00e0");
            OBJ_103.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_103.setName("OBJ_103");
            panel1.add(OBJ_103);
            OBJ_103.setBounds(350, 263, 50, 20);

            //---- OBJ_109 ----
            OBJ_109.setText("\u00e0");
            OBJ_109.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_109.setName("OBJ_109");
            panel1.add(OBJ_109);
            OBJ_109.setBounds(350, 291, 50, 20);

            //---- OBJ_115 ----
            OBJ_115.setText("\u00e0");
            OBJ_115.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_115.setName("OBJ_115");
            panel1.add(OBJ_115);
            OBJ_115.setBounds(350, 319, 50, 20);

            //---- OBJ_122 ----
            OBJ_122.setText("\u00e0");
            OBJ_122.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_122.setName("OBJ_122");
            panel1.add(OBJ_122);
            OBJ_122.setBounds(350, 347, 50, 20);

            //---- OBJ_128 ----
            OBJ_128.setText("\u00e0");
            OBJ_128.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_128.setName("OBJ_128");
            panel1.add(OBJ_128);
            OBJ_128.setBounds(350, 375, 50, 20);

            //---- OBJ_147 ----
            OBJ_147.setText("\u00e0");
            OBJ_147.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_147.setName("OBJ_147");
            panel1.add(OBJ_147);
            OBJ_147.setBounds(350, 459, 50, 20);

            //---- OBJ_154 ----
            OBJ_154.setText("\u00e0");
            OBJ_154.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_154.setName("OBJ_154");
            panel1.add(OBJ_154);
            OBJ_154.setBounds(350, 487, 50, 20);

            //---- OBJ_161 ----
            OBJ_161.setText("\u00e0");
            OBJ_161.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_161.setName("OBJ_161");
            panel1.add(OBJ_161);
            OBJ_161.setBounds(350, 515, 50, 20);

            //---- OBJ_145 ----
            OBJ_145.setText("\u00e0");
            OBJ_145.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_145.setName("OBJ_145");
            panel1.add(OBJ_145);
            OBJ_145.setBounds(350, 431, 50, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 704, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- buttonGroup2 ----
    buttonGroup2.add(TIDX3);
    buttonGroup2.add(TIDX11);
    buttonGroup2.add(TIDX16);
    buttonGroup2.add(TIDX12);
    buttonGroup2.add(TIDX17);
    buttonGroup2.add(TIDX20);
    buttonGroup2.add(TIDX6);
    buttonGroup2.add(TIDX1);
    buttonGroup2.add(TIDX7);
    buttonGroup2.add(TIDX9);
    buttonGroup2.add(TIDX8);
    buttonGroup2.add(TIDX10);
    buttonGroup2.add(TIDX14);
    buttonGroup2.add(TIDX4);
    buttonGroup2.add(TIDX5);
    buttonGroup2.add(TIDX13);

    //---- buttonGroup1 ----
    buttonGroup1.add(OPTDAT);
    buttonGroup1.add(OPTDAT_P);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_34;
  private XRiTextField WETB;
  private XRiTextField WNTO;
  private JLabel OBJ_35;
  private XRiTextField R13SEL;
  private JLabel OBJ_89;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX11;
  private XRiRadioButton TIDX16;
  private XRiRadioButton TIDX12;
  private JLabel OBJ_142;
  private XRiRadioButton TIDX17;
  private XRiRadioButton TIDX20;
  private JLabel OBJ_163;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX7;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDX8;
  private XRiRadioButton TIDX10;
  private XRiRadioButton TIDX14;
  private XRiRadioButton TIDX4;
  private XRiRadioButton OPTDAT;
  private XRiRadioButton TIDX5;
  private XRiRadioButton OPTDAT_P;
  private XRiRadioButton TIDX13;
  private XRiTextField VOLDEB;
  private XRiTextField VOLFIN;
  private XRiTextField PDSDEB;
  private XRiTextField PDSFIN;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private XRiTextField CRTDEB;
  private XRiTextField ZGEDEB;
  private XRiTextField ZGEFIN;
  private XRiTextField CLIDEB;
  private XRiTextField CLIFIN;
  private XRiTextField CRTFIN;
  private XRiTextField ORDDEB;
  private XRiTextField ORDFIN;
  private XRiTextField CDPDEB;
  private XRiTextField CDPFIN;
  private XRiTextField M2PDEB;
  private XRiTextField M2PFIN;
  private XRiTextField LGMDEB;
  private XRiTextField LGMFIN;
  private XRiTextField PREDEB;
  private XRiTextField PREFIN;
  private XRiTextField CANDEB;
  private XRiTextField CANFIN;
  private XRiTextField CATDEB;
  private XRiTextField CATFIN;
  private XRiTextField VDEDEB;
  private XRiTextField VDEFIN;
  private XRiTextField MAGDEB;
  private XRiTextField MAGFIN;
  private XRiTextField MEXDEB;
  private XRiTextField MEXFIN;
  private XRiTextField CTRDEB;
  private XRiTextField CTRFIN;
  private XRiTextField LIVDEB;
  private XRiTextField LIVFIN;
  private JLabel OBJ_51;
  private JLabel OBJ_61;
  private JLabel OBJ_71;
  private JLabel OBJ_78;
  private JLabel OBJ_91;
  private JLabel OBJ_100;
  private JLabel OBJ_106;
  private JLabel OBJ_112;
  private JLabel OBJ_118;
  private JLabel OBJ_125;
  private JLabel OBJ_131;
  private JLabel OBJ_143;
  private JLabel OBJ_150;
  private JLabel OBJ_157;
  private JLabel OBJ_162;
  private JLabel OBJ_43;
  private JLabel OBJ_45;
  private JLabel OBJ_84;
  private JLabel OBJ_86;
  private JLabel OBJ_135;
  private JLabel OBJ_138;
  private XRiTextField IN7DEB;
  private XRiTextField IN7FIN;
  private JLabel OBJ_53;
  private JLabel OBJ_63;
  private JLabel OBJ_73;
  private JLabel OBJ_80;
  private JLabel OBJ_93;
  private JLabel OBJ_97;
  private JLabel OBJ_103;
  private JLabel OBJ_109;
  private JLabel OBJ_115;
  private JLabel OBJ_122;
  private JLabel OBJ_128;
  private JLabel OBJ_147;
  private JLabel OBJ_154;
  private JLabel OBJ_161;
  private JLabel OBJ_145;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private ButtonGroup buttonGroup2;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
