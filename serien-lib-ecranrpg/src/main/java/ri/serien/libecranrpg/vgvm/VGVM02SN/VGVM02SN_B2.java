
package ri.serien.libecranrpg.vgvm.VGVM02SN;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM02SN_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM02SN_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    SNIN2.setValeursSelection("O", "N");
    SNIN1.setValeursSelection("O", "N");
    SNIN4.setValeursSelection("O", "N");
    SNIN3.setValeursSelection("O", "N");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    SNC32.setEnabled(lexique.isPresent("SNC32"));
    SNC31.setEnabled(lexique.isPresent("SNC31"));
    SNC22.setEnabled(lexique.isPresent("SNC22"));
    SNC21.setEnabled(lexique.isPresent("SNC21"));
    SNC12.setEnabled(lexique.isPresent("SNC12"));
    SNC11.setEnabled(lexique.isPresent("SNC11"));
    SNLG3.setEnabled(lexique.isPresent("SNLG3"));
    SNLG2.setEnabled(lexique.isPresent("SNLG2"));
    SNLG1.setEnabled(lexique.isPresent("SNLG1"));
    SNTC3.setEnabled(lexique.isPresent("SNTC3"));
    SNTC2.setEnabled(lexique.isPresent("SNTC2"));
    SNTC1.setEnabled(lexique.isPresent("SNTC1"));
    // SNIN4.setSelected(lexique.HostFieldGetData("SNIN4").equalsIgnoreCase("O"));
    // SNIN3.setSelected(lexique.HostFieldGetData("SNIN3").equalsIgnoreCase("O"));
    SNF32.setEnabled(lexique.isPresent("SNF32"));
    SND32.setEnabled(lexique.isPresent("SND32"));
    SNF31.setEnabled(lexique.isPresent("SNF31"));
    SND31.setEnabled(lexique.isPresent("SND31"));
    SNF22.setEnabled(lexique.isPresent("SNF22"));
    SND22.setEnabled(lexique.isPresent("SND22"));
    SNF21.setEnabled(lexique.isPresent("SNF21"));
    SND21.setEnabled(lexique.isPresent("SND21"));
    SNF12.setEnabled(lexique.isPresent("SNF12"));
    SND12.setEnabled(lexique.isPresent("SND12"));
    SNF11.setEnabled(lexique.isPresent("SNF11"));
    SND11.setEnabled(lexique.isPresent("SND11"));
    // SNIN2.setSelected(lexique.HostFieldGetData("SNIN2").equalsIgnoreCase("O"));
    // SNIN1.setSelected(lexique.HostFieldGetData("SNIN1").equalsIgnoreCase("O"));
    SNLC3.setEnabled(lexique.isPresent("SNLC3"));
    SNLC2.setEnabled(lexique.isPresent("SNLC2"));
    SNLC1.setEnabled(lexique.isPresent("SNLC1"));
    SNLIB.setEnabled(lexique.isPresent("SNLIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@ STATISTIQUES"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (SNIN4.isSelected())
    // lexique.HostFieldPutData("SNIN4", 0, "O");
    // else
    // lexique.HostFieldPutData("SNIN4", 0, "N");
    // if (SNIN3.isSelected())
    // lexique.HostFieldPutData("SNIN3", 0, "O");
    // else
    // lexique.HostFieldPutData("SNIN3", 0, "N");
    // if (SNIN2.isSelected())
    // lexique.HostFieldPutData("SNIN2", 0, "O");
    // else
    // lexique.HostFieldPutData("SNIN2", 0, "N");
    // if (SNIN1.isSelected())
    // lexique.HostFieldPutData("SNIN1", 0, "O");
    // else
    // lexique.HostFieldPutData("SNIN1", 0, "N");
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_55 = new JLabel();
    SNLIB = new XRiTextField();
    xTitledPanel1 = new JXTitledPanel();
    SNLC1 = new XRiTextField();
    SNLC2 = new XRiTextField();
    SNLC3 = new XRiTextField();
    SND11 = new XRiTextField();
    SNF11 = new XRiTextField();
    SND12 = new XRiTextField();
    SNF12 = new XRiTextField();
    SND21 = new XRiTextField();
    SNF21 = new XRiTextField();
    SND22 = new XRiTextField();
    SNF22 = new XRiTextField();
    SND31 = new XRiTextField();
    SNF31 = new XRiTextField();
    SND32 = new XRiTextField();
    SNF32 = new XRiTextField();
    OBJ_93 = new JLabel();
    SNIN3 = new XRiCheckBox();
    SNIN4 = new XRiCheckBox();
    OBJ_94 = new JLabel();
    OBJ_96 = new JLabel();
    SNTC1 = new XRiTextField();
    SNTC2 = new XRiTextField();
    SNTC3 = new XRiTextField();
    OBJ_97 = new JLabel();
    SNLG1 = new XRiTextField();
    SNLG2 = new XRiTextField();
    SNLG3 = new XRiTextField();
    OBJ_95 = new JLabel();
    SNC11 = new XRiTextField();
    SNC12 = new XRiTextField();
    SNC21 = new XRiTextField();
    SNC22 = new XRiTextField();
    SNC31 = new XRiTextField();
    SNC32 = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    SNIN1 = new XRiCheckBox();
    SNIN2 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_55 ----
            OBJ_55.setText("Libell\u00e9");
            OBJ_55.setName("OBJ_55");
            panel2.add(OBJ_55);
            OBJ_55.setBounds(15, 12, 61, 24);

            //---- SNLIB ----
            SNLIB.setComponentPopupMenu(BTD);
            SNLIB.setName("SNLIB");
            panel2.add(SNLIB);
            SNLIB.setBounds(115, 10, 310, SNLIB.getPreferredSize().height);
          }

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Crit\u00e8res");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- SNLC1 ----
            SNLC1.setComponentPopupMenu(BTD);
            SNLC1.setName("SNLC1");
            xTitledPanel1ContentContainer.add(SNLC1);
            SNLC1.setBounds(145, 40, 310, SNLC1.getPreferredSize().height);

            //---- SNLC2 ----
            SNLC2.setComponentPopupMenu(BTD);
            SNLC2.setName("SNLC2");
            xTitledPanel1ContentContainer.add(SNLC2);
            SNLC2.setBounds(145, 115, 310, SNLC2.getPreferredSize().height);

            //---- SNLC3 ----
            SNLC3.setComponentPopupMenu(BTD);
            SNLC3.setName("SNLC3");
            xTitledPanel1ContentContainer.add(SNLC3);
            SNLC3.setBounds(145, 190, 310, SNLC3.getPreferredSize().height);

            //---- SND11 ----
            SND11.setComponentPopupMenu(BTD);
            SND11.setName("SND11");
            xTitledPanel1ContentContainer.add(SND11);
            SND11.setBounds(455, 40, 160, SND11.getPreferredSize().height);

            //---- SNF11 ----
            SNF11.setComponentPopupMenu(BTD);
            SNF11.setName("SNF11");
            xTitledPanel1ContentContainer.add(SNF11);
            SNF11.setBounds(615, 40, 160, SNF11.getPreferredSize().height);

            //---- SND12 ----
            SND12.setComponentPopupMenu(BTD);
            SND12.setName("SND12");
            xTitledPanel1ContentContainer.add(SND12);
            SND12.setBounds(455, 70, 160, SND12.getPreferredSize().height);

            //---- SNF12 ----
            SNF12.setComponentPopupMenu(BTD);
            SNF12.setName("SNF12");
            xTitledPanel1ContentContainer.add(SNF12);
            SNF12.setBounds(615, 70, 160, SNF12.getPreferredSize().height);

            //---- SND21 ----
            SND21.setComponentPopupMenu(BTD);
            SND21.setName("SND21");
            xTitledPanel1ContentContainer.add(SND21);
            SND21.setBounds(455, 115, 160, SND21.getPreferredSize().height);

            //---- SNF21 ----
            SNF21.setComponentPopupMenu(BTD);
            SNF21.setName("SNF21");
            xTitledPanel1ContentContainer.add(SNF21);
            SNF21.setBounds(615, 115, 160, SNF21.getPreferredSize().height);

            //---- SND22 ----
            SND22.setComponentPopupMenu(BTD);
            SND22.setName("SND22");
            xTitledPanel1ContentContainer.add(SND22);
            SND22.setBounds(455, 145, 160, SND22.getPreferredSize().height);

            //---- SNF22 ----
            SNF22.setComponentPopupMenu(BTD);
            SNF22.setName("SNF22");
            xTitledPanel1ContentContainer.add(SNF22);
            SNF22.setBounds(615, 145, 160, SNF22.getPreferredSize().height);

            //---- SND31 ----
            SND31.setComponentPopupMenu(BTD);
            SND31.setName("SND31");
            xTitledPanel1ContentContainer.add(SND31);
            SND31.setBounds(455, 190, 160, SND31.getPreferredSize().height);

            //---- SNF31 ----
            SNF31.setComponentPopupMenu(BTD);
            SNF31.setName("SNF31");
            xTitledPanel1ContentContainer.add(SNF31);
            SNF31.setBounds(615, 190, 160, SNF31.getPreferredSize().height);

            //---- SND32 ----
            SND32.setComponentPopupMenu(BTD);
            SND32.setName("SND32");
            xTitledPanel1ContentContainer.add(SND32);
            SND32.setBounds(455, 220, 160, SND32.getPreferredSize().height);

            //---- SNF32 ----
            SNF32.setComponentPopupMenu(BTD);
            SNF32.setName("SNF32");
            xTitledPanel1ContentContainer.add(SNF32);
            SNF32.setBounds(615, 220, 160, SNF32.getPreferredSize().height);

            //---- OBJ_93 ----
            OBJ_93.setText("D\u00e9but");
            OBJ_93.setName("OBJ_93");
            xTitledPanel1ContentContainer.add(OBJ_93);
            OBJ_93.setBounds(457, 19, 58, 20);

            //---- SNIN3 ----
            SNIN3.setText("Total");
            SNIN3.setComponentPopupMenu(BTD);
            SNIN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNIN3.setName("SNIN3");
            xTitledPanel1ContentContainer.add(SNIN3);
            SNIN3.setBounds(60, 44, 52, 20);

            //---- SNIN4 ----
            SNIN4.setText("Total");
            SNIN4.setComponentPopupMenu(BTD);
            SNIN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNIN4.setName("SNIN4");
            xTitledPanel1ContentContainer.add(SNIN4);
            SNIN4.setBounds(60, 119, 52, 20);

            //---- OBJ_94 ----
            OBJ_94.setText("Fin");
            OBJ_94.setName("OBJ_94");
            xTitledPanel1ContentContainer.add(OBJ_94);
            OBJ_94.setBounds(617, 19, 40, 20);

            //---- OBJ_96 ----
            OBJ_96.setText("Code");
            OBJ_96.setName("OBJ_96");
            xTitledPanel1ContentContainer.add(OBJ_96);
            OBJ_96.setBounds(17, 20, 42, 18);

            //---- SNTC1 ----
            SNTC1.setComponentPopupMenu(BTD);
            SNTC1.setName("SNTC1");
            xTitledPanel1ContentContainer.add(SNTC1);
            SNTC1.setBounds(15, 40, 40, SNTC1.getPreferredSize().height);

            //---- SNTC2 ----
            SNTC2.setComponentPopupMenu(BTD);
            SNTC2.setName("SNTC2");
            xTitledPanel1ContentContainer.add(SNTC2);
            SNTC2.setBounds(15, 115, 40, SNTC2.getPreferredSize().height);

            //---- SNTC3 ----
            SNTC3.setComponentPopupMenu(BTD);
            SNTC3.setName("SNTC3");
            xTitledPanel1ContentContainer.add(SNTC3);
            SNTC3.setBounds(15, 190, 40, SNTC3.getPreferredSize().height);

            //---- OBJ_97 ----
            OBJ_97.setText("Lg");
            OBJ_97.setName("OBJ_97");
            xTitledPanel1ContentContainer.add(OBJ_97);
            OBJ_97.setBounds(117, 20, 25, 18);

            //---- SNLG1 ----
            SNLG1.setComponentPopupMenu(BTD);
            SNLG1.setName("SNLG1");
            xTitledPanel1ContentContainer.add(SNLG1);
            SNLG1.setBounds(115, 40, 30, SNLG1.getPreferredSize().height);

            //---- SNLG2 ----
            SNLG2.setComponentPopupMenu(BTD);
            SNLG2.setName("SNLG2");
            xTitledPanel1ContentContainer.add(SNLG2);
            SNLG2.setBounds(115, 115, 30, SNLG2.getPreferredSize().height);

            //---- SNLG3 ----
            SNLG3.setComponentPopupMenu(BTD);
            SNLG3.setName("SNLG3");
            xTitledPanel1ContentContainer.add(SNLG3);
            SNLG3.setBounds(115, 190, 30, SNLG3.getPreferredSize().height);

            //---- OBJ_95 ----
            OBJ_95.setText("I/E");
            OBJ_95.setName("OBJ_95");
            xTitledPanel1ContentContainer.add(OBJ_95);
            OBJ_95.setBounds(777, 19, 19, 20);

            //---- SNC11 ----
            SNC11.setComponentPopupMenu(BTD);
            SNC11.setName("SNC11");
            xTitledPanel1ContentContainer.add(SNC11);
            SNC11.setBounds(775, 40, 20, SNC11.getPreferredSize().height);

            //---- SNC12 ----
            SNC12.setComponentPopupMenu(BTD);
            SNC12.setName("SNC12");
            xTitledPanel1ContentContainer.add(SNC12);
            SNC12.setBounds(775, 70, 20, SNC12.getPreferredSize().height);

            //---- SNC21 ----
            SNC21.setComponentPopupMenu(BTD);
            SNC21.setName("SNC21");
            xTitledPanel1ContentContainer.add(SNC21);
            SNC21.setBounds(775, 115, 20, SNC21.getPreferredSize().height);

            //---- SNC22 ----
            SNC22.setComponentPopupMenu(BTD);
            SNC22.setName("SNC22");
            xTitledPanel1ContentContainer.add(SNC22);
            SNC22.setBounds(775, 145, 20, SNC22.getPreferredSize().height);

            //---- SNC31 ----
            SNC31.setComponentPopupMenu(BTD);
            SNC31.setName("SNC31");
            xTitledPanel1ContentContainer.add(SNC31);
            SNC31.setBounds(775, 190, 20, SNC31.getPreferredSize().height);

            //---- SNC32 ----
            SNC32.setComponentPopupMenu(BTD);
            SNC32.setName("SNC32");
            xTitledPanel1ContentContainer.add(SNC32);
            SNC32.setBounds(775, 220, 20, SNC32.getPreferredSize().height);
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Option(s)");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- SNIN1 ----
            SNIN1.setText("Statistiques journali\u00e8res");
            SNIN1.setComponentPopupMenu(BTD);
            SNIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNIN1.setName("SNIN1");
            xTitledPanel2ContentContainer.add(SNIN1);
            SNIN1.setBounds(15, 20, 183, 20);

            //---- SNIN2 ----
            SNIN2.setText("Statistiques \u00e0 l'heure");
            SNIN2.setComponentPopupMenu(BTD);
            SNIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNIN2.setName("SNIN2");
            xTitledPanel2ContentContainer.add(SNIN2);
            SNIN2.setBounds(220, 20, 148, 20);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 835, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 835, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(36, Short.MAX_VALUE))
          );
          p_contenuLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {panel2, xTitledPanel1});
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JLabel OBJ_55;
  private XRiTextField SNLIB;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField SNLC1;
  private XRiTextField SNLC2;
  private XRiTextField SNLC3;
  private XRiTextField SND11;
  private XRiTextField SNF11;
  private XRiTextField SND12;
  private XRiTextField SNF12;
  private XRiTextField SND21;
  private XRiTextField SNF21;
  private XRiTextField SND22;
  private XRiTextField SNF22;
  private XRiTextField SND31;
  private XRiTextField SNF31;
  private XRiTextField SND32;
  private XRiTextField SNF32;
  private JLabel OBJ_93;
  private XRiCheckBox SNIN3;
  private XRiCheckBox SNIN4;
  private JLabel OBJ_94;
  private JLabel OBJ_96;
  private XRiTextField SNTC1;
  private XRiTextField SNTC2;
  private XRiTextField SNTC3;
  private JLabel OBJ_97;
  private XRiTextField SNLG1;
  private XRiTextField SNLG2;
  private XRiTextField SNLG3;
  private JLabel OBJ_95;
  private XRiTextField SNC11;
  private XRiTextField SNC12;
  private XRiTextField SNC21;
  private XRiTextField SNC22;
  private XRiTextField SNC31;
  private XRiTextField SNC32;
  private JXTitledPanel xTitledPanel2;
  private XRiCheckBox SNIN1;
  private XRiCheckBox SNIN2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
